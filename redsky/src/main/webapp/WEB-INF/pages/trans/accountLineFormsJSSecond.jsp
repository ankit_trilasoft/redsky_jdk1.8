<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<SCRIPT LANGUAGE="JavaScript">
var http66 = getHTTPObject66();
var http67 = getHTTPObject66();
function getHTTPObject66(){
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
function VATCalculateEst(){
    var originCountryCode=document.forms['accountLineForms'].elements['serviceOrder.originCountryCode'].value
    var destinationCountryCode=document.forms['accountLineForms'].elements['serviceOrder.destinationCountryCode'].value
    var url="findCountryCodeEU.html?ajax=1&decorator=simple&popup=true&originCountryCode=" + encodeURI(originCountryCode)+ "&destinationCountryCode=" + encodeURI(destinationCountryCode);
    http66.open("GET", url, true);
    http66.onreadystatechange = CountrycodeEUResponseEst;
    http66.send(null);
    }
    function CountrycodeEUResponseEst(){
    if (http66.readyState == 4) {
    var results = http66.responseText
    results = results.trim(); 
    if(results=='Ok') {
    findBilltoCodeVatNumberEst();
    }else{
    document.forms['accountLineForms'].elements['accountLine.estVatDescr'].value=1;
    getVatPercentEst();
    }     }  }
    function findBilltoCodeVatNumberEst(){ 
	   var billToCode=document.forms['accountLineForms'].elements['accountLine.billToCode'].value
	   var url="findBilltoCodeVatNumber.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billToCode);
	   http67.open("GET", url, true);
	   http67.onreadystatechange = billtoCodeVatNumberResponseEst;
	   http67.send(null); 
     }
      function billtoCodeVatNumberResponseEst(){
    if (http67.readyState == 4) {
    var results = http67.responseText
    results = results.trim();  
    if(results=='notOk') {
    findbillingCountryCodeEst();
    }else{
    results=results.substring(0,2) 
    if(results.indexOf("NL")  >= 0){
    document.forms['accountLineForms'].elements['accountLine.estVatDescr'].value=4;
    getVatPercentEst();
    }
    if(results.indexOf("CZ")  >= 0){
    document.forms['accountLineForms'].elements['accountLine.estVatDescr'].value=5;
    getVatPercentEst();
    }   }   }   }
    function getVatPercentEst(){ 
    	<c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){ 
        if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=='${entry.key}') {
        document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value='${entry.value}'; 
        calculateVatAmtEst();
        relo="yes"; 
       }  }
    </c:forEach>
      if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value==''){
      document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
      calculateVatAmtEst();
      }        
     
      
      
  	var payAccDate='';
  	 <c:if test="${accountInterface=='Y'}">
  	  payAccDate=document.forms['accountLineForms'].elements['accountLine.payAccDate'].value;
       </c:if>
         if (payAccDate!='') { 
          alert("You can not change the VAT as Sent To Acc has been already filled");
          document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${accountLine.payVatDescr}';
          } else{
  	var relo='';
  	<c:forEach var="entry" items="${payVatPercentList}" > 
  	if(relo==''){
  	  if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=='${entry.key}'){
  	      document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value='${entry.value}';
  	    calculateESTVatAmt();
  	      relo="yes";
  	      }   }
  	     </c:forEach>  	     
  	     if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value==''){
              document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0;
              calculateESTVatAmt();
        }  }
        </c:if>       
	} 
	 function findbillingCountryCodeEst(){ 
	   var billToCode=document.forms['accountLineForms'].elements['accountLine.billToCode'].value
	   var url="findbillingCountryCode.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billToCode);
	   http66.open("GET", url, true);
	   http66.onreadystatechange = billingCountryCodeResponseEst;
	   http66.send(null); 
     }
     function billingCountryCodeResponseEst(){
     if (http66.readyState == 4) {
    var results = http66.responseText
    results = results.trim(); 
    if(results=='notOk') {
    document.forms['accountLineForms'].elements['accountLine.estVatDescr'].value=2;
    getVatPercentEst();
    }else{
    results=results.substring(0,2) 
    if(results.indexOf("NL")  >= 0){
    document.forms['accountLineForms'].elements['accountLine.estVatDescr'].value=4;
    getVatPercentEst();
    } else if(results.indexOf("CZ")  >= 0){
    document.forms['accountLineForms'].elements['accountLine.estVatDescr'].value=5;
    getVatPercentEst();
    }  else{
    document.forms['accountLineForms'].elements['accountLine.estVatDescr'].value=3;
    getVatPercentEst();
    }    }   }   } 
    function calculateVatAmtEst(){
    	<c:if test="${systemDefaultVatCalculation=='true'}">
	var estVatAmt=0;
	if(document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value!=''){
	var estVatPercent=eval(document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value);
	var estimateRevenueAmount= eval(document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value);
	<c:if test="${contractType}">
	  estimateRevenueAmount= eval(document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value);
	</c:if>
	estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
	estVatAmt=Math.round(estVatAmt*10000)/10000; 
	document.forms['accountLineForms'].elements['accountLine.estVatAmt'].value=estVatAmt;
	}
    </c:if>
	}
	function checkFloatEstVat(temp)  { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.name;  
	if(temp.value>100){
	alert("You cannot enter more than 100% VAT ")
	document.forms['accountLineForms'].elements[fieldName].select();
    document.forms['accountLineForms'].elements[fieldName].value='0.00';
    document.forms['accountLineForms'].elements['accountLine.estVatAmt'].value=''; 
	return false;
	}  
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
       	  alert("Invalid data in vat%." ); 
          document.forms['accountLineForms'].elements[fieldName].select();
          document.forms['accountLineForms'].elements[fieldName].value='';
          document.forms['accountLineForms'].elements['accountLine.estVatAmt'].value=''; 
       	  return false;
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  } 
    if(check=='Invalid'){ 
    alert("Invalid data in vat%." ); 
    document.forms['accountLineForms'].elements[fieldName].select();
    document.forms['accountLineForms'].elements[fieldName].value='';
    document.forms['accountLineForms'].elements['accountLine.estVatAmt'].value=''; 
    return false;
    }  else{
    s=Math.round(s*10000)/10000;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.forms['accountLineForms'].elements[fieldName].value=value;
    calculateVatAmtEst();
    calculateESTVatAmt();
    return true;
    }  }
     function checkVatForReadonlyFieldsEst(var1){
    	 
    }
</script>



<SCRIPT LANGUAGE="JavaScript">
function VATCalculateRevision(){
    var originCountryCode=document.forms['accountLineForms'].elements['serviceOrder.originCountryCode'].value
    var destinationCountryCode=document.forms['accountLineForms'].elements['serviceOrder.destinationCountryCode'].value
    var url="findCountryCodeEU.html?ajax=1&decorator=simple&popup=true&originCountryCode=" + encodeURI(originCountryCode)+ "&destinationCountryCode=" + encodeURI(destinationCountryCode);
    http66.open("GET", url, true);
    http66.onreadystatechange = CountrycodeEUResponseRevision;
    http66.send(null);
    }
    function CountrycodeEUResponseRevision(){
    if (http66.readyState == 4) {
    var results = http66.responseText
    results = results.trim();  
    if(results=='Ok') {
    findBilltoCodeVatNumberRevision();
    }else{
    document.forms['accountLineForms'].elements['accountLine.revisionVatDescr'].value=1;
    getVatPercentRevision();
    }     }  }
    function findBilltoCodeVatNumberRevision(){ 
	   var billToCode=document.forms['accountLineForms'].elements['accountLine.billToCode'].value
	   var url="findBilltoCodeVatNumber.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billToCode);
	   http67.open("GET", url, true);
	   http67.onreadystatechange = billtoCodeVatNumberResponseRevision;
	   http67.send(null); 
     }
      function billtoCodeVatNumberResponseRevision(){
    if (http67.readyState == 4) {
    var results = http67.responseText
    results = results.trim();  
    if(results=='notOk') {
    findbillingCountryCodeRevision();
    }else{
    results=results.substring(0,2) 
    if(results.indexOf("NL")  >= 0){
    document.forms['accountLineForms'].elements['accountLine.revisionVatDescr'].value=4;
    getVatPercentRevision();
    }
    if(results.indexOf("CZ")  >= 0){
    document.forms['accountLineForms'].elements['accountLine.revisionVatDescr'].value=5;
    getVatPercentRevision();
    }   }   }   }
    function getVatPercentRevision(){ 
    	 <c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){ 
        if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=='${entry.key}') {
        document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value='${entry.value}'; 
        calculateVatAmtRevision();
        relo="yes"; 
       }  }
    </c:forEach>
      if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value==''){
      document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
      calculateVatAmtRevision();
      }  
    	var payAccDate='';
    	 <c:if test="${accountInterface=='Y'}">
    	  payAccDate=document.forms['accountLineForms'].elements['accountLine.payAccDate'].value;
         </c:if>
           if (payAccDate!='') { 
            alert("You can not change the VAT as Sent To Acc has been already filled");
            document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${accountLine.payVatDescr}';
            } else{
    	var relo='';
    	<c:forEach var="entry" items="${payVatPercentList}" > 
    	if(relo==''){
    	  if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=='${entry.key}'){
    	      document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value='${entry.value}';
    	    calculateREVVatAmt();
    	      relo="yes";
    	      }   }
    	     </c:forEach>  	     
    	     if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value==''){
                document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0;
                calculateREVVatAmt();
          }  }
          </c:if>        
	} 
	 function findbillingCountryCodeRevision(){ 
	   var billToCode=document.forms['accountLineForms'].elements['accountLine.billToCode'].value
	   var url="findbillingCountryCode.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billToCode);
	   http66.open("GET", url, true);
	   http66.onreadystatechange = billingCountryCodeResponseRevision;
	   http66.send(null); 
     }
     function billingCountryCodeResponseRevision(){
     if (http66.readyState == 4) {
    var results = http66.responseText
    results = results.trim(); 
    if(results=='notOk') {
    document.forms['accountLineForms'].elements['accountLine.revisionVatDescr'].value=2;
    getVatPercentRevision();
    }else{
    results=results.substring(0,2) 
    if(results.indexOf("NL")  >= 0){
    document.forms['accountLineForms'].elements['accountLine.revisionVatDescr'].value=4;
    getVatPercentRevision();
    } else if(results.indexOf("CZ")  >= 0){
    document.forms['accountLineForms'].elements['accountLine.revisionVatDescr'].value=5;
    getVatPercentRevision();
    }  else{
    document.forms['accountLineForms'].elements['accountLine.revisionVatDescr'].value=3;
    getVatPercentRevision();
    }    }   }   } 
    function calculateVatAmtRevision(){
    	 <c:if test="${systemDefaultVatCalculation=='true'}">
	var revisionVatAmt=0;
	if(document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value!=''){
	var revisionVatPercent=eval(document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value);
	var revisionRevenueAmount= eval(document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value);
	<c:if test="${contractType}">
	revisionRevenueAmount= eval(document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value);
	</c:if>
	revisionVatAmt=(revisionRevenueAmount*revisionVatPercent)/100;
	revisionVatAmt=Math.round(revisionVatAmt*10000)/10000; 
	document.forms['accountLineForms'].elements['accountLine.revisionVatAmt'].value=revisionVatAmt;
	}
    </c:if>
	}
	function checkFloatRevisionVat(temp)  { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.name;  
	if(temp.value>100){
	alert("You cannot enter more than 100% VAT ")
	document.forms['accountLineForms'].elements[fieldName].select();
    document.forms['accountLineForms'].elements[fieldName].value='0.00';
    document.forms['accountLineForms'].elements['accountLine.revisionVatAmt'].value=''; 
	return false;
	}  
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
       	  alert("Invalid data in vat%." ); 
          document.forms['accountLineForms'].elements[fieldName].select();
          document.forms['accountLineForms'].elements[fieldName].value='';
          document.forms['accountLineForms'].elements['accountLine.revisionVatAmt'].value=''; 
       	  return false;
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  } 
    if(check=='Invalid'){ 
    alert("Invalid data in vat%." ); 
    document.forms['accountLineForms'].elements[fieldName].select();
    document.forms['accountLineForms'].elements[fieldName].value='';
    document.forms['accountLineForms'].elements['accountLine.revisionVatAmt'].value=''; 
    return false;
    }  else{
    s=Math.round(s*10000)/10000;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.forms['accountLineForms'].elements[fieldName].value=value;
    calculateVatAmtRevision();
    calculateREVVatAmt();
    return true;
    }  }
	
	function getHTTPObject()
	 {
	     var xmlhttp;
	     if(window.XMLHttpRequest)
	     {
	         xmlhttp = new XMLHttpRequest();
	     }
	     else if (window.ActiveXObject)
	     {
	         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	         if (!xmlhttp)
	         {
	             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	         }
	     }
	     return xmlhttp;
	 }

	/* function autoCompleterAjaxCallChargeCode(){
		 <c:if test="${empty accountLine.id}">
		 document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value="";
		 document.forms['accountLineForms'].elements['accountLine.description'].value="";
		 document.forms['accountLineForms'].elements['accountLine.note'].value="";
		 </c:if>
	 	var ChargeName =document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
	 	var contract = '${billing.contract}';
	 	contract=contract.trim();
	 	if(contract!=""){
	 	if(ChargeName.length>=3){
	 	var data = 'ChargeCodeAutocompleteAjax.html?ajax=1&searchName='+ChargeName+'&contract='+contract+'&decorator=simple&popup=true';
	 	///alert("data--"+data)
	 		$( "#chargeCode").autocomplete({				 
	 		      source: data
	 		    });
	 	}
	 	else{
	 		$( "#chargeCode").autocomplete({				 
	 		      source: null
	 		    });
	 	}
	 	}
	 	else{
	 		alert("There is no pricing contract: Please select.");
	 	}
	 } */
	 
	 function autoCompleterAjaxCallChargeCode(divid,idCondition){
		 <configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
		 document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value='CHARGEVALUE';
		 document.getElementById('chargeCodeValidationMessage').value=idCondition;
		 <c:if test="${empty accountLine.id}">
		 document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value="";
		 document.forms['accountLineForms'].elements['accountLine.description'].value="";
		 document.forms['accountLineForms'].elements['accountLine.note'].value="";
		 </c:if>
		 	var category = document.forms['accountLineForms'].elements['accountLine.category'].value;
			var originCountry=document.forms['accountLineForms'].elements['serviceOrder.originCountryCode'].value;
			var destinationCountry=document.forms['accountLineForms'].elements['serviceOrder.destinationCountryCode'].value;
			var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value
			var mode;
	 	var ChargeName =document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
	 	var contract = '${billing.contract}';
	 	contract=contract.trim();
	 	var pageCondition="";
	 	if(category=='Internal Cost'){
	 		pageCondition='AccountInternalCategory';
	 	}
	 	if(category!='Internal Cost'){
	 		pageCondition='AccountNonInternalCategory';
	 	}
		var mode;		
		if(document.forms['accountLineForms'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['accountLineForms'].elements['serviceOrder.mode'].value; 
			 }
		else{
			mode="";
			}
	 	id=''; 
	 	
	 	idval="charge";
			if(contract!=""){
				if(ChargeName.length>=3){
		            $.get("ChargeCodeAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
		            	searchName:ChargeName,
		            	contract: contract,
		            	chargeCodeId:idval,
		            	autocompleteDivId: divid,
		            	id:id,
		            	idCondition:idCondition, 
		            	searchcompanyDivision:accountCompanyDivision,
		            	originCountry:originCountry,
		            	destinationCountry:destinationCountry,
		            	serviceMode:mode,
		            	categoryCode:category,
		            	pageCondition:pageCondition,
		            	costElementFlag:${costElementFlag}
		            }, function(idCondition) {
		                document.getElementById(divid).style.display = "block";
		                $("#" + divid).html(idCondition)
		            })
		        } else {
		            document.getElementById(divid).style.display = "none"
		        }
			}
			else{
				alert("Charge code does not exist according the billing contract, Please select another");
				document.forms['accountLineForms'].elements['accountLine.chargeCode'].value='';
				
			}
			</configByCorp:fieldVisibility>
		}


		function copyChargeDetails(chargecode,chargecodeId,autocompleteDivId,id,idCondition){
			document.getElementById(autocompleteDivId).style.display = "none";
			document.forms['accountLineForms'].elements['accountLine.chargeCode'].value=chargecode;
			document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value='';
			if(idCondition=='A'){
				changeStatus();
				findRadioValue();
				fillRevQtyLocalAmounts();
				myFunctionGL();
				setLHF();
				checkLHFValue();
				setVatExcludeNew();
				driverCommission();
				exchangeContractAfterChargeChange();
				checkChargeCode();
				driverCommission(); 
			}
			if(idCondition=='B'){
				changeStatus();
				findRadioValue();
				fillRevQtyLocalAmounts();
				setLHF();
				checkLHFValue();
				setVatExcludeNew();
				driverCommission(); 
				exchangeContractAfterChargeChange();
				checkChargeCode();
				driverCommission(); 
				findRollUpInvoiceValFromChargeAjax();	
			}
			if(idCondition=='C'){
				exchangeContractAfterChargeChange();
				changeStatus();
				findRadioValue();
				fillRevQtyLocalAmounts();
				myFunctionGL();
				setLHF();
				checkLHFValue();
				setVatExcludeNew();
				driverCommission();
				checkChargeCode();
				driverCommission(); 
				findRollUpInvoiceValFromChargeAjax();
			}
			if(idCondition=='D'){
				exchangeContractAfterChargeChange();
				changeStatus();
				findRadioValue();
				fillRevQtyLocalAmounts();
				setLHF();
				checkLHFValue();
				setVatExcludeNew();
				driverCommission();
				checkChargeCode();
				driverCommission(); 
				findRollUpInvoiceValFromChargeAjax();
			}
			if(idCondition=='E'){
				changeStatus();
				findRadioValue();
				fillRevQtyLocalAmounts();
				myFunctionGL();
				setLHF();
				checkLHFValue();
				setVatExcludeNew();
				driverCommission();
				checkChargeCode(); 
				driverCommission(); 
				findRollUpInvoiceValFromChargeAjax();
			}
			if(idCondition=='F'){
				changeStatus();
				findRadioValue();
				fillRevQtyLocalAmounts();
				setLHF();
				checkLHFValue();
				setVatExcludeNew();
				driverCommission();
				checkChargeCode();
				driverCommission(); 
				findRollUpInvoiceValFromChargeAjax();
			}
			if(idCondition=='G'){
				changeStatus();
				findRadioValue();
				fillRevQtyLocalAmounts();
				myFunctionGL();
				setLHF();
				checkLHFValue();
				setVatExcludeNew();
				driverCommission();
				checkChargeCode(); 
				findRollUpInvoiceValFromChargeAjax();
			}
			if(idCondition=='H'){
				changeStatus();
				findRadioValue();
				fillRevQtyLocalAmounts();
				setLHF();
				checkLHFValue();
				setVatExcludeNew();
				driverCommission();
				checkChargeCode(); 
				findRollUpInvoiceValFromChargeAjax();
			}
			
			
		}

		function closeMyChargeDiv(autocompleteDivId,chargecode,chargecodeId,id,idCondition){
			document.getElementById(autocompleteDivId).style.display = "none";
			if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value==''){
			document.forms['accountLineForms'].elements['accountLine.description'].value="";	
			document.forms['accountLineForms'].elements['accountLine.note'].value="";
			}
			 document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value='';
			 if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value !='' && idCondition=='A'){
					changeStatus();
					findRadioValue();
					fillRevQtyLocalAmounts();
					myFunctionGL();
					setLHF();
					checkLHFValue();
					setVatExcludeNew();
					driverCommission();
					exchangeContractAfterChargeChange();
					checkChargeCode();
					driverCommission(); 
				}
				if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value !='' && idCondition=='B'){
					changeStatus();
					findRadioValue();
					fillRevQtyLocalAmounts();
					setLHF();
					checkLHFValue();
					setVatExcludeNew();
					driverCommission(); 
					exchangeContractAfterChargeChange();
					checkChargeCode();
					driverCommission(); 
					findRollUpInvoiceValFromChargeAjax();	
				}
				if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value !='' && idCondition=='C'){
					exchangeContractAfterChargeChange();
					changeStatus();
					findRadioValue();
					fillRevQtyLocalAmounts();
					myFunctionGL();
					setLHF();
					checkLHFValue();
					setVatExcludeNew();
					driverCommission();
					checkChargeCode();
					driverCommission(); 
					findRollUpInvoiceValFromChargeAjax();
				}
				if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value !='' && idCondition=='D'){
					exchangeContractAfterChargeChange();
					changeStatus();
					findRadioValue();
					fillRevQtyLocalAmounts();
					setLHF();
					checkLHFValue();
					setVatExcludeNew();
					driverCommission();
					checkChargeCode();
					driverCommission(); 
					findRollUpInvoiceValFromChargeAjax();
				}
				if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value !='' && idCondition=='E'){
					changeStatus();
					findRadioValue();
					fillRevQtyLocalAmounts();
					myFunctionGL();
					setLHF();
					checkLHFValue();
					setVatExcludeNew();
					driverCommission();
					checkChargeCode(); 
					driverCommission(); 
					findRollUpInvoiceValFromChargeAjax();
				}
				if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value !='' && idCondition=='F'){
					changeStatus();
					findRadioValue();
					fillRevQtyLocalAmounts();
					setLHF();
					checkLHFValue();
					setVatExcludeNew();
					driverCommission();
					checkChargeCode();
					driverCommission(); 
					findRollUpInvoiceValFromChargeAjax();
				}
				if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value !='' && idCondition=='G'){
					changeStatus();
					findRadioValue();
					fillRevQtyLocalAmounts();
					myFunctionGL();
					setLHF();
					checkLHFValue();
					setVatExcludeNew();
					driverCommission();
					checkChargeCode(); 
					findRollUpInvoiceValFromChargeAjax();
				}
				if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value !='' && idCondition=='H'){
					changeStatus();
					findRadioValue();
					fillRevQtyLocalAmounts();
					setLHF();
					checkLHFValue();
					setVatExcludeNew();
					driverCommission();
					checkChargeCode(); 
					findRollUpInvoiceValFromChargeAjax();
				}
				
			}
		
		 function closeMyChargeDiv2(chargecodeId,id){
			 if(document.forms['accountLineForms'].elements['accountLine.chargeCode'].value==''){
					document.forms['accountLineForms'].elements['accountLine.description'].value="";	
					document.forms['accountLineForms'].elements['accountLine.note'].value="";
					}
			 		document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value='';
			 		document.forms['accountLineForms'].elements['chargeCodeValidationMessage'].value='NothingData';
			}
		 function enablePreviousFunction(){
				document.getElementById('ChargeNameDivId').style.display = "none"; 
				if(document.getElementById("chargeCodeValidationMessage").value!='NothingData' && document.getElementById("chargeCodeValidationMessage").value!=''){
			    	 document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value='';
						checkChargeCode();
			    	 }
			}
			
			 function waitResponseFromAjax(){
				setTimeout(function(){checkChargeCode();},9000);
			} 
	function exchangeContractAfterChargeChange(){
		var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
		if(chargeCheck=='' || chargeCheck==null){
		var currentchargeCode =document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
		var previouschargeCode='${accountLine.chargeCode}';
		if(currentchargeCode.trim()!=previouschargeCode.trim()){
			document.forms['accountLineForms'].elements['accountLine.contract'].value='${billing.contract}';
			}
		//alert('${cmmDmmFlag}')
		<c:if test="${cmmDmmFlag}">
    	try{
    		var chargeCodeNew = document.forms['accountLineForms'].elements["accountLine.chargeCode"].value;
    		chargeCodeNew=chargeCodeNew.trim();
    		if(chargeCodeNew=='DMMFXFEE' || chargeCodeNew=='DMMFEE'){
    			alert("You are not allowed to select this charge code");
   		     document.forms['accountLineForms'].elements["accountLine.chargeCode"].value="";
    		}
    	}catch(e){}
    	</c:if>}
		}
	function checkVendorCodeForPay(xyz) {
		 var vendorCode= document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
		 vendorCode =vendorCode.trim();
		     if(vendorCode=='') {
		     	   alert("Vendor code is missing.");
		     	} else {
		     	fillDefaultInvoiceNumber();
		     	animatedcollapse.toggle('pay');
		     	} } 
		function checkchargeStarForRac(xyz) {
		var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value; 
		chargeCode =chargeCode.trim();
		    if(chargeCode=='') {
		     	   alert("Charge code needs to be selected")  
		     	}else {
		     	animatedcollapse.toggle('rec');
		     	} } 
		function checkchargeForRac(xyz) {
		 var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value; 
		 var glCode= document.forms['accountLineForms'].elements['accountLine.recGl'].value;
		 var billToCode= document.forms['accountLineForms'].elements['accountLine.billToCode'].value; 
		 chargeCode =chargeCode.trim();
		 glCode =glCode.trim();
		 billToCode =billToCode.trim(); 
		 	if(chargeCode=='') {
		     	   alert("Charge code needs to be selected")  
		     	}else if (document.forms['accountLineForms'].elements['grossMarginActualization'].value=="Y") {
		     		alert("Estimate or Revision gross margin is less than 10%")
		     	}
		 	else if(glCode=='')
		     	{
		     		checkChargeCodeOpenDiv('rec'); 
		     	} else if(chargeCode!='' && glCode!='' )   { 
		       if(billToCode=='')  {   
		            var url="getPrimaryBillToCode.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
				    http2.open("GET", url, true);
				    http2.onreadystatechange = function(){ handleHttpResponseBilltocode(xyz);};
				    http2.send(null);  
		       } 
		       else if(billToCode!='')
		       { 
		         animatedcollapse.toggle('rec');
		       }  } } 
		 function handleHttpResponseBilltocode(param){
		      if (http2.readyState == 4){
		      var results = http2.responseText 
		      results = results.trim();  
		      var res = results.split("@"); 
		      document.forms['accountLineForms'].elements['accountLine.billToCode'].value=res[1]
		      document.forms['accountLineForms'].elements['accountLine.billToName'].value=res[2]
		      checkMultiAuthorization();
		      animatedcollapse.toggle('rec');
		      } } 
		function accrualQuantitycheck(target) { 
		<c:if test="${accountInterface=='Y'}"> 
		 var estimateQuantity= document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
		 var basis= document.forms['accountLineForms'].elements['accountLine.basis'].value;
		 var revisionQuantity= document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		 var revisionPassPercentage= document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value;
		 var revisionExpense= document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;
		 var revisionRate= document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
		 var revisionRevenueAmount= document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;
		 var revisionSellRate= document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value; 
		 var accrueRevenue= document.forms['accountLineForms'].elements['accountLine.accrueRevenue'].value;
		 var accruePayable= document.forms['accountLineForms'].elements['accountLine.accruePayable'].value; 
		 accrueRevenue=accrueRevenue.trim();
		 accruePayable=accruePayable.trim();
		 if(target=='1') { 
		 if(accrueRevenue=='' && accruePayable=='') { 
		 } else if(accrueRevenue!='' || accruePayable!='' ) { 
		  document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].readOnly=true;
		  document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=revisionQuantity;
		  document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].readOnly=true;
		  document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionPassPercentage;
		  document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=estimateQuantity;
		  document.forms['accountLineForms'].elements['accountLine.basis'].value='${accountLine.basis}';
		  document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=true;
		 }
		 if(accruePayable=='') 
		 { } else if(accruePayable!='') {
		  document.forms['accountLineForms'].elements['accountLine.revisionRate'].readOnly=true;
		  document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRate;
		  document.forms['accountLineForms'].elements['accountLine.revisionExpense'].readOnly=true;
		  document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=revisionExpense;
		 }
		 if(accrueRevenue=='') {  }
		 else if(accrueRevenue!='') { 
		   document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].readOnly=true;
		   document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRate;
		   document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].readOnly=true;
		   document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=revisionRevenueAmount;
		 } }
		 if(target=='2')  {
		  if(accrueRevenue=='' && accruePayable=='') {
		   expense('accountLine.basis');
		 }
		 else if(accrueRevenue!='' || accruePayable!='' )  { 
		  document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].readOnly=true;
		  document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=revisionQuantity;
		  document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].readOnly=true;
		  document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionPassPercentage;
		  document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=estimateQuantity;
		  document.forms['accountLineForms'].elements['accountLine.basis'].value='${accountLine.basis}';
		  document.forms['accountLineForms'].elements['accountLine.compute3'].disabled=true; 
		 }
		 if(accruePayable=='') 
		 {  }
		 else if(accruePayable!='') {
		  document.forms['accountLineForms'].elements['accountLine.revisionRate'].readOnly=true;
		  document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRate;
		  document.forms['accountLineForms'].elements['accountLine.revisionExpense'].readOnly=true;
		  document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=revisionExpense; 
		 }
		 if(accrueRevenue=='')
		 { }
		 else if(accrueRevenue!='')  { 
		   document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].readOnly=true;
		   document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRate;
		   document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].readOnly=true;
		   document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=revisionRevenueAmount;
		 }  }
		 </c:if>
		} 
		function convertItProperDateFormate(target){ 
			var calArr=target.split("-");
			var year=calArr[2];
			year="20"+year;
			var month=calArr[1];
			  	   if(month == 'Jan') { month = "01"; }  
			  else if(month == 'Feb') { month = "02"; } 
			  else if(month == 'Mar') { month = "03"; } 
			  else if(month == 'Apr') { month = "04"; } 
			  else if(month == 'May') { month = "05"; } 
			  else if(month == 'Jun') { month = "06"; }  
			  else if(month == 'Jul') { month = "07"; } 
			  else if(month == 'Aug') { month = "08"; }  
			  else if(month == 'Sep') { month = "09"; }  
			  else if(month == 'Oct') { month = "10"; }  
			  else if(month == 'Nov') { month = "11"; }  
			  else if(month == 'Dec') { month = "12"; }	
			var day=calArr[0];
			var date=year+"-"+month+"-"+day;
			return date;
		}
		function revConvertItProperDateFormate(target){
			     var date="";
			     if((target!='null')&&(target!='')){
			    	 var targett=target.substring(0,10);
			    		var calArr=targett.split("-");
			    		var year=calArr[0];
			    		year=year.substring(2,4);
			    		var month=calArr[1];
			    		  	   if(month == '01') { month = "Jan"; }  
			    		  else if(month == '02') { month = "Feb"; } 
			    		  else if(month == '03') { month = "Mar"; } 
			    		  else if(month == '04') { month = "Apr"; } 
			    		  else if(month == '05') { month = "May"; } 
			    		  else if(month == '06') { month = "Jun"; }  
			    		  else if(month == '07') { month = "Jul"; } 
			    		  else if(month == '08') { month = "Aug"; }  
			    		  else if(month == '09') { month = "Sep"; }  
			    		  else if(month == '10') { month = "Oct"; }  
			    		  else if(month == '11') { month = "Nov"; }  
			    		  else if(month == '12') { month = "Dec"; }	
			    		var day=calArr[2];
			    		date=day+"-"+month+"-"+year;
			     }
			return date;
		}
		var httpTemplate = getHTTPObject();
		function checkVendorInvoice(name) { 
			   document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value=document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value.trim();
			   var payingStatus=document.forms['accountLineForms'].elements['accountLine.payingStatus'].value; 
			   var vendorCode=document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
			   vendorCode=vendorCode.trim();
			   var country=document.forms['accountLineForms'].elements['accountLine.country'].value;
			   country=country.trim();
			   var invoiceNumber=document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value;
			   var invoiceDate='';
			   try{
			   invoiceDate=document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value;
			   }catch(e){}
			   invoiceDate=invoiceDate.trim();
			   if(invoiceDate!=''){
				   invoiceDate=convertItProperDateFormate(invoiceDate);
			   }
			   invoiceNumber=invoiceNumber.trim();
			   if(payingStatus=='A' || payingStatus=='C') {
				     if(invoiceNumber==''){
		   	         alert("Please fill invoice# in payable detail.");
		   	         document.forms['accountLineForms'].elements['accountLine.payingStatus'].value =''
				     }else if(invoiceDate=='') {
			  	         alert("Please fill Invoice Date in the Payable Details section.");
			  	         document.forms['accountLineForms'].elements['accountLine.payingStatus'].value =''
				      }else{		    	  
					<c:choose>
				          <c:when test="${accCorpID=='UTSI'}">
							   <c:if test="${not empty accountLine.id}">
							   var idss='${accountLine.id}';
					            var url="getAllAccListAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&invoiceDateNew="+invoiceDate+"&currencyNew="+country+"&idss="+idss;
							    http1985.open("GET", url, true);
							    http1985.onreadystatechange = handleHttpResponseAccListData;
							    http1985.send(null); 
								</c:if>
							   <c:if test="${empty accountLine.id}">
					            var url="getAllAccListAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&invoiceDateNew="+invoiceDate+"&currencyNew="+country+"&idss=null";
							    http1985.open("GET", url, true);
							    http1985.onreadystatechange = handleHttpResponseAccListData;
							    http1985.send(null); 
								</c:if>
				          </c:when>
				          <c:otherwise>
				          
				          </c:otherwise>
				     </c:choose>	       
					  }    	}   
		       <c:if test="${accCorpID=='SSCW'}">
		       if(name=='INV'){
			          if((invoiceNumber!='')&&(vendorCode!='')){
							   <c:if test="${not empty accountLine.id}">
							   var idss='${accountLine.id}';
							            var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss="+idss;
									    http1981.open("GET", url, true);
									    http1981.onreadystatechange = handleHttpResponseVendorInvoiceEnhancement;
									    http1981.send(null); 
							   </c:if>
							   <c:if test="${empty accountLine.id}">
							            var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss=null";
									    http1981.open("GET", url, true);
									    http1981.onreadystatechange = handleHttpResponseVendorInvoiceEnhancement;
									    http1981.send(null); 
							   </c:if>		   	            
			          }   } 
		       </c:if>
		       if(document.forms['accountLineForms'].elements['accountLine.payingStatus'].value=='C') {
		    	   var mydate=new Date();
		   		var daym;
		   		var year=mydate.getFullYear()
		   		var y=""+year;
		   		if (year < 1000)
		   		year+=1900
		   		var day=mydate.getDay()
		   		var month=mydate.getMonth()+1
		   		if(month == 1)month="Jan";
		   		if(month == 2)month="Feb";
		   		if(month == 3)month="Mar";
		   		if(month == 4)month="Apr";
		   		if(month == 5)month="May";
		   		if(month == 6)month="Jun";
		   		if(month == 7)month="Jul";
		   		if(month == 8)month="Aug";
		   		if(month == 9)month="Sep";
		   		if(month == 10)month="Oct";
		   		if(month == 11)month="Nov";
		   		if(month == 12)month="Dec"; 
		   		var daym=mydate.getDate()
		   		if (daym<10)
		   		daym="0"+daym
		   		var datam = daym+"-"+month+"-"+y.substring(2,4); 
		   		document.forms['accountLineForms'].elements['accountLine.payPayableDate'].value=datam;
		   		document.forms['accountLineForms'].elements['accountLine.payPayableStatus'].selectedIndex =2;
		   		document.forms['accountLineForms'].elements['accountLine.payPayableVia'].value='Credit Card';
		   		document.forms['accountLineForms'].elements['accountLine.payPayableAmount'].value=document.forms['accountLineForms'].elements['accountLine.actualExpense'].value;
		   		document.forms['accountLineForms'].elements['accountLine.payPostDate'].value=document.forms['accountLineForms'].elements['systemDefaultPostDate1'].value;;
		   		document.forms['accountLineForms'].elements['accountLine.payAccDate'].value=datam;
		   		document.forms['accountLineForms'].elements['accountLine.payXferUser'].value='${updatedUserName}';
		   		document.forms['accountLineForms'].elements['accountLine.payXfer'].value='Credit Card';
		       }else{
		    	   <c:if test="${empty accountLine.payPayableDate}">
		    	document.forms['accountLineForms'].elements['accountLine.payPayableDate'].value='${accountLine.payPayableDate}'
		    		</c:if>
		        document.forms['accountLineForms'].elements['accountLine.payPayableStatus'].value='${accountLine.payingStatus}';
		        document.forms['accountLineForms'].elements['accountLine.payPayableVia'].value='${accountLine.payPayableVia}';
		      	document.forms['accountLineForms'].elements['accountLine.payPayableAmount'].value='${accountLine.payPayableAmount}';
		      	<c:if test="${empty accountLine.payPostDate}">
		      	document.forms['accountLineForms'].elements['accountLine.payPostDate'].value='${accountLine.payPostDate}';
		      	</c:if>
		      	document.forms['accountLineForms'].elements['accountLine.payAccDate'].value='${accountLine.payAccDate}';
		      	document.forms['accountLineForms'].elements['accountLine.payXferUser'].value='${accountLine.payXferUser}';
		      	document.forms['accountLineForms'].elements['accountLine.payXfer'].value='${accountLine.payXfer}';  
		       }
		       <c:if test="${allowAgentInvoiceUpload==true}" >
		       var vendorCode=document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
		       var invoiceNumber=document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value;
			   var invoiceDate='';
			   vendorCode=vendorCode.trim();
			   var sidNum =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
		       if((payingStatus=='R' || payingStatus=='P') && invoiceNumber!='' ) {
		    	   var url="notesStatusAcc.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&sidNum=${serviceOrder.id}";
		    		httpTemplate.open("GET", url, true);
		    		httpTemplate.onreadystatechange = getHTTPObject19;
		    		httpTemplate.send(null);
		    	   
		       }
		       </c:if>
			   	}
		function getHTTPObject19(){
			if (httpTemplate.readyState == 4){
			   var results= httpTemplate.responseText;
			   var results=results.trim();
			   var results=results.split("~");
			   var count= results[0]; 
		       if(count> 0) { 
		       }else{
		    	   alert("Cannot select status pending with note/rejected as there is no note available");
		    	   var payStatus = "${accountLine.payingStatus}"; 
		    	   document.forms['accountLineForms'].elements['accountLine.payingStatus'].value =payStatus;
		       }
			}
			}
		
		function handleHttpResponseAccListData(){
		    if (http1985.readyState == 4){
		        var results = http1985.responseText
		        results = results.trim();
		        if(results!=''){
			    		 alert(results);
			     		  document.forms['accountLineForms'].elements['accountLine.payingStatus'].value =''
			    }  } }
		function handleHttpResponseVendorInvoiceEnhancement(){
		    if (http1981.readyState == 4){
		        var results = http1981.responseText
		        results = results.trim();
		        if(results!=''){
		        	var catVal = document.forms['accountLineForms'].elements['accountLine.category'].value;
		        	if(catVal=='Internal Cost'){
		        	}else{
			                alert(results);
			                /* if(agree) {
			                } else{ */
			                	document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value="${accountLine.invoiceNumber}";
			                 } } }	 }
		var varNameAcct = '';
		function checkAcctStatus(elementName)
		{
			if(elementName.indexOf('_') > 0)
				varNameAcct = elementName.split('_')[0];
			else
				varNameAcct = elementName;
			return varNameAcct;
		}
		function hitAcct(){
			if(varNameAcct == 'invoiceDate')
			{
				checkPurchaseInvoiceProcessing('','INV');
				varNameAcct='';
			}else{
				varNameAcct='';
				return false;
			} }
		function onlyDelForInvoiceDate(evt,targetElement){
			if(document.forms['accountLineForms'].elements['accountLine.payingStatus'].value!='A'){
				var keyCode = evt.which ? evt.which : evt.keyCode;
				  if(keyCode==46){
				  		targetElement.value = '';
				  }else{
				  if(keyCode==8){
				  	targetElement.value = '';
				  }
				  	return false;
				  }
			}
			else{
			  	return false;
			} }
		function checkPurchaseInvoiceProcessing(target,name) { 
			   var payingStatus=document.forms['accountLineForms'].elements['accountLine.payingStatus'].value; 
			   var vendorCode=document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
			   vendorCode=vendorCode.trim();
			   var country=document.forms['accountLineForms'].elements['accountLine.country'].value;
			   country=country.trim();
			   var invoiceNumber=document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value;
			   var invoiceDate='';
			   try{
			   invoiceDate=document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value;
			   }catch(e){}
			   invoiceDate=invoiceDate.trim();
			   if(invoiceDate!=''){
				   invoiceDate=convertItProperDateFormate(invoiceDate);
			   }
			   invoiceNumber=invoiceNumber.trim();
			  <c:choose>
		          <c:when test="${accCorpID=='UTSI'}">
				   	   if(payingStatus=='A') {
						   <c:if test="${not empty accountLine.id}">
						   var idss='${accountLine.id}';
						            var url="getAllAccListAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&invoiceDateNew="+invoiceDate+"&currencyNew="+country+"&idss="+idss;
								    http1986.open("GET", url, true);
								    http1986.onreadystatechange = function(){ handleHttpResponseAccListDataNew(target,name);};
								    http1986.send(null); 
						   </c:if>
						   <c:if test="${empty accountLine.id}">
						            var url="getAllAccListAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&invoiceDateNew="+invoiceDate+"&currencyNew="+country+"&idss=null";
								    http1986.open("GET", url, true);
								    http1986.onreadystatechange = function(){ handleHttpResponseAccListDataNew(target,name);};
								    http1986.send(null); 
						   </c:if>		   
							  } else{
					              if(name=='CUR'){
					                  changeStatus();findExchangeRate();
					                  }else if(name=='VEN'){
					                	  <c:if test="${accountInterface!='Y'}">
					                	  checkVendorName();fillCurrencyByChargeCode();changeStatus();
					                	  </c:if>
					                	  <c:if test="${accountInterface=='Y'}">
					                	  vendorCodeForActgCode();fillCurrencyByChargeCode();disableButton();changeStatus();
					                	  </c:if> 
					                  }   }            
		    	  </c:when>
		          <c:when test="${accCorpID=='SSCW'}">
			          if(name=='VEN'){
				          if((invoiceNumber!='')&&(vendorCode!='')){
								   <c:if test="${not empty accountLine.id}">
								   var idss='${accountLine.id}';
								            var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss="+idss;
										    http1983.open("GET", url, true);
										    http1983.onreadystatechange = function(){ handleHttpResponseVendorInvoiceEnhancementNew(target,name);};
										    http1983.send(null); 
								   </c:if>
								   <c:if test="${empty accountLine.id}">
								            var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss=null";
										    http1983.open("GET", url, true);
										    http1983.onreadystatechange = function(){ handleHttpResponseVendorInvoiceEnhancementNew(target,name);};
										    http1983.send(null); 
								   </c:if>		   	            
				          }else{
			            	  <c:if test="${accountInterface!='Y'}">
			            	  checkVendorName();fillCurrencyByChargeCode();changeStatus();
			            	  </c:if>
			            	  <c:if test="${accountInterface=='Y'}">
			            	  vendorCodeForActgCode();fillCurrencyByChargeCode();disableButton();changeStatus();
			            	  </c:if>
				          }  
			          }else{
			                  changeStatus();findExchangeRate();
			          }
				  </c:when>
		    	  <c:otherwise>
		          if(name=='CUR'){
		              changeStatus();findExchangeRate();
		              }else if(name=='VEN'){
		            	  <c:if test="${accountInterface!='Y'}">
		            	  checkVendorName();fillCurrencyByChargeCode();changeStatus();
		            	  </c:if>
		            	  <c:if test="${accountInterface=='Y'}">
		            	  vendorCodeForActgCode();fillCurrencyByChargeCode();disableButton();changeStatus();
		            	  </c:if>
		              }    	  
		          </c:otherwise>
		      </c:choose>	  
			   	} 	
			function handleHttpResponseAccListDataNew(target,name){
			      if (http1986.readyState == 4){
		              var results = http1986.responseText
		              results = results.trim();
		              if(results!=''){
		                  if(name=='CUR'){
		 		    		 alert(results);
				  	         document.forms['accountLineForms'].elements['accountLine.country'].value ="${accountLine.country}"; 
		                  }else if(name=='VEN'){
		  		    		 alert(results);
				  	       document.forms['accountLineForms'].elements['accountLine.vendorCode'].value ="${accountLine.vendorCode}";
				  	       document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="${accountLine.estimateVendorName}";
				  	       document.forms['accountLineForms'].elements['accountLine.actgCode'].value="${accountLine.actgCode}";
		                  }else if(name=='INV'){
		   		    		 alert(results);
		   		    		 var tt="${accountLine.invoiceDate}";
		   		    		 var payStatus = "${accountLine.payingStatus}";
		   		    		tt=revConvertItProperDateFormate(tt);
		               	     document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value =tt; 
		               	  document.forms['accountLineForms'].elements['accountLine.payingStatus'].value =payStatus;
		                  } 
		              }else{
		              if(name=='CUR'){
		              changeStatus();findExchangeRate();
		              }else if(name=='VEN'){
		            	  <c:if test="${accountInterface!='Y'}">
		            	  checkVendorName();fillCurrencyByChargeCode();changeStatus();
		            	  </c:if>
		            	  <c:if test="${accountInterface=='Y'}">
		            	  vendorCodeForActgCode();fillCurrencyByChargeCode();disableButton();changeStatus();
		            	  </c:if> 
		              }  }  } }
			function handleHttpResponseVendorInvoiceEnhancementNew(target,name){
			      if (http1983.readyState == 4){
		            var results = http1983.responseText
		            results = results.trim();
		            if(results!=''){
		            	var catVal = document.forms['accountLineForms'].elements['accountLine.category'].value;
		            	if(catVal=='Internal Cost'){
			             	  <c:if test="${accountInterface!='Y'}">
			              	  checkVendorName();fillCurrencyByChargeCode();changeStatus();
			              	  </c:if>
			              	  <c:if test="${accountInterface=='Y'}">
			              	  vendorCodeForActgCode();fillCurrencyByChargeCode();disableButton();changeStatus();
			              	  </c:if>	                                  	
		            	}else{
			               alert(results);
			                /* if(agree) {
				             	  <c:if test="${accountInterface!='Y'}">
				              	  checkVendorName();fillCurrencyByChargeCode();changeStatus();
				              	  </c:if>
				              	  <c:if test="${accountInterface=='Y'}">
				              	  vendorCodeForActgCode();fillCurrencyByChargeCode();disableButton();changeStatus();
				              	  </c:if>	                  
			                } else{ */
			 		  	      
			                document.forms['accountLineForms'].elements['accountLine.vendorCode'].value ="${accountLine.vendorCode}";
					  	    document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="${accountLine.estimateVendorName}";
					  	    document.forms['accountLineForms'].elements['accountLine.actgCode'].value="${accountLine.actgCode}";
			                 }
		            }else{
		          	  <c:if test="${accountInterface!='Y'}">
		          	  checkVendorName();fillCurrencyByChargeCode();changeStatus();
		          	  </c:if>
		          	  <c:if test="${accountInterface=='Y'}">
		          	  vendorCodeForActgCode();fillCurrencyByChargeCode();disableButton();changeStatus();
		          	  </c:if>
		            } } }	
		  function copyActSenttoclient() {
		  document.forms['accountLineForms'].elements['checkActSenttoclient'].value ='1';
		  }
		  function copyReceivedInvoiceDate() { 
		   <c:if test="${multiCurrency=='Y'}">
		    document.forms['accountLineForms'].elements['checkReceivedInvoiceDate'].value ='1';
		    </c:if>
		  }
		  function updateReceivedInvoiceDate(){
		  <c:if test="${multiCurrency=='Y'}">
		   if(document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value!='') {
		   var invoiceNumber = document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value; 
		   alert("Invoice for this line has already been generated, so this date will affect for the all invoice no "+invoiceNumber+".");
		   }
		   document.forms['accountLineForms'].elements['checkReceivedInvoiceDate'].value ='';
		    </c:if>
		  } 
		  function updateActSenttoclient() { 
		    if(document.forms['accountLineForms'].elements['accountLine.sendActualToClient'].value!='' && document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value!='') {
		     var invoice= document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value
		     var agree =confirm("Do you want to copy the actual sent to client date to all lines of invoice # ("+invoice+")?");
		     if(agree) {
		      document.forms['accountLineForms'].elements['accountLine.sendActualToClient'].select();
		      document.forms['accountLineForms'].elements['dateUpdate'].value='Y'; 
		     } else{
		     document.forms['accountLineForms'].elements['dateUpdate'].value='N';
		     }  }
		  document.forms['accountLineForms'].elements['checkActSenttoclient'].value ='';
		  } 
		  function calculateRecRate(target,field1,field2,field3) {
				 if(field1!='' && field2!='' & field3!=''){
					 rateOnActualizationDate(field1,field2,field3);
					 }	   
		        var country =document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value; 
		        if(country=='') {
		          alert("Please select billing currency ");
		          document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=0;
		          document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value=1;
		        } else if(country!=''){
		         var contractRate=0;
		         <c:if test="${contractType}">
		            contractRate = document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
		         </c:if> 
		        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value
		        var recCurrencyRate=document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value
		        recCurrencyRate=document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=Math.round(recCurrencyRate*10000)/10000;
		        var recRate=recCurrencyRate/recRateExchange;
		        var roundValue=Math.round(recRate*10000)/10000;
		        document.forms['accountLineForms'].elements['accountLine.recRate'].value=roundValue ; 
		        convertedAmountBilling(target,'','',''); 
		        calFrgnCurrAmount(target);
		        calRecContractRate(target);
		        }   }
		  function calculateRecRateByExchangerate(){ 
		      var country =document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value; 
		      if(country=='') {
		        alert("Please select billing currency ");
		        document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=0;
		        document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value=1;
		      } else if(country!=''){
		       var contractRate=0;
		       <c:if test="${contractType}">
		          contractRate = document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
		       </c:if>
		      if(contractRate==0){ 
		      var recRateExchange=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value
		      var recCurrencyRate=document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value
		      recCurrencyRate=document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=Math.round(recCurrencyRate*10000)/10000;
		      var recRate=recCurrencyRate/recRateExchange;
		      var roundValue=Math.round(recRate*10000)/10000;
		      document.forms['accountLineForms'].elements['accountLine.recRate'].value=roundValue ; 
		      convertedAmountBilling('none','','','');
		      }else{
		      	calRecCurrencyRate('none')
		      }
		      calFrgnCurrAmount('none'); 
		      }   }
		        function calRecContractRate(target){
		        <c:if test="${contractType}"> 
		          var country =document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value; 
		          if(country=='') {
		          alert("Please select Contract Currency "); 
		          document.forms['accountLineForms'].elements['accountLine.recRate'].value=0; 
		        } else if(country!=''){
		        	var roundValue=0.00;
		        	 <c:if test="${contractType}">        	
					 	var QTemp=0.00; 
					    var ATemp =0.00;
					    var FTemp =0.00;
					    var ETemp =0.00;
					    ETemp= document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value*1 ; 
					    FTemp =document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value;
					    FTemp=FTemp/ETemp;
					    ETemp= document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value*1 ;				    
					    FTemp=FTemp*ETemp;
					    roundValue=Math.round(FTemp*10000)/10000;
		          </c:if>
		          <c:if test="${!contractType}">        
				        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value
				        var recRate=document.forms['accountLineForms'].elements['accountLine.recRate'].value
				        var recCurrencyRate=recRate*recRateExchange;
				        roundValue=Math.round(recCurrencyRate*10000)/10000;
		            </c:if>
		        if(target!='accountLine.contractRate'){
		        document.forms['accountLineForms'].elements['accountLine.contractRate'].value=roundValue ;
		        }
		        calContractRateAmmount(target);
		        }
		      </c:if>
		  }
		        function calEstimateContractRate(target){
		            <c:if test="${contractType}"> 
		              var country =document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value; 
		              if(country=='') { 
		              document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value=0; 
		            } else if(country!=''){
		            	var roundValue=0.00;
		            	<c:if test="${contractType}"> 
						 	var QTemp=0.00; 
						    var ATemp =0.00;
						    var FTemp =0.00;
						    var ETemp =0.00;
						    ETemp= document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value*1 ; 
						    FTemp =document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value;
						    FTemp=FTemp/ETemp;
						    ETemp= document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value*1 ;				    
						    FTemp=FTemp*ETemp;
			            roundValue=Math.round(FTemp*10000)/10000; 
			        	</c:if>
			        	<c:if test="${!contractType}"> 
		                    var recRateExchange=document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value
		                    var recRate=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value
		                    var recCurrencyRate=recRate*recRateExchange;
		                    roundValue=Math.round(recCurrencyRate*10000)/10000;
		                    </c:if>
		            if(target!='accountLine.estimateContractRate'){
		            document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value=roundValue ;
		            }
		            calEstimateContractRateAmmount(target);
		            }
		          </c:if>
		      }  
		        function calEstimatePayableContractRateByBase(target){
		            <c:if test="${contractType}"> 
		              var country =document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value; 
		              if(country=='') { 
		              document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value=0; 
		            } else if(country!=''){ 
		            	 var roundValue=0.00; 
		            <c:if test="${contractType}">            
						 	var QTemp=0.00; 
						    var ATemp =0.00;
						    var FTemp =0.00;
						    var ETemp =0.00;
						    ETemp= document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value*1 ; 
						    FTemp =document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value;
						    FTemp=FTemp/ETemp;
						    ETemp= document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value*1 ;				    
						    FTemp=FTemp*ETemp;
			            roundValue=Math.round(FTemp*10000)/10000;
					</c:if>
					<c:if test="${!contractType}">                
			            var recRateExchange=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value;
			            var recRate=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
			            var recCurrencyRate=recRate*recRateExchange;
			            roundValue=Math.round(recCurrencyRate*10000)/10000;
					</c:if>
		            if(target!='accountLine.estimatePayableContractRate'){
		            document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value=roundValue ;
		            }
		            calEstimatePayableContractRateAmmount(target);
		            }
		          </c:if>
		      } 
		        function calRevisionContractRate(target){ 
		            <c:if test="${contractType}"> 
		              var country =document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value; 
		              if(country=='') { 
		              document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value=0; 
		            } else if(country!=''){
		            	var roundValue=0.00;
		            	 <c:if test="${contractType}">
		 					 	var QTemp=0.00; 
							    var ATemp =0.00;
							    var FTemp =0.00;
							    var ETemp =0.00;
							    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value*1 ; 
							    FTemp =document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value;
							    FTemp=FTemp/ETemp;
							    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value*1 ;				    
							    FTemp=FTemp*ETemp;
				            roundValue=Math.round(FTemp*10000)/10000;
				            </c:if>
				            <c:if test="${!contractType}">
					            var recRateExchange=document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value
					            var recRate=document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value
					            var recCurrencyRate=recRate*recRateExchange;
					            roundValue=Math.round(recCurrencyRate*10000)/10000;
					            
					            </c:if>
							if(target!='accountLine.revisionContractRate'){
					            document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value=roundValue ;
					            }
		            calRevisionContractRateAmmount(target);
		            }
		          </c:if>
		      } 
		        function calRevisionPayableContractRateByBase(target){
		            <c:if test="${contractType}"> 
		              var country =document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value; 
		              if(country=='') { 
		              document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value=0; 
		            } else if(country!=''){
		                var roundValue=0.00;
		                <c:if test="${contractType}"> 
						 	var QTemp=0.00; 
						    var ATemp =0.00;
						    var FTemp =0.00;
						    var ETemp =0.00;
						    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value*1 ; 
						    FTemp =document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value;
						    FTemp=FTemp/ETemp;
						    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value*1 ;				    
						    FTemp=FTemp*ETemp;
			            roundValue=Math.round(FTemp*10000)/10000;       
			            </c:if>
			            <c:if test="${!contractType}"> 
		                        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value
		                        var recRate=document.forms['accountLineForms'].elements['accountLine.revisionRate'].value
		                        var recCurrencyRate=recRate*recRateExchange;
		                       roundValue=Math.round(recCurrencyRate*10000)/10000; 
		                   	
		                       </c:if>
		                if(target!='accountLine.revisionPayableContractRate'){
		                	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value=roundValue ;
		                }             	
		            calRevisionPayableContractRateAmmount(target);
		            }
		          </c:if>
		      }       
		   function  calculateRecRateByContractRate(target,field1,field2,field3) {
				 if(field1!='' && field2!='' & field3!=''){
					 rateOnActualizationDate(field1,field2,field3);
					 }	   
		   <c:if test="${contractType}">
		        var country =document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value; 
		        if(country=='') {
		          alert("Please select Contract Currency ");
		          document.forms['accountLineForms'].elements['accountLine.contractRate'].value=0;
		          document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value=1;
		        } else if(country!=''){
		        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value
		        var recCurrencyRate=document.forms['accountLineForms'].elements['accountLine.contractRate'].value
		        recCurrencyRate=document.forms['accountLineForms'].elements['accountLine.contractRate'].value=Math.round(recCurrencyRate*10000)/10000;
		        var recRate=recCurrencyRate/recRateExchange;
		        var roundValue=Math.round(recRate*10000)/10000;
		        document.forms['accountLineForms'].elements['accountLine.recRate'].value=roundValue ; 
		        calRecCurrencyRate(target);
		        calContractRateAmmount(target);
		        }   
		   </c:if>
		   }
		   function calContractRateAmmount(target) { 
		   <c:if test="${contractType}">
		   if(target=='accountLine.contractRate'){
		 		 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
			        ATemp=(ATemp*quantityVal)/baseRateVal;
					var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}
			        roundValue=Math.round(ATemp*10000)/10000;
			        document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value=roundValue;
		   }else if(target=='accountLine.recCurrencyRate'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value;
			        ETemp=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;
			        ATemp=ATemp/ETemp;
			        ETemp=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value;
			        BTemp=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;
			        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
					var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}	   
		       roundValue=Math.round(ATemp*10000)/10000;
		       document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value=roundValue;
		   }else if(target=='accountLine.recRate'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.recRate'].value;
			        ETemp=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value;
			        BTemp=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;
			        
			        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
					var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}
		       roundValue=Math.round(ATemp*10000)/10000;
		       document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value=roundValue;
		   }else if(target=='accountLine.actualRevenue'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;    		 
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			     ETemp = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;  
			     ATemp = document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
			     BTemp = document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value;
			     ATemp=ATemp*BTemp;
					var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}
		       roundValue=Math.round(ATemp*10000)/10000;
		       document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value=roundValue;
		   } else{
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
			        ATemp=(ATemp*quantityVal)/baseRateVal;
					var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}
			        roundValue=Math.round(ATemp*10000)/10000;
			        document.forms['accountLineForms'].elements['accountLine.contractRateAmmount'].value=roundValue;
		   		}  
		   </c:if>
		  		}    
		   function calRecCurrencyRate(target){
		   <c:if test="${multiCurrency=='Y'}">
		          var country =document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value; 
		          if(country=='') {
		          alert("Please select billing currency "); 
		          document.forms['accountLineForms'].elements['accountLine.recRate'].value=0; 
		          document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value=0;
		        } else if(country!=''){
		        	 <c:if test="${contractType}">
		        	 if(target=='accountLine.recRate'){
		 		        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value
				        var recRate=document.forms['accountLineForms'].elements['accountLine.recRate'].value
				        var recCurrencyRate=recRate*recRateExchange;
				        var roundValue=Math.round(recCurrencyRate*10000)/10000;
				        document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=roundValue ; 
		        	 }else if(target=='accountLine.actualRevenue'){
		        		 var roundValue='0.00';
		        	     var ETemp=0.00;
		        	     var ATemp=0.00;
		        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
		        		 var baseRateVal=1;
		        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
		        	         } 
		        	        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	        	baseRateVal=100;
		        	         }  else if(bassisVal=="per 1000"){
		        	        	 baseRateVal=1000;
		        	       	 } else {
		        	       		baseRateVal=1;  	
		        		    }
		        	        ATemp=document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;  
		        	        ATemp=(ATemp*baseRateVal)/quantityVal; 
		        	     ETemp = document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;  
		        	     ATemp=ATemp*ETemp;
		        	     roundValue=Math.round(ATemp*10000)/10000;
		            	document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=roundValue;
		        	 }else if(target=='accountLine.recCurrencyRate'){
		        	 }else{
			            var recRateExchangeTemp=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value
			            var recCurrencyRateTemp=document.forms['accountLineForms'].elements['accountLine.contractRate'].value
			            var recRateTemp=recCurrencyRateTemp/recRateExchangeTemp;
			            
				        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value
				        var recRate=recRateTemp;
				        var recCurrencyRate=recRate*recRateExchange;
				        var roundValue=Math.round(recCurrencyRate*10000)/10000;
				        document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=roundValue ; 
		        	 }    
					</c:if>
					<c:if test="${!contractType}">
				        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value
				        var recRate=document.forms['accountLineForms'].elements['accountLine.recRate'].value
				        var recCurrencyRate=recRate*recRateExchange;
				        var roundValue=Math.round(recCurrencyRate*10000)/10000;
				        document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value=roundValue ; 
					</c:if>
		        convertedAmountBilling(target,'','',''); 
		        calFrgnCurrAmount(target);
		        calRecContractRate(target);
		        }
		  </c:if>
		  }
		  function calFrgnCurrAmount(target) { 
		  <c:if test="${multiCurrency=='Y'}">
		         <c:if test="${contractType}"> 
		    	 if(target=='accountLine.contractRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;
		    	     var BTemp =0.00;    	     	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
		    	        ETemp=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value;
		    	        BTemp=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;
		    	        ATemp=((ATemp/ETemp)*quantityVal*BTemp)/baseRateVal;
						var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
						if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
							   ATemp=(ATemp*receivableSellDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*10000)/10000;
				        document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value=roundValue; 
		    	 }else if(target=='accountLine.recCurrencyRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;     		 
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;
		    	     var BTemp =0.00;    	     	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	     ETemp = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;  
		    	     ATemp = document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value;
		    	     ATemp=(ATemp*ETemp)/baseRateVal;
						var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
						if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
							   ATemp=(ATemp*receivableSellDeviation)/100;
						}    	     
		    	     roundValue=Math.round(ATemp*10000)/10000;
		        	document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value=roundValue; 
		    	 }else if(target=='accountLine.recRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;    		 
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;
		    	     var BTemp =0.00;    	     	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	     ETemp = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;  
		    	     ATemp = document.forms['accountLineForms'].elements['accountLine.recRate'].value;
		    	     BTemp = document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;
		    	     ATemp=(ATemp*BTemp*ETemp)/baseRateVal;
						var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
						if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
							   ATemp=(ATemp*receivableSellDeviation)/100;
						}
		    	     roundValue=Math.round(ATemp*10000)/10000;
		        	document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value=roundValue; 
		    	 }else if(target=='accountLine.actualRevenue'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;    		 
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;
		    	     var BTemp =0.00;    	     	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	     ETemp = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;  
		    	     ATemp = document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
		    	     BTemp = document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;
		    	     ATemp=ATemp*BTemp;
						var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
						if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
							   ATemp=(ATemp*receivableSellDeviation)/100;
						}
		    	     roundValue=Math.round(ATemp*10000)/10000;
		        	document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value=roundValue; 
		    	 }else{
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;
		    	     var BTemp =0.00;    	     	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
		    	        ETemp=document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value;
		    	        BTemp=document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;
		    	        ATemp=(((ATemp/ETemp)*BTemp)*quantityVal)/baseRateVal;
		 				var receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
						if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
							   ATemp=(ATemp*receivableSellDeviation)/100;
						}
		    	     roundValue=Math.round(ATemp*10000)/10000;
		        	document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value=roundValue;
		               }
		    	 </c:if>
		    	 <c:if test="${!contractType}">
		    	 var estimate=0;
		  	   var estimaternd=0;
		  	   var receivableSellDeviation=0;
		  	       var quantity = document.forms['accountLineForms'].elements['accountLine.recQuantity'].value;
		  	       var rate = document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].value;
		  	   	   if(quantity<999999999999&& rate<999999999999) {
		  	  	 	estimate = (quantity*rate);
		  	  	 	estimaternd=Math.round(estimate*10000)/10000;
		  	  	 	} else { 
		  	  	 	 estimate=0;
		  	  	 	 estimaternd=Math.round(estimate*10000)/10000;
		  	  	 	} 
		  	         if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age") {
		  	      	    estimate = estimate/100; 
		  	      	    estimaternd=Math.round(estimate*10000)/10000;	 
		  	      	    document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value = estimaternd; 
		  	      	      }
		  	          else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000")  {
		  	      	    estimate = estimate/1000; 
		  	      	    estimaternd=Math.round(estimate*10000)/10000;	 
		  	      	    document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value = estimaternd; 
		  	      	    } else{
		  	            document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value = estimaternd;
		  	              }

		        
		          receivableSellDeviation=document.forms['accountLineForms'].elements['accountLine.receivableSellDeviation'].value;
		           if(receivableSellDeviation!='' && receivableSellDeviation>0){
		             estimaternd=estimaternd*receivableSellDeviation/100;
		             estimaternd=Math.round(estimaternd*10000)/10000;
		             document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value = estimaternd;	 
		         }
		    	 </c:if>
		            if(document.forms['accountLineForms'].elements['accountLine.paymentStatus'].value=='Fully Paid'){
		               document.forms['accountLineForms'].elements['accountLine.receivedAmount'].value= estimaternd;
		             }
		            calculateVatAmt();
		            calculateVatAmtRevision();
		            calculateVatAmtEst();        
		   </c:if>
		  		}  
		        var recRateCurrency ; 
		  function saveInvoiceData()  {
		   <c:if test="${multiCurrency=='Y'}">  
		       recRateCurrency = document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value;  
		   </c:if> 
		      }  
		   function findExchangeRecRate(){
		        var invoiceNumber = document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value; 
		        var recRateCurrencyValue = document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value
		        invoiceNumber=invoiceNumber.trim();
		        if(invoiceNumber!='' && (document.forms['accountLineForms'].elements['userRoleExecutive'].value=="no")){
		        alert("Invoice for this line has already been generated, You cannot change Billing Currency.");
		        document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value='${accountLine.recRateCurrency}';
		        }
		        else if (recRateCurrencyValue =='' && invoiceNumber!='' && (document.forms['accountLineForms'].elements['userRoleExecutive'].value=="yes") ){
		        	alert("You cannot select blank Currency as Invoice for this line has already been generated. ")
		        	document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value='${accountLine.recRateCurrency}';
		        }
		        else if (invoiceNumber==''||(document.forms['accountLineForms'].elements['userRoleExecutive'].value=="yes") ){
		          if(invoiceNumber!=''){
		        var agree =confirm("Invoice for this line has already been generated and it will change all corresponding value for the invoice no "+invoiceNumber+".\n\n Are you sure you want to continue with the changes?");
		        if(agree) {
		          document.forms['accountLineForms'].elements['oldRecRateCurrency'].value='${accountLine.recRateCurrency}';
		          var country =document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value; 
		          	document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value=findExchangeRateGlobal(country)
			        document.forms['accountLineForms'].elements['accountLine.racValueDate'].value=currentDateGlobal();
			        var contractRate=0;
	                <c:if test="${contractType}">
	                var contractCurrency = document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value;
		             if(contractCurrency==country){
		            	 document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;
		            	 document.forms['accountLineForms'].elements['accountLine.contractValueDate'].value = document.forms['accountLineForms'].elements['accountLine.racValueDate'].value;
		             }
	                contractRate = document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
	                </c:if>
	                if(contractRate==0){
		                calculateRecRate('none','','','');
		                convertedAmountBilling('none','','','');
	                }else{
	                	calRecCurrencyRate('accountLine.contractRate');
	                }
		       }else {
		        document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value='${accountLine.recRateCurrency}';  
		       }  
		     }else if (invoiceNumber==''){
		        var country =document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value; 
		        document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value=findExchangeRateGlobal(country)
		        document.forms['accountLineForms'].elements['accountLine.racValueDate'].value=currentDateGlobal();
		        var contractRate=0;
                <c:if test="${contractType}">
                var contractCurrency = document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value;
	             if(contractCurrency==country){
	            	 document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value;
	            	 document.forms['accountLineForms'].elements['accountLine.contractValueDate'].value = document.forms['accountLineForms'].elements['accountLine.racValueDate'].value;
	             }
                contractRate = document.forms['accountLineForms'].elements['accountLine.contractRate'].value;
                </c:if>
                if(contractRate==0){
	                calculateRecRate('none','','','');
	                convertedAmountBilling('none','','','');
                }else{
                	calRecCurrencyRate('accountLine.contractRate');
                }
		  }    }  }
		   
		   function currentDateGlobal(){
				var mydate=new Date();
			   	var daym;
			   	var year=mydate.getFullYear()
			   	var y=""+year;
			   	if (year < 1000)
			   	year+=1900
			   	var day=mydate.getDay()
			   	var month=mydate.getMonth()+1
			   	if(month == 1)month="Jan";
			   	if(month == 2)month="Feb";
			   	if(month == 3)month="Mar";
				  if(month == 4)month="Apr";
				  if(month == 5)month="May";
				  if(month == 6)month="Jun";
				  if(month == 7)month="Jul";
				  if(month == 8)month="Aug";
				  if(month == 9)month="Sep";
				  if(month == 10)month="Oct";
				  if(month == 11)month="Nov";
				  if(month == 12)month="Dec";
				  var daym=mydate.getDate()
				  if (daym<10)
				  daym="0"+daym
				  var datam = daym+"-"+month+"-"+y.substring(2,4); 
				  return datam;
			}
		  function findExchangeEstRate(){
		        var country =document.forms['accountLineForms'].elements['accountLine.estCurrency'].value; 
		        document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value=findExchangeRateGlobal(country)
		        document.forms['accountLineForms'].elements['accountLine.estValueDate'].value=currentDateGlobal();
                var contractRate=0;
				<c:if test="${contractType}">
				var estPayableContractCurrency = document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value;
                if(estPayableContractCurrency==country){
                	document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value;
                	document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].value = document.forms['accountLineForms'].elements['accountLine.estValueDate'].value;
                }
                	contractRate = document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
                </c:if> 
                if(contractRate==0){ 
				  calculateEstimateExpense('none');
                }else{
              	  calEstLocalRate('NoCal','accountLine.estimatePayableContractRate');    
                }
		    	<c:if test="${systemDefaultVatCalculation=='true'}">
			    	calculateESTVatAmt();
			    	calculateVatAmtEst();    
		        </c:if>
		  }
		 
		  function calculateEstimateExpense(target){
			  var contractRate=0;
			  <c:if test="${contractType}">
		      contractRate = document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
		      </c:if> 
		      //if(contractRate==0){  
		   var estLocalAmount= document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value ;
		   var estExchangeRate= document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value*1 ;
		   var roundValue=0; 
		  	 if(estExchangeRate==0) {
		         document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=0*1;
		     } else if(estExchangeRate >0) {
		  	 <c:if test="${contractType}"> 
		  	 if(target=='accountLine.estimatePayableContractRate'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
			        ETemp= document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value*1 ;
			        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
					var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
					   if(estimateDeviation!='' && estimateDeviation>0){ 
						   ATemp=(ATemp*estimateDeviation)/100;
						    }
			        roundValue=Math.round(ATemp*10000)/10000;
		     document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue; 
			 }else if(target=='accountLine.estLocalRate'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value;
			        ETemp= document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value*1 ;
			        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
					var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
					   if(estimateDeviation!='' && estimateDeviation>0){ 
						   ATemp=(ATemp*estimateDeviation)/100;
						    }
			        roundValue=Math.round(ATemp*10000)/10000;
		     document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue; 
			 }else if(target=='accountLine.estimateExpense'){
			 }else if(target=='accountLine.estimateRate'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
			        ATemp=((ATemp*quantityVal)/baseRateVal);
					var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
					   if(estimateDeviation!='' && estimateDeviation>0){ 
						   ATemp=(ATemp*estimateDeviation)/100;
						    }
			        roundValue=Math.round(ATemp*10000)/10000;
		     		document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue; 
			 }else{
			       var amount=estLocalAmount/estExchangeRate; 
			       roundValue=Math.round(amount*10000)/10000;
			       document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue ;
		      	 }		       
		  	 </c:if>
		   <c:if test="${!contractType}"> 
				   var amount=estLocalAmount/estExchangeRate; 
				   roundValue=Math.round(amount*10000)/10000;
				   document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue ;
		    </c:if>
		     }
		     var estimatepasspercentageRound=0;
		     var estimatepasspercentage = 0;
		     var estimateRevenue = document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value; 
		     estimatepasspercentage = (estimateRevenue/roundValue)*100;
		  	 estimatepasspercentageRound=Math.round(estimatepasspercentage);
		  	 if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
		  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
		  	   }else{
		  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
		  	   } 
		  	if(target!='accountLine.estimateRate'){
		     calculateEstimateRate(target);
		  	}
		     if(target!='accountLine.estLocalRate'){
		     	calculateEstimateLocalRate(target);
		     }
		     <c:if test="${contractType}">
		     try{
		     calEstimatePayableContractRateByBase(target);
		     }catch(e){}
		     </c:if> 
		     calculateESTVatAmt();
		  }  
		  function calculateEstimateExpenseByExchangeRate(){
			  var contractRate=0;
			  <c:if test="${contractType}">
		      contractRate = document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
		      </c:if> 
		      if(contractRate==0){  
		   var estLocalAmount= document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value ;
		   var estExchangeRate= document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value*1 ;
		   var roundValue=0; 
		  	 if(estExchangeRate ==0) {
		         document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=0*1;
		     } else if(estExchangeRate >0) {
		       var amount=estLocalAmount/estExchangeRate; 
		       roundValue=Math.round(amount*10000)/10000;
		       document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue ; 
		     }
		     var estimatepasspercentageRound=0;
		     var estimatepasspercentage = 0;
		     var estimateRevenue = document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value; 
		     estimatepasspercentage = (estimateRevenue/roundValue)*100;
		  	 estimatepasspercentageRound=Math.round(estimatepasspercentage);
		  	 if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
		  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
		  	   }else{
		  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
		  	   } 
		     calculateEstimateRate('none');
		     calculateEstimateLocalRate('none');
		      }else{
		    	  calEstLocalRate('NoCal','accountLine.estimatePayableContractRate');    
		      } }         
		  function calculateEstimateRate(target){
			   <c:if test="${contractType}">
			   if((target=='accountLine.estimateQuantity')||(target=='accountLine.estLocalRate')||(target=='accountLine.estimatePayableContractRate')||(target=='accountLine.basis')){
			      var estimatePayableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value;
			      var estimatePayableContractRate=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
			      estimatePayableContractRate=Math.round(estimatePayableContractRate*10000)/10000;
			      var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
			      var roundValue=Math.round(estimateRate*10000)/10000;
			      document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=roundValue ;
			   }else{
				    var estimateQuantity =0;
				    var estimateDeviation=100;
				    estimateQuantity=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
				    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
				    var estimateExpense =eval(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value); 
				    var estimateRate=0;
				    var estimateRateRound=0; 
				    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
			         estimateQuantity =document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			         } 
			        if( basis=="cwt" || basis=="%age"){
			       	  estimateRate=(estimateExpense/estimateQuantity)*100;
			       	  estimateRateRound=Math.round(estimateRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=estimateRateRound;	  	
			       	}  else if(basis=="per 1000"){
			       	  estimateRate=(estimateExpense/estimateQuantity)*1000;
			       	  estimateRateRound=Math.round(estimateRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=estimateRateRound;		  	
			       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
			          estimateRate=(estimateExpense/estimateQuantity); 
			       	  estimateRateRound=Math.round(estimateRate*10000)/10000; 
			       	  document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=estimateRateRound;		  	
				    }
				    estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value 
				    if(estimateDeviation!='' && estimateDeviation>0){
				    	estimateRateRound=estimateRateRound*100/estimateDeviation;
				    	estimateRateRound=Math.round(estimateRateRound*10000)/10000;
				    document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=estimateRateRound;		
				    } } 
		      </c:if>
		      <c:if test="${!contractType}">
			    var estimateQuantity =0;
			    var estimateDeviation=100;
			    estimateQuantity=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
			    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
			    var estimateExpense =eval(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value); 
			    var estimateRate=0;
			    var estimateRateRound=0; 
			    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
		         estimateQuantity =document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		         } 
		        if( basis=="cwt" || basis=="%age"){
		       	  estimateRate=(estimateExpense/estimateQuantity)*100;
		       	  estimateRateRound=Math.round(estimateRate*10000)/10000;
		       	  document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=estimateRateRound;	  	
		       	}  else if(basis=="per 1000"){
		       	  estimateRate=(estimateExpense/estimateQuantity)*1000;
		       	  estimateRateRound=Math.round(estimateRate*10000)/10000;
		       	  document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=estimateRateRound;		  	
		       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		          estimateRate=(estimateExpense/estimateQuantity); 
		       	  estimateRateRound=Math.round(estimateRate*10000)/10000; 
		       	  document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=estimateRateRound;		  	
			    }
			    estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value 
			    if(estimateDeviation!='' && estimateDeviation>0){
			    	estimateRateRound=estimateRateRound*100/estimateDeviation;
			    	estimateRateRound=Math.round(estimateRateRound*10000)/10000;
			    document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=estimateRateRound;		
			    }
			  </c:if>
		  }
		 function calculateEstimateLocalRate(target){
			 <c:if test="${contractType}"> 
			 if(target=='accountLine.estLocalAmount'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value;
			        ATemp=(ATemp*baseRateVal)/quantityVal;
				    if(estimateDeviationVal!='' && estimateDeviationVal>0){
				    	ATemp=ATemp*100/estimateDeviationVal;
				    	ATemp=Math.round(ATemp*10000)/10000;
				    }
		     document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=ATemp; 
			 }else if(target=='accountLine.estimateRate'){
				 var roundValue='0.00';
			     var ETemp=0.00;
			     var ATemp=0.00;
			     ETemp = document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value;  
			     ATemp = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
			     ATemp=ATemp*ETemp;
			     roundValue=Math.round(ATemp*10000)/10000;
		    	document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=roundValue; 
			 }else if(target=='accountLine.estimateExpense'){
				 var roundValue='0.00';
			     var ETemp=0.00;
			     var ATemp=0.00;
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
				 var baseRateVal=1;
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;  
			        ATemp=(ATemp*baseRateVal)/quantityVal; 
				    if(estimateDeviationVal!='' && estimateDeviationVal>0){
				    	ATemp=ATemp*100/estimateDeviationVal;
				    }
			     ETemp = document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value;  
			     ATemp=ATemp*ETemp;
			     roundValue=Math.round(ATemp*10000)/10000;
		    	document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=roundValue; 
			 }else if(target=='accountLine.estLocalRate'){
			 }else{
				 var roundValue='0.00';
				 	var QTemp=0.00; 
				    var ATemp =0.00;
				    var FTemp =0.00;
				    var ETemp =0.00;
				    ETemp= document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value*1 ; 
				    FTemp =document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
				    FTemp=FTemp/ETemp;
				    ETemp= document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value*1 ;				    
				    FTemp=FTemp*ETemp;
		     roundValue=Math.round(FTemp*10000)/10000;
		     document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=roundValue;
			 }
			 </c:if>
			 <c:if test="${!contractType}">
		    var estimateQuantity =0;
		    var estimateExpense =0;
			    estimateQuantity=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
			    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
			    estimateExpense =document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value; 
			    var estimateRate=0;
			    var estimateRateRound=0; 
			    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
		         estimateQuantity =document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		         } 
		        if( basis=="cwt" || basis=="%age"){
		       	  estimateRate=(estimateExpense/estimateQuantity)*100;
		       	  estimateRateRound=Math.round(estimateRate*10000)/10000;
		       	  document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=estimateRateRound;	  	
		       	}  else if(basis=="per 1000"){
		       	  estimateRate=(estimateExpense/estimateQuantity)*1000;
		       	  estimateRateRound=Math.round(estimateRate*10000)/10000;
		       	  document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=estimateRateRound;		  	
		       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		          estimateRate=(estimateExpense/estimateQuantity); 
		       	  estimateRateRound=Math.round(estimateRate*10000)/10000; 
		       	  document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=estimateRateRound;		  	
			    }  
		        estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value 
			    if(estimateDeviation!='' && estimateDeviation>0){
			    	estimateRateRound=estimateRateRound*100/estimateDeviation;
			    	estimateRateRound=Math.round(estimateRateRound*10000)/10000;
			    document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=estimateRateRound;		
			    }
			 </c:if>
		 }
		function calculateEstimateLocalAmount(temp,target){ 
			<c:if test="${contractType}"> 
		      	  if(target=='accountLine.estLocalRate'){
		   		 var roundValue='0.00';
		   		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		   		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;   		
		   		 var baseRateVal=1;
		   	 	 var ATemp=0.00; 
		   	     var ETemp =0.00;	    		 
		   		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		   			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		   	         } 
		   	        if( bassisVal=="cwt" || bassisVal=="%age"){
		   	        	baseRateVal=100;
		   	         }  else if(bassisVal=="per 1000"){
		   	        	 baseRateVal=1000;
		   	       	 } else {
		   	       		baseRateVal=1;  	
		   		    }
		   	        ATemp=document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value;
		   	        ATemp=(ATemp*quantityVal)/baseRateVal;
		 		   var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
				   if(estimateDeviation!='' && estimateDeviation>0){ 
					   ATemp=(ATemp*estimateDeviation)/100;
					    }
		   	     	roundValue=Math.round(ATemp*10000)/10000;
		        	document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value=roundValue; 
		   			 }else if(target=='accountLine.estimateRate'){
		   		 var roundValue='0.00';
		   		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		   		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;   		
		   		 var baseRateVal=1;
		   	 	 var ATemp=0.00; 
		   	     var ETemp =0.00;	    		 
		   		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		   			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		   	         } 
		   	        if( bassisVal=="cwt" || bassisVal=="%age"){
		   	        	baseRateVal=100;
		   	         }  else if(bassisVal=="per 1000"){
		   	        	 baseRateVal=1000;
		   	       	 } else {
		   	       		baseRateVal=1;  	
		   		    }
		   	        ETemp=document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value;
		   	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
		   	        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
		  		   var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
				   if(estimateDeviation!='' && estimateDeviation>0){ 
					   ATemp=(ATemp*estimateDeviation)/100;
					    }
		   	     	roundValue=Math.round(ATemp*10000)/10000;
		        	document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value=roundValue;
		   		 
		   			 }else if(target=='accountLine.estLocalAmount'){
		 			  }else{
		   		 var roundValue='0.00';
		   		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		   		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;   		
		   		 var estimatePayableContractExchangeRateVal=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value;
		   		 var estExchangeRateVal=document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value; 
		   		 var baseRateVal=1;
		   	 	 var ATemp=0.00; 
		   	     var ETemp =0.00;	    		 
		   		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		   			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		   	         } 
		   	        if( bassisVal=="cwt" || bassisVal=="%age"){
		   	        	baseRateVal=100;
		   	         }  else if(bassisVal=="per 1000"){
		   	        	 baseRateVal=1000;
		   	       	 } else {
		   	       		baseRateVal=1;  	
		   		    }
		   	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
		   	        ATemp=(((ATemp/estimatePayableContractExchangeRateVal)*estExchangeRateVal)*quantityVal)/baseRateVal;
		   	        var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
				   if(estimateDeviation!='' && estimateDeviation>0){ 
					   ATemp=(ATemp*estimateDeviation)/100;
					    }   	        
		   	     roundValue=Math.round(ATemp*10000)/10000;
		        document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value=roundValue;
		  		 	 }		     
			  </c:if>
			   <c:if test="${!contractType}"> 
			       var estimateQuantity =0;
			       var estLocalRate =0;
			       	    estimateQuantity=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
			       	    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
			       	    estLocalRate =document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value;  
			       	    var estimateLocalAmount=0;
			       	    var estimateLocalAmountRound=0; 
			       	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
			                estimateQuantity =document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			                } 
			               if( basis=="cwt" || basis=="%age"){
			              	  estimateLocalAmount=(estLocalRate*estimateQuantity)/100;
			              	  estimateLocalAmountRound=Math.round(estimateLocalAmount*10000)/10000;
			              	  document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value=estimateLocalAmountRound;	  	
			              	}  else if(basis=="per 1000"){
			              	  estimateLocalAmount=(estLocalRate*estimateQuantity)/1000;
			              	  estimateLocalAmountRound=Math.round(estimateLocalAmount*10000)/10000;
			              	  document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value=estimateLocalAmountRound;		  	
			              	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
			                 estimateLocalAmount=(estLocalRate*estimateQuantity); 
			              	  estimateLocalAmountRound=Math.round(estimateLocalAmount*10000)/10000; 
			              	  document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value=estimateLocalAmountRound;		  	
			       	    }
			               estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
			               if(estimateDeviation!='' && estimateDeviation>0){
			               	estimateLocalAmountRound=estimateLocalAmountRound*estimateDeviation/100;
			               	estimateLocalAmountRound=Math.round(estimateLocalAmountRound*10000)/10000;
			                 document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value = estimateLocalAmountRound;	 
			             }
		    </c:if> 
		        var contractRate=0;
		  	  <c:if test="${contractType}">
		        contractRate = document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
		        </c:if>  
		        //if(contractRate==0 && temp == "form"){   
			    
			    if(temp == "form"){
			    calEstimatePayableContractRateByBase(target);
			    	calculateESTVatAmt();
		        } calculateEstimateExpense(target);}	    
		  function findExchangeRevisionRate (){
		        var country =document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value; 
		        document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value=findExchangeRateGlobal(country);
		        document.forms['accountLineForms'].elements['accountLine.revisionValueDate'].value=currentDateGlobal();
                var contractRate=0;
				<c:if test="${contractType}">
				   var revisionPayableContractCurrency = document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value;
	                if(revisionPayableContractCurrency==country){
	                	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value ;
	                	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].value = document.forms['accountLineForms'].elements['accountLine.revisionValueDate'].value ;
	               } 
                	contractRate = document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
                </c:if>
                if(contractRate==0){
				  calculateRevisionExpense('none'); 
                } else{
              	  calRevisionLocalRate('NoCal','accountLine.revisionPayableContractRate');
                }
			    try{
			    	<c:if test="${systemDefaultVatCalculation=='true'}">
				    	calculateREVVatAmt();	  
				    	calculateVatAmtRevision();    
			        </c:if>
			    }catch(e){}
		  }  
		  
		   function calculateRevisionExpense(target){
			   var contractRate=0;
				  <c:if test="${contractType}">
		    contractRate = document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
		    </c:if>
		   // if(contractRate==0){
		   var revisionLocalAmount= document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value ;
		   var revisionExchangeRate= document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value*1 ;
		   var roundValue=0; 
		  	 if(revisionExchangeRate ==0) {
		         document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=0*1;
		     } else if(revisionExchangeRate >0) { 
		    	 <c:if test="${contractType}"> 
		      	 if(target=='accountLine.revisionPayableContractRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
		    	        ETemp= document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value*1 ;
		    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
						var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*10000)/10000;
		         document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue; 
		    	 }else if(target=='accountLine.revisionLocalRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value;
		    	        ETemp= document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value*1 ;
		    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
						var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*10000)/10000;
		         document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue; 
		    	 }else if(target=='accountLine.revisionExpense'){
		    	 }else if(target=='accountLine.revisionRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
		    	        ATemp=((ATemp*quantityVal)/baseRateVal);
						var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*10000)/10000;
		         		document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue; 
		    	 }else{
		    	       var amount=revisionLocalAmount/revisionExchangeRate; 
		    	       roundValue=Math.round(amount*10000)/10000;
		    	       document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue ; 
		          	 }		       
		      	 </c:if>
		       <c:if test="${!contractType}"> 
			       var amount=revisionLocalAmount/revisionExchangeRate; 
			       roundValue=Math.round(amount*10000)/10000;
			       document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue ; 
		        </c:if>
		     }
		      var revisionRevenue = document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;
		      var revisionPassPercentageRound=0;
		      var revisionPassPercentage=0; 
		      revisionPassPercentage = (revisionRevenue/roundValue)*100;
		  	  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
		  	  if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
		  	   	document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
		  	   }else{
		  	    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionPassPercentageRound;
		  	   }    
		 	   if(target!='accountLine.revisionRate'){
		     	calculateRevisionRate(target);
		 	   }
		     calculateRevisionLocalRate(target);
		     <c:if test="${contractType}">
		     try{
		    calRevisionPayableContractRateByBase(target);
		     }catch(e){}
		     </c:if>
		     calculateREVVatAmt(); 
		   } 
		   function calculateRevisionExpenseByExchangerate(target){
			   var contractRate=0;
				  <c:if test="${contractType}">
		    contractRate = document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
		    </c:if>
		   if(contractRate==0){
		   var revisionLocalAmount= document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value ;
		   var revisionExchangeRate= document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value*1 ;
		   var roundValue=0; 
		  	 if(revisionExchangeRate ==0) {
		         document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=0*1;
		     } else if(revisionExchangeRate >0) {
		       var amount=revisionLocalAmount/revisionExchangeRate; 
		       roundValue=Math.round(amount*10000)/10000;
		       document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue ; 
		     }
		      var revisionRevenue = document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;
		      var revisionPassPercentageRound=0;
		      var revisionPassPercentage=0; 
		      revisionPassPercentage = (revisionRevenue/roundValue)*100;
		  	  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
		  	  if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
		  	   	document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
		  	   }else{
		  	    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionPassPercentageRound;
		  	   }
		     calculateRevisionRate('none');
		     calculateRevisionLocalRate('none');
		     } else{
		    	  calRevisionLocalRate('NoCal','accountLine.revisionPayableContractRate');
		     } }         
		   function calculateRevisionRate(target){
			   <c:if test="${contractType}">
			   if((target=='accountLine.revisionQuantity')||(target=='accountLine.revisionLocalRate')||(target=='accountLine.revisionPayableContractRate')||(target=='accountLine.basis')){
			      var estimatePayableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value;
			      var estimatePayableContractRate=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
			      estimatePayableContractRate=Math.round(estimatePayableContractRate*10000)/10000;
			      var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
			      var roundValue=Math.round(estimateRate*10000)/10000;
			      document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=roundValue ;
			   }else{
			        var revisionQuantity =0;
			        var revisionDeviation=100;
			        revisionQuantity=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value; 
				    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
				    var revisionExpense =eval(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value); 
				    var revisionRate=0;
				    var revisionRateRound=0; 
				    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
			         revisionQuantity =document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
			         } 
			        if( basis=="cwt" || basis=="%age"){
			       	  revisionRate=(revisionExpense/revisionQuantity)*100;
			       	  revisionRateRound=Math.round(revisionRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRateRound;	  	
			       	} else if(basis=="per 1000"){
			       	  revisionRate=(revisionExpense/revisionQuantity)*1000;
			       	  revisionRateRound=Math.round(revisionRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRateRound;	  	  	
			       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
			          revisionRate=(revisionExpense/revisionQuantity);
			       	  revisionRateRound=Math.round(revisionRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRateRound;	  			  	
				    } 
				    revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value 
				    if(revisionDeviation!='' && revisionDeviation>0){
				    	revisionRateRound=revisionRateRound*100/revisionDeviation;
				        revisionRateRound=Math.round(revisionRateRound*10000)/10000;
				    document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRateRound;		 
				       }   }
				   </c:if>
				   <c:if test="${!contractType}">
		        var revisionQuantity =0;
		        var revisionDeviation=100;
		        revisionQuantity=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value; 
			    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
			    var revisionExpense =eval(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value); 
			    var revisionRate=0;
			    var revisionRateRound=0; 
			    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		         revisionQuantity =document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		         } 
		        if( basis=="cwt" || basis=="%age"){
		       	  revisionRate=(revisionExpense/revisionQuantity)*100;
		       	  revisionRateRound=Math.round(revisionRate*10000)/10000;
		       	  document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRateRound;	  	
		       	} else if(basis=="per 1000"){
		       	  revisionRate=(revisionExpense/revisionQuantity)*1000;
		       	  revisionRateRound=Math.round(revisionRate*10000)/10000;
		       	  document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRateRound;	  	  	
		       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		          revisionRate=(revisionExpense/revisionQuantity);
		       	  revisionRateRound=Math.round(revisionRate*10000)/10000;
		       	  document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRateRound;	  			  	
			    } 
			    revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value 
			    if(revisionDeviation!='' && revisionDeviation>0){
			    	revisionRateRound=revisionRateRound*100/revisionDeviation;
			        revisionRateRound=Math.round(revisionRateRound*10000)/10000;
			    document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=revisionRateRound;		 
			       }
			       </c:if>
			       }
		  function calculateRevisionLocalRate(target){
		     <c:if test="${contractType}"> 
		         if(target=='accountLine.revisionLocalAmount'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value;
		    	        ATemp=(ATemp*baseRateVal)/quantityVal;
		    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		    		    	ATemp=ATemp*100/estimateDeviationVal;
		    		    	ATemp=Math.round(ATemp*10000)/10000;
		    		    }
		         document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=ATemp; 
		    	 }else if(target=='accountLine.revisionRate'){
		    		 var roundValue='0.00';
		    	     var ETemp=0.00;
		    	     var ATemp=0.00;
		    	     ETemp = document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value;  
		    	     ATemp = document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
		    	     ATemp=ATemp*ETemp;
		    	     roundValue=Math.round(ATemp*10000)/10000;
		        	document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=roundValue; 
		    	 }else if(target=='accountLine.revisionExpense'){
		    		 var roundValue='0.00';
		    	     var ETemp=0.00;
		    	     var ATemp=0.00;
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    		 var baseRateVal=1;
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;  
		    	        ATemp=(ATemp*baseRateVal)/quantityVal; 
		    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		    		    	ATemp=ATemp*100/estimateDeviationVal;
		    		    }
		    	     ETemp = document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value;  
		    	     ATemp=ATemp*ETemp;
		    	     roundValue=Math.round(ATemp*10000)/10000;
		        	document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=roundValue; 
		    	 }else if(target=='accountLine.revisionLocalRate'){
		    	 }else{
				 	var QTemp=0.00; 
				    var ATemp =0.00;
				    var FTemp =0.00;
				    var ETemp =0.00;
				    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value*1 ; 
				    FTemp =document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
				    FTemp=FTemp/ETemp;
				    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value*1 ;				    
				    FTemp=FTemp*ETemp;
		     roundValue=Math.round(FTemp*10000)/10000;
		     document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=roundValue;
		    	 }        
				</c:if>
				<c:if test="${!contractType}">
			    var revisionQuantity =0;
			    var revisionExpense=0;
			        revisionQuantity=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value; 
				    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
				    revisionExpense = document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value; 
				    var revisionRate=0;
				    var revisionRateRound=0; 
				    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
			         revisionQuantity =document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
			         } 
			        if( basis=="cwt" || basis=="%age"){
			       	  revisionRate=(revisionExpense/revisionQuantity)*100;
			       	  revisionRateRound=Math.round(revisionRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=revisionRateRound;	  	
			       	} else if(basis=="per 1000"){
			       	  revisionRate=(revisionExpense/revisionQuantity)*1000;
			       	  revisionRateRound=Math.round(revisionRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=revisionRateRound;	  	  	
			       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
			          revisionRate=(revisionExpense/revisionQuantity);
			       	  revisionRateRound=Math.round(revisionRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=revisionRateRound;	  			  	
				    } 
			        revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value 
				    if(revisionDeviation!='' && revisionDeviation>0){
				    	revisionRateRound=revisionRateRound*100/revisionDeviation;
				    	revisionRateRound=Math.round(revisionRateRound*10000)/10000;
				    document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=revisionRateRound;		 
				       }
				</c:if>  
			    } 
		 function calculateRevisionLocalAmount(temp,target){
			 <c:if test="${contractType}"> 
		  	  if(target=='accountLine.revisionLocalRate'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;   		
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value;
			        ATemp=(ATemp*quantityVal)/baseRateVal;
					var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
					if(revisionDeviation!='' && revisionDeviation>0){ 
						   ATemp=(ATemp*revisionDeviation)/100;
					}	        
			     	roundValue=Math.round(ATemp*10000)/10000;
		    	document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=roundValue; 
					 }else if(target=='accountLine.revisionRate'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;   		
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ETemp=document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value;
			        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
			        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
					var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
					if(revisionDeviation!='' && revisionDeviation>0){ 
						   ATemp=(ATemp*revisionDeviation)/100;
					}
			     	roundValue=Math.round(ATemp*10000)/10000;
		    	document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=roundValue;
				 
					 }else if(target=='accountLine.revisionLocalAmount'){
					  }else{
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;   		
				 var estimatePayableContractExchangeRateVal=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value;
				 var estExchangeRateVal=document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
			        ATemp=(((ATemp/estimatePayableContractExchangeRateVal)*estExchangeRateVal)*quantityVal)/baseRateVal;
					var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
					if(revisionDeviation!='' && revisionDeviation>0){ 
						   ATemp=(ATemp*revisionDeviation)/100;
					}
			     	roundValue=Math.round(ATemp*10000)/10000;
		    		document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=roundValue;
				 	 }		     
		  </c:if>
		   <c:if test="${!contractType}"> 
		   var revisionQuantity =0;
		   var revisionLocalRate=0;
		         revisionQuantity=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value; 
		 	    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
		 	     revisionLocalRate =document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value; 
		 	    var revisionLocalAmount=0;
		 	    var revisionLocalAmountRound=0; 
		 	    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		          revisionQuantity =document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		          } 
		         if( basis=="cwt" || basis=="%age"){
		        	  revisionLocalAmount=(revisionLocalRate*revisionQuantity)/100;
		        	  revisionLocalAmountRound=Math.round(revisionLocalAmount*10000)/10000;
		        	  document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=revisionLocalAmountRound;	  	
		        	} else if(basis=="per 1000"){
		        	  revisionLocalAmount=(revisionLocalRate*revisionQuantity)/1000;
		        	  revisionLocalAmountRound=Math.round(revisionLocalAmount*10000)/10000;
		        	  document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=revisionLocalAmountRound;	  	  	
		        	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		           revisionLocalAmount=(revisionLocalRate*revisionQuantity);
		        	  revisionLocalAmountRound=Math.round(revisionLocalAmount*10000)/10000;
		        	  document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value=revisionLocalAmountRound;	  			  	
		 	    } 
		         revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		         if(revisionDeviation!='' && revisionDeviation>0){
		         	revisionLocalAmountRound=revisionLocalAmountRound*revisionDeviation/100;
		         	revisionLocalAmountRound=Math.round(revisionLocalAmountRound*10000)/10000;
		           document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value = revisionLocalAmountRound;	 
		       }  
					  </c:if>
		        var contractRate=0;
				  <c:if test="${contractType}">
		  contractRate = document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
		  </c:if>
		  //if(contractRate==0 && temp == "form"){
			    
			    if(temp == "form"){
			    	calRevisionPayableContractRateByBase(target);
		  } calculateRevisionExpense(target);}               
		  function checkBilltoCode(){
		        var invoiceNumber = document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value; 
		        invoiceNumber=invoiceNumber.trim();
		        if(invoiceNumber!='' && (document.forms['accountLineForms'].elements['userRoleExecutive'].value=="no")){
		        alert('You can not change the Billing Party as Invoice# has been already generated')
		        } else if (invoiceNumber==''||(document.forms['accountLineForms'].elements['userRoleExecutive'].value=="yes")){
		        if(invoiceNumber!=''){
		          var agree =confirm("Invoice for this line has already been generated and it will change all corresponding value for the invoice no "+invoiceNumber+".\n\n Are you sure you want to continue with the changes?");
		          if(agree) {
		            document.forms['accountLineForms'].elements['oldRecRateCurrency'].value='${accountLine.recRateCurrency}';
		            openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.billToName&fld_code=accountLine.billToCode');document.forms['accountLineForms'].elements['accountLine.recQuantity'].select();
		          }else { 
		        }  }
		     else if (invoiceNumber==''){
		        openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.billToName&fld_code=accountLine.billToCode');document.forms['accountLineForms'].elements['accountLine.recQuantity'].select();
		       } }  } 
 
		   function checkInvoice()  {
		    <c:if test="${multiCurrency=='Y'}"> 
		    var invoiceNumber = document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value; 
		    invoiceNumber=invoiceNumber.trim(); 
		    if(invoiceNumber!=''){
		    if(document.forms['accountLineForms'].elements['userRoleExecutive'].value=="no"){
		     document.forms['accountLineForms'].elements['accountLine.recQuantity'].readOnly=true;
		     document.forms['accountLineForms'].elements['accountLine.itemNew'].readOnly=true;
		     document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=true;
		     <c:if test="${adminInvoiceAmountEdit}">
		     document.forms['accountLineForms'].elements['accountLine.recRate'].readOnly=false;
		     </c:if>
		     document.forms['accountLineForms'].elements['accountLine.basisNew'].readOnly=true;
		     document.forms['accountLineForms'].elements['accountLine.basisNewType'].readOnly=true; 
		     document.forms['accountLineForms'].elements['accountLine.authorization'].readOnly=true;
		     document.forms['accountLineForms'].elements['accountLine.actualRevenue'].readOnly=true;
		     document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=true;
		     <c:if test="${adminInvoiceAmountEdit}">
		     document.forms['accountLineForms'].elements['accountLine.recCurrencyRate'].readOnly=false;
		     </c:if>
		     document.forms['accountLineForms'].elements['accountLine.recRateExchange'].readOnly=true;
		     document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].readOnly=true;
		     document.forms['accountLineForms'].elements['accountLine.recRateExchange'].readOnly=true;  
		    }  }
		    </c:if> 
		   }  
		    function roundRecRate() {
		     var recRateDummy =document.forms['accountLineForms'].elements['accountLine.recRate'].value;
		     document.forms['accountLineForms'].elements['accountLine.recRate'].value=Math.round(recRateDummy*10000)/10000; 
		     document.forms['accountLineForms'].setAttribute('bypassDirtyCheck',true);
		     }
		    function receivedAmountActive()  {  
		     <c:if test="${accountInterface!='Y'}">
		       if(document.forms['accountLineForms'].elements['accountLine.paymentStatus'].value=='Partially Paid'){
				document.forms['accountLineForms'].elements['accountLine.receivedAmount'].readOnly=false;
				}else {
				document.forms['accountLineForms'].elements['accountLine.receivedAmount'].readOnly=true;
				}
				</c:if>
		     } 
		     function changeCalOpenarvalue() {
				  	document.forms['accountLineForms'].elements['calOpener'].value='open';
				  }  
		  function goPrev() {
			progressBarAutoSave('1');
			var soIdNum =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
			var seqNm =document.forms['accountLineForms'].elements['serviceOrder.sequenceNumber'].value;
			var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
			 http5.open("GET", url, true); 
		     http5.onreadystatechange = handleHttpResponseOtherShip; 
		     http5.send(null); 
		   }
		   function goNext() {
			progressBarAutoSave('1');
			var soIdNum =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
			var seqNm =document.forms['accountLineForms'].elements['serviceOrder.sequenceNumber'].value;
			var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
			 http5.open("GET", url, true); 
		     http5.onreadystatechange = handleHttpResponseOtherShip; 
		     http5.send(null); 
		   }
		   function handleHttpResponseOtherShip(){
		             if (http5.readyState == 4) {
		               var results = http5.responseText
		               results = results.trim();
		               location.href = 'accountLineList.html?sid='+results;
		             }   }     
		function findCustomerOtherSO(position) {
		 var sid=document.forms['accountLineForms'].elements['customerFile.id'].value;
		 var soIdNum=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
		 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
		  ajax_showTooltip(url,position);	
		  } 
		function goToUrl(id) {
				location.href = "accountLineList.html?sid="+id;
			}
			function goPrevChild() {
				 if(document.forms['accountLineForms'].elements['formStatus'].value == '1'){
				var accountSaveConfirmation = confirm("Do you want to save the changes you made?");
				if(accountSaveConfirmation){
					progressBarAutoSave('1');
					processAutoSave(document.forms['accountLineForms'], 'saveAccountLine!saveOnTabChange.html', 'editAccountLine.html?sid=${serviceOrder.id}&id='+document.forms['accountLineForms'].elements['accountLine.id'].value);
					var sidNum =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
					var soIdNum =document.forms['accountLineForms'].elements['accountLine.id'].value;
					var url="editPrevAccountLine.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
					 http5.open("GET", url, true); 
				     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
				     http5.send(null); 
				}else{
					progressBarAutoSave('1');
					var sidNum =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
					var soIdNum =document.forms['accountLineForms'].elements['accountLine.id'].value;
					var url="editPrevAccountLine.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
					 http5.open("GET", url, true); 
				     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
				     http5.send(null); 
				}
			}else{
				progressBarAutoSave('1');
				var sidNum =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
				var soIdNum =document.forms['accountLineForms'].elements['accountLine.id'].value;
				var url="editPrevAccountLine.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
				 http5.open("GET", url, true); 
			     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
			     http5.send(null); 
			} }
		   function goNextChild() {
			   if(document.forms['accountLineForms'].elements['formStatus'].value == '1'){
			   var accountSaveConfirmation = confirm("Do you want to save the changes you made?");
				if(accountSaveConfirmation){
					progressBarAutoSave('1');
					processAutoSave(document.forms['accountLineForms'], 'saveAccountLine!saveOnTabChange.html', 'editAccountLine.html?sid=${serviceOrder.id}&id='+document.forms['accountLineForms'].elements['accountLine.id'].value);
					var sidNum =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
					var soIdNum =document.forms['accountLineForms'].elements['accountLine.id'].value;
					var url="editNextAccountLine.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
					 http5.open("GET", url, true); 
				     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
				     http5.send(null); 
				}else{
					progressBarAutoSave('1');
					var sidNum =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
					var soIdNum =document.forms['accountLineForms'].elements['accountLine.id'].value;
					var url="editNextAccountLine.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
					 http5.open("GET", url, true); 
				     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
				     http5.send(null); 
				}
			   }else{
					progressBarAutoSave('1');
					var sidNum =document.forms['accountLineForms'].elements['serviceOrder.id'].value;
					var soIdNum =document.forms['accountLineForms'].elements['accountLine.id'].value;
					var url="editNextAccountLine.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
					 http5.open("GET", url, true); 
				     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
				     http5.send(null); 
				} }
		   function handleHttpResponseOtherShipChild(){
		             if (http5.readyState == 4)  {
		               var results = http5.responseText
		               results = results.trim();
		               location.href = 'editAccountLine.html?sid=${serviceOrder.id}&id='+results;
		             } }     
		function findCustomerOtherSOChild(position) {
		 var sidNum=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
		 var soIdNum=document.forms['accountLineForms'].elements['accountLine.id'].value;
		 var url="accountLineOtherSO.html?ajax=1&decorator=simple&popup=true&sidNum=" + encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
		  ajax_showTooltip(url,position);	
		  }   
		function goToUrlChild(id) {
				location.href = "editAccountLine.html?sid=${serviceOrder.id}&id="+id;
			}

		function settSelectedAccountId(id){
			document.getElementById('setSelectedAccountId').value = id;
		}
		function chechCreditInvoice(){
			 var actualRevenue=eval(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value); 
			 var creditInvoice=document.forms['accountLineForms'].elements['accountLine.creditInvoiceNumber'].value;
			 creditInvoice=creditInvoice.trim(); 
			 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
			 var distributionAmount=eval(document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value);
			 if((distributionAmount<0) && (creditInvoice!='')){
			 document.getElementById("creditInvoice").style.display="block";
			 document.getElementById("creditInvoice1").style.display="block";
			 }else{
			 document.getElementById("creditInvoice").style.display="none";
			 document.getElementById("creditInvoice1").style.display="none";
			 }
			 </c:if>
			 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
			 if((actualRevenue<0) && (creditInvoice!='')){
			 document.getElementById("creditInvoice").style.display="block";
			 document.getElementById("creditInvoice1").style.display="block";
			}else{
			document.getElementById("creditInvoice").style.display="none";
			document.getElementById("creditInvoice1").style.display="none";
			}
			</c:if>
			} 
			function getPayPercent(){ 
			<c:if test="${systemDefaultVatCalculation=='true'}">
			var payAccDate='';
			 <c:if test="${accountInterface=='Y'}">
			  payAccDate=document.forms['accountLineForms'].elements['accountLine.payAccDate'].value;
		     </c:if>
		       if (payAccDate!='') { 
		        alert("You can not change the VAT as Sent To Acc has been already filled");
		        document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value='${accountLine.payVatDescr}';
		        } else{
			var relo='';
			<c:forEach var="entry" items="${payVatPercentList}" > 
			if(relo==''){
			  if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=='${entry.key}'){
			      document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value='${entry.value}';
			      calculatePayVatAmt();
			      calculateESTVatAmt();
			      calculateREVVatAmt();	      
			      relo="yes";
			      }   }
			     </c:forEach>
			     <configByCorp:fieldVisibility componentId="accountLine.payVatGl">
			     var relo1='';
				 	<c:forEach var="entry1" items="${payVatPayGLMap}" > 
				 	if(relo1 ==''){
				 	  if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=='${entry1.key}'){
				 	      document.forms['accountLineForms'].elements['accountLine.payVatGl'].value='${entry1.value}'; 
				 	      relo1 ="yes";
				 	      }   }
				 	     </c:forEach>
				 	    relo1='';
					 	<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
					 	if(relo1 ==''){
					 	  if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=='${entry1.key}'){
					 	      document.forms['accountLineForms'].elements['accountLine.qstPayVatGl'].value='${entry1.value}'; 
					 	      relo1 ="yes";
					 	      }   }
					 	     </c:forEach>		 	     
			 </configByCorp:fieldVisibility> 
			     if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value==''){
		            document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value=0;
		            calculatePayVatAmt();
		  	        calculateESTVatAmt();
			        calculateREVVatAmt();	      
		            
		      }  }
		      </c:if>
			}
			function calculateREVVatAmt(){
				   <c:if test="${systemDefaultVatCalculation=='true'}">
				    var payVatAmt=0;
					if(document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value!=''){
					var payVatPercent=eval(document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value);
					var actualExpense= eval(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value);
					<c:if test="${contractType}"> 
					actualExpense= eval(document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value);
					</c:if>
					payVatAmt=(actualExpense*payVatPercent)/100;
					payVatAmt=Math.round(payVatAmt*10000)/10000; 
					document.forms['accountLineForms'].elements['accountLine.revisionExpVatAmt'].value=payVatAmt;
					}
					</c:if>
					}	
			function calculateESTVatAmt(){
				   <c:if test="${systemDefaultVatCalculation=='true'}">
				    var estExpVatAmt=0;
					if(document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value!=''){
					var payVatPercent=eval(document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value);
					var estimateExpense= eval(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value);
					<c:if test="${contractType}"> 
					estimateExpense= eval(document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value);
					</c:if>
					estExpVatAmt=(estimateExpense*payVatPercent)/100;
					estExpVatAmt=Math.round(estExpVatAmt*10000)/10000; 
					document.forms['accountLineForms'].elements['accountLine.estExpVatAmt'].value=estExpVatAmt;
					}
					</c:if>
					}	
			function calculatePayVatAmt(){
		   <c:if test="${systemDefaultVatCalculation=='true'}">
		    var payVatAmt=0;
			if(document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value!=''){
			var payVatPercent=eval(document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value);
			var actualExpense= eval(document.forms['accountLineForms'].elements['accountLine.actualExpense'].value);
			<c:if test="${contractType}"> 
			actualExpense= eval(document.forms['accountLineForms'].elements['accountLine.localAmount'].value);
			</c:if>
			var firstPersent="";
			var secondPersent=""; 
			var relo="" 
			     <c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
			        if(relo==""){ 
			        if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=='${entry.key}') {
				        var arr='${entry.value}';
				        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
				        	firstPersent=arr.split(':')[0];
				        	secondPersent=arr.split(':')[1];
				        }
			        relo="yes"; 
			       }  }
			    </c:forEach> 
			    if(firstPersent!='' && secondPersent!=''){
				    var p1=parseFloat(firstPersent);
				    var p2=parseFloat(secondPersent);
				    payVatAmt=(actualExpense*p1)/100;
				    payVatAmt=Math.round(payVatAmt*10000)/10000; 
					document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=payVatAmt;
					payVatAmt=(actualExpense*p2)/100;
					payVatAmt=Math.round(payVatAmt*10000)/10000; 
					document.forms['accountLineForms'].elements['accountLine.qstPayVatAmt'].value=payVatAmt;
					payQstHideBlock();
			    }else{
					payVatAmt=(actualExpense*payVatPercent)/100;
					payVatAmt=Math.round(payVatAmt*10000)/10000; 
					document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=payVatAmt;
					document.forms['accountLineForms'].elements['accountLine.qstPayVatAmt'].value=0;
					payQstHideBlock();
			    } }
			</c:if>
			} 
			function recQstHideBlock(){
				var firstPersent="";
				var secondPersent=""; 
				var relo="" 
				     <c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
				        if(relo==""){ 
				        if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=='${entry.key}') {
					        var arr='${entry.value}';
					        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
					        	firstPersent=arr.split(':')[0];
					        	secondPersent=arr.split(':')[1];
					        }
				        relo="yes"; 
				       }  }
				    </c:forEach> 
				    if(firstPersent!='' && secondPersent!=''){
				        document.getElementById("qstRecVatGlLabel").style.display="block"; 
				        document.getElementById("qstRecVatGlId").style.display="block";   
				        document.getElementById("qstRecVatAmtGlLabel").style.display="block"; 
				        document.getElementById("qstRecVatAmtGlId").style.display="block";  
				    } else{
				        document.getElementById("qstRecVatGlLabel").style.display="none"; 
				        document.getElementById("qstRecVatGlId").style.display="none";   
				        document.getElementById("qstRecVatAmtGlLabel").style.display="none"; 
				        document.getElementById("qstRecVatAmtGlId").style.display="none";  			    
				    }	 	}
			function payQstHideBlock(){
				var firstPersent="";
				var secondPersent=""; 
				var relo="" 
				     <c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
				        if(relo==""){ 
				        if(document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value=='${entry.key}') {
					        var arr='${entry.value}';
					        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
					        	firstPersent=arr.split(':')[0];
					        	secondPersent=arr.split(':')[1];
					        }
				        relo="yes"; 
				       }  }
				    </c:forEach> 
				    if(firstPersent!='' && secondPersent!=''){
				        document.getElementById("qstPayVatGlLabel").style.display="block"; 
				        document.getElementById("qstPayVatGlId").style.display="block";   
				        document.getElementById("qstPayVatAmtGlLabel").style.display="block"; 
				        document.getElementById("qstPayVatAmtGlId").style.display="block";   
				    }else{
				        document.getElementById("qstPayVatGlLabel").style.display="none"; 
				        document.getElementById("qstPayVatGlId").style.display="none";   
				        document.getElementById("qstPayVatAmtGlLabel").style.display="none"; 
				        document.getElementById("qstPayVatAmtGlId").style.display="none";   			    
				    } } 
			function getVatPercent(){ 
			<c:if test="${systemDefaultVatCalculation=='true'}">
			var recAccDate='';
			 <c:if test="${accountInterface=='Y'}">
			  recAccDate=document.forms['accountLineForms'].elements['accountLine.recAccDate'].value;
		     </c:if>
		     <c:if test="${!utsiRecAccDateFlag && !utsiPayAccDateFlag}">
		       if (!${accNonEditFlag} && recAccDate!='')  { 
		        alert("You can not change the VAT as Sent To Acc has been already filled");
		        document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value='${accountLine.recVatDescr}';
		        } else if (${accNonEditFlag} && ${!adminInvoiceAmountEdit} && document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value!=''){
		        	
		        	alert("You can not change the VAT as as invoice has been generated. "); 
		            document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value='${accountLine.recVatDescr}';
		          
		        }else{
			 var relo="" 
		     <c:forEach var="entry" items="${euVatPercentList}">
		        if(relo==""){ 
		        if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=='${entry.key}') {
		        document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value='${entry.value}'; 
		        calculateVatAmt();
		        calculateVatAmtRevision();
		        calculateVatAmtEst();        
		        relo="yes"; 
		       }  }
		    </c:forEach> 
		    <configByCorp:fieldVisibility componentId="accountLine.recVatGL"> 
		    var relo1=""  
		        <c:forEach var="entry1" items="${euvatRecGLMap}">
		           if(relo1==""){ 
		           if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=='${entry1.key}') {
		           document.forms['accountLineForms'].elements['accountLine.recVatGl'].value='${entry1.value}';  
		           relo1="yes"; 
		          }  }
		       </c:forEach>
		       relo1='';
			 	<c:forEach var="entry1" items="${qstEuvatRecGLMap}" > 
			 	if(relo1 ==''){
			 	  if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=='${entry1.key}'){
			 	      document.forms['accountLineForms'].elements['accountLine.qstRecVatGl'].value='${entry1.value}'; 
			 	      relo1 ="yes";
			 	      }   }
			 	     </c:forEach>        
		      </configByCorp:fieldVisibility> 
		      if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value==''){
		      document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value=0;
		      calculateVatAmt();
		      calculateVatAmtRevision();  
		      calculateVatAmtEst();
		          
		      }  }
		     </c:if>
		     <c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}"> 
		     alert("You can not change the VAT as sent to Accounting and/or already Invoiced in UTSI Instance");
		     document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value='${accountLine.recVatDescr}'; 
		     </c:if>
		      </c:if> 
			}
			function calculateVatAmt(){
			<c:if test="${systemDefaultVatCalculation=='true'}"> 
			var recVatAmt=0;
			if(document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value!=''){
			var recVatPercent=eval(document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value);
			var actualRevenueForeign= eval(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value);
			<c:if test="${contractType}"> 
			actualRevenueForeign= eval(document.forms['accountLineForms'].elements['accountLine.actualRevenueForeign'].value);
			</c:if>
				var firstPersent="";
				var secondPersent=""; 
				var relo="" 
				     <c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
				        if(relo==""){ 
				        if(document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=='${entry.key}') {
					        var arr='${entry.value}';
					        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
					        	firstPersent=arr.split(':')[0];
					        	secondPersent=arr.split(':')[1];
					        }
				        relo="yes"; 
				       }  }
				    </c:forEach> 
				    if(firstPersent!='' && secondPersent!=''){
					    var p1=parseFloat(firstPersent);
					    var p2=parseFloat(secondPersent);
						recVatAmt=(actualRevenueForeign*p1)/100;
						recVatAmt=Math.round(recVatAmt*10000)/10000; 
						document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=recVatAmt;
						recVatAmt=(actualRevenueForeign*p2)/100;
						recVatAmt=Math.round(recVatAmt*10000)/10000;
						document.forms['accountLineForms'].elements['accountLine.qstRecVatAmt'].value=recVatAmt;
						recQstHideBlock();
				    }else{
						recVatAmt=(actualRevenueForeign*recVatPercent)/100;
						recVatAmt=Math.round(recVatAmt*10000)/10000;
						document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=recVatAmt;
						document.forms['accountLineForms'].elements['accountLine.qstRecVatAmt'].value=0;
						recQstHideBlock();
				    }
			}
		    </c:if>
			} 
		 function checkFloatPayVat(temp){
			var check='';  
		    var i; 
			var s = temp.value;
			var fieldName = temp.name;  
			if(temp.value>100){
			alert("You cannot enter more than 100% VAT ")
			document.forms['accountLineForms'].elements[fieldName].select();
		    document.forms['accountLineForms'].elements[fieldName].value='0.00';
		    document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=''; 

		    document.forms['accountLineForms'].elements['accountLine.estExpVatAmt'].value=''; 
		    document.forms['accountLineForms'].elements['accountLine.revisionExpVatAmt'].value=''; 
			return false;
			} 
			var count = 0;
			var countArth = 0;
		    for (i = 0; i < s.length; i++) {   
		        var c = s.charAt(i); 
		        if(c == '.') {
		        	count = count+1
		        }
		        if(c == '-')  {
		        	countArth = countArth+1
		        }
		        if( ((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
		       	  alert("Invalid data in vat%." ); 
		          document.forms['accountLineForms'].elements[fieldName].select();
		          document.forms['accountLineForms'].elements[fieldName].value='';
		          document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=''; 
		          document.forms['accountLineForms'].elements['accountLine.estExpVatAmt'].value=''; 
		          document.forms['accountLineForms'].elements['accountLine.revisionExpVatAmt'].value='';           
		       	  return false;
		       	} 
		        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))  {
		        	check='Invalid'; 
		        }  } 
		    if(check=='Invalid'){ 
		    alert("Invalid data in vat%." ); 
		    document.forms['accountLineForms'].elements[fieldName].select();
		    document.forms['accountLineForms'].elements[fieldName].value='';
		    document.forms['accountLineForms'].elements['accountLine.payVatAmt'].value=''; 
		    document.forms['accountLineForms'].elements['accountLine.estExpVatAmt'].value=''; 
		    document.forms['accountLineForms'].elements['accountLine.revisionExpVatAmt'].value='';     
		    return false;
		    }  else{
		    s=Math.round(s*10000)/10000;
		    var value=""+s;
		    if(value.indexOf(".") == -1){
		    value=value+".00";
		    } 
		    if((value.indexOf(".")+3 != value.length)){
		    value=value+"0";
		    }
		    document.forms['accountLineForms'].elements[fieldName].value=value;
		    calculatePayVatAmt();
		    calculateESTVatAmt();
		    calculateREVVatAmt();	          
		    return true;
		    } }
			function checkFloat(temp)  { 
		    var check='';  
		    var i; 
			var s = temp.value;
			var fieldName = temp.name;  
			if(temp.value>100){
			alert("You cannot enter more than 100% VAT ")
			document.forms['accountLineForms'].elements[fieldName].select();
		    document.forms['accountLineForms'].elements[fieldName].value='0.00';
		    document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=''; 
		    document.forms['accountLineForms'].elements['accountLine.estVatAmt'].value=''; 
		    document.forms['accountLineForms'].elements['accountLine.revisionVatAmt'].value='';     
			return false;
			}  
			var count = 0;
			var countArth = 0;
		    for (i = 0; i < s.length; i++) {   
		        var c = s.charAt(i); 
		        if(c == '.')  {
		        	count = count+1
		        }
		        if(c == '-')    {
		        	countArth = countArth+1
		        }
		        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
		       	  alert("Invalid data in vat%." ); 
		          document.forms['accountLineForms'].elements[fieldName].select();
		          document.forms['accountLineForms'].elements[fieldName].value='';
		          document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=''; 
		          document.forms['accountLineForms'].elements['accountLine.estVatAmt'].value=''; 
		          document.forms['accountLineForms'].elements['accountLine.revisionVatAmt'].value='';           
		       	  return false;
		       	} 
		        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
		        	check='Invalid'; 
		        }  } 
		    if(check=='Invalid'){ 
		    alert("Invalid data in vat%." ); 
		    document.forms['accountLineForms'].elements[fieldName].select();
		    document.forms['accountLineForms'].elements[fieldName].value='';
		    document.forms['accountLineForms'].elements['accountLine.recVatAmt'].value=''; 
		    document.forms['accountLineForms'].elements['accountLine.estVatAmt'].value=''; 
		    document.forms['accountLineForms'].elements['accountLine.revisionVatAmt'].value='';     
		    return false;
		    }  else{
		    s=Math.round(s*10000)/10000;
		    var value=""+s;
		    if(value.indexOf(".") == -1){
		    value=value+".00";
		    } 
		    if((value.indexOf(".")+3 != value.length)){
		    value=value+"0";
		    }
		    document.forms['accountLineForms'].elements[fieldName].value=value;
		    calculateVatAmt();
		    calculateVatAmtEst();
		    calculateVatAmtRevision();        
		    return true;
		    }  } 
		    function VATCalculate(){
		    var originCountryCode=document.forms['accountLineForms'].elements['serviceOrder.originCountryCode'].value
		    var destinationCountryCode=document.forms['accountLineForms'].elements['serviceOrder.destinationCountryCode'].value
		    var url="findCountryCodeEU.html?ajax=1&decorator=simple&popup=true&originCountryCode=" + encodeURI(originCountryCode)+ "&destinationCountryCode=" + encodeURI(destinationCountryCode);
		    http6.open("GET", url, true);
		    http6.onreadystatechange = CountrycodeEUResponse;
		    http6.send(null);
		    }
		    function checkVatForReadonlyFields(var1){
		    <c:if test="${systemDefaultVatCalculation=='true'}"> 
		       var var2= document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value;
		       var var3= document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value;  
		       if(var1!='second'){
		       if(var1=='first' && var2==''){   
		           document.forms['accountLineForms'].elements['accountLine.recVatPercent'].readOnly=true;
		           document.forms['accountLineForms'].elements['accountLine.recVatPercent'].className='input-textUpper';
		           }else{
		           <c:if test="${((!accNonEditFlag && empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber==''  || empty accountLine.recInvoiceNumber))) && !utsiRecAccDateFlag && !utsiPayAccDateFlag}">
		           document.forms['accountLineForms'].elements['accountLine.recVatPercent'].readOnly=false;
		           document.forms['accountLineForms'].elements['accountLine.recVatPercent'].className='input-text';
		            </c:if>
		           }}
		       if(var1!='first'){   
		       if(var1=='second' && var3 ==''){ 
		           document.forms['accountLineForms'].elements['accountLine.payVatPercent'].readOnly=true;
		           document.forms['accountLineForms'].elements['accountLine.payVatPercent'].className='input-textUpper';  
		        }else{
		        <c:if test="${empty accountLine.payAccDate}">
		         document.forms['accountLineForms'].elements['accountLine.payVatPercent'].readOnly=false;
		         document.forms['accountLineForms'].elements['accountLine.payVatPercent'].className='input-text';
		        </c:if>
		        } }
		     </c:if>  
		    }
		    function CountrycodeEUResponse(){
		    if (http6.readyState == 4) {
		    var results = http6.responseText
		    results = results.trim();  
		    if(results=='Ok') {
		    findBilltoCodeVatNumber();
		    }else{
		    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=1;
		    getVatPercent();
		    }     }  }
		     function findBilltoCodeVatNumber(){ 
			   var billToCode=document.forms['accountLineForms'].elements['accountLine.billToCode'].value
			   var url="findBilltoCodeVatNumber.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billToCode);
			   http7.open("GET", url, true);
			   http7.onreadystatechange = billtoCodeVatNumberResponse;
			   http7.send(null); 
		     }
		    function billtoCodeVatNumberResponse(){
		    if (http7.readyState == 4) {
		    var results = http7.responseText
		    results = results.trim();  
		    if(results=='notOk') {
		    findbillingCountryCode();
		    }else{
		    results=results.substring(0,2) 
		    if(results.indexOf("NL")  >= 0){
		    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=4;
		    getVatPercent();
		    }
		    if(results.indexOf("CZ")  >= 0){
		    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=5;
		    getVatPercent();
		    }   }   }   }
		    function findbillingCountryCode(){ 
			   var billToCode=document.forms['accountLineForms'].elements['accountLine.billToCode'].value
			   var url="findbillingCountryCode.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billToCode);
			   http6.open("GET", url, true);
			   http6.onreadystatechange = billingCountryCodeResponse;
			   http6.send(null); 
		     }
		     function billingCountryCodeResponse(){
		     if (http6.readyState == 4) {
		    var results = http6.responseText
		    results = results.trim(); 
		    if(results=='notOk') {
		    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=2;
		    getVatPercent();
		    }else{
		    results=results.substring(0,2) 
		    if(results.indexOf("NL")  >= 0){
		    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=4;
		    getVatPercent();
		    } else if(results.indexOf("CZ")  >= 0){
		    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=5;
		    getVatPercent();
		    }  else{
		    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value=3;
		    getVatPercent();
		    }    }   }   } 
		var httpDistribute=getHTTPObject();
		  function distributeOrdersAmount(targetButton){
		  var agree=confirm("This expense will be distributed to all service orders for this groupage order.  Would you like to continue?" );
		  if(agree){ 
			  document.forms['accountLineForms'].action ='distributeOrdersAmount.html';
				document.forms['accountLineForms'].submit();
		  }  } 
		 function changeBuyDeviation() { 
		   var estimateExpense=0;
		   var finalEstimateExpense=0;
		   var estimateDeviation=0;
		   var oldEstimateDeviation=0;
		   var finalEstimateRevenueAmount=0;
		   var  estimatepasspercentage=0;
		   var  estimatepasspercentageRound=0;
		   oldEstimateDeviation=document.forms['accountLineForms'].elements['oldEstimateDeviation'].value;
		   if(oldEstimateDeviation=='' || oldEstimateDeviation==0){
		   oldEstimateDeviation=100;
		   }
		   estimateExpense=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
		   estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value; 
		   finalEstimateExpense=estimateExpense*estimateDeviation/oldEstimateDeviation;
		   finalEstimateExpense=Math.round((finalEstimateExpense*10000)/10000); 
		   document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=finalEstimateExpense;
		   finalEstimateRevenueAmount=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value
		   estimatepasspercentage = (finalEstimateRevenueAmount/finalEstimateExpense)*100;
		   estimatepasspercentageRound=Math.round(estimatepasspercentage);
		   if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
		  	   document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
		   }else{
		   document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
		   } 
		   document.forms['accountLineForms'].elements['oldEstimateDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
		   calEstimatePayableContractRateAmmount('none');
		   calculateEstimateLocalAmount('cal','none');
		   }
		function  changeSellDeviation(){
		  var estimateExpense=0;
		  var finalEstimateExpense=0;
		  var estimateDeviation=0;
		  var estimateSellDeviation=0;
		  var oldEstimateSellDeviation=0;
		  var estimateRevenueAmount=0; 
		  var finalEstimateRevenueAmount=0;
		  var actualEstimateRevenueAmount=0;
		  var  estimatepasspercentage=0;
		   var  estimatepasspercentageRound=0;
		  var buyDependSellAccount=document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value;
		  estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
		  estimateRevenueAmount=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;
		  oldEstimateSellDeviation=document.forms['accountLineForms'].elements['oldEstimateSellDeviation'].value;
		  if(oldEstimateSellDeviation=='' || oldEstimateSellDeviation==0){
		   oldEstimateSellDeviation=100;
		   }
		  estimateExpense=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;
		  estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value; 
		  if(estimateDeviation=='' || estimateDeviation==0){
		   estimateDeviation=100;
		   } 
		  actualEstimateRevenueAmount=Math.round(estimateRevenueAmount*100/oldEstimateSellDeviation*10000)/10000; 
		  finalEstimateRevenueAmount=Math.round(estimateRevenueAmount*estimateSellDeviation/oldEstimateSellDeviation*10000)/10000; 
		  document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=finalEstimateRevenueAmount; 
		   if(buyDependSellAccount=="Y"){
		   finalEstimateExpense=Math.round((finalEstimateRevenueAmount*(estimateDeviation/100))*10000)/10000; 
		    }else{
		   finalEstimateExpense=Math.round((actualEstimateRevenueAmount*(estimateDeviation/100))*10000)/10000; 
		  } 
		  document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=finalEstimateExpense;
		   estimatepasspercentage = (finalEstimateRevenueAmount/finalEstimateExpense)*100;
		   estimatepasspercentageRound=Math.round(estimatepasspercentage);
		   if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
		  	   document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
		   }else{
		   document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
		   } 
		  document.forms['accountLineForms'].elements['oldEstimateSellDeviation'].value=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
		 calculateEstimateRate('none');
		 calculateVatAmtEst();
		 calEstimateContractRateAmmount('none');
		 calculateEstimateSellLocalAmount('cal','none','','','');
		}
		function changeRevisionBuyDeviation() { 
		   var revisionExpense=0;
		   var finalRevisionExpense=0;
		   var revisionDeviation=0;
		   var oldRevisionDeviation=0;
		   var finalRevisionRevenueAmount=0;
		   var revisionpasspercentage=0;
		   var revisionpasspercentageRound=0;
		   oldRevisionDeviation=document.forms['accountLineForms'].elements['oldRevisionDeviation'].value;
		   if(oldRevisionDeviation=='' || oldRevisionDeviation==0){
		   oldRevisionDeviation=100;
		   }
		   revisionExpense=document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;
		   revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value; 
		   finalRevisionExpense=revisionExpense*revisionDeviation/oldRevisionDeviation;
		   finalRevisionExpense=Math.round((finalRevisionExpense*10000)/10000); 
		   document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=finalRevisionExpense;
		   finalRevisionRevenueAmount=  document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;
		   revisionpasspercentage = (finalRevisionRevenueAmount/finalRevisionExpense)*100;
		   revisionpasspercentageRound=Math.round(revisionpasspercentage);
		   if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
		   document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
		   }else{
		  	 document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionpasspercentageRound;
		   }
		   document.forms['accountLineForms'].elements['oldRevisionDeviation'].value=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		   calRevisionPayableContractRateAmmount('none');
		   calculateRevisionLocalAmount('cal','none');
		   }
		function  changeRevisionSellDeviation(){
		  var revisionExpense=0;
		  var finalRevisionExpense=0;
		  var revisionDeviation=0;
		  var revisionSellDeviation=0;
		  var oldRevisionSellDeviation=0;
		  var revisionRevenueAmount=0; 
		  var finalRevisionRevenueAmount=0;
		  var actualRevisionRevenueAmount=0;
		  var revisionpasspercentage=0;
		  var revisionpasspercentageRound=0;
		  var buyDependSellAccount=document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value;
		  revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		  revisionRevenueAmount=document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;
		  oldRevisionSellDeviation=document.forms['accountLineForms'].elements['oldRevisionSellDeviation'].value;
		  if(oldRevisionSellDeviation=='' || oldRevisionSellDeviation==0){
		   oldRevisionSellDeviation=100;
		   }
		  revisionExpense=document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;
		  revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		  if(revisionDeviation=='' || revisionDeviation==0){
		   revisionDeviation=100;
		   }  
		  actualRevisionRevenueAmount=Math.round(revisionRevenueAmount*100/oldRevisionSellDeviation*10000)/10000; 
		  finalRevisionRevenueAmount=Math.round(revisionRevenueAmount*revisionSellDeviation/oldRevisionSellDeviation*10000)/10000; 
		  document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=finalRevisionRevenueAmount; 
		   if(buyDependSellAccount=="Y"){
		   finalRevisionExpense=Math.round((finalRevisionRevenueAmount*(revisionDeviation/100))*10000)/10000; 
		    }else{
		   finalRevisionExpense=Math.round((actualRevisionRevenueAmount*(revisionDeviation/100))*10000)/10000; 
		  }  
		  document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=finalRevisionExpense;
		  revisionpasspercentage = (finalRevisionRevenueAmount/finalRevisionExpense)*100;
		   revisionpasspercentageRound=Math.round(revisionpasspercentage);
		   if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
		   document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
		   }else{
		  	 document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionpasspercentageRound;
		   }
		  document.forms['accountLineForms'].elements['oldRevisionSellDeviation'].value=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		 calculateRevisionRate('none');
		 calculateVatAmtRevision();
		 calRevisionContractRateAmmount('none');
		 calculateRevisionSellLocalAmount('cal','none','','','');
		}
		function checkChargeCodeOpenDiv(gltype){  
			var val = document.forms['accountLineForms'].elements['accountLine.category'].value;
			var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
			var billingContract = '${billing.contract}';
			var jobtype = document.forms['accountLineForms'].elements['serviceOrder.Job'].value; 
		    var routing = document.forms['accountLineForms'].elements['serviceOrder.routing'].value; 
		    var sid=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
		    var soCompanyDivision=document.forms['accountLineForms'].elements['serviceOrder.companyDivision'].value;
			chargeCode=chargeCode.trim();
			if(chargeCode!='') { 
			if(val=='Internal Cost'){
			var accountCompanyDivision =  document.forms['accountLineForms'].elements['accountLine.companyDivision'].value; 
			 var url="checkInternalCostChargeCode.html?ajax=1&decorator=simple&popup=true&accountCompanyDivision="+encodeURIComponent(accountCompanyDivision)+"&chargeCode="+encodeURIComponent(chargeCode)+"&jobType="+encodeURIComponent(jobtype)+"&routing="+encodeURIComponent(routing)+"&soId="+sid+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
			 http5.open("GET", url, true);
		     http5.onreadystatechange = function(){ handleHttpResponseChargeCodeOpenDiv("Internal",gltype);};
		     http5.send(null);
			}else{
			if(billingContract=='' || billingContract==' '){ 
				alert("There is no pricing contract in billing: Please select.");
				document.forms['accountLineForms'].elements['accountLine.chargeCode'].value = "";
			}else{
			 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(billingContract)+"&chargeCode="+encodeURIComponent(chargeCode)+"&jobType="+encodeURIComponent(jobtype)+"&routing="+encodeURIComponent(routing)+"&soId="+sid+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
		     http5.open("GET", url, true);
		     http5.onreadystatechange =function(){ handleHttpResponseChargeCodeOpenDiv("NoInternal",gltype);};
		     http5.send(null);
			} } }  }
		function handleHttpResponseChargeCodeOpenDiv(temp,gltype){
		             if (http5.readyState == 4){
		                var results = http5.responseText
		                results = results.trim(); 
		                results = results.replace('[','');
		                results=results.replace(']',''); 
		                var res = results.split("#"); 
		                var description1 = "";
		                var note = "";  
		                var quoteDescription="";
		                var wordingTemp="";
		                if(results.length>6){
		                   <c:if test="${accountInterface=='Y'}">
		                   <c:if test="${costElementFlag}">
		                   wordingTemp=res[14];
		                   </c:if>
		                   <c:if test="${!costElementFlag}">
		                   wordingTemp=res[11];
		                   </c:if>
		                   <c:if test="${costElementFlag}">
		                   		document.forms['accountLineForms'].elements['chargeCostElement'].value=res[11]; 
		                    </c:if>
		                    if(res[1]!=undefined || res[1]!="undefined"){
		                    	document.forms['accountLineForms'].elements['accountLine.recGl'].value = res[1];	
		               		}else{
		               			document.forms['accountLineForms'].elements['accountLine.recGl'].value = "";
		               		}
		                    
		                    if(res[2]!=undefined || res[2]!="undefined"){
		                    	document.forms['accountLineForms'].elements['accountLine.payGl'].value = res[2];	
		               		}else{
		               			document.forms['accountLineForms'].elements['accountLine.payGl'].value = "";
		               		}
							
			                
			           <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
			                if(res[5]=='Y'){
			                document.forms['accountLineForms'].elements['accountLine.includeLHF'].checked=true;
			                document.forms['accountLineForms'].elements['fifthDescription'].value=true;
			                }else{
			                 document.forms['accountLineForms'].elements['accountLine.includeLHF'].checked=false;
			                 document.forms['accountLineForms'].elements['fifthDescription'].value=false;
			                }
			          </configByCorp:fieldVisibility>
			                if(res[6]=='Y'){
			                document.forms['accountLineForms'].elements['accountLine.ignoreForBilling'].checked=true;
			                document.forms['accountLineForms'].elements['sixthDescription'].value=true;
			                }else{
			                 document.forms['accountLineForms'].elements['accountLine.ignoreForBilling'].checked=false;
			                 document.forms['accountLineForms'].elements['sixthDescription'].value=false;
			                } 
			                </c:if>
			                description1 = document.forms['accountLineForms'].elements['accountLine.description'].value;
			                note = document.forms['accountLineForms'].elements['accountLine.note'].value;
			                quoteDescription = document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value; 
			                document.forms['accountLineForms'].elements['accountLine.buyDependSell'].value=res[7];
			                note=note.trim();
			                description1=description1.trim();
			                quoteDescription=quoteDescription.trim();
			                if(description1==''){
				                if(wordingTemp==''){
				                	if(res[4]!=undefined || res[4]!="undefined"){
				                		document.forms['accountLineForms'].elements['accountLine.description'].value=res[4];	
		                			}else{
		                				document.forms['accountLineForms'].elements['accountLine.description'].value="";
		                			}
				                }else{
				                	document.forms['accountLineForms'].elements['accountLine.description'].value=wordingTemp;
				                }  }
			                if(note==''){
			                if(wordingTemp==''){
			                	if(res[4]!=undefined || res[4]!="undefined"){
			                		document.forms['accountLineForms'].elements['accountLine.note'].value=res[4];	
	                			}else{
	                				document.forms['accountLineForms'].elements['accountLine.note'].value="";
	                			}
			                }else{
			                	document.forms['accountLineForms'].elements['accountLine.note'].value=wordingTemp;
			                }  }
			                if(quoteDescription==''){
				                if(wordingTemp==''){
				                	if(res[4]!=undefined || res[4]!="undefined"){
				                		document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=res[4];	
		                			}else{
		                				document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value="";
		                			}
				                }else{
				                	document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value=wordingTemp;
				                }   }
		                	<c:if test="${systemDefaultVatCalculation=='true'}">
		                	<c:if test="${(((accountLine.createdBy != 'Networking' && ( !(((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0 ) &&  not empty accountLine.networkSynchedId ) && !(trackingStatus.accNetworkGroup)) )) && (accountLine.chargeCode != 'MGMTFEE')) || !(trackingStatus.soNetworkGroup) )}">
		                	if(res[8]=='Y'){
		                	document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=true;
		    				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=true;
		    				document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=true; 
		    				document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=true;
		    				document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=true;
		    				document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=true;
		    				document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=true;
				    		var vatRec=0.00;
				    		vatRec=document.forms['accountLineForms'].elements['accountLine.recVatPercent'].value;
				    		vatRec=parseFloat(vatRec);
				    		var vatPay=0.00;
				    		vatPay=document.forms['accountLineForms'].elements['accountLine.payVatPercent'].value;
				    		vatPay=parseFloat(vatPay);
				    		if((vatRec>0)||(vatPay>0))
				    		{ }
		                	}else{
		                	    document.forms['accountLineForms'].elements['accountLine.VATExclude'].value=false; 
		                		document.forms['accountLineForms'].elements['accountLine.payVatDescr'].disabled=false; 
			    				if(temp=="NoInternal"){
			    				document.forms['accountLineForms'].elements['accountLine.recVatPercent'].disabled=false;
			    				document.forms['accountLineForms'].elements['accountLine.recVatAmt'].disabled=false;
			    				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false; 
			    				}
			    				document.forms['accountLineForms'].elements['accountLine.payVatPercent'].disabled=false;
			    				document.forms['accountLineForms'].elements['accountLine.payVatAmt'].disabled=false;
		                	}
		                	</c:if> 
		                	</c:if>
		              }   
		                if(gltype=='rec'){ 
		                	 var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value; 
		                	 var glCode= document.forms['accountLineForms'].elements['accountLine.recGl'].value;
		                	 var billToCode= document.forms['accountLineForms'].elements['accountLine.billToCode'].value; 
		                	 chargeCode =chargeCode.trim();
		                	 glCode =glCode.trim();
		                	 billToCode =billToCode.trim(); 
		                     if(glCode==''){
		                	 alert("Receivable GL is missing") 
		                    } else if(chargeCode!='' && glCode!='' )   { 
		                        if(billToCode=='')  {   
		                            var url="getPrimaryBillToCode.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
		                		    http2.open("GET", url, true);
		                		    http2.onreadystatechange = function(){ handleHttpResponseBilltocode('rec');};
		                		    http2.send(null);  
		                       } 
		                       else if(billToCode!='')
		                       { 
		                         animatedcollapse.toggle('rec');
		                       }  }}
		               if(gltype=='pay'){
		            	    var companyDivision = document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;   
		                	var vendorCode= document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
		                	var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value; 
		                	var actgCode= document.forms['accountLineForms'].elements['accountLine.actgCode'].value;
		                	var payGl= document.forms['accountLineForms'].elements['accountLine.payGl'].value; 
		                	vendorCode =vendorCode.trim();
		                	chargeCode =chargeCode.trim();
		                	actgCode =actgCode.trim(); 
		                	payGl=payGl.trim(); 
		                   if(payGl=='') {
		                	alert("Payable GL Code is missing") 
		                	 } else if(vendorCode!='' && chargeCode!='' && payGl!='' && actgCode=='') { 
		             		    var url=""
		             		    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}"> 
		             		    try{
		             		    	actgCodeFlag=document.forms['accountLineForms'].elements['actgCodeFlag'].value;
		             		    }catch(e){} 
		             		     url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorCode)+"&companyDivision="+encodeURI(companyDivision)+"&actgCodeFlag="+encodeURI(actgCodeFlag);
		             		    </c:if>
		             		    <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}"> 
		             		     url="vendorNameForUniqueActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorCode)+"&companyDivision="+encodeURI(companyDivision);
		             		    </c:if>
		             		    http2.open("GET", url, true);
		             		    http2.onreadystatechange = function(){ handleHttpResponse2222('pay');};
		             		    http2.send(null);  
		                  	} else if(vendorCode!='' && actgCode!='' && chargeCode!='' && payGl!='')
		                  	{
		                  	  fillDefaultInvoiceNumber();
		                  	  animatedcollapse.toggle('pay');
		                  	} } }}
		                  	
		  function findExchangeContractRate(field1,field2,field3){
		   		 if(field1!='' && field2!='' & field3!=''){
					 rateOnActualizationDate(field1,field2,field3);
				 }
		   		 <c:if test="${contractType}">
			        var contractCurrency= '${accountLine.contractCurrency}' 
			        var country =document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value;
			        document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value=findExchangeRateGlobal(country)
			        document.forms['accountLineForms'].elements['accountLine.contractValueDate'].value=currentDateGlobal(); 
			        var recRateCurrency = document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value;
		             if(recRateCurrency==country){
		             	document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value = document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value;
		             	document.forms['accountLineForms'].elements['accountLine.racValueDate'].value = document.forms['accountLineForms'].elements['accountLine.contractValueDate'].value;
		             } 
					  calculateRecRateByContractRate('accountLine.contractRate','','','');
			    </c:if>
		   }
		   
		   function trap()   { 
				  if(document.images) {
				      var totalImages = document.images.length;
				      	for (var i=0;i<totalImages;i++) {
								if(document.images[i].src.indexOf('calender.png')>0) { 
									<c:choose>
									<c:when test="${((!(trackingStatus.soNetworkGroup))  && billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' ))}">
									var el = document.getElementById(document.images[i].id);  
									if((el.getAttribute("id")=='invoiceDate_trigger') || (el.getAttribute("id")=='receivedDate_trigger')|| (el.getAttribute("id")=='payPostDate_trigger') ){ 
										
									}else{
										document.images[i].src = 'images/navarrow.gif'; 
										if((el.getAttribute("id")).indexOf('trigger')>0){ 
											el.removeAttribute('id');
										} }	
									</c:when>
									<c:when test="${(trackingStatus.soNetworkGroup  && billingDMMContractType && accountLine.chargeCode != 'DMMFEE' && accountLine.chargeCode != 'DMMFXFEE'  && (accountLine.createdBy == 'Networking' || ((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0) &&  not empty accountLine.networkSynchedId )))}"> 
									var el = document.getElementById(document.images[i].id); 
		                            if((el.getAttribute("id")=='auditCompleteDate-trigger')){ 
										
									   }else{
										document.images[i].src = 'images/navarrow.gif'; 
										if((el.getAttribute("id")).indexOf('trigger')>0){ 
											el.removeAttribute('id');
										} }	
									</c:when>
									<c:otherwise> 
									var el = document.getElementById(document.images[i].id);  
									document.images[i].src = 'images/navarrow.gif'; 
									if((el.getAttribute("id")).indexOf('trigger')>0){ 
										el.removeAttribute('id');
									}
									</c:otherwise>
									</c:choose>	
								}
								if(document.images[i].src.indexOf('open-popup.gif')>0) {  
										var el = document.getElementById(document.images[i].id); 
										try{
										el.onclick = false;
										}catch(e){}
									    document.images[i].src = 'images/navarrow.gif';
								}
								if(document.images[i].src.indexOf('notes_empty1.jpg')>0) {
										var el = document.getElementById(document.images[i].id);
										//alert(el)
										try{
										el.onclick = false;
										}catch(e){}
								        document.images[i].src = 'images/navarrow.gif';
								}
								if(document.images[i].src.indexOf('notes_open1.jpg')>0) {
									var el = document.getElementById(document.images[i].id);
									try{
									el.onclick =false;
									}catch(e){}
									document.images[i].src = 'images/navarrow.gif';
								} 				
								if(document.images[i].src.indexOf('images/nav')>0) {
									var el = document.getElementById(document.images[i].id);
									try{
									el.onclick = false;
									}catch(e){}
									document.images[i].src = 'images/navarrow.gif';
								}
								if(document.images[i].src.indexOf('images/image')>0) {
									var el = document.getElementById(document.images[i].id);
									try{
									el.onclick = false;
									}catch(e){}
									document.images[i].src = 'images/navarrow.gif';
								} } 
							var elementsLen=document.forms['accountLineForms'].elements.length;
				for(i=0;i<=elementsLen-1;i++) {
						if(document.forms['accountLineForms'].elements[i].type=='text') {
								document.forms['accountLineForms'].elements[i].readOnly =true;
								document.forms['accountLineForms'].elements[i].className = 'input-textUpper';
								document.forms['accountLineForms'].elements[i].onkeydown=false;						
							} else 	{
							 document.forms['accountLineForms'].elements[i].disabled=true;
							} 	} 
					if(document.forms['accountLineForms'].elements['Cancel']) {
						document.forms['accountLineForms'].elements['Cancel'].disabled=false;
					}  }
				    document.forms['accountLineForms'].setAttribute('bypassDirtyCheck',false);
				    <%--  <c:if test="${disableALL == 'YES'}">--%>
				     <c:if test="${(trackingStatus.soNetworkGroup  && billingDMMContractType && accountLine.chargeCode != 'DMMFEE' && accountLine.chargeCode != 'DMMFXFEE'  && (accountLine.createdBy == 'Networking' || ((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0) &&  not empty accountLine.networkSynchedId )))}"> 
				   if(document.forms['accountLineForms'].elements['accountLine.recAccDate'].value=='' ){
				    document.forms['accountLineForms'].elements['method:save'].disabled=false;
				    document.forms['accountLineForms'].elements['Reset'].disabled=false;
				    if(document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value==''){
				    document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].readOnly =false; 
				    document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].className ='input-text';
				    document.forms['accountLineForms'].elements['accountLine.recRateExchange'].readOnly =false; 
				    document.forms['accountLineForms'].elements['accountLine.recRateExchange'].className ='input-text'; 
				    }
				    document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false; 
				    var vatExcludeFlag = document.forms['accountLineForms'].elements['accountLine.VATExclude'].value;  
				    if(vatExcludeFlag != 'true') {
				    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false;
				    }
				    document.forms['accountLineForms'].elements['accountLine.description'].disabled =false;  
				    if(!(document.forms['accountLineForms'].elements['accountLine.recInvoiceNumber'].value!='' && ${accNonEditFlag})){
				    document.getElementById('accountLineBillToCodeOpenpopup').src='images/open-popup.gif'; 
				    document.getElementById('accountLineBillToCodeOpenpopup').onclick = function(){
				    	 openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.billToName&fld_code=accountLine.billToCode');
				    	 document.forms['accountLineForms'].elements['accountLine.recQuantity'].select();
					    }
				    }
		            document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].readOnly =false; 
				    document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].className ='input-text';
				    document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].readOnly =false; 
				    document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].className ='input-text';
				    document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].disabled=false; 
				    document.forms['accountLineForms'].elements['accountLine.quoteDescription'].disabled =false;  
		            document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].readOnly =false; 
				    document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].className ='input-text';
				    document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].readOnly =false; 
				    document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].className ='input-text';
				    document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].disabled=false;
				    document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=false; 
				   }  
			        </c:if> 
			        <c:if test="${((!(trackingStatus.soNetworkGroup))  && billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' ))}">
			        document.forms['accountLineForms'].elements['method:save'].disabled=false;
				    document.forms['accountLineForms'].elements['Reset'].disabled=false;
			        document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].readOnly =false; 
				    document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].className ='input-text';
				    document.forms['accountLineForms'].elements['accountLine.payingStatus'].disabled=false; 
			       </c:if> 
				  } 
		          function iehack(){ 
		        		if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){  
		        			var ieversion=new Number(RegExp.$1)  
		        				var elementList = document.getElementsByTagName('*');
		        				var imageList = document.getElementsByTagName('img');
		        				 for(var i = 0 ; i < imageList.length ; i++){
		        					 if(imageList[i].nodeName == 'IMG'){
		        						if(imageList[i].src.indexOf('calender.png') > 0){
		        							<c:choose>
		        							<c:when test="${((!(trackingStatus.soNetworkGroup))  && billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' ))}">
		        							var el = document.getElementById(imageList[i].id);  
		        							if((el.getAttribute("id")=='invoiceDate_trigger') || (el.getAttribute("id")=='receivedDate_trigger')|| (el.getAttribute("id")=='payPostDate_trigger') ){ 
		        								setOnSelectBasedMethods(['actSentToclient(),changeStatus(),calcDays()']);
		        								setCalendarFunctionality();
		        							}else{
		        								imageList[i].src = 'images/navarrow.gif'; 
		            							var elementID  = imageList[i].id;
		            							if(elementID.indexOf('trigger')>0){
		            								imageList[i].style.display = 'none';
		            							}
		        							}	
		        							</c:when>
		        							<c:when test="${(trackingStatus.soNetworkGroup  && billingDMMContractType && accountLine.chargeCode != 'DMMFEE' && accountLine.chargeCode != 'DMMFXFEE'  && (accountLine.createdBy == 'Networking' || ((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0) &&  not empty accountLine.networkSynchedId )))}"> 
		        							var el = document.getElementById(imageList[i].id); 
		                                    if((el.getAttribute("id")=='auditCompleteDate-trigger')){ 
		      								    setOnSelectBasedMethods(['actSentToclient(),changeStatus(),calcDays()']);
		        						        setCalendarFunctionality();
		      							   }else{
		        								imageList[i].src = 'images/navarrow.gif'; 
		            							var elementID  = imageList[i].id;
		            							if(elementID.indexOf('trigger')>0){
		            								imageList[i].style.display = 'none';
		            							}
		        							}
		      							</c:when>
		        							<c:otherwise> 
		        							imageList[i].src = 'images/navarrow.gif'; 
		        							var elementID  = imageList[i].id;
		        							if(elementID.indexOf('trigger')>0){
		        								imageList[i].style.display = 'none';
		        							}
		        							</c:otherwise>
		            						</c:choose>
		        						}
		        						if(imageList[i].src.indexOf('open-popup.gif')>0) { 
		     								<c:choose>
		        							<c:when test="${(trackingStatus.soNetworkGroup  && billingDMMContractType && accountLine.chargeCode != 'DMMFEE' && accountLine.chargeCode != 'DMMFXFEE'  && (accountLine.createdBy == 'Networking' || ((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0) &&  not empty accountLine.networkSynchedId)))}">
		        							//if(document.getElementsByName('accountLine.billToCode')[0].value.trim()==document.getElementsByName('bookingAgentCodeDMM')[0].value.trim()){
		        								if(imageList[i].id != 'accountLineBillToCodeOpenpopup') {
		            								imageList[i].src = 'images/navarrow.gif';
		                							imageList[i].style.display = 'none';
		        	        					}  
		        							</c:when>
		        							<c:otherwise>
		        							imageList[i].src = 'images/navarrow.gif';
		        							imageList[i].style.display = 'none';
		        							</c:otherwise>
		        							</c:choose> 
		        						}
		        						if(imageList[i].src.indexOf('notes_empty1.jpg')>0) {
		        							imageList[i].src = 'images/navarrow.gif';
		        							imageList[i].style.display = 'none';
		        						}
		        						if(imageList[i].src.indexOf('images/image')>0) {
		        							imageList[i].src = 'images/navarrow.gif';
		        							imageList[i].style.display = 'none';
		        						} 
		        						if(imageList[i].src.indexOf('notes_open1.jpg')>0) 	{
		        							imageList[i].src = 'images/navarrow.gif';
		        							imageList[i].style.display = 'none';
		        						}
		        						if(imageList[i].src.indexOf('images/nav')>0) 	{
		        							imageList[i].src = 'images/navarrow.gif';
		        							imageList[i].style.display = 'none';
		        						}  }  }
		        				 for(var i in elementList){
		        					 if(elementList[i].type == 'text') {
			         					 elementList[i].readonly =true;
			        					 elementList[i].onkeydown=function(){return false;};//=false;	
			        					 elementList[i].className = 'input-textUpper';
			         						 
		        						}
		        					 if(elementList[i].type == 'textarea') {
			         					 elementList[i].readonly =true;
			        					 elementList[i].onkeydown=function(){return false;};//=false;	
			        					 elementList[i].className = 'input-textUpper';
			         						 
		     						}
		        					 if(elementList[i].type == 'checkbox') {
		        						 elementList[i].disabled =true;
		        						}
		        					 if(elementList[i].type == 'select-one') {
		        							 elementList[i].disabled =true;
		        							 elementList[i].style.className = 'list-menu';
		        						} 
		        					 if(elementList[i] != null && (elementList[i].type=='button'|| elementList[i].type=='submit' || elementList[i].type=='reset'))
		        						{
		         							if(elementList[i].name != 'Cancel'){ 
			        							 elementList[i].disabled=true ; 
		         							} 	} }
		        				 document.forms['accountLineForms'].setAttribute('bypassDirtyCheck',false);
		        				    <c:if test="${(trackingStatus.soNetworkGroup  && billingDMMContractType && (accountLine.createdBy == 'Networking' || ((fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0) &&  not empty accountLine.networkSynchedId )))}">
		        				    if(document.forms['accountLineForms'].elements['accountLine.recAccDate'].value==''){ 
		        				    document.forms['accountLineForms'].elements['method:save'].disabled=false;
		        				    document.forms['accountLineForms'].elements['Reset'].disabled=false; 
		        				    document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].className ='input-text';
		        				    
		        				    document.forms['accountLineForms'].elements['accountLine.recRateExchange'].className ='input-text'; 
		        				    document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].disabled=false;
		        				    var vatExcludeFlag = document.forms['accountLineForms'].elements['accountLine.VATExclude'].value;
		        				    if(vatExcludeFlag != 'true') { 
		        				    document.forms['accountLineForms'].elements['accountLine.recVatDescr'].disabled=false;
		        				    }
		        				    //document.forms['accountLineForms'].elements['accountLine.description'].disabled =false;  
		        				    //document.getElementById('accountLineBillToCodeOpenpopup').style.display='block'; 
		        		            document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].className ='input-text';
		        				    document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].className ='input-text';
		        				    document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].disabled=false; 
		        				    //document.forms['accountLineForms'].elements['accountLine.quoteDescription'].disabled =false;  
		        		            document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].className ='input-text';
		        				    document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].className ='input-text';
		        				    document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].disabled=false; 
		        				    var editableElementList = ['accountLine.contractExchangeRate','accountLine.recRateExchange',
		        				                               'accountLine.estimateContractExchangeRate','accountLine.estSellExchangeRate',
		        				                               'accountLine.revisionContractExchangeRate','accountLine.revisionSellExchangeRate'];
		        				    var counter = 0 ;
		        				    var elementList1 = document.getElementsByTagName('*'); 
		    				    	for(var k in elementList1){
			           					 if(elementList1[k].type == 'text')
			           						{
			            						for(var j in editableElementList){
			       	         						if(elementList1[k].name == editableElementList[j]){ 
			       	         						elementList1[k].readonly =false;
			       	         					    elementList1[k].onkeydown=function(){return true;};//=false;	
			       	         				        elementList1[k].className = 'input-text';
			       	         						} } }
			           					if(elementList1[k].type == 'textarea')
		           						{    
		       	         						if(elementList1[k].name == 'accountLine.description'||elementList1[k].name == 'accountLine.quoteDescription'){ 
		       	         						elementList1[k].readonly =false;
		       	         					    elementList1[k].onkeydown=function(){return true;};//=false;	
		       	         				        elementList1[k].className = 'input-text';
		       	         						 
		            						} } }  } 
		        			        </c:if> 
		        			        <c:if test="${((!(trackingStatus.soNetworkGroup))  && billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' ))}">
		        			        document.forms['accountLineForms'].elements['method:save'].disabled=false;
		        				    document.forms['accountLineForms'].elements['Reset'].disabled=false;
		        			        document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].readonly = false; 
		        			        document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].onkeydown=function(){return true;};
		        				    document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].className ='input-text';
		        				    document.forms['accountLineForms'].elements['accountLine.payingStatus'].disabled=false; 
		        				    </c:if>
		        		  } }  
		          function editTrap()   {  	
					var elementsLen=document.forms['accountLineForms'].elements.length;
				     for(i=0;i<=elementsLen-1;i++)
					 {
						if(document.forms['accountLineForms'].elements[i].type=='text') {
								document.forms['accountLineForms'].elements[i].readOnly =false;
								document.forms['accountLineForms'].elements[i].className = 'input-text';						
							} else {
							 document.forms['accountLineForms'].elements[i].disabled=false;
							} }    } 
		          function findEstSellExchangeRate(){
		              var country =document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value; 
		              document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value=findExchangeRateGlobal(country)
		              document.forms['accountLineForms'].elements['accountLine.estSellValueDate'].value=currentDateGlobal();
                      var contractRate=0;
      				  <c:if test="${contractType}">
      				   var estContractCurrency = document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value;
      	                if(estContractCurrency==country){
      	                	document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value;
      	                	document.forms['accountLineForms'].elements['accountLine.estimateContractValueDate'].value = document.forms['accountLineForms'].elements['accountLine.estSellValueDate'].value ;
      	               } 
                      contractRate = document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
                      </c:if>
                    if(contractRate==0){  
      				  calculateEstimateRevenue('none','','','');
                    }else{
                    	calEstSellLocalRate('NoCal','accountLine.estimateContractRate'); 
                        }
			    	<c:if test="${systemDefaultVatCalculation=='true'}">
				    	calculateESTVatAmt();
				    	calculateVatAmtEst();    
			        </c:if>
                 
		        }
		   
		       function calculateEstimateRevenue(target,field1,field2,field3){
		    		 if(field1!='' && field2!='' & field3!=''){
		    			 rateOnActualizationDate(field1,field2,field3);
		    			 }           
		    	   var contractRate=0;
					  <c:if test="${contractType}">
		        contractRate = document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
		        </c:if>
		      //if(contractRate==0){
		    	   var estSellLocalAmount= document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value ;
		    	   var estSellExchangeRate= document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value*1 ;
		    	   var roundValue=0; 
		    	  	 if(estSellExchangeRate ==0) {
		    	         document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=0*1;
		    	     } else if(estSellExchangeRate >0) {
		    	    	 <c:if test="${contractType}"> 
			          	 if(target=='accountLine.estimateContractRate'){
			        		 var roundValue='0.00';
			        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
			        		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
			        		 var baseRateVal=1;
			        	 	 var ATemp=0.00; 
			        	     var ETemp =0.00;	    		 
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
			        	        ETemp= document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value*1 ;
			        	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
				         		   var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
				        		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
				        			   ATemp=(ATemp*estimateSellDeviation)/100;
				        			    }
			        	        roundValue=Math.round(ATemp*10000)/10000;
			             document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue; 
			        	 }else if(target=='accountLine.estSellLocalRate'){
			        		 var roundValue='0.00';
			        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
			        		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
			        		 var baseRateVal=1;
			        	 	 var ATemp=0.00; 
			        	     var ETemp =0.00;	    		 
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value;
			        	        ETemp= document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value*1 ;
			        	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
				         		   var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
				        		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
				        			   ATemp=(ATemp*estimateSellDeviation)/100;
				        			    }
			        	        roundValue=Math.round(ATemp*10000)/10000;
			             document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue; 
			        	 }else if(target=='accountLine.estimateRevenueAmount'){
			        	 }else if(target=='accountLine.estimateSellRate'){
			        		 var roundValue='0.00';
			        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
			        		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
			        		 var baseRateVal=1;
			        	 	 var ATemp=0.00; 
			        	     var ETemp =0.00;	    		 
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
			        	        ATemp=((ATemp*quantityVal)/baseRateVal);
			         		   var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
			        		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
			        			   ATemp=(ATemp*estimateSellDeviation)/100;
			        			    }	        	        
			        	        roundValue=Math.round(ATemp*10000)/10000;
			             		document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue;
			        		 
			        	 }else{
				      	       var amount=estSellLocalAmount/estSellExchangeRate; 
				    	       roundValue=Math.round(amount*10000)/10000;
				    	       document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue ; 
				    	     }		       
		              	 </c:if>
				       <c:if test="${!contractType}"> 
				      	       var amount=estSellLocalAmount/estSellExchangeRate; 
				    	       roundValue=Math.round(amount*10000)/10000;
				    	       document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue ; 
				        </c:if>
		    	     }
		    	     var estimatepasspercentageRound=0;
		    	     var estimatepasspercentage = 0;
		    	     var estimateExpense = document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value; 
		    	     estimatepasspercentage = (roundValue/estimateExpense)*100;
		    	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage);
		    	  	 if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
		    	  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
		    	  	   }else{
		    	  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
		    	  	   }   	   
		  	  	   if(target!='accountLine.estimateSellRate'){
		    	  	 calculateSellEstimateRate(target);
		  	  	   } 
		    	  	calculateEstimateSellLocalRate(target);
		    	  	<c:if test="${contractType}">
		   	        try{
		   	        calEstimateContractRate(target);
		   	        }catch(e){}
		   	        </c:if>  
		    	  	calculateVatAmtEst(); 
		           } 
		       function  calculateEstimateRevenueByExchangerate(){
		    	   var contractRate=0;
					  <c:if test="${contractType}">
		        contractRate = document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
		        </c:if>
		      if(contractRate==0){
		    	   var estSellLocalAmount= document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value ;
		    	   var estSellExchangeRate= document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value*1 ;
		    	   var roundValue=0; 
		    	  	 if(estSellExchangeRate ==0) {
		    	         document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=0*1;
		    	     } else if(estSellExchangeRate >0) {
		    	       var amount=estSellLocalAmount/estSellExchangeRate; 
		    	       roundValue=Math.round(amount*10000)/10000;
		    	       document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue ;
		    	     }
		    	     var estimatepasspercentageRound=0;
		    	     var estimatepasspercentage = 0;
		    	     var estimateExpense = document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value; 
		    	     estimatepasspercentage = (roundValue/estimateExpense)*100;
		    	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage);
		    	  	 if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
		    	  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
		    	  	   }else{
		    	  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
		    	  	   } 
		    	  	 calculateSellEstimateRate('none'); 
		    	  	calculateEstimateSellLocalRate('none');
		            }else{
		       	        calEstSellLocalRate('NoCal','accountLine.estimateContractRate'); 
		             }  }
		       function calculateEstimateSellLocalRate(target){
		    	   <c:if test="${contractType}">  
		   	 if(target=='accountLine.estSellLocalAmount'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value;
			        ATemp=(ATemp*baseRateVal)/quantityVal;
				    if(estimateDeviationVal!='' && estimateDeviationVal>0){
				    	ATemp=ATemp*100/estimateDeviationVal;
				    	ATemp=Math.round(ATemp*10000)/10000;
				    }
		     document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=ATemp; 
			 }else if(target=='accountLine.estimateSellRate'){
				 var roundValue='0.00';
			     var ETemp=0.00;
			     var ATemp=0.00;
			     ETemp = document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value;  
			     ATemp = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
			     ATemp=ATemp*ETemp;
			     roundValue=Math.round(ATemp*10000)/10000;
		    	document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=roundValue; 
			 }else if(target=='accountLine.estimateRevenueAmount'){
				 var roundValue='0.00';
			     var ETemp=0.00;
			     var ATemp=0.00;
				 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
				 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value; 
				 var baseRateVal=1;
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;  
			        ATemp=(ATemp*baseRateVal)/quantityVal;
			        
				    if(estimateDeviationVal!='' && estimateDeviationVal>0){
				    	ATemp=ATemp*100/estimateDeviationVal;
				    }
			     ETemp = document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value;  
			     ATemp=ATemp*ETemp;
			     roundValue=Math.round(ATemp*10000)/10000;
		    	document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=roundValue;
				 
			 }else if(target=='accountLine.estSellLocalRate'){
			 }else{
				 	var QTemp=0.00; 
				    var ATemp =0.00;
				    var FTemp =0.00;
				    var ETemp =0.00;
				    ETemp= document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value*1 ; 
				    FTemp =document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
				    FTemp=FTemp/ETemp;
				    ETemp= document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value*1 ;				    
				    FTemp=FTemp*ETemp;
		        roundValue=Math.round(FTemp*10000)/10000;
		        document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=roundValue;
		      	 }        
		    	   </c:if>
		    	   <c:if test="${!contractType}"> 
		    	    var estimateQuantity =0;
		    	    var estimateRevenueAmount =0;
		    		    estimateQuantity=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
		    		    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
		    		    estimateRevenueAmount =document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value; 
		    		    var estimateSellRate=0;
		    		    var estimateSellRateRound=0; 
		    		    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
		    	         estimateQuantity =document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		    	         } 
		    	        if( basis=="cwt" || basis=="%age"){
		    	        	estimateSellRate=(estimateRevenueAmount/estimateQuantity)*100;
		    	        	estimateSellRateRound=Math.round(estimateSellRate*10000)/10000;
		    	       	  document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=estimateSellRateRound;	  	
		    	       	}  else if(basis=="per 1000"){
		    	       		estimateSellRate=(estimateRevenueAmount/estimateQuantity)*1000;
		    	       		estimateSellRateRound=Math.round(estimateSellRate*10000)/10000;
		    	       	  document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=estimateSellRateRound;		  	
		    	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		    	       		estimateSellRate=(estimateRevenueAmount/estimateQuantity); 
		    	       		estimateSellRateRound=Math.round(estimateSellRate*10000)/10000; 
		    	       	  document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=estimateSellRateRound;		  	
		    		    }
		    	        estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value 
		    	   	    if(estimateSellDeviation!='' && estimateSellDeviation>0){
		    	   	    	estimateSellRate=estimateSellRateRound*100/estimateSellDeviation;
		    	   	    	estimateSellRateRound=Math.round(estimateSellRate*10000)/10000;
		    	   	    document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=estimateSellRateRound;		
		    	   	    }
		    	        </c:if>  
		    	 } 
		       function calculateSellEstimateRate(target){
		    	   <c:if test="${contractType}">
		    	   if((target=='accountLine.estimateSellQuantity')||(target=='accountLine.estSellLocalRate')||(target=='accountLine.estimateContractRate')||(target=='accountLine.basis')){
		    	      var estimatePayableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value;
		    	      var estimatePayableContractRate=document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
		    	      estimatePayableContractRate=Math.round(estimatePayableContractRate*10000)/10000;
		    	      var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
		    	      var roundValue=Math.round(estimateRate*10000)/10000;
		    	      document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=roundValue ;
		    	   }else{
		   	   	    var estimateQuantity =0;
			   	    var estimateDeviation=100;
			   	    estimateQuantity=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value; 
			   	    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
			   	    var estimateRevenueAmount =eval(document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value); 
			   	    var estimateSellRate=0;
			   	    var estimateSellRateRound=0; 
			   	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
			            estimateQuantity =document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
			            } 
			           if( basis=="cwt" || basis=="%age"){
			        	   estimateSellRate=(estimateRevenueAmount/estimateQuantity)*100;
			        	   estimateSellRateRound=Math.round(estimateSellRate*10000)/10000;
			          	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=estimateSellRateRound;	  	
			          	}  else if(basis=="per 1000"){
			          		estimateSellRate=(estimateRevenueAmount/estimateQuantity)*1000;
			          		estimateSellRateRound=Math.round(estimateSellRate*10000)/10000;
			          	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=estimateSellRateRound;		  	
			          	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
			          		estimateSellRate=(estimateRevenueAmount/estimateQuantity); 
			          		estimateSellRateRound=Math.round(estimateSellRate*10000)/10000; 
			          	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=estimateSellRateRound;		  	
			   	    }
			           estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value 
			   	    if(estimateSellDeviation!='' && estimateSellDeviation>0){
			   	    	estimateSellRateRound=estimateSellRateRound*100/estimateSellDeviation;
			   	    	estimateSellRateRound=Math.round(estimateSellRateRound*10000)/10000;
			   	    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=estimateSellRateRound;		
			   	    }   }
					</c:if>
					<c:if test="${!contractType}">
			   	    var estimateQuantity =0;
			   	    var estimateDeviation=100;
			   	    estimateQuantity=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
			   	    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
			   	    var estimateRevenueAmount =eval(document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value); 
			   	    var estimateSellRate=0;
			   	    var estimateSellRateRound=0; 
			   	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
			            estimateQuantity =document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			            } 
			           if( basis=="cwt" || basis=="%age"){
			        	   estimateSellRate=(estimateRevenueAmount/estimateQuantity)*100;
			        	   estimateSellRateRound=Math.round(estimateSellRate*10000)/10000;
			          	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=estimateSellRateRound;	  	
			          	}  else if(basis=="per 1000"){
			          		estimateSellRate=(estimateRevenueAmount/estimateQuantity)*1000;
			          		estimateSellRateRound=Math.round(estimateSellRate*10000)/10000;
			          	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=estimateSellRateRound;		  	
			          	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
			          		estimateSellRate=(estimateRevenueAmount/estimateQuantity); 
			          		estimateSellRateRound=Math.round(estimateSellRate*10000)/10000; 
			          	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=estimateSellRateRound;		  	
			   	    }
			           estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value 
			   	    if(estimateSellDeviation!='' && estimateSellDeviation>0){
			   	    	estimateSellRateRound=estimateSellRateRound*100/estimateSellDeviation;
			   	    	estimateSellRateRound=Math.round(estimateSellRateRound*10000)/10000;
			   	    document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=estimateSellRateRound;		
			   	    }  
					</c:if>
		     	    } 
		       function calculateEstimateSellLocalAmount(temp,target,field1,field2,field3){
		    		 if(field1!='' && field2!='' & field3!=''){
		    			 rateOnActualizationDate(field1,field2,field3);
		    			 }
		           
		    	   <c:if test="${contractType}"> 
		       	  if(target=='accountLine.estSellLocalRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;   		
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value;
		    	        ATemp=(ATemp*quantityVal)/baseRateVal;
		    	        var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
		    			   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
		    				   ATemp=(ATemp*estimateSellDeviation)/100;
		    				    }
		    	     	roundValue=Math.round(ATemp*10000)/10000;
		         	document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value=roundValue; 
		    			 }else if(target=='accountLine.estimateSellRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;   		
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ETemp=document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value;
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
		    	        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
		    	        var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
		    			   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
		    				   ATemp=(ATemp*estimateSellDeviation)/100;
		    				    }
		    	     	roundValue=Math.round(ATemp*10000)/10000;
		         	document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value=roundValue; 
		    			 }else if(target=='accountLine.estSellLocalAmount'){
		  			  }else{
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;   		
		    		 var estimatePayableContractExchangeRateVal=document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value;
		    		 var estExchangeRateVal=document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value; 
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
		    	        ATemp=(((ATemp/estimatePayableContractExchangeRateVal)*estExchangeRateVal)*quantityVal)/baseRateVal;
		    	        var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
		    			   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
		    				   ATemp=(ATemp*estimateSellDeviation)/100;
		    				    }
		    	     roundValue=Math.round(ATemp*10000)/10000;
		         document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value=roundValue;
		   		 	 }		     
		 	  </c:if>
		 	   <c:if test="${!contractType}"> 
		 	  var estimateQuantity =0;
			   var estLocalRate =0;
			   	    estimateQuantity=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
			   	    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
			   	    estSellLocalRate =document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value;  
			   	    var estimateSellLocalAmount=0;
			   	    var estimateSellLocalAmountRound=0; 
			   	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
			            estimateQuantity =document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			            } 
			           if( basis=="cwt" || basis=="%age"){
			        	   estimateSellLocalAmount=(estSellLocalRate*estimateQuantity)/100;
			        	   estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount*10000)/10000;
			          	  document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value=estimateSellLocalAmountRound;	  	
			          	}  else if(basis=="per 1000"){
			          		estimateSellLocalAmount=(estSellLocalRate*estimateQuantity)/1000;
			          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount*10000)/10000;
			          	  document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value=estimateSellLocalAmountRound;		  	
			          	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
			          		estimateSellLocalAmount=(estSellLocalRate*estimateQuantity); 
			          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount*10000)/10000; 
			          	  document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value=estimateSellLocalAmountRound;		  	
			   	    }
			           estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
			             if(estimateDeviation!='' && estimateDeviation>0){
			            	estimateSellLocalAmountRound=estimateSellLocalAmountRound*estimateDeviation/100;
			            	estimateSellLocalAmountRound=Math.round(estimateSellLocalAmountRound*10000)/10000;
			               document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value = estimateSellLocalAmountRound;	 
			           }  
		   			  </c:if>
		   	    	   var contractRate=0;
		 			  <c:if test="${contractType}">
		         contractRate = document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
		         </c:if> 
		    	           
		    	           if(temp == "form"){
		    	           calEstimateContractRate(target);
		       }calculateEstimateRevenue(target,'','','');
		    	           calculateVatAmtEst();
		    	   } 
		       function findRevisionSellExchangeRate(){
		           var country =document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value;
		           document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value=findExchangeRateGlobal(country)
		           document.forms['accountLineForms'].elements['accountLine.revisionSellValueDate'].value=currentDateGlobal();
	                 var contractRate=0;
	     			<c:if test="${contractType}">
	     			var revisionContractCurrency = document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value;
	                if(revisionContractCurrency == country){
	                	document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value ;
	                	document.forms['accountLineForms'].elements['accountLine.revisionContractValueDate'].value = document.forms['accountLineForms'].elements['accountLine.revisionSellValueDate'].value ;
	                } 
	                contractRate = document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
	                </c:if>
	                if(contractRate==0){  
	   				  calculateRevisionRevenueContract('none','','','');
	                }else{
	                	calRevisionSellLocalRate('NoCal','accountLine.revisionContractRate');
	                    }
				    try{
				    	<c:if test="${systemDefaultVatCalculation=='true'}">
					    	calculateREVVatAmt();	  
					    	calculateVatAmtRevision();    
				        </c:if>
				        }catch(e){}
		     }
		     
		    function calculateRevisionRevenueContract(target,field1,field2,field3) {
				 if(field1!='' && field2!='' & field3!=''){
					 rateOnActualizationDate(field1,field2,field3);
					 }	   
		    	var contractRate=0;
				<c:if test="${contractType}">
		    contractRate = document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
		    </c:if>
		    //if(contractRate==0){ 
		    	   var revisionSellLocalAmount= document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value ;
		    	   var revisionSellExchangeRate= document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value*1 ;
		    	   var roundValue=0; 
		    	  	 if(revisionSellExchangeRate ==0) {
		    	         document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=0*1;
		    	     } else if(revisionSellExchangeRate >0) {
		    	    	 <c:if test="${contractType}"> 
		    	      	 if(target=='accountLine.revisionContractRate'){
		    	    		 var roundValue='0.00';
		    	    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    	    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
		    	    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    	    		 var baseRateVal=1;
		    	    	 	 var ATemp=0.00; 
		    	    	     var ETemp =0.00;	    		 
		    	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    	    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
		    	    	         } 
		    	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	    	        	baseRateVal=100;
		    	    	         }  else if(bassisVal=="per 1000"){
		    	    	        	 baseRateVal=1000;
		    	    	       	 } else {
		    	    	       		baseRateVal=1;  	
		    	    		    }
		    	    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
		    	    	        ETemp= document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value*1 ;
		    	    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
		    					var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		    					if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
		    						   ATemp=(ATemp*revisionSellDeviation)/100;
		    					}
		    	    	        roundValue=Math.round(ATemp*10000)/10000;
		    	         document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue; 
		    	    	 }else if(target=='accountLine.revisionSellLocalRate'){
		    	    		 var roundValue='0.00';
		    	    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    	    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
		    	    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    	    		 var baseRateVal=1;
		    	    	 	 var ATemp=0.00; 
		    	    	     var ETemp =0.00;	    		 
		    	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    	    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
		    	    	         } 
		    	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	    	        	baseRateVal=100;
		    	    	         }  else if(bassisVal=="per 1000"){
		    	    	        	 baseRateVal=1000;
		    	    	       	 } else {
		    	    	       		baseRateVal=1;  	
		    	    		    }
		    	    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value;
		    	    	        ETemp= document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value*1 ;
		    	    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
		    					var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		    					if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
		    						   ATemp=(ATemp*revisionSellDeviation)/100;
		    					}
		    	    	        roundValue=Math.round(ATemp*10000)/10000;
		    	         document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue; 
		    	    	 }else if(target=='accountLine.revisionRevenueAmount'){
		    	    	 }else if(target=='accountLine.revisionSellRate'){
		    	    		 var roundValue='0.00';
		    	    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    	    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
		    	    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    	    		 var baseRateVal=1;
		    	    	 	 var ATemp=0.00; 
		    	    	     var ETemp =0.00;	    		 
		    	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    	    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
		    	    	         } 
		    	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	    	        	baseRateVal=100;
		    	    	         }  else if(bassisVal=="per 1000"){
		    	    	        	 baseRateVal=1000;
		    	    	       	 } else {
		    	    	       		baseRateVal=1;  	
		    	    		    }
		    	    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
		    	    	        ATemp=((ATemp*quantityVal)/baseRateVal);
		    					var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		    					if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
		    						   ATemp=(ATemp*revisionSellDeviation)/100;
		    					}
		    	    	        roundValue=Math.round(ATemp*10000)/10000;
		    	         		document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue;
		    	    		 
		    	    	 }else{
		    	    	       var amount=revisionSellLocalAmount/revisionSellExchangeRate; 
		    	    	       roundValue=Math.round(amount*10000)/10000;
		    	    	       document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue ; 
		    	          	 }		       
		    	      	 </c:if>
		    	       <c:if test="${!contractType}"> 
				    	       var amount=revisionSellLocalAmount/revisionSellExchangeRate; 
				    	       roundValue=Math.round(amount*10000)/10000;
				    	       document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue ; 
		    	        </c:if>
		    	     }
		    	      var revisionExpense = document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;
		    	      var revisionPassPercentageRound=0;
		    	      var revisionPassPercentage=0; 
		    	      revisionPassPercentage = (roundValue/revisionExpense)*100;
		    	  	  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
		    	  	  if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
		    	  	   	document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
		    	  	   }else{
		    	  	    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionPassPercentageRound;
		    	  	   }   
		   	  	   if(target!='accountLine.revisionSellRate'){
		    	     calculateRevisionSelllRate(target);
		   	  	   }
		     	     calculateRevisionSellLocalRate(target);
		     	 <c:if test="${contractType}">
		   	     try{
		   	     calRevisionContractRate(target);   
		   	     }catch(e){}
		   	     </c:if> 
		   	    calculateVatAmtRevision(); 
		    	     }
		    function calculateRevisionRevenueContractByExchangerate(){
		    	var contractRate=0;
				<c:if test="${contractType}">
		    contractRate = document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
		    </c:if>
		    if(contractRate==0){ 
		    	   var revisionSellLocalAmount= document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value ;
		    	   var revisionSellExchangeRate= document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value*1 ;
		    	   var roundValue=0; 
		    	  	 if(revisionSellExchangeRate ==0) {
		    	         document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=0*1;
		    	     } else if(revisionSellExchangeRate >0) {
		    	       var amount=revisionSellLocalAmount/revisionSellExchangeRate; 
		    	       roundValue=Math.round(amount*10000)/10000;
		    	       document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue ; 
		    	     }
		    	      var revisionExpense = document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;
		    	      var revisionPassPercentageRound=0;
		    	      var revisionPassPercentage=0; 
		    	      revisionPassPercentage = (roundValue/revisionExpense)*100;
		    	  	  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
		    	  	  if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
		    	  	   	document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
		    	  	   }else{
		    	  	    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionPassPercentageRound;
		    	  	   }
		    	     calculateRevisionSelllRate('none');
		     	     calculateRevisionSellLocalRate('none');
		         }else{
		    	        calRevisionSellLocalRate('NoCal','accountLine.revisionContractRate');
		                  }  }          
		    	   function calculateRevisionSelllRate(target){
		    		   <c:if test="${contractType}">
		    		   if((target=='accountLine.revisionSellQuantity')||(target=='accountLine.revisionSellLocalRate')||(target=='accountLine.revisionContractRate')||(target=='accountLine.basis')){
		    		      var estimatePayableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value;
		    		      var estimatePayableContractRate=document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
		    		      estimatePayableContractRate=Math.round(estimatePayableContractRate*10000)/10000;
		    		      var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
		    		      var roundValue=Math.round(estimateRate*10000)/10000;
		    		      document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=roundValue ;
		    		   }else{
		       	        var revisionQuantity =0;
		    	        var revisionDeviation=100;
		    	        revisionQuantity=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value; 
		    		    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
		    		    var revisionRevenueAmount =eval(document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value); 
		    		    var revisionSellRate=0;
		    		    var revisionSellRateRound=0; 
		    		    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		    	         revisionQuantity =document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
		    	         } 
		    	        if( basis=="cwt" || basis=="%age"){
		    	        	revisionSellRate=(revisionRevenueAmount/revisionQuantity)*100;
		    	        	revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
		    	       	  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRateRound;	  	
		    	       	} else if(basis=="per 1000"){
		    	       		revisionSellRate=(revisionRevenueAmount/revisionQuantity)*1000;
		    	       		revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
		    	       	  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRateRound;	  	  	
		    	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		    	       		revisionSellRate=(revisionRevenueAmount/revisionQuantity);
		    	       		revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
		    	       	  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRateRound;	  			  	
		    		    } 
		    	        revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value 
		    		    if(revisionSellDeviation!='' && revisionSellDeviation>0){
		    		    	revisionSellRateRound=revisionSellRateRound*100/revisionSellDeviation;
		    		    	revisionSellRateRound=Math.round(revisionSellRateRound*10000)/10000;
		    		    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRateRound;		 
		    		       }  }
		    		   </c:if>
		    		   <c:if test="${!contractType}">
		   	        var revisionQuantity =0;
			        var revisionDeviation=100;
			        revisionQuantity=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value; 
				    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
				    var revisionRevenueAmount =eval(document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value); 
				    var revisionSellRate=0;
				    var revisionSellRateRound=0; 
				    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
			         revisionQuantity =document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
			         } 
			        if( basis=="cwt" || basis=="%age"){
			        	revisionSellRate=(revisionRevenueAmount/revisionQuantity)*100;
			        	revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRateRound;	  	
			       	} else if(basis=="per 1000"){
			       		revisionSellRate=(revisionRevenueAmount/revisionQuantity)*1000;
			       		revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRateRound;	  	  	
			       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
			       		revisionSellRate=(revisionRevenueAmount/revisionQuantity);
			       		revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
			       	  document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRateRound;	  			  	
				    } 
			        revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value 
				    if(revisionSellDeviation!='' && revisionSellDeviation>0){
				    	revisionSellRateRound=revisionSellRateRound*100/revisionSellDeviation;
				    	revisionSellRateRound=Math.round(revisionSellRateRound*10000)/10000;
				    document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=revisionSellRateRound;		 
				       }    		   
		    		   </c:if>
		}
		    	  function calculateRevisionSellLocalRate(target){
		         	 var roundValue=0.00; 
		             <c:if test="${contractType}">
		             if(target=='accountLine.revisionSellLocalAmount'){
		        		 var roundValue='0.00';
		        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
		        		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		        		 var baseRateVal=1;
		        	 	 var ATemp=0.00; 
		        	     var ETemp =0.00;	    		 
		        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
		        	         } 
		        	        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	        	baseRateVal=100;
		        	         }  else if(bassisVal=="per 1000"){
		        	        	 baseRateVal=1000;
		        	       	 } else {
		        	       		baseRateVal=1;  	
		        		    }
		        	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value;
		        	        ATemp=(ATemp*baseRateVal)/quantityVal;
		        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		        		    	ATemp=ATemp*100/estimateDeviationVal;
		        		    	ATemp=Math.round(ATemp*10000)/10000;
		        		    }
		             document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=ATemp; 
		        	 }else if(target=='accountLine.revisionSellRate'){
		        		 var roundValue='0.00';
		        	     var ETemp=0.00;
		        	     var ATemp=0.00;
		        	     ETemp = document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value;  
		        	     ATemp = document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
		        	     ATemp=ATemp*ETemp;
		        	     roundValue=Math.round(ATemp*10000)/10000;
		            	document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=roundValue; 
		        	 }else if(target=='accountLine.revisionRevenueAmount'){
		        		 var roundValue='0.00';
		        	     var ETemp=0.00;
		        	     var ATemp=0.00;
		        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
		        		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		        		 var baseRateVal=1;
		        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
		        	         } 
		        	        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	        	baseRateVal=100;
		        	         }  else if(bassisVal=="per 1000"){
		        	        	 baseRateVal=1000;
		        	       	 } else {
		        	       		baseRateVal=1;  	
		        		    }
		        	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;  
		        	        ATemp=(ATemp*baseRateVal)/quantityVal; 
		        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		        		    	ATemp=ATemp*100/estimateDeviationVal;
		        		    }
		        	     ETemp = document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value;  
		        	     ATemp=ATemp*ETemp;
		        	     roundValue=Math.round(ATemp*10000)/10000;
		            	document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=roundValue; 
		        	 }else if(target=='accountLine.revisionSellLocalRate'){
		        	 }else{
		        		 	var QTemp=0.00; 
		        		    var ATemp =0.00;
		        		    var FTemp =0.00;
		        		    var ETemp =0.00;
		        		    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value*1 ; 
		        		    FTemp =document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
		        		    FTemp=FTemp/ETemp;
		        		    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value*1 ;				    
		        		    FTemp=FTemp*ETemp;
		                roundValue=Math.round(FTemp*10000)/10000;
		                document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=roundValue;
		              	 }
		 			</c:if>
		 			<c:if test="${!contractType}"> 
		    	    var revisionQuantity =0;
		    	    var revisionExpense=0;
		    	        revisionQuantity=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value; 
		    		    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
		    		    revisionRevenue = document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value; 
		    		    var revisionSellRate=0;
		    		    var revisionSellRateRound=0; 
		    		    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		    	         revisionQuantity =document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		    	         } 
		    	        if( basis=="cwt" || basis=="%age"){
		    	        	revisionSellRate=(revisionRevenue/revisionQuantity)*100;
		    	        	revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
		    	       	  document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=revisionSellRateRound;	  	
		    	       	} else if(basis=="per 1000"){
		    	       		revisionSellRate=(revisionRevenue/revisionQuantity)*1000;
		    	       		revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
		    	       	  document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=revisionSellRateRound;	  	  	
		    	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		    	       		revisionSellRate=(revisionRevenue/revisionQuantity);
		    	       		revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
		    	       	  document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=revisionSellRateRound;	  			  	
		    		    } 
		    	        revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value 
		     		    if(revisionDeviation!='' && revisionDeviation>0){
		     		    	revisionSellRateRound=revisionSellRateRound*100/revisionDeviation;
		     		    	revisionSellRateRound=Math.round(revisionSellRateRound*10000)/10000;
		     		    document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=revisionSellRateRound;
		     	
			   		 }
				</c:if>
		    		    } 
		    	 function calculateRevisionSellLocalAmount(temp,target,field1,field2,field3) {
		    		 if(field1!='' && field2!='' & field3!=''){
		    			 rateOnActualizationDate(field1,field2,field3);
		    			 }	   
		    		 <c:if test="${contractType}"> 
		    	  	  if(target=='accountLine.revisionSellLocalRate'){
		    			 var roundValue='0.00';
		    			 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    			 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;   		
		    			 var baseRateVal=1;
		    		 	 var ATemp=0.00; 
		    		     var ETemp =0.00;	    		 
		    			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    				 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
		    		         } 
		    		        if( bassisVal=="cwt" || bassisVal=="%age"){
		    		        	baseRateVal=100;
		    		         }  else if(bassisVal=="per 1000"){
		    		        	 baseRateVal=1000;
		    		       	 } else {
		    		       		baseRateVal=1;  	
		    			    }
		    		        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value;
		    		        ATemp=(ATemp*quantityVal)/baseRateVal;
		    				var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		    				if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
		    					   ATemp=(ATemp*revisionSellDeviation)/100;
		    				}    		        
		    		     	roundValue=Math.round(ATemp*10000)/10000;
		    	    	document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value=roundValue; 
		    				 }else if(target=='accountLine.revisionSellRate'){
				    			 var roundValue='0.00';
				    			 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
				    			 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;   		
				    			 var baseRateVal=1;
				    		 	 var ATemp=0.00; 
				    		     var ETemp =0.00;	    		 
				    			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
				    				 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
				    		         } 
				    		        if( bassisVal=="cwt" || bassisVal=="%age"){
				    		        	baseRateVal=100;
				    		         }  else if(bassisVal=="per 1000"){
				    		        	 baseRateVal=1000;
				    		       	 } else {
				    		       		baseRateVal=1;  	
				    			    }
				    		        ETemp=document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value;
				    		        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
				    		        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
				    				var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
				    				if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
				    					   ATemp=(ATemp*revisionSellDeviation)/100;
				    				}		    		        
				    		     	roundValue=Math.round(ATemp*10000)/10000;
				    	    	document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value=roundValue;
		    			 
		    				 }else if(target=='accountLine.revisionSellLocalAmount'){
		    				  }else{
		    			 var roundValue='0.00';
		    			 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    			 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;   		
		    			 var estimatePayableContractExchangeRateVal=document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value;
		    			 var estExchangeRateVal=document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value; 
		    			 var baseRateVal=1;
		    		 	 var ATemp=0.00; 
		    		     var ETemp =0.00;	    		 
		    			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    				 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
		    		         } 
		    		        if( bassisVal=="cwt" || bassisVal=="%age"){
		    		        	baseRateVal=100;
		    		         }  else if(bassisVal=="per 1000"){
		    		        	 baseRateVal=1000;
		    		       	 } else {
		    		       		baseRateVal=1;  	
		    			    }
		    		        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
		    		        ATemp=(((ATemp/estimatePayableContractExchangeRateVal)*estExchangeRateVal)*quantityVal)/baseRateVal;
		    				var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		    				if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
		    					   ATemp=(ATemp*revisionSellDeviation)/100;
		    				}
		    		     	roundValue=Math.round(ATemp*10000)/10000;
		    	    		document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value=roundValue;
		    			 	 }		     
		    	  </c:if>
		    	   <c:if test="${!contractType}"> 
		    	   var revisionQuantity =0;
		     	  var revisionLocalRate=0;
		     	        revisionQuantity=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value; 
		     		    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
		     		    revisionSellLocalRate =document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value; 
		     		    var revisionSellLocalAmount=0;
		     		    var revisionSellLocalAmountRound=0; 
		     		    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		     	         revisionQuantity =document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		     	         } 
		     	        if( basis=="cwt" || basis=="%age"){
		     	        	revisionSellLocalAmount=(revisionSellLocalRate*revisionQuantity)/100;
		     	        	revisionSellLocalAmountRound=Math.round(revisionSellLocalAmount*10000)/10000;
		     	       	  document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value=revisionSellLocalAmountRound;	  	
		     	       	} else if(basis=="per 1000"){
		     	       		revisionSellLocalAmount=(revisionSellLocalRate*revisionQuantity)/1000;
		     	       		revisionSellLocalAmountRound=Math.round(revisionSellLocalAmount*10000)/10000;
		     	       	  document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value=revisionSellLocalAmountRound;	  	  	
		     	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		     	       		revisionSellLocalAmount=(revisionSellLocalRate*revisionQuantity);
		     	       		revisionSellLocalAmountRound=Math.round(revisionSellLocalAmount*10000)/10000;
		     	       	  document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value=revisionSellLocalAmountRound;	  			  	
		     		    }
		     	        revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
		 	             if(revisionDeviation!='' && revisionDeviation>0){
		 	            	 revisionSellLocalAmountRound=revisionSellLocalAmountRound*revisionDeviation/100;
		 	            	 revisionSellLocalAmountRound=Math.round(revisionSellLocalAmountRound*10000)/10000;
		 	               document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value = revisionSellLocalAmountRound;	 
		 	           } 
		    				  </c:if> 
			             var contractRate=0;
			 			<c:if test="${contractType}">
			         contractRate = document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
			         </c:if> 
			            
			             if(temp ==="form"){
			            	 calRevisionContractRate(target);   
			         }   calculateRevisionRevenueContract(target,'','','');} 
		    	 function  calRevisionPayableContractRate(){
		        	 var roundValue=0.00; 
		    		 var revisionQuantity =0;
		      	    var revisionExpense=0;
		      	        revisionQuantity=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value; 
		      		    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
		      		    revisionRevenue = document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value; 
		      		    var revisionSellRate=0;
		      		    var revisionSellRateRound=0; 
		      		    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		      	         revisionQuantity =document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		      	         } 
		      	        if( basis=="cwt" || basis=="%age"){
		      	        	revisionSellRate=(revisionRevenue/revisionQuantity)*100;
		      	        	revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
		      	       	  document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value=revisionSellRateRound;	  	
		      	       	} else if(basis=="per 1000"){
		      	       		revisionSellRate=(revisionRevenue/revisionQuantity)*1000;
		      	       		revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
		      	       	  document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value=revisionSellRateRound;	  	  	
		      	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		      	       		revisionSellRate=(revisionRevenue/revisionQuantity);
		      	       		revisionSellRateRound=Math.round(revisionSellRate*10000)/10000;
		      	       	  document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value=revisionSellRateRound;	  			  	
		      		    }
		      	        revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value 
		      		    if(revisionDeviation!='' && revisionDeviation>0){
		      		    revisionSellRateRound=revisionSellRateRound*100/revisionDeviation;
		      		    revisionSellRateRound=Math.round(revisionSellRateRound*10000)/10000;
		      		    document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value=revisionSellRateRound;
		      		    } }

		    function findEstimatePayableContractExchangeRate(){
		        var country =document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value; 
		        document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value=findExchangeRateGlobal(country)
		        document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].value=currentDateGlobal();
		        var estCurrency = document.forms['accountLineForms'].elements['accountLine.estCurrency'].value;
                if(estCurrency==country){
                	document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value;
                	document.forms['accountLineForms'].elements['accountLine.estValueDate'].value = document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].value;
                }
				  calculateEstimateRateByContractRate('accountLine.estimatePayableContractRate');
				    try{
				    	<c:if test="${systemDefaultVatCalculation=='true'}">
					    	calculateESTVatAmt();
					    	calculateVatAmtEst();    
				        </c:if>
				        }catch(e){}
		  }
		   
		 function  calculateEstimateRateByContractRate(target) {
			   <c:if test="${contractType}">
			        var country =document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value; 
			        if(country=='') {
			          alert("Please select Contract Currency ");
			          document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value=0;
			          document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value=1;
			        } else if(country!=''){
			        var estimatePayableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value
			        var estimatePayableContractRate=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value
			        estimatePayableContractRate=Math.round(estimatePayableContractRate*10000)/10000;
			        var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
			        var roundValue=Math.round(estimateRate*10000)/10000;
			        document.forms['accountLineForms'].elements['accountLine.estimateRate'].value=roundValue ;  
			        calEstLocalRate('NoCal',target);
			        //calRecCurrencyRate();
			        calEstimatePayableContractRateAmmount(target);
			        calculateESTVatAmt();
			        }   
			   </c:if>
			   }
		 function calculateEstimateExpenseByContractRate(target){ 
		   var estLocalAmount= document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value ;
		   var estExchangeRate= document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value*1 ;
		   var roundValue=0; 
		  	 if(estExchangeRate ==0) {
		         document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=0*1;
		     } else if(estExchangeRate >0) {
		       <c:if test="${contractType}"> 
		    	 if(target=='accountLine.estimatePayableContractRate'){
		  		 var roundValue='0.00';
		  		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		  		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
		  		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
		  		 var baseRateVal=1;
		  	 	 var ATemp=0.00; 
		  	     var ETemp =0.00;	    		 
		  		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		  			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		  	         } 
		  	        if( bassisVal=="cwt" || bassisVal=="%age"){
		  	        	baseRateVal=100;
		  	         }  else if(bassisVal=="per 1000"){
		  	        	 baseRateVal=1000;
		  	       	 } else {
		  	       		baseRateVal=1;  	
		  		    }
		  	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
		  	        ETemp= document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value*1 ;
		  	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
					var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
					   if(estimateDeviation!='' && estimateDeviation>0){ 
						   ATemp=(ATemp*estimateDeviation)/100;
						    }  	        
		  	        roundValue=Math.round(ATemp*10000)/10000;
		       document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue; 
		  	 }else if(target=='accountLine.estLocalRate'){
		  		 var roundValue='0.00';
		  		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		  		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
		  		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
		  		 var baseRateVal=1;
		  	 	 var ATemp=0.00; 
		  	     var ETemp =0.00;	    		 
		  		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		  			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		  	         } 
		  	        if( bassisVal=="cwt" || bassisVal=="%age"){
		  	        	baseRateVal=100;
		  	         }  else if(bassisVal=="per 1000"){
		  	        	 baseRateVal=1000;
		  	       	 } else {
		  	       		baseRateVal=1;  	
		  		    }
		  	        ATemp=document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value;
		  	        ETemp= document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value*1 ;
		  	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
					var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
					   if(estimateDeviation!='' && estimateDeviation>0){ 
						   ATemp=(ATemp*estimateDeviation)/100;
						    }  	        
		  	        roundValue=Math.round(ATemp*10000)/10000;
		       document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue; 
		  	 }else if(target=='accountLine.estimateExpense'){
		  	 }else if(target=='accountLine.estimateRate'){
		  		 var roundValue='0.00';
		  		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		  		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
		  		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
		  		 var baseRateVal=1;
		  	 	 var ATemp=0.00; 
		  	     var ETemp =0.00;	    		 
		  		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		  			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		  	         } 
		  	        if( bassisVal=="cwt" || bassisVal=="%age"){
		  	        	baseRateVal=100;
		  	         }  else if(bassisVal=="per 1000"){
		  	        	 baseRateVal=1000;
		  	       	 } else {
		  	       		baseRateVal=1;  	
		  		    }
		  	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
		  	        ATemp=((ATemp*quantityVal)/baseRateVal);
					var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
					   if(estimateDeviation!='' && estimateDeviation>0){ 
						   ATemp=(ATemp*estimateDeviation)/100;
						    }  	        
		  	        roundValue=Math.round(ATemp*10000)/10000;
		       		document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue;
		  		 
		  	 }else{
		  	       var amount=estLocalAmount/estExchangeRate; 
		  	       roundValue=Math.round(amount*10000)/10000;
		  	       document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue ;
		        	 }		       
		    	 </c:if>
		     <c:if test="${!contractType}"> 
		  		   var amount=estLocalAmount/estExchangeRate; 
		  		   roundValue=Math.round(amount*10000)/10000;
		  		   document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value=roundValue ;
		      </c:if>        
		     }
		     var estimatepasspercentageRound=0;
		     var estimatepasspercentage = 0;
		     var estimateRevenue = document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value; 
		     estimatepasspercentage = (estimateRevenue/roundValue)*100;
		  	 estimatepasspercentageRound=Math.round(estimatepasspercentage);
		  	 if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
		  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
		  	   }else{
		  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
		  	   }  
			 }
		 function calEstLocalRate(temp,target){ 
			          var country =document.forms['accountLineForms'].elements['accountLine.estCurrency'].value; 
			          if(country=='') {
			          alert("Please select currency "); 
			          document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=0; 
			        } else if(country!=''){ 
			        	<c:if test="${contractType}"> 
			       	 if(target=='accountLine.estLocalAmount'){
			    		 var roundValue='0.00';
			    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
			    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
			    		 var baseRateVal=1;
			    	 	 var ATemp=0.00; 
			    	     var ETemp =0.00;	    		 
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	        ATemp=document.forms['accountLineForms'].elements['accountLine.estLocalAmount'].value;
			    	        ATemp=(ATemp*baseRateVal)/quantityVal;
			    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			    		    	ATemp=ATemp*100/estimateDeviationVal;
			    		    	ATemp=Math.round(ATemp*10000)/10000;
			    		    }
			         document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=ATemp; 
			    	 }else if(target=='accountLine.estimateRate'){
			    		 var roundValue='0.00';
			    	     var ETemp=0.00;
			    	     var ATemp=0.00;
			    	     ETemp = document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value;  
			    	     ATemp = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
			    	     ATemp=ATemp*ETemp;
			    	     roundValue=Math.round(ATemp*10000)/10000;
			        	document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=roundValue; 
			    	 }else if(target=='accountLine.estimateExpense'){
			    		 var roundValue='0.00';
			    	     var ETemp=0.00;
			    	     var ATemp=0.00;
			    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
			    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
			    		 var baseRateVal=1;
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value;  
			    	        ATemp=(ATemp*baseRateVal)/quantityVal; 
			    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			    		    	ATemp=ATemp*100/estimateDeviationVal;
			    		    }
			    	     ETemp = document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value;  
			    	     ATemp=ATemp*ETemp;
			    	     roundValue=Math.round(ATemp*10000)/10000;
			        	document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=roundValue; 
			    	 }else if(target=='accountLine.estLocalRate'){
			    	 }else{
			    		 var roundValue='0.00';
			    		 	var QTemp=0.00; 
			    		    var ATemp =0.00;
			    		    var FTemp =0.00;
			    		    var ETemp =0.00;
			    		    ETemp= document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value*1 ; 
			    		    FTemp =document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
			    		    FTemp=FTemp/ETemp;
			    		    ETemp= document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value*1 ;				    
			    		    FTemp=FTemp*ETemp;
			         roundValue=Math.round(FTemp*10000)/10000;
			         document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=roundValue;
			    	 }		       </c:if>
				       <c:if test="${!contractType}"> 
				        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value
				        var recRate=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value
				        var recCurrencyRate=recRate*recRateExchange;
				        var roundValue=Math.round(recCurrencyRate*10000)/10000;
				        document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value=roundValue ; 
				        </c:if>
			         calculateEstimateLocalAmount('cal',target);
			         if(temp=='cal'){
			         calEstimatePayableContractRateByBase(target);
			         }  }   } 
		 function calEstimatePayableContractRateAmmount(target) { 
			   <c:if test="${contractType}">
			 	  if(target=='accountLine.estLocalRate'){ 
			 		 var roundValue='0.00';
					 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    } 
				        ATemp=document.forms['accountLineForms'].elements['accountLine.estLocalRate'].value;
				        ETemp=document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value;
				        ATemp=ATemp/ETemp;
				        ETemp=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
			 		   var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
					   if(estimateDeviation!='' && estimateDeviation>0){ 
						   ATemp=(ATemp*estimateDeviation)/100;
						    }			        
				     	roundValue=Math.round(ATemp*10000)/10000;
			    		document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value=roundValue; 
		 			}else if(target=='accountLine.estimateRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    }
				        ETemp=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value;
				        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
				 		   var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
						   if(estimateDeviation!='' && estimateDeviation>0){ 
							   ATemp=(ATemp*estimateDeviation)/100;
							    }	
				     	roundValue=Math.round(ATemp*10000)/10000;
			    		document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value=roundValue; 
						 }else if(target=='accountLine.estimatePayableContractRateAmmount'){
						  }else{
								 var roundValue='0.00';
								 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
								 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;   		
								 var baseRateVal=1;
							 	 var ATemp=0.00; 
							     var ETemp =0.00;	    		 
								 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
									 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
							         } 
							        if( bassisVal=="cwt" || bassisVal=="%age"){
							        	baseRateVal=100;
							         }  else if(bassisVal=="per 1000"){
							        	 baseRateVal=1000;
							       	 } else {
							       		baseRateVal=1;  	
								    }
							        ATemp=document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value;
							        ATemp=(ATemp*quantityVal)/baseRateVal;
							 		   var estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
									   if(estimateDeviation!='' && estimateDeviation>0){ 
										   ATemp=(ATemp*estimateDeviation)/100;
										    }	
							     	roundValue=Math.round(ATemp*10000)/10000;
						    	document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value=roundValue;
					 	 } 
			             calculateEstimateExpenseByContractRate(target);   
			   </c:if>
			  		}
		 function calEstimatePayableContractRate() { 
			  var estimateQuantity =0;
			    var estimateDeviation=100;
			    estimateQuantity=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
			    var basis =document.forms['accountLineForms'].elements['accountLine.basis'].value; 
			    var estimateExpense =eval(document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRateAmmount'].value); 
		        var estimateRate=0;
			    var estimateRateRound=0; 
			    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
		       estimateQuantity =document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
		       } 
		      if( basis=="cwt" || basis=="%age"){
		     	  estimateRate=(estimateExpense/estimateQuantity)*100;
		     	  estimateRateRound=Math.round(estimateRate*10000)/10000;
		     	  document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value=estimateRateRound;	  	
		     	}  else if(basis=="per 1000"){
		     	  estimateRate=(estimateExpense/estimateQuantity)*1000;
		     	  estimateRateRound=Math.round(estimateRate*10000)/10000;
		     	  document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value=estimateRateRound;		  	
		     	} else  {
		        estimateRate=(estimateExpense/estimateQuantity); 
		     	  estimateRateRound=Math.round(estimateRate*10000)/10000; 
		     	  document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value=estimateRateRound;		  	
			    }
			    estimateDeviation=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value 
			    if(estimateDeviation!='' && estimateDeviation>0){
			    estimateRate=estimateRateRound*100/estimateDeviation;
			    estimateRateRound=Math.round(estimateRate*10000)/10000;
			    document.forms['accountLineForms'].elements['accountLine.estimatePayableContractRate'].value=estimateRateRound;		
			    } } 
		 function findEstimateContractCurrencExchangeyRate(field1,field2,field3){
			 if(field1!='' && field2!='' & field3!=''){
				 rateOnActualizationDate(field1,field2,field3);
				 }	 
		     var country =document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value; 
		     document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value=findExchangeRateGlobal(country)
		     document.forms['accountLineForms'].elements['accountLine.estimateContractValueDate'].value=currentDateGlobal();  
		     var estSellCurrency = document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value;
             if(estSellCurrency==country){
             	document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value;
             	document.forms['accountLineForms'].elements['accountLine.estSellValueDate'].value = document.forms['accountLineForms'].elements['accountLine.estimateContractValueDate'].value;
             } 
		     calculateEstimateSellRateByContractRate('accountLine.estimateContractRate','','','');
		    	<c:if test="${systemDefaultVatCalculation=='true'}">
			    	calculateESTVatAmt();
			    	calculateVatAmtEst();    
		        </c:if>
		}
		
		function  calculateEstimateSellRateByContractRate(target,field1,field2,field3) {
			 if(field1!='' && field2!='' & field3!=''){
				 rateOnActualizationDate(field1,field2,field3);
				 }
			
			   <c:if test="${contractType}">
			        var country =document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value; 
			        if(country=='') {
			          alert("Please select Contract Currency ");
			          document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value=0;
			          document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value=1;
			        } else if(country!=''){
			        var estimateContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value
			        var estimateContractRate=document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value
			        estimateContractRate=Math.round(estimateContractRate*10000)/10000;
			        var estimateSellRate=estimateContractRate/estimateContractExchangeRate;
			        var roundValue=Math.round(estimateSellRate*10000)/10000;
			        document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=roundValue ; 
			        calEstSellLocalRate('NoCal',target);
			        //calRecCurrencyRate();
			        calEstimateContractRateAmmount(target);
			        }   
			   </c:if>
			   }
		function calEstSellLocalRate(temp,target){
			<c:if test="${multiCurrency=='Y'}">
			          var country =document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value; 
			          if(country=='') {
			          alert("Please select billing currency "); 
			          document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=0; 
			        } else if(country!=''){  
			        	<c:if test="${contractType}"> 
			          	 if(target=='accountLine.estSellLocalAmount'){
			        		 var roundValue='0.00';
			        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
			        		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
			        		 var baseRateVal=1;
			        	 	 var ATemp=0.00; 
			        	     var ETemp =0.00;	    		 
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['accountLineForms'].elements['accountLine.estSellLocalAmount'].value;
			        	        ATemp=(ATemp*baseRateVal)/quantityVal;
			        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			        		    	ATemp=ATemp*100/estimateDeviationVal;
			        		    	ATemp=Math.round(ATemp*10000)/10000;
			        		    }
			             document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=ATemp; 
			        	 }else if(target=='accountLine.estimateSellRate'){
			        		 var roundValue='0.00';
			        	     var ETemp=0.00;
			        	     var ATemp=0.00;
			        	     ETemp = document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value;  
			        	     ATemp = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
			        	     ATemp=ATemp*ETemp;
			        	     roundValue=Math.round(ATemp*10000)/10000;
			            	document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=roundValue; 
			        	 }else if(target=='accountLine.estimateRevenueAmount'){
			        		 var roundValue='0.00';
			        	     var ETemp=0.00;
			        	     var ATemp=0.00;
			        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
			        		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
			        		 var baseRateVal=1;
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value; 
			        	        ATemp=(ATemp*baseRateVal)/quantityVal;
			        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			        		    	ATemp=ATemp*100/estimateDeviationVal;
			        		    }
			        	     ETemp = document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value;  
			        	     ATemp=ATemp*ETemp;
			        	     roundValue=Math.round(ATemp*10000)/10000; 
			            	document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=roundValue; 
			        	 }else if(target=='accountLine.estSellLocalRate'){
			        	 }else{
			        		 	var QTemp=0.00; 
			        		    var ATemp =0.00;
			        		    var FTemp =0.00;
			        		    var ETemp =0.00;
			        		    ETemp= document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value*1 ; 
			        		    FTemp =document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
			        		    FTemp=FTemp/ETemp;
			        		    ETemp= document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value*1 ;				    
			        		    FTemp=FTemp*ETemp;
			                roundValue=Math.round(FTemp*10000)/10000;
			                document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=roundValue;
			              	 }		       </c:if>
				       <c:if test="${!contractType}"> 
					        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value
					        var recRate=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value
					        var recCurrencyRate=recRate*recRateExchange;
					        var roundValue=Math.round(recCurrencyRate*10000)/10000;
					        document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value=roundValue ; 			        
					        </c:if>
			        calculateEstimateSellLocalAmount('cal',target,'','','');
			        if(temp=='cal'){
			        calEstimateContractRate('none');
			        }  } 
		     </c:if>		        
			  } 
		function calEstimateContractRateAmmount(target) { 
			   <c:if test="${contractType}">
			   if(target=='accountLine.estSellLocalRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    } 
				        ATemp=document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value;
				        ETemp=document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value;
				        ATemp=ATemp/ETemp;
				        ETemp=document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						   var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
						   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
							   ATemp=(ATemp*estimateSellDeviation)/100;
							    }		        
				     	roundValue=Math.round(ATemp*10000)/10000;
			    	document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value=roundValue; 
						 }else if(target=='accountLine.estimateSellRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    }
				        ETemp=document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value;
				        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						   var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
						   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
							   ATemp=(ATemp*estimateSellDeviation)/100;
							    }		        
				     	roundValue=Math.round(ATemp*10000)/10000;
			    	document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value=roundValue; 
						 }else if(target=='accountLine.estimateContractRateAmmount'){
						  }else{
								 var roundValue='0.00';
								 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
								 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;   		
								 var baseRateVal=1;
							 	 var ATemp=0.00; 
							     var ETemp =0.00;	    		 
								 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
									 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
							         } 
							        if( bassisVal=="cwt" || bassisVal=="%age"){
							        	baseRateVal=100;
							         }  else if(bassisVal=="per 1000"){
							        	 baseRateVal=1000;
							       	 } else {
							       		baseRateVal=1;  	
								    }
							        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
							        ATemp=(ATemp*quantityVal)/baseRateVal;
									   var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
									   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
										   ATemp=(ATemp*estimateSellDeviation)/100;
										    }					        
							     	roundValue=Math.round(ATemp*10000)/10000;
						    	document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value=roundValue;
					 	 }	
			             calculateEstimateRevenueAmountByContractRate(target); 
			   </c:if>
			  		} 
		function calculateEstimateRevenueAmountByContractRate(target) { 
			   var estSellLocalAmount= document.forms['accountLineForms'].elements['accountLine.estimateContractRateAmmount'].value ;
			   var estSellExchangeRate= document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value*1 ;
			   var roundValue=0; 
			  	 if(estSellExchangeRate ==0) {
			         document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=0*1;
			     } else if(estSellExchangeRate >0) {
			       <c:if test="${contractType}"> 
		        	 if(target=='accountLine.estimateContractRate'){
		      		 var roundValue='0.00';
		      		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		      		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
		      		 var baseRateVal=1;
		      	 	 var ATemp=0.00; 
		      	     var ETemp =0.00;	    		 
		      		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		      			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
		      	         } 
		      	        if( bassisVal=="cwt" || bassisVal=="%age"){
		      	        	baseRateVal=100;
		      	         }  else if(bassisVal=="per 1000"){
		      	        	 baseRateVal=1000;
		      	       	 } else {
		      	       		baseRateVal=1;  	
		      		    }
		      	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateContractRate'].value;
		      	        ETemp= document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value*1 ;
		      	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal); 
		    		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
		    			   ATemp=(ATemp*estimateSellDeviation)/100;
		    			    }      	        
		      	        roundValue=Math.round(ATemp*10000)/10000;
		           document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue; 
		      	 }else if(target=='accountLine.estSellLocalRate'){
		      		 var roundValue='0.00';
		      		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		      		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
		      		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
		      		 var baseRateVal=1;
		      	 	 var ATemp=0.00; 
		      	     var ETemp =0.00;	    		 
		      		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		      			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
		      	         } 
		      	        if( bassisVal=="cwt" || bassisVal=="%age"){
		      	        	baseRateVal=100;
		      	         }  else if(bassisVal=="per 1000"){
		      	        	 baseRateVal=1000;
		      	       	 } else {
		      	       		baseRateVal=1;  	
		      		    }
		      	        ATemp=document.forms['accountLineForms'].elements['accountLine.estSellLocalRate'].value;
		      	        ETemp= document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value*1 ;
		      	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
		      		   var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
		    		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
		    			   ATemp=(ATemp*estimateSellDeviation)/100;
		    			    }      	        
		      	        roundValue=Math.round(ATemp*10000)/10000;
		           document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue; 
		      	 }else if(target=='accountLine.estimateRevenueAmount'){
		      	 }else if(target=='accountLine.estimateSellRate'){
		      		 var roundValue='0.00';
		      		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		      		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value;
		      		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.estimateDeviation'].value;
		      		 var baseRateVal=1;
		      	 	 var ATemp=0.00; 
		      	     var ETemp =0.00;	    		 
		      		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		      			 quantityVal = document.forms['accountLineForms'].elements['accountLine.estimateSellQuantity'].value=1;
		      	         } 
		      	        if( bassisVal=="cwt" || bassisVal=="%age"){
		      	        	baseRateVal=100;
		      	         }  else if(bassisVal=="per 1000"){
		      	        	 baseRateVal=1000;
		      	       	 } else {
		      	       		baseRateVal=1;  	
		      		    }
		      	        ATemp=document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
		      	        ATemp=((ATemp*quantityVal)/baseRateVal);
		      		   var estimateSellDeviation=document.forms['accountLineForms'].elements['accountLine.estimateSellDeviation'].value;
		    		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
		    			   ATemp=(ATemp*estimateSellDeviation)/100;
		    			    }      	        
		      	        roundValue=Math.round(ATemp*10000)/10000;
		           		document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue;
		      		 
		      	 }else{
			      	       var amount=estSellLocalAmount/estSellExchangeRate; 
			    	       roundValue=Math.round(amount*10000)/10000;
			    	       document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue ; 
		            	 }		       
		        	 </c:if>
			       <c:if test="${!contractType}"> 
			      	       var amount=estSellLocalAmount/estSellExchangeRate; 
			    	       roundValue=Math.round(amount*10000)/10000;
			    	       document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=roundValue ; 
			        </c:if>
			     }
			     var estimatepasspercentageRound=0;
			     var estimatepasspercentage = 0;
			     var estimateExpense = document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value; 
			     estimatepasspercentage = (roundValue/estimateExpense)*100;
			  	 estimatepasspercentageRound=Math.round(estimatepasspercentage);
			  	 if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
			  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
			  	   }else{
			  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
			  	   } } 
		function findRevisionContractCurrencyExchangeRate(field1,field2,field3){
			 if(field1!='' && field2!='' & field3!=''){
				 rateOnActualizationDate(field1,field2,field3);
				 }
		    var country =document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value; 
		    document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value=findExchangeRateGlobal(country)
		    document.forms['accountLineForms'].elements['accountLine.revisionContractValueDate'].value=currentDateGlobal();
		    var revisionSellCurrency = document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value;
            if(revisionSellCurrency == country){
            	document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value;
            	document.forms['accountLineForms'].elements['accountLine.revisionSellValueDate'].value = document.forms['accountLineForms'].elements['accountLine.revisionContractValueDate'].value;
            } 
			  calculateRevisionSellRateByContractRate('accountLine.revisionContractRate','','','');
			    try{
			    	<c:if test="${systemDefaultVatCalculation=='true'}">
			    	calculateREVVatAmt();	  
			    	calculateVatAmtRevision();    
			        </c:if>
			        }catch(e){}
		}
		
		function  calculateRevisionSellRateByContractRate(target,field1,field2,field3) {
			 if(field1!='' && field2!='' & field3!=''){
				 rateOnActualizationDate(field1,field2,field3);
				 }	   
			   <c:if test="${contractType}">
			        var country =document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value; 
			        if(country=='') {
			          alert("Please select Contract Currency ");
			          document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value=0;
			          document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value=1;
			        } else if(country!=''){
			        var estimateContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value
			        var estimateContractRate=document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value
			        estimateContractRate=Math.round(estimateContractRate*10000)/10000;
			        var estimateSellRate=estimateContractRate/estimateContractExchangeRate;
			        var roundValue=Math.round(estimateSellRate*10000)/10000;
			        document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value=roundValue ; 
			        calRevisionSellLocalRate('NoCal',target);
			        //calRecCurrencyRate();
			        calRevisionContractRateAmmount(target);
			        }   
			   </c:if>
			   }
		function calRevisionSellLocalRate(temp,target){
			<c:if test="${multiCurrency=='Y'}">
			          var country =document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value; 
			          if(country=='') {
			          alert("Please select billing currency "); 
			          document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=0; 
			        } else if(country!=''){
			        	 <c:if test="${contractType}">  
			        	 if(target=='accountLine.revisionSellLocalAmount'){
			        		 var roundValue='0.00';
			        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
			        		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
			        		 var baseRateVal=1;
			        	 	 var ATemp=0.00; 
			        	     var ETemp =0.00;	    		 
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellLocalAmount'].value;
			        	        ATemp=(ATemp*baseRateVal)/quantityVal;
			        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			        		    	ATemp=ATemp*100/estimateDeviationVal;
			        		    	ATemp=Math.round(ATemp*10000)/10000;
			        		    }
			             document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=ATemp; 
			        	 }else if(target=='accountLine.revisionSellRate'){
			        		 var roundValue='0.00';
			        	     var ETemp=0.00;
			        	     var ATemp=0.00;
			        	     ETemp = document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value;  
			        	     ATemp = document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
			        	     ATemp=ATemp*ETemp;
			        	     roundValue=Math.round(ATemp*10000)/10000;
			            	document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=roundValue; 
			        	 }else if(target=='accountLine.revisionRevenueAmount'){
			        		 var roundValue='0.00';
			        	     var ETemp=0.00;
			        	     var ATemp=0.00;
			        		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			        		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
			        		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
			        		 var baseRateVal=1;
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;  
			        	        ATemp=(ATemp*baseRateVal)/quantityVal; 
			        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			        		    	ATemp=ATemp*100/estimateDeviationVal;
			        		    }
			        	     ETemp = document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value;  
			        	     ATemp=ATemp*ETemp;
			        	     roundValue=Math.round(ATemp*10000)/10000;
			            	document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=roundValue; 
			        	 }else if(target=='accountLine.revisionSellLocalRate'){
			        	 }else{
			        		 	var QTemp=0.00; 
			        		    var ATemp =0.00;
			        		    var FTemp =0.00;
			        		    var ETemp =0.00;
			        		    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value*1 ; 
			        		    FTemp =document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
			        		    FTemp=FTemp/ETemp;
			        		    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value*1 ;				    
			        		    FTemp=FTemp*ETemp;
			                roundValue=Math.round(FTemp*10000)/10000;
			                document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=roundValue;
			              	 }
						</c:if>
						 <c:if test="${!contractType}"> 
					        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value
					        var recRate=document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value
					        var recCurrencyRate=recRate*recRateExchange;
					        var roundValue=Math.round(recCurrencyRate*10000)/10000;
					        document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value=roundValue ;
						</c:if> 
				        calculateRevisionSellLocalAmount('cal',target,'','','');
				        if(temp=='cal'){
				        	calRevisionContractRate('none');  
				        }  } 
		  </c:if>		        
			  } 
		function calRevisionContractRateAmmount(target) { 
			   <c:if test="${contractType}">
			   if(target=='accountLine.revisionSellLocalRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    } 
				        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value;
				        ETemp=document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value;
				        ATemp=ATemp/ETemp;
				        ETemp=document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
						if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
							   ATemp=(ATemp*revisionSellDeviation)/100;
						}
				     	roundValue=Math.round(ATemp*10000)/10000;
			    	document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value=roundValue; 
						 }else if(target=='accountLine.revisionSellRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    }
				        ETemp=document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value;
				        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
						if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
							   ATemp=(ATemp*revisionSellDeviation)/100;
						}		        
				     	roundValue=Math.round(ATemp*10000)/10000;
			    	document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value=roundValue; 
						 }else if(target=='accountLine.revisionContractRateAmmount'){
						  }else{
								 var roundValue='0.00';
								 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
								 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;   		
								 var baseRateVal=1;
							 	 var ATemp=0.00; 
							     var ETemp =0.00;	    		 
								 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
									 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
							         } 
							        if( bassisVal=="cwt" || bassisVal=="%age"){
							        	baseRateVal=100;
							         }  else if(bassisVal=="per 1000"){
							        	 baseRateVal=1000;
							       	 } else {
							       		baseRateVal=1;  	
								    }
							        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
							        ATemp=(ATemp*quantityVal)/baseRateVal;
									var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
									if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
										   ATemp=(ATemp*revisionSellDeviation)/100;
									}					        
							     	roundValue=Math.round(ATemp*10000)/10000;
						    	document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value=roundValue;
					 	 }	
			              calculateRevisionRevenueAmountByContractRate(target);   
			   </c:if>
			  		}  
		function  calculateRevisionRevenueAmountByContractRate(target){ 
			   var revisionSellLocalAmount= document.forms['accountLineForms'].elements['accountLine.revisionContractRateAmmount'].value ;
			   var revisionSellExchangeRate= document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value*1 ;
			   var roundValue=0; 
			  	 if(revisionSellExchangeRate ==0) {
			         document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=0*1;
			     } else if(revisionSellExchangeRate >0) {
			       <c:if test="${contractType}"> 
			      	 if(target=='accountLine.revisionContractRate'){
			    		 var roundValue='0.00';
			    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
			    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
			    		 var baseRateVal=1;
			    	 	 var ATemp=0.00; 
			    	     var ETemp =0.00;	    		 
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionContractRate'].value;
			    	        ETemp= document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value*1 ;
			    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
							var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
							if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
								   ATemp=(ATemp*revisionSellDeviation)/100;
							}
			    	        roundValue=Math.round(ATemp*10000)/10000;
			         document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue; 
			    	 }else if(target=='accountLine.revisionSellLocalRate'){
			    		 var roundValue='0.00';
			    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
			    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
			    		 var baseRateVal=1;
			    	 	 var ATemp=0.00; 
			    	     var ETemp =0.00;	    		 
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellLocalRate'].value;
			    	        ETemp= document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value*1 ;
			    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
							var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
							if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
								   ATemp=(ATemp*revisionSellDeviation)/100;
							}
			    	        roundValue=Math.round(ATemp*10000)/10000;
			         document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue; 
			    	 }else if(target=='accountLine.revisionRevenueAmount'){
			    	 }else if(target=='accountLine.revisionSellRate'){
			    		 var roundValue='0.00';
			    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
			    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value;
			    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
			    		 var baseRateVal=1;
			    	 	 var ATemp=0.00; 
			    	     var ETemp =0.00;	    		 
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionSellQuantity'].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionSellRate'].value;
			    	        ATemp=((ATemp*quantityVal)/baseRateVal);
							var revisionSellDeviation=document.forms['accountLineForms'].elements['accountLine.revisionSellDeviation'].value;
							if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
								   ATemp=(ATemp*revisionSellDeviation)/100;
							}
			    	        roundValue=Math.round(ATemp*10000)/10000;
			         		document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue; 
			    	 }else{
					       var amount=revisionSellLocalAmount/revisionSellExchangeRate; 
					       roundValue=Math.round(amount*10000)/10000;
					       document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue ; 
			          	 }		       
			      	 </c:if>
			       <c:if test="${!contractType}"> 
					       var amount=revisionSellLocalAmount/revisionSellExchangeRate; 
					       roundValue=Math.round(amount*10000)/10000;
					       document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value=roundValue ;
			        </c:if>
			     }
			      var revisionExpense = document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;
			      var revisionPassPercentageRound=0;
			      var revisionPassPercentage=0; 
			      revisionPassPercentage = (roundValue/revisionExpense)*100;
			  	  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
			  	  if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
			  	   	document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
			  	   }else{
			  	    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionPassPercentageRound;
			  	   }  } 
		function findRevisionPayableContractCurrencyExchangeRate(){
		    var country =document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value; 
		    	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value=findExchangeRateGlobal(country)
		    	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].value=currentDateGlobal();
		    	var revisionCurrency = document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value;
	             if(revisionCurrency==country){
	             	document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value;
	             	document.forms['accountLineForms'].elements['accountLine.revisionValueDate'].value = document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].value;
	             } 
				  calculateRevisionRateByContractRate('accountLine.revisionPayableContractRate');
				    try{
				    	<c:if test="${systemDefaultVatCalculation=='true'}">
					    	calculateREVVatAmt();	  
					    	calculateVatAmtRevision();    
				        </c:if>
				        }catch(e){}
		}
		 
		function  calculateRevisionRateByContractRate(target) {
			   <c:if test="${contractType}">
			        var country =document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value; 
			        if(country=='') {
			          alert("Please select Contract Currency ");
			          document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value=0;
			          document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value=1;
			        } else if(country!=''){
			        var estimatePayableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value
			        var estimatePayableContractRate=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value
			        estimatePayableContractRate=Math.round(estimatePayableContractRate*10000)/10000;
			        var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
			        var roundValue=Math.round(estimateRate*10000)/10000;
			        document.forms['accountLineForms'].elements['accountLine.revisionRate'].value=roundValue ; 
			        calRevisionLocalRate('NoCal',target); 
			        calRevisionPayableContractRateAmmount(target);
			        }   
			   </c:if>
			   }
		function calRevisionLocalRate(temp,target){ 
			          var country =document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value; 
			          if(country=='') {
			          alert("Please select currency "); 
			          document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=0; 
			        } else if(country!=''){  
			        	 	<c:if test="${contractType}"> 
					        if(target=='accountLine.revisionLocalAmount'){
					    		 var roundValue='0.00';
					    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
					    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
					    		 var baseRateVal=1;
					    	 	 var ATemp=0.00; 
					    	     var ETemp =0.00;	    		 
					    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
					    	         } 
					    	        if( bassisVal=="cwt" || bassisVal=="%age"){
					    	        	baseRateVal=100;
					    	         }  else if(bassisVal=="per 1000"){
					    	        	 baseRateVal=1000;
					    	       	 } else {
					    	       		baseRateVal=1;  	
					    		    }
					    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionLocalAmount'].value;
					    	        ATemp=(ATemp*baseRateVal)/quantityVal;
					    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
					    		    	ATemp=ATemp*100/estimateDeviationVal;
					    		    	ATemp=Math.round(ATemp*10000)/10000;
					    		    }
					         document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=ATemp; 
					    	 }else if(target=='accountLine.revisionRate'){
					    		 var roundValue='0.00';
					    	     var ETemp=0.00;
					    	     var ATemp=0.00;
					    	     ETemp = document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value;  
					    	     ATemp = document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
					    	     ATemp=ATemp*ETemp;
					    	     roundValue=Math.round(ATemp*10000)/10000;
					        	document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=roundValue; 
					    	 }else if(target=='accountLine.revisionExpense'){
					    		 var roundValue='0.00';
					    	     var ETemp=0.00;
					    	     var ATemp=0.00;
					    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
					    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
					    		 var baseRateVal=1;
					    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
					    	         } 
					    	        if( bassisVal=="cwt" || bassisVal=="%age"){
					    	        	baseRateVal=100;
					    	         }  else if(bassisVal=="per 1000"){
					    	        	 baseRateVal=1000;
					    	       	 } else {
					    	       		baseRateVal=1;  	
					    		    }
					    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value;  
					    	        ATemp=(ATemp*baseRateVal)/quantityVal; 
					    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
					    		    	ATemp=ATemp*100/estimateDeviationVal;
					    		    }
					    	     ETemp = document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value;  
					    	     ATemp=ATemp*ETemp;
					    	     roundValue=Math.round(ATemp*10000)/10000;
					        	document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=roundValue; 
					    	 }else if(target=='accountLine.revisionLocalRate'){
					    	 }else{
							 	var QTemp=0.00; 
							    var ATemp =0.00;
							    var FTemp =0.00;
							    var ETemp =0.00;
							    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value*1 ; 
							    FTemp =document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
							    FTemp=FTemp/ETemp;
							    ETemp= document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value*1 ;				    
							    FTemp=FTemp*ETemp;
					     roundValue=Math.round(FTemp*10000)/10000;
					     document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=roundValue;
					    	 } 
					        </c:if>
					        <c:if test="${!contractType}"> 
					        var recRateExchange=document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value
					        var recRate=document.forms['accountLineForms'].elements['accountLine.revisionRate'].value
					        var recCurrencyRate=recRate*recRateExchange;
					        var roundValue=Math.round(recCurrencyRate*10000)/10000;
					        document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value=roundValue ; 
							</c:if>
				         calculateRevisionLocalAmount('cal',target);
					        if(temp=='cal'){
					        	calRevisionPayableContractRateByBase('none');
					        }   }   } 
		function calRevisionPayableContractRateAmmount(target) { 
			   <c:if test="${contractType}"> 
			   if(target=='accountLine.revisionLocalRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    } 
				        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value;
				        ETemp=document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value;
				        ATemp=ATemp/ETemp;
				        ETemp=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
				     	roundValue=Math.round(ATemp*10000)/10000;
			    	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value=roundValue; 
						 }else if(target=='accountLine.revisionRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
					 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    }
				        ETemp=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value;
				        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}		        
				     	roundValue=Math.round(ATemp*10000)/10000;
			    	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value=roundValue; 
						 }else if(target=='accountLine.revisionPayableContractRateAmmount'){
						  }else{
								 var roundValue='0.00';
								 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
								 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;   		
								 var baseRateVal=1;
							 	 var ATemp=0.00; 
							     var ETemp =0.00;	    		 
								 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
									 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
							         } 
							        if( bassisVal=="cwt" || bassisVal=="%age"){
							        	baseRateVal=100;
							         }  else if(bassisVal=="per 1000"){
							        	 baseRateVal=1000;
							       	 } else {
							       		baseRateVal=1;  	
								    }
							        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
							        ATemp=(ATemp*quantityVal)/baseRateVal;
									var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
									if(revisionDeviation!='' && revisionDeviation>0){ 
										   ATemp=(ATemp*revisionDeviation)/100;
									}
							     	roundValue=Math.round(ATemp*10000)/10000;
						    	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value=roundValue;
					 	 } 
			             calculateRevisionExpenseByContractRate(target) ;     
			   </c:if>
			  		} 
		function calculateRevisionExpenseByContractRate(target){ 
		var revisionLocalAmount= document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRateAmmount'].value ;
		var revisionExchangeRate= document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value*1 ;
		var roundValue=0; 
			 if(revisionExchangeRate ==0) {
		      document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=0*1;
		  } else if(revisionExchangeRate >0) {
		    	 <c:if test="${contractType}"> 
		      	 if(target=='accountLine.revisionPayableContractRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionPayableContractRate'].value;
		    	        ETemp= document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value*1 ;
		    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
						var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*10000)/10000;
		         document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue;

		    	 }else if(target=='accountLine.revisionLocalRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionLocalRate'].value;
		    	        ETemp= document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value*1 ;
		    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
						var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*10000)/10000;
		         document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue; 
		    	 }else if(target=='accountLine.revisionExpense'){
		    	 }else if(target=='accountLine.revisionRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['accountLineForms'].elements['accountLine.basis'].value;
		    		 var quantityVal=document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value;
		    		 var estimateDeviationVal=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['accountLineForms'].elements['accountLine.revisionQuantity'].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['accountLineForms'].elements['accountLine.revisionRate'].value;
		    	        ATemp=((ATemp*quantityVal)/baseRateVal);
						var revisionDeviation=document.forms['accountLineForms'].elements['accountLine.revisionDeviation'].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*10000)/10000;
		         		document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue;
		    		 
		    	 }else{
		    	       var amount=revisionLocalAmount/revisionExchangeRate; 
		    	       roundValue=Math.round(amount*10000)/10000;
		    	       document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue ; 
		          	 }		       
		      	 </c:if>
		       <c:if test="${!contractType}"> 
			       var amount=revisionLocalAmount/revisionExchangeRate; 
			       roundValue=Math.round(amount*10000)/10000;
			       document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value=roundValue ; 
		        </c:if>
		  }
		   var revisionRevenue = document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value;
		   var revisionPassPercentageRound=0;
		   var revisionPassPercentage=0; 
		   revisionPassPercentage = (revisionRevenue/roundValue)*100;
			  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
			  if(document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.revisionExpense'].value == 0){
			   	document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value='';
			   }else{
			    document.forms['accountLineForms'].elements['accountLine.revisionPassPercentage'].value=revisionPassPercentageRound;
			   }  } 
		function findPayableContractCurrencyExchangeRate(){
		    var country =document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value; 
		    document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value=findExchangeRateGlobal(country)
		    document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].value=currentDateGlobal();  
		    var payCountry = document.forms['accountLineForms'].elements['accountLine.country'].value;
            if(payCountry==country){
            	document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value = document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value;
            	document.forms['accountLineForms'].elements['accountLine.valueDate'].value = document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].value;
            }
			calculateActualExpenseByContractRate('accountLine.payableContractRateAmmount');
		}
		  
		function  calculateActualExpenseByContractRate(target) {
			   <c:if test="${contractType}">
			        var country =document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value; 
			        if(country=='') {
			          alert("Please select Contract Currency ");
			          document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value=0;
			          document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value=1;
			        } else if(country!=''){
			        var payableContractExchangeRate=document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value
			        var payableContractRateAmmount=document.forms['accountLineForms'].elements['accountLine.payableContractRateAmmount'].value
			        payableContractRateAmmount= Math.round(payableContractRateAmmount*10000)/10000;
			        var actualExpense=payableContractRateAmmount/payableContractExchangeRate;
			        var roundValue=Math.round(actualExpense*10000)/10000;
			        document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=roundValue ; 
			        convertLocalAmount(target);
			        }   
			   </c:if>
			   }
		function findExchangeRateGlobal(currency){
			 var rec='1';
				<c:forEach var="entry" items="${currencyExchangeRate}">
					if('${entry.key}'==currency.trim()){
						rec='${entry.value}';
					}
				</c:forEach>
				return rec;
		}
		function findExchangeContractRateByCharges(temp){
		    <c:if test="${contractType}">
		    var country=document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value;
		    var country1='';
		    if(temp=='estimate'){
		    	country =document.forms['accountLineForms'].elements['accountLine.estimateContractCurrency'].value;
		    	country1 =document.forms['accountLineForms'].elements['accountLine.estSellCurrency'].value;
		    }
		    if(temp=='revision'){
		    	country =document.forms['accountLineForms'].elements['accountLine.revisionContractCurrency'].value;
		    	country1 =document.forms['accountLineForms'].elements['accountLine.revisionSellCurrency'].value;
		    }
		    if(temp=='receivable'){
		    	country =document.forms['accountLineForms'].elements['accountLine.contractCurrency'].value;
		    	country1 =document.forms['accountLineForms'].elements['accountLine.recRateCurrency'].value;
		    }
		   
            if(temp=='estimate'){
            	document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value=findExchangeRateGlobal(country);
            	document.forms['accountLineForms'].elements['accountLine.estSellExchangeRate'].value=findExchangeRateGlobal(country1);
            }else if(temp=='revision'){
            	document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value=findExchangeRateGlobal(country);
            	document.forms['accountLineForms'].elements['accountLine.revisionSellExchangeRate'].value=findExchangeRateGlobal(country1);
            }else if(temp=='receivable'){ 
            	document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value=findExchangeRateGlobal(country);
            	document.forms['accountLineForms'].elements['accountLine.recRateExchange'].value=findExchangeRateGlobal(country1);
            }else{  
	              document.forms['accountLineForms'].elements['accountLine.contractExchangeRate'].value=findExchangeRateGlobal(country);
	              document.forms['accountLineForms'].elements['accountLine.estimateContractExchangeRate'].value=findExchangeRateGlobal(country);
	              document.forms['accountLineForms'].elements['accountLine.revisionContractExchangeRate'].value=findExchangeRateGlobal(country);
            } 
		            
			  if(temp=='estimate'){
				  	document.forms['accountLineForms'].elements['accountLine.estimateContractValueDate'].value=currentDateGlobal();
				  	document.forms['accountLineForms'].elements['accountLine.estSellValueDate'].value=currentDateGlobal();
              }else if(temp=='revision'){
                	document.forms['accountLineForms'].elements['accountLine.revisionContractValueDate'].value=currentDateGlobal();
                	document.forms['accountLineForms'].elements['accountLine.revisionSellValueDate'].value=currentDateGlobal();
              }else if(temp=='receivable'){ 
                	document.forms['accountLineForms'].elements['accountLine.contractValueDate'].value=currentDateGlobal();
                	document.forms['accountLineForms'].elements['accountLine.racValueDate'].value=currentDateGlobal();
              }else{
				  	document.forms['accountLineForms'].elements['accountLine.contractValueDate'].value=currentDateGlobal();
				  	document.forms['accountLineForms'].elements['accountLine.estimateContractValueDate'].value=currentDateGlobal(); 
				  	document.forms['accountLineForms'].elements['accountLine.revisionContractValueDate'].value=currentDateGlobal();
				  	calculateRecRateByContractRate('none','','','');
				  	calculateEstimateSellRateByContractRate('none','','','');
				  	calculateRevisionSellRateByContractRate('none','','','');
              }  
		</c:if>	
		}     
		function findPayableContractCurrencyExchangeRateByCharges(temp){
			var country1='';
		    var country =document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value;
		    if(temp=='payable'){
		        country =document.forms['accountLineForms'].elements['accountLine.payableContractCurrency'].value;
		        country1 =document.forms['accountLineForms'].elements['accountLine.country'].value;
		     }
		    if(temp=='estimate'){
		        country =document.forms['accountLineForms'].elements['accountLine.estimatePayableContractCurrency'].value;
		        country1 =document.forms['accountLineForms'].elements['accountLine.estCurrency'].value;
		     }
	        if(temp=='revision'){
	        	country =document.forms['accountLineForms'].elements['accountLine.revisionPayableContractCurrency'].value;
	        	country1 =document.forms['accountLineForms'].elements['accountLine.revisionCurrency'].value;
	        }
		   
           	if(temp=='estimate'){
               	document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value=findExchangeRateGlobal(country);
               	document.forms['accountLineForms'].elements['accountLine.estExchangeRate'].value=findExchangeRateGlobal(country1);
             }else if(temp=='revision'){
               	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value=findExchangeRateGlobal(country);
               	document.forms['accountLineForms'].elements['accountLine.revisionExchangeRate'].value=findExchangeRateGlobal(country1);
             }else if(temp=='payable'){ 
               	document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value=findExchangeRateGlobal(country);
               	document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value=findExchangeRateGlobal(country1);
             }else{  
	              document.forms['accountLineForms'].elements['accountLine.payableContractExchangeRate'].value=findExchangeRateGlobal(country); 
	              document.forms['accountLineForms'].elements['accountLine.estimatePayableContractExchangeRate'].value=findExchangeRateGlobal(country);
	              document.forms['accountLineForms'].elements['accountLine.revisionPayableContractExchangeRate'].value=findExchangeRateGlobal(country);
		     }
		               
			  if(temp=='estimate'){
				  	document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].value=currentDateGlobal();
			  		document.forms['accountLineForms'].elements['accountLine.estValueDate'].value=currentDateGlobal();
              }else if(temp=='revision'){
                	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].value=currentDateGlobal();
                	document.forms['accountLineForms'].elements['accountLine.revisionValueDate'].value=currentDateGlobal();
              }else if(temp=='payable'){ 
                	document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].value=currentDateGlobal();
                	document.forms['accountLineForms'].elements['accountLine.valueDate'].value=currentDateGlobal();
              }else{
				  	document.forms['accountLineForms'].elements['accountLine.payableContractValueDate'].value=currentDateGlobal(); 
				  	document.forms['accountLineForms'].elements['accountLine.estimatePayableContractValueDate'].value=currentDateGlobal(); 
				  	document.forms['accountLineForms'].elements['accountLine.revisionPayableContractValueDate'].value=currentDateGlobal();
	              	calculateEstimateRateByContractRate('none');
	              	calculateRevisionRateByContractRate('none');
	              	calculateActualExpenseByContractRate('none');
             }   }   
		function copyDMMAmount(){
			<c:if test="${(!(trackingStatus.accNetworkGroup)) && (billingDMMContractType) && (trackingStatus.soNetworkGroup) && accountLine.createdBy == 'Networking' }">
			 </c:if>
		}
		function fillDefaultInvoiceNumber(){ 
			<configByCorp:fieldVisibility componentId="component.field.Alternative.SetRegistrationNumber">
			var invoiceNumber=document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value;
			var actgCode=document.forms['accountLineForms'].elements['accountLine.actgCode'].value;
			var payGl=document.forms['accountLineForms'].elements['accountLine.payGl'].value;
			payGl=payGl.trim();
			invoiceNumber=invoiceNumber.trim();
			actgCode=actgCode.trim();
			if((invoiceNumber=='')&&(actgCode!='')&&(payGl!='')){
			if(document.forms['accountLineForms'].elements['soRegistrationNumber'].value!=''){
			document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value = document.forms['accountLineForms'].elements['soRegistrationNumber'].value;
			document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value = document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value.trim();
			}else{	
			document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value = document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
			}
			document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='A';
			var mydate=new Date();
			var daym;
			var year=mydate.getFullYear()
			var y=""+year;
			if (year < 1000)
			year+=1900
			var day=mydate.getDay()
			var month=mydate.getMonth()+1
			if(month == 1)month="Jan";
			if(month == 2)month="Feb";
			if(month == 3)month="Mar";
			if(month == 4)month="Apr";
			if(month == 5)month="May";
			if(month == 6)month="Jun";
			if(month == 7)month="Jul";
			if(month == 8)month="Aug";
			if(month == 9)month="Sep";
			if(month == 10)month="Oct";
			if(month == 11)month="Nov";
			if(month == 12)month="Dec"; 
			var daym=mydate.getDate()
			if (daym<10)
			daym="0"+daym
			var datam = daym+"-"+month+"-"+y.substring(2,4);
			document.forms['accountLineForms'].elements['accountLine.receivedDate'].value=datam;
			document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value=datam; 
			}
			</configByCorp:fieldVisibility>
			
			<configByCorp:fieldVisibility componentId="component.field.PayableInvoiceNumber.SetServiceOrderId">
			var vendorCodeOOCheck=document.forms['accountLineForms'].elements['vendorCodeOwneroperator'].value;
			var vendorCode=document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
			vendorCode=vendorCode.trim();
			if(vendorCodeOOCheck!=undefined && vendorCodeOOCheck!=null && vendorCodeOOCheck!='' && vendorCodeOOCheck=='Y' && vendorCode!='${accountLine.vendorCode}'){
					
				    var invoiceNumber=document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value;
					var actgCode=document.forms['accountLineForms'].elements['accountLine.actgCode'].value;
					var payGl=document.forms['accountLineForms'].elements['accountLine.payGl'].value;
					payGl=payGl.trim();
					invoiceNumber=invoiceNumber.trim();
					actgCode=actgCode.trim();
					if((actgCode!='')&&(payGl!='')){ 
					document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value = document.forms['accountLineForms'].elements['sid'].value+""+document.forms['accountLineForms'].elements['accountLine.vendorCode'].value; 
					document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value = document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value.trim();
					document.forms['accountLineForms'].elements['accountLine.payingStatus'].value ='A';
					var mydate=new Date();
					var daym;
					var year=mydate.getFullYear()
					var y=""+year;
					if (year < 1000)
					year+=1900
					var day=mydate.getDay()
					var month=mydate.getMonth()+1
					if(month == 1)month="Jan";
					if(month == 2)month="Feb";
					if(month == 3)month="Mar";
					if(month == 4)month="Apr";
					if(month == 5)month="May";
					if(month == 6)month="Jun";
					if(month == 7)month="Jul";
					if(month == 8)month="Aug";
					if(month == 9)month="Sep";
					if(month == 10)month="Oct";
					if(month == 11)month="Nov";
					if(month == 12)month="Dec"; 
					var daym=mydate.getDate()
					if (daym<10)
					daym="0"+daym
					var datam = daym+"-"+month+"-"+y.substring(2,4);
					document.forms['accountLineForms'].elements['accountLine.receivedDate'].value=datam;
					document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value=datam; 
					}
			}
			</configByCorp:fieldVisibility>
		}
		function driverCommission(){
			var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
			if(chargeCheck=='' || chargeCheck==null){
			var actualRevenue="0";
			var distributionAmount="0";
			var estimateRevenueAmount="0";
			var driverCommissionSetUp='${driverCommissionSetUp}';
			var revisionRevenueAmount="0"; 
			var category=document.forms['accountLineForms'].elements['accountLine.category'].value;
			var vendorCode=document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
			var chargeCode=document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
			 actualRevenue=document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
			 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		     distributionAmount=document.forms['accountLineForms'].elements['accountLine.distributionAmount'].value;
		     </c:if>
		     estimateRevenueAmount=document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value;
		     revisionRevenueAmount=document.forms['accountLineForms'].elements['accountLine.revisionRevenueAmount'].value; 
		     var sid='<%=request.getParameter("sid")%>';
			if(category!=null && category!='' && vendorCode!=null && vendorCode!='' && chargeCode!=null && chargeCode!='' && (driverCommissionSetUp=='true' || driverCommissionSetUp==true)){
				 var url="driverCommissionAjax.html?ajax=1&decorator=simple&popup=true&sid="+sid+"&actualRevenue="+actualRevenue+"&accountLineDistribution="+distributionAmount+"&accountEstimateRevenueAmount="+estimateRevenueAmount+"&accountRevisionRevenueAmount="+revisionRevenueAmount+"&chargeCode="+chargeCode+"&vendorCode="+vendorCode;
				http100.open("GET", url, true);
			    http100.onreadystatechange = handleHttpResponseDriverCommission;
			    http100.send(null);
				}} }
		function handleHttpResponseDriverCommission() { 
			if (http100.readyState == 4) {
				var results = http100.responseText
				results = results.trim();
			    if(results.length>1 && results!='0.00')
		        {
			    	results=Math.round(results*10000)/10000;
			    	document.forms['accountLineForms'].elements['accountLine.actualExpense'].value=results;
				    document.forms['accountLineForms'].elements['accountLine.localAmount'].value=results;
		        }    } } 
		function disableDriverCompanyDivision(){   
		<c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
		<c:if test="${accountLine.driverCompanyDivision}">
		if(document.forms['accountLineForms'].elements['accountLine.companyDivision'].value.trim()!=''){
			document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=true;
		}else{
			document.forms['accountLineForms'].elements['accountLine.companyDivision'].disabled=false;
		}
		</c:if>
		</c:if>
		}
		function driverValueCompany(){
			var ae= document.forms['accountLineForms'].elements['accountLine.actualExpense'].value;
			var al=document.forms['accountLineForms'].elements['accountLine.localAmount'].value;
			if(ae=='0.00' && al=='0.00'){
				driverCommission()
			} }
		function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
			document.getElementById(autocompleteDivId).style.display = "none";
			if(document.getElementById(paertnerCodeId).value==''){
				document.getElementById(partnerNameId).value="";	
			} }
		function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
			lastName=lastName.replace("~","'");
			document.getElementById(partnerNameId).value=lastName;
			document.getElementById(paertnerCodeId).value=partnercode;
			document.getElementById(autocompleteDivId).style.display = "none";	
			if(paertnerCodeId=='accountLinevendorCodeId'){
				checkPurchaseInvoiceProcessing('','VEN');
				driverCommission();
				changeStatus();
			} }
		function findAgent(position,agentType) {
			var partnerCode = "";
			var shipNumberA="";
			var originalCorpID = document.forms['accountLineForms'].elements['accountLine.corpID'].value
			partnerCode = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value; 
			var url="customerAddress.html?ajax=1&decorator=simple&popup=true&accountLineBillingParam=YES&partnerCode=" + encodeURI(partnerCode)+"&originalCorpID=" + encodeURI(originalCorpID);
			ajax_showTooltip(url,position); 
			return false
		} 
		function AddDocumentDetails(){ 
		    var vendorId = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
			if(vendorId != ''){   
				var shipnumber='${serviceOrder.shipNumber}';
		    var url="myfileDocumentList.html?ajax=1&decorator=simple&popup=true&shipNumber="+shipnumber+"&partnerCode=" + encodeURI(vendorId);
				http34.open("GET", url, true);
				http34.onreadystatechange = handleHttpResponse34;
				http34.send(null);
		    }  }
		function handleHttpResponse34(){
			 var results = http34.responseText
		    results = results.trim();
			var res = results.split("#");
		   var targetElement=document.forms['accountLineForms'].elements['accountLine.myFileFileName'];
			     targetElement.length= res.length;
				 for(i=0; i<res.length; i++){
			     		if(res[i] == ''){
			     		}else{
			     		document.forms['accountLineForms'].elements['accountLine.myFileFileName'].options[i].text =res[i].split('~')[1];
			     		document.forms['accountLineForms'].elements['accountLine.myFileFileName'].options[i].value=res[i].split('~')[0]; 
				         }  } 
				 document.forms['accountLineForms'].elements['accountLine.myFileFileName'].value='${accountLine.myFileFileName}';
				 }
		function detailsView(){
			var res=document.forms['accountLineForms'].elements['accountLine.myFileFileName'].value;
			if(res!=''){		
			res = res.split(":");
				if(res[0]!=null && res[0].trim()!=''){		
					//openWindow('ImageServletAction.html?id='+res[0].trim()+'&decorator=popup&popup=true',900,600);
					var acId = document.forms['accountLineForms'].elements['id'].value;
					var sid = document.forms['accountLineForms'].elements['sid'].value;
					var checkLHF = document.forms['accountLineForms'].elements['checkLHF'].value;
					var myFileJspName = "editAccountLine";
					var myFileForVal = "AC";
					var url="ImageServletAction.html?id="+res[0].trim()+"&myFileForVal="+myFileForVal+"&acId="+acId+"&sid="+sid+"&checkLHF="+checkLHF+"&myFileJspName="+myFileJspName+"";
					location.href=url;
				}
			}else{
				alert("Please Select Document....");
			} }
		var fileVal = '${resultType}';
		if ((fileVal!=null && fileVal!='') && (fileVal=='errorNoFile')){
			alert('This File is Temporarily Unavailable.')
			
			var sid =  "<%=request.getAttribute("sid")%>";
			var acId = "<%=request.getAttribute("id")%>";
			var checkLHF = "<%=request.getAttribute("checkLHF")%>";
			var replaceURL = "editAccountLine.html?sid="+sid+"&id="+acId+"&checkLHF="+checkLHF+"";
			window.location.replace(replaceURL);
		}
		
		var populateCostElementDatahttp=getHTTPObject77(); 
		function populateCostElementData(){
			<c:if test="${costElementFlag}">
			var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
			var billingContract = '${billing.contract}';
		    var sid=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
			var url="populateCostElementDataAjax.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(billingContract)+"&chargeCode="+encodeURIComponent(chargeCode)+"&soId="+sid;
			populateCostElementDatahttp.open("GET", url, true);
			populateCostElementDatahttp.onreadystatechange = handleHttpResponsePopulateCostElementData;
			populateCostElementDatahttp.send(null);
			</c:if>
		}
		function handleHttpResponsePopulateCostElementData() { 
			if (populateCostElementDatahttp.readyState == 4) {
				var results = populateCostElementDatahttp.responseText
				results = results.trim();
			    if(results.length>1){
			    	document.forms['accountLineForms'].elements['accountLine.accountLineCostElement'].value=results.split("~")[0];
				    document.forms['accountLineForms'].elements['accountLine.accountLineScostElementDescription'].value=results.split("~")[1];
			    }  } } 
		function findRollUpInvoiceValFromChargeAjax(){
			var chargeCheck=document.forms['accountLineForms'].elements['chargeCodeValidationVal'].value;
			if(chargeCheck=='' || chargeCheck==null){
			<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
				var contract = '${billing.contract}';
				var chargeCode = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
				if(contract!='' && chargeCode!=''){
				var url = "findRollUpInvoiceValFromChargeAjax.html?ajax=1&decorator=simple&popup=true&contract="+contract+"&chargeCode="+chargeCode;
				  http233.open("GET", url, true); 
				  http233.onreadystatechange = handleHttpResponse233;
				  http233.send(null);
				}
			</configByCorp:fieldVisibility>}
		}
		function handleHttpResponse233(){
			if (http233.readyState == 4){
		            var results = http233.responseText
		            results = results.trim();
		            if(results!='' && results=='Y'){
		            	document.forms['accountLineForms'].elements['accountLine.rollUpInInvoice'].checked=true;
		            }else{
		            	document.forms['accountLineForms'].elements['accountLine.rollUpInInvoice'].checked=false;          
		            } } }
		var http233 = getHTTPObject();
		function checkDescriptionfromCharges(){
			var val = document.forms['accountLineForms'].elements['accountLine.category'].value;
			var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
			var billingContract = '${billing.contract}';
			var jobtype = document.forms['accountLineForms'].elements['serviceOrder.Job'].value; 
		    var routing = document.forms['accountLineForms'].elements['serviceOrder.routing'].value; 
		    var sid=document.forms['accountLineForms'].elements['serviceOrder.id'].value;
		    var soCompanyDivision=document.forms['accountLineForms'].elements['serviceOrder.companyDivision'].value;
			chargeCode=chargeCode.trim();
			if(chargeCode!='') {
			if(billingContract=='' || billingContract==' '){
			alert("There is no pricing contract in billing: Please select.");
			document.forms['accountLineForms'].elements['accountLine.chargeCode'].value = "";
			}else{
			 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(billingContract)+"&chargeCode="+encodeURIComponent(chargeCode)+"&jobType="+encodeURIComponent(jobtype)+"&routing="+encodeURIComponent(routing)+"&soId="+sid+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
		     http5.open("GET", url, true);
		     http5.onreadystatechange =function(){ handleCheckDescriptionfromCharges("NoInternal");};
		     http5.send(null);
			}   
		}
			}
		
		function handleCheckDescriptionfromCharges(temp){
			 if (http5.readyState == 4){
	                var results = http5.responseText
	                results = results.trim(); 
	                results = results.replace('[','');
	                results=results.replace(']',''); 
	                var res = results.split("#"); 
	                var description1 = "";
	                var wordingTemp="";
	                 if(results.length>6){
	                	 if(results.indexOf("Login")==-1)
	            	    {      
		                try{
		                    if(temp=="NoInternal"){ 
		                    wordingTemp=res[14];
		                    }
		                    }catch(e){} 
		                description1 = document.forms['accountLineForms'].elements['accountLine.description'].value;
		                if(description1==''){
		                	if(wordingTemp==''){
		                		if(res[4]!=undefined || res[4]!="undefined"){
		                			document.forms['accountLineForms'].elements['accountLine.description'].value=res[4];	
	                			}else{
	                				document.forms['accountLineForms'].elements['accountLine.description'].value="";
	                			}
			                }else{
			                	document.forms['accountLineForms'].elements['accountLine.description'].value=wordingTemp;
			                }  }
	                	
	               }else{
			    		alert("Session has been ended, Login Again");
			    		window.location=document.location.href;  
	               } 
	              }else{
	                  
					}   progressBarAutoSave('0');   }   }

</script>