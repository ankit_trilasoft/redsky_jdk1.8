
package com.trilasoft.app.dao.hibernate;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;  
import java.math.BigDecimal; 

import org.appfuse.dao.hibernate.GenericDaoHibernate;   

import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.ItemsJEquip;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerQuote;   
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.dao.PartnerQuoteDao;   


public class PartnerQuoteDaoHibernate extends GenericDaoHibernate<PartnerQuote, Long> implements PartnerQuoteDao {   
  private String newstatus;
private List partnerQuotes;
	public PartnerQuoteDaoHibernate() {   
        super(PartnerQuote.class);   
    } 
    public List findMaximumId() {
    	return getHibernateTemplate().find("select max(id) from PartnerQuote");
    	
    }
    public int updateQuoteStatus(String mail, String reqSO )

    	{
    		newstatus = "Requested";
    		
    		return getHibernateTemplate().bulkUpdate("update PartnerQuote set requestedSO='" + reqSO + "',quoteStatus='" + newstatus + "',sentDate=Now() where vendorMail=?", mail); 
    	}	
    	
    public int updateQuoteReminder(String mail )

	{
		newstatus = "Processing";
		
		return getHibernateTemplate().bulkUpdate("update PartnerQuote set reminderDate=Now() where vendorMail=?", mail); 
	}


    public List findPartnerCode(String partnerCode){
    	//System.out.println(partnerCode);
    	return getHibernateTemplate().find("select ship from PartnerQuote where quote=?", partnerCode);
    	
    }
    
// 	@@@@@@@@@@@@@@@@@@@@@ By Madhu - For Quation Management @@@@@@@@@@@@@@@@@@@@@@@@
    public List findByQuoteType(String quoteType, String sequenceNumber){
   
    	if(quoteType.equals("")){
    		//System.out.println("\n\n quoteType is :\t"+quoteType);
    		partnerQuotes = getHibernateTemplate().find("from PartnerQuote where quoteType<>'Origin' AND quoteType<>'Freight' AND quoteType<>'Destination' AND quoteType<>'Origin+Freight' and sequenceNumber=?", sequenceNumber);
    	}else{
		    		if(quoteType.equals("OA")||quoteType.equals("ORF")){
		    			//System.out.println("\n\n quoteType is :\t"+quoteType);
		    			partnerQuotes = getHibernateTemplate().find("from PartnerQuote where (quoteType='Origin' OR quoteType='Origin+Freight' ) and sequenceNumber=?", sequenceNumber);
			    	}
		    		if(quoteType.equals("FW")||quoteType.equals("ORF")){
		    			//System.out.println("\n\n quoteType is :\t"+quoteType);
		    			partnerQuotes = getHibernateTemplate().find("from PartnerQuote where (quoteType='Freight' OR quoteType='Origin+Freight' ) and sequenceNumber=?", sequenceNumber);
			    	}
		    		if(quoteType.equals("DA")){
		    			//System.out.println("\n\n quoteType is :\t"+quoteType);
		    			partnerQuotes = getHibernateTemplate().find("from PartnerQuote where quoteType='Destination' and sequenceNumber=?", sequenceNumber);
			    	}
    		}
    	//System.out.println("\n\n partnerQuotes is :\t"+partnerQuotes);
    	return partnerQuotes;
    }

    
    
    public List findBySequenceNumber(String sequenceNumber){
    	return getHibernateTemplate().find("from PartnerQuote where sequenceNumber=?", sequenceNumber);
    	
    }
	public List <PartnerQuote> findByStatus(String quoteStatus,String sessionCorpID, String vendorCodeSet) {
		//System.out.println("\n\n Quotation quoteStatus="+quoteStatus);
		List status = new ArrayList();
		if(vendorCodeSet!=null && (!(vendorCodeSet.trim().equals("")))){
			status = getHibernateTemplate().find("from PartnerQuote where quoteStatus<>'"+quoteStatus+"' and vendorCode in ("+vendorCodeSet+") and corpID= '"+sessionCorpID+"'");	
		}else{
		   status = getHibernateTemplate().find("from PartnerQuote where quoteStatus<>'"+quoteStatus+"' and corpID= '"+sessionCorpID+"'");
		}
		return status;
	}
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@by Raj @@@@@@@@@@@@@@@@@@@@@
	public List <PartnerQuote> findByStatusSubmitted(String quoteStatus,String searchByCode,String searchByName,String shipNumber) {
		//System.out.println("\n\n Quotation quoteStatus="+quoteStatus);
		String query ="from PartnerQuote where quoteStatus='"+quoteStatus+"' AND vendorCode like '"+searchByCode+"%' AND vendorName like '"+searchByName+"%' AND requestedSO like '%"+shipNumber+"%'";
		//System.out.println(query);
		List status = getHibernateTemplate().find(query);
		return status;
	}
	
// 	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
	public List findByVendorEmail(String email, String seqNum){
		//System.out.println(email);
        
		return getHibernateTemplate().find("from PartnerQuote where sequenceNumber = '" +seqNum +"' AND vendorMail=?", email);
	}
	public List getGeneralListVendor(String searchByCode,String searchByName,String searchByCountryCode,String searchByState,String searchByCity){
		List generalVenDor = getHibernateTemplate().find("from Partner where isVendor=true AND (lastName like '" + replaceWord(searchByName, "'", "''") + "%' OR  firstName like '" + replaceWord(searchByName, "'", "''") + "%') AND partnerCode like '" + searchByCode +"%' AND billingCountryCode like '" +searchByCountryCode+"%' AND billingState like '" +searchByState+"%' AND billingCity like '" +searchByCity+"%'" );
		//List generalVenDor = getHibernateTemplate().find("from Partner where isVendor=true AND partnerCode regexp '^[" + searchByCode +"]' AND billingCountryCode regexp '^[" +searchByCountryCode+"]' AND billingState regexp '^[" +searchByState+"]' AND billingCity regexp '^[" +searchByCity+"]'" );
		return generalVenDor;
	}
	static String replaceWord(String original, String find, String replacement)
	{
		int i = original.indexOf(find);
	    if (i < 0) {
	        return original;  // return original if 'find' is not in it.
	    }
	  
	    String partBefore = original.substring(0, i);
	    String partAfter  = original.substring(i + find.length());
	  
	    return partBefore + replacement + partAfter;

	}
	public class QuotationServiceOrder{
		
		private Object shipNumber;
		private Object commodity;
		private Object mode;
		private Object estimateGrossWeight;
		private Object equipment;
		private Object estimateCubicFeet;
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getCommodity() {
			return commodity;
		}
		public void setCommodity(Object commodity) {
			this.commodity = commodity;
		}
		public Object getMode() {
			return mode;
		}
		public void setMode(Object mode) {
			this.mode = mode;
		}
		public Object getEstimateGrossWeight() {
			return estimateGrossWeight;
		}
		public void setEstimateGrossWeight(Object estimateGrossWeight) {
			this.estimateGrossWeight = estimateGrossWeight;
		}
		public Object getEquipment() {
			return equipment;
		}
		public void setEquipment(Object equipment) {
			this.equipment = equipment;
		}
		public Object getEstimateCubicFeet() {
			return estimateCubicFeet;
		}
		public void setEstimateCubicFeet(Object estimateCubicFeet) {
			this.estimateCubicFeet = estimateCubicFeet;
		}
	}
	public List findServiceOrders(String requestedSO,String sessionCorpID) {
		List listId =new ArrayList();
		List list = new ArrayList();
		List <Object> quotationServiceOrderlist = new ArrayList();
		try{
		String reqSO =  requestedSO.replaceAll(",", "','");
		listId = getHibernateTemplate().find("select id from ServiceOrder where shipNumber in ('"+reqSO+"') and corpID='"+sessionCorpID+"'");
		Iterator idIterator =listId.iterator();
        String agentId="";
        while(idIterator.hasNext()){
        	if(agentId.equals("")){
        		agentId	=idIterator.next().toString();
        	}else{
        	agentId=agentId+","+idIterator.next().toString();
        	}
        }
        list= this .getSession() .createSQLQuery("select s.shipNumber,s.commodity,s.Mode,m.estimateGrossWeight,m.equipment,m.estimateCubicFeet  from serviceorder s , miscellaneous m  where s.id=m.id and s.id in ("+agentId+") and s.corpID='"+sessionCorpID+"'").list();
        Iterator it = list.iterator();
		QuotationServiceOrder quotationServiceOrder = null;
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			quotationServiceOrder = new QuotationServiceOrder();
			quotationServiceOrder.setShipNumber(row[0]);
			quotationServiceOrder.setCommodity(row[1]);
			quotationServiceOrder.setMode(row[2]);
			quotationServiceOrder.setEstimateGrossWeight(row[3]);
			quotationServiceOrder.setEquipment(row[4]);
			quotationServiceOrder.setEstimateCubicFeet(row[5]);
			quotationServiceOrderlist.add(quotationServiceOrder);
		}
		}catch(Exception e){
			
		}
		return quotationServiceOrderlist;
	}
	public List vendorNamenEmail(String partnerCode){
		return getHibernateTemplate().find("select lastName,billingEmail from Partner where partnerCode=?",partnerCode);
	}
	public List findSoList(String sequenceNumber, String corpId) {
		return getHibernateTemplate().find("select shipNumber from ServiceOrder where sequenceNumber='"+sequenceNumber+"' and corpID='"+corpId+"'");
		
	}
	public List findSoDetail(String shipNumber, String corpId) {
		
		return getHibernateTemplate().find("select id from ServiceOrder where (commodity is null or commodity='' or mode is null or mode ='' or routing is null or routing ='' or estimateGrossWeight is null  or estimatedNetWeight is null  or unit1 is null or unit1 ='' or estimateCubicFeet is null ) and shipNumber='"+shipNumber+"' and corpID='"+corpId+"'");
	}
}  
