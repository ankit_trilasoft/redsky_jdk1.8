/**
 *  @Class Name	 ServiceOrderDashboard
 *  @author      Surya 
 *  @Date        14-Mar-2014
 */
package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
@Entity
@Table(name = "sodashboard")
public class ServiceOrderDashboard extends BaseObject{	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private String role;
		private String job;	
		private Long id;
		private Long serviceOrderId;
		private String shipNumber;
		private Date actualSurvey;
		private BigDecimal estweight;
		private BigDecimal estvolume;
		private BigDecimal actVolume;		
		private Date actualLoading;
		private BigDecimal actualweight;
		private String driver;
		private String truck;		
		private Date actualDelivery;
		private String claims;
		private String  qc;		
		private String lastName;
		private String firstName;
		private String corpID;
		private Date estimateLoading;
		private Date estimateDelivery;
		private Date estimateSurvey;
		private String customFlag;
		private String customDate;
		
		
		@Override
		public String toString() {
			return new ToStringBuilder(this).append("role", role)
					.append("job", job).append("id", id)
					.append("serviceOrderId", serviceOrderId)
					.append("shipNumber", shipNumber)
					.append("actualSurvey", actualSurvey)
					.append("estweight", estweight).append("estvolume", estvolume)
					.append("actVolume", actVolume)
					.append("actualLoading", actualLoading)
					.append("actualweight", actualweight).append("driver", driver)
					.append("truck", truck)
					.append("actualDelivery", actualDelivery)
					.append("claims", claims).append("qc", qc)
					.append("lastName", lastName).append("firstName", firstName)
					.append("corpID", corpID)
					.append("estimateLoading", estimateLoading)
					.append("estimateDelivery", estimateDelivery)
					.append("estimateSurvey", estimateSurvey)
					.append("customFlag", customFlag)
					.append("customDate", customDate).toString();
		}
		@Override
		public boolean equals(final Object other) {
			if (!(other instanceof ServiceOrderDashboard))
				return false;
			ServiceOrderDashboard castOther = (ServiceOrderDashboard) other;
			return new EqualsBuilder().append(role, castOther.role)
					.append(job, castOther.job).append(id, castOther.id)
					.append(serviceOrderId, castOther.serviceOrderId)
					.append(shipNumber, castOther.shipNumber)
					.append(actualSurvey, castOther.actualSurvey)
					.append(estweight, castOther.estweight)
					.append(estvolume, castOther.estvolume)
					.append(actVolume, castOther.actVolume)
					.append(actualLoading, castOther.actualLoading)
					.append(actualweight, castOther.actualweight)
					.append(driver, castOther.driver)
					.append(truck, castOther.truck)
					.append(actualDelivery, castOther.actualDelivery)
					.append(claims, castOther.claims).append(qc, castOther.qc)
					.append(lastName, castOther.lastName)
					.append(firstName, castOther.firstName)
					.append(corpID, castOther.corpID)
					.append(estimateLoading, castOther.estimateLoading)
					.append(estimateDelivery, castOther.estimateDelivery)
					.append(estimateSurvey, castOther.estimateSurvey)
					.append(customFlag, castOther.customFlag)
					.append(customDate, castOther.customDate).isEquals();
		}
		@Override
		public int hashCode() {
			return new HashCodeBuilder().append(role).append(job).append(id)
					.append(serviceOrderId).append(shipNumber).append(actualSurvey)
					.append(estweight).append(estvolume).append(actVolume)
					.append(actualLoading).append(actualweight).append(driver)
					.append(truck).append(actualDelivery).append(claims).append(qc)
					.append(lastName).append(firstName).append(corpID)
					.append(estimateLoading).append(estimateDelivery)
					.append(estimateSurvey).append(customFlag).append(customDate)
					.toHashCode();
		}
		
		@Column
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)	
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		@Column
		public String getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(String shipNumber) {
			this.shipNumber = shipNumber;
		}
		
		public BigDecimal getActualweight() {
			return actualweight;
		}
		public void setActualweight(BigDecimal actualweight) {
			this.actualweight = actualweight;
		}
		@Column
		public String getDriver() {
			return driver;
		}
		public void setDriver(String driver) {
			this.driver = driver;
		}
		@Column
		public String getTruck() {
			return truck;
		}
		public void setTruck(String truck) {
			this.truck = truck;
		}
		
		
		public String getClaims() {
			return claims;
		}
		public void setClaims(String claims) {
			this.claims = claims;
		}
		@Column
		public String getQc() {
			return qc;
		}
		public void setQc(String qc) {
			this.qc = qc;
		}	
		
		
		@Column
		public String getCorpID() {
			return corpID;
		}
		public void setCorpID(String corpID) {
			this.corpID = corpID;
		}
		@Column
		public BigDecimal getEstvolume() {
			return estvolume;
		}
		public void setEstvolume(BigDecimal estvolume) {
			this.estvolume = estvolume;
		}
		@Column
		public BigDecimal getActVolume() {
			return actVolume;
		}
		public void setActVolume(BigDecimal actVolume) {
			this.actVolume = actVolume;
		}
		@Column
		public Long getServiceOrderId() {
			return serviceOrderId;
		}
		public void setServiceOrderId(Long serviceOrderId) {
			this.serviceOrderId = serviceOrderId;
		}
		@Column
		public String getCustomFlag() {
			return customFlag;
		}
		public void setCustomFlag(String customFlag) {
			this.customFlag = customFlag;
		}
		@Column
		public String getCustomDate() {
			return customDate;
		}
		public void setCustomDate(String customDate) {
			this.customDate = customDate;
		}
		@Column
		public Date getActualSurvey() {
			return actualSurvey;
		}
		public void setActualSurvey(Date actualSurvey) {
			this.actualSurvey = actualSurvey;
		}
		@Column
		public Date getActualLoading() {
			return actualLoading;
		}
		public void setActualLoading(Date actualLoading) {
			this.actualLoading = actualLoading;
		}
		@Column
		public Date getActualDelivery() {
			return actualDelivery;
		}
		public void setActualDelivery(Date actualDelivery) {
			this.actualDelivery = actualDelivery;
		}
		@Column
		public Date getEstimateLoading() {
			return estimateLoading;
		}
		public void setEstimateLoading(Date estimateLoading) {
			this.estimateLoading = estimateLoading;
		}
		@Column
		public Date getEstimateDelivery() {
			return estimateDelivery;
		}
		public void setEstimateDelivery(Date estimateDelivery) {
			this.estimateDelivery = estimateDelivery;
		}
		@Column
		public Date getEstimateSurvey() {
			return estimateSurvey;
		}
		public void setEstimateSurvey(Date estimateSurvey) {
			this.estimateSurvey = estimateSurvey;
		}

}
