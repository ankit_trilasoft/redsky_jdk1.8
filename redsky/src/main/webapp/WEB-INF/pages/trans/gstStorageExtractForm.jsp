<%@ include file="/common/taglibs.jsp"%> 

<head>

	<meta name="heading" content="GST Storage Extracts"/> 
	<title>GST Storage Extracts</title>
	
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
	function valbutton(thisform) {
	
	   var date1 = document.forms['gstStorageExtractForm'].elements['beginDate'].value;	 
   var date2 = document.forms['gstStorageExtractForm'].elements['endDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("End Date should be greater than  or equal to Begin Date");
    document.forms['gstStorageExtractForm'].elements['endDate'].value='';
    return false;
  }
	
	
	
	if(document.forms['gstStorageExtractForm'].elements['beginDate'].value=='')
	{
		alert('Please enter begin date');
		return false;
	}
		
	return checkDateField();
		
}

function checkDateField(){

	if(document.forms['gstStorageExtractForm'].elements['endDate'].value=='')
	{
		alert('Please enter end date ');
		return false;
	}

}
		
		
</script>
	
	
<style type="text/css">


/* collapse */

</style>

</head>
<body style="background-color:#444444;">
<s:form id="gstStorageExtractForm" name="gstStorageExtractForm" action="GSTStorageExtractProcess.html" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:70%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>GST Storage Extracts Processing</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
 <table class="" border="0" width="">
 <tbody>
		  
			<tr><td colspan="10">
		  	<table>
		  	<tr>		  		
		  		<td align="right" width="" class="listwhitetext" >Begin Posting Date<font color="red" size="2">*</font></td>	
		  		
		  		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="8" maxlength="11" readonly="true" /> 
		  		<img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext" >End Posting Date<font color="red" size="2">*</font></td>	
				<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="8" maxlength="11" readonly="true" />
				<img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  	</tr>
		  	</table>
        	</td></tr>
		  	<tr>		  	  		
	  			<td><s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " method="GSTStorageExtracts" name="WCRI" align="top"  value="WCRI" onclick="return valbutton(gstStorageExtractForm);" />	  	
				<s:submit cssClass="cssbutton" cssStyle="width:140px; height:25px " name="State" id="State" align="top" method="stateStorageExtracts" value="Dept. of State(500440)" onclick="return valbutton(gstStorageExtractForm);" />	  		 				           		 				           	
				<s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " name="USAID" id="USAID" align="top" method="usAidStorageExtracts" value="USAID" onclick="return valbutton(gstStorageExtractForm);" />	  		 				           		 				           	
				<s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " name="USDA" id="USDA" align="top" method="USDAStorageExtracts" value="USDA" onclick="return valbutton(gstStorageExtractForm);" />
				<s:submit cssClass="cssbutton" cssStyle="width:150px; height:25px " name="Commerce" id="Commerce" align="top" method="deptOfCommerce" value="Dept. of Commerce" onclick="return valbutton(gstStorageExtractForm);" />	
				<s:reset cssClass="cssbutton" key="Clear" cssStyle="width:70px; height:25px "/>   
				</td>
        	</tr>        			
        	<tr>
		  		<td align="left" height="15px"></td>
		  	</tr>
        	</tbody>
        	</table>
        </div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div> 
</s:form>
<script language="javascript" type="text/javascript">
//document.forms['gstStorageExtractForm'].elements['State'].disabled=true; 
</script>

 <script type="text/javascript">
    var myimage = document.getElementsByTagName("img"); 
   	for (var i = 0; i < myimage.length; i++) {
	   	var idinms=myimage[i].getAttribute('id');
	   	if(idinms != null && (idinms.split('_')[1]=="trigger")){
		   	var args1=idinms.split('_');
		   	RANGE_CAL_1 = new Calendar({
		           inputField: args1[0],
		           dateFormat: "%d-%b-%y",
		           trigger: idinms,
		           bottomBar: true,
		           animation:true,
		           onSelect: function() {                             
		               this.hide();
		       }
		   });
	   	}
   	}
   	
</script>