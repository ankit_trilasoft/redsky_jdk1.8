package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.PartnerProfileInfoDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.PartnerProfileInfo;

public class PartnerProfileInfoDaoHibernate extends GenericDaoHibernate<PartnerProfileInfo, Long> implements PartnerProfileInfoDao {
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	
	public PartnerProfileInfoDaoHibernate() {
		super(PartnerProfileInfo.class);
	} 
	
	public List getProfileInfoListByPartnerCode(String corpID, String partnerCode) {
		getHibernateTemplate().setMaxResults(500);
		return getHibernateTemplate().find("FROM PartnerProfileInfo WHERE partnerCode = '"+ partnerCode +"' ");
	}

}
