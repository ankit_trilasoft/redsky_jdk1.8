package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table ( name="clipboard")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class ScrachCard extends BaseObject {
	
	private Long id;
	private String noteDetail;
	private String corpID;
	private Date createdOn;
	private Date updatedOn;
	private String createdBy;
	private String updatedBy;
	private Date workDate;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("noteDetail",
				noteDetail).append("corpID", corpID).append("createdOn",
				createdOn).append("updatedOn", updatedOn).append("createdBy",
				createdBy).append("updatedBy", updatedBy).append("workDate", workDate).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ScrachCard))
			return false;
		ScrachCard castOther = (ScrachCard) other;
		return new EqualsBuilder().append(id, castOther.id).append(noteDetail,
				castOther.noteDetail).append(corpID, castOther.corpID).append(
				createdOn, castOther.createdOn).append(updatedOn,
				castOther.updatedOn).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(workDate, castOther.workDate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(noteDetail).append(
				corpID).append(createdOn).append(updatedOn).append(createdBy)
				.append(updatedBy).append(workDate).toHashCode();
	}
	@Column( length = 15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length = 82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column( length = 5000)
	public String getNoteDetail() {
		return noteDetail;
	}
	public void setNoteDetail(String noteDetail) {
		this.noteDetail = noteDetail;
	}
	@Column( length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}
	
	

}
