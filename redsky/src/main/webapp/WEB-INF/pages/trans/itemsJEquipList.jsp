<%@ include file="/common/taglibs.jsp"%> 
 
<head> 
    <title><fmt:message key="itemsJEquipList.title"/></title> 
    <meta name="heading" content="<fmt:message key='itemsJEquipList.heading'/>"/> 

	<script type="text/javascript">
		function mailtovendor1(targetElement) {
	var mailToVendorvalue = document.forms['assignItemsForm'].elements['mailtovendor'].value;
		if(document.forms['assignItemsForm'].elements['mailtovendor'].value == '' ){
			mailToVendorvalue=','+targetElement.value;
		}else{
			if(mailToVendorvalue.indexOf(targetElement.value)>=0){
				if(targetElement.value == '' ){
					mailToVendorvalue = mailToVendorvalue.replace(targetElement.value,"");
				}else{
				mailToVendorvalue = mailToVendorvalue.replace(','+targetElement.value,"");
			}
			}else{
				mailToVendorvalue = document.forms['assignItemsForm'].elements['mailtovendor'].value +','+ targetElement.value;
			}
		}
	document.forms['assignItemsForm'].elements['mailtovendor'].value = 	mailToVendorvalue;
	
	} 

	function sendEmailtoVendor() {
		//document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value = document.forms['customerFileForm'].elements['customerFile.corpID'].value + document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
		var daReferrer = document.referrer; 
		var email = document.forms['assignItemsForm'].elements['mailtovendor'].value; 
		document.forms['assignItemsForm'].elements['assignItem'].value = "assignItems";
	}
		</script> 
<script language="javascript" type="text/javascript">

function clear_fields(){
		document.forms['itemsJEquipListForm'].elements['itemsJEquip.descript'].value = "";
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-33px;
padding:2px 0px;
text-align:right;
width:80%;
}
</style>		
</head>

 <s:form id="itemsJEquipListForm" action="searchItemsJEquips" method="post" >
 <c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>  
 <s:hidden name="id" value="<%=request.getParameter("id")%>"/>
 <s:hidden name="id1" value="%{workTicket.id}"/> 
 <s:hidden name="itemType" value="<%=request.getParameter("itemType") %>" />
 <c:set var="itemType" value="<%=request.getParameter("itemType") %>" />
 <s:hidden name="wcontract" value="<%=request.getParameter("wcontract") %>" />
 <c:set var="wcontract" value="<%=request.getParameter("wcontract") %>" />
  	
   
<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div>
<table class="table" style="width:680px">
<thead>
<tr>
	<th><fmt:message key="itemsJEquip.descript"/></th>
	<th></th>
	</tr></thead>	
	<tbody>
		<tr>
			<s:hidden name="itemsJEquip.type" value="${itemType }"/>
			<s:hidden name="itemsJEquip.contract" value="${wcontract}"/>
			<td>
			    <s:textfield name="itemsJEquip.descript" required="true" cssClass="text medium" />
			</td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
	</tbody>
</table>

<!-- <c:out value="${buttons}" escapeXml="false" /> -->

 <!-- 
 <div align="center"><table border="1">
		<tbody>
		<tr>
			<td>
				<b>Search By Type</b>
			</td>
			<td>
			    <s:textfield name="itemsJEquip.type"  cssClass="text medium"/>
			
			    <s:submit cssClass="button" method="search" key="button.search"/>   
			</td>
		</tr>
		</tbody>
 </table>
 </div>
 -->

<!-- 
<c:out value="${buttons}" escapeXml="false" />
 <table>
 <tbody>
 <tr>
 			<td align="left"><b>Notes:</b></td>
			<td align="left" width="100%" colspan="12">
			<s:textarea  rows="4" cols="10" name="workTicket.notes"  cssClass="text medium"/> </td>
 </tr>
		<tbody>
 </table>-->


<%--
<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
    <td width="7" align="right"><img width="7" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
	<td width="140" align="center" class="detailActiveTabLabel content-tab">Unassigned Crate Items
	</td>
	<td width="7" align="left"><img width="7" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
	<td width="1"></td>
	<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="130" align="center" class="content-tab"><a href="itemsJbkEquips.html?id=<%=request.getParameter("id")%> ">Assigned crate items</a></td>
	<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
	
	<!-- <td width="7" align="left"><img width="7" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td> -->
	<td width="0"></td>
	<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="110" align="center" class="content-tab"><a href="editWorkTicketUpdate.html?id=<%=request.getParameter("id")%> ">Work ticket</td>
	<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
    </tr>
</tbody>
</table>
--%>
<s:set name="itemsJEquips" value="itemsJEquips" scope="request"/> 
	<div id="newmnav">
		    <ul>
			  <li><a href="itemsJbkEquips.html?id=<%=request.getParameter("id")%>&itemType=${itemType }&wcontract=${wcontract}"><span>Assigned</span></a></li>
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Unassigned<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <li><a  href="editWorkTicketUpdate.html?id=<%=request.getParameter("id")%>"><span>Work Ticket</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div><br>
	<display:table name="itemsJEquips" class="table" requestURI="" id="itemsJEquipList" export="true" style="width:680px"> 
	<display:column style="width:40px">
	<s:checkbox name="Opt"
				fieldValue="${itemsJEquipList.id}"
				onclick="mailtovendor1(this);"
				 />
	</display:column>
    
    

    <display:column property="descript" sortable="true" titleKey="itemsJEquip.descript" paramId="id" paramProperty="id" style="width:240px"/> 
    <display:column property="contract" sortable="true" titleKey="itemsJEquip.contract"/> 
    
    <display:setProperty name="paging.banner.item_name" value="itemsJEquip"/> 
    <display:setProperty name="paging.banner.items_name" value="itemsJEquip"/>    
    
    <display:setProperty name="export.excel.filename" value="ItemsJEquip List.xls"/> 
    <display:setProperty name="export.csv.filename" value="ItemsJEquip List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="ItemsJEquip List.pdf"/> 
</display:table> 

</s:form>   
<script type="text/javascript"> 
    highlightTableRows("itemsJEquipList"); 
</script>
<!-- 
<script>
function Validate(form)
{
if(confirm("Are You Ready to assign these Items?"))
{
alert(Opt.selected);
 // form.action="itemsJbkEquips.html";
  
  if (Opt.selected="true")
  {
  alert("B");
  	itemsJEquipManager.remove(itemsJEquip.getId()); 
  }
  form.submit();
  //form.action="itemsJbkEquips.html";
}
else
{
  form.action="itemsJbkEquips.html";
  form.submit();
}
}
</script>
 -->
<script>
function Validate1(form)
{
if(confirm("Are You Ready to pull Items to the Job?"))
{
  form.action=confirm("There is nothing to pull");
  form.action="mainMenu.html";
  form.submit();
} 
else
{
  form.action="mainMenu.html";
  form.submit();
} 
}
</script> 


<script>
function check(targetElement)
{
		document.forms['assignItemsForm'].elements['id'].value=targetElement.value;
		alert("Are you ready to assign this Item?");
		//alert("Please click on Assign button to Assign the selected item");
		//document.forms['assignItemsForm'].action="editItemsJEquip2.html";
		//document.forms['assignItemsForm'].submit();
}
</script> 
<s:form id="assignItemsForm" action="itemsJEquips" method="post">
	<s:hidden name="id" value="<%=request.getParameter("id")%>"/> 
	<s:hidden name="mailtovendor" />
	<s:hidden name="id1" value="%{workTicket.id}"/> 
	<s:hidden name="ticket" value="%{workTicket.ticket}" />
<s:hidden name="itemType" value="<%=request.getParameter("itemType") %>" />
 <c:set var="itemType" value="<%=request.getParameter("itemType") %>" />
<s:hidden name="wcontract" value="<%=request.getParameter("wcontract") %>" />
 <c:set var="wcontract" value="<%=request.getParameter("wcontract") %>" />
  <s:hidden name="assignItem" />
	<input type="submit" name="forwardBtn3" value="Assign"
			class="cssbutton" style="width:50px; height:25px" onclick="sendEmailtoVendor()" />
		 </s:form>

<script language="JavaScript1.2"> 
try{
if (document.all){ 
document.onkeydown = function (){ 
var key_f5 = 116; // 116 = F5 

if (key_f5==event.keyCode){ 
event.keyCode = 27; 

return false; 
} 
} 
}
}
catch(e){}
</script>  