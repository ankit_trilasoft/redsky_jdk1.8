<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="CrewRoster" pageWidth="983" pageHeight="822" orientation="Landscape" columnWidth="923" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="428f0184-9072-42b0-bf18-78fad65a6c95">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[select
"CrewRoster.jrxml",
if(c.warehouse='','',c.warehouse) AS  'Warehouse',
concat(c.lastName,', ', c.firstName) AS  'Name',
c.title AS  'Title',
c.typeofWork AS 'TypeOfWork',
c.payhour AS 'Pay Rate',
date(c.hired) AS  'Hired',
date(c.unionName) AS  'Union',
date(c.beginhealthwelf) AS  'Health Welfare',
date(c.begginingPension) AS  'Pension',
date(c.terminateddate) AS 'Termination',
c.socialSecurityNumber AS 'SocialSecurityNumber',
c.employeeId AS 'EmployeeId'

FROM crew c
where c.corpId =$P{Corporate ID}
and (c.terminatedDate is null or c.terminateddate > DATE_ADD(now(), INTERVAL -365 DAY))
and c.hired <= now()
order by c.warehouse,c.lastName,c.hired]]>
	</queryString>
	<field name="CrewRoster.jrxml" class="java.lang.String"/>
	<field name="Warehouse" class="java.lang.String"/>
	<field name="Name" class="java.lang.String"/>
	<field name="Title" class="java.lang.String"/>
	<field name="TypeOfWork" class="java.lang.String"/>
	<field name="Pay Rate" class="java.lang.Double"/>
	<field name="Hired" class="java.sql.Date"/>
	<field name="Union" class="java.sql.Date"/>
	<field name="Health Welfare" class="java.sql.Date"/>
	<field name="Pension" class="java.sql.Date"/>
	<field name="Termination" class="java.sql.Date"/>
	<field name="SocialSecurityNumber" class="java.lang.String"/>
	<field name="EmployeeId" class="java.lang.String"/>
	<sortField name="Warehouse"/>
	<sortField name="Name"/>
	<sortField name="Hired" order="Descending"/>
	<group name="Warehouse">
		<groupExpression><![CDATA[$F{Warehouse}]]></groupExpression>
		<groupHeader>
			<band height="18" splitType="Stretch">
				<staticText>
					<reportElement key="staticText-7" x="0" y="0" width="71" height="18" printWhenGroupChanges="Warehouse" forecolor="#3333FF" uuid="9ba0c082-03d2-4eb0-9977-8ae80a695988"/>
					<textElement>
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Warehouse:]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" isPrintRepeatedValues="false" x="71" y="0" width="107" height="18" printWhenGroupChanges="Warehouse" forecolor="#3333FF" uuid="9f4c59cd-5014-412a-9e3b-64586c69c365"/>
					<textFieldExpression><![CDATA[$F{Warehouse}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="31" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-8" x="0" y="0" width="923" height="31" uuid="e1a88de2-a4eb-4d63-8f3b-d0dc3a5bf502"/>
				<textElement textAlignment="Center">
					<font size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Roster of Active Crew and Crew terminated within last year]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="18" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" mode="Opaque" x="42" y="0" width="136" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="d1956d4b-ed66-49d8-9818-09091579d03a"/>
				<box leftPadding="1"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-2" mode="Opaque" x="178" y="0" width="210" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="6e0a138d-0b91-4ae3-b4c7-77341d97a01d"/>
				<box leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Title]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" mode="Opaque" x="388" y="0" width="38" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="b2f65e4b-01df-4fa0-a9b4-84dd18917963"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Type]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" mode="Opaque" x="426" y="0" width="65" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="fcf07555-2345-44d2-8286-5ac5463a7601"/>
				<box rightPadding="4"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Pay Rate]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" mode="Opaque" x="491" y="0" width="50" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="753f0d3e-108e-4e46-be51-e5651f121fa7"/>
				<box leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Hired]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" mode="Opaque" x="541" y="0" width="50" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="be2a70a6-35f4-4972-898d-2a5d68127568"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Union]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" mode="Opaque" x="591" y="0" width="76" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="e83283c3-aaf4-4e0d-9611-f65108d3b218"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Health Welfare]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" mode="Opaque" x="854" y="0" width="69" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="67b46d34-8c73-42cd-b647-e068829c13dc"/>
				<box leftPadding="2"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Employee Id]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" mode="Opaque" x="667" y="0" width="55" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="b3b2faa4-a585-4742-b61d-41cab96c65ea"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Pension]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" mode="Opaque" x="722" y="0" width="70" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="5210f025-f8c3-4892-b93a-8c4aa9d4a3ff"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Termination]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" mode="Opaque" x="792" y="0" width="62" height="18" forecolor="#000000" backcolor="#CCCCCC" uuid="e629f92b-a021-4b50-8d8c-04b9b6714659"/>
				<box leftPadding="5"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[SS#]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="42" y="0" width="136" height="18" backcolor="#CCCCCC" uuid="c2faebb6-c0d0-4393-a270-f27ba9c6f768"/>
				<box leftPadding="1"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" x="178" y="0" width="210" height="18" uuid="9b8a4ced-96bb-4c7e-acaa-5108485216a8"/>
				<box leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Title}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="388" y="0" width="38" height="18" backcolor="#CCCCCC" uuid="38506f65-2d53-4a67-80a7-99a85db94e13"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{TypeOfWork}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="426" y="0" width="65" height="18" uuid="4b148354-7857-46fe-9c2e-907a58f890c6"/>
				<box rightPadding="4"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Pay Rate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="491" y="0" width="50" height="18" backcolor="#CCCCCC" uuid="41cbe3df-d41b-4257-93a8-800afeb7b43e"/>
				<box leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Hired}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" x="541" y="0" width="50" height="18" uuid="bebc78ae-8468-4e0d-8bce-05b9c6aa6121"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Union}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-4" mode="Opaque" x="591" y="0" width="76" height="18" backcolor="#CCCCCC" uuid="9fb89b16-bc81-45b1-a345-e38086a5c0e3"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Health Welfare}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-5" mode="Opaque" x="854" y="0" width="69" height="18" backcolor="#CCCCCC" uuid="4c366695-6221-4b2c-b5c9-fca4996e8910"/>
				<box leftPadding="2"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{EmployeeId}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-6" x="667" y="0" width="56" height="18" uuid="d5b6d369-c21e-478d-b24b-535ba5e66c71"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Pension}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-7" x="792" y="0" width="62" height="18" uuid="e9216fa4-764f-476b-9f5d-60b5328107dc"/>
				<box leftPadding="5"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SocialSecurityNumber}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-8" mode="Opaque" x="723" y="0" width="69" height="18" backcolor="#CCCCCC" uuid="c027ae7b-3da0-4fb2-9eb2-a9d6efa9f53a"/>
				<box leftPadding="3"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Termination}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Prevent"/>
	</columnFooter>
	<pageFooter>
		<band height="17" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-9" x="178" y="2" width="614" height="15" uuid="0a315f42-bfe9-47d3-84ff-5283551e5fb3"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<text><![CDATA[CrewRoster.jrxml]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-1" x="0" y="2" width="178" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="416a76c5-78b0-460f-a335-580d97ab2744"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" x="792" y="2" width="62" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="1c9610e4-2e75-45b2-8ea8-060efffc5521"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-3" x="854" y="2" width="69" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="d97d09a6-824f-4eed-b11d-dd29d3ae2a9a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-1" x="0" y="0" width="923" height="2" uuid="093d1d71-8095-46a3-8bf9-d4f01420c359"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
