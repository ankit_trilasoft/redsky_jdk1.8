DELIMITER $$

CREATE TRIGGER `partnerpublic_AUPD` AFTER UPDATE ON partnerpublic FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
BEGIN
IF OLD.partnerCode != NEW.partnerCode THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.lastName != NEW.lastName THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.partnerPrefix != NEW.partnerPrefix THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.middleInitial != NEW.middleInitial THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.mailingAddress1 != NEW.mailingAddress1 THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.mailingAddress2 != NEW.mailingAddress2 THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.mailingCity != NEW.mailingCity THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.mailingCountryCode != NEW.mailingCountryCode THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.mailingState != NEW.mailingState THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.mailingZip != NEW.mailingZip THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.billingAddress1 != NEW.billingAddress1 THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.billingAddress2 != NEW.billingAddress2 THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.billingCity != NEW.billingCity THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.billingCountryCode != NEW.billingCountryCode THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.billingState != NEW.billingState THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.billingZip != NEW.billingZip THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.firstName != NEW.firstName THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
END IF;
     IF OLD.billingEmail != NEW.billingEmail THEN
     UPDATE partnerprivate SET billToExtractAccess = 1,vendorExtractAccess = 1 where partnerprivate.partnerCode = NEW.partnerCode and partnerprivate.corpid = 'NWVL';
     END IF;
     END $$


