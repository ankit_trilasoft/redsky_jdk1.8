<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="truckingOperationsDetails.title"/></title>   
    <meta name="heading" content="<fmt:message key='truckingOperationsDetails.heading'/>"/>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
<script language="JavaScript">
	function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39)||(keyCode==110) || (keyCode==109) || ( keyCode==190); 
	}
	function onlyRateAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	  
	}
</script>
<script>
function checkdate(clickType){
	if ('${autoSavePrompt}' == 'No'){
			var noSaveAction = '<c:out value="${truckingOperations.id}"/>';
      		var id = document.forms['truckingOperationsForm'].elements['truckingOperations.id'].value;	
      		
			if(document.forms['truckingOperationsForm'].elements['gotoPageString'].value == 'gototab.turckList'){
				noSaveAction = 'truckingOperationsList.html';
			}
			
			processAutoSave(document.forms['truckingOperationsForm'], 'savetruckingOperations!saveOnTabChange.html', noSaveAction);
	}	
}
function check(){
	if(document.forms['truckingOperationsForm'].elements['truckingOperations.localTruckNumber'].value == '')
	{
		alert('Please select the Truck Number');
		return false;
	}
	
}



function dataPopulate(){
openWindow('truckList.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=truckingOperations.description&fld_code=truckingOperations.localTruckNumber');
}

function findTruckName(){
    var billToCode = document.forms['truckingOperationsForm'].elements['truckingOperations.localTruckNumber'].value;
    if(billToCode!=''){
     var url="truckLineList.html?ajax=1&decorator=simple&popup=true&localTruckNumber=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
     }
     
}
function handleHttpResponse2(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                if(res.length >= 2)
                { 
	           	  document.forms['truckingOperationsForm'].elements['truckingOperations.description'].value=res;
	            }
	            else
	            {
	            alert("Selected Truck Number is not valid, Please select another.");
	            document.forms['truckingOperationsForm'].elements['truckingOperations.localTruckNumber'].value='';
	            document.forms['truckingOperationsForm'].elements['truckingOperations.description'].value='';
	            document.forms['truckingOperationsForm'].elements['truckingOperations.localTruckNumber'].focus();
			   }
           }
}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
                  
        
	var http2 = getHTTPObject();
	function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
</script>

</head> 

<s:form id="truckingOperationsForm" name="truckingOperationsForm" action="savetruckingOperations" method="post" validate="true">   
<s:hidden name="truckingOperations.id" />
<s:hidden name="truckingOperations.corpID" />
<c:set var="ticket" value="<%= request.getParameter("ticket")%>" />
<s:hidden name="ticket" value="<%= request.getParameter("ticket")%>" />
<c:set var="sid" value="<%=request.getParameter("sid") %>" />
 <s:hidden name="sid" value="${sid}" /> 
 <c:set var="tid" value="<%=request.getParameter("tid") %>" />
 <s:hidden name="tid" value="${tid}" /> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>   
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.turckingOperationsList' }">
	<c:redirect url="/truckingOperationsList.html?ticket=${ticket}&sid=${sid}&tid=${tid}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="newmnav">
		  <ul>
		    <li><a href="editWorkTicketUpdate.html?id=${tid}"><span>Work Ticket</span></a></li>
		     <li id="newmnav1" style="background:#FFF "><a class="current"><span>Truck<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  </ul>
  </div>
 <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty sid}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${sid}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
  <div class="spn">&nbsp;</div>
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
<div style="width:850px">
<div id="newmnav">
	<ul>
		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Trucking Operations Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a href="truckingOperationsList.html?ticket=${ticket}&sid=${sid}&tid=${tid}"><span>Trucking Operations List</span></a></li>
	</ul>
</div>
	<div class="spn">&nbsp;</div>
	<div style="padding-bottom:0px;"></div>
</div>

<div id="Layer1" style="width:100%;">

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">   
				<table cellspacing="0" cellpadding="2" border="0" >
					<tbody>
						
						<tr>
						    <td align="right" class="listwhitetext"><fmt:message key='truckingOperations.ticket'/></td>
							<td><s:textfield cssClass="input-text" name='truckingOperations.ticket' readonly="true" cssStyle="width:60px" maxlength="28"/></td>
							</tr>
							<tr>
							<td align="right" class="listwhitetext"><fmt:message key='truckingOperations.localTruckNumber'/><font color="red" size="2">*</font></td>
							<td><s:textfield cssClass="input-text" name='truckingOperations.localTruckNumber' onchange="findTruckName();" cssStyle="width:225px" maxlength="20"/></td>
							<td><img class="openpopup" width="17" height="20" align="top" onclick= "dataPopulate();" src="<c:url value='/images/open-popup.gif'/>"/></td>
							</tr>
							<tr>
							<td align="right" class="listwhitetext"><fmt:message key='truckingOperations.description'/></td>
							<td><s:textfield cssClass="input-text" name='truckingOperations.description'  readonly="true" cssStyle="width:225px" maxlength="28"/></td>
							</tr>
			
	</tbody>
</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>	
 
<table>
	<tbody>
		<tr>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='truckingOperations.createdOn'/></b></td>
			<td valign="top"></td>
			<td style="width:120px">
				<fmt:formatDate var="agentBaseCreatedOnFormattedValue" value="${truckingOperations.createdOn}" pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="truckingOperations.createdOn" value="${agentBaseCreatedOnFormattedValue}"/>
				<fmt:formatDate value="${truckingOperations.createdOn}" pattern="${displayDateTimeFormat}"/>
			</td>		
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='truckingOperations.createdBy' /></b></td>
			<c:if test="${not empty truckingOperations.id}">
				<s:hidden name="truckingOperations.createdBy"/>
				<td style="width:85px"><s:label name="createdBy" value="%{truckingOperations.createdBy}"/></td>
			</c:if>
			<c:if test="${empty truckingOperations.id}">
				<s:hidden name="truckingOperations.createdBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='truckingOperations.updatedOn'/></b></td>
			<fmt:formatDate var="agentBaseupdatedOnFormattedValue" value="${truckingOperations.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
			<s:hidden name="truckingOperations.updatedOn" value="${agentBaseupdatedOnFormattedValue}"/>
			<td style="width:120px"><fmt:formatDate value="${truckingOperations.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='truckingOperations.updatedBy' /></b></td>
			<c:if test="${not empty truckingOperations.id}">
				<s:hidden name="truckingOperations.updatedBy"/>
				<td style="width:85px"><s:label name="updatedBy" value="%{truckingOperations.updatedBy}"/></td>
			</c:if>
			<c:if test="${empty truckingOperations.id}">
				<s:hidden name="truckingOperations.updatedBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
		</tr>
	</tbody>
</table>               
        <s:submit cssClass="cssbutton1" key="button.save" cssStyle="width:55px;" onclick="return check();"/>   
        <s:reset cssClass="cssbutton1" cssStyle="width:55px;" key="Reset" />
        <input type="button" class="cssbutton1" style="width:55px;" onclick="location.href='<c:url value="/truckingOperationsList.html?ticket=${ticket}&sid=${sid}&tid=${tid}"/>'"  value="<fmt:message key="button.cancel"/>"/>   
  
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/truckingOperationsList.html?ticket=${ticket}&sid=${sid}&tid=${tid}"  />
		</c:if>

<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
</s:form>
<script type="text/javascript">   
   document.forms['truckingOperationsForm'].elements['truckingOperations.localTruckNumber'].focus();
</script>