package com.trilasoft.app.webapp.action; 

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.RefMaster; 
import com.trilasoft.app.model.RefQuoteServices;
import com.trilasoft.app.service.ParameterControlManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.TimeSheetManager;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;
import org.appfuse.webapp.action.BaseAction;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List; 
import java.util.Map;
import java.util.Set;
 
public class RefMasterAction extends BaseAction implements Preparable {
	private RefMasterManager refMasterManager;
	private List refMasters;
	private List ls;
	private String s=new String("");
    private List refMastermenus;
    private List qc;
    private  Map<String, String> lead;
    private  Map<String, String> state;
    private  Map<String, String> country;
    private  Map<String, String> survey;
    private  Map<String, String> schedule;
    private  Map<String, String> paytype;
    private  Map<String, String> job;
    private  Map<String, String> organiza;
    private  Map<String, String> peak;
    private  Map<String, String> mode;
    private  Map<String, String> service;
    private  Map<String, String> tcktservc;
    private  Map<String, String> dept;
    private  Map<String, String> house;
    private  Map<String, String> confirm;
    private  Map<String, String> tcktactn;
    private  Map<String, String> woinstr;
    private  Map<String, String> usertype;
    private  Map<String, String> languageList;
    private  Map<String, String> documentCategoryList;
    private  Map<String, String> warehouse;
    private String sessionCorpID;
    //private String parameter;
    private Long maxId;
    private   Date createdon;
    private int fieldLength ;  
    private List supervisorList;
    private Set<Role> userRoles;
	private String enableUser;
    private ParameterControlManager parameterControlManager;
    private List branch; 
    private List customDocumentList;
    private String customDocCode;
    private String customDocDescription;
    private List getCodeList;
    private Boolean activeStatus;
    private String isactiveStatus;
    private String sdHubWarehouseOperationalLimit;
    private  List sequenceList;
    private String sequenceNumber;
    private  Map<String, String> childParameterValues;
    private String childParameterValue;
    private List multiplParentParameterValue;
    private List multiplWareHouseValue;
   
    
    public List getMultiplParentParameterValue() {
		return multiplParentParameterValue;
	}

	public void setMultiplParentParameterValue(List multiplParentParameterValue) {
		this.multiplParentParameterValue = multiplParentParameterValue;
	}

	public void prepare() throws Exception {
    	branch=new ArrayList();
    	branch.add("Yes");
    	branch.add("No");
    	sequenceList=new ArrayList();
    	for(int i =1;i<=50;i++){
	  	 	sequenceList.add(i);
	  	 	}
	}

	public RefMasterAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        if(user.isDefaultSearchStatus()){
            activeStatus=true;
           }
           else{
            activeStatus=false;
           }
	}
  
    public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }
    public List getRefMasters() { 
        return refMasters; 
    } 
    public List getRefMastermenus() { 
        return refMastermenus; 
    }
    private Boolean costElementFlag;
    public String list() {
   
    	/*try{
    	     if(sessionCorpID.equalsIgnoreCase("TSFT"))
    	     {
    	    	 
    			ls = refMasterManager.findAll(sessionCorpID);	
    	    }
    		else
    		{
    			
    			ls = refMasterManager.findAccordingCorpId();
    			
    		}
    	
    	}catch(Exception ex)
    	{
    		if(sessionCorpID.equalsIgnoreCase("TSFT"))
    	    {
    			ls = refMasterManager.findAll(sessionCorpID);	
    	    }
    		else
    		{
    		  
    		  ls = refMasterManager.findAccordingCorpId();
    		 
    		  
    		}
    		return "error";
    	}*/
    	
    	// getComboList(sessionCorpID);
    	this.setSessionCorpID(sessionCorpID);
    	costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
    	parameterListSize = refMasterManager.findByParameterlist(parameter,sessionCorpID).size();
		return SUCCESS;
         
    } 
    public String getComboList(String corpId){
  	   lead = refMasterManager.findByParameter(corpId, "LEAD");
  	   survey = refMasterManager.findByParameter(corpId, "SURVEY");
  	   state = refMasterManager.findByParameter(corpId, "STATE");
  	   country = refMasterManager.findByParameter(corpId, "COUNTRY");
  	   schedule = refMasterManager.findByParameter(corpId, "SCHEDULE");
  	   paytype = refMasterManager.findByParameter(corpId, "PAYTYPE");
  	   job = refMasterManager.findByParameter(corpId, "JOB");
  	   organiza = refMasterManager.findByParameter(corpId, "ORGANIZA");
  	   peak = refMasterManager.findByParameter(corpId, "PEAK");
  	   mode = refMasterManager.findByParameter(corpId, "MODE");
  	 	service = refMasterManager.findByParameter(corpId, "SERVICE");
  	 	documentCategoryList = refMasterManager.findByParameter(corpId, "Document Category");
  	 	tcktservc = refMasterManager.findByParameter(corpId, "TCKTSERVC");
  	 	dept= refMasterManager.findByParameter(corpId, "DEPT");
  	 	house= refMasterManager.findByParameter(corpId, "HOUSE");
  	 	confirm= refMasterManager.findByParameter(corpId, "CONFIRM");
  	 	tcktactn= refMasterManager.findByParameter(corpId, "TCKTACTN");
  	 	woinstr= refMasterManager.findByParameter(corpId, "WOINSTR");
  	 	usertype = refMasterManager.findByParameter(corpId, "USER_TYPE");
  	 	if(!corpId.equalsIgnoreCase("TSFT"))
		{
		usertype.remove("AGENT");
		}
		
  	 	supervisorList= refMasterManager.getSuperVisor(corpId);
  	 	costElementFlag = refMasterManager.getCostElementFlag(corpId);
  	 	languageList = refMasterManager.findByflexValue(corpId, "LANGUAGE");
  	 	wareHouse = refMasterManager.findByParameter(corpId, "HOUSE");
  	 	if(languageList==null){
  	 		languageList=new HashMap<String,String>();
  	 	}
  	   return SUCCESS;
     }
    public  Map<String, String> getCountry() {
		return country;
	}

	public  Map<String, String> getJob() {
		return job;
	}

	public  Map<String, String> getLead() {
		return lead;
	}

	public  Map<String, String> getOrganiza() {
		return organiza;
	}

	public  Map<String, String> getPaytype() {
		return paytype;
	}

	public  Map<String, String> getPeak() {
		return peak;
	}

	public  Map<String, String> getSchedule() {
		return schedule;
	}

	public  Map<String, String> getState() {
		return state;
	}

	public  Map<String, String> getSurvey() {
		return survey;
	}
	public  Map<String, String> getMode() {
		return mode;
	}
	
	 public  Map<String, String> getService() {
			return service;
		}
	 public  Map<String, String> getTcktservc() {
			return tcktservc;
		}
	 public  Map<String, String> getDept() {
			return dept;
		}
	 public  Map<String, String> getHouse() {
			return house;
		}
	 public  Map<String, String> getConfirm() {
			return confirm;
		}
	 public  Map<String, String> getTcktactn() {
			return tcktactn;
		}
	 public  Map<String, String> getWoinstr() {
			return woinstr;
		}
	 public Map<String, String> getUsertype() {
			return usertype;
		}
	private RefMaster refMaster; 
    private Long id;
    private String parameter;
    private User user;
    private List users;
    private List searchCodes; 
	private String firstName;
	private String lastName;
	private String username;
	private String userType;
	private String email;
	private String supervisor;
	private List roles;
	private Role role;
	private RoleManager roleManager;
	private List list;

	private List roleList;

	private String searchCode;
	private String searchDescription;
	private Set parameterSet;
	private String hitFlag;
	private Boolean childParameterFlag;
	private String controlParameterDescription;
	private String nameonform;
	
	public void setId(Long id) { 
        this.id = id; 
    }
    
    public RefMaster getRefMaster() { 
        return refMaster; 
    } 
     
    public void setRefMaster(RefMaster refMaster) { 
        this.refMaster = refMaster; 
    } 
    
    public String delete() {
    	refMaster = refMasterManager.get(id); 
		if(refMaster.getCorpID().equalsIgnoreCase("TSFT")){
			tsftFlag=true;
		}
		if(tsftFlag==false){
			refMasterManager.remove(id); 
			saveMessage(getText("refMaster.deleted")); 
	    	
		}else{
			saveMessage("Standard entries can't be deleted."); 
		}
		refMasters = refMasterManager.findByParameterlist(parameter,sessionCorpID);
		try{
		hybridFlag=checkHybridFlag(parameter,sessionCorpID);
 	   tsftFlag=checkTsftFlag(parameter, sessionCorpID);
 	   if(tsftFlag==true && hybridFlag==false){
 		   editable=false; 
 	   }else{
 		   editable=true; 
 	   }
		}catch(Exception ex){
			
		}
    	return SUCCESS;
    } 
     
    public String edit() { 
    	getComboList(sessionCorpID);
    	try{
    		if (id != null) { 
    			refMaster = refMasterManager.get(id); 
    			if(refMaster.getCorpID().equalsIgnoreCase("TSFT")){
    				tsftFlag=true;
    			}
    			multiplParentParameterValue = new ArrayList();
   			 String parameterValue = refMaster.getChildParameterValues();
   			if(parameterValue!=null){
   			 String[] parameterValues = parameterValue.split(",");
   			 for(String rType:parameterValues){
   				multiplParentParameterValue.add(rType.trim());
   			 }
   			}
   			multiplWareHouseValue = new ArrayList();
   			String warehouseValue = refMaster.getServiceWarehouse();
   			
   			if(warehouseValue!=null){
      			 String[] warehouseValues = warehouseValue.split(",");
      			 for(String rType:warehouseValues){
      				multiplWareHouseValue.add(rType.trim());
      			 }
      			}
   			
    			createdon=refMaster.getCreatedOn();
    			refMaster.setCreatedOn(createdon);
        	} else { 
    			refMaster = new RefMaster(); 
    			refMaster.setCreatedOn(new Date());
    			refMaster.setUpdatedOn(new Date());
    			refMaster.setLanguage("en");
    			if(!sessionCorpID.equalsIgnoreCase("TSFT")){
    			try{
    			hybridFlag=checkHybridFlag(parameter, sessionCorpID);
    			tsftFlag=checkTsftFlag(parameter, sessionCorpID);
    			if(tsftFlag==true && hybridFlag==false){
    	       		   editable=false; 
    	       		saveMessage("Can't add item to standard parameter");
    	       	   }else{
    	       		   editable=true; 
    	       	   }
    			}catch(Exception ex){
    				
    			}
    			}
    			List maxId = refMasterManager.findMaxIdByParameter(parameter,sessionCorpID);
            	if(maxId!=null && !maxId.isEmpty() && maxId.get(0)!=null){
            		id = Long.parseLong(maxId.get(0).toString());
            		RefMaster refMasterNew = refMasterManager.get(id);
            		refMaster.setLabel1(refMasterNew.getLabel1());
            		refMaster.setLabel2(refMasterNew.getLabel2());
            		refMaster.setLabel3(refMasterNew.getLabel3());
            		refMaster.setLabel4(refMasterNew.getLabel4());
            		refMaster.setLabel5(refMasterNew.getLabel5());
            		refMaster.setLabel6(refMasterNew.getLabel6());
            		refMaster.setLabel7(refMasterNew.getLabel7());
            		refMaster.setLabel8(refMasterNew.getLabel8());
            	}
        	}
    		if(sessionCorpID.equalsIgnoreCase("TSFT"))
    		{
    			editable=true;
    		}
    		sequenceList=new ArrayList();
        	for(int i =1;i<=50;i++){
    	  	 	sequenceList.add(i);
    	  	 	}
        	childParameterValue=refMasterManager.getValueFromParameterControl(sessionCorpID,refMaster.getParameter());
        	if((childParameterValue!=null) && !(childParameterValue.equals(""))){
        	childParameterFlag=true;
        	}
        	childParameterValues=refMasterManager.findByParentParameterValues(sessionCorpID,childParameterValue);
        	if(childParameterValues==null){
				childParameterValues=new HashMap<String, String>();
			}
        	
        	controlParameterDescription=parameterControlManager.getDescription(parameter,sessionCorpID);
        	
   	    }catch(Exception ex){
    		  String key ="There is some internal issue. Please contact support@redskymobility.com.";
    		  saveMessage(getText(key));
    		  refMasters = refMasterManager.findByParameterlist(parameter,sessionCorpID);
    		  if(!sessionCorpID.equalsIgnoreCase("TSFT")){
    		  try{
    		  hybridFlag=checkHybridFlag(parameter,sessionCorpID);
       	   	  tsftFlag=checkTsftFlag(parameter, sessionCorpID);
       	   if(tsftFlag==true && hybridFlag==false){
       		   editable=false; 
       	   }else{
       		   editable=true; 
       	   }
    	}catch(Exception ex1){
    		
    	}
    	}
    	if(sessionCorpID.equalsIgnoreCase("TSFT"))
		{
			editable=true;
		}
    		  return "error";
   	    }
   	 if(sessionCorpID.equalsIgnoreCase("TSFT"))
		{
			editable=true;
		}
    return SUCCESS; 
    } 
     
    public String save() throws Exception {
    	getComboList(sessionCorpID);
    	if (cancel != null) {
    		return "cancel"; 
        } 
    	if (delete != null) { 
        	return delete(); 
        } 
        refMaster.setCorpID(sessionCorpID);
    	refMaster.setUpdatedOn(new Date());
        refMaster.setUpdatedBy(getRequest().getRemoteUser());
        boolean isNew = (refMaster.getId() == null);
        List isexisted=refMasterManager.isExisted(refMaster.getCode(), parameter, sessionCorpID,refMaster.getLanguage());
        if(sessionCorpID.equalsIgnoreCase("TSFT"))
		{
			editable=true;
		}
        if(parameter!=null && parameter.equalsIgnoreCase("DOCUMENT")){
        	if((refMaster.getflex4()==null) || (refMaster.getflex4().equalsIgnoreCase("")) ){
        		errorMessage("Document Category can not be blank  for Document Parameter");
        		return INPUT;
        		
        	}
        }
		if(!isexisted.isEmpty() && isNew){
			errorMessage("Code already exist for this Parameter");
			return INPUT; 
		}else{  
			if (!isNew) { 
				// refMaster.setCreatedOn(createdon);
	        	refMaster=refMasterManager.save(refMaster);
	        } else {
	        	refMaster.setCreatedOn(new Date());
	        	refMaster=refMasterManager.save(refMaster);
	        }
			childParameterValue=refMasterManager.getValueFromParameterControl(sessionCorpID,refMaster.getParameter());
			if((childParameterValue!=null) && !(childParameterValue.equals(""))){
	        	childParameterFlag=true;
	        	}
			childParameterValues=refMasterManager.findByParentParameterValues(sessionCorpID,childParameterValue);
			if(childParameterValues==null){
				childParameterValues=new HashMap<String, String>();
			}
			if(gotoPageString==null || gotoPageString.equalsIgnoreCase("")){
			String key = (isNew) ? "refMaster.added" : "refMaster.updated"; 
		    saveMessage(getText(key));
			}
			if(sessionCorpID.equalsIgnoreCase("TSFT"))
    		{
    			editable=true;
    		}
		    if (!isNew) {   
		    	return SUCCESS;   
	        } else {
		        maxId = Long.parseLong(refMasterManager.findMaximumId().get(0).toString());
		        refMaster = refMasterManager.get(maxId);
		        createdon = refMaster.getCreatedOn();
		        refMaster.setCreatedOn(createdon);
		        refMasters = refMasterManager.findByParameterlist(parameter,sessionCorpID);
		        try{
		        hybridFlag=checkHybridFlag(parameter,sessionCorpID);
		    	   tsftFlag=checkTsftFlag(parameter, sessionCorpID);
		    	   if(tsftFlag==true && hybridFlag==false){
		    		   editable=false; 
		    	   }else{
		    		   editable=true; 
		    	   }
		        }catch(Exception ex){
		        	
		        }
		        if(sessionCorpID.equalsIgnoreCase("TSFT"))
	    		{
	    			editable=true;
	    		}
		        return SUCCESS;
	        }
		}  
     
    }
    

    @SkipValidation
    public String refMasterParameterList() {
    	costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
    	try{
    		if(sessionCorpID.equalsIgnoreCase("TSFT"))
    		{
    			if(refMaster.getParameter()!=null)
    			{
    				ls = refMasterManager.searchList(refMaster.getParameter(),sessionCorpID,nameonform);
    			    editable=true; 
    		    	  
    			}
    		}
    		else
    		{
    			if(refMaster.getParameter()!=null)
    			{
    				ls = refMasterManager.searchListWithoutState(refMaster.getParameter(), sessionCorpID,nameonform);
    				try{
    				hybridFlag=checkHybridFlag(refMaster.getParameter(),sessionCorpID);
    				tsftFlag=checkTsftFlag(refMaster.getParameter(), sessionCorpID);
    		    	   if(tsftFlag==true && hybridFlag==false){
    		    		   editable=false; 
    		    	   }else{
    		    		   editable=true; 
    		    	   }
    				}catch(Exception ex){
    					
    				}
    				if(refMaster.getParameter().equalsIgnoreCase("state")||refMaster.getParameter().equalsIgnoreCase("country"))
    					{
    						ls.removeAll(ls);
    					}
    				else
    				{
    					ls = refMasterManager.searchListWithoutState(refMaster.getParameter(),sessionCorpID,nameonform);
    					ls.remove("state");
    					ls.remove("COUNTRY");
    					try{
    					hybridFlag=checkHybridFlag(refMaster.getParameter(),sessionCorpID);
    					tsftFlag=checkTsftFlag(refMaster.getParameter(), sessionCorpID);
     		    	   if(tsftFlag==true && hybridFlag==false){
     		    		   editable=false; 
     		    	   }else{
     		    		   editable=true; 
     		    	   }
    				 }catch(Exception ex){
    					 
    				 }
    				}
    				
    			}
    		}
    		hitFlag="1";
    		}catch(Exception ex) 
    		{
    			parameter=new String("");
    			if(sessionCorpID.equalsIgnoreCase("TSFT"))
        		{
    				 refMasters = refMasterManager.findByParameterlist(parameter,sessionCorpID);
    				//ls = refMasterManager.searchList(parameter,sessionCorpID);
    				 editable=true; 
    		    	
        		}
    			else
    			{
    				refMasters = refMasterManager.findByParameterlistWithoutState(parameter,sessionCorpID);
    				try{
    				hybridFlag=checkHybridFlag(parameter,sessionCorpID);
    		    	   tsftFlag=checkTsftFlag(parameter, sessionCorpID);
    		    	   if(tsftFlag==true && hybridFlag==false){
    		    		   editable=false; 
    		    	   }else{
    		    		   editable=true; 
    		    	   }
    				}catch(Exception ex2){
    					
    				}
    				/*ls = refMasterManager.searchListWithoutState(parameter, sessionCorpID);
    				if(refMaster.getParameter().equalsIgnoreCase("state")||refMaster.getParameter().equalsIgnoreCase("country"))
					{
						ls.removeAll(ls);
					}
					else
					{
						ls = refMasterManager.searchListWithoutState(refMaster.getParameter(),sessionCorpID);
						ls.remove("state");
						ls.remove("COUNTRY");
						
					}*/
    				
    			}
    			hitFlag="2";
    			return "error";
    		}
    	return SUCCESS;   
      }
    private int parameterListSize;
	private Boolean tsftFlag=false;
	private Boolean hybridFlag;
	private Boolean editable;
    @SkipValidation
    public String refMastermenusList() {
    	
    	try
    	{  
    		sdHubWarehouseOperationalLimit = refMasterManager.getSDHubWarehouseOperationalLimit(sessionCorpID);
    		refMasters = refMasterManager.findByRefParameterlist(parameter,sessionCorpID);
    	   try{
    	   hybridFlag=checkHybridFlag(parameter,sessionCorpID);
    	   tsftFlag=checkTsftFlag(parameter, sessionCorpID);
    	   if(tsftFlag==true && hybridFlag==false){
    		   editable=false; 
    	   }else{
    		   editable=true; 
    	   }
    	   }catch(Exception ex){
    		   
    	   }
    	   if(sessionCorpID.equalsIgnoreCase("TSFT")){
    		   editable=true;   
    	   }
    	}catch(Exception ex)
    	{
    		parameter=new String("");
    		refMasters = refMasterManager.findByRefParameterlist(parameter,sessionCorpID);
    		try{
    	    	   hybridFlag=checkHybridFlag(parameter,sessionCorpID);
    	    	   tsftFlag=checkTsftFlag(parameter, sessionCorpID);
    	    	   if(tsftFlag==true && hybridFlag==false){
    	    		   editable=false; 
    	    	   }else{
    	    		   editable=true; 
    	    	   }
    	    	   }catch(Exception ex1){
    	    		   
    	    	   }
    	    	   if(sessionCorpID.equalsIgnoreCase("TSFT")){
    	    		   editable=true;   
    	    	   }
    		return "error";
    	}
    	parameterListSize = refMasterManager.findByParameterlist(parameter,sessionCorpID).size();
    	sequenceList=new ArrayList();
    	for(int i =1;i<=50;i++){
	  	 	sequenceList.add(i);
	  	 	}
          return SUCCESS;   
      }
    
    
    
    private Boolean checkHybridFlag(String parameter2, String sessionCorpID2) {
    	Boolean hybridFlagValue=parameterControlManager.checkHybridFlag(parameter2, sessionCorpID2);
		return hybridFlagValue;
	}

	private Boolean checkTsftFlag(String parameter2, String sessionCorpID2) {
		Boolean tsftFlagValue=parameterControlManager.checkTsftFlag(parameter2, sessionCorpID2);
		return tsftFlagValue;
	}

	@SkipValidation
    public String refMasterListByParameter() {
    	parameterListSize = refMasterManager.findByParameterlist(parameter,sessionCorpID).size();
        return SUCCESS;   
     }
    @SkipValidation
    public String search(){
    	String status="";
    	if(isactiveStatus==null || isactiveStatus.toString().trim().equalsIgnoreCase("") || isactiveStatus.toString().trim().equalsIgnoreCase("All")){
    		status="";
    	}else{
    		status=isactiveStatus;
    	}
    		if(parameter !=null){
    			if(sessionCorpID.equalsIgnoreCase("TSFT")){
    				refMasters = refMasterManager.findByParameter(parameter, refMaster.getCode(), refMaster.getDescription(), sessionCorpID, status );
    				try{
    				hybridFlag=checkHybridFlag(parameter,sessionCorpID);
    		    	tsftFlag=checkTsftFlag(parameter, sessionCorpID);
    		    	   if(tsftFlag==true && hybridFlag==false){
    		    		   editable=false; 
    		    	   }else{
    		    		   editable=true; 
    		    	   }
    				}catch(Exception ex){
    					
    				}
    			}else{
    				refMasters = refMasterManager.findByParameterWithoutState(parameter, refMaster.getCode(), refMaster.getDescription(), sessionCorpID, status );
    				try{
    				hybridFlag=checkHybridFlag(parameter,sessionCorpID);
    		    	tsftFlag=checkTsftFlag(parameter, sessionCorpID);
    		    	   if(tsftFlag==true && hybridFlag==false){
    		    		   editable=false; 
    		    	   }else{
    		    		   editable=true; 
    		    	   }
    				}catch(Exception ex){
    					
    				}
    			}	
    		}		
    	/*}catch(Exception ex){
    		parameter=new String("");
    		String code=new String("");
    		String description=new String("");
    		if(sessionCorpID.equalsIgnoreCase("TSFT")){
    			refMasters = refMasterManager.findByParameter(parameter,code,description,sessionCorpID);
			}else{
    			refMasters = refMasterManager.findByParameterWithoutState(refMaster.getParameter(),refMaster.getCode() ,refMaster.getDescription(),sessionCorpID);
			}
  		    return "error";
    	}*/
        return SUCCESS;   
    }
    
    
    

	private String gotoPageString;
	private String validateFormNav;
	
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
    	String s = save();
    	return gotoPageString;
    }
	
	
	@SkipValidation
	public String lockPayrollForm()
	{
		state = refMasterManager.findState(sessionCorpID);
		refMaster=refMasterManager.get(id);
		//lockPayrollList=timeSheetManager.getLockPayrollList(sessionCorpID);
		return SUCCESS;
	}
	private  Map<String, String> wareHouse;
	@SkipValidation
	public String updatelockPayroll()
	{
		wareHouse= refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		state = refMasterManager.findByParameter(sessionCorpID, "STATE");
		refMasterManager.updateRefmasterStopDate(refMaster.getStopDate(),refMaster.getFieldLength(), refMaster.getBucket2(), refMaster.getBucket(), refMaster.getNumber(), refMaster.getAddress1(),refMaster.getAddress2(),refMaster.getCity(),refMaster.getState(),refMaster.getZip(), refMaster.getBranch() ,sessionCorpID, refMaster.getCode(), refMaster.getUpdatedOn(), getRequest().getRemoteUser());
		lockPayrollList();
		return SUCCESS;
	}
	private List lockPayrollList;
	private TimeSheetManager timeSheetManager;
	private String name;
	private List findUserByRole;
	private String hitFlag1="";
	@SkipValidation
	public String lockPayrollList()
	{
		//wareHouse= refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		lockPayrollList=timeSheetManager.getLockPayrollList(sessionCorpID);
		hitFlag1="1";
		return SUCCESS;
	}
	
    
    
    public String searchByCode() {
     	refMasters = refMasterManager.searchByCodeAndDescription(refMaster.getCode(), refMaster.getDescription(),sessionCorpID);
     	try{
     	hybridFlag=checkHybridFlag(refMaster.getParameter(),sessionCorpID);
     	tsftFlag=checkTsftFlag(refMaster.getParameter(), sessionCorpID);
 	   if(tsftFlag==true && hybridFlag==false){
 		   editable=false; 
 	   }else{
 		   editable=true; 
 	   }
     	}catch(Exception ex){
     		
     	}
     	return SUCCESS;
        }
    @SkipValidation
    public String updateRefmasterStatus(){
    	if (id != null) { 
    		String status=getRequest().getParameter("status");
    		if(status==null || status.equalsIgnoreCase("")){
    			status="Inactive";
    		}
			RefMaster refMasterTemp = refMasterManager.get(id); 
			refMasterTemp.setUpdatedOn(new Date());
			refMasterTemp.setUpdatedBy(getRequest().getRemoteUser());
			refMasterTemp.setStatus(status);
			refMasterManager.save(refMasterTemp);
    	}
    	return SUCCESS;
    }
    @SkipValidation   
public String customsDocumentList()
    {
    	getCodeList = refMasterManager.findCodeList();
    	customDocumentList = refMasterManager.customsDocList(sessionCorpID);
	return SUCCESS;
    }
    @SkipValidation
	public String customsDocumentSearchLists()
    {
    	getCodeList = refMasterManager.findCodeList();
    	customDocumentList = refMasterManager.searchCustomDoc(customDocCode==null ?"":customDocCode ,customDocDescription==null ?"":customDocDescription,sessionCorpID);
     return SUCCESS;   
    	
    	}
    	
	/*public String 	getCodeList()
	{
		getCodeList = refMasterManager.findCodeList();
		return SUCCESS;
	}*/
	
    public List getUsers() {
		return users;
	}

	public void setUsers(List users) {
		this.users = users;
	}
	
	 public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@SkipValidation
	public String userListBySearch() {
		if(sessionCorpID == null){sessionCorpID = "";}
		if(userType == null){userType = "";}
		if(username == null){username = "";}
		if(firstName == null){firstName = "";}
		if(lastName == null){lastName = "";}
		if(email == null){email = "";}
		if(supervisor == null){supervisor = "";}
		if(enableUser == null){enableUser = "";}
 	//System.out.println("\n\n In User search list:\t");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        userRoles = user.getRoles();
        boolean adminAccess =false;
        if(userRoles!=null){
			for (Role role : userRoles){				
				if(role.getName().toString().equals("ROLE_ADMIN")){
					adminAccess=true;
				}
		}
			 
		}
        if(adminAccess) {     
	      users = refMasterManager.searchUser(userType, username, firstName, lastName, email, supervisor,enableUser,"");
        }else {
			if(user.getParentAgent()!=null && (!(user.getParentAgent().toString().trim().equals("")))) {
				
				 users = refMasterManager.searchUser(userType, username, firstName, lastName, email, supervisor,enableUser,user.getParentAgent().toString().trim());
			}else {
				
				 users = refMasterManager.searchUser(userType, username, firstName, lastName, email, supervisor,enableUser,"");	
			}
		}
	getComboList(sessionCorpID);
        return SUCCESS;
    }
	 
	 @SkipValidation
	    public String updateRefmasterSequenceNumber(){
		 try{
	    	if (id != null) { 
				RefMaster refMasterTemp = refMasterManager.get(id); 
				refMasterTemp.setUpdatedOn(new Date());
				refMasterTemp.setUpdatedBy(getRequest().getRemoteUser());
				if(sequenceNumber!=null && !sequenceNumber.equals("")){
				refMasterTemp.setSequenceNo(Integer.parseInt(sequenceNumber));
				}else{
					refMasterTemp.setSequenceNo(null);
				}
				refMasterManager.save(refMasterTemp);
	    	}
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	    	return SUCCESS;
	    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	@SkipValidation
	public String corpRoles() {
		
		roles = refMasterManager.findByRoles(sessionCorpID);
	   return SUCCESS;
	  }
	@SkipValidation
	public String userRoles() {
		roles = refMasterManager.findRolesUser(id,sessionCorpID);
	   return SUCCESS;
	  }
	@SkipValidation
	public String findUserByRole() {
		findUserByRole = refMasterManager.findUserRoles(sessionCorpID,name);
		return SUCCESS;
	  }
	

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public void setRoleManager(RoleManager roleManager) {
		this.roleManager = roleManager;
	}

	public String getSearchCode() {
		return searchCode;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public String getSearchDescription() {
		return searchDescription;
	}

	public void setSearchDescription(String searchDescription) {
		this.searchDescription = searchDescription;
	}

	public List getSearchCodes() {
		return searchCodes;
	}

	public void setSearchCodes(List searchCodes) {
		this.searchCodes = searchCodes;
	}

	public Set getParameterSet() {
		return parameterSet;
	}

	public void setParameterSet(Set parameterSet) {
		this.parameterSet = parameterSet;
	}

	public List getLs() {
		return ls;
	}

	public void setLs(List ls) {
		this.ls = ls;
	}

	 public Long getMaxId() {
			return maxId;
		}

		public void setMaxId(Long maxId) {
			this.maxId = maxId;
		}

		public  Date getCreatedon() {
			return createdon;
		}

		public  void setCreatedon(Date createdon) {
			this.createdon = createdon;
		}
		/*
		@SkipValidation	
	public void serching()throws ServletException,IOException 
	{
		HttpServletRequest req=getRequest();
		HttpServletResponse res =getResponse();
		res.setContentType("text/html");
		PrintWriter out =res.getWriter(); 
		List list = refMasterManager.searchList(parameter);
		out.println(list+"singh");
		HttpSession ses=req.getSession();
		ses.setAttribute("list", list);
		String add="/WEB-INF/pages/trans/refMasterList.jsp";
		RequestDispatcher rd =req.getRequestDispatcher(add);
		rd.forward(req, res);
		
	}
		*/
	@SkipValidation	
	public String test()
	{
		singh=refMasterManager.testJoin();
		//System.out.println("singh in action"+s);
		return SUCCESS;
	}
	
	private List singh;
	public List getSingh() {
		return singh;
	}

	public void setSingh(List ls) {
		this.singh = ls;
	}
	/*
		@SkipValidation	
		public String test1()
		{
		
			//singh=refMasterManager.sample();
			singh=refMasterManager.test();
			//for(Object obj:)
			System.out.println("\n\n\n\nsingh in action"+singh);
			return SUCCESS;
		}
		
        */
		public List getList() {
			return list;
		}

		public void setList(List list) {
			this.list = list;
		}

		public List getRoles() {
			return roles;
		}

		public void setRoles(List roles) {
			this.roles = roles;
		}

		public List getRoleList() {
			return roleList;
		}

		public void setRoleList(List roleList) {
			this.roleList = roleList;
		}

		public Long getId() {
			return id;
		}

		public int getFieldLength() {
			return fieldLength;
		}

		public void setFieldLength(int fieldLength) {
			this.fieldLength = fieldLength;
		}
		
		public void setParameter(String parameter) { 
	        this.parameter = parameter; 
	    }

		public String getParameter() {
			return parameter;
		}

		public String getSessionCorpID() {
			return sessionCorpID;
		}

		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}

		public String getGotoPageString() {
			return gotoPageString;
		}

		public void setGotoPageString(String gotoPageString) {
			this.gotoPageString = gotoPageString;
		}

		public String getValidateFormNav() {
			return validateFormNav;
		}

		public void setValidateFormNav(String validateFormNav) {
			this.validateFormNav = validateFormNav;
		}

		public String getUserType() {
			return userType;
		}

		public void setUserType(String userType) {
			this.userType = userType;
		}

		public  Map<String, String> getWareHouse() {
			return wareHouse;
		}

		public  void setWareHouse(Map<String, String> wareHouse) {
			this.wareHouse = wareHouse;
		}

		public String getSupervisor() {
			return supervisor;
		}

		public void setSupervisor(String supervisor) {
			this.supervisor = supervisor;
		}

		public int getParameterListSize() {
			return parameterListSize;
		}

		public void setParameterListSize(int parameterListSize) {
			this.parameterListSize = parameterListSize;
		}

		public List getLockPayrollList() {
			return lockPayrollList;
		}

		public void setLockPayrollList(List lockPayrollList) {
			this.lockPayrollList = lockPayrollList;
		}

		public void setTimeSheetManager(TimeSheetManager timeSheetManager) {
			this.timeSheetManager = timeSheetManager;
		}

		public List getSupervisorList() {
			return supervisorList;
		}

		public void setSupervisorList(List supervisorList) {
			this.supervisorList = supervisorList;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List getFindUserByRole() {
			return findUserByRole;
		}

		public void setFindUserByRole(List findUserByRole) {
			this.findUserByRole = findUserByRole;
		}

		public void setTsftFlag(Boolean tsftFlag) {
			this.tsftFlag = tsftFlag;
		}

		public Boolean getTsftFlag() {
			return tsftFlag;
		}

		public Boolean getHybridFlag() {
			return hybridFlag;
		}

		public void setHybridFlag(Boolean hybridFlag) {
			this.hybridFlag = hybridFlag;
		}

		public void setParameterControlManager(
				ParameterControlManager parameterControlManager) {
			this.parameterControlManager = parameterControlManager;
		}

		public Boolean getEditable() {
			return editable;
		}

		public void setEditable(Boolean editable) {
			this.editable = editable;
		}

		public String getHitFlag() {
			return hitFlag;
		}

		public void setHitFlag(String hitFlag) {
			this.hitFlag = hitFlag;
		}

		public String getHitFlag1() {
			return hitFlag1;
		}

		public void setHitFlag1(String hitFlag1) {
			this.hitFlag1 = hitFlag1;
		}

		public Boolean getCostElementFlag() {
			return costElementFlag;
		}

		public void setCostElementFlag(Boolean costElementFlag) {
			this.costElementFlag = costElementFlag;
		}
		 public String getEnableUser() {
				return enableUser;
			}

			public void setEnableUser(String enableUser) {
				this.enableUser = enableUser;
			}

			public Map<String, String> getLanguageList() {
				return languageList;
			}

			public void setLanguageList(Map<String, String> languageList) {
				this.languageList = languageList;
			}
			public List getBranch() {
				return branch;
			}

			public void setBranch(List branch) {
				this.branch = branch;
			}

			public Map<String, String> getDocumentCategoryList() {
				return documentCategoryList;
			}

			public void setDocumentCategoryList(Map<String, String> documentCategoryList) {
				this.documentCategoryList = documentCategoryList;
			}


			public List getCustomDocumentList() {
				return customDocumentList;
			}

			public void setCustomDocumentList(List customDocumentList) {
				this.customDocumentList = customDocumentList;
			}

			public String getCustomDocCode() {
				return customDocCode;
			}

			public void setCustomDocCode(String customDocCode) {
				this.customDocCode = customDocCode;
			}

			public String getCustomDocDescription() {
				return customDocDescription;
			}

			public void setCustomDocDescription(String customDocDescription) {
				this.customDocDescription = customDocDescription;
			}

			public List getGetCodeList() {
				return getCodeList;
			}

			public void setGetCodeList(List getCodeList) {
				this.getCodeList = getCodeList;
			}

			public Boolean getActiveStatus() {
				return activeStatus;
			}

			public void setActiveStatus(Boolean activeStatus) {
				this.activeStatus = activeStatus;
			}

			public String getSdHubWarehouseOperationalLimit() {
				return sdHubWarehouseOperationalLimit;
			}

			public void setSdHubWarehouseOperationalLimit(
					String sdHubWarehouseOperationalLimit) {
				this.sdHubWarehouseOperationalLimit = sdHubWarehouseOperationalLimit;
			}

			public String getIsactiveStatus() {
				return isactiveStatus;
			}

			public void setIsactiveStatus(String isactiveStatus) {
				this.isactiveStatus = isactiveStatus;
			}

			public List getSequenceList() {
				return sequenceList;
			}

			public void setSequenceList(List sequenceList) {
				this.sequenceList = sequenceList;
			}

			public String getSequenceNumber() {
				return sequenceNumber;
			}

			public void setSequenceNumber(String sequenceNumber) {
				this.sequenceNumber = sequenceNumber;
			}

			public String getChildParameterValue() {
				return childParameterValue;
			}

			public void setChildParameterValue(String childParameterValue) {
				this.childParameterValue = childParameterValue;
			}

			public Map<String, String> getChildParameterValues() {
				return childParameterValues;
			}

			public void setChildParameterValues(Map<String, String> childParameterValues) {
				this.childParameterValues = childParameterValues;
			}

			public Boolean getChildParameterFlag() {
				return childParameterFlag;
			}

			public void setChildParameterFlag(Boolean childParameterFlag) {
				this.childParameterFlag = childParameterFlag;
			}

			public String getControlParameterDescription() {
				return controlParameterDescription;
			}

			public void setControlParameterDescription(String controlParameterDescription) {
				this.controlParameterDescription = controlParameterDescription;
			}

			public String getNameonform() {
				return nameonform;
			}

			public void setNameonform(String nameonform) {
				this.nameonform = nameonform;
			}

			public Map<String, String> getWarehouse() {
				return warehouse;
			}

			public void setWarehouse(Map<String, String> warehouse) {
				this.warehouse = warehouse;
			}

			public List getMultiplWareHouseValue() {
				return multiplWareHouseValue;
			}

			public void setMultiplWareHouseValue(List multiplWareHouseValue) {
				this.multiplWareHouseValue = multiplWareHouseValue;
			}

			


}


