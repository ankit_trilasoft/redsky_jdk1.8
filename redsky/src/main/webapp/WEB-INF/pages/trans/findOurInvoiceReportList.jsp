<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head> 
    <title><fmt:message key="formsList.title"/></title> 
    <meta name="heading" content="<fmt:message key='formsList.heading'/>"/> 
    
<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['searchForm'].elements['reports.menu'].value = "";
	document.forms['searchForm'].elements['reports.description'].value = "";
}	


</script>

<style type="text/css">
div#page {
margin:15px 0px 0px 10px;
padding:0pt;
text-align:center;
}
#mainPopup {
padding-left:0px;
padding-right:0px;
}

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
!margin-top:-18px;
padding:2px 0;
text-align:right;
width:100%;
}


</style>
	
</head>

<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b>Forms List</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<s:form id="searchForm" name="searchForm" action="searchReportsModule.html?decorator=popup&popup=true" method="post" validate="true">   
<s:set name="reportss" value="reportss" scope="request"/> 
<c:set var="for" value="<%=request.getParameter("for") %>" />


<s:hidden name="claimNumber" value="<%=request.getParameter("claimNumber") %>" />
<c:set var="claimNumber" value="<%=request.getParameter("claimNumber") %>" />

<s:hidden name="bookNumber" value="<%=request.getParameter("bookNumber") %>" />
<c:set var="bookNumber" value="<%=request.getParameter("bookNumber") %>" />

<s:hidden name="noteID" value="<%=request.getParameter("noteID") %>" />
<c:set var="noteID" value="<%=request.getParameter("noteID") %>" />

<s:hidden name="custID" value="<%=request.getParameter("custID") %>" />
<c:set var="custID" value="<%=request.getParameter("custID") %>" />


<s:hidden name="recInvNumb" value="<%=request.getParameter("recInvNumb") %>"/>
<c:set var="recInvNumb" value="<%=request.getParameter("recInvNumb") %>" />

<s:hidden name="jobNumber" value="<%=request.getParameter("jobNumber")%>"/>
<c:set var="jobNumber" value="<%=request.getParameter("jobNumber") %>" />
<s:hidden name="jobType" value="<%=request.getParameter("jobType") %>"/>
<c:set var="jobType" value="<%=request.getParameter("jobType") %>" />
<s:set name="reportss" value="reportss" scope="request"/>


<display:table name="reportss" class="table" requestURI="" id="reportsList" style="width:450px;margin-top:42px;" defaultsort="1" >
<c:if test="${reportModule!='Groupage'}">  
<display:column title="Invoice Number" style="width:50px" >
   <a href="javascript:openReport('${reportsList.id}','${claimNumber}','${recInvNumb}','${jobNumber}','${bookNumber}','${reportsList.description}','${reportsList.docsxfer}','${jobType}');">
   <c:out value="${recInvNumb}"></c:out>
</display:column>
</c:if>   
<c:if test="${reportModule=='Groupage'}">  
<display:column title="G/O#" style="width:50px" > 
   	<a href="javascript:openReport('${reportsList.id}','${claimNumber}','${recInvNumb}','${jobNumber}','${bookNumber}','${reportsList.description}','${reportsList.docsxfer}','${jobType}');">
     <c:out value="${recInvNumb}"></c:out>
</display:column>
</c:if>   
<display:column property="description" title="Form Name" style="width:100px"/>
<display:column title="PDF" style="width:50px">	
		  <c:if test="${(reportsList.runTimeParameterReq == 'No' && reportsList.pdf == true)}">    	
				<img alt="PDF File" src="<c:url value='/images/pdf1.gif'/>" onclick="validatefields('${reportsList.id}','${recInvNumb}','${jobNumber}','${reportsList.description}','${reportsList.docsxfer}','${jobType}','PDF');"/>
			</c:if>
</display:column>
<display:column title="DOC" style="width:50px">
	<c:if test="${(reportsList.runTimeParameterReq == 'No' && reportsList.rtf == true)}">	
		<img alt="RTF File" src="<c:url value='/images/doc.gif'/>" onclick="validatefields('${reportsList.id}','${recInvNumb}','${jobNumber}','${reportsList.description}','${reportsList.docsxfer}','${jobType}','RTF');"/>
	</c:if>
</display:column>
<display:column title="DOCX" style="width:50px">
	<c:if test="${(reportsList.runTimeParameterReq == 'No' && reportsList.docx == true)}">	
		<img alt="DOCX File" src="<c:url value='/images/docx.gif'/>" onclick="validatefields('${reportsList.id}','${recInvNumb}','${jobNumber}','${reportsList.description}','${reportsList.docsxfer}','${jobType}','DOCX');"/>
	</c:if>
</display:column>
	<display:column title="Upload" style="width:50px">
		<c:if test="${(reportsList.docsxfer == 'Yes')}">
			<img align="middle" onclick="javascript:window.open('viewFormParam.html?id=${reportsList.id}&invoiceNumber=${recInvNumb}&jobNumber=${jobNumber}&docsxfer=${reportsList.docsxfer}&reportModule=${reportsList.module}&reportSubModule=${reportsList.subModule}&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=120, scrollbars=yes,resizable=yes')" style="text-align:center;padding-left:5px;" src="${pageContext.request.contextPath}/images/upload.png"/>
		</c:if> 
</display:column>
<display:column title="Email" style="width:50px">
						<c:url value="viewFormParamEmailSetup.html" var="url1" >
							<c:param name="id" value="${reportsList.id}"/>
							<c:param name="claimNumber" value="${claimNumber}"/>
							<c:param name="cid" value="${custID}"/>
							<c:param name="jobNumber" value="${jobNumber}"/>
							<c:param name="regNumber" value="${regNumber}"/>
							<c:param name="bookNumber" value="${bookNumber}"/>
							<c:param name="noteID" value="${noteID}"/>
							<c:param name="custID" value="${custID}"/>
							<c:param name="docsxfer" value="${reportsList.emailOut}"/>
							<c:param name="reportModule" value="${reportsList.module}"/>
							<c:param name="reportSubModule" value="${reportsList.subModule}"/>
							<c:param name="formReportFlag" value="F"/>
							<c:param name="formNameVal" value="${reportsList.description}"/>							
							<c:param name="decorator" value="popup"/>
							<c:param name="popup" value="true"/>
						</c:url>		 

	<c:if test="${(reportsList.emailOut == 'Yes')}">
		<a><img align="middle" title="Email" onclick="winClose('${reportsList.id}','${recInvNumb}','${claimNumber}','${custID}','${jobNumber}','${regNumber}','${bookNumber}','${noteID}','${custID}','${reportsList.emailOut}','${reportsList.module}','${reportsList.subModule}','F','${reportsList.description}');" src="${pageContext.request.contextPath}/images/email_go.png"/></a>
	</c:if> 
</display:column>

</display:table>
 
</s:form>

<!-- <c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if> -->
 
<script type="text/javascript"> 
    highlightTableRows("reportsList");
</script> 