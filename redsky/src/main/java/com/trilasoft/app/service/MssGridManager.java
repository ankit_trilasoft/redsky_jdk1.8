package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.MssGrid;

public interface MssGridManager extends GenericManager<MssGrid, Long>{
	
	public List findRecord(Long id, String sessionCorpID);
    public void deleteDataOfInventoryData(String inventoryFlag);
}
