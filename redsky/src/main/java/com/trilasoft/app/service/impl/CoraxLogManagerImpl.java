package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CoraxLogDao;
import com.trilasoft.app.model.CoraxLog;
import com.trilasoft.app.service.CoraxLogManager;

public class CoraxLogManagerImpl extends GenericManagerImpl<CoraxLog, Long> implements CoraxLogManager {
	CoraxLogDao coraxLogDao;
	
	public CoraxLogManagerImpl(CoraxLogDao coraxLogDao) {
		super(coraxLogDao);
		this.coraxLogDao=coraxLogDao;
		// TODO Auto-generated constructor stub
	}
	
	public List getByCorpID(String sessionCorpID )
	{
		return coraxLogDao.getByCorpID( sessionCorpID );
	} 

}
