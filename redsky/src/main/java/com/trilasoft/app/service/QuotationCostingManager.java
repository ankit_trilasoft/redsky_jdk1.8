package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.QuotationCosting;

public interface QuotationCostingManager extends GenericManager<QuotationCosting, Long> { 
       public List checkById(Long id);
       public List<QuotationCosting> findBySequenceNumber(String sequenceNumber);
}