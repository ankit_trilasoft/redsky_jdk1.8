package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.RoleAccessDao;
import com.trilasoft.app.model.RoleAccess;

public class RoleAccessDaoHibernate extends GenericDaoHibernate<RoleAccess, Long> implements RoleAccessDao {

	public RoleAccessDaoHibernate() {
		super(RoleAccess.class);
	}

	public List<RoleAccess> findByCorpID(String corpId) {
		return getHibernateTemplate().find("from RoleAccess where corpID=?",corpId);
	}
}
