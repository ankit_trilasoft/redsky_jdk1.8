<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>

<meta name="heading" content="<fmt:message key='notesControl.heading'/>"/> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/redskyditor/tiny_mce.js"></script>
<title><fmt:message key="notesControl.title"/></title> 
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</head>

<style type="text/css">h2 {background-color: #FBBFFF}
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<script language="JavaScript">	
function closeForm(){ 
	parent.window.opener.document.location.reload();
	window.self.close();
	
}
function checkFields(){
	if(document.forms['rulesNotes'].elements['notes.subject'].value.trim()==''){
		alert('Please enter subject');
		return false;
	}else{
		var rules=document.forms['rulesNotes'].elements['notes.subject'].value;
		if (rules!=''||rules!=null){
			var agree = confirm("Do you want to save the note and continue? press ok to continue with save or press Cancel");
	     	if(agree){
			document.forms['rulesNotes'].action = 'saveNotesfromRules.html?decorator=popup&popup=true';
			document.forms['rulesNotes'].submit();
			}else{
				return false;
			}	
		}	
	}
}

window.onload = function(){
	<c:if test="${saveFlag=='TRUE'}">
	closeForm();
	</c:if>
}

</SCRIPT>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="javascript" type="text/javascript">
		function setTimeMask() {
		oTimeMask1 = new Mask("##:##", "remindTime");
		oTimeMask1.attach(document.forms['rulesNotes'].remindTime);
		}
		function completeTimeString() {
			stime1 = document.forms['rulesNotes'].elements['notes.remindTime'].value;
			if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
				if(stime1.length==1 || stime1.length==2){
					if(stime1.length==2){
						document.forms['rulesNotes'].elements['notes.remindTime'].value = stime1 + ":00";
					}
					if(stime1.length==1){
						document.forms['rulesNotes'].elements['notes.remindTime'].value = "0" + stime1 + ":00";
					}
				}else{
					document.forms['rulesNotes'].elements['notes.remindTime'].value = stime1 + "00";
				}
			}
			
		}
		function IsValidTime(value) {
			var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
			var timeStr = document.forms['rulesNotes'].elements['notes.remindTime'].value;
			var matchArray = timeStr.match(timePat);
			if (document.forms['rulesNotes'].elements['notes.forwardDate'].value == '' || document.forms['rulesNotes'].elements['notes.remindTime'].value == '' || document.forms['rulesNotes'].elements['notes.remindTime'].value == '00:00' || document.forms['rulesNotes'].elements['notes.followUpFor'].value == '' || document.forms['rulesNotes'].elements['notes.remindInterval'].value == '') {
				if(document.forms['rulesNotes'].elements['notes.forwardDate'].value == '' && (document.forms['rulesNotes'].elements['notes.remindTime'].value == '' || document.forms['rulesNotes'].elements['notes.remindTime'].value=='00:00')){
					alert ("Please select follow up date and time.");
					document.forms['rulesNotes'].elements['notes.forwardDate'].focus();
					return false;
				}else {
					if(document.forms['rulesNotes'].elements['notes.forwardDate'].value == ''){
						alert ("Please select follow up date");
						document.forms['rulesNotes'].elements['notes.forwardDate'].focus();
						return false;
					}
					if(document.forms['rulesNotes'].elements['notes.remindTime'].value == '' || document.forms['rulesNotes'].elements['notes.remindTime'].value == '00:00'){
						alert ("Please select follow up time");
						document.forms['rulesNotes'].elements['notes.remindTime'].focus();
						return false;
					}
					if(document.forms['rulesNotes'].elements['notes.followUpFor'].value == ''){
						alert ("Please select follow up For ");
						document.forms['rulesNotes'].elements['notes.followUpFor'].focus();
						return false;
					}
					if(document.forms['rulesNotes'].elements['notes.remindInterval'].value == ''){
						alert ("Please select reminder interval");
						document.forms['rulesNotes'].elements['notes.remindInterval'].focus();
						return false;
					}
				}
			}
			if (matchArray == null) {
			alert("Time is not in a valid format.");
			document.forms['rulesNotes'].elements['notes.remindTime'].value = '';
			document.forms['rulesNotes'].elements['notes.remindTime'].focus();
			//document.forms['rulesNotes'].elements['remindBtn'].disabled = true ;
			return false;
			}
			hour = matchArray[1];
			minute = matchArray[2];
			second = matchArray[4];
			ampm = matchArray[6];

			if (second=="") { second = null; }
			if (ampm=="") { ampm = null }
			if (hour < 0  || hour > 23) {
			alert("Follow up  time must be between 0 and 23(Hrs)");
			document.forms['rulesNotes'].elements['notes.remindTime'].value = '';
			document.forms['rulesNotes'].elements['notes.remindTime'].focus();
			return false;
			}
			if (minute<0 || minute > 59) {
			alert ("Follow up  time must be between 0 and 59(Mins)");
			document.forms['rulesNotes'].elements['notes.remindTime'].value = '';
			document.forms['rulesNotes'].elements['notes.remindTime'].focus();
			return false;
			}
			if (second != null && (second < 0 || second > 59)) {
			alert ("Second must be between 0 and 59.");
			document.forms['rulesNotes'].elements['notes.remindTime'].value = '';
			document.forms['rulesNotes'].elements['notes.remindTime'].focus();
			return false;
			}
			var date1 = document.forms['rulesNotes'].elements['notes.forwardDate'].value; 
			var systenDate = new Date();
			var mySplitResult = date1.split("-");
		   	var day = mySplitResult[0];
		   	var month = mySplitResult[1];
		   	var year = mySplitResult[2];
		 	if(month == 'Jan'){
		       month = "01";
		   	}else if(month == 'Feb'){
		       month = "02";
		   	}else if(month == 'Mar'){
		       month = "03"
		   	}else if(month == 'Apr'){
		       month = "04"
		   	}else if(month == 'May'){
		       month = "05"
		   	}else if(month == 'Jun'){
		       month = "06"
		   	}else if(month == 'Jul'){
		       month = "07"
		   	}else if(month == 'Aug'){
		       month = "08"
		   	}else if(month == 'Sep'){
		       month = "09"
		   	}else if(month == 'Oct'){
		       month = "10"
		   	}else if(month == 'Nov'){
		       month = "11"
		   	}else if(month == 'Dec'){
		       month = "12";
		   	}
		   	var finalDate = month+"-"+day+"-"+year;
		   	date1 = finalDate.split("-");
		  	var enterDate = new Date(date1[0]+"/"+date1[1]+"/20"+date1[2]);
		  	var newSystenDate = new Date(systenDate.getMonth()+1+"/"+systenDate.getDate()+"/"+systenDate.getFullYear());
		  	var daysApart = Math.round((enterDate-newSystenDate)/86400000);
		  	if(daysApart < 0){
		    	alert("Cannot Enter Past Date - Please Re-Enter");
		    	document.forms['rulesNotes'].elements['notes.forwardDate'].value='';
		    	return false;
		  	}
		  	if(date1 != ''){
		  		if(daysApart == 0){
		  			var time = document.forms['rulesNotes'].elements['notes.remindTime'].value;
					var hour = time.substring(0, time.indexOf(":"))
					var min = time.substring(time.indexOf(":")+1, time.length);
		  			
		  			if (hour < 0  || hour > 23) {
						alert("Follow up  time must be between 0 and 23(Hrs)");
						document.forms['rulesNotes'].elements['notes.remindTime'].value = '00:00'
						document.forms['rulesNotes'].elements['notes.remindTime'].focus();
						return false;
					}
					if (min<0 || min > 59) {
						alert ("Follow up  time must be between 0 and 59(Mins)");
						document.forms['rulesNotes'].elements['notes.remindTime'].value = '00:00'
						document.forms['rulesNotes'].elements['notes.remindTime'].focus();
						return false;
					}
					
					if(systenDate.getHours() > hour){
						document.forms['rulesNotes'].elements['notes.remindTime'].value = '00:00'
						alert("Cannot Enter Past Time - Please Re-Enter");
						return false;
					}else if(systenDate.getHours() == hour && systenDate.getMinutes() > min){
						document.forms['rulesNotes'].elements['notes.remindTime'].value = '00:00'
						alert("Cannot Enter Past Time - Please Re-Enter");
						return false;
					}
		  		}
				}
		  	if(value=='setfollowup'){
			document.forms['rulesNotes'].action = 'saveNotesfromRules.html?decorator=popup&popup=true';
			document.forms['rulesNotes'].submit();
		  	}
			return false;
			}
		function onlyTimeFormatAllowed(evt)
		{
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
		}
</script>
 <script type="text/javascript">
			// Use it to attach the editor to all textareas with full featured setup
			//WYSIWYG.attach('all', full);
			
			// Use it to attach the editor directly to a defined textarea
			<configByCorp:fieldVisibility componentId="component.field.partnerUser.showEditor">
			 tinyMCE.init({
			
			 mode : "textareas",
			 theme : "advanced",
			 relative_urls : false,
			 remove_script_host : false,
			 plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
			 
			 theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,cleanup,code,|,insertdate,inserttime|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
				 theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : true,
					// Example content CSS (should be your site CSS)
					content_css : "css/content.css",

					// Drop lists for link/image/media/template dialogs
					template_external_list_url : "lists/template_list.js",
					external_link_list_url : "lists/link_list.js",
					external_image_list_url : "lists/image_list.js",
					media_external_list_url : "lists/media_list.js"
			 }) 
			 </configByCorp:fieldVisibility>
			 function setbtnType(targetElement)
			 {
				   document.forms['rulesNotes'].elements['btntype'].value = targetElement.value;
				   document.forms['rulesNotes'].elements['remindMe'].value = "remind";
			 }
		</script> 
<s:form id="rulesNotes" name="rulesNotes"  action="" validate="true" >
<s:hidden name="notes.id" />
<s:hidden name="notes.noteStatus" />
<s:hidden name="notes.notesId" />
<s:hidden name="notes.toDoRuleId" />
<s:hidden name="notes.customerNumber" />
<s:hidden name="notes.name" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
 <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="fileNumber" value="<%=request.getParameter("fileNumber") %>"/>
<s:hidden name="noteType" value="<%=request.getParameter("noteType") %>"/>
<s:hidden name="noteSubType" value="<%=request.getParameter("noteSubType") %>"/>
<c:set var="noteType" value="<%=request.getParameter("noteType") %>" />
<c:set var="noteSubType" value="<%=request.getParameter("noteSubType") %>" />
<c:if test="${noteType!=''}">
<s:hidden name="notes.noteType" value="<%=request.getParameter("noteType") %>"/>
</c:if>

<c:if test="${noteType==''}">
<s:hidden name="notes.noteType"/>
</c:if>

<c:if test="${noteSubType!=''}">
<s:hidden name="notes.noteSubType" value="<%=request.getParameter("noteSubType") %>"/>
</c:if>
<c:if test="${noteSubType==''}">
<s:hidden name="notes.noteSubType"/>
</c:if>
<s:hidden name="notes.notesKeyId" />
<s:hidden name="resultRecordId" value="<%=request.getParameter("resultRecordId") %>"/>
<s:hidden name="checkListType" value="<%=request.getParameter("checkListType") %>"/>
 <table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0" style="height: 150px; width: 600px;">
  <tbody>
  <tr>
	<td align="left" colspan="3" class="subcontent-tab">Note</td>
</tr>
  <tr>
  	<td align="left" class="listwhitetext">
	  			  
		<table class="detailTabLabel" border="0">
		<tbody> 
			  <tr>
			  		<td align="left" class="listwhitetext"><fmt:message key="notes.subject"/><font color="red" size="2">*</font></td>
			  		<td align="left" ><s:textfield cssClass="input-text"  cssStyle="height:16px" id="subject" name="notes.subject" size="98" maxlength="75" /></td>
			  	</tr>
			  
			  
			  	<tr>
			  		<td align="left" class="listwhitetext" valign="top"><fmt:message key="notes.note"/></td>
			  		<td align="left" colspan="4"><s:textarea name="notes.note" cssClass="textarea"   cols="122" rows="10" readonly="false"/></td>
			  	</tr>
		</tbody>
  </table>	  
<c:if test="${checkListType!='ForCheckList'}">
  <div class="mainDetailTable" style="width:746px;">
					<table  class="colored_bg" style="width:100%; !width:100%; margin:0px; padding-bottom:5px;"  border="0">
					<tbody>
					<tr><td align="right"><img style="cursor:default" src="${pageContext.request.contextPath}/images/clock-blue.gif" /></td><td class="listwhitetext" colspan="3"><font size="2"><b>Follow Up</b></font></td></tr>
						<tr>					
							<td align="right" class="listwhitetext" style="width:68px; !width:65px; !padding-right:0;">On</td>
							
							<td align="left" colspan="2">
							<table border="0" class="detailTabLabel" cellpadding="0" cellspacing="0">
							<tr>
							<c:if test="${empty notes.forwardDate}">
							<td align="left" width="63px" style="!margin-left:-10px;"><s:textfield cssClass="input-text"  id="forwardDate" name="notes.forwardDate" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<c:if test="${userType=='USER'}">
							<td align="left" width="20px" ><img id="forwardDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</c:if>
							<c:if test="${not empty notes.forwardDate}">
							<s:text id="notesForwardDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.forwardDate"/></s:text>
							<td align="left" width="63px"><s:textfield cssClass="input-text"  id="forwardDate" name="notes.forwardDate" value="%{notesForwardDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<c:if test="${userType=='USER'}">
							<td align="left" width="20px"><img id="forwardDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</c:if>
							</tr>
							</table>
							</td>
							
							<td align="right" class="listwhitetext" width="20px">@</td>
							
							<td align="left" colspan="2">
							<table border="0" class="detailTabLabel" cellpadding="0" cellspacing="0">
							<tr>
							<td align="left" class="listwhitetext" width="40px"><s:textfield cssClass="input-text" id="remindTime" name="notes.remindTime" size="2" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onclick="setTimeMask();" onchange = "completeTimeString(); return IsValidTime('remindTime');"/></td>
							<td width="10px"><img id="calender" style="cursor: default" src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=15 WIDTH=15 "/></td>
							<td width="10px"></td>
							<td align="right" class="listwhitetext" width="20px">For</td>
							<td align="left" class="listwhitetext" width="50px" colspan="2"><s:select cssClass="list-menu" name="notes.followUpFor" list="%{all_user}" cssStyle="width:141px" headerKey="" headerValue="" /></td>
							<td align="right" class="listwhitetext" width="60px">Remind&nbsp;@</td>
							<td align="left" class="listwhitetext" width="100px"><s:select cssClass="list-menu" name="notes.remindInterval" list="%{remindIntervals}" cssStyle="width:95px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							<td align="left" class="listwhitetext" style=""><input type="submit" name="remindBtn" class="cssbutton"  value="Set Follow Up" style="width:87px; height:25px" onclick="setbtnType(this);completeTimeString();return IsValidTime('setfollowup'); return false"/></td>
							<s:hidden name="remindMe" />
							<s:hidden name="btntype" />
							</tr>
							</table>
							</td>
							
						</tr>
							</tbody>
				</table>	
				</div>	</c:if>	
		</td>
		</tr>
			<table border="0">
					<tbody>
							<tr>
							<td align="left" class="listwhitetext"><b><fmt:message key='notes.createdOn'/></b></td>
							<td ><fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${notes.createdOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="notes.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${notes.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>
							<td width="2px"></td>		
							<td align="left" class="listwhitetext"><b><fmt:message key='notes.createdBy' /></b></td>
							<c:if test="${not empty notes.id}">
								<s:hidden name="notes.createdBy"/>
								<td ><s:label name="createdBy" value="%{notes.createdBy}"/></td>
							</c:if>
							<c:if test="${empty notes.id}">
								<s:hidden name="notes.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							<td width="2px"></td>
							<td align="left" class="listwhitetext"><b><fmt:message key='notes.updatedOn'/></b></td>
							<td ><fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${notes.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="notes.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td ><fmt:formatDate value="${notes.updatedOn}" pattern="${displayDateTimeFormat}"/>
							</td>
							<td width="2px"></td>
							<td align="left" class="listwhitetext"><b><fmt:message key='notes.updatedBy' /></b></td>
							<c:if test="${not empty notes.id}">
								<s:hidden name="notes.updatedBy"/>
								<td ><s:label name="updatedBy" value="%{notes.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty notes.id}">
								<s:hidden name="notes.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
			<table class="detailTabLabel" border="0">
				<tbody>					
				<tr align="center">
				<td align="left"> 
				<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save"  onclick="return checkFields();"/>
				<input type="button"   value="Close" class="cssbutton1" name="Close" onclick="self.close()"/>
					
				</td>   
			</tr>
		</tbody>
	</table>
			
		</tr>
	</tbody>
</table>
<div id="mydiv" style="position:absolute"></div>
</s:form>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>