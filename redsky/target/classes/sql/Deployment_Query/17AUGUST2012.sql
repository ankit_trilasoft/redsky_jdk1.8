ALTER TABLE `redsky`.`accountassignmenttype` ADD INDEX `Index_2`(`assignment`, `partnerCode`, `corpID`);

alter table workticket add column description varchar(400);

alter table miscellaneous add column estimatedExpense decimal(19,2);

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('STAR', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('STAR', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('SSCW', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('SSCW', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('VOER', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOER', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('VORU', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VORU', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('VOCZ', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOCZ', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('ADAM', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('ADAM', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('UTSI', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UTSI', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('UGHK', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGHK', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('BOUR', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BOUR', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('JOHN', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('JOHN', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('UGCN', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGCN', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('VOAO', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Existing Customer','Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('VOAO', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('CWMS', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('CWMS', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('UGMY', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGMY', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('UGSG', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGSG', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('BONG', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('BONG', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('UGWW', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('UGWW', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('RSKY', 'Cold Call', 'Cold Call', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Existing Customer', 'Existing Customer', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Self Generated', 'Self Generated', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Employee', 'Employee', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Partner', 'Partner', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Conference', 'Conference', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Trade Show', 'Trade Show', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Web Site', 'Web Site', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Word of mouth', 'Word of mouth', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Email', 'Email', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', '3299 Form', '3299 Form', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Customer Referral', 'Customer Referral', 20, 'CRMLead',now(),'naveen',now(),'naveen'),
('RSKY', 'Other', 'Other', 20, 'CRMLead',now(),'naveen',now(),'naveen');

insert into parametercontrol(corpID, parameter, comments, fieldLength, active, createdon, createdby, updatedon, updatedby, tsftFlag, hybridFlag, customType) values('TSFT', 'CRMLead', '', 20, b'1', now(), 'nkumar', now(), 'nkumar', 1, 1, '');

 

