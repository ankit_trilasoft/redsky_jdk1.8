<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title>Profile Information</title>
<meta name="heading" content="Profile Information" />
<style type="text/css">
input[type="checkbox"] {vertical-align: bottom; margin:0px;}
.table td, .table th, .tableHeaderTable td {border: 1px solid #cbcbcb; padding: 0.3em;}
.table thead th, .tableHeaderTable td {height:16px;}
.table tbody tr:hover, .table tr.over, .contribTable tr:hover {
    background-color: #fff !important; border-bottom: none !important;border-top: none !important; color: #000000; cursor: default;}
.input-text { background-color: #fff !important;border:none;font-size:11px;}
.input-text:hover {background-color: #fff !important;border:none;}
textarea.textarea {font-size: 11px;}
.listwhitetext2 {background-color:#FFFFFF;color:#444444;font-family:arial,verdana;font-size:11px;font-weight:normal;text-decoration:none;}
.bluefieldset {border:1px solid #219DD1;}
</style>
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script>
function changeStatus(){
	document.forms['partnerProfileInfo'].elements['formStatus'].value = '1';
}
</script>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('GeneralInfo', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('IntrnationalTrms', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('accounting', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('billing', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('IntBillingInfo', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('pricing', 'fade=0,persist=1,show=1')
//animatedcollapse.addDiv('IntPricing', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('transitIns', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('insurance', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('InvTerms', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('reportDetails', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('AuthExpProcess', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('intlTranAddInfo', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('reportAddInfo', 'fade=0,persist=1,show=1')
animatedcollapse.addDiv('invoiceAddInfo', 'fade=0,persist=1,show=1')
animatedcollapse.init()
</script>
</head>

<s:form id="partnerProfileInfo"  name="partnerProfileInfo" action="" method="post" validate="true" >
	<s:hidden name="popupval" value="${papam.popup}" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
	<s:hidden name="partnerProfileInfo.partnerCode" />
	<s:hidden name="partnerProfileInfo.id" />
	<s:hidden name="partnerProfileInfo.corpId" />
	<s:hidden name="partnerProfileInfo.partnerPrivateId" />
	<s:hidden name="secondDescription" />
		<s:hidden name="thirdDescription" />
		<s:hidden name="firstDescription" />
		<s:hidden name="fourthDescription" />
		<s:hidden name="fifthDescription" />
		<s:hidden name="sixthDescription" />
		<s:hidden name="seventhDescription"/>
 <c:if test="${empty partnerPrivate.id}">
	<c:set var="isTrue" value="false" scope="request"/>
 </c:if>
<c:if test="${not empty partnerPrivate.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value="" />

	<div id="newmnav" style="!margin-bottom:3px;">
	<ul>
		<c:if test="${partnerType == 'AC'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=AC"><span>Account Detail</span></a></li>
			<li><a href="editNewAccountProfile.html?id=${partnerId}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
			<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Profile Information</span></a></li>
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<c:if test="${not empty partnerPrivate.id}">
			<configByCorp:fieldVisibility componentId="component.standard.accountContactTab">
				<li><a href="accountContactList.html?id=${partnerId}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
				</configByCorp:fieldVisibility>
				<c:if test="${checkTransfereeInfopackage==true}">
				<li><a href="editContractPolicy.html?id=${partnerId}&partnerType=${partnerType}"><span>Policy</span></a></li>
				</c:if>
			</c:if>
			<c:if test="${partnerPublic.partnerPortalActive == true}">
			 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
			<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
			</configByCorp:fieldVisibility>
			</c:if>
			<c:if test="${paramValue == 'View'}">
					<li><a href="searchPartnerView.html"><span>Public List</span></a></li>
			</c:if>
			<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
			</c:if>
			<li><a href="partnerReloSvcs.html?id=${partnerId}&partnerType=${partnerType}"><span>Services</span></a></li>
			<c:if test="${not empty partnerPublic.id}">
				<c:url value="frequentlyAskedQuestionsList.html" var="url">
					<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
					<c:param name="partnerType" value="AC"/>
					<c:param name="partnerId" value="${partnerPublic.id}"/>
					<c:param name="lastName" value="${partnerPublic.lastName}"/>
					<c:param name="status" value="${partnerPublic.status}"/>
				</c:url>				 
				<li><a href="${url}"><span>FAQ</span></a></li>
			<c:if test="${partnerPublic.partnerPortalActive == true}">
				<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
			</c:if>
			</c:if>
				
				<c:if test="${not empty partnerPrivate.id}">
					<li><a onclick="openNotesPopupTab(this);"><span>Notes</span></a></li>
				</c:if>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.tab.partner.preferredAgent.show">
				<c:if test="${partnerType == 'AC'}">
						<li><a href="searchPreferredAgentList.html?partnerId=${partnerPublic.id}&partnerCode=${partnerCode}&partnerType=${partnerType}"><span>Preferred Agent</span></a></li>
				</c:if>
		</configByCorp:fieldVisibility>
	</ul>
	</div>
	<div class="spn">&nbsp;</div>
	<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
	<div id="content" align="center">
	<div id="liquid-round-top">
	<div class="top"><span></span></div>
		
	<!-- Start AccoutType Section	-->
	
	<c:if test="${partnerType == 'AC'}">
		<div class="center-content">
			<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;margin-bottom:0px;">
				<tbody>
					<tr>						
						<td colspan="20" width="100%" align="left" style="margin: 0px">
     						
			 					<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 					<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd" style="cursor:default; ">&nbsp;&nbsp;General Information
            						</td> 
            						<td class="headtab_bg_center" style="cursor:default; ">
<a href="javascript:animatedcollapse.hide(['IntrnationalTrms', 'accounting', 'billing', 'IntBillingInfo','pricing','transitIns','insurance','InvTerms','reportDetails','AuthExpProcess','intlTranAddInfo','reportAddInfo','invoiceAddInfo'])">Collapse All<img src="${pageContext.request.contextPath}/images/section_contract.png" HEIGHT=14 WIDTH=14 align="top"></a> | <a href="javascript:animatedcollapse.show(['IntrnationalTrms', 'accounting', 'billing', 'IntBillingInfo','pricing','transitIns','insurance','InvTerms','reportDetails'])">Expand All<img src="${pageContext.request.contextPath}/images/section_expanded.png" HEIGHT=14 WIDTH=14 align="top"></a>
</td>           
            						<td class="headtab_right"></td>
          						</tr>
								</table>
					  		
					  		<div id="">
					  		<table class="table" border="0" style="margin:0px;border:1px solid #74b3dc;width:auto !important;">
						<tr>
						<td align="right" class="listwhitetext" style="width:133px;text-align:right;"><b>Services&nbsp;Provided</b></td>
						<td class="listwhitetext" style="width:120px;text-align:left;">
						<c:set var="isdomFlag" value="false" />
						<c:if test="${partnerProfileInfo.dom}">
										<c:set var="isdomFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.dom" fieldValue="true" value="${isdomFlag}"  tabindex="" />
						<b><fmt:message key='partnerProfileInfo.dom' /></b>
						</td>
						<td  class="listwhitetext" style="width:120px;text-align:left;">
						<c:set var="isintlFlag" value="false" />
						<c:if test="${partnerProfileInfo.intl}">
									<c:set var="isintlFlag" value="true" />
								</c:if>
								<s:checkbox key="partnerProfileInfo.intl" fieldValue="true" value="${isintlFlag}"  tabindex="" />
								<b><fmt:message key='partnerProfileInfo.intl' /></b>
								</td>
						<td class="listwhitetext" style="width:125px;text-align:left;">
						<c:set var="iscomFlag" value="false" />
						<c:if test="${partnerProfileInfo.com}">
									<c:set var="iscomFlag" value="true" />
								</c:if>
								<s:checkbox key="partnerProfileInfo.com" fieldValue="true" value="${iscomFlag}"  tabindex="" />
								<b><fmt:message key='partnerProfileInfo.com' /></b>
								</td>
						<td class="listwhitetext" style="width:142px;text-align:left;">
						<c:set var="isrloFlag" value="false" />
						<c:if test="${partnerProfileInfo.rlo}">
									<c:set var="isrloFlag" value="true" />
								</c:if>
								<s:checkbox key="partnerProfileInfo.rlo" fieldValue="true" value="${isrloFlag}"  tabindex="" />
								<b><fmt:message key='partnerProfileInfo.rlo' /></b>
								</td>
								
								<td class="listwhitetext" style="width:120px;text-align:left;">
								<c:set var="isothFlag" value="false" />
								<c:if test="${partnerProfileInfo.oth}">
									<c:set var="isothFlag" value="true" />
								</c:if>
								<s:checkbox key="partnerProfileInfo.oth" fieldValue="true" value="${isothFlag}"  tabindex="" />
								<b><fmt:message key='partnerProfileInfo.oth' /></b>
								</td>
								<td  align="left" style="text-align:right;" class="listwhitetext" ><b><fmt:message key='partnerProfileInfo.othComnts' /></b></td>
							<td style="text-align:left;" class="listwhitetext" >
								<s:textfield cssClass="input-text" id="partnerProfileInfo.othComnts" name="partnerProfileInfo.othComnts" maxLength="500" cssStyle="width:265px;" size="35"  tabindex=""/>
							</td>
						
						</tr>
							
							
							</table>
							<table width="100%" cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px;">
								<tbody><tr class="pricetab_bg">
								<td width="215px" align="center" style="color:#0084c8;"><b>&nbsp;Service&nbsp;Providers</b></td>
								</tr>
								</tbody></table>
						
							
						
						<table style="margin:0px;border:2px solid #74b3dc;width:auto !important;margin-bottom:4px;" class="table">
						<thead>
						<tr style="height:16px;">
						<th class="listwhitetext" style="text-align:right;"><b>Agent</b></th>
						<th class="listwhitetext" style="text-align:left;"><b>Company&nbsp;Name</b></th>
						<th class="listwhitetext" style="text-align:left;"><b>Contact</b></th>
						<th class="listwhitetext" style="text-align:left;"><b>Phone</b></th>
						<th class="listwhitetext" style="text-align:left;"><b>Email&nbsp;Address</b></th>
						<th class="listwhitetext" style="text-align:center;"><b>Various Contacts</b></th>						
						</tr>
						</thead>
						<tbody>
						<tr>
						<td class="listwhitetext" style="text-align:right;"><b><fmt:message key='partnerProfileInfo.rmc' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.agentCompanyName" name="partnerProfileInfo.agentCompanyName" title="${partnerProfileInfo.agentCompanyName}" maxLength="45"  size="30"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.agentContact" name="partnerProfileInfo.agentContact"    maxLength="45" size="30" tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.agentPhone" name="partnerProfileInfo.agentPhone"   maxLength="30" size="30" tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.agentEmail" name="partnerProfileInfo.agentEmail" title="${partnerProfileInfo.agentEmail}" maxLength="45"  size="30" tabindex="" onchange="checkEmail(this,'partnerProfileInfo.agentEmail')"/></td>
						<td class="listwhitetext" rowspan="7" style="vertical-align:top">
						<s:textarea name="partnerProfileInfo.variousContacts" id="variousContacts" required="true" cssClass="textarea" tabindex="1" cssStyle="width:255px;height:200px;"  />
						</td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right;"><b><fmt:message key='partnerProfileInfo.auditform' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.auditfirmCompanyName" name="partnerProfileInfo.auditfirmCompanyName" title="${partnerProfileInfo.auditfirmCompanyName}" size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.auditfirmContact" name="partnerProfileInfo.auditfirmContact"  size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.auditfirmPhone" name="partnerProfileInfo.auditfirmPhone"  size="30" maxLength="30" tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.auditfirmEmail" name="partnerProfileInfo.auditfirmEmail" size="30" title="${partnerProfileInfo.auditfirmEmail}"  maxLength="45"  tabindex="" onchange="checkEmail(this,'partnerProfileInfo.auditfirmEmail')"/></td>
						
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right;width:144px;"><b><fmt:message key='partnerProfileInfo.hhgProviderDomestic' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderDomCompanyName" name="partnerProfileInfo.hhgProviderDomCompanyName" title="${partnerProfileInfo.hhgProviderDomCompanyName}" size="30"  maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderDomContact" name="partnerProfileInfo.hhgProviderDomContact" size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderDomPhone" name="partnerProfileInfo.hhgProviderDomPhone" size="30" maxLength="30"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProvideDomEmail" name="partnerProfileInfo.hhgProvideDomEmail"  size="30" title="${partnerProfileInfo.hhgProvideDomEmail}" maxLength="45"  tabindex="" onchange="checkEmail(this,'partnerProfileInfo.hhgProvideDomEmail')"/></td>
						
						</tr>
						
						<tr>
						<td class="listwhitetext" rowspan="4" style="text-align:right;"><b><fmt:message key='partnerProfileInfo.hhgProviderIntl' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlCompanyName" name="partnerProfileInfo.hhgProviderIntlCompanyName" title="${partnerProfileInfo.hhgProviderIntlCompanyName}" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlContact" name="partnerProfileInfo.hhgProviderIntlContact" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlPhone" name="partnerProfileInfo.hhgProviderIntlPhone"  size="30" maxLength="30"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProvideIntlEmail" name="partnerProfileInfo.hhgProvideIntlEmail" size="30" title="${partnerProfileInfo.hhgProvideIntlEmail}" maxLength="45"   tabindex="" onchange="checkEmail(this,'partnerProfileInfo.hhgProvideIntlEmail')"/></td>
						</tr>
						<tr>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlCompanyName2" name="partnerProfileInfo.hhgProviderIntlCompanyName2" title="${partnerProfileInfo.hhgProviderIntlCompanyName2}" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlContact2" name="partnerProfileInfo.hhgProviderIntlContact2" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlPhone2" name="partnerProfileInfo.hhgProviderIntlPhone2"  size="30" maxLength="30" tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProvideIntlEmail2" name="partnerProfileInfo.hhgProvideIntlEmail2"  size="30" title="${partnerProfileInfo.hhgProvideIntlEmail2}" maxLength="45"  tabindex="" onchange="checkEmail(this,'partnerProfileInfo.hhgProvideIntlEmail2')"/></td>
						</tr>
						<tr>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlCompanyName3" name="partnerProfileInfo.hhgProviderIntlCompanyName3" title="${partnerProfileInfo.hhgProviderIntlCompanyName3}" size="30" maxLength="45"   tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlContact3" name="partnerProfileInfo.hhgProviderIntlContact3"  size="30" maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlPhone3" name="partnerProfileInfo.hhgProviderIntlPhone3"   size="30" maxLength="30" tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProvideIntlEmail3" name="partnerProfileInfo.hhgProvideIntlEmail3"   size="30" title="${partnerProfileInfo.hhgProvideIntlEmail3}"  maxLength="45" tabindex="" onchange="checkEmail(this,'partnerProfileInfo.hhgProvideIntlEmail3')"/></td>
						</tr>
						<tr>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlCompanyName4" name="partnerProfileInfo.hhgProviderIntlCompanyName4" title="${partnerProfileInfo.hhgProviderIntlCompanyName4}" size="30"  maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlContact4" name="partnerProfileInfo.hhgProviderIntlContact4"  size="30"  maxLength="45"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProviderIntlPhone4" name="partnerProfileInfo.hhgProviderIntlPhone4"   size="30" maxLength="30"  tabindex=""/></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.hhgProvideIntlEmail4" name="partnerProfileInfo.hhgProvideIntlEmail4"   size="30" title="${partnerProfileInfo.hhgProvideIntlEmail4}" maxLength="45"  tabindex="" onchange="checkEmail(this,'partnerProfileInfo.hhgProvideIntlEmail4')"/></td>
						</tr>
						</tbody>
						</table>
					  		</div>
					  		
					  	</td>
					 </tr>
				   
				
				
					<tr>						
						<td colspan="20" width="100%" align="left" style="margin: 0px">
     						<div onClick="javascript:animatedcollapse.toggle('IntrnationalTrms')" style="margin: 0px">
			 					<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 					<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;International Terms
            						</td>            
            						<td class="headtab_right"></td>
          						</tr>
								</table>
					  		</div>
					  		<div id="IntrnationalTrms">
					  		<table style="margin:0px;border:2px solid #74b3dc;width:auto !important;" class="table">
						<tbody>
						<tr>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.superGroupName' /></b></td>
						<td colspan="3"><s:textfield cssClass="input-text" id="partnerProfileInfo.superGroupName" name="partnerProfileInfo.superGroupName" title="${partnerProfileInfo.superGroupName}" maxLength="45" size="75"   tabindex="" /></td>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.customer' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.customer" name="partnerProfileInfo.customer"  maxLength="45"    tabindex="" /></td>
						<td class="listwhitetext" style="text-align:right;background-color:#efefef;" colspan="3"></td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.superGroup' /></b></td>
						<td class="listwhitetext"><s:textfield cssClass="input-text" id="partnerProfileInfo.superGroup" name="partnerProfileInfo.superGroup"  maxLength="45"   tabindex=""  /></td>
						<td class="listwhitetext" style="text-align:right;background-color:#efefef;" colspan="2"></td>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.username' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.username" name="partnerProfileInfo.username"  maxLength="45"    tabindex="" /></td>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.password' /></b></td>
						<td colspan="2"><s:password key="partnerProfileInfo.password"  cssClass="input-text"  showPassword="true"  maxLength="45"    tabindex="" /></td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.mPointOfContact' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.mPointOfContact" name="partnerProfileInfo.mPointOfContact" title="${partnerProfileInfo.mPointOfContact}" maxLength="45" size="30"   tabindex="" /></td>
						<td class="listwhitetext" style="text-align:right;background-color:#efefef;" colspan="2"></td>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.intlTermsPhone' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.intlTermsPhone" name="partnerProfileInfo.intlTermsPhone"  maxLength="30"    tabindex="" onchange="onlyNumeric(this);"/></td>
						<td class="listwhitetext" style="text-align:right;background-color:#efefef;" colspan="3"></td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.intlTermsEmail' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.intlTermsEmail" name="partnerProfileInfo.intlTermsEmail" title="${partnerProfileInfo.intlTermsEmail}" cssStyle="width:195px;" maxLength="45"    tabindex="" onchange="checkEmail(this,'partnerProfileInfo.intlTermsEmail')"/></td>
						<td class="listwhitetext" style="text-align:right;background-color:#efefef;" colspan="2"></td>
						<td class="listwhitetext" style="text-align:right;" >
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="border:none;">Contract&nbsp;in&nbsp;Place:</td>
						<td style="border:none;padding:0px;">
						<c:set var="contractInPlaceYFlag" value="false" />
						<c:if test="${partnerProfileInfo.contractInPlaceY}">
							<c:set var="contractInPlaceYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.contractInPlaceY" fieldValue="true" value="${contractInPlaceYFlag}"  tabindex="" />&nbsp;Yes
						</td>
						<td style="border:none;padding:0px;">
						<c:set var="contractInPlaceNFlag" value="false" />
						<c:if test="${partnerProfileInfo.contractInPlaceN}">
							<c:set var="contractInPlaceNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.contractInPlaceN" fieldValue="true" value="${contractInPlaceNFlag}"  tabindex="" />&nbsp;No
						</td>
						</tr>
						</table>
						
						</td>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.startDate' /></b></td>
						<td width="100">
						<c:if test="${not empty partnerProfileInfo.startDate}">
							<s:text id="startDateFormattedValue" name="${FormDateValue}" ><s:param name="value" value="partnerProfileInfo.startDate"/></s:text>
				 			<s:textfield cssClass="input-text" cssStyle="border:1px solid #219DD1;" id="startDate" name="partnerProfileInfo.startDate" value="%{startDateFormattedValue}" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" />
				 			<img id="startDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
						</c:if>	
						<c:if test="${empty partnerProfileInfo.startDate}">
							<s:textfield cssClass="input-text" cssStyle="border:1px solid #219DD1;" id="startDate" name="partnerProfileInfo.startDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"  />
							<img id="startDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
						</c:if>
						</td>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.lnthOfContract' /></b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.lnthOfContract" name="partnerProfileInfo.lnthOfContract" title="${partnerProfileInfo.lnthOfContract}" maxLength="45"    tabindex=""  /></td>
						</tr>
						
						
						<tr>
						<td class="listwhitetext" style="text-align:right;" ><b>How are moves Awarded:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="indRequestforBidsFlag" value="false" />
						<c:if test="${partnerProfileInfo.indRequestforBids}">
										<c:set var="indRequestforBidsFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.indRequestforBids" fieldValue="true" value="${indRequestforBidsFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.indRequestforBids' />
						</td>
						<td class="listwhitetext" style="text-align:left;vertical-align:top;">
						<c:set var="cldFlag" value="false" />
						<c:if test="${partnerProfileInfo.cld}">
										<c:set var="cldFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.cld" fieldValue="true" value="${cldFlag}"  tabindex="" /><fmt:message key='partnerProfileInfo.cld' />
						</td>
						<td class="listwhitetext" style="text-align:left;vertical-align:top;">
						<c:set var="rloProvidersFlag" value="false" />
						<c:if test="${partnerProfileInfo.rloProviders}">
										<c:set var="rloProvidersFlag" value="true" />
						</c:if>
						<div style="float: left; width: 118px;">
						<div style="float:left;">
						<s:checkbox key="partnerProfileInfo.rloProviders" fieldValue="true" value="${rloProvidersFlag}"  tabindex="" />
						</div>
						<div style="float:left;">
						<fmt:message key='partnerProfileInfo.rloProviders' />
						</div>
						</div>
						</td>
						<td class="listwhitetext" style="text-align:left;vertical-align:top;">
						<c:set var="tsFlag" value="false" />
						<c:if test="${partnerProfileInfo.ts}">
										<c:set var="tsFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.ts" fieldValue="true" value="${tsFlag}"  tabindex="" /><fmt:message key='partnerProfileInfo.ts' />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="rapFlag" value="false" />
						<c:if test="${partnerProfileInfo.rap}">
										<c:set var="rapFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.rap" fieldValue="true" value="${rapFlag}"  tabindex="" /><fmt:message key='partnerProfileInfo.rap' />
						</td>
						<td class="listwhitetext" style="text-align:left;" colspan="2"><b><fmt:message key='partnerProfileInfo.fixPricingforOrigOrDest' /></b></td>
						<td ><s:textfield cssClass="input-text" id="partnerProfileInfo.fixPricingforOrigOrDest" name="partnerProfileInfo.fixPricingforOrigOrDest" title="${partnerProfileInfo.fixPricingforOrigOrDest}" maxLength="45"   tabindex="" /></td>
						</tr>
						
						<tr>
						<td class="listwhitetext" rowspan="2" style="text-align:right;">
						<b>Allowance&nbsp;Parameters(List allowance in blank cells):</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="airFlag" value="false" />
						<c:if test="${partnerProfileInfo.air}">
										<c:set var="airFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.air" fieldValue="true" value="${airFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.air' />
						</td>
						<td class="listwhitetext"  style="text-align: left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.airDesc" name="partnerProfileInfo.airDesc" title="${partnerProfileInfo.airDesc}" maxLength="45"   tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="seaFlag" value="false" />
						<c:if test="${partnerProfileInfo.sea}">
										<c:set var="seaFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.sea" fieldValue="true" value="${seaFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.sea' />
						</td>
						<td class="listwhitetext"  style="text-align: left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.seaDesc" name="partnerProfileInfo.seaDesc" title="${partnerProfileInfo.seaDesc}"  maxLength="45" size="30"   tabindex=""/></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="permStorageFlag" value="false" />
						<c:if test="${partnerProfileInfo.permStorage}">
										<c:set var="permStorageFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.permStorage" fieldValue="true" value="${permStorageFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.permStorage' />
						</td>
						<td class="listwhitetext"  style="text-align: right; text-transform: uppercase;" colspan="2"><b>SIT</b></td>
						<td><s:textfield cssClass="input-text" id="partnerProfileInfo.sit" name="partnerProfileInfo.sit" title="${partnerProfileInfo.sit}"  maxLength="45"   tabindex=""/></td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="singleFlag" value="false" />
						<c:if test="${partnerProfileInfo.single}">
										<c:set var="singleFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.single" fieldValue="true" value="${singleFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.single' />
						</td>
						<td class="listwhitetext"  style="text-align: left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.singleDesc" name="partnerProfileInfo.singleDesc" title="${partnerProfileInfo.singleDesc}" maxLength="45"   tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="marriedFlag" value="false" />
						<c:if test="${partnerProfileInfo.married}">
										<c:set var="marriedFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.married" fieldValue="true" value="${marriedFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.married' />
						</td>
						<td class="listwhitetext"  style="text-align: left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.marriedDesc" name="partnerProfileInfo.marriedDesc" title="${partnerProfileInfo.marriedDesc}" maxLength="45"  size="30" tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="marriedWithChildFlag" value="false" />
						<c:if test="${partnerProfileInfo.marriedWithChild}">
										<c:set var="marriedWithChildFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.marriedWithChild" fieldValue="true" value="${marriedWithChildFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.marriedWithChild' />
						</td>
						<td class="listwhitetext"  style="text-align: left; text-transform: uppercase;" colspan="3">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.seaAllowanceMatrix" name="partnerProfileInfo.seaAllowanceMatrix" title="${partnerProfileInfo.seaAllowanceMatrix}"  maxLength="45" size="60"   tabindex=""/></td>
						</tr>
					
						
						</tbody>
						</table>
						
						<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('accounting');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Accounting</td>            
            						<td class="headtab_right"></td>
          							</tr>
										
									</table>
					  			</div>
					  			<div id="accounting">
					  		<table style="margin:0px;border:2px solid #74b3dc;width:auto !important;" class="table">
						<tbody>
						<tr>
						<td class="listwhitetext" style="text-align:right;width:160px;"><b><fmt:message key='partnerProfileInfo.accountPointOfContact' /></b></td>
						<td colspan="2">
						<s:textfield cssClass="input-text" size="40" id="partnerProfileInfo.accountPointOfContact" name="partnerProfileInfo.accountPointOfContact" title="${partnerProfileInfo.accountPointOfContact}" maxLength="45"    tabindex="" /></td>
						<td class="listwhitetext" style="text-align:right;width:137px;" ><b><fmt:message key='partnerProfileInfo.accountEmail' /></b></td>
						<td colspan="2"><s:textfield cssClass="input-text" size="40" id="partnerProfileInfo.accountEmail" name="partnerProfileInfo.accountEmail"  maxLength="45"    tabindex="" onchange="checkEmail(this,'partnerProfileInfo.accountEmail')"/></td>
						<td class="listwhitetext" style="text-align:right;width:100px;" ><b><fmt:message key='partnerProfileInfo.accountPhone' /></b></td>
						<td><s:textfield cssClass="input-text" size="40" id="partnerProfileInfo.accountPhone" name="partnerProfileInfo.accountPhone"  maxLength="30"    tabindex="" onchange="onlyNumeric(this);"/></td>
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:right;"><b><fmt:message key='partnerProfileInfo.rloCompanyInvolved' /></b></td>
						<td colspan="2">
						<s:textfield cssClass="input-text"  size="40" id="partnerProfileInfo.rloCompanyInvolved" name="partnerProfileInfo.rloCompanyInvolved" title="${partnerProfileInfo.rloCompanyInvolved}" maxLength="45"    tabindex="" /></td>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.rloCompanyInvolvedPhone' /></b></td>
						<td colspan="2"><s:textfield cssClass="input-text" size="40" id="partnerProfileInfo.rloCompanyInvolvedPhone" name="partnerProfileInfo.rloCompanyInvolvedPhone"  maxLength="30"    tabindex="" onchange="onlyNumeric(this);"/></td>
						<td colspan="2" style="background-color:#efefef;"></td>
						</tr>
						
						<tr>
						<td class="listwhitetext" style="text-align:right;"><b><fmt:message key='partnerProfileInfo.mainContact' /></b></td>
						<td colspan="2">
						<s:textfield cssClass="input-text" size="40" id="partnerProfileInfo.mainContact" name="partnerProfileInfo.mainContact" title="${partnerProfileInfo.mainContact}" maxLength="45"    tabindex="" /></td>
						<td class="listwhitetext" style="text-align:right;" ><b><fmt:message key='partnerProfileInfo.mainContactEmail' /></b></td>
						<td colspan="2"><s:textfield  size="40" cssClass="input-text" id="partnerProfileInfo.mainContactEmail" name="partnerProfileInfo.mainContactEmail"  maxLength="45"    tabindex="" onchange="checkEmail(this,'partnerProfileInfo.mainContactEmail')"/></td>
						<td colspan="2" style="background-color:#efefef;"></td>
						</tr>
						
						
						
						</tbody>
						</table>
						</div>
					  			</td>
					  			</tr>
					  			</tbody>
					  			</table>
					  			
					  			
					  			<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('billing');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Billing</td>            
            						<td class="headtab_right"></td>
          							</tr>
									</table>
					  			</div>
					  			<div id="billing">
					  		<table style="margin:0px;border:2px solid #74b3dc;width:1650px !important;margin-bottom:4px;" class="table">
						
						<tbody>
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b><fmt:message key='partnerProfileInfo.billDeliveryPreference' /></b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="billDeliveryPreferenceMailFlag" value="false" />
						<c:if test="${partnerProfileInfo.billDeliveryPreferenceMail}">
										<c:set var="billDeliveryPreferenceMailFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.billDeliveryPreferenceMail" fieldValue="true" value="${billDeliveryPreferenceMailFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.billDeliveryPreferenceMail' />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="billDeliveryPreferenceEmailFlag" value="false" />
						<c:if test="${partnerProfileInfo.billDeliveryPreferenceEmail}">
										<c:set var="billDeliveryPreferenceEmailFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.billDeliveryPreferenceEmail" fieldValue="true" value="${billDeliveryPreferenceEmailFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.billDeliveryPreferenceEmail' />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="billDeliveryPreferenceEDIFlag" value="false" />
						<c:if test="${partnerProfileInfo.billDeliveryPreferenceEDI}">
										<c:set var="billDeliveryPreferenceEDIFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.billDeliveryPreferenceEDI" fieldValue="true" value="${billDeliveryPreferenceEDIFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.billDeliveryPreferenceEDI' />
						</td>
						<td colspan="5" style="background-color:#efefef;">&nbsp;</td>
						</tr>
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b><fmt:message key='partnerProfileInfo.billingCutOff' /></b></td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
						<s:textfield cssStyle="border:1px solid #dfdfdf"  cssClass="input-text" id="partnerProfileInfo.billingCutOff" name="partnerProfileInfo.billingCutOff"  maxLength="45"    tabindex="" onchange="onlyNumeric(this);"/>
						+
						<s:textfield cssStyle="border:1px solid #dfdfdf;width:100px;" cssClass="input-text" id="partnerProfileInfo.billingCutOffDays" name="partnerProfileInfo.billingCutOffDays"  maxLength="45"    tabindex="" onchange="onlyNumeric(this);"/>
						<b><fmt:message key='partnerProfileInfo.billingCutOffDays' /></b>
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="daysfromDeliveryFlag" value="false" />
						<c:if test="${partnerProfileInfo.daysfromDelivery}">
										<c:set var="daysfromDeliveryFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.daysfromDelivery" fieldValue="true" value="${daysfromDeliveryFlag}"  tabindex="" />
						60&nbsp;<fmt:message key='partnerProfileInfo.daysfromDelivery' />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="supplimentalBillingAllowedFlag" value="false" />
						<c:if test="${partnerProfileInfo.supplimentalBillingAllowed}">
								<c:set var="supplimentalBillingAllowedFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.supplimentalBillingAllowed" fieldValue="true" value="${supplimentalBillingAllowedFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.supplimentalBillingAllowed' />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="approvalRequiredFlag" value="false" />
						<c:if test="${partnerProfileInfo.approvalRequired}">
										<c:set var="approvalRequiredFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.approvalRequired" fieldValue="true" value="${approvalRequiredFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.approvalRequired' />
						</td>
						<td colspan="3" style="background-color:#efefef;">&nbsp;</td>
						</tr>
						
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b><fmt:message key='partnerProfileInfo.billingContact' /></b></td>
						<td colspan="2" class="listwhitetext" style="text-align:left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.billingContact" name="partnerProfileInfo.billingContact"  maxLength="45"    tabindex="" cssStyle="width:381px;" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<fmt:message key='partnerProfileInfo.billingContactEmail' />
						</td>
						<td colspan="2" class="listwhitetext" style="text-align:left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.billingContactEmail" name="partnerProfileInfo.billingContactEmail"  maxLength="45"    tabindex="" onchange="checkEmail(this,'partnerProfileInfo.billingContactEmail')" cssStyle="width:381px;"/>
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<fmt:message key='partnerProfileInfo.billingContactPhone' />
						</td>
						<td colspan="2" class="listwhitetext" style="text-align:left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.billingContactPhone" name="partnerProfileInfo.billingContactPhone"  maxLength="30"   tabindex="" onchange="onlyNumeric(this);"/>
						</td>
						</tr>
						
						
						<tr>
						<td class="listwhitetext" rowspan="3" style="text-align:right;"><b><fmt:message key='partnerProfileInfo.invoiceBackUpRequired' /></b></td>
						<td class="listwhitetext" style="text-align:left;width:200px;">
						<c:set var="accessFlag" value="false" />
						<c:if test="${partnerProfileInfo.access}">
										<c:set var="accessFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.access" fieldValue="true" value="${accessFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.access' />
						</td>
						<td class="listwhitetext" style="text-align:left;width:150px;">
						<c:set var="billOfLadingFlag" value="false" />
						<c:if test="${partnerProfileInfo.billOfLading}">
										<c:set var="billOfLadingFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.billOfLading" fieldValue="true" value="${billOfLadingFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.billOfLading' />
						</td>
						<td class="listwhitetext" style="text-align:left;width:185px;">
						<c:set var="cubeSheetFlag" value="false" />
						<c:if test="${partnerProfileInfo.cubeSheet}">
										<c:set var="cubeSheetFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.cubeSheet" fieldValue="true" value="${cubeSheetFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.cubeSheet' />
						</td>
						<td class="listwhitetext" style="text-align:left;vertical-align:top;" rowspan="2">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
						<tr>
						<td style="padding:2px !important;border:none;">
						<c:set var="airwaysBillFlag" value="false" />
						<c:if test="${partnerProfileInfo.airwaysBill}">
										<c:set var="airwaysBillFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.airwaysBill" fieldValue="true" value="${airwaysBillFlag}"  tabindex="" />
						</td>
						<td style="padding:2px !important;border:none;">
						<fmt:message key='partnerProfileInfo.airwaysBill' />
						</td>
						<td style="padding:2px !important;border:none;">
						<c:set var="ratedFlag" value="false" />
						<c:if test="${partnerProfileInfo.rated}">
										<c:set var="ratedFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.rated" fieldValue="true" value="${ratedFlag}"  tabindex="" />
						</td>
						<td style="padding:2px !important;border:none;">
						<fmt:message key='partnerProfileInfo.rated' />
						</td>
						</tr>
						<tr><td style="padding:6px !important;border:none;"></td></tr>
						<tr>
						<td style="padding:2px !important;border:none;">
						<c:set var="unratedFlag" value="false" />
						<c:if test="${partnerProfileInfo.unrated}">
										<c:set var="unratedFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.unrated" fieldValue="true" value="${unratedFlag}"  tabindex="" />
						</td>
						<td style="padding:2px !important;border:none;">
						<fmt:message key='partnerProfileInfo.Unrated' />
						</td>
						<td style="padding:2px !important;border:none;">
						<c:set var="proofOfPmtFlag" value="false" />
						<c:if test="${partnerProfileInfo.proofOfPmt}">
										<c:set var="proofOfPmtFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.proofOfPmt" fieldValue="true" value="${proofOfPmtFlag}"  tabindex="" />
						</td>
						<td style="padding:2px !important;border:none;">
						<fmt:message key='partnerProfileInfo.proofOfPmt' />
						</td>
						</tr>
						
						</table>
						
						</td>
						<td class="listwhitetext" style="text-align:left;vertical-align:top;" rowspan="2">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
						<tr>
						<td style="padding:2px !important;border:none;">
						<c:set var="singleFlag" value="false" />
						<c:if test="${partnerProfileInfo.single}">
										<c:set var="singleFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.single" fieldValue="true" value="${singleFlag}"  tabindex="" />
						</td>
						<td style="padding:2px !important;border:none;">Ocean&nbsp;Bill&nbsp;(OBL)
						</td>
						<td style="padding:2px !important;border:none;">
						<c:set var="approvalRequiredRatedFlag" value="false" />
						<c:if test="${partnerProfileInfo.approvalRequiredRated}">
										<c:set var="approvalRequiredRatedFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.approvalRequiredRated" fieldValue="true" value="${approvalRequiredRatedFlag}"  tabindex="" />
						</td>
						<td style="padding:2px !important;border:none;">
						<fmt:message key='partnerProfileInfo.approvalRequiredRated' />
						</td>
						</tr>
						<tr><td style="padding:6px !important;border:none;"></td></tr>
						<tr>
						<td style="padding:2px !important;border:none;">
						<c:set var="approvalRequiredUnratedFlag" value="false" />
						<c:if test="${partnerProfileInfo.approvalRequiredUnrated}">
										<c:set var="approvalRequiredUnratedFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.approvalRequiredUnrated" fieldValue="true" value="${approvalRequiredUnratedFlag}"  tabindex="" />
						</td>
						<td style="padding:2px !important;border:none;">
						<fmt:message key='partnerProfileInfo.approvalRequiredUnrated' />
						</td>
						<td style="padding:2px !important;border:none;">
						<c:set var="approvalRequiredProofOfPmtFlag" value="false" />
						<c:if test="${partnerProfileInfo.single}">
										<c:set var="approvalRequiredProofOfPmtFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.approvalRequiredProofOfPmt" fieldValue="true" value="${approvalRequiredProofOfPmtFlag}"  tabindex="" />
						</td>
						<td style="padding:2px !important;border:none;">
						<fmt:message key='partnerProfileInfo.approvalRequiredProofOfPmt' />
						</td>
						</tr>
						
						</table>
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="excApprovalsFlag" value="false" />
						<c:if test="${partnerProfileInfo.excApprovals}">
										<c:set var="excApprovalsFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.excApprovals" fieldValue="true" value="${excApprovalsFlag}"  tabindex="" />
						<fmt:message key='partnerProfileInfo.excApprovals' />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="billingOthersFlag" value="false" />
						<c:if test="${partnerProfileInfo.billingOthers}">
										<c:set var="billingOthersFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.billingOthers" fieldValue="true" value="${billingOthersFlag}"  tabindex="" />
						</td>
						<td style="padding:2px !important;border:none;">
						<fmt:message key='partnerProfileInfo.billingOthers' />
						</td>
						</tr>
						<tr>
						
						<td style="text-align:right;padding:0px !important;border:none;" colspan="2">If&nbsp;other,&nbsp;list</td>
						</tr>
						</table>
						
						</td>
						<td class="listwhitetext" rowspan="3" style="vertical-align:top">
						Freight&nbsp;Invoice<br/>
						<s:textarea name="partnerProfileInfo.frieghtInvoice" id="frieghtInvoice" required="true" cssClass="textarea" cssStyle="height:65px;" />
						</td>
						</tr>
						
						<tr>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="authFlag" value="false" />
						<c:if test="${partnerProfileInfo.auth}">
										<c:set var="authFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.auth" fieldValue="true" value="${authFlag}"  tabindex="" />Authorization(AUTH),&nbsp;if&nbsp;applicable
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="opsFlag" value="false" />
						<c:if test="${partnerProfileInfo.ops}">
										<c:set var="opsFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.ops" fieldValue="true" value="${opsFlag}"  tabindex="" />Order&nbsp;for&nbsp;Service(OPS)/PO
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="inspFlag" value="false" />
						<c:if test="${partnerProfileInfo.insp}">
										<c:set var="inspFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.insp" fieldValue="true" value="${inspFlag}"  tabindex="" />Insurance&nbsp;Policy&nbsp;(INSP)
						</td>
						
						
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="tarrifRateSheetFlag" value="false" />
						<c:if test="${partnerProfileInfo.tarrifRateSheet}">
										<c:set var="tarrifRateSheetFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.tarrifRateSheet" fieldValue="true" value="${tarrifRateSheetFlag}"  tabindex="" />Tariff&nbsp;Rate&nbsp;Sheet
						</td>
						<td class="listwhitetext" style="background-color:#efefef;">
						
						</td>
						
						</tr>
						<tr>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="nvtdFlag" value="false" />
						<c:if test="${partnerProfileInfo.nvtd}">
										<c:set var="nvtdFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.nvtd" fieldValue="true" value="${nvtdFlag}"  tabindex="" />Signed&nbsp;Inventory&nbsp;(NVTD)</td>

						<td class="listwhitetext" style="text-align:left;">
						<c:set var="nvtvFlag" value="false" />
						<c:if test="${partnerProfileInfo.nvtv}">
										<c:set var="nvtvFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.nvtv" fieldValue="true" value="${nvtvFlag}"  tabindex="" />Signed&nbsp;Inventory&nbsp;(NVTV)</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="wtctFlag" value="false" />
						<c:if test="${partnerProfileInfo.wtct}">
										<c:set var="wtctFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.wtct" fieldValue="true" value="${wtctFlag}"  tabindex="" />Weight&nbsp;Tickets&nbsp;(WTCT):&nbsp;If&nbsp;available</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="officialReFAdditionalFlag" value="false" />
						<c:if test="${partnerProfileInfo.officialReFAdditional}">
										<c:set var="officialReFAdditionalFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.officialReFAdditional" fieldValue="true" value="${officialReFAdditionalFlag}"  tabindex="" />Official&nbsp;receipts&nbsp;for&nbsp;Additional</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="crateListFlag" value="false" />
						<c:if test="${partnerProfileInfo.crateList}">
										<c:set var="crateListFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.crateList" fieldValue="true" value="${crateListFlag}"  tabindex="" />Crate&nbsp;list&nbsp;with&nbsp;Dims</td>
						
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="thirdPartyDocumentsFlag" value="false" />
						<c:if test="${partnerProfileInfo.thirdPartyDocuments}">
						<c:set var="thirdPartyDocumentsFlag" value="true" />
						</c:if>
						
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="text-align:left;border:none;padding:0px;">
						<s:checkbox key="partnerProfileInfo.thirdPartyDocuments" fieldValue="true" value="${thirdPartyDocumentsFlag}"  tabindex="" />
						</td>
						<td style="text-align:left;border:none;">3rd&nbsp;Party&nbsp;Documents</td>
						</tr>
						</table>
						</td>
						
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="backUpFlag" value="false" />
						<c:if test="${partnerProfileInfo.backUp}">
										<c:set var="backUpFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.backUp" fieldValue="true" value="${backUpFlag}"  tabindex="" />Backup&nbsp;for&nbsp;"pass&nbsp;thru"&nbsp;cost
						</td>
						</tr>
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b><fmt:message key='partnerProfileInfo.specialInvoiceInstructions' /></b>
						</td>

						<td class="listwhitetext" style="text-align:left;" colspan="8">
						<s:textarea name="partnerProfileInfo.specialInvoiceInstructions"  id="specialInvoiceInstructions" required="true" cssClass="textarea" rows="1" cols="220"  />
						</td>
						</tr>
						</tbody>
						</table>
						
						</div>
					  			</td>
					  			</tr>
					  			</tbody>
					  			</table>
					  			
					  			
					  	<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('IntBillingInfo');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;International&nbsp;Billing&nbsp;Additional&nbsp;Information</td>            
            						<td class="headtab_right"></td>
          							</tr>
										
									</table>
					  			</div>
					<div id="IntBillingInfo">
					 <table style="margin:0px;border:2px solid #74b3dc;width:auto !important;margin-bottom:4px;" class="table">
						<tbody>
							<tr>
								<td class="listwhitetext" style="text-align:left;" colspan="9">
								<s:textarea name="partnerProfileInfo.intlBillingAdditionalInfo"  id="intlBillingAdditionalInfoId" required="true" cssClass="textarea" rows="3" cols="186"  />
																
								</td>
							</tr>
						</tbody>
					</table>
				  </div>
					</td>
					</tr>
					</tbody>
					</table>
					
					
					
							<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('pricing');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Pricing</td>            
            						<td class="headtab_right"></td>
          							</tr>
									</table>
					  			</div>
					  			<div id="pricing">
					  		<table style="margin:0px;border:2px solid #74b3dc;width:auto !important;margin-bottom:4px;" class="table">
						
						<tbody>
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Path&nbsp;for&nbsp;Pricing&nbsp;on&nbsp;file</b></td>
						<td class="listwhitetext" style="text-align:left;" colspan="4">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.pathforPricing" name="partnerProfileInfo.pathforPricing"  cssStyle="width:644px;" maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<b>Contracted&nbsp;Rated:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<c:set var="contratedYesFlag" value="false" />
						<c:if test="${partnerProfileInfo.contratedYes}">
								<c:set var="contratedYesFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.contratedYes" fieldValue="true" value="${contratedYesFlag}"  tabindex="" />
						Yes
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="contratedNoFlag" value="false" />
						<c:if test="${partnerProfileInfo.contratedNo}">
										<c:set var="contratedNoFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.contratedNo" fieldValue="true" value="${contratedNoFlag}"  tabindex="" />
						No
						</td>
						<td  style="background-color:#efefef;">&nbsp;</td>
						</tr>
						
						<tr>
						<td class="listwhitetext" style="text-align:right;width: 10%;">
						<b>Pricing Method:</b></td>
						<td class="listwhitetext" style="text-align:left;width: 8%;">
						<c:set var="triRegionalFlag" value="false" />
						<c:if test="${partnerProfileInfo.triRegional}">
								<c:set var="triRegionalFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.triRegional" fieldValue="true" value="${triRegionalFlag}"  tabindex="" />
						Tri-Regional
						</td>
						<td class="listwhitetext" style="text-align:left;width: 9%;">
						<c:set var="indBidsFlag" value="false" />
						<c:if test="${partnerProfileInfo.indBids}">
										<c:set var="indBidsFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.indBids" fieldValue="true" value="${indBidsFlag}"  tabindex="" />
						Individual Bids
						</td>
						<td class="listwhitetext" style="text-align:left;width: 8%;">
						<c:set var="costPlsFlag" value="false" />
						<c:if test="${partnerProfileInfo.costPls}">
							<c:set var="costPlsFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.costPls" fieldValue="true" value="${costPlsFlag}"  tabindex="" />
						Cost Plus
						</td>
						<td class="listwhitetext" style="text-align:left;width: 10%;">
						<c:set var="rateMatrixFlag" value="false" />
						<c:if test="${partnerProfileInfo.rateMatrix}">
							<c:set var="rateMatrixFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.rateMatrix" fieldValue="true" value="${rateMatrixFlag}"  tabindex="" />
						Rate&nbsp;Matrix
						</td>
						<td class="listwhitetext" style="text-align:left;width: 10%;">
						<c:set var="onlineRateFillFlag" value="false" />
						<c:if test="${partnerProfileInfo.onlineRateFill}">
							<c:set var="onlineRateFillFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.onlineRateFill" fieldValue="true" value="${onlineRateFillFlag}"  tabindex="" />
						Online&nbsp;Rate&nbsp;filing
						</td>
						<td class="listwhitetext" style="text-align:left;width: 10%;" colspan="2">
						<c:set var="pricingOtherFlag" value="false" />
						<c:if test="${partnerProfileInfo.pricingOther}">
										<c:set var="pricingOtherFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.pricingOther" fieldValue="true" value="${pricingOtherFlag}"  tabindex="" />
						Other
						</td>
						<td  class="listwhitetext" style="text-align:right;width: 8%;">If&nbsp;other,explain</td>
						<td  class="listwhitetext"  >
						<s:textfield cssClass="input-text" id="partnerProfileInfo.pricingOtherDesc" name="partnerProfileInfo.pricingOtherDesc"  maxLength="240"   cssStyle="width:366px;" tabindex="" />
						</td>
						</tr>
						
						
						<tr>
						<td class="listwhitetext" style="text-align:right;"><b>
						Tri-Regional&nbsp;Model:&nbsp;If<br/>
						checked,&nbsp;list&nbsp;other&nbsp;providers<br/>
						and&nbsp;regions&nbsp;supported</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" colspan="8">
						<s:textarea name="partnerProfileInfo.triRegionalModelDesc"  id="triRegionalModelDesc" required="true" cssClass="textarea" rows="3" cols="205"  />
						</td>
						</tr>
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Services:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="oAFlag" value="false" />
						<c:if test="${partnerProfileInfo.oA}">
							<c:set var="oAFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.oA" fieldValue="true" value="${oAFlag}"  tabindex="" />
						OA
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="dAFlag" value="false" />
						<c:if test="${partnerProfileInfo.dA}">
								<c:set var="dAFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.dA" fieldValue="true" value="${dAFlag}"  tabindex="" />
						DA
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="freightFlag" value="false" />
						<c:if test="${partnerProfileInfo.freight}">
										<c:set var="freightFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.freight" fieldValue="true" value="${freightFlag}"  tabindex="" />
						Freight
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="servicesOtherFlag" value="false" />
						<c:if test="${partnerProfileInfo.servicesOther}">
										<c:set var="servicesOtherFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.servicesOther" fieldValue="true" value="${servicesOtherFlag}"  tabindex="" />
						Other
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="4"></td>
						
						</tr>
						
						<td class="listwhitetext" style="text-align:right;">
						<b>Minimum&nbsp;Margin&nbsp;Required</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.minMargin" name="partnerProfileInfo.minMargin"  maxLength="45"  onchange="onlyFloat(this);"  tabindex="" />
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" ></td>
						<td class="listwhitetext" style="text-align:right;">
						<b>%&nbsp;Administrative&nbsp;Fee</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.adminFee" name="partnerProfileInfo.adminFee"  maxLength="45"    tabindex="" onchange="onlyFloat(this);"/>
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="4">
						</tr> 			
						
				
						
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Third&nbsp;Party&nbsp;Services:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="providedRequuiredFlag" value="false" />
						<c:if test="${partnerProfileInfo.providedRequuired}">
							<c:set var="providedRequuiredFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.providedRequuired" fieldValue="true" value="${providedRequuiredFlag}"  tabindex="" />
						Their&nbsp;Provider&nbsp;Required
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="discontAgrementFlag" value="false" />
						<c:if test="${partnerProfileInfo.discontAgrement}">
										<c:set var="discontAgrementFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.discontAgrement" fieldValue="true" value="${discontAgrementFlag}"  tabindex="" />
						Discount&nbsp;Agreement
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="providertoBeUsedFlag" value="false" />
						<c:if test="${partnerProfileInfo.providertoBeUsed}">
										<c:set var="providertoBeUsedFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.providertoBeUsed" fieldValue="true" value="${providertoBeUsedFlag}"  tabindex="" />
						Our&nbsp;Provider&nbsp;to&nbsp;be&nbsp;Used
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="5">
						</tr> 
						
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Weight&nbsp;Validation&nbsp;Method</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="weighttktRequiredFlag" value="false" />
						<c:if test="${partnerProfileInfo.weighttktRequired}">
										<c:set var="weighttktRequiredFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.weighttktRequired" fieldValue="true" value="${weighttktRequiredFlag}"  tabindex="" />
						Weight&nbsp;Ticket&nbsp;Required
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="fortyfivePerFlag" value="false" />
						<c:if test="${partnerProfileInfo.fortyfivePer}">
								<c:set var="fortyfivePerFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.fortyfivePer" fieldValue="true" value="${fortyfivePerFlag}"  tabindex="" />
						45&nbsp;#&nbsp;Per
						<c:set var="fiftyFivePerFlag" value="false" />
						<c:if test="${partnerProfileInfo.fiftyFivePer}">
										<c:set var="fiftyFivePerFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.fiftyFivePer" fieldValue="true" value="${fiftyFivePerFlag}"  tabindex="" />
						55&nbsp;#&nbsp;Per
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="cbmRequiredFlag" value="false" />
						<c:if test="${partnerProfileInfo.cbmRequired}">
							<c:set var="cbmRequiredFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.cbmRequired" fieldValue="true" value="${cbmRequiredFlag}"  tabindex="" />
						Cbm&nbsp;Requirement
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="reWeightFlag" value="false" />
						<c:if test="${partnerProfileInfo.reWeight}">
										<c:set var="reWeightFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.reWeight" fieldValue="true" value="${reWeightFlag}"  tabindex="" />
						Reweight&nbsp;in&nbsp;USA&nbsp;Required
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="4">
						</tr> 
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Quoting&nbsp;Method</b></td>
						 <td class="listwhitetext" style="text-align:left;">
						<c:set var="dtoDALLFlag" value="false" />
						<c:if test="${partnerProfileInfo.dtoDALL}">
								<c:set var="dtoDALLFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.dtoDALL" fieldValue="true" value="${dtoDALLFlag}"  tabindex="" />
						Door&nbsp;to&nbsp;Door&nbsp;ALL&nbsp;in&nbsp;Pricing
						</td> 
						 <td class="listwhitetext" style="text-align:left;">
						<c:set var="dtoDFlag" value="false" />
						<c:if test="${partnerProfileInfo.dtoD}">
										<c:set var="dtoDFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.dtoD" fieldValue="true" value="${dtoDFlag}"  tabindex="" />
						Door&nbsp;to&nbsp;Door
						</td> 
						 <td class="listwhitetext" style="text-align:left;">
						<c:set var="seperateLinePricingFlag" value="false" />
						<c:if test="${partnerProfileInfo.seperateLinePricing}">
							<c:set var="seperateLinePricingFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.seperateLinePricing" fieldValue="true" value="${seperateLinePricingFlag}"  tabindex="" />
						Seperate&nbsp;line&nbsp;item&nbsp;Pricing
						</td> 
						 <td class="listwhitetext" style="text-align:left;">
						<c:set var="dtoDWithOceanFreightFlag" value="false" />
						<c:if test="${partnerProfileInfo.dtoDWithOceanFreight}">
										<c:set var="dtoDWithOceanFreightFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.dtoDWithOceanFreight" fieldValue="true" value="${dtoDWithOceanFreightFlag}"  tabindex="" />
						Door&nbsp;to&nbsp;Door&nbsp;w/o&nbsp;ocean&nbsp;freight
						</td> 
						  <td class="listwhitetext" style="text-align:left;" colspan="2">
						<c:set var="dtoDWithOceanFlag" value="false" />
						<c:if test="${partnerProfileInfo.dtoDWithOcean}">
										<c:set var="dtoDWithOceanFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.dtoDWithOcean" fieldValue="true" value="${dtoDWithOceanFlag}"  tabindex="" />
						Door&nbsp;to&nbsp;Door&nbsp;w/o&nbsp;ocean
						</td> 
						<td class="listwhitetext" style="background-color:#efefef;" colspan="2">
						</tr>
						
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Quote&nbsp;Required</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="atSurveyFlag" value="false" />
						<c:if test="${partnerProfileInfo.atSurvey}">
										<c:set var="atSurveyFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.atSurvey" fieldValue="true" value="${atSurveyFlag}"  tabindex="" />
						At&nbsp;Survey
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="atSurveyWithFlag" value="false" />
						<c:if test="${partnerProfileInfo.atSurveyWith}">
										<c:set var="atSurveyWithFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.atSurveyWith" fieldValue="true" value="${atSurveyWithFlag}"  tabindex="" />At&nbsp;Survey&nbsp;and&nbsp;with
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="withFinalsFlag" value="false" />
						<c:if test="${partnerProfileInfo.withFinals}">
										<c:set var="withFinalsFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.withFinals" fieldValue="true" value="${withFinalsFlag}"  tabindex="" />
						with&nbsp;finals
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="5"></td>
						</tr>
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Restricted items:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" colspan="3">
						<s:textarea name="partnerProfileInfo.restrItemsDesc"  id="restrItemsDesc" required="true" cssClass="textarea" rows="1" cols="75" />
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="restrItemsListFlag" value="false" />
						<c:if test="${partnerProfileInfo.restrItemsList}">
										<c:set var="restrItemsListFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.restrItemsList" fieldValue="true" value="${restrItemsListFlag}"  tabindex="" />
						Restricted&nbsp;Items&nbsp;list&nbsp;attached
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="4"></td>
						
						</tr>
						</tbody>
						</table>
						
						</div>
					  			</td>
					  			</tr>
					  			</tbody>
					  			</table>
					  			
					  			
					  			<!-- <table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('IntPricing');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;International&nbsp;Pricing&nbsp;Additional&nbsp;Information</td>            
            						<td class="headtab_right"></td>
          							</tr>
										
									</table>
					  			</div>
					<div id="IntPricing">
					 <table style="margin:0px;border:2px solid #74b3dc;width:94% !important;margin-bottom:4px;" class="table">
						<tbody>
							<tr>
								<td class="listwhitetext" style="background-color:#efefef;" colspan="9" rowspan="15">
								</td>
							</tr>
							<tr>
								<td class="listwhitetext" style="background-color:#efefef;" colspan="9">
								</td>
							</tr>
							<tr>
								<td class="listwhitetext" style="background-color:#efefef;" colspan="9">
								</td>
							</tr>
						</tbody>
					</table>
				  </div>
					</td>
					</tr>
					</tbody>
					</table> -->
					
					
					
					<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('transitIns');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Transit&nbsp;Insurance</td>            
            						<td class="headtab_right"></td>
          							</tr>
										
									</table>
					  			</div>
					<div id="transitIns">
					 <table style="margin:0px;border:2px solid #74b3dc;width:auto !important;margin-bottom:4px;" class="table">
						<tbody>
							<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Transit&nbsp;Insurance</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="tranInsYFlag" value="false" />
						<c:if test="${partnerProfileInfo.tranInsY}">
								<c:set var="tranInsYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.tranInsY" fieldValue="true" value="${tranInsYFlag}"  tabindex="" />
						Yes
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="tranInsNFlag" value="false" />
						<c:if test="${partnerProfileInfo.tranInsN}">
										<c:set var="tranInsNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.tranInsN" fieldValue="true" value="${tranInsNFlag}"  tabindex="" />
						No
						</td>
						<td  style="background-color:#efefef;" colspan="5">&nbsp;</td>
						</tr>
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;width: 10%;">
						<b>Billed&nbsp;Insurance&nbsp;Premium:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="bipYFlag" value="false" />
						<c:if test="${partnerProfileInfo.bipY}">
								<c:set var="bipYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.bipY" fieldValue="true" value="${bipYFlag}"  tabindex="" />
						Yes
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="bipNFlag" value="false" />
						<c:if test="${partnerProfileInfo.bipN}">
										<c:set var="bipNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.bipN" fieldValue="true" value="${bipNFlag}"  tabindex="" />
						No
						</td>
						<td class="listwhitetext" style="text-align:right;width: 8%;" rowspan="2">
						<b>If&nbsp;Yes,&nbsp;complete&nbsp;the<br/>
						following:</b>
						</td>
						
						<td class="listwhitetext" style="text-align:left;width: 10%;" colspan="2">
						<s:textfield cssClass="input-text" id="" name="partnerProfileInfo.bipYComments"  maxLength="300"    tabindex="" cssStyle="width:100%"/>
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="2">
						</td>
						
						</tr> 
						
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;"><b>
						Self&nbsp;Insured<br/>
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="selfInsYFlag" value="false" />
						<c:if test="${partnerProfileInfo.selfInsY}">
								<c:set var="selfInsYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.selfInsY" fieldValue="true" value="${selfInsYFlag}"  tabindex="" />
						Yes
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="selfInsNFlag" value="false" />
						<c:if test="${partnerProfileInfo.selfInsN}">
										<c:set var="selfInsNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.selfInsN" fieldValue="true" value="${selfInsNFlag}"  tabindex="" />
						No
						</td>
						<td class="listwhitetext" style="text-align:right;"><b>
						Insurer:<br/>
						</td>
						<td class="listwhitetext" style="text-align:left;width: 10%;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.insurer" name="partnerProfileInfo.insurer"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;"><b>
						Subrogation&nbsp;($&nbsp;amount):<br/>
						</td>
						<td class="listwhitetext" style="text-align:left;width: 10%;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.subrogationAmnt" name="partnerProfileInfo.subrogationAmnt"  maxLength="45"    tabindex="" />
						</td>
						</tr> 
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Documentation &nbsp;Required:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="valuedInvFlag" value="false" />
						<c:if test="${partnerProfileInfo.valuedInv}">
							<c:set var="valuedInvFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.valuedInv" fieldValue="true" value="${valuedInvFlag}"  tabindex="" />
						Valued&nbsp;Inventory
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="highValuedInvFlag" value="false" />
						<c:if test="${partnerProfileInfo.highValuedInv}">
								<c:set var="highValuedInvFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.highValuedInv" fieldValue="true" value="${highValuedInvFlag}"  tabindex="" />
						High &nbsp;Valued&nbsp;Inventory
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="documentReqOthrFlag" value="false" />
						<c:if test="${partnerProfileInfo.documentReqOthr}">
										<c:set var="documentReqOthrFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.documentReqOthr" fieldValue="true" value="${documentReqOthrFlag}"  tabindex="" />
						Other
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>If other,&nbsp;list:</b></td>
						<td class="listwhitetext" style="text-align:left;width: 10%;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.documentReqOthrDesc" name="partnerProfileInfo.documentReqOthrDesc"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="3"></td>
						
						</tr>
						
									
						</tbody>
					</table>
				  </div>
					</td>
					</tr>
					</tbody>
					</table>
					<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('AuthExpProcess');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Authorizations|Exception Process</td>            
            						<td class="headtab_right"></td>
          							</tr>
										
									</table>
					  			</div>
					<div id="AuthExpProcess">
					 <table style="margin:0px;border:2px solid #74b3dc;width:auto !important;margin-bottom:4px;" class="table">
						<tbody>
							<tr>
								<td class="listwhitetext" style="text-align:left;" colspan="9">
								<s:textarea name="partnerProfileInfo.authorizationsExceptions"  id="authorizationsExceptionsId" required="true" cssClass="textarea" rows="3" cols="186"  />
																
								</td>
							</tr>
						</tbody>
					</table>
				  </div>
					</td>
					</tr>
					</tbody>
					</table>
					<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('intlTranAddInfo');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;International Transit Insurance Additional Information</td>            
            						<td class="headtab_right"></td>
          							</tr>
										
									</table>
					  			</div>
					<div id="intlTranAddInfo">
					 <table style="margin:0px;border:2px solid #74b3dc;width:auto !important;margin-bottom:4px;" class="table">
						<tbody>
							<tr>
								<td class="listwhitetext" style="text-align:left;" colspan="9">
								<s:textarea name="partnerProfileInfo.intlTransitInsAddInfo"  id="intlTransitInsAddInfoId" required="true" cssClass="textarea" rows="3" cols="186"  />
																
								</td>
							</tr>
						</tbody>
					</table>
				  </div>
					</td>
					</tr>
					</tbody>
					</table>
					
					<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('insurance');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Insurance</td>            
            						<td class="headtab_right"></td>
          							</tr>
										
									</table>
					  			</div>
					  			<div id="insurance">
					 <table style="margin:0px;border:2px solid #74b3dc;width:auto !important;margin-bottom:4px;" class="table">
						<tbody>
							<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Maximum&nbsp;Insurance<br/>
						($&nbsp;amount):
						</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.maxInsurance" name="partnerProfileInfo.maxInsurance"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Do&nbsp;we&nbsp;collect&nbsp;VI:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="collectYFlag" value="false" />
						<c:if test="${partnerProfileInfo.collectY}">
										<c:set var="collectYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.collectY" fieldValue="true" value="${collectYFlag}"  tabindex="" />&nbsp;YES
						</td>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="collectNFlag" value="false" />
						<c:if test="${partnerProfileInfo.collectN}">
										<c:set var="collectNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.collectN" fieldValue="true" value="${collectNFlag}"  tabindex="" />&nbsp;NO
						</td>
						</tr>
						</table>
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>If&nbsp;yes&nbsp;,&nbsp;where&nbsp;do&nbsp;we&nbsp;send&nbsp;it:</b></td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.InsSendDesc" name="partnerProfileInfo.InsSendDesc"  maxLength="45"    tabindex="" />
						</td>
						</tr>
						
						 <tr>
						<td class="listwhitetext" style="text-align:left;width: 10%;" colspan="2">
						<s:radio name="partnerProfileInfo.selfInsuredInv" list="{'Self Insured (We collect Valued Inventory)'}" onclick="changeStatus();" tabindex=""/>&nbsp;
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Deductible</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="byUsFlag" value="false" />
						<c:if test="${partnerProfileInfo.byUs}">
										<c:set var="byUsFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.byUs" fieldValue="true" value="${byUsFlag}"  tabindex="" />&nbsp;By&nbsp;Us
						</td>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="byAccFlag" value="false" />
						<c:if test="${partnerProfileInfo.byAcc}">
										<c:set var="byAccFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.byAcc" fieldValue="true" value="${byAccFlag}"  tabindex="" />&nbsp;By&nbsp;Account
						</td>
						</tr>
						</table>
						</td>
						
						<td class="listwhitetext" style="text-align:right;">
						<b>How&nbsp;do&nbsp;we&nbsp;settle&nbsp;claims:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="throughCarrFlag" value="false" />
						<c:if test="${partnerProfileInfo.throughCarr}">
										<c:set var="throughCarrFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.throughCarr" fieldValue="true" value="${throughCarrFlag}"  tabindex="" />&nbsp;Through&nbsp;our<br/>&nbsp;carrier
						</td>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="inHouseFlag" value="false" />
						<c:if test="${partnerProfileInfo.inHouse}">
										<c:set var="inHouseFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.inHouse" fieldValue="true" value="${inHouseFlag}"  tabindex="" />&nbsp;In&nbsp;House
						</td>
						</tr>
						</table>
						</td>
						</tr> 
						
						
						<tr>
						<td class="listwhitetext" style="text-align:left;width: 10%;" colspan="2">
						<s:radio name="partnerProfileInfo.selfInsured" list="{'Self Insured (No Involvement)'}"  tabindex=""/>
						</td>
						<td class="listwhitetext" style="background-color:#efefef;"></td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="2"></td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Do&nbsp;we&nbsp;charge&nbsp;for&nbsp;setting<br/>
						claims:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="claimYFlag" value="false" />
						<c:if test="${partnerProfileInfo.claimY}">
										<c:set var="claimYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.claimY" fieldValue="true" value="${claimYFlag}"  tabindex="" />&nbsp;YES
						</td>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="claimNFlag" value="false" />
						<c:if test="${partnerProfileInfo.claimN}">
										<c:set var="claimNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.claimN" fieldValue="true" value="${claimNFlag}"  tabindex="" />&nbsp;NO
						</td>
						</tr>
						</table>
						</td>
						</tr> 
						
						
						 <tr>
							<td class="listwhitetext" rowspan="2" style="text-align:left;" colspan="2">
								<s:radio name="partnerProfileInfo.siInsured" list="{'Suddath Insures'}"  tabindex=""/>
							</td>
							<td class="listwhitetext" style="text-align:right;">
								<b>Type&nbsp;of&nbsp;Insurance&nbsp;:</b>
							</td>
							<td class="listwhitetext" style="text-align:left;" colspan="2">
								<s:textfield cssClass="input-text" id="partnerProfileInfo.typeIns" name="partnerProfileInfo.typeIns"  maxLength="45"    tabindex="" />
							</td>
							<td class="listwhitetext" style="text-align:right;">
								<b>How&nbsp;do&nbsp;we&nbsp;charge:</b>
							</td>
							<td class="listwhitetext" style="text-align:left;">
								<s:textfield cssClass="input-text" id="partnerProfileInfo.chargeDesc" name="partnerProfileInfo.chargeDesc"  maxLength="45"    tabindex="" />
							</td>
							
						</tr> 
						
						<tr>
							<td class="listwhitetext" style="text-align:left;">
							<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="border:none;">
						<c:set var="fullValuedInvFlag" value="false" />
								<c:if test="${partnerProfileInfo.fullValuedInv}">
										<c:set var="fullValuedInvFlag" value="true" />
								</c:if>
								<s:checkbox key="partnerProfileInfo.fullValuedInv" fieldValue="true" value="${fullValuedInvFlag}"  tabindex="" />
						</td>
						<td style="border:none;">Full&nbsp;Valued&nbsp;Inventory&nbsp;Required</td>
						</tr>
						</table>
							</td>
							<td class="listwhitetext" style="text-align:right;">
								<b>Value&nbsp;based&nbsp;on&nbsp;(per&nbsp;lb):</b>
							</td>
							<td class="listwhitetext" style="text-align:left;">
								<s:textfield cssClass="input-text" id="partnerProfileInfo.valueBased" name="partnerProfileInfo.valueBased"  maxLength="45"    tabindex="" />
							</td>
							<td class="listwhitetext" style="text-align:right;">
								<b>Value&nbsp;based&nbsp;on&nbsp;(per&nbsp;lb.)w/<br/>
								&nbsp;high&nbsp;value&nbsp;list:
								</b>
							</td>
							<td class="listwhitetext" style="text-align:left;">
								<s:textfield cssClass="input-text" id="partnerProfileInfo.valueBasedWHL" name="partnerProfileInfo.valueBasedWHL"  maxLength="45"    tabindex="" />
							</td>
						</tr> 
						
						<tr>
							<td class="listwhitetext" style="text-align:right;">
								<b>Other&nbsp;Carrier&nbsp;(who):</b>
							</td>
							<td class="listwhitetext" style="text-align:left;">
								<s:textfield cssClass="input-text" id="partnerProfileInfo.otherCarrier" name="partnerProfileInfo.otherCarrier"  maxLength="45"    tabindex="" />
							</td>
							<td class="listwhitetext" style="background-color:#efefef;" ></td>
							<td class="listwhitetext" style="background-color:#efefef;" colspan="4"></td>
						</tr> 
						
						
						<tr>
							<td class="listwhitetext" style="text-align:right;">
								<c:set var="unInsurableItemsFlag" value="false" />
								<c:if test="${partnerProfileInfo.unInsurableItems}">
										<c:set var="unInsurableItemsFlag" value="true" />
								</c:if>
								<s:checkbox key="partnerProfileInfo.unInsurableItems" fieldValue="true" value="${unInsurableItemsFlag}"  tabindex="" />&nbsp;uninsurable&nbsp;items
							</td>
							<td class="listwhitetext" style="text-align:right;">
								<b>List&nbsp;uninsurable&nbsp;items</b>
							</td>
							<td class="listwhitetext" style="text-align:left;">
								<s:textfield cssClass="input-text" id="partnerProfileInfo.listUnSurableItems" name="partnerProfileInfo.listUnSurableItems"  maxLength="45" cssStyle="width:100%;"   tabindex="" />
							</td>
							<td class="listwhitetext" style="background-color:#efefef;" colspan="4"></td>
						</tr> 
						
						<tr>
							<td class="listwhitetext" style="text-align:right;">
								<b>Perm&nbsp;Storage</b>
							</td>
					<td class="listwhitetext" style="text-align:left;">
								<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="permStOYFlag" value="false" />
						<c:if test="${partnerProfileInfo.permStOY}">
										<c:set var="permStOYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.permStOY" fieldValue="true" value="${permStOYFlag}"  tabindex="" />&nbsp;YES
						</td>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="permStONFlag" value="false" />
						<c:if test="${partnerProfileInfo.permStON}">
										<c:set var="permStONFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.permStON" fieldValue="true" value="${permStONFlag}"  tabindex="" />&nbsp;NO
						</td>
						</tr>
						</table>
					</td>
					<td class="listwhitetext" style="background-color:#efefef;" ></td>
							<td class="listwhitetext" style="background-color:#efefef;" colspan="4"></td>
						</tr> 
						</tbody>
					</table>
				  </div>
					  			
					  	</td>
					  	</tr>
					  	</tbody>
					  	</table>
					  	
					  	
					  	
					  	
						
					  </div>
					 </td>
					</tr>
					
					<tr>						
						<td colspan="20" width="100%" align="left" style="margin: 0px">
     						<div onClick="javascript:animatedcollapse.toggle('reportDetails')" style="margin: 0px">
			 					<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 					<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Reporting&nbsp;Details</td>            
            						<td class="headtab_right"></td>
          						</tr>
								</table>
					  		</div>
					  		<div id="reportDetails">
					  		<table width="100%" cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px;">
								<tbody><tr class="pricetab_bg">
								<td width="215px" align="center" style="color:#0084c8;"><b>&nbsp;Reporting</b></td>
								</tr>
								</tbody></table>
					  		
						 <table style="margin:0px;border:2px solid #74b3dc;width:auto!important;margin-bottom:4px;" class="table">
						<tbody>
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Status&nbsp;Update&nbsp;Required:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="statusUpdateRequiredYFlag" value="false" />
						<c:if test="${partnerProfileInfo.statusUpdateRequiredY}">
										<c:set var="statusUpdateRequiredYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.statusUpdateRequiredY" fieldValue="true" value="${statusUpdateRequiredYFlag}"  tabindex="" />&nbsp;YES
						</td>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="statusUpdateRequiredNFlag" value="false" />
						<c:if test="${partnerProfileInfo.statusUpdateRequiredN}">
										<c:set var="statusUpdateRequiredNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.statusUpdateRequiredN" fieldValue="true" value="${statusUpdateRequiredNFlag}"  tabindex="" />&nbsp;NO
						</td>
						</tr>
						</table>
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="7"></td>
						</tr>
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Reports&nbsp;Required:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="reportYFlag" value="false" />
						<c:if test="${partnerProfileInfo.reportY}">
										<c:set var="reportYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.reportY" fieldValue="true" value="${reportYFlag}"  tabindex="" />&nbsp;YES
						</td>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="reportNFlag" value="false" />
						<c:if test="${partnerProfileInfo.reportN}">
										<c:set var="reportNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.reportN" fieldValue="true" value="${reportNFlag}"  tabindex="" />&nbsp;NO
						</td>
						</tr>
						</table>
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="7"></td>
						</tr> 
						
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Report&nbsp;Name:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.reportName1" name="partnerProfileInfo.reportName1" title="${partnerProfileInfo.reportName1}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Deliver&nbsp;To:&nbsp;(distribution&nbsp;list):</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.deliverTo1" name="partnerProfileInfo.deliverTo1" title="${partnerProfileInfo.deliverTo1}" maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Frequency:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.frequency1" name="partnerProfileInfo.frequency1"  title="${partnerProfileInfo.frequency1}" maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Due&nbsp;date:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" id="partnerProfileInfo.dueDate1" name="partnerProfileInfo.dueDate1" title="${partnerProfileInfo.dueDate1}"  maxLength="45"    tabindex="" />
						</td>
						</tr>
						
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Report&nbsp;Name:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.reportName2" name="partnerProfileInfo.reportName2" title="${partnerProfileInfo.reportName2}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Deliver&nbsp;To:&nbsp;(distribution&nbsp;list):</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.deliverTo2" name="partnerProfileInfo.deliverTo2" title="${partnerProfileInfo.deliverTo2}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Frequency:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.frequency2" name="partnerProfileInfo.frequency2" title="${partnerProfileInfo.frequency2}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Due&nbsp;date:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" id="partnerProfileInfo.dueDate2" name="partnerProfileInfo.dueDate2" title="${partnerProfileInfo.dueDate2}"  maxLength="45"    tabindex="" />
						</td>
						</tr>
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Report&nbsp;Name:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.reportName3" name="partnerProfileInfo.reportName3" title="${partnerProfileInfo.reportName3}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Deliver&nbsp;To:&nbsp;(distribution&nbsp;list):</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.deliverTo3" name="partnerProfileInfo.deliverTo3"  title="${partnerProfileInfo.deliverTo3}" maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Frequency:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.frequency3" name="partnerProfileInfo.frequency3" title="${partnerProfileInfo.frequency3}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Due&nbsp;date:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text"  id="partnerProfileInfo.dueDate3" name="partnerProfileInfo.dueDate3" title="${partnerProfileInfo.dueDate3}"  maxLength="45"    tabindex="" />
						</td>
						</tr>
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Report&nbsp;Name:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.reportName4" name="partnerProfileInfo.reportName4" title="${partnerProfileInfo.reportName4}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Deliver&nbsp;To:&nbsp;(distribution&nbsp;list):</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.deliverTo4" name="partnerProfileInfo.deliverTo4" title="${partnerProfileInfo.deliverTo4}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Frequency:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" size="30" id="partnerProfileInfo.frequency4" name="partnerProfileInfo.frequency4" title="${partnerProfileInfo.frequency4}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Due&nbsp;date:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" id="partnerProfileInfo.dueDate4" name="partnerProfileInfo.dueDate4"  title="${partnerProfileInfo.dueDate4}" maxLength="45"    tabindex="" />
						</td>
						</tr>
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>KPIs:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" id="partnerProfileInfo.kpi" name="partnerProfileInfo.kpi" title="${partnerProfileInfo.kpi}"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="7"></td>
						</tr>
						
						</tbody>
					</table>
					  		</div>
					  	</td>
					 </tr>
					 
					 <table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('reportAddInfo');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Reporting Additional Information</td>            
            						<td class="headtab_right"></td>
          							</tr>
										
									</table>
					  			</div>
					<div id="reportAddInfo">
					 <table style="margin:0px;border:2px solid #74b3dc;width:94% !important;margin-bottom:4px;" class="table">
						<tbody>
							<tr>
								<td class="listwhitetext" style="text-align:left;" colspan="9">
								<s:textarea name="partnerProfileInfo.reportAddInfo"  id="reportAddInfoId" required="true" cssClass="textarea" rows="3" cols="221"  />
																
								</td>
							</tr>
						</tbody>
					</table>
				  </div>
					</td>
					</tr>
					</tbody>
					</table>
					
					<tr>						
						<td colspan="20" width="100%" align="left" style="margin: 0px">
     						<div onClick="javascript:animatedcollapse.toggle('InvTerms')" style="margin: 0px">
			 					<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 					<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Invoicing&nbsp;Terms</td>            
            						<td class="headtab_right"></td>
          						</tr>
								</table>
					  		</div>
					  		<div id="InvTerms">
					  		
						 <table style="margin:0px;border:2px solid #74b3dc;width:94% !important;margin-bottom:4px;" class="table">
						<tbody>
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Invoice&nbsp;Method:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<c:set var="electronicFlag" value="false" />
						<c:if test="${partnerProfileInfo.electronic}">
										<c:set var="electronicFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.electronic" fieldValue="true" value="${electronicFlag}"  tabindex="" />&nbsp;Electronic
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<c:set var="hardCopyFlag" value="false" />
						<c:if test="${partnerProfileInfo.hardCopy}">
										<c:set var="hardCopyFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.hardCopy" fieldValue="true" value="${hardCopyFlag}"  tabindex="" />&nbsp;Hard&nbsp;Copy
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="6"></td>
						</tr>
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Batch&nbsp;Invoicing:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:100%;">
						<tr>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="batchInvYFlag" value="false" />
						<c:if test="${partnerProfileInfo.batchInvY}">
										<c:set var="batchInvYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.batchInvY" fieldValue="true" value="${batchInvYFlag}"  tabindex="" />&nbsp;YES
						</td>
						<td style="padding:2px !important;border:none;width:10px;">
						<c:set var="batchInvNFlag" value="false" />
						<c:if test="${partnerProfileInfo.batchInvN}">
										<c:set var="batchInvNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.batchInvN" fieldValue="true" value="${batchInvNFlag}"  tabindex="" />&nbsp;NO
						</td>
						</tr>
						</table>
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="7"></td>
						</tr> 
						
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Upload&nbsp;Detail:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
						<s:textfield cssClass="input-text" id="partnerProfileInfo.uploadDetails" name="partnerProfileInfo.uploadDetails"  maxLength="45" size="40"    tabindex="" />
						</td>
						<td class="listwhitetext" style="background-color:#efefef;" colspan="6"></td>
						</tr>
						
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Days&nbsp;to&nbsp;Invoice&nbsp;(from<br/>
						delivery&nbsp;date):</b></td>
						<td class="listwhitetext" style="text-align:right;">
						<c:set var="within30DaysFlag" value="false" />
						<c:if test="${partnerProfileInfo.within30Days}">
										<c:set var="within30DaysFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.within30Days" fieldValue="true" value="${within30DaysFlag}"  tabindex="" />&nbsp;within&nbsp;30&nbsp;Days
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<c:set var="within45DaysFlag" value="false" />
						<c:if test="${partnerProfileInfo.within45Days}">
										<c:set var="within45DaysFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.within45Days" fieldValue="true" value="${within45DaysFlag}"  tabindex="" />&nbsp;within&nbsp;45&nbsp;Days
						</td>
						<td class="listwhitetext" style="text-align: left; padding-left: 5px;">
						<c:set var="within60DaysFlag" value="false" />
						<c:if test="${partnerProfileInfo.within60Days}">
										<c:set var="within60DaysFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.within60Days" fieldValue="true" value="${within60DaysFlag}"  tabindex="" />&nbsp;within&nbsp;60&nbsp;Days
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<c:set var="within90DaysFlag" value="false" />
						<c:if test="${partnerProfileInfo.within90Days}">
										<c:set var="within90DaysFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.within90Days" fieldValue="true" value="${within90DaysFlag}"  tabindex="" />&nbsp;within&nbsp;90&nbsp;Days
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<c:set var="withinOtherFlag" value="false" />
						<c:if test="${partnerProfileInfo.withinOther}">
										<c:set var="withinOtherFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.withinOther" fieldValue="true" value="${withinOtherFlag}"  tabindex="" />&nbsp;other
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>if&nbsp;other&nbsp;,list&nbsp;days:</b>
						</td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
								<s:textfield cssClass="input-text" id="partnerProfileInfo.invoiceDelDateDesc" name="partnerProfileInfo.invoiceDelDateDesc"  maxLength="45"    tabindex="" />
						</td>
						</tr> 
						
						 <tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>Credit&nbsp;Terms:</b></td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" id="partnerProfileInfo.creditTerms" name="partnerProfileInfo.creditTerms"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Credit&nbsp;Approved:</b></td>
						<td class="listwhitetext" style="text-align:left;">
						<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;width:110px;">
						<tr>
						<td style="padding:2px !important;border:none;width:7px;">
						<c:set var="creditYFlag" value="false" />
						<c:if test="${partnerProfileInfo.creditY}">
										<c:set var="creditYFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.creditY" fieldValue="true" value="${creditYFlag}"  tabindex="" />&nbsp;YES
						</td>
						<td style="padding:2px !important;border:none;width:7px;">
						<c:set var="creditNFlag" value="false" />
						<c:if test="${partnerProfileInfo.creditN}">
										<c:set var="creditNFlag" value="true" />
						</c:if>
						<s:checkbox key="partnerProfileInfo.creditN" fieldValue="true" value="${creditNFlag}"  tabindex="" />&nbsp;NO
						</td>
						</tr>
						</table>
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Amount&nbsp;Approved:</b></td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" id="partnerProfileInfo.amountApproved" name="partnerProfileInfo.amountApproved"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;" colspan="2">
						<b>Audit&nbsp;Response&nbsp;SLA&nbsp;days&nbsp;to&nbsp;respond&nbsp;to&nbsp;Audit<br/>
						Enquiries:</b></td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" id="partnerProfileInfo.auditResponseDesc" name="partnerProfileInfo.auditResponseDesc"  maxLength="45"    tabindex="" />
						</td>
						</tr>  
						
						
						<tr>
						<td class="listwhitetext" style="text-align:right;">
						<b>A/R&nbsp;Contact:</b></td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
						<s:textfield  cssClass="input-text" id="partnerProfileInfo.arContact" name="partnerProfileInfo.arContact"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Email:</b></td>
						<td class="listwhitetext" style="text-align:left;" colspan="2">
						<s:textfield  cssClass="input-text" id="partnerProfileInfo.arContactEmail" name="partnerProfileInfo.arContactEmail"  maxLength="45" size="40"   tabindex="" />
						</td>
						<td class="listwhitetext" style="text-align:right;">
						<b>Phone:</b></td>
						<td class="listwhitetext" style="text-align:left;" >
						<s:textfield cssClass="input-text" id="partnerProfileInfo.arContactPhone" name="partnerProfileInfo.arContactPhone"  maxLength="45"    tabindex="" />
						</td>
						<td class="listwhitetext" style="background-color:#efefef;"></td>
						</tr>  
						
						
						
						
						</tbody>
					</table>
					  		</div>
					  		
					  	</td>
					 </tr>
					 <table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:2px 0px 0px 0px;">
						<tbody>
 		  					<tr>
 		  						<td>
 		  							<div  onClick="javascript:animatedcollapse.toggle('invoiceAddInfo');" style="margin: 0px"  >
			 						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			 						<tr>
            						<td class="headtab_left"></td>
            						<td width="28" valign="top" class="headtab_bg"></td>
            						<td nowrap class="headtab_bg_special_sudd">&nbsp;&nbsp;Invoicing Additional Information</td>            
            						<td class="headtab_right"></td>
          							</tr>
										
									</table>
					  			</div>
					<div id="invoiceAddInfo">
					 <table style="margin:0px;border:2px solid #74b3dc;width:94% !important;margin-bottom:4px;" class="table">
						<tbody>
							<tr>
								<td class="listwhitetext" style="text-align:left;" colspan="9">
								<s:textarea name="partnerProfileInfo.invoiceAddInfo"  id="invoiceAddInfoId" required="true" cssClass="textarea" rows="3" cols="221"  />
																
								</td>
							</tr>
						</tbody>
					</table>
				  </div>
					</td>
					</tr>
					</tbody>
					</table>
					 
					 
						
						
				</tbody></table>
						

   <!--Information new page end-->
	</div>
	</c:if>		
		
	<div style="margin-top: 46px;" class="bottom-header"><span></span></div>	
	</div>
	</div>
	</div>
	<table border="0" style="width:850px;">
		<tr>
			<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn' /></b></td>
			<fmt:formatDate var="profileInfoCreatedOnFormattedValue" value="${partnerProfileInfo.createdOn}" pattern="${displayDateTimeEditFormat}" />
			<s:hidden name="partnerProfileInfo.createdOn" value="${profileInfoCreatedOnFormattedValue}" />
			<td width="120px"><fmt:formatDate value="${partnerProfileInfo.createdOn}" pattern="${displayDateTimeFormat}" /></td>
			<td align="right" class="listwhitetext" style="width:105px"><b><fmt:message key='container.createdBy' /></td>
			<c:if test="${not empty partnerProfileInfo.id}">
				<s:hidden name="partnerProfileInfo.createdBy" />
				<td><s:label name="createdBy" value="%{partnerProfileInfo.createdBy}" /></td>
			</c:if>
			<c:if test="${empty partnerProfileInfo.id}">
				<s:hidden name="partnerProfileInfo.createdBy" value="${pageContext.request.remoteUser}" />
				<td><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
			</c:if>
			<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn' /></td>
			<fmt:formatDate var="profileInfoUpdatedOnFormattedValue" value="${partnerProfileInfo.updatedOn}" pattern="${displayDateTimeEditFormat}" />
			<s:hidden name="partnerProfileInfo.updatedOn" value="${profileInfoUpdatedOnFormattedValue}" />
			<td width="120px"><fmt:formatDate value="${partnerProfileInfo.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
			<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
			<c:if test="${not empty partnerProfileInfo.id}">
				<s:hidden name="partnerProfileInfo.updatedBy" />
				<td style="width:85px"><s:label name="updatedBy" value="%{partnerProfileInfo.updatedBy}" /></td>
			</c:if>
			<c:if test="${empty partnerProfileInfo.id}">
				<s:hidden name="partnerProfileInfo.updatedBy" value="${pageContext.request.remoteUser}" />
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
			</c:if>
		</tr>
	</table>

	<c:if test="${empty param.popup}">
		<input id="button.save" type="button" class="cssbutton"  value="Save" name="button.save" style="width:60px;" onclick="return submitPartner();" tabindex="" />
		<s:reset cssClass="cssbutton" key="Reset"  cssStyle="width:60px; height:25px " tabindex="" />
	</c:if>
	

</s:form>

<script type="text/javascript">

function submitPartner(){
	document.forms['partnerProfileInfo'].action= "savePartnerProfileInfo.html?partnerId=${partnerId}&partnerCode=${partnerCode}&partnerType=${partnerType}";
	 document.forms['partnerProfileInfo'].submit();
}

function checkEmail(targetElelemnt,id) {
	var elementValue=targetElelemnt.value;
	var status = false;     
	var emailRegEx = /^[A-Z0-9.'_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
	     if (elementValue.search(emailRegEx) == -1) {
	    	 targetElelemnt.value="";
	    	 document.getElementById(id).focus();
	    	 document.forms['partnerProfileInfo'].elements[id].focus();
	         alert("Please enter a valid email address.");
	     }
	     else {
	          status = true;
	     }
	     return status;
	}
	


document.getElementById('variousContacts').setAttribute('maxlength', '950');
document.getElementById('frieghtInvoice').setAttribute('maxlength', '250');
document.getElementById('specialInvoiceInstructions').setAttribute('maxlength', '250');
document.getElementById('triRegionalModelDesc').setAttribute('maxlength', '995');
document.getElementById('restrItemsDesc').setAttribute('maxlength', '250');
document.getElementById('intlBillingAdditionalInfoId').setAttribute('maxlength', '995');
document.getElementById('authorizationsExceptionsId').setAttribute('maxlength', '995');
document.getElementById('intlTransitInsAddInfoId').setAttribute('maxlength', '995');
document.getElementById('reportAddInfoId').setAttribute('maxlength', '995');
document.getElementById('invoiceAddInfoId').setAttribute('maxlength', '995');


</script>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
