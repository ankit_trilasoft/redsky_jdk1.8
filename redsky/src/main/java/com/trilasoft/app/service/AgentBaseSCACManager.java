package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.AgentBaseSCAC;

public interface AgentBaseSCACManager  extends GenericManager<AgentBaseSCAC, Long> {
	
	public List getbaseSCACList(String partnerCode, String baseCode, String sessionCorpID);

	public List getSCACList(String sessionCorpID);

	public List isExisted(String baseCode, String partnerCode, String code, String sessionCorpID);

}
