<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerRateGridList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerRateGridList.heading'/>"/>  
    
<style>
.tab{
border:1px solid #74B3DC;

}
</style> 
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partnerId}" />
<c:set var="fileID" value="%{partnerId}"/>
<s:hidden name="ppType" id ="ppType" value="AG" />
<c:set var="ppType" value="AG"/>

<s:form id="partnerRateGridList" action="" method="post" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="newmnav">
				  <ul>
				    <sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
				    	<li><a href="editPartnerPublic.html?id=${partnerPublic.id}&partnerType=AG" ><span>Agent Detail</span></a></li>
			  			<li><a href="findPartnerProfileList.html?code=${partnerPublic.partnerCode}&partnerType=AG&id=${partnerPublic.id}"><span>Agent Profile</span></a></li>
			  			<c:if test="${sessionCorpID!='TSFT' }">
			  			<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerType=AG&partnerCode=${partnerPublic.partnerCode}"><span>Additional Info</span></a></li>
			  			</c:if>
							<c:if test="${not empty partnerPublic.id}">
								<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=AG"><span>Acct Ref</span></a></li>
								<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=AG"><span>Vanline Ref</span></a></li>
								<li><a href="baseList.html?id=${partnerPublic.id}"><span>Base</span></a></li>
							</c:if>
					
						<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
						<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
						<c:if test='${paramValue == "View"}'>
							<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
							<!--<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						--></c:if>
						<c:if test='${paramValue != "View"}'>
							<li><a href="partnerPublics.html?partnerType=AG"><span>Partner List</span></a></li>
							<!--<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
							--><li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=AG"><span>Services</span></a></li>
						</c:if>
						
					</sec-auth:authComponent>
					<c:if test="${partnerPublic.partnerPortalActive == true}">
						<sec-auth:authComponent componentId="module.partnerPublic.section.partnerPortalDetail.edit"> 
							<li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=AG"><span>Portal Users & contacts</span></a></li>
							<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=AG&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
						</sec-auth:authComponent>
					</c:if>
					<sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
						<li><a href="editPartnerPublic.html?id=${partnerPublic.id}&partnerType=AG" ><span>Agent Detail</span></a></li>
						
				  			<li><a href="partnerUsersList.html?id=${partnerPublic.id}"><span>Users & contacts</span></a></li>
						
			  			<!--<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						--><li><a href="partnerAgent.html"><span>Agent List</span></a></li>
					</sec-auth:authComponent>
				  </ul>
		</div>
		<div style="width:100%;">
		<div class="spn">&nbsp;</div>
	
		</div>
<div id="Layer4" style="width:100%;">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:100px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerRateGrid.html?partnerId=${partnerPublic.id}"/>'"  
        value="Add New Tariff"/>   
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0" border="0" width="100%">
	<tbody>
		<tr>
			<td >
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
				<table  cellspacing="1" cellpadding="1" border="0" >
					<tbody>
						<tr>
							<td width="" height="30px"><s:label label="  "/></td>
							<td class="listwhitetext"  ><b>Rates For</b></td>
							<td width="50px"><s:hidden name="partnerPublic.id" />
							    <s:textfield cssClass="input-textUpper" name="partnerPublic.partnerCode" required="true" size="15" readonly="true"/>
							</td>
							<td>
							    <s:textfield cssClass="input-textUpper" name="partnerPublic.lastName" required="true" size="50" readonly="true"/>
							</td>
						</tr>
						<tr><td style="!height:25px;" ></td></tr>
					</tbody>
				</table>
				</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<s:set name="partnerRatesGrids" value="partnerRatesGrids" scope="request"/>  
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>List</span></a></li>
		  </ul>
</div>
<div class="spnblk">&nbsp;</div>
<display:table name="partnerRatesGrids" class="table" requestURI="" id="partnerRateGridList" export="true" defaultsort="2" defaultorder="descending" pagesize="10" >   
	 <display:column sortable="true" title="Tariff #" style="width: 100px;">
    	<a href="#" onclick="location.href='<c:url value="/editPartnerRateGrid.html?partnerId=${partnerPublic.id}&id=${partnerRateGridList.id}" />'"/>
			<c:out value="${partnerRateGridList.id}" /></a>
    </display:column>
    
    <display:column sortable="true" title="PartnerCode" style="width: 100px;">
    		<c:out value="${partnerRateGridList.partnerCode}" /></a>
    </display:column>
    <display:column sortable="true" title="Effective From Date" property="effectiveDate" style="width: 110px;" format="{0,date,dd-MMM-yyyy}"/>
    <display:column sortable="true" title="Tariff Applicability" property="tariffApplicability" style="width: 100px;"/>
    <display:column sortable="true" title="Metro City" property="metroCity" />
    <display:column sortable="true" title="Country" property="terminalCountry" /> 
    <display:column sortable="true" title="Status" property="status" /> 
   <sec-auth:authComponent componentId="module.button.rateGrid.removeButton">
    <display:column title="Remove" style="width: 15px;">
		<a>
			<img align="middle" title="" onclick="confirmSubmit('${partnerRateGridList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
		</a>
	</display:column>
   </sec-auth:authComponent> 

    <display:setProperty name="paging.banner.item_name" value="partnerRates"/>   
    <display:setProperty name="paging.banner.items_name" value="partnerRatesGrids"/>  
    <display:setProperty name="export.excel.filename" value="PartnerRates List.xls"/>   
    <display:setProperty name="export.csv.filename" value="PartnerRates List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="PartnerRates List.pdf"/>   
</display:table>  
</td>
</tr>
</tbody>
</table> 
<table>
<c:out value="${buttons}" escapeXml="false" /> 
<tr><td style="height:60px; !height:100px"></td></tr></table>
</div>
</s:form>
<script type="text/javascript">   
   function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure to delete this entry?");
	if (agree){
			location.href = 'deleteRateGrid.html?id='+encodeURI(targetElement)+'&partnerId='+${partnerPublic.id};
	   }else{
		return false;
	}
}
<c:if test="${hitFlag == 1}" >
		<c:redirect url="/partnerRateGrids.html">
		<c:param name="partnerId" value="${partnerId}"/>
		</c:redirect>
</c:if>
</script>