package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PartnerPublicDao;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.service.PartnerPublicManager;

public class PartnerPublicManagerImpl extends GenericManagerImpl<PartnerPublic, Long> implements PartnerPublicManager {   
	    PartnerPublicDao partnerPublicDao;   
	  
	    public PartnerPublicManagerImpl(PartnerPublicDao partnerPublicDao) {   
	        super(partnerPublicDao);   
	        this.partnerPublicDao = partnerPublicDao;   
	    }

		public List getPartnerPublicList(String partnerType, String corpID, String firstName, String lastName, String aliasName,String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean isIgnoreInactive, String extRefernece,String vanlineCode,String typeOfVendor, boolean cmmdmmflag, String fidiNumber,String OMNINumber,String AMSANumber,String WERCNumber,String IAMNumber,String utsNumber ,String eurovanNetwork ,String PAIMA,String LACMA) {
			return partnerPublicDao.getPartnerPublicList(partnerType, corpID, firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, isPP, isAcc, isAgt, isVndr, isCarr, isOO, isIgnoreInactive, extRefernece,vanlineCode, typeOfVendor,cmmdmmflag,  fidiNumber, OMNINumber, AMSANumber, WERCNumber, IAMNumber, utsNumber , eurovanNetwork , PAIMA, LACMA);
		}
		
		public List getPartnerPublicListByPartnerView(String partnerType, String corpID, String firstName, String lastName, String aliasName,String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String status, Boolean isPP, Boolean isAcc, Boolean isAgt, Boolean isVndr, Boolean isCarr, Boolean isOO, Boolean isIgnoreInactive, String extRefernece) {
			return partnerPublicDao.getPartnerPublicListByPartnerView(partnerType, corpID, firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, isPP, isAcc, isAgt, isVndr, isCarr, isOO, isIgnoreInactive, extRefernece);
		}

		public List findMaxByCode(String initial) {
			return partnerPublicDao.findMaxByCode(initial);
		}

		public List findPartnerCode(String partnerCode) {
			return partnerPublicDao.findPartnerCode(partnerCode);
		}   
		public List findPartnerpublicCode(String partnerCode) {
			return partnerPublicDao.findPartnerpublicCode(partnerCode);
		}
		
		public List findPartnerId(String partnerCode, String sessionCorpID){
			return partnerPublicDao.findPartnerId(partnerCode, sessionCorpID);
		}
		
		public PartnerPublic getByID(Long id){
			return partnerPublicDao.getByID(id);
		}

		public List getPartnerPublicMergeList(String partnerType, String sessionCorpID, String firstName, String lastName,String aliasName, String partnerCode, String billingCountryCode, String billingCountry, String billingStateCode, String status, Boolean isPrivateParty, Boolean isAccount, Boolean isAgent, Boolean isVendor, Boolean isCarrier, Boolean isOwnerOp, String city) {
			return partnerPublicDao.getPartnerPublicMergeList(partnerType, sessionCorpID, firstName, lastName,aliasName, partnerCode, billingCountryCode, billingCountry, billingStateCode, status, isPrivateParty, isAccount, isAgent, isVendor, isCarrier, isOwnerOp, city);
		}
		
		public StringBuffer updateMaregedParter(String userCheck, String partnerCode, String corpID){
			return partnerPublicDao.updateMaregedParter(userCheck, partnerCode , corpID);
		}
		
		public PartnerPublic getPartnerByCode(String partnerCode){
			return partnerPublicDao.getPartnerByCode(partnerCode);
		}

		public List getAgentBaseByPartnerCode(String partnerCode, String corpID) {
			return partnerPublicDao.getAgentBaseByPartnerCode(partnerCode, corpID);
		}

		public List getAgentBaseSCACByPartnerCode(String partnerCode, String baseCode, String corpID) {
			return partnerPublicDao.getAgentBaseSCACByPartnerCode(partnerCode, baseCode, corpID);
		}

		public List getAccLineRefByPartnerCode(String partnerCode, String corpID) {
			return partnerPublicDao.getAccLineRefByPartnerCode(partnerCode, corpID);
		}

		public List getAccContactByPartnerCode(String partnerCode, String corpID) {
			return partnerPublicDao.getAccContactByPartnerCode(partnerCode, corpID);
		}

		public List getAccProfileByPartnerCode(String partnerCode, String corpID) {
			return partnerPublicDao.getAccProfileByPartnerCode(partnerCode, corpID);
		}

		public List getContPolicyByPartnerCode(String partnerCode, String corpID) {
			return partnerPublicDao.getContPolicyByPartnerCode(partnerCode, corpID);
		}

		public List getFAQByPartnerCode(String partnerCode, String corpID){
			return partnerPublicDao.getFAQByPartnerCode(partnerCode,corpID);
		}
		
		public List getPolicyByPartnerCode(String partnerCode, String corpID){
			return partnerPublicDao.getPolicyByPartnerCode(partnerCode, corpID);
		}
		
		public StringBuffer updatePartnerVanLineRef(String userCheck, String partnerCode, String corpID, String partnerCodewithoutMaster, String masterPartnerCode) {
			return partnerPublicDao.updatePartnerVanLineRef(userCheck, partnerCode, corpID,partnerCodewithoutMaster, masterPartnerCode);
		}

		public StringBuffer updateCodes(String userCheck, String partnerCode, String lastName, String corpID) {
			return partnerPublicDao.updateCodes(userCheck, partnerCode, lastName, corpID);
		}
		
		public List isExistAccContact(String corpID, String partnerCode){
			return partnerPublicDao.isExistAccContact(corpID, partnerCode);
		}

		public List isExistAccProfile(String corpID, String partnerCode) {
			return partnerPublicDao.isExistAccProfile(corpID, partnerCode);
		}

		public List isExistContractPolicy(String corpID, String partnerCode) {
			return partnerPublicDao.isExistContractPolicy(corpID, partnerCode);
		}

		public void updateStatus(String partnerCode) {
			partnerPublicDao.updateStatus(partnerCode);
		}
		public List  findOwnerOperatorList(String parentId,String corpId){
		return partnerPublicDao.findOwnerOperatorList(parentId, corpId);
		}
		public void deleteAgentParent(Long id,String partnerCode,String corpID){
			partnerPublicDao.deleteAgentParent(id,partnerCode,corpID);
		}
		public StringBuffer updateMaregedParterPrivate(String userCheck, String partnerCode, Long id) {
			return partnerPublicDao.updateMaregedParterPrivate(userCheck, partnerCode, id);
		}

		public StringBuffer updateMaregedParterSameCorpid(String userCheck, String partnerCode, String corpID) {
			return partnerPublicDao.updateMaregedParterSameCorpid(userCheck, partnerCode, corpID);
		}

		public int updateConvertPartnerCorpid(String userCheck) {
			return partnerPublicDao.updateConvertPartnerCorpid(userCheck);
		}

		public String findPartnerCodeCount(String partnerCode) { 
			return partnerPublicDao.findPartnerCodeCount(partnerCode);
		}
		public List getFindDontNerge(String corpId,String partnerCode){
		return	partnerPublicDao.getFindDontNerge(corpId,partnerCode);
		}
		public String getCountryCode(String billingCountryCode, String parameter,String sessionCorpID){
			return partnerPublicDao.getCountryCode(billingCountryCode, parameter, sessionCorpID);
		}
		public List getUserNameList(String sessionCorpID, String partnerCode){
			return partnerPublicDao.getUserNameList(sessionCorpID, partnerCode);
		}

		public List getPartnerChildList(String sessionCorpID, String partnerCode) {
			return partnerPublicDao.getPartnerChildList(sessionCorpID, partnerCode);
		}
		public Map<String, String> getPartnerChildMap(String sessionCorpID, Long partnerCode) {
			return partnerPublicDao.getPartnerChildMap(sessionCorpID, partnerCode);
		}
		public Boolean getExcludeFPU(String partnerCode,String corpId){
			return partnerPublicDao.getExcludeFPU(partnerCode, corpId);
		}
		public List getPartnerPublicDefaultAccount(String partnerCode,String corpId){
			return partnerPublicDao.getPartnerPublicDefaultAccount(partnerCode, corpId);
		}
		public List getPartnerPublicChildAccount(String partnerCode,String corpId){
			return partnerPublicDao.getPartnerPublicChildAccount(partnerCode, corpId);
		}
		public List getVanLine(String partnerVanLineCode, String sessionCorpID){
			return partnerPublicDao.getVanLine(partnerVanLineCode,sessionCorpID);
		}
		public Map<String, String> getCountryWithBranch(String parameter,String sessionCorpID){
			return partnerPublicDao.getCountryWithBranch(parameter,sessionCorpID);
		}
		public List agentUpdatedIn24Hour(){
			return partnerPublicDao.agentUpdatedIn24Hour();
		}
		public Boolean getPrivatePartyStatus(String billToCode, String corpId){
			return partnerPublicDao.getPrivatePartyStatus(billToCode, corpId);
		}
		public void updateDefaultAccountLine(String corpId,String partnerCode,String partnerType,String userName) {
			partnerPublicDao.updateDefaultAccountLine(corpId,partnerCode,partnerType,userName);
		}
	    public PartnerPublic checkById(Long id){
			return partnerPublicDao.checkById(id);		}
	    public int updateAgentParentName(String lastName,String partnerCode,String updatedBy) {
	    	
	    	return partnerPublicDao.updateAgentParentName(lastName,partnerCode,updatedBy);		
	    }

		public void updatePartnerPortal(String partnerCode) {
			 partnerPublicDao.updatePartnerPortal(partnerCode);
		}


		
}
