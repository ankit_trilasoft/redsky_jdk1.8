package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.EquipMaterialsLimits;
import com.trilasoft.app.model.OperationsResourceLimits;

public interface EquipMaterialsLimitsDao   extends GenericDao<EquipMaterialsLimits, Long>{
	public List getEquipMaterialLimitList(String branch,String div,String cat);
	public void saveEquipMaterialObject(EquipMaterialsLimits obj);
	public EquipMaterialsLimits getByDesc(String cat,String corpid);
	public void updateEquipMaterialObject(EquipMaterialsLimits equipMaterialsLimits,String cngres,String cngwhouse);
	public Map<String,String> getCompanyDivisionMap(String corpid);

}
