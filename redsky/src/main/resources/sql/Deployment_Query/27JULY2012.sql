ALTER TABLE `redsky`.`charges` ADD COLUMN `grossMargin` BIT(1) DEFAULT b'0' AFTER `commissionable`;

ALTER TABLE `redsky`.`billing` MODIFY COLUMN `sequenceNumber` VARCHAR(20)  DEFAULT NULL, MODIFY COLUMN `shipNumber` VARCHAR(20)  DEFAULT NULL;

ALTER TABLE `redsky`.`customerfile` MODIFY COLUMN `sequenceNumber` VARCHAR(20) DEFAULT NULL;

ALTER TABLE `redsky`.`miscellaneous` MODIFY COLUMN `sequenceNumber` VARCHAR(20)  DEFAULT NULL,MODIFY COLUMN `shipNumber` VARCHAR(20)  DEFAULT NULL;

ALTER TABLE `redsky`.`serviceorder` MODIFY COLUMN `sequenceNumber` VARCHAR(20) DEFAULT NULL,MODIFY COLUMN `shipNumber` VARCHAR(20)  DEFAULT NULL;

ALTER TABLE `redsky`.`trackingstatus` MODIFY COLUMN `sequenceNumber` VARCHAR(20) DEFAULT NULL,MODIFY COLUMN `shipNumber` VARCHAR(20)  DEFAULT NULL;

delete from refmaster where parameter= 'SERVICE' and flex1 ='Relo' and description='Network';

ALTER TABLE `redsky`.`company` ADD COLUMN `automaticLinkup` BIT(1) AFTER `postingDateStop`;

DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`findnetworkcorpid` $$
CREATE DEFINER=`root`@`localhost` FUNCTION `n`(code varchar(10)) RETURNS varchar(10) CHARSET utf8
    DETERMINISTIC
BEGIN
DECLARE Name varchar(10) DEFAULT 0;

SELECT distinct agentcorpid INTO Name FROM networkpartners WHERE agentpartnercode=code;

RETURN Name;

END $$

DELIMITER ;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type) values('AccountLine','activateAccPortal' ,'Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','activateAccPortal' ,'Update','amishra',now(),'amishra',now(),false,'','','');

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `truckRequired` VARCHAR(1) DEFAULT NULL AFTER `commissionable`;

ALTER TABLE `redsky`.`notes` ADD COLUMN `category` VARCHAR(10) DEFAULT '' AFTER `complitionDate`,ADD COLUMN `uvlSentStatus` VARCHAR(10) DEFAULT '' AFTER `category`;
 
Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby) values('TSFT', 'Process', 'Send to UVL', 50, 'UVLMemoXfer','en',now(),'dkumar',now(),'dkumar'),('TSFT', 'Sent', 'Successfully sent', 50, 'UVLMemoXfer','en',now(),'dkumar',now(),'dkumar'),('TSFT', 'Error', 'Error', 50, 'UVLMemoXfer','en',now(),'dkumar',now(),'dkumar');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','UVLMemoXfer',50,true,now(),'dkumar',now(),'dkumar',1,1,'');
 
Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'AGTF', 'File/ Document are in agents file room', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'AUDT', 'Invoice completion of audit forwarded to account', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'AUTH', 'Comments authorizing special controls (e.g extra labor, wait time, etc.)', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'BILL', 'Billing comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'CAS', 'Corporate account services', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'CCLM', 'Cargo claims comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'COLL', 'Collection information', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'CSC', 'Customer service center comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'DCLM', 'Delay claim comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'DISP', 'Trade show phone-mail notification (used for display and exhibit shipments only)', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'EDI', 'Special EDI data entry requirements (e.g. mutiple PO numbers)', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'EXCP', 'Inventory exceptions', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'FILE', 'File/ Documents are in Unigroup s file room at World Headquarters', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'INTL', 'International shipments comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'MISC', 'Miscellaneous Notes', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'MKTG', 'Marketing', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'OSI', 'Order status comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'PRIC', 'Pricing comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'QUOT', 'Safeguards/ spec comm. Quote comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'OPTS', 'Operations dispatch comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'REGS', 'Registration comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'SIT', 'Destination SIT comments', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'SPEC', 'Special Instructions', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'SUPP', 'Supplemental billing information', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', 'UTIL', 'Changes made to order using utilities screen', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar'),
('TSFT', '24HR', '24 hour mandatory call confirmations', 150, 'UVLMemoCategory','en',now(),'dkumar',now(),'dkumar');
 
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','UVLMemoCategory',150,true,now(),'dkumar',now(),'dkumar',1,1,'');

ALTER TABLE role ADD COLUMN category VARCHAR(20) DEFAULT NULL;

CREATE TABLE `accountassignmenttype` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `assignment` VARCHAR(500),
  `description` TEXT,
  `updatedOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `createdOn` DATETIME,
  `createdBy` VARCHAR(82),
  `partnerPrivateId` BIGINT(20) DEFAULT 0,
  `corpID` VARCHAR(45),
  `partnerCode` VARCHAR(8),
  `parentId` BIGINT(20) UNSIGNED DEFAULT 0,
   PRIMARY KEY  (`id`)
  );
 
ALTER TABLE `redsky`.`role` MODIFY COLUMN `category` VARCHAR(40);






