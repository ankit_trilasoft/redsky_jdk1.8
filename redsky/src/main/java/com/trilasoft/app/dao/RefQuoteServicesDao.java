package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.RefQuoteServices;

public interface RefQuoteServicesDao extends GenericDao<RefQuoteServices,Long> {
	public List searchList(String job, String mode,String routing, String langauge, String sessionCorpID ,String description,String checkIncludeExclude,String companyDivision,String commodity,String packingMode,String originCountry,String destinationCountry,String serviceType,String contract);
	public void autoSaveRefQuotesSevisesAjax(String sessionCorpID,String fieldName,String fieldVal,Long serviceId);
	public Map<String, String> findForContract(String corpID) ;
	/*public String findUniqueCode(String code, String sessionCorpID);*/
}
