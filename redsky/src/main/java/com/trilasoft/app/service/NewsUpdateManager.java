package com.trilasoft.app.service;
import com.trilasoft.app.model.NewsUpdate;
import org.appfuse.service.GenericManager;

import java.util.Date;
import java.util.List;

public interface NewsUpdateManager extends GenericManager<NewsUpdate, Long> {
	   public List<NewsUpdate> findByDetail(String details);
	   public List findUpdatedDate(String corpId);
	   public List findDistinct();
	   public List findListOnCorpId(String corpId,Boolean addValues);
	   public List findListByModuleCorp(String module,String corpid, Date publishedDate,String isVisible,Boolean activeStatus);
	   public List findByModule(String module,String corpid,Date publishedDate);
       public String findNewsUpdateDescForToolTip(Long id, String corpId);
       
   }