<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/> 
    <style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}
.table tfoot td {
border:1px solid #E0E0E0;
}
.table tfoot th, tfoot td {
background:#BCD2EF url(images/bg_listheader.png) repeat scroll 0%;
border-color:#3dafcb rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

span.pagelinks {
display:block;
font-size:0.8em;
margin-bottom:4px;
margin-top:-22px;
padding:2px 0;
text-align:right;
width:100%
}
.table thead th, .tableHeaderTable td .headerClassLeft .sortable{
text-align:left;
}


.table thead th, .tableHeaderTable td {
background:#BCD2EF url(images/bg_listheader.png) repeat-x scroll 0 0;
border-color:#3dafcb #3dafcb #3dafcb -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tr.odd {
color:#000d47;
}

.table tr.even {
color:#000000;
background-color:{#e4e6f2}
}

.table tr:hover {
color:#0b0148;
background-color:{#fffcd6}
}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
 form {margin-top:-10px;}
</style>

</head>
<div id="Layer5" style="width:100%">
<s:form id="costingDetail" name="costingDetail" action="saveCostingDetail" method="post"> 
  <c:set var="totalactualRevenueForeign" value="${0}" />
  <c:set var="totalActualRevenueForeignInvoice" value="${0}" />
  <c:set var="totalEstCost" value="${0}" />
  <c:set var="totalRevisionCost" value="${0}" />
<s:hidden name="customerFile.id"/>
<s:hidden name="serviceOrder.id" />
<s:hidden name="accLineStatusList"/>
<s:hidden name="serviceOrder.sequenceNumber" />
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session" /> 
			    <c:set var="noteID"	value="${serviceOrder.shipNumber}" scope="session" /> 
			    <c:set var="noteFor" value="ServiceOrder" scope="session" /> 
			    <c:if test="${empty serviceOrder.id}">
				<c:set var="isTrue" value="false" scope="request" />
			    </c:if> 
			    <c:if test="${not empty serviceOrder.id}">
				<c:set var="isTrue" value="true" scope="request" />
			    </c:if>
<s:hidden name="id" value="<%=request.getParameter("sid") %>"/>
<c:set var="id"  value="<%=request.getParameter("sid") %>"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="newmnav" style="float:left;">
		  <ul>
		  <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
	        <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
	        <c:set var="relocationServicesKey" value="" />
	        <c:set var="relocationServicesValue" value="" /> 
	        <c:forEach var="entry" items="${relocationServices}">
                <c:if test="${relocationServicesKey==''}">
                <c:if test="${entry.key==serviceOrder.serviceType}">
	               <c:set var="relocationServicesKey" value="${entry.key}" />
	               <c:set var="relocationServicesValue" value="${entry.value}" /> 
                </c:if>
                </c:if> 
            </c:forEach>
            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		  	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
		  	</sec-auth:authComponent>
		  		 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
      		  	<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    <sec-auth:authComponent componentId="module.accountingPortalTab.serviceorder.operationResourceTab">
		      <li><a href="operationResourceFromAcPortal.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
		      </sec-auth:authComponent>		  	
		      </c:if>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Costing</span></a></li>
		  	<sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
		  	<c:if test="${usertype!='ACCOUNT'}">
		  	<li><a href="containers.html?id=${serviceOrder.id}" ><span>Forwarding</span></a></li>
		  	</c:if>
		  	<c:if test="${usertype=='ACCOUNT' && serviceOrder.job !='RLO'}">
		 	 <%-- 			  	<c:if test="${serviceOrder.corpID!='CWMS' || (serviceOrder.job !='OFF' && serviceOrder.corpID=='CWMS')}"> --%>
	  	<c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">  
              <li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
              </c:if>
             </c:if>
		  	</sec-auth:authComponent>
		  	<sec-auth:authComponent componentId="module.tab.costing.statusTab">
		  	<c:if test="${serviceOrder.job=='RLO'}">  
                <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
             </c:if>
             <c:if test="${serviceOrder.job!='RLO'}"> 
		  	<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
		  	</c:if>
		  	</sec-auth:authComponent>
		  	<sec-auth:authComponent componentId="module.tab.summary.summaryTab">
		  	<c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">  
		  <%--	 	<c:if test="${serviceOrder.corpID!='CWMS' || (serviceOrder.job !='OFF' && serviceOrder.corpID=='CWMS')}"> --%>
				<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
				</c:if>
			</sec-auth:authComponent>
			 <configByCorp:fieldVisibility componentId="component.standard.claimTab">	
		  	<c:if test="${serviceOrder.job!='RLO'}"> 
		  	<sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
		    <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
		  	 </sec-auth:authComponent>
		  	</c:if>
		  	</configByCorp:fieldVisibility>
		  	<sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
             <li><a  href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.costing.reportTab">
			  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Claims&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			  </sec-auth:authComponent>
		  	<sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
		  	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
		  	</sec-auth:authComponent>
		   </ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right" style="vertical-align:top;">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left" style="vertical-align:top;">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" style="vertical-align:top;!padding-top:1px;">		
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>
		
		</c:if></tr></table>
			<c:if test="${not empty serviceOrder.id }">
			<div class="spn">&nbsp;</div>
			</c:if>
	</div>
<div id="content" align="center">
	
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%> 
	
</div>	
		<div class="spn">&nbsp;</div>
<div style="!margin-top:5px;">
<display:table name="accountCostLineList" class="table" requestURI="" id="accountLineList" style="width:99%;margin-left:5px;margin-top:2px;" defaultsort="1" pagesize="100" >
	
	<display:column sortable="true" titleKey="accountLine.accountLineNumber"  style="width:20px;" ><c:out value="${accountLineList.accountLineNumber}" /></display:column>
	
	<display:column property="chargeCode" sortable="true" title="Charge Code" style="width:60px; border-right:medium solid #3DAFCB; " />
	<%-- <display:column headerClass="containeralign" sortable="true"  title="Entitle. Amount" style="text-align:right !important; border-right:medium solid #3DAFCB; ">
	       <fmt:formatNumber  type="number" maxFractionDigits="0" groupingUsed="true" value="${accountLineList.entitlementAmount}" />
  </display:column>
    --%>
    <c:if test="${multiCurrency!='Y' }">
   <display:column   sortable="true" sortProperty="estimateRevenueAmount" title="Estimate Cost" style="text-align:right; width:100px ">
		  <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${accountLineList.estimateRevenueAmount}" /></div>
   </display:column>
    <display:column   title="Currency Code" style="border-right:medium solid #3DAFCB;" >
       <c:out value="${ baseCurrencyAccountPortal }" /></display:column>
    </c:if>
      <c:if test="${multiCurrency=='Y' }">
   <display:column   sortable="true" sortProperty="estSellLocalAmount" title="Estimate Cost" style="text-align:right; width:100px ">
		  <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${accountLineList.estSellLocalAmount}" /></div>
                  <c:set var="totalEstCost" value="${totalEstCost+accountLineList.estSellLocalAmount }" />
   </display:column>
    <display:column  property="estSellCurrency" title="Currency Code" style="border-right:medium solid #3DAFCB;"  sortable="true" />
      
    </c:if>
    <c:if test="${DisplayPortalRevision}">
    <c:if test="${multiCurrency!='Y' }">
   <display:column  sortable="true" sortProperty="revisionRevenueAmount" title="Revision Cost" style=" text-align:right; width:100px ">
		   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${accountLineList.revisionRevenueAmount}" /></div>
   </display:column>
    <display:column   title="Currency Code" style="border-right:medium solid #3DAFCB;" >
       <c:out value="${ baseCurrencyAccountPortal }" /></display:column> 
   </c:if>
   <c:if test="${multiCurrency=='Y' }">
   <display:column  sortable="true" sortProperty="revisionSellLocalAmount" title="Revision Cost" style=" text-align:right; width:100px ">
		   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${accountLineList.revisionSellLocalAmount}" /></div>
                  <c:set var="totalRevisionCost" value="${totalRevisionCost + accountLineList.revisionSellLocalAmount }" />
   </display:column>
    <display:column property="revisionSellCurrency"  title="Currency Code" style="border-right:medium solid #3DAFCB;" sortable="true" />
    </c:if>
    </c:if>
    <c:if test="${serviceOrder.corpID != 'UTSI' }">
	   <display:column sortable="true"  sortProperty="estimateVendorName" title="Vendor Name"  style="width:200px" >
	    	<div style="text-align:left;"   onmouseover="findPartnerDetail('${accountLineList.vendorCode}',this);" >
	   		<c:if test="${accountLineList.estimateVendorName != '' && accountLineList.estimateVendorName != null}">
	   		<c:set var="string2" value="${fn1:substring(accountLineList.estimateVendorName, 0, 30)}" />
	   		<c:out value="${string2}"  />
	   		</c:if>
	   		</div>
	   </display:column>
  </c:if>
   <c:if test="${multiCurrency!='Y' }">
       <display:column  sortable="true" sortProperty="actualRevenue"  title="Actual Cost" style="text-align:right;">
            <div style="text-align:right;" ><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenue}"  maxFractionDigits="2"  minFractionDigits="2" groupingUsed="true"/>
           </div>
       </display:column>
       <display:column   title="Currency Code"  >
       <c:out value="${ baseCurrencyAccountPortal }" /></display:column>
    </c:if>
  
      <c:if test="${multiCurrency=='Y'}"> 
        <display:column  sortable="true" sortProperty="actualRevenueForeign"  title="Actual Cost" style="text-align:right;" >
            <div style="text-align:right;" ><fmt:formatNumber type="number"  
            value="${accountLineList.actualRevenueForeign}"  maxFractionDigits="2"  minFractionDigits="2" groupingUsed="true"/>
           </div>
            
            <c:set var="totalactualRevenueForeign" value="${totalactualRevenueForeign+accountLineList.actualRevenueForeign}" />
            <c:choose>
            <c:when test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
            <c:set var="totalActualRevenueForeignInvoice" value="${totalActualRevenueForeignInvoice+accountLineList.actualRevenueForeign}" />
            </c:when>
            <c:otherwise>
            <c:set var="totalActualRevenueForeignInvoice" value="${totalActualRevenueForeignInvoice}" />
            </c:otherwise>
            </c:choose>
      
       </display:column>
       <display:column  sortable="true" sortProperty="recRateCurrency"  title="Currency Code" style="text-align:right;" >
      	 <c:out value="${accountLineList.recRateCurrency}" />
       </display:column>    
     </c:if>  
    <display:column property="recInvoiceNumber" sortable="true" titleKey="accountLine.recInvoiceNumber"  style="width:50px;text-align:right;" />
    
    <display:column property="receivedInvoiceDate" sortable="true" titleKey="accountLine.receivedInvoiceDate"  style="width:50px; !width:110px" format="{0,date,dd-MMM-yyyy}"/>
    
     <display:column  sortable="true" title="Bill&nbsp;To"  sortProperty="billToName"   headerClass="headerClassLeft" style="width:200px" >
       <c:if test="${accountLineList.actualRevenue!='0.00'}">
       <div style="text-align:left;" onmouseover="findPartnerDetail('${accountLineList.billToCode}',this);" >
          <c:if test="${accountLineList.billToName != '' && accountLineList.billToName != null}">
          <c:set var="string3" value="${fn1:substring(accountLineList.billToName, 0, 30)}" />
   		   <c:out value="${string3}"  />
   		   </c:if>
          </div>
       </c:if> 
       </display:column>
     
     <display:column property="description" sortable="true" title="Description"  maxLength="30"  style="width:150px; border-right:medium solid #3DAFCB;"/>
          
     <display:column property="category"  sortable="true" titleKey="accountLine.category"  maxLength="7"  style="width:60px"/>
     <configByCorp:fieldVisibility componentId="component.tab.accountLine.estimateStatus">
     <display:column title="Status"  style="" >
        
     <c:if test="${(accountLineList.estimateStatus=='Estimate Sent')}">
		<select class="list-menu" name="estimateList" value="'${accountLineList.estimateStatus}'"  id="${accountLineList.id}" disabled="disabled" onchange="collectingAllStatus();doNotChangeEstmateStatus(this);" style="width:130px"> 
							<option value="<c:out value='' />">
							<c:out value=""></c:out>
						    </option>
			<c:forEach var="chrms" items="${accEstmateStatus}" varStatus="loopStatus">
				<c:choose>
	                        <c:when test="${chrms.key == accountLineList.estimateStatus}">
	                        <c:set var="selectedInd" value=" selected"></c:set>
	                        </c:when>
	                        <c:otherwise>
	                        <c:set var="selectedInd" value=""></c:set>
	                        </c:otherwise>
                  </c:choose>
		      				<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
		                    <c:out value="${chrms.value}"></c:out>
		                    </option>
			</c:forEach> 
		</select>  
		 </c:if>
		 <c:if test="${accountLineList.estimateStatus!='Estimate Sent'}">
		 <select class="list-menu" name="estimateList" value="'${accountLineList.estimateStatus}'" disabled="disabled" id="${accountLineList.id}" onchange="collectingAllStatus();" style="width:130px"> 
							<option value="<c:out value='' />">
							<c:out value=""></c:out>
						    </option>
			<c:forEach var="chrms" items="${accEstmateStatus}" varStatus="loopStatus">
				<c:choose>
	                        <c:when test="${chrms.key == accountLineList.estimateStatus}">
	                        <c:set var="selectedInd" value=" selected"></c:set>
	                        </c:when>
	                        <c:otherwise>
	                        <c:set var="selectedInd" value=""></c:set>
	                        </c:otherwise>
                  </c:choose>
		      				<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
		                    <c:out value="${chrms.value}"></c:out>
		                    </option>
				</c:forEach> 
			</select>  
		 </c:if>
	 
	</display:column> 
	  <display:column title="Notes"  style="" >
	    <c:choose><c:when test="${accountLineList.countPayableDetailNotes == '0' || accountLineList.countPayableDetailNotes == '' || accountLineList.countPayableDetailNotes == null}">
		  <img id="countAgentNotesImage${accountLineList.id}" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&agentcorpID=${serviceOrder.corpID}&subType=EstimateDetail&imageId=countAgentNotesImage${accountLineList.id}&fieldId=countAgentNotes&accportalFlag=true&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${accountLineList.id }&notesId=${accountLineList.id}&agentcorpID=${serviceOrder.corpID}&noteFor=AccountLine&subType=EstimateDetail&imageId=countVipNotesImage&fieldId=countVipNotes&accportalFlag=true&decorator=popup&popup=true',800,580);" ></a>
		 
		 </c:when>
		 <c:otherwise>
		  <img id="countAgentNotesImage${accountLineList.id}" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&agentcorpID=${serviceOrder.corpID}&subType=EstimateDetail&imageId=countAgentNotesImage${accountLineList.id}&fieldId=countAgentNotes&accportalFlag=true&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${accountLineList.id }&notesId=${accountLineList.id}&agentcorpID=${serviceOrder.corpID}&noteFor=AccountLine&subType=EstimateDetail&imageId=countVipNotesImage&fieldId=countVipNotes&accportalFlag=true&decorator=popup&popup=true',800,580);" ></a>
		 </c:otherwise>
		 </c:choose>
	  </display:column>
    </configByCorp:fieldVisibility>
     <display:footer>
<tr>
		        
		          <td align="right" colspan="2"  style="border-right:medium solid #3dafcb; "><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
		         <!--<td align="right" width="70px" style="border-right:medium solid #3dafcb; "><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${serviceOrder.entitledTotalAmount}" /></div></td>-->
                  <c:if test="${multiCurrency!='Y' }"> 
                   <td align="right" width="70px"  ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true"  value="${serviceOrder.estimatedTotalRevenue}" /></div></td>
                  </c:if>
                  <c:if test="${multiCurrency=='Y' }">
                  <td align="center" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalEstCost }" />
                  </div></td>
                  </c:if>
                  <c:if test="${DisplayPortalRevision}">
                  <td style="border-right:medium solid #3dafcb; "> </td>
                   <c:if test="${multiCurrency!='Y' }"> 
                  <td align="right" width="70px" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.revisedTotalRevenue}" /></div></td>
		  	      </c:if>
		  	      <c:if test="${multiCurrency=='Y' }">
                  <td align="center" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalRevisionCost }" />
                  </div></td>
                  </c:if>
                  </c:if>
		 	     
        <td style="border-right:medium solid #3dafcb; "> </td>
        <c:if test="${serviceOrder.corpID != 'UTSI' }">
        <td></td>
        </c:if>
        
         		<c:if test="${multiCurrency!='Y' }">
                  <td align="center" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${serviceOrder.projectedActualRevenue}" /></div></td>
                  </c:if>
                  <c:if test="${multiCurrency=='Y' }">
                  <td align="center" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalactualRevenueForeign }" />
                  </div></td>
                  </c:if>
                  <td align="right" width="70px">
                   </td>
                   <td align="right" >
                  <td align="right" width="70px">
		  	      <td colspan=""></td>  
		  	      <td align="right" width="70px" style="border-right:medium solid #3dafcb; "></td>
		  	      <td colspan=""></td>
		  	       <configByCorp:fieldVisibility componentId="component.tab.accountLine.estimateStatus">
		  	      <td colspan=""></td>
		  	      </configByCorp:fieldVisibility>
		  	      <td></td>
			</tr>
</display:footer>
</display:table></div>
<configByCorp:fieldVisibility componentId="component.tab.accountLine.estimateStatus">
<s:submit cssClass="cssbutton1" cssStyle="width:65px; height:25px" key="button.save" onclick=""/> 
<input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="collectingAllStatusNew();" />
<sec-auth:authComponent componentId="module.field.accountLine.estimateStatus">  
<input class="cssbutton" style="width:75px; height:25px;" type="button" value="Approve All" onclick="approveAccEstmateStatus();" />
<input class="cssbutton" style="width:75px; height:25px;" type="button" value="Reject All" onclick="rejectAccEstmateStatus();" />
</sec-auth:authComponent>
 </configByCorp:fieldVisibility>
</s:form>
</div> 

<script language="javascript" >
function volumeDisplay(){
	var weightVolSectionId = document.getElementById('weightVolMain');
	if(weightVolSectionId != null && weightVolSectionId != undefined){
		document.getElementById('weightVolMain').style.display = 'none';
		document.getElementById('chargeableMain').style.display = 'block';
	}
}
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	window.onload = function() { 
	//trap();
	//document.forms['claimForm'].elements['addButton'].disabled=true;
}
</sec-auth:authComponent>
function trap(){
if(document.images){
	  for(i=0;i<document.images.length;i++){
	      if(document.images[i].src.indexOf('nav')>0){
				document.images[i].onclick= right; 
	        	document.images[i].src = 'images/navarrow.gif';  
		  }
	  }
 }
}
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['costingDetail'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['costingDetail'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['costingDetail'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['costingDetail'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'costingDetail.html?sid='+results;
             }
        }     
function findCustomerOtherSO(position) {
 var sid=document.forms['costingDetail'].elements['customerFile.id'].value;
 var soIdNum=document.forms['costingDetail'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
function goToUrl(id)
	{
		location.href = "costingDetail.html?sid="+id;
	}
	
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http5 = getHTTPObject();

   function findPartnerDetail(partnerCode,position){
  	  var url="findToolTipPartnerDetail.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
  	ajax_showTooltip(url,position);
    }
	
	function collectingAllStatus(){
		try{
			var status='';
			var estimateStatusList="";	
			<c:forEach items="${accountCostLineList}" var="list1">
			var aid="${list1.id}";
			var status=document.getElementById(aid).value;
			if(status==''){
				status="NOT";
				}
			if(estimateStatusList==''){   
				 estimateStatusList=aid+":"+status;
			 }else{
				 estimateStatusList=estimateStatusList + ',' + aid+":"+status;
			 }
			</c:forEach>
				document.forms['costingDetail'].elements['accLineStatusList'].value=estimateStatusList;
			}catch(e){}	
	}	

	function collectingAllStatusNew(){
			try{
					<c:forEach items="${accountCostLineList}" var="list1">
						var aid="${list1.id}";
						var status="${list1.estimateStatus}";
						document.getElementById(aid).value = status;
					</c:forEach>
			}catch(e){}	
	}
	
 function approveAccEstmateStatus(){
	 document.forms['costingDetail'].action = 'approveAccEstmateStatus.html?btntype=yes';
	 document.forms['costingDetail'].submit();
 }
 function rejectAccEstmateStatus(){
	 document.forms['costingDetail'].action = 'rejectAccEstmateStatus.html?btntype=yes';
	 document.forms['costingDetail'].submit();
 }
try{	
		var roleId=0; 
		<sec-auth:authComponent componentId="module.field.accountLine.estimateStatus"> 
		roleId=14;
		</sec-auth:authComponent>
		if(roleId==14){
			<c:forEach items="${accountCostLineList}" var="list1">
			var aid="${list1.id}";
			var status="${list1.estimateStatus}";
			if(status=='Estimate Sent')
			{
				document.getElementById(aid).disabled=false
			}
			
		</c:forEach>
		}
	 } catch(e){}

	 function doNotChangeEstmateStatus(target){
		 var status=target.value;
			if(status==''){
				alert('You cannot change Status to blank.');
				<c:forEach items="${accountCostLineList}" var="list1">
				var aid="${list1.id}";
				var status="${list1.estimateStatus}";
				document.getElementById(aid).value = status;
			</c:forEach>
			}			
	 }	 
</script>
<script type="text/JavaScript">
try{
	<sec-auth:authComponent componentId="module.tab.serviceorder.weightvol">  
	 volumeDisplay();
   </sec-auth:authComponent>
}catch(e){
}
</script>