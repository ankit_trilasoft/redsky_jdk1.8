package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;






import com.trilasoft.app.dao.ExtractedFileLogDao;
import com.trilasoft.app.model.ExtractedFileLog;
import com.trilasoft.app.service.ExtractedFileLogManager;



public class ExtractedFileLogManagerImpl  extends GenericManagerImpl<ExtractedFileLog, Long> implements ExtractedFileLogManager {
 
	ExtractedFileLogDao extractedFileLogDao; 
	    
	    
	    public ExtractedFileLogManagerImpl(ExtractedFileLogDao extractedFileLogDao) { 
	        super(extractedFileLogDao); 
	        this.extractedFileLogDao = extractedFileLogDao; 
	    }


		public List<ExtractedFileLog> searchExtractedFileLog(String sessionCorpID, String moduleName, String extractCreatedbyFirstName, String extractCreatedbyLastName, String fromExtractDate, String toExtractDate) {
			return extractedFileLogDao.searchExtractedFileLog(sessionCorpID, moduleName, extractCreatedbyFirstName,extractCreatedbyLastName, fromExtractDate, toExtractDate);
		}
		
		public List getFileLogListByModule(String sessionCorpID,String moduleName){
			return extractedFileLogDao.getFileLogListByModule(sessionCorpID, moduleName);
		}
}
