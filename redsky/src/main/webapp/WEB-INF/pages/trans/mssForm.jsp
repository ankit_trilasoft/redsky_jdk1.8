<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head> 
<title>MSS</title> 
<meta name="heading" content="MSS"/>
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/jquery.slimscroll.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('pholder', 'fade=1,hide=0,persist=0');
animatedcollapse.addDiv('crating', 'fade=1,hide=0,persist=0');
animatedcollapse.addDiv('orgserv', 'fade=0,persist=1,hide=1');
animatedcollapse.addDiv('desserv', 'fade=0,persist=1,hide=1');
animatedcollapse.addDiv('ppreview', 'fade=0,persist=1,hide=1');
animatedcollapse.init();
</script>
<style type="text/css">
.text-area {
border:1px solid #219DD1;
}
span.pagelinks {   
    margin-bottom: -21px; 
}
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/pdfobject.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
		var cal1=new CalendarPopup('mydiv');
		cal1.showNavigationDropdowns();
		var now = new Date();
		cal1.addDisabledDates(formatDate(now,"yyyy-MM-dd"),null);
	</script>
 <style type="text/css">    
            .pg-normal {
                color: 1B6DE0;
                font-weight: normal;
                text-decoration: none;    
                cursor: pointer;    
            }
            .pg-selected {
                color: 1B6DE0;
                font-weight: bold;        
                text-decoration: underline;
                cursor: pointer;
            }
  </style>
    <script type="text/javascript">
      window.onload = function (){
        //var myPDF = new PDFObject({ url: "file:///C:/usr/local/redskydoc/MssDoc/339264_SSCW_MSS.pdf" }).embed("pdf");
      };
    </script>     
</head>	
<s:form id="mssForm" name="mssForm" action="" method="post" validate="true">
<s:hidden name="mss.id" value="${mss.id}"/>
<s:hidden name="mss.corpId" />
<s:hidden name="wid" value="${workTicket.id}" />
<s:hidden name="delieveryStartDate"/> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden id="countPayNotes" name="countPayNotes" value="<%=request.getParameter("countPayNotes") %>"/>
<c:set var="countPayNotes" value="<%=request.getParameter("countPayNotes") %>" />
<s:hidden name="newlineid" id="newlineid" value=""/>
<s:hidden name="newlineIdOri" id="newlineIdOri" value=""/>
<s:hidden name="newlineIdDes" id="newlineIdDes" value=""/>
<s:hidden name="transportListServer" id="transportListServer" />
<s:hidden name="descriptionListServer" id="descriptionListServer" />
<s:hidden name="lengthListServer" id="lengthListServer" />
<s:hidden name="widthListServer" id="widthListServer" />
<s:hidden name="heightListServer" id="heightListServer" />
<s:hidden name="plyListServer" id="plyListServer" />
<s:hidden name="intlIspmisListServer" id="intlIspmisListServer" />
<s:hidden name="apprListServer" id="apprListServer" />
<s:hidden name="codListServer" id="codListServer" />
<s:hidden name="mssRowlist" id="mssRowlist" value=""/> 
<s:hidden name="notes.id" />
<s:hidden name="mss.purchaseOrderNumber" value="${mss.purchaseOrderNumber}"/>
<s:hidden name="mss.soNumber" value="${mss.soNumber}"/>
<s:hidden name="mss.vip" value="${mss.vip}"/>
<s:hidden name="mss.shipperFirstName" value="${mss.shipperFirstName}"/>
<s:hidden name="mss.shipperLastName" value="${mss.shipperLastName}"/>
<s:hidden name="mss.shipperEmailAddress" value="${mss.shipperEmailAddress}"/>
<s:hidden name="mss.shipperOriginAddress1" value="${mss.shipperOriginAddress1}"/>
<s:hidden name="mss.shipperOriginAddress2" value="${mss.shipperOriginAddress2}"/>
<s:hidden name="mss.shipperOriginCity" value="${mss.shipperOriginCity}"/>
<s:hidden name="mss.shipperOriginState" value="${mss.shipperOriginState}"/>
<s:hidden name="mss.shipperOriginZip" value="${mss.shipperOriginZip}"/>
<s:hidden name="mss.shipperDestinationAddress1" value="${mss.shipperDestinationAddress1}"/>
<s:hidden name="mss.shipperDestinationAddress2" value="${mss.shipperDestinationAddress2}"/>
<s:hidden name="mss.shipperDestinationCity" value="${mss.shipperDestinationCity}"/>
<s:hidden name="mss.shipperDestinationState" value="${mss.shipperDestinationState}"/>
<s:hidden name="mss.shipperDestinationZip" value="${mss.shipperDestinationZip}"/>
<s:hidden name="mss.mssOrderOriginPhoneNumbers" value="${mss.mssOrderOriginPhoneNumbers}"/>
<s:hidden name="mss.mssOrderDestinationPhoneNumbers" value="${mss.mssOrderDestinationPhoneNumbers}"/>
<s:hidden name="mss.workTicketNumber" value="${mss.workTicketNumber}"/>
<s:hidden name="mss.companyDivision" value="${mss.companyDivision}"/>
<s:hidden name="rowCountvalue" value="${rowCountvalue}"/>
<s:hidden name="bDivision" value="${mss.billingDivision}"/>
<s:hidden name="affTo" value="${mss.affiliatedTo}"/>
<s:hidden name="loadSDate" value="${mss.loadingStartDate}"/>
<s:hidden name="packSDate" value="${mss.packingStartDate}"/>
<s:hidden name="deliveryEndDate" value="${mss.deliveryEndDate}"/>
<s:hidden name="transRadio" value="${mss.transportTypeRadio}"/>
<s:hidden name="weight" value="${mss.weight}"/>
<s:hidden name="delSDate" value="${mss.deliveryStartDate}"/>
<s:hidden name="reqDesDate" value="${mss.requestedDestinationDate}"/>
<s:hidden name="reqestODate" value="${mss.requestedOriginDate}"/>
<s:hidden name="mss.location" value="${mss.location}"/>

<s:hidden name="oriItemListServer" id="oriItemListServer" />
<s:hidden name="oriCodListServer" id="oriCodListServer" />
<s:hidden name="oriApprListServer" id="oriApprListServer" />
<s:hidden name="oriServicelist" id="oriServicelist" value=""/>
<s:hidden name="destItemListServer" id="destItemListServer" />
<s:hidden name="destApprListServer" id="destApprListServer" />
<s:hidden name="destCodListServer" id="destCodListServer" />
<s:hidden name="desServicelist" id="desServicelist" value=""/>  
 
<div id="Layer1" style="width:95%"> 
   <div id="newmnav"> 
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			 <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                 <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			  
			  		   <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>	
			  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div>	
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
</div>
   
 <div id="Layer6" style="width:100%">
<div id="newmnav">
		    <ul>
			  <li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
			  <li><a href="itemsJbkEquips.html?id=${workTicket.id}&itemType=M&sid=${serviceOrder.id}"><span>Material</span></a></li>
			  <!--<li><a href="itemsJbkResourceList.html?id=${workTicket.id}&sid=${sid}"><span>Resource</span></a></li>
			  -->
			  <li><a href="itemsJbkResourceList.html?id=${workTicket.id}&itemType=M&sid=${serviceOrder.id}"><span>Resource</span></a></li>
			  <li><a href="workTicketCrews.html?id=${workTicket.id}&itemType=M&sid=${serviceOrder.id}"><span>Crew</span></a></li>			  
		      <li><a href="truckingOperationsList.html?ticket=${workTicket.ticket}&sid=${serviceOrder.id}&tid=${workTicket.id}"><span>Truck</span></a></li>
		      <configByCorp:fieldVisibility componentId="component.tab.workTicket.whseMgmtTab">
		      <li><a href="bookStorages.html?id=${workTicket.id}"><span>Whse/Mgmt</span></a></li>
		      <li id="newmnav1"><a herf=url="/editMss.html?id=${workTicket.id}" class="current"><span>MSS</span></a></li>
		      </configByCorp:fieldVisibility>
			</ul>
		</div>
		<div class="spn">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div> 
   <div class="center-content"> 
 <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;">
  <tbody>
  <!-- Start Placeholder -->
   <tr>
   <td height="10" width="100%" align="left" >
   <div  onClick="javascript:animatedcollapse.toggle('pholder')"  style="margin: 0px;">    
	 <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">&nbsp;Place Order
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
   </div>
   <div id="pholder">
   <table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
	  <tbody>
		  <tr>
		  	<td align="left" class="listwhitetext">
			  	<table class="detailTabLabel" border="0" cellpadding="2" style="width:80%">
					  <tbody>  	
					  	<tr>
					  		<td align="left" height="3px"></td>
					  	</tr>
					   					  	
					  	<tr>
					  	<td align="right" class="listwhitetext">Submit&nbsp;As<font color="red" size="2">*</font></td>
					  	<td align="left"><s:select name="mss.submitAs" list="%{submitAs}" cssClass="list-menu" cssStyle="width:125px" headerKey="" headerValue="" /></td>
					  	<td align="right" class="listwhitetext">Bill&nbsp;To&nbsp;Division<font color="red" size="2">*</font></td>
					  	<td align="left"><s:select name="mss.billingDivision" list="%{billToDivision}"  cssClass="list-menu" cssStyle="width:180px" headerKey="" headerValue="" /></td>
					  	<td align="right" class="listwhitetext">Affiliated&nbsp;To<font color="red" size="2">*</font></td>
					  	<td width="180"><s:select name="mss.affiliatedTo" list="%{affiliatedTo}" cssClass="list-menu" cssStyle="width:125px" headerKey="" headerValue="" /></td>
					  	<td align="left"  colspan="2" class="listwhitetext" style="padding-left:10px;!padding-left:5px;">Urgent
						<s:checkbox cssStyle="vertical-align:middle;" name="mss.urgent" value="${mss.urgent}" />
						</td>
					  	</tr>
					  	
					  	<tr>
					  	<td colspan="4">
    					<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;"> 
							<td align="right" class="listwhitetext" width="">Service Type:</td>
							<td colspan="" class="listwhitetext" width=""><s:radio id="" name="mss.transportTypeRadio" list="%{transportTypeRadio}" onclick="check();checkTransValueOrigin(this);checkTransValueDestination(this);" /></td>
						</table></td>
						</tr>
					  	
					  	<tr>					  	 
					  	<td colspan="8">
					  	<table class="detailTabLabel" border="0" cellpadding="2" style="margin:0px;padding:0px;">					  	
					  	<tr>
					  	<td align="right" class="listwhitetext">Requested&nbsp;Origin&nbsp;Date</td>
								<c:if test="${not empty mss.requestedOriginDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="mss.requestedOriginDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="requestedOriginDate" name="mss.requestedOriginDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="requestedOriginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if> 
									 	<c:if test="${empty mss.requestedOriginDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="requestedOriginDate" name="mss.requestedOriginDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="requestedOriginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>
						<td align="right" class="listwhitetext"  width="150">Requested&nbsp;Destination&nbsp;Date</td>
									<c:if test="${not empty mss.requestedDestinationDate}">
										<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}">
											<s:param name="value" value="mss.requestedDestinationDate" />
										</s:text>
										<td align="left"><s:textfield cssClass="input-text" id="requestedDestinationDate" name="mss.requestedDestinationDate" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
										<td><img id="requestedDestinationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
									</c:if>
									<c:if test="${empty mss.requestedDestinationDate}">
										<td align="left"><s:textfield cssClass="input-text"
											id="requestedDestinationDate" name="mss.requestedDestinationDate" required="true"
											cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td><td><img id="requestedDestinationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
										</td>
									</c:if>	
						<td class="listwhitetext" align="right"  width="60">Weight<font color="red" size="2">*</font></td> 
						<td width="80"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="mss.weight" onblur="return isInteger(this)" size="10" maxlength="10" /></td>
					  	  
					  	<c:if test="${empty mss.id}">
							<td  align="right" style="width:150px;"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
						</c:if>
						<c:if test="${not empty mss.id}">
						<c:choose>
							<c:when test="${countPayNotes == '0' || countPayNotes == '' || countPayNotes == null}">
							<td  align="right" style="width:150px;"><img id="countPayNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${mss.id }&notesKeyId=${mss.id }&noteFor=MSS&subType=MSS&imageId=countPayNotesImage&fieldId=countPayNotesImage&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${mss.id}&notesId=${mss.id}&noteFor=Mss&subType=Mss&imageId=countPayNotesImage&fieldId=countPayNotesImage&decorator=popup&popup=true',800,600);" ></a></td>
						</c:when>
						<c:otherwise>
							<td  align="right" style="width:150px;"><img id="countPayNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${mss.id }&notesKeyId=${mss.id }&noteFor=MSS&subType=MSS&imageId=countPayNotesImage&fieldId=countPayNotesImage&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${mss.id}&notesId=${mss.id}&noteFor=Mss&subType=Mss&imageId=countPayNotesImage&fieldId=countPayNotesImage&decorator=popup&popup=true',800,600);" ></a></td>
						</c:otherwise>
						</c:choose> 
						</c:if>
					  	
					  	</tr>
					  	</table>
					  	</td>
					  	</tr>				  	
					  	
					  	<tr>
					  	<c:set var="serviceOverMultipleDays" value="false"/>
						<c:if test="${mss.serviceOverMultipleDays}">
							<c:set var="serviceOverMultipleDays" value="true"/>
						</c:if>
					  	<td align="right" class="listwhitetext"><s:checkbox cssStyle="vertical-align:middle;margin:0px;" name="mss.serviceOverMultipleDays" onclick="check();" fieldValue="true" value="${serviceOverMultipleDays}" /></td>
					  	<td align="left"  colspan="7" class="listwhitetext" >Services to be performed over multiple days</td>
						</tr>
						
						<tr>						
						<td colspan="5" style="padding-left:16px;">
						<table style="margin:0px;padding:0px;">
						<tr>
						<td></td>
						<td align="center" class="listwhitetext"><b>StartDate</b></td>
						<td width="40"></td>
						<td align="center" class="listwhitetext"><b>EndDate</b></td>
						</tr>
						<tr>
						<td align="right" class="listwhitetext">Packing</td>
						<c:if test="${not empty mss.packingStartDate}">
						<s:text id="trackingStatusDeliveryShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="mss.packingStartDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="packingStartDate" name="mss.packingStartDate" value="%{trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="packingStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="packingStartDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty mss.packingStartDate}">
						<td><s:textfield cssClass="input-text" id="packingStartDate" name="mss.packingStartDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="packingStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="packingStartDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${not empty mss.packingEndDate}">
						<s:text id="trackingStatusDeliveryShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="mss.packingEndDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="packingEndDate" name="mss.packingEndDate" value="%{trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="packingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="packingEndDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty mss.packingEndDate}">
						<td><s:textfield cssClass="input-text" id="packingEndDate" name="mss.packingEndDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="packingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="packingEndDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</tr>
						
						<tr>
						<td align="right" class="listwhitetext">Loading</td>
						<c:if test="${not empty mss.loadingStartDate}">
						<s:text id="trackingStatusDeliveryShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="mss.loadingStartDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="loadingStartDate" name="mss.loadingStartDate" value="%{trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="loadingStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="loadingStartDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty mss.loadingStartDate}">
						<td><s:textfield cssClass="input-text" id="loadingStartDate" name="mss.loadingStartDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="loadingStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="loadingStartDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${not empty mss.loadingEndDate}">
						<s:text id="trackingStatusDeliveryShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="mss.loadingEndDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="loadingEndDate" name="mss.loadingEndDate" value="%{trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="loadingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="loadingEndDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty mss.loadingEndDate}">
						<td><s:textfield cssClass="input-text" id="loadingEndDate" name="mss.loadingEndDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="loadingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="loadingEndDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</tr>
				
						<tr>
						<td align="right" class="listwhitetext">Delivery</td>
						<c:if test="${not empty mss.deliveryStartDate}">
						<s:text id="trackingStatusDeliveryShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="mss.deliveryStartDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryStartDate" name="mss.deliveryStartDate" value="%{trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="deliveryStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="deliveryStartDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty mss.deliveryStartDate}">
						<td><s:textfield cssClass="input-text" id="deliveryStartDate" name="mss.deliveryStartDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="deliveryStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="deliveryStartDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${not empty mss.deliveryEndDate}">
						<s:text id="trackingStatusDeliveryShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="mss.deliveryEndDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryEndDate" name="mss.deliveryEndDate" value="%{trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="deliveryEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="deliveryEndDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty mss.deliveryEndDate}">
						<td><s:textfield cssClass="input-text" id="deliveryEndDate" name="mss.deliveryEndDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="deliveryEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="deliveryEndDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</tr>
						
						</table>
						</td>
						<td colspan="5" rowspan="5" valign="top">
						<table style="margin:0px;padding:0px;">
						<tr>
						<td>
						<display:table name="notess" class="table" requestURI="" id="notesList"   pagesize="10" style="min-width:375px;">
						    <display:column property="noteSubType" title="O/D/B" />
						    <display:column property="createdOn" title="Date/Time" format="{0,date,dd-MMM-yy}" />
						    <display:column title="Description" >
						    <c:if test="${notesList.note != ''}" >
							<c:if test="${fn:length(notesList.note) > 20}" >
							<c:set var="abc" value="${fn:substring(notesList.note, 0, 25)}" />
							<div align="left" >
							<a href="javascript:openWindow('editNewNoteForMss.html?from=list&id=${notesList.id}&id1=${mss.id}&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true',775,650)">
							<c:out value="${abc}" />&nbsp;.....</div>
							</c:if>
							<c:if test="${fn:length(notesList.note) <= 20}" >
							<c:set var="abc" value="${fn:substring(notesList.note, 0, 25)}" />
						    <div align="left" >
						    <a href="javascript:openWindow('editNewNoteForMss.html?from=list&id=${notesList.id}&id1=${mss.id}&imageId=${imageId}&fieldId=${fieldId}&noteFor=${noteFor}&decorator=popup&popup=true',775,650)">
						    <c:out value="${abc}" /></div>
							</c:if>
							</c:if>
							</display:column>
						</display:table>
						</td>
						</tr>
						</table>
						</td>
						</tr>
						 
						<tr>
						<td align="left" style="width:50px;"></td>						
						<td colspan="8">
						<table border="0" cellpadding="2" style="margin:0px;padding:0px;">
						<tr>
						<c:if test="${not empty mss.id }">
						<c:if test="${(mss.location=='' || mss.location==null) || (mss.mssOrderNumber=='' || mss.mssOrderNumber==null)}">						
						<td align="left"><input type="button" style="width:100px;" class="cssbutton1" value="Import Crates" onclick="return findImportFromVoxme();"/></td>
						</c:if>
						</c:if>
						<c:if test="${not empty mss.id }">
						<c:if test="${mss.location!=''  && mss.mssOrderNumber!='' }">						
						<td align="left"><input type="button" style="width:100px;" class="cssbutton1" value="Import Crates" onclick="return findImportFromVoxme();" disabled="disabled" /></td>
						</c:if>
						</c:if>
						<c:if test="${empty mss.id}">
						<td align="left"><input type="button" style="width:100px;" class="cssbutton1" value="Import Crates" onclick="return findImportFromVoxme();" disabled="disabled" /></td>
						</c:if>
						<c:if test="${not empty mss.id }">
						<c:if test="${(mss.location=='' || mss.location==null) || (mss.mssOrderNumber=='' || mss.mssOrderNumber==null)}">						
						<td align="left"><input type="button" style="width:100px;" class="cssbutton1" value="Pricing Preview" onclick="return findQuotePDFValuesAjax();"/></td>
						</c:if>
						</c:if>
						<c:if test="${not empty mss.id }">
						<c:if test="${mss.location!=''  && mss.mssOrderNumber!='' }">						
						<td align="left"><input type="button" style="width:100px;" class="cssbutton1" value="Pricing Preview" onclick="return findQuotePDFValuesAjax();" disabled="disabled"/></td>
						</c:if>
						</c:if>
						<c:if test="${empty mss.id}">
						<td align="left"><input type="button" style="width:100px;" class="cssbutton1" value="Pricing Preview" onclick="return findQuotePDFValuesAjax();" disabled="disabled"/></td>
						</c:if>
						<c:if test="${not empty mss.id}">
						<c:if test="${mss.mssOrderNumber=='' || mss.mssOrderNumber==null }">
						<td align="left"><input type="button" style="width:75px;" class="cssbutton1" value="Submit" onclick="return placeOrderNumber();"/></td>
						</c:if>
						</c:if>
						<c:if test="${not empty mss.id}">
						<c:if test="${mss.mssOrderNumber!=''}">
						<td align="left"><input type="button" style="width:75px;" class="cssbutton1" value="Submit" onclick="" disabled="disabled"/></td>
						</c:if>
						</c:if>
						<c:if test="${empty mss.id}">
						<td align="left"><input type="button" style="width:75px;" class="cssbutton1" value="Submit" onclick="" disabled="disabled"/></td>
						</c:if>
						<td class="listwhitetext" align="right">MSS Order</td> 
						<td width="200"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="mss.mssOrderNumber"  size="20" maxlength="20" readonly="true" onchange=""  /></td>					 
						</tr>
					  	</table>
					  	</td>
					  	</tr>
					  	</tbody>
				</table>
			 </td>
		    </tr>		      		  	
		 </tbody>
	</table>
   </div>
  </td>
  </tr>
    <!-- End Placeholder -->
    
   <!-- Start Crating --> 
  <tr>
   <td height="10" width="100%" align="left" class="listwhitetext" >
   <div  onClick="javascript:animatedcollapse.toggle('crating')"  style="margin: 0px;">    
	 <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">&nbsp;Crating
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
   </div>
   <div id="crating">
   <table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%;margin:0px;">
	  <tbody>	 	
		  <tr>
		  <td>		  	
		  	<div id="otabs" style="margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Crating List</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
		<div id="ABC11" style="height:150px;">	
		<table style="margin-bottom:0px;">
		    	<tr>
		    	<!--<div id="pageNavPosition" width="85%" style="text-align: right; width: 99%; display: block; margin-top:-3px;"></div>-->
		    	</tr>
		    	</table>		
					 <table class="table" id="dataTable" style="width:99%;margin-bottom:0px;">
					 <thead>
					 <tr>
					 <th >O/D/B</th>
					 <th >Description</th>
					 <th >Length&nbsp;(Inches)</th>
					 <th >Width&nbsp;(Inches)</th>
					 <th >Height&nbsp;(Inches)</th>
					 <th style="text-align:center;">Ply</th>
					 <th style="text-align:center;">INTL-ISPMIS</th>
					 <th style="text-align:center;">APPR</th>
					 <th style="text-align:center;">COD</th>
					 <th >Copy</th>
					 <th >Remove</th>
					 </tr>
					 </thead>
					 <tbody>
					 <c:forEach var="individualItem" items="${mssItemList}" varStatus="rowCounter">
					 	
					<tr>
			  		<td><s:select cssClass="list-menu" list="%{transportTypeValue}" name="transportList" id="tra${rowCounter.count}" value="'${individualItem.transportType}'" onchange="updateMssCratingGridAjax('${rowCounter.count}','${individualItem.id}')"  headerKey="" headerValue="" cssStyle="width:50px;"/></td>
				    <td><s:textfield name="descriptionList" value="${individualItem.description}" id="des${rowCounter.count}" size="20" maxlength="94" onchange="updateMssCratingGridAjax('${rowCounter.count}','${individualItem.id}')" cssClass="input-text" cssStyle="width:200px;"/></td>
					<td><s:textfield name="lengthList" value="${individualItem.length}" id="len${rowCounter.count}" onchange="updateMssCratingGridAjax('${rowCounter.count}','${individualItem.id}')" onblur="return ValidateDecimal(this);" cssClass="input-text" maxlength="10" size="10" cssStyle="width:70px;"/></td>
					<td><s:textfield name="widthList" value="${individualItem.width}" id="wid${rowCounter.count}" onchange="updateMssCratingGridAjax('${rowCounter.count}','${individualItem.id}')" onblur="return ValidateDecimal(this);" cssClass="input-text" maxlength="10" size="10" cssStyle="width:70px;"/></td>
					<td><s:textfield name="heightList" value="${individualItem.height}" id="hei${rowCounter.count}" onchange="updateMssCratingGridAjax('${rowCounter.count}','${individualItem.id}')" onblur="return ValidateDecimal(this);" cssClass="input-text" maxlength="10" size="10" cssStyle="width:70px;"/></td>
					<td><s:checkbox name="plyList" value="${individualItem.ply}" id="ply${rowCounter.count}" onchange="updateMssCratingGridAjax('${rowCounter.count}','${individualItem.id}');" cssStyle="width:30px;"/></td>
					<td><s:checkbox name="intlIspmisList" value="${individualItem.intlIspmis}" id="int${rowCounter.count}" onchange="updateMssCratingGridAjax('${rowCounter.count}','${individualItem.id}');"  cssStyle="width:30px;"/></td>
					<td><s:checkbox name="apprList" value="${individualItem.appr}" id="app${rowCounter.count}" onchange="onlyCheckAppOrCod('app${rowCounter.count}','${rowCounter.count}');updateMssCratingGridAjax('${rowCounter.count}','${individualItem.id}');" onclick=""  cssStyle="width:30px;"/></td>
					<td><s:checkbox name="codList" value="${individualItem.cod}" id="cod${rowCounter.count}" onchange="onlyCheckAppOrCod('cod${rowCounter.count}','${rowCounter.count}');updateMssCratingGridAjax('${rowCounter.count}','${individualItem.id}');" onclick=""  cssStyle="width:30px;"/></td>
					
					<td class="listwhitetext"><a onclick="copyRow('dataTable','${rowCounter.count}');" </a>Copy</td>
									    
					<td><a><img align="middle" onclick="confirmSubmit('${individualItem.id}','${workTicket.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a></td>
					
				    </tr>
					  </c:forEach>
					 </tbody>		 		  
					 </table>
					 </div>
					 </td>
		    		</tr><tr><td>
		    		 <table style="margin-bottom:0px;">
		    	<tr>
		    	<td ><input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:70px; height:25px;" value="Add Line" onclick="addRow('dataTable');" /></td>
		    	
		    	</tr>
		    	</table>
		    	</td>
		    	</tr>
		      	<tr>
				<td align="left" height="13px"></td>
			</tr>		  	
		 </tbody>
	</table>
   </div>
  </td>
  </tr>
   <!-- End Crating  -->
   <!-- Origin Services Starts -->
   <tr>
   <td height="10" width="100%" align="left" >
   
   <div  onClick="javascript:animatedcollapse.toggle('orgserv');alertMsgOrigin();" style="margin: 0px;">    
	 <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">&nbsp;Origin&nbsp;Services
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
   </div>   
   <div id="orgserv">
   <table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%" id="OriServ" >
	  <tbody>	 
		  <tr>		  
		  	<td align="left" class="listwhitetext" valign="top" style="width:35%">
		  				<div style="height:23px;"></div>
		  				<div id="originCatsc">
					  	<display:table name="originCat" class="table" requestURI=""  id="originCat" style="width:98%;margin:0px;">
					  		<display:column title="Origin Service Categories" style="min-width:50%;text-align:center;padding:5px;"><B>${originCat}</B>								
							<img id="mdown_${originCat_rowNum}" onclick="displayResultOrigin('${originCat}','C',this,this.id);" align="right" src="${pageContext.request.contextPath}/images/mdown-arrow.png" style="margin:0px;padding:0px;"/>
					  		<img id="mup_${originCat_rowNum}" onclick ="closeDisplayResultOrigin(this)" align="right" src="${pageContext.request.contextPath}/images/mup-arrow.png" ALIGN=TOP style="display: none;" />
					  		</display:column>					  	
					  	</display:table>
					  	</div>
			 </td>			 
			 <td align="left" width="3%"></td>
			 <td align="left" class="listwhitetext" valign="top">
			
			 <div id="otabs" style="margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Origin Service</span></a></li>
		  </ul>
		</div>
			 <div id="spnblk" style="clear:both;"> </div>
			 <!--<div id="pageNavPosition1" style="text-align: right; width:60%; display: block; margin-top: -15px;"></div>	-->
			 	  		 <div id="Origin11" style="height:220px;width:60%;">		
					<table class="table" id="dataTableO" style="margin:0px;width:98%;">
					 <thead>
					 <tr>
					 <th width="50px">Items</th>					 
					 <th style="text-align:center;width:30px">APPR</th>
					 <th style="text-align:center;width:30px">COD</th>					 
					 <th width="50px">Remove</th>
					 </tr>
					 </thead>
					 <tbody>
					 <c:forEach var="individualItem" items="${mssOriServList}" varStatus="rowCounter">					 	
					<tr>
				    <td><s:textfield cssClass="input-text" name="oriItemList" value="${individualItem.oriItems}" id="ori${rowCounter.count}" size="20" readonly="true" onkeydown="return onlyDel(event,this)" cssStyle="width:200px;" onchange=""/></td>
					<td><s:checkbox name="oriApprList" value="${individualItem.oriAppr}" id="orp${rowCounter.count}" onchange="" cssStyle="width:30px;" onclick="checkOriAppOrCod('${rowCounter.count}',this,'orp');"/></td>
					<td><s:checkbox name="oriCodList" value="${individualItem.oriCod}" id="orc${rowCounter.count}" onchange="" cssStyle="width:30px;" onclick="checkOriAppOrCod('${rowCounter.count}',this,'orc');"/></td>
														    
					<td><a><img align="middle" onclick="deletrOriSerRow('${individualItem.id}','${workTicket.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a></td>
					
				    </tr>
					  </c:forEach>					  
					 </tbody>					 	 		  
					 </table>
					 </div>
					 <table style="margin-bottom:0px;">
					 <tr>
		    		<td><input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:70px; height:25px;" value="Add Line" onclick="addRowOrigin('dataTableO');" /></td>
		    		
		    		</tr>	
					 </table>					 
			 </td>
		    </tr>		    
		      	<tr>
				<td align="left" height="13px"></td>
			</tr>		  	
		 </tbody>
	</table>
   </div>
  </td>
  </tr>
  <!-- Origin Services Ends -->
   <!-- Destination Services Starts -->
  <tr>
   <td height="10" width="100%" align="left" >
   <div  onClick="javascript:animatedcollapse.toggle('desserv');alertMsgDestination();"  style="margin: 0px;">    
	 <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">&nbsp;Destination&nbsp;Services
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
   </div>
   <div id="desserv">
   
   <table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%" id="DestServ" >
	  <tbody>
		  <tr>		  
			<td align="left" class="listwhitetext" valign="top" style="width:35%">
						<div style="height:23px;"></div>	
						<div id="destinationCatsc">	  	
					  	<display:table name="destinationCat" class="table" requestURI=""  id="destinationCat" style="width:98%;margin:0px;">
					  		<display:column title="Destination Service Categories" style="min-width:50%;text-align:center;padding:5px;"><B>${destinationCat}</B> 
					  		<img id="mdown1_${destinationCat_rowNum}" onclick="displayResultDestination('${destinationCat}','C',this,this.id);" align="right" src="${pageContext.request.contextPath}/images/mdown1-arrow.png" style="margin:0px;padding:0px;"/>
					  		<img id="mup1_${destinationCat_rowNum}" onclick ="closeDisplayResultDestination(this)" align="right" src="${pageContext.request.contextPath}/images/mup1-arrow.png" ALIGN=TOP style="display: none;" />
					  		</display:column>
					  	</display:table>
					  	</div>					  				
			 </td>
			 
			 <td align="left" width="3%"></td>	
			 <td align="left" class="listwhitetext" valign="top">
			 
			 <div id="otabs" style="margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Destination Service</span></a></li>
		  </ul>
		</div>
			 <div id="spnblk" style="clear:both;"> </div>	
			 <div id="destinationCat111" style="height:248px;width:60%;">
			 <!--<div id="pageNavPosition2" style="text-align: right; width:60%; display: block; margin-top: -15px;"></div>	-->	
			  	<table class="table" id="dataTableD" style="width:98%;margin:0px;">
					 <thead>
					 <tr>
					 <th width="50px">Items</th>					 
					 <th style="text-align:center;width:30px">APPR</th>
					 <th style="text-align:center;width:30px">COD</th>					 
					 <th width="50px">Remove</th>
					 </tr>
					 </thead>
					 <tbody>
					 <c:forEach var="individualItem" items="${mssDestinationServList}" varStatus="rowCounter">					 	
					<tr>
				    <td><s:textfield cssClass="input-text" name="destItemList" value="${individualItem.destinationItems}" id="dei${rowCounter.count}" size="20"  cssStyle="width:200px;" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
					<td><s:checkbox name="destApprList" value="${individualItem.destinationAppr}" id="dep${rowCounter.count}" onchange="" cssStyle="width:30px;" onclick="checkDesAppOrCod('${rowCounter.count}',this,'dep');"/></td>
					<td><s:checkbox name="destCodList" value="${individualItem.destinationCod}" id="dec${rowCounter.count}" onchange="" cssStyle="width:30px;" onclick="checkDesAppOrCod('${rowCounter.count}',this,'dec');"/></td>
														    
					<td><a><img align="middle" onclick="deletrDestSerRow('${individualItem.id}','${workTicket.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a></td>
					
				    </tr>
					  </c:forEach>					  
					 </tbody>					 	 		  
					 </table>
					 </div>
					 <table style="margin-bottom:0px;">
					 <tr>
		    		<td><input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:70px; height:25px;" value="Add Line" onclick="addRowDestination('dataTableD');" /></td>
		    		</tr>	
					 </table>
						</td>
		   			 </tr>
		      	<tr>
				<td align="left" height="13px"></td>
			</tr>		  	
		 </tbody>
	</table>   
   
   </div>
  </td>
  </tr>
   <!-- Destination Services Ends -->
    <!-- Pricing Preview Starts -->
   <tr>
   <td height="10" width="100%" align="left" >
   <div  onClick="javascript:animatedcollapse.toggle('ppreview');"  style="margin: 0px;">    
	 <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">&nbsp;Price&nbsp;Preview
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
   </div>
   <div id="ppreview">
   
   <table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%" id="" >
	  <tbody>
		  <tr>		  
		<!-- 	/images/339264_SSCW_MSS.pdf -->
			 <td>
			 <c:if test="${mss.location!='' && mss.location !=null}">
			<iframe id="fred" style="border:1px solid #666CCC" title="PDF in an i-Frame" src="MssPdf?location=${mss.location}" frameborder="1" scrolling="auto" height="1100" width="1100" >
			</iframe>
			</c:if>
				</td>
			 
		   		</tr>
		      			  	
		 </tbody>
	</table>   
   
   </div>
  </td>
  </tr>  
  <!-- Pricing Preview Ends --> 
   
   
 </tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:45px;"><span></span></div>
</div>
</div>


	
<table border="0" width="750px;">
	<tbody>
		<tr>
			<td align="left" class="listwhitetext" width="30px"></td>
			<td colspan="5"></td>
		</tr>
		<tr>
			<td align="left" class="listwhitetext" width="30px"></td>
			<td colspan="5"></td>
		</tr>
		<tr>
			<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
		<td valign="top">
		
		</td>
		<td style="width:120px">
		<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${mss.createdOn}" 
		pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="mss.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
		<fmt:formatDate value="${mss.createdOn}" pattern="${displayDateTimeFormat}"/>
		</td>		
		<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
		<c:if test="${not empty mss.id}">
			<s:hidden name="mss.createdBy"/>
			<td style="width:85px"><s:label name="createdBy" value="%{mss.createdBy}"/></td>
		</c:if>
		<c:if test="${empty mss.id}">
			<s:hidden name="mss.createdBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		
		<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
		<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${mss.updatedOn}" 
		pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="mss.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
		<td style="width:120px"><fmt:formatDate value="${mss.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
		<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
		<c:if test="${not empty mss.id}">
			<s:hidden name="mss.updatedBy"/>
			<td style="width:85px"><s:label name="updatedBy" value="%{mss.updatedBy}"/></td>
		</c:if>
		<c:if test="${empty mss.id}">
			<s:hidden name="mss.updatedBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		</tr>
	</tbody>
</table>
	<table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
		       	<td align="left"><input type="button" class="cssbutton1"  style="width:55px; height:25px;margin-left:10px;" value="Save" onclick="saveMssRowList();"/></td>
		       	<td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset"/></td>
			</tr>		  	
		</tbody>
	</table>

</div>
</s:form>
<%@ include file="/common/mssFormJavaScript.js"%>
<div id="mydiv" style="position:absolute;top:110px;margin-top:-15px;"></div> 
<script type="text/javascript">
try{
	check();	
}catch(e){
	
}
</script>
