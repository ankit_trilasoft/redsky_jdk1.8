<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="companyPermissionList.title"/></title> 
<meta name="heading" content="<fmt:message key='companyPermissionList.heading'/>"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-20px;
!margin-top:-22px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>
<c:set var="buttons">  

     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/companyPermissionForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>

	<s:form cssClass="form_magn" id="companyPermissionList" name="companyPermissionList" action="searchCompanyPermissionList" method="post" validate="true">   
	<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
   <div id="content" align="center">
   <div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">			
	<table class="table" style="width:100%;">
	<thead>
	<tr>
	<th><fmt:message key="companyPermission.componentId"/></th>
	<th><fmt:message key="companyPermission.description"/></th>
	</tr></thead>
	<tbody>
		<tr>
		    <td width="20" align="left"><s:textfield name="corpComponentPermission.componentId" required="true" cssClass="input-text" size="80"/></td>
			<td width="20" align="left"><s:textfield name="corpComponentPermission.description" required="true" cssClass="input-text" size="80"/></td>
			 </tr>
			 <tr>
			 <td colspan="1"></td>
			 <td style="border-left: hidden;">
       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="button.search"/>  
       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
       
   			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Tab Permission List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	
	<display:table name="corpComponentPermissionList" class="table" requestURI="" id="CompanyPermisionList" export="true" defaultsort="1" pagesize="10" style="width:100%;margin-top:1px;" >   
	<display:column property="componentId"  href="companyPermissionForm.html" paramId="id" paramProperty="id" sortable="true" titleKey="companyPermission.componentId"/>
   	<display:column property="description"  sortable="true" titleKey="companyPermission.description"/>
   	<display:column property="corpID"  sortable="true" title="CorpID"/>
   	<c:if test="${CompanyPermisionList.mask=='2' }">
	<display:column  title="Display"><img src="${pageContext.request.contextPath}/images/tick01.gif"  /></display:column> 
	</c:if>
   	<c:if test="${CompanyPermisionList.mask=='0' }">
	<display:column  title="Display"><img src="${pageContext.request.contextPath}/images/cancel001.gif"  /></display:column> 
	</c:if>
 	<c:if test="${empty CompanyPermisionList.mask }">
	 	<display:column  title="Display"><img src="${pageContext.request.contextPath}/images/cancel001.gif"  /></display:column> 
	</c:if>
 	 <%--<display:column title="Remove" style="width:50px">
			<a><img title="Remove" align="middle" onclick="confirmSubmit(${CompanyPermisionList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>--%>
  </display:table>
	<c:out value="${buttons}" escapeXml="false" />   
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
</s:form>

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=1;i++)
			{
					document.forms['companyPermissionList'].elements[i].value = "";
			}
}

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Tab Permission");
	var did = targetElement;
		if (agree){
			location.href="deleteTabPermission.html?id="+did;
		}else{
			return false;
		}
}
</script>