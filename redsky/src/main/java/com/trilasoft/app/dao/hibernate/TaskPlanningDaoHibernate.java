package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;


import com.trilasoft.app.dao.TaskPlanningDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;

import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TaskPlanning;

public class TaskPlanningDaoHibernate extends GenericDaoHibernate<TaskPlanning, Long> implements TaskPlanningDao{
	private HibernateUtil hibernateUtil;
	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public TaskPlanningDaoHibernate() {
		super(TaskPlanning.class);
	}
	public List<TaskPlanning> findTaskPlanningList(String corpId,Long id){
		try{
		String query = "from TaskPlanning where corpID='"+corpId+"' and customerFileId ="+id+" ";
		return getHibernateTemplate().find(query);
		}catch(Exception e){}
			return null;
	}
	
}
