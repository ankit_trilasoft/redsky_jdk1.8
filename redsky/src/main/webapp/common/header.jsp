<%@ include file="/common/taglibs.jsp"%>
<%@ page import="org.appfuse.model.User" %>
<%@ page import="org.acegisecurity.Authentication" %>
<%@ page import="org.acegisecurity.context.SecurityContextHolder" %>
<%@ page import="com.trilasoft.app.model.Company" %>
<%@ page import="com.trilasoft.app.service.CompanyManager" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>


<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User quickUser = (User) auth.getPrincipal();
String quickCorpId = quickUser.getCorpID();
String quickUserType = quickUser.getUserType();
String defaultOperationCalendarVal = quickUser.getDefaultOperationCalendar();
if(defaultOperationCalendarVal==null || defaultOperationCalendarVal.equalsIgnoreCase("")){
	defaultOperationCalendarVal="Ops Calendar";
}
Boolean newsUpdateFlag=quickUser.isNewsUpdateFlag();
String quickFileCabinetView = quickUser.getFileCabinetView();
%>
<%
WebApplicationContext  context=WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
CompanyManager a = (CompanyManager) context.getBean("companyManager");
Company c=(Company)a.findByCorpID(quickCorpId).get(0);
Boolean accessQuotationFrom = c.getAccessQuotationFromCustomerFile(); 
Boolean forceDocCenter=c.getForceDocCenter();
Boolean activityMgmtVersion2 = c.getActivityMgmtVersion2();
%> 

<style type="text/css">
body {
margin-top: 0px;
margin-bottom: 0px;
}
#overlayMoodal {
display:none;
filter:alpha(opacity=60);
opacity:0.6;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:991; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
#moodal {
display:none;
opacity: 0;
}

#TopSearchBox {position:absolute;top:54px;float:right;right:4%;z-index:1;}

#searchBox {
    background-image: url("images/searchblue-left.png");
    background-position: right top;
    background-repeat: no-repeat;
    float: left;
    height: 23px;    
    padding: 0;
    width: 158px;
}

.ajaxSearch_input {
    background-color: transparent;
    border: medium none;
    color: #444444;
    float: left;
    font-size: 11px;
    height: 17px;
    margin: 2px 2px 0 14px;
    width: 132px;
}
#searchBox .ajaxSearch_input:hover{
    background-color: transparent;       
}
input[type=text]:hover
{
  background: none;
}
input::-webkit-input-placeholder {
    color:#999;
}
input:-moz-placeholder {
    color:#999;
}
input:-ms-input-placeholder {
    color:#999;
}
.ajaxSearch_submit {
    margin: 5px 0 0;
    width: 25px;
    border:none;
}


/* The hint to Hide and Show */
#hint {
   	display: none;
    position: absolute;
	color:#999;
    top:36px;
	left:5px;
    width: 215px;
    margin-top: -4px;
    border: 1px solid #d7d7d7;
	-moz-border-radius: 3px;
  	border-radius: 3px;
    padding: 5px 5px;
	font:normal 11px/16px Arial;
    /* to fix IE6, I can't just declare a background-color,
    I must do a bg image, too!  So I'm duplicating the pointer.gif
    image, and positioning it so that it doesn't show up
    within the box */
    background: #ffffff ;
}

/* The pointer image is hadded by using another span */
#hint .hint-pointer {
    position: absolute;
    top: -10px;
    width: 19px;
    height: 10px;
    background: url(images/pointer2.gif) left top no-repeat;
}
#qsearch ul
{
list-style-type:none;
padding:0px;
margin:0px;
}
#qsearch ul li
{
background-image:url(images/hint-arrow.gif);
background-repeat:no-repeat;
background-position:0px 5px; 
padding-left:10px;
padding-bottom:4px;
}
#qsearch ul li b{color:#7c7c7c;}
</style>  
<c:if test="${pageContext.request.locale.language != 'en'}">
    <div id="switchLocale"><a href="<c:url value='/?locale=en'/>"><fmt:message key="webapp.name"/> in English</a></div>
    <fmt:setLocale value="en_US" scope="session"/>
</c:if>
<c:set var="salesPortalAccess" value="false" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>
<div id="branding">
<!--<s:textfield name="fileID" id ="fileID" value="<%=request.getParameter("id") %>" />
<c:set var="fileID" value="<%=request.getParameter("id") %>"/> -->

<div id="main-top">    
          <table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
          <td width="17">&nbsp;</td>
          <td width="11%"><img src="<c:url value='/images/logo_redsky.png'/>" width="112" height="63" /></td>		  
            <c:if test="${!pwdreset}">
    		<c:if test="${!isTrue}">
    		 <td width="9%">
   	 	  <sec-auth:authComponent componentId="module.section.header.topImages">
          	<a href="toDos.html?fromUser=${pageContext.request.remoteUser}"><img src="<c:url value='/images/activity-trans.png'/>" width="94" height="63" border="0"/></a>
          </sec-auth:authComponent>
          <sec-auth:authComponent componentId="module.section.header.topActivityMgtForAgent">
        	 <a href="toDos.html?fromUser=${pageContext.request.remoteUser}"><img src="<c:url value='/images/activity-trans.png'/>" width="94" height="63" border="0"/></a>
         </sec-auth:authComponent>
          </td>
          <td width="8%">
          <sec-auth:authComponent componentId="module.section.header.topImages">
          <c:if test="${fileID == ''}">
			<a href="#"><img src="<c:url value='/images/file-cabinet-trans.png'/>" width="85" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false" onclick="message(); return false;"/></a>
			</c:if>
          <c:if test="${fileID != ''}">		          
	          <a href="#"><img src="<c:url value='/images/file-cabinet-trans.png'/>" width="85" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false" onclick="message(); return false;"/></a>
	      </c:if> 
          </sec-auth:authComponent>
          <sec-auth:authComponent componentId="module.section.header.driver">
          <c:if test="${fileID == ''}">
			<a href="#"><img src="<c:url value='/images/file-cabinet-trans.png'/>" width="85" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false" onclick="message(); return false;"/></a>
			</c:if>
          <c:if test="${fileID != ''}">		          
	          <a href="#"><img src="<c:url value='/images/file-cabinet-trans.png'/>" width="85" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false" onclick="message(); return false;"/></a>
	      </c:if>
         </sec-auth:authComponent>
          </td> 
          <td width="6%">
          <sec-auth:authComponent componentId="module.section.header.topImages">
        	 <img src="<c:url value='/images/notes-trans-new.png'/>" width="55" height="63" border="0" />
          </sec-auth:authComponent>
           <sec-auth:authComponent componentId="module.section.header.topImagesExternal">
        	 <img src="<c:url value='/images/notes-trans-new.png'/>" width="55" height="63" border="0" />
          </sec-auth:authComponent>
          </td>
          
         
          <td width="6%">
          <sec-auth:authComponent componentId="module.section.header.topImages">
          <c:if test="${idOfTasks!=''}">
          <a onclick="openTasks();"><img src="<c:url value='/images/task-trans-new.png'/>" width="55" height="63" border="0" /></a>
          </c:if>
          <c:if test="${idOfTasks== ''}">
          <img src="<c:url value='/images/task-trans-new.png'/>" width="55" height="63" border="0" />
          </c:if>
          </sec-auth:authComponent> 
          </td>
       
          <td width="8%">
          	<sec-auth:authComponent componentId="module.section.header.topImages">          
          		<a onclick="window.open('surveysList.html?decorator=popup&popup=true&from=survey','surveysList','height=600,width=875,top=20, left=60, scrollbars=yes,resizable=yes').focus();" />
          			<img src="<c:url value='/images/survery-new.png'/>" width="70" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false;"/>
          		</a>
         	</sec-auth:authComponent>
         </td>
         
         <configByCorp:fieldVisibility componentId="component.tab.header.operations">
	         <td width="8%">
	          	<sec-auth:authComponent componentId="module.section.header.topImages">          
	          		<a onclick="defaultOpsCalendar();" />
	          			<img src="<c:url value='/images/ops-calendar.png'/>" width="82" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false;"/>
	          		</a>
	         	</sec-auth:authComponent>
	         </td>
         </configByCorp:fieldVisibility>
                 
          
          <td width="9%">
           <a href="${pageContext.request.contextPath}/editProfile.html"><img src="<c:url value='/images/myaccount-new.png'/>" width="83" height="63" border="0" /></a>
           </td>
           <c:if test="${userType!='DRIVER'}">
          <configByCorp:userGuideVisibility userType="<%=quickUserType %>" corpId="<%=quickCorpId %>">  
          
          <td width="6%">
          
          <c:if test="${userType == 'USER'}">
            <a onclick="window.open('salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=ALL/ General','guideList','height=600,width=995,top=20, left=60, scrollbars=yes,resizable=yes').focus();" />
          </c:if>
         <c:if test="${userType != 'USER'}">
           <a onclick="window.open('salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=Portals','guideList','height=600,width=995,top=20, left=60, scrollbars=yes,resizable=yes').focus();" />
     	</c:if>
           <img src="<c:url value='/images/guide-trans-new.png'/>" width="55" height="63" border="0"/>
            </td>
           </configByCorp:userGuideVisibility>
           </c:if>      
        <%--  <td  width="7%"> 
         <sec-auth:authComponent componentId="module.section.header.topImages"> 
          <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
          <img src="<c:url value='/images/updates-trans.png'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          </sec-auth:authComponent> 
            <sec-auth:authComponent componentId="module.section.header.topImages">            
       <c:choose>
       <c:when test="${newsUpdateFlag == false}">
           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}'+'&updateFlag=false','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
          <img src="<c:url value='/images/updates-trans.png'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
           </c:when>
           <c:when test="${newsUpdateFlag == true}">

           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}'+'&updateFlag=true','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
           <img src="<c:url value='/images/update_final.gif'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
           </c:when>
           <c:otherwise>
            <%if(newsUpdateFlag==false){ %>         
           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}'+'&updateFlag=false','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
          <img src="<c:url value='/images/updates-trans.png'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          <%} %>
         <%if(newsUpdateFlag==true){ %> 
           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}'+'&updateFlag=true','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
           <img src="<c:url value='/images/update_final.gif'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          <%} %>
           </c:otherwise>
       </c:choose>
       
         <%if(newsUpdateFlag==false){ %>         
           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
          <img src="<c:url value='/images/updates-trans.png'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          <%} %>
         <%if(newsUpdateFlag==true){ %> 
           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
           <img src="<c:url value='/images/update_final.gif'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          <%} %>
           </sec-auth:authComponent> 
          </td> --%>
          
          <td width="5%">
          <sec-auth:authComponent componentId="module.section.header.topImages">
         <a href="#"><img oncontextmenu="alert('Right click is disabled!');return false" src="<c:url value='/images/help-new.png'/>" width="45" height="63" border="0" 
          onclick="window.open('${pageContext.request.contextPath}/resources/help/${userType}/Help.htm?decorator=popup&popup=true','help','height=800,width=750,top=0, left=610, scrollbars=yes,resizable=yes').focus();"/></a>
           </sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.section.header.topImagesExternal">
            <a href="#"><img oncontextmenu="alert('Right click is disabled!');return false" src="<c:url value='/images/help-new.png'/>" width="45" height="63" border="0" 
           onclick="window.open('${pageContext.request.contextPath}/resources/help/${userType}/Help.htm?decorator=popup&popup=true','help','height=800,width=750,top=0, left=610, scrollbars=yes,resizable=yes').focus();"/></a>
           </sec-auth:authComponent>
           </td>
           
           <td width="8%">
          <sec-auth:authComponent componentId="module.section.header.topImages">
          <!--<a onclick="window.open('editScrachCard.html?decorator=popup&popup=true','clipboard','height=600,width=475,top=0, left=610, scrollbars=no,resizable=yes').focus();">
          -->
          <a href="javascript:;" onclick="displayMoodal()" >
          <img src="<c:url value='/images/clipboard-trans-new.png'/>" width="70" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          </sec-auth:authComponent>
          </td>
          </c:if>
          
          
             
          <c:if test="${isTrue}">
          <td width="9%">
          <sec-auth:authComponent componentId="module.section.header.topImages">
          <a href="toDos.html?fromUser=${pageContext.request.remoteUser}"><img src="<c:url value='/images/activity-trans.png'/>" width="94" height="63" border="0"/></a>
          </sec-auth:authComponent>
           <sec-auth:authComponent componentId="module.section.header.topActivityMgtForAgent">
         	<a href="toDos.html?fromUser=${pageContext.request.remoteUser}"><img src="<c:url value='/images/activity-trans.png'/>" width="94" height="63" border="0"/></a>
          </sec-auth:authComponent>
          </td>
          
          <td width="8%">
           <sec-auth:authComponent componentId="module.section.header.topImages">
          <c:if test="${fileID == ''}">
			<a href="#"><img src="<c:url value='/images/file-cabinet-trans.png'/>" width="85" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false" onclick="message(); return false;"/></a>
			</c:if>
          <c:if test="${fileID != ''}">		          
	          <a href="#"><img src="<c:url value='/images/file-cabinet-trans.png'/>" width="85" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false" onclick="message(); return false;"/></a>
	      </c:if> 
          </sec-auth:authComponent>
           <sec-auth:authComponent componentId="module.section.header.driver">
             <c:if test="${fileID == ''}">
			<a href="#"><img src="<c:url value='/images/file-cabinet-trans.png'/>" width="85" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false" onclick="message(); return false;"/></a>
			</c:if>
          <c:if test="${fileID != ''}">		          
	          <a onclick="message(); return false;" ><img src="<c:url value='/images/file-cabinet-trans.png'/>" width="85" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false" /></a>
	      </c:if>
           </sec-auth:authComponent>
          </td>
          
          <td width="6%">
          <sec-auth:authComponent componentId="module.section.header.topImages">
         	<c:if test="${noteID == ''}"><img src="<c:url value='/images/notes-trans-new.png'/>" width="55" height="63" border="0" /></c:if>
         	<c:if test="${noteID != ''}"><a onclick="openNotes();"><img src="<c:url value='/images/notes-trans-new.png'/>" width="55" height="63" border="0" /></a></c:if>
          </sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.section.header.topImagesExternal">
         	<c:if test="${noteID == ''}"><img src="<c:url value='/images/notes-trans-new.png'/>" width="55" height="63" border="0" /></c:if>
         	<c:if test="${noteID != ''}"><a onclick="openRelatedNotes();"><img src="<c:url value='/images/notes-trans-new.png'/>" width="55" height="63" border="0" /></a></c:if>
          </sec-auth:authComponent>
          </td>
          <td width="6%">
           <sec-auth:authComponent componentId="module.section.header.topImages">
          <c:if test="${idOfTasks!=''}">
          <a onclick="openTasks();"><img src="<c:url value='/images/task-trans-new.png'/>" width="55" height="63" border="0" /></a>
          </c:if>
          <c:if test="${idOfTasks== ''}">
          <img src="<c:url value='/images/task-trans-new.png'/>" width="55" height="63" border="0" />
          </c:if>
          </sec-auth:authComponent> 
          </td>
             
          <td width="8%">
          <sec-auth:authComponent componentId="module.section.header.topImages">
         
          <a onclick="window.open('surveysList.html?decorator=popup&popup=true&from=survey','surveysList','height=600,width=875,top=20, left=60, scrollbars=yes,resizable=yes').focus();" />
          <img src="<c:url value='/images/survery-new.png'/>" width="70" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          
          </sec-auth:authComponent>
          </td>
          
          <configByCorp:fieldVisibility componentId="component.tab.header.operations">
          	<td width="8%">
          		<sec-auth:authComponent componentId="module.section.header.topImages">          
	          		<a onclick="defaultOpsCalendar();" />
	          			<img src="<c:url value='/images/ops-calendar.png'/>" width="82" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false;"/>
	          		</a>
	         	</sec-auth:authComponent>
        	</td>       
          </configByCorp:fieldVisibility>
          
          <td width="9%">
          <a href="${pageContext.request.contextPath}/editProfile.html"><img src="<c:url value='/images/myaccount-new.png'/>" width="83" height="63" border="0" /></a>
          </td>
          <c:if test="${userType!='DRIVER'}">
          <configByCorp:userGuideVisibility userType="<%=quickUserType %>" corpId="<%=quickCorpId %>"> 
          <td width="6%">
          
          <c:if test="${userType == 'USER'}">
            <a onclick="window.open('salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=ALL/ General','guideList','height=600,width=995,top=20, left=60, scrollbars=yes,resizable=yes').focus();" />
          </c:if>
         <c:if test="${userType != 'USER'}">
           <a onclick="window.open('salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=Portals','guideList','height=600,width=995,top=20, left=60, scrollbars=yes,resizable=yes').focus();" />
     	</c:if>
           <img src="<c:url value='/images/guide-trans-new.png'/>" width="55" height="63" border="0"/>
            </td>
          </configByCorp:userGuideVisibility>
          </c:if>
          <%--  <td width="7%">
         <sec-auth:authComponent componentId="module.section.header.topImages">          
                   
          <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
          <img src="<c:url value='/images/updates-trans.png'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          </sec-auth:authComponent>           
           <sec-auth:authComponent componentId="module.section.header.topImages">          
           <c:choose>
       <c:when test="${newsUpdateFlag == false}">
           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}'+'&updateFlag=true','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
          <img src="<c:url value='/images/updates-trans.png'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
           </c:when>
           <c:when test="${newsUpdateFlag == true}">

           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}'+'&updateFlag=true','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
           <img src="<c:url value='/images/update_final.gif'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
           </c:when>
           <c:otherwise>
            <%if(newsUpdateFlag==false){ %>         
           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}'+'&updateFlag=true','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
          <img src="<c:url value='/images/updates-trans.png'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          <%} %>
         <%if(newsUpdateFlag==true){ %> 
           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}'+'&updateFlag=true','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
           <img src="<c:url value='/images/update_final.gif'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          <%} %>
           </c:otherwise>
       </c:choose>
          <% if(newsUpdateFlag==false){%>            
          <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
          <img src="<c:url value='/images/updates-trans.png'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
           <%}%>
         <% if(newsUpdateFlag==true){%>
           <a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes').focus();">
           <img src="<c:url value='/images/update_final.gif'/>" width="69" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
           <%}%>
           </sec-auth:authComponent> 
          </td> --%>
          
		  <td width="5%"> 
          <sec-auth:authComponent componentId="module.section.header.topImages">
          <a href="#">
         <img oncontextmenu="alert('Right click is disabled!');return false" src="<c:url value='/images/help-new.png'/>" width="45" height="63" border="0" 
          onclick="window.open('${pageContext.request.contextPath}/resources/help/${userType}/Help.htm?decorator=popup&popup=true','help','height=800,width=750,top=0, left=610, scrollbars=no,resizable=yes').focus();"/></a>
          </sec-auth:authComponent>
          <sec-auth:authComponent componentId="module.section.header.topImagesExternal">
          <a href="#">
         <img oncontextmenu="alert('Right click is disabled!');return false" src="<c:url value='/images/help-new.png'/>" width="45" height="63" border="0" 
          onclick="window.open('${pageContext.request.contextPath}/resources/help/${userType}/Help.htm?decorator=popup&popup=true','help','height=800,width=750,top=0, left=610, scrollbars=no,resizable=yes').focus();"/></a>
          </sec-auth:authComponent>
          </td> 
          <td width="8%">
          <sec-auth:authComponent componentId="module.section.header.topImages">
          <!--<a onclick="window.open('editScrachCard.html?decorator=popup&popup=true','clipboard','height=600,width=475,top=0, left=610, scrollbars=no,resizable=yes').focus();">
          --><a href="javascript:;" onclick="displayMoodal()" >
          <img src="<c:url value='/images/clipboard-trans-new.png'/>" width="70" height="63" border="0" oncontextmenu="alert('Right click is disabled!');return false"/></a>
          </sec-auth:authComponent>
          </td> 
          </c:if>
             
         <td>
          <c:if test="${userType!='ACCOUNT'}">
          <a onclick="logOut();"><img src="<c:url value='/images/logoff-trans-new.png'/>" width="57" height="63" border="0" /></a>
          </c:if>
          <c:if test="${userType=='ACCOUNT'}">
          <a onclick="accountUserlogOut();"><img src="<c:url value='/images/logoff-trans-new.png'/>" width="57" height="63" border="0" /></a>
           </c:if>
           </c:if>    
 		
         </td>          
          </tr>
          </table>
          </div>
 
</div>


 <%if(quickUserType.equalsIgnoreCase("user")){ %>
<c:if test="${salesPortalAccess=='false'}">
<div id="TopSearchBox">
<div id="searchBox"> 
 <div id="hint">
 <table style="margin:0px;padding:0px;">
<th align="left">HINT</th>
<tr>
<td>
<div id="qsearch">
<ul>

<li>job#<b>db</b> -> DashBoard Details page</li>
<li>job#<b>cf</b> -> Customer File page</li>
<li>job#<b>so</b> -> S/O Details page</li>
<li>job#<b>ts</b> -> Status page</li>
<configByCorp:fieldVisibility componentId="component.standard.claimTab">
<li>job#<b>cl</b> -> Claims page</li>
	</configByCorp:fieldVisibility>
<li>job#<b>bl</b> -> Billing page</li>
<li>job#<b>ac</b> -> Accounting page</li>
<li>job#<b>wt</b> -> Ticket page</li>
<li>job#<b>fo</b> -> Forwarding page</li>
<%if(accessQuotationFrom==false){ %> 
<li>job#<b>qf</b> -> Quotation File page</li>
<li>job#<b>qu</b> -> Quote page</li>
 <%} %>
<li>job#<b>or</b> -> Order Management</li>
<li>job#<b>pr</b> -> Partner view page</li>
<li>job#<b>fc</b> -> File Cabinet on CF / So level</li> 
<li>job#<b>uf</b> -> Upload document screen on CF / SO level</li> 
<li>job#<b>nt</b> -> Notes Overview on CF / So level</li> 
<li>job#<b>nn</b> -> New Note on CF / SO level</li> 
<li>Ext.job#<b>so</b> -> SO Details page own SO#</li>
<li>Ext.job#<b>ts</b> -> Status page own SO#</li> 
<li>Ext.job#<b>bl</b> -> Billing page own SO#</li>
<li>Ext.job#<b>ac</b> -> Accounting page own SO#</li>
<li>Ext.job#<b>wt</b> -> Ticket page own SO#</li>
<li>Ext.job#<b>fo</b> -> Forwarding page own SO#</li>
<li>Ext.job#<b>fc</b> -> File Cabinet own SO#</li>
<li>Ext.job#<b>uf</b> -> Upload document screen own SO#</li>
<li>Ext.job#<b>nt</b> -> Notes overview own SO#</li>
<li>Ext.job#<b>nn</b> -> New Note own SO#</li>
</ul>
</div>
</td>
</tr>
</table>
 <div class="hint-pointer"></div></div>
 <input type="text" value="" onclick="searchDisplay();" onblur="searchDisplay1();" onkeyup="validCheck(this,'special1')" placeholder="Quick Search..." class="ajaxSearch_input" id="quickSearch" >
  </div>
 <img src="${pageContext.request.contextPath}/images/searchblue-right.png" border="0" style="vertical-align:bottom;" onclick="return testPattern();" />
</div>
</c:if>
	<%} %>	

<hr/>
<%-- Put constants into request scope --%>
<appfuse:constants scope="request"/>
<div id="overlayMoodal"></div>
<script type="text/javascript">
var r1={
		 'special1':/['\#'&'\$'&'\~'&'\!'&'\@'&'\+'&'\\'&'\/'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\|'&'\['&'\]'&'\,'&'\`'&'\='&'('&'\)']/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		
function validCheck(targetElement,w){
	 targetElement.value = targetElement.value.replace(r1[w],'');
	}
</script>
<script>
function displayMoodal()
  {
	window.open('editScrachCard.html?&decorator=popup&popup=true','test','height=700,width=725,top=0');   
  
  }
  
  function hideMoodal()
  {
	
  
   }
</script>
</script>
<script type="text/javascript">
    function searchDisplay() {
       var e = document.getElementById('hint');
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }
    function searchDisplay1() {
        var e = document.getElementById('hint');
           e.style.display = 'none';
     }
</script>
<script language="javascript">
function keyHandler(e)
{
 var pressedKey;
 if(document.all) {e = window.event}
 if(document.layers || e.which){pressedKey = 27; }
 hideMoodal();
}
document.onkeypress = keyHandler;
</script>
<script type="text/javascript">
 window.onkeypress = getActiveHTMLElement;
 function getActiveHTMLElement(keyevent) {
  keyevent = (keyevent) ? keyevent : ((window.event) ? event : null);
  if (keyevent) {
  if(keyevent.keyCode == 13) {
	 
  if(document.activeElement.id =='quickSearch'){
	       testPattern();
	 }
	
   }
  }
 }
</script>


<script type="text/JavaScript">

Array.prototype.findSuffix = function(searchStr) {
	  var returnArray = false;
	  for (i=0; i<this.length; i++) {
	    if (typeof(searchStr) == 'function') {
	      if (searchStr.test(this[i])) {
	        if (!returnArray) { 
	        	returnArray = [];
	        	}
	        returnArray.push(i);
	      }
	    } else {
	      if (this[i]===searchStr) {
	        if (!returnArray) {
	        	returnArray = [];
	        	}
	        returnArray.push(i);
	      }
	    }
	  }
	  if(returnArray==false){
		  
		  return 0;
	  }
		  return 1;
	}
function testPattern(){
     var corpID='<%=quickCorpId %>';
     var accessQuotationFromCustomerfi = '<%=accessQuotationFrom %>';
     <% String b1="false"; %>
     <configByCorp:fieldVisibility componentId="component.standard.claimTab">
   		 <% b1="true"; %>
     </configByCorp:fieldVisibility>
     if(accessQuotationFromCustomerfi=='true'){
	 var suffixArray = ['invalid','db','so','bl','ac','fo','ts','wt','cf','or','pr','fc','uf','nt','nn'];
	 <% if(b1=="true"){ %>
		suffixArray = ['invalid','db','so','bl','ac','cl','fo','ts','wt','cf','or','pr','fc','uf','nt','nn'];
         <% }%>
     }else {
    var suffixArray = ['invalid','db','so','bl','ac','fo','ts','wt','cf','qf','or','qu','pr','fc','uf','nt','nn'];
	 <% if(b1=="true"){ %>
		 suffixArray = ['invalid','db','so','bl','ac','cl','fo','ts','wt','cf','qf','or','qu','pr','fc','uf','nt','nn'];
         <% }%>
    	 }
var myText = document.getElementById('quickSearch');

if(myText.value != ""){
	var str1 = myText.value;
	 var str= str1.toLowerCase(); 
	 str = str.replace('-','');
	str = str.replace(/\s/g,'');
	
	var n=str.indexOf(corpID);
	
	
	if(n>0){
	 	var atStart= str.substring(0,n);
	
	 	if(suffixArray.findSuffix(atStart)){
	 		if(suffixArray.findSuffix(str.substring(str.length-2,str.length))){
	 			alert("invalid suffix on both side");
			       var e1 = document.getElementById('quickSearch');
			       e1.focus();
	 			return;
	 		}else{
	 			
	 			location.href='globalSearchProcess.html?searchId='+str;
				return ;
	 		
	 		}
	 	}else{
	 		if(suffixArray.findSuffix(str.substring(str.length-2,str.length))){
	 			if(suffixArray.findSuffix(str.substring(o,2))){
	 				alert("invalid without corpID and code on both side");
	 		       var e1 = document.getElementById('quickSearch');
			       e1.focus();
	 			}else{
	 				alert("valid found at last in case corpID is absent");
	 		       var e1 = document.getElementById('quickSearch');
			       e1.focus();
	 			}
	 		}else{
	 			alert("enter valid suffix");
			       var e1 = document.getElementById('quickSearch');
			       e1.focus();
	 			return ;
	 		}
	 			
	 		
	 			
	 		
	 	}
		
	}else{
        
		if(suffixArray.findSuffix(str.substring(str.length-2,str.length))){
			if(suffixArray.findSuffix(str.substring(0,2))){
				alert("invalid suffix on both side");
			       var e1 = document.getElementById('quickSearch');
			       e1.focus();
				return;
			}else{
				location.href='globalSearchProcess.html?searchId='+str;
			       var e1 = document.getElementById('quickSearch');
			       e1.focus();
				return ;
			}
 			
	}else{
		if(suffixArray.findSuffix(str.substring(0,2))){
			location.href='globalSearchProcess.html?searchId='+str;
			return ;
		}else{
			alert("Enter valid suffix");
		       var e1 = document.getElementById('quickSearch');
		       e1.focus();
			return false;
		}
		
		
	}
		}
	
	
	
	var atLast = str.substring(str.length-2,str.length);
	
	
   
  	var atBegin= str.substring(0,2);
  	
	

var resultAtLast = suffixArray.findSuffix(atLast);


var resultAtBegin = suffixArray.findSuffix(atBegin);


if(resultAtBegin==1){
	
}else if(resultAtLast==1){
	
}

if((resultAtBegin==1) && (resultAtLast==1)){
	
    str1 = str.substring(2,str.length-2);
    str2=str1.substring(0,4);
    alert(str2);
	if(str2==corpID){
 		alert("invalid suffix");
 		return;
	}else{
		alert("invalid suffix");
		return;
	}
 	}else{
 		alert("LAst Elseif  resultAtLast----"   + atLast);
 	}

}


	
else{ 
	alert("Would you please enter some text?");
	return;
}
}
</script>
<c:set var="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
	<s:hidden name="sessionCorpID" value="<%=request.getParameter("sessionCorpID") %>" />
	

<script>
function message(){	
	var fileId=document.getElementById( "fileID" ).value;
	var fileCabView = '<%= quickFileCabinetView %>';
	//10457 - Company Table
	var forceDocCenter='<%=forceDocCenter %>';
	var noteFor="";
	var PPID ="";
	if(document.getElementById( "noteFor" )==null || document.getElementById( "noteFor" )=='null'){
		noteFor="";
	}else{
	   noteFor=document.getElementById( "noteFor" ).value;
	}
	if(fileId == '' ){
		alert('Please choose a valid record before opening the File Cabinet.');
	}else{
		var fileNameFor=document.getElementById( "fileNameFor" ).value;
		var fileNameFor=document.getElementById( "fileNameFor" ).value;
		var ppType=document.getElementById( "ppType" ).value;
		var from = "";
		if(document.getElementById( "from" ) == 'null' || document.getElementById( "from" ) == null){
			from = "";
		}else{
			from = document.getElementById( "from" ).value;
		}
		var forQuotationCheck=document.getElementById( "forQuotation" );
		if(forQuotationCheck!=null){
		var forQuotation =forQuotationCheck.value; 
		}else{
		var forQuotation ="";
		}
	  	if(fileId != '' && fileNameFor ==""){
	    	alert('Please choose a valid record before opening the File Cabinet.');
	  	}
		if(fileId != '' && fileNameFor !="" && ppType =="" && forQuotation==""){
			if(fileCabView == 'Document Centre' || forceDocCenter == "true" ){
				location.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false';
			}else{
				location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false'; 
			}
			//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); 
		}
		if(fileId != '' && fileNameFor !="" && ppType =="" && forQuotation=="QC"){
			if(fileCabView == 'Document Centre' || forceDocCenter == "true" ){
				location.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&forQuotation=QC&active=true&secure=false'; 
			}else{
				location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&forQuotation=QC&active=true&secure=false';  
			}
		//	location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&forQuotation=QC&active=true&secure=false'; 
			//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); 
		}
		if(fileId != '' && fileNameFor !="" && fileNameFor =="Truck"){
			if(fileCabView == 'Document Centre' || forceDocCenter == "true" ){
				location.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false'; 
			}else{
				location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false';  
			}
			//location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false'; 
			//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); 
		}
		if(fileId != '' && fileNameFor !="" && ppType !="" && from != 'View'){
			try{
				PPID = document.getElementById("PPID").value;
			}catch(e){
				PPID=document.getElementById( "fileID" ).value;
				}
			if(fileCabView == 'Document Centre' || forceDocCenter == "true" ){
				location.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&PPID='+PPID;  
			}else{
				location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&PPID='+PPID; 
			}
			//location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&PPID='+PPID; 
			//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); 
		}
		if(fileId != '' && fileNameFor !="" && ppType !="" && from == 'View'){
			try{
			    PPID = document.getElementById("PPID").value;
			}catch(e){
				PPID=document.getElementById( "fileID" ).value;
				}
			if(fileCabView == 'Document Centre' || forceDocCenter == "true" ){
				location.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&from=View&ppCode='+ppCode; 
			}else{
				location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&from=View&ppCode='+ppCode; 
			}
			//location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&from=View&ppCode='+ppCode; 
			//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); 
		}
	}
}

function logOut(){
	location.href="userLogOut.html";
}
function accountUserlogOut(){
	location.href="accountUserLogOut.html";
}
function alertList(){
	location.href="alertHistoryList.html";
}

function openNotes(){
	var from = "";
	var PPID="";
	if(document.getElementById( "from" ) == 'null' || document.getElementById( "from" ) == null){
		from = "";
	}else{
		from = document.getElementById( "from" ).value;
	}
	var ppType=document.getElementById( "ppType" ).value;
	if(ppType !=""){
		var ppType=document.getElementById( "ppType" ).value;
		try{
		PPID = document.getElementById("PPID").value;
		}catch(e){
			PPID='${idOfWhom}';
			}
	    location.href="notess.html?id=${idOfWhom }&notesId=${noteID }&noteFor=${noteFor }&from="+from+"&PPID="+PPID+"&ppType="+ppType;
	}else{
		location.href="notess.html?id=${idOfWhom }&notesId=${noteID }&noteFor=${noteFor }";
	}
}
function openTasks(){
	var corpID='<%=quickCorpId %>';
	var flagActivityMgmtVersion2 =  <%=activityMgmtVersion2 %>;
	var idOfTasks='${idOfTasks}';
	if(idOfTasks ==''){
		alert('Please choose a valid record before opening the Tasks.');
	}else{
		
		if(!flagActivityMgmtVersion2 || ${tableName=='customerfile'})
		{
		if(${tableName=='workticket'}||${tableName=='claim'}){
			location.href="tasks.html?id=${idOfTasks}&tableName=${tableName}&soId=${tasksSoId}";
		}else{
			location.href="tasks.html?id=${idOfTasks}&tableName=${tableName}";
		}}
		else{
			  if(${tableName=='workticket'}||${tableName=='claim'}){
				  location.href="tasksCheckList.html?id=${tasksSoId}&tableName=${tableName}";	  
			  }else{
			location.href="tasksCheckList.html?id=${idOfTasks}&tableName=${tableName}";
			//location.href="tasks.html?id=${idOfTasks}&tableName=${tableName}";
			//<a href="tasksCheckList.html?id=${soObj.id}&shipnumber=${soObj.shipNumber}" ><span>Tasks Check List</span></a>
			  }
		}
	}
	<c:remove var="idOfTasks" scope="session"/>
	<c:remove var="tableName" scope="session"/>
	}
function openRelatedNotes(){
	var notefor='${noteFor}';
	if(notefor!='Partner'){		
	location.href="raleventNotess.html?id=${idOfWhom }&customerNumber=${noteID }&noteFor=${noteFor}&noteFrom=${noteFor}";	
	}
}
function defaultOpsCalendar(){
	var defaultOperationCalendarVal='';
	<configByCorp:fieldVisibility componentId="component.field.Resource.OpsCalendarView">
		defaultOperationCalendarVal='<%=defaultOperationCalendarVal %>';
	</configByCorp:fieldVisibility>
	 if(defaultOperationCalendarVal=="Planning Calendar"){ 
			window.open('planningCalendarList.html?decorator=popup&popup=true','planingCalendar','height=600,width=1100,top=90, left=50, scrollbars=yes,resizable=yes');
	 }else if(defaultOperationCalendarVal=="Crew Calendar"){ 
		 	window.open('crewCalenderView.html?decorator=popup&popup=true','crewCalendar','height=600,width=1100,top=90, left=50, scrollbars=yes,resizable=yes');
	 }else if(defaultOperationCalendarVal=="Work Planning"){ 
	 		window.open('workPlan.html?decorator=popup&popup=true','workPlanning','height=600,width=1100,top=90, left=50, scrollbars=yes,resizable=yes');
	 }else{ 
	 		window.open('opsCalendar.html?decorator=popup&popup=true','opsCalendar','height=600,width=1100,top=90, left=50, scrollbars=yes,resizable=yes');
	 } 
}

</script>

<%-- 
<script type="text/javascript" src="scripts/jquery-2.0.3.min.js"></script>


<script>
var $j = jQuery.noConflict();
$j(document).ready(
  function() {
	  $j("form").submit(function(){
		  var fromValue='<%=request.getParameter("from") %>';
		  var value="";
		  var value11="";
		  if(fromValue!='' && fromValue=='rule'){
			  var ruleNumber='<%=request.getParameter("ruleNumber") %>'; 
			  var field='<%=request.getParameter("field") %>';
			  var field1='<%=request.getParameter("field1") %>';
		  if(field!=''){
		   value = $j("input[name*='"+field+"']").val();
		  }
         if(field1!=''){
          value11 = $j("input[name*='"+field1+"']").val(); 
		 }
         if(field!='' && value!='' && field1 !='' && value11!=''){
        	 alert("To do #"+ruleNumber+" is fixed. Your activity management list will be updated in a short while"); 
         }else if(field!='' && value!=''){
        	alert("To do #"+ruleNumber+" is fixed. Your activity management list will be updated in a short while"); 
         }
		 
		
		  }
	  });
  }
);
</script>
--%>