/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "Claim" object in Redsky that allows for Claim management.
 * @Class Name	ClaimAction
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.net.InetAddress;
import java.net.UnknownHostException; 
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.beanutils.converters.BigIntegerConverter;
import org.apache.commons.lang.time.DateUtils;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.model.UserDevice;
import org.appfuse.service.UserManager;
import org.appfuse.util.StringUtil;
import org.appfuse.webapp.action.BaseAction;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.IntegrationLogInfo;
import com.trilasoft.app.model.Loss;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.RefJobType;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServiceOrderDashboard;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Userlogfile;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.LossManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NewsUpdateManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.UserDeviceManager;
import com.trilasoft.app.service.UserlogfileManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;
import com.trilasoft.app.init.AppInitServlet;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import com.google.gson.Gson;
import org.acegisecurity.ui.rememberme.TokenBasedRememberMeServices;
import org.bouncycastle.asn1.ocsp.Request;
import org.hibernate.Session;

public class ClaimAction extends BaseAction implements Preparable{
//	  Variables
	private  Map<String, String> state;
	private  Map<String, String> dcountry;
	private  Map<String, String> PWD_HINT;
	private  Map<String, String> dstates;
	private  Map<String, String> currency;
	private  Map<String, String> country;
	private List accountImageList;
	private String basedAt;
	private PartnerPublic partnerPub;
	private Boolean surveyTab = new Boolean(false);
	private  CustomerFile customerFile;
	private  TrackingStatus trackingStatus;
	private  Miscellaneous miscellaneous;
	private Date sysdate = new Date();
	private Set claims;
	private Set losss;
	private Set findAllClaimList;
	private String idNumber;
	private Long autoId;
	private int autoClaimNumber;
	private Long id;
	private Long sid;
	private Long soId;
	private Long claimNumber;
	private Long maxClm;
	private Long newClaimNumber;
	private List maxClaimNumber;
	private List refMasters;
	private List ls;
	private String currencySign;
	private String imageSelected;
	private String sessionCorpID;
	private String countForClaimNotes;
	private String countClaimValuationNotes;
	private String decorator;
	private String gotoPageString;
	private String soTab;
	private String soSortOrder;
	private String csoSortOrder;
	private String ticketSortOrder;
	private String claimSortOrder;
	private String orderForJob;
	private String orderForCso;
	private String orderForTicket;
	private String orderForClaim;
	private String defaultCss;
	private String companyNameString;
	private String userPortalCheckType;
	private String soLastName;
	private String globalSessionCorp;
	private Map<String, String> claimPersonList= new HashMap<String, String>();
	private RefJobType refJobType;
	private String jobType;
	private List claimResponsiblePersonList;
	private List claimResponsiblePerson;
	private String cDivision;
	private Claim claim;
	private ServiceOrder serviceOrder;
	private Billing billing;
	private Loss loss;
	private User user;
	private Userlogfile userlogfile;
	private UserlogfileManager  userlogfileManager;
	private ExtendedPaginatedList claimExt;
	private PaginateListFactory paginateListFactory;
	private ClaimManager claimManager;
	private LossManager lossManager;
	private ServiceOrderManager serviceOrderManager;
	private TrackingStatusManager trackingStatusManager;
	private MiscellaneousManager miscellaneousManager;
	private BillingManager billingManager;
	private NotesManager notesManager;
	private CustomerFileManager customerFileManager;
	private NewsUpdateManager newsUpdateManager;
	private UserManager userManager;
	private RefMasterManager refMasterManager;
	private PagingLookupManager pagingLookupManager;
	private PartnerManager partnerManager;
	private Company company;
	public String redirect;
	String passwordNew;
    String confirmPasswordNew;
    String newPwd;
	String newConPwd;
	String pwdHintQues;
	String pwdHint;
	boolean passreset;
	String detailPage="";
	private String shipSize;
	private List customerSO;
	private Map<String, String> DeductibleInsurer;
	private String minShip;
	private String countShip;
	private CompanyManager companyManager;
	private Map<String, String> claimDeductible;
	private Map<String, String>  claimStatus;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(ClaimAction.class);
	private String enbState;
    private Map<String, String> countryCod;
    private String userType;
    private boolean responsibleAgent=false;
    private String voxmeIntergartionFlag;
    private List<SystemDefault> sysDefaultDetail;
    private String vanLineAffiliation;
	private String utsNumber;
	private String start;
	private String calId;
	String parentAgent;
	private String usertype;
	private SystemDefault systemDefault;
    private SystemDefaultManager systemDefaultManager;
    private boolean cportalAccessCompanyDivisionLevel=false;    
    private String landingPageWelcomeMassage;
    private ExtendedPaginatedList claimsExt;
    private Boolean utsiFlag;
    private Boolean visibilityFornewAccountline=false;
    private Map<String, String> claimTypeList= new HashMap<String, String>();
    private  Map<String, String> location;
    private  Map<String, String> factor;
    private  Map<String, String> occurence;
    private  Map<String, String> claimVal;
	private boolean roleEmployee=true;
	private boolean roleSecurityAdmin=true;
	private UserDevice userDevice;
	private UserDeviceManager userDeviceManager;
    private String oiJobList;
    private int moveCount;
    private String emailSetupValue;
    private ReportAction reportAction;
    private String sessionExp;//	 Method used in user form  for forced password reset calling from user form.
	private String hitFlag;
	private Long customerFileId; 
	private String partnerDetailLocation1;
	private String partnerCompanyDivision="";
	private List userDeviceList = new ArrayList();
	private  IntegrationLogInfoManager  integrationLogInfoManager;
	private EmailSetupManager emailSetupManager;
	private String message="";
    private String dashBoardHideJobsList;
	
    
	//	 A Method to Authenticate User, calling from main menu to set CorpID.
	public ClaimAction() {
		try{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		user = (User) auth.getPrincipal();
		userType =user.getUserType();
		this.sessionCorpID = user.getCorpID();
        Set<Role> roles  = user.getRoles();
        Role role=new Role();
        Iterator it = roles.iterator();
        while(it.hasNext()) {
        	role=(Role)it.next();
        	String userRole = role.getName();
        	if(userRole.equalsIgnoreCase("ROLE_EMPLOYEE")){
        		roleEmployee=false;
        		break;
        	}
        	
        }
       
		    	
        
		}catch(Exception ex)
		{
			 ex.printStackTrace();	
		}
	}
//	  End of Method.
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		if(getRequest().getParameter("skipPrepare") == null){
		if(getRequest().getParameter("ajax") == null){  
			getComboList(sessionCorpID);
			dstates = new HashMap<String, String>();
			enbState = customerFileManager.enableStateList(sessionCorpID);
			if (id != null) {
				claim = claimManager.get(id);
				serviceOrder = claim.getServiceOrder();
				claimPersonList = refMasterManager.findCordinaor(serviceOrder.getJob(), "ROLE_CLAIM_HANDLER",sessionCorpID);
			}else{
				claimPersonList = refMasterManager.findCordinaor("", "ROLE_CLAIM_HANDLER",sessionCorpID);
			}
			systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
		}
		company=companyManager.findByCorpID(sessionCorpID).get(0);
        if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }

        if (company != null && company.getDashBoardHideJobs() != null) {
       					dashBoardHideJobsList = company.getDashBoardHideJobs();	 
       				}
        if(company!=null){
			cportalAccessCompanyDivisionLevel=company.getCportalAccessCompanyDivisionLevel();
		if(company.getVoxmeIntegration()!=null){
			voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
		if(company.getUTSI()!=null){
			utsiFlag=company.getUTSI();
			}else{
				utsiFlag=false;
			}
		}
		 try {
			 if((usertype!=null && usertype.trim().equalsIgnoreCase("AGENT"))){ 
			id = new Long(getRequest().getParameter("id").toString());
			 if(id!=null){
				 ServiceOrder agentServiceOrderCombo = serviceOrderManager.getForOtherCorpid(id);
				 TrackingStatus agentTS = trackingStatusManager.getForOtherCorpid(id);
				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(agentServiceOrderCombo.getCorpID()); 
				 if(agentServiceOrderCombo.getIsNetworkRecord() && (agentTS.getSoNetworkGroup() || chkCompanySurveyFlag)){
						surveyTab= true;
				}
				}
			 }
		 } catch (Exception e) {
			e.printStackTrace();
		 }
		}
		location = refMasterManager.findByParameter(sessionCorpID, "LOCATION");
		factor = refMasterManager.findByParameter(sessionCorpID, "FACTOR");
		occurence = refMasterManager.findByParameter(sessionCorpID, "OCCURRENCE");
		claimVal=new HashMap<String, String>();
		claimVal.put("Y", "Yes");
		claimVal.put("N", "No");
		}
		catch(Exception ex)
		{
			 ex.printStackTrace();	
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
//	 A Method to get the list of All Claims, calling from main menu in then move management.
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		claimsExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
		List defaultSortForClaimList=claimManager.claimSortOrderByUser(getRequest().getRemoteUser());
		if(defaultSortForClaimList!=null && !(defaultSortForClaimList.isEmpty())){
			String[] str1= defaultSortForClaimList.get(0).toString().split("#");
			if(str1[0].equalsIgnoreCase("1")){
				claimSortOrder="1";
			}else{
				claimSortOrder=str1[0];
			}
			if(str1[1].equalsIgnoreCase("1")){
				orderForClaim="ascending";
			}else{
				orderForClaim=str1[1];
			}
		}}
		 catch(Exception ex)
		{
			 ex.printStackTrace();	
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
//	  End of Method.
	
//  A Method to get the list of Claims search view , calling from search Claim List.	
	@SkipValidation
	public String search() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		boolean claimNumber = (claim.getClaimNumber() == null);
		boolean lastName = (claim.getLastName() == null);
		boolean shipNumber = (claim.getShipNumber() == null);
		boolean claimPerson = (claim.getClaimPerson() == null);
		boolean closeDate=(claim.getCloseDate() == null);
		boolean registrationNumber=(claim.getRegistrationNumber() == null);
		boolean clmsClaimNbr = (claim.getClmsClaimNbr()==null);
		if (!claimNumber || !lastName || !shipNumber || !closeDate || !claimPerson || !registrationNumber || !clmsClaimNbr) {
		claimsExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
        List<SearchCriterion> searchCriteria = new ArrayList();
		searchCriteria.add(new SearchCriterion("claimNumber", claim.getClaimNumber() , SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
		searchCriteria.add(new SearchCriterion("lastName", claim.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
        searchCriteria.add(new SearchCriterion("shipNumber", claim.getShipNumber().replaceAll("'", "").replace("-", "").trim(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
        searchCriteria.add(new SearchCriterion("registrationNumber", claim.getRegistrationNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
        searchCriteria.add(new SearchCriterion("closeDate", claim.getCloseDate(), SearchCriterion.FIELD_TYPE_DATE, SearchCriterion.OPERATOR_EQUALS));
        searchCriteria.add(new SearchCriterion("claimPerson", claim.getClaimPerson(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
        searchCriteria.add(new SearchCriterion("clmsClaimNbr", claim.getClmsClaimNbr() , SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
		pagingLookupManager.getAllRecordsPage(Claim.class, claimsExt, searchCriteria);
		}
		if(claimsExt.getFullListSize()==1){
			detailPage = "true";	
		}}
		catch(Exception ex)
		{
			 ex.printStackTrace();	
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
//	  End of Method.


	//	 A Method to get the list of All Claims related to service order , calling from service order tabs.	
	/** This Method is use in the claim geeting executitive role user list */
	@SkipValidation
	public String findClaimPersonList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		try
		{
		claimResponsiblePersonList = refMasterManager.findCordinaorList(jobType,"ROLE_CLAIM_HANDLER",sessionCorpID);
		}
		catch(Exception ex)
		{
			 ex.printStackTrace();	
		}
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findResponsiblePerson(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		try
		{
		claimResponsiblePerson = claimManager.findResponsiblePerson(jobType,cDivision,sessionCorpID);
		}
		catch(Exception ex)
		{
			 ex.printStackTrace();	
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	/** End of Method. */
	
	@SkipValidation
	public String showPentaho(){
		  try{
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = (User) auth.getPrincipal();			
				getRequest().setAttribute("corpid",user.getCorpID());	
				sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(user.getCorpID());
				if(sysDefaultDetail!=null && !(sysDefaultDetail.isEmpty()) && sysDefaultDetail.get(0)!=null){
					SystemDefault sys=sysDefaultDetail.get(0);
					if(sys.getBaseCurrency()!=null){
					getRequest().setAttribute("curr",sys.getBaseCurrency());
					}
				}
				}catch(Exception ex)
				{
					 ex.printStackTrace();	
				}
		  return SUCCESS;
	  }
	@SkipValidation
	public String showPentahoSec(){
		  return SUCCESS;
	  }
	@SkipValidation
	public String updatecustomCalanderSec(){
		try
		{
				claimManager.updateSurveyCalData(start,calId);
		}
		catch(Exception ex)
		{
			 ex.printStackTrace();	
		}return SUCCESS;
	  }
	
	private String defaultVal;
	private String msgClicked;
public String claimList()	{
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	  try
	  {
	        serviceOrder = serviceOrderManager.get(id);
	    	getRequest().setAttribute("soLastName",serviceOrder.getLastName());
	    	trackingStatus=trackingStatusManager.get(id);
			miscellaneous = miscellaneousManager.get(id); 
			billing = billingManager.get(id);
			customerFile=serviceOrder.getCustomerFile(); 
	    	claims = new HashSet( serviceOrder.getClaims() );
	    	if(claims.size()==1){
	    		defaultVal="true";
	    	}
	    	shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
			minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
	        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
	  }
	  catch(Exception ex)
		{
			 ex.printStackTrace();	
		}
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return SUCCESS;   
	}
//	  End of Method.
 // Method for editing and adding the Claim record calling from Claim form list and add buttons.	
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		redirect="";
		try
		{
		if (id != null) { 
		claim = claimManager.getForOtherCorpid(id);
		dstates = customerFileManager.findDefaultStateList(claim.getCountry(), sessionCorpID);
		serviceOrder = claim.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		String claimPersonName = claimManager.getClaimPersonName(claim.getClaimPerson(),sessionCorpID);
		claimPersonList.put(claim.getClaimPerson(), claimPersonName);
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		claim.setShipNumber(serviceOrder.getShipNumber());
		Integer claimByValue=claimManager.findClaimByValue(sessionCorpID,customerFile.getSequenceNumber());
		claim.setClaimBy(claimByValue.intValue());
		if ((billingManager.checkById(serviceOrder.getId())).isEmpty()) {
			billing = new Billing();
			billing.setShipNumber(serviceOrder.getShipNumber());
		} else {
			billing = billingManager.get(serviceOrder.getId());
		}
		if(claim.getNoProcess()== null) {
		claim.setNoProcess(billing.getNoMoreWork());
		}
		emailSetupValue=integrationLogInfoManager.findSendMoveCloudStatusCrew(sessionCorpID, claim.getClaimNumber().toString());
		if(emailSetupValue!= null && !emailSetupValue.equals("")){
			moveCount=1;
		}
		} else {   
		claim = new Claim();
		serviceOrder = serviceOrderManager.get(sid);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		trackingStatus = trackingStatusManager.get(sid);
		miscellaneous = miscellaneousManager.get(sid);
		billing = billingManager.get(sid);
		customerFile = serviceOrder.getCustomerFile();
		claim.setShipNumber(serviceOrder.getShipNumber());
		claim.setPaidByName(serviceOrder.getCustomerFile().getBillToName());
		claim.setLastName(serviceOrder.getCustomerFile().getLastName());
		claim.setFirstName(serviceOrder.getFirstName());
		claim.setAddressLine1(serviceOrder.getDestinationAddressLine1());
		claim.setAddressLine2(serviceOrder.getDestinationAddressLine2());
		claim.setAddressLine3(serviceOrder.getDestinationAddressLine3());
		claim.setCountry(serviceOrder.getDestinationCountry());
		claim.setState(serviceOrder.getDestinationState());
		claim.setCity(serviceOrder.getDestinationCity());
		claim.setCityCode(serviceOrder.getDestinationCityCode());
		claim.setCountryCode(serviceOrder.getDestinationCountryCode());
		claim.setZip(serviceOrder.getDestinationZip());
		claim.setEmail(serviceOrder.getDestinationEmail());
		claim.setEmail2(serviceOrder.getDestinationEmail2());
		claim.setFax(serviceOrder.getDestinationFax());
		Integer claimByValue=claimManager.findClaimByValue(sessionCorpID,customerFile.getSequenceNumber());
		claim.setClaimBy(claimByValue.intValue());
		if(null!=serviceOrder.getDestinationHomePhone()){
		claim.setPhone(serviceOrder.getDestinationHomePhone());
		}
		if((null!=serviceOrder.getDestinationDayPhone() && !(serviceOrder.getDestinationDayPhone().equals("")))&& (null!=serviceOrder.getDestinationDayExtn()&& !(serviceOrder.getDestinationDayExtn().equals("")))){
		claim.setShipperWorkPhone(serviceOrder.getDestinationDayPhone()+"("+serviceOrder.getDestinationDayExtn()+")");
		}
		else{
			claim.setShipperWorkPhone("");
		}
	
		claim.setStatus("New");
		dstates = customerFileManager.findDefaultStateList(claim.getCountry(), sessionCorpID);
		if ((billingManager.checkById(serviceOrder.getId())).isEmpty()) {
			billing = new Billing();
			billing.setShipNumber(serviceOrder.getShipNumber());
		} else {
			billing = billingManager.get(serviceOrder.getId());
		}
		if (billing.getTotalInsrVal() != null && !billing.getTotalInsrVal().equalsIgnoreCase("") && billing.getTotalInsrVal().trim().length() > 0) {
			claim.setPremium(new BigDecimal(billing.getTotalInsrVal()));
		} else {
			claim.setPremium(new BigDecimal(0.0));
		}
		claim.setNoProcess(billing.getNoMoreWork());
		claim.setInsurerCode(billing.getVendorCode());
		claim.setInsurer(billing.getVendorName());
		claim.setClaimCertificateNumber(billing.getCertnum());
		claim.setCreatedOn(new Date());
		claim.setUpdatedOn(new Date());
		claim.setUpdatedBy(getRequest().getRemoteUser());
		claim.setFirstName(serviceOrder.getFirstName());
		claim.setLastName(serviceOrder.getLastName());
		claim.setShipNumber(serviceOrder.getShipNumber());
		claim.setSequenceNumber(serviceOrder.getSequenceNumber());
		if((!(serviceOrder.getJob().equalsIgnoreCase("UVL"))) && (!(serviceOrder.getJob().equalsIgnoreCase("MVL")))){
			claim.setConfirmedDeliveryDateByCustomer(trackingStatus.getDeliveryA());
		}
		}
		if(userType.equalsIgnoreCase("AGENT")){
		 String responsibleAgentCode="";
	        List partnerCorpIDList = new ArrayList(); 
	        List list2 = new ArrayList();
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();	
	        Map filterMap=user.getFilterMap();
	        if(filterMap.containsKey("agentFilter")){
		    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
		    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
		    	Iterator iterator = valueSet.iterator();
		    	while (iterator.hasNext()) {
		    		String  mapkey = (String)iterator.next(); 
				    if(filterMap.containsKey(mapkey)){	
					HashMap valueMap=	(HashMap)filterMap.get(mapkey);
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
						Map.Entry entry = (Map.Entry) mapIterator.next();
						String key = (String) entry.getKey(); 
						List partnerCorpIDList1 = new ArrayList();
						partnerCorpIDList1= (List)entry.getValue();
						Iterator listIterator= partnerCorpIDList1.iterator();
				    	while (listIterator.hasNext()) {
				    		String value=listIterator.next().toString();
				    		if(value!=null && (!(value.trim().equalsIgnoreCase("--NO-FILTER--")))){
				    		partnerCorpIDList.add("'" + value + "'");
				    		}
				    	}
						
						}
					}
					}
					}
		    	 }
	        }
		    	 Iterator listIterator= partnerCorpIDList.iterator();
			    	while (listIterator.hasNext()) {
			    		responsibleAgentCode= listIterator.next().toString();
			    		responsibleAgentCode= responsibleAgentCode.toString().replace("'","").replace("'", "");
			    		if(claim.getResponsibleAgentCode()!=null){
			    		if(responsibleAgentCode.equals(claim.getResponsibleAgentCode())){
			    			responsibleAgent=true;
			    			break;
			    		}
			    		}
			    		
			    	}
		}
			    	
		getNotesForIconChange();
		}
		catch(Exception ex)
		{
			 ex.printStackTrace();	
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
//	  End of Method.
//	 Method used to save the claim calling from claim form save button.
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		serviceOrder = serviceOrderManager.get(soId);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		if(claim.getInsurerCode() != null && (!claim.getInsurerCode().equals(""))){
		if(insurerCodeValid().equals("input")){
			String key = "Insurer Code is not valid, please select again.";
			errorMessage(getText(key));
			return SUCCESS;
		} else {
			claim.setInsurer(billingManager.findByBillToCode(claim.getInsurerCode(), sessionCorpID).get(0).toString());
		}
		}
		boolean isNew = (claim.getId() == null);
		if (isNew) {
			newClaimNumber = claimManager.findMaximum(sessionCorpID);
			claim.setClaimNumber(newClaimNumber);			
			List maxIdNumber = claimManager.findMaximumIdNumber(serviceOrder.getShipNumber());
	   	    if ( maxIdNumber.get(0) == null  ) {          
	           	 idNumber = "01";
	           }else {
	        	   if( maxIdNumber.get(0).toString().equals("")){
	        		   idNumber = "01";   
	        	   }else{
	        		   autoId = Long.parseLong((maxIdNumber).get(0).toString()) + 1;
	        		   if((autoId.toString()).length() == 1) {
	        			   idNumber = "0"+(autoId.toString());
	        		   }else {
	        			   idNumber=autoId.toString();
	        		   }
	        	   }
	           }
			}
		
		claim.setServiceOrder(serviceOrder);
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();		
		if (isNew) {
			claim.setCreatedOn(new Date());
			claim.setIdNumber(idNumber);
		}else{
			claim.setIdNumber(claim.getIdNumber());
		}
		claim.setShipNumber(serviceOrder.getShipNumber());
		claim.setUpdatedOn(new Date());
		claim.setUpdatedBy(getRequest().getRemoteUser());
		if(claim.getCorpID()==null || claim.getCorpID().equals("")){
		claim.setCorpID(sessionCorpID);
		}
		if(claim.getSubmitDate()!=null && (claim.getStatus()==null || claim.getStatus().trim().equals("") || claim.getStatus().trim().equalsIgnoreCase("New"))){
			claim.setStatus("Submitted");
			claim.setSubmitFlag("True");
		}
		if(claim.getCloseDate()!=null){
			claim.setStatus("Closed");
			claim.setSubmitFlag("True");
		}
		if(claim.getCancelled()!=null){
			claim.setStatus("Cancelled");
			claim.setSubmitFlag("True");
		}
		if(claim.getSubmitDate()==null && claim.getCloseDate()==null && claim.getCancelled()==null && (claim.getStatus()==null || claim.getStatus().trim().equals(""))){
			claim.setStatus("New");
		}
		
		if((claim.getResponsibleAgentCode()!=null) && (!claim.getResponsibleAgentCode().toString().trim().equals(""))){
			try{
			List ResponsibleAgentList=customerFileManager.findByBillToCode(claim.getResponsibleAgentCode(), sessionCorpID);
			if(ResponsibleAgentList.size()>0 && !ResponsibleAgentList.isEmpty()){
				claim.setResponsibleAgentName(ResponsibleAgentList.get(0).toString());
			}
			}catch (NullPointerException nullExc){
				nullExc.printStackTrace();						
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
		if(sessionCorpID.equals("UTSI") && claim.getCportalAccess() && claim.getClaimEmailSent()==null)
		{
		try {
			String from ="claims@harmonyrelo.com";
			String msgText1="Dear " +customerFile.getPrefix()+" "+customerFile.getLastName()+",";
			String appUrl = getRequest().getRequestURL().toString();
			if (appUrl.indexOf("localhost") >= 0) {
				appUrl = appUrl.substring(0, appUrl.indexOf("/", appUrl.indexOf("//")+2 ) + 1);
			}else{
			appUrl = appUrl.substring(0, appUrl.indexOf("/", appUrl.indexOf("/", appUrl.indexOf("//")+2 ) + 1)+1).replaceAll("redsky/", "");
			}
			msgText1 = msgText1+"\n\nYou have reported a claim and therefore we first would like to extend our apologies. We are truly sorry that something went wrong with your shipment and we would like to assist you with settling your claim as soon as possible to your content.";
			msgText1 = msgText1+"\n\n<B>Claim details</B>";
			msgText1 = msgText1+"\n\nService Order number:"+serviceOrder.getShipNumber();
			msgText1 = msgText1+"\n\nType Move:"+serviceOrder.getMode();
			msgText1 = msgText1+"\n\nClaim file number:"+claim.getClaimNumber();
			msgText1 = msgText1+"\n\nTo file your claim with all necessary information, please go to your registered claim number:<a href='"+appUrl+"cportal/addClaimsDetails.html?ship="+claim.getShipNumber()+"&claimNumber="+claim.getClaimNumber()+"&localeSession=en'>"+claim.getClaimNumber()+"</a>";
			msgText1 = msgText1+"\n\nTo login, please use your username and password which you have received at the initiation of the move. Please find enclosed important information to assist you with correctly filing your claim.";
			msgText1 = msgText1+"\n\nPlease complete the digital claim form and upload all relevant supporting documentation, as described in the enclosed �Important information for filing a claim� (e.g. digital photographs, corresponding repair estimate from local vendor, etc).";
			msgText1 = msgText1+"\n\nAll damages and/or losses must be filled without delay, no later than 30 days after delivery. Once you have filled the information into the system we will process your claim as soon as possible.";
			msgText1 = msgText1+"\n\nPlease do not hesitate to contact us should any questions arise.";
			msgText1 = msgText1+"\n\nKind Regards,";
			msgText1 = msgText1+"\n\nHarmony Relocation Network";
			msgText1 = msgText1+"\n\nClient Services";

			String subject = "Claim instructions";
			String attachementLocation="/usr/local/redskydoc/Important_in_ormation_for_filing_a_claim.pdf";
			emailSetupManager.globalEmailSetupProcess(from, customerFile.getEmail(), "", "", attachementLocation, msgText1, subject, sessionCorpID,"",claim.getClaimNumber().toString(),"");
			claim.setClaimEmailSent(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		claim = claimManager.save(claim);
		
		if(claim.getDocCompleted()!=null && utsiFlag){
		reportAction.claimFormFileUpload(claim.getShipNumber(),claim.getClaimNumber(),"DocsCompleted.jrxml",claim.getDocCompleted(),"Claim Declaration Form");
		}
		if(claim.getSettlementForm()!=null && utsiFlag){
		reportAction.claimFormFileUpload(claim.getShipNumber(),claim.getClaimNumber(),"ClaimSettlement.jrxml",claim.getSettlementForm(),"Claim Settlement");
		}
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "Claim Detail has been successfully added and the claim number is: "	+ claim.getClaimNumber() : "Claim has been updated successfully";
			saveMessage(getText(key));
		}
		if((!(serviceOrder.getJob().equals("STO")))&& (!(serviceOrder.getJob().equals("TPS")))&& (!(serviceOrder.getJob().equals("STF")))&& (!(serviceOrder.getJob().equals("STL"))))
		 {
			 List claimList = claimManager.checkBySirviceOrderId(serviceOrder.getId(),sessionCorpID);
			 serviceOrder=serviceOrderManager.get(serviceOrder.getId());
			if(id!=null){
				String claimPersonName = claimManager.getClaimPersonName(claim.getClaimPerson(),sessionCorpID);
				claimPersonList.put(claim.getClaimPerson(), claimPersonName);
			}
			 String claimCount=claimList.get(0).toString();
			 if((serviceOrder.getStatusNumber().intValue()<70)&& !(claimCount.equalsIgnoreCase("0"))){
				 String status="CLMS";
	     		  int statusNumber=70;
	     		  int i= serviceOrderManager.updateStorageJob(serviceOrder.getId(),status,statusNumber,sessionCorpID,getRequest().getRemoteUser()); 
			 } 
		 }
		 try{
			 if(serviceOrder.getIsNetworkRecord() && trackingStatus.getSoNetworkGroup())	{
		        	List linkedShipNumber=findLinkedShipNumber(serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					synchornizeClaim(serviceOrderRecords,claim,isNew,serviceOrder,trackingStatus);
					
			   }
		  }catch(Exception ex){
				 ex.printStackTrace();
		   }
		
		dstates = customerFileManager.findDefaultStateList(claim.getCountry(), sessionCorpID);
		getNotesForIconChange();
		redirect = "yes";
		if (!isNew) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		} else {
			maxClm = Long.parseLong(claimManager.findMaximumId().get(0).toString());
			claim = claimManager.get(maxClm);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	}
	
//	  End of Method.

	
	
	
    private void synchornizeClaim(List<Object> serviceOrderRecords,Claim claim, boolean isNew,ServiceOrder serviceOrderold,TrackingStatus trackingStatus) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	try
    	{
    	Iterator  it=serviceOrderRecords.iterator();
    	while(it.hasNext()){
    		ServiceOrder serviceOrderObj=(ServiceOrder)it.next();
    		TrackingStatus trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrderObj.getId());
			if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecod.getSoNetworkGroup()){
    		if(!(serviceOrderObj.getShipNumber().equals(serviceOrderold.getShipNumber()))){
    		try{
    			if(isNew){
    				newClaimNumber = claimManager.findMaximumForOtherCorpid(serviceOrderObj.getCorpID());
    				Claim claimNew= new Claim();
    				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.getConvertUtils().register( new BigIntegerConverter(null), BigInteger.class);
    				beanUtilsBean.copyProperties(claimNew, claim);
    				
    				claimNew.setId(null);
    				claimNew.setCorpID(serviceOrderObj.getCorpID());
    				claimNew.setShipNumber(serviceOrderObj.getShipNumber());
    				claimNew.setSequenceNumber(serviceOrderObj.getSequenceNumber());
    				claimNew.setServiceOrder(serviceOrderObj);
    				claimNew.setServiceOrderId(serviceOrderObj.getId());
    				claimNew.setCreatedBy("Networking");
    				claimNew.setUpdatedBy("Networking");
    				claimNew.setCreatedOn(new Date());
    				claimNew.setUpdatedOn(new Date()); 
    				claimNew.setClaimNumber(newClaimNumber);
    				claimNew.setMoveCloudStatus("");
    				try{
    				if(claim.getInsurerCode()!=null && !claim.getInsurerCode().equals("")){
    				List  l1 = claimManager.findPartnerCodeOtherCorpID(claim.getInsurerCode(),sessionCorpID);
						if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
							Boolean agentChk   = (Boolean) l1.get(0);
							if(!agentChk){
								claimNew.setInsurerCode("");
								claimNew.setInsurer("");
							}
						}	
					}else{
						claimNew.setInsurerCode("");
						claimNew.setInsurer("");
					}
    				}catch(Exception e){
    					claimNew.setInsurerCode("");
						claimNew.setInsurer("");
						e.getStackTrace();
    				}
    				Claim claim1 = claimManager.save(claimNew);
    				
    				if(claim1.getDocCompleted()!=null && utsiFlag){
    					reportAction.claimFormFileUploadLink(claim1,"DocsCompleted.jrxml","Claim Declaration Form",claim);
    					}
    					if(claim1.getSettlementForm()!=null && utsiFlag){
    					reportAction.claimFormFileUploadLink(claim1,"ClaimSettlement.jrxml","Claim Settlement",claim);
    					}
    				
    	       }else{
    	    	   Long claimId =claimManager.findRemoteClaim(claim.getIdNumber(),serviceOrderObj.getId()); 
    				Claim claimTo= claimManager.getForOtherCorpid(claimId);  
        			Long id=claimTo.getId();
        			String createdBy=claimTo.getCreatedBy();
        			Date createdOn=claimTo.getCreatedOn();
        			String insCode = claimTo.getInsurerCode();
        			String insurer = claimTo.getInsurer();
        			Long claimNum = claimTo.getClaimNumber();
        			String idNum = claimTo.getIdNumber();
        			String moveClStatus = claimTo.getMoveCloudStatus();
        			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.getConvertUtils().register( new BigIntegerConverter(null), BigInteger.class);
    				beanUtilsBean.copyProperties(claimTo, claim);
    				
    				claimTo.setId(id);
    				claimTo.setCreatedBy(createdBy);
    				claimTo.setCreatedOn(createdOn);
    				claimTo.setCorpID(serviceOrderObj.getCorpID());
    				claimTo.setShipNumber(serviceOrderObj.getShipNumber());
    				claimTo.setSequenceNumber(serviceOrderObj.getSequenceNumber());
    				claimTo.setServiceOrder(serviceOrderObj);
    				claimTo.setServiceOrderId(serviceOrderObj.getId());
    				claimTo.setUpdatedBy(claim.getCorpID()+":"+getRequest().getRemoteUser());
    				claimTo.setUpdatedOn(new Date()); 
    				claimTo.setInsurerCode(insCode);
    				claimTo.setInsurer(insurer);
    				claimTo.setClaimNumber(claimNum);
    				claimTo.setIdNumber(idNum);
    				claimTo.setMoveCloudStatus(moveClStatus);
    				Claim claim1 = claimManager.save(claimTo);
    				if(claim1.getDocCompleted()!=null && utsiFlag){
    					reportAction.claimFormFileUploadLink(claim1,"DocsCompleted.jrxml","Claim Declaration Form",claim);
    					}
    					if(claim1.getSettlementForm()!=null && utsiFlag){
    					reportAction.claimFormFileUploadLink(claim1,"ClaimSettlement.jrxml","Claim Settlement",claim);
    					}
    			}
    		}catch(Exception ex){
    			 ex.printStackTrace();
    		}
    		if((!(serviceOrderObj.getJob().equals("STO")))&& (!(serviceOrderObj.getJob().equals("TPS")))&& (!(serviceOrderObj.getJob().equals("STF")))&& (!(serviceOrderObj.getJob().equals("STL")))){
    		if((serviceOrderObj.getStatusNumber().intValue()<70)){
				 String status="CLMS";
	     		  int statusNumber=70;
	     		serviceOrderObj.setUpdatedBy(claim.getCorpID()+":"+getRequest().getRemoteUser());
	     		serviceOrderObj.setUpdatedOn(new Date());
	     		 serviceOrderManager.save(serviceOrderObj);
			 } 
    		}
    	 }
		}	
     }}
    	catch(Exception ex)
		{
			 ex.printStackTrace();	
		}

    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
	}
	
private List findLinkedShipNumber(ServiceOrder serviceOrder) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List linkedShipNumberList= new ArrayList();
		try
		{
    	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
    	}else{
    		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
    	}}
		catch(Exception ex)
		{
			 ex.printStackTrace();	
		}

    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return linkedShipNumberList;
}	
	
private List<Object> findServiceOrderRecords(List linkedShipNumber,ServiceOrder serviceOrderLocal) {
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	List<Object> recordList= new ArrayList();
	try
	{
	Iterator it =linkedShipNumber.iterator();
	while(it.hasNext()){
		String shipNumber= it.next().toString();
		if(!serviceOrderLocal.getShipNumber().equalsIgnoreCase(shipNumber)){
			ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
			recordList.add(serviceOrderRemote);
		}
	}}
	catch(Exception e)
	{
		e.printStackTrace();
	}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return recordList;
}

public String insurerCodeValid()throws Exception{
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(billingManager.findByBillToCode(claim.getInsurerCode(), sessionCorpID).isEmpty()){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		}
		else{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
		
	}
	
	
	

// Method for getting the values for all comboboxes calling from claim form for dropdown list.
	public String getComboList(String corpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		try {
			
			state = refMasterManager.findCountry(corpID, "STATE");
			DeductibleInsurer=refMasterManager.findCountry(corpID, "DeductibleInsurer");
			claimStatus=refMasterManager.findDocumentList(corpID, "CLAIMSTATUS");
			dcountry = refMasterManager.findCountry(corpID, "COUNTRY");
			country = refMasterManager.findCountryForPartner(corpID, "COUNTRY");
			PWD_HINT = refMasterManager.findByParameter(corpID, "PWD_HINT");
			claimDeductible=refMasterManager.findByParameter(corpID, "claimdeductible");
			company= companyManager.findByCorpID(corpID).get(0);
			currencySign=company.getCurrencySign();	
			currency = refMasterManager.findCodeOnleByParameter(corpID, "CURRENCY");
			countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
			claimTypeList = refMasterManager.findByParameter(sessionCorpID, "CLAIMTYPE");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
//	  End of Method.

// Method getting the count of notes according to which the icon color changes in form
	@SkipValidation
	public String getNotesForIconChange() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		List noteClaim = notesManager.countForClaimNotes(claim.getClaimNumber());
		List noteValuation = notesManager.countClaimValuationNotes(claim.getClaimNumber());
		if (noteClaim.isEmpty()) {
			countForClaimNotes = "0";
		} else {
			countForClaimNotes = ((noteClaim).get(0)).toString();
		}
		if (noteValuation.isEmpty()) {
			countClaimValuationNotes = "0";
		} else {
			countClaimValuationNotes = ((noteValuation).get(0)).toString();
		}}
		 catch(Exception e)
		 {e.printStackTrace();}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
//	  End of Method.

//	 Method used in if any change is done in calling from claimform for auto save button
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		String s = save();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}
//	  End of Method.

//	 A Method to get the list of All Losses related to Claim , calling from Claim form.
	public String lossList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		claim = claimManager.getForOtherCorpid(id);
		serviceOrder = claim.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus = trackingStatusManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile();
		billing = billingManager.get(serviceOrder.getId());
		losss = new HashSet(lossManager.lossForOtherCorpId(claim.getId(),claim.getCorpID()));
    	if(losss.size()==1){
    		defaultVal="true";
    	}}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
//	  End of Method.
	public String findUserDetails() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		PWD_HINT = refMasterManager.findByParameter(sessionCorpID, "PWD_HINT");
		systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		if(company !=null && (!(company.getStatus()))){ 
			sessionExp="true";
			ServletContext sc =getRequest().getSession(true).getServletContext();
    		sc.setAttribute("restrictLoginMsg", "Your company is not using RedSky.");
			getRequest().getSession().setAttribute("sessionExp", sessionExp);
			return "rajeshlogin";
		}
		if(user.getUserType().equalsIgnoreCase("ACCOUNT")){
			accountImageList=claimManager.findUserByBasedAt(user.getBasedAt(),sessionCorpID);
			if(accountImageList!=null && (!(accountImageList.isEmpty())) && accountImageList.get(0)!=null){
			partnerDetailLocation1=(accountImageList.get(0)!=null?accountImageList.get(0).toString():"");
			}
			
			if(company!=null){
				cportalAccessCompanyDivisionLevel=company.getCportalAccessCompanyDivisionLevel();
			}
			if(cportalAccessCompanyDivisionLevel){
			List accountCompanyDivision=claimManager.findUserCompanyDivision(user.getBasedAt(),sessionCorpID);	
			if(accountCompanyDivision!=null && (!(accountCompanyDivision.isEmpty())) && accountCompanyDivision.get(0)!=null){
				partnerCompanyDivision=accountCompanyDivision.get(0).toString();
			}
			}			
		}
		if(user.getUserType().equalsIgnoreCase("AGENT")){
			this.parentAgent=user.getParentAgent();
			if(parentAgent==null || parentAgent.equals("")){
				parentAgent=user.getBasedAt();
			}
			List partnerDetails=customerFileManager.getPartnerDetails(parentAgent,sessionCorpID);
			if(partnerDetails!=null && !partnerDetails.isEmpty()){
			Iterator it = partnerDetails.iterator();
			while (it.hasNext()) {
				Object[] object = (Object[]) it.next();
				if(object[0]!=null && object[0].toString().contains("United")){
					vanLineAffiliation="United";	
				}
				if(object[1]!=null){
					utsNumber=object[1].toString();	
				}
			}
			}	
		}
		getRequest().getSession().setAttribute("userType", user.getUserType());
		getRequest().getSession().setAttribute("newsUpdateFlag", user.isNewsUpdateFlag());		
		try{
			sessionExp="false";
			getRequest().getSession().setAttribute("sessionExp", sessionExp);
		Date passwordExp=user.getPwdexpiryDate();
		Date cal = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String pwdExpDate =formatter.format(cal);
		List dateDiffExp=companyManager.getDateDiffExp(user.getUsername(), sessionCorpID, passwordExp);
		List dateDiffAlert=companyManager.getDateDiffAlert(user.getUsername(), sessionCorpID, passwordExp, pwdExpDate);
		if(Long.parseLong(dateDiffAlert.get(0).toString())<=10 && Long.parseLong(dateDiffAlert.get(0).toString())>=0){
			String clickHere = "window.open('resetAlertPassword.html?decorator=popup&popup=true','forms','height=300,width=530,top=120,left=150')";
			saveMessage("Your password will expire in "+dateDiffAlert.get(0).toString()+" days, please click <b><a style='text-decoration: underline; cursor: pointer;' onclick="+clickHere+">here</a></b> to change your password, to extend the expiry date.");
		}
		if(Long.parseLong(dateDiffExp.get(0).toString())>0)
		{
		String expAction="";
		 if((user.getUserType().toString().trim().equalsIgnoreCase("ACCOUNT"))  || (user.getUserType().toString().trim().equalsIgnoreCase("AGENT")) || (user.getUserType().toString().trim().equalsIgnoreCase("PARTNER")))
		 {       
			 expAction=company.getPartnerActionOnExpiry();
		}else
		{
			expAction=company.getUserActionOnExpiry();
		}
			
		if(expAction.equalsIgnoreCase("reset"))
		{
			saveMessage("Please reset your password as it has been expired per the company security policies.");
			return "rajesh";
		}
		if(expAction.equalsIgnoreCase("expire"))
		{
			userManager.setExpirePassword(user.getUsername(), sessionCorpID);
			saveMessage("Your userid has been expired, please contact support@redskymobility.com for re-activation.");
			sessionExp="true";
			getRequest().getSession().setAttribute("sessionExp", sessionExp);
			return "rajeshlogin";
		}
		}
		if(company.getRestrictAccess() && user.getUserType().trim().equalsIgnoreCase("USER")){
			
			Set<Role> roles  = user.getRoles();
	        Role role=new Role();
	        Iterator it = roles.iterator();
	        while(it.hasNext()) {
	        	role=(Role)it.next();
	        	String userRole = role.getName();
	        	if(userRole.equalsIgnoreCase("ROLE_SECURITY_ADMIN")){
	        		roleSecurityAdmin=false;
	        		break;
	        	}
	        	
	        }
	        if(roleSecurityAdmin){
			String dnsclient="";
				List nameservers = sun.net.dns.ResolverConfiguration.open().nameservers();
				try{
					for( Object dns : nameservers ) {
						dnsclient=dns.toString();
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			String IPaddress=getRequest().getRemoteAddr(); 
			boolean loginFlag=false;
			nameservers.add(IPaddress);
			String excludeIpCheck = "";
			String [] includeIpData = systemDefault.getExcludeIPs().split(",");
			for (String includeIp:includeIpData){
				if(nameservers.contains(includeIp)){
					loginFlag=true;
					break;
				}
			}
			if((systemDefault.getExcludeIPs()!=null && !systemDefault.getExcludeIPs().equals("")) && (!loginFlag)){
			sessionExp="true";
			ServletContext sc =getRequest().getSession(true).getServletContext();
    		sc.setAttribute("restrictLoginMsg", company.getRestrictedMassage());
			getRequest().getSession().setAttribute("sessionExp", sessionExp);
			return "rajeshlogin";
			}else getRequest().getSession().removeAttribute(sessionExp);
		}
	        }
		}catch(Exception ex){
			ex.printStackTrace();
		}
		decorator = "pwdreset";
		List newsUpdatList = newsUpdateManager.findUpdatedDate(sessionCorpID);
		imageSelected="Normal";
		if (newsUpdatList.isEmpty()) {

			getRequest().getSession().setAttribute("imageSelected", "Normal");
			imageSelected = "Normal";
		} else {
			getRequest().getSession().setAttribute("imageSelected", "Updated");
			imageSelected = "Updated";
		}
		List userCompanyList=customerFileManager.findCompanyName(sessionCorpID);
		if(userCompanyList !=null && !(userCompanyList.isEmpty())){
			Iterator it=userCompanyList.iterator();
			while(it.hasNext()){
				Object[] object = (Object[]) it.next();
				if(object[0]!=null ){
				companyNameString=object[0].toString();	
				}
				if(object[1]!=null){
				landingPageWelcomeMassage=object[1].toString();	
				}
			}
		}
		getRequest().getSession().setAttribute("globalSessionCorp",user.getCorpID());
		globalSessionCorp = getRequest().getSession().getAttribute("globalSessionCorp").toString();
		getRequest().setAttribute("soLastName","");
		soLastName=getRequest().getAttribute("soLastName").toString();
		String permKey = sessionCorpID +"-"+"module.tab.serviceorder.newAccountingTab";
        visibilityFornewAccountline=AppInitServlet.roleBasedComponentPerms.containsKey(permKey);
        if(visibilityFornewAccountline && user.getDefaultSoURL()!=null && user.getDefaultSoURL().toString().equalsIgnoreCase("accountLineList.html?sid=")){
        	getRequest().getSession().setAttribute("soTab", "pricingList.html?sid=");
    		soTab="pricingList.html?sid=";
        }else{
		getRequest().getSession().setAttribute("soTab", user.getDefaultSoURL());
		soTab=user.getDefaultSoURL();
        }
		getRequest().getSession().setAttribute("userPortalCheckType", user.getUserType());
		userPortalCheckType=user.getUserType(); 
		getRequest().getSession().setAttribute("soSortOrder", user.getDefaultSortForJob());
		soSortOrder=user.getDefaultSortForJob();
		getRequest().getSession().setAttribute("csoSortOrder", user.getDefaultSortForCso());
		csoSortOrder=user.getDefaultSortForCso();
		getRequest().getSession().setAttribute("ticketSortOrder", user.getDefaultSortForTicket());
		ticketSortOrder=user.getDefaultSortForTicket();
		getRequest().getSession().setAttribute("orderForJob", user.getSortOrderForJob());
		orderForJob=user.getSortOrderForJob();
		getRequest().getSession().setAttribute("orderForCso", user.getSortOrderForCso());
		orderForCso=user.getSortOrderForCso();
		getRequest().getSession().setAttribute("orderForTicket", user.getSortOrderTicket());
		orderForTicket=user.getSortOrderTicket();
		getRequest().getSession().setAttribute("defaultCss", user.getDefaultCss());
		getRequest().getSession().setAttribute("newsUpdateFlag", user.isNewsUpdateFlag());
		if(user.getDefaultCss()!=null && (!(user.getDefaultCss().equalsIgnoreCase("")))){
			defaultCss=user.getDefaultCss();
		}else{
			defaultCss="blue";
		} 
		if (user.isPasswordReset()) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		} else {
			try{
				userlogfileSave(user);
			}
			catch(Exception e)
			{
				 e.printStackTrace();	
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		}
	}
//	  End of Method.

	public void userlogfileSave(User user) throws UnknownHostException{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		user =this.user;
		String dnsclient="";
	   
		 try{
			List nameservers = sun.net.dns.ResolverConfiguration.open().nameservers();
			for( Object dns : nameservers ) {
				dnsclient=dns.toString();
			}
		}catch(Exception e){
			 e.printStackTrace();
		 }
		String IPaddress=getRequest().getRemoteAddr(); 
		Userlogfile userlogfile=new Userlogfile();
		userlogfile.setUserName(user.getUsername());
		userlogfile.setUserType(user.getUserType());
		userlogfile.setCorpID(user.getCorpID()); 
		userlogfile.setIPaddress(IPaddress);
		userlogfile.setDNS(dnsclient);
		userlogfile.setLogin(new Date());
		userlogfile.setSessionId(getSession().getId());
		userlogfile = userlogfileManager.save(userlogfile);  
		getSession().getCreationTime();
		user.getUsername(); 
		
		try{
			String deviceIdVal="";
			Cookie[] cookies = getRequest().getCookies();
		        if (cookies != null) {
		            for (Cookie cookie : cookies) {
		            	if (cookie.getName().equals("d_id")) {
		            		deviceIdVal = cookie.getValue();
		            	}
		            }
		        }
		    if(!deviceIdVal.equalsIgnoreCase("")){
		    	userDeviceList = userDeviceManager.findRecordByLoginUser(user.getUsername(), deviceIdVal);
		    }
			if(userDeviceList==null || userDeviceList.isEmpty()){
				String uuid = UUID.randomUUID().toString();
				Cookie dIdCookie = new Cookie("d_id", uuid);
				String contextPath = getRequest().getContextPath();
				dIdCookie.setPath(contextPath != null && contextPath.length() > 0 ? contextPath : "/");
				dIdCookie.setMaxAge(60 * 60 * 24 * 365 * 1);
				getResponse().addCookie(dIdCookie);
				
				UserDevice usrDevice = new UserDevice();
				usrDevice.setUserName(user.getUsername());
				usrDevice.setDeviceId(uuid);
				usrDevice.setCreatedOn(new Date());
				usrDevice.setUserLogFileId(userlogfile.getId());
				usrDevice.setCorpID(user.getCorpID());
				userDeviceManager.save(usrDevice);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	public String  findUserLogOut(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		if (getRequest().getSession(false) != null) {
			int i=userlogfileManager.updateUserLogOut(getSession().getId());
			getSession().invalidate();
		}
		Cookie terminate = new Cookie(TokenBasedRememberMeServices.ACEGI_SECURITY_HASHED_REMEMBER_ME_COOKIE_KEY, null);
		String contextPath = getRequest().getContextPath();
		terminate.setPath(contextPath != null && contextPath.length() > 0 ? contextPath : "/");
		terminate.setMaxAge(0);
		getResponse().addCookie(terminate);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
//	 Method used in login form  for forced password reset calling from main menu.
	public String resetPassword() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		user = (User) claimManager.findUserDetails(getRequest().getRemoteUser()).get(0);
		String oldPassword = user.getPassword();
		Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);
		if ("true".equals(getRequest().getParameter("encryptPass"))	&& (encrypt != null && encrypt)) {
			String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);
			if (algorithm == null) {
				log.debug("assuming testcase, setting algorithm to 'SHA'");
				algorithm = "SHA";
			}
			newPwd = StringUtil.encodePassword(passwordNew, algorithm);
			if(newPwd.equalsIgnoreCase(oldPassword)){
				errorMessage("New Password can't be same as old password, please select a new password!");
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
			newConPwd = newPwd;
		}
		
		claimManager.resetPassword(newPwd, newConPwd, pwdHintQues, pwdHint,	getRequest().getRemoteUser());
		try{
		company= companyManager.findByCorpID(user.getCorpID()).get(0);
		if((user.getUserType().toString().trim().equalsIgnoreCase("ACCOUNT"))  || (user.getUserType().toString().trim().equalsIgnoreCase("AGENT")) || (user.getUserType().toString().trim().equalsIgnoreCase("PARTNER")))
        {
			Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));
        	userManager.updatePasswordExpiryDate(user.getUsername(), sessionCorpID, compareWithDate);
        }else{
        	Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getUserPasswordExpiryDuration().toString()));
        	userManager.updatePasswordExpiryDate(user.getUsername(), sessionCorpID, compareWithDate);
        }
		}catch(Exception ex)
		{
			 ex.printStackTrace();	
		}}
		catch(Exception e)
		{e.printStackTrace();}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	private java.util.Date getCurrentDate() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dfm.format(new Date());
		Date currentDate = null;
		try {
			currentDate = dfm.parse(dateStr);
			
		} catch (java.text.ParseException e) {
			
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return currentDate;
		
	}
//	  End of Method.

//	 Method used in login form  for password hint calling from main menu.
	public String findPwordHQ() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		user = (User) claimManager.findPwordHintQ(user.getPasswordHintQues(), getRequest().getRemoteUser()).get(0);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
//	  End of Method.

//	 Method used in login form  for forgot password calling from main menu.
	public String forgotPassword() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		claimManager.forgotPassword(pwdHintQues, pwdHint, getRequest().getRemoteUser());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
//	  End of Method.

//	 Method used in to reset alert when pwdexpiryDate is 10 days greater then now  
	public String resetAlertPassword(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	}

//	 Method used for reset password when pwdexpiryDate is 10 days greater then now  
	public String submitResetAlertPassword(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try
		{
		user = (User) claimManager.findUserDetails(getRequest().getRemoteUser()).get(0);
		String oldPassword = user.getPassword();
		Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);
		if ("true".equals(getRequest().getParameter("encryptPass"))	&& (encrypt != null && encrypt)) {
			String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);
			if (algorithm == null) {
				log.debug("assuming testcase, setting algorithm to 'SHA'");
				algorithm = "SHA";
			}
			newPwd = StringUtil.encodePassword(passwordNew, algorithm);
			if(newPwd.equalsIgnoreCase(oldPassword)){
				hitFlag="22";
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
			newConPwd = newPwd;
		}
		claimManager.resetPassword(newPwd, newConPwd, user.getPasswordHintQues(), user.getPasswordHint(),	getRequest().getRemoteUser());
		try{
			company= companyManager.findByCorpID(user.getCorpID()).get(0);
			if((user.getUserType().toString().trim().equalsIgnoreCase("ACCOUNT"))  || (user.getUserType().toString().trim().equalsIgnoreCase("AGENT")) || (user.getUserType().toString().trim().equalsIgnoreCase("PARTNER")))
	        {
				Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));
	        	userManager.updatePasswordExpiryDate(user.getUsername(), sessionCorpID, compareWithDate);
	        }else{
	        	Date compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getUserPasswordExpiryDuration().toString()));
	        	userManager.updatePasswordExpiryDate(user.getUsername(), sessionCorpID, compareWithDate);
	        }
			}catch(Exception ex)
			{
				 ex.printStackTrace();	
			}
			
		hitFlag="1";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	/*
	 * Method added for claim apps to mailing 
	 */
	
	
	public String sendClaimEmailForApp(){
		try
		{
		claim = claimManager.get(id);
		
		IntegrationLogInfo inloginfo= new IntegrationLogInfo();
	    inloginfo.setSource("RedSky");
	    inloginfo.setDestination("MoveCloud");
	    inloginfo.setIntegrationId(id.toString());
	    inloginfo.setSourceOrderNum(claim.getClaimNumber().toString());
	    inloginfo.setDestOrderNum("Claim");
	    inloginfo.setOperation("CREATE");
	    inloginfo.setOrderComplete("N");
	    inloginfo.setStatusCode("S");
	    inloginfo.setEffectiveDate(new Date());
	    inloginfo.setDetailedStatus("Ready For Sending");
	    inloginfo.setCreatedBy(getRequest().getRemoteUser());
	    inloginfo.setCreatedOn(new Date());
	    inloginfo.setUpdatedBy(getRequest().getRemoteUser());
	    inloginfo.setUpdatedOn(new Date());
	    inloginfo.setCorpID(sessionCorpID);
	    inloginfo =  integrationLogInfoManager.save(inloginfo);
	    claim.setFormSent(new Date());
	    claim.setCreatedBy(getRequest().getRemoteUser());
	    claim.setCreatedOn(new Date());
	    claim.setUpdatedBy(getRequest().getRemoteUser());
	    claim.setUpdatedOn(new Date());
	    claim.setMoveCloudStatus("Pending");
	    claim = claimManager.save(claim);
	    message="Y";
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/*
	 * End of Method
	 */
	
	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setLossManager(LossManager lossManager) {
		this.lossManager = lossManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setNewsUpdateManager(NewsUpdateManager newsUpdateManager) {
		this.newsUpdateManager = newsUpdateManager;
	}
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public Set getClaims() {
		return claims;
	}

	public Set getLosss() {
		return losss;
	}

	public void setLosss(Set losss) {
		this.losss = losss;
	}

	public Set getFindAllClaimList() {
		return findAllClaimList;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public Claim getClaim() {
		return claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public  Map<String, String> getPWD_HINT() {
		return PWD_HINT;
	}

	public Map<String, String> getState() {
		return state;
	}

	public Map<String, String> getDcountry() {
		return dcountry;
	}

	public  Map<String, String> getDstates() {
		return dstates;
	}

	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}

	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}

	public String getCountForClaimNotes() {
		return countForClaimNotes;
	}

	public void setCountForClaimNotes(String countForClaimNotes) {
		this.countForClaimNotes = countForClaimNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public String getCountClaimValuationNotes() {
		return countClaimValuationNotes;
	}

	public void setCountClaimValuationNotes(String countClaimValuationNotes) {
		this.countClaimValuationNotes = countClaimValuationNotes;
	}

	public String getConfirmPasswordNew() {
		return confirmPasswordNew;
	}

	public void setConfirmPasswordNew(String confirmPasswordNew) {
		this.confirmPasswordNew = confirmPasswordNew;
	}

	public String getPasswordNew() {
		return passwordNew;
	}

	public void setPasswordNew(String passwordNew) {
		this.passwordNew = passwordNew;
	}

	public boolean getPassreset() {
		return passreset;
	}

	public void setPassreset(boolean passreset) {
		this.passreset = passreset;
	}

	public String getPwdHint() {
		return pwdHint;
	}

	public void setPwdHint(String pwdHint) {
		this.pwdHint = pwdHint;
	}

	public String getPwdHintQues() {
		return pwdHintQues;
	}

	public void setPwdHintQues(String pwdHintQues) {
		this.pwdHintQues = pwdHintQues;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDecorator() {
		return decorator;
	}

	public void setDecorator(String decorator) {
		this.decorator = decorator;
	}

	public CustomerFile getCustomerFile() {
		return (customerFile!=null ?customerFile:customerFileManager.get(customerFileId));
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public ExtendedPaginatedList getClaimExt() {
		return claimExt;
	}

	public void setClaimExt(ExtendedPaginatedList claimExt) {
		this.claimExt = claimExt;
	}

	public  Miscellaneous getMiscellaneous() {
		return( miscellaneous !=null )?miscellaneous :miscellaneousManager.get(soId);
	}

	public  void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public  TrackingStatus getTrackingStatus() {
		return( trackingStatus!=null )?trackingStatus:trackingStatusManager.get(soId);
	}

	public  void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public String getImageSelected() {
		return imageSelected;
	}

	public void setImageSelected(String imageSelected) {
		this.imageSelected = imageSelected;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	/**
	 * @return the soTab
	 */
	public String getSoTab() {
		return soTab;
	}

	/**
	 * @param soTab the soTab to set
	 */
	public void setSoTab(String soTab) {
		this.soTab = soTab;
	}
	public String getDetailPage() {
		return detailPage;
	}
	public void setDetailPage(String detailPage) {
		this.detailPage = detailPage;
	}
	public Long getSoId() {
		return soId;
	}
	public void setSoId(Long soId) {
		this.soId = soId;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public Userlogfile getUserlogfile() {
		return userlogfile;
	}
	public void setUserlogfile(Userlogfile userlogfile) {
		this.userlogfile = userlogfile;
	}
	public void setUserlogfileManager(UserlogfileManager userlogfileManager) {
		this.userlogfileManager = userlogfileManager;
	}
	/**
	 * @return the soSortOrder
	 */
	public String getSoSortOrder() {
		return soSortOrder;
	}
	/**
	 * @param soSortOrder the soSortOrder to set
	 */
	public void setSoSortOrder(String soSortOrder) {
		this.soSortOrder = soSortOrder;
	}
	/**
	 * @return the csoSortOrder
	 */
	public String getCsoSortOrder() {
		return csoSortOrder;
	}
	/**
	 * @param csoSortOrder the csoSortOrder to set
	 */
	public void setCsoSortOrder(String csoSortOrder) {
		this.csoSortOrder = csoSortOrder;
	}
	/**
	 * @return the ticketSortOrder
	 */
	public String getTicketSortOrder() {
		return ticketSortOrder;
	}
	/**
	 * @param ticketSortOrder the ticketSortOrder to set
	 */
	public void setTicketSortOrder(String ticketSortOrder) {
		this.ticketSortOrder = ticketSortOrder;
	}
	/**
	 * @return the claimSortOrder
	 */
	public String getClaimSortOrder() {
		return claimSortOrder;
	}
	/**
	 * @param claimSortOrder the claimSortOrder to set
	 */
	public void setClaimSortOrder(String claimSortOrder) {
		this.claimSortOrder = claimSortOrder;
	}
	/**
	 * @return the orderForClaim
	 */
	public String getOrderForClaim() {
		return orderForClaim;
	}
	/**
	 * @param orderForClaim the orderForClaim to set
	 */
	public void setOrderForClaim(String orderForClaim) {
		this.orderForClaim = orderForClaim;
	}
	/**
	 * @return the orderForCso
	 */
	public String getOrderForCso() {
		return orderForCso;
	}
	/**
	 * @param orderForCso the orderForCso to set
	 */
	public void setOrderForCso(String orderForCso) {
		this.orderForCso = orderForCso;
	}
	/**
	 * @return the orderForJob
	 */
	public String getOrderForJob() {
		return orderForJob;
	}
	/**
	 * @param orderForJob the orderForJob to set
	 */
	public void setOrderForJob(String orderForJob) {
		this.orderForJob = orderForJob;
	}
	/**
	 * @return the orderForTicket
	 */
	public String getOrderForTicket() {
		return orderForTicket;
	}
	/**
	 * @param orderForTicket the orderForTicket to set
	 */
	public void setOrderForTicket(String orderForTicket) {
		this.orderForTicket = orderForTicket;
	}
	public String getCountShip() {
		return countShip;
	}
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}
	public String getMinShip() {
		return minShip;
	}
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}
	public String getShipSize() {
		return shipSize;
	}
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}
	public List getCustomerSO() {
		return customerSO;
	}
	public void setCustomerSO(List customerSO) {
		this.customerSO = customerSO;
	}
	/**
	 * @return the companyNameString
	 */
	public String getCompanyNameString() {
		return companyNameString;
	}
	/**
	 * @param companyNameString the companyNameString to set
	 */
	public void setCompanyNameString(String companyNameString) {
		this.companyNameString = companyNameString;
	}

	public String getDefaultCss() {
		return defaultCss;
	}
	public void setDefaultCss(String defaultCss) {
		this.defaultCss = defaultCss;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public String getSessionExp() {
		return sessionExp;
	}
	public void setSessionExp(String sessionExp) {
		this.sessionExp = sessionExp;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public Long getCustomerFileId() {
		return customerFileId;
	}
	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	public Map<String, String> getClaimDeductible() {
		return claimDeductible;
	}
	public void setClaimDeductible(Map<String, String> claimDeductible) {
		this.claimDeductible = claimDeductible;
	}
	/**
	 * @return the currencySign
	 */
	public String getCurrencySign() {
		return currencySign;
	}
	/**
	 * @param currencySign the currencySign to set
	 */
	public void setCurrencySign(String currencySign) {
		this.currencySign = currencySign;
	}
	public Map<String, String> getDeductibleInsurer() {
		return DeductibleInsurer;
	}
	public void setDeductibleInsurer(Map<String, String> deductibleInsurer) {
		DeductibleInsurer = deductibleInsurer;
	}
	public String getUserPortalCheckType() {
		return userPortalCheckType;
	}
	public void setUserPortalCheckType(String userPortalCheckType) {
		this.userPortalCheckType = userPortalCheckType;
	}
	
	public Map<String,String> getClaimPersonList() {
		return claimPersonList;
	}
	public void setClaimPersonList(Map<String,String> claimPerson) {
		this.claimPersonList = claimPerson;
	}
	public Map<String, String> getCurrency() {
		return currency;
	}
	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}
	public RefJobType getRefJobType() {
		return refJobType;
	}
	public void setRefJobType(RefJobType refJobType) {
		this.refJobType = refJobType;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public List getClaimResponsiblePerson() {
		return claimResponsiblePerson;
	}
	public void setClaimResponsiblePerson(List claimResponsiblePerson) {
		this.claimResponsiblePerson = claimResponsiblePerson;
	}
	public String getcDivision() {
		return cDivision;
	}
	public void setcDivision(String cDivision) {
		this.cDivision = cDivision;
	}
	public List getClaimResponsiblePersonList() {
		return claimResponsiblePersonList;
	}
	public void setClaimResponsiblePersonList(List claimResponsiblePersonList) {
		this.claimResponsiblePersonList = claimResponsiblePersonList;
	}
	public Long getSid() {
		return sid;
	}
	public Map<String, String> getClaimStatus() {
		return claimStatus;
	}
	public void setClaimStatus(Map<String, String> claimStatus) {
		this.claimStatus = claimStatus;
	}
	public String getEnbState() {
		return enbState;
	}
	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}
	public Map<String, String> getCountryCod() {
		return countryCod;
	}
	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}
	public String getSoLastName() {
		return soLastName;
	}
	public void setSoLastName(String soLastName) {
		this.soLastName = soLastName;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public Long getAutoId() {
		return autoId;
	}
	public void setAutoId(Long autoId) {
		this.autoId = autoId;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public String getRedirect() {
		return redirect;
	}
	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}
	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}
	public List getAccountImageList() {
		return accountImageList;
	}
	public void setAccountImageList(List accountImageList) {
		this.accountImageList = accountImageList;
	}
	public String getBasedAt() {
		return basedAt;
	}
	public void setBasedAt(String basedAt) {
		this.basedAt = basedAt;
	}
	public PartnerPublic getPartnerPub() {
		return partnerPub;
	}
	public void setPartnerPub(PartnerPublic partnerPub) {
		this.partnerPub = partnerPub;
	}
	public String getPartnerDetailLocation1() {
		return partnerDetailLocation1;
	}
	public void setPartnerDetailLocation1(String partnerDetailLocation1) {
		this.partnerDetailLocation1 = partnerDetailLocation1;
	}
	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}
	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}
	public String getVanLineAffiliation() {
		return vanLineAffiliation;
	}
	public void setVanLineAffiliation(String vanLineAffiliation) {
		this.vanLineAffiliation = vanLineAffiliation;
	}
	public String getUtsNumber() {
		return utsNumber;
	}
	public void setUtsNumber(String utsNumber) {
		this.utsNumber = utsNumber;
	}
	  public String getDefaultVal() {
			return defaultVal;
		}
		public void setDefaultVal(String defaultVal) {
			this.defaultVal = defaultVal;
		}
		public String getStart() {
			return start;
		}
		public void setStart(String start) {
			this.start = start;
		}
		public String getCalId() {
			return calId;
		}
		public void setCalId(String calId) {
			this.calId = calId;
		}
		public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
			this.systemDefaultManager = systemDefaultManager;
		}
		public SystemDefault getSystemDefault() {
			return systemDefault;
		}
		public void setSystemDefault(SystemDefault systemDefault) {
			this.systemDefault = systemDefault;
		}
		public boolean isCportalAccessCompanyDivisionLevel() {
			return cportalAccessCompanyDivisionLevel;
		}
		public void setCportalAccessCompanyDivisionLevel(
				boolean cportalAccessCompanyDivisionLevel) {
			this.cportalAccessCompanyDivisionLevel = cportalAccessCompanyDivisionLevel;
		}
		public String getPartnerCompanyDivision() {
			return partnerCompanyDivision;
		}
		public void setPartnerCompanyDivision(String partnerCompanyDivision) {
			this.partnerCompanyDivision = partnerCompanyDivision;
		}

		public String getLandingPageWelcomeMassage() {
			return landingPageWelcomeMassage;
		}
		public void setLandingPageWelcomeMassage(String landingPageWelcomeMassage) {
			this.landingPageWelcomeMassage = landingPageWelcomeMassage;
		}
		public Boolean getSurveyTab() {
			return surveyTab;
		}
		public void setSurveyTab(Boolean surveyTab) {
			this.surveyTab = surveyTab;
		}
		public String getUsertype() {
			return usertype;
		}
		public void setUsertype(String usertype) {
			this.usertype = usertype;
		}
		public void setReportAction(ReportAction reportAction) {
			this.reportAction = reportAction;
		}
		public Map<String, String> getClaimTypeList() {
			return claimTypeList;
		}
		public void setClaimTypeList(Map<String, String> claimTypeList) {
			this.claimTypeList = claimTypeList;
		}
		public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
			this.emailSetupManager = emailSetupManager;
		}
		public Map<String, String> getLocation() {
			return location;
		}
		public void setLocation(Map<String, String> location) {
			this.location = location;
		}
		public Map<String, String> getFactor() {
			return factor;
		}
		public void setFactor(Map<String, String> factor) {
			this.factor = factor;
		}
		public Map<String, String> getOccurence() {
			return occurence;
		}
		public void setOccurence(Map<String, String> occurence) {
			this.occurence = occurence;
		}
		public boolean isResponsibleAgent() {
			return responsibleAgent;
		}
		public void setResponsibleAgent(boolean responsibleAgent) {
			this.responsibleAgent = responsibleAgent;
		}
		public boolean isRoleSecurityAdmin() {
			return roleSecurityAdmin;
		}
		public void setRoleSecurityAdmin(boolean roleSecurityAdmin) {
			this.roleSecurityAdmin = roleSecurityAdmin;
		}
		public String getMsgClicked() {
			return msgClicked;
		}
		public void setMsgClicked(String msgClicked) {
			this.msgClicked = msgClicked;
		}
		public Map<String, String> getClaimVal() {
			return claimVal;
		}
		public void setClaimVal(Map<String, String> claimVal) {
			this.claimVal = claimVal;
		}
		public boolean isRoleEmployee() {
			return roleEmployee;
		}
		public void setRoleEmployee(boolean roleEmployee) {
			this.roleEmployee = roleEmployee;
		}
		public UserDevice getUserDevice() {
			return userDevice;
		}
		public void setUserDevice(UserDevice userDevice) {
			this.userDevice = userDevice;
		}
		public void setUserDeviceManager(UserDeviceManager userDeviceManager) {
			this.userDeviceManager = userDeviceManager;
		}
		public List getUserDeviceList() {
			return userDeviceList;
		}
		public void setUserDeviceList(List userDeviceList) {
			this.userDeviceList = userDeviceList;
		}
		public String getOiJobList() {
			return oiJobList;
		}
		public void setOiJobList(String oiJobList) {
			this.oiJobList = oiJobList;
		}
		public int getMoveCount() {
			return moveCount;
		}
		public void setMoveCount(int moveCount) {
			this.moveCount = moveCount;
		}
		public String getEmailSetupValue() {
			return emailSetupValue;
		}
		public void setEmailSetupValue(String emailSetupValue) {
			this.emailSetupValue = emailSetupValue;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public Map<String, String> getCountry() {
			return country;
		}
		public void setCountry(Map<String, String> country) {
			this.country = country;
		}
		public ExtendedPaginatedList getClaimsExt() {
			return claimsExt;
		}
		public void setClaimsExt(ExtendedPaginatedList claimsExt) {
			this.claimsExt = claimsExt;
		}
		public void setIntegrationLogInfoManager(IntegrationLogInfoManager integrationLogInfoManager) {
			this.integrationLogInfoManager = integrationLogInfoManager;
		}
		public String getDashBoardHideJobsList() {
			return dashBoardHideJobsList;
		}
		public void setDashBoardHideJobsList(String dashBoardHideJobsList) {
			this.dashBoardHideJobsList = dashBoardHideJobsList;
		}

}
