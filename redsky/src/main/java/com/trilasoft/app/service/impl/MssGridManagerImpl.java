package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.MssGridDao;
import com.trilasoft.app.model.MssGrid;
import com.trilasoft.app.service.MssGridManager;

public class MssGridManagerImpl extends GenericManagerImpl<MssGrid, Long> implements MssGridManager{
	MssGridDao mssGridDao;
	public MssGridManagerImpl(MssGridDao mssGridDao) {
		super(mssGridDao);
		this.mssGridDao = mssGridDao;
	}
	
	public List findRecord(Long id, String sessionCorpID){
		return mssGridDao.findRecord(id,sessionCorpID);
	}
	public void deleteDataOfInventoryData(String inventoryFlag){
		mssGridDao.deleteDataOfInventoryData(inventoryFlag);
	}
}
