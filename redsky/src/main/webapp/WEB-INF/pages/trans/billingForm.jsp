<%@ include file="/common/taglibs.jsp"%>   
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.List"%>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<%@page import="java.util.Iterator"%>
<head>   
    <title><fmt:message key="billingDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='billingDetail.heading'/>"/>   
<style>
<%@ include file="/common/calenderStyle.css"%> 
</style>
<style type="text/css">
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}

#loader {filter:alpha(opacity=70);-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.mainDetailTable { width:742px;}
.noteShow {display:inline;position:absolute;padding:8px 10px 10px 80px;float:right; }
</style>

<script type="text/javascript">
function showPartnerAlert(display, code, position){
    if(code == ''){ 	
    	document.getElementById(position).style.display="none";
    	ajax_hideTooltip();
    }else if(code != ''){
    	getBAPartner(display,code,position);
    }      
}
var httpPA = getHTTPObjectPA();
function getHTTPObjectPA(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getBAPartner(display,code,position){
    var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
     httpPA.open("GET", url, true);
     httpPA.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
     httpPA.send(null);
}
function handleHttpResponsePartner(display,code,position){
    if (httpPA.readyState == 4){
        var results = httpPA.responseText
        results = results.trim();
        if(results.length > 565){
            document.getElementById(position).style.display="block";
            if(display != 'onload'){
                getPartnerAlert(code,position);
            }
        }else{
            document.getElementById(position).style.display="none";
            ajax_hideTooltip();
        }    }}


function viewPartnerDetailsForBillToCode(position) {
    var partnerCode = "";
    var originalCorpID='${billing.corpID}';
    partnerCode = document.getElementById("billingBillToCodeId").value
    var url="viewPartnerDetailsForBillTo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&sessionCorpID=" + encodeURI(originalCorpID);
    ajax_showTooltip(url,position);
    return false
}

</script>

</head>  
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" id="noteFor" value="ServiceOrder"></s:hidden>
<s:form id="billingForm" name="billingForm" action="saveBilling.html?&btntype=yes" method="post" validate="true" onsubmit="return validateBillToCode();">

<c:set var="serviceOrderLastName" value="${fn:escapeXml(serviceOrder.lastName)}" />
<c:set var="readableChargeCode" value="true" />
<configByCorp:fieldVisibility componentId="component.billing.readable.chargeCode">		
		<c:set var="readableChargeCode" value="false" />
</configByCorp:fieldVisibility>
<s:hidden name="chargeCodeValidationVal" id="chargeCodeValidationVal"/>  
<s:hidden name="billingDMMContractTypeCheck" value="${billingDMMContractType}"/>
<s:hidden name="oldBillingNetworkBillToCode" value="${billing.networkBillToCode}"/>
<configByCorp:fieldVisibility componentId="component.billing.issueDate.mandatory">
<s:hidden name="isIssueDateRequired" value="YES"/>
</configByCorp:fieldVisibility>
<s:hidden name="billing.fXRateOnActualizationDate"/>
<s:hidden name="serviceOrder.companyDivision" value="%{serviceOrder.companyDivision}"/> 
 <s:hidden name="isSOExtract"/>
<s:hidden name="billingBillCompleteVar" id="billingBillCompleteVar"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
</configByCorp:fieldVisibility>
<s:hidden name="invoiceCount" value="${invoiceCount}"/>
<configByCorp:fieldVisibility componentId="component.field.autoupdate.contract">
	<s:hidden name="autoupdateContractFlag" value="YES"/>
	<s:hidden name="autoupdateContract" value="${billing.contract}"/>
	</configByCorp:fieldVisibility>
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<s:hidden name="billing.actgCode"/>
<s:hidden name="billing.id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="billing.ugwIntId" />
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("id")%>"/>
<s:hidden name="billing.shipNumber" value="%{serviceOrder.shipNumber}"/> 
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/> 
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>
<s:hidden name="billing.imfJob"/>
<s:hidden name="billing.isCreditCard"/>
<s:hidden name="isCreditCard" value="${billing.id}"/>
<s:hidden name="comdit" />
<s:hidden name="authNum" />
<s:hidden name="shipNum" />
<s:hidden name="serviceOrder.bookingAgentCode" value="<%=request.getParameter("serviceOrder.bookingAgentCode")%>"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="validateFormNav" />
<s:hidden name="serviceOrder.serviceType"/>
<s:hidden name="accountSearchValidation"/>
<configByCorp:fieldVisibility componentId="component.field.Alternative.AdditionalNoShow">
<s:hidden name="additionalNoFlag" value="Y"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.SO.Extarct">
	<s:hidden name="isDecaExtract" value="1"/>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.auditCompleteDateForUTSI">		
		<c:set var="auditCompleteDate" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="indicatorFieldsView" value="N" />
<configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">		
	<c:set var="indicatorFieldsView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="checkPropertyAmountComponent" value="N" />
<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
	<c:set var="checkPropertyAmountComponent" value="Y" />
</configByCorp:fieldVisibility>
<s:hidden name="msgClicked" />
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
<%
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = (User)auth.getPrincipal();
    	List avaliableRoles=user.getRoleList();
    	Iterator it=avaliableRoles.iterator();
    	int index=-1;
    	while(it.hasNext()){
    		String str=it.next().toString();
    		index=str.lastIndexOf("ROLE_AUDITOR");
    		if(index!=-1){
    		break;
    		}
    	}
%>

<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   <c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/accountLineList.html?sid=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.newAccounting' }">
	<c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.forwarding' }">
<c:if test="${forwardingTabVal!='Y'}">
	<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
</c:if>
<c:if test="${forwardingTabVal=='Y'}">
	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   <c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   <c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
	<c:if test="${serviceOrder.job =='RLO'}"> 
		 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
	</c:if>
         <c:if test="${serviceOrder.job !='RLO'}"> 
		<c:if test="${checkPropertyAmountComponent!='Y'}">
		   <c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
		</c:if>
		<c:if test="${checkPropertyAmountComponent=='Y'}">
			<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
		</c:if>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.ticket' }">
	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   <c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}" />
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   <c:redirect url="/claims.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/claims.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
	<c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.creditcard' }">
	<c:if test="${billing.isCreditCard == '1'}" >
		<c:redirect url="/creditCards.html?id=${serviceOrder.id}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.reports' }">
	<c:redirect url="/subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&reportModule=serviceOrder&reportSubModule=Billing"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.OI' }">
	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   <c:redirect url="/operationResource.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/operationResource.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:when>

<c:when test="${gotoPageString == 'gototab.criticaldate' }">
	<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
		<c:redirect url="/soAdditionalDateDetails.html?sid=${serviceOrder.id}" />
	</sec-auth:authComponent>
</c:when>	
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<s:hidden id="countBillingPrimaryNotes" name="countBillingPrimaryNotes" value="<%=request.getParameter("countBillingPrimaryNotes") %>"/>
<s:hidden id="countBillingSecondaryNotes" name="countBillingSecondaryNotes" value="<%=request.getParameter("countBillingSecondaryNotes") %>"/>
<s:hidden id="countBillingPrivatePartyNotes" name="countBillingPrivatePartyNotes" value="<%=request.getParameter("countBillingPrivatePartyNotes") %>"/>
<s:hidden id="countBillingDomesticNotes" name="countBillingDomesticNotes" value="<%=request.getParameter("countBillingDomesticNotes") %>"/>
<s:hidden id="countBillingValuationNotes" name="countBillingValuationNotes" value="<%=request.getParameter("countBillingValuationNotes") %>"/>
<s:hidden id="countBillingAuthorizedNotes" name="countBillingAuthorizedNotes" value="<%=request.getParameter("countBillingAuthorizedNotes") %>"/>
<s:hidden id="countBillingCompleteStatusNotes" name="countBillingCompleteStatusNotes" value="<%=request.getParameter("countBillingCompleteStatusNotes") %>"/>
<c:set var="countBillingPrimaryNotes" value="<%=request.getParameter("countBillingPrimaryNotes") %>" />
<c:set var="countBillingSecondaryNotes" value="<%=request.getParameter("countBillingSecondaryNotes") %>" />
<c:set var="countBillingPrivatePartyNotes" value="<%=request.getParameter("countBillingPrivatePartyNotes") %>" />
<c:set var="countBillingDomesticNotes" value="<%=request.getParameter("countBillingDomesticNotes") %>" />
<c:set var="countBillingValuationNotes" value="<%=request.getParameter("countBillingValuationNotes") %>" />
<c:set var="countBillingAuthorizedNotes" value="<%=request.getParameter("countBillingAuthorizedNotes") %>" />
<c:set var="countBillingCompleteStatusNotes" value="<%=request.getParameter("countBillingCompleteStatusNotes") %>" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
		<div id="Layer1"  style="width:100%;">
		<div id="newmnav" style="float: left;"> 
		    <ul>
		    <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
            <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
            <c:set var="relocationServicesKey" value="" />
            <c:set var="relocationServicesValue" value="" /> 
            <c:forEach var="entry" items="${relocationServices}">
            <c:if test="${relocationServicesKey==''}">
            <c:if test="${entry.key==serviceOrder.serviceType}">
            <c:set var="relocationServicesKey" value="${entry.key}" />
            <c:set var="relocationServicesValue" value="${entry.value}" /> 
            </c:if>
           </c:if> 
           </c:forEach>
           

	        <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	            <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	             <c:if test="${(fn:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
             <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
         		<li><a  href="redskyDashboard.html?sid=${serviceOrder.id}" ><span>Dashboard</span></a></li>
		      </c:if>
		      </c:if>
		      </sec-auth:authComponent>
		      </configByCorp:fieldVisibility>
            <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		      <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.serviceorder');return autoSaveFunc('none');"><span>S/O Details</span></a></li>
		      </c:if>
		       <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		      <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.serviceorder');return autoSaveFunc('none');"><span>Quotes</span></a></li>
		      </c:if>
			  <li  id="newmnav1" style="background:#FFF"><a class="current"><span>Billing</span></a></li>
			  <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
			 <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <c:choose>
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			   <c:otherwise> 
		       <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.accounting');return autoSaveFunc('none');"><span>Accounting</span></a></li>
		      </c:otherwise>
		     </c:choose>
		     </sec-auth:authComponent> 
		     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		     <c:choose> 
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			   <c:otherwise> 
		       <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.newAccounting');return autoSaveFunc('none');"><span>Accounting</span></a></li>
		      </c:otherwise>
		     </c:choose>	
		     </sec-auth:authComponent>
		     <%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
		      <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
		      <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.OI');return autoSaveFunc('none');"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
		     <c:if test="${serviceOrder.job !='RLO'}">
		    <c:if test="${serviceOrder.corpID!='CWMS' || (fn:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 	
			  	<li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.forwarding');return autoSaveFunc('none');"><span>Forwarding</span></a></li>
			  	</c:if>			 
			 </c:if>
			 </c:if>
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  <c:if test="${serviceOrder.job !='RLO'}"> 
			  <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.domestic');return autoSaveFunc('none');"><span>Domestic</span></a></li>
			  </c:if>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                <c:if test="${serviceOrder.job =='INT'}">
                <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.domestic');return autoSaveFunc('none');"><span>Domestic</span></a></li>
                </c:if>
                </sec-auth:authComponent>
			  <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.status');return autoSaveFunc('none');"><span>Status</span></a></li>
			  <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
			  <c:if test="${serviceOrder.job !='RLO'}"> 
			  <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.ticket');return autoSaveFunc('none');"><span>Ticket</span></a></li>
			 <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.claims');return autoSaveFunc('none');"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			  </c:if>
			  </c:if>
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
	<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
	<c:if test="${serviceOrder.job =='RLO'}"> 
		<li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.ticket');return autoSaveFunc('none');"><span>Ticket</span></a></li>
	</c:if>
	</c:if>
	</sec-auth:authComponent>
	<configByCorp:fieldVisibility componentId="component.standard.claimTab">	
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
	<c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
	<c:if test="${serviceOrder.job =='RLO'}"> 	
		<li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.claims');return autoSaveFunc('none');"><span>Claims</span></a></li>
	</c:if> 
	</c:if>
	</sec-auth:authComponent>
	</configByCorp:fieldVisibility>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
			 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
		 </configByCorp:fieldVisibility>
			</sec-auth:authComponent>
			  <li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.customerfile');return autoSaveFunc('none');"><span>Customer File</span></a></li>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
	 			<li><a onmouseover="checkHasValuation()" onclick="setReturnString('gototab.criticaldate');return autoSaveFunc('none');"><span>Critical Dates</span></a></li>
			</sec-auth:authComponent>
			  <li><a onmouseover="checkHasValuation()" onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Billing&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			  <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${billing.id}&tableName=billing&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			<c:if test="${usertype=='USER'}">
				<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
			  		<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
			  	</configByCorp:fieldVisibility>
		  	</c:if>
			</ul> 
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left" valign="top">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;!padding-bottom:7px;"></div>

<div id="Layer1" style="width:100%" onkeydown="changeStatus();">

 <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>

<div id="newmnav" style="!margin-bottom:5px;">   
 <ul>   
 <li id="newmnav1" style="background:#FFF"><a class="current"><span>Billing Info</span></a></li>
 <li><a onclick="setReturnString('gototab.creditcard'); goToCreditcard();"><span>Credit Card</span></a></li>
 </ul> 
</div><div class="spn">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%; margin:0px;padding:0px;">
	<tbody>
	<tr>
	<td colspan="2">
	
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;<fmt:message key="billing.billTo"/>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>	
	</td>
	</tr>
	<tr>
	<td height="10" align="left" class="listwhitetext" style="margin-bottom:0px">
		<table  class="detailTabLabel" border="0" style="margin-bottom:-2px; " width="">
 		  <tbody>
			<tr><td height="5px"></td></tr>
			<tr>
			<c:choose>
			   <c:when test="${hasDataSecuritySet}">
				<td align="right" width="120px">Bill&nbsp;To<font color="red" size="2">*</font></td><td align="left"><s:textfield   name="billing.billToCode" required="true" cssClass="input-text" cssStyle="width:55px;" maxlength="8"  onchange="changeStatus();valid(this,'special');fillBillToAuthority();billToCodeFieldValueCopy();getBillToCode();findPrimaryRecVatWithVatGroup();" readonly='true' tabindex=""/></td>
				<td align="left"><img class="openpopup" width="17" height="20" onclick="setBillToCode();document.forms['billingForm'].elements['billing.billToCode'].focus();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
				<td align="right" width="65px">Bill&nbsp;To&nbsp;Name</td>
				<td align="left" colspan=""><s:textfield name="billing.billToName"  onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" cssStyle="width:226px;" maxlength="250" readonly='true' tabindex="2"/>
				<img align="top" class="openpopup" width="17" height="20" title="View details" onclick="viewPartnerDetailsForBillToCode(this);" src="<c:url value='/images/address2.png'/>" />&nbsp;
				</td>
              
               </c:when>
		      <c:otherwise>
		      <td align="right" width="120px">Bill&nbsp;To<font color="red" size="2">*</font></td><td align="left"><s:textfield id="billingBillToCodeId" name="billing.billToCode"  required="true" cssClass="input-text" cssStyle="width:55px;" maxlength="8" onchange="changeStatus();valid(this,'special');fillBillToAuthority();billToCodeFieldValueCopy();getBillToCode();fillSellBuyRate();findBillingCycle();findPrimaryRecVatWithVatGroup();" tabindex=""/></td>
				<td align="left"><img class="openpopup" width="17" height="20" onclick="assignBillToCode();openPopWindow(this);document.forms['billingForm'].elements['billing.billToCode'].focus();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
				<td align="right" width="65px">Bill&nbsp;To&nbsp;Name</td>
				<td align="left" colspan="">
				 <c:if test="${accountSearchValidation}">
				<s:textfield name="billing.billToName" id="billingBillToNameId" onkeyup="findPartnerDetails('billingBillToNameId','billingBillToCodeId','BillToNameDivId',' and (isAccount=true or isPrivateParty=true )','${serviceOrder.companyDivision}',event);" onchange="findPartnerDetailsByName('billingBillToCodeId','billingBillToNameId');fillSellBuyRate();" cssClass="input-text" cssStyle="width:226px;" maxlength="250"  tabindex=""/>
				<div id="BillToNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
					</c:if>
					 <c:if test="${!accountSearchValidation}">
				<s:textfield name="billing.billToName" id="billingBillToNameId"  onkeyup="findPartnerDetails('billingBillToNameId','billingBillToCodeId','BillToNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true )','${serviceOrder.companyDivision}',event);" onchange="findPartnerDetailsByName('billingBillToCodeId','billingBillToNameId');fillSellBuyRate();" cssClass="input-text" cssStyle="width:226px;" maxlength="250"  tabindex=""/>
				<div id="BillToNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
					</c:if>
				<img align="top" class="openpopup" width="17" height="20" title="View details" onclick="viewPartnerDetailsForBillToCode(this);" src="<c:url value='/images/address2.png'/>" />&nbsp;
				</td>
		      </c:otherwise>
           </c:choose>
             	<td align="left" width="20">
					<div id="hidBBillTo" style="vertical-align:middle;"><a><img style="margin-left:-4px;" class="openpopup" onclick="getPartnerAlert(document.forms['billingForm'].elements['billing.billToCode'].value,'hidBBillTo');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
				</td>
				<script type="text/javascript">
					showPartnerAlert('onload','${billing.billToCode}','hidBBillTo');
				</script>
             <td>             
             </td>
             <c:if test="${serviceOrder.job !='RLO'}"> 
<td align="right" width="63"><fmt:message key="billing.revenueRecognition"/></td>
<c:if test="${not empty billing.revenueRecognition}">
<s:text id="billingRevenueRecognitionFormattedValue" name="${FormDateValue}"><s:param name="value" value="billing.revenueRecognition"/></s:text>
<td width="57px"><s:textfield cssClass="input-textUpper" id="revenueRecognition" name="billing.revenueRecognition" value="%{billingRevenueRecognitionFormattedValue}" onchange="changeStatus();" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex=""/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['billingForm'].revenueRecognition,'calender2',document.forms['billingForm'].dateFormat.value); return false;"/></td>--%>
</c:if>
<c:if test="${empty billing.revenueRecognition}">
<td width="57px"><s:textfield cssClass="input-textUpper" id="revenueRecognition" name="billing.revenueRecognition" onchange="changeStatus();" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex=""/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['billingForm'].revenueRecognition,'calender2',document.forms['billingForm'].dateFormat.value); return false;"/></td>--%>
</c:if>
</c:if>
<td align="left" class="listwhitetext" style="width:40px; text-align: right;">Email</td>
<td align="left"><s:textfield cssClass="input-text" name="billing.billingEmail" onblur="billingEmailImage();" onchange="changeStatus();" size="30" maxlength="65" cssStyle="width:205px" tabindex="" /></td>
<td align="left"  width="17" height="20" class="listwhitetext"> <div id="openpopupnetworkimg" style=""><img class="openpopup" onclick="getContact(document.forms['billingForm'].elements['billing.billToCode'].value);"  src="<c:url value='/images/plus-small.png'/>" /></td>
<td align="center" class="listwhitetext" style="width:25px"><div id="billingEmailImage" style="vertical-align:middle; "><img class="openpopup" onclick="sendEmail(document.forms['billingForm'].elements['billing.billingEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to:${billing.billingEmail} "/></td>	

			<c:if test="${serviceOrder.job =='RLO'}"> 
				<s:hidden name="billing.revenueRecognition"/>
			</c:if>
			</tr>
		  </tbody>
 		</table>
 		</td></tr>
 		<c:if test="${( (!(networkAgent)))}">
		<tr>
		<td align="left" id="showHideNetworkBillToCode" >
		<table  class="detailTabLabel" border="0" >
 		  <tbody>
			    <tr>
		        <td width="120" align="right" class="listwhitetext">Network&nbsp;Bill&nbsp;To</td>
				<td align="left" class="listwhitetext" nowrap="nowrap"><s:textfield  cssClass="input-text" name="billing.networkBillToCode" id="billingNetworkBillToCodeId" onselect="changeStatus();" onblur="changeStatus();" onchange="changeStatus();checkNetworkBillToName();" cssStyle="width:55px;" maxlength="17"  tabindex="26"/>
				<img align="top" id="billingNetworkBillToCodeOpenpopup"  class="openpopup" width="17" height="20" onclick="javascript:openWindow('findNetworkBillToCodeList.html?networkPartnerCode=${customerFile.accountCode}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.networkBillToName&fld_code=billing.networkBillToCode');document.forms['billingForm'].elements['billing.networkBillToCode'].select();" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" />
				 </td>
				<td width="" align="right" class="listwhitetext">Network&nbsp;Bill&nbsp;To&nbsp;Name</td>
				<td align="left" colspan="2" class="listwhitetext"><s:textfield cssClass="input-text" id="billingNetworkBillToNameId" name="billing.networkBillToName" onkeydown="return onlyCharsAllowed(event)" onkeyup="findNetworkPartnerDetails('billingNetworkBillToNameId','billingNetworkBillToCodeId','billingNetworkBillToNameDivId',' ','${customerFile.accountCode}',event);"  onchange="findPartnerDetailsByName('billingNetworkBillToCodeId','billingNetworkBillToNameId');"  cssStyle="width:191px"  tabindex="27" /></td>
			  <div id="billingNetworkBillToNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
			</tr>
			</tbody>
		</table>
		</td>
		</tr>
	   </c:if>
	<tr>
	<td height="10" align="left" class="listwhitetext" >
		<table  class="detailTabLabel" border="0" style="margin-top:-1px">
 		  <tbody>
			    <tr>
		    	<td align="right">Account&nbsp;PO#
		    	/SAP&nbsp;Number</td>
		    	<td align="left" ><s:textfield name="billing.billToAuthority" onfocus="check();" onchange="authUpdatedDate(); changeStatus();" cssStyle="width:228px" required="true" cssClass="input-text" maxlength="35" tabindex=""/></td>
		    	<td align="left" width="1">
				<div id="hid" style="margin:0px; float:left; display:inline;">				
				<input type="button" class="cssbutton" onclick= "return checkBillToAuthority();" value="<fmt:message key="button.authorization"/>" style="width:83px; height:21px" tabindex=""/>
				</div>
				</td>
				<td align="right" class="listwhitetext" width="69"><fmt:message key='billing.authUpdated'/></td>
		    	<c:if test="${not empty billing.authUpdated}">
		    	<s:text id="billingAuthUpdatedFormattedValue" name="${FormDateValue}"><s:param name="value" value="billing.authUpdated" /></s:text>
				<td align="left" width="75"><s:textfield cssClass="input-textUpper" name="billing.authUpdated" value="%{billingAuthUpdatedFormattedValue}" onchange="changeStatus();" cssStyle="width:65px;" maxlength="11" readonly="true"  tabindex=""/></td>
		    	</c:if>
		    	<c:if test="${empty billing.authUpdated}">
		    	<td align="left" width="75"><s:textfield cssClass="input-textUpper" name="billing.authUpdated" onchange="changeStatus();" cssStyle="width:61px;margin-left:4px;" maxlength="11" readonly="true" tabindex="" /></td>
		    	</c:if>
		    	<td align="right" class="listwhitetext" width="87">Remarks </td>
	       		<td width="52">
	       		<s:textfield name="billing.invoiceRemarks1" cssClass="input-text" id="invoiceRemarks1" maxlength="40"/>
	       		</td> 
		    	<td colspan="1" width="80" align="right" style="margin: 0px;">
		    	<table class="detailTabLabel" border="0" style="margin: 0px;">
		    	<tr>
		    	<c:if test="${billing.imfJob=='1'}">
			    	<td align="right"><fmt:message key="billing.matchShip"/></td>
			    	<td align="left" colspan="2"><s:textfield name="billing.matchShip" required="true" cssClass="input-text" size="7" onkeydown="return onlyAlphaNumericAllowed(event)" onchange="changeStatus();" maxlength="9" tabindex=""/></td>
				</c:if>
				<c:if test="${billing.imfJob!='1'}">
				<td align="right"></td>
				    <td align="left" colspan="2"><s:hidden name="billing.matchShip"/></td>
				</c:if>
				</tr>
				</table>
				</td>
				
<td colspan="2" align="right" style="margin: 0px;">
<div id="hid1" style="margin: 0px;">
<table class="detailTabLabel" border="0" style="margin: 0px;">
	<tbody>
		<tr>
				<td align="right">Auth&nbsp;#2</td>
		    	<td><s:textfield name="billing.authorizationNo2" onchange="changeStatus();" cssStyle="width:80px;" required="true" cssClass="input-text" maxlength="25" tabindex=""/>
		    	</td>
		  </tr>
		</tbody>
		</table>
		</div>
		</td>		    	
		    </tr>
		    <tr>
		    	<td align="right" width="120px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=billing.billToReference',this);return false" >Account&nbsp;Reference&nbsp;#
		    	/Cost&nbsp;Centre&nbsp;#</a></td>
		    	<td align="left" width="185px"><s:textfield name="billing.billToReference" onkeydown="return onlyAlphaNumericWithDot(event)" onchange="changeStatus();" size="30" cssStyle="width:228px" required="true" cssClass="input-text" maxlength="50" tabindex=""/></td>

		    	<td colspan="10" align="left" style="margin: 0px;">
		<table class="detailTabLabel" border="0" style="margin: 0px;">
				<tr>	
    <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">			    	

		    	<td align="right" width="76px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=billing.taExpires',this);return false" ><fmt:message key="billing.taExpires"/></a></td>
		    	<td align="left" width="63px">
				<c:if test="${not empty billing.taExpires}">
					<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.taExpires"/></s:text>
				    <s:textfield id="taExpires" name="billing.taExpires" value="%{FormatedInvoiceDate}" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:60px;" tabindex=""/></td>
				</c:if>
				<c:if test="${empty billing.taExpires}">
					<s:textfield id="taExpires" name="billing.taExpires" onkeydown="onlyDel(event,this);changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:60px;" tabindex=""/></td>
				</c:if>
				<td align="left" width="24px"><img id="taExpires-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>				
    				</configByCorp:fieldVisibility>
    				 <% String b1="false"; %>
    				<configByCorp:fieldVisibility componentId="component.field.Alternative.billName">	
    				<c:if test="${hasDataSecuritySet}">
    				 <% b1="true"; %>
    				</c:if>
    				</configByCorp:fieldVisibility>
				<td align="right" width="66px"><fmt:message key="billing.billingid"/></td>
				<td  align="left" colspan="" ><s:textfield cssClass="input-text" id="billingCodeId" name="billing.billingId" readonly="<%=b1 %>" onchange="changeStatus();findBillingIdName();" required="true" cssStyle="width:55px;" maxlength="25" tabindex=""/></td>
				<configByCorp:fieldVisibility componentId="component.field.Alternative.billName">	
				<td align="left"><img class="openpopup" width="17" height="20" onclick="setBillToCode1();document.forms['billingForm'].elements['billing.billingId'].focus();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
				<td align="right" width="65px">Billing&nbsp;Name</td>
				<td align="left" colspan=""><s:textfield name="billing.billName" id="billNameId"  onkeyup=" findPartnerDetails08('billNameId','billingCodeId','billingBillNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true or isVendor=true  )',event);" required="true" cssClass="input-text" cssStyle="width:159px;" maxlength="250"  tabindex=""/>
				<div id="billingBillNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				
				</configByCorp:fieldVisibility>
				<configByCorp:fieldVisibility componentId="component.field.Alternative.showForVocz">	
				<td align="right" class="listwhitetext" width="66px"><fmt:message key="billing.taxableOn"/></td>
				<td align="left" width="100px">		    	
		    	<c:if test="${not empty billing.taxableOn}">
					<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.taxableOn"/></s:text>
				    <s:textfield id="taxableOn" name="billing.taxableOn" value="%{FormatedInvoiceDate}" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:60px;" tabindex=""/>
				    <img id="taxableOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty billing.taxableOn}">
					<s:textfield id="taxableOn" name="billing.taxableOn" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:60px;" tabindex=""/>
					<img id="taxableOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				</configByCorp:fieldVisibility>	
				<configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">	
				<td align="right" width="63px">Billing&nbsp;ID&nbsp;Out</td>
				<td  align="left" colspan="" ><s:textfield cssClass="input-text" name="billing.billingIdOut"  onchange="authorizationNo2();changeStatus();" required="true" cssStyle="width:80px;" maxlength="25" tabindex=""/></td>
				</configByCorp:fieldVisibility>
				</tr>
				</table>
				</td>
				</tr>
			<tr>
			<c:set var="ischeckedValueHts" value="false"/>
					<c:if test="${billing.historicalContracts}">
						<c:set var="ischeckedValueHts" value="true"/>
					</c:if>
				<td align="right" width="120px"><fmt:message key="billing.contract"/><font color="red" size="2">*</font></td>
				<td  align="left" colspan="" ><s:select cssClass="list-menu" name="billing.contract" list="%{contractBillList}" cssStyle="width:230px"  headerKey="" headerValue="" onchange="findActualizationDateOfContract();findDMMTypeContract();findContractTypeByContract();changeStatus();" tabindex=""/></td>
				
				<td colspan="9" align="left" style="margin: 0px;">
		<table class="detailTabLabel" border="0" style="margin:0px;">
				<tr>
					<configByCorp:fieldVisibility componentId="component.tab.billing.contractSystem">
				<td align="right"><fmt:message key="billing.contractSystem"/><font color="red" size="2">*</font></td>
				<td  align="left"><configByCorp:customDropDown listType="map" list="${contractSystemList}" fieldValue="${billing.contractSystem}"
		       attribute="id=billingForm_billing_contractSystem class=list-menu name=billing.contractSystem  style=width:150px  headerKey='' headerValue='' tabindex='' "/></td>
					</configByCorp:fieldVisibility>
				<td align="right"><configByCorp:fieldVisibility componentId="component.field.Alternative.historicalContracts"><s:checkbox name="billing.historicalContracts" value="${ischeckedValueHts}" fieldValue="true" onclick="accordingBillToCode();" onchange="changeStatus();" tabindex=""/></configByCorp:fieldVisibility></td>							
				<td align="left"><configByCorp:fieldVisibility componentId="component.field.Alternative.historicalContracts">Historical/Future&nbsp;Contracts</configByCorp:fieldVisibility></td>
				<td align="right" width="86px"><fmt:message key="billing.billTo1Point"/><font color="red" size="2">*</font></td>
		    	<td align="left"><configByCorp:customDropDown 	listType="map" list="${paytype}" fieldValue="${billing.billTo1Point}"
		        attribute="id=billPayMethod class=list-menu name=billing.billTo1Point  style=width:151px  headerKey='' headerValue='' onchange='changeStatus();autoLinkCreditCard();autoStorageBilling('0')'; tabindex='' "/></td>
		    	
		    	  <!--<s:select cssClass="list-menu" id="billPayMethod" name="billing.billTo1Point" list="%{paytype}"  onchange="changeStatus();autoLinkCreditCard();autoStorageBilling('0');" cssStyle="width:204px;" headerKey="" headerValue="" tabindex=""/>-->
				<td class="listwhitetext" align="right" style="width: 63px;"><fmt:message key='accountInfo.creditTerms' /></td>
				<td>
				<configByCorp:customDropDown 	listType="map" list="${creditTerms}" fieldValue="${billing.creditTerms}"
		attribute="id=billingForm_billing_creditTerms class=list-menu name=billing.creditTerms  style=width:94px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/></td>
		    	
				
				<%--  <s:select cssClass="list-menu" name="billing.creditTerms" list="%{creditTerms}" cssStyle="width:125px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>--%>
		    </tr>
		    </table>
		    </td>		    
		    </tr>
		   <tr>
		   <td colspan="25">
		 <table class="detailTabLabel" border="0" style="margin:0px;">   
		    <tr>		    
		    	<td align="right" width="117">Attn&nbsp;Billing</td>
		    	<td><s:textfield name="billing.attnBilling" onchange="changeStatus();" cssStyle="width:228px;" required="true" cssClass="input-text" maxlength="100" tabindex=""/>
		    	</td>
		    	
		    	<c:if test="${fn:indexOf(serviceOrder.serviceType,'TEN')>-1}"> 
					<c:if test="${systemDefaultVatCalculation=='true'}">
						<td width="80px" align="right" class="listwhitetext" >Billing&nbsp;Currency&nbsp;</td>
						<td><configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.billingCurrency}"
		attribute="id=billingForm_billing_billingCurrency class=list-menu name=billing.billingCurrency  style=width:60px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/></td>
<%-- 						<s:select cssClass="list-menu" name="billing.billingCurrency" list="%{currency}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:60px" tabindex=""/></td>
 --%>					</c:if>
				</c:if>
		    	
		    	<sec-auth:authComponent componentId="module.field.billing.isAutomatedStorageBillingProcessing">
					<%
						String sectionRreceivableDetail="disabled";
						int permission  = (Integer)request.getAttribute("module.field.billing.isAutomatedStorageBillingProcessing" + "Permission");
						if (permission > 2 ){
					 		sectionRreceivableDetail = "";
					 	}
					%>
					<c:if test="${serviceOrder.job=='RLO'}">
					<s:hidden name="billing.isAutomatedStorageBillingProcessing"/>
					</c:if>
					<c:if test="${serviceOrder.job!='RLO'}">
					<c:set var="ischecked" value="false"/>
					<c:if test="${billing.isAutomatedStorageBillingProcessing}">
						<c:set var="ischecked" value="true"/>
					</c:if>	</c:if>
					<td align="right"><c:if test="${serviceOrder.job!='RLO'}"><s:checkbox key="billing.isAutomatedStorageBillingProcessing" onchange="changeStatus();" id="autoProcessing" value="${ischecked}"  onclick="payMethodType()" fieldValue="true" disabled="<%= sectionRreceivableDetail%>" tabindex="" cssStyle="margin-left:8px;!margin-left:0px;"/></c:if></td>							
					<td align="left" colspan="7"><c:if test="${serviceOrder.job!='RLO'}">For Automated Storage Billing Processing</c:if></td>
					
				</sec-auth:authComponent>
				 <td width="80px"  align="right" class="listwhitetext">Billing Cycle</td>
			     <td class="listwhitetext"><s:select cssClass="list-menu" id="billingCycle1" name="billing.billingCycle" list="%{billCycle}" cssStyle="width:170px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>						
			</tr>
			<tr>
			 <c:set var="invoiceByEmail" value="false"/>
	  		  <c:if test="${billing.invoiceByEmail}">
	  			<c:set var="invoiceByEmail" value="true"/>
	  		 </c:if>
	  		<c:if test="${fn:indexOf(systemDefaultstorage,serviceOrder.job)>=0}">	  		 
	  		 <td align="right">Storage Email</td>
	  		 <td><s:textfield id="storageEmail"  name="billing.storageEmail" onchange="changeStatus();" cssClass="input-text" cssStyle="width:228px;" tabindex=""/></td>
			 <td align="right" width="20px">
	  		 <s:checkbox name="billing.invoiceByEmail" id="emailInvoice" value="${invoiceByEmail}" fieldValue="true" 
	  		 onclick="changeStatus();fillInvoiceNumber();blankEmailField();" tabindex=""/></td>
	  		 <td>
	  		 <fmt:message key="billing.invoiceByEmail"/>
	  		 </td>
			<td align="right" style="width:30px;padding-top:17px;">&nbsp;</td>
			<td align="right" class="listwhitetext">Storage Email Type</td>
			<td class="listwhitetext"><s:select cssClass="list-menu" name="billing.storageEmailType" list="%{storageEmailType}" cssStyle="width:173px" headerKey="" headerValue="" /></td>			
			</c:if>
			</tr>
		    </table>
		    </td>
		    </tr>		  
			</tbody>
 		</table>
 		</td></tr>
 		<tr>
			<td height="" align="left" class="listwhitetext" width="" style="margin: 0px;">
 		<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;">
 		  <tbody>
 		  <tr><td align="left" class="vertlinedata"></td></tr>
 		  </tbody>
 		</table>
 		</td></tr>
 		<tr>
			<td align="left" class="listwhitetext" style="margin: 0px">
 		<table  class="detailTabLabel" border="0"  style="margin: 0px;">
 		  <tbody>
				<c:set var="noCharge" value="false"/>
	  		<c:if test="${billing.noCharge}">
	  			<c:set var="noCharge" value="true"/>
	  		</c:if>
			<c:set var="cod" value="false"/>
	  		<c:if test="${billing.cod}">
	  			<c:set var="cod" value="true"/>
	  		</c:if>
	  		<c:set var="paymentReceived" value="false"/>
	  		<c:if test="${billing.paymentReceived}">
	  			<c:set var="paymentReceived" value="true"/>
	  		</c:if>
	  		<tr>
			<sec-auth:authComponent componentId="module.billing.section.billingApproved.edit">
			<%;
			String billingApproved="true";
			int permissionDate1  = (Integer)request.getAttribute("module.billing.section.billingApproved.edit" + "Permission");
 			if (permissionDate1 > 2 ){
 				billingApproved = "false";
 			}
  			%>
  			
			<td align="right" class="listwhitetext" width="120px"><fmt:message key="billing.noCharge"/></td><td align="left"><s:checkbox name="billing.noCharge" value="${noCharge}" fieldValue="true" onclick="changeStatus();" disabled="<%= billingApproved%>" cssStyle="margin:0px;" tabindex=""/></td>
			
			</sec-auth:authComponent>
				<c:if test="${serviceOrder.job !='RLO'}"> 
				<td align="right" class="listwhitetext" width="426px" colspan="2">
				<table  style="margin-top:15px;  padding:0px; margin:0px; ">
				<td>COD</td>
				<td><s:checkbox name="billing.cod" value="${cod}" fieldValue="true" onclick="changeStatus();" cssStyle="width:12px;" tabindex=""/></td>
				<td>COD Amount</td>
				<td style="padding-right:28px;"> <s:textfield cssClass="input-text" name="billing.codAmount" onchange="onlyFloat(this);changeStatus();" required="true" size="11" maxlength="15" tabindex=""/></td>
				<configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate"><td>Payment Received</td>
				<td><s:checkbox name="billing.paymentReceived" value="${paymentReceived}" fieldValue="true" onclick="changeStatus();" cssStyle="width:12px" tabindex=""/></td>
				</configByCorp:fieldVisibility>
				</table>				
				</td>
				</c:if>
				<td width="50" colspan="3"></td>
			</tr>
			<tr>
			<sec-auth:authComponent componentId="module.billing.section.billingApproved.edit">
			<%;
			String billingApproved1="true";
			int permissionDate11  = (Integer)request.getAttribute("module.billing.section.billingApproved.edit" + "Permission");
 			if (permissionDate11 > 2 ){
 				billingApproved1 = "false"; 		
 			} 	
  			%>
				
                <td align="right" class="listwhitetext" width=""><fmt:message key="billing.approvedBy"/></td>
				<td align="left" >				
				<s:select cssClass="list-menu" id="approvedBy" name="billing.approvedBy" list="%{execList}" disabled="<%= billingApproved1%>" cssStyle="width:232px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/>
				</td>
				<td align="right" class="listwhitetext" width=""><fmt:message key="billing.dateApproved"/></td>
				<td align="left" width="123px">		    	
		    	<c:if test="${not empty billing.dateApproved}">
					<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.dateApproved"/></s:text>
					<% if (billingApproved1.equalsIgnoreCase("true")){%>
					<s:textfield id="dateApproved" name="billing.dateApproved" value="%{FormatedInvoiceDate}"  readonly="true" cssClass="input-text" cssStyle="width:65px" size="7" onfocus="changeStatus();" tabindex=""/>
					 <%} %>
				    <% if (billingApproved1.equalsIgnoreCase("false")){%>
				    <s:textfield id="dateApproved" name="billing.dateApproved" value="%{FormatedInvoiceDate}" onkeydown="onlyDel(event,this)"  readonly="true" cssClass="input-text" cssStyle="width:65px" size="7" onfocus="changeStatus();" tabindex=""/>
				    <img id="dateApproved-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				   <%} %>
				</c:if>
				
				<c:if test="${empty billing.dateApproved}">
					<% if (billingApproved1.equalsIgnoreCase("true")){%>
					<s:textfield id="dateApproved" cssStyle="width:69px" name="billing.dateApproved"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
					 <%} %>
					 <% if (billingApproved1.equalsIgnoreCase("false")){%>
					<s:textfield id="dateApproved" cssStyle="width:69px" name="billing.dateApproved" onkeydown="onlyDel(event,this)"  readonly="true" cssClass="input-text" size="7" tabindex=""/>
					<img id="dateApproved-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				    <%} %>
				</c:if>
			
			
			</sec-auth:authComponent>
			<td></td>
			</tr>
			<tr>		
		  	<td align="right" colspan="" width="" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=billing.gstVATrefund',this);return false" ><fmt:message key="billing.gstVATrefund"/></a></td>
				
				<td align="left" width="100px">
					<c:if test="${not empty billing.gstVATrefund}">
						<s:text id="billingGstVATrefundFormatedDate" name="${FormDateValue}"><s:param name="value" value="billing.gstVATrefund"/></s:text>
					    <s:textfield id="gstVATrefund" name="billing.gstVATrefund" value="%{billingGstVATrefundFormatedDate}" onkeydown="onlyDel(event, this);changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:65px" tabindex=""/>
					</c:if>
					<c:if test="${empty billing.gstVATrefund}">
						<s:textfield id="gstVATrefund" name="billing.gstVATrefund" onkeydown="onlyDel(event, this)" readonly="true" cssClass="input-text" cssStyle="width:65px" tabindex=""/>
					</c:if>
						<img id="gstVATrefund-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</td>
				
                    <c:if test="${systemDefaultVatCalculation=='true'}">
                    <c:choose>
                    <c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup && networkAgent }">
                    <td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
				    <td class="listwhitetext" width="" ><configByCorp:customDropDown 	listType="map" list="${euVatList}" fieldValue="${billing.primaryVatCode}"
		attribute="id=billing.primaryVatCode class=list-menu name=billing.primaryVatCode style=width:93px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
				    <%-- <s:select  cssClass="list-menu"  key="billing.primaryVatCode" id="billing.primaryVatCode" cssStyle="width:93px" list="%{euVatList}" headerKey="" onchange="changeStatus();"  headerValue="" tabindex=""  disabled="true"/> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick=""/></td>
                    </c:when>
                    <c:otherwise>
				    <td class="listwhitetext" width="" align="right">VAT&nbsp;Desc</td>
				    <td class="listwhitetext" width="" ><configByCorp:customDropDown 	listType="map" list="${euVatList}" fieldValue="${billing.primaryVatCode}"
		attribute="id=billing.primaryVatCode class=list-menu name=billing.primaryVatCode style=width:93px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
<%-- 				    <s:select  cssClass="list-menu"  key="billing.primaryVatCode" id="billing.primaryVatCode" cssStyle="width:93px" list="%{euVatList}" headerKey="" onchange="changeStatus();" headerValue="" tabindex=""  /> --%><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillRecAccVat('primaryVatCode');"/></td>
					</c:otherwise>
					</c:choose>
					</c:if>	
							    <configByCorp:fieldVisibility componentId="component.button.trackingStatusContractReceived">				
			     <td align="right" class="listwhitetext">Contract&nbsp;Received</td>
			 <c:if test="${not empty trackingStatus.contractReceived}"> 
			  <s:text id="accountLineFormattedEstimateContractValueDate" name="${FormDateValue}"><s:param name="value" value="trackingStatus.contractReceived"/></s:text>
			  <td ><s:textfield id="contractReceived" name="trackingStatus_contractReceived" value="%{accountLineFormattedEstimateContractValueDate}" onkeydown="onlyDel(event,this)" cssStyle="width:65px" readonly="true" cssClass="input-text" size="8" tabindex=""/>
			  <img id="contractReceived_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			 </td>
			 </c:if>
			     <c:if test="${empty trackingStatus.contractReceived}">
			   <td><s:textfield id="contractReceived" name="trackingStatus_contractReceived" onkeydown="onlyDel(event,this)" readonly="true" cssClass="input-text" cssStyle="width:65px" size="8" tabindex=""/>
			   <img id="contractReceived_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			   </td>
			     </c:if>
			</configByCorp:fieldVisibility>	
					
					 <c:if test="${systemDefaultVatCalculation!='true'}">
					 <s:hidden name="billing.primaryVatCode"/>
					 <td class="listwhitetext" width="" >
					 <td class="listwhitetext" width="20" >
					 </c:if>		
					<c:if test="${empty billing.id}">
						<td align="right" colspan="1"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
					</c:if>
					<c:if test="${not empty billing.id}">
						<c:choose>
							<c:when test="${countBillingPrimaryNotes == '0' || countBillingPrimaryNotes == '' || countBillingPrimaryNotes == null}">
								<td  align="right" colspan="1"><img id="countBillingPrimaryNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingPrimary&imageId=countBillingPrimaryNotesImage&fieldId=countBillingPrimaryNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingPrimary&imageId=countBillingPrimaryNotesImage&fieldId=countBillingPrimaryNotes&decorator=popup&popup=true',800,600);" ></a></td>
							</c:when>
							<c:otherwise>
								<td align="right" colspan="1"><img id="countBillingPrimaryNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingPrimary&imageId=countBillingPrimaryNotesImage&fieldId=countBillingPrimaryNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingPrimary&imageId=countBillingPrimaryNotesImage&fieldId=countBillingPrimaryNotes&decorator=popup&popup=true',800,600);" ></a></td>
							</c:otherwise>
						</c:choose> 
					</c:if>
			</tr>			
	      </tbody>
 		</table>
 		</td></tr> 		
 		<tr>
			<td align="left" class="listwhitetext" style="margin: 0px;" width="100%" style="margin: 0px;" >
			
		<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" style="margin: 0px;" >
 		  <tbody>
 		  	<tr><td align="left" class="vertlinedata"></td></tr>
	 	  </tbody>
 		</table>
 		</td> 		 		
 		</tr> 		
	<tr><td class="listwhitetext">
 		  <table class="detailTabLabel" border="0">
				<tbody> 		  
 		  		<tr>
					<td align="right" class="listwhitetext"><fmt:message key="billing.billingInstruction"/><font color="red" size="2">*</font></td>
					<td align="left" width="363">
					<configByCorp:customDropDown 	listType="map" list="${billinginstruction}" fieldValue="${billing.billingInstructionCodeWithDesc}"
		attribute="id=billingForm_billing_billingInstructionCodeWithDesc class=list-menu name=billing.billingInstructionCodeWithDesc style=width:365px  headerKey='' headerValue='' onchange='changeStatus();autoPopulate_billingInstruction(this)'; tabindex='' "/></td>
					<%-- <s:select cssClass="list-menu" name="billing.billingInstructionCodeWithDesc" list="%{billinginstruction}" cssStyle="width:365px" headerKey="" headerValue="" onchange="changeStatus();autoPopulate_billingInstruction(this);" tabindex=""/></td> --%>
					<td align="left" class="listwhitetext" style="width:15px"></td>
					<td align="left"><s:textfield name="billing.billingInstruction" onchange="changeStatus();" required="true" cssClass="input-text" maxlength="60" cssStyle="width:240px" tabindex=""/></td>
				<td style="width:24px"></td>
				</tr>
				<tr>
					<td align="right" class="listwhitetext" width="120px"><fmt:message key="billing.specialInstruction"/></td>
					<td align="left" colspan="5"><s:textfield name="billing.specialInstruction" onchange="changeStatus();" required="true" cssClass="input-text" maxlength="130" cssStyle="width:628px" tabindex=""/></td>
				</tr>
				<s:hidden key="billing.billingInstructionCode" />
				<tr>
				<td colspan="5">
				<table style="margin: 0px;">
				<tr>
				<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
				<td align="left" class="listwhitetext">Pricing</td>
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext">Billing</td>
				<configByCorp:fieldVisibility componentId="component.field.billing.internalBillingPerson">
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext">Internal&nbsp;Billing</td>
				</configByCorp:fieldVisibility>
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext">
				<configByCorp:fieldVisibility componentId="component.field.customerFile.payable">
				Payable
				</configByCorp:fieldVisibility>
				</td> 
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext">Auditor</td>
			    <configByCorp:fieldVisibility componentId="component.field.billing.personReceivable">
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext">Receivable</td>
				</configByCorp:fieldVisibility>
				<configByCorp:fieldVisibility componentId="component.field.billing.claimHandler">
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext">Claim&nbsp;Handler</td>
				</configByCorp:fieldVisibility>
				<configByCorp:fieldVisibility componentId="component.field.billing.forwarderPerson">
				<td align="left" class="listwhitetext" style="width:5px"></td>
				<td align="left" class="listwhitetext">Forwarder</td>
				</configByCorp:fieldVisibility>
				</tr>
			<tr>
			<td align="right" class="listwhitetext" style="width:118px">Responsible Person:</td>
			<td><s:select name="billing.personPricing" id="personPricing" cssClass="list-menu" list="%{pricingList}" cssStyle="width:140px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td> 
			<td align="left" class="listwhitetext" style="width:15px"></td>
			<td>
			<s:select name="billing.personBilling" id="personBilling" cssClass="list-menu" list="%{billingList}" cssStyle="width:140px" headerKey="" headerValue="" onchange="changeStatus();checkBillingPerson();" tabindex=""/>
			</td>
			<td align="left" class="listwhitetext" style="width:15px"></td>
			<configByCorp:fieldVisibility componentId="component.field.billing.internalBillingPerson">
			<td><s:select name="billing.internalBillingPerson" cssClass="list-menu" list="%{internalBillingPersonList}" cssStyle="width:140px" headerKey="" headerValue="" /></td>
			<td align="left" class="listwhitetext" style="width:10px"></td>
			</configByCorp:fieldVisibility>
			<td>
			<configByCorp:fieldVisibility componentId="component.field.customerFile.payable">
			<s:select cssClass="list-menu" id="personPayable" name="billing.personPayable" list="%{payableList}" cssStyle="width:140px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/>
			</configByCorp:fieldVisibility>
			</td>
			<td align="left" class="listwhitetext" style="width:16px"></td>
			<td>
			<s:select name="billing.auditor" id="auditorBilling" cssClass="list-menu" list="%{auditorList}" cssStyle="width:140px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/>
			</td>
		    <td align="left" class="listwhitetext" style="width:15px"></td>
			<configByCorp:fieldVisibility componentId="component.field.billing.personReceivable">
			<td><s:select cssClass="list-menu"  name="billing.personReceivable" list="%{receivableList}" cssStyle="width:140px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
			<td align="left" class="listwhitetext" style="width:10px"></td>
			</configByCorp:fieldVisibility> 
			<configByCorp:fieldVisibility componentId="component.field.billing.claimHandler">			
			<td><s:select cssClass="list-menu"  id="claimHandler" name="billing.claimHandler" list="%{claimPersonList}" cssStyle="width:140px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
			<td align="left" class="listwhitetext" style="width:10px"></td>
			</configByCorp:fieldVisibility>
			<configByCorp:fieldVisibility componentId="component.field.billing.forwarderPerson">
			<td><s:select cssClass="list-menu"  name="billing.personForwarder" list="%{forwarderList}" cssStyle="width:140px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
			<td align="left" class="listwhitetext" style="width:10px"></td>
			</configByCorp:fieldVisibility>
	       </tr>
	       </table>
	       </td>
	       </tr>
			</tbody>
			</table></td></tr>
    <tr><td align="left" height="5px"></td></tr>    
    <tr>
	<td height="10" width="100%" align="left" >	
        <div  onClick="javascript:animatedcollapse.toggle('status')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Billing Status
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>	                     
					   <div id="status" >
<s:hidden name="componentId" value="module.billing" /> 		
	<table class="detailTabLabel" border="0" style="display:inline; padding:0px;margin:0px;position:relative;">
 		<tbody> 	   
<sec-auth:authComponent componentId="module.billing.section.billingComplete.edit">
	<%;
	String billingComplete="true";
	int permissionDate  = (Integer)request.getAttribute("module.billing.section.billingComplete.edit" + "Permission");
 	if (permissionDate > 2 ){
 		billingComplete = "false";
 	}	
  %> 
  <tr>
				<td align="right" class="listwhitetext" width="120px"><fmt:message key="billing.Complete"/></td>
				<configByCorp:fieldVisibility componentId="component.field.billing.billingComplete">
				<td align="left" width="75">			
				<configByCorp:customDropDown 	listType="map" list="${taract}" fieldValue="${billing.billCompleteA}"
		attribute="id=billingForm_billing_billCompleteA class=list-menu name=billing.billCompleteA style=width:65px  headerKey='' headerValue='' onchange='populateBillingCompleteDate(),checkBillingCompleteStatus(this.name),changeStatus();' tabindex='' "/></td>
				<%--<s:select cssClass="list-menu" disabled="<%=billingComplete%>" name="billing.billCompleteA" list="%{taract}" cssStyle="width:65px" onchange="populateBillingCompleteDate(),checkBillingCompleteStatus(this.name),changeStatus();" tabindex=""/></td>--%>
				</configByCorp:fieldVisibility>
				<td align="left" width="40px">
				<% if (billingComplete.equalsIgnoreCase("false")){%>
	 				<c:if test="${not empty billing.billComplete}">
						<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.billComplete"/></s:text>
					    <s:textfield id="billComplete" name="billing.billComplete" value="%{FormatedInvoiceDate}" onkeydown="onlyDel(event,this)" onselect="changeBillingBillCompleteVar();changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
					</c:if>
					<c:if test="${empty billing.billComplete}">
						<s:textfield id="billComplete" name="billing.billComplete" onkeydown="onlyDel(event,this)" onselect="changeBillingBillCompleteVar();changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
					</c:if>
				<%} %>				
				<% if (billingComplete.equalsIgnoreCase("true")){%>
	 				<c:if test="${not empty billing.billComplete}">
						<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.billComplete"/></s:text>
					    <s:textfield id="billComplete" name="billing.billComplete" value="%{FormatedInvoiceDate}"  onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
					</c:if>
					<c:if test="${empty billing.billComplete}">
						<s:textfield id="billComplete" name="billing.billComplete"  onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" onselect="checkBillingCompleteStatus(this.id);" tabindex=""/>
					</c:if>
				<%} %>	
				</td>			
				<% if (billingComplete.equalsIgnoreCase("false")){%>
				<td>
					<div id="myimageDiv" style="position:relative;">
				
					<img id="billComplete-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkBillingCompleteStatus(this.id);document.forms['billingForm'].elements['billing.billComplete'].select(); return false; "/>
					</div>
				</td>
				<%} %>
	
				
				</sec-auth:authComponent>
				<configByCorp:fieldVisibility componentId="component.field.billing.FIDIISO">
				<td align="right" colspan="" width="" class="listwhitetext">FIDI / ISO</td>
				<td align="left" width="100px">
					<c:if test="${not empty billing.FIDIISOAudit}">
						<s:text id="billingFIDIISOAuditFormatedDate" name="${FormDateValue}"><s:param name="value" value="billing.FIDIISOAudit"/></s:text>
					    <s:textfield id="FIDIISOAudit" name="billing.FIDIISOAudit" value="%{billingFIDIISOAuditFormatedDate}"  onkeydown="onlyDel(event, this)"  readonly="true" cssClass="input-text" cssStyle="width:65px" tabindex=""/>
					</c:if>
					<c:if test="${empty billing.FIDIISOAudit}">
						<s:textfield id="FIDIISOAudit" name="billing.FIDIISOAudit" onkeydown="onlyDel(event, this)" readonly="true" cssClass="input-text" cssStyle="width:65px" tabindex=""/>
					</c:if>
						<img id="FIDIISOAudit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</td>
				</configByCorp:fieldVisibility>
				<!-- As per Bug# 6315 ** Hidded these field for RLO job ** -->
				<c:if test="${serviceOrder.job !='RLO'}"> 
                <td align="right"  class="listwhitetext" width="145px"><fmt:message key="billing.noMoreWork"/></td>
                <td align="left" >
                
                
<c:set var="chkNoMoreWork" value="No" />
<sec-auth:authComponent componentId="module.billing.noMoreWork.edit">
<c:set var="chkNoMoreWork" value="Yes" />
						<c:if test="${not empty billing.noMoreWork}">
						<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.noMoreWork"/></s:text>
					    <s:textfield id="noMoreWork"  name="billing.noMoreWork" value="%{FormatedInvoiceDate}"  onselect="billingCompleteStorageOutControl();changeStatus();" readonly="true" onkeydown="onlyDel(event,this)" cssClass="input-text" size="7" tabindex=""/>
					</c:if>
					<c:if test="${empty billing.noMoreWork}">
						<s:textfield id="noMoreWork"  name="billing.noMoreWork"   readonly="true" onkeydown="onlyDel(event,this)" onselect="billingCompleteStorageOutControl();changeStatus(); " cssClass="input-text" size="7" tabindex=""/>
					</c:if>
                <img id="noMoreWork-trigger" style="vertical-align:bottom;position:relative;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					</sec-auth:authComponent>
                
                <c:if test ="${chkNoMoreWork=='No' }" >
	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.noMoreWork"/></s:text>
					 <c:if test="${not empty billing.noMoreWork}">
						<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.noMoreWork"/></s:text>
					    <s:textfield id="noMoreWork"  name="billing.noMoreWork" value="%{FormatedInvoiceDate}"  onselect="billingCompleteStorageOutControl();changeStatus();" readonly="true"  cssClass="input-text" size="7" tabindex=""/>
					</c:if>
					<c:if test="${empty billing.noMoreWork}">
						<s:textfield id="noMoreWork"  name="billing.noMoreWork"   readonly="true"  onselect="billingCompleteStorageOutControl();changeStatus(); " cssClass="input-text" size="7" tabindex=""/>
					</c:if>
                </c:if>
                
              
				</td>				
				<td>
				 <input type="button" class="cssbuttonA" style="width:75px; height:21px"  name="TicketList"  value="Ticket List" onclick="findWorkTicketList();" tabindex=""/>
				</td>
				</c:if>
<sec-auth:authComponent componentId="module.billing.section.billingComplete.edit">
	<%;
	String billingComplete="true";
	int permissionDate  = (Integer)request.getAttribute("module.billing.section.billingComplete.edit" + "Permission");
 	if (permissionDate > 2 ){
 		billingComplete = "false";
 	}
 	
  %>
<c:if test="${auditCompleteDate!='Y'}">
<td align="right"  class="listwhitetext">Audit&nbsp; Complete</td>
</c:if>
<c:if test="${auditCompleteDate=='Y'}">
<td align="right"  class="listwhitetext">Last&nbsp;Audit&nbsp;Date</td>
</c:if>
<td align="left" >
<%
if(index!=-1){
%>
				<c:if test="${not empty billing.auditComplete}">
					<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.auditComplete"/></s:text>
				    <s:textfield id="auditComplete" name="billing.auditComplete" value="%{FormatedInvoiceDate}" onkeydown="onlyDel(event,this);"  readonly="true" cssClass="input-text" cssStyle="width:65px;" onfocus="changeStatus();" tabindex=""/>
				   <img id="auditComplete-trigger" style="vertical-align:bottom;position:relative;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkBillingCompleteStatus(this.id);"/>
				   <c:if test="${auditCompleteDate=='Y'}">
				   <td><img id="rateImage1"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillRecAccAuditDate();"/></td>
				   </c:if>
				</c:if>
				<c:if test="${empty billing.auditComplete}">
					<s:textfield id="auditComplete" name="billing.auditComplete" onkeydown="onlyDel(event,this);changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
					<img id="auditComplete-trigger" style="vertical-align:bottom;position:relative;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkBillingCompleteStatus(this.id);"/>
					<c:if test="${auditCompleteDate=='Y'}">
					<td><img id="rateImage1"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="fillRecAccAuditDate();"/></td>
				</c:if>
				</c:if>
<%
}
else{
%>
</td>
<td>
<c:if test="${not empty billing.auditComplete}">
					<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.auditComplete"/></s:text>
				    <s:textfield id="auditComplete" name="billing.auditComplete" value="%{FormatedInvoiceDate}" readonly="true"   cssClass="input-textUpper" cssStyle="width:65px;" onfocus="changeStatus();" tabindex=""/>
				    <img id="auditComplete" style="vertical-align:bottom;position:relative;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
				</c:if>
				<c:if test="${empty billing.auditComplete}">
					<s:textfield id="auditComplete" name="billing.auditComplete" readonly="true" onkeydown="onlyDel(event,this)"   cssClass="input-textUpper" cssStyle="width:65px;" onfocus="changeStatus();" tabindex=""/>
					<img id="auditComplete" style="vertical-align:bottom;position:relative;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
				</c:if>
<%
}
%>
</td>
 <configByCorp:fieldVisibility componentId="component.billing.FinancialsComplete.Visibility">	
 <td align="right"  class="listwhitetext">Financial&nbsp;Complete</td> 
				<td align="left" width="40px">
				<% if (billingComplete.equalsIgnoreCase("false")){%>
	 				<c:if test="${not empty billing.financialsComplete}">
						<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.financialsComplete"/></s:text>
					    <s:textfield id="financialsComplete" name="billing.financialsComplete" value="%{FormatedInvoiceDate}" onkeydown="onlyDel(event,this)" onselect="changeBillingBillCompleteVar();changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
					</c:if>
					<c:if test="${empty billing.financialsComplete}">
						<s:textfield id="financialsComplete" name="billing.financialsComplete" onkeydown="onlyDel(event,this)" onselect="changeStatus();"  readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
					</c:if>
				<%} %>				
				<% if (billingComplete.equalsIgnoreCase("true")){%>
	 				<c:if test="${not empty billing.financialsComplete}">
						<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="billing.financialsComplete"/></s:text>
					    <s:textfield id="financialsComplete" name="billing.financialsComplete" value="%{FormatedInvoiceDate}"  onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
					</c:if>
					<c:if test="${empty billing.financialsComplete}">
						<s:textfield id="financialsComplete" name="billing.financialsComplete"  onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" onselect="" tabindex=""/>
					</c:if>
				<%} %>	
				</td>			
				<% if (billingComplete.equalsIgnoreCase("false")){%>
				<td>
					<div id="myimageDiv" style="position:relative;">
				
					<img id="financialsComplete-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkBillingCompleteStatus(this.id);document.forms['billingForm'].elements['billing.financialsComplete'].select(); return false; "/>
					</div>
				</td>
				<%} %> 
</configByCorp:fieldVisibility>	
<c:set var="readyForInvoicingCorpIdEdit" value="true"/>
<configByCorp:fieldVisibility componentId="component.billing.section.readyForInvoicing.edit">
<c:set var="readyForInvoicingCorpIdEdit" value="false"/>
</configByCorp:fieldVisibility>
    
<c:set var="readyForInvoicingEdit" value="false"/>
<sec-auth:authComponent componentId="module.billing.section.readyForInvoicing.edit"> 
 <c:set var="readyForInvoicingEdit" value="true"/> 
</sec-auth:authComponent>
<c:choose>
<c:when test="${!readyForInvoicingCorpIdEdit}"> 
<c:choose>
 <c:when test="${readyForInvoicingEdit}">
<td align="right" class="listwhitetext" width="100px">Ready&nbsp;For&nbsp;Invoicing</td>
           <td align="left"><s:checkbox name="billing.readyForInvoicing" id="checkInvoice" value="${billing.readyForInvoicing}" fieldValue="true"  onchange="changeStatus();changeReadyForInvoiceCheckEdit()" tabindex=""/></td>			
</c:when> 
<c:otherwise>  
<c:choose>
 
<c:when test="${(!(billing.readyForInvoicing))}">
			    
	  <td align="right" class="listwhitetext" width="100px">Ready&nbsp;For&nbsp;Invoicing</td>
      <td align="left"><s:checkbox name="billing.readyForInvoicing" id="checkInvoice" value="${billing.readyForInvoicing}" fieldValue="true"     onchange="changeStatus() ; changeReadyForInvoiceCheckEdit();" tabindex=""/></td>			
 </c:when>	
<c:otherwise>
  <td align="right" class="listwhitetext" width="100px">Ready&nbsp;For&nbsp;Invoicing</td>
        <td align="left"><s:checkbox name="billing.readyForInvoicing" id="checkInvoice" value="${billing.readyForInvoicing}" fieldValue="true"  disabled="true"    onchange="changeStatus() ;" tabindex=""/></td>			
 </c:otherwise> 
</c:choose>
</c:otherwise></c:choose>
</c:when>
<c:otherwise>
		<c:if test="${from=='rule'}">
				<c:if test="${field=='billing.readyForInvoicing'}">
				<td align="right" class="listwhitetext" width="100px"><font color="red">Ready&nbsp;For&nbsp;Invoicing</font> </td>
				</c:if></c:if>
				<c:if test="${from=='rule'}">
				<c:if test="${field!='billing.readyForInvoicing'}">
              <td align="right" class="listwhitetext" width="100px">Ready&nbsp;For&nbsp;Invoicing</td>
				</c:if></c:if>
			<c:if test="${from!='rule'}">
			<td align="right" class="listwhitetext" width="100px">Ready&nbsp;For&nbsp;Invoicing</td>
				</c:if> 

<td align="left"><s:checkbox name="billing.readyForInvoicing" id="checkInvoice" value="${billing.readyForInvoicing}" fieldValue="true"  onchange="changeStatus();changeReadyForInvoiceCheck()" tabindex=""/></td>
</c:otherwise></c:choose>
            <td align="right" class="listwhitetext">Dated</td>
            <c:if test="${not empty billing.readyForInvoiceCheck}">
			<s:text id="billingFormattedValue" name="${FormDateValue}"><s:param name="value" value="billing.readyForInvoiceCheck"/></s:text>
			<td><s:textfield id="readyForInvoiceCheck" name="billing.readyForInvoiceCheck" value="%{billingFormattedValue}" readonly="true"   cssClass="input-textUpper" cssStyle="width:65px;"  tabindex=""/>
			</c:if>
			<c:if test="${empty billing.readyForInvoiceCheck}">
			<td><s:textfield id="readyForInvoiceCheck" name="billing.readyForInvoiceCheck" value="%{billingFormattedValue}" readonly="true"   cssClass="input-textUpper" cssStyle="width:65px;" tabindex=""/>
			</c:if>
			
           <td align="right" width="50">			
 						<c:if test="${empty billing.id}">
						<img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/>
					</c:if>
					<c:if test="${not empty billing.id}">
						<c:choose>
							<c:when test="${countBillingCompleteStatusNotes == '0' || countBillingCompleteStatusNotes == '' || countBillingCompleteStatusNotes == null}">
								<img id="countBillingCompleteStatusNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingStatus&imageId=countBillingCompleteStatusNotesImage&fieldId=countBillingCompleteStatusNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingStatus&imageId=countBillingCompleteStatusNotesImage&fieldId=countBillingCompleteStatusNotes&decorator=popup&popup=true',800,600);" ></a>
							</c:when>
							<c:otherwise>
								<img id="countBillingCompleteStatusNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingStatus&imageId=countBillingCompleteStatusNotesImage&fieldId=countBillingCompleteStatusNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingStatus&imageId=countBillingCompleteStatusNotesImage&fieldId=countBillingCompleteStatusNotes&decorator=popup&popup=true',800,600);" ></a>
							</c:otherwise>
						</c:choose> 
					</c:if>				
			</td>				
       </sec-auth:authComponent>
  	</tbody>
 		</table>
 		
		 </div></td></tr>
		 
	<div id="billingSetionDetail">
		<jsp:include flush="true" page="billingFormBillingSection.jsp"></jsp:include>
	</div>
  
<c:if test="${serviceOrder.job =='UVL' || serviceOrder.job =='MVL' || serviceOrder.job =='DOM' || serviceOrder.job =='DOF' }">
  <tr>
	<td height="10" align="left" class="listwhitetext">
	 <div  onClick="javascript:animatedcollapse.toggle('domestic')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Domestic Data
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>
  		<div id="domestic">  		
  		<table class="detailTabLabel" border="0">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
 		  	  <tr>
 		  	  <td align="right" width="120px" class="listwhitetext"><fmt:message key="billing.scheduleCode"/></td>
 		  	  <td align="left">
 		  	  <configByCorp:customDropDown 	listType="map" list="${schedule}" fieldValue="${billing.scheduleCode}"
		attribute="id=billingForm_billing_scheduleCode class=list-menu name=billing.scheduleCode style=width:456px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
 		  	  
 		  	  
 		  	 </td>
			 </tr>
 		 </tbody>
 	    </table>
		<table class="detailTabLabel" border="0"  width="">
 		  <tbody>
 		  <tr>
 		  	  <td align="right" width="" class="listwhitetext"><fmt:message key="billing.authorityType"/></td>
			  <td align="left" width="83px" >
			 <configByCorp:customDropDown 	listType="map" list="${auth}" fieldValue="${billing.authorityType}"
		attribute="id=billingForm_billing_authorityType class=list-menu name=billing.authorityType style=width:93px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
 		  	  
		      <td align="right" class="listwhitetext"><fmt:message key="billing.securityAccount"/></td>
		      <td align="left" width="70px">
		       <configByCorp:customDropDown 	listType="map" list="${scac}" fieldValue="${billing.securityAccount}"
		attribute="id=billingForm_billing_securityAccount class=list-menu name=billing.securityAccount style=width:60px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/>
 		  	
	      </tr>
	      <tr>
		    	<td align="right" class="listwhitetext"><fmt:message key="billing.discount"/></td>
		    	<td align="left"><s:textfield name="billing.discount" maxlength="15" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" tabindex="" size="7"/></td>
		    	<td align="right" width="130px" class="listwhitetext"><fmt:message key="billing.transportationDiscount"/></td>
		    	<td align="left"><s:textfield name="billing.transportationDiscount" maxlength="6" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" tabindex="" size="7"/></td>
		    	<td align="right" width="90px" class="listwhitetext"><fmt:message key="billing.sitDiscount"/></td>
		    	<td align="left"><s:textfield name="billing.sitDiscount" maxlength="15" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text"  tabindex="" size="7"/></td>
		    </tr>
   		    <tr>
   		    	<td align="right" width="120px" class="listwhitetext"><fmt:message key="billing.sharedDiscount"/></td>
   		    	<td align="left"><s:textfield name="billing.sharedDiscount" maxlength="9" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" tabindex="" size="7"/></td>
   		    	<td align="right" class="listwhitetext"><fmt:message key="billing.packDiscount"/></td>
   		    	<td align="left"><s:textfield name="billing.packDiscount" maxlength="6" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" tabindex="" size="7"/></td>
		    	<td align="right" class="listwhitetext"><fmt:message key="billing.OrderForService"/></td>
		    	<td align="left"><s:textfield name="billing.OrderForService" maxlength="2" cssStyle="text-align:right; width:55px" onchange="onlyNumeric(this);changeStatus();" required="true" cssClass="input-text"  tabindex="" size="2"/></td>
   		    </tr>
   		    <tr>
   		    	<td align="right" class="listwhitetext"><fmt:message key="billing.otherDiscount"/></td>
   		    	<td align="left"><s:textfield name="billing.otherDiscount" maxlength="6" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" size="7" tabindex=""/></td>
   		    	<td align="right" class="listwhitetext"><fmt:message key="billing.deposit"/></td>
   		    	<td align="left" colspan="4"><s:textfield name="billing.deposit" maxlength="15" cssStyle="text-align:right" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" size="20" tabindex=""/></td>
   		    
   		     <td width="220"></td>
							<c:if test="${empty billing.id}">
							<td  align="left" width="65px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty billing.id}">
							<c:choose>
								<c:when test="${countBillingDomesticNotes == '0' || countBillingDomesticNotes == '' || countBillingDomesticNotes == null}">
								<td  align="left" width="65px"><img id="countBillingDomesticNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingDomestic&imageId=countBillingDomesticNotesImage&fieldId=countBillingDomesticNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingDomestic&imageId=countBillingDomesticNotesImage&fieldId=countBillingDomesticNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td  align="left" width="65px"><img id="countBillingDomesticNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingDomestic&imageId=countBillingDomesticNotesImage&fieldId=countBillingDomesticNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingDomestic&imageId=countBillingDomesticNotesImage&fieldId=countBillingDomesticNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>   		    
   		    </tr>
   		    <tr><td align="left" height="5px"></td></tr>
	      </tbody>
 		</table></div></td>
 		  </tr>  
 </c:if>	
 <c:if test="${serviceOrder.job!='RLO'}">
 <configByCorp:fieldVisibility componentId="component.button.gstAuthorizationServiceProvide">
 
<c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <tr>
	<td height="10" align="left" class="listwhitetext">
	 <div  onClick="javascript:animatedcollapse.toggle('gstAutho')" style="margin: 0px"  >
<c:if test="${from=='rule'}">
<c:if test="${field=='billing.gstThirdPartyAuthReceivedOrigin'|| field=='billing.gstThirdPartyAuthNoOrigin'}">	 
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" ><font color="red">&nbsp;GST Authorization
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>
</c:if>
</c:if>
<c:if test="${from=='rule'}">
<c:if test="${field!='billing.gstThirdPartyAuthReceivedOrigin'&& field!='billing.gstThirdPartyAuthNoOrigin'}">	 
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;GST Authorization
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>
</c:if>
</c:if>
<c:if test="${from!='rule'}">
	 
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>

<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;GST Authorization
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>
</c:if>
</div>	
  		<div id="gstAutho">  		
  		<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr> 		  	  			
						<tr>
							<td align="left" class="listwhitetext" style="width:90px"></td>
							<td align="left" class="listwhitetext" ></td>
							
							<td align="left" class="listwhitetext" style="width:70px">Service&nbsp;Date</td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>							
							<td align="left" class="listwhitetext" width="70px">Request&nbsp;Date</td>
							
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext" style="width:65px"></td>
							<td width="100px" class="listwhitetext">Authorization&nbsp;Received</td>							
							<td align="left" class="listwhitetext" width="60px"></td>
							<td align="left" class="listwhitetext">Authorization#</td>
							</tr>						
			 				<tr>
							<td align="right" class="listwhitetext" style="width:118px">3rd&nbsp;Party&nbsp;Origin</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<c:if test="${not empty billing.gstThirdPartyOrigin}">
								<s:text id="billingGstThirdPartyOrigin" name="${FormDateValue}"><s:param name="value" value="billing.gstThirdPartyOrigin"/></s:text>
								<td><s:textfield id="gstThirdPartyOrigin" cssClass="input-text" name="billing.gstThirdPartyOrigin" value="%{billingGstThirdPartyOrigin}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstThirdPartyOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstThirdPartyOrigin}">
								<td><s:textfield id="gstThirdPartyOrigin" cssClass="input-text" name="billing.gstThirdPartyOrigin" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstThirdPartyOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:65px">
							<c:if test="${not empty billing.gstThirdPartyRequestOrigin}">
								<s:text id="billingGstThirdPartyRequestOrigin" name="${FormDateValue}"><s:param name="value" value="billing.gstThirdPartyRequestOrigin"/></s:text>
								<td><s:textfield id="gstThirdPartyRequestOrigin" cssClass="input-text" name="billing.gstThirdPartyRequestOrigin" value="%{billingGstThirdPartyRequestOrigin}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstThirdPartyRequestOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstThirdPartyRequestOrigin}">
								<td><s:textfield id="gstThirdPartyRequestOrigin" cssClass="input-text" name="billing.gstThirdPartyRequestOrigin" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstThirdPartyRequestOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:8px"></td>
							<td>
							<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1">
							<tr>
							<c:if test="${not empty billing.gstThirdPartyAuthReceivedOrigin}">
								<s:text id="billingGstThirdPartyAuthReceivedOrigin" name="${FormDateValue}"><s:param name="value" value="billing.gstThirdPartyAuthReceivedOrigin"/></s:text>
								<td><s:textfield id="gstThirdPartyAuthReceivedOrigin" cssClass="input-text" name="billing.gstThirdPartyAuthReceivedOrigin" value="%{billingGstThirdPartyAuthReceivedOrigin}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstThirdPartyAuthReceivedOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstThirdPartyAuthReceivedOrigin}">
								<td><s:textfield id="gstThirdPartyAuthReceivedOrigin" cssClass="input-text" name="billing.gstThirdPartyAuthReceivedOrigin" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstThirdPartyAuthReceivedOrigin-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<td align="left"><s:textfield cssClass="input-text" name="billing.gstThirdPartyAuthNoOrigin" onchange="changeStatus();"  size="22" maxlength="20" tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
			  				</tr>			  
			  			 	<tr>
							<td align="right" class="listwhitetext" style="width:100px">3rd&nbsp;Party&nbsp;Destin</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<c:if test="${not empty billing.gstThirdPartyDestination}">
								<s:text id="billingGstThirdPartyDestination" name="${FormDateValue}"><s:param name="value" value="billing.gstThirdPartyDestination"/></s:text>
								<td><s:textfield id="gstThirdPartyDestination" cssClass="input-text" name="billing.gstThirdPartyDestination" value="%{billingGstThirdPartyDestination}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstThirdPartyDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstThirdPartyDestination}">
								<td><s:textfield id="gstThirdPartyDestination" cssClass="input-text" name="billing.gstThirdPartyDestination" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstThirdPartyDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:65px">
							<c:if test="${not empty billing.gstThirdPartyRequestDestination}">
								<s:text id="billingGstThirdPartyRequestDestination" name="${FormDateValue}"><s:param name="value" value="billing.gstThirdPartyRequestDestination"/></s:text>
								<td><s:textfield id="gstThirdPartyRequestDestination" cssClass="input-text" name="billing.gstThirdPartyRequestDestination" value="%{billingGstThirdPartyRequestDestination}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstThirdPartyRequestDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstThirdPartyRequestDestination}">
								<td><s:textfield id="gstThirdPartyRequestDestination" cssClass="input-text" name="billing.gstThirdPartyRequestDestination" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstThirdPartyRequestDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:8px"></td>
							<td>
							<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1">
							<tr>
							<c:if test="${not empty billing.gstThirdPartyAuthReceivedDestination}">
								<s:text id="billingGstThirdPartyAuthReceivedDestination" name="${FormDateValue}"><s:param name="value" value="billing.gstThirdPartyAuthReceivedDestination"/></s:text>
								<td><s:textfield id="gstThirdPartyAuthReceivedDestination" cssClass="input-text" name="billing.gstThirdPartyAuthReceivedDestination" value="%{billingGstThirdPartyAuthReceivedDestination}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstThirdPartyAuthReceivedDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstThirdPartyAuthReceivedDestination}">
								<td><s:textfield id="gstThirdPartyAuthReceivedDestination" cssClass="input-text" name="billing.gstThirdPartyAuthReceivedDestination" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstThirdPartyAuthReceivedDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<td align="left"><s:textfield cssClass="input-text" name="billing.gstThirdPartyAuthNoDestination" size="22" maxlength="20" tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
			  			</tr>			  
			  			<tr>
							<td align="right" class="listwhitetext" style="width:100px">Shuttle&nbsp;Origin</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<c:if test="${not empty billing.gstShuttleOriginService}">
								<s:text id="billingGstShuttleOriginService" name="${FormDateValue}"><s:param name="value" value="billing.gstShuttleOriginService"/></s:text>
								<td><s:textfield id="gstShuttleOriginService" cssClass="input-text" name="billing.gstShuttleOriginService" value="%{billingGstShuttleOriginService}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstShuttleOriginService-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstShuttleOriginService}">
								<td><s:textfield id="gstShuttleOriginService" cssClass="input-text" name="billing.gstShuttleOriginService" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstShuttleOriginService-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:65px">
							<c:if test="${not empty billing.gstShuttleOriginRequest}">
								<s:text id="billingGstShuttleOriginPartyRequest" name="${FormDateValue}"><s:param name="value" value="billing.gstShuttleOriginRequest"/></s:text>
								<td><s:textfield id="gstShuttleOriginRequest" cssClass="input-text" name="billing.gstShuttleOriginRequest" value="%{billingGstShuttleOriginPartyRequest}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstShuttleOriginRequest-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstShuttleOriginRequest}">
								<td><s:textfield id="gstShuttleOriginRequest" cssClass="input-text" name="billing.gstShuttleOriginRequest" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstShuttleOriginRequest-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:8px"></td>
							<td>
							<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1">
							<tr>
							<c:if test="${not empty billing.gstShuttleOriginAuthReceived}">
								<s:text id="billingGstShuttleOriginAuthReceived" name="${FormDateValue}"><s:param name="value" value="billing.gstShuttleOriginAuthReceived"/></s:text>
								<td><s:textfield id="gstShuttleOriginAuthReceived" cssClass="input-text" name="billing.gstShuttleOriginAuthReceived" value="%{billingGstShuttleOriginAuthReceived}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstShuttleOriginAuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstShuttleOriginAuthReceived}">
								<td><s:textfield id="gstShuttleOriginAuthReceived" cssClass="input-text" name="billing.gstShuttleOriginAuthReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstShuttleOriginAuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<td align="left"><s:textfield cssClass="input-text" name="billing.gstShuttleOriginAuthNo" onchange="changeStatus();" size="22" maxlength="20" tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
			  			</tr>			  
			  			<tr>
							<td align="right" class="listwhitetext" style="width:100px">Shuttle&nbsp;Destination</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<c:if test="${not empty billing.gstShuttleDestService}">
								<s:text id="billingGstShuttleDestService" name="${FormDateValue}"><s:param name="value" value="billing.gstShuttleDestService"/></s:text>
								<td><s:textfield id="gstShuttleDestService" cssClass="input-text" name="billing.gstShuttleDestService" value="%{billingGstShuttleDestService}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstShuttleDestService-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstShuttleDestService}">
								<td><s:textfield id="gstShuttleDestService" cssClass="input-text" name="billing.gstShuttleDestService" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstShuttleDestService-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:65px">
							<c:if test="${not empty billing.gstShuttleDestRequest}">
								<s:text id="billingGstShuttleDestRequest" name="${FormDateValue}"><s:param name="value" value="billing.gstShuttleDestRequest"/></s:text>
								<td><s:textfield id="gstShuttleDestRequest" cssClass="input-text" name="billing.gstShuttleDestRequest" value="%{billingGstShuttleDestRequest}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstShuttleDestRequest-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstShuttleDestRequest}">
								<td><s:textfield id="gstShuttleDestRequest" cssClass="input-text" name="billing.gstShuttleDestRequest" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstShuttleDestRequest-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:8px"></td>
							<td>
							<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1">
							<tr>
							<c:if test="${not empty billing.gstShuttleDestAuthReceived}">
								<s:text id="billingGstShuttleDestAuthReceived" name="${FormDateValue}"><s:param name="value" value="billing.gstShuttleDestAuthReceived"/></s:text>
								<td><s:textfield id="gstShuttleDestAuthReceived" cssClass="input-text" name="billing.gstShuttleDestAuthReceived" value="%{billingGstShuttleDestAuthReceived}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstShuttleDestAuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstShuttleDestAuthReceived}">
								<td><s:textfield id="gstShuttleDestAuthReceived" cssClass="input-text" name="billing.gstShuttleDestAuthReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstShuttleDestAuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<td align="left"><s:textfield cssClass="input-text" name="billing.gstShuttleDestAuthNo" onchange="changeStatus();" size="22" maxlength="20" tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
			 			 </tr>			  
			  			<tr>
							<td align="right" class="listwhitetext" style="width:100px">Access&nbsp;1</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<c:if test="${not empty billing.gstAccess1Service}">
								<s:text id="billingGstAccess1Service" name="${FormDateValue}"><s:param name="value" value="billing.gstAccess1Service"/></s:text>
								<td><s:textfield id="gstAccess1Service" cssClass="input-text" name="billing.gstAccess1Service" value="%{billingGstAccess1Service}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess1Service-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstAccess1Service}">
								<td><s:textfield id="gstAccess1Service" cssClass="input-text" name="billing.gstAccess1Service" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess1Service-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:65px">
							<c:if test="${not empty billing.gstAccess1Request}">
								<s:text id="billingGstAccess1Request" name="${FormDateValue}"><s:param name="value" value="billing.gstAccess1Request"/></s:text>
								<td><s:textfield id="gstAccess1Request" cssClass="input-text" name="billing.gstAccess1Request" value="%{billingGstAccess1Request}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess1Request-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstAccess1Request}">
								<td><s:textfield id="gstAccess1Request" cssClass="input-text" name="billing.gstAccess1Request" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess1Request-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:8px"></td>
							<td>
							<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1">
							<tr>
							<c:if test="${not empty billing.gstAccess1AuthReceived}">
								<s:text id="billingGstAccess1AuthReceived" name="${FormDateValue}"><s:param name="value" value="billing.gstAccess1AuthReceived"/></s:text>
								<td><s:textfield id="gstAccess1AuthReceived" cssClass="input-text" name="billing.gstAccess1AuthReceived" value="%{billingGstAccess1AuthReceived}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstAccess1AuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstAccess1AuthReceived}">
								<td><s:textfield id="gstAccess1AuthReceived" cssClass="input-text" name="billing.gstAccess1AuthReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstAccess1AuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<td align="left"><s:textfield cssClass="input-text" name="billing.gstAccess1AuthNo" onchange="changeStatus();" size="22" maxlength="20" tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
			 			 </tr>			  
			  			<tr>
							<td align="right" class="listwhitetext" style="width:100px">Access&nbsp;2</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<c:if test="${not empty billing.gstAccess2Service}">
								<s:text id="billingGstAccess2Service" name="${FormDateValue}"><s:param name="value" value="billing.gstAccess2Service"/></s:text>
								<td><s:textfield id="gstAccess2Service" cssClass="input-text" name="billing.gstAccess2Service" value="%{billingGstAccess2Service}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess2Service-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstAccess2Service}">
								<td><s:textfield id="gstAccess2Service" cssClass="input-text" name="billing.gstAccess2Service" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess2Service-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:65px">
							<c:if test="${not empty billing.gstAccess2Request}">
								<s:text id="billingGstAccess2Request" name="${FormDateValue}"><s:param name="value" value="billing.gstAccess2Request"/></s:text>
								<td><s:textfield id="gstAccess2Request" cssClass="input-text" name="billing.gstAccess2Request" value="%{billingGstAccess2Request}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess2Request-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstAccess2Request}">
								<td><s:textfield id="gstAccess2Request" cssClass="input-text" name="billing.gstAccess2Request" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess2Request-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:8px"></td>
							<td>
							<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1">
							<tr>
							<c:if test="${not empty billing.gstAccess2AuthReceived}">
								<s:text id="billingGstAccess2AuthReceived" name="${FormDateValue}"><s:param name="value" value="billing.gstAccess2AuthReceived"/></s:text>
								<td><s:textfield id="gstAccess2AuthReceived" cssClass="input-text" name="billing.gstAccess2AuthReceived" value="%{billingGstAccess2AuthReceived}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstAccess2AuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstAccess2AuthReceived}">
								<td><s:textfield id="gstAccess2AuthReceived" cssClass="input-text" name="billing.gstAccess2AuthReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstAccess2AuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<td align="left"><s:textfield cssClass="input-text" name="billing.gstAccess2AuthNo" onchange="changeStatus();" size="22" maxlength="20" tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
			  			</tr>			  
			  			<tr>
							<td align="right" class="listwhitetext" style="width:100px">Access&nbsp;3</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<c:if test="${not empty billing.gstAccess3Service}">
								<s:text id="billingGstAccess3Service" name="${FormDateValue}"><s:param name="value" value="billing.gstAccess3Service"/></s:text>
								<td><s:textfield id="gstAccess3Service" cssClass="input-text" name="billing.gstAccess3Service" value="%{billingGstAccess3Service}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess3Service-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstAccess3Service}">
								<td><s:textfield id="gstAccess3Service" cssClass="input-text" name="billing.gstAccess3Service" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess3Service-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:65px">
							<c:if test="${not empty billing.gstAccess3Request}">
								<s:text id="billingAccess3Request" name="${FormDateValue}"><s:param name="value" value="billing.gstAccess3Request"/></s:text>
								<td><s:textfield id="gstAccess3Request" cssClass="input-text" name="billing.gstAccess3Request" value="%{billingAccess3Request}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess3Request-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstAccess3Request}">
								<td><s:textfield id="gstAccess3Request" cssClass="input-text" name="billing.gstAccess3Request" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstAccess3Request-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:8px"></td>
							<td>
							<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1">
							<tr>
							<c:if test="${not empty billing.gstAccess3AuthReceived}">
								<s:text id="billingGstAccess3AuthReceived" name="${FormDateValue}"><s:param name="value" value="billing.gstAccess3AuthReceived"/></s:text>
								<td><s:textfield id="gstAccess3AuthReceived" cssClass="input-text" name="billing.gstAccess3AuthReceived" value="%{billingGstAccess3AuthReceived}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstAccess3AuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstAccess3AuthReceived}">
								<td><s:textfield id="gstAccess3AuthReceived" cssClass="input-text" name="billing.gstAccess3AuthReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstAccess3AuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<td align="left"><s:textfield cssClass="input-text" name="billing.gstAccess3AuthNo" onchange="changeStatus();" size="22" maxlength="20" tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
			  			</tr>			  
			  			<tr>
							<td align="right" class="listwhitetext" style="width:100px">Extra&nbsp;Stop</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<c:if test="${not empty billing.gstExtraStopService}">
								<s:text id="billingGstExtraStopService" name="${FormDateValue}"><s:param name="value" value="billing.gstExtraStopService"/></s:text>
								<td><s:textfield id="gstExtraStopService" cssClass="input-text" name="billing.gstExtraStopService" value="%{billingGstExtraStopService}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstExtraStopService-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstExtraStopService}">
								<td><s:textfield id="gstExtraStopService" cssClass="input-text" name="billing.gstExtraStopService" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstExtraStopService-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:65px">
							<c:if test="${not empty billing.gstExtraStopRequest}">
								<s:text id="billingGstExtraStopRequest" name="${FormDateValue}"><s:param name="value" value="billing.gstExtraStopRequest"/></s:text>
								<td><s:textfield id="gstExtraStopRequest" cssClass="input-text" name="billing.gstExtraStopRequest" value="%{billingGstExtraStopRequest}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstExtraStopRequest-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstExtraStopRequest}">
								<td><s:textfield id="gstExtraStopRequest" cssClass="input-text" name="billing.gstExtraStopRequest" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstExtraStopRequest-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:8px"></td>
							<td>
							<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1">
							<tr>
							<c:if test="${not empty billing.gstExtraStopAuthReceived}">
								<s:text id="billingGstExtraStopAuthReceived" name="${FormDateValue}"><s:param name="value" value="billing.gstExtraStopAuthReceived"/></s:text>
								<td><s:textfield id="gstExtraStopAuthReceived" cssClass="input-text" name="billing.gstExtraStopAuthReceived" value="%{billingGstExtraStopAuthReceived}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstExtraStopAuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstExtraStopAuthReceived}">
								<td><s:textfield id="gstExtraStopAuthReceived" cssClass="input-text" name="billing.gstExtraStopAuthReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstExtraStopAuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<td align="left"><s:textfield cssClass="input-text" name="billing.gstExtraStopAuthNo" onchange="changeStatus();" size="22" maxlength="20" tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
			  			</tr>			  
			  			<tr>
							<td align="right" class="listwhitetext" style="width:100px">Partial&nbsp;Delivery</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<c:if test="${not empty billing.gstPartialDelivery}">
								<s:text id="billingGstPartialDelivery" name="${FormDateValue}"><s:param name="value" value="billing.gstPartialDelivery"/></s:text>
								<td><s:textfield id="gstPartialDelivery" cssClass="input-text" name="billing.gstPartialDelivery" value="%{billingGstPartialDelivery}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstPartialDelivery-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstPartialDelivery}">
								<td><s:textfield id="gstPartialDelivery" cssClass="input-text" name="billing.gstPartialDelivery" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstPartialDelivery-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:65px">
							<c:if test="${not empty billing.gstPartialDeliveryRequest}">
								<s:text id="billingGstPartialDeliveryRequest" name="${FormDateValue}"><s:param name="value" value="billing.gstPartialDeliveryRequest"/></s:text>
								<td><s:textfield id="gstPartialDeliveryRequest" cssClass="input-text" name="billing.gstPartialDeliveryRequest" value="%{billingGstPartialDeliveryRequest}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstPartialDeliveryRequest-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstPartialDeliveryRequest}">
								<td><s:textfield id="gstPartialDeliveryRequest" cssClass="input-text" name="billing.gstPartialDeliveryRequest" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td><td><img id="gstPartialDeliveryRequest-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</td>
							<td align="left" class="listwhitetext" style="width:8px"></td>
							<td>
							<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1">
							<tr>
							<c:if test="${not empty billing.gstPartialDeliveryAuthReceived}">
								<s:text id="billingGstPartialDeliveryAuthReceived" name="${FormDateValue}"><s:param name="value" value="billing.gstPartialDeliveryAuthReceived"/></s:text>
								<td><s:textfield id="gstPartialDeliveryAuthReceived" cssClass="input-text" name="billing.gstPartialDeliveryAuthReceived" value="%{billingGstPartialDeliveryAuthReceived}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstPartialDeliveryAuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							<c:if test="${empty billing.gstPartialDeliveryAuthReceived}">
								<td><s:textfield id="gstPartialDeliveryAuthReceived" cssClass="input-text" name="billing.gstPartialDeliveryAuthReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onfocus="changeStatus();" tabindex=""/></td>
								<td><img id="gstPartialDeliveryAuthReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" class="listwhitetext" style="width:8px">
							<td align="left"><s:textfield cssClass="input-text" name="billing.gstPartialDeliveryAuthNo" onchange="changeStatus();" size="22" maxlength="20" tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
			  </tr>			  
 		 </tbody>
 	    </table>  		
		<table class="detailTabLabel" border="0"  width="100%"> 		  
 		</table></div></td>
 <td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td></tr>  
 </c:if>
 
 </configByCorp:fieldVisibility>
 </c:if>
	<div id="storageValuationDetail" class="switchgroup1">
		<jsp:include flush="true" page="billingFormStorageValuation.jsp"></jsp:include>
	</div>
 		  <c:if test="${storageJob != 'true'}">
 		    <c:if test="${serviceOrder.job !='RLO'}"> 
 		   <tr>
	<td height="10" align="left" class="listwhitetext">
	 <div  onClick="javascript:animatedcollapse.toggle('insurance');showDetailed();" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Valuation&nbsp;/&nbsp;Insurance

</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center" >&nbsp;</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
		<div id="insurance">
 		<table class="detailTabLabel" border="0" style="margin-left:-6px" >
 		  <tbody>
 		  	<tr><c:set var="fullDayFlag" value="false"/>
				<c:if test="${billing.signedDisclaimer}">
					<c:set var="fullDayFlag" value="true"/>
				</c:if>
	  			<td align="right" width="" class="listwhitetext"><fmt:message key="billing.insuranceHas"/></td>
	  			<td align="left" colspan="1"><configByCorp:customDropDown 	listType="map" list="${hasval}" fieldValue="${billing.insuranceHas}"
		attribute="id=billingForm_billing_insuranceHas class=list-menu name=billing.insuranceHas style=width:118px  headerKey='' headerValue='' onchange='fillVendorName();changeStatus();makeMandIssueDt();showIndicatorFields();'; tabindex='' "/></td>
<%-- 	  			<s:select cssClass="list-menu" name="billing.insuranceHas" list="%{hasval}" cssStyle="width:128px" onchange="fillVendorName();changeStatus();makeMandIssueDt()" tabindex=""/></td>
 --%>
 <configByCorp:fieldVisibility componentId="component.field.billing.certificateNumber">		
 <td align="right" width="" class="listwhitetext">Valuation</td>
 <td align="left" colspan="1"><configByCorp:customDropDown 	listType="map" list="${valuationList}" fieldValue="${billing.valuation}"
		attribute="id=billingForm_billing_valuation class=list-menu name=billing.valuation style=width:128px  headerKey='' headerValue='' onchange=''; tabindex='' "/></td>	
</configByCorp:fieldVisibility>
 <td valign="bottom" colspan="2">
	  			<div id="disDiv" style="height:20px">
	  			<table style="vertical-align:bottom;margin-bottom: 0px;" >
	  			<tr>
				<td align="right" class="listwhitetext" style="padding-left:77px;">Obtain&nbsp;Waiver/Signed&nbsp;Disclaimer</td>
				<td><s:checkbox key="billing.signedDisclaimer" value="${fullDayFlag}" fieldValue="true" onclick="changeStatus();" tabindex=""/></td>
				</tr></table>
				</div>
				</td>
			</tr>	
			<tr>
				<td valign="middle" colspan="8" style="margin:0px;padding:0px; ">
				<div id="disDiv1" style="margin:0px;padding:0px;  ">
		  			<table class="detailTabLabel" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="1" >
		  			<tr>
					<td align="right" class="listwhitetext" width="125px">Vendor&nbsp;Code&nbsp;</td>
					<c:if test="${accountInterface!='Y'}">	
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="margin-left:3px;" key="billing.vendorCode" id="VendorCodeId" readonly="false" size="7" maxlength="8"
												onchange="checkVendorName();findPayableCurrencyCodeAjax();changeStatus();" tabindex=""/></td> 
					<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen()"
									id="openpopup9.img"	src="<c:url value='/images/open-popup.gif'/>" /></td>
					</c:if>
					<c:if test="${accountInterface=='Y'}">
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text"  cssStyle="margin-left:3px;" key="billing.vendorCode" id="VendorCodeId" readonly="false" size="7" maxlength="8"
												onchange="checkVendorName();findPayableCurrencyCodeAjax();changeStatus();" tabindex="" /></td> 
					<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen()"
									id="openpopup9.img"	src="<c:url value='/images/open-popup.gif'/>" /></td>							
					</c:if>
					<td align="right" class="listwhitetext">&nbsp;Vendor&nbsp;Name&nbsp;</td>
					<td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" key="billing.vendorName" cssStyle="width:224px;" id="VendorNameDivId" onchange="changeStatus();checkVendorName();" onkeyup="findPartnerDetails('VendorNameDivId','VendorCodeId','VendorName1DivId',' and (isVendor=true or isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true )','',event);"  size="43" tabindex=""/><div id="VendorName1DivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div></td>
					<td align="right" class="listwhitetext" width="106px"><fmt:message key="billing.charge"/>&nbsp;</td>
					<td align="left"><s:textfield id="billingInsCharge" name="billing.insuranceCharge" cssStyle="width:131px;"  onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivIdC','insuranceCharge','C')" onblur="checkValueAfterChargeCode('insuranceCharge')" onkeydown="return onlyAlphaNumericAllowed(event)" onchange="changeStatus();" required="true"  cssClass="input-text" maxlength="25" readonly="${readableChargeCode}" size="23" tabindex=""/>
		    	 <img class="openpopup" align="top" width="17" height="20" onclick="chkInsurance();" id="openpopup6.img" src="<c:url value='/images/open-popup.gif'/>" />
		    	 <div id="ChargeNameDivIdC" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
		    	 </td>
					</tr></table>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key="billing.insuranceOptionCode"/></td>
				
				<s:hidden name="billing.insuranceOption"/>
				<s:hidden name="billing.insuranceOptionCode"/>
				<td align="left" colspan="6"><configByCorp:customDropDown 	listType="map" list="${insopt}" fieldValue="${billing.insuranceOptionCodeWithDesc}"
		attribute="id=billingForm_billing_insuranceOptionCodeWithDesc class=list-menu name=billing.insuranceOptionCodeWithDesc style=width:378px  headerKey='' headerValue='' onchange='insValues(this);baseRateValues(this);changeStatus();autoPopulate_billingForm_insuranceOption(this);calculateTotal();' tabindex='' "/></td>
<%-- 				<s:select cssClass="list-menu" name="billing.insuranceOptionCodeWithDesc" list="%{insopt}" cssStyle="width:390px" onchange="insValues(this);baseRateValues(this);changeStatus();autoPopulate_billingForm_insuranceOption(this);calculateTotal();" tabindex="" /></td>
 --%>			</tr> 		

			<tr>
				<td align="right" width="120px" class="listwhitetext"><fmt:message key="billing.insuranceValueEntitle"/></td>
				<td align="left"><s:textfield name="billing.insuranceValueEntitle" cssStyle="text-align:right;width:111px;" onchange="onlyFloat(this);changeStatus();" required="true" cssClass="input-text" maxlength="15" size="15" tabindex=""/></td>
				
				<td align="right" width="150px" class="listwhitetext">Entitled&nbsp;Insur&nbsp;Value&nbsp;Currency</td>
		    	<td align="left" colspan="1"><configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.insuranceValueEntitleCurrency}"
		attribute="id=billingForm_billing_insuranceValueEntitleCurrency class=list-menu name=billing.insuranceValueEntitleCurrency  style=width:102px  headerKey='' headerValue='' onchange='changeStatus()'; tabindex='' "/></td>
<%-- 		    	<s:select name="billing.insuranceValueEntitleCurrency" list="%{currency}" onchange="changeStatus();" cssClass="list-menu" cssStyle="width:105px" headerKey="" headerValue="" tabindex=""/></td>
 --%>				<td valign="middle" colspan="2" style="margin:0px;padding:0px; ">
				<c:if test="${serviceOrder.job =='STO' || serviceOrder.job =='RLO'}"> 
					<s:hidden name="billing.estMoveCost"/>
				</c:if>	 
				<c:if test="${serviceOrder.job != 'STO' && serviceOrder.job != 'RLO'}"> 
					<div id="displayDiv">
		  			<table style="vertical-align:bottom;margin-bottom: 0px;" >
		  			<tr>
						<td align="right" class="listwhitetext" width="145">Est.&nbsp;Move&nbsp;Cost&nbsp;</td>
						<td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" key="billing.estMoveCost" onchange="changeStatus();" maxlength="10" size="11" tabindex=""/></td>
					</tr>
					</table>
				    </div>
				</c:if>
				<div id="disDiv2" ></div>   
				</td>			    			
			</tr>
			<configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">	
	        <c:if test="${serviceOrder.job =='INT'}"> 	
			<tr>
				<td align="right" class="listwhitetext">Auto Billed Rate </td>
				<td align="left" ><s:textfield name="billing.autoBilledRate" cssStyle="text-align:right"  cssClass="input-text" maxlength="15" size="15"  onchange="onlyFloat(this);" tabindex="" /></td>
				<td align="right" class="listwhitetext">Auto Insured Value </td>
				<td align="left" ><s:textfield name="billing.autoInsuredValue" cssStyle="text-align:right" cssClass="input-text" maxlength="15" size="16"  onchange="onlyFloat(this);" tabindex="" /></td>
				<td align="left" class="listwhitetext" style="padding-left:45px;">Auto Internal Rate 
				<s:textfield name="billing.autoInternalRate" cssStyle="text-align:right"  cssClass="input-text" maxlength="15" size="15"   onchange="onlyFloat(this);" tabindex="" /></td>
				</tr>
		    	</c:if>
		    	</configByCorp:fieldVisibility>
			<tr>
				<td align="right" class="listwhitetext">Insurance&nbsp;Value</td>
				<td align="left" ><s:textfield name="billing.insuranceValueActual" cssStyle="text-align:right;width:111px;" required="true" cssClass="input-text" maxlength="15" size="15" onchange="setDefaultvalue(this);calculateTotal(); onlyFloat(this);calBaseInsVal();changeStatus();makeMandIssueDt()" tabindex="" /></td>
				
		    	<td align="right" width="" class="listwhitetext">Insurance&nbsp;Value&nbsp;Currency</td>
		    	<td align="left" colspan="1"><configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.currency}"
		attribute="id=billingForm_billing_currency class=list-menu name=billing.currency  style=width:102px  headerKey='' headerValue='' onchange='findExchangeEstRate();changeStatus();'; tabindex='' "/></td>

<%-- 		    	<s:select name="billing.currency" list="%{currency}" cssClass="list-menu" cssStyle="width:105px" headerKey="" headerValue="" onchange="findExchangeEstRate();changeStatus();" tabindex=""/></td>
 --%>		    	<td align="left" colspan="2" class="listwhitetext">&nbsp;&nbsp;&nbsp;Insurance&nbsp;Payable&nbsp;Currency&nbsp;
		    	
		    	<configByCorp:customDropDown 	listType="map" list="${currency}" fieldValue="${billing.billingInsurancePayableCurrency}"
		attribute="id=billingForm_billing_billingInsurancePayableCurrency class=list-menu name=billing.billingInsurancePayableCurrency  style=width:80px;  headerKey='' headerValue='' onchange='changeStatus();'; tabindex='' "/></td>
		    	
<%-- 		    	<s:select name="billing.billingInsurancePayableCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:80px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
 --%>			</tr>
			<tr>
				<td align="right" class="listwhitetext" width="120px"><div id="isuDtDiv1" ><fmt:message key="billing.issueDate"/></div><div id="isuDtDiv2" style="display:none"><font color="red" size="2">*</font><fmt:message key="billing.issueDate"/></div></td>				
				<td align="left">
				<c:if test="${not empty billing.issueDate}">
					<s:text id="FormatedIssueDate" name="${FormDateValue}"><s:param name="value" value="billing.issueDate"/></s:text>
				    <s:textfield id="billingForm_billing_issueDate" name="billing.issueDate" value="%{FormatedIssueDate}" onkeydown="onlyDel(event,this)" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:70px" tabindex=""/>
				</c:if>
				<c:if test="${empty billing.issueDate}">
					<s:textfield id="billingForm_billing_issueDate" name="billing.issueDate" onkeydown="onlyDel(event,this)" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:70px" tabindex=""/>
				</c:if>
				<img id="billingForm_billing_issueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_issueDate"/></td>
				
				<td align="right" width="" class="listwhitetext"><fmt:message key="billing.certnum"/></td>
				<td align="left" colspan="1"><s:textfield name="billing.certnum" cssStyle="width:98px"  onchange="changeStatus();makeMandIssueDt()" required="true" cssClass="input-text" maxlength="15" size="16" tabindex=""/></td>
				<c:if test="${serviceOrder.job =='STO' || serviceOrder.job =='RLO'}"> 
					<s:hidden name="billing.additionalNo"/>
				</c:if>	 
				<c:if test="${serviceOrder.job != 'STO' && serviceOrder.job != 'RLO'}"> 				
				<td colspan="5">
				<div id="displayDivAdditionalNo" >
		  			<table style="vertical-align:bottom;margin-bottom:0px;" >
		  			<tr>
		  			<td align="right" width="88" class="listwhitetext"></td>
				<td align="right" width="" class="listwhitetext">Additional&nbsp;#</td>
				<td align="left" ><s:textfield name="billing.additionalNo" onchange="changeStatus();" readonly="true" cssClass="input-textUpper" size="20" tabindex=""/><img id="autoAdditionalImage" class="openpopup" align="top" onclick="findAdditionalNo();" src="<c:url value='/images/arrow-loadern.gif'/>"/></td>
				</tr>				
				</table>
				</div>
				</td>				
				</c:if>				
			</tr>	
			<tr>	
				<td align="right" class="listwhitetext">Insurance&nbsp;Sell&nbsp;Rate&nbsp;%</td>
				<td align="left"><s:textfield name="billing.insuranceRate" cssStyle="text-align:right;width:111px;" required="true" cssClass="input-text" maxlength="15" size="15" onchange="calculateTotal(); onlyFloat(this); changeStatus();" tabindex=""/></td>
				
				<td align="right" class="listwhitetext">Ins.Total&nbsp;</td>
				<td align="left"><s:textfield name="billing.totalInsrVal" cssStyle="text-align:right; width:98px" readonly="true" cssClass="input-text" maxlength="15" size="10" onchange="insOption(); onlyFloat(this);changeStatus();" tabindex="" /></td>
				<c:if test="${serviceOrder.job =='STO' || serviceOrder.job =='RLO'}"> 
					<s:hidden name="billing.deductible"/>
				</c:if>	 
				<c:if test="${serviceOrder.job != 'STO' && serviceOrder.job != 'RLO'}"> 
				<div >
				<td id="displayDivDeductible" align="right">
				<table style="vertical-align: bottom; margin-bottom: 0px; padding-left: 71px;" >
	  			<tr>
				<td align="right" class="listwhitetext">Deductible&nbsp;Amt.</td>
				<td align="left" style="">
				<configByCorp:customDropDown 	listType="map" list="${claimDeductible}" fieldValue="${billing.deductible}"
		attribute="id=billingForm_billing_deductible class=list-menu name=billing.deductible  style=width:80px  headerKey='' headerValue='' onchange='changeStatus();'; tabindex='' "/></td>
<%-- 				<s:select cssClass="list-menu" name="billing.deductible" list="%{claimDeductible}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:80px" tabindex=""/></td>
 --%>				</tr>
				</table></td>
				</div>
				</c:if>				
						<c:if test="${empty billing.id}">
							<td width="105" align="right" colspan="3"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty billing.id}">
							<c:choose>
								<c:when test="${countBillingValuationNotes == '0' || countBillingValuationNotes == '' || countBillingValuationNotes == null}">
								<td width="105" align="right" colspan="3"><img id="countBillingValuationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td width="105" align="right" colspan="3"><img id="countBillingValuationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BillingValuation&imageId=countBillingValuationNotesImage&fieldId=countBillingValuationNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>				
			</tr>
			<tr>
				<td align="right" class="listwhitetext">Base&nbsp;Insurance&nbsp;Value</td>
				<td align="left"> <s:textfield name="billing.baseInsuranceValue" cssStyle="text-align:right;width:111px;" cssClass="input-text" maxlength="10" size="15" onchange="onlyFloat(this);calBaseTotal();calInsval();changeStatus();" tabindex="" /></td>
				<td align="right" class="listwhitetext">Insurance&nbsp;Buy&nbsp;Rate&nbsp;%</td>
				<td align="left"> <s:textfield name="billing.insuranceBuyRate" cssStyle="text-align:right; width:98px" cssClass="input-text" maxlength="10" size="10" onchange="onlyFloat(this);calBaseTotal();changeStatus();" tabindex="" /></td>
				<td align="right" class="listwhitetext" width="180">Base&nbsp;Ins.Total
			    <s:textfield name="billing.baseInsuranceTotal" cssStyle="text-align:right;width:78px;margin-right: 3px;" cssClass="input-textUpper" maxlength="10" size="9" onchange="onlyFloat(this);changeStatus();" tabindex="" readonly="true" /></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext">Exchange&nbsp;Rate</td>
				<td align="left"> <s:textfield name="billing.exchangeRate" cssStyle="text-align:right;width:111px;" cssClass="input-text" maxlength="10" size="15" onchange="onlyFloat(this);changeStatus();"  onkeydown="return onlyFloatNumsAllowed(event);" tabindex="" /></td>
				<td align="right" class="listwhitetext">Exchange&nbsp;Date</td>
				<td align="left" colspan="2">
				<c:if test="${not empty billing.valueDate}">
					<s:text id="formatedValueDate" name="${FormDateValue}"><s:param name="value" value="billing.valueDate"/></s:text>
				    <s:textfield id="billingForm_billing_valueDate" name="billing.valueDate" value="%{formatedValueDate}" onkeydown="onlyDel(event,this);" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<c:if test="${empty billing.valueDate}">
					<s:textfield id="billingForm_billing_valueDate" name="billing.valueDate" onkeydown="onlyDel(event,this)" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<img id="billingForm_billing_valueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_valueDate"/></td>
				</td>
			</tr>
			 <c:if test="${usertype!='ACCOUNT' || usertype!='AGENT'}">
			<tr>
			   <td align="right" class="listwhitetext">Cportal&nbsp;Insurance&nbsp;Options</td>
			   <%-- <td><s:select cssClass="list-menu" name="billing.noInsurance" list="{}" id="insuranceNo" onchange="showDetailed();" headerKey="" headerValue="" cssStyle="width:120px" /></td> --%>
			  <td><s:textfield name="billing.noInsurance" onchange="changeStatus();" cssClass="input-textUpper" cssStyle="width:111px;" readonly="true" ></s:textfield> </td>
			   <td align="right" class="listwhitetext">Insurance&nbsp;Submit&nbsp;Date</td>
			   <td align="left">
				<c:if test="${not empty billing.insSubmitDate}">
					<s:text id="formatedValueDate" name="${FormDateValue}"><s:param name="value" value="billing.insSubmitDate"/></s:text>
				    <s:textfield id="billingForm_billing_insSubmitDate" name="billing.insSubmitDate" value="%{formatedValueDate}" onkeydown="onlyDel(event,this)" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<c:if test="${empty billing.insSubmitDate}">
					<s:textfield id="billingForm_billing_insSubmitDate" name="billing.insSubmitDate" onkeydown="onlyDel(event,this)" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<img id="billingForm_billing_insSubmitDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_insSubmitDate"/></td>
				
				<configByCorp:fieldVisibility componentId="component.field.billing.recSignedDisclaimerDate">
				<td align="right" class="listwhitetext" width="235">Received&nbsp;Signed&nbsp;Disclaimer&nbsp;				
				<c:if test="${not empty billing.recSignedDisclaimerDate}">
					<s:text id="formatedValueDate" name="${FormDateValue}"><s:param name="value" value="billing.recSignedDisclaimerDate"/></s:text>
				    <s:textfield id="billingForm_billing_recSignedDisclaimerDate" name="billing.recSignedDisclaimerDate" value="%{formatedValueDate}" onkeydown="onlyDel(event,this);" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<c:if test="${empty billing.recSignedDisclaimerDate}">
					<s:textfield id="billingForm_billing_recSignedDisclaimerDate" name="billing.recSignedDisclaimerDate" onkeydown="onlyDel(event,this)" onchange="changeStatus();" readonly="true" cssClass="input-text" cssStyle="width:65px;" tabindex=""/>
				</c:if>
				<img id="billingForm_billing_recSignedDisclaimerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 textelementname="billingForm_billing_recSignedDisclaimerDate"/>
				</td>
				</configByCorp:fieldVisibility>
			
			<td id="detailed12" style="display:none;" ><input id="detailed" class="cssbutton" type="button" name="detailedList" style="margin-right: 5px;height: 25px;width:120px; font-size:15" value="Detailed List"
			onclick="return opendetailedList();" tabindex=""/>  </td>
			</tr>
			</c:if>
			<c:if test="${indicatorFieldsView=='Y'}">
				<c:if test="${serviceOrder.job == 'INT'}">
					<tr>
						<td colspan="10">
							<table id="indicatorFieldsDiv">
								<td align="right" width="120px" class="listwhitetext">Total&nbsp;Loss&nbsp;Coverage Indicator</td>
						  		<td align="left" ><s:select cssClass="list-menu" name="billing.totalLossCoverageIndicator" list="{'Y','N'}" headerKey="" headerValue="" cssStyle="width:40px" onchange="" /></td>
						  		<td align="right" width="240px" class="listwhitetext">Mechanical Malfunction Indicator</td>
						  		<td align="left" ><s:select cssClass="list-menu" name="billing.mechanicalMalfunctionIndicator" list="{'Y','N'}" headerKey="" headerValue="" cssStyle="width:40px" onchange="" /></td>
						  		<td align="right" width="150px" class="listwhitetext">Pairs Or Sets Indicator</td>
						  		<td align="left" ><s:select cssClass="list-menu" name="billing.pairsOrSetsIndicator" list="{'Y','N'}" headerKey="" headerValue="" cssStyle="width:40px" onchange="" /></td>
						  		<td align="right" width="140px" class="listwhitetext">Mold Or Mildew Indicator</td>
						  		<td align="left" ><s:select cssClass="list-menu" name="billing.moldOrMildewIndicator" list="{'Y','N'}" headerKey="" headerValue="" cssStyle="width:40px" onchange="" /></td>
							</table>
						</td>
					</tr>
				</c:if>
				<c:if test="${serviceOrder.job != 'INT'}">
					<s:hidden name="billing.totalLossCoverageIndicator"/>
					<s:hidden name="billing.mechanicalMalfunctionIndicator"/>
					<s:hidden name="billing.pairsOrSetsIndicator"/>
					<s:hidden name="billing.moldOrMildewIndicator"/>
				</c:if>
			</c:if> 
	      </tbody>
 		</table>
 		</div>
 		</td>
 		 </tr>  
 		 </c:if>
 		 <c:if test="${serviceOrder.job =='RLO'}"> 
 		 <s:hidden name="billing.vendorCode" />
 		 <s:hidden name="billing.insuranceHas" value=""/>
 		 </c:if>
 		  <tr>
 		  </c:if>
 		  </tr>  		    
 		  <tr height="15px"></tr>
  </tbody>
  </table>
  </div>
<div class="bottom-header" style="margin-top:40px;"><span></span></div>
</div>
</div>
<table border="0" width="">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.createdOn'/></b></td>							
							<td valign="top"></td>
							<td style="width:120px">
							<fmt:formatDate var="billingCreatedOnFormattedValue" value="${billing.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="billing.createdOn" value="${billingCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${billing.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.createdBy' /></b></td>
							<c:if test="${not empty billing.id}">
								<s:hidden name="billing.createdBy"/>
								<td style="width:105px"><s:label name="createdBy" value="%{billing.createdBy}"/></td>
							</c:if>
							<c:if test="${empty billing.id}">
								<s:hidden name="billing.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:105px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedOn'/></b></td>
							<fmt:formatDate var="billingupdatedOnFormattedValue" value="${billing.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="billing.updatedOn" value="${billingupdatedOnFormattedValue}"/>
							<td style="width:140px"><fmt:formatDate value="${billing.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='billing.updatedBy' /></b></td>
							<c:if test="${not empty billing.id}">
								<s:hidden name="billing.updatedBy"/>
								<td style="width:135px"><s:label name="updatedBy" value="%{billing.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty billing.id}">
								<s:hidden name="billing.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:135px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>  
 			</div>        
        <s:submit cssClass="cssbuttonA" method="save" id="saveButton" key="button.save" onclick="enableCheckBox();" onmouseover="checkHasValuation();" tabindex="" />
       
        <s:reset type="button" cssClass="cssbutton1"  cssStyle="margin-left:10px;" id="reset" key="button.reset" tabindex="" onclick="resetIssueDt();resetIndicatorsFields();"/> 
   		<s:hidden name="billing.sequenceNumber" />
		<s:hidden name="billing.corpID"/>
		<s:hidden name="billing.insurancePolicy"/>
		<s:hidden name="billing.insuranceThrough"/>
		<s:hidden name="billing.serial"/>
		<s:text id="billingSystemDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="billing.systemDate" /></s:text>
 		<s:hidden name="billing.systemDate" value="%{billingSystemDateFormattedValue}" />
		<s:hidden name="billing.customerFileNumber"/>
		<s:hidden name="secondDescription" />
		<s:hidden name="thirdDescription" />
		<s:hidden name="firstDescription" />
		<s:hidden name="fourthDescription" />
		<s:hidden name="fifthDescription" />
		<s:hidden name="sixthDescription" />
		<s:hidden name="seventhDescription"/>
		<s:hidden name="eigthDescription" />
	    <s:hidden name="ninthDescription" />
	    <s:hidden name="tenthDescription" />
		<s:hidden name="customerFile.id"/>
		<s:hidden name="shipSize" />
		<s:hidden name="minShip" />
		<s:hidden name="countShip"/>
		<s:hidden name="billToContract"  value="<%=request.getParameter("billToContract") %>" />
		<s:hidden name="bookingAgentContract"  value="<%=request.getParameter("bookingAgentContract") %>" />
		<s:hidden name="billJob"  value="<%=request.getParameter("billJob") %>" />
		<s:hidden name="billCreatedOn"  value="<%=request.getParameter("billCreatedOn") %>" />
		<s:hidden name="billing.storageVatExclude"   />
		<s:hidden name="billing.insuranceVatExclude"  />
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
    <c:set var="tableName" value="billing" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
</div>
<s:hidden name="description"/>

<div id="loader" style="text-align:center; display:none">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
					<tr><td height="200px"></td></tr>
					<tr>
				       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
				           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
				       </td>
				    </tr>
				    <tr>
				      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
				           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
				       </td>
				    </tr>
		       </table>
		     </td>
	  	</tr>
	</table>
</div>
</s:form>
<%-- <%@ include file="/WEB-INF/pages/trans/billingFormJS.jsp"%> --%>
<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
	animatedcollapse.addDiv('status', 'fade=1,hide=1')
	animatedcollapse.addDiv('sec', 'fade=1,hide=1')
	animatedcollapse.addDiv('private', 'fade=1,hide=1')
	animatedcollapse.addDiv('payvat', 'fade=1,hide=1')
	animatedcollapse.addDiv('domestic', 'fade=1,hide=1')
	animatedcollapse.addDiv('gstAutho', 'fade=1,hide=1')
	animatedcollapse.addDiv('storage', 'fade=1,hide=1')
	animatedcollapse.addDiv('insurance', 'fade=1,hide=1')
	animatedcollapse.init()
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
function checkShowHideNetworkBillToCode(temp){
	<c:if test="${(!(networkAgent))}">
	var billingDMMContractTypePage = document.forms['billingForm'].elements['billingDMMContractTypeCheck'].value; 
	if(billingDMMContractTypePage=='true'){
		document.getElementById('showHideNetworkBillToCode').style.display = 'block'; 
		var billingNetworkBillToCode=document.forms['billingForm'].elements['billing.networkBillToCode'].value;
		billingNetworkBillToCode=billingNetworkBillToCode.trim();
		if(billingNetworkBillToCode=='' && temp=='changeData'){
			document.forms['billingForm'].elements['billing.networkBillToCode'].value="${customerFile.accountCode}";	
			document.forms['billingForm'].elements['billing.networkBillToName'].value="${customerFile.accountName}";
			document.forms['billingForm'].elements['formStatus'].value = '1';
		}
	}else{
		document.getElementById('showHideNetworkBillToCode').style.display = 'none';
	}
	</c:if>
	
}
function showDetailed(){
	 var noInsurance11 = document.forms['billingForm'].elements['billing.noInsurance'].value;
	 var noInsurance=noInsurance11.trim();
	 if(noInsurance!='' && noInsurance!='No Insurance'){
		document.getElementById('detailed12').style.display = 'block';
	 }else if(noInsurance=='No Insurance'){
		 document.getElementById('detailed12').style.display = 'none';
	 }else {
		 document.getElementById('detailed12').style.display = 'none';
	 }
}
function InsuranceValue(){
	var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	var url="InsuranceGetValue.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
    http257.open("GET", url, true);
    http257.onreadystatechange = handleHttpResponseInsuranceValue;
    http257.send(null);	
}
function handleHttpResponseInsuranceValue(){
	 var results = http257.responseText
      results = results.trim();
	 var res = results.replace("[",'');
     res = res.replace("]",'');
	 res = res.split(",");
   var targetElement=document.forms['billingForm'].elements['billing.noInsurance'];
	     targetElement.length= res.length;
		 for(i=0; i<res.length; i++){
	     		if(res[i] == ''){
	     		}else{
	     		document.forms['billingForm'].elements['billing.noInsurance'].options[i].text =res[i].trim();
	     		document.forms['billingForm'].elements['billing.noInsurance'].options[i].value= res[i].trim();
	     		if (document.getElementById("insuranceNo").options[i].value == '${billing.noInsurance}'){
					   document.getElementById("insuranceNo").options[i].defaultSelected = true;
					}
		         }
    }
}
function opendetailedList(){
	 var noInsurance11= document.forms['billingForm'].elements['billing.noInsurance'].value;
	 var noInsurance=noInsurance11.trim();
	 if(noInsurance=='Detailed List' && noInsurance!='' && noInsurance!='No Insurance'){
		 openWindow('insuranceDetailedList.html?sid=${serviceOrder.id}&commodity=${serviceOrder.commodity}&decorator=popup&popup=true',800,450);
	 }else if(noInsurance!='Detailed List' && noInsurance!='' && noInsurance!='No Insurance'){
		 openWindow('insuranceLumpSumAmountList.html?sid=${serviceOrder.id}&commodity=${serviceOrder.commodity}&noInsuranceValue='+noInsurance11+'&decorator=popup&popup=true',500,450); 
	 }  
   return true;
	}
 function test(){
			return false;
		}
 function test1(){
		var img = this;
		img.onclick = false;
		return false;
	}		
		function trap1(){
			 if(document.images)
			    {
			      var totalImages = document.images.length;
			      	for (var i=0;i<totalImages;i++) {
				      	try{
							if(document.images[i].src.indexOf('images/navarrow.gif')>0) { 
								var el = document.getElementById(document.images[i].id);
								if((el.getAttribute("id")).indexOf('-trigger')>0){
									el.removeAttribute("id");
								} } }
			      	catch(e){}
						} }  }
		function trap(){
		  if(document.images){
		      var totalImages = document.images.length;
		       	for (var i=0;i<totalImages;i++)
					{
						if(document.images[i].src.indexOf('calender.png')>0) 	{ 
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('open-popup.gif')>0) { 
								var el = document.getElementById(document.images[i].id);
								if(document.images[i].id!='openpopup4.img' && document.images[i].id!='openpopup5.img' &&  document.images[i].id!='openpopup6.img'  &&  document.images[i].id!='openpopup9.img')
								{
										el.onclick = test;
										document.images[i].src = 'images/navarrow.gif'; 
								} 
						}
						if(document.images[i].src.indexOf('notes_empty1.jpg')>0) 	{
								var el = document.getElementById(document.images[i].id);
								el.onclick = test;
						        document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('notes_open1.jpg')>0)
						{
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}  
					}
		    }
		  }
 function accordingBillToCode(){	
	 var billToContr = document.forms['billingForm'].elements['billing.billToCode'].value;
	  document.forms['billingForm'].elements['billToContract'].value = billToContr;
	 var job = document.forms['billingForm'].elements['serviceOrder.job'].value;
	     document.forms['billingForm'].elements['billJob'].value = job;
	 var create = document.forms['billingForm'].elements['billing.createdOn'].value;
	   document.forms['billingForm'].elements['billCreatedOn'].value = create;
	 var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value;
	 var htsContract; 
	 var bookingAgentContr = document.forms['billingForm'].elements['serviceOrder.bookingAgentCode'].value;
	  document.forms['billingForm'].elements['bookingAgentContract'].value = bookingAgentContr;
	 
	 try{
		htsContract= document.forms['billingForm'].elements['billing.historicalContracts'].checked;
	 }catch(e){
		 htsContract=false;
	}
	 var historicalContractsValue;
	 if(htsContract){
		 historicalContractsValue="true";
	 }else{
		 historicalContractsValue="false";
		 }
     var url="getContract.html?ajax=1&decorator=simple&popup=true&billToContract="+encodeURI(billToContr)+"&billJob="+encodeURI(job)+"&billCreatedOn="+encodeURI(create)+"&custCompanyDivision="+encodeURI(companyDivision)+"&historicalContractsValue="+historicalContractsValue+"&bookingAgentContract="+encodeURI(bookingAgentContr);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse49;
     http3.send(null); 
}    
function findQuantity(){
	document.getElementById('rateImage').src = "<c:url value='/images/loader-move.gif'/>";
    var ship = document.forms['billingForm'].elements['billing.shipNumber'].value;
    var url="maxLineNumberList.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(ship);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse9;
     http2.send(null);
} 
// function to check the review status from work ticket. 
function findReviewStatus(){	
	 var ship = document.forms['billingForm'].elements['billing.shipNumber'].value;
     var url="reviewStatusList.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(ship);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse999;
     http2.send(null);
}
function handleHttpResponse999(){ 
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');
                if(results.length >=1){	
                	document.forms['billingForm'].elements['billing.billComplete'].readOnly = true;
                	var oimageDiv=document.getElementById('myimageDiv')
					oimageDiv.style.display=(oimageDiv.style.display=='none')?'none':'none'
                }
               }
        }
function findBillingCompleteA(){
     var ship = document.forms['billingForm'].elements['billing.shipNumber'].value;
     var url="reviewStatusList.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(ship);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse9999;
     http2.send(null);
}

function handleHttpResponse9999(){ 
             if (http2.readyState == 4)   {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results.length >=1)  {	
                	alert('Billing cannot be marked complete as there are work tickets which have not been cleared. Please click the Ticket List to see open tickets and clear them first before marking the billing complete.');
		            document.forms['billingForm'].elements['billingBillCompleteVar'].value='0';
		            document.forms['billingForm'].elements['billing.billComplete'].value ='';
		            document.getElementById("billingForm").reset();
                }else{
                	findBillingCompleteAccrueAudit();
                }
               }
        } 
        
// End of function. 
 function findBillingCompleteAudit(){
     var ship = document.forms['billingForm'].elements['billing.shipNumber'].value;
     var url="reviewStatusList.html?ajax=1&decorator=simple&popup=true&shipNumber="+encodeURI(ship);
     http555.open("GET", url, true);
     http555.onreadystatechange = handleHttpResponse9991;
     http555.send(null);
} 
function handleHttpResponse9991()
        { 
             if (http555.readyState == 4)  {
                var results = http555.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results.length >=1)
                {	
                	alert('Billing cannot be marked complete as there are work tickets which have not been cleared. Please click the Ticket List to see open tickets and clear them first before marking the billing complete.');
		            document.forms['billingForm'].elements['billingBillCompleteVar'].value='0';
		            new Calendar().hide();
		            document.forms['billingForm'].elements['billing.auditComplete'].focus();
		            document.getElementById("billingForm").reset();
                }else{
                	findBillingCompleteAccrueAudit();
                }
               }
        }

function findBillingCompleteAccrueAudit(){ 
	<configByCorp:fieldVisibility componentId="component.billing.accuralBillingComplete">
	var sid = document.forms['billingForm'].elements['sid'].value;
    var url="reviewAccrueStatusList.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid);
    http5555.open("GET", url, true);
    http5555.onreadystatechange = handleHttpResponseAccrue;
    http5555.send(null);
    </configByCorp:fieldVisibility>	
}

function handleHttpResponseAccrue(){ 
     if (http5555.readyState == 4)  {
        var results = http5555.responseText
        results = results.trim();
        results = results.replace('[','');
        results=results.replace(']',''); 
        if(results.length >=1)
        {	
        	alert('There are open accrual(s) on line # '+results);
            document.forms['billingForm'].elements['billingBillCompleteVar'].value='0';
            new Calendar().hide();
            document.forms['billingForm'].elements['billing.auditComplete'].focus();
            document.getElementById("billingForm").reset();
        } 
       }
}
// End of function. 
// auth updated date  
function authUpdatedDate() {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";if(month == 2)month="Feb";if(month == 3)month="Mar";if(month == 4)month="Apr";if(month == 5)month="May";if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		document.forms['billingForm'].elements['billing.authUpdated'].value=datam;
	} 
// End of function  
function computeCurrentCharges(fieldId){
    var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
    var onHand = document.forms['billingForm'].elements['billing.onHand'].value;
    var cycle = document.forms['billingForm'].elements['billing.cycle'].value;
    var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
    var payableRate = document.forms['billingForm'].elements['billing.payableRate'].value;
    var vendorStoragePerMonth= document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value;
    var storagePerMonth =document.forms['billingForm'].elements['billing.storagePerMonth'].value;
    var totalInsrVal = document.forms['billingForm'].elements['billing.totalInsrVal'].value;
    var insurancePerMonth =document.forms['billingForm'].elements['billing.insurancePerMonth'].value;
    var insuranceValueEntitle=document.forms['billingForm'].elements['billing.insuranceValueEntitle'].value;
    if(vendorStoragePerMonth == null || vendorStoragePerMonth == ''){ 
    	vendorStoragePerMonth = 0;
    }
    if(storagePerMonth == null || storagePerMonth == ''){ 
    	storagePerMonth = 0;
    }
    if(totalInsrVal == null || totalInsrVal == ''){ 
    	totalInsrVal = 0;
    }
    if(insurancePerMonth == null || insurancePerMonth == ''){ 
    	insurancePerMonth = 0;
    }
    if(insuranceValueEntitle == null || insuranceValueEntitle == ''){ 
    	insuranceValueEntitle = 0;
    }
    var inputBuildFormula="&vendorStoragePerMonth=" + encodeURI(vendorStoragePerMonth) +"&storagePerMonth=" + encodeURI(storagePerMonth)+"&totalInsrVal=" + encodeURI(totalInsrVal) + "&insurancePerMonth=" + encodeURI(insurancePerMonth) +"&insuranceValueEntitle=" + encodeURI(insuranceValueEntitle);
    if(payableRate == null || payableRate == ''){ 
		payableRate = 0;
    }
    if(postGrate !=null && postGrate !=''){ 
        postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
    }else{
         postGrate = document.forms['billingForm'].elements['billing.postGrate'].value=0*1;
    }
    if(fieldId == 'StoragePerMonth1'){
		var charge = document.forms['billingForm'].elements['billing.charge'].value;
		if(charge=='' || charge==' '){
			alert("Please select charge.");
			return false;
		}
     	var url="chargeRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle)  + "&postGrate=" + encodeURI(postGrate)+ "&payableRate=" + encodeURI(payableRate)+inputBuildFormula ;
     	http2.open("GET", url, true);
        http2.onreadystatechange = handleHttpResponse11;
        http2.send(null);
    }else if(fieldId == 'StoragePerMonth2'){
    	//var vendorCharge = document.forms['billingForm'].elements['billing.insuranceCharge1'].value;
    	var vendorCharge = document.forms['billingForm'].elements['billing.charge'].value;
    	if(vendorCharge == ''){
    		alert('Charge Type is empty');
    		return false;
    	} 
    	var url="getVendorChargeRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(vendorCharge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle) + "&postGrate=" + encodeURI(postGrate)  + "&payableRate=" + encodeURI(payableRate)+inputBuildFormula;
    	http2.open("GET", url, true);
        http2.onreadystatechange = handleHttpResponseForVendorCharges;
        http2.send(null);
    }else if(fieldId == 'InsurancePerMonth'){
    	var insurCharge = document.forms['billingForm'].elements['billing.insuranceCharge'].value;
    	if(insurCharge == '' || insurCharge == null){
    		alert('Insurance Charge Type is empty');
    		return false;
    	}
    	var insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value;
        var insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
        var baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
        
    	 if(insuranceValueActual !=null && insuranceValueActual !=''){
          	insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
         }else {
            insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value=0*1;
         }
         if(baseInsuranceValue !=null && baseInsuranceValue !=''){
        	 baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
         }else{
        	 baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value=0*1;
         }
         if(insuranceRate !=null && insuranceRate !=''){
          	insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value;
         }else{
            insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value=0*1;
         }
    	var url="chargeInsuranceRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(insurCharge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle) + "&insuranceValueActual=" + encodeURI(insuranceValueActual) + "&baseInsuranceValue="+ encodeURI(baseInsuranceValue) + "&insuranceRate=" + encodeURI(insuranceRate) + "&postGrate=" + encodeURI(postGrate)+ "&payableRate=" + encodeURI(payableRate)+inputBuildFormula;
    	http2.open("GET", url, true);
        http2.onreadystatechange = handleHttpResponse19;
        http2.send(null);
    }
}
function findChargeRate(){	
     var charge = document.forms['billingForm'].elements['billing.charge'].value;
     var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
     var onHand = document.forms['billingForm'].elements['billing.onHand'].value;
     var cycle = document.forms['billingForm'].elements['billing.cycle'].value;
     var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
     
     if(postGrate !=null && postGrate !='')  { 
       postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
     }else {
        postGrate = document.forms['billingForm'].elements['billing.postGrate'].value=0*1;
     }
     if(charge=='' || charge==' '){
		alert("Please select charge.");
	 }else{
     	var url="chargeRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle)  + "&postGrate=" + encodeURI(postGrate);
     }
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse11;
     http2.send(null);
}
function findInsuranceChargeRate(){	
     var charge = document.forms['billingForm'].elements['billing.insuranceCharge'].value;
     var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
     var onHand = document.forms['billingForm'].elements['billing.onHand'].value;
     var cycle = document.forms['billingForm'].elements['billing.cycle'].value;
     var insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value;
     var insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
     var baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
     var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
     if(postGrate !=null && postGrate !=''){ 
       postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
     }else{
        postGrate = document.forms['billingForm'].elements['billing.postGrate'].value=0*1;
     }
     if(insuranceValueActual !=null && insuranceValueActual !=''){
      	insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
     }else {
        insuranceValueActual = document.forms['billingForm'].elements['billing.insuranceValueActual'].value=0*1;
     }
     if(baseInsuranceValue !=null && baseInsuranceValue !=''){
    	 baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
     }else{
    	 baseInsuranceValue = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value=0*1;
     }
     if(insuranceRate !=null && insuranceRate !=''){
      	insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value;
     }else{
        insuranceRate = document.forms['billingForm'].elements['billing.insuranceRate'].value=0*1;
     }
    if(charge=='' || charge==' '){
		alert("Please select charge.");
	}else{
     var url="chargeInsuranceRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract) + "&onHand=" + encodeURI(onHand) + "&cycle=" + encodeURI(cycle) + "&insuranceValueActual=" + encodeURI(insuranceValueActual) + "&baseInsuranceValue="+ encodeURI(baseInsuranceValue) + "&insuranceRate=" + encodeURI(insuranceRate) + "&postGrate=" + encodeURI(postGrate);
     }
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse19;
     http2.send(null);
}
function findChargeRateFast()
{	
     var charge = document.forms['billingForm'].elements['billing.charge'].value;
     var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
     var url="chargeRateList.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse12;
     http2.send(null);
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.lntrim = function() {
    return this.replace(/^\s+/,"","\n");
}
function handleHttpResponse49()
        {
 	    if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                 res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
                targetElement = document.forms['billingForm'].elements['billing.contract'];
				targetElement.length = res.length;
				var tempContract='${billing.contract}';
				try{			
				tempContract=tempContract.trim();
				}catch(e){}
				for(i=0;i<res.length;i++)
					{
					  if(res[i] == ''){
					  document.forms['billingForm'].elements['billing.contract'].options[i].text = '';
					  document.forms['billingForm'].elements['billing.contract'].options[i].value = '';
					}
					else
					{
					  document.forms['billingForm'].elements['billing.contract'].options[i].value=res[i].trim();
					  document.forms['billingForm'].elements['billing.contract'].options[i].text=res[i].trim();
					}
             }
           document.forms['billingForm'].elements['billing.contract'].value=tempContract; 
           findDMMTypeContract(); 
       }
   }
   
function getHTTPObjectActualizationDate()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var actualizationDate = getHTTPObjectActualizationDate(); 
function findActualizationDateOfContract(){
    var contracts = document.forms['billingForm'].elements['billing.contract'].value;
    if(contracts!=null && contracts.trim()!=''){
	    var url="findActualizationDateOfContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
	    actualizationDate.open("GET", url, true);
	    actualizationDate.onreadystatechange = handleHttpResponseActualizationDate;
	    actualizationDate.send(null);
    }else{
    	checkPriceContract();
    }
}
function handleHttpResponseActualizationDate(){
     if (actualizationDate.readyState == 4) {
        var results = actualizationDate.responseText
        results = results.trim();
        if(results.length>1){ 
	       	 if(results=='TRUE'){
	       		document.forms['billingForm'].elements['billing.fXRateOnActualizationDate'].value=true;
	       	 }else{
	       		document.forms['billingForm'].elements['billing.fXRateOnActualizationDate'].value=false;
	       	 } 
	       	checkPriceContract();
        }
     }
}
function getHTTPObjectDMMtypeContract()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var dmmTypeContractHTTP = getHTTPObjectDMMtypeContract(); 
function findDMMTypeContract(){ 
	<c:if test="${(!(networkAgent))}">
	var contracts = document.forms['billingForm'].elements['billing.contract'].value;
    if(contracts!=null && contracts.trim()!='' && contracts!='${billing.contract}'){
	    var url="findDMMTypeContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
	    dmmTypeContractHTTP.open("GET", url, true);
	    dmmTypeContractHTTP.onreadystatechange = handleHttpResponseDmmTypeContract;
	    dmmTypeContractHTTP.send(null);
    }
    </c:if>
}
function handleHttpResponseDmmTypeContract(){
     if (dmmTypeContractHTTP.readyState == 4) {
        var results = dmmTypeContractHTTP.responseText
        results = results.trim(); 
        if(results.length>1){ 
	       	 if(results=='TRUE'){
	       		document.forms['billingForm'].elements['billingDMMContractTypeCheck'].value=true;
   			    checkShowHideNetworkBillToCode('changeData');
	       	 }else{
	       		document.forms['billingForm'].elements['billingDMMContractTypeCheck'].value=false;
	      	       checkShowHideNetworkBillToCode('changeData');
	       	 } 
        }
     }
}
function findContractTypeByContract(){
	<c:if test="${cmmDmmAgent}">
		var contracts = document.forms['billingForm'].elements['billing.contract'].value;
		var linkedFile = '${trackingStatus.soNetworkGroup}';
		linkedFile = linkedFile.trim();
		var formContractType = "";
		var dbContractType = "";
		var dbContract = '${billing.contract}';
		if(contracts!=null && contracts!=''){
			<c:forEach var="entry" items="${contractAndContractTypeMap}">
			if(contracts=="${entry.key}"){
				formContractType="${entry.value}";
			}
			</c:forEach>
			
			<c:forEach var="entry1" items="${contractAndContractTypeMap}">
			if(dbContract=="${entry1.key}"){
				dbContractType="${entry1.value}";
			}
			</c:forEach>
		}
		if((dbContractType=='CMM' || dbContractType=='DMM') && (formContractType=='NAA' || formContractType=='KGA' ) && (linkedFile==true || linkedFile=='true')){
			alert('DE-link file before changing contract to Non CMM/DMM type contract.');
			document.forms['billingForm'].elements['billing.contract'].value = '${billing.contract}';
		}else if((dbContractType=='DMM') && (formContractType=='CMM' || formContractType=='NAA' || formContractType=='KGA') && (linkedFile==true || linkedFile=='true')){
			findAccountLineInvoicedLines(formContractType);
		}else if((dbContractType=='CMM') && (formContractType=='DMM' || formContractType=='NAA' || formContractType=='KGA') && (linkedFile==true || linkedFile=='true')){
			findAccountLineInvoicedLines(formContractType);
		}
	</c:if>
}
function findAccountLineInvoicedLines(formContractType){
	var shipNumber=document.forms['billingForm'].elements['serviceOrder.shipNumber'].value;
	new Ajax.Request('getAccountLineInvoicedLinesAjax.html?ajax=1&decorator=simple&popup=true&shipNumber='+shipNumber,
		{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			response = response.trim();
			if(response>0){
				alert('The contract can not be changed. Linked lines are already invoiced.');
				document.forms['billingForm'].elements['billing.contract'].value = '${billing.contract}';
			}else{
				findOtherShipNumberInvoicedLines(formContractType);
			}
		},
		onFailure: function(){ 
		}
	});
}

function findOtherShipNumberInvoicedLines(contractType){
	var shipNumber=document.forms['billingForm'].elements['serviceOrder.shipNumber'].value;
	var sequenceNumber =document.forms['billingForm'].elements['serviceOrder.sequenceNumber'].value;
	new Ajax.Request('findOtherShipNumberInvoicedLinesAjax.html?ajax=1&decorator=simple&popup=true&shipNumber='+shipNumber+'&sequenceNumber='+sequenceNumber+'&contractType='+contractType,
		{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			response = response.trim();
			if(response==1){
				alert('The contract can not be changed. Linked lines are already invoiced.');
				document.forms['billingForm'].elements['billing.contract'].value = '${billing.contract}';
			}else if(response==0){
				alert('Please change contract for other S/Os as well.');
			}
		},
		onFailure: function(){ 
		}
	});
}

function checkNetworkBillToName(){ 
	<c:if test="${(!(networkAgent))}">
	var networkBillToCode = document.forms['billingForm'].elements['billing.networkBillToCode'].value;
	networkBillToCode=networkBillToCode.trim();
    if(networkBillToCode==''){
      document.forms['billingForm'].elements['billing.networkBillToName'].value="";
    }
	if(networkBillToCode!=''){
	    var url="findNetworkBillToName.html?ajax=1&decorator=simple&popup=true&networkPartnerCode=${customerFile.accountCode}&networkBillToCode=" + encodeURI(networkBillToCode);
	    dmmTypeContractHTTP.open("GET", url, true);
	    dmmTypeContractHTTP.onreadystatechange = handleHttpResponseNetworkBillToName;
	    dmmTypeContractHTTP.send(null);
    }
    </c:if>
}
function handleHttpResponseNetworkBillToName(){
	if (dmmTypeContractHTTP.readyState == 4){
        var results = dmmTypeContractHTTP.responseText
        results = results.trim();
        var res = results.split("#"); 
        if(res.length >= 2){ 
        		if(res[2] == 'Approved'){
        			document.forms['billingForm'].elements['billing.networkBillToName'].value = res[1];
        		}else{
        			alert("Network Bill to code is not approved" ); 
        			document.forms['billingForm'].elements['billing.networkBillToName'].value=""; 
        			document.forms['billingForm'].elements['billing.networkBillToCode'].value=""; 
        		}			
        }else{
            alert("Network Bill to code not valid");    
            document.forms['billingForm'].elements['billing.networkBillToName'].value="";
            document.forms['billingForm'].elements['billing.networkBillToCode'].value=""; 
		   }  } 
}
function checkPriceContract(){
    var contracts = document.forms['billingForm'].elements['billing.contract'].value;
    if(contracts!=null && contracts.trim()!=''){
    var url="priceContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse222333;
     http2.send(null);
    }
}
function setDefaultvalue(target){
	var eleValue= target.value;
	if(eleValue==null || eleValue==''){
		target.value='0.00';
	}
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function handleHttpResponse222333(){
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.indexOf("Login")==-1){
                var res = results.split("#");  
                if(res.length>=1)
                {
                	 var billPayMethod = document.getElementById('billPayMethod').selectedIndex;
                	 var partnerCode = document.forms['billingForm'].elements['billing.billToCode'].value;
                     if(billPayMethod == 0){
                 document.forms['billingForm'].elements['billing.billTo1Point'].value= res[5];
                     }else if(billPayMethod != 0 && (partnerCode==null || partnerCode=='')){
    			    	 document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value= res[5]; 
    			     }
                 document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].value= res[6];
                 if(res[6] !=undefined && res[6]!=''){
                 var resValue= res[6].split(":");
                 document.forms['billingForm'].elements['billing.billingInstruction'].value= resValue[1];                 
			     document.forms['billingForm'].elements['billing.specialInstruction'].value=resValue[1];
                 }else{
                 document.forms['billingForm'].elements['billing.billingInstruction'].value='';
                 document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].value='';
                 document.forms['billingForm'].elements['billing.specialInstruction'].value='';
                 }
                 <c:if test="${serviceOrder.job =='UVL' || serviceOrder.job =='MVL' || serviceOrder.job =='DOM' || serviceOrder.job =='DOF' }">
	                 if(document.forms['billingForm'].elements['billing.transportationDiscount'].value=='' || document.forms['billingForm'].elements['billing.transportationDiscount'].value=='0.00' || document.forms['billingForm'].elements['billing.transportationDiscount'].value=='0')
	                 {
	                 	document.forms['billingForm'].elements['billing.transportationDiscount'].value= res[0]; 
	                 }
	                 if(document.forms['billingForm'].elements['billing.discount'].value=='' || document.forms['billingForm'].elements['billing.discount'].value=='0.00' || document.forms['billingForm'].elements['billing.discount'].value=='0')
	                 {
	                 	document.forms['billingForm'].elements['billing.discount'].value= res[1]; 
	                 }
	                 if(document.forms['billingForm'].elements['billing.packDiscount'].value=='' || document.forms['billingForm'].elements['billing.packDiscount'].value=='0.00' || document.forms['billingForm'].elements['billing.packDiscount'].value=='0')
	                 {
	                 	document.forms['billingForm'].elements['billing.packDiscount'].value= res[2]; 
	                 }
	                 if(document.forms['billingForm'].elements['billing.sitDiscount'].value=='' || document.forms['billingForm'].elements['billing.sitDiscount'].value=='0.00' || document.forms['billingForm'].elements['billing.sitDiscount'].value=='0')
	                 {
	                 	document.forms['billingForm'].elements['billing.sitDiscount'].value= res[3]; 
	                 }
	                 if(document.forms['billingForm'].elements['billing.otherDiscount'].value=='' || document.forms['billingForm'].elements['billing.otherDiscount'].value=='0.00' || document.forms['billingForm'].elements['billing.otherDiscount'].value=='0')
	                 {
	                 	document.forms['billingForm'].elements['billing.otherDiscount'].value= res[4];       
	                 }
                 </c:if>
			     }
             }else{
            	 alert("Session has been ended, Login Again");
		    		window.location=document.location.href;  
             }}
        }
function handleHttpResponse9(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');
                if(results.length>=1){
 					 if (results == 'null') {
 					   alert("No work ticket")
                       document.forms['billingForm'].elements['billing.onHand'].value = "";
 					 } else{
 					    var newRes="";
 					    newRes=Math.round(results*100)/100;
 					    document.forms['billingForm'].elements['billing.onHand'].value=newRes;
 					 }
                }else{ 
                    document.forms['billingForm'].elements['billing.onHand'].value = "";
                 }
                 document.getElementById('rateImage').src = "<c:url value='/images/loader-move.gif'/>";
             }
        }
 function handleHttpResponse12()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                if(results.length>=1)
                {
                if(res[1]=='Preset') 
                {              
                var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
					if(postGrate == '0' || postGrate == '0.0' || postGrate == ''){ 
                  		document.forms['billingForm'].elements['billing.postGrate'].value= res[0];
                  	}	
                } 
               }
               } 
        }
      function handleHttpResponse19(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                var quantityOnHand=document.forms['billingForm'].elements['billing.onHand'].value;
		        var billingCycle=document.forms['billingForm'].elements['billing.cycle'].value;
		        document.forms['billingForm'].elements['billing.onHand'].value=(Math.round(quantityOnHand*100)/100).toFixed(2)
                if(results.length>=1){
                if(res[1]=='BuildFormula'){
                    if(res[3]=='Multiply'){
                        if(billingCycle!=0){
            	          	document.forms['billingForm'].elements['billing.insurancePerMonth'].value = (Math.round(((res[2]*res[4])/billingCycle)*100)/100).toFixed(2);
            	        }else{
            	           	document.forms['billingForm'].elements['billing.insurancePerMonth'].value=(0*1).toFixed(2);
            	        }
                     }else if(res[3]=='Division'){
                         if(billingCycle!=0){
            	          	document.forms['billingForm'].elements['billing.insurancePerMonth'].value = (Math.round((((res[2])/(res[4]))/billingCycle)*100)/100).toFixed(2);
            	          }else{
            	             document.forms['billingForm'].elements['billing.insurancePerMonth'].value=(0*1).toFixed(2);
            	          }
                      }else{
                    	  	if(billingCycle!=0){
		                	  document.forms['billingForm'].elements['billing.insurancePerMonth'].value = (Math.round((res[2]/billingCycle)*100)/100).toFixed(2);
		               		}else{
		               			document.forms['billingForm'].elements['billing.insurancePerMonth'].value=(0*1).toFixed(2);
		                	 }
	                  }
               }
           } 
        }  
       } 
      function handleHttpResponseForVendorCharges(){
          if (http2.readyState == 4){
             var results = http2.responseText
             results = results.trim();  
             var res = results.split("#");
                 if(results.length>=1){
	                if(res[1]=='Preset'){               
		                document.forms['billingForm'].elements['billing.payableRate'].value= res[0];
		            }
		                document.forms['billingForm'].elements['billing.payableRate'].value;
		                var rateCharge=document.forms['billingForm'].elements['billing.payableRate'].value;
		                var quantityOnHand=document.forms['billingForm'].elements['billing.onHand'].value;
		                document.forms['billingForm'].elements['billing.onHand'].value=(Math.round(quantityOnHand*100)/100).toFixed(2);
		                document.forms['billingForm'].elements['billing.payableRate'].value=(Math.round(rateCharge*100000)/100000).toFixed(5);
		                rateCharge=(Math.round(rateCharge*100000)/100000).toFixed(5);
		                var billingCycle=document.forms['billingForm'].elements['billing.cycle'].value;
		                var storagePerMonth=rateCharge*quantityOnHand*1;
		                var storagePerMonthRound="";
		                storagePerMonthRound=Math.round(storagePerMonth*100)/100;
		                document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value=storagePerMonthRound.toFixed(2);
	                
	                    if(res[1]=='BuildFormula'){
	                          if(res[3]=='Multiply'){
	                             if(billingCycle!=0){
	                	          	document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value = (Math.round(((res[2]*res[4])/billingCycle)*100)/100).toFixed(2);
	                	          }else{
	                	             document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value=(0*1).toFixed(2);
	                	          }
	                          }else if(res[3]=='Division'){
	                             if(billingCycle!=0){
	                	          	document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value = (Math.round((((res[2])/(res[4]))/billingCycle)*100)/100).toFixed(2);
	                	         }else{
	                	             document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value=(0*1).toFixed(2);
	                	          }
	                            }else{
		                           if(billingCycle!=0){
		                	          	document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value = (Math.round((res[2]/billingCycle)*100)/100).toFixed(2);
		                	       }else{
		                	             document.forms['billingForm'].elements['billing.vendorStoragePerMonth'].value=(0*1).toFixed(2);
		                	       }
	                            }
	                     }
				}
	        }
      }
        function handleHttpResponse11(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();  
                var res = results.split("#");
                    if(results.length>=1){
	                if(res[1]=='Preset'){               
		                document.forms['billingForm'].elements['billing.postGrate'].value= res[0];
		            }
		                document.forms['billingForm'].elements['billing.postGrate'].value;
		                var rateCharge=document.forms['billingForm'].elements['billing.postGrate'].value;
		                var quantityOnHand=document.forms['billingForm'].elements['billing.onHand'].value;
		                document.forms['billingForm'].elements['billing.onHand'].value=(Math.round(quantityOnHand*100)/100).toFixed(2);
		                document.forms['billingForm'].elements['billing.postGrate'].value=(Math.round(rateCharge*100000)/100000).toFixed(5);
		                rateCharge=(Math.round(rateCharge*100000)/100000).toFixed(5);
		                var billingCycle=document.forms['billingForm'].elements['billing.cycle'].value;
		                var storagePerMonth=rateCharge*quantityOnHand*1;
		                var storagePerMonthRound="";
		                storagePerMonthRound=Math.round(storagePerMonth*100)/100;
		                document.forms['billingForm'].elements['billing.storagePerMonth'].value=storagePerMonthRound.toFixed(2);
	                    if(res[1]=='BuildFormula'){
	                          if(res[3]=='Multiply'){
	                             if(billingCycle!=0){
	                	          	document.forms['billingForm'].elements['billing.storagePerMonth'].value = (Math.round(((res[2]*res[4])/billingCycle)*100)/100).toFixed(2);
	                	          }else{
	                	             document.forms['billingForm'].elements['billing.storagePerMonth'].value=(0*1).toFixed(2);
	                	          }
	                          }else if(res[3]=='Division'){
	                             if(billingCycle!=0){
	                	          	document.forms['billingForm'].elements['billing.storagePerMonth'].value = (Math.round((((res[2])/(res[4]))/billingCycle)*100)/100).toFixed(2);
	                	         }else{
	                	             document.forms['billingForm'].elements['billing.storagePerMonth'].value=(0*1).toFixed(2);
	                	          }
	                            }else{
		                           if(billingCycle!=0){
		                	          	document.forms['billingForm'].elements['billing.storagePerMonth'].value = (Math.round((res[2]/billingCycle)*100)/100).toFixed(2);
		                	       }else{
		                	             document.forms['billingForm'].elements['billing.storagePerMonth'].value=(0*1).toFixed(2);
		                	       }
	                            }
	                          var pageStoragePerMonth="0";
	                          var minimumValue="0";
	                          if(res[5]!=null && res[5]!=''){
	                        	  if(billingCycle!=0){
	                        		  minimumValue  =  (Math.round((res[5]/billingCycle)*100)/100).toFixed(2);
	                        	  }else{
	                        	  minimumValue  = (Math.round((res[5])*100)/100).toFixed(2)
	                        	  }
	                          }
	                          if(document.forms['billingForm'].elements['billing.storagePerMonth'].value!=''){
	                        	  pageStoragePerMonth  =document.forms['billingForm'].elements['billing.storagePerMonth'].value;
	                          }
	                          var floatStoragePerMonth=parseFloat(pageStoragePerMonth)
	                          var floatMinimum=parseFloat(minimumValue)
	                          if(document.forms['billingForm'].elements['billing.storagePerMonth'].value=='' ||document.forms['billingForm'].elements['billing.storagePerMonth'].value==0 || document.forms['billingForm'].elements['billing.storagePerMonth'].value==0.0 || document.forms['billingForm'].elements['billing.storagePerMonth'].value==0.00 || floatStoragePerMonth < floatMinimum){
	                        	  document.forms['billingForm'].elements['billing.storagePerMonth'].value=floatMinimum;
	                          }
	                     }
				}
	        }
        }
function autoPopulate_billingForm_insuranceOption(targetElement) {
		var insuranceOptionCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['billingForm'].elements['billing.insuranceOptionCode'].value=insuranceOptionCode.substring(0,insuranceOptionCode.indexOf(":")-1);
		targetElement.form.elements['billing.insuranceOption'].value=insuranceOptionCode.substring(insuranceOptionCode.indexOf(":")+2,insuranceOptionCode.length);
	 }
function autoPopulate_billingInstruction(targetElement) {
		var billingInstruction=targetElement.options[targetElement.selectedIndex].value;
		targetElement.form.elements['billing.billingInstruction'].value=billingInstruction.substring(billingInstruction.indexOf(":")+2,billingInstruction.length);
	}
function autoLinkCreditCard() {
		var creditCardType=document.forms['billingForm'].elements['billing.billTo1Point'].value;
		var creditCardTypeSec=document.forms['billingForm'].elements['billing.billTo2Point'].value;
		var creditCardTypePr=document.forms['billingForm'].elements['billing.payMethod'].value
		var creditCard;
		creditCard=0;
		if(creditCardType=='AE' || creditCardType=='MC' || creditCardType=='VC' || creditCardType=='DD' || creditCardTypePr=='AE' || creditCardTypePr=='MC' || creditCardTypePr=='VC' || creditCardTypePr=='DD' || creditCardTypeSec=='AE' || creditCardTypeSec=='MC' || creditCardTypeSec=='VC' || creditCardTypeSec=='DD')
		{
			creditCard=1;
		}
   if(creditCard==1 && document.forms['billingForm'].elements['billing.isCreditCard'].value!='1')
  	{
  		document.forms['billingForm'].elements['billing.isCreditCard'].value='1';
  	}
  	else if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1' && creditCard!=1)
  	{
  		document.forms['billingForm'].elements['billing.isCreditCard'].value='0';
  	}
  }
function assignDateApproved(){
		var d = new Date();
		var tempdate;
		tempdate=d.getMonth();
		tempdate=Number(tempdate)+1;
		tempdate=tempdate+"/"+d.getDate();
		tempdate=tempdate+"/"+d.getFullYear();
		document.forms['billingForm'].elements['billing.dateApproved'].value=tempdate;
	}
 function checkData(){
	 var f = document.getElementById('billingForm');
   		 f.setAttribute("autocomplete", "off");
	var imfCode;
	imfCode=0;
	if(document.forms['billingForm'].elements['billing.billToCode'].value=='A21629' || document.forms['billingForm'].elements['billing.billToCode'].value=='A18942' || document.forms['billingForm'].elements['billing.billToCode'].value=='A21109' || document.forms['billingForm'].elements['billing.billToCode'].value=='500132' || document.forms['billingForm'].elements['billing.billToCode'].value=='500130' || document.forms['billingForm'].elements['billing.billToCode'].value=='500139')
	{
		imfCode=1;
	}
	var MF="<%=request.getParameter("matchField")%>";
  	if(imfCode==1 && document.forms['billingForm'].elements['billing.imfJob'].value!='1')
  	{
  		document.forms['billingForm'].elements['billing.imfJob'].value='1';
  	}
  	else if(document.forms['billingForm'].elements['billing.imfJob'].value=='1' && imfCode!=1)
  	{
  		document.forms['billingForm'].elements['billing.imfJob'].value='0';
  	}
  }
  function assignBillToCode(){
		javascript:openWindow("partnersPopup.html?partnerType=AC&onlyAC=y&flag=0&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billToName&fld_code=billing.billToCode&fld_seventhDescription=billing.billingEmail");
		
	}
  function setBillToCode(){ 
	    window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billToName&fld_code=billing.billToCode');
	}
  function setBillToCode1(){ 
		<c:choose>
		<c:when test="${hasDataSecuritySet}">
		window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billName&fld_code=billing.billingId');
		</c:when>
		<c:otherwise>
		javascript:openWindow("partnersPopup.html?partnerType=AC&flag=0&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billName&fld_code=billing.billingId&fld_seventhDescription=billing.billingEmail");
		</c:otherwise>
		</c:choose>
	}
function assignSecBillToCode()
{
	javascript:openWindow("partnersPopup.html?partnerType=AC&onlyAC=y&flag=1&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billTo2Name&fld_code=billing.billTo2Code");
}
function assignPrivateBillToCode(){
javascript:openWindow("partnersPopup.html?partnerType=AC&onlyAC=y&flag=2&firstName=${customerFile.firstName}&lastName=${customerFile.lastName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.privatePartyBillingName&fld_code=billing.privatePartyBillingCode");
}
function check(){}

function validate_email(field)
{
with (field)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert("Not a valid e-mail address!");return false}
else {return true}
}
}
function checkHasValuation(){
	try{
		var chargeType = document.forms['billingForm'].elements['billing.charge'].value;
		var hasValuation = document.forms['billingForm'].elements['billing.insuranceHas'].value;
		if(chargeType != "" && hasValuation == ""){
			alert("Please confirm the data for Has Valuation Field");
			return false;
		}
	}catch(e){}
	}
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==67) || (keyCode==86) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==110) || (keyCode==67) || (keyCode==86); 
	}
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
	function onlyAlphaNumericWithDot(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	}
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        return false;
	        }
	    }
	    return true;
	}
var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}
function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}
function notExists(){
	alert("The Billing  is not saved yet");
}
/**
 * method makeMandIssueDt  added Regarding #9594
 */function makeMandIssueDt(){	
	<configByCorp:fieldVisibility componentId="component.billing.issueDate.mandatory">
	var hasVal=document.forms['billingForm'].elements['billing.insuranceHas'].value;
	var insuranceValue=document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
	var certnum=document.forms['billingForm'].elements['billing.certnum'].value;
	if((hasVal=='Y') && (insuranceValue!=null && insuranceValue!='' && insuranceValue!='0.00') && (certnum!=null && certnum!='')){
		document.getElementById("isuDtDiv1").style.display = "none";
		document.getElementById("isuDtDiv2").style.display = "block";
	}else{
		document.getElementById("isuDtDiv1").style.display = "block";
		document.getElementById("isuDtDiv2").style.display = "none";
	}
	</configByCorp:fieldVisibility>
}
/**
 * method resetIssueDt  added Regarding #9594
 */function resetIssueDt(){	
	<configByCorp:fieldVisibility componentId="component.billing.issueDate.mandatory">
	var hasVal='${billing.insuranceHas}';	
	var insuranceValue='${billing.insuranceValueActual}';
	var certnum='${billing.certnum}';
	if((hasVal=='Y') && (insuranceValue!=null && insuranceValue!='' && insuranceValue!='0.00') && (certnum!=null && certnum!='')){
		document.getElementById("isuDtDiv1").style.display = "none";
		document.getElementById("isuDtDiv2").style.display = "block";
	}else{
		document.getElementById("isuDtDiv1").style.display = "block";
		document.getElementById("isuDtDiv2").style.display = "none";
	}
	</configByCorp:fieldVisibility>
}
function showIndicatorFields(){
	<c:if test="${indicatorFieldsView=='Y'}">
	var hasVal=document.forms['billingForm'].elements['billing.insuranceHas'].value;
	if(hasVal=='Y'){
		document.getElementById("indicatorFieldsDiv").style.display = "block";
	}else{
		document.getElementById("indicatorFieldsDiv").style.display = "none";
	}
	</c:if>
}
function resetIndicatorsFields(){
	<c:if test="${indicatorFieldsView=='Y'}">
	var hasVal='${billing.insuranceHas}';
	if(hasVal=='Y'){
		document.getElementById("indicatorFieldsDiv").style.display = "block";
	}else{
		document.getElementById("indicatorFieldsDiv").style.display = "none";
	}
	</c:if>
}
function displayField(insuranceHas){
	try{
		var additionalNoVal = 'N';
				try{
				additionalNoVal = document.forms['billingForm'].elements['additionalNoFlag'].value;
				}catch(e){}
	if(insuranceHas == 'N'){
	    document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].selectedIndex =0;
		document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].disabled = true;
		document.getElementById("disDiv").style.display = "block"; 
		if(document.getElementById("displayDiv") != null){
			document.getElementById("displayDiv").style.display = "none";
			document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
		}
		if(document.getElementById("displayDivAdditionalNo") != null){
			document.getElementById("displayDivAdditionalNo").style.display = "none";
		}
		if(additionalNoVal=='Y'){
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
		}else{
			if(insuranceHas == 'Y'){
				document.getElementById("disDiv1").style.display = "block";
				document.getElementById("disDiv2").style.display = "block";
			}else if(insuranceHas == 'V'){
					document.getElementById("disDiv1").style.display = "block";
					document.getElementById("disDiv2").style.display = "block";
			}else{
				document.getElementById("disDiv1").style.display = "none";
				document.getElementById("disDiv2").style.display = "none";
			}
		}
	}else{
	    document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].disabled = false;
		document.getElementById("disDiv").style.display = "none";
		if(insuranceHas == 'Y'){
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
			try{
			    checkVendorName();
			    }catch(e){}
			if(document.forms['billingForm'].elements['billing.vendorCode'].value==''){
				document.forms['billingForm'].elements['billing.vendorCode'].value="${billing.vendorCode}";
			}
			if(document.forms['billingForm'].elements['billing.vendorName'].value==''){
				document.forms['billingForm'].elements['billing.vendorName'].value="${billing.vendorName}";
			}
		}else if(insuranceHas == 'V'){
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
			document.forms['billingForm'].elements['billing.vendorCode'].value="${billing.vendorCode}";
			document.forms['billingForm'].elements['billing.vendorName'].value="${billing.vendorName}";
     		if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}	
       		if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}	
		}else if(insuranceHas == '' ){
			document.forms['billingForm'].elements['billing.vendorCode'].value="";
			document.forms['billingForm'].elements['billing.vendorName'].value="";
			document.getElementById("disDiv1").style.display = "none";
			document.getElementById("disDiv2").style.display = "none";	
			if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
			if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
				}			
		}else{
			if(additionalNoVal=='N'){
			document.forms['billingForm'].elements['billing.vendorCode'].value="";
			document.forms['billingForm'].elements['billing.vendorName'].value="";
			document.getElementById("disDiv1").style.display = "none";
			document.getElementById("disDiv2").style.display = "none";
			if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
			if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
				}			
		}else{
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
			if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
				}
			}
		}
	}
	}catch(e){}
}
function checkdate(clickType){
if(document.forms['billingForm'].elements['billing.billCompleteA'] !=undefined){
	if((document.forms['billingForm'].elements['billing.billCompleteA'].value == 'T' || document.forms['billingForm'].elements['billing.billCompleteA'].value == 'A') && document.forms['billingForm'].elements['billing.billComplete'].value == ''){
		 document.forms['billingForm'].elements['billing.billComplete'].select();
		return false;
	}
	}
	}
function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}
	function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
		lastName=lastName.replace("~","'");
		document.getElementById(partnerNameId).value=lastName;
		document.getElementById(paertnerCodeId).value=partnercode;
		document.getElementById(autocompleteDivId).style.display = "none";	
		if(paertnerCodeId=='billingBillToCodeId'){
			getBillToCode();
		}else if(paertnerCodeId=='billingBillTo2CodeId'){
			getBillTo2Code();
		}else if(paertnerCodeId=='billingBrivatePartyBillingCodeId'){
			getBillTo3Code();
		}else if(paertnerCodeId=='billingVendorCode1Id'){
			checkVendorName1();
		}else if(paertnerCodeId=='billingLocationAgentCodeId'){
			checkAgentCode();
		}else if(paertnerCodeId=='billingVendorCodeId'){
			checkVendorName();
			findPayableCurrencyCodeAjax();
		}
		fillSellBuyRate();
	}
// function to populate billingComplete Date.
function populateBillingCompleteDate(){
	    var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		if(document.forms['billingForm'].elements['billing.billCompleteA'].value!='')
		{
		  document.forms['billingForm'].elements['billing.billComplete'].value=datam;
	     }
	    else
	    {
	     document.forms['billingForm'].elements['billing.billComplete'].value='';
	    } 
	}
// End of function
function autoSaveFunc(clickType){
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	<c:set var="checkPropertyAmountComponent" value="N" />
	<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
		<c:set var="checkPropertyAmountComponent" value="Y" />
	</configByCorp:fieldVisibility>
	var checkMsgClickedValue = '${msgClicked}';
enableCheckBox();
progressBarAutoSave('1');
if ('${autoSavePrompt}' == 'No'){
	var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
      		var id1 =document.forms['billingForm'].elements['serviceOrder.id'].value;
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.serviceorder'){
             //noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
        	   		noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		noSaveAction = 'editServiceOrderUpdate.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.billing'){
             noSaveAction = 'editBilling.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.accounting'){
              //noSaveAction = 'accountLineList.html?sid='+id1;
        	  	<c:if test="${checkPropertyAmountComponent!='Y'}">
	  	   			noSaveAction = 'accountLineList.html?sid='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		noSaveAction = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.newAccounting'){
             noSaveAction = 'pricingList.html?sid='+id1;
          }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.forwarding'){
        	  <c:if test="${forwardingTabVal!='Y'}">
	  			noSaveAction = 'containers.html?id='+id1;
	  		</c:if>
		  	<c:if test="${forwardingTabVal=='Y'}">
		  		//noSaveAction = 'containersAjaxList.html?id='+id1;
		  		<c:if test="${checkPropertyAmountComponent!='Y'}">
	  	   			noSaveAction = 'containersAjaxList.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		noSaveAction = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
		  	</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.domestic'){
             //noSaveAction = 'editMiscellaneous.html?id='+id1;
        	  	<c:if test="${checkPropertyAmountComponent!='Y'}">
	   				noSaveAction = 'editMiscellaneous.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		noSaveAction = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.status'){
                <c:if test="${serviceOrder.job =='RLO'}">
                	noSaveAction = 'editDspDetails.html?id='+id1; 
				</c:if>
                <c:if test="${serviceOrder.job !='RLO'}">
                	//noSaveAction =  'editTrackingStatus.html?id='+id1;
                	<c:if test="${checkPropertyAmountComponent!='Y'}">
		   				noSaveAction = 'editTrackingStatus.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		noSaveAction = 'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
				</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.ticket'){
             //noSaveAction = 'customerWorkTickets.html?id='+id1;
        	  	<c:if test="${checkPropertyAmountComponent!='Y'}">
 					noSaveAction = 'customerWorkTickets.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		noSaveAction = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI'){
            //noSaveAction = 'operationResource.html?id='+id1;
            <c:if test="${checkPropertyAmountComponent!='Y'}">
				noSaveAction = 'operationResource.html?id='+id1;
			</c:if>
		  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  		noSaveAction = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  	</c:if>
          }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.claims'){
             //noSaveAction = 'claims.html?id='+id1;
        	  	<c:if test="${checkPropertyAmountComponent!='Y'}">
					noSaveAction = 'claims.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		noSaveAction = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.customerfile'){
        	  var cidVal='${customerFile.id}';
             noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
           if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1'){
				if(document.forms['billingForm'].elements['gotoPageString'].value == 'gototab.creditcard'){
					noSaveAction = 'creditCards.html?id='+id1;
					}
           }
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.criticaldate'){
             noSaveAction ='soAdditionalDateDetails.html?sid='+id1;
           }           
          processAutoSave(document.forms['billingForm'], 'saveBilling!saveOnTabChange.html', noSaveAction);
	}else{
   if(!(clickType == 'save')){
     var id1 =document.forms['billingForm'].elements['serviceOrder.id'].value;
     if (document.forms['billingForm'].elements['formStatus'].value == '1'){
       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='billingDetail.heading'/>");
       if(agree){
           document.forms['billingForm'].action ='saveBilling!saveOnTabChange.html';
           document.forms['billingForm'].submit();
        }else{
         if(id1 != ''){
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.serviceorder'){
             //location.href = 'editServiceOrderUpdate.html?id='+id1;
             <c:if test="${checkPropertyAmountComponent!='Y'}">
             	location.href = 'editServiceOrderUpdate.html?id='+id1;
			 </c:if>
		  	 <c:if test="${checkPropertyAmountComponent=='Y'}">
		  		location.href = 'editServiceOrderUpdate.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  	 </c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.billing'){
             location.href = 'editBilling.html?id='+id1;
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI'){
            //noSaveAction = 'operationResource.html?id='+id1;
        	  	<c:if test="${checkPropertyAmountComponent!='Y'}">
        	  		location.href = 'operationResource.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
          }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.accounting'){
              //location.href = 'accountLineList.html?sid='+id1;
        	  	<c:if test="${checkPropertyAmountComponent!='Y'}">
	           		location.href = 'accountLineList.html?sid='+id1;
				 </c:if>
			  	 <c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
			  	 </c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.newAccounting'){
             location.href = 'pricingList.html?sid='+id1;
          }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.forwarding'){
        	  <c:if test="${forwardingTabVal!='Y'}">
				 	location.href = 'containers.html?id='+id1;
	  		</c:if>
		  	<c:if test="${forwardingTabVal=='Y'}">
		  			//location.href = 'containersAjaxList.html?id='+id1;
		  			<c:if test="${checkPropertyAmountComponent!='Y'}">
		           		location.href = 'containersAjaxList.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
		  	</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.domestic'){
             //location.href = 'editMiscellaneous.html?id='+id1;
        	  	<c:if test="${checkPropertyAmountComponent!='Y'}">
         			location.href = 'editMiscellaneous.html?id='+id1;
				</c:if>
		  		<c:if test="${checkPropertyAmountComponent=='Y'}">
		  			location.href = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.status'){
              <c:if test="${serviceOrder.job =='RLO'}">
              	location.href = 'editDspDetails.html?id='+id1; 
			</c:if>
          	<c:if test="${serviceOrder.job !='RLO'}">
          		//location.href =  'editTrackingStatus.html?id='+id1;
          		<c:if test="${checkPropertyAmountComponent!='Y'}">
         			location.href = 'editTrackingStatus.html?id='+id1;
				</c:if>
		  		<c:if test="${checkPropertyAmountComponent=='Y'}">
		  			location.href = 'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  		</c:if>
			</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.ticket'){
             //location.href = 'customerWorkTickets.html?id='+id1;
        	  	<c:if test="${checkPropertyAmountComponent!='Y'}">
   					location.href = 'customerWorkTickets.html?id='+id1;
				</c:if>
	  			<c:if test="${checkPropertyAmountComponent=='Y'}">
	  				location.href = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
	  			</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.claims'){
             //location.href = 'claims.html?id='+id1;
        	<c:if test="${checkPropertyAmountComponent!='Y'}">
				location.href = 'claims.html?id='+id1;
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				location.href = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			</c:if>
           }
          if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.customerfile'){
        	  var cidVal='${customerFile.id}';
             location.href ='editCustomerFile.html?id='+cidVal;
           }
           if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1'){
				if(document.forms['billingForm'].elements['gotoPageString'].value == 'gototab.creditcard'){
					location.href = 'creditCards.html?id='+id1;
					}
           }
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.criticaldate'){
        	   location.href ='soAdditionalDateDetails.html?sid='+id1;
           }
           if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI'){
             noSaveAction = 'operationResource.html?id='+id1;
           }
         }
       }
    }else{
     if(id1 != ''){
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.serviceorder'){
          //location.href = 'editServiceOrderUpdate.html?id='+id1;
	    	<c:if test="${checkPropertyAmountComponent!='Y'}">
				location.href = 'editServiceOrderUpdate.html?id='+id1;
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				location.href = 'editServiceOrderUpdate.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			</c:if>
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.billing'){
          location.href = 'editBilling.html?id='+id1;
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.accounting'){
          //location.href = 'accountLineList.html?sid='+id1;
	    	<c:if test="${checkPropertyAmountComponent!='Y'}">
				location.href = 'accountLineList.html?sid='+id1;
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				location.href = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
			</c:if>
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.newAccounting'){
         location.href = 'pricingList.html?sid='+id1;
       }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI'){
         //noSaveAction = 'operationResource.html?id='+id1;
    	   	<c:if test="${checkPropertyAmountComponent!='Y'}">
				location.href = 'operationResource.html?id='+id1;
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				location.href = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			</c:if>
       }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.forwarding'){
    	   <c:if test="${forwardingTabVal!='Y'}">
		 		location.href = 'containers.html?id='+id1;
 			</c:if>
	  		<c:if test="${forwardingTabVal=='Y'}">
	  			//location.href = 'containersAjaxList.html?id='+id1;
	  			<c:if test="${checkPropertyAmountComponent!='Y'}">
					location.href = 'containersAjaxList.html?id='+id1;
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					location.href = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				</c:if>
	  		</c:if>
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.domestic'){
           //location.href = 'editMiscellaneous.html?id='+id1;
	    	<c:if test="${checkPropertyAmountComponent!='Y'}">
				location.href = 'editMiscellaneous.html?id='+id1;
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				location.href = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			</c:if>
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.status'){
           <c:if test="${serviceOrder.job =='RLO'}">
              location.href = 'editDspDetails.html?id='+id1; 
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}">
              //location.href =  'editTrackingStatus.html?id='+id1;
	            <c:if test="${checkPropertyAmountComponent!='Y'}">
					location.href = 'editTrackingStatus.html?id='+id1;
				</c:if>
				<c:if test="${checkPropertyAmountComponent=='Y'}">
					location.href = 'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				</c:if>
			</c:if>
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.ticket'){
           //location.href = 'customerWorkTickets.html?id='+id1;
	    	<c:if test="${checkPropertyAmountComponent!='Y'}">
				location.href = 'customerWorkTickets.html?id='+id1;
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				location.href = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			</c:if>
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.claims'){
           //location.href = 'claims.html?id='+id1;
	    	<c:if test="${checkPropertyAmountComponent!='Y'}">
				location.href = 'claims.html?id='+id1;
			</c:if>
			<c:if test="${checkPropertyAmountComponent=='Y'}">
				location.href = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			</c:if>
        }
       if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.customerfile'){
    	   var cidVal='${customerFile.id}';
          location.href ='editCustomerFile.html?id='+cidVal;
        }
        if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1'){
				if(document.forms['billingForm'].elements['gotoPageString'].value == 'gototab.creditcard'){
					location.href = 'creditCards.html?id='+id1;
					}
          }
        if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.criticaldate'){
     	   location.href ='soAdditionalDateDetails.html?sid='+id1;
        }
        /* if(document.forms['billingForm'].elements['gotoPageString'].value =='gototab.OI'){
          noSaveAction = 'operationResource.html?id='+id1;
        } */
      }
   }
  }
  }
 }
function changeStatus(){
    document.forms['billingForm'].elements['formStatus'].value = '1';
}
function findBillToName(){ 
    var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
    billToCode=billToCode.trim();
    if(billToCode==''){
      document.forms['billingForm'].elements['billing.billToName'].value="";
    }
    var accountSearchValidation = document.forms['billingForm'].elements['accountSearchValidation'].value;
    if(billToCode!=''){
    	 <c:if test="${accountSearchValidation}">
     	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
    	</c:if>
    	 <c:if test="${!accountSearchValidation}">
      	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
     	</c:if>
     	http2.open("GET", url, true);
     	http2.onreadystatechange = function(){ handleHttpResponse2(billToCode);};
     	http2.send(null);
    }
}
function findBillTo2Name() { 
    var billTo2Code = document.forms['billingForm'].elements['billing.billTo2Code'].value;
    billTo2Code=billTo2Code.trim();
    if(billTo2Code=='') { 
      	document.forms['billingForm'].elements['billing.billTo2Name'].value="";
    }
    var accountSearchValidation = document.forms['billingForm'].elements['accountSearchValidation'].value;
    if(billTo2Code!=''){
      	 <c:if test="${accountSearchValidation}">
    	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billTo2Code)+"&accountSearchValidation="+accountSearchValidation;
    	</c:if>
    	 <c:if test="${!accountSearchValidation}">
     	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billTo2Code)+"&accountSearchValidation="+accountSearchValidation;
     	</c:if>
     	http2.open("GET", url, true);
     	http2.onreadystatechange = function(){ handleHttpResponse3(billTo2Code);};
     	http2.send(null);
	}
}
function checkVendorName(){
    var vendorId = document.forms['billingForm'].elements['billing.vendorCode'].value;
    if(vendorId==''){
    	document.forms['billingForm'].elements['billing.vendorName'].value='';
    }
    if(vendorId!=''){
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + 
	encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);
     } else {
     if(document.getElementById("displayDiv") != null){
			document.getElementById("displayDiv").style.display = "none";
			document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
		}
     if(document.getElementById("displayDivAdditionalNo") != null){
			document.getElementById("displayDivAdditionalNo").style.display = "none";
		}		
     }
}

function vendorCodeForActgCode(){
    var vendorId = document.forms['billingForm'].elements['billing.vendorCode'].value;
    if(vendorId!=''){
    var url="vendorNameForActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse222;
     http2.send(null);
    }
} 
function handleHttpResponse222(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length >= 2){ 
		                if(res[3]=='Approved'){
		                	if(res[2] != "null" && res[2] !=''){
		                		document.forms['billingForm'].elements['billing.vendorName'].value = res[1];
                 				document.forms['billingForm'].elements['billing.actgCode'].value=res[2]; 
				            }
				            else{
				                alert("Vendor Approved but missing Accounting Code" ); 
								document.forms['billingForm'].elements['billing.vendorName'].value="";
								document.forms['billingForm'].elements['billing.vendorCode'].value="";
								document.forms['billingForm'].elements['billing.actgCode'].value="";
								document.forms['billingForm'].elements['billing.vendorCode'].select();
				            }	
				        }else{
				            alert("Vendor code is not approved" ); 
							document.forms['billingForm'].elements['billing.vendorName'].value="";
							document.forms['billingForm'].elements['billing.vendorCode'].value="";
							document.forms['billingForm'].elements['billing.actgCode'].value="";
							document.forms['billingForm'].elements['billing.vendorCode'].select();
				        }     
		        }else{
                 alert("Vendor code not valid" ); 
				 document.forms['billingForm'].elements['billing.vendorName'].value="";
				 document.forms['billingForm'].elements['billing.vendorCode'].value="";
				 document.forms['billingForm'].elements['billing.actgCode'].value="";
				 document.forms['billingForm'].elements['billing.vendorCode'].select();
			   }
      }
}
function findBillingName(){
    var billingCode = document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value;
    billingCode = billingCode.trim();
    if(billingCode==''){
    	document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
    }
    var accountSearchValidation = document.forms['billingForm'].elements['accountSearchValidation'].value;
    if(billingCode!=''){
    	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billingCode)+"&accountSearchValidation="+accountSearchValidation;
     	http2.open("GET", url, true);
     	http2.onreadystatechange = function(){ handleHttpResponse4(billingCode);};
     	http2.send(null);
    }
}
function findBillingIdName(){
	<configByCorp:fieldVisibility componentId="component.field.Alternative.billName">
    var billingCode = document.forms['billingForm'].elements['billing.billingId'].value;
    billingCode = billingCode.trim();
    if(billingCode!=''){
    	var url="findBillingIdNameAjax.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billingCode);
     	http21984.open("GET", url, true);
     	http21984.onreadystatechange = function(){ handleHttpResponse41984(billingCode);};
     	http21984.send(null);
    }else{
		 document.forms['billingForm'].elements['billing.billingId'].value="";
		 document.forms['billingForm'].elements['billing.billName'].value="";
		 document.forms['billingForm'].elements['billing.billingId'].focus();    
    }
    </configByCorp:fieldVisibility>
}
function handleHttpResponse41984(billingCode){
     if (http21984.readyState == 4) {
        var results = http21984.responseText
        results = results.trim();
        if(results.length>=1){
       	 document.forms['billingForm'].elements['billing.billName'].value=results;
        }else{
            alert("Billing ID not Approved" ); 
			 document.forms['billingForm'].elements['billing.billingId'].value="";
			 document.forms['billingForm'].elements['billing.billName'].value="";
			 document.forms['billingForm'].elements['billing.billingId'].focus();
        }
     }
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function handleHttpResponse2(billToCode){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value; 
                var res = results.split("~"); 
                var billCurr = document.forms['billingForm'].elements['billing.billingCurrency'];
                     if(res.length >= 2){ 
           			if(res[5]== 'Approved'){
           				var test = res[4];
							var x=document.getElementById("billPayMethod");
							var billPayMethod = document.getElementById('billPayMethod').selectedIndex;	
							
								for(var a = 0; a < x.length; a++){
									if(test == document.forms['billingForm'].elements['billing.billTo1Point'].options[a].value){
										document.forms['billingForm'].elements['billing.billTo1Point'].options[a].selected="true";
									}}
           				if(res[2] == '0'){
		           			if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
		           				document.forms['billingForm'].elements['billing.billToName'].value = res[0];
		           				var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value; 
		           				var dataBaseBillToCode = '${billing.billToCode}';	
		           				if(dataBaseBillToCode!=billToCode){
			           				if(res[7]!='#'){
			           				  	document.forms['billingForm'].elements['billing.creditTerms'].value = res[7];
			           				}else{
			           					document.forms['billingForm'].elements['billing.creditTerms'].value ="";
			           				}
		           				}else{
		           					document.forms['billingForm'].elements['billing.creditTerms'].value='${billing.creditTerms}';
		           				}
		           				if(res[9]!='#'){
		           					document.forms['billingForm'].elements['billing.billingEmail'].value = res[9];
			           				}else{
			           			document.forms['billingForm'].elements['billing.billingEmail'].value="";
			           				}
		           				if(res[6] == '1' && document.forms['billingForm'].elements['billing.insuranceHas'].value == '' && '${serviceOrder.job}' != 'RLO'){
		           					document.forms['billingForm'].elements['billing.insuranceHas'].value = 'Y';
		           				}
		           				if(document.forms['billingForm'].elements['billing.billToReference'].value =="" && res[8]!='#'){
		           					document.forms['billingForm'].elements['billing.billToReference'].value= res[8];
		           				}
		           				showOrHide(0);
		 						showPartnerAlert('onchange',billToCode,'hidBBillTo');
	 						}else{
			           			if(companyDivision != '' || res[1] != companyDivision ){
			           				alert("Company Division of selected bill to code is not "+ companyDivision); 
			           			}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
			           				alert('Please select company division in service order.');
			           			}
			           			if(billCurr != null && billCurr != 'undefind'){
			                  		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
			                  	 }
			           			document.forms['billingForm'].elements['billing.billToName'].value="";
								document.forms['billingForm'].elements['billing.billToCode'].value="";
								document.forms['billingForm'].elements['billing.creditTerms'].value="";
								document.forms['billingForm'].elements['billing.billToCode'].select();
								showOrHide(0);
		 						showPartnerAlert('onchange','','hidBBillTo');
			           		}	
			           	}else if(res[2] == '1'){
			           			if(res[3] <= 0){
			           				showOrHide(0);		
			           				alert('The Vendor cannot be selected as credit limit exceeded.');
			           				if(billCurr != null && billCurr != 'undefind'){
			                      		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
			                      	 }
								    document.forms['billingForm'].elements['billing.billToName'].value="";
									document.forms['billingForm'].elements['billing.billToCode'].value="";
									document.forms['billingForm'].elements['billing.creditTerms'].value="";
									document.forms['billingForm'].elements['billing.billToCode'].select();
									showPartnerAlert('onchange','','hidBBillTo');
			           			}
			           			if(res[3] > 0){
			           				if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
				           				document.forms['billingForm'].elements['billing.billToName'].value = res[0];
				           				if(document.forms['billingForm'].elements['billing.billToReference'].value =="" &&  res[8]!='#'){
				           					document.forms['billingForm'].elements['billing.billToReference'].value= res[8];
				           				}
			 							alert("The Vendor currently has available credit of "+res[3]+", please ensure that this s/o will be for less than this amount before selecting this vendor.");
		 								showOrHide(0);
			 							showPartnerAlert('onchange',billToCode,'hidBBillTo');	
		 							}else{
		 								showOrHide(0);
					           			if(companyDivision != '' || res[1] != companyDivision ){
				           					alert("Company Division of selected bill to code is not "+ companyDivision); 
				           				}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
				           					alert('Please select company division in service order.');
				           				}
					           			if(billCurr != null && billCurr != 'undefind'){
					                  		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
					                  	 }
					           			document.forms['billingForm'].elements['billing.billToName'].value="";
										document.forms['billingForm'].elements['billing.billToCode'].value="";
										document.forms['billingForm'].elements['billing.creditTerms'].value="";
										document.forms['billingForm'].elements['billing.billToCode'].select();
										showPartnerAlert('onchange','','hidBBillTo');	
				           			}
				           		}
			           		}
           			 }else{
           				showOrHide(0);
	           			alert("Bill To code not approved");
	           			if(billCurr != null && billCurr != 'undefind'){
	                  		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
	                  	 }
	                  	document.forms['billingForm'].elements['billing.billToName'].value="";
			 			document.forms['billingForm'].elements['billing.billToCode'].value="";
			 			document.forms['billingForm'].elements['billing.creditTerms'].value="";
			 			document.forms['billingForm'].elements['billing.billToCode'].select();
			 			showPartnerAlert('onchange','','hidBBillTo');
			 		 }
           		}else{
               		showOrHide(0);
           			alert("Bill To code not valid");
           			if(billCurr != null && billCurr != 'undefind'){
                  		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
                  	 }
                  	document.forms['billingForm'].elements['billing.billToName'].value="";
		 			document.forms['billingForm'].elements['billing.billToCode'].value="";
		 			document.forms['billingForm'].elements['billing.creditTerms'].value="";
		 			document.forms['billingForm'].elements['billing.billToCode'].select();
		 			showPartnerAlert('onchange','','hidBBillTo');
            	}
       	}
}
function handleHttpResponse3(billTo2Code){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim(); 
                var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value; 
                var res = results.split("~"); 
                if(res.length >= 2){ 
                	if(res[5]== 'Approved'){
           				if(res[2] == '0'){
		           			if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
		           				document.forms['billingForm'].elements['billing.billTo2Name'].value = res[0];
		           				if(document.forms['billingForm'].elements['billing.billToReference'].value =="" &&  res[8]!='#'){
		           					document.forms['billingForm'].elements['billing.billToReference'].value= res[8];
		           				}
		           				showOrHide(0);
		 						showPartnerAlert('onchange',billTo2Code,'hidBBillTo2');
	 						}else{
	 							showOrHide(0);	
			           			if(companyDivision != '' || res[1] != companyDivision ){
			           				alert("Company Division of selected bill to code is not "+ companyDivision); 
			           			}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
			           				alert('Please select company division in service order.');
			           			}
			           			document.forms['billingForm'].elements['billing.billTo2Name'].value="";
						 		document.forms['billingForm'].elements['billing.billTo2Code'].value="";
						 		document.forms['billingForm'].elements['billing.billTo2Code'].select();
						 		showPartnerAlert('onchange','','hidBBillTo2');
			           		}	
		           		}else if(res[2] == '1'){
		           			if(res[3] <= 0){
		           				showOrHide(0);
		           				alert('The Vendor cannot be selected as credit limit exceeded.');
							    document.forms['billingForm'].elements['billing.billTo2Name'].value="";
								document.forms['billingForm'].elements['billing.billTo2Code'].value="";
								document.forms['billingForm'].elements['billing.billTo2Code'].select();
								showPartnerAlert('onchange','','hidBBillTo2');
		           			}
		           			if(res[3] > 0){
		           				if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
			           				document.forms['billingForm'].elements['billing.billTo2Name'].value = res[0];
			           				if(document.forms['billingForm'].elements['billing.billToReference'].value =="" &&  res[8]!='#'){
			           					document.forms['billingForm'].elements['billing.billToReference'].value= res[8];
			           				}
		 							alert("The Vendor currently has available credit of "+res[3]+", please ensure that this s/o will be for less than this amount before selecting this vendor.");
	 								showOrHide(0);
		 							showPartnerAlert('onchange',billTo2Code,'hidBBillTo2');
	 							}else{
	 								showOrHide(0);
				           			if(companyDivision != '' || res[1] != companyDivision ){
			           					alert("Company Division of selected bill to code is not "+ companyDivision); 
				           			}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
				           				alert('Please select company division in service order.');
				           			}
				           			document.forms['billingForm'].elements['billing.billTo2Name'].value="";
									document.forms['billingForm'].elements['billing.billTo2Code'].value="";
									document.forms['billingForm'].elements['billing.billTo2Code'].select();
									showPartnerAlert('onchange','','hidBBillTo2');
			           			}
		           			}
		           		}
           			}else{
           				showOrHide(0);
	           			alert("Bill To code not approved");
	                  	document.forms['billingForm'].elements['billing.billToName'].value="";
			 			document.forms['billingForm'].elements['billing.billTo2Code'].value="";
			 			document.forms['billingForm'].elements['billing.billTo2Code'].select();
			 			showPartnerAlert('onchange','','hidBBillTo2');
			 		}
           		}else if(res.length < 2){
           			 showOrHide(0);	 
                     alert("Bill To code not valid");
                     document.forms['billingForm'].elements['billing.billTo2Name'].value="";
					 document.forms['billingForm'].elements['billing.billTo2Code'].value="";
					 document.forms['billingForm'].elements['billing.billTo2Code'].select();
					 showPartnerAlert('onchange','','hidBBillTo2');
				}
        }
}
 function handleHttpResponse4(billingCode){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value; 
                var res = results.split("~"); 
                if(res.length >= 2){
                	if(res[5]== 'Approved'){
                		if(res[2] == '0'){
		           			if(res[1] == '' || res[1] == companyDivision || res[1] == 'null' || res[1] == '#'){
		           				document.forms['billingForm'].elements['billing.privatePartyBillingName'].value = res[0];
		           				showOrHide(0);
		 			 			showPartnerAlert('onchange',billingCode,'hidBBillTo3');
	 						}else{
	 							showOrHide(0);
			           			if(companyDivision != '' || res[1] != companyDivision ){
			           				alert("Company Division of selected bill to code is not "+ companyDivision); 
			           			}else if(companyDivision == '' || companyDivision == 'null' || res[1] == '#'){
			           				alert('Please select company division in service order.');
			           			}
			           			document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
						 		document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
						 		document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
						 		showPartnerAlert('onchange','','hidBBillTo3');
			           		}	
		           		}else if(res[2] == '1'){
		           			if(res[3] <= 0){
		           				showOrHide(0);
		 			 			alert('The Vendor cannot be selected as credit limit exceeded.');
							    document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
						 		document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
						 		document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
						 		showPartnerAlert('onchange','','hidBBillTo3');
		           			}
		           			if(res[3] > 0){
		           				if(res[1]== '' || res[1] == companyDivision || res[1] == 'null' || res[1]== '#'){
			           				document.forms['billingForm'].elements['billing.privatePartyBillingName'].value = res[0];
		 							alert("The Vendor currently has available credit of "+res[3]+", please ensure that this s/o will be for less than this amount before selecting this vendor.");
	 								showOrHide(0);
		 			 				showPartnerAlert('onchange',billingCode,'hidBBillTo3');
	 							}else{
	 								showOrHide(0);
		 			 				if(companyDivision != '' || res[1] != companyDivision ){
			           					alert("Company Division of selected bill to code is not "+ companyDivision); 
				           			}else if(companyDivision == ''||companyDivision == 'null' || res[1] == '#'){
				           				alert('Please select company division in service order.');
				           			}
				           			document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
									document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
									document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
									showPartnerAlert('onchange','','hidBBillTo3');
			           			}
		           			}
		           			
		           		}
                	}else{
                		showOrHide(0);
		 			 	alert("Bill To code not approved");
                     	document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
					 	document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
					 	document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
					 	showPartnerAlert('onchange','','hidBBillTo3');
                	} 
           		 }else{
                  	 showOrHide(0);
		 			 alert("Bill To code not valid");
                     document.forms['billingForm'].elements['billing.privatePartyBillingName'].value="";
					 document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value="";
					 document.forms['billingForm'].elements['billing.privatePartyBillingCode'].select();
					 showPartnerAlert('onchange','','hidBBillTo3');
                 }
       }
}
function handleHttpResponse5(){ 
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>2){
                	if(res[2] == 'Approved'){
           				document.forms['billingForm'].elements['billing.vendorName'].value = res[1];
           				
           				if(res[10]=='Y' && '${serviceOrder.job}' != 'RLO'){
           					var hasValuationNew = document.forms['billingForm'].elements['billing.insuranceHas'].value;
           					hasValuationNew=hasValuationNew.trim();
           					var additionalNoVal = 'N';
           						try{
           						additionalNoVal = document.forms['billingForm'].elements['additionalNoFlag'].value;
           						}catch(e){}
	           				 var vendorId = document.forms['billingForm'].elements['billing.vendorCode'].value;
	           				 var contract = document.forms['billingForm'].elements['billing.contract'].value;
    	           			if(document.getElementById("displayDivAdditionalNo") != null){
        	           			if(additionalNoVal=='Y'){
                   						<c:if test="${printCertificateFlag=='true'}">
                   						document.getElementById("displayDivAdditionalNo").style.display = "block";
                   						</c:if>	        	           			
		        	           			<c:if test="${printCertificateFlag!='true'}">
		           						document.getElementById("displayDivAdditionalNo").style.display = "none";
           								</c:if>
        	           			}else{        	           				
		        	           			if((hasValuationNew=='Y') && (additionalNoVal=='N')){        	           				
		                   						<c:if test="${printCertificateFlag=='true'}">
		                   						document.getElementById("displayDivAdditionalNo").style.display = "block";
		                   						</c:if>	        	           			
				        	           			<c:if test="${printCertificateFlag!='true'}">
				           						document.getElementById("displayDivAdditionalNo").style.display = "none";
		           								</c:if>
		        	           			}else{
		        	           				document.getElementById("displayDivAdditionalNo").style.display = "none";
		        	           			}
    	           					}	
        	           			}
    	           			if(document.getElementById("displayDiv") != null){
    	           				if(hasValuationNew=='Y'){
           						document.getElementById("displayDiv").style.display = "block";
    	           				}else{
    	           					document.getElementById("displayDiv").style.display = "none";
    	           					document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
    	           				}
           					}	
           				}else{
    	           			if(document.getElementById("displayDiv") != null){
           						document.getElementById("displayDiv").style.display = "none";
           						document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
           					}	
    	           			if(document.getElementById("displayDivAdditionalNo") != null){
           						document.getElementById("displayDivAdditionalNo").style.display = "none";
           						document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
           					}	
           				}
	           		}else{
	           			alert("Vendor Code is not approved" ); 
					    document.forms['billingForm'].elements['billing.vendorName'].value="";
				 		document.forms['billingForm'].elements['billing.vendorCode'].value="";
				 		document.forms['billingForm'].elements['billing.billingInsurancePayableCurrency'].value="";
				 		if(document.getElementById("displayDiv") != null){
							document.getElementById("displayDiv").style.display = "none";
							document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
						}
	           			if(document.getElementById("displayDivAdditionalNo") != null){
       						document.getElementById("displayDivAdditionalNo").style.display = "none";
       					}	
				 		//document.forms['billingForm'].elements['billing.vendorCode'].select();
	           		}
               	}else{
                     alert("Vendor Code not valid" );
                 	 document.forms['billingForm'].elements['billing.vendorName'].value="";
				 	 document.forms['billingForm'].elements['billing.vendorCode'].value="";
				 	document.forms['billingForm'].elements['billing.billingInsurancePayableCurrency'].value="";
				 	if(document.getElementById("displayDiv") != null){
						document.getElementById("displayDiv").style.display = "none";
						document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
					}
           			if(document.getElementById("displayDivAdditionalNo") != null){
   						document.getElementById("displayDivAdditionalNo").style.display = "none";
   					}					
				 	 //document.forms['billingForm'].elements['billing.vendorCode'].select();
			   }
       }
}
function autoStorageBilling(val){
	var  permissionTest  = "";
	<sec-auth:authComponent componentId="module.field.">  
		  permissionTest  = <%=(Integer)request.getAttribute("module.field.billing.isAutomatedStorageBillingProcessing" + "Permission")%>;
		
	</sec-auth:authComponent>
	if(permissionTest > 2){
		try{
		 var payMethod =document.forms['billingForm'].elements['billing.billTo1Point'].value;
		 var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
		 if(('${serviceOrder.job}' != 'STO' && '${serviceOrder.job}' != 'STF' && '${serviceOrder.job}' != 'TPS') || payMethod == '' || billToCode == ''){
		 	document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = true;
		 }
		 
		 if(payMethod != 'AE' || payMethod != 'VC' || payMethod != 'MC' || payMethod != 'DEB'){
		 	document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = true;
		 }
		 
		 if(('${serviceOrder.job}' == 'STO' || '${serviceOrder.job}' == 'STF' || '${serviceOrder.job}' == 'TPS') && billToCode != '' && (payMethod == 'AE' || payMethod == 'VC' || payMethod == 'MC' || payMethod == 'DEB')){
			var url="autoStorageBilling.html?ajax=1&decorator=simple&popup=true&billToCode=" + encodeURI(billToCode);
		    httpAutoStorageBilling.open("GET", url, true);
		    httpAutoStorageBilling.onreadystatechange = function() {autoStorageBillingRes(val);};
		    httpAutoStorageBilling.send(null);
	     } else{
     	document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = true;
     }
	}catch(e){}
	}
}
function autoStorageBillingRes(val){
	if (httpAutoStorageBilling.readyState == 4){
		var results = httpAutoStorageBilling.responseText;
		results = results.trim();
		var res = results.split("#"); 
		if(res.length >= 2){
			if(res[1] =='null' || res[1] =='' || res[2] == 'yes'){
				document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = false;
				if(val != '1'){
					alert('Please confirm automated storage billing processing flag.');
				}
			}else{
				document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = true; 
			}
		}else{
			document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = false;
			if(val != '1'){
				alert('Please confirm automated storage billing processing flag.');
			}
		}
	}
}
function findAdditionalNo(){
	  document.getElementById('autoAdditionalImage').src = "<c:url value='/images/loader-move.gif'/>";
	  var addNo=document.forms['billingForm'].elements['billing.additionalNo'].value;
	  if(addNo.trim()==''){
     var url="maxAdditionalNumberList.html?ajax=1&decorator=simple&popup=true";
     http9321.open("GET", url, true);
     http9321.onreadystatechange = handleHttpResponse9321;
     http9321.send(null);
	  }
} 
function handleHttpResponse9321()
{
     if (http9321.readyState == 4) {
        var results = http9321.responseText
        results = results.trim();
        if(results.length>=1){
       	 document.forms['billingForm'].elements['billing.additionalNo'].value=results;
        }
        document.getElementById('autoAdditionalImage').src = "<c:url value='/images/loader-move.gif'/>";
     }
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http9321 = getHTTPObject();
var http2 = getHTTPObject();
var http21984 = getHTTPObject();
var httpAutoStorageBilling = getHTTPObjectAutoStorageBilling();
var http3 = getHTTPObject();
var http4 = getHTTPObject();
var http5 = getHTTPObject();
var http6 = getHTTPObject();
function getHTTPObject6(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
    
 var http55 = getHTTPObject55();
 var http9 = getHTTPObject9();  
 function getHTTPObject9()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
var http555 = getHTTPObject555();  
var http5555 = getHTTPObject555();
var http257= getHTTPObject555(); 
var http5551984=getHTTPObject555(); 
 function getHTTPObject555()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
  function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getHTTPObjectAutoStorageBilling()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http222 = getHTTPObjectPricingBilling();
var http77 = getHTTPObjectPricingBilling();
function getHTTPObjectPricingBilling()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function chk()
{	
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	if(billingContract=='' || billingContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	}
	else{		
		var mode;
		if(document.forms['billingForm'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['billingForm'].elements['serviceOrder.mode'].value; }else{mode="";}
		var categoryFlag="F";
		<configByCorp:fieldVisibility componentId="component.billingTab.categoryType">
		var categoryFlag='T';
		</configByCorp:fieldVisibility>   
		var	val='Storage';
		if( categoryFlag=='T'){
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.storageVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.charge&categoryType='+val);
		}else{
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.storageVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.charge');
		}
		findChargeRateFast();
		document.forms['billingForm'].elements['billing.postGrate'].focus();
	}
}
function chkInsurance()
{	
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	if(billingContract=='' || billingContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	}
	else{
		var mode;
		if(document.forms['billingForm'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['billingForm'].elements['serviceOrder.mode'].value; }else{mode="";}
		var categoryFlag="F";
		<configByCorp:fieldVisibility componentId="component.billingTab.categoryType">
		var categoryFlag='T';
		</configByCorp:fieldVisibility>	
		val='Insurance';
		if( categoryFlag=='T'){
		javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.insuranceVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.insuranceCharge&categoryType='+val);
		}else{
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.insuranceVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.insuranceCharge');	
		}
}
}
function chkInsurance1()
{	
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	if(billingContract=='' || billingContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	}
	else{
		var mode;
		if(document.forms['billingForm'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['billingForm'].elements['serviceOrder.mode'].value; }else{mode="";}
		var categoryFlag="F";
		<configByCorp:fieldVisibility componentId="component.billingTab.categoryType">
		var categoryFlag='T';
		</configByCorp:fieldVisibility>   
		var val='Insurance';
		if( categoryFlag=='T'){
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.insuranceVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.insuranceCharge1&categoryType='+val);
		}else{
			javascript:openWindow('chargess.html?contract='+billingContract+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_ninthDescription=billing.insuranceVatExclude&fld_eigthDescription=eigthDescription&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_thirdDescription=thirdDescription&fld_code=billing.insuranceCharge1');
		}}
}

function winOpen()
 {
		finalVal='Other';
 		openWindow('brokerPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.vendorName&fld_code=billing.vendorCode');
 		 }
function winOpen1()
{
		finalVal='Other';
		openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.vendorName1&fld_code=billing.vendorCode1');
}
function winOpen2()
{
		finalVal='Other';
		openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.locationAgentName&fld_code=billing.locationAgentCode');
}
 function winOpenForActgCode()
 {
 		finalVal='Other'; 
 		openWindow('brokerForActgCode.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.vendorName&fld_code=billing.vendorCode&fld_seventhDescription=billing.actgCode');
 }
	function calculateTotal(){
	var insrVal = document.forms['billingForm'].elements['billing.insuranceValueActual'].value*1;
	var insrRate = document.forms['billingForm'].elements['billing.insuranceRate'].value*1;
	var s=document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value;
		if( !(document.forms['billingForm'].elements['billing.insuranceRate'].value == '') ){
			var valu = (insrVal*insrRate)/100;
			var valu1 = Math.round(valu*100)/100;
			document.forms['billingForm'].elements['billing.totalInsrVal'].value = (valu1).toFixed(2);
			}
			else {
			document.forms['billingForm'].elements['billing.totalInsrVal'].value = '';
			}
	}
	function goToCreditcard(clickType){
   		if(document.forms['billingForm'].elements['billing.isCreditCard'].value=='1')
  		{
  			checkdate(clickType);
  			autoSaveFunc(clickType);
  		}
	}
function handleHttpResponse6()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                 alert(http2.responseText);
                results = results.trim();
                if(results.length>=1)
                {
                 alert(results);
                 
                 }
                 else
                 {
                     alert("Vendor code does not exist, Please select another" );
			   }
             }
        }
function insValues(targetElement){
var optionCode = document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value;
 if(optionCode!=''){
    var url="bucketNumber.html?ajax=1&decorator=simple&popup=true&optionCode="+optionCode;  
    url=url.replace('%','trila')
     http55.open("GET", url, true); 
     http55.onreadystatechange = handleHttpResponse411111; 
     http55.send(null); 
     setTimeout(function(){calculateTotal();},600); 
     }
}
function handleHttpResponse411111() {    
if (http55.readyState == 4)
             {
                var results = http55.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']','');
                results=results.replace(',','');
                 if(results.length>=1){
                document.forms['billingForm'].elements['billing.insuranceRate'].value = results;
                 } else {
                  document.forms['billingForm'].elements['billing.insuranceRate'].value = '';
                 } 
             }
        }
function baseRateValues(targetElement){
	var optionCode = document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value;
	 if(optionCode!=''){
	    var url="getFlex1OfParameter.html?ajax=1&decorator=simple&popup=true&optionCode="+optionCode;
	  	url=url.replace('%','trila');
	     httpFlex1.open("GET", url, true); 
	     httpFlex1.onreadystatechange = handleHttpResponseFlex1; 
	     httpFlex1.send(null); 
	     }
	}
	function handleHttpResponseFlex1() {    
	if (httpFlex1.readyState == 4){
	                var results = httpFlex1.responseText
	                results = results.trim();
	                results = results.replace('[','');
	                results=results.replace(']','');
	                results=results.replace(',','');
	                res = results.split("~");
	                 if(res[0].length>=1 && res[0] != 'NO'){
	                document.forms['billingForm'].elements['billing.insuranceBuyRate'].value = res[0];
	                 } else {
	                  document.forms['billingForm'].elements['billing.insuranceBuyRate'].value = '';
	                 } 
	                 <c:if test="${serviceOrder.corpID == 'NWVL'}">
	                 if(res[1].length>=1 && res[1] != 'NO'){
	                document.forms['billingForm'].elements['billing.deductible'].value = res[1];	 
	                 }else {
	                	 document.forms['billingForm'].elements['billing.deductible'].value  = '';
	                 } 
	                 </c:if>
	      }
    calBaseTotal();
    }
function calBaseTotal(){
	var buyRate =0
	buyRate= document.forms['billingForm'].elements['billing.insuranceBuyRate'].value ;
	var baseInsVal = 0
	baseInsVal=	document.forms['billingForm'].elements['billing.baseInsuranceValue'].value ;
	if(buyRate!='' && baseInsVal!=''){
		var temp = (buyRate*baseInsVal)/100;
		temp = Math.round(temp*100)/100;
		document.forms['billingForm'].elements['billing.baseInsuranceTotal'].value = temp;
	}else{
		document.forms['billingForm'].elements['billing.baseInsuranceTotal'].value='';
	}
}
function authorizationNos(){
	var authNo = document.forms['billingForm'].elements['billing.billToAuthority'].value;
	var billId = document.forms['billingForm'].elements['billing.id'].value;
	var billingId = document.forms['billingForm'].elements['billing.shipNumber'].value;
	var serviceId = document.forms['billingForm'].elements['serviceOrder.id'].value;
	var serviceJob = document.forms['billingForm'].elements['serviceOrder.job'].value;
	var serviceCom = document.forms['billingForm'].elements['serviceOrder.commodity'].value;
	javascript:openWindow('authorizationNos.html?billId=${billing.id}&billingId=${billing.shipNumber}&authNo='+authNo+'&serviceJob=${serviceOrder.job}&serviceId=${serviceOrder.id}&serviceCom=${serviceOrder.commodity}&decorator=popup&popup=true');
	
}
 function checkMultiAuthorization()
    {  
    var billCode = document.forms['billingForm'].elements['billing.billToCode'].value; 
    var url="checkMultiAuthorization.html?ajax=1&decorator=simple&popup=true&accPartnerCode="+billCode;  
     http55.open("GET", url, true); 
     http55.onreadystatechange = handleHttpResponse41; 
     http55.send(null);
}
function handleHttpResponse41()
        {    if (http55.readyState == 4)
             {
                var results = http55.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results=='true') 
                {
                document.getElementById("hid").style.display="block";
                }
                else{
                document.getElementById("hid").style.display="none";
                
                }
             }
        }
 function authorizationNo2()
 {
	 var billingIdOut='';
	try{
 		var billingIdOut = document.forms['billingForm'].elements['billing.billingIdOut'].value;
	}catch(e){}

 if(billingIdOut!='') 
                {
                document.getElementById("hid1").style.display = 'block';
                }
                else{
                document.getElementById("hid1").style.display = 'none';
                }
 }
function enableCheckBox(){
	try{
	document.forms['billingForm'].elements['billing.isAutomatedStorageBillingProcessing'].disabled = false;
	var elementsLen=document.forms['billingForm'].elements.length;
			for(i=0;i<=elementsLen-1;i++){				
						document.forms['billingForm'].elements[i].disabled=false;
			}
	}catch(e){}
}
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['billingForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['billingForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['billingForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['billingForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editBilling.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['billingForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['billingForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
  function goToUrl(id)
	{
		location.href = "editBilling.html?id="+id;
	}
function findWorkTicketList()
{
  var shipNumber=document.forms['billingForm'].elements['serviceOrder.shipNumber'].value;
  var sid=document.forms['billingForm'].elements['serviceOrder.id'].value;
  openWindow('activeWorkTicketList.html?sid='+sid+'&shipNumber='+shipNumber+'&decorator=popup&popup=true',900,300);
}

function chargePerValue(){
	 var charge = document.forms['billingForm'].elements['billing.charge'].value;
	 var postGrate = document.forms['billingForm'].elements['billing.postGrate'].value;
	 var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	 if(charge !=''){
	 	if(postGrate == '0' || postGrate == '0.0' || postGrate == '' || postGrate == '0.00000'){
		     var url="chargePerValue.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract);
		     http6.open("GET", url, true);
		     http6.onreadystatechange = handleHttpResponsePerValue;
		     http6.send(null);
     	}
	 }
}
function handleHttpResponsePerValue(){
     if(http6.readyState == 4){
        var results = http6.responseText
        results = results.trim();               
        if(results.length>=1){
	     	document.forms['billingForm'].elements['billing.postGrate'].value= results;         
		}
	 }
}
function billingCompleteStorageOutControl()
{
if(document.forms['billingForm'].elements['billingBillCompleteVar'].value== '1'){
if('${serviceOrder.job}' == 'STO' || '${serviceOrder.job}' == 'STF')
{
var agree = confirm("Does repetitive billing needs to be stopped YES/NO? press OK to YES OR CANCEL to NO.");
			if(agree){
			document.forms['billingForm'].elements['billing.storageOut'].value = document.forms['billingForm'].elements['billing.billComplete'].value;
			}
			}
			}
	document.forms['billingForm'].elements['billingBillCompleteVar'].value='0';		
}
function changeBillingBillCompleteVar(){

document.forms['billingForm'].elements['billingBillCompleteVar'].value= '1';      
}

function checkBillToAuthority(){
	if(document.forms['billingForm'].elements['billing.billToAuthority'].value == ""){
 		alert("Enter Authorization No to continue.....");
 		return false;
 		}else{
 		return authorizationNos();
 	}
}
var http52 = getHTTPObject52();
	function getHTTPObject52()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http53 = getHTTPObject53();
	function getHTTPObject53()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http54 = getHTTPObject54();
	function getHTTPObject54()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http55 = getHTTPObject55();
 httpFlex1 = getHTTPObject55();
	function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 
 function getPricing(){
    var job = '${serviceOrder.job}';
   	if(job!=''){
     	var url = "findPricing.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http52.open("GET", url, true);
     	http52.onreadystatechange = handleHttpResponse52;
     	http52.send(null);
    }
    
}
function handleHttpResponse52(){
		if (http52.readyState == 4){
                var results = http52.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['billingForm'].elements['billing.personPricing'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.personPricing'].options[i].text = '';
					document.forms['billingForm'].elements['billing.personPricing'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.personPricing'].options[i].text = stateVal[1];
					document.forms['billingForm'].elements['billing.personPricing'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("personPricing").value = '${billing.personPricing}';
        getBilling();
        }
  }
 
  function getBilling(){
     var job = '${serviceOrder.job}';
   	if(job!=''){
     	var url = "findBilling.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http53.open("GET", url, true);
     	http53.onreadystatechange = handleHttpResponse53;
     	http53.send(null);
    }
    
}
function handleHttpResponse53(){
		if (http53.readyState == 4){
                var results = http53.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['billingForm'].elements['billing.personBilling'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.personBilling'].options[i].text = '';
					document.forms['billingForm'].elements['billing.personBilling'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.personBilling'].options[i].text = stateVal[1];
					document.forms['billingForm'].elements['billing.personBilling'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("personBilling").value = '${billing.personBilling}';
        getPayable();
        }
  }
  function getPayable(){
    var job = '${serviceOrder.job}';
   	if(job!=''){
     	var url = "findPayable.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http54.open("GET", url, true);
     	http54.onreadystatechange = handleHttpResponse54;
     	http54.send(null);
    }
}
function handleHttpResponse54(){
		if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['billingForm'].elements['billing.personPayable'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.personPayable'].options[i].text = '';
					document.forms['billingForm'].elements['billing.personPayable'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.personPayable'].options[i].text = stateVal[1];
					document.forms['billingForm'].elements['billing.personPayable'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("personPayable").value = '${billing.personPayable}';
        getApprovedBy();
        }
  }
   function getApprovedBy(){
     var job = '${serviceOrder.job}';
   	if(job!=''){
     	var url = "findApprovedBy.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http55.open("GET", url, true);
     	http55.onreadystatechange = handleHttpResponse55;
     	http55.send(null);
    }
}
function handleHttpResponse55(){
		if (http55.readyState == 4){
                var results = http55.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['billingForm'].elements['billing.approvedBy'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.approvedBy'].options[i].text = '';
					document.forms['billingForm'].elements['billing.approvedBy'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.approvedBy'].options[i].text = stateVal[1];
					document.forms['billingForm'].elements['billing.approvedBy'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("approvedBy").value = '${billing.approvedBy}';
        }
  }
function changeReadyForInvoiceCheck(){
var checkInvoiceStatus=document.getElementById('checkInvoice').checked;    
if(checkInvoiceStatus)
{
var currentDate=currentDateGlobal();
document.forms['billingForm'].elements['billing.readyForInvoiceCheck'].value=currentDateGlobal();
}
}  
function changeReadyForInvoiceCheckEdit(){
	
	var checkInvoiceStatus=document.getElementById('checkInvoice').checked; 
 if(checkInvoiceStatus)
	{
	var currentDate=currentDateGlobal();
	document.forms['billingForm'].elements['billing.readyForInvoiceCheck'].value=currentDateGlobal();
	}
	else{
		document.forms['billingForm'].elements['billing.readyForInvoiceCheck'].value='';
	}
	}


function validateBillToCode(){
var billToCode=document.forms['billingForm'].elements['billing.billToCode'].value;
var contract=document.forms['billingForm'].elements['billing.contract'].value;
var billTo1Point=document.forms['billingForm'].elements['billing.billTo1Point'].value;
var billingInstructionCodeWithDesc=document.forms['billingForm'].elements['billing.billingInstructionCodeWithDesc'].value; 
<configByCorp:fieldVisibility componentId="component.tab.billing.contractSystem">
var contractSystem =document.forms['billingForm'].elements['billing.contractSystem'].value;
 if(contractSystem==''){
    alert("Contract System  is Required Field");
    return false;   
	}
</configByCorp:fieldVisibility> 
if(billToCode==''){
     alert("Bill To Code is Required Field");
   return false;
   }else if(contract==''){
     alert("Contract is Required Field");
    return false;
   }else if(billTo1Point==''){
     alert("Pay Method is Required Field");
    return false;
   }else if(billingInstructionCodeWithDesc==''){
     alert("Instruction is Required Field");
    return false;   
	} else
   return true;
   }

function checkFloat(temp)
    { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.name;  
	if(temp.value>100){
	alert("You cannot enter more than 100% VAT ")
	document.forms['billingForm'].elements[fieldName].select();
    document.forms['billingForm'].elements[fieldName].value='0.00'; 
	return false;
	} 
	
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if((i!=0)&&(c=='-')) 	{
       	  alert("Invalid data in vat%." ); 
          document.forms['billingForm'].elements[fieldName].select();
          document.forms['billingForm'].elements[fieldName].value=''; 
       	  return false;
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  } 
    if(check=='Invalid'){ 
    alert("Invalid data in vat%." ); 
    document.forms['billingForm'].elements[fieldName].select();
    document.forms['billingForm'].elements[fieldName].value=''; 
    return false;
    }  else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.forms['billingForm'].elements[fieldName].value=value; 
    return true;
    }  }
   function getStorageVatPercent(fromFieldId,targetFieldId){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){ 
	        if(document.forms['billingForm'].elements[fromFieldId].value=='${entry.key}') {
	        	document.forms['billingForm'].elements[targetFieldId].value='${entry.value}'; 
	        	relo="yes"; 
	       }
	    }
    </c:forEach>
      if(document.forms['billingForm'].elements[fromFieldId].value==''){
      document.forms['billingForm'].elements[targetFieldId].value=0;
      calculateVatAmt();
      }
      </c:if>
	} 
   function getStoragePayVatPercent(fromFieldId,targetFieldId){
		<c:if test="${systemDefaultVatCalculation=='true'}">
		 var relo="" 
	     <c:forEach var="entry" items="${payVatPercentList}">
	        if(relo==""){ 
		        if(document.forms['billingForm'].elements[fromFieldId].value=='${entry.key}') {
		        	document.forms['billingForm'].elements[targetFieldId].value='${entry.value}'; 
		        	relo="yes"; 
		       }
		    }
	    </c:forEach>
	      if(document.forms['billingForm'].elements[fromFieldId].value==''){
	      document.forms['billingForm'].elements[targetFieldId].value=0;
	      calculateVatAmt();
	      }
	      </c:if>
		}
	function getInsuranceVatPercent(){ 
	<c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){ 
        if(document.forms['billingForm'].elements['billing.insuranceVatDescr'].value=='${entry.key}') {
        document.forms['billingForm'].elements['billing.insuranceVatPercent'].value='${entry.value}'; 
        relo="yes"; 
       }  }
    </c:forEach>
      if(document.forms['billingForm'].elements['billing.insuranceVatDescr'].value==''){
      document.forms['billingForm'].elements['billing.insuranceVatPercent'].value=0;
      calculateVatAmt();
      }
      </c:if>
	} 
function historicalContract(){
		   var job='${serviceOrder.job}';
		   var createdon='${billing.createdOn}';
		   var companyDivision='${serviceOrder.companyDivision}';
		   var billing='${billing.billToCode}'; 
		   var hsContract=document.forms['billingForm'].elements['Historical'].value;
	       var url="historicalContracts.html?ajax=1&decorator=simple&popup=true&hsContract="+ encodeURI(hsContract)+"&job="+ encodeURI(job)+"&createdon="+ encodeURI(createdon)+"&companyDivision="+encodeURI(companyDivision)+"&billing="+encodeURI(billing);
	       httpHC.open("GET", url, true);
	       httpHC.onreadystatechange = handleHttpResponseHC;
	       httpHC.send(null);
	}

	function handleHttpResponseHC(){
			if (httpHC.readyState == 4){
	                var results = httpHC.responseText
	                results = results.trim();
	                results=results.replace("[","")
	                var res=results.replace("]","")
	                
	               	}
	       }
	var httpHC = getHTTPObjectHC();
	function getHTTPObjectHC(){
	    var xmlhttp;
	    if(window.XMLHttpRequest){
	        xmlhttp = new XMLHttpRequest();
	    }else if (window.ActiveXObject){
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp){
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}
	function sendEmail(target){
		var reg = /^([A-Za-z0-9_\-\.\'])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if(reg.test(target) == false){
		alert('Invalid Email Address');
		}else{
     	var billingEmail = target;
   		var subject = 'S/O# ${serviceOrder.shipNumber}';
   		subject = subject+"  "+"${serviceOrder.firstName} ";
   		subject = subject+"${serviceOrder.lastName}"; 
		var mailto_link = 'mailto:'+encodeURI(billingEmail)+'?subject='+encodeURI(subject);		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
		}
}

	function billingEmailImage(){
		var billingEmailCode=document.forms['billingForm'].elements['billing.billingEmail'].value;
	     if(billingEmailCode=='')
	     { 
	     document.getElementById("billingEmailImage").style.display="none";
	     }
	     else if(billingEmailCode!='')
	     {
	      document.getElementById("billingEmailImage").style.display="block";
	     }
	}

	function fillVendorName(){		
	    var insuranceHas = document.forms['billingForm'].elements['billing.insuranceHas'].value;
	    if(insuranceHas!=''){
	    	var url="vendorNameDetails.html?ajax=1&decorator=simple&popup=true&code="+encodeURI(insuranceHas);
	     	http55555.open("GET", url, true);
	     	http55555.onreadystatechange = handleHttpResponse55555;
	     	http55555.send(null);	     
	     }else{
	    	 displayField(insuranceHas);
	     }
	}
	function handleHttpResponse55555(){
        if (http55555.readyState == 4){
           var selected = http55555.responseText
           selected = selected.trim();
           if(selected!="" && selected!="~"){
           var sel = selected.split("~");
           			document.forms['billingForm'].elements['billing.vendorCode'].value=sel[0];
				    document.forms['billingForm'].elements['billing.vendorName'].value=sel[1];
           }
           var insuranceHas = document.forms['billingForm'].elements['billing.insuranceHas'].value;
		   displayField(insuranceHas);
  		}
	}
	var http55555 = getHTTPObject55555();
	function getHTTPObject55555()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function calInsval(){
	var baseIns = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
	var exchngeRate = document.forms['billingForm'].elements['billing.exchangeRate'].value;
	var temp= baseIns*exchngeRate;
	temp = Math.round(temp*1000)/1000;
	document.forms['billingForm'].elements['billing.insuranceValueActual'].value = temp;
}

function checkVendorName1(){
    var vendorId = document.forms['billingForm'].elements['billing.vendorCode1'].value;
    if(vendorId!=''){
    	showHide("block");
    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse555;
	    http2.send(null);
     } else {
	     document.forms['billingForm'].elements['billing.vendorName1'].value="";
	     document.forms['billingForm'].elements['billing.vendorBillingCurency'].value="";
	     if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
		}
		if(document.getElementById("displayDivAdditionalNo") != null){
				document.getElementById("displayDivAdditionalNo").style.display = "none";
		}			
    }
}
function handleHttpResponse555(){
    if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       var res = results.split("#"); 
       if(res.length>2){
       	if(res[2] == 'Approved'){
  				document.forms['billingForm'].elements['billing.vendorName1'].value = res[1];  		
  				findVendorCurrencyCodeAjax();
       		}else{
      			showHide("none");
      			alert("Vendor Code is not approved" ); 
			    document.forms['billingForm'].elements['billing.vendorName1'].value="";
		 		document.forms['billingForm'].elements['billing.vendorCode1'].value="";
		 		document.forms['billingForm'].elements['billing.vendorBillingCurency'].value="";
      		}
      	}else{
      		showHide("none");
            alert("Vendor Code not valid" );
        	 document.forms['billingForm'].elements['billing.vendorName1'].value="";
		 	 document.forms['billingForm'].elements['billing.vendorCode1'].value="";
		 	document.forms['billingForm'].elements['billing.vendorBillingCurency'].value="";
	   }
}
}

function checkAgentCode(){
    var vendorId = document.forms['billingForm'].elements['billing.locationAgentCode'].value;
    if(vendorId!=''){
    	showHide("block");
    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse5555;
	    http2.send(null);
     }
    document.forms['billingForm'].elements['billing.locationAgentName'].value="";
}
function handleHttpResponse5555(){
    if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       var res = results.split("#"); 
       if(res.length>2){
       	if(res[2] == 'Approved'){
       		showHide("none");
  				document.forms['billingForm'].elements['billing.locationAgentName'].value = res[1];  		
       		}else{
      			showHide("none");
      			alert("Location Agent Code is not approved" ); 
			    document.forms['billingForm'].elements['billing.locationAgentCode'].value="";
		 		document.forms['billingForm'].elements['billing.locationAgentName'].value="";
      		}
      	}else{
      		showHide("none");
            alert("Location Agent Code is not valid" );
            document.forms['billingForm'].elements['billing.locationAgentCode'].value="";
	 		document.forms['billingForm'].elements['billing.locationAgentName'].value="";
	   }
	}
}

function getContact(partnerCode){
		openWindow('searchBillingEmail.html?billToCode='+partnerCode+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billingEmail&fld_code=billing.billingEmail');
}
function fillBillToAuthority(){
	var bCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	if(bCode!='') {
	    var url="fillBillToAuthority.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http51.open("GET", url, true); 
	   	http51.onreadystatechange = handleHttp90; 
	   	http51.send(null);	     
     }else{
    	 var billCurr = document.forms['billingForm'].elements['billing.billingCurrency'];
    	 if(billCurr != null && billCurr != 'undefind'){
    		 document.forms['billingForm'].elements['billing.billingCurrency'].value="";
    	 }
     }
}
function handleHttp90(){
    if (http51.readyState == 4){
    	try{
    		findBillToCurrencyCodeAjax();
    	}catch(e){}
    	var results = http51.responseText
    	results = results.trim(); 
    	if(results=="undefined"){
    		results="";
		}
		 	document.forms['billingForm'].elements['billing.billToAuthority'].value = results;
    	}
	}

var http51 = getHTTPObject();
function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}
function billToCodeFieldValueCopy(){
	var bCode=document.forms['billingForm'].elements['billing.billToCode'].value;
	if(bCode==''){
		document.forms['billingForm'].elements['billing.billToAuthority'].value="";
		document.forms['billingForm'].elements['billing.billToReference'].value="";
		document.forms['billingForm'].elements['billing.billingEmail'].value="";
	}
}
var http511 = getHTTPObject();

function fillInvoiceNumber(){
	if(document.getElementById("emailInvoice").checked){ 
	var bCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	if(bCode!='') {
	    var url="getEmailInvoiceAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http511.open("GET", url, true); 
	   	http511.onreadystatechange = handleHttp905; 
	   	http511.send(null);	     
     }
   }
}
function handleHttp905(){
    if (http511.readyState == 4){    	
    	var results = http511.responseText
    		results = results.trim();
		 	document.forms['billingForm'].elements['billing.storageEmail'].value = results;
    	}
       }
function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}

storageForVatExclude();
insuranceForVatExclude();

function storageForVatExclude(){
	var forStorageVat=document.forms['billingForm'].elements['billing.storageVatExclude'].value; 
	if(forStorageVat =='true' || forStorageVat =='True'){ 
		document.forms['billingForm'].elements['billing.storageVatDescr'].value="";
		document.forms['billingForm'].elements['billing.storageVatDescr'].disabled = true;
		document.forms['billingForm'].elements['billing.storageVatPercent'].value="0";
		document.forms['billingForm'].elements['billing.storageVatPercent'].disabled = true;
		document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].value="";
		document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].disabled = true;
		document.forms['billingForm'].elements['billing.vendorStorageVatPercent'].value="0";
		document.forms['billingForm'].elements['billing.vendorStorageVatPercent'].disabled = true;
	}
	
	}
	
	
function insuranceForVatExclude(){
	var forInsuranceVat=document.forms['billingForm'].elements['billing.insuranceVatExclude'].value; 
	if(forInsuranceVat == 'true' || forInsuranceVat == 'True'){
		document.forms['billingForm'].elements['billing.insuranceVatDescr'].value="";
		document.forms['billingForm'].elements['billing.insuranceVatDescr'].disabled = true;
		document.forms['billingForm'].elements['billing.insuranceVatPercent'].value="0";
		document.forms['billingForm'].elements['billing.insuranceVatPercent'].disabled = true;
	}
	
	}
</script>
<script type="text/javascript">
window.onload= function(){
	if (document.getElementById('autoProcessing').checked){
		payMethodType();
	}
}
try{
	checkShowHideNetworkBillToCode('onload');
}catch(e){}
</script>

<script type="text/javascript">

var http111 = getHTTPObject();

function payMethodType(){
	if(document.getElementById("autoProcessing").checked){ 
	    var url="getAutoProcessingAjax.html?ajax=1&decorator=simple&popup=true&bCode";
	    http111.open("GET", url, true); 
	    http111.onreadystatechange = handleHttp905111; 
	    http111.send(null);	     
   }else{
	   var targetElement = document.forms['billingForm'].elements['billing.billTo1Point'];
       var targetElement2 = document.forms['billingForm'].elements['billing.payMethod'];
		targetElement.length = "${fn:length(stoPaytype)+1}";
		targetElement2.length = "${fn:length(stoPaytype)+1}";
			
	   	var i = 1;
	    targetElement.options[0].text = '';
	 	targetElement.options[0].value = '';
	 		
	 	targetElement2.options[0].text = '';
	 	targetElement2.options[0].value = '';
	 	   
	   <c:forEach items="${stoPaytype}" var="entry">
		    targetElement.options[i].text = '${entry.value}';
			targetElement.options[i].value = '${entry.key}';
					
			targetElement2.options[i].text = '${entry.value}';
			targetElement2.options[i].value = '${entry.key}';
			i++;
	  </c:forEach>
	  document.forms['billingForm'].elements['billing.billTo1Point'].value = '${billing.billTo1Point}';	
		document.forms['billingForm'].elements['billing.payMethod'].value ='${billing.payMethod}';
   }
}
function handleHttp905111(){
    if (http111.readyState == 4){    	
            var results = http111.responseText
            results = results.trim();
            var res = results.split("@");
            var targetElement = document.forms['billingForm'].elements['billing.billTo1Point'];
            var targetElement2 = document.forms['billingForm'].elements['billing.payMethod'];
				targetElement.length = res.length;
				targetElement2.length = res.length;
					for(i=1;i<res.length;i++)
					{
						var ss=res[i].split('~');
						document.forms['billingForm'].elements['billing.billTo1Point'].options[i].text = ss[1];
						document.forms['billingForm'].elements['billing.billTo1Point'].options[i].value = ss[0];
						
						document.forms['billingForm'].elements['billing.payMethod'].options[i].text = ss[1];
						document.forms['billingForm'].elements['billing.payMethod'].options[i].value = ss[0];
					}
					if(${billing.billTo1Point!=null && billing.billTo1Point!=''}){
						document.forms['billingForm'].elements['billing.billTo1Point'].value = '${billing.billTo1Point}';	
						document.forms['billingForm'].elements['billing.payMethod'].value = '${billing.payMethod}';	
					}
    	}
 }


function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}

</script>
<%-- Shifting Complete --%>

<script type="text/javascript">
<c:if test="${redirectflag=='1'}">
	 <c:if test="${checkPropertyAmountComponent!='Y'}">
	 	<c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/editBilling.html?id=${serviceOrder.id}&msgClicked=${msgClicked}"/>
	</c:if>
</c:if>
showOrHide(0);
try{
	if('${hitflag}'!= 1){	
		getPricing();
	}
}
catch(e){}

try {
	accordingBillToCode();
}catch(e){
}finally {
	authorizationNo2();
}
 
try {
	//checkBillingInstructionCode();
} catch(e) {
} finally {
} 
try {
	var additionalNoVal = 'N';
		try{
		additionalNoVal = document.forms['billingForm'].elements['additionalNoFlag'].value;
		}catch(e){}
	if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'N'){
	    document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].selectedIndex =0;
		document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].disabled = true;
		document.getElementById("disDiv").style.display = "block";
		if(document.getElementById("displayDiv") != null){
			document.getElementById("displayDiv").style.display = "none";
			document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
		}
			if(document.getElementById("displayDivAdditionalNo") != null){
					document.getElementById("displayDivAdditionalNo").style.display = "none";
				}
			if(additionalNoVal=='N'){		
				if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'Y'){
					document.getElementById("disDiv1").style.display = "block";
					document.getElementById("disDiv2").style.display = "block";
				}else if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'V'){
					document.getElementById("disDiv1").style.display = "block";
					document.getElementById("disDiv2").style.display = "block";
				}else{
					document.getElementById("disDiv1").style.display = "none";
					document.getElementById("disDiv2").style.display = "none";
				}
			}else{
				document.getElementById("disDiv1").style.display = "block";
				document.getElementById("disDiv2").style.display = "block";
				checkVendorName();
			}	
	}else{
	    document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].disabled = false;
		document.getElementById("disDiv").style.display = "none";
		if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'Y'){
			document.getElementById("disDiv1").style.visibility = "visible";
			document.getElementById("disDiv2").style.visibility = "visible";
			try{
			    checkVendorName();
			    }catch(e){}
		}else if(document.forms['billingForm'].elements['billing.insuranceHas'].value == 'V'){
			if(additionalNoVal=='N'){
			document.getElementById("disDiv1").style.display = "block";
			document.getElementById("disDiv2").style.display = "block";
			if(document.getElementById("displayDiv") != null){				
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
   			if(document.getElementById("displayDivAdditionalNo") != null){
					document.getElementById("displayDivAdditionalNo").style.display = "none";
				}
			}else{
				document.getElementById("disDiv1").style.display = "block";
				document.getElementById("disDiv2").style.display = "block";
				checkVendorName();
			}
		}else if(document.forms['billingForm'].elements['billing.insuranceHas'].value == ''){
			document.getElementById("disDiv1").style.display = "none";
			document.getElementById("disDiv2").style.display = "none";
			if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
   			if(document.getElementById("displayDivAdditionalNo") != null){
					document.getElementById("displayDivAdditionalNo").style.display = "none";
				}					
		}else{
			if(additionalNoVal=='N'){
			document.getElementById("disDiv1").style.display = "none";
			document.getElementById("disDiv2").style.display = "none";
			if(document.getElementById("displayDiv") != null){
				document.getElementById("displayDiv").style.display = "none";
				document.forms['billingForm'].elements['billing.estMoveCost'].value="0.00";
			}
   			if(document.getElementById("displayDivAdditionalNo") != null){
					document.getElementById("displayDivAdditionalNo").style.display = "none";
				}			
			}else{
				document.getElementById("disDiv1").style.display = "block";
				document.getElementById("disDiv2").style.display = "block";
				checkVendorName();
			}
		}
	}	
} finally {
	autoLinkCreditCard();
}   
 
function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	}
}

function getBillToCode(){
	
	
	var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	findBillToName();
    if(billToCode!=''){
      showOrHide(1);
      <c:if test="${cmmDmmAgent}">
      var linkedFile = '${trackingStatus.soNetworkGroup}';
      var dbBillToCode = '${billing.billToCode}';
      if((linkedFile==true || linkedFile=='true') && (billToCode!=dbBillToCode)){
      	var agree = confirm("Changing of Bill To Code may leads to change of contract, Do you Agree ?");
  			if(agree){
  			}else{
  				document.forms['billingForm'].elements['billing.billToCode'].value = '${billing.billToCode}';
  			}
      }
     </c:if>
    } 
	accordingBillToCode();
	checkMultiAuthorization();
	findPricingBillingPaybaleBILLName();
	var vatBillingGroupCheck=true;
    <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
    vatBillingGroupCheck=false;
    </configByCorp:fieldVisibility>
    if(vatBillingGroupCheck){
	findDefaultSettingFromPartner();
    }
}
// var http222 = getHTTPObjectPricingBilling();
function findPricingBillingPaybaleBILLName(){
    var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
    var job = '${serviceOrder.job}';
    var compDivision='${serviceOrder.companyDivision}';
    var url="pricingBillingPaybaleName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode) + "&jobType=" + encodeURI(job) +"&compDivision="+encodeURI(compDivision);
    http222.open("GET", url, true);
    http222.onreadystatechange = handleHttpResponse2001;
    http222.send(null);
}
function handleHttpResponse2001(){	
             if (http222.readyState == 4){
                var results = http222.responseText
                results = results.trim();
                var res = results.split("#");                 
                       	if(res.length >= 3){ 
                			var test = res[0];
  							var x=document.getElementById("personPricing")
  							var personPricing = document.getElementById('personPricing').selectedIndex;
  						
 							for(var a = 0; a < x.length; a++)
 							{
 								if(test == document.forms['billingForm'].elements['billing.personPricing'].options[a].value)
 								{
 									document.forms['billingForm'].elements['billing.personPricing'].options[a].selected="true";
 								}
 							}
 							var testBilling = res[1];
  							var y=document.getElementById("personBilling")
  							var personBilling = document.getElementById('personBilling').selectedIndex;
  							
 							for(var a = 0; a < y.length; a++)
 							{
 								if(testBilling == document.forms['billingForm'].elements['billing.personBilling'].options[a].value)
 								{
 									document.forms['billingForm'].elements['billing.personBilling'].options[a].selected="true";
 								}
 							
 							}
 							var testPayable = res[2];
  							var z=document.getElementById("personPayable")
  							var personPayable = document.getElementById('personPayable').selectedIndex;
  							
 							for(var a = 0; a < z.length; a++)
 							{
 								if(testPayable == document.forms['billingForm'].elements['billing.personPayable'].options[a].value)
 								{
 									document.forms['billingForm'].elements['billing.personPayable'].options[a].selected="true";
 								}
 							
 							}
		           		    
		           		    var testAuditor = res[3];
		           		    var t=document.getElementById("auditorBilling")
  							var personAuditor = document.getElementById('auditorBilling').selectedIndex;
  							
 							for(var a = 0; a < t.length; a++)
 							{
 								if(testAuditor == document.forms['billingForm'].elements['billing.auditor'].options[a].value)
 								{
 									document.forms['billingForm'].elements['billing.auditor'].options[a].selected="true";
 								}
 							
 							}
 							var testClaimHandler = res[5];
 							if(document.getElementById("claimHandler")!=undefined){
 							var c=document.getElementById("claimHandler");
 							var claimPerson=document.getElementById('claimHandler').selectedIndex;
 							for(var a=0; a<c.length;a++)
		           			{
 								if(testClaimHandler == document.forms['billingForm'].elements['billing.claimHandler'].options[a].value){
 									document.forms['billingForm'].elements['billing.claimHandler'].options[a].selected="true";
 								}
 								
							}
 							}
 							if(res[6]!= null && res[6]!='' && res[6]!= '#'){
 			               		document.forms['billingForm'].elements['billing.internalBillingPerson'].value=res[6];
 		                    }
 							if(res[7]!= null && res[7]!='' && res[7]!= '#'){
 			               		document.forms['billingForm'].elements['billing.storageEmail'].value=res[7];
 		                    }else{
 		                    	document.forms['billingForm'].elements['billing.storageEmail'].value='';
 		                    }
 							if(res[8]!= null && res[8]!='' && res[8]!= '#'){
 			               		document.forms['billingForm'].elements['billing.storageEmailType'].value=res[8];
 		                    }else{
 		                    	document.forms['billingForm'].elements['billing.storageEmailType'].value='';
 		                    }
		           		}
		           		
           }
             billingEmailImage();
}
function getBillTo2Code(){
	findBillTo2Name();
	
	var billTo2Code = document.forms['billingForm'].elements['billing.billTo2Code'].value;
    if(billTo2Code!='') { 
      	showOrHide(1);
    }
    var vatBillingGroupCheck=true;
    <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
    vatBillingGroupCheck=false;
    </configByCorp:fieldVisibility>
    if(vatBillingGroupCheck){
    findDefaultVatFromPartner();
    }
}
function getBillTo3Code(){
	findBillingName();
	
	var billingCode = document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value;
    if(billingCode!=''){
    	showOrHide(1);
    }
    var vatBillingGroupCheck=true;
    <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
    vatBillingGroupCheck=false;
    </configByCorp:fieldVisibility>
    if(vatBillingGroupCheck){
    findDefaultVatFromPartnerForPP();
    }
}

function findExchangeEstRate(){
    var country =document.forms['billingForm'].elements['billing.currency'].value; 
    var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(country);
    http77.open("GET", url, true);
    http77.onreadystatechange = handleHttpResponseEstRate;
    http77.send(null);
}
function handleHttpResponseEstRate() { 
         if (http77.readyState == 4) {
            var results = http77.responseText
            results = results.trim(); 
            results = results.replace('[','');
            results=results.replace(']','');  
            if(results.length>1) {
              document.forms['billingForm'].elements['billing.exchangeRate'].value=results ;
              } else {
              document.forms['billingForm'].elements['billing.exchangeRate'].value=1;
              document.forms['billingForm'].elements['billing.exchangeRate'].readOnly=false; 
              }
              var mydate=new Date();
	          var daym;
	          var year=mydate.getFullYear()
	          var y=""+year;
	          if (year < 1000)
	          year+=1900
	          var day=mydate.getDay()
	          var month=mydate.getMonth()+1
	          if(month == 1)month="Jan";
	          if(month == 2)month="Feb";
	          if(month == 3)month="Mar";
			  if(month == 4)month="Apr";
			  if(month == 5)month="May";
			  if(month == 6)month="Jun";
			  if(month == 7)month="Jul";
			  if(month == 8)month="Aug";
			  if(month == 9)month="Sep";
			  if(month == 10)month="Oct";
			  if(month == 11)month="Nov";
			  if(month == 12)month="Dec";
			  var daym=mydate.getDate()
			  if (daym<10)
			  daym="0"+daym
			  var datam = daym+"-"+month+"-"+y.substring(2,4); 
			  document.forms['billingForm'].elements['billing.valueDate'].value=datam;  
			 }
         calBaseInsVal();  
		 }
function calBaseInsVal(){
	var insActVal = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
	var exchangeRate = document.forms['billingForm'].elements['billing.exchangeRate'].value;
	if(insActVal!='' && exchangeRate!='' ){
		var temp =Math.round((insActVal/exchangeRate)*100)/100;
		document.forms['billingForm'].elements['billing.baseInsuranceValue'].value = temp; 
	}
	calBaseTotal();
}

</script>
<script type="text/javascript">
var fieldName = document.forms['billingForm'].elements['field'].value; 
var fieldName1 = document.forms['billingForm'].elements['field1'].value;
if(fieldName!=''){
document.forms['billingForm'].elements[fieldName].className = 'rules-textUpper';
animatedcollapse.addDiv('status', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('sec', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('private', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('domestic', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('gstAutho', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('insurance', 'fade=0,persist=0,show=1')
animatedcollapse.init()
}
if(fieldName1!=''){
document.forms['billingForm'].elements[fieldName1].className = 'rules-textUpper';
animatedcollapse.addDiv('status', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('sec', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('private', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('domestic', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('gstAutho', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('storage', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('insurance', 'fade=0,persist=0,show=1')
animatedcollapse.init()
}

try{
    billingEmailImage();
}catch(e){}
</script>
<script type="text/javascript">
var varName = '';
	function checkBillingCompleteStatus(elementName)
	{
		if(elementName.indexOf('-') > 0)
			varName = elementName.split('-')[0];
		else
			varName = elementName;
		return varName;
	}
	function hitBilling(){
		if(varName == 'auditComplete')
		{
			findBillingCompleteAudit();
			varName='';
		}
		else if(varName == 'billing.billCompleteA' || varName == 'billComplete')
		{
			findBillingCompleteA();
			varName='';
		}
		else
		{
			varName='';
			return false;
		}
	}
	setOnSelectBasedMethods(["hitBilling(),changeBillingBillCompleteVar()"]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">
function setChargeCdProperty(){
	var billingInsuranceCrg = document.forms['billingForm'].elements['billing.insuranceCharge'].value;
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
    var url="findChargeInsuranceAjax.html?ajax=1&decorator=simple&popup=true&charge="+billingInsuranceCrg+"&contract="+billingContract;
	http17.open("GET", url, true);
    http17.onreadystatechange = handleHttpResponse17;
    http17.send(null);
}
function handleHttpResponse17(){
	if (http17.readyState == 4){
            var results = http17.responseText
            results = results.trim();
            var split = results.split("-");
            if(split[0]>0 ||split[1]>0 ){
            document.forms['billingForm'].elements['billing.insuranceBuyRate'].value=split[0];
            document.forms['billingForm'].elements['billing.insuranceRate'].value=split[1];

            calculateTotal();
            calBaseTotal();
            }
	} 
}
var http17 = getHTTPObject17();
function getHTTPObject17() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

String.prototype.replaceAt=function(index, char) {
    return this.substr(0, index) + char + this.substr(index+char.length);
 }
</script>
<script type="text/javascript">
var httpVendorCurr = getHTTPObject();
var httpInsPayableCurr = getHTTPObject();
function findVendorCurrencyCodeAjax() {
	showHide("block");
    var partnerCode = document.forms['billingForm'].elements['billing.vendorCode1'].value;  
    var url="findVendorCurrencyCodeAjax.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerCode;  
    httpVendorCurr.open("GET", url, true); 
    httpVendorCurr.onreadystatechange = responseVendorCurrencyCode; 
    httpVendorCurr.send(null); 
} 
function responseVendorCurrencyCode() {
	if (httpVendorCurr.readyState == 4){
	   showHide("none");
       var results = httpVendorCurr.responseText
       results = results.trim();
       document.forms['billingForm'].elements['billing.vendorBillingCurency'].value=results;
	}
}
function findPayableCurrencyCodeAjax(){ 
    var partnerCode = document.forms['billingForm'].elements['billing.vendorCode'].value;  
    if(partnerCode!=''){
    showHide("block");
    var url="findVendorCurrencyCodeAjax.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerCode;
    httpInsPayableCurr.open("GET", url, true);
    httpInsPayableCurr.onreadystatechange = responsePayableCurrencyCode; 
    httpInsPayableCurr.send(null); 
    }else{
    	document.forms['billingForm'].elements['billing.billingInsurancePayableCurrency'].value="";
    }
    
}
function responsePayableCurrencyCode() {
	if (httpInsPayableCurr.readyState == 4){
	   showHide("none");
       var results = httpInsPayableCurr.responseText
       results = results.trim();
       document.forms['billingForm'].elements['billing.billingInsurancePayableCurrency'].value=results;
	}
}
function findBillToCurrencyCodeAjax() {
	showHide("block");
    var partnerCode = document.forms['billingForm'].elements['billing.billToCode'].value;  
    var url="findVendorCurrencyCodeAjax.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerCode;  
    httpVendorCurr.open("GET", url, true); 
    httpVendorCurr.onreadystatechange = findBillToCurrencyCode; 
    httpVendorCurr.send(null); 
} 

function findBillToCurrencyCode() {
	try{

	if (httpVendorCurr.readyState == 4){
	   showHide("none");
       var results = httpVendorCurr.responseText
       results = results.trim();
       document.forms['billingForm'].elements['billing.billingCurrency'].value=results;
	}
	}catch(e){}
}
function showHide(action){
	document.getElementById("loader").style.display = action;
}

function openPopWindow(target){
	var targetName=target.name;
	var first = '${serviceOrder.firstName}';
	var last = '${serviceOrderLastName}';
	last = last.replace(/&#039;/g,"'");
	var orgAdd1= '${serviceOrder.originAddressLine1}';
	var orgAdd2= '${serviceOrder.originAddressLine2}';
	var orgAdd3= '${serviceOrder.originAddressLine3}';
	var orgCountry= '${serviceOrder.originCountry}';
	var orgState= '${serviceOrder.originState}';
	var orgCity= '${serviceOrder.originCity}';
	var orgZip= '${serviceOrder.originZip}';
	var phone = '${serviceOrder.originHomePhone}';
	var orgphone = '${serviceOrder.originHomePhone}';
	var orgemail = '${serviceOrder.email}';
	var email = '${serviceOrder.email}';
	var orgeFax= '${serviceOrder.originFax}';
	var prefix= '${serviceOrder.prefix}';
	var companyDiv='${serviceOrder.companyDivision}';
	var status='${serviceOrder.status}'; 
	var orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var destAdd1= '${serviceOrder.destinationAddressLine1}';
	var destAdd2= '${serviceOrder.destinationAddressLine2}';
	var destAdd3= '${serviceOrder.destinationAddressLine3}';
	var destCountry= '${serviceOrder.destinationCountry}';
	var destState= '${serviceOrder.destinationState}';
	var destCity= '${serviceOrder.destinationCity}';
	var destZip= '${serviceOrder.destinationZip}';
	var destphone = '${serviceOrder.destinationHomePhone}';
	var destemail = '${serviceOrder.destinationEmail}';
	var destFax= '${serviceOrder.destinationFax}';
	var destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var compDiv= '${serviceOrder.companyDivision}';
	javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&onlyAC=y&flag=3&firstName=${serviceOrder.firstName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&orgAddress='+orgAddress+'&lastName='+encodeURIComponent(last)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=serviceOrder.billToName&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billToName&fld_code=billing.billToCode');
}
function openPopWindowSec(target){
	var targetName=target.name;
	var first = '${serviceOrder.firstName}';
	var last = '${serviceOrderLastName}';
	last = last.replace(/&#039;/g,"'");
	var orgAdd1= '${serviceOrder.originAddressLine1}';
	var orgAdd2= '${serviceOrder.originAddressLine2}';
	var orgAdd3= '${serviceOrder.originAddressLine3}';
	var orgCountry= '${serviceOrder.originCountry}';
	var orgState= '${serviceOrder.originState}';
	var orgCity= '${serviceOrder.originCity}';
	var orgZip= '${serviceOrder.originZip}';
	var phone = '${serviceOrder.originHomePhone}';
	var orgphone = '${serviceOrder.originHomePhone}';
	var orgemail = '${serviceOrder.email}';
	var email = '${serviceOrder.email}';
	var orgeFax= '${serviceOrder.originFax}';
	var prefix= '${serviceOrder.prefix}';
	var companyDiv='${serviceOrder.companyDivision}';
	var status='${serviceOrder.status}'; 
	var orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var destAdd1= '${serviceOrder.destinationAddressLine1}';
	var destAdd2= '${serviceOrder.destinationAddressLine2}';
	var destAdd3= '${serviceOrder.destinationAddressLine3}';
	var destCountry= '${serviceOrder.destinationCountry}';
	var destState= '${serviceOrder.destinationState}';
	var destCity= '${serviceOrder.destinationCity}';
	var destZip= '${serviceOrder.destinationZip}';
	var destphone = '${serviceOrder.destinationHomePhone}';
	var destemail = '${serviceOrder.destinationEmail}';
	var destFax= '${serviceOrder.destinationFax}';
	var destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var compDiv= '${serviceOrder.companyDivision}';
	javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&onlyAC=y&flag=1&firstName=${serviceOrder.firstName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&orgAddress='+encodeURIComponent(orgAddress)+'&lastName='+encodeURIComponent(last)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=serviceOrder.billToName&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billTo2Name&fld_code=billing.billTo2Code');
}
function openPopWindowPrivate(target){
	var targetName=target.name;
	var first = '${serviceOrder.firstName}';
	var last = '${serviceOrderLastName}';
	last = last.replace(/&#039;/g,"'");
	var orgAdd1= '${serviceOrder.originAddressLine1}';
	var orgAdd2= '${serviceOrder.originAddressLine2}';
	var orgAdd3= '${serviceOrder.originAddressLine3}';
	var orgCountry= '${serviceOrder.originCountry}';
	var orgState= '${serviceOrder.originState}';
	var orgCity= '${serviceOrder.originCity}';
	var orgZip= '${serviceOrder.originZip}';
	var phone = '${serviceOrder.originHomePhone}';
	var orgphone = '${serviceOrder.originHomePhone}';
	var orgemail = '${serviceOrder.email}';
	var email = '${serviceOrder.email}';
	var orgeFax= '${serviceOrder.originFax}';
	var prefix= '${serviceOrder.prefix}';
	var companyDiv='${serviceOrder.companyDivision}';
	var status='${serviceOrder.status}'; 
	var orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var destAdd1= '${serviceOrder.destinationAddressLine1}';
	var destAdd2= '${serviceOrder.destinationAddressLine2}';
	var destAdd3= '${serviceOrder.destinationAddressLine3}';
	var destCountry= '${serviceOrder.destinationCountry}';
	var destState= '${serviceOrder.destinationState}';
	var destCity= '${serviceOrder.destinationCity}';
	var destZip= '${serviceOrder.destinationZip}';
	var destphone = '${serviceOrder.destinationHomePhone}';
	var destemail = '${serviceOrder.destinationEmail}';
	var destFax= '${serviceOrder.destinationFax}';
	var destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var compDiv= '${serviceOrder.companyDivision}';
	javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&onlyAC=y&flag=2&firstName=${serviceOrder.firstName}&phone=${serviceOrder.originHomePhone}&email=${serviceOrder.email}&orgAddress='+encodeURIComponent(orgAddress)+'&lastName='+encodeURIComponent(last)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv=${serviceOrder.companyDivision}&decorator=popup&popup=true&fld_sixthDescription=serviceOrder.billToName&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.privatePartyBillingName&fld_code=billing.privatePartyBillingCode');
}
</script>
<script type="text/javascript"> 
try{
checkMultiAuthorization();	
}catch(e){
}         
 </script>
 <script type="text/javascript"> 
 var httpContractCurr = getHTTPObject();
	function getContractCurrency(){
		var charge = document.forms['billingForm'].elements['billing.charge'].value;
		var contract = document.forms['billingForm'].elements['billing.contract'].value;
	    var url="findContractCurrencyFromChargeAjax.html?ajax=1&decorator=simple&popup=true&contract=${billing.contract}&charge="+charge;
	    httpContractCurr.open("GET", url, true);
	    httpContractCurr.onreadystatechange = getContractCurrencyResponse;
	    httpContractCurr.send(null);
	}
	
	function getContractCurrencyResponse(){
		if (httpContractCurr.readyState == 4){
		       var results = httpContractCurr.responseText;
		       results = results.trim();
		       var curr = results.split('@-');
		       document.forms['billingForm'].elements['billing.contractCurrency'].value = curr[0];
		       document.forms['billingForm'].elements['billing.payableContractCurrency'].value = curr[1];
		}
	}
	function fillRecAccAuditDate(){
		 var sid = document.forms['billingForm'].elements['billing.id'].value;
		 var auditComplete =null;
		 if(document.forms['billingForm'].elements['billing.auditComplete'].value!=''){
			 auditComplete= document.forms['billingForm'].elements['billing.auditComplete'].value;
		 }
		if(sid!=''){
			var agree = confirm("Update the Audit Complete Date for non-invoiced accounting lines");
		       if(agree)
		        {
		    	   progressBarAutoSave('1');
		    	   var url="updateAccAuditDate.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid)+"&auditComplete="+auditComplete;
		    	     http4.open("GET", url, true);
		    	     http4.onreadystatechange = handleHttpResponseUpdateAccAuditDate;
		    	     http4.send(null);   
		        }
		       else
		        {
			        return false;
		        }
		}
	}
	function handleHttpResponseUpdateAccAuditDate(){
	     if (http4.readyState == 4){
	           var result= http4.responseText 
	           progressBarAutoSave('0');        
	     }
	}
	 
	function fillRecAccVat(temp){
		var billtoCode="";
		var check="";
		var billingVatCode="";
		
		if(temp=='primaryVatCode'){
			billtoCode=document.forms['billingForm'].elements['billing.billToCode'].value
			billingVatCode=document.forms['billingForm'].elements['billing.primaryVatCode'].value
			check="1";
		}
		if(temp=='secondaryVatCode'){
			billtoCode=document.forms['billingForm'].elements['billing.billTo2Code'].value
			billingVatCode=document.forms['billingForm'].elements['billing.secondaryVatCode'].value
			check="1";
		}
		if(temp=='privatePartyVatCode'){
			billtoCode=document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value
			billingVatCode=document.forms['billingForm'].elements['billing.privatePartyVatCode'].value
			check="1";
		}
		if(billtoCode!='' && check=='1' && billingVatCode!=''){
			var agree = confirm("Update the VAT code for "+ billtoCode);
		       if(agree)
		        {
		    	   progressBarAutoSave('1');
		    	   var sid = document.forms['billingForm'].elements['billing.id'].value;
		    	   var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
		    	   var url="updateAccRecVat.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid)+"&billToCode="+encodeURI(billtoCode)+"&billingContract="+encodeURI(billingContract)+"&billingVatCode="+encodeURI(billingVatCode);
		    	 //alert(url)
		    	      http3.open("GET", url, true);
		    	      http3.onreadystatechange = handleHttpResponseUpdateAccRecVat;
		    	      http3.send(null);   
		        }
		       else
		        {
		        }
		}
	}
	function handleHttpResponseUpdateAccRecVat(){
	     if (http3.readyState == 4){
	           var result= http3.responseText 
	           progressBarAutoSave('0');        
	     }
	}

	function fillPayAccVat(temp){
		var vendorCode="";
		var check="";
		var vendorVatCode="";
		if(temp=='originAgentVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.originAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.originAgentVatCode'].value
			check="1";
		}	
		if(temp=='destinationAgentVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.destinationAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.destinationAgentVatCode'].value
			check="1";
		}
		if(temp=='originSubAgentVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.originSubAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.originSubAgentVatCode'].value
			check="1";
		}
		if(temp=='destinationSubAgentVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.destinationSubAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.destinationSubAgentVatCode'].value
			check="1";
		}
		if(temp=='forwarderVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.forwarderCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.forwarderVatCode'].value
			check="1";
		}
		if(temp=='brokerVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.brokerCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.brokerVatCode'].value
			check="1";
		}
		if(temp=='networkPartnerVatCode'){
			vendorCode=document.forms['billingForm'].elements['trackingStatus.networkPartnerCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.networkPartnerVatCode'].value
			check="1";
		}
		if(temp=='bookingAgentVatCode'){ 
			vendorCode=document.forms['billingForm'].elements['serviceOrder.bookingAgentCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.bookingAgentVatCode'].value
			check="1";
		}
		if(temp=='vendorCodeVatCode'){
			vendorCode=document.forms['billingForm'].elements['billingVendorCode'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.vendorCodeVatCode'].value
			check="1";
		}
		if(temp=='vendorCodeVatCode1'){
			vendorCode=document.forms['billingForm'].elements['billingVendorCode1'].value
			vendorVatCode=document.forms['billingForm'].elements['billing.vendorCodeVatCode1'].value
			check="1";
		}  
		if(vendorCode!='' && check=='1' && vendorVatCode!=''){
			var agree = confirm("Update the VAT code for "+ vendorCode);
		       if(agree)
		        {
		    	   progressBarAutoSave('1');
		    	   var sid = document.forms['billingForm'].elements['billing.id'].value;
		    	   var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
		    	   var url="updateAccPayVat.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(sid)+"&vendorCode="+encodeURI(vendorCode)+"&billingContract="+encodeURI(billingContract)+"&vendorVatCode="+encodeURI(vendorVatCode);
		    	 //alert(url)
		    	      http3.open("GET", url, true);
		    	      http3.onreadystatechange = handleHttpResponseUpdateAccPayVat;
		    	      http3.send(null);   
		        }
		       else
		        {
		        }
		}
	}
	function handleHttpResponseUpdateAccPayVat(){
	     if (http3.readyState == 4){
	           var result= http3.responseText 
	           progressBarAutoSave('0');        
	     }
	}


	function checkPayAccVat(temp,vendorCode){ 
		var vendorVatCode=temp.value;
		<c:if test="${trackingStatus.originAgentCode !='' && not empty trackingStatus.originAgentCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.originAgentCode'].value){
			  document.forms['billingForm'].elements['billing.originAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.destinationAgentCode !='' && not empty trackingStatus.destinationAgentCode}">	
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.destinationAgentCode'].value){
			 document.forms['billingForm'].elements['billing.destinationAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.originSubAgentCode !='' && not empty trackingStatus.originSubAgentCode}">	
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.originSubAgentCode'].value){
			 document.forms['billingForm'].elements['billing.originSubAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.destinationSubAgentCode !='' && not empty trackingStatus.destinationSubAgentCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.destinationSubAgentCode'].value){
			 document.forms['billingForm'].elements['billing.destinationSubAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.forwarderCode !='' && not empty trackingStatus.forwarderCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.forwarderCode'].value){
			 document.forms['billingForm'].elements['billing.forwarderVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.brokerCode !='' && not empty trackingStatus.brokerCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.brokerCode'].value){
			 document.forms['billingForm'].elements['billing.brokerVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${trackingStatus.networkPartnerCode !='' && not empty trackingStatus.networkPartnerCode}">
		if(vendorCode==document.forms['billingForm'].elements['trackingStatus.networkPartnerCode'].value){
			 document.forms['billingForm'].elements['billing.networkPartnerVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${serviceOrder.bookingAgentCode !='' && not empty serviceOrder.bookingAgentCode}">
		if(vendorCode==document.forms['billingForm'].elements['serviceOrder.bookingAgentCode'].value){ 
			 document.forms['billingForm'].elements['billing.bookingAgentVatCode'].value=vendorVatCode
			 
		}
		</c:if>
		<c:if test="${billing.vendorCode !='' && not empty billing.vendorCode}">
		if(vendorCode==document.forms['billingForm'].elements['billingVendorCode'].value){
			 document.forms['billingForm'].elements['billing.vendorCodeVatCode'].value=vendorVatCode
			 
		} 
		</c:if>
		<c:if test="${billing.vendorCode1 !='' && not empty billing.vendorCode1}">
		if(vendorCode==document.forms['billingForm'].elements['billingVendorCode1'].value){
			 document.forms['billingForm'].elements['billing.vendorCodeVatCode1'].value=vendorVatCode
			 
		} 
		</c:if>
		 
	}
	
	
	var baseInsV = document.forms['billingForm'].elements['billing.baseInsuranceValue'].value;
	var InsValA = document.forms['billingForm'].elements['billing.insuranceValueActual'].value;
	var InsBuyR = document.forms['billingForm'].elements['billing.insuranceBuyRate'].value;	
	if(baseInsV=='0.000'){
		document.forms['billingForm'].elements['billing.baseInsuranceValue'].value='';
	}
	if(InsValA=='0.000'){ 
		document.forms['billingForm'].elements['billing.insuranceValueActual'].value='';
	}
	if(InsBuyR=='0.000'){
		document.forms['billingForm'].elements['billing.insuranceBuyRate'].value='';
	}
	
	try{
	var vendorVatP = document.forms['billingForm'].elements['billing.vendorStorageVatPercent'].value;
	if(vendorVatP=='0.0000'){
		document.forms['billingForm'].elements['billing.vendorStorageVatPercent'].value='';
	}
	}catch(e){}
	try{
		showDetailed();
	}catch(e){
		
	}
</script> 
 <script type="text/javascript">
 function checkBillingPerson(){
	 
	 <configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
	var saleManType=document.forms['billingForm'].elements['billing.personBilling'].value;
		if(document.forms['billingForm'].elements['invoiceCount'].value!='0'){
			var oldPersonBilling='${billing.personBilling}';
			var newPersonBilling=saleManType;
			<c:if test="${serviceOrder.companyDivision == 'CAJ' || serviceOrder.companyDivision == 'AIF'}">
			if(newPersonBilling!=oldPersonBilling){
			var url="checkParentAgentAjax.html?ajax=1&decorator=simple&popup=true&oldPersonBilling="+encodeURI(oldPersonBilling)+"&newPersonBilling="+encodeURI(newPersonBilling);			
			 http5551984.open("GET", url, true);
			 http5551984.onreadystatechange = handleHttpResponse9991984;
			 http5551984.send(null);
			}
			</c:if>
  	}
	</configByCorp:fieldVisibility> 
}	    

 function handleHttpResponse9991984() { 
      if (http5551984.readyState == 4)  {
         var results = http5551984.responseText
         results = results.trim();
         if(results=='NO'){
		 		alert("Invoice Already Generated.Cannot change the Billing Person.") 
	 	       document.forms['billingForm'].elements['billing.personBilling'].value='${billing.personBilling}';
         }      } }
function checkEmail() {
		var status = false;     
		var emailRegEx = /^[A-Z0-9.'_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
		     if (document.forms['billingForm'].elements['billing.storageEmail'].value.search(emailRegEx) == -1) {
		    	 document.forms['billingForm'].elements['billing.storageEmail'].value="";
		          alert("Please enter a valid email address.");
		     }else {
		        status = true;
		     }
		     return status;
		}
function blankEmailField(){
	if(!document.getElementById("emailInvoice").checked){
		 document.forms['billingForm'].elements['billing.storageEmail'].value="";
		 document.getElementById("storageEmail").readOnly = true;
	}else{
		document.getElementById("storageEmail").readOnly = false;
	}}
function findDefaultSettingFromPartner(){
	var bCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http521.open("GET", url, true); 
	   	http521.onreadystatechange = handleHttp921; 
	   	http521.send(null);	     
     }}
function handleHttp921(){
    if (http521.readyState == 4){    	
    	var results = http521.responseText
    	results = results.trim();
    	if(results!=''){
	    	var	res = results.split("~");
		    	if(document.forms['billingForm'].elements['billing.contract'].value==''){
		    		if(res[0]!='NA'){
						document.forms['billingForm'].elements['billing.contract'].value=res[0];
		    		}
		    	}
		    	if(document.forms['billingForm'].elements['billing.billToAuthority'].value==''){
		    		if(res[9]!='NA'){
		    			if(res[9]!="undefined" || res[9]!=undefined){
						document.forms['billingForm'].elements['billing.billToAuthority'].value=res[9];
		    			}else{
		    				document.forms['billingForm'].elements['billing.billToAuthority'].value="";	
		    			}
		    		}
		    	}
		    	if(document.forms['billingForm'].elements['billing.vendorCode'].value==''){
		    		if(res[4]!='NA'){
		    			document.forms['billingForm'].elements['billing.vendorCode'].value = res[4];
		    		}
		    	}
		    	var insOps=document.forms['billingForm'].elements['billing.insuranceOption'].value;
		    	if(insOps==''){
		    		if(res[7]!='NA'){
		    			document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value = res[7];
		    		}
		    	}
		    	var vatBillingGroupCheck=true;
		        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
		        vatBillingGroupCheck=false;
		        </configByCorp:fieldVisibility>
		        if(vatBillingGroupCheck){
		    	if(res[8]!='NA'){
			    	if(document.forms['billingForm'].elements['billing.primaryVatCode'].value==''){
					 	document.forms['billingForm'].elements['billing.primaryVatCode'].value = res[8];
			    	}			    	
		         }
		        }else{
		        	 var primaryVatCode = '${billing.primaryVatCode}';
		        	if(res[8]!='NA' && res[12]=='NODATA'){
				    	if(document.forms['billingForm'].elements['billing.primaryVatCode'].value==''){
						 	document.forms['billingForm'].elements['billing.primaryVatCode'].value = res[8];
				    	}			    	
			         }else{
			        	 if(res[8]!='NA' && res[12]!='NODATA'){
			        		 var chkVatBillingGroup=false;
			        		 chkVatBillingGroup = flex1RecVatCheck(res[12],res[8]); 
                             if(chkVatBillingGroup){
                            	 if(primaryVatCode != res[8]){
                            		 var agree = confirm("Updating Billtocode code will also change the  VAT code. Do you wish to continue? ");
                  					if(agree){
                  						document.forms['billingForm'].elements['billing.primaryVatCode'].value = res[8];
                  					}
                                 }
                            	   
                             } 
                              
			        	 }
			         }
		        	
		        }
		    	}    	} }
var http521 = getHTTPObject();

function findDefaultVatFromPartner(){ 
	<c:if test="${systemDefaultVatCalculation=='true'}">
	var bCode = document.forms['billingForm'].elements['billing.billTo2Code'].value;
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http5222.open("GET", url, true); 
	    http5222.onreadystatechange = handleHttp9222; 
	    http5222.send(null);	     
     }</c:if>}
function handleHttp9222(){
    if (http5222.readyState == 4){    	
    	var results = http5222.responseText
    	results = results.trim();
    	if(results!=''){
	    	var	res = results.split("~");
	    	var vatBillingGroupCheck=true;
	        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	        vatBillingGroupCheck=false;
	        </configByCorp:fieldVisibility>
	        if(vatBillingGroupCheck){ 
		    	if(res[8]!='NA'){
			    	if(document.forms['billingForm'].elements['billing.secondaryVatCode'].value==''){
					 	document.forms['billingForm'].elements['billing.secondaryVatCode'].value = res[8];
			    	}  }
	        }else{
	       	 var secondaryVatCode = '${billing.secondaryVatCode}';
	        	if(res[8]!='NA' && res[12]=='NODATA'){
			    	if(document.forms['billingForm'].elements['billing.secondaryVatCode'].value==''){
					 	document.forms['billingForm'].elements['billing.secondaryVatCode'].value = res[8];
			    	}			    	
		         }else{
		        	 if(res[8]!='NA' && res[12]!='NODATA'){
		        		 var chkVatBillingGroup=false;
		        		 chkVatBillingGroup = flex1RecVatCheck(res[12],res[8]); 
                      if(chkVatBillingGroup){
                    	  if(secondaryVatCode != res[8]){
                     		 var agree = confirm("Updating Billtocode code will also change the VAT code. Do you wish to continue? ");
           					if(agree){
           					 document.forms['billingForm'].elements['billing.secondaryVatCode'].value = res[8];
           					}
                          }  
                      } 
                      
		        	 }
		         }
	        	
	        }
		    	}
	        
    	} }
		    
function findDefaultVatFromPartnerForPP(){
	<c:if test="${systemDefaultVatCalculation=='true'}">

	var bCode = document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value;
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http5222.open("GET", url, true); 
	    http5222.onreadystatechange = handleHttp92222; 
	    http5222.send(null);	     
     }</c:if>}
function handleHttp92222(){
    if (http5222.readyState == 4){    	
    	var results = http5222.responseText
    	results = results.trim();
    	if(results!=''){
    		var	res = results.split("~");
	    	var vatBillingGroupCheck=true;
	        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	        vatBillingGroupCheck=false;
	        </configByCorp:fieldVisibility>
	        if(vatBillingGroupCheck){ 
		    	if(res[8]!='NA'){
			    	if(document.forms['billingForm'].elements['billing.privatePartyVatCode'].value==''){
					 	document.forms['billingForm'].elements['billing.privatePartyVatCode'].value = res[8];
			    	}  }
	        }else{
	       	 var privatePartyVatCode = '${billing.privatePartyVatCode}';
	        	if(res[8]!='NA' && res[12]=='NODATA'){
			    	if(document.forms['billingForm'].elements['billing.privatePartyVatCode'].value==''){
					 	document.forms['billingForm'].elements['billing.privatePartyVatCode'].value = res[8];
			    	}			    	
		         }else{
		        	 if(res[8]!='NA' && res[12]!='NODATA'){
		        		 var chkVatBillingGroup=false;
		        		 chkVatBillingGroup = flex1RecVatCheck(res[12],res[8]); 
                      if(chkVatBillingGroup){
                    	  if(privatePartyVatCode != res[8]){
                      		 var agree = confirm("Updating Billtocode code will also change the  VAT code. Do you wish to continue? ");
            					if(agree){
            						document.forms['billingForm'].elements['billing.privatePartyVatCode'].value = res[8];	
            					}
                           }
                     	  
                      } 
                       
		        	 }
		         }
	        	
	        }
		    	}    	} }	





function findDefaultVatFromPartnerForStorageVat(){
	<c:if test="${systemDefaultVatCalculation=='true'}">

	var bCode = document.forms['billingForm'].elements['billing.vendorCode1'].value;
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http5222.open("GET", url, true); 
	    http5222.onreadystatechange = handleHttpStorageVat92222; 
	    http5222.send(null);	     
     }</c:if>}
function handleHttpStorageVat92222(){
    if (http5222.readyState == 4){    	
    	var results = http5222.responseText
    	results = results.trim();
    	if(results!=''){
    		var	res = results.split("~");
	    	var vatBillingGroupCheck=true;
	        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	        vatBillingGroupCheck=false;
	        </configByCorp:fieldVisibility>
	        if(vatBillingGroupCheck){ 
		    	if(res[8]!='NA'){
			    	if(document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].value==''){
					 	document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].value = res[8];
			    	}  }
	        }else{
	       	 var vendorStorageVatDescr = '${billing.vendorStorageVatDescr}';
	        	if(res[8]!='NA' && res[12]=='NODATA'){
			    	if(document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].value==''){
					 	document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].value = res[8];
			    	}			    	
		         }else{
		        	 if(res[8]!='NA' && res[12]!='NODATA'){
		        		 var chkVatBillingGroup=false;
		        		 chkVatBillingGroup = flex2PayVatCheck(res[12],res[8]); 
                      if(chkVatBillingGroup){ 
                     	if(vendorStorageVatDescr != res[8]){
                   		 var agree = confirm("Updating Vendor code will also change the  VAT code. Do you wish to continue?");
         					if(agree){
         						 document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].value = res[8];	
         					}
                        }
                      } 
                       
		        	 }
		         }
	        	
	        }
		    	}    	} }
		    
var http5222 = getHTTPObject();

<c:if test="${(billing.invoiceByEmail != 'true' || billing.invoiceByEmail != true) && serviceOrder.job == 'STO'}">
document.getElementById("storageEmail").readOnly = true;
</c:if>
    </script>
<script type="text/javascript">
var anchor = document.getElementsByTagName("a");
for( var i = 0, j =  anchor.length; i < j; i++ ) {
anchor[i].setAttribute( 'tabindex', '-1' );
}
function isSoExtFlag(){
    document.forms['billingForm'].elements['isSOExtract'].value="yes";
  }
try{
makeMandIssueDt();
showIndicatorFields();
}catch(e){}
function autoCompleterAjaxCallChargeCode(divid,fieldName,idval){
	 <configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
	document.getElementById('chargeCodeValidationVal').value='BillingCharge';
	var ChargeName="";
		ChargeName = document.forms['billingForm'].elements['billing.'+fieldName].value;
	var contract = document.forms['billingForm'].elements['billing.contract'].value;
	contract=contract.trim();
	var category = "";
	var companyDiv="";
	companyDiv="";
	var pageCondition='TemplateNonInternalCategory';
	var id='';
	if(contract!=""){
		if(ChargeName.length>=3){
          $.get("ChargeCodeAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
          	searchName:ChargeName,
          	contract: contract,
          	chargeCodeId:idval,
          	autocompleteDivId: divid,
          	id:id,
          	idCondition:fieldName,
          	categoryCode:category,
          	pageCondition:pageCondition,
          	searchcompanyDivision:companyDiv
          }, function(idval) {
              document.getElementById(divid).style.display = "block";
              $("#" + divid).html(idval)
          })
      } else {
          document.getElementById(divid).style.display = "none"
      }	} 	else{
		alert("There is no Billing contract: Please select.");
		document.forms['billingForm'].elements['billing.'+fieldName].value="";
		document.forms['billingForm'].elements['billing.contract'].focus();
	}
	</configByCorp:fieldVisibility>	
}
function copyChargeDetails(chargecode,chargecodeId,autocompleteDivId,id,fieldName){
		document.forms['billingForm'].elements['billing.'+fieldName].value=chargecode;
		document.getElementById(autocompleteDivId).style.display = "none";
		document.forms['billingForm'].elements['chargeCodeValidationVal'].value='';
		checkValueAfterChargeCode(fieldName);
		findChargeRateFast();
		if(chargecodeId=='A'){
		chargePerValue();
		}
		changeStatus();
}
function closeMyChargeDiv(autocompleteDivId,chargecode,chargecodeId,id,fieldName){
	document.getElementById(autocompleteDivId).style.display = "none";
	 if(document.forms['billingForm'].elements['billing.'+fieldName].value==''){
		 document.forms['billingForm'].elements['billing.'+fieldName].value="";	
	}
	 document.forms['billingForm'].elements['chargeCodeValidationVal'].value='';
	 checkValueAfterChargeCode(fieldName);
	 findChargeRateFast();
	 if(chargecodeId=='A'){
		chargePerValue();
			}
	 changeStatus();
	}
function closeMyChargeDiv2(chargecodeId,id){
	 document.forms['billingForm'].elements['chargeCodeValidationVal'].value='';
	}
function checkValueAfterChargeCode(fieldName){
	var chargeCheck=document.forms['billingForm'].elements['chargeCodeValidationVal'].value;
	if(chargeCheck=='' || chargeCheck==null){
	var billingContract = document.forms['billingForm'].elements['billing.contract'].value;
	var chargeCode =document.forms['billingForm'].elements['billing.'+fieldName].value;
	if(billingContract=='' || billingContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	}
	else { 
    var url="checkChargeCodeNew.html?ajax=1&decorator=simple&popup=true&contractCde="+encodeURIComponent(billingContract)+"&chargeCde="+encodeURIComponent(chargeCode); 
    http5.open("GET", url, true);
    http5.onreadystatechange = function(){ handleHttpResponseValue(fieldName);};
    http5.send(null);
	}	}}
function handleHttpResponseValue(fieldName){
		if (http5.readyState == 4){
               var results = http5.responseText
               results = results.trim(); 
                results = results.replace('[','');
               results=results.replace(']',''); 
               var res = results.split("#"); 
   	        if(results.length>3)  {
               var vatExclude=false;
               if(results[0]=='N'){
               	vatExclude=false;
               }
               if(results[0]=='Y'){
               	vatExclude=true;
               }
               if(fieldName=='charge'){
               document.forms['billingForm'].elements['billing.storageVatExclude'].value =vatExclude;
               }
               if(fieldName=='insuranceCharge'){
               	document.forms['billingForm'].elements['billing.insuranceVatExclude'].value =vatExclude;
               }
				progressBarAutoSave('0'); 
        }
   	        else{
   	        	if(document.getElementById("ChargeNameDivIdA")!=null){
   	        		document.getElementById("ChargeNameDivIdA").style.display = "none";
   	        		}
   	        		if(document.getElementById("ChargeNameDivIdB")!=null){
   	        			document.getElementById("ChargeNameDivIdB").style.display = "none"; 
   	        			}
   	        		if(document.getElementById("ChargeNameDivIdC")!=null){
   	        			document.getElementById("ChargeNameDivIdC").style.display = "none";
   	        			}
   	        		document.forms['billingForm'].elements['billing.'+fieldName].value='';
   		            alert("Charge code does not exist according the Billing contract , Please select another" );
   		            return false;
   	        }}}
function fillSellBuyRate(){
 var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	if(billToCode != null && billToCode != ''){
 $.get("findSellBuyRate.html?ajax=1&decorator=simple&popup=true", 
         {billToCode: billToCode},
         function(data){
    var sellRateNew  =data.trim().split("~")[0];
    var buyRateNew  =data.trim().split("~")[1];
    var sellRate= document.forms['billingForm'].elements['billing.insuranceRate'].value;
    var buyRate =  document.forms['billingForm'].elements['billing.insuranceBuyRate'].value ;
    if(sellRateNew != 0.00 && sellRateNew!=undefined){
        document.forms['billingForm'].elements['billing.insuranceRate'].value=sellRateNew;
        var insrVal = document.forms['billingForm'].elements['billing.insuranceValueActual'].value*1;
        var insrRate = document.forms['billingForm'].elements['billing.insuranceRate'].value*1;
        var s=document.forms['billingForm'].elements['billing.insuranceOptionCodeWithDesc'].value;
            
        if( !(document.forms['billingForm'].elements['billing.insuranceRate'].value == '') ){
                var valu = (insrVal*insrRate)/100;
                var valu1 = Math.round(valu*100)/100;
                document.forms['billingForm'].elements['billing.totalInsrVal'].value = (valu1).toFixed(2);
                }
                else {
                document.forms['billingForm'].elements['billing.totalInsrVal'].value = '';
                }
    }
    
    if(buyRateNew != 0.00 && buyRateNew!=undefined){
       document.forms['billingForm'].elements['billing.insuranceBuyRate'].value=buyRateNew;
    }
     });
	}else{
		 document.forms['billingForm'].elements['billing.insuranceRate'].value=0.00000;
		 document.forms['billingForm'].elements['billing.insuranceBuyRate'].value=0.00000;
		 document.forms['billingForm'].elements['billing.totalInsrVal'].value = '';
	}
	}
	

function currentDateGlobal(){
	var mydate=new Date();
   	var daym;
   	var year=mydate.getFullYear()
   	var y=""+year;
   	if (year < 1000)
   	year+=1900
   	var day=mydate.getDay()
   	var month=mydate.getMonth()+1
   	if(month == 1)month="Jan";
   	if(month == 2)month="Feb";
   	if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function validation()
{
    var exchangeRate=document.forms['billingForm'].elements['billing.exchangeRate'].value;
    if(exchangeRate == 0.00000)
    	{
    	alert("ExchangeRate value not valid")
    	}
	
}

var httpTemplate = getHTTPObject9(); 
function getHTTPObject9(){
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
} 

function findBillingCycle(){

	 var billToCode = document.forms['billingForm'].elements['billing.billToCode'].value;
	if(billToCode!='') {
		  var url="billingCycle.html?ajax=1&decorator=simple&popup=true&billToCode=" +encodeURI(billToCode) 
				
	    httpTemplate.open("GET", url, true); 
	    httpTemplate.onreadystatechange = getHTTPObject19; 
	    httpTemplate.send(null);	     
     }
	
}

function getHTTPObject19()
{
	if (httpTemplate.readyState == 4){
		   var results= httpTemplate.responseText;
		   var results=results.trim();
		   results = results.replace('[','');
           results=results.replace(']',''); 
		   var res = results.split("#");  
	      	if(res !=''){
	       		var test = res[0];
						var x=document.getElementById("billingCycle1")
						var billingCycle = document.getElementById('billingCycle1').selectedIndex;

						for(var a = 1; a < x.length; a++)
							{	
								if(test == document.forms['billingForm'].elements['billing.billingCycle'].options[a].value)
								{

									document.forms['billingForm'].elements['billing.billingCycle'].options[a].selected="true";
								}
							}
	       	}
		else{
			   document.forms['billingForm'].elements['billing.billingCycle'].value='';
		}
 
       	
		}
}
var httprecVatBilling = gethttprecVatBilling();
function gethttprecVatBilling() {
var xmlhttp;
if(window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)  {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }   } 
return xmlhttp;
}

function findPrimaryRecVatWithVatGroup(){
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"> 
	  
	      var partnerCode = document.forms['billingForm'].elements['billing.billToCode'].value;
          var defaultAccountLineid = document.forms['billingForm'].elements['billing.id'].value;
          var primaryVatCode='${billing.primaryVatCode}';
    	  var url="primaryrecVatCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&id="+encodeURI(defaultAccountLineid)+"&primaryVatCode="+encodeURI(primaryVatCode);
		 httprecVatBilling.open("GET", url, true);
		 httprecVatBilling.onreadystatechange = handleHttpResponseprimaryrecVatBilling;
		 httprecVatBilling.send(null);
		 </configByCorp:fieldVisibility >
}

function handleHttpResponseprimaryrecVatBilling() { 
	 if (httprecVatBilling.readyState == 4){
    var results = httprecVatBilling.responseText 
      results = results.trim();
    results = results.replace('[','');
    results=results.replace(']',''); 
    res = results.split("@");
    var targetElement = document.forms['billingForm'].elements['billing.primaryVatCode'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					console.log(res);
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.primaryVatCode'].options[i].text = '';
					document.forms['billingForm'].elements['billing.primaryVatCode'].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.primaryVatCode'].options[i].value = stateVal[0];
					document.forms['billingForm'].elements['billing.primaryVatCode'].options[i].text = stateVal[1];
					} 
					}
				document.forms['billingForm'].elements['billing.primaryVatCode'].value = '${billing.primaryVatCode}';
				  findDefaultSettingFromPartner();
					 
  }
}

function secondaryRecVatWithVatGroup(){
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	var flag="secondary";
    var partnerCode = document.forms['billingForm'].elements['billing.billTo2Code'].value;
    var defaultAccountLineid = document.forms['billingForm'].elements['billing.id'].value;
    var secondaryVatCode='${billing.secondaryVatCode}';
	  var url="primaryrecVatCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&id="+encodeURI(defaultAccountLineid)+"&primaryVatCode="+encodeURI(secondaryVatCode);
	 httprecVatBilling.open("GET", url, true);
	 httprecVatBilling.onreadystatechange = handleHttpResponsesecondaryrecVatBilling;
	 httprecVatBilling.send(null);
	 </configByCorp:fieldVisibility >
}

function handleHttpResponsesecondaryrecVatBilling() { 
	 if (httprecVatBilling.readyState == 4){
    var results = httprecVatBilling.responseText
      results = results.trim();
    results = results.replace('[','');
    results=results.replace(']',''); 
    res = results.split("@");
    var targetElement = document.forms['billingForm'].elements['billing.secondaryVatCode'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					console.log(res);
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.secondaryVatCode'].options[i].text = '';
					document.forms['billingForm'].elements['billing.secondaryVatCode'].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.secondaryVatCode'].options[i].value = stateVal[0];
					document.forms['billingForm'].elements['billing.secondaryVatCode'].options[i].text = stateVal[1];
					}
					} 
				   document.forms['billingForm'].elements['billing.secondaryVatCode'].value = '${billing.secondaryVatCode}';
					findDefaultVatFromPartner();
				 
  }
}

function privatePartyRecVatWithVatGroup(){

	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	
    var partnerCode = document.forms['billingForm'].elements['billing.privatePartyBillingCode'].value;
    var defaultAccountLineid = document.forms['billingForm'].elements['billing.id'].value;
    var privatePartyVatCode='${billing.privatePartyVatCode}';
    var url="primaryrecVatCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&id="+encodeURI(defaultAccountLineid)+"&primaryVatCode="+encodeURI(privatePartyVatCode);
	 httprecVatBilling.open("GET", url, true);
	 httprecVatBilling.onreadystatechange = handleHttpResponseprivatePartyrecVatBilling;
	 httprecVatBilling.send(null);
	 </configByCorp:fieldVisibility >

}

function handleHttpResponseprivatePartyrecVatBilling() { 
	 if (httprecVatBilling.readyState == 4){
		    var results = httprecVatBilling.responseText
		      results = results.trim();
		    results = results.replace('[','');
		    results=results.replace(']',''); 
		    res = results.split("@");
		   
		    var targetElement = document.forms['billingForm'].elements['billing.privatePartyVatCode'];
					targetElement.length = res.length;
						for(i=0;i<res.length;i++)
							{
							console.log(res);
							if(res[i] == ''){
							document.forms['billingForm'].elements['billing.privatePartyVatCode'].options[i].text = '';
							document.forms['billingForm'].elements['billing.privatePartyVatCode'].options[i].value = '';
							}else{
							var stateVal = res[i].split("#");
							document.forms['billingForm'].elements['billing.privatePartyVatCode'].options[i].value = stateVal[0];
							document.forms['billingForm'].elements['billing.privatePartyVatCode'].options[i].text = stateVal[1];
							}
							} 
						     document.forms['billingForm'].elements['billing.privatePartyVatCode'].value = '${billing.privatePartyVatCode}';
							findDefaultVatFromPartnerForPP();
						 
		  }

}
function findStorageVatWithVatGroup(){
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
    var partnerCode = document.forms['billingForm'].elements['billing.vendorCode1'].value;
    var defaultAccountLineid = document.forms['billingForm'].elements['billing.id'].value;
    var vendorStorageVatDescr='${billing.vendorStorageVatDescr}';
    var url="findStorageVatDescr.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&id="+encodeURI(defaultAccountLineid)+"&vendorStorageVatDescr="+encodeURI(vendorStorageVatDescr);
	 httprecVatBilling.open("GET", url, true);
	 httprecVatBilling.onreadystatechange = handleHttpResponseVendorStoragerecVatBilling;
	 httprecVatBilling.send(null);
	 </configByCorp:fieldVisibility >

}

function handleHttpResponseVendorStoragerecVatBilling() { 
	 if (httprecVatBilling.readyState == 4){
     var results = httprecVatBilling.responseText
       results = results.trim();
     results = results.replace('[','');
     results=results.replace(']',''); 
     res = results.split("@");
     var targetElement = document.forms['billingForm'].elements['billing.vendorStorageVatDescr'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					console.log(res);
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].options[i].text = '';
					document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].options[i].value = stateVal[0];
					document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].options[i].text = stateVal[1];
					}
					}
				document.forms['billingForm'].elements['billing.vendorStorageVatDescr'].value = '${billing.vendorStorageVatDescr}';
				findDefaultVatFromPartnerForStorageVat();
          
   }
}

function flex1RecVatCheck(defaultVatBillingGroup,currentDefaultVatValue) { 
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"> 
	var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value;
	var vatBillingGroupFlexValue="";
	var defaultVatBillingGroupDesc="";
	var relo1="" 
	<c:forEach var="entry" items="${vatBillingGroupList}">
	  if(relo1==""){ 
      if(defaultVatBillingGroup=='${entry.key}') {
    	  defaultVatBillingGroupDesc='${entry.value}';
    	  relo1="yes";
      }} 
	</c:forEach>
	var relo=""
		<c:forEach var="entry" items="${flex1RecCodeList}">
	  if(relo==""){ 
        if(defaultVatBillingGroupDesc+"~"+companyDivision=='${entry.key}' || defaultVatBillingGroupDesc+"~NODATA"=='${entry.key}') {
        	vatBillingGroupFlexValue='${entry.value}';
           relo="yes";
        }} 
	</c:forEach> 
	vatBillingGroupFlexValue = vatBillingGroupFlexValue+",";
	var currentDefaultVatValueTest =  currentDefaultVatValue+",";
	if(currentDefaultVatValue !="" && vatBillingGroupFlexValue.includes(currentDefaultVatValueTest)){
		 return true; 
	}else
	{
		return false;
		}
	</configByCorp:fieldVisibility >  
	  }
function flex2PayVatCheck(defaultVatBillingGroup,currentDefaultVatValue) { 
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	var companyDivision=document.forms['billingForm'].elements['serviceOrder.companyDivision'].value;
	var vatBillingGroupFlexValue="";
	var defaultVatBillingGroupDesc="";
	var relo1="" 
	<c:forEach var="entry" items="${vatBillingGroupList}">
	  if(relo1==""){ 
      if(defaultVatBillingGroup=='${entry.key}') {
    	  defaultVatBillingGroupDesc='${entry.value}';
    	  relo1="yes";
      }} 
	</c:forEach>
	var relo=""
		<c:forEach var="entry" items="${flex2PayCodeList}">
	  if(relo==""){ 
        if(defaultVatBillingGroupDesc+"~"+companyDivision=='${entry.key}' || defaultVatBillingGroupDesc+"~NODATA"=='${entry.key}') {
        	vatBillingGroupFlexValue='${entry.value}';
           relo="yes";
        }} 
	</c:forEach> 
	var currentDefaultVatValueTest =  currentDefaultVatValue+",";
	vatBillingGroupFlexValue = vatBillingGroupFlexValue+",";
	if(currentDefaultVatValue !="" && vatBillingGroupFlexValue.includes(currentDefaultVatValueTest)){
		 return true; 
	}else
	{
		return false;
		}
	</configByCorp:fieldVisibility >  
	  }  
function findStorageVendorVatWithVatGroup(){
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
    var partnerCode = document.forms['billingForm'].elements['billing.vendorCode'].value;
    var defaultAccountLineid = document.forms['billingForm'].elements['billing.id'].value;
    var vendorStorageVatDescr='${billing.insuranceVatDescr}';
    var url="findStorageVatDescr.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&id="+encodeURI(defaultAccountLineid)+"&vendorStorageVatDescr="+encodeURI(vendorStorageVatDescr);
    httprecVatBilling.open("GET", url, true);
	 httprecVatBilling.onreadystatechange = handleHttpResponseVendor1StoragerecVatBilling;
	 httprecVatBilling.send(null);
	 </configByCorp:fieldVisibility >

}

function handleHttpResponseVendor1StoragerecVatBilling() { 
	 if (httprecVatBilling.readyState == 4){
     var results = httprecVatBilling.responseText
       results = results.trim();
     results = results.replace('[','');
     results=results.replace(']',''); 
     res = results.split("@");
     var targetElement = document.forms['billingForm'].elements['billing.insuranceVatDescr'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					console.log(res);
					if(res[i] == ''){
					document.forms['billingForm'].elements['billing.insuranceVatDescr'].options[i].text = '';
					document.forms['billingForm'].elements['billing.insuranceVatDescr'].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['billingForm'].elements['billing.insuranceVatDescr'].options[i].value = stateVal[0];
					document.forms['billingForm'].elements['billing.insuranceVatDescr'].options[i].text = stateVal[1];
					}
					}
				document.forms['billingForm'].elements['billing.insuranceVatDescr'].value = '${billing.insuranceVatDescr}';
				findDefaultVatForVendorStorageVat();
          
   }
}
function findDefaultVatForVendorStorageVat(){
	<c:if test="${systemDefaultVatCalculation=='true'}">
    var bCode = document.forms['billingForm'].elements['billing.vendorCode'].value;
	if(bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http5222.open("GET", url, true); 
	    http5222.onreadystatechange = handleHttpStorageVendorVat92222; 
	    http5222.send(null);	     
     }</c:if>}
function handleHttpStorageVendorVat92222(){
    if (http5222.readyState == 4){    	
    	var results = http5222.responseText
    	results = results.trim();
    	if(results!=''){
    		var	res = results.split("~");
	    	var vatBillingGroupCheck=true;
	        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	        vatBillingGroupCheck=false;
	        </configByCorp:fieldVisibility>
	        if(vatBillingGroupCheck){ 
		    	if(res[8]!='NA'){
			    	if(document.forms['billingForm'].elements['billing.insuranceVatDescr'].value==''){
					 	document.forms['billingForm'].elements['billing.insuranceVatDescr'].value = res[8];
			    	}  }
	        }else{
	       	 var vendorStorageVatDescr = '${billing.insuranceVatDescr}';
	        	if(res[8]!='NA' && res[12]=='NODATA'){
			    	if(document.forms['billingForm'].elements['billing.insuranceVatDescr'].value==''){
					 	document.forms['billingForm'].elements['billing.insuranceVatDescr'].value = res[8];
			    	}			    	
		         }else{
		        	 if(res[8]!='NA' && res[12]!='NODATA'){
		        		 var chkVatBillingGroup=false;
		        		 chkVatBillingGroup = flex2PayVatCheck(res[12],res[8]); 
                      if(chkVatBillingGroup){ 
                     	if(vendorStorageVatDescr != res[8]){
                   		 var agree = confirm("Updating Vendor code will also change the  VAT code. Do you wish to continue?");
         					if(agree){
         						 document.forms['billingForm'].elements['billing.insuranceVatDescr'].value = res[8];	
         					}
                        }
                      } 
                       
		        	 }
		         }
	        	
	        }
		    	}    	} }
	  
</script>
