<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.List"%>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<%@page import="java.util.Iterator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>

 
 <title><fmt:message key="Dashboard.title"/></title>   
    <meta name="heading" content="<fmt:message key='Dashboard.heading'/>"/>
<div id="newmnav" class="clearfix">
		<jsp:include page="/common/dashboardtab.jsp" flush="true"></jsp:include>
	</div>
<%@ include file="/WEB-INF/pages/trans/soDashBoardHeader.jsp"%>
<s:form id="sodashbord" name="" action="" method="post" validate="true"onsubmit="">

	<!--test-->
	<div class="row dashboard-page">
		<div class="col-md-12 col-lg-12 col-xs-6 col-xl-4 cus-col-6">
			<div class="portlet  card-sec widge-sec">
				<div class="portlet-body origin-sec">
					<div class="tittle-heading">
						<h4>Origin</h4>
						<c:if test="${serviceOrder.job == 'RLO'}">
						<i class="fa fa-share" aria-hidden="true" onclick="trackingstatus_Relojob();"></i>
						</c:if>
							<c:if test="${serviceOrder.job != 'RLO'}">
						<i class="fa fa-share" aria-hidden="true" onclick="trackingstatus_tab();"></i>
						</c:if>
							<c:choose>
    <c:when test="${noteOriginCount!='0'}">
    <i class="fa fa-pencil-square-o note-filled" aria-hidden="true"data-toggle="tooltip" title="${noteOriginCount} Notes" onclick="notes_Origin();"></i></a>
    </c:when>    
    <c:otherwise>
 <i class="fa fa-pencil-square-o  " aria-hidden="true"data-toggle="tooltip" title="${noteOriginCount} Notes" onclick="notes_Origin();"></i></a>
    </c:otherwise>
</c:choose>
								<c:if test="${serviceOrder.status=='NEW'}">
							<!-- <i class="fa fa-circle status-icon orange " aria-hidden="true"></i> -->
							</c:if>
								<c:if test="${serviceOrder.status=='OSRV'}">
							<i class="fa fa-circle status-icon light-green " aria-hidden="true"></i>
							</c:if>
							
					</div>
					<div class="info-row">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading"><i class="fa fa-user"
											aria-hidden="true"></i> Origin Agent</label>
									</div>
									<div class="col-md-6">
										<label class="eclipse-text cus-sm-ml" data-toggle="tooltip" data-original-title="${trackingStatus.originAgent}">${trackingStatus.originAgent}</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Reconfirm Shipper">
											<i class="far fa-calendar-alt"></i> Reconfirm
											Shipper</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.reconfirmShipper}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Confirm OA/Local Ops">
											<i class="far fa-calendar-alt"></i> Confirm
											OA/Local Ops</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${miscellaneous.confirmOriginAgent}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading">
										<i class="far fa-calendar-alt"></i> Packing Date</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.packA}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading"><i class="far fa-calendar-alt"></i> Loading Date</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.loadA}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Doc. Sent To Client"><i class="far fa-calendar-alt"></i> Doc. Sent To
											Client</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.documentSentClient}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Doc. received from client"><i class="far fa-calendar-alt"></i> Doc. Received
											from client</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.documentReceivedClient}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Start of Customs Clearance"><i class="far fa-calendar-alt"></i> Start of
											Customs Clearance </label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.customClearance}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="bottom-panal">
						<div class="tittle-heading">
							<h5 class="sub-heading ">SIT @ Origin</h5>
							<c:if test="${serviceOrder.status=='OSIT'}">
							<i class="fa fa-circle status-icon yellow" aria-hidden="true"></i>
							</c:if>
						</div>
						<div class="info-row">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading"><i class="far fa-calendar-alt"></i> Date In</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.sitOriginOn}" timeZone="IST"/></label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading"><i class="far fa-calendar-alt"></i> Date Out</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.sitOriginA}" timeZone="IST"/></label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading no-icon"> SIT # Days</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml">${trackingStatus.sitOriginDays}</label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading no-icon"> SIT Reason</label>
										</div>
										<div class="col-md-6">
											 <c:if test="${trackingStatus.originSITReason=='1'}">
										 	<label class="cus-sm-ml">Arrival</label>
										 </c:if>
										  <c:if test="${trackingStatus.originSITReason=='2'}">
										 
										 	<label class="cus-sm-ml">Credential</label>
										 </c:if>
										  <c:if test="${trackingStatus.originSITReason=='3'}">
										 
										 	<label class="cus-sm-ml">Housing</label>
										 </c:if>
										  <c:if test="${trackingStatus.originSITReason=='4'}">
										 	<label class="cus-sm-ml">Post</label>
										 </c:if>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--forwarding-->
		<div class="col-md-12 col-lg-12 col-xs-6 col-xl-4 cus-col-6">
			<div class="portlet card-sec ">
				<div class="portlet-body origin-sec forwarding-sec">
					<div class="tittle-heading">
						<h4>Forwarding</h4>
							<c:if test="${serviceOrder.job == 'RLO'}">
						<i class="fa fa-share" aria-hidden="true" onclick="forWording_tab_tab();"></i>
						</c:if>
							<c:if test="${serviceOrder.job != 'RLO'}">
						<i class="fa fa-share" aria-hidden="true" onclick="forWording_tab_tab();"></i>
						</c:if>
						
					</div>
					<div class="info-row">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading"><i class="fa fa-user"
											aria-hidden="true"></i> Forwarder</label>
									</div>
									<div class="col-md-6">
										<label class="eclipse-text cus-sm-ml" data-toggle="tooltip" data-original-title="${trackingStatus.forwarder}">${trackingStatus.forwarder}</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title=" Inst. To Forwarder"><i class="far fa-calendar-alt"></i> Inst. To
											Forwarder</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.instruction2FF}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading no-icon"> BL #/AWB #</label>
									</div>
									<div class="col-md-6">
										<label class="eclipse-text cus-sm-ml" data-toggle="tooltip" data-original-title="${trackingStatus.billLading}">${trackingStatus.billLading}</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading"><i class="far fa-calendar-alt"></i> Pre-Advise</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.preliminaryNotification}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Final Notification"><i class="far fa-bell-slash"></i> Final
											Notification</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.finalNotification}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading no-icon"> Piece Count</label>
									</div>
									<div class="col-md-6">
									<c:forEach var="transptDetails" items="${transptDetails}">
										<label class="cus-sm-ml"><c:out value="${transptDetails.pieces}" /></label>
										</c:forEach>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="bottom-panal">
						<div class="tittle-heading">
							<h5 class="sub-heading ">Transport</h5>
							<c:if test="${serviceOrder.status=='TRNS'}">
							<i class="fa fa-circle status-icon orange " aria-hidden="true"></i>
							</c:if>
						</div>
						<div class="info-row">
						   <c:forEach var="transptDetails" items="${transptDetails}">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-sm-3">
											<label class="label-heading no-icon" > Carrier</label>
										</div>
										<div class="col-md-9">
											<label class="eclipse-text cus-sm-ml" data-toggle="tooltip"><c:out value="${transptDetails.carrierName}" /></label>  
											
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading no-icon">POL</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml"><c:out value="${transptDetails.carrierDeparture}" /></label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading no-icon"> POE</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml"><c:out value="${transptDetails.carrierArrival}" /></label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading"><i class="far fa-calendar-alt"></i>&nbspATD</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml">	<c:out value="${transptDetails.atDepart}" /></label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading"><i class="far fa-calendar-alt"></i>&nbspATA</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml"><c:out value="${transptDetails.atArrival}" /></label>
										</div>
									</div>
								</div>
							</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--end forward-->
		<!--Destination-->
		<div class="col-md-12 col-xs-6 col-lg-12 col-xl-4 cus-col-6">
			<div class="portlet  card-sec ">
				<div class="portlet-body origin-sec">
					<div class="tittle-heading">
						<h4>Destination</h4>
						<c:if test="${serviceOrder.job == 'RLO'}">
						<i class="fa fa-share" aria-hidden="true" onclick="trackingstatus_Relojob();"></i>
						</c:if>
							<c:if test="${serviceOrder.job != 'RLO'}">
						<i class="fa fa-share" aria-hidden="true" onclick="trackingstatus_tab();"></i>
						</c:if>
						<c:choose>
					  <c:when test="${noteDestCount!='0'}">
    <i class="fa fa-pencil-square-o note-filled " aria-hidden="true"data-toggle="tooltip" title="${noteDestCount} Notes" onclick="notes_Destination();"></i></a>
    </c:when>    
    <c:otherwise>
     <i class="fa fa-pencil-square-o  " aria-hidden="true"data-toggle="tooltip" title="${noteDestCount} Notes" onclick="notes_Destination();"></i></a>
    </c:otherwise>
</c:choose>
								 <c:if test="${serviceOrder.status=='DLVR'}">
							<i class="fa fa-circle status-icon light-blue " aria-hidden="true"></i>
							</c:if>
								<c:if test="${serviceOrder.status=='DSRV'}">
							<i class="fa fa-circle status-icon sky-blue " aria-hidden="true"></i>
							</c:if>
					</div>
					<div class="info-row">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Destination Agent"><i
											class="fa fa-user" aria-hidden="true"></i> Destination Agent</label>
									</div>
									<div class="col-md-6">
										<label class="eclipse-text cus-sm-ml" data-toggle="tooltip" title="${trackingStatus.destinationAgent}">${trackingStatus.destinationAgent}</label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Origin OBL/AWB Rcvd"><i class="far fa-calendar-alt"></i> Origin OBL/AWB
											Rcvd</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.originBL}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Entry Form Sent To Client"><i class="far fa-calendar-alt"></i> Entry Form
											Sent To Client</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.mail3299}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Entry Form Rcvd From Client"><i class="far fa-calendar-alt"></i> Entry Form
											Rcvd From Client</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.recived3299}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip" title="Doc. received from Origin Agent"><i class="far fa-calendar-alt"></i> Doc. received
											from Origin Agent </label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.documentReceivedFromOA}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text"
											data-toggle="tooltip"
											title="Delivery Receipt sent to Origin Agent"><i class="far fa-calendar-alt"></i> Delivery
											Receipt sent to Origin Agent</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.deliveryReceiptSentOA}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-sm-6">
										<label class="label-heading eclipse-text" data-toggle="tooltip"
											title="Delivery"><i class="far fa-calendar-alt"></i> Delivery</label>
									</div>
									<div class="col-md-6">
										<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.deliveryA}" timeZone="IST"/></label>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="bottom-panal">
						<div class="tittle-heading">
							<h5 class="sub-heading ">SIT @ Destination</h5>
							 <c:if test="${serviceOrder.status=='DSIT'}">
							<i class="fa fa-circle status-icon yellow " aria-hidden="true"></i>
							</c:if>
						</div>
						<div class="row21 info-row">
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading"><i class="far fa-calendar-alt"></i> Date In</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.sitDestinationIn}" timeZone="IST"/></label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading"><i class="far fa-calendar-alt"></i> Date Out</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml"><fmt:formatDate pattern="dd-MMM-yyyy" value="${trackingStatus.sitDestinationA}" timeZone="IST"/></label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading no-icon"> SIT # Days</label>
										</div>
										<div class="col-md-6">
											<label class="cus-sm-ml">${trackingStatus.sitDestinationDays}</label>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<div class="col-sm-6">
											<label class="label-heading no-icon"> SIT Reason</label>
										</div>
										<div class="col-md-6">
										 <c:if test="${trackingStatus.destinationSITReason=='1'}">
										 
										 	<label class="cus-sm-ml">Arrival</label>
										 </c:if>
										  <c:if test="${trackingStatus.destinationSITReason=='2'}">
										 
										 	<label class="cus-sm-ml">Credential</label>
										 </c:if>
										  <c:if test="${trackingStatus.destinationSITReason=='3'}">
										 
										 	<label class="cus-sm-ml">Housing</label>
										 </c:if>
										  <c:if test="${trackingStatus.destinationSITReason=='4'}">
										 
										 	<label class="cus-sm-ml">Post</label>
										 </c:if>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--end destination-->
		<!-- </div> -->
		<!--second row-->
		<!-- <div class="row "> -->
		<!--row 2 col1-->
		<div class="col-md-12 col-xs-6 col-lg-12 col-xl-4 cus-col-6">
			<!-- BEGIN Portlet PORTLET-->
			<div class="portlet card-sec ">
				<div class="portlet-body">
					<div class="tittle-heading">
						<h4>Task</h4>
						<i class="fa fa-share" aria-hidden="true" onClick="task_tab();"></i>
					</div>
					<div class="row info-row task">
						<div class="col-md-12 scroll-dv">
							<table>
                                       <tr>
                                          <th class="custom-wd"> Message</th>
                                          <th>Name</th>
                                          <th>Due Days</th>
                                          <th>Note</th>
                                       </tr>
                                       <tbody>
                                     
                                       	     <c:if test="${ empty taskList}">           
                                    <tr><td colspan="4"><h5 class="text-center no-result-info"><i class="fa fa-info-circle" aria-hidden="true"></i> No Task</h5></td></tr> 
                                       	 </c:if>
                                        <c:if test="${not empty taskList}">
                                       <c:forEach var="taskList" items="${taskList}">
                                      <tr>
                                          <td><label data-toggle="tooltip" data-original-title="${taskList.messagedisplayed}" class="ellpsis-150"><c:out value="${taskList.messagedisplayed}" /></label></td>
                                          <td> <label data-toggle="tooltip" data-original-title="${taskList.owner}" class="ellpsis-150"><c:out value="${taskList.owner}" /></label></td>
                                          <td class="pl-0"><i class="fa fa-clock-o" aria-hidden="true"></i> <c:out value="${taskList.durationAddSub}" /></td>
                                          	
                                               	<c:if test="${empty taskList.note}">
                                        <td><i class="fa fa-sticky-note  example-popover" aria-hidden="true" data-placement="top" data-trigger="hover" title="" data-toggle="popover" data-content="" data-original-title=""></i></td>
                                              </c:if> 
                                               	<c:if test="${not empty taskList.note}">
                                       <td> <i class="fa fa-sticky-note note-filled example-popover" aria-hidden="true" data-placement="top" data-trigger="hover" title="" data-toggle="popover" data-content="<c:out value="${taskList.note}" />" data-original-title="<c:out value="${taskList.subject}" />"></i></td>
                                              </c:if>
                                       </tr> 
                                       </c:forEach>
                                     </c:if>
                      
                                    </tbody></table>
						</div>
					</div>
				</div>
			</div>
			<!--end-->
		</div>
		<!--end asa-->
		<!--Work Ticket-->
		<div class="col-md-12  col-xs-6 col-lg-12 col-xl-4 cus-col-6">
			<!-- BEGIN Portlet PORTLET-->
			<div class="portlet card-sec">
				<div class="portlet-body">
					<div class="tittle-heading">
						<h4>Work Ticket</h4>
						<i class="fa fa-share" aria-hidden="true" onclick="workTicket_tab();"></i>
					</div>
					<div class="Workticket info-row">
						<table>
							<tbody>
								<tr>
									<th>Ticket</th>
									<th>Status</th>
									<th style="width: 180px;">Service</th>
									<th></th>
								</tr>
								 <c:if test="${empty workTicketsList}">
							<tr><td colspan="4"><h5 class="text-center no-result-info"><i class="fa fa-info-circle" aria-hidden="true"></i> No  Ticket</h5></td></tr> 
								 </c:if>
								 <c:if test="${ not empty workTicketsList}">
								<c:forEach var="workTicketsList" items="${workTicketsList}">
									<tr>
										<td><c:out value="${workTicketsList.ticket}" /></td>
                                           <c:if test="${workTicketsList.targetActual=='T'}">
                                      <td> <span class="status yellow">Target</span> </td>
                                            </c:if>
                                              <c:if test="${workTicketsList.targetActual=='C'}">
                                      <td><span class="status red-mint">Cancelled</span></td>
                                            </c:if>
                                              <c:if test="${workTicketsList.targetActual=='A'}">
                                            <td><span class="status green-meadow">Actual</span></td>
                                            </c:if>
                                              <c:if test="${workTicketsList.targetActual=='D'}">
                                           <td><span class="status blue">Dispatching</span> </td>
                                            </c:if>
                                              <c:if test="${workTicketsList.targetActual=='P'}">
                                           <td><span class="status lavender">Pending</span> </td>
                                            </c:if>
                                              <c:if test="${workTicketsList.targetActual=='F'}">
                                           <td><span class="status Violet">Forecasting</span>  </td>
                                            </c:if>
                                              <c:if test="${workTicketsList.targetActual=='R'}">
                                           <td><span class="status light-orange">Rejected</span> </td>
                                            </c:if>
                                       
			                            
										<td>
										<label class="eclipse-text" data-toggle="tooltip" data-original-title="${workTicketsList.service}"><c:out value="${workTicketsList.service}" /></label>
										</td>
										<td style="text-align: right;">
										
										
										 <span class="outer-span">
												<i class="fa fa-truck mb-truck" aria-hidden="true" data-target="#full-width" data-toggle="modal"></i> 
												<span class="badge "><c:out value="${workTicketsList.trucks}" /></span>
												
										</span> 
										
										
										
										<span class="outer-span"> 
												<i class="fa fa-user" 	aria-hidden="true" data-target="#full-width" data-toggle="modal"></i> 
												<span class="badge "><c:out value="${workTicketsList.crews}" /></span>
										</span>
										 
											<span class="outer-span">
											<i class="fas fa-box-open" aria-hidden="true"></i> 
											<span class="badge "><c:out value="${workTicketsList.material}" />
											</span>
										</span>
											
											
											</td>
									</tr>
								</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!--end-->
		</div>
		<!--end forward-->
		<!--Finance 2-->
		<!--forwarding-->
		<div class="col-md-12 col-xs-6 col-lg-12 col-xl-4 cus-col-6">
			<!-- BEGIN Portlet PORTLET-->
			<div class="portlet  card-sec">
				<div class="portlet-body finance">
					<div class="tittle-heading">
						<h4>Finance</h4>
						<h4 class="  d-none d-sm-inline"
							style="color: #047790; margin-left: 176px;">Recent Invoices</h4>
						<i class="fa fa-share" aria-hidden="true" onclick="AccountLine_tab();"></i>
					</div>
					<div class="row info-row finance">


						<div class="col-md-5 ">
							<c:forEach var="totalrevenueList" items="${totalrevenueList}">
							<div class="row">
								<div class="col-sm-6 col-6">

									<label class="label-heading  ">Revenue</label>
								</div>
								<div class="col-sm-6 col-6 text-right">
                                 <c:if test="${baseCurrency=='USD'}">
									<label class="label-heading"> <i class="fa fa-usd"></i><c:out value="${totalrevenueList.actualRevenue}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='INR'}">
									<label class="label-heading"> <i class="fa fa-rupee"></i><c:out value="${totalrevenueList.actualRevenue}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='EUR'}">
									<label class="label-heading"> <i class="fa fa-eur"></i><c:out value="${totalrevenueList.actualRevenue}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='GBP'}">
									<label class="label-heading"> <i class="fa fa-gbp"></i><c:out value="${totalrevenueList.actualRevenue}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='KRW'}">
									<label class="label-heading"> <i class="fa fa-krw"></i><c:out value="${totalrevenueList.actualRevenue}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='SGD'}">
									<label class="label-heading">&#36; <c:out value="${totalrevenueList.actualRevenue}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='AED'}">
									<label class="label-heading"> &#x62f;&#x2e;&#x625; <c:out value="${totalrevenueList.actualRevenue}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='MYR'}">
									<label class="label-heading">&#82;&#77; <c:out value="${totalrevenueList.actualRevenue}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='SEK'}">
									<label class="label-heading">&#107;&#114; <c:out value="${totalrevenueList.actualRevenue}" /></label>
									</c:if>
								</div>

							</div>
							<div class="row">
								<div class="col-sm-6 col-6">

									<label class="label-heading"> Expense </label>
								</div>
								<div class="col-sm-6 col-6 text-right">
                                   <c:if test="${baseCurrency=='USD'}">
									<label class="label-heading"> <i class="fa fa-usd"></i><c:out value="${totalrevenueList.actualExpense}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='INR'}">
									<label class="label-heading"> <i class="fa fa-rupee"></i><c:out value="${totalrevenueList.actualExpense}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='EUR'}">
									<label class="label-heading"> <i class="fa fa-eur"></i><c:out value="${totalrevenueList.actualExpense}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='GBP'}">
									<label class="label-heading"> <i class="fa fa-gbp"></i><c:out value="${totalrevenueList.actualExpense}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='KRW'}">
									<label class="label-heading"> <i class="fa fa-krw"></i><c:out value="${totalrevenueList.actualExpense}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='SGD'}">
									<label class="label-heading"> &#107;&#114;<c:out value="${totalrevenueList.actualExpense}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='AED'}">
									<label class="label-heading">&#x62f;&#x2e;&#x625; <c:out value="${totalrevenueList.actualExpense}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='MYR'}">
									<label class="label-heading"> <i class="fa fa-usd"></i><c:out value="${totalrevenueList.actualExpense}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='SEK'}">
									<label class="label-heading"> <i class="fa fa-usd"></i><c:out value="${totalrevenueList.actualExpense}" /></label>
									</c:if>
									
								</div>

							</div>
							<div class="row">
								<div class="col-sm-6 col-6">
									<p class="line text-right"></p>
									<label class="label-heading"> Gross Margin</label>
								</div>
								<div class="col-sm-6 col-6 text-right">
									<p class="line text-right"></p>
									<c:set var="GrossMargin" scope="session" value="${totalrevenueList.actualRevenue-totalrevenueList.actualExpense}"/>  
									    <c:if test="${baseCurrency=='USD'}">
									<label class="label-heading"> <i class="fa fa-usd"></i><c:out value="${GrossMargin}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='INR'}">
									<label class="label-heading"> <i class="fa fa-rupee"></i><c:out value="${GrossMargin}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='EUR'}">
									<label class="label-heading"> <i class="fa fa-eur"></i><c:out value="${GrossMargin}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='GBP'}">
									<label class="label-heading"> <i class="fa fa-gbp"></i><c:out value="${GrossMargin}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='KRW'}">
									<label class="label-heading"> <i class="fa fa-krw"></i><c:out value="${GrossMargin}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='SGD'}">
									<label class="label-heading"> <i class="fa fa-usd"></i><c:out value="${GrossMargin}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='AED'}">
									<label class="label-heading"> &#x62f;&#x2e;&#x625;<c:out value="${GrossMargin}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='MYR'}">
									<label class="label-heading"> <i class="fa fa-usd"></i><c:out value="${GrossMargin}" /></label>
									</c:if>
									 <c:if test="${baseCurrency=='SEK'}">
									<label class="label-heading"> <i class="fa fa-usd"></i><c:out value="${GrossMargin}" /></label>
									</c:if>
								</div>

							</div>
							<div class="row">
								<div class="col-sm-6 col-6">

									<label class="label-heading"> G. Margin %</label>
								</div>
							<div class="col-sm-6 col-6 text-right">
										<c:if test="${ empty accountLineList}">
											<label class="label-heading "><c:out value="" /></label>
										</c:if>
										<c:if test="${not empty accountLineList}">
											<c:set var="GMargin1" scope="session"
												value="${((totalrevenueList.actualRevenue-totalrevenueList.actualExpense)*100)/totalrevenueList.actualRevenue}" />
											<label class="label-heading "><c:out value="${GMargin1}" /></label>
										</c:if>
									</div>

							</div>
								</c:forEach>
						</div>
						<h4 class=" d-md-none d-xl-none d-lg-none "
							style="color: #047790; font-size: 16px; font-weight: 400; margin-left: 15px;">
							Recent Invoices</h4>
						<div class="col-md-7 ">	
							 <c:if test="${empty accountLineList}">
							<tr><td colspan="4"><h5 class="text-center no-result-info"><i class="fa fa-info-circle" aria-hidden="true"></i> No  Invoices</h5></td></tr> 
								 </c:if>	
							<c:if test="${not empty accountLineList}">
	               <c:forEach var="accountLineList" items="${accountLineList}"> 
	                       <div class="row">
								<div class="col-sm-4 col-4">
									<span class="invoice-span "><c:out value="${accountLineList.recinvoiceNumber}" /><i class="fas fa-file-invoice"></i></span>
								</div>
								<div class="col-sm-4 col-4">
									<label class="invoice-date"> <fmt:formatDate pattern="dd-MMM-yyyy" value="${accountLineList.receivedInvoiceDate}" timeZone="IST"/> </label>
								</div>
								<div class="col-sm-4 col-4 text-right">
									<c:if test="${baseCurrency=='USD'}">
									<span class="invoice-span-price"> <i class="fa fa-usd"></i><c:out value="${accountLineList.actualRevenue}" /> </span>
									<%-- <label class="invoice-span-price"> <i class="fa fa-usd"></i><c:out value="${accountLineList.actualRevenue}" /></label> --%>
									</c:if>
									 <c:if test="${baseCurrency=='INR'}">
									 <span class="invoice-span-price"> <i class="fa fa-rupee"></i><c:out value="${accountLineList.actualRevenue}" /> </span>
																		</c:if>
									 <c:if test="${baseCurrency=='EUR'}">
									  <span class="invoice-span-price"><i class="fa fa-eur"></i><c:out value="${accountLineList.actualRevenue}" /> </span>
									
									</c:if>
									 <c:if test="${baseCurrency=='GBP'}">
									 <span class="invoice-span-price"> <i class="fa fa-gbp"></i><c:out value="${accountLineList.actualRevenue}" /> </span>
									
									</c:if>
									 <c:if test="${baseCurrency=='KRW'}">
									 <span class="invoice-span-price"><i class="fa fa-krw"></i><c:out value="${accountLineList.actualRevenue}" /> </span>
									
									</c:if>
									 <c:if test="${baseCurrency=='SGD'}">
									 <span class="invoice-span-price">&#107;&#114;<c:out value="${accountLineList.actualRevenue}" /> </span>
									
									</c:if>
									 <c:if test="${baseCurrency=='AED'}">
									 <span class="invoice-span-price"> <i class="fa fa-usd"></i><c:out value="${accountLineList.actualRevenue}" /> </span>
									
									</c:if>
									 <c:if test="${baseCurrency=='MYR'}">
									 <span class="invoice-span-price"><i class="fa fa-usd"></i><c:out value="${accountLineList.actualRevenue}" /> </span>
									
									</c:if>
									 <c:if test="${baseCurrency=='SEK'}">
									 <span class="invoice-span-price"><i class="fa fa-usd"></i><c:out value="${accountLineList.actualRevenue}" /> </span>
									
									</c:if>
									
								</div>
							</div>
					</c:forEach> 
							</c:if>
						
						</div>
					</div>
				</div>
			</div>
			<!--end-->

			<!--Doccument-->
			<div class="col-md-12 pl-0 pr-0 ">
				<!-- BEGIN Portlet PORTLET-->
				<div class="portlet  card-sec ">
					<div class="portlet-body document">
						<div class="tittle-heading">
							<h4>Document</h4>
							<i class="fa fa-share" aria-hidden="true" onclick="document_tab();"></i>
							<div class="dropdown">
						
								 <button class="btn filter-btn dropdown-toggle" type="button"data-toggle="dropdown">
									<i class="fa fa-filter" aria-hidden="true"></i>
											<div class="abc">
											</div>
								</button> 
							
								<ul class="dropdown-menu" id="dropdown-menu" >
									
									<li><a href="JavaScript:void(0);" class="dcategory" ><c:out value=""/>All</a></li>
								    <c:forEach var="documentCategoryList" items="${documentCategoryList}">
									<li class="eclipse-text-100" id="abc"><a href="JavaScript:void(0);" class="dcategory eclipse-text-100" ><c:out value="${documentCategoryList.documentCategory}" /> </a></li>
									</c:forEach>
								</ul>  
							</div>
						</div>
						<div class=" info-row ">
				
							<!-- <div class="col-md-3">
						<label class="label-heading  text-bold">Doc. Type</label>
						</div>
						<div class="col-md-3">
						 <label class="label-heading  text-bold">Description</label>
						</div>
						<div class="col-md-3">
						 <label class="label-heading  text-bold">Uploaded By</label>
						</div>
						
						<div class="col-md-3">
						 <label class="label-heading  text-bold">Uploaded On</label>
						</div> -->
						<table id="docCategory" class='table-borderless' style="margin-bottom:0px;">
						<thead>
						  <tr>
						   <th class="custom-wd">Doc.Type</th>
						    <th class="custom-wd">Description</th>
						     <th class="custom-wd-xs">Uploaded By</th>
						      <th class="custom-wd-xs">Uploaded On</th>
						  </tr>
						  </thead>
							<tbody >
							    <c:if test="${empty documentList}">
							          <tr><td colspan="4"><h5 class="text-center no-result-info"><i class="fa fa-info-circle" aria-hidden="true"></i> No Document</h5></td></tr> 
							    </c:if>
						    <c:if test="${not empty documentList}">
								<c:forEach var="myFileList" items="${documentList}">
									  <tr>
										<td><label class="ellpsis-130" data-toggle="tooltip" data-original-title="${myFileList.fileType}"><c:out value="${myFileList.fileType}" /></label></td>
										<td><label class="ellpsis-170" data-toggle="tooltip" data-original-title="${myFileList.description}"><c:out value="${myFileList.description}" /></label></td>
										<td class="custom-wd-xs"><label ><c:out value="${myFileList.updatedBy}" /></label></td>
										<td class="custom-wd-xs"><label><c:out value="${myFileList.updatedOn}" /><%-- <a href="ImageServletAction.html?id=${myFileList.id}&myFileForVal=''&noteForVal=''&activeVal=''&fieldVal3=''&fieldVal4=''&fileIdVal=${myFileList.id}&myFileJspName=''" title=""  id="abc"><i class="fa fa-download" aria-hidden="true"></i></a> --%></label></td>
										<td><a href="ImageServletAction.html?id=${myFileList.id}&sid=${serviceOrder.id}&myFileForVal=SD&noteForVal=''&activeVal=''&fieldVal3=''&fieldVal4=''&fileIdVal=${myFileList.id}&myFileJspName=''" title=""  id="abc"><i class="fa fa-download" aria-hidden="true"></i></a></td>
									</tr>  
								</c:forEach>

                              </c:if>
							</tbody>
						</table>
						</div>
					</div>
				</div>
				<!--end-->
			</div>
			<!--End document-->

		</div>
		<!--end forward-->

		<!--end destination-->
	</div>
    <script>

$('.dcategory').on('click',function(){
var str = $(this).text();
var str1 = jQuery("#shipNumber").val();

/*  */
var response = '';
if(str!="All")
	{
	var URL = "dashboardFilter.html?sid=${serviceOrder.id}&fileid="+str1+"&documentCategory="+str;
}
 if(str=="All")
{
var URL = "dashboardFilter.html?sid=${serviceOrder.id}&fileid="+str1;
} 

$.ajax({
type: "POST", 
url:URL,
async: false,
success: function(response) {
var jsonData = JSON.parse($(response).find("#rdData").html());
var tRow = "";

var href='';

{
	  $('#docCategory > tbody > tr').remove(); 
	//if we are getting single recoard this is execute
/* 	tRow = "<tr>"
	   	 + "<td>"+ jsonData.FileType +"</td>"
	   	 + "<td>"+ jsonData.Description +"</td>"
	   	 + "<td>"+ jsonData.FileType +"</td>"
	   	  +"</tr>";
  	 $('#docCategory > tbody').append(tRow); */
 	//if we are getting multiple recoard this is execute
	$.each(jsonData, function(index, element) {
href='ImageServletAction.html?id='+element.id+'&myFileForVal=""&noteForVal=""&activeVal=""&fieldVal3=""&fieldVal4=""&fileIdVal='+element.id+'&myFileJspName=""';
	    tRow = "<tr>"
	    	 + "<td>"+ element.fileType +"</td>"
	   	   	 + "<td>"+ element.description +"</td>"
	   	   	 + "<td>"+ element.updatedBy +"</td>"
	   	   	 + "<td>"+ element.updatedOn 
	   	   	 +"<a href="+href+" title='' id='abc'><i class='fa fa-download' aria-hidden='true'></i></a>"
	   	   	 +"</td>"
	   	 
	   $('#docCategory > tbody').append(tRow);
	}); 
	
}

}
});

});

</script>
 <!-- Tooltip -->
<script>
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
<!-- End Tooltip -->



<script>
$(function () {
	  $('.example-popover').popover({
	    container: 'body'
	  })
	})
</script>
  </script> 
      
<!--  <script type="text/javascript"> 
    $("li a").click(function(){
    	  var selText = $(this).text();
     $('.filter-btn').html(selText); 
     $('.filter-btn').show();
    	});

   
    </script> --> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
   <s:hidden value="${serviceOrder.shipNumber}" id="shipNumber"/>
    <s:hidden value="${baseCurrency}" id="baseCurrency"/> 
     <s:hidden value="${serviceOrder.status}" id="status"/>
 
</s:form>



