<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Agent Directory</title>   
    <meta name="heading" content="Agent Directory"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">

function clear_fields(){
	document.getElementsByName('userName')[0].value='';
	document.getElementsByName('userEmailId')[0].value='';
}


function quotesNotAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==222){
  return false;
  }
  else{
  return true;
  }
}
function quotesRemoval(){
	if(document.getElementsByName('userName')[0].value.indexOf("'")>-1){
		var val =document.getElementsByName('userName')[0].value;
		document.getElementsByName('userName')[0].value = val.substring(0,val.indexOf("'")) +  val.substring(val.indexOf("'")+1,val.length);
	}
	if(document.getElementsByName('userEmailId')[0].value.indexOf("'")>-1){
		val =document.getElementsByName('userEmailId')[0].value;
		document.getElementsByName('userEmailId')[0].value = val.substring(0,val.indexOf("'")) +  val.substring(val.indexOf("'")+1,val.length);
	}
  }
</script>

    <script type="text/javascript">
		// window.opener.document.form.field.value
		function getValue(name, mail, phone,type,isEnable){
			//alert("hiiiii");
			var agentType = '<%= request.getAttribute("agentType")%>';
			if(agentType == 'OA'){
				//alert("hiiiii");
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value = name;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value = mail;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.originAgentPhoneNumber'].value = phone;
				//alert(isEnable);
				if(type=='AGENT' && isEnable=='true'){
					//alert("true");
					window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value="true"
				}else{
					//alert("false");
					window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value=""
				}
				window.opener.showOriginAgentEmailImage();
			}else if(agentType == 'DA'){
				//alert("hiiiiitruuuueeee");
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContact'].value = name;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value = mail;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentPhoneNumber'].value = phone;
				
				if(type=='AGENT' && isEnable=='true'){
					//alert("hiiiii");
					//window.opener.document.getElementById['hidDestinActiveAP'].style.display="block"
					window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value="true"
				}else{
					window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value=""
				}
				window.opener.showDestinationAgentEmailImage();
			}else if(agentType == 'SOA'){
				//alert("hiiiii");
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContact'].value = name;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value = mail;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentPhoneNumber'].value = phone;
				
				if(type=='AGENT' && isEnable=='true'){
					//window.opener.document.getElementById['hidSubOrigActiveAP'].style.display="block"
					window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value="true"
				}else{
					window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value=""
				}
				window.opener.showSubOriginAgentEmailImage();
			}else if(agentType == 'SDA'){
				//alert("hiiiii");
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentAgentContact'].value = name;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = mail;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentPhoneNumber'].value = phone;
							if(type=='AGENT' && isEnable=='true'){
					//alert("trueeee");
					//window.opener.document.getElementById['hidSubDestinActiveAP'].style.display="block"
					window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value="true"
				}else{
					window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value=""
				}
			window.opener.showSubDestinationAgentEmailImage();
			}else if(agentType == 'BC'){
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.brokerContact'].value = name;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.brokerEmail'].value = mail;
				window.opener.showBrokerEmailImage();
			}else if(agentType == 'FC'){
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.forwarderContact'].value = name;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.forwarderEmail'].value = mail;
				window.opener.showForwarderEmailImage();
			}else if(agentType == 'BA'){
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentContact'].value = name;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentEmail'].value = mail;
				window.opener.document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentPhoneNumber'].value = phone;
				window.opener.showEmailImage();
			}else if(agentType == 'BA1'){
				 window.opener.document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentContact'].value = name;
				 window.opener.document.forms['dspDetailsForm'].elements['trackingStatusBookingAgentEmail'].value = mail;
				window.opener.showEmailImage();
			}
			window.close();
		}

		function sendEmail(target){
		   		var originEmail = target;
		   		var subject = '';
//		   		subject = subject+" ${serviceOrder.shipNumber}";
//		   		subject = subject+" ${serviceOrder.firstName}";
//		   		subject = subject+" ${serviceOrder.lastName}";
		 
				var mailto_link = "mailto:"+encodeURI(originEmail)+"?subject="+encodeURI(subject);		
				win = window.open(mailto_link,'emailWindow'); 
				if (win && win.open &&!win.closed) win.close(); 
		 }
	</script>
<s:form name="searchStatusBookingAgent" id="searchStatusBookingAgent" action="searchStatusBookingAgent" method="post">
<s:hidden name="decorator" value="popup"/>
<s:hidden name="popup" value="true"/>
<s:hidden name="partnerCode" value="${partnerCode}"/>
<s:hidden name="agentType" value="${agentType}"/>
<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
	<div id="liquid-round-top">
	   <div class="top" style="margin-top:10px;!margin-top:0px;"><span></span></div>
	   <div class="center-content">		
		<table class="table">
		<thead>
		<tr>
		<th>Name</th>
		<th>Email ID</th>
		<th>&nbsp;</th>
		</tr>
		</thead>	
		<tbody>
			<tr>	
				<td width="" align="left"><s:textfield name="userName" size="30" required="true" cssClass="input-text" /></td>
				<td width="" align="left"><s:textfield name="userEmailId" size="30" required="true" cssClass="input-text" /></td>		
				<td>
	       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom:10px;" key="button.search" onclick="quotesRemoval();"/>  
	       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/>       
	   			</td>
				 </tr>			 
			</tbody>
		</table>
		</div>
		<div class="bottom-header"><span></span></div>
	</div>
</div> 
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-left:385px;
margin-bottom:22px;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:60%;
!width:98%;
}
</style>
</head>

</div><div class="spn" style="width:100%">&nbsp;</div><br><br><br>

<s:set name="userContactList" value="userContactList" scope="request"/>  
<display:table name="userContactList" class="table" requestURI="" id="userContactList" defaultsort="2" pagesize="10" style="width:100%;margin-top:-10px;" >   
	    <display:column sortable="true" title="Full Name" style="width:65px"> 
	    	<a href="javascript: void(0)" onclick= "getValue('${fn:replace(userContactList.fullName,"'","\\'")}','${userContactList.email}','${userContactList.phoneNumber}','${userContactList.userType}','${userContactList.enabled}');">
	    	 <c:out value="${userContactList.fullName}" /></a>
	    </display:column>
        <display:column property="jobFunction" sortable="true" title="Job Function" style="width:65px"/>  
        <display:column title="RedSky User" sortable="true" style="width:45px">      
 		<c:if test="${userContactList.enabled==false}">				
			<img src="${pageContext.request.contextPath}/images/cancel001.gif" />				 	
		</c:if>
		<c:if test="${userContactList.enabled==true}">				
			 <img src="${pageContext.request.contextPath}/images/tick01.gif" />		 	
		</c:if>
		</display:column>	
        <display:column property="userTitle" sortable="true" title="Job Title" style="width:65px"/>
	    <display:column sortable="true" title="Email Address" style="width:65px"> 
	    	<a href="javascript: void(0)" onclick= "sendEmail('${userContactList.email}');">
	    	 <c:out value="${userContactList.email}" /></a>
	    </display:column>
	    <display:column sortable="true" title="Phone&nbsp;Number" style="width:65px"> 
	    	  	 <c:out value="${userContactList.phoneNumber}" /></a>
	    </display:column>
</display:table> 
 
</div>
</s:form>

