/**
 * This class interacts with Spring's HibernateTemplate to save/delete and
 * retrieve the basic "Billing" objects in Redsky that allows for Billing  of Shipment .
 * @Class Name	BillingDaoHibernate
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 * Extended by GenericDaoHibernate to  implement BillingDao
 */

package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;   
import java.util.Set;
import java.util.Map; 
import javax.servlet.http.HttpServletRequest; 
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import org.appfuse.model.User;
import org.hibernate.Hibernate;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException; 
import com.trilasoft.app.dao.BillingDao;
import com.trilasoft.app.dao.hibernate.dto.BillingDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.service.ErrorLogManager;
  
public class BillingDaoHibernate extends GenericDaoHibernate<Billing, Long> implements BillingDao {   
	private HibernateUtil hibernateUtil;
	private ErrorLogManager errorLogManager;
	private String sessionCorpID;
    
	public BillingDaoHibernate() {   
        super(Billing.class);   
    }    

	protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    } 
	
	public void getSessionCorpID()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();	
	}
	 
    public List checkById(Long id) {
    	try {
			return getHibernateTemplate().find("select id from Billing where id=?",id);
		} catch (Exception e) {
			 e.printStackTrace();
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
    
	public int updateServiceOrder(String billToC, String billToN, String shipNumber , String vendorCode){
		try {
			return getHibernateTemplate().bulkUpdate("update ServiceOrder set billToCode='" +billToC.replaceAll("'", "''")+ "' , billToName='" +billToN.replaceAll("'", "''")+ "' , vendorCode ='"+vendorCode+"' where shipNumber=?", shipNumber);
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		} 
		return 0;
	}
	
	public List findInsurarFromAccLine(String insurar, String vendorCode, String shipNumber){
    	try {
			return getHibernateTemplate().find("from AccountLine where vendorCode='"+vendorCode.replaceAll("'", "''")+"' AND category='"+insurar.replaceAll("'", "''")+"' AND shipNumber='"+shipNumber.replaceAll("'", "''")+"' and status is true ");
		} catch (Exception e) {
			 e.printStackTrace();
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
    }
	
	public List findDefaultBookingAgentDetail(String sessionCorpID){
		try {
			return getHibernateTemplate().find("from SystemDefault where corpID = ?", sessionCorpID);
		} catch (DataAccessException e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return null;
    }
	public List findQuantity(String shipNumber)
	{
		try {
			return getSession().createSQLQuery("select sum(if(stgHow = '-' , -stgAmount , stgAmount)) from workticket where shipNumber='"+shipNumber+"'")
			.addScalar("sum(if(stgHow = '-' , -stgAmount , stgAmount))", Hibernate.DOUBLE).list();
		} catch (Exception e) {
			 e.printStackTrace();
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return null;
	}
	public List getChargeRate(String charge,String billingContract , String sessionCorpID){
try {
			String newQuery ="select concat( if(quantity2preset is null,'',quantity2preset),'#',if(priceType is null,'',priceType),'#',"+
		               		" if(priceFormula is null,'',priceFormula),'#',if(divideMultiply is null,'',divideMultiply ),'#',"+
		               		" if(perValue is null,'',perValue),'#',if(minimum is null ,'', minimum),'#',"+
		               		" if(payablePreset is null,'',payablePreset),'#',if(payablePriceType is null,'',payablePriceType),'#',"+
		               		" if(expensePrice is null,'',expensePrice),'#', if(payableContractCurrency is null,'',payableContractCurrency) ) AS formulaText"+
		               		" from charges where charge='"+charge+"' AND contract='"+billingContract+"' and corpID ='"+sessionCorpID+"'";
			List list = getSession().createSQLQuery(newQuery).addScalar("formulaText", Hibernate.STRING).list();
	      return list;
} catch (Exception e) {
	getSessionCorpID();
	 e.printStackTrace();
	logger.error("Error executing query"+ e,e);
	String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	 
}
   return null;
	}
	public List getInsuranceChargeRate(String charge,String billingContract,String sessionCorpID)
	{
		try {
			return getSession().createSQLQuery("select concat( if(quantity2preset is null,'',quantity2preset),'#',if(priceType is null,'',priceType),'#',if(priceFormula is null,'',priceFormula),'#',if(divideMultiply is null,'',divideMultiply ), '#',if(perValue is null,'',perValue),'#',if(minimum is null ,'', minimum)) AS formulaText from charges where charge='"+charge+"' AND contract='"+billingContract+"' and corpID ='"+sessionCorpID+"' ").addScalar("formulaText", Hibernate.STRING).list();
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  
		}
		return null;
	}
	
	public int updateTicket(String shipNumber, String storageMeasurement){
		try {
			return getHibernateTemplate().bulkUpdate("update WorkTicket set storageMeasurement= '"+storageMeasurement+"' where shipNumber=?",shipNumber);
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
	return 0;
	}
	
	public List insuranceValues(String insuranceOption, String sessionCorpID){
		try {
			return getHibernateTemplate().find("select bucket2 from RefMaster where parameter='insopt' and description in (ltrim('"+(insuranceOption.trim()).replaceAll("'", "''")+"'))" );
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}	
		return null;
	}
	public List baseBuyRate(String insuranceOption, String sessionCorpID){
		try {
			return this.getSession().createSQLQuery("select concat((if(flex1 is null or flex1 = '','NO',flex1)),'~',(if(flex2 is null or flex2 = '','NO',flex2))  ) from refmaster where parameter='insopt' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(sessionCorpID)+ "','"+ sessionCorpID+ "') and description in (ltrim('"+(insuranceOption.trim()).replaceAll("'", "''")+"'))" ).list();
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		}	
		return null;
	}

	public List<Billing>getByShipNumber(String shipNumber)
	{
		try {
			return getHibernateTemplate().find("from Billing where shipNumber=?",shipNumber);
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		}
		return null;
	}

	public List searchStorageBilling(String invoiceBillingDate,String sessionCorpID,String condition,String conditionInEx,
			String conditionBookingCodeType,String beginDate,String endDate,String shipNumbers,String firstName,String lastName,String cycles,String charges){
		 try {
			String searchCondition = "";
			if(shipNumbers != null && !"".equals(shipNumbers.trim())){
				searchCondition = " and s.shipNumber like '"+shipNumbers+"%' ";
			}
			if(cycles != null && !"".equals(cycles)){
				searchCondition = searchCondition + " and b.cycle like '"+cycles+"%'";
			}
			if(cycles != null && !"".equals(cycles)){
				searchCondition = searchCondition + " and b.cycle like '"+cycles+"%'";
			}
			if(firstName != null && !"".equals(firstName)){
				searchCondition = searchCondition + " and concat(s.lastName,' ' , s.firstName) like '%"+firstName.replaceAll(",","")+"%' ";
			}
			
			String query = "(select if( datediff(sysdate(), loada) < 91," +
					"concat('*',s.shipNumber), concat('''',s.shipNumber)) as shipNumber ," +
					"if(s.registrationNumber is null,'', s.registrationNumber) as registrationNumber, " +
					"s.firstName as firstName,concat(s.lastName,', ' ,s.firstName) as name," +
					"case when b.storageBillingParty='1' then b.billToCode  " +
					"when b.storageBillingParty='2' then b.billTo2Code " +
					"when b.storageBillingParty='3' then b.privatePartyBillingCode else b.billToCode end as billToCode," +
					"case when b.storageBillingParty='1' then b.billToName " +
					"when b.storageBillingParty='2' then b.billTo2Name " +
					"when b.storageBillingParty='3' then b.privatePartyBillingName else b.billToName end as billToName," +
					"(b.storagePerMonth * b.cycle)as storagePerMonth,if(b.charge is null,'', b.charge) as charge," +
					"if(s.job is null,'',s.job) as job, c.wording as t , if(b.postGrate is null,'',b.postGrate) as postGrate , " +
					"b.onHand  as onHand , if( b.createdBy is null,'', b.createdBy) as createdBy,if(b.updatedBy is null ,''," +
					"b.updatedBy) as updatedBy ,b.createdOn as createdOn ,b.updatedOn as updatedOn ,'storage' as insuranceHas ," +
					"if(b.insuranceRate is null ,'',b.insuranceRate) as insuranceRate," +
					"if(b.insuranceValueActual is null,'',b.insuranceValueActual) as insuranceValueActual," +
					"if(c.perValue is null,'', c.perValue) as perValue,p.storageBillingGroup as storageBillingGroup," +
					"b.cycle as cycle,if(b.contract is null,'', b.contract) as contract," +
					"if(c.minimum is null,'',c.minimum) as minimum ," +
					"if(c.quantityItemRevised is null ,'',c.quantityItemRevised) as quantityItemRevised ,  " +
					"if(c.quantityRevised='Preset2', if(c.quantityRevisedPreset is null || c.quantityRevisedPreset='','0',c.quantityRevisedPreset), " +
					"if(c.quantityRevisedSource='' || c.quantityRevisedSource is null,'0',c.quantityRevisedSource))  as quantityRevisedPreset ," +
					"if(c.perItem is null ,'',c.perItem) as perItem , " +
					"last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)) as toDates," +
					"DATEDIFF(last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)),'"+invoiceBillingDate+"' )+1 as dateDiff," +
					"c.gl as glCode , c.divideMultiply as divideMultiply,c.description as description,c.useDiscount as useDiscount," +
					"c.pricePre as pricePre, b.sitdiscount as sitdiscount,b.billToAuthority as billToAuthority, " +
					"t.deliveryShipper as deliveryShipper, t.deliveryA as deliveryA, DATE_ADD(b.billthrough, interval 1 day) as billThroughFrom, " +
					"c.expGl as payglCode,  "+
					"if(s.companyDivision is null ,'' ,s.companyDivision) as companyDivision,  "+
					"if(b.storageMeasurement is null ,'' ,b.storageMeasurement) as storageMeasurement, "+
					"if(b.baseInsuranceValue is null,'',b.baseInsuranceValue) as baseInsuranceValue, " +
					"if(b.vendorStoragePerMonth is null,'',b.vendorStoragePerMonth) as vendorStoragePerMonth, "+
					"if(b.insuranceBuyRate is null,'',b.insuranceBuyRate) as insuranceBuyRate, "+
					"if(b.insurancePerMonth is null,'',b.insurancePerMonth) as insurancePerMonth, " +
					"if(b.insuranceValueEntitle is null,'',b.insuranceValueEntitle) as insuranceValueEntitle, " +
					"if(b.payableRate is null,'',b.payableRate) as payableRate, " +
					"if(b.vendorCode1 is null,'',b.vendorCode1) as vendorCode1,if(b.vendorName1 is null,'',b.vendorName1) as vendorName1 " +
					"from billing b left outer join serviceorder s on b.id=s.id and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD') and s.status is not null left outer join trackingstatus t on b.id= t.id " +
					"left outer join charges c on b.contract=c.contract and b.charge=c.charge  and  c.corpID='"+sessionCorpID+"' "  +
					"inner join partnerprivate p on ( case b.storageBillingParty when '1' then b.billToCode when '2' then b.billTo2Code " +
					"when '3' then b.privatePartyBillingCode else b.billToCode end)=p.partnerCode  AND p.corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')   " +
					"where "+condition+" " +conditionInEx+conditionBookingCodeType+" " +
					"and (DATE_FORMAT(b.billThrough,'%Y-%m-%d') <= ('"+beginDate+" ')) " +
					"AND (DATE_FORMAT(b.billThrough,'%Y-%m-%d') >= ('"+endDate+"')) " +
					searchCondition+" and b.charge like '"+charges+"%' "+
					"and (b.storageOut is null OR b.storageOut >= (last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)))) and s.corpID='"+sessionCorpID+"' group by b.shipNumber)" + 
					
					" UNION (select if( datediff(sysdate(), loada) < 91,concat('*',s.shipNumber), " +
					"concat('''',s.shipNumber)) as shipNumber ,if(s.registrationNumber is null,'', s.registrationNumber) as registrationNumber, " +
					"s.firstName as firstName ,concat(s.lastName,', ' ,s.firstName) as name," +
					"case when b.storageBillingParty='1' then b.billToCode " +
					"when b.storageBillingParty='2' then b.billTo2Code " +
					"when b.storageBillingParty='3' then b.privatePartyBillingCode else b.billToCode end as billToCode," +
					"case when b.storageBillingParty='1' then b.billToName " +
					"when b.storageBillingParty='2' then b.billTo2Name " +
					"when b.storageBillingParty='3' then b.privatePartyBillingName else b.billToName  end as billToName," +
					"(b.insurancePerMonth * b.cycle) as storagePerMonth," +
					"if(b.insuranceCharge is null || b.insuranceCharge='','nocharge', b.insuranceCharge ) as charge," +
					"if(s.job is null,'',s.job) as job,c.wording as t , if(b.postGrate is null,'',b.postGrate) as postGrate , " +
					"b.onHand  as onHand , if( b.createdBy is null,'', b.createdBy) as createdBy," +
					"if(b.updatedBy is null ,'',b.updatedBy) as updatedBy ,b.createdOn as createdOn ,b.updatedOn as updatedOn ," +
					"if(b.insuranceHas is null ,'' ,b.insuranceHas) as insuranceHas ," +
					"if(b.insuranceRate is null,'',b.insuranceRate) as insuranceRate," +
					"if(b.insuranceValueActual is null ,'' , b.insuranceValueActual) as insuranceValueActual," +
					"if(c.perValue is null ,'',c.perValue) as perValue,p.storageBillingGroup as storageBillingGroup," +
					"b.cycle as cycle, if(b.contract is null,'', b.contract) as contract," +
					"if(c.minimum is null,'',c.minimum) as minimum , " +
					"if(c.quantityItemRevised is null ,'',c.quantityItemRevised) as quantityItemRevised , " +
					"if(c.quantityRevised='Preset2', if(c.quantityRevisedPreset is null || c.quantityRevisedPreset='','0'," +
					"c.quantityRevisedPreset), if(c.quantityRevisedSource='' || c.quantityRevisedSource is null,'0'," +
					"c.quantityRevisedSource)) as quantityRevisedPreset , if(c.perItem is null ,'',c.perItem) as perItem ,  " +
					"last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)) as toDates," +
					"DATEDIFF(last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)),'"+invoiceBillingDate+"' )+1 as dateDiff," +
					"c.gl as glCode , c.divideMultiply as divideMultiply,  c.description as description," +
					"c.useDiscount as useDiscount,c.pricePre as pricePre, b.sitdiscount as sitdiscount," +
					"b.billToAuthority as billToAuthority, t.deliveryShipper as deliveryShipper, t.deliveryA as deliveryA, " +
					"DATE_ADD(b.billthrough, interval 1 day) as billThroughFrom, " +
					"c.expGl as payglCode,  "+
					"if(s.companyDivision is null ,'' ,s.companyDivision) as companyDivision,  "+
					"if(b.storageMeasurement is null ,'' ,b.storageMeasurement) as storageMeasurement, "+
					"if(b.baseInsuranceValue is null,'',b.baseInsuranceValue) as baseInsuranceValue, " +
					"0.00 as vendorStoragePerMonth, "+
					"if(b.insuranceBuyRate is null,'',b.insuranceBuyRate) as insuranceBuyRate, "+
					"if(b.insurancePerMonth is null,'',b.insurancePerMonth) as insurancePerMonth, " +
					"if(b.insuranceValueEntitle is null,'',b.insuranceValueEntitle) as insuranceValueEntitle, " +
					"if(b.payableRate is null,'',b.payableRate) as payableRate, " +
					"if(b.vendorCode is null,'',b.vendorCode) as vendorCode1,if(b.vendorName is null,'',b.vendorName) as vendorName1 "+
					"from billing b left outer join serviceorder s on b.id=s.id  and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD') and s.status is not null " +
					"left outer join trackingstatus t on b.id= t.id  " +
					"left outer join charges c on b.contract=c.contract and b.insuranceCharge=c.charge  and  c.corpID='"+sessionCorpID+"' "  +
					"inner join partnerprivate p on ( case b.storageBillingParty when '1' then b.billToCode " +
					"when '2' then b.billTo2Code when '3' then b.privatePartyBillingCode else b.billToCode end)=p.partnerCode  AND p.corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')   " +
					"where "+condition+" " + conditionInEx+conditionBookingCodeType+" " +
					"and (DATE_FORMAT(b.billThrough,'%Y-%m-%d') <= ('"+beginDate+" ')) " +
					"AND (DATE_FORMAT(b.billThrough,'%Y-%m-%d') >= ('"+endDate+"')) " +
					"and (b.storageOut is null OR b.storageOut >= (last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)))) and s.corpID='"+sessionCorpID+"' " +
					"and (b.insuranceCharge <> null || b.insuranceCharge <> '') " +
					searchCondition+"and b.insuranceCharge like '"+charges+"%' "+
					"group by b.shipNumber) order by shipNumber";
			
			List list= getSession().createSQLQuery(query)
					.addScalar("shipNumber", Hibernate.STRING) 
					.addScalar("registrationNumber", Hibernate.STRING) 
					.addScalar("firstName", Hibernate.STRING) 
					.addScalar("name", Hibernate.STRING) 
					.addScalar("billToCode", Hibernate.STRING) 
					.addScalar("billToName", Hibernate.STRING) 
					.addScalar("storagePerMonth", Hibernate.STRING) 
					.addScalar("charge", Hibernate.STRING) 
					.addScalar("job", Hibernate.STRING) 
					.addScalar("t", Hibernate.STRING) 
					.addScalar("postGrate", Hibernate.BIG_DECIMAL) 
					.addScalar("onHand", Hibernate.BIG_DECIMAL) 
					.addScalar("createdBy", Hibernate.STRING) 
					.addScalar("updatedBy", Hibernate.STRING) 
					.addScalar("createdOn", Hibernate.DATE) 
					.addScalar("updatedOn", Hibernate.DATE) 
					.addScalar("insuranceHas", Hibernate.STRING) 
					.addScalar("insuranceRate", Hibernate.BIG_DECIMAL) 
					.addScalar("insuranceValueActual", Hibernate.BIG_DECIMAL) 
					.addScalar("perValue", Hibernate.BIG_DECIMAL) 
					.addScalar("storageBillingGroup", Hibernate.STRING) 
			        .addScalar("cycle", Hibernate.BIG_DECIMAL) 
			        .addScalar("contract", Hibernate.STRING)
			        .addScalar("minimum", Hibernate.BIG_DECIMAL)
			        .addScalar("quantityItemRevised", Hibernate.STRING)
			        .addScalar("quantityRevisedPreset", Hibernate.STRING)
			        .addScalar("perItem", Hibernate.STRING)
			        .addScalar("toDates", Hibernate.STRING)
			        .addScalar("dateDiff", Hibernate.INTEGER)
			        .addScalar("glCode", Hibernate.STRING)
			        .addScalar("divideMultiply", Hibernate.STRING)
			        .addScalar("description", Hibernate.STRING)
			        .addScalar("useDiscount", Hibernate.STRING)
			        .addScalar("pricePre", Hibernate.DOUBLE)
			        .addScalar("sitdiscount", Hibernate.BIG_DECIMAL)
			        .addScalar("billToAuthority", Hibernate.STRING)
			        .addScalar("deliveryShipper", Hibernate.DATE)
			        .addScalar("deliveryA", Hibernate.DATE)
			        .addScalar("billThroughFrom", Hibernate.DATE)
			        .addScalar("payglCode",  Hibernate.STRING)
			        .addScalar("companyDivision",  Hibernate.STRING)
			        .addScalar("storageMeasurement", Hibernate.STRING)
			        .addScalar("baseInsuranceValue",  Hibernate.BIG_DECIMAL) 
                    .addScalar("vendorStoragePerMonth",  Hibernate.STRING)
                    .addScalar("insuranceBuyRate",  Hibernate.STRING)
                    .addScalar("insurancePerMonth",  Hibernate.BIG_DECIMAL)
                    .addScalar("insuranceValueEntitle",  Hibernate.BIG_DECIMAL)
                    .addScalar("payableRate",  Hibernate.BIG_DECIMAL)
                    .addScalar("vendorCode1", Hibernate.STRING)
					.addScalar("vendorName1", Hibernate.STRING)
					.list();
			
			return list;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  
		}
		return null;
	}

	public List previewStroageBilling(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID){
	
	
		
		try {
			
			
				List list= getSession().createSQLQuery("(select if( datediff(sysdate(), loada) < 91," +
					"concat('*',s.shipNumber), concat('''',s.shipNumber)) as shipNumber ," +
					"if(s.registrationNumber is null,'', s.registrationNumber) as registrationNumber, " +
					"s.firstName as firstName,concat(s.lastName,', ' ,s.firstName) as name," +
					"case when b.storageBillingParty='1' then b.billToCode  " +
					"when b.storageBillingParty='2' then b.billTo2Code " +
					"when b.storageBillingParty='3' then b.privatePartyBillingCode else b.billToCode end as billToCode," +
					"case when b.storageBillingParty='1' then b.billToName " +
					"when b.storageBillingParty='2' then b.billTo2Name " +
					"when b.storageBillingParty='3' then b.privatePartyBillingName else b.billToName end as billToName," +
					"(b.storagePerMonth * b.cycle)as storagePerMonth,if(b.charge is null,'', b.charge) as charge," +
					"if(s.job is null,'',s.job) as job, c.wording as t , if(b.postGrate is null,'',b.postGrate) as postGrate , " +
					"b.onHand  as onHand , if( b.createdBy is null,'', b.createdBy) as createdBy,if(b.updatedBy is null ,''," +
					"b.updatedBy) as updatedBy ,b.createdOn as createdOn ,b.updatedOn as updatedOn ,'storage' as insuranceHas ," +
					"if(b.insuranceRate is null ,'',b.insuranceRate) as insuranceRate," +
					"if(b.insuranceValueActual is null,'',b.insuranceValueActual) as insuranceValueActual," +
					"if(c.perValue is null,'', c.perValue) as perValue,p.storageBillingGroup as storageBillingGroup," +
					"b.cycle as cycle,if(b.contract is null,'', b.contract) as contract," +
					"if(c.minimum is null,'',c.minimum) as minimum ," +
					"if(c.quantityItemRevised is null ,'',c.quantityItemRevised) as quantityItemRevised ,  " +
					"if(c.quantityRevised='Preset2', if(c.quantityRevisedPreset is null || c.quantityRevisedPreset='','0',c.quantityRevisedPreset), " +
					"if(c.quantityRevisedSource='' || c.quantityRevisedSource is null,'0',c.quantityRevisedSource))  as quantityRevisedPreset ," +
					"if(c.perItem is null ,'',c.perItem) as perItem , " +
					"last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)) as toDates," +
					"DATEDIFF(last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)),'"+invoiceBillingDate+"' )+1 as dateDiff," +
					"c.gl as glCode , c.divideMultiply as divideMultiply,c.description as description,c.useDiscount as useDiscount," +
					"c.pricePre as pricePre, b.sitdiscount as sitdiscount,b.billToAuthority as billToAuthority, " +
					"t.deliveryShipper as deliveryShipper, t.deliveryA as deliveryA, DATE_ADD(b.billthrough, interval 1 day) as billThroughFrom, " +
					"c.expGl as payglCode,  "+
					"s.companyDivision as companyDivision,  "+
					"if(b.storageMeasurement is null ,'' ,b.storageMeasurement) as storageMeasurement, "+
					"if(b.baseInsuranceValue is null,'',b.baseInsuranceValue) as baseInsuranceValue, " +
					"if(b.vendorStoragePerMonth is null,'',b.vendorStoragePerMonth) as vendorStoragePerMonth, "+
					"if(b.insuranceBuyRate is null,'',b.insuranceBuyRate) as insuranceBuyRate, "+
					"if(b.insurancePerMonth is null,'',b.insurancePerMonth) as insurancePerMonth, " +
					"if(b.insuranceValueEntitle is null,'',b.insuranceValueEntitle) as insuranceValueEntitle, " +
					"if(b.payableRate is null,'',b.payableRate) as payableRate, " +
					"if(b.vendorCode1 is null,'',b.vendorCode1) as vendorCode1,if(b.vendorName1 is null,'',b.vendorName1) as vendorName1 " +
					"from billing b left outer join serviceorder s on b.id=s.id and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD') and s.status is not null left outer join trackingstatus t on b.id= t.id " +
					"left outer join charges c on b.contract=c.contract and b.charge=c.charge  and  c.corpID='"+sessionCorpID+"' "  +
					"inner join partnerprivate p on ( case b.storageBillingParty when '1' then b.billToCode when '2' then b.billTo2Code " +
					"when '3' then b.privatePartyBillingCode else b.billToCode end)=p.partnerCode  AND p.corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')   " +
					"where "+condition+" " +conditionInEx+conditionBookingCodeType+" " +
					"and (DATE_FORMAT(b.billThrough,'%Y-%m-%d') <= ('"+beginDate+" ')) " +
					"AND (DATE_FORMAT(b.billThrough,'%Y-%m-%d') >= ('"+endDate+"')) " +
					"and (b.storageOut is null OR b.storageOut >= (last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)))) and s.corpID='"+sessionCorpID+"' group by b.shipNumber)" + 
					
					" UNION (select if( datediff(sysdate(), loada) < 91,concat('*',s.shipNumber), " +
					"concat('''',s.shipNumber)) as shipNumber ,if(s.registrationNumber is null,'', s.registrationNumber) as registrationNumber, " +
					"s.firstName as firstName ,concat(s.lastName,', ' ,s.firstName) as name," +
					"case when b.storageBillingParty='1' then b.billToCode " +
					"when b.storageBillingParty='2' then b.billTo2Code " +
					"when b.storageBillingParty='3' then b.privatePartyBillingCode else b.billToCode end as billToCode," +
					"case when b.storageBillingParty='1' then b.billToName " +
					"when b.storageBillingParty='2' then b.billTo2Name " +
					"when b.storageBillingParty='3' then b.privatePartyBillingName else b.billToName  end as billToName," +
					"(b.insurancePerMonth * b.cycle) as storagePerMonth," +
					"if(b.insuranceCharge is null || b.insuranceCharge='','nocharge', b.insuranceCharge ) as charge," +
					"if(s.job is null,'',s.job) as job,c.wording as t , if(b.postGrate is null,'',b.postGrate) as postGrate , " +
					"b.onHand  as onHand , if( b.createdBy is null,'', b.createdBy) as createdBy," +
					"if(b.updatedBy is null ,'',b.updatedBy) as updatedBy ,b.createdOn as createdOn ,b.updatedOn as updatedOn ," +
					"if(b.insuranceHas is null ,'' ,b.insuranceHas) as insuranceHas ," +
					"if(b.insuranceRate is null,'',b.insuranceRate) as insuranceRate," +
					"if(b.insuranceValueActual is null ,'' , b.insuranceValueActual) as insuranceValueActual," +
					"if(c.perValue is null ,'',c.perValue) as perValue,p.storageBillingGroup as storageBillingGroup," +
					"b.cycle as cycle, if(b.contract is null,'', b.contract) as contract," +
					"if(c.minimum is null,'',c.minimum) as minimum , " +
					"if(c.quantityItemRevised is null ,'',c.quantityItemRevised) as quantityItemRevised , " +
					"if(c.quantityRevised='Preset2', if(c.quantityRevisedPreset is null || c.quantityRevisedPreset='','0'," +
					"c.quantityRevisedPreset), if(c.quantityRevisedSource='' || c.quantityRevisedSource is null,'0'," +
					"c.quantityRevisedSource)) as quantityRevisedPreset , if(c.perItem is null ,'',c.perItem) as perItem ,  " +
					"last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)) as toDates," +
					"DATEDIFF(last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)),'"+invoiceBillingDate+"' )+1 as dateDiff," +
					"c.gl as glCode , c.divideMultiply as divideMultiply,  c.description as description," +
					"c.useDiscount as useDiscount,c.pricePre as pricePre, b.sitdiscount as sitdiscount," +
					"b.billToAuthority as billToAuthority, t.deliveryShipper as deliveryShipper, t.deliveryA as deliveryA, " +
					"DATE_ADD(b.billthrough, interval 1 day) as billThroughFrom, " +
					"c.expGl as payglCode,  "+
					"s.companyDivision as companyDivision,  "+
					"if(b.storageMeasurement is null ,'' ,b.storageMeasurement) as storageMeasurement, "+
					"if(b.baseInsuranceValue is null,'',b.baseInsuranceValue) as baseInsuranceValue, " +
					"0.00 as vendorStoragePerMonth, "+
					"if(b.insuranceBuyRate is null,'',b.insuranceBuyRate) as insuranceBuyRate, "+
					"if(b.insurancePerMonth is null,'',b.insurancePerMonth) as insurancePerMonth, " +
					"if(b.insuranceValueEntitle is null,'',b.insuranceValueEntitle) as insuranceValueEntitle, " +
					"if(b.payableRate is null,'',b.payableRate) as payableRate, " +
					"if(b.vendorCode is null,'',b.vendorCode) as vendorCode1,if(b.vendorName is null,'',b.vendorName) as vendorName1 "+
					"from billing b left outer join serviceorder s on b.id=s.id  and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD') and s.status is not null " +
					"left outer join trackingstatus t on b.id= t.id  " +
					"left outer join charges c on b.contract=c.contract and b.insuranceCharge=c.charge  and  c.corpID='"+sessionCorpID+"' "  +
					"inner join partnerprivate p on ( case b.storageBillingParty when '1' then b.billToCode " +
					"when '2' then b.billTo2Code when '3' then b.privatePartyBillingCode else b.billToCode end)=p.partnerCode  AND p.corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')   " +
					"where "+condition+" " + conditionInEx+conditionBookingCodeType+" " +
					"and (DATE_FORMAT(b.billThrough,'%Y-%m-%d') <= ('"+beginDate+" ')) " +
					"AND (DATE_FORMAT(b.billThrough,'%Y-%m-%d') >= ('"+endDate+"')) " +
					"and (b.storageOut is null OR b.storageOut >= (last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)))) and s.corpID='"+sessionCorpID+"' " +
					"and (b.insuranceCharge <> null || b.insuranceCharge <> '') " +
					"group by b.shipNumber) order by shipNumber") 
					.addScalar("shipNumber", Hibernate.STRING) 
					.addScalar("registrationNumber", Hibernate.STRING) 
					.addScalar("firstName", Hibernate.STRING) 
					.addScalar("name", Hibernate.STRING) 
					.addScalar("billToCode", Hibernate.STRING) 
					.addScalar("billToName", Hibernate.STRING) 
					.addScalar("storagePerMonth", Hibernate.STRING) 
					.addScalar("charge", Hibernate.STRING) 
					.addScalar("job", Hibernate.STRING) 
					.addScalar("t", Hibernate.STRING) 
					.addScalar("postGrate", Hibernate.BIG_DECIMAL) 
					.addScalar("onHand", Hibernate.BIG_DECIMAL) 
					.addScalar("createdBy", Hibernate.STRING) 
					.addScalar("updatedBy", Hibernate.STRING) 
					.addScalar("createdOn", Hibernate.DATE) 
					.addScalar("updatedOn", Hibernate.DATE) 
					.addScalar("insuranceHas", Hibernate.STRING) 
					.addScalar("insuranceRate", Hibernate.BIG_DECIMAL) 
					.addScalar("insuranceValueActual", Hibernate.BIG_DECIMAL) 
					.addScalar("perValue", Hibernate.BIG_DECIMAL) 
					.addScalar("storageBillingGroup", Hibernate.STRING) 
			        .addScalar("cycle", Hibernate.BIG_DECIMAL) 
			        .addScalar("contract", Hibernate.STRING)
			        .addScalar("minimum", Hibernate.BIG_DECIMAL)
			        .addScalar("quantityItemRevised", Hibernate.STRING)
			        .addScalar("quantityRevisedPreset", Hibernate.STRING)
			        .addScalar("perItem", Hibernate.STRING)
			        .addScalar("toDates", Hibernate.STRING)
			        .addScalar("dateDiff", Hibernate.INTEGER)
			        .addScalar("glCode", Hibernate.STRING)
			        .addScalar("divideMultiply", Hibernate.STRING)
			        .addScalar("description", Hibernate.STRING)
			        .addScalar("useDiscount", Hibernate.STRING)
			        .addScalar("pricePre", Hibernate.DOUBLE)
			        .addScalar("sitdiscount", Hibernate.BIG_DECIMAL)
			        .addScalar("billToAuthority", Hibernate.STRING)
			        .addScalar("deliveryShipper", Hibernate.DATE)
			        .addScalar("deliveryA", Hibernate.DATE)
			        .addScalar("billThroughFrom", Hibernate.DATE)
			        .addScalar("payglCode",  Hibernate.STRING)
			        .addScalar("companyDivision",  Hibernate.STRING)
			        .addScalar("storageMeasurement",  Hibernate.STRING)
			        .addScalar("baseInsuranceValue",  Hibernate.BIG_DECIMAL) 
					.addScalar("vendorStoragePerMonth",  Hibernate.STRING)
					.addScalar("insuranceBuyRate",  Hibernate.STRING)
					.addScalar("insurancePerMonth",  Hibernate.BIG_DECIMAL)
					.addScalar("insuranceValueEntitle",  Hibernate.BIG_DECIMAL)
					.addScalar("payableRate",  Hibernate.BIG_DECIMAL)
					.addScalar("vendorCode1", Hibernate.STRING)
					.addScalar("vendorName1", Hibernate.STRING)
			
					.list();
				
					List result= new ArrayList(); 
					BillingDTO billingDTO=new BillingDTO();
				if(list.size()>0){
					try
					{
					Iterator it=list.iterator();
					while(it.hasNext()) 
					{ 
					Object []obj=(Object[]) it.next();
					if(obj[7].toString().equalsIgnoreCase("nocharge")) 
					{
					}
					else 
					{
						
					billingDTO=new BillingDTO(); 
					billingDTO.setShipNumber(obj[0]);
					billingDTO.setRegistrationNumber(obj[1]);
					billingDTO.setFirstName(obj[2]);
					billingDTO.setLastName(obj[3]);
					billingDTO.setBillToCode(obj[4]);
					billingDTO.setBillToName(obj[5]);
					if(obj[6]==null)
					{
						billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
					}else
					{
						billingDTO.setStoragePerMonth(obj[6]);
					}
					
					billingDTO.setCharge(obj[7]);
					billingDTO.setJob(obj[8]);
					billingDTO.setWording(obj[9]);
					billingDTO.setPostGrate(obj[10]);
					try
					{
					billingDTO.setOnHand(obj[11]);
						}
						catch(Exception ex)
						{
							 ex.printStackTrace();
						}
					billingDTO.setCreatedBy(obj[12]);
					billingDTO.setUpdatedBy(obj[13]);
					billingDTO.setCreatedOn(obj[14]);
					billingDTO.setUpdatedOn(obj[15]);
					billingDTO.setInsuranceHas(obj[16]);
					billingDTO.setInsuranceRate(obj[17]);
					billingDTO.setInsuranceValueActual(obj[18]);
					billingDTO.setPerValue(obj[19]);
					billingDTO.setStorageBillingParty(obj[20]);//storageBillingGroup
					billingDTO.setCycle(obj[21]);
					billingDTO.setContract(obj[22]);
					billingDTO.setMinimum(obj[23]); 
					billingDTO.setQuantityItemRevised(obj[24]);
					if(obj[25].toString().startsWith("0")|| obj[25].toString().startsWith("1")
					   ||obj[25].toString().startsWith("2")|| obj[25].toString().startsWith("3")
					   || obj[25].toString().startsWith("4")|| obj[25].toString().startsWith("5")
					   ||obj[25].toString().startsWith("6")|| obj[25].toString().startsWith("7")
					   ||obj[25].toString().startsWith("8")|| obj[25].toString().startsWith("9"))
					{
					  billingDTO.setQuantityRevisedPreset(obj[25]);
					}
					else
					{
						billingDTO.setQuantityRevisedPreset(("0.00"));	
					}   
					billingDTO.setPerItem(obj[26]);
					billingDTO.setToDates(obj[27]);
					billingDTO.setDateDiff(obj[28]);
					billingDTO.setGlCode(obj[29]);
					billingDTO.setDivideMultiply(obj[30]);
					billingDTO.setDescription(obj[31]);
					billingDTO.setUseDiscount(obj[32]);
					billingDTO.setPricePre(obj[33]);
					billingDTO.setSitdiscount(obj[34]);
					billingDTO.setBillToAuthority(obj[35]);
					billingDTO.setDeliveryShipper(obj[36]);
					billingDTO.setDeliveryA(obj[37]);
					billingDTO.setBillThroughFrom(obj[38]);
					if(obj[39]!=null){
					billingDTO.setPayglCode(obj[39]);
					}else{
						billingDTO.setPayglCode("");	
					}
					if(obj[40]!=null){
						billingDTO.setCompanyDivision(obj[40]);
						}else{
							billingDTO.setCompanyDivision("");	
						}
					if(obj[41]!=null){
						billingDTO.setStorageMeasurement(obj[41]);
					}else{
						billingDTO.setStorageMeasurement("");
					}
					if(obj[42]!=null){
						billingDTO.setBaseInsuranceValue(obj[42]);
					}else{
						billingDTO.setBaseInsuranceValue("");
					}
					if(obj[43]!=null){
						billingDTO.setVendorStoragePerMonth(obj[43]);
					}else{
						billingDTO.setVendorStoragePerMonth("");
					}
					if(obj[44]!=null){
						billingDTO.setInsuranceBuyRate(obj[44]);
					}else{
						billingDTO.setInsuranceBuyRate("");
					}
					if(obj[45]!=null){
						billingDTO.setInsurancePerMonth(obj[45]);
					}else{
						billingDTO.setInsurancePerMonth("");
					}
					if(obj[46]!=null){
						billingDTO.setInsuranceValueEntitle(obj[46]);
					}else{
						billingDTO.setInsuranceValueEntitle("");
					}
					if(obj[47]!=null){
						billingDTO.setPayableRate(obj[47]);
					}else{
						billingDTO.setPayableRate("");
					}
					if(obj[48]!=null){
						billingDTO.setVendorCode1(obj[48]);
					}else{
						billingDTO.setVendorCode1("");
					}
					if(obj[49]!=null){
						billingDTO.setVendorName1(obj[49]);
					}else{
						billingDTO.setVendorName1("");
					}
					result.add(billingDTO);
					}
			}
					}catch(Exception ex)
					{
						 ex.printStackTrace();
					}
					
				}
				else
				{
					billingDTO.setShipNumber("");
					billingDTO.setRegistrationNumber("");
					billingDTO.setFirstName("");
					billingDTO.setLastName("");
					billingDTO.setBillToCode("");
					billingDTO.setBillToName("");
					billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
					billingDTO.setCharge("");
					billingDTO.setJob("");
					billingDTO.setWording("");
					billingDTO.setPostGrate("");
					billingDTO.setOnHand("");
					billingDTO.setCreatedBy("");
					billingDTO.setUpdatedBy("");
					billingDTO.setCreatedOn("");
					billingDTO.setUpdatedOn("");
					billingDTO.setInsuranceHas("");
					billingDTO.setInsuranceRate("");
					billingDTO.setInsuranceValueActual("");
					billingDTO.setPerValue("");
					billingDTO.setStorageBillingParty("");//storageBillingGroup
					billingDTO.setCycle("");
					billingDTO.setContract("Contract Mismatch");
					billingDTO.setMinimum(""); 
					billingDTO.setQuantityItemRevised("");
					billingDTO.setQuantityRevisedPreset("");
					billingDTO.setPerItem("");
					billingDTO.setToDates("");
					billingDTO.setDateDiff("");
					billingDTO.setGlCode("");
					billingDTO.setDivideMultiply("");
					billingDTO.setForChecking("y");
					billingDTO.setDescription("");
					billingDTO.setUseDiscount("");
					billingDTO.setPricePre("");
					billingDTO.setSitdiscount("");
					billingDTO.setBillToAuthority("");
					billingDTO.setDeliveryShipper("");
					billingDTO.setDeliveryA("");
					billingDTO.setBillThroughFrom("");
					billingDTO.setPayglCode("");
					billingDTO.setCompanyDivision("");
					billingDTO.setStorageMeasurement("");
					result.add(billingDTO);
				}
				return result;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}	
	return null;
	} 
	
	// UTSI
	private List getStorageBillingQuery(String sessionCorpID,String condition,String conditionInEx,String conditionBookingCodeType,String beginDate,String endDate,String shipNumbers,String firstName,String cycles,String charges,String  radioContractType){
		try {
			String query = "(select if( datediff(sysdate(), loada) < 91," +
			"concat('*',s.shipNumber), concat('''',s.shipNumber)) as shipNumber ," +
			"if(s.registrationNumber is null,'', s.registrationNumber) as registrationNumber, " +
			"s.firstName as firstName,concat(s.lastName,', ' ,s.firstName) as name," +
			"case when b.storageBillingParty='1' then b.billToCode  " +
			"when b.storageBillingParty='2' then b.billTo2Code " +
			"when b.storageBillingParty='3' then b.privatePartyBillingCode else b.billToCode end as billToCode," +
			"case when b.storageBillingParty='1' then b.billToName " +
			"when b.storageBillingParty='2' then b.billTo2Name " +
			"when b.storageBillingParty='3' then b.privatePartyBillingName else b.billToName end as billToName," +
			"(b.storagePerMonth * b.cycle)as storagePerMonth,if(b.charge is null,'', b.charge) as charge," +
			"if(s.job is null,'',s.job) as job, c.wording as t , if(b.postGrate is null,'',b.postGrate) as postGrate , " +
			"b.onHand  as onHand , if( b.createdBy is null,'', b.createdBy) as createdBy,if(b.updatedBy is null ,''," +
			"b.updatedBy) as updatedBy ,b.createdOn as createdOn ,b.updatedOn as updatedOn ,'storage' as insuranceHas ," +
			"if(b.insuranceRate is null ,'',b.insuranceRate) as insuranceRate," +
			"if(b.insuranceValueActual is null,'',b.insuranceValueActual) as insuranceValueActual," +
			"if(c.perValue is null,'', c.perValue) as perValue,p.storageBillingGroup as storageBillingGroup," +
			"b.cycle as cycle,if(b.contract is null,'', b.contract) as contract," +
			"if(c.minimum is null,'',c.minimum) as minimum ," +
			"if(c.quantityItemRevised is null ,'',c.quantityItemRevised) as quantityItemRevised ,  " +
			"if(c.quantityRevised='Preset2', if(c.quantityRevisedPreset is null || c.quantityRevisedPreset='','0',c.quantityRevisedPreset), " +
			"if(c.quantityRevisedSource='' || c.quantityRevisedSource is null,'0',c.quantityRevisedSource))  as quantityRevisedPreset ," +
			"if(c.perItem is null ,'',c.perItem) as perItem , " +
			"last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)) as toDates," +
			"DATEDIFF(last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)),b.billthrough ) as dateDiff," +
			"c.gl as glCode , c.divideMultiply as divideMultiply,c.description as description,c.useDiscount as useDiscount," +
			"c.pricePre as pricePre, b.sitdiscount as sitdiscount,b.billToAuthority as billToAuthority, " +
			"t.deliveryShipper as deliveryShipper, t.deliveryA as deliveryA, DATE_ADD(b.billthrough, interval 1 day) as billThroughFrom, " +
			"if(b.storageVatPercent is null or b.storageVatPercent='',0.00,b.storageVatPercent) as vatPercent,  "+
			"if(b.storageVatDescr is null or b.storageVatDescr='','',b.storageVatDescr) as vatDescr,  "+
			"c.expGl as payglCode,  "+
			"if(b.baseInsuranceValue is null,'',b.baseInsuranceValue) as baseInsuranceValue, " +
			"if(b.billingCurrency is null,'',b.billingCurrency) as billingCurrency, "+
			"if(b.vendorStoragePerMonth is null,'',b.vendorStoragePerMonth) as vendorStoragePerMonth, "+
			"if(b.vendorStorageVatDescr is null,'',b.vendorStorageVatDescr) as vendorStorageVatDescr, "+
			"if(b.vendorStorageVatPercent is null,'',b.vendorStorageVatPercent) as vendorStorageVatPercent, "+
			"if(b.vendorBillingCurency is null,'',b.vendorBillingCurency) as vendorBillingCurency, "+
			"if(b.vendorCode1 is null,'',b.vendorCode1) as vendorCode1,if(b.vendorName1 is null,'',b.vendorName1) as vendorName1, "+
			"if(b.insuranceCharge1 is null,'',b.insuranceCharge1) as insuranceCharge1, "+
			"if(c.contractCurrency is null,'',c.contractCurrency) as contractCurrency, "+
			"if(c.payableContractCurrency is null,'',c.payableContractCurrency) as payableContractCurrency, "+
			"if(b.contractCurrency is null,'',b.contractCurrency) as billingContractCurrency, "+
			"if(b.payableContractCurrency is null,'',b.payableContractCurrency) as billingPayableContractCurrency, "+
			"if(b.currency is null,'',b.currency) as currency, "+
			"if(b.insuranceBuyRate is null,'',b.insuranceBuyRate) as insuranceBuyRate, "+
			"if(c.VATExclude = '' || c.VATExclude is null,false,c.VATExclude) as VATExclude, "+
			"if(b.insurancePerMonth is null,'',b.insurancePerMonth) as insurancePerMonth, " +
			"if(b.insuranceValueEntitle is null,'',b.insuranceValueEntitle) as insuranceValueEntitle, " +
			"if(b.payableRate is null,'',b.payableRate) as payableRate, " +
			"if(b.vendorCodeVatCode is null,'',b.vendorCodeVatCode) as vendorCodeVatCode , " +
			"if(ct.dmmInsurancePolicy is true,true,false) as dmmInsurancePolicy , " +
			"if(b.bookingAgentVatCode is null,'',b.bookingAgentVatCode) as bookingAgentVatCode,  " +
			"concat( if(c.quantity2preset is null,'',c.quantity2preset),'#',if(c.priceType is null,'',c.priceType),'#',if(c.priceFormula is null,'',c.priceFormula),'#',if(c.divideMultiply is null,'',c.divideMultiply ),'#',if(c.perValue is null,'',c.perValue),'#',if(c.minimum is null ,'', c.minimum),'#',if(c.payablePreset is null,'',c.payablePreset),'#',if(c.payablePriceType is null,'',c.payablePriceType),'#',if(c.expensePrice is null,'',c.expensePrice),'#', if(c.payableContractCurrency is null,'',c.payableContractCurrency) ) AS formulaText ,  "+
            "s.id as serviceOrderId,   "+
            "if(costElement is null,'',costElement) as costElement,  " +
            "if(scostElementDescription is null,'',scostElementDescription) as costElementDescription,   "+
            "if(b.storageMeasurement is null ,'' ,b.storageMeasurement) as storageMeasurement "+
			"from billing b left outer join serviceorder s on b.id=s.id and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD') and s.status is not null left outer join trackingstatus t on b.id= t.id " +
			"left outer join charges c on b.contract=c.contract and b.charge=c.charge and  c.corpID='"+sessionCorpID+"' "  +
			"left outer join contract ct on b.contract=ct.contract  and ct.corpID='"+sessionCorpID+"' "    +
			"inner join partnerprivate p on ( case b.storageBillingParty when '1' then b.billToCode when '2' then b.billTo2Code " +
			"when '3' then b.privatePartyBillingCode else b.billToCode end)=p.partnerCode  AND p.corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')   " +
			"where "+condition+" " +conditionInEx+conditionBookingCodeType+" " +
			"and (DATE_FORMAT(b.billThrough,'%Y-%m-%d') <= ('"+beginDate+" ')) " +
			"AND (DATE_FORMAT(b.billThrough,'%Y-%m-%d') >= ('"+endDate+"')) " +
			"and (b.storageOut is null OR b.storageOut >= (last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH))))  and s.corpID='"+sessionCorpID+"' "+
			"and s.shipNumber like '"+shipNumbers+"%' and concat(s.lastName,' ' , s.firstName) like '%"+firstName.replaceAll(",","")+"%'  and b.cycle like '"+cycles+"%' and b.charge like '"+charges+"%' "+
			"and ct.contractType='"+radioContractType+"' group by b.shipNumber)" + 
			
" UNION (select if( datediff(sysdate(), loada) < 91,concat('*',s.shipNumber), " +
			"concat('''',s.shipNumber)) as shipNumber ,if(s.registrationNumber is null,'', s.registrationNumber) as registrationNumber, " +
			"s.firstName as firstName ,concat(s.lastName,', ' ,s.firstName) as name," +
			"case when b.storageBillingParty='1' then b.billToCode " +
			"when b.storageBillingParty='2' then b.billTo2Code " +
			"when b.storageBillingParty='3' then b.privatePartyBillingCode else b.billToCode end as billToCode," +
			"case when b.storageBillingParty='1' then b.billToName " +
			"when b.storageBillingParty='2' then b.billTo2Name " +
			"when b.storageBillingParty='3' then b.privatePartyBillingName else b.billToName  end as billToName," +
			"(b.insurancePerMonth * b.cycle) as storagePerMonth," +
			"if(b.insuranceCharge is null || b.insuranceCharge='','nocharge', b.insuranceCharge ) as charge," +
			"if(s.job is null,'',s.job) as job,c.wording as t , if(b.postGrate is null,'',b.postGrate) as postGrate , " +
			"b.onHand  as onHand , if( b.createdBy is null,'', b.createdBy) as createdBy," +
			"if(b.updatedBy is null ,'',b.updatedBy) as updatedBy ,b.createdOn as createdOn ,b.updatedOn as updatedOn ," +
			"if(b.insuranceHas is null ,'' ,b.insuranceHas) as insuranceHas ," +
			"if(b.insuranceRate is null,'',b.insuranceRate) as insuranceRate," +
			"if(b.insuranceValueActual is null ,'' , b.insuranceValueActual) as insuranceValueActual," +
			"if(c.perValue is null ,'',c.perValue) as perValue,p.storageBillingGroup as storageBillingGroup," +
			"b.cycle as cycle, if(b.contract is null,'', b.contract) as contract," +
			"if(c.minimum is null,'',c.minimum) as minimum , " +
			"if(c.quantityItemRevised is null ,'',c.quantityItemRevised) as quantityItemRevised , " +
			"if(c.quantityRevised='Preset2', if(c.quantityRevisedPreset is null || c.quantityRevisedPreset='','0'," +
			"c.quantityRevisedPreset), if(c.quantityRevisedSource='' || c.quantityRevisedSource is null,'0'," +
			"c.quantityRevisedSource)) as quantityRevisedPreset , if(c.perItem is null ,'',c.perItem) as perItem ,  " +
			"last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)) as toDates," +
			"DATEDIFF(last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)),b.billthrough ) as dateDiff," +
			"c.gl as glCode , c.divideMultiply as divideMultiply,  c.description as description," +
			"c.useDiscount as useDiscount,c.pricePre as pricePre, b.sitdiscount as sitdiscount," +
			"b.billToAuthority as billToAuthority, t.deliveryShipper as deliveryShipper, t.deliveryA as deliveryA, " +
			"DATE_ADD(b.billthrough, interval 1 day) as billThroughFrom, " +
			"if(b.insuranceVatPercent is null or b.insuranceVatPercent='',0.00,b.insuranceVatPercent) as vatPercent, "+
			"if(b.insuranceVatDescr is null or b.insuranceVatDescr='','',b.insuranceVatDescr) as vatDescr, "+
			"c.expGl as payglCode,  "+
			"if(b.baseInsuranceValue is null,'',b.baseInsuranceValue) as baseInsuranceValue, " +
			"if(b.billingCurrency is null,'',b.billingCurrency) as billingCurrency, "+
			"if(b.vendorStoragePerMonth is null,'',b.vendorStoragePerMonth) as vendorStoragePerMonth, "+
			"if(b.vendorStorageVatDescr is null,'',b.vendorStorageVatDescr) as vendorStorageVatDescr, "+
			"if(b.vendorStorageVatPercent is null,'',b.vendorStorageVatPercent) as vendorStorageVatPercent, "+
			"if(b.billingInsurancePayableCurrency is null,'',b.billingInsurancePayableCurrency) as vendorBillingCurency, "+
			"if(b.vendorCode is null,'',b.vendorCode) as vendorCode1,if(b.vendorName is null,'',b.vendorName) as vendorName1, "+
			"if(b.insuranceCharge1 is null,'',b.insuranceCharge1) as insuranceCharge1, "+
			"if(c.contractCurrency is null,'',c.contractCurrency) as contractCurrency, "+
			"if(c.payableContractCurrency is null,'',c.payableContractCurrency) as payableContractCurrency, "+
			"if(b.contractCurrency is null,'',b.contractCurrency) as billingContractCurrency, "+
			"if(b.payableContractCurrency is null,'',b.payableContractCurrency) as billingPayableContractCurrency, "+
			"if(b.currency is null,'',b.currency) as currency, "+
			"if(b.insuranceBuyRate is null,'',b.insuranceBuyRate) as insuranceBuyRate, "+
			"if(c.VATExclude = '' || c.VATExclude is null,false,c.VATExclude) as VATExclude, "+
			"if(b.insurancePerMonth is null,'',b.insurancePerMonth) as insurancePerMonth, " +
			"if(b.insuranceValueEntitle is null,'',b.insuranceValueEntitle) as insuranceValueEntitle, " +
			"if(b.payableRate is null,'',b.payableRate) as payableRate, " +
			"if(b.vendorCodeVatCode is null,'',b.vendorCodeVatCode) as vendorCodeVatCode, " +
			"if(ct.dmmInsurancePolicy is true,true,false) as dmmInsurancePolicy ,  " +
			"if(b.bookingAgentVatCode is null,'',b.bookingAgentVatCode) as bookingAgentVatCode,  " +
			"concat( if(c.quantity2preset is null,'',c.quantity2preset),'#',if(c.priceType is null,'',c.priceType),'#',if(c.priceFormula is null,'',c.priceFormula),'#',if(c.divideMultiply is null,'',c.divideMultiply ),'#',if(c.perValue is null,'',c.perValue),'#',if(c.minimum is null ,'', c.minimum),'#',if(c.payablePreset is null,'',c.payablePreset),'#',if(c.payablePriceType is null,'',c.payablePriceType),'#',if(c.expensePrice is null,'',c.expensePrice),'#', if(c.payableContractCurrency is null,'',c.payableContractCurrency) ) AS formulaText ,  "+
			"s.id as serviceOrderId ,  "+
			"if(costElement is null,'',costElement) as costElement,  " +
            "if(scostElementDescription is null,'',scostElementDescription) as costElementDescription,   "+
            "if(b.storageMeasurement is null ,'' ,b.storageMeasurement) as storageMeasurement "+
			"from billing b left outer join serviceorder s on b.id=s.id and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD') and s.status is not null " +
			"left outer join trackingstatus t on b.id= t.id  " +
			"left outer join charges c on b.contract=c.contract and b.insuranceCharge=c.charge and  c.corpID='"+sessionCorpID+"' " +
			"left outer join contract ct on b.contract=ct.contract  and ct.corpID='"+sessionCorpID+"' "    +
			"inner join partnerprivate p on ( case b.storageBillingParty when '1' then b.billToCode " +
			"when '2' then b.billTo2Code when '3' then b.privatePartyBillingCode else b.billToCode end)=p.partnerCode AND p.corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')    " +
			"where "+condition+" " + conditionInEx+conditionBookingCodeType+" " +
			"and (DATE_FORMAT(b.billThrough,'%Y-%m-%d') <= ('"+beginDate+" ')) " +
			"AND (DATE_FORMAT(b.billThrough,'%Y-%m-%d') >= ('"+endDate+"')) " +
			"and (b.storageOut is null OR b.storageOut >= (last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH))))  and s.corpID='"+sessionCorpID+"' " +
			"and (b.insuranceCharge <> null || b.insuranceCharge <> '') " +
			"and s.shipNumber like '"+shipNumbers+"%' and concat(s.lastName,' ' , s.firstName) like '%"+firstName.replaceAll(",","")+"%'  and b.cycle like '"+cycles+"%' and b.insuranceCharge like '"+charges+"%' "+
			"and ct.contractType='"+radioContractType+"' group by b.shipNumber)"+
			"order by shipNumber";
			
			List list= getSession().createSQLQuery(query)
			.addScalar("shipNumber", Hibernate.STRING) 
			.addScalar("registrationNumber", Hibernate.STRING) 
			.addScalar("firstName", Hibernate.STRING) 
			.addScalar("name", Hibernate.STRING) 
			.addScalar("billToCode", Hibernate.STRING) 
			.addScalar("billToName", Hibernate.STRING) 
			.addScalar("storagePerMonth", Hibernate.STRING) 
			.addScalar("charge", Hibernate.STRING) 
			.addScalar("job", Hibernate.STRING) 
			.addScalar("t", Hibernate.STRING) 
			.addScalar("postGrate", Hibernate.BIG_DECIMAL) 
			.addScalar("onHand", Hibernate.BIG_DECIMAL) 
			.addScalar("createdBy", Hibernate.STRING) 
			.addScalar("updatedBy", Hibernate.STRING) 
			.addScalar("createdOn", Hibernate.DATE) 
			.addScalar("updatedOn", Hibernate.DATE) 
			.addScalar("insuranceHas", Hibernate.STRING) 
			.addScalar("insuranceRate", Hibernate.BIG_DECIMAL) 
			.addScalar("insuranceValueActual", Hibernate.BIG_DECIMAL) 
			.addScalar("perValue", Hibernate.BIG_DECIMAL) 
			.addScalar("storageBillingGroup", Hibernate.STRING) 
			.addScalar("cycle", Hibernate.BIG_DECIMAL) 
			.addScalar("contract", Hibernate.STRING)
			.addScalar("minimum", Hibernate.BIG_DECIMAL)
			.addScalar("quantityItemRevised", Hibernate.STRING)
			.addScalar("quantityRevisedPreset", Hibernate.STRING)
			.addScalar("perItem", Hibernate.STRING)
			.addScalar("toDates", Hibernate.STRING)
			.addScalar("dateDiff", Hibernate.INTEGER)
			.addScalar("glCode", Hibernate.STRING)
			.addScalar("divideMultiply", Hibernate.STRING)
			.addScalar("description", Hibernate.STRING)
			.addScalar("useDiscount", Hibernate.STRING)
			.addScalar("pricePre", Hibernate.DOUBLE)
			.addScalar("sitdiscount", Hibernate.BIG_DECIMAL)
			.addScalar("billToAuthority", Hibernate.STRING)
			.addScalar("deliveryShipper", Hibernate.DATE)
			.addScalar("deliveryA", Hibernate.DATE)
			.addScalar("billThroughFrom", Hibernate.DATE)
			.addScalar("vatPercent", Hibernate.STRING)
			.addScalar("vatDescr", Hibernate.STRING)
			.addScalar("payglCode",  Hibernate.STRING)
			.addScalar("baseInsuranceValue",  Hibernate.BIG_DECIMAL)
			.addScalar("billingCurrency",  Hibernate.STRING)
			.addScalar("vendorStoragePerMonth",  Hibernate.STRING)
			.addScalar("vendorStorageVatDescr",  Hibernate.STRING)
			.addScalar("vendorStorageVatPercent",  Hibernate.BIG_DECIMAL)
			.addScalar("vendorBillingCurency",  Hibernate.STRING)
			.addScalar("vendorCode1",  Hibernate.STRING)
			.addScalar("vendorName1",  Hibernate.STRING)
			.addScalar("insuranceCharge1",  Hibernate.STRING)
			.addScalar("contractCurrency",  Hibernate.STRING)
			.addScalar("payableContractCurrency",  Hibernate.STRING)
			.addScalar("billingContractCurrency",  Hibernate.STRING)
			.addScalar("billingPayableContractCurrency",  Hibernate.STRING)
			.addScalar("currency",  Hibernate.STRING)
			.addScalar("insuranceBuyRate",  Hibernate.STRING)
			.addScalar("VATExclude",  Hibernate.BOOLEAN)
			.addScalar("insurancePerMonth",  Hibernate.BIG_DECIMAL)
			.addScalar("insuranceValueEntitle",  Hibernate.BIG_DECIMAL)
			.addScalar("payableRate",  Hibernate.BIG_DECIMAL)
			.addScalar("vendorCodeVatCode",  Hibernate.STRING) 
			.addScalar("dmmInsurancePolicy",  Hibernate.BOOLEAN) 
			.addScalar("bookingAgentVatCode",  Hibernate.STRING) 
			.addScalar("formulaText",  Hibernate.STRING) 
			.addScalar("serviceOrderId",  Hibernate.LONG) 
			.addScalar("costElement",  Hibernate.STRING) 
			.addScalar("costElementDescription",  Hibernate.STRING)
			.addScalar("storageMeasurement", Hibernate.STRING)
			.list();
			
			return list;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	
		}
		return null;
	}
	public List previewStorageBillingNetworkAgentSearch(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String shipNumbers,String firstName,String lastName,String billsTo,String cycles,String charges,String sessionCorpID,String invoiceBillingDate, String radioContractType){
		try {
			List list = getStorageBillingQuery(sessionCorpID,condition,conditionInEx,conditionBookingCodeType,beginDate,endDate,shipNumbers,firstName,cycles,charges,radioContractType);
			
			List result= new ArrayList(); 
			BillingDTO billingDTO=new BillingDTO();
			if(list.size()>0){
				try{
					Iterator it=list.iterator();
					while(it.hasNext()){ 
						Object []obj=(Object[]) it.next();
						if(obj[7].toString().equalsIgnoreCase("nocharge")){
						}else{
							billingDTO=new BillingDTO(); 
							billingDTO.setShipNumber(obj[0]);
							billingDTO.setRegistrationNumber(obj[1]);
							billingDTO.setFirstName(obj[2]);
							billingDTO.setLastName(obj[3]);
							billingDTO.setBillToCode(obj[4]);
							billingDTO.setBillToName(obj[5]);
							if(obj[6]==null){
								billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
							}else{
								billingDTO.setStoragePerMonth(obj[6]);
							}
					
							billingDTO.setCharge(obj[7]);
							billingDTO.setJob(obj[8]);
							billingDTO.setWording(obj[9]);
							billingDTO.setPostGrate(obj[10]);
							try{
								billingDTO.setOnHand(obj[11]);
							}catch(Exception ex){
								 ex.printStackTrace();
							}
							billingDTO.setCreatedBy(obj[12]);
							billingDTO.setUpdatedBy(obj[13]);
							billingDTO.setCreatedOn(obj[14]);
							billingDTO.setUpdatedOn(obj[15]);
							billingDTO.setInsuranceHas(obj[16]);
							billingDTO.setInsuranceRate(obj[17]);
							billingDTO.setInsuranceValueActual(obj[18]);
							billingDTO.setPerValue(obj[19]);
							billingDTO.setStorageBillingParty(obj[20]);//storageBillingGroup
							billingDTO.setCycle(obj[21]);
							billingDTO.setContract(obj[22]);
							billingDTO.setMinimum(obj[23]); 
							billingDTO.setQuantityItemRevised(obj[24]);
							if(obj[25].toString().startsWith("0")|| obj[25].toString().startsWith("1")
							   ||obj[25].toString().startsWith("2")|| obj[25].toString().startsWith("3")
							   || obj[25].toString().startsWith("4")|| obj[25].toString().startsWith("5")
							   ||obj[25].toString().startsWith("6")|| obj[25].toString().startsWith("7")
							   ||obj[25].toString().startsWith("8")|| obj[25].toString().startsWith("9"))
							{
							  billingDTO.setQuantityRevisedPreset(obj[25]);
							}else{
								billingDTO.setQuantityRevisedPreset(("0.00"));	
								}   
							billingDTO.setPerItem(obj[26]);
							billingDTO.setToDates(obj[27]);
							billingDTO.setDateDiff(obj[28]);
							billingDTO.setGlCode(obj[29]);
							billingDTO.setDivideMultiply(obj[30]);
							billingDTO.setDescription(obj[31]);
							billingDTO.setUseDiscount(obj[32]);
							billingDTO.setPricePre(obj[33]);
							billingDTO.setSitdiscount(obj[34]);
							billingDTO.setBillToAuthority(obj[35]);
							billingDTO.setDeliveryShipper(obj[36]);
							billingDTO.setDeliveryA(obj[37]);
							billingDTO.setBillThroughFrom(obj[38]);
							billingDTO.setVatPercent(obj[39]); 
							billingDTO.setVatDescr(obj[40]); 
							if(obj[41]!=null){
								billingDTO.setPayglCode(obj[41]);
							}else{
								billingDTO.setPayglCode("");	
							}
							if(obj[42]!=null){
								billingDTO.setBaseInsuranceValue(obj[42]);
							}else{
								billingDTO.setBaseInsuranceValue("");	
							}
							
							billingDTO.setBillingCurrency(obj[43]);
							billingDTO.setVendorStoragePerMonth(obj[44]);
							billingDTO.setVendorStorageVatDescr(obj[45]);
							billingDTO.setVendorStorageVatPercent(obj[46]);
							billingDTO.setVendorBillingCurency(obj[47]);
							billingDTO.setVendorCode1(obj[48]);
							billingDTO.setVendorName1(obj[49]);
							billingDTO.setInsuranceCharge1(obj[50]); 
							billingDTO.setContractCurrency(obj[51]);
							billingDTO.setPayableContractCurrency(obj[52]);
							billingDTO.setBillingContractCurrency(obj[53]);
							billingDTO.setBillingPayableContractCurrency(obj[54]);
							billingDTO.setCurrency(obj[55]);
							billingDTO.setInsuranceBuyRate(obj[56]);
							billingDTO.setVATExclude(Boolean.parseBoolean(obj[57].toString()));
							billingDTO.setInsurancePerMonth(obj[58]);
							billingDTO.setInsuranceValueEntitle(obj[59]);
							billingDTO.setPayableRate(obj[60]);
							billingDTO.setVendorCodeVatCode(obj[61]);
							billingDTO.setDmmInsurancePolicy(Boolean.parseBoolean(obj[62].toString()));
							billingDTO.setBookingAgentVatCode(obj[63]);
							if(obj[64]!=null && (!(obj[64].toString().trim().equals("#########")))){
							billingDTO.setFormulaText(obj[64]);
							}else{
							billingDTO.setFormulaText("");	
							}
							billingDTO.setServiceOrderId(obj[65]);
							billingDTO.setCostElement(obj[66]);
							billingDTO.setCostElementDescription(obj[67]);
							if(obj[68]!=null){
								billingDTO.setStorageMeasurement(obj[68]);
							}else{
								billingDTO.setStorageMeasurement("");	
							}
							
							result.add(billingDTO);
					}
				}
			}catch(Exception e){
				 e.printStackTrace();
			}
}else{
			billingDTO.setShipNumber("");
			billingDTO.setRegistrationNumber("");
			billingDTO.setFirstName("");
			billingDTO.setLastName("");
			billingDTO.setBillToCode("");
			billingDTO.setBillToName("");
			billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
			billingDTO.setCharge("");
			billingDTO.setJob("");
			billingDTO.setWording("");
			billingDTO.setPostGrate("");
			billingDTO.setOnHand("");
			billingDTO.setCreatedBy("");
			billingDTO.setUpdatedBy("");
			billingDTO.setCreatedOn("");
			billingDTO.setUpdatedOn("");
			billingDTO.setInsuranceHas("");
			billingDTO.setInsuranceRate("");
			billingDTO.setInsuranceValueActual("");
			billingDTO.setPerValue("");
			billingDTO.setStorageBillingParty("");//storageBillingGroup
			billingDTO.setCycle("");
			billingDTO.setContract("Contract Mismatch");
			billingDTO.setMinimum(""); 
			billingDTO.setQuantityItemRevised("");
			billingDTO.setQuantityRevisedPreset("");
			billingDTO.setPerItem("");
			billingDTO.setToDates("");
			billingDTO.setDateDiff("");
			billingDTO.setGlCode("");
			billingDTO.setDivideMultiply("");
			billingDTO.setForChecking("y");
			billingDTO.setDescription("");
			billingDTO.setUseDiscount("");
			billingDTO.setPricePre("");
			billingDTO.setSitdiscount("");
			billingDTO.setBillToAuthority("");
			billingDTO.setDeliveryShipper("");
			billingDTO.setDeliveryA("");
			billingDTO.setBillThroughFrom("");
			billingDTO.setVatPercent("");
			billingDTO.setVatDescr(""); 
			billingDTO.setPayglCode("");
			billingDTO.setBaseInsuranceValue("");
			
			billingDTO.setBillingCurrency("");
			billingDTO.setVendorStoragePerMonth("");
			billingDTO.setVendorStorageVatDescr("");
			billingDTO.setVendorStorageVatPercent("");
			billingDTO.setVendorBillingCurency("");
			billingDTO.setVendorCode1("");
			billingDTO.setVendorName1("");
			billingDTO.setInsuranceCharge1("");
			billingDTO.setContractCurrency("");
			billingDTO.setPayableContractCurrency("");
			billingDTO.setBillingContractCurrency("");
			billingDTO.setBillingPayableContractCurrency("");
			billingDTO.setCurrency("");
			billingDTO.setInsuranceBuyRate("");
			billingDTO.setVATExclude(false);
			billingDTO.setInsurancePerMonth("");
			billingDTO.setInsuranceValueEntitle("");
			billingDTO.setPayableRate("");
			billingDTO.setVendorCodeVatCode("");
			billingDTO.setDmmInsurancePolicy(true);
			billingDTO.setBookingAgentVatCode("");
			billingDTO.setFormulaText("");
			billingDTO.setServiceOrderId("");
			billingDTO.setCostElement("");
			billingDTO.setCostElementDescription("");
			billingDTO.setStorageMeasurement("");
			result.add(billingDTO);
}
return result;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}	
         return null;
	}
	// search SSCW
	
	public List previewStroageBillingSearch(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String shipNumbers,String firstName,String lastName,String billsTo,String cycles,String charges,String sessionCorpID,String invoiceBillingDate)
	{
		try {
			List list = searchStorageBilling( invoiceBillingDate, sessionCorpID, condition, conditionInEx, conditionBookingCodeType, beginDate, endDate,shipNumbers,firstName,lastName,cycles,charges);
			
					List result= new ArrayList(); 
					BillingDTO billingDTO=new BillingDTO();
					boolean isNoInsurance=false; 
					if(list.size()>0){
						try{
							Iterator it=list.iterator();
							while(it.hasNext()){ 
								Object []obj=(Object[]) it.next();
								if(obj[7].toString().equalsIgnoreCase("nocharge")){
								}else{
										
									billingDTO=new BillingDTO(); 
									billingDTO.setShipNumber(obj[0]);
									billingDTO.setRegistrationNumber(obj[1]);
									billingDTO.setFirstName(obj[2]);
									billingDTO.setLastName(obj[3]);
									billingDTO.setBillToCode(obj[4]);
									billingDTO.setBillToName(obj[5]);
									if(obj[6]==null){
										billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
									}else{
										billingDTO.setStoragePerMonth(obj[6]);
									}
									billingDTO.setCharge(obj[7]);
									billingDTO.setJob(obj[8]);
									billingDTO.setWording(obj[9]);
									billingDTO.setPostGrate(obj[10]);
									try{
										billingDTO.setOnHand(obj[11]);
									}catch(Exception ex){
										ex.printStackTrace();
									}
									billingDTO.setCreatedBy(obj[12]);
									billingDTO.setUpdatedBy(obj[13]);
									billingDTO.setCreatedOn(obj[14]);
									billingDTO.setUpdatedOn(obj[15]);
									billingDTO.setInsuranceHas(obj[16]);
									billingDTO.setInsuranceRate(obj[17]);
									billingDTO.setInsuranceValueActual(obj[18]);
									billingDTO.setPerValue(obj[19]);
									billingDTO.setStorageBillingParty(obj[20]);//storageBillingGroup
									billingDTO.setCycle(obj[21]);
									billingDTO.setContract(obj[22]);
									billingDTO.setMinimum(obj[23]); 
									billingDTO.setQuantityItemRevised(obj[24]);
									if(obj[25].toString().startsWith("0")|| obj[25].toString().startsWith("1")
									   ||obj[25].toString().startsWith("2")|| obj[25].toString().startsWith("3")
									   || obj[25].toString().startsWith("4")|| obj[25].toString().startsWith("5")
									   ||obj[25].toString().startsWith("6")|| obj[25].toString().startsWith("7")
									   ||obj[25].toString().startsWith("8")|| obj[25].toString().startsWith("9"))
									{
									  billingDTO.setQuantityRevisedPreset(obj[25]);
									}else{
									    
										billingDTO.setQuantityRevisedPreset(("0.00"));
									}
									billingDTO.setPerItem(obj[26]);
									billingDTO.setToDates(obj[27]);
									billingDTO.setDateDiff(obj[28]);
									billingDTO.setGlCode(obj[29]);
									billingDTO.setDivideMultiply(obj[30]);
									billingDTO.setDescription(obj[31]);
									billingDTO.setUseDiscount(obj[32]);
									billingDTO.setPricePre(obj[33]);
									billingDTO.setSitdiscount(obj[34]);
									billingDTO.setBillToAuthority(obj[35]);
									billingDTO.setDeliveryShipper(obj[36]);
									billingDTO.setDeliveryA(obj[37]);
									billingDTO.setBillThroughFrom(obj[38]);
									if(obj[39]!=null){
										billingDTO.setPayglCode(obj[39]);
									}else{
										billingDTO.setPayglCode("");	
									}
									if(obj[40]!=null){
										billingDTO.setCompanyDivision(obj[40]);
									}else{
										billingDTO.setCompanyDivision("");	
									}
									if(obj[41]!=null){
										billingDTO.setStorageMeasurement(obj[41]);
									}
									else{
										billingDTO.setStorageMeasurement("");
									}
									if(obj[42]!=null){
										billingDTO.setBaseInsuranceValue(obj[42]);
									}else{
										billingDTO.setBaseInsuranceValue("");
									}
									if(obj[43]!=null){
										billingDTO.setVendorStoragePerMonth(obj[43]);
									}else{
										billingDTO.setVendorStoragePerMonth("");
									}
									if(obj[44]!=null){
										billingDTO.setInsuranceBuyRate(obj[44]);
									}else{
										billingDTO.setInsuranceBuyRate("");
									}
									if(obj[45]!=null){
										billingDTO.setInsurancePerMonth(obj[45]);
									}else{
										billingDTO.setInsurancePerMonth("");
									}
									if(obj[46]!=null){
										billingDTO.setInsuranceValueEntitle(obj[46]);
									}else{
										billingDTO.setInsuranceValueEntitle("");
									}
									if(obj[47]!=null){
										billingDTO.setPayableRate(obj[47]);
									}else{
										billingDTO.setPayableRate("");
									}
									if(obj[48]!=null){
										billingDTO.setVendorCode1(obj[48]);
									}else{
										billingDTO.setVendorCode1("");
									}
									if(obj[49]!=null){
										billingDTO.setVendorName1(obj[49]);
									}else{
										billingDTO.setVendorName1("");
									}
									result.add(billingDTO);
									}
							}
							}catch(Exception ex){
								ex.printStackTrace();
						}
					}else{
						billingDTO.setShipNumber("");
						billingDTO.setRegistrationNumber("");
						billingDTO.setFirstName("");
						billingDTO.setLastName("");
						billingDTO.setBillToCode("");
						billingDTO.setBillToName("");
						billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
						billingDTO.setCharge("");
						billingDTO.setJob("");
						billingDTO.setWording("");
						billingDTO.setPostGrate("");
						billingDTO.setOnHand("");
						billingDTO.setCreatedBy("");
						billingDTO.setUpdatedBy("");
						billingDTO.setCreatedOn("");
						billingDTO.setUpdatedOn("");
						billingDTO.setInsuranceHas("");
						billingDTO.setInsuranceRate("");
						billingDTO.setInsuranceValueActual("");
						billingDTO.setPerValue("");
						billingDTO.setStorageBillingParty("");//storageBillingGroup
						billingDTO.setCycle("");
						billingDTO.setContract("Contract Mismatch");
						billingDTO.setMinimum(""); 
						billingDTO.setQuantityItemRevised("");
						billingDTO.setQuantityRevisedPreset("");
						billingDTO.setPerItem("");
						billingDTO.setToDates("");
						billingDTO.setDateDiff("");
						billingDTO.setGlCode("");
						billingDTO.setDivideMultiply("");
						billingDTO.setForChecking("y");
						billingDTO.setDescription("");
						billingDTO.setUseDiscount("");
						billingDTO.setPricePre("");
						billingDTO.setSitdiscount("");
						billingDTO.setBillToAuthority("");
						billingDTO.setDeliveryShipper("");
						billingDTO.setDeliveryA("");
						billingDTO.setBillThroughFrom("");
						billingDTO.setPayglCode("");
						billingDTO.setCompanyDivision("");
						billingDTO.setStorageMeasurement("");
						result.add(billingDTO);
					}
					return result;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	public int updateBillingFromStorage( String billThrough,String shipNumber, String loginUser, Date updatedOn)
	{
		try {
			return getHibernateTemplate().bulkUpdate("update Billing set billthrough='"+billThrough+"', updatedBy='"+loginUser+"', updatedOn=now() where shipNumber=?", shipNumber);
		} catch (DataAccessException e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return 0;
	}
	public List<Billing> getShipNumbers(String beginDate, String endDate,String sessionCorpID)
	{
		try {
			return getHibernateTemplate().find("from Billing where  (DATE_FORMAT(b.billThrough,'%Y-%m-%d') <= ('"+beginDate+" ')) AND (DATE_FORMAT(b.billThrough,'%Y-%m-%d') >= ('"+endDate+"'))   and s.corpID='"+sessionCorpID+"'");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return null;
	}
	
	
	 public List findBillContractAccountList(String historicalContractsValue,String billToCode, String job, Date createdon, String corpID,String companyDivision , String bookingAgentCode){
		  
		 try {

			 List list3 = new ArrayList();
			 List agentparentList= new ArrayList();
			 List companydivisionBookingAgentList= new ArrayList();
				String bookingAgentSet="";
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		        User user = (User)auth.getPrincipal();
		        Map filterMap=user.getFilterMap(); 
		    	List userList=new ArrayList();
		    	try {
				if(filterMap.containsKey("serviceOrderBillToCodeFilter") || filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
					if(filterMap.containsKey("serviceOrderBillToCodeFilter") && bookingAgentCode!=null && (!(bookingAgentCode.trim().equalsIgnoreCase("null"))) && (!(bookingAgentCode.trim().equalsIgnoreCase("")))){
					list3.add("'" + bookingAgentCode + "'");	
					}
					if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
					
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
		    		list3.add("'" + code + "'");
		    		
		    		}
		    			
		    	} 
				}
					bookingAgentSet = list3.toString().replace("[","").replace("]", "");
			    	agentparentList=this.getSession().createSQLQuery("select distinct agentparent from partnerpublic where partnercode in ("+ bookingAgentSet + ") and agentparent is not null and agentparent!='' ").list();
			    	Iterator agentparentListIterator= agentparentList.iterator();
			    	while (agentparentListIterator.hasNext()) {
			    		String code=agentparentListIterator.next().toString();
			    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
			    		list3.add("'" + code + "'");
			    		} 
			    	}
			    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
			    	companydivisionBookingAgentList=this.getSession().createSQLQuery("select distinct bookingAgentCode from companydivision where bookingAgentCode is not null and bookingAgentCode !='' and  corpid in (select corpid from companydivision where bookingAgentCode in ("+ bookingAgentSet + "))").list(); 
			    	Iterator companydivisionBookingAgentListIterator= companydivisionBookingAgentList.iterator();
			    	while (companydivisionBookingAgentListIterator.hasNext()) {
			    		String code1=companydivisionBookingAgentListIterator.next().toString();
			    		if(code1!=null && (!(code1.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code1.trim().equalsIgnoreCase("")))){ 
			    		list3.add("'" + code1 + "'");
			    		} 
			    	}
			    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
				}
		    	}catch (Exception e) {
		    		 e.printStackTrace();
		    		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  logger.error("Error executing query "+ e.getStackTrace()[0]);

				}
		    	
		    if ((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter"))  && bookingAgentSet != null && (!(bookingAgentSet.trim().equals(""))) ) {
				 if(createdon !=null){
				    Date date1Str = createdon;
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
					if(historicalContractsValue!=null && historicalContractsValue.equals("true")){
						 return getSession().createSQLQuery(("select distinct c.contract from contract c, contractaccount ca, contractaccount ca1 where ca.accountcode = '"+billToCode+"' and ca1.accountcode in ("+ bookingAgentSet + ") and ca.contract = ca1.contract and ca.corpID = ca1.corpID and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='') and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision=''))  order by 1")).list();
					}else{
					     return getSession().createSQLQuery(("select distinct c.contract from contract c, contractaccount ca, contractaccount ca1 where ca.accountcode = '"+billToCode+"' and ca1.accountcode in ("+ bookingAgentSet + ") and ca.contract = ca1.contract and ca.corpID = ca1.corpID and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='') and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((date_format(c.ending,'%Y-%m-%d') >= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.ending is null )) and ((date_format(c.begin,'%Y-%m-%d') <= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.begin is null )) order by 1")).list();
					}
					
				   }else{
					   if(historicalContractsValue!=null && historicalContractsValue.equals("true")){
						   return getSession().createSQLQuery(("select distinct c.contract from contract c, contractaccount ca, contractaccount ca1 where ca.accountcode = '"+billToCode+"' and ca1.accountcode in ("+ bookingAgentSet + ") and ca.contract = ca1.contract and ca.corpID = ca1.corpID and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='')  and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision=''))   order by 1")).list();
					   }else{
					       return getSession().createSQLQuery(("select distinct c.contract from contract c, contractaccount ca, contractaccount ca1 where ca.accountcode = '"+billToCode+"' and ca1.accountcode in ("+ bookingAgentSet + ") and ca.contract = ca1.contract and ca.corpID = ca1.corpID and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='')  and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((c.ending is null or date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ( (c.begin is null or date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d'))) order by 1")).list();
					   }
				   }
			    
		    }else{
			 if(createdon !=null){
			    Date date1Str = createdon;
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
				if(historicalContractsValue!=null && historicalContractsValue.equals("true")){
					 return getSession().createSQLQuery(("select  distinct c.contract from contract c where (c.jobtype like '%"+job+"%' or c.jobtype='') and c.corpID = '"+corpID+"' and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision=''))  and c.contract not in (select contract from contractaccount where corpID='"+corpID+"' and accounttype='Account') union select distinct c.contract from contract c, contractaccount ca where ca.accountcode = '"+billToCode+"' and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='') and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision=''))  order by 1")).list();
				}else{
				     return getSession().createSQLQuery(("select  distinct c.contract from contract c where (c.jobtype like '%"+job+"%' or c.jobtype='') and c.corpID = '"+corpID+"' and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((date_format(c.ending,'%Y-%m-%d') >= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.ending is null )) and ((date_format(c.begin,'%Y-%m-%d') <= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.begin is null )) and c.contract not in (select contract from contractaccount where corpID='"+corpID+"' and accounttype='Account') union select distinct c.contract from contract c, contractaccount ca where ca.accountcode = '"+billToCode+"' and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='') and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((date_format(c.ending,'%Y-%m-%d') >= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.ending is null )) and ((date_format(c.begin,'%Y-%m-%d') <= if('"+formatedDate1+"'='null','2001-01-01','"+formatedDate1+"')) or (c.begin is null )) order by 1")).list();
				}

			   }else{
				   if(historicalContractsValue!=null && historicalContractsValue.equals("true")){
					   return getSession().createSQLQuery(("select  distinct c.contract from contract c where (c.jobtype like '%"+job+"%' or c.jobtype='') and c.corpID = '"+corpID+"' and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision=''))  and c.contract not in (select contract from contractaccount where corpID='"+corpID+"' and accounttype='Account') union select distinct c.contract from contract c, contractaccount ca where ca.accountcode = '"+billToCode+"' and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='')  and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision=''))   order by 1")).list();
				   }else{
				       return getSession().createSQLQuery(("select  distinct c.contract from contract c where (c.jobtype like '%"+job+"%' or c.jobtype='') and c.corpID = '"+corpID+"' and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((c.ending is null or date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ((c.begin is null or date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d'))) and c.contract not in (select contract from contractaccount where corpID='"+corpID+"' and accounttype='Account') union select distinct c.contract from contract c, contractaccount ca where ca.accountcode = '"+billToCode+"' and ca.contract = c.contract and ca.corpID = c.corpID and ca.corpID = '"+corpID+"' and (c.jobtype like '%"+job+"%' or c.jobtype='')  and (c.companyDivision='"+companyDivision+"' or (c.companyDivision is null or c.companyDivision='')) and ((c.ending is null or date_format(c.ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))) and ( (c.begin is null or date_format(c.begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d'))) order by 1")).list();
				   }
			   }
		    }
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	
		}
		return null;
		}

	 public List findReviewStatus(String shipNumber){
		 try {
			return getHibernateTemplate().find("select reviewStatus from WorkTicket where reviewStatus='UnBilled' and targetActual not in ('C','F','R') and shipNumber=?",shipNumber);
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		}
	 return null;
	 }
	

	public List findDateDifference(Date invoiceBillingDate, Date dateFrom) {
		
		try {
			Date date1Str = invoiceBillingDate;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			
			Date date2Str = dateFrom;
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate2 = new StringBuilder( dateformatYYYYMMDD2.format( date2Str ) );
			return this.getSession().createSQLQuery("select datediff('"+formatedDate1+"', '"+formatedDate2+"') from dual").list();
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return null;
	}
	
	public List priceContract(String contract){
		try {
			return getHibernateTemplate().find("select concat(trnsDisc,'#',discount,'#',packDisc,'#',sitDiscount,'#',otherDiscount,'#',payMethod,'#',billingInstructionCode) from Contract where contract=?",contract);
		} catch (Exception e) {
			getSessionCorpID();
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	
	public List findCompanyCode(String corpID) {
		try {
			return getHibernateTemplate().find("select companyCode from SystemDefault where corpID=?",corpID);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
	}
	
	public List findBookingAgent(String corpID) {
		try {
			return getHibernateTemplate().find("select bookingAgentCode  from CompanyDivision  where corpID=?",corpID);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	
		}
		return null;
	}

	public List getPrimaryBillToCode(Long id,String corpID) {
		
		try {
			return getHibernateTemplate().find("select billToCode,billToName  from Billing where corpID='"+corpID+"' and id=?",id);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(corpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	public void updatePresentCount(Double countTotalNumber, String sessionCorpID, Long id) {
	
		try {
			List maxid=getHibernateTemplate().find("select max(id) from JobStatus");
			getHibernateTemplate().bulkUpdate("update JobStatus set presentCount="+countTotalNumber+" where corpID='"+sessionCorpID+"' and id='"+id+"'");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		}	
		
	}

	public List getPresentCount(String sessionCorpID) {
		try {
			List maxid=getHibernateTemplate().find("select max(id) from JobStatus");
			List pCount=new ArrayList();
			if(maxid!=null)
			{
				pCount= getHibernateTemplate().find("select presentCount from JobStatus where corpID='"+sessionCorpID+"' and id='"+maxid.get(0)+"'");
				
			}
			return pCount;
		} catch (Exception e) {
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	public List getTotalCount(String sessionCorpID) {
		try {
			List maxid=getHibernateTemplate().find("select max(id) from JobStatus");
			List tCount=new ArrayList();
			if(maxid!=null)
			{
				tCount= getHibernateTemplate().find("select totalCount from JobStatus where corpID='"+sessionCorpID+"' and id='"+maxid.get(0)+"'");
				
			}
			return tCount;
		} catch (Exception e) {
	    	 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  
		}
		return null;
	}

	public List getMaxId(String sessionCorpID) {
		try {
			return getHibernateTemplate().find("select max(id) from JobStatus where corpID='"+sessionCorpID+"'");
		} catch (Exception e) {
	    	 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	
		}
		return null;
	}
	
	public List autoStorageBillingProc(String payMentod, String partnerCode, String corpId){
		try {
			return this.getSession().createSQLQuery("select storagebillinggroup, if(isPrivateParty=true,'yes','no') from partner Where partnercode = '"+partnerCode+"' and corpid in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') ").list();
		} catch (Exception e) {
	    	 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	   
		}
		return null;
	}
	
	
	public List getDateAdd(Date invoiceBillingDate) {
		try {
			Date date1Str = invoiceBillingDate;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			
			List addDays = this.getSession().createSQLQuery("SELECT CAST(DATE_ADD('"+formatedDate1+"', INTERVAL 1 DAY) AS CHAR)").list();
			return addDays;
		} catch (Exception e) {
	    	 e.printStackTrace();
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	

	public List findByBillToCode(String billToCode, String sessionCorpID) {
		try {
			return getHibernateTemplate().find("select lastName from PartnerPublic  where partnerCode = '"+billToCode+"'");
		} catch (Exception e) {
	    	 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		}
		return null;
	}

	public List findByBillTo2Code(String billTo2Code, String sessionCorpID) {
		// TODO Auto-generated method stub
		try {
			return getHibernateTemplate().find("select lastName from PartnerPublic  where partnerCode = '"+billTo2Code+"'");
		} catch (Exception e) {
	    	 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		}
		return null;
	}

	public List findByPrivateBillToCode(String privatePartyBillingCode, String sessionCorpID) {
		// TODO Auto-generated method stub
		try {
			return getHibernateTemplate().find("select lastName from PartnerPublic  where partnerCode = '"+privatePartyBillingCode+"'");
		} catch (Exception e) {
	    	 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  
		}
		return null;
	}

	public List findByVendorCode(String vendorCode, String sessionCorpID) {
		// TODO Auto-generated method stub
		try {
			return getHibernateTemplate().find("select lastName from PartnerPublic  where partnerCode = '"+vendorCode+"'");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return null;
	}

	public void updateStorageMeasurementWT(String storageMeasurement, String shipNumber) {
		try {
			List desc = this.getSession().createSQLQuery("select description from refmaster where code='"+storageMeasurement+"' and parameter = 'MEASURE'").addScalar("description",Hibernate.TEXT).list();
			if(!desc.isEmpty()){
				getHibernateTemplate().bulkUpdate("update WorkTicket set storageMeasurement='"+desc.get(0)+"' where shipNumber=?", shipNumber);
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 
		}
	}

	public List accRefComDivision(String companyDivision, String billToCode) {
		try {
			return getSession().createSQLQuery("select * from partner p, partneraccountref r where p.partnercode = r.partnercode and isagent is true and r.companydivision = '"+companyDivision+"'  and p.partnercode = '"+billToCode+"'").list();
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));

		}
		return null;
	}
	
	public List contractBillingInstCode(String contract)
	{
		try {
			return getHibernateTemplate().find("select billingInstructionCode from Contract where contract='"+contract+"'");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
		}
		return null;
	}

	public List getChargePerValue(String charge, String billingContract) {
		try {
			return getHibernateTemplate().find("select perValue From Charges where charge='"+charge+"' AND contract='"+billingContract+"'");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	
	 public List searchByBillingID(String billToAuthority ,String billToReference,String billingId,String billingIdOut){
		 try {
			getHibernateTemplate().setMaxResults(500);
			 return getHibernateTemplate().find("from Billing where billToAuthority like '"+billToAuthority.replaceAll("'", "''").replaceAll(":", "''")+"%' and  billToReference like '"+billToReference.replaceAll("'", "''").replaceAll(":", "''")+"%' and billingId like '"+billingId.replaceAll("'", "''").replaceAll(":", "''")+"%' and billingIdOut like '"+billingIdOut.replaceAll("'", "''").replaceAll(":", "''")+"%'");
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		return null;
		}

	
	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	 public List previewStorageBillingForNetworkAgentCount(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID, String radioContractType){
			try {
				List list = getStorageBillingQuery(sessionCorpID,condition,conditionInEx,conditionBookingCodeType,beginDate,endDate,"","","","",radioContractType);

				List result= new ArrayList(); 
							BillingDTO billingDTO=new BillingDTO();
							if(list.size()>0){
								try{
									Iterator it=list.iterator();
									while(it.hasNext()){ 
										Object []obj=(Object[]) it.next();
										if(obj[7].toString().equalsIgnoreCase("nocharge")){
										}else{
											billingDTO=new BillingDTO(); 
											billingDTO.setShipNumber(obj[0]);
											billingDTO.setRegistrationNumber(obj[1]);
											billingDTO.setFirstName(obj[2]);
											billingDTO.setLastName(obj[3]);
											billingDTO.setBillToCode(obj[4]);
											billingDTO.setBillToName(obj[5]);
											if(obj[6]==null){
												billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
											}else{
												billingDTO.setStoragePerMonth(obj[6]);
											}
											
											billingDTO.setCharge(obj[7]);
											billingDTO.setJob(obj[8]);
											billingDTO.setWording(obj[9]);
											billingDTO.setPostGrate(obj[10]);
											try{
												billingDTO.setOnHand(obj[11]);
											}catch(Exception ex){
												 ex.printStackTrace();
											}
											billingDTO.setCreatedBy(obj[12]);
											billingDTO.setUpdatedBy(obj[13]);
											billingDTO.setCreatedOn(obj[14]);
											billingDTO.setUpdatedOn(obj[15]);
											billingDTO.setInsuranceHas(obj[16]);
											billingDTO.setInsuranceRate(obj[17]);
											billingDTO.setInsuranceValueActual(obj[18]);
											billingDTO.setPerValue(obj[19]);
											billingDTO.setStorageBillingParty(obj[20]);//storageBillingGroup
											billingDTO.setCycle(obj[21]);
											billingDTO.setContract(obj[22]);
											billingDTO.setMinimum(obj[23]); 
											billingDTO.setQuantityItemRevised(obj[24]);
											if(obj[25].toString().startsWith("0")|| obj[25].toString().startsWith("1")
											   ||obj[25].toString().startsWith("2")|| obj[25].toString().startsWith("3")
											   || obj[25].toString().startsWith("4")|| obj[25].toString().startsWith("5")
											   ||obj[25].toString().startsWith("6")|| obj[25].toString().startsWith("7")
											   ||obj[25].toString().startsWith("8")|| obj[25].toString().startsWith("9"))
											{
											  billingDTO.setQuantityRevisedPreset(obj[25]);
											}else{
												billingDTO.setQuantityRevisedPreset("0.00");	
											}   
											billingDTO.setPerItem(obj[26]);
											billingDTO.setToDates(obj[27]);
											billingDTO.setDateDiff(obj[28]);
											billingDTO.setGlCode(obj[29]);
											billingDTO.setDivideMultiply(obj[30]);
											billingDTO.setDescription(obj[31]);
											billingDTO.setUseDiscount(obj[32]);
											billingDTO.setPricePre(obj[33]);
											billingDTO.setSitdiscount(obj[34]);
											billingDTO.setBillToAuthority(obj[35]);
											billingDTO.setDeliveryShipper(obj[36]);
											billingDTO.setDeliveryA(obj[37]);
											billingDTO.setBillThroughFrom(obj[38]);
											billingDTO.setVatPercent(obj[39]); 
											billingDTO.setVatDescr(obj[40]); 
											if(obj[41]!=null){
												billingDTO.setPayglCode(obj[41]);
											}else{
												billingDTO.setPayglCode("");	
											}
											if(obj[42]!=null){
												billingDTO.setBaseInsuranceValue(obj[42]);
											}else{
												billingDTO.setBaseInsuranceValue("");	
											}
											
											billingDTO.setBillingCurrency(obj[43]);
											billingDTO.setVendorStoragePerMonth(obj[44]);
											billingDTO.setVendorStorageVatDescr(obj[45]);
											billingDTO.setVendorStorageVatPercent(obj[46]);
											billingDTO.setVendorBillingCurency(obj[47]);
											billingDTO.setVendorCode1(obj[48]);
											billingDTO.setVendorName1(obj[49]);
											billingDTO.setInsuranceCharge1(obj[50]); 
											billingDTO.setContractCurrency(obj[51]);
											billingDTO.setPayableContractCurrency(obj[52]);
											billingDTO.setBillingContractCurrency(obj[53]);
											billingDTO.setBillingPayableContractCurrency(obj[54]);
											billingDTO.setCurrency(obj[55]);
											billingDTO.setInsuranceBuyRate(obj[56]);
											billingDTO.setVATExclude(Boolean.parseBoolean(obj[57].toString()));
											billingDTO.setInsurancePerMonth(obj[58]);
											billingDTO.setInsuranceValueEntitle(obj[59]);
											billingDTO.setPayableRate(obj[60]);
											billingDTO.setVendorCodeVatCode(obj[61]);
											billingDTO.setDmmInsurancePolicy(Boolean.parseBoolean(obj[62].toString()));
											billingDTO.setBookingAgentVatCode(obj[63]);
											if(obj[64]!=null && (!(obj[64].toString().trim().equals("#########")))){
											billingDTO.setFormulaText(obj[64]);
											}else{
												billingDTO.setFormulaText("");	
											}
											billingDTO.setServiceOrderId(obj[65]);
											billingDTO.setCostElement(obj[66]);
											billingDTO.setCostElementDescription(obj[67]);
											if(obj[68]!=null){
												billingDTO.setStorageMeasurement(obj[68]);
											}
											else{
												billingDTO.setStorageMeasurement("");
											}
											result.add(billingDTO);
									}
								}
							}catch(Exception ex){}
						}else{
							billingDTO.setShipNumber("");
							billingDTO.setRegistrationNumber("");
							billingDTO.setFirstName("");
							billingDTO.setLastName("");
							billingDTO.setBillToCode("");
							billingDTO.setBillToName("");
							billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
							billingDTO.setCharge("");
							billingDTO.setJob("");
							billingDTO.setWording("");
							billingDTO.setPostGrate("");
							billingDTO.setOnHand("");
							billingDTO.setCreatedBy("");
							billingDTO.setUpdatedBy("");
							billingDTO.setCreatedOn("");
							billingDTO.setUpdatedOn("");
							billingDTO.setInsuranceHas("");
							billingDTO.setInsuranceRate("");
							billingDTO.setInsuranceValueActual("");
							billingDTO.setPerValue("");
							billingDTO.setStorageBillingParty("");//storageBillingGroup
							billingDTO.setCycle("");
							billingDTO.setContract("Contract Mismatch");
							billingDTO.setMinimum(""); 
							billingDTO.setQuantityItemRevised("");
							billingDTO.setQuantityRevisedPreset("");
							billingDTO.setPerItem("");
							billingDTO.setToDates("");
							billingDTO.setDateDiff("");
							billingDTO.setGlCode("");
							billingDTO.setDivideMultiply("");
							billingDTO.setForChecking("y");
							billingDTO.setDescription("");
							billingDTO.setUseDiscount("");
							billingDTO.setPricePre("");
							billingDTO.setSitdiscount("");
							billingDTO.setBillToAuthority("");
							billingDTO.setDeliveryShipper("");
							billingDTO.setDeliveryA("");
							billingDTO.setBillThroughFrom("");
							billingDTO.setVatPercent("");
							billingDTO.setVatDescr(""); 
							billingDTO.setPayglCode("");
							billingDTO.setBaseInsuranceValue("");
							
							billingDTO.setBillingCurrency("");
							billingDTO.setVendorStoragePerMonth("");
							billingDTO.setVendorStorageVatDescr("");
							billingDTO.setVendorStorageVatPercent("");
							billingDTO.setVendorBillingCurency("");
							billingDTO.setVendorCode1("");
							billingDTO.setVendorName1("");
							billingDTO.setInsuranceCharge1("");
							billingDTO.setContractCurrency("");
							billingDTO.setPayableContractCurrency("");
							billingDTO.setBillingContractCurrency("");
							billingDTO.setBillingPayableContractCurrency("");
							billingDTO.setCurrency("");
							billingDTO.setInsuranceBuyRate("");
							billingDTO.setVATExclude(false);
							billingDTO.setInsurancePerMonth("");
							billingDTO.setInsuranceValueEntitle("");
							billingDTO.setPayableRate("");
							billingDTO.setVendorCodeVatCode("");
							billingDTO.setDmmInsurancePolicy(true);
							billingDTO.setBookingAgentVatCode("");
							billingDTO.setFormulaText("");
							billingDTO.setServiceOrderId("");
							billingDTO.setCostElement("");
							billingDTO.setCostElementDescription("");
							billingDTO.setStorageMeasurement("");
							result.add(billingDTO);
						}
						return result;
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	
			}
			return null;
		}
	public List previewStorageBillingForNetworkAgent(String beginDate, String endDate, String condition, String conditionInEx, String conditionBookingCodeType, String invoiceBillingDate, String sessionCorpID, String radioContractType){
		try {
			List list = getStorageBillingQuery(sessionCorpID,condition,conditionInEx,conditionBookingCodeType,beginDate,endDate,"","","","",radioContractType);

			List result= new ArrayList(); 
						BillingDTO billingDTO=new BillingDTO();
						if(list.size()>0){
							try{
								Iterator it=list.iterator();
								while(it.hasNext()){ 
									Object []obj=(Object[]) it.next();
									if(obj[7].toString().equalsIgnoreCase("nocharge")){
									}else{
										billingDTO=new BillingDTO(); 
										billingDTO.setShipNumber(obj[0]);
										billingDTO.setRegistrationNumber(obj[1]);
										billingDTO.setFirstName(obj[2]);
										billingDTO.setLastName(obj[3]);
										billingDTO.setBillToCode(obj[4]);
										billingDTO.setBillToName(obj[5]);
										if(obj[6]==null){
											billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
										}else{
											billingDTO.setStoragePerMonth(obj[6]);
										}
									
										billingDTO.setCharge(obj[7]);
										billingDTO.setJob(obj[8]);
										billingDTO.setWording(obj[9]);
										billingDTO.setPostGrate(obj[10]);
										try{
											billingDTO.setOnHand(obj[11]);
										}catch(Exception ex){
											 ex.printStackTrace();
										}
										billingDTO.setCreatedBy(obj[12]);
										billingDTO.setUpdatedBy(obj[13]);
										billingDTO.setCreatedOn(obj[14]);
										billingDTO.setUpdatedOn(obj[15]);
										billingDTO.setInsuranceHas(obj[16]);
										billingDTO.setInsuranceRate(obj[17]);
										billingDTO.setInsuranceValueActual(obj[18]);
										billingDTO.setPerValue(obj[19]);
										billingDTO.setStorageBillingParty(obj[20]);//storageBillingGroup
										billingDTO.setCycle(obj[21]);
										billingDTO.setContract(obj[22]);
										billingDTO.setMinimum(obj[23]); 
										billingDTO.setQuantityItemRevised(obj[24]);
										if(obj[25].toString().startsWith("0")|| obj[25].toString().startsWith("1")
										   ||obj[25].toString().startsWith("2")|| obj[25].toString().startsWith("3")
										   || obj[25].toString().startsWith("4")|| obj[25].toString().startsWith("5")
										   ||obj[25].toString().startsWith("6")|| obj[25].toString().startsWith("7")
										   ||obj[25].toString().startsWith("8")|| obj[25].toString().startsWith("9"))
										{
										  billingDTO.setQuantityRevisedPreset(obj[25]);
										}else{
											billingDTO.setQuantityRevisedPreset("0.00");	
										}   
										billingDTO.setPerItem(obj[26]);
										billingDTO.setToDates(obj[27]);
										billingDTO.setDateDiff(obj[28]);
										billingDTO.setGlCode(obj[29]);
										billingDTO.setDivideMultiply(obj[30]);
										billingDTO.setDescription(obj[31]);
										billingDTO.setUseDiscount(obj[32]);
										billingDTO.setPricePre(obj[33]);
										billingDTO.setSitdiscount(obj[34]);
										billingDTO.setBillToAuthority(obj[35]);
										billingDTO.setDeliveryShipper(obj[36]);
										billingDTO.setDeliveryA(obj[37]);
										billingDTO.setBillThroughFrom(obj[38]);
										billingDTO.setVatPercent(obj[39]); 
										billingDTO.setVatDescr(obj[40]); 
										if(obj[41]!=null){
											billingDTO.setPayglCode(obj[41]);
										}else{
											billingDTO.setPayglCode("");	
										}
										if(obj[42]!=null){
											billingDTO.setBaseInsuranceValue(obj[42]);
										}else{
											billingDTO.setBaseInsuranceValue("");	
										}
										
										billingDTO.setBillingCurrency(obj[43]);
										billingDTO.setVendorStoragePerMonth(obj[44]);
										billingDTO.setVendorStorageVatDescr(obj[45]);
										billingDTO.setVendorStorageVatPercent(obj[46]);
										billingDTO.setVendorBillingCurency(obj[47]);
										billingDTO.setVendorCode1(obj[48]);
										billingDTO.setVendorName1(obj[49]);
										billingDTO.setInsuranceCharge1(obj[50]); 
										billingDTO.setContractCurrency(obj[51]);
										billingDTO.setPayableContractCurrency(obj[52]);
										billingDTO.setBillingContractCurrency(obj[53]);
										billingDTO.setBillingPayableContractCurrency(obj[54]);
										billingDTO.setCurrency(obj[55]);
										billingDTO.setInsuranceBuyRate(obj[56]);
										billingDTO.setVATExclude(Boolean.parseBoolean(obj[57].toString()));
										billingDTO.setInsurancePerMonth(obj[58]);
										billingDTO.setInsuranceValueEntitle(obj[59]);
										billingDTO.setPayableRate(obj[60]);
										billingDTO.setVendorCodeVatCode(obj[61]);
										billingDTO.setDmmInsurancePolicy(Boolean.parseBoolean(obj[62].toString()));
										billingDTO.setBookingAgentVatCode(obj[63]);
										if(obj[64]!=null && (!(obj[64].toString().trim().equals("#########")))){
										billingDTO.setFormulaText(obj[64]);
										}else{
											billingDTO.setFormulaText("");	
										}
										billingDTO.setServiceOrderId(obj[65]);
										billingDTO.setCostElement(obj[66]);
										billingDTO.setCostElementDescription(obj[67]);
										if(obj[68]!=null){
											billingDTO.setStorageMeasurement(obj[68]);
										}
										else{
											billingDTO.setStorageMeasurement("");
										}
										result.add(billingDTO);
								}
							}
						}catch(Exception ex){
							 ex.printStackTrace();
						}
					}else{
						billingDTO.setShipNumber("");
						billingDTO.setRegistrationNumber("");
						billingDTO.setFirstName("");
						billingDTO.setLastName("");
						billingDTO.setBillToCode("");
						billingDTO.setBillToName("");
						billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
						billingDTO.setCharge("");
						billingDTO.setJob("");
						billingDTO.setWording("");
						billingDTO.setPostGrate("");
						billingDTO.setOnHand("");
						billingDTO.setCreatedBy("");
						billingDTO.setUpdatedBy("");
						billingDTO.setCreatedOn("");
						billingDTO.setUpdatedOn("");
						billingDTO.setInsuranceHas("");
						billingDTO.setInsuranceRate("");
						billingDTO.setInsuranceValueActual("");
						billingDTO.setPerValue("");
						billingDTO.setStorageBillingParty("");//storageBillingGroup
						billingDTO.setCycle("");
						billingDTO.setContract("Contract Mismatch");
						billingDTO.setMinimum(""); 
						billingDTO.setQuantityItemRevised("");
						billingDTO.setQuantityRevisedPreset("");
						billingDTO.setPerItem("");
						billingDTO.setToDates("");
						billingDTO.setDateDiff("");
						billingDTO.setGlCode("");
						billingDTO.setDivideMultiply("");
						billingDTO.setForChecking("y");
						billingDTO.setDescription("");
						billingDTO.setUseDiscount("");
						billingDTO.setPricePre("");
						billingDTO.setSitdiscount("");
						billingDTO.setBillToAuthority("");
						billingDTO.setDeliveryShipper("");
						billingDTO.setDeliveryA("");
						billingDTO.setBillThroughFrom("");
						billingDTO.setVatPercent("");
						billingDTO.setVatDescr(""); 
						billingDTO.setPayglCode("");
						billingDTO.setBaseInsuranceValue("");
						
						billingDTO.setBillingCurrency("");
						billingDTO.setVendorStoragePerMonth("");
						billingDTO.setVendorStorageVatDescr("");
						billingDTO.setVendorStorageVatPercent("");
						billingDTO.setVendorBillingCurency("");
						billingDTO.setVendorCode1("");
						billingDTO.setVendorName1("");
						billingDTO.setInsuranceCharge1("");
						billingDTO.setContractCurrency("");
						billingDTO.setPayableContractCurrency("");
						billingDTO.setBillingContractCurrency("");
						billingDTO.setBillingPayableContractCurrency("");
						billingDTO.setCurrency("");
						billingDTO.setInsuranceBuyRate("");
						billingDTO.setVATExclude(false);
						billingDTO.setInsurancePerMonth("");
						billingDTO.setInsuranceValueEntitle("");
						billingDTO.setPayableRate("");
						billingDTO.setVendorCodeVatCode("");
						billingDTO.setDmmInsurancePolicy(true);
						billingDTO.setBookingAgentVatCode("");
						billingDTO.setFormulaText("");
						billingDTO.setServiceOrderId("");
						billingDTO.setCostElement("");
						billingDTO.setCostElementDescription("");
						billingDTO.setStorageMeasurement("");
						result.add(billingDTO);
					}
					return result;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	
		}
		return null;
	}
	
	//FOR BOUR
	public List getVatStorageBillingQuery(String condition,String conditionInEx,String conditionBookingCodeType,String sessionCorpID,String beginDate,String endDate,String shipNumbers,String firstName,String cycles,String charges){
		try {
			String storageQuery="(select if( datediff(sysdate(), loada) < 91," +
			"concat('*',s.shipNumber), concat('''',s.shipNumber)) as shipNumber ," +
			"if(s.registrationNumber is null,'', s.registrationNumber) as registrationNumber, " +
			"s.firstName as firstName,concat(s.lastName,', ' ,s.firstName) as name," +
			"case when b.storageBillingParty='1' then b.billToCode  " +
			"when b.storageBillingParty='2' then b.billTo2Code " +
			"when b.storageBillingParty='3' then b.privatePartyBillingCode else b.billToCode end as billToCode," +
			"case when b.storageBillingParty='1' then b.billToName " +
			"when b.storageBillingParty='2' then b.billTo2Name " +
			"when b.storageBillingParty='3' then b.privatePartyBillingName else b.billToName end as billToName," +
			"(b.storagePerMonth * b.cycle)as storagePerMonth,if(b.charge is null,'', b.charge) as charge," +
			"if(s.job is null,'',s.job) as job, c.wording as t , if(b.postGrate is null,'',b.postGrate) as postGrate , " +
			"b.onHand  as onHand , if( b.createdBy is null,'', b.createdBy) as createdBy,if(b.updatedBy is null ,''," +
			"b.updatedBy) as updatedBy ,b.createdOn as createdOn ,b.updatedOn as updatedOn ,'storage' as insuranceHas ," +
			"if(b.insuranceRate is null ,'',b.insuranceRate) as insuranceRate," +
			"if(b.insuranceValueActual is null,'',b.insuranceValueActual) as insuranceValueActual," +
			"if(c.perValue is null,'', c.perValue) as perValue,p.storageBillingGroup as storageBillingGroup," +
			"b.cycle as cycle,if(b.contract is null,'', b.contract) as contract," +
			"if(c.minimum is null,'',c.minimum) as minimum ," +
			"if(c.quantityItemRevised is null ,'',c.quantityItemRevised) as quantityItemRevised ,  " +
			"if(c.quantityRevised='Preset2', if(c.quantityRevisedPreset is null || c.quantityRevisedPreset='','0',c.quantityRevisedPreset), " +
			"if(c.quantityRevisedSource='' || c.quantityRevisedSource is null,'0',c.quantityRevisedSource))  as quantityRevisedPreset ," +
			"if(c.perItem is null ,'',c.perItem) as perItem , " +
			"if(b.billingFrequency ='MONTH',DATE_ADD(b.billthrough , interval b.cycle MONTH),if(b.billingFrequency ='WEEK',DATE_ADD(b.billthrough , interval b.cycle WEEK),if(b.billingFrequency ='DAY',DATE_ADD(b.billthrough , interval b.cycle DAY),last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH))))) as toDates, " +
			"if(b.billingFrequency ='MONTH',(DATEDIFF(DATE_ADD(b.billthrough , interval b.cycle MONTH),b.billthrough )),if(b.billingFrequency ='WEEK',(DATEDIFF(DATE_ADD(b.billthrough , interval b.cycle WEEK), b.billthrough )),if(b.billingFrequency ='DAY',(DATEDIFF(DATE_ADD(b.billthrough , interval b.cycle DAY), b.billthrough )),(DATEDIFF(last_day((DATE_ADD(b.billthrough , interval b.cycle MONTH))),b.billthrough ))))) as dateDiff, " +
			"c.gl as glCode , c.divideMultiply as divideMultiply,c.description as description,c.useDiscount as useDiscount," +
			"c.pricePre as pricePre, b.sitdiscount as sitdiscount,b.billToAuthority as billToAuthority, " +
			"t.deliveryShipper as deliveryShipper, t.deliveryA as deliveryA, DATE_ADD(b.billthrough, interval 1 day) as billThroughFrom, " +
			"if(b.storageVatPercent is null or b.storageVatPercent='',0.00,b.storageVatPercent) as vatPercent,  "+
			"if(b.storageVatDescr is null or b.storageVatDescr='','',b.storageVatDescr) as vatDescr,  "+
			"c.expGl as payglCode,  "+
			"if(b.baseInsuranceValue is null,'',b.baseInsuranceValue) as baseInsuranceValue, " +
			"if(b.billingCurrency is null,'',b.billingCurrency) as billingCurrency, "+
			"if(b.vendorStoragePerMonth is null,'',b.vendorStoragePerMonth) as vendorStoragePerMonth, "+
			"if(b.vendorStorageVatDescr is null,'',b.vendorStorageVatDescr) as vendorStorageVatDescr, "+
			"if(b.vendorStorageVatPercent is null,'',b.vendorStorageVatPercent) as vendorStorageVatPercent, "+
			"if(b.vendorBillingCurency is null,'',b.vendorBillingCurency) as vendorBillingCurency, "+
			"if(b.vendorCode1 is null,'',b.vendorCode1) as vendorCode1,if(b.vendorName1 is null,'',b.vendorName1) as vendorName1, "+
			"if(b.insuranceCharge1 is null,'',b.insuranceCharge1) as insuranceCharge1, "+
			"if(c.VATExclude = '' || c.VATExclude is null,false,c.VATExclude) as VATExclude, "+
			"if(ct.contractType = '' || ct.contractType is null,'',ct.contractType) as contractType, "+
			"if(b.storageMeasurement is null ,'' ,b.storageMeasurement) as storageMeasurement,  "+	
			"concat( if(c.quantity2preset is null,'',c.quantity2preset),'#',if(c.priceType is null,'',c.priceType),'#',if(c.priceFormula is null,'',c.priceFormula),'#',if(c.divideMultiply is null,'',c.divideMultiply ),'#',if(c.perValue is null,'',c.perValue),'#',if(c.minimum is null ,'', c.minimum),'#',if(c.payablePreset is null,'',c.payablePreset),'#',if(c.payablePriceType is null,'',c.payablePriceType),'#',if(c.expensePrice is null,'',c.expensePrice),'#', if(c.payableContractCurrency is null,'',c.payableContractCurrency) ) AS formulaText,  "+
			"if(b.insuranceBuyRate is null,'',b.insuranceBuyRate) as insuranceBuyRate,  "+
			"if(b.vendorStoragePerMonth is null,'',b.vendorStoragePerMonth) as vendorStoragePerMonth,   "+
			"(b.storagePerMonth * b.cycle)as storagePerMonth, "+
			"if(b.insurancePerMonth is null,'',b.insurancePerMonth) as insurancePerMonth,  "+
			"if(b.payableRate is null,'',b.payableRate) as payableRate , "+
			"if(c.description is null,'',c.description) as chargesDescription  "+
			"from billing b left outer join serviceorder s on b.id=s.id and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD') and s.status is not null left outer join trackingstatus t on b.id= t.id " +
			"left outer join charges c on b.contract=c.contract and b.charge=c.charge and  c.corpID='"+sessionCorpID+"' "  +
			"left outer join contract ct on b.contract=ct.contract  and ct.corpID='"+sessionCorpID+"' "+
			"inner join partnerprivate p on ( case b.storageBillingParty when '1' then b.billToCode when '2' then b.billTo2Code " +
			"when '3' then b.privatePartyBillingCode else b.billToCode end)=p.partnerCode  AND p.corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')   " +
			"where "+condition+" " +conditionInEx+conditionBookingCodeType+" " +
			"and (DATE_FORMAT(b.billThrough,'%Y-%m-%d') <= ('"+beginDate+" ')) " +
			"AND (DATE_FORMAT(b.billThrough,'%Y-%m-%d') >= ('"+endDate+"')) " +
			//"and ct.contractType not in ('CMM','DMM') "+
			"and (b.storageOut is null OR ( b.storageOut >= (if(b.billingFrequency ='MONTH',DATE_ADD(b.billthrough , interval b.cycle MONTH),if(b.billingFrequency ='WEEK',DATE_ADD(b.billthrough , interval b.cycle WEEK),if(b.billingFrequency ='DAY',DATE_ADD(b.billthrough , interval b.cycle DAY),last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)))))))) and s.corpID='"+sessionCorpID+"' "+
			"and s.shipNumber like '"+shipNumbers+"%' and concat(s.lastName,' ',s.firstName) like '%"+firstName.replaceAll(",","")+"%'  and b.cycle like '"+cycles+"%' and b.charge like '"+charges+"%' group by b.shipNumber)" + 
			
" UNION (select if( datediff(sysdate(), loada) < 91,concat('*',s.shipNumber), " +
			"concat('''',s.shipNumber)) as shipNumber ,if(s.registrationNumber is null,'', s.registrationNumber) as registrationNumber, " +
			"s.firstName as firstName ,concat(s.lastName,', ' ,s.firstName) as name," +
			"case when b.storageBillingParty='1' then b.billToCode " +
			"when b.storageBillingParty='2' then b.billTo2Code " +
			"when b.storageBillingParty='3' then b.privatePartyBillingCode else b.billToCode end as billToCode," +
			"case when b.storageBillingParty='1' then b.billToName " +
			"when b.storageBillingParty='2' then b.billTo2Name " +
			"when b.storageBillingParty='3' then b.privatePartyBillingName else b.billToName  end as billToName," +
			"(b.insurancePerMonth * b.cycle) as storagePerMonth," +
			"if(b.insuranceCharge is null || b.insuranceCharge='','nocharge', b.insuranceCharge ) as charge," +
			"if(s.job is null,'',s.job) as job,c.wording as t , if(b.postGrate is null,'',b.postGrate) as postGrate , " +
			"b.onHand  as onHand , if( b.createdBy is null,'', b.createdBy) as createdBy," +
			"if(b.updatedBy is null ,'',b.updatedBy) as updatedBy ,b.createdOn as createdOn ,b.updatedOn as updatedOn ," +
			"if(b.insuranceHas is null ,'' ,b.insuranceHas) as insuranceHas ," +
			"if(b.insuranceRate is null,'',b.insuranceRate) as insuranceRate," +
			"if(b.insuranceValueActual is null ,'' , b.insuranceValueActual) as insuranceValueActual," +
			"if(c.perValue is null ,'',c.perValue) as perValue,p.storageBillingGroup as storageBillingGroup," +
			"b.cycle as cycle, if(b.contract is null,'', b.contract) as contract," +
			"if(c.minimum is null,'',c.minimum) as minimum , " +
			"if(c.quantityItemRevised is null ,'',c.quantityItemRevised) as quantityItemRevised , " +
			"if(c.quantityRevised='Preset2', if(c.quantityRevisedPreset is null || c.quantityRevisedPreset='','0'," +
			"c.quantityRevisedPreset), if(c.quantityRevisedSource='' || c.quantityRevisedSource is null,'0'," +
			"c.quantityRevisedSource)) as quantityRevisedPreset , if(c.perItem is null ,'',c.perItem) as perItem ,  " +
			"if(b.billingFrequency ='MONTH',DATE_ADD(b.billthrough , interval b.cycle MONTH),if(b.billingFrequency ='WEEK',DATE_ADD(b.billthrough , interval b.cycle WEEK),if(b.billingFrequency ='DAY',DATE_ADD(b.billthrough , interval b.cycle DAY),last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH))))) as toDates, " +
			"if(b.billingFrequency ='MONTH',(DATEDIFF(DATE_ADD(b.billthrough , interval b.cycle MONTH),b.billthrough )),if(b.billingFrequency ='WEEK',(DATEDIFF(DATE_ADD(b.billthrough , interval b.cycle WEEK), b.billthrough )),if(b.billingFrequency ='DAY',(DATEDIFF(DATE_ADD(b.billthrough , interval b.cycle DAY), b.billthrough )),(DATEDIFF(last_day((DATE_ADD(b.billthrough , interval b.cycle MONTH))),b.billthrough ))))) as dateDiff, " +
			"c.gl as glCode , c.divideMultiply as divideMultiply,  c.description as description," +
			"c.useDiscount as useDiscount,c.pricePre as pricePre, b.sitdiscount as sitdiscount," +
			"b.billToAuthority as billToAuthority, t.deliveryShipper as deliveryShipper, t.deliveryA as deliveryA, " +
			"DATE_ADD(b.billthrough, interval 1 day) as billThroughFrom, " +
			"if(b.insuranceVatPercent is null or b.insuranceVatPercent='',0.00,b.insuranceVatPercent) as vatPercent, "+
			"if(b.insuranceVatDescr is null or b.insuranceVatDescr='','',b.insuranceVatDescr) as vatDescr, "+
			"c.expGl as payglCode,  "+
			"if(b.baseInsuranceValue is null,'',b.baseInsuranceValue) as baseInsuranceValue, " +
			"if(b.billingCurrency is null,'',b.billingCurrency) as billingCurrency, "+
			"if(b.vendorStoragePerMonth is null,'',b.vendorStoragePerMonth) as vendorStoragePerMonth, "+
			"if(b.vendorCodeVatCode is null,'',b.vendorCodeVatCode) as vendorStorageVatDescr, "+
			" 0.00 as vendorStorageVatPercent, "+
			"if(b.vendorBillingCurency is null,'',b.vendorBillingCurency) as vendorBillingCurency, "+
			"if(b.vendorCode is null,'',b.vendorCode) as vendorCode1,if(b.vendorName is null,'',b.vendorName) as vendorName1, "+
			"if(b.insuranceCharge1 is null,'',b.insuranceCharge1) as insuranceCharge1, "+
			"if(c.VATExclude = '' || c.VATExclude is null,false,c.VATExclude) as VATExclude, "+
			"if(ct.contractType = '' || ct.contractType is null,'',ct.contractType) as contractType, "+
			"if(b.storageMeasurement is null ,'' ,b.storageMeasurement) as storageMeasurement, "+
			"concat( if(c.quantity2preset is null,'',c.quantity2preset),'#',if(c.priceType is null,'',c.priceType),'#',if(c.priceFormula is null,'',c.priceFormula),'#',if(c.divideMultiply is null,'',c.divideMultiply ),'#',if(c.perValue is null,'',c.perValue),'#',if(c.minimum is null ,'', c.minimum),'#',if(c.payablePreset is null,'',c.payablePreset),'#',if(c.payablePriceType is null,'',c.payablePriceType),'#',if(c.expensePrice is null,'',c.expensePrice),'#', if(c.payableContractCurrency is null,'',c.payableContractCurrency) ) AS formulaText,   "+
			"if(b.insuranceBuyRate is null,'',b.insuranceBuyRate) as insuranceBuyRate,  "+
			"if(b.vendorStoragePerMonth is null,'',b.vendorStoragePerMonth) as vendorStoragePerMonth,   "+
			"(b.insurancePerMonth * b.cycle)as storagePerMonth, "+
			"if(b.insurancePerMonth is null,'',b.insurancePerMonth) as insurancePerMonth,  "+
			"if(b.payableRate is null,'',b.payableRate) as payableRate, "+
			"if(c.description is null,'',c.description) as chargesDescription  "+
			"from billing b left outer join serviceorder s on b.id=s.id and s.status not in ('CLSD','CNCL','DWND','DWNLD','HOLD') and s.status is not null " +
			"left outer join trackingstatus t on b.id= t.id  " +
			"left outer join charges c on b.contract=c.contract and b.insuranceCharge=c.charge and  c.corpID='"+sessionCorpID+"' " +
			"left outer join contract ct on b.contract=ct.contract  and ct.corpID='"+sessionCorpID+"' "+
			"inner join partnerprivate p on ( case b.storageBillingParty when '1' then b.billToCode " +
			"when '2' then b.billTo2Code when '3' then b.privatePartyBillingCode else b.billToCode end)=p.partnerCode AND p.corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')    " +
			"where "+condition+" " + conditionInEx+conditionBookingCodeType+" " +
			"and (DATE_FORMAT(b.billThrough,'%Y-%m-%d') <= ('"+beginDate+" ')) " +
			"AND (DATE_FORMAT(b.billThrough,'%Y-%m-%d') >= ('"+endDate+"')) " +
			//"and ct.contractType not in ('CMM','DMM') "+
			"and (b.storageOut is null OR ( b.storageOut >= (if(b.billingFrequency ='MONTH',DATE_ADD(b.billthrough , interval b.cycle MONTH),if(b.billingFrequency ='WEEK',DATE_ADD(b.billthrough , interval b.cycle WEEK),if(b.billingFrequency ='DAY',DATE_ADD(b.billthrough , interval b.cycle DAY),last_day(DATE_ADD(b.billthrough , interval b.cycle MONTH)))))))) and s.corpID='"+sessionCorpID+"' " +
			"and (b.insuranceCharge <> null || b.insuranceCharge <> '') " +
			"and s.shipNumber like '"+shipNumbers+"%' and concat(s.lastName,' ',s.firstName) like '%"+firstName.replaceAll(",","")+"%'  and b.cycle like '"+cycles+"%' and b.insuranceCharge like '"+charges+"%' group by b.shipNumber) " +
			" order by shipNumber";

List list= getSession().createSQLQuery(storageQuery) 
			.addScalar("shipNumber", Hibernate.STRING) 
			.addScalar("registrationNumber", Hibernate.STRING) 
			.addScalar("firstName", Hibernate.STRING) 
			.addScalar("name", Hibernate.STRING) 
			.addScalar("billToCode", Hibernate.STRING) 
			.addScalar("billToName", Hibernate.STRING) 
			.addScalar("storagePerMonth", Hibernate.STRING) 
			.addScalar("charge", Hibernate.STRING) 
			.addScalar("job", Hibernate.STRING) 
			.addScalar("t", Hibernate.STRING) 
			.addScalar("postGrate", Hibernate.BIG_DECIMAL) 
			.addScalar("onHand", Hibernate.BIG_DECIMAL) 
			.addScalar("createdBy", Hibernate.STRING) 
			.addScalar("updatedBy", Hibernate.STRING) 
			.addScalar("createdOn", Hibernate.DATE) 
			.addScalar("updatedOn", Hibernate.DATE) 
			.addScalar("insuranceHas", Hibernate.STRING) 
			.addScalar("insuranceRate", Hibernate.BIG_DECIMAL) 
			.addScalar("insuranceValueActual", Hibernate.BIG_DECIMAL) 
			.addScalar("perValue", Hibernate.BIG_DECIMAL) 
			.addScalar("storageBillingGroup", Hibernate.STRING) 
			.addScalar("cycle", Hibernate.BIG_DECIMAL) 
			.addScalar("contract", Hibernate.STRING)
			.addScalar("minimum", Hibernate.BIG_DECIMAL)
			.addScalar("quantityItemRevised", Hibernate.STRING)
			.addScalar("quantityRevisedPreset", Hibernate.STRING)
			.addScalar("perItem", Hibernate.STRING)
			.addScalar("toDates", Hibernate.STRING)
			.addScalar("dateDiff", Hibernate.INTEGER)
			.addScalar("glCode", Hibernate.STRING)
			.addScalar("divideMultiply", Hibernate.STRING)
			.addScalar("description", Hibernate.STRING)
			.addScalar("useDiscount", Hibernate.STRING)
			.addScalar("pricePre", Hibernate.DOUBLE)
			.addScalar("sitdiscount", Hibernate.BIG_DECIMAL)
			.addScalar("billToAuthority", Hibernate.STRING)
			.addScalar("deliveryShipper", Hibernate.DATE)
			.addScalar("deliveryA", Hibernate.DATE)
			.addScalar("billThroughFrom", Hibernate.DATE)
			.addScalar("vatPercent", Hibernate.STRING)
			.addScalar("vatDescr", Hibernate.STRING)
			.addScalar("payglCode",  Hibernate.STRING)
			.addScalar("baseInsuranceValue",  Hibernate.BIG_DECIMAL)
			.addScalar("billingCurrency",  Hibernate.STRING)
			.addScalar("vendorStoragePerMonth",  Hibernate.STRING)
			.addScalar("vendorStorageVatDescr",  Hibernate.STRING)
			.addScalar("vendorStorageVatPercent",  Hibernate.BIG_DECIMAL)
			.addScalar("vendorBillingCurency",  Hibernate.STRING)
			.addScalar("vendorCode1",  Hibernate.STRING)
			.addScalar("vendorName1",  Hibernate.STRING)
			.addScalar("insuranceCharge1",  Hibernate.STRING)
			.addScalar("VATExclude",  Hibernate.BOOLEAN)
			.addScalar("contractType",  Hibernate.STRING)
			.addScalar("storageMeasurement", Hibernate.STRING)
			.addScalar("formulaText",  Hibernate.STRING)
			.addScalar("insuranceBuyRate",  Hibernate.STRING)
            .addScalar("vendorStoragePerMonth",  Hibernate.STRING)
            .addScalar("storagePerMonth", Hibernate.STRING) 
            .addScalar("insurancePerMonth",  Hibernate.BIG_DECIMAL)
            .addScalar("payableRate",  Hibernate.BIG_DECIMAL)
            .addScalar("chargesDescription", Hibernate.STRING)
			.list();

return list;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	public List previewVatStroageBilling(String beginDate, String endDate, String condition, String conditionInEx, String conditionBookingCodeType, String invoiceBillingDate, String sessionCorpID) {
		try {
			List list = getVatStorageBillingQuery(condition,conditionInEx,conditionBookingCodeType,sessionCorpID,beginDate,endDate,"","","","");
			
						List result= new ArrayList(); 
						BillingDTO billingDTO=new BillingDTO();
						
						Set<String> contractTypeSet = new HashSet<String>();
						contractTypeSet.add("CMM");contractTypeSet.add("DMM");
						
						if(list.size()>0){
							try{
								Iterator it=list.iterator();
								while(it.hasNext()){ 
									Object []obj=(Object[]) it.next();
									if(obj[7].toString().equalsIgnoreCase("nocharge") || contractTypeSet.contains(obj[52].toString())){
									}else{
										billingDTO=new BillingDTO(); 
										billingDTO.setShipNumber(obj[0]);
										billingDTO.setRegistrationNumber(obj[1]);
										billingDTO.setFirstName(obj[2]);
										billingDTO.setLastName(obj[3]);
										billingDTO.setBillToCode(obj[4]);
										billingDTO.setBillToName(obj[5]);
										if(obj[6]==null){
											billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
										}else{
											billingDTO.setStoragePerMonth(obj[6]);
										}
									
										billingDTO.setCharge(obj[7]);
										billingDTO.setJob(obj[8]);
										billingDTO.setWording(obj[9]);
										billingDTO.setPostGrate(obj[10]);
										try{
											billingDTO.setOnHand(obj[11]);
										}catch(Exception ex){
											 ex.printStackTrace();
										}
										billingDTO.setCreatedBy(obj[12]);
										billingDTO.setUpdatedBy(obj[13]);
										billingDTO.setCreatedOn(obj[14]);
										billingDTO.setUpdatedOn(obj[15]);
										billingDTO.setInsuranceHas(obj[16]);
										billingDTO.setInsuranceRate(obj[17]);
										billingDTO.setInsuranceValueActual(obj[18]);
										billingDTO.setPerValue(obj[19]);
										billingDTO.setStorageBillingParty(obj[20]);//storageBillingGroup
										billingDTO.setCycle(obj[21]);
										billingDTO.setContract(obj[22]);
										billingDTO.setMinimum(obj[23]); 
										billingDTO.setQuantityItemRevised(obj[24]);
										if(obj[25].toString().startsWith("0")|| obj[25].toString().startsWith("1")
										   ||obj[25].toString().startsWith("2")|| obj[25].toString().startsWith("3")
										   || obj[25].toString().startsWith("4")|| obj[25].toString().startsWith("5")
										   ||obj[25].toString().startsWith("6")|| obj[25].toString().startsWith("7")
										   ||obj[25].toString().startsWith("8")|| obj[25].toString().startsWith("9"))
										{
										  billingDTO.setQuantityRevisedPreset(obj[25]);
										}else{
										    
											billingDTO.setQuantityRevisedPreset(("0.00"));
										}   
										billingDTO.setPerItem(obj[26]);
										billingDTO.setToDates(obj[27]);
										billingDTO.setDateDiff(obj[28]);
										billingDTO.setGlCode(obj[29]);
										billingDTO.setDivideMultiply(obj[30]);
										billingDTO.setDescription(obj[31]);
										billingDTO.setUseDiscount(obj[32]);
										billingDTO.setPricePre(obj[33]);
										billingDTO.setSitdiscount(obj[34]);
										billingDTO.setBillToAuthority(obj[35]);
										billingDTO.setDeliveryShipper(obj[36]);
										billingDTO.setDeliveryA(obj[37]);
										billingDTO.setBillThroughFrom(obj[38]);
										billingDTO.setVatPercent(obj[39]); 
										billingDTO.setVatDescr(obj[40]); 
										if(obj[41]!=null){
											billingDTO.setPayglCode(obj[41]);
										}else{
											billingDTO.setPayglCode("");	
										}
										if(obj[42]!=null){
											billingDTO.setBaseInsuranceValue(obj[42]);
										}else{
											billingDTO.setBaseInsuranceValue("");	
										}
										
										billingDTO.setBillingCurrency(obj[43]);
										billingDTO.setVendorStoragePerMonth(obj[44]);
										billingDTO.setVendorStorageVatDescr(obj[45]);
										billingDTO.setVendorStorageVatPercent(obj[46]);
										billingDTO.setVendorBillingCurency(obj[47]);
										billingDTO.setVendorCode1(obj[48]);
										billingDTO.setVendorName1(obj[49]);
										billingDTO.setInsuranceCharge1(obj[50]);
										billingDTO.setVATExclude(Boolean.parseBoolean(obj[51].toString()));
										if(obj[53]!=null){
										billingDTO.setStorageMeasurement(obj[53]);
										}else{
											billingDTO.setStorageMeasurement("");
										}
										if(obj[54]!=null){
											billingDTO.setFormulaText(obj[54]);
											}else{
												billingDTO.setFormulaText("");
											}
										if(obj[55]!=null){
											billingDTO.setInsuranceBuyRate(obj[55]);
											}else{
												billingDTO.setInsuranceBuyRate("");
											}
										if(obj[56]!=null){
											billingDTO.setVendorStoragePerMonth(obj[56]);
											}else{
												billingDTO.setVendorStoragePerMonth("");
											}
										if(obj[57]!=null){
											billingDTO.setStoragePerMonth(obj[57]);
											}else{
												billingDTO.setStoragePerMonth("");
											}
										if(obj[58]!=null){
											billingDTO.setInsurancePerMonth(obj[58]);
											}else{
												billingDTO.setInsurancePerMonth(new BigDecimal("0.00"));
											}
										if(obj[59]!=null){
											billingDTO.setPayableRate(obj[59]);
											}else{
												billingDTO.setPayableRate(new BigDecimal("0.00"));
											}
										if(obj[60]!=null){
											billingDTO.setChargesDescription(obj[60]);
											}else{
												billingDTO.setChargesDescription("");
											} 
										result.add(billingDTO);
								}
							}
						}catch(Exception ex){}
					}else{
						billingDTO.setShipNumber("");
						billingDTO.setRegistrationNumber("");
						billingDTO.setFirstName("");
						billingDTO.setLastName("");
						billingDTO.setBillToCode("");
						billingDTO.setBillToName("");
						billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
						billingDTO.setCharge("");
						billingDTO.setJob("");
						billingDTO.setWording("");
						billingDTO.setPostGrate("");
						billingDTO.setOnHand("");
						billingDTO.setCreatedBy("");
						billingDTO.setUpdatedBy("");
						billingDTO.setCreatedOn("");
						billingDTO.setUpdatedOn("");
						billingDTO.setInsuranceHas("");
						billingDTO.setInsuranceRate("");
						billingDTO.setInsuranceValueActual("");
						billingDTO.setPerValue("");
						billingDTO.setStorageBillingParty("");//storageBillingGroup
						billingDTO.setCycle("");
						billingDTO.setContract("Contract Mismatch");
						billingDTO.setMinimum(""); 
						billingDTO.setQuantityItemRevised("");
						billingDTO.setQuantityRevisedPreset("");
						billingDTO.setPerItem("");
						billingDTO.setToDates("");
						billingDTO.setDateDiff("");
						billingDTO.setGlCode("");
						billingDTO.setDivideMultiply("");
						billingDTO.setForChecking("y");
						billingDTO.setDescription("");
						billingDTO.setUseDiscount("");
						billingDTO.setPricePre("");
						billingDTO.setSitdiscount("");
						billingDTO.setBillToAuthority("");
						billingDTO.setDeliveryShipper("");
						billingDTO.setDeliveryA("");
						billingDTO.setBillThroughFrom("");
						billingDTO.setVatPercent("");
						billingDTO.setVatDescr(""); 
						billingDTO.setPayglCode("");
						billingDTO.setBaseInsuranceValue("");
						
						billingDTO.setBillingCurrency("");
						billingDTO.setVendorStoragePerMonth("");
						billingDTO.setVendorStorageVatDescr("");
						billingDTO.setVendorStorageVatPercent("");
						billingDTO.setVendorBillingCurency("");
						billingDTO.setVendorCode1("");
						billingDTO.setVendorName1("");
						billingDTO.setInsuranceCharge1("");
						billingDTO.setVATExclude(false);
						billingDTO.setStorageMeasurement("");
						billingDTO.setFormulaText("");
						billingDTO.setInsuranceBuyRate("");
						billingDTO.setVendorStoragePerMonth("");
						billingDTO.setStoragePerMonth("");
						billingDTO.setInsurancePerMonth(new BigDecimal("0.00"));
						billingDTO.setPayableRate(new BigDecimal("0.00")); 
						billingDTO.setChargesDescription("");
						result.add(billingDTO);
					}
					return result;
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 
		}
		return null;
		}

	public List previewVatStroageBillingSearch(String beginDate, String endDate, String condition, String conditionInEx, String conditionBookingCodeType, String shipNumbers, String firstName, String lastName, String billsTo, String cycles, String charges, String sessionCorpID, String invoiceBillingDate) {
		List list = getVatStorageBillingQuery(condition,conditionInEx,conditionBookingCodeType,sessionCorpID,beginDate,endDate,shipNumbers,firstName,cycles,charges);
		
		List result= new ArrayList(); 
		BillingDTO billingDTO=new BillingDTO();
		
		Set<String> contractTypeSet = new HashSet<String>();
		contractTypeSet.add("CMM");contractTypeSet.add("DMM");
		
		if(list.size()>0){
			try{
				Iterator it=list.iterator();
				while(it.hasNext()){ 
					Object []obj=(Object[]) it.next();
					if(obj[7].toString().equalsIgnoreCase("nocharge") || contractTypeSet.contains(obj[52].toString())){
					}else{
						billingDTO=new BillingDTO(); 
						billingDTO.setShipNumber(obj[0]);
						billingDTO.setRegistrationNumber(obj[1]);
						billingDTO.setFirstName(obj[2]);
						billingDTO.setLastName(obj[3]);
						billingDTO.setBillToCode(obj[4]);
						billingDTO.setBillToName(obj[5]);
						if(obj[6]==null){
							billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
						}else{
							billingDTO.setStoragePerMonth(obj[6]);
						}
					
						billingDTO.setCharge(obj[7]);
						billingDTO.setJob(obj[8]);
						billingDTO.setWording(obj[9]);
						billingDTO.setPostGrate(obj[10]);
						try{
							billingDTO.setOnHand(obj[11]);
						}catch(Exception ex){
							 ex.printStackTrace();
						}
						billingDTO.setCreatedBy(obj[12]);
						billingDTO.setUpdatedBy(obj[13]);
						billingDTO.setCreatedOn(obj[14]);
						billingDTO.setUpdatedOn(obj[15]);
						billingDTO.setInsuranceHas(obj[16]);
						billingDTO.setInsuranceRate(obj[17]);
						billingDTO.setInsuranceValueActual(obj[18]);
						billingDTO.setPerValue(obj[19]);
						billingDTO.setStorageBillingParty(obj[20]);//storageBillingGroup
						billingDTO.setCycle(obj[21]);
						billingDTO.setContract(obj[22]);
						billingDTO.setMinimum(obj[23]); 
						billingDTO.setQuantityItemRevised(obj[24]);
						if(obj[25].toString().startsWith("0")|| obj[25].toString().startsWith("1")
						   ||obj[25].toString().startsWith("2")|| obj[25].toString().startsWith("3")
						   || obj[25].toString().startsWith("4")|| obj[25].toString().startsWith("5")
						   ||obj[25].toString().startsWith("6")|| obj[25].toString().startsWith("7")
						   ||obj[25].toString().startsWith("8")|| obj[25].toString().startsWith("9"))
						{
						  billingDTO.setQuantityRevisedPreset(obj[25]);
						}else{
						    
							billingDTO.setQuantityRevisedPreset(("0.00"));
						}   
						billingDTO.setPerItem(obj[26]);
						billingDTO.setToDates(obj[27]);
						billingDTO.setDateDiff(obj[28]);
						billingDTO.setGlCode(obj[29]);
						billingDTO.setDivideMultiply(obj[30]);
						billingDTO.setDescription(obj[31]);
						billingDTO.setUseDiscount(obj[32]);
						billingDTO.setPricePre(obj[33]);
						billingDTO.setSitdiscount(obj[34]);
						billingDTO.setBillToAuthority(obj[35]);
						billingDTO.setDeliveryShipper(obj[36]);
						billingDTO.setDeliveryA(obj[37]);
						billingDTO.setBillThroughFrom(obj[38]);
						billingDTO.setVatPercent(obj[39]); 
						billingDTO.setVatDescr(obj[40]); 
						if(obj[41]!=null){
							billingDTO.setPayglCode(obj[41]);
						}else{
							billingDTO.setPayglCode("");	
						}
						if(obj[42]!=null){
							billingDTO.setBaseInsuranceValue(obj[42]);
						}else{
							billingDTO.setBaseInsuranceValue("");	
						}
						
						billingDTO.setBillingCurrency(obj[43]);
						billingDTO.setVendorStoragePerMonth(obj[44]);
						billingDTO.setVendorStorageVatDescr(obj[45]);
						billingDTO.setVendorStorageVatPercent(obj[46]);
						billingDTO.setVendorBillingCurency(obj[47]);
						billingDTO.setVendorCode1(obj[48]);
						billingDTO.setVendorName1(obj[49]);
						billingDTO.setInsuranceCharge1(obj[50]);
						billingDTO.setVATExclude(Boolean.parseBoolean(obj[51].toString()));
						if(obj[53]!=null){
							billingDTO.setStorageMeasurement(obj[53]);
						}else{
							billingDTO.setStorageMeasurement("");	
						}
						if(obj[54]!=null){
							billingDTO.setFormulaText(obj[54]);
							}else{
								billingDTO.setFormulaText("");
							}
						if(obj[55]!=null){
							billingDTO.setInsuranceBuyRate(obj[55]);
							}else{
								billingDTO.setInsuranceBuyRate("");
							}
						if(obj[56]!=null){
							billingDTO.setVendorStoragePerMonth(obj[56]);
							}else{
								billingDTO.setVendorStoragePerMonth("");
							}
						if(obj[57]!=null){
							billingDTO.setStoragePerMonth(obj[57]);
							}else{
								billingDTO.setStoragePerMonth("");
							}
						if(obj[58]!=null){
							billingDTO.setInsurancePerMonth(obj[58]);
							}else{
								billingDTO.setInsurancePerMonth(new BigDecimal("0.00"));
							}
						if(obj[59]!=null){
							billingDTO.setPayableRate(obj[59]);
							}else{
								billingDTO.setPayableRate(new BigDecimal("0.00"));
							} 
						if(obj[60]!=null){
							billingDTO.setChargesDescription(obj[60]);
							}else{
								billingDTO.setChargesDescription("");
							}
						result.add(billingDTO);
				}
			}
		}catch(Exception e){
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
	}else{
		billingDTO.setShipNumber("");
		billingDTO.setRegistrationNumber("");
		billingDTO.setFirstName("");
		billingDTO.setLastName("");
		billingDTO.setBillToCode("");
		billingDTO.setBillToName("");
		billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
		billingDTO.setCharge("");
		billingDTO.setJob("");
		billingDTO.setWording("");
		billingDTO.setPostGrate("");
		billingDTO.setOnHand("");
		billingDTO.setCreatedBy("");
		billingDTO.setUpdatedBy("");
		billingDTO.setCreatedOn("");
		billingDTO.setUpdatedOn("");
		billingDTO.setInsuranceHas("");
		billingDTO.setInsuranceRate("");
		billingDTO.setInsuranceValueActual("");
		billingDTO.setPerValue("");
		billingDTO.setStorageBillingParty("");//storageBillingGroup
		billingDTO.setCycle("");
		billingDTO.setContract("Contract Mismatch");
		billingDTO.setMinimum(""); 
		billingDTO.setQuantityItemRevised("");
		billingDTO.setQuantityRevisedPreset("");
		billingDTO.setPerItem("");
		billingDTO.setToDates("");
		billingDTO.setDateDiff("");
		billingDTO.setGlCode("");
		billingDTO.setDivideMultiply("");
		billingDTO.setForChecking("y");
		billingDTO.setDescription("");
		billingDTO.setUseDiscount("");
		billingDTO.setPricePre("");
		billingDTO.setSitdiscount("");
		billingDTO.setBillToAuthority("");
		billingDTO.setDeliveryShipper("");
		billingDTO.setDeliveryA("");
		billingDTO.setBillThroughFrom("");
		billingDTO.setVatPercent("");
		billingDTO.setVatDescr(""); 
		billingDTO.setPayglCode("");
		billingDTO.setBaseInsuranceValue("");
		
		billingDTO.setBillingCurrency("");
		billingDTO.setVendorStoragePerMonth("");
		billingDTO.setVendorStorageVatDescr("");
		billingDTO.setVendorStorageVatPercent("");
		billingDTO.setVendorBillingCurency("");
		billingDTO.setVendorCode1("");
		billingDTO.setVendorName1("");
		billingDTO.setInsuranceCharge1("");
		billingDTO.setVATExclude(false);
		billingDTO.setStorageMeasurement("");	
		billingDTO.setFormulaText("");
		billingDTO.setInsuranceBuyRate("");
		billingDTO.setVendorStoragePerMonth("");
		billingDTO.setStoragePerMonth("");
		billingDTO.setInsurancePerMonth(new BigDecimal("0.00"));
		billingDTO.setPayableRate(new BigDecimal("0.00"));
		billingDTO.setChargesDescription("");
		result.add(billingDTO);
	}
	return result;

	}
	
	
	public void updateContract(String contract, String sequenceNumber){
		try{
		getHibernateTemplate().bulkUpdate("update Billing set contract='"+contract+"' where sequenceNumber='"+sequenceNumber+"' and (contract is null or contract='')");
		}catch(Exception e){
			 e.printStackTrace();
			getSessionCorpID();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		}

	public boolean getweeklyBilling(String sessionCorpID) {
		List list=getHibernateTemplate().find("select count(*) from Company where corpID='"+sessionCorpID+"' and weeklyBilling =true");
		String tracInternal= list.get(0).toString();
		if(tracInternal.equals("0")){
		return false;
		}else{
		return true;	
		}
		}

	public List dynamicPreviewVatStroageBilling(String beginDate, String endDate, String condition, String conditionInEx, String conditionBookingCodeType, String invoiceBillingDate, String sessionCorpID) {


			try {
				List list = getVatStorageBillingQuery(condition,conditionInEx,conditionBookingCodeType,sessionCorpID,beginDate,endDate,"","","","");
				
				List result= new ArrayList(); 
				BillingDTO billingDTO=new BillingDTO();
				
				Set<String> contractTypeSet = new HashSet<String>();
				contractTypeSet.add("CMM");contractTypeSet.add("DMM");
				
				if(list.size()>0){
					try{
						Iterator it=list.iterator();
						while(it.hasNext()){ 
							Object []obj=(Object[]) it.next();
							if(obj[7].toString().equalsIgnoreCase("nocharge") || contractTypeSet.contains(obj[52].toString())){
							}else{
								billingDTO=new BillingDTO(); 
								billingDTO.setShipNumber(obj[0]);
								billingDTO.setRegistrationNumber(obj[1]);
								billingDTO.setFirstName(obj[2]);
								billingDTO.setLastName(obj[3]);
								billingDTO.setBillToCode(obj[4]);
								billingDTO.setBillToName(obj[5]);
								if(obj[6]==null){
									billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
								}else{
									billingDTO.setStoragePerMonth(obj[6]);
								}
								
								billingDTO.setCharge(obj[7]);
								billingDTO.setJob(obj[8]);
								billingDTO.setWording(obj[9]);
								billingDTO.setPostGrate(obj[10]);
								try{
									billingDTO.setOnHand(obj[11]);
								}catch(Exception ex){
									 ex.printStackTrace();
								}
								billingDTO.setCreatedBy(obj[12]);
								billingDTO.setUpdatedBy(obj[13]);
								billingDTO.setCreatedOn(obj[14]);
								billingDTO.setUpdatedOn(obj[15]);
								billingDTO.setInsuranceHas(obj[16]);
								billingDTO.setInsuranceRate(obj[17]);
								billingDTO.setInsuranceValueActual(obj[18]);
								billingDTO.setPerValue(obj[19]);
								billingDTO.setStorageBillingParty(obj[20]);//storageBillingGroup
								billingDTO.setCycle(obj[21]);
								billingDTO.setContract(obj[22]);
								billingDTO.setMinimum(obj[23]); 
								billingDTO.setQuantityItemRevised(obj[24]);
								if(obj[25].toString().startsWith("0")|| obj[25].toString().startsWith("1")
								   ||obj[25].toString().startsWith("2")|| obj[25].toString().startsWith("3")
								   || obj[25].toString().startsWith("4")|| obj[25].toString().startsWith("5")
								   ||obj[25].toString().startsWith("6")|| obj[25].toString().startsWith("7")
								   ||obj[25].toString().startsWith("8")|| obj[25].toString().startsWith("9"))
								{
								  billingDTO.setQuantityRevisedPreset(obj[25]);
								}else{
								    
									billingDTO.setQuantityRevisedPreset(("0.00"));
								}   
								billingDTO.setPerItem(obj[26]);
								billingDTO.setToDates(obj[27]);
								billingDTO.setDateDiff(obj[28]);
								billingDTO.setGlCode(obj[29]);
								billingDTO.setDivideMultiply(obj[30]);
								billingDTO.setDescription(obj[31]);
								billingDTO.setUseDiscount(obj[32]);
								billingDTO.setPricePre(obj[33]);
								billingDTO.setSitdiscount(obj[34]);
								billingDTO.setBillToAuthority(obj[35]);
								billingDTO.setDeliveryShipper(obj[36]);
								billingDTO.setDeliveryA(obj[37]);
								billingDTO.setBillThroughFrom(obj[38]);
								billingDTO.setVatPercent(obj[39]); 
								billingDTO.setVatDescr(obj[40]); 
								if(obj[41]!=null){
									billingDTO.setPayglCode(obj[41]);
								}else{
									billingDTO.setPayglCode("");	
								}
								if(obj[42]!=null){
									billingDTO.setBaseInsuranceValue(obj[42]);
								}else{
									billingDTO.setBaseInsuranceValue("");	
								}
								
								billingDTO.setBillingCurrency(obj[43]);
								billingDTO.setVendorStoragePerMonth(obj[44]);
								billingDTO.setVendorStorageVatDescr(obj[45]);
								billingDTO.setVendorStorageVatPercent(obj[46]);
								billingDTO.setVendorBillingCurency(obj[47]);
								billingDTO.setVendorCode1(obj[48]);
								billingDTO.setVendorName1(obj[49]);
								billingDTO.setInsuranceCharge1(obj[50]);
								billingDTO.setVATExclude(Boolean.parseBoolean(obj[51].toString()));
								if(obj[53]!=null){
									billingDTO.setStorageMeasurement(obj[53]);
								}else{
									billingDTO.setStorageMeasurement("");	
								}
								if(obj[54]!=null){
									billingDTO.setFormulaText(obj[54]);
									}else{
										billingDTO.setFormulaText("");
									}
								if(obj[55]!=null){
									billingDTO.setInsuranceBuyRate(obj[55]);
									}else{
										billingDTO.setInsuranceBuyRate("");
									}
								if(obj[56]!=null){
									billingDTO.setVendorStoragePerMonth(obj[56]);
									}else{
										billingDTO.setVendorStoragePerMonth("");
									}
								if(obj[57]!=null){
									billingDTO.setStoragePerMonth(obj[57]);
									}else{
										billingDTO.setStoragePerMonth("");
									}
								if(obj[58]!=null){
									billingDTO.setInsurancePerMonth(obj[58]);
									}else{
										billingDTO.setInsurancePerMonth(new BigDecimal("0.00"));
									}
								if(obj[59]!=null){
									billingDTO.setPayableRate(obj[59]);
									}else{
										billingDTO.setPayableRate(new BigDecimal("0.00"));
									}
								if(obj[60]!=null){
									billingDTO.setChargesDescription(obj[60]);
									}else{
										billingDTO.setChargesDescription("");
									}
								result.add(billingDTO);
						}
					}
				}catch(Exception ex){
					 ex.printStackTrace();
				}
			}else{
				billingDTO.setShipNumber("");
				billingDTO.setRegistrationNumber("");
				billingDTO.setFirstName("");
				billingDTO.setLastName("");
				billingDTO.setBillToCode("");
				billingDTO.setBillToName("");
				billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
				billingDTO.setCharge("");
				billingDTO.setJob("");
				billingDTO.setWording("");
				billingDTO.setPostGrate("");
				billingDTO.setOnHand("");
				billingDTO.setCreatedBy("");
				billingDTO.setUpdatedBy("");
				billingDTO.setCreatedOn("");
				billingDTO.setUpdatedOn("");
				billingDTO.setInsuranceHas("");
				billingDTO.setInsuranceRate("");
				billingDTO.setInsuranceValueActual("");
				billingDTO.setPerValue("");
				billingDTO.setStorageBillingParty("");//storageBillingGroup
				billingDTO.setCycle("");
				billingDTO.setContract("Contract Mismatch");
				billingDTO.setMinimum(""); 
				billingDTO.setQuantityItemRevised("");
				billingDTO.setQuantityRevisedPreset("");
				billingDTO.setPerItem("");
				billingDTO.setToDates("");
				billingDTO.setDateDiff("");
				billingDTO.setGlCode("");
				billingDTO.setDivideMultiply("");
				billingDTO.setForChecking("y");
				billingDTO.setDescription("");
				billingDTO.setUseDiscount("");
				billingDTO.setPricePre("");
				billingDTO.setSitdiscount("");
				billingDTO.setBillToAuthority("");
				billingDTO.setDeliveryShipper("");
				billingDTO.setDeliveryA("");
				billingDTO.setBillThroughFrom("");
				billingDTO.setVatPercent("");
				billingDTO.setVatDescr(""); 
				billingDTO.setPayglCode("");
				billingDTO.setBaseInsuranceValue("");
				
				billingDTO.setBillingCurrency("");
				billingDTO.setVendorStoragePerMonth("");
				billingDTO.setVendorStorageVatDescr("");
				billingDTO.setVendorStorageVatPercent("");
				billingDTO.setVendorBillingCurency("");
				billingDTO.setVendorCode1("");
				billingDTO.setVendorName1("");
				billingDTO.setInsuranceCharge1("");
				billingDTO.setVATExclude(false);
				billingDTO.setStorageMeasurement("");
				billingDTO.setFormulaText("");
				billingDTO.setInsuranceBuyRate("");
				billingDTO.setVendorStoragePerMonth("");
				billingDTO.setStoragePerMonth("");
				billingDTO.setInsurancePerMonth(new BigDecimal("0.00"));
				billingDTO.setPayableRate(new BigDecimal("0.00"));
				billingDTO.setChargesDescription(""); 
				result.add(billingDTO);
			}
			return result;
} catch (Exception e) {
				getSessionCorpID();
				 e.printStackTrace();
				 logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}	
		
		return null;
	}

	public List dynamicPreviewVatStroageBillingSearch(String beginDate, String endDate, String condition, String conditionInEx, String conditionBookingCodeType, String shipNumbers, String firstName, String lastName, String billsTo, String cycles, String charges, String sessionCorpID, String invoiceBillingDate) {
		
           try {
   			List list = getVatStorageBillingQuery(condition,conditionInEx,conditionBookingCodeType,sessionCorpID,beginDate,endDate,shipNumbers,firstName,cycles,charges);
			
			List result= new ArrayList(); 
			BillingDTO billingDTO=new BillingDTO();
			
			Set<String> contractTypeSet = new HashSet<String>();
			contractTypeSet.add("CMM");contractTypeSet.add("DMM");
			
			if(list.size()>0){
				try{
					Iterator it=list.iterator();
					while(it.hasNext()){ 
						Object []obj=(Object[]) it.next();
						if(obj[7].toString().equalsIgnoreCase("nocharge") || contractTypeSet.contains(obj[52].toString())){
						}else{
							billingDTO=new BillingDTO(); 
							billingDTO.setShipNumber(obj[0]);
							billingDTO.setRegistrationNumber(obj[1]);
							billingDTO.setFirstName(obj[2]);
							billingDTO.setLastName(obj[3]);
							billingDTO.setBillToCode(obj[4]);
							billingDTO.setBillToName(obj[5]);
							if(obj[6]==null){
								billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
							}else{
								billingDTO.setStoragePerMonth(obj[6]);
							}
							//billingDTO.setStoragePerMonth(obj[6]);
							billingDTO.setCharge(obj[7]);
							billingDTO.setJob(obj[8]);
							billingDTO.setWording(obj[9]);
							billingDTO.setPostGrate(obj[10]);
							try{
								billingDTO.setOnHand(obj[11]);
							}catch(Exception ex){
								 ex.printStackTrace();
							}
							billingDTO.setCreatedBy(obj[12]);
							billingDTO.setUpdatedBy(obj[13]);
							billingDTO.setCreatedOn(obj[14]);
							billingDTO.setUpdatedOn(obj[15]);
							billingDTO.setInsuranceHas(obj[16]);
							billingDTO.setInsuranceRate(obj[17]);
							billingDTO.setInsuranceValueActual(obj[18]);
							billingDTO.setPerValue(obj[19]);
							billingDTO.setStorageBillingParty(obj[20]);//storageBillingGroup
							billingDTO.setCycle(obj[21]);
							billingDTO.setContract(obj[22]);
							billingDTO.setMinimum(obj[23]); 
							billingDTO.setQuantityItemRevised(obj[24]);
							if(obj[25].toString().startsWith("0")|| obj[25].toString().startsWith("1")
							   ||obj[25].toString().startsWith("2")|| obj[25].toString().startsWith("3")
							   || obj[25].toString().startsWith("4")|| obj[25].toString().startsWith("5")
							   ||obj[25].toString().startsWith("6")|| obj[25].toString().startsWith("7")
							   ||obj[25].toString().startsWith("8")|| obj[25].toString().startsWith("9"))
							{
							  billingDTO.setQuantityRevisedPreset(obj[25]);
							}else{
							    
								billingDTO.setQuantityRevisedPreset(("0.00"));
							}   
							billingDTO.setPerItem(obj[26]);
							billingDTO.setToDates(obj[27]);
							billingDTO.setDateDiff(obj[28]);
							billingDTO.setGlCode(obj[29]);
							billingDTO.setDivideMultiply(obj[30]);
							billingDTO.setDescription(obj[31]);
							billingDTO.setUseDiscount(obj[32]);
							billingDTO.setPricePre(obj[33]);
							billingDTO.setSitdiscount(obj[34]);
							billingDTO.setBillToAuthority(obj[35]);
							billingDTO.setDeliveryShipper(obj[36]);
							billingDTO.setDeliveryA(obj[37]);
							billingDTO.setBillThroughFrom(obj[38]);
							billingDTO.setVatPercent(obj[39]); 
							billingDTO.setVatDescr(obj[40]); 
							if(obj[41]!=null){
								billingDTO.setPayglCode(obj[41]);
							}else{
								billingDTO.setPayglCode("");	
							}
							if(obj[42]!=null){
								billingDTO.setBaseInsuranceValue(obj[42]);
							}else{
								billingDTO.setBaseInsuranceValue("");	
							}
							
							billingDTO.setBillingCurrency(obj[43]);
							billingDTO.setVendorStoragePerMonth(obj[44]);
							billingDTO.setVendorStorageVatDescr(obj[45]);
							billingDTO.setVendorStorageVatPercent(obj[46]);
							billingDTO.setVendorBillingCurency(obj[47]);
							billingDTO.setVendorCode1(obj[48]);
							billingDTO.setVendorName1(obj[49]);
							billingDTO.setInsuranceCharge1(obj[50]);
							billingDTO.setVATExclude(Boolean.parseBoolean(obj[51].toString()));
							if(obj[53]!=null){
								billingDTO.setStorageMeasurement(obj[53]);
							}else{
								billingDTO.setStorageMeasurement("");	
							}
							if(obj[54]!=null){
								billingDTO.setFormulaText(obj[54]);
								}else{
									billingDTO.setFormulaText("");
								}
							if(obj[55]!=null){
								billingDTO.setInsuranceBuyRate(obj[55]);
								}else{
									billingDTO.setInsuranceBuyRate("");
								}
							if(obj[56]!=null){
								billingDTO.setVendorStoragePerMonth(obj[56]);
								}else{
									billingDTO.setVendorStoragePerMonth("");
								}
							if(obj[57]!=null){
								billingDTO.setStoragePerMonth(obj[57]);
								}else{
									billingDTO.setStoragePerMonth("");
								}
							if(obj[58]!=null){
								billingDTO.setInsurancePerMonth(obj[58]);
								}else{
									billingDTO.setInsurancePerMonth(new BigDecimal("0.00"));
								}
							if(obj[59]!=null){
								billingDTO.setPayableRate(obj[59]);
								}else{
									billingDTO.setPayableRate(new BigDecimal("0.00"));
								} 
							if(obj[60]!=null){
								billingDTO.setChargesDescription(obj[60]);
								}else{
									billingDTO.setChargesDescription("");
								}
							result.add(billingDTO);
					}
				}
			}catch(Exception ex){
				 ex.printStackTrace();
			}
		}else{
			billingDTO.setShipNumber("");
			billingDTO.setRegistrationNumber("");
			billingDTO.setFirstName("");
			billingDTO.setLastName("");
			billingDTO.setBillToCode("");
			billingDTO.setBillToName("");
			billingDTO.setStoragePerMonth(new BigDecimal("0.00"));
			billingDTO.setCharge("");
			billingDTO.setJob("");
			billingDTO.setWording("");
			billingDTO.setPostGrate("");
			billingDTO.setOnHand("");
			billingDTO.setCreatedBy("");
			billingDTO.setUpdatedBy("");
			billingDTO.setCreatedOn("");
			billingDTO.setUpdatedOn("");
			billingDTO.setInsuranceHas("");
			billingDTO.setInsuranceRate("");
			billingDTO.setInsuranceValueActual("");
			billingDTO.setPerValue("");
			billingDTO.setStorageBillingParty("");//storageBillingGroup
			billingDTO.setCycle("");
			billingDTO.setContract("Contract Mismatch");
			billingDTO.setMinimum(""); 
			billingDTO.setQuantityItemRevised("");
			billingDTO.setQuantityRevisedPreset("");
			billingDTO.setPerItem("");
			billingDTO.setToDates("");
			billingDTO.setDateDiff("");
			billingDTO.setGlCode("");
			billingDTO.setDivideMultiply("");
			billingDTO.setForChecking("y");
			billingDTO.setDescription("");
			billingDTO.setUseDiscount("");
			billingDTO.setPricePre("");
			billingDTO.setSitdiscount("");
			billingDTO.setBillToAuthority("");
			billingDTO.setDeliveryShipper("");
			billingDTO.setDeliveryA("");
			billingDTO.setBillThroughFrom("");
			billingDTO.setVatPercent("");
			billingDTO.setVatDescr(""); 
			billingDTO.setPayglCode("");
			billingDTO.setBaseInsuranceValue("");
			
			billingDTO.setBillingCurrency("");
			billingDTO.setVendorStoragePerMonth("");
			billingDTO.setVendorStorageVatDescr("");
			billingDTO.setVendorStorageVatPercent("");
			billingDTO.setVendorBillingCurency("");
			billingDTO.setVendorCode1("");
			billingDTO.setVendorName1("");
			billingDTO.setInsuranceCharge1("");
			billingDTO.setVATExclude(false);
			billingDTO.setStorageMeasurement("");
			billingDTO.setFormulaText("");
			billingDTO.setInsuranceBuyRate("");
			billingDTO.setVendorStoragePerMonth("");
			billingDTO.setStoragePerMonth("");
			billingDTO.setInsurancePerMonth(new BigDecimal("0.00"));
			billingDTO.setPayableRate(new BigDecimal("0.00")); 
			billingDTO.setChargesDescription("");
			result.add(billingDTO);
		}
		return result;
} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	   
		} 
	return null;
	}

	public int updateBillingBilltocode(String billingId, String accountCode, String accountName, String billtocode) {
		try {
			int i =this.getSession().createSQLQuery("update billing set billToCode='"+accountCode+"' ,billToName ='"+accountName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where id in ("+billingId+") and billToCode='"+billtocode+"'   ").executeUpdate();
			int j =this.getSession().createSQLQuery("update serviceorder set billToCode='"+accountCode+"' ,billToName ='"+accountName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where id in ("+billingId+")  and billToCode='"+billtocode+"'  ").executeUpdate();
			return i;
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return 0;
	}
	
	public String findVendorCurrency(String partnerCode, String sessionCorpID){
		try {
			List list = this.getSession().createSQLQuery("select billingCurrency from partnerpublic where partnerCode='"+partnerCode+"' limit 1").list();
			if(list != null && list.size()>0 && list.get(0) != null && !"".equals(list.get(0).toString().trim())){
				return list.get(0).toString().trim();
			}else{
				list = this.getSession().createSQLQuery("select baseCurrency from systemdefault where corpId='"+sessionCorpID+"'").list();
				if (list != null && list.size() > 0) {
					return list.get(0).toString();
				}else{
					return "";
				}
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return "";
	}
	public List getBillingCurrencyAndExchangeRate(String sessionCorpID,String shipNumber,String contract){
		List list1 = new ArrayList();
		String query = "select b.billingCurrency,b.exchangeRate from  billing b,contract c"+
						" where b.shipNumber='"+shipNumber+"' and b.corpId='"+sessionCorpID+"' and b.corpId=c.corpId"+
						" and b.contract=c.contract and c.contract='"+contract+"' and c.contractType in ('CMM','DMM','KGA')";
		List list = getSession().createSQLQuery(query).list();
		if(list!=null && !list.isEmpty() && list.size()>0){
			Iterator it=list.iterator();
			while (it.hasNext()) {
				Object[] obj=((Object[]) it.next());
				if(obj[0] != null && !"".equals(obj[0].toString())){
					list1.add(obj[0]);
					list1.add(obj[1]);
					return list1;
				}
			}
		}
		return null;
	}
	
	public int updateAccountLineBillToCode(String billToCode, String billToName, Long sid,String billToCodeAcc){
		try {
			String query="update AccountLine set billToCode='" +billToCode.replaceAll("'", "")+ "' , billToName='" +billToName.replaceAll("'", "")+ "',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where receivedInvoiceDate is null and status=true and (recInvoiceNumber='' or  recInvoiceNumber is null)  and billToCode='"+billToCodeAcc+"' and serviceOrderId='"+sid+"'" ;
			return getHibernateTemplate().bulkUpdate(query);
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 
		} 
		return 0;
	}

	public int updateAgentBillingFromStorage(String toDates, String shipNumber, String loginUser, Date date, String externalCorpID) {
		try {
			int i= this.getSession().createSQLQuery("update billing set billthrough='"+toDates+"', updatedBy='"+loginUser+"', updatedOn=now() where shipNumber= '"+shipNumber+"' and corpID = '"+externalCorpID+"' ").executeUpdate(); ;
			return i;
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
		}
		return 0;
	}
	public String findBillToCodeFromBilling(Long sid,String sessionCorpID){
		String accType="";
		try {
			String query="select billToCode from billing where id='"+sid+"' and corpID = '"+sessionCorpID+"'";
			List al=this.getSession().createSQLQuery(query).list();
			if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
				accType=al.get(0).toString();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	   
		}
		return accType;
	}
	public String findSecBillToCodeFromBilling(Long sid,String sessionCorpID){
		String accType="";
		try {
			String query="select billTo2Code from billing where id='"+sid+"' and corpID = '"+sessionCorpID+"'";
			List al=this.getSession().createSQLQuery(query).list();
			if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
				accType=al.get(0).toString();
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return accType;
	}
	public String findPrivateBillToCodeFromBilling(Long sid,String sessionCorpID){
		String accType="";
		try {
			String query="select privatePartyBillingCode from billing where id='"+sid+"' and corpID = '"+sessionCorpID+"'";
			List al=this.getSession().createSQLQuery(query).list();
			if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)){
				accType=al.get(0).toString();
			}
		} catch (DataAccessResourceFailureException e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return accType;
	}
	public int updateAccountLineBillToCode1(String billToCode, String billToName, Long sid,String billToCodeAcc1){
		try {
			String query="update AccountLine set billToCode='" +billToCode.replaceAll("'", "")+ "' , billToName='" +billToName.replaceAll("'", "")+ "',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where receivedInvoiceDate is null and status=true and (recInvoiceNumber='' or  recInvoiceNumber is null)  and billToCode='"+billToCodeAcc1+"' and serviceOrderId='"+sid+"'";
			return getHibernateTemplate().bulkUpdate(query);
		} catch (Exception  e) {
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
		} 
		return 0;
	}
	public int updateAccountLineBillToCode2(String billToCode, String billToName, Long sid,String billToCodeAcc2){
		try {
			String query="update AccountLine set billToCode='" +billToCode.replaceAll("'", "")+ "' , billToName='" +billToName.replaceAll("'", "")+ "',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where receivedInvoiceDate is null and status=true and (recInvoiceNumber='' or  recInvoiceNumber is null)  and billToCode='"+billToCodeAcc2+"' and serviceOrderId='"+sid+"'";
			return getHibernateTemplate().bulkUpdate(query);
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		} 
		return 0;
	}
	public String getPartnerStorageMail(String partnerCode,String sessionCorpID){		
		try {
			String accType="";
			String query="select storageEmail from partnerprivate where partnerCode='"+partnerCode+"' and storageEmail is not null and storageEmail!='' and corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')";
			List	list = this.getSession().createSQLQuery(query).list();
			if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null)){
				accType=list.get(0).toString();
			}
			return accType;
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return "" ;
		}
	public String findBillingIdNameAjax(String billToCode,String sessionCorpID){
		 List Bucket2List= new ArrayList();
		 String billName="";
			 try {
				String query="select p.lastName from partnerpublic p,partnerprivate pp " +
				 		" where p.id=pp.partnerPublicId  " +
				 		" and p.partnerCode is not null and p.partnerCode!='' " +
				 		" and p.corpId in ('TSFT','"+sessionCorpID+"') " +
				 		" and pp.corpId='"+sessionCorpID+"' and p.status ='Approved' and p.partnerCode='"+billToCode+"'";
				 Bucket2List= this.getSession().createSQLQuery(query).list();	 

				 if((Bucket2List!=null)&&(!Bucket2List.isEmpty())&&(Bucket2List.get(0)!=null)){
					 billName=Bucket2List.get(0).toString();
				 }
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	
			}
		return billName;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public boolean getDmmInsurancePolicy(String sessionCorpID, String contract) {
		boolean dmmInsurancePolicy = true;
		try{
		List list= getSession().createSQLQuery("select  if(dmmInsurancePolicy is true,'true','false') from contract where corpID ='"+sessionCorpID+"' and contract ='"+contract+"'").list();
		if(list!=null && (!(list.isEmpty())) && list.get(0)!=null && (!(list.get(0).toString().trim().equals("")))){
			dmmInsurancePolicy = Boolean.parseBoolean(list.get(0).toString().trim()) ;	
		}
		}catch(Exception e){
			 e.printStackTrace();
		}
		return dmmInsurancePolicy;
	}

	public int updateBillingNoInsurance(String billingId, String noInsurance, String updatedBy) {
		try {
			int i =this.getSession().createSQLQuery("update billing set noInsurance='"+noInsurance+"' , updatedBy='"+updatedBy+"', updatedOn=now() where id in ("+billingId+")  ").executeUpdate();
			
			return i;
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return 0;
	}

	public List findReviewStatusAccrue(Long sid) { 
		return getHibernateTemplate().find("select  accountLineNumber from AccountLine where serviceorderid='"+sid+"' and ((accrueRevenue is not null and accrueRevenueReverse is null) or (accruePayable is not null and accruePayableReverse is null) ) and status is true");
	}

	public boolean findVendorCode(String corpID, String partnerCode) {
		 boolean checkVendor=false;
		 try{
		 List pList = new ArrayList();
		 pList = this.getSession().createSQLQuery("select id from partnerpublic where partnerCode='"+partnerCode+"' and corpid in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') and status ='Approved'  ").list();
			if(pList!=null && (!(pList.isEmpty())) && pList.get(0)!=null){
				checkVendor=true;
			}
		 }catch(Exception e){
			 e.printStackTrace();  
		 }
		 return checkVendor;
	}

	public int updateBillingNetworkBilltocode(String billingId, String accountCode, String accountName, String billtocode) {
		try {
			int i =this.getSession().createSQLQuery("update billing set networkBillToCode='"+accountCode+"' ,networkBillToName ='"+accountName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where id in ("+billingId+") and networkBillToCode='"+billtocode+"'   ").executeUpdate();
			
			return i;
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	
		}
		return 0;
	}

	public int updateAccNetworkBilltocode(String sameInstanceAccId, String accountCode, String accountName, String billtocode, String sameInstanceBillingId) {
		try {
			int i=0;
			if(sameInstanceBillingId !=null && (!(sameInstanceBillingId.trim().equals("")))){
				if(sameInstanceAccId !=null && (!(sameInstanceAccId.trim().equals(""))) ){
				i =this.getSession().createSQLQuery("update accountline set networkBillToCode='"+accountCode+"' ,networkBillToName ='"+accountName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where serviceOrderId in ("+sameInstanceBillingId+") and id not in ("+sameInstanceAccId+") and networkBillToCode='"+billtocode+"' and (recInvoiceNumber='' or  recInvoiceNumber is null)   ").executeUpdate();
				}else{
					i =this.getSession().createSQLQuery("update accountline set networkBillToCode='"+accountCode+"' ,networkBillToName ='"+accountName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where serviceOrderId in ("+sameInstanceBillingId+") and networkBillToCode='"+billtocode+"' and (recInvoiceNumber='' or  recInvoiceNumber is null)    ").executeUpdate();	
				}
				}else{
				
			   i =this.getSession().createSQLQuery("update accountline set networkBillToCode='"+accountCode+"' ,networkBillToName ='"+accountName+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where id in ("+sameInstanceAccId+") and networkBillToCode='"+billtocode+"' and (recInvoiceNumber='' or  recInvoiceNumber is null)    ").executeUpdate();
			}
			return i;
		} catch (Exception e) {
			getSessionCorpID();
			 e.printStackTrace();
			logger.error("Error executing query"+ e,e);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    
		}
		return 0;
	}
	
	public String getSellBuyRate(String billToCode, String sessionCorpID){
		String query ="select concat(sellrate,'~',buyrate) from partnerprivate where partnerCode='"+billToCode+"' and corpid='"+sessionCorpID+"'" ;
		String value="";
		List al=getSession().createSQLQuery(query).list();
		 if(al!=null && !al.isEmpty() && al.get(0)!=null){
			value = al.get(0).toString();
		 }
		return value;
	}

	public void updateBillingRevenueRecognition(String postDate, String shipNumber, String loginUser,String corpId) {
		this.getSession().createSQLQuery("update billing set revenueRecognition = '" +postDate+"',updatedOn=now(), updatedBy='"+loginUser+"' where  revenueRecognition is null and  shipNumber='"+shipNumber+"'").executeUpdate();
		
	}
	
	public void updateOIValuation(Double salesTaxForOI, String shipNumber, String loginUser,String corpId) {
		getHibernateTemplate().bulkUpdate("update OperationsIntelligence set revisionValuation = '" +salesTaxForOI+"',estmatedValuation = '" +salesTaxForOI+"',updatedOn=now(), updatedBy='"+loginUser+"' where shipNumber='"+shipNumber+"'");
		
	}

	public String findBillToCode(Long id, String sessionCorpID) {
		// TODO Auto-generated method stub
		 List billToList= new ArrayList();
		 String billCode="";
			 try {
				String query="select billToCode from billing  where id='"+id+"' and corpId in ('TSFT','"+sessionCorpID+"')";
				billToList= this.getSession().createSQLQuery(query).list();	 

				 if((billToList!=null)&&(!billToList.isEmpty())&&(billToList.get(0)!=null)){
					 billCode=billToList.get(0).toString();
				 }
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Error executing query"+ e,e);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	
			}
		return billCode;
	}
	
	}
  

