// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;
update networkdatafields set type='CMM/DMM' where modelName='CustomerFile' and fieldName='comptetive' and id=871;

CREATE  TABLE `redsky`.`emailsetuptemplate` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `corpId` VARCHAR(10) NULL ,
  `updatedOn` DATETIME NULL ,
  `updatedBy` VARCHAR(82) NULL ,
  `createdOn` DATETIME NULL ,
  `createdBy` VARCHAR(82) NULL ,
  `saveAs` VARCHAR(25) NULL ,
  `module` VARCHAR(25) NULL ,
  `subModule` VARCHAR(25) NULL ,
  `jobType` VARCHAR(3) NULL ,
  `mode` VARCHAR(20) NULL ,
  `routing` VARCHAR(5) NULL ,
  `moveType` VARCHAR(25) NULL ,
  `service` VARCHAR(50) NULL ,
  `language` VARCHAR(2) NULL ,
  `others` VARCHAR(500) NULL ,
  `companyDivision` VARCHAR(10) NULL ,
  `triggerField` VARCHAR(50) NULL ,
  `emailFrom` VARCHAR(50) NULL ,
  `emailTo` VARCHAR(50) NULL ,
  `emailCc` VARCHAR(500) NULL ,
  `emailBcc` VARCHAR(500) NULL ,
  `emailSubject` VARCHAR(200) NULL ,
  `attachedFileLocation` VARCHAR(5000) NULL ,
  `distributionMethod` VARCHAR(10) NULL ,
  `fileTypeOfFileCabinet` VARCHAR(35) NULL ,
  `formsId` VARCHAR(200) NULL ,
  `status` VARCHAR(10) NULL ,
  `emailBody` VARCHAR(10000) NULL ,
   `enable` BIT(1) NULL,
  `addSignatureFromAppUser` BIT(1) NULL,
  PRIMARY KEY (`id`) );
  
  INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','DISTRIBUTIONMETHOD',10,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'Scheduler', 'Scheduler', 10, 'DISTRIBUTIONMETHOD','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'Manual', 'Manual', 10, 'DISTRIBUTIONMETHOD','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'Both', 'Both', 10, 'DISTRIBUTIONMETHOD','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP');


ALTER TABLE `redsky`.`billing` ADD COLUMN 'autoBilledRate' decimal(19,3) NULL  ,ADD COLUMN `autoInsuredValue' decimal(19,3) NULL, ADD COLUMN `autoInternalRate` decimal(19,3) NULL  ;
ALTER TABLE redsky.trackingstatus add COLUMN originLocationType varchar(25) NULL ,add  COLUMN destinationLocationType varchar(25) NULL ;


