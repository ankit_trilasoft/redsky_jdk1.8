package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "intellead")
public class IntelLead {
	private Long id;
	private String corpId;
	private Date createdOn;
    private String createdBy;
    private Date updatedOn;
    private String updatedBy;
    private String employeeName;
    private String emplid;
    private String assignmentName;
    private String employeeEmailAddress;
    private String supplierCode;
    private String supplierName;
    private String serviceCountryID;
    private String serviceCountryName;
    private String accountName;
    private String accountDescription;
    private String authID;
    private Date authDate;
    private String authType;
    private String assignmentType;
    private Date assignmentStartDate;
    private Date assignmentProjectedReturnDate;
    private Date serviceStartDate;
    private Date serviceProjectedEndDate;
    private String duration;
    private String flexibility;
    private String howMany;
    private String howManyDesc;
    private String limitAmount;
    private String currencyCode;
    private String homePhone;
    private String workPhone;
    private String otherPhone;
    private String fromAddress1;
    private String fromAddress2;
    private String fromAddress3;
    private String fromCity;
    private String fromState;
    private String fromPostalCode;
    private String fromCountry;
    private String toCityToState;
    private String toCountry;
    private String comments;
    private String action;
    private String customerFileNumber;
    private String serviceOrderNumber;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId)
				.append("createdOn", createdOn)
				.append("createdBy", createdBy)
				.append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy)
				.append("employeeName", employeeName)
				.append("emplid", emplid)
				.append("assignmentName", assignmentName)
				.append("employeeEmailAddress", employeeEmailAddress)
				.append("supplierCode", supplierCode)
				.append("supplierName", supplierName)
				.append("serviceCountryID", serviceCountryID)
				.append("serviceCountryName", serviceCountryName)
				.append("accountName", accountName)
				.append("accountDescription", accountDescription)
				.append("authID", authID)
				.append("authDate", authDate)
				.append("authType", authType)
				.append("assignmentType", assignmentType)
				.append("assignmentStartDate", assignmentStartDate)
				.append("assignmentProjectedReturnDate", assignmentProjectedReturnDate)
				.append("serviceStartDate", serviceStartDate)
				.append("serviceProjectedEndDate", serviceProjectedEndDate)
				.append("duration", duration)
				.append("flexibility", flexibility)
				.append("howMany", howMany)
				.append("howManyDesc", howManyDesc)
				.append("limitAmount", limitAmount)
				.append("currencyCode", currencyCode)
				.append("homePhone", homePhone)
				.append("workPhone", workPhone)
				.append("otherPhone", otherPhone)
				.append("fromAddress1", fromAddress1)
				.append("fromAddress2", fromAddress2)
				.append("fromAddress3", fromAddress3)
				.append("fromCity", fromCity)
				.append("fromState", fromState)
				.append("fromPostalCode", fromPostalCode)
				.append("fromCountry", fromCountry)
				.append("toCityToState", toCityToState)
				.append("toCountry", toCountry)
				.append("comments", comments)	
				.append("action",action)
				.append("customerFileNumber", customerFileNumber)	
				.append("serviceOrderNumber",serviceOrderNumber)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof IntelLead))
			return false;
		IntelLead castOther = (IntelLead) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(employeeName, castOther.employeeName)
				.append(emplid, castOther.emplid)
				.append(assignmentName, castOther.assignmentName)
				.append(employeeEmailAddress, castOther.employeeEmailAddress)
				.append(supplierCode, castOther.supplierCode)
				.append(supplierName, castOther.supplierName)
				.append(serviceCountryID, castOther.serviceCountryID)
				.append(serviceCountryName, castOther.serviceCountryName)
				.append(accountName, castOther.accountName)
				.append(accountDescription, castOther.accountDescription)
				.append(authID, castOther.authID)
				.append(authDate, castOther.authDate)
				.append(authType, castOther.authType)
				.append(assignmentType, castOther.assignmentType)
				.append(assignmentStartDate, castOther.assignmentStartDate)
				.append(assignmentProjectedReturnDate, castOther.assignmentProjectedReturnDate)
				.append(serviceStartDate, castOther.serviceStartDate)
				.append(serviceProjectedEndDate, castOther.serviceProjectedEndDate)
				.append(duration, castOther.duration)
				.append(flexibility, castOther.flexibility)
				.append(howMany, castOther.howMany)
				.append(howManyDesc, castOther.howManyDesc)
				.append(limitAmount, castOther.limitAmount)
				.append(currencyCode, castOther.currencyCode)
				.append(homePhone, castOther.homePhone)
				.append(workPhone, castOther.workPhone)
				.append(otherPhone, castOther.otherPhone)
				.append(fromAddress1, castOther.fromAddress1)
				.append(fromAddress2, castOther.fromAddress2)
				.append(fromAddress3, castOther.fromAddress3)
				.append(fromCity, castOther.fromCity)
				.append(fromState, castOther.fromState)
				.append(fromPostalCode, castOther.fromPostalCode)
				.append(fromCountry, castOther.fromCountry)
				.append(toCityToState, castOther.toCityToState)
				.append(toCountry, castOther.toCountry)
				.append(comments, castOther.comments)
				.append(action, castOther.action)
				.append(customerFileNumber, castOther.customerFileNumber)
				.append(serviceOrderNumber, castOther.serviceOrderNumber)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id)
				.append(corpId).append(createdOn)
				.append(createdBy)
				.append(updatedOn)
				.append(updatedBy)
				.append(employeeName)
				.append(emplid)
				.append(assignmentName)
				.append(employeeEmailAddress)
				.append(supplierCode)
				.append(supplierName)
				.append(serviceCountryID)
				.append(serviceCountryName)
				.append(accountName)
				.append(accountDescription)
				.append(authID)
				.append(authDate)
				.append(authType)
				.append(assignmentType)
				.append(assignmentStartDate)
				.append(assignmentProjectedReturnDate)
				.append(serviceStartDate)
				.append(serviceProjectedEndDate)
				.append(duration)
				.append(flexibility)
				.append(howMany)
				.append(howManyDesc)
				.append(limitAmount)
				.append(currencyCode)
				.append(homePhone)
				.append(workPhone)
				.append(otherPhone)
				.append(fromAddress1)
				.append(fromAddress2)
				.append(fromAddress3)
				.append(fromCity)
				.append(fromState)
				.append(fromPostalCode)
				.append(fromCountry)
				.append(toCityToState)
				.append(toCountry)
				.append(comments)
				.append(action)
				.append(customerFileNumber)
				.append(serviceOrderNumber)
				.toHashCode();
	}
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	@Column
	public String getEmplid() {
		return emplid;
	}
	public void setEmplid(String emplid) {
		this.emplid = emplid;
	}
	@Column
	public String getAssignmentName() {
		return assignmentName;
	}
	public void setAssignmentName(String assignmentName) {
		this.assignmentName = assignmentName;
	}
	@Column
	public String getEmployeeEmailAddress() {
		return employeeEmailAddress;
	}
	public void setEmployeeEmailAddress(String employeeEmailAddress) {
		this.employeeEmailAddress = employeeEmailAddress;
	}
	@Column
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	@Column
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	@Column
	public String getServiceCountryID() {
		return serviceCountryID;
	}
	public void setServiceCountryID(String serviceCountryID) {
		this.serviceCountryID = serviceCountryID;
	}
	@Column
	public String getServiceCountryName() {
		return serviceCountryName;
	}
	public void setServiceCountryName(String serviceCountryName) {
		this.serviceCountryName = serviceCountryName;
	}
	@Column
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	@Column
	public String getAccountDescription() {
		return accountDescription;
	}
	public void setAccountDescription(String accountDescription) {
		this.accountDescription = accountDescription;
	}
	@Column
	public String getAuthID() {
		return authID;
	}
	public void setAuthID(String authID) {
		this.authID = authID;
	}
	@Column
	public Date getAuthDate() {
		return authDate;
	}
	public void setAuthDate(Date authDate) {
		this.authDate = authDate;
	}
	@Column
	public String getAuthType() {
		return authType;
	}
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	@Column
	public String getAssignmentType() {
		return assignmentType;
	}
	public void setAssignmentType(String assignmentType) {
		this.assignmentType = assignmentType;
	}
	@Column
	public Date getAssignmentStartDate() {
		return assignmentStartDate;
	}
	public void setAssignmentStartDate(Date assignmentStartDate) {
		this.assignmentStartDate = assignmentStartDate;
	}
	@Column
	public Date getAssignmentProjectedReturnDate() {
		return assignmentProjectedReturnDate;
	}
	public void setAssignmentProjectedReturnDate(
			Date assignmentProjectedReturnDate) {
		this.assignmentProjectedReturnDate = assignmentProjectedReturnDate;
	}
	@Column
	public Date getServiceStartDate() {
		return serviceStartDate;
	}
	public void setServiceStartDate(Date serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}
	@Column
	public Date getServiceProjectedEndDate() {
		return serviceProjectedEndDate;
	}
	public void setServiceProjectedEndDate(Date serviceProjectedEndDate) {
		this.serviceProjectedEndDate = serviceProjectedEndDate;
	}
	@Column
	public String getDuration() {
		return duration;
	}
	@Column
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getFlexibility() {
		return flexibility;
	}
	public void setFlexibility(String flexibility) {
		this.flexibility = flexibility;
	}
	@Column
	public String getHowMany() {
		return howMany;
	}
	public void setHowMany(String howMany) {
		this.howMany = howMany;
	}
	@Column
	public String getHowManyDesc() {
		return howManyDesc;
	}
	public void setHowManyDesc(String howManyDesc) {
		this.howManyDesc = howManyDesc;
	}
	@Column
	public String getLimitAmount() {
		return limitAmount;
	}
	public void setLimitAmount(String limitAmount) {
		this.limitAmount = limitAmount;
	}
	@Column
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	@Column
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	@Column
	public String getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	@Column
	public String getOtherPhone() {
		return otherPhone;
	}
	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}
	@Column
	public String getFromAddress1() {
		return fromAddress1;
	}
	public void setFromAddress1(String fromAddress1) {
		this.fromAddress1 = fromAddress1;
	}
	@Column
	public String getFromAddress2() {
		return fromAddress2;
	}
	public void setFromAddress2(String fromAddress2) {
		this.fromAddress2 = fromAddress2;
	}
	@Column
	public String getFromAddress3() {
		return fromAddress3;
	}
	public void setFromAddress3(String fromAddress3) {
		this.fromAddress3 = fromAddress3;
	}
	@Column
	public String getFromCity() {
		return fromCity;
	}
	public void setFromCity(String fromCity) {
		this.fromCity = fromCity;
	}
	@Column
	public String getFromState() {
		return fromState;
	}
	public void setFromState(String fromState) {
		this.fromState = fromState;
	}
	@Column
	public String getFromPostalCode() {
		return fromPostalCode;
	}
	public void setFromPostalCode(String fromPostalCode) {
		this.fromPostalCode = fromPostalCode;
	}
	@Column
	public String getFromCountry() {
		return fromCountry;
	}
	public void setFromCountry(String fromCountry) {
		this.fromCountry = fromCountry;
	}
	@Column
	public String getToCityToState() {
		return toCityToState;
	}
	public void setToCityToState(String toCityToState) {
		this.toCityToState = toCityToState;
	}
	@Column
	public String getToCountry() {
		return toCountry;
	}
	public void setToCountry(String toCountry) {
		this.toCountry = toCountry;
	}
	@Column
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Column
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getCustomerFileNumber() {
		return customerFileNumber;
	}
	public void setCustomerFileNumber(String customerFileNumber) {
		this.customerFileNumber = customerFileNumber;
	}
	public String getServiceOrderNumber() {
		return serviceOrderNumber;
	}
	public void setServiceOrderNumber(String serviceOrderNumber) {
		this.serviceOrderNumber = serviceOrderNumber;
	}
}
