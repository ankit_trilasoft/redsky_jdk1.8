package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsMoveMgmtDao;
import com.trilasoft.app.model.DsMoveMgmt;
import com.trilasoft.app.service.DsMoveMgmtManager;


public class DsMoveMgmtManagerImpl extends GenericManagerImpl<DsMoveMgmt, Long> implements DsMoveMgmtManager { 
	DsMoveMgmtDao dsMoveMgmtDao;
	public DsMoveMgmtManagerImpl(DsMoveMgmtDao dsMoveMgmtDao) {
		super(dsMoveMgmtDao); 
        this.dsMoveMgmtDao = dsMoveMgmtDao; 	
	}
	
	public List getListById(Long id) {
		return dsMoveMgmtDao.getListById(id);
	}

}

