// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`serviceorder` MODIFY COLUMN `destinationDayExtn` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
 MODIFY COLUMN `originDayExtn` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
 
 ALTER TABLE `redsky`.`itemdata` ADD COLUMN `shippingPackId` VARCHAR(255);
 
 Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,status,createdon,createdby,updatedon,updatedby)values
('VOER', 'SurveyEmail', 'SurveyEmail', 10, 'CPORTAL_DOC_TYPE','en','Active',now(),'VOER_SETUP',now(),'VOER_SETUP');