package com.trilasoft.app.webapp.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.InventoryPath;
import com.trilasoft.app.service.AccessInfoManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.InventoryDataManager;
import com.trilasoft.app.service.InventoryPathManager;
import com.trilasoft.app.service.ToDoRuleManager;

public class SurveyJob extends QuartzJobBean {
	
	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private ApplicationContext appCtx;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
	private InventoryData inventoryData;
	private AccessInfo accessInfo;
	private InventoryPath inventoryPath;
	private CustomerFile customerFile;
	
	private ApplicationContext getApplicationContext(JobExecutionContext context)
	throws Exception {
			ApplicationContext appCtx = null;
				appCtx = (ApplicationContext) context.getScheduler().getContext().get(
						APPLICATION_CONTEXT_KEY);
					if (appCtx == null) {
						throw new JobExecutionException(
								"No application context available in scheduler context for key \""
					+ APPLICATION_CONTEXT_KEY + "\"");
					}
					return appCtx;
				}

	
	private String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if(nl != null && nl.getLength() > 0) {
			Element el = (Element)nl.item(0);
			if(el.getFirstChild()!=null){
				textVal = el.getFirstChild().getNodeValue();
			}	
		}

		return textVal;
	}
	private String getBoxAttributeValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if(nl != null && nl.getLength() > 0) {
			Element el = (Element)nl.item(0);
			if(el.getAttribute("name")!=null){
				textVal = el.getAttribute("name");
			}	
		}

		return textVal;
	}
	
	//This method automatically called when you run jetty see applicationContext.xml
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		try{
		appCtx = getApplicationContext(context);
		AccessInfoManager accessInfoManager=(AccessInfoManager)appCtx.getBean("accessInfoManager");
		CustomerFileManager customerFileManager =(CustomerFileManager)appCtx.getBean("customerFileManager");
		InventoryPathManager inventoryPathManager=(InventoryPathManager)appCtx.getBean("inventoryPathManager");
		InventoryDataManager inventoryDataManager=(InventoryDataManager)appCtx.getBean("inventoryDataManager");
		
		inventoryData= new InventoryData();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		String files;
		//ServletContext servletContext = ServletActionContext.getServletContext(); 
		String dataDir = "/home/redskyuser/redint/voxme/survey_email/";
		//dataDir=dataDir.replace("redsky","userData").replace("images","voxmeIntegrationXML");
		File voxmeFolder = new File(dataDir);
		File voxmeFetchedFolder= new File(dataDir.replace("voxme", "voxmeFetched"));
		 if (!voxmeFolder.exists()) {
			 voxmeFolder.mkdirs();
	     }
		 if (!voxmeFetchedFolder.exists()) {
			 voxmeFetchedFolder.mkdirs();
	     }
	     File[] listOfDirectory = voxmeFolder.listFiles(); 	 
		  for (int i = 0; i < listOfDirectory.length; i++) 
		  {	 
			  if (listOfDirectory[i].isDirectory()) 
			   {
				  File[] listOfFiles=listOfDirectory[i].listFiles();
				  
				  for (int z = 0; z < listOfFiles.length; z++) 
				  {
					
					  if (listOfFiles[z].isFile()) 
					   {
					   files = listOfFiles[z].getName();
					       if (files.endsWith(".xml"))
					       {
					          try {					        	  
					      		//Using factory get an instance of document builder
					      		DocumentBuilder db = dbf.newDocumentBuilder();
					      		//parse using builder to get DOM representation of the XML file
					      		Document dom = db.parse(listOfFiles[z].getAbsolutePath());	
					    		Element docEle = dom.getDocumentElement();
					    		NodeList nl = docEle.getElementsByTagName("AccessInfo");
					    		NodeList nl2 = docEle.getElementsByTagName("MovingData");
					    		accessInfo= new AccessInfo();
					    		accessInfo.setId(new Long(((Element)dom.getElementsByTagName("MovingData").item(0)).getAttribute("ID")));
					    		//accessInfo.setCorpID(docEle.getElementsByTagName("EMFID").item(0).getFirstChild().getNodeValue().substring(0,4));
					    		//accessInfo.setCorpID("SSCW");
					    		String EstimatorName = "";
					    		String signatureImagePath="";
					    		NodeList nlGeneralInfo = docEle.getElementsByTagName("GeneralInfo");
					    		if(nlGeneralInfo != null && nlGeneralInfo.getLength() > 0) {
				    				Element el = (Element)nlGeneralInfo.item(0);
				    				EstimatorName=getTextValue(el,"EstimatorName");
				    				signatureImagePath=getTextValue(el,"SurveySignature");
				    				}
					    		
					    		try{
					    		    accessInfo.setCorpID(((Element)dom.getElementsByTagName("OrgID").item(0)).getFirstChild().getNodeValue());
					    			//accessInfo.setCorpID("SSCW");
					    		}catch(Exception e){
					    	     accessInfo.setCorpID("SSCW");
					    		}
					    		if(nl != null && nl.getLength() > 0) {
					    				Element el = (Element)nl.item(0);
					    				Element el2 = (Element)nl.item(1);
					    				accessInfo.setOriginResidenceType(getTextValue(el,"PropertyType"));
					    				accessInfo.setOriginResidenceSize(getTextValue(el,"PropertySize"));
					    				accessInfo.setOriginLongCarry(new Boolean(getTextValue(el,"CarryRequired")));
					    				accessInfo.setOriginCarryDistance(getTextValue(el,"CarryLength"));
					    				accessInfo.setOriginCarrydetails(getTextValue(el,"CarryDescription"));
					    				accessInfo.setOriginStairCarry(new Boolean(getTextValue(el,"StairCarryRequired")));
					    				accessInfo.setOriginStairDistance(getTextValue(el,"StairCarryLength"));
					    				accessInfo.setOriginStairdetails(getTextValue(el,"StairCarryDescription"));
					    				accessInfo.setOriginfloor(getTextValue(el,"Floor"));
					    				accessInfo.setOriginNeedCrane(new Boolean(getTextValue(el,"NeedCrane")));
					    				accessInfo.setOriginElevator(new Boolean(getTextValue(el,"HasElevator")));
					    				accessInfo.setOriginElevatorDetails(getTextValue(el,"ElevatorDetails"));
					    				accessInfo.setOriginElevatorType(getTextValue(el,"ExternalElevatorType"));
					    				accessInfo.setOriginReserveParking(new Boolean(getTextValue(el,"ParkingReservationRequired")));
					    				accessInfo.setOriginParkingType(getTextValue(el,"ParkingType"));
					    				accessInfo.setOriginParkinglotsize(getTextValue(el,"ParkingSpotSize"));
					    				accessInfo.setOriginParkingSpots(getTextValue(el,"NumOfParkingSpots"));
					    				accessInfo.setOriginDistanceToParking(getTextValue(el,"DistanceToParking"));
					    				accessInfo.setOriginShuttleRequired(new Boolean(getTextValue(el,"shuttleCarryRequired")));
					    				accessInfo.setOriginShuttleDistance(getTextValue(el,"ShuttleDistance"));
					    				accessInfo.setOriginAdditionalStopRequired(new Boolean(getTextValue(el,"AdditionalStopRequired")));
					    				accessInfo.setOriginAdditionalStopDetails(getTextValue(el,"AdditionalStop"));
					    				//destination
					    				accessInfo.setDestinationResidenceType(getTextValue(el2,"PropertyType"));
					    				accessInfo.setDestinationResidenceSize(getTextValue(el2,"PropertySize"));
					    				accessInfo.setDestinationLongCarry(new Boolean(getTextValue(el2,"CarryRequired")));
					    				accessInfo.setDestinationCarryDistance(getTextValue(el2,"CarryLength"));
					    				accessInfo.setDestinationCarrydetails(getTextValue(el2,"CarryDescription"));
					    				accessInfo.setDestinationStairCarry(new Boolean(getTextValue(el2,"StairCarryRequired")));
					    				accessInfo.setDestinationStairDistance(getTextValue(el2,"StairCarryLength"));
					    				accessInfo.setDestinationStairdetails(getTextValue(el2,"StairCarryDescription"));
					    				accessInfo.setDestinationfloor(getTextValue(el2,"Floor"));
					    				accessInfo.setDestinationNeedCrane(new Boolean(getTextValue(el2,"NeedCrane")));
					    				accessInfo.setDestinationElevator(new Boolean(getTextValue(el2,"HasElevator")));
					    				accessInfo.setDestinationElevatorDetails(getTextValue(el2,"ElevatorDetails"));
					    				accessInfo.setDestinationElevatorType(getTextValue(el2,"ExternalElevatorType"));
					    				accessInfo.setDestinationReserveParking(new Boolean(getTextValue(el2,"ParkingReservationRequired")));
					    				accessInfo.setDestinationParkingType(getTextValue(el2,"ParkingType"));
					    				accessInfo.setDestinationParkinglotsize(getTextValue(el2,"ParkingSpotSize"));
					    				accessInfo.setDestinationParkingSpots(getTextValue(el2,"NumOfParkingSpots"));
					    				accessInfo.setDestinationDistanceToParking(getTextValue(el2,"DistanceToParking"));
					    				accessInfo.setDestinationShuttleRequired(new Boolean(getTextValue(el2,"shuttleCarryRequired")));
					    				accessInfo.setDestinationShuttleDistance(getTextValue(el2,"ShuttleDistance"));
					    				accessInfo.setDestinationAdditionalStopRequired(new Boolean(getTextValue(el2,"AdditionalStopRequired")));
					    				accessInfo.setDestinationAdditionalStopDetails(getTextValue(el2,"AdditionalStop"));
					    				accessInfo=accessInfoManager.save(accessInfo);	
					    				if(getTextValue(el,"PictureFileName")!=null){
					    					String imageName=getTextValue(el,"PictureFileName");
					    					if((imageName!=null)&&!imageName.equals("")){
					    						String imagesDir ="/home/redsky/voxme/images";
					    						customerFile = customerFileManager.get(accessInfo.getId());
					    						File   savedFile = new File(imagesDir+"/"+customerFile.getSequenceNumber());	    						   					   
					    						 if (!savedFile.exists()) {
					    							 savedFile.mkdirs();
					    						} 
					    						String[] imageNameList=imageName.split(";");
					    						if(imageNameList.length>=1){
					    							for(int p=0;p<imageNameList.length;p++){
					    						    inventoryPath =new InventoryPath();
					    					        inventoryPath.setCorpID(accessInfo.getCorpID());
					    					        inventoryPath.setAccessInfoId(accessInfo.getId());
					    					        inventoryPath.setAccessInfoType("origin");
					    					        inventoryPath.setPath(listOfDirectory[i].getAbsolutePath().replace("voxme", "voxmeFetched")+"/"+ imageNameList[p]);
					    					        inventoryPathManager.save(inventoryPath);
					    						 }
					    						}
					    					}
					    				}
					    				if(getTextValue(el2,"PictureFileName")!=null){
					    					String imageName=getTextValue(el2,"PictureFileName ");
					    					if((imageName!=null)&&!imageName.equals("")){
					    						String imagesDir ="/home/redsky/voxme/images";
					    						customerFile = customerFileManager.get(accessInfo.getId());
					    						File   savedFile = new File(imagesDir+"/"+customerFile.getSequenceNumber());	    						   					   
					    						 if (!savedFile.exists()) {
					    							 savedFile.mkdirs();
					    					     }
					    						String[] imageNameList=imageName.split(";");
					    						if(imageNameList.length>=1){
					    							for(int p=0;p<imageNameList.length;p++){
					    						    inventoryPath =new InventoryPath();
					    					        inventoryPath.setCorpID(accessInfo.getCorpID());
					    					        inventoryPath.setAccessInfoId(accessInfo.getId());
					    					        inventoryPath.setAccessInfoType("dest");
					    					        inventoryPath.setPath(listOfDirectory[i].getAbsolutePath().replace("voxme", "voxmeFetched")+"/" + imageNameList[p]);
					    					        inventoryPathManager.save(inventoryPath);
					    							}
					    						}
					    					}
					    				}
					    			
					    					if((signatureImagePath!=null)&&!signatureImagePath.equals("")){				    						
					    						String[] imageNameList=signatureImagePath.split(";");
					    						if(imageNameList.length>=1){
					    							for(int p=0;p<imageNameList.length;p++){
					    						    inventoryPath =new InventoryPath();
					    					        inventoryPath.setCorpID(accessInfo.getCorpID());
					    					        inventoryPath.setAccessInfoId(accessInfo.getId());
					    					        inventoryPath.setAccessInfoType("sign");
					    					        inventoryPath.setPath(listOfDirectory[i].getAbsolutePath().replace("voxme", "voxmeFetched")+"/" + imageNameList[p]);
					    					        inventoryPathManager.save(inventoryPath);
					    							}
					    						}
					    					}	
					    		}	
					    		NodeList inventoryList = docEle.getElementsByTagName("Piece");
					    		if(inventoryList != null && inventoryList.getLength() > 0) {
					    			for(int p = 0 ; p < inventoryList.getLength();p++) {
					    				Element el = (Element)inventoryList.item(p);
					    				inventoryData= new InventoryData();
					    				inventoryData.setCustomerFileID(new Long(((Element)dom.getElementsByTagName("MovingData").item(0)).getAttribute("ID")));
					    				inventoryData.setRoom(getTextValue(el,"Location"));
					    				inventoryData.setWeight(getTextValue(el,"Weight"));
						    			inventoryData.setCft(getTextValue(el,"Volume"));
					    				if(getTextValue(el,"Destination")!=null){
					    					String mode=getTextValue(el,"Destination");
					    					if(!mode.equals("")){
					    						if(mode.length()>3&&!(mode.substring(0, 3).equalsIgnoreCase("sto"))){
					    							inventoryData.setMode("other");
					    							inventoryData.setOther(mode);
					    						}else{
					    							inventoryData.setMode(mode);
					    						}
					    					}else{
					    						inventoryData.setMode(mode);
					    					}
					    				}
					    				//inventoryData.setMode(getTextValue(el,"Destination"));	    				
					    				NodeList itemList=el.getElementsByTagName("Item");
					    				Element el2=(Element)itemList.item(0);
					    				inventoryData.setAtricle(el2.getAttribute("name"));
					    				inventoryData.setQuantity(getTextValue(el2,"Quantity"));
					    				try{ 
					    					Integer total=(new Integer(inventoryData.getQuantity()))*(new Integer(inventoryData.getCft()));
					    					inventoryData.setTotal(total.toString());
						    				}catch(Exception e){
						    					inventoryData.setTotal("0");
						    				}
					    				try{
					    					inventoryData.setValuation((new Double(getTextValue(el2,"Value"))).intValue());
						    				}catch(Exception e){					    					
						    				}
					    				//inventoryData.setValuation(new Integer(getTextValue(el2,"Value")));
					    				inventoryData.setArticleCondition(getTextValue(el2,"Condition"));
					    				if(getTextValue(el2,"Comment")!=null){
					    					inventoryData.setComment(getTextValue(el2,"Comment"));
					    				}else{
					    					inventoryData.setComment("");
					    					}	    				
					    				NodeList sizeList=el2.getElementsByTagName("Size");
					    				Element el3=(Element)sizeList.item(0);
					    				try{
					    				inventoryData.setWidth((new Double(getTextValue(el3,"Width"))).intValue());
					    				}catch(Exception e){
					    					inventoryData.setWidth(0);
					    				}
					    				try{
					    					inventoryData.setHeight((new Double(getTextValue(el3,"Height"))).intValue());
						    				}catch(Exception e){
						    					inventoryData.setHeight(0);
						    				}
						    			 try{
						    				 inventoryData.setLength((new Double(getTextValue(el3,"Length"))).intValue());
							    				}catch(Exception e){
							    					inventoryData.setLength(0);
							    				}
							    				
							    	if( getBoxAttributeValue(el,"Box")!=null){
							    			if(! getBoxAttributeValue(el,"Box").equals("")){
							    					inventoryData.setType("M");
							    			}else{
							    				inventoryData.setType("A");
							    				}
							    	}else{
						    				inventoryData.setType("A");
						    			}		
					    				//inventoryData.setHeight(new Integer(getTextValue(el3,"Height")));
					    				//inventoryData.setLength(new Integer(getTextValue(el3,"Length")));
					    				inventoryData.setDismantling(new Boolean(getTextValue(el2,"Dismantling")));
					    				inventoryData.setIsValuable(new Boolean(getTextValue(el2,"IsValuable")));
					    				inventoryData.setAssembling(new Boolean(getTextValue(el2,"Assembling")));
					    				inventoryData.setSpecialContainer(new Boolean(getTextValue(el2,"SpecialContainer")));
					    				//inventoryData.setCorpID(docEle.getElementsByTagName("EMFID").item(0).getFirstChild().getNodeValue().substring(0,4));
					    				try{
					    					inventoryData.setCorpID(((Element)dom.getElementsByTagName("OrgID").item(0)).getFirstChild().getNodeValue());
					    					//inventoryData.setCorpID("SSCW");
					    				}catch(Exception e){
					    		    		inventoryData.setCorpID("SSCW");
					    		    		}
					    				inventoryData.setCreatedOn(new Date());
					    				inventoryData.setCreatedBy(EstimatorName+" - XML");
					    				inventoryData=inventoryDataManager.save(inventoryData);
					    				
					    				if(getTextValue(el2,"PictureFileName")!=null){
					    					String imageName=getTextValue(el2,"PictureFileName");
					    					if((imageName!=null)&&!imageName.equals("")){
					    						String imagesDir ="/home/redsky/voxme/images";
					    						customerFile = customerFileManager.get(inventoryData.getCustomerFileID());
					    						File   savedFile = new File(imagesDir+"/"+customerFile.getSequenceNumber());	    						   					   
					    						 if (!savedFile.exists()) {
					    							 savedFile.mkdirs();
					    					     }
					    						String[] imageNameList=imageName.split(";");
					    						if(imageNameList.length>=1){
					    							for(int s=0;s<imageNameList.length;s++){
					    						    inventoryPath =new InventoryPath();
					    					        inventoryPath.setCorpID(inventoryData.getCorpID());
					    					        inventoryPath.setInventoryDataId(inventoryData.getId());
					    					        inventoryPath.setPath(listOfDirectory[i].getAbsolutePath().replace("voxme", "voxmeFetched")+"/" + imageNameList[s]);
					    					        inventoryPathManager.save(inventoryPath);
					    							}
					    							inventoryData.setImagesAvailablityFlag(true);
					    						}
					    					}
					    				}
					    				inventoryData=inventoryDataManager.save(inventoryData);
					    			}
					    		}
					          	}catch(ParserConfigurationException pce) {
					      		pce.printStackTrace();
					      	    }catch(SAXException se) {
					      		se.printStackTrace();
					      	   }catch(IOException ioe) {
					      		ioe.printStackTrace();
					         	  }				      	 
					       }
					     }
					  								  
				  }
				   String str=  listOfDirectory[i].getAbsolutePath();
					str=str.replace("voxme", "voxmeFetched");
					listOfDirectory[i].renameTo(new File(str));
				 }
		   }
             }catch (Exception e) {
			
		}
		System.out.println(">>>>>>>>>>>>>>>>>>survey job Done");
		
	}


	public InventoryData getInventoryData() {
		return inventoryData;
	}


	public void setInventoryData(InventoryData inventoryData) {
		this.inventoryData = inventoryData;
	}


	public AccessInfo getAccessInfo() {
		return accessInfo;
	}


	public void setAccessInfo(AccessInfo accessInfo) {
		this.accessInfo = accessInfo;
	}


	public InventoryPath getInventoryPath() {
		return inventoryPath;
	}


	public void setInventoryPath(InventoryPath inventoryPath) {
		this.inventoryPath = inventoryPath;
	}


	public CustomerFile getCustomerFile() {
		return customerFile;
	}


	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	
	   
}
