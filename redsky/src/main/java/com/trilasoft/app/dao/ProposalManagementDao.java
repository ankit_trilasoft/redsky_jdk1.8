package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;   

import com.trilasoft.app.model.NewsUpdate;
import com.trilasoft.app.model.ProposalManagement;   
  
import java.util.Date;
import java.util.List;

public interface ProposalManagementDao extends GenericDao<ProposalManagement, Long> {
	 public List findDistinct();
	 public List findListByModuleCorpStatus(String module,String corpid,String status , Date approvalDate , Date initiationDate,String ticketNo,Boolean activeStatus);
	 public String findProposalDescForToolTip(Long id, String sessionCorpID);
	 public void autoSaveProposalManagementAjax(String fieldName,String fieldVal,Long serviceId);
	 public List listOnLoad();

	}
