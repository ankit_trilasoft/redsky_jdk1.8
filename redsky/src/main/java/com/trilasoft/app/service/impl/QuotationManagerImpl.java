package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.QuotationDao;
import com.trilasoft.app.model.PartnerQuote;
import com.trilasoft.app.model.Quotation;
import com.trilasoft.app.service.QuotationManager;



public class QuotationManagerImpl extends GenericManagerImpl<Quotation, Long> implements QuotationManager {   
	QuotationDao quotationDao; 
	 
	
	public QuotationManagerImpl(QuotationDao quotationDao) {   
        super(quotationDao);   
        this.quotationDao = quotationDao;   
    } 
    
    public List findMaximumId(){
     	return quotationDao.findMaximumId(); 
     }
    
    public List checkById(Long id) { 
        return quotationDao.checkById(id); 
    }

	public List maxId(String requestedSO) {
		return quotationDao.maxId(requestedSO);
		}


	public List minId(String requestedSO) {
		return quotationDao.minId(requestedSO);
		}

	public List <PartnerQuote> findByQuotation(String quote, String quoteStatus, String sessionCorpID, String vendorCodeSet) {
		return quotationDao.findByQuotation(quote, quoteStatus, sessionCorpID, vendorCodeSet);
	}

	public List checkQuotation(Long quote) {
		return quotationDao.checkQuotation(quote);
	}

	public List<Quotation> getQuotation(Long sID, Long quoteID) {
		return quotationDao.getQuotation(sID, quoteID);
	}

	public List checkQuotation2(Long sID, Long quoteID) {
		return quotationDao.checkQuotation2(sID, quoteID);
	}

	public void submitQuotation(Long quoteID) {
		quotationDao.submitQuotation(quoteID);
		
	}

	public List nextServiceOrder(String requestedSO, Long sid) {
		return quotationDao.nextServiceOrder(requestedSO, sid);
	}

	public List previousServiceOrder(String requestedSO, Long sid) {
		return quotationDao.previousServiceOrder(requestedSO, sid);
	}

	public void processQuotation(Long quoteID) {
		quotationDao.processQuotation(quoteID);
		
	}
	 public List  BookingAgentName(String partnerCode, String corpid, Boolean cmmDmmFlag){
		 return quotationDao.BookingAgentName(partnerCode, corpid, cmmDmmFlag);
	 }
		public List findBookCodeStatus(String corpId, String bookAg){
			return quotationDao.findBookCodeStatus(corpId,bookAg);
		}
	public  Map<String, String> findBookCodeQuotes(String corpId, Long id){
		return quotationDao.findBookCodeQuotes(corpId,id);
	}
	
	public String findBookCodeStatusAjax(String corpId, String bookAg){
		return quotationDao.findBookCodeStatusAjax(corpId, bookAg);
	}

}
