package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table ( name="taskplanning")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class TaskPlanning extends BaseObject{

	private Long id;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private Long serviceOrderId;
	private Long customerFileId;
	private String taskVia;
	private String taskCompany;
	private String taskPlanner;
	private String taskAssignee;
	private String taskLocation;
	private String taskDuration;
	private String taskType;
	private Date taskDate;
	private String taskTimeFrom;
	private String taskTimeTo;
	private Date taskCompleteBefore;
	private String taskComment;
	private String taskCompanyDesc;
	private String taskPlannerDesc;
	private String taskAssigneeDesc;
	private String orgLocId;
	private String destLocId;
	private String taskTypeId;
	private String taskStatus;
	
	/*
	 * (non-Javadoc)
	 * @see org.appfuse.model.BaseObject#toString()
	 * needs to create GUUID for tasks
	 */
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
		.append("id", id).append("corpID", corpID).append("createdBy", createdBy).append("createdOn", createdOn).append("updatedBy", updatedBy).append("updatedOn", updatedOn).append("serviceOrderId", serviceOrderId)
		.append("customerFileId", customerFileId).append("taskVia", taskVia).append("taskCompany", taskCompany).append("taskPlanner", taskPlanner).append("taskAssignee", taskAssignee).append("taskLocation", taskLocation)
		.append("taskDuration", taskDuration).append("taskType", taskType).append("taskDate", taskDate).append("taskTimeFrom", taskTimeFrom)
		.append("taskTimeTo", taskTimeTo).append("taskCompleteBefore", taskCompleteBefore).append("taskComment", taskComment)
		.append("taskCompanyDesc", taskCompanyDesc).append("taskPlannerDesc", taskPlannerDesc).append("taskAssigneeDesc", taskAssigneeDesc)
		.append("orgLocId", orgLocId)
		.append("destLocId", destLocId)
		.append("taskTypeId", taskTypeId)
		.append("taskStatus", taskStatus)
		.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof TaskPlanning))
			return false;
		TaskPlanning castOther = (TaskPlanning) other;
		return new EqualsBuilder()
		.append(id, castOther.id)
		.append(corpID, castOther.corpID)
		.append(createdBy, castOther.createdBy)
		.append(createdOn, castOther.createdOn)
		.append(updatedBy, castOther.updatedBy)
		.append(updatedOn, castOther.updatedOn)
		.append(serviceOrderId, castOther.serviceOrderId)
		.append(customerFileId, castOther.customerFileId).append(taskVia, castOther.taskVia).append(taskCompany, castOther.taskCompany)
		.append(taskPlanner, castOther.taskPlanner).append(taskAssignee, castOther.taskAssignee).append(taskLocation, castOther.taskLocation)
		.append(taskDuration, castOther.taskDuration).append(taskType, castOther.taskType).append(taskDate, castOther.taskDate).append(taskTimeFrom, castOther.taskTimeFrom)
		.append(taskTimeTo, castOther.taskTimeTo).append(taskCompleteBefore, castOther.taskCompleteBefore).append(taskComment, castOther.taskComment)
		.append(taskCompanyDesc, castOther.taskCompanyDesc).append(taskPlannerDesc, castOther.taskPlannerDesc).append(taskAssigneeDesc, castOther.taskAssigneeDesc)
		.append(orgLocId, castOther.orgLocId)
		.append(destLocId, castOther.destLocId)
		.append(taskTypeId, castOther.taskTypeId)
		.append(taskStatus, castOther.taskStatus)
		.isEquals();

	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
		.append(id).append(corpID).append(createdBy).append(createdOn).append(updatedBy).append(updatedOn)
		.append(serviceOrderId).append(customerFileId).append(taskVia).append(taskCompany).append(taskPlanner).append(taskAssignee)
		.append(taskLocation).append(taskDuration).append(taskType).append(taskDate).append(taskTimeFrom).append(taskTimeTo)
		.append(taskCompleteBefore).append(taskComment)
		.append(taskCompanyDesc).append(taskPlannerDesc).append(taskAssigneeDesc)
		.append(orgLocId)
		.append(destLocId)
		.append(taskTypeId)
		.append(taskStatus)
		.toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column
	public Long getCustomerFileId() {
		return customerFileId;
	}

	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	@Column
	public String getTaskVia() {
		return taskVia;
	}

	public void setTaskVia(String taskVia) {
		this.taskVia = taskVia;
	}
	@Column
	public String getTaskCompany() {
		return taskCompany;
	}

	public void setTaskCompany(String taskCompany) {
		this.taskCompany = taskCompany;
	}
	@Column
	public String getTaskPlanner() {
		return taskPlanner;
	}

	public void setTaskPlanner(String taskPlanner) {
		this.taskPlanner = taskPlanner;
	}
	@Column
	public String getTaskAssignee() {
		return taskAssignee;
	}

	public void setTaskAssignee(String taskAssignee) {
		this.taskAssignee = taskAssignee;
	}
	@Column
	public String getTaskLocation() {
		return taskLocation;
	}

	public void setTaskLocation(String taskLocation) {
		this.taskLocation = taskLocation;
	}
	@Column
	public String getTaskDuration() {
		return taskDuration;
	}

	public void setTaskDuration(String taskDuration) {
		this.taskDuration = taskDuration;
	}
	@Column
	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	@Column
	public Date getTaskDate() {
		return taskDate;
	}

	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
	}
	@Column
	public String getTaskTimeFrom() {
		return taskTimeFrom;
	}

	public void setTaskTimeFrom(String taskTimeFrom) {
		this.taskTimeFrom = taskTimeFrom;
	}
	@Column
	public String getTaskTimeTo() {
		return taskTimeTo;
	}

	public void setTaskTimeTo(String taskTimeTo) {
		this.taskTimeTo = taskTimeTo;
	}
	@Column
	public Date getTaskCompleteBefore() {
		return taskCompleteBefore;
	}

	public void setTaskCompleteBefore(Date taskCompleteBefore) {
		this.taskCompleteBefore = taskCompleteBefore;
	}
	@Column
	public String getTaskComment() {
		return taskComment;
	}

	public void setTaskComment(String taskComment) {
		this.taskComment = taskComment;
	}
	@Column
	public String getTaskCompanyDesc() {
		return taskCompanyDesc;
	}

	public void setTaskCompanyDesc(String taskCompanyDesc) {
		this.taskCompanyDesc = taskCompanyDesc;
	}
	@Column
	public String getTaskPlannerDesc() {
		return taskPlannerDesc;
	}

	public void setTaskPlannerDesc(String taskPlannerDesc) {
		this.taskPlannerDesc = taskPlannerDesc;
	}
	@Column
	public String getTaskAssigneeDesc() {
		return taskAssigneeDesc;
	}

	public void setTaskAssigneeDesc(String taskAssigneeDesc) {
		this.taskAssigneeDesc = taskAssigneeDesc;
	}
	@Column
	public String getOrgLocId() {
		return orgLocId;
	}

	public void setOrgLocId(String orgLocId) {
		this.orgLocId = orgLocId;
	}
	@Column
	public String getDestLocId() {
		return destLocId;
	}

	public void setDestLocId(String destLocId) {
		this.destLocId = destLocId;
	}
	@Column
	public String getTaskTypeId() {
		return taskTypeId;
	}

	public void setTaskTypeId(String taskTypeId) {
		this.taskTypeId = taskTypeId;
	}

	/**
	 * @return the taskStatus
	 */
	@Column
	public String getTaskStatus() {
		return taskStatus;
	}

	/**
	 * @param taskStatus the taskStatus to set
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

}
