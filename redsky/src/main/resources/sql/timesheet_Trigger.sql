
DELIMITER $$
create trigger redsky.trigger_add_history_timesheet BEFORE UPDATE on redsky.timesheet
for each row
BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;
   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

  IF ((OLD.workDate <> NEW.workDate) or (OLD.workDate is null and NEW.workDate is not null)
	  or (OLD.workDate is not null and NEW.workDate is null)) THEN
       select CONCAT(fieldNameList,'timesheet.workDate~') into fieldNameList;
       IF(OLD.workDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.workDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;
    	IF(OLD.workDate is not null) THEN
        	select CONCAT(oldValueList,OLD.workDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.workDate is not null) THEN
        	select CONCAT(newValueList,NEW.workDate,'~') into newValueList;
      	END IF;
   END IF;
   
  
   IF (OLD.userName <> NEW.userName ) THEN
       select CONCAT(fieldNameList,'timesheet.userName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.userName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.userName,'~') into newValueList;
   END IF;
   IF (OLD.action <> NEW.action ) THEN
       select CONCAT(fieldNameList,'timesheet.action~') into fieldNameList;
       select CONCAT(oldValueList,OLD.action,'~') into oldValueList;
       select CONCAT(newValueList,NEW.action,'~') into newValueList;
   END IF;
   IF (OLD.ticket <> NEW.ticket ) THEN
       select CONCAT(fieldNameList,'timesheet.ticket~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ticket,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ticket,'~') into newValueList;
   END IF;
   IF (OLD.crewName <> NEW.crewName ) THEN
       select CONCAT(fieldNameList,'timesheet.crewName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.crewName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.crewName,'~') into newValueList;
   END IF;
   IF (OLD.crewType <> NEW.crewType ) THEN
       select CONCAT(fieldNameList,'timesheet.crewType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.crewType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.crewType,'~') into newValueList;
   END IF;
   IF (OLD.shipper <> NEW.shipper ) THEN
       select CONCAT(fieldNameList,'timesheet.shipper~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipper,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipper,'~') into newValueList;
   END IF;
   IF (OLD.crewBillAs <> NEW.crewBillAs ) THEN
       select CONCAT(fieldNameList,'timesheet.crewBillAs~') into fieldNameList;
       select CONCAT(oldValueList,OLD.crewBillAs,'~') into oldValueList;
       select CONCAT(newValueList,NEW.crewBillAs,'~') into newValueList;
   END IF;
    IF ((OLD.beginHours <> NEW.beginHours) or (OLD.beginHours is null and NEW.beginHours is not null)
	  or (OLD.beginHours is not null and NEW.beginHours is null)) THEN
       select CONCAT(fieldNameList,'timesheet.beginHours~') into fieldNameList;
       select CONCAT(oldValueList,OLD.beginHours,'~') into oldValueList;
       select CONCAT(newValueList,NEW.beginHours,'~') into newValueList;
   END IF;
  IF ((OLD.endHours <> NEW.endHours) or (OLD.endHours is null and NEW.endHours is not null)
	  or (OLD.endHours is not null and NEW.endHours is null)) THEN
       select CONCAT(fieldNameList,'timesheet.endHours~') into fieldNameList;
       select CONCAT(oldValueList,OLD.endHours,'~') into oldValueList;
       select CONCAT(newValueList,NEW.endHours,'~') into newValueList;
   END IF;
   IF (OLD.contract <> NEW.contract ) THEN
       select CONCAT(fieldNameList,'timesheet.contract~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
       select CONCAT(newValueList,NEW.contract,'~') into newValueList;
   END IF;
      IF (OLD.distributionCode <> NEW.distributionCode ) THEN
       select CONCAT(fieldNameList,'timesheet.distributionCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.distributionCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.distributionCode,'~') into newValueList;
   END IF;
   IF (OLD.calculationCode <> NEW.calculationCode ) THEN
       select CONCAT(fieldNameList,'timesheet.calculationCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.calculationCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.calculationCode,'~') into newValueList;
   END IF;
   
   IF (OLD.crewEmail <> NEW.crewEmail ) THEN
       select CONCAT(fieldNameList,'timesheet.crewEmail~') into fieldNameList;
       select CONCAT(oldValueList,OLD.crewEmail,'~') into oldValueList;
       select CONCAT(newValueList,NEW.crewEmail,'~') into newValueList;
   END IF;
   
   IF (OLD.integrationTool <> NEW.integrationTool ) THEN
       select CONCAT(fieldNameList,'timesheet.integrationTool~') into fieldNameList;
       select CONCAT(oldValueList,OLD.integrationTool,'~') into oldValueList;
       select CONCAT(newValueList,NEW.integrationTool,'~') into newValueList;
   END IF;
   
   IF (OLD.integrationStatus <> NEW.integrationStatus ) THEN
       select CONCAT(fieldNameList,'timesheet.integrationStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.integrationStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.integrationStatus,'~') into newValueList;
   END IF;
   
      IF ((OLD.regularHours <> NEW.regularHours) or (OLD.regularHours is null and NEW.regularHours is not null)
      or (OLD.regularHours is not null and NEW.regularHours is null)) THEN
        select CONCAT(fieldNameList,'timesheet.regularHours~') into fieldNameList;
        IF(OLD.regularHours is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.regularHours is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.regularHours is not null) then
            select CONCAT(oldValueList,OLD.regularHours,'~') into oldValueList;
        END IF;
        IF(NEW.regularHours is not null) then
            select CONCAT(newValueList,NEW.regularHours,'~') into newValueList;
        END IF;
   END IF;     
    IF ((OLD.overTime <> NEW.overTime) or (OLD.overTime is null and NEW.overTime is not null)
      or (OLD.overTime is not null and NEW.overTime is null)) THEN
        select CONCAT(fieldNameList,'timesheet.overTime~') into fieldNameList;
        IF(OLD.overTime is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.overTime is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.overTime is not null) then
            select CONCAT(oldValueList,OLD.overTime,'~') into oldValueList;
        END IF;
        IF(NEW.overTime is not null) then
            select CONCAT(newValueList,NEW.overTime,'~') into newValueList;
        END IF;
   END IF;
    IF ((OLD.doubleOverTime <> NEW.doubleOverTime) or (OLD.doubleOverTime is null and NEW.doubleOverTime is not null)
      or (OLD.doubleOverTime is not null and NEW.doubleOverTime is null)) THEN
        select CONCAT(fieldNameList,'timesheet.doubleOverTime~') into fieldNameList;
        IF(OLD.doubleOverTime is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.doubleOverTime is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.doubleOverTime is not null) then
            select CONCAT(oldValueList,OLD.doubleOverTime,'~') into oldValueList;
        END IF;
        IF(NEW.doubleOverTime is not null) then
            select CONCAT(newValueList,NEW.doubleOverTime,'~') into newValueList;
        END IF;
   END IF;     
   IF (OLD.regularPayPerHour <> NEW.regularPayPerHour ) THEN
       select CONCAT(fieldNameList,'timesheet.regularPayPerHour~') into fieldNameList;
       select CONCAT(oldValueList,OLD.regularPayPerHour,'~') into oldValueList;
       select CONCAT(newValueList,NEW.regularPayPerHour,'~') into newValueList;
   END IF;
   IF (OLD.overTimePayPerHour <> NEW.overTimePayPerHour ) THEN
       select CONCAT(fieldNameList,'timesheet.overTimePayPerHour~') into fieldNameList;
       select CONCAT(oldValueList,OLD.overTimePayPerHour,'~') into oldValueList;
       select CONCAT(newValueList,NEW.overTimePayPerHour,'~') into newValueList;
   END IF;
   IF (OLD.doubleOverTimePayPerHour <> NEW.doubleOverTimePayPerHour ) THEN
       select CONCAT(fieldNameList,'timesheet.doubleOverTimePayPerHour~') into fieldNameList;
       select CONCAT(oldValueList,OLD.doubleOverTimePayPerHour,'~') into oldValueList;
       select CONCAT(newValueList,NEW.doubleOverTimePayPerHour,'~') into newValueList;
   END IF;
   IF (OLD.paidRevenueShare <> NEW.paidRevenueShare ) THEN
       select CONCAT(fieldNameList,'timesheet.paidRevenueShare~') into fieldNameList;
       select CONCAT(oldValueList,OLD.paidRevenueShare,'~') into oldValueList;
       select CONCAT(newValueList,NEW.paidRevenueShare,'~') into newValueList;
   END IF;
   IF (OLD.differentPayBasis <> NEW.differentPayBasis ) THEN
       select CONCAT(fieldNameList,'timesheet.differentPayBasis~') into fieldNameList;
       select CONCAT(oldValueList,OLD.differentPayBasis,'~') into oldValueList;
       select CONCAT(newValueList,NEW.differentPayBasis,'~') into newValueList;
   END IF;
   IF (OLD.adjustmentToRevenue <> NEW.adjustmentToRevenue ) THEN
       select CONCAT(fieldNameList,'timesheet.adjustmentToRevenue~') into fieldNameList;
       select CONCAT(oldValueList,OLD.adjustmentToRevenue,'~') into oldValueList;
       select CONCAT(newValueList,NEW.adjustmentToRevenue,'~') into newValueList;
   END IF;
      IF (OLD.reasonForAdjustment <> NEW.reasonForAdjustment ) THEN
       select CONCAT(fieldNameList,'timesheet.reasonForAdjustment~') into fieldNameList;
       select CONCAT(oldValueList,OLD.reasonForAdjustment,'~') into oldValueList;
       select CONCAT(newValueList,NEW.reasonForAdjustment,'~') into newValueList;
   END IF;
   IF (OLD.adjustedBy <> NEW.adjustedBy ) THEN
       select CONCAT(fieldNameList,'timesheet.adjustedBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.adjustedBy,'~') into oldValueList;
       select CONCAT(newValueList,NEW.adjustedBy,'~') into newValueList;
   END IF;
   IF (OLD.notes <> NEW.notes ) THEN
       select CONCAT(fieldNameList,'timesheet.notes~') into fieldNameList;
       select CONCAT(oldValueList,OLD.notes,'~') into oldValueList;
       select CONCAT(newValueList,NEW.notes,'~') into newValueList;
   END IF;
   IF (OLD.lunch <> NEW.lunch ) THEN
       select CONCAT(fieldNameList,'timesheet.lunch~') into fieldNameList;
       select CONCAT(oldValueList,OLD.lunch,'~') into oldValueList;
       select CONCAT(newValueList,NEW.lunch,'~') into newValueList;
   END IF;
   
   IF (OLD.warehouse <> NEW.warehouse ) THEN
       select CONCAT(fieldNameList,'timesheet.warehouse~') into fieldNameList;
       select CONCAT(oldValueList,OLD.warehouse,'~') into oldValueList;
       select CONCAT(newValueList,NEW.warehouse,'~') into newValueList;
   END IF;
   IF (OLD.doneForTheDay <> NEW.doneForTheDay ) THEN
       select CONCAT(fieldNameList,'timesheet.doneForTheDay~') into fieldNameList;
       select CONCAT(oldValueList,OLD.doneForTheDay,'~') into oldValueList;
       select CONCAT(newValueList,NEW.doneForTheDay,'~') into newValueList;
   END IF;
   CALL add_tblHistory (OLD.id,"timesheet", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END $$
