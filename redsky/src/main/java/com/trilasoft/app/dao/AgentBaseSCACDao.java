package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.AgentBaseSCAC;

public interface AgentBaseSCACDao extends GenericDao<AgentBaseSCAC, Long>  {
	
	public List getbaseSCACList(String partnerCode, String baseCode, String sessionCorpID);
	
	public List getSCACList(String sessionCorpID);
	
	public List isExisted(String baseCode, String partnerCode, String code, String sessionCorpID);
}
