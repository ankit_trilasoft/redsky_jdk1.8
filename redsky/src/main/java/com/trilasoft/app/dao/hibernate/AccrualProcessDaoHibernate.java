package com.trilasoft.app.dao.hibernate;
 
import java.text.SimpleDateFormat;
import java.util.*;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.AccrualProcessDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccountLine;

public class AccrualProcessDaoHibernate extends GenericDaoHibernate<AccountLine,Long> implements AccrualProcessDao {
	private HibernateUtil hibernateUtil;
	public AccrualProcessDaoHibernate() {   
        super(AccountLine.class);   
    }   
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }

	public List payableReverseAccruedList(Date endDate, String sessionCorpID){
		SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder newEndDate = new StringBuilder( dateformatYYYYMMDD.format( endDate ) );   
        List list1 = new ArrayList();	
		//-------------UPDATE ACCRUED EXPENSES ACTUALIZED SINCE ACCRUAL S/O STILL ACTIVE----------------//
        try{
			String queryString1="update AccountLine set accruePayableReverse = '"+newEndDate+"' where accruePayable is not null and payPostDate is not null and payPostDate <= '"+newEndDate+"' and  actualExpense <> 0 and  accruePayableReverse is null";
			getHibernateTemplate().bulkUpdate(queryString1);
        }
		catch(Exception e){
			System.out.println("ERROR"+ e);
		}
		//-------------UPDATE ACCRUED ENTRIES THAT HAVE BEEN CANCELLED OR CLOSED-----------------//
		List idList2 = this.getSession().createSQLQuery("select a.id from serviceorder s, accountline a where s.id = a.serviceOrderId and a.accruePayable is not null  and a.payPostDate <= '"+newEndDate+"' and s.job not in ('STO','STF','STL')  and a.status=true and a.accruePayableReverse is null and s.status in ('CNCL','DWNLD','CLSD') and s.corpId = '"+sessionCorpID+"' ").list();
		if(idList2!=null && !idList2.isEmpty()){
			Iterator it = idList2.iterator();
			String ids = "";
			while(it.hasNext()){
				ids = ids +", "+ it.next().toString();
			}
			ids = ids.replaceFirst(",", "");
			String queryString2="update AccountLine a  set accruePayableReverse = '"+newEndDate+"' where a.id in ("+ids+")";
			getHibernateTemplate().bulkUpdate(queryString2);
		}
				
		//-------------UPDATE ACCRUED ENTRIES THAT HAVE BEEN DEACTIVATED--------------------//
		List idList3 = this.getSession().createSQLQuery("select  a.id from serviceorder s, accountline a  where s.id = a.serviceOrderId and a.accruePayable is not null and s.job not in ('STO','STF','STL') and a.status is false  and a.accruePayableReverse is null and s.corpId = '"+sessionCorpID+"' ").list();
		if(idList3!=null && !idList3.isEmpty()){
			Iterator it = idList3.iterator();
			String ids = "";
			while(it.hasNext()){
				ids = ids +", "+ it.next().toString();
			}
			ids = ids.replaceFirst(",", "");
			String queryString3="update AccountLine a set accruePayableReverse = '"+newEndDate+"' where a.id in ("+ids+")";
			getHibernateTemplate().bulkUpdate(queryString3);
		}
		
		//-------------UPDATED ACCRUED ENTRIES THAT HAVE MANUALLY FLAGED FOR REVERSAL---//		
		try{
			String queryString4="update AccountLine a set accruePayableReverse = '"+newEndDate+"' where a.accruePayable is not null and a.accruePayableManual = '1'  and accruePayableReverse is null";
			getHibernateTemplate().bulkUpdate(queryString4);
        }
		catch(Exception e){
			System.out.println("ERROR"+ e);
		}
		//-------------QUERY FOR EXTRACT EXCEL---------------------------------------------//
		String extractQuery="select s.companyDivision, s.job, s.shipNumber, a.revisionExpense, a.actualExpense, a.chargeCode, date(b.billcomplete) as 'billComplete', date(b.revenueRecognition) as 'revRecog',if(a.status=true,'Active','Inactive') as 'actStatus', s.status, a.payGl,a.id from accountline a , serviceorder s, billing b where s.id = a.serviceOrderId and  s.id=b.id and b.revenueRecognition >='2013-01-01' and a.accruePayableReverse = '"+newEndDate+"' and s.corpId = '"+sessionCorpID+"' order by s.job, 2 ";
		List list = this.getSession().createSQLQuery(extractQuery).list();
		Iterator it = list.iterator();
		while(it.hasNext()){
				Object []row= (Object [])it.next();
				AccrualProcessDTO dTO=new AccrualProcessDTO();					
				dTO.setCompanyDivision(row[0]);
				dTO.setJob(row[1]);
				dTO.setShipNumber(row[2]);
				dTO.setRevisionExpense(row[3]);
				dTO.setActualExpense(row[4]);				
				dTO.setChargeCode(row[5]);				
				dTO.setBillComplete(row[6]);
				dTO.setRevRecog(row[7]);
				dTO.setActStatus(row[8]);
				dTO.setStatus(row[9]);
				dTO.setPayGl(row[10]);
				dTO.setAccountLineId(row[11]);
				list1.add(dTO);
	       }
		return list1;
	}
	
	public List payableNewAccruals(Date endDate, String sessionCorpID){
		SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder newEndDate = new StringBuilder( dateformatYYYYMMDD.format( endDate ) );   
        List list1 = new ArrayList();	
        
		//-------------UPDATE ACCRUED EXPENSES ACTUALIZED SINCE ACCRUAL S/O STILL ACTIVE----------------//
        List idList = this.getSession().createSQLQuery("select  a.id from serviceorder s, billing b, accountline a  where s.id = a.serviceOrderId and  s.id=b.id and a.accruePayable is null and s.job not in ('STO','STF','STL') and a.chargeCode is not null and a.chargeCode!='' and b.revenueRecognition >='2013-01-01' and b.revenueRecognition <='"+newEndDate+"' and a.revisionExpense<>0 and a.actualExpense=0 and a.status=true and upper(a.actgCode) <> 'TEMP' and a.category <> 'Internal Cost' and s.status not in ('CNCL','DWNLD','CLSD') and s.corpId = '"+sessionCorpID+"' ").list();
		if(idList!=null && !idList.isEmpty()){
			Iterator it = idList.iterator();
			String ids = "";
			while(it.hasNext()){
				ids = ids +", "+ it.next().toString();
			}
			ids = ids.replaceFirst(",", "");
			String queryString="update AccountLine a set a.accruePayable = '"+newEndDate+"', a.updatedBy = 'ACCR_PAY', a.updatedOn = sysdate() where a.id in ("+ids+")";
		    getHibernateTemplate().bulkUpdate(queryString);
		}
       
		
		//-------------QUERY FOR EXTRACT EXCEL---------------------------------------------//
		String extractQuery="select s.companyDivision, s.job, s.shipNumber, a.revisionExpense, a.actualExpense, a.chargeCode, date(b.billcomplete) as 'billComplete', date(b.revenueRecognition) as 'revRecog', a.payGl,a.id from accountline a , serviceorder s, billing b where s.id = a.serviceOrderId and  s.id=b.id and a.chargeCode is not null and a.chargeCode!='' and a.accruePayable = '"+newEndDate+"' and s.corpId = '"+sessionCorpID+"' order by s.job, 2";
		List list = this.getSession().createSQLQuery(extractQuery).list();
		Iterator it = list.iterator();
		while(it.hasNext()){
				Object []row= (Object [])it.next();
				AccrualProcessDTO dTO=new AccrualProcessDTO();					
				dTO.setCompanyDivision(row[0]);
		        dTO.setJob(row[1]);
				dTO.setShipNumber(row[2]);
				dTO.setRevisionExpense(row[3]);
				dTO.setActualExpense(row[4]);				
				dTO.setChargeCode(row[5]);				
				dTO.setBillComplete(row[6]);
				dTO.setRevRecog(row[7]);
				dTO.setPayGl(row[8]);
				dTO.setAccountLineId(row[9]);
				list1.add(dTO);
	       }
		return list1;
	}
	
	public List payableBalance(String sessionCorpID){   
        List list1 = new ArrayList();	
		//-------------QUERY FOR EXTRACT EXCEL---------------------------------------------//
		String extractQuery="select s.companyDivision, s.job, s.shipNumber, a.revisionExpense,a.actualExpense ,a.chargeCode, date(b.billComplete) as 'billComplete', date(b.revenuerecognition) as 'revRecog', a.payGl, b.billToCode, b.billToName, a.vendorCode, a.estimateVendorName as 'vendorName',a.invoiceDate, a.payingStatus, a.payPostDate, a.accruePayable,t.deliveryA,b.personBilling,b.personPayable,a.id from accountline a , serviceorder s, billing b,trackingstatus t where s.id = a.serviceOrderId and  s.id=b.id and  s.id=t.id and a.accruePayable is not null and a.accruePayableReverse is null and b.revenueRecognition >='2013-01-01'  and s.corpId = '"+sessionCorpID+"' order by s.job, 2 ";
		List list = this.getSession().createSQLQuery(extractQuery).list();
		Iterator it = list.iterator();
		while(it.hasNext()){
				Object []row= (Object [])it.next();
				AccrualProcessDTO dTO=new AccrualProcessDTO();					
				dTO.setCompanyDivision(row[0]);
				dTO.setJob(row[1]);
				dTO.setShipNumber(row[2]);
				dTO.setRevisionExpense(row[3]);
				dTO.setActualExpense(row[4]);				
				dTO.setChargeCode(row[5]);				
				dTO.setBillComplete(row[6]);
				dTO.setRevRecog(row[7]);
				dTO.setPayGl(row[8]);				
				dTO.setBillToCode(row[9]);				
				dTO.setBillToName(row[10]);
				dTO.setVendorCode(row[11]);
				dTO.setVendorName(row[12]);			
				dTO.setInvoiceDate(row[13]);
				dTO.setPayingStatus(row[14]);			
				dTO.setPayPostDate(row[15]);
				dTO.setAccruePayable(row[16]);
				dTO.setActualDelivery(row[17]);
				dTO.setPersonBilling(row[18]);
				dTO.setPersonPayable(row[19]);
				dTO.setAccountLineId(row[20]);
				list1.add(dTO);
	       }
		return list1;
	}
	public List receivableReverseAccruedList(Date endDate, String sessionCorpID){
		SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder newEndDate = new StringBuilder( dateformatYYYYMMDD.format( endDate ) );   
        List list1 = new ArrayList();	
		//-------------UPDATE ACCRUED EXPENSES ACTUALIZED SINCE ACCRUAL S/O STILL ACTIVE----------------//
        List idList1 = this.getSession().createSQLQuery("select a.id from  serviceorder s, accountline a where s.id = a.serviceOrderId and a.recPostDate is not null and a.accrueRevenue is not null and a.recPostDate <= '"+newEndDate+"' and s.job not in ('STO','STF','STL')  and a.actualRevenue <> 0  and a.accrueRevenueReverse is null and s.corpId = '"+sessionCorpID+"' ").list();
		if(idList1!=null && !idList1.isEmpty()){
			Iterator it = idList1.iterator();
			String ids = "";
			while(it.hasNext()){
				ids = ids +", "+ it.next().toString();
			}
			ids = ids.replaceFirst(",", "");
			String queryString1="update AccountLine a set accrueRevenueReverse= '"+newEndDate+"' where a.id in ("+ids+")";
			getHibernateTemplate().bulkUpdate(queryString1);
		}
		//-------------UPDATE ACCRUED ENTRIES THAT HAVE BEEN CANCELLED OR CLOSED-----------------//
		List idList2 = this.getSession().createSQLQuery("select a.id from serviceorder s, accountline a where s.id = a.serviceOrderId and a.accrueRevenue is not null  and a.recPostDate <= '"+newEndDate+"' and a.status=true and a.accrueRevenueReverse is null and s.status in ('CNCL','DWNLD','CLSD') and s.corpId = '"+sessionCorpID+"' ").list();
		if(idList2!=null && !idList2.isEmpty()){
			Iterator it = idList2.iterator();
			String ids = "";
			while(it.hasNext()){
				ids = ids +", "+ it.next().toString();
			}
			ids = ids.replaceFirst(",", "");
			String queryString2="update AccountLine a set accrueRevenueReverse= '"+newEndDate+"' where a.id in ("+ids+")";
			getHibernateTemplate().bulkUpdate(queryString2);
		}
				
		//-------------UPDATE ACCRUED ENTRIES THAT HAVE BEEN DEACTIVATED--------------------//
		try{
			String queryString3="update AccountLine a set accrueRevenueReverse= '"+newEndDate+"' where  a.accrueRevenue is not null  and a.status=false and a.accrueRevenueReverse is null";
			getHibernateTemplate().bulkUpdate(queryString3);
		}catch(Exception e){
			System.out.println("ERROR"+ e);
		}
		//-------------UPDATED ACCRUED ENTRIES THAT HAVE MANUALLY FLAGED FOR REVERSAL---//		
		try{
			String queryString4="update AccountLine a set accrueRevenueReverse= '"+newEndDate+"' where a.accrueRevenue is not null  and a.accrueRevenueManual = '1' and a.accrueRevenueReverse is null";
			getHibernateTemplate().bulkUpdate(queryString4);
        }catch(Exception e){
			System.out.println("ERROR"+ e);
		}
		//-------------QUERY FOR EXTRACT EXCEL---------------------------------------------//
		String extractQuery="select  s.companyDivision, s.job, s.shipNumber, a.revisionRevenueAmount, a.actualRevenue ,a.chargeCode ,  date(b.billComplete) as 'billComplete', date(b.revenuerecognition) as 'revRecog', if(a.status = true,'Active','Inactive') as 'actStatus', s.status, a.recgl,a.id from accountline a , serviceorder s, billing b where s.id = a.serviceOrderId and  s.id=b.id and b.revenueRecognition >='2013-01-01' and a.accrueRevenueReverse ='"+newEndDate+"' and s.corpId = '"+sessionCorpID+"' order by s.job, 2";
		List list = this.getSession().createSQLQuery(extractQuery).list();
		Iterator it = list.iterator();
		while(it.hasNext()){
				Object []row= (Object [])it.next();
				AccrualProcessDTO dTO=new AccrualProcessDTO();					
				dTO.setCompanyDivision(row[0]);
				dTO.setJob(row[1]);
				dTO.setShipNumber(row[2]);				
				dTO.setRevisionRevenueAmount(row[3]);
				dTO.setActualRevenue(row[4]);		
				dTO.setChargeCode(row[5]);			
				dTO.setBillComplete(row[6]);
				dTO.setRevRecog(row[7]);
				dTO.setActStatus(row[8]);
				dTO.setStatus(row[9]);
				dTO.setRecGl(row[10]);	
				dTO.setAccountLineId(row[11]);	
				list1.add(dTO);
	       }
		return list1;
	}
	public List receivableNewAccruals(Date endDate, String sessionCorpID){
		SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder newEndDate = new StringBuilder( dateformatYYYYMMDD.format( endDate ) );   
        List list1 = new ArrayList();	
        
		//-------------UPDATE ACCRUED EXPENSES ACTUALIZED SINCE ACCRUAL S/O STILL ACTIVE----------------//
        List idList = this.getSession().createSQLQuery("select a.id from serviceorder s, billing b, accountline a where s.id = a.serviceOrderId and  s.id=b.id and a.accrueRevenue is null and s.job not in ('STO','STF','STL','JVS') and b.revenueRecognition >='2013-01-01' and b.revenueRecognition <='"+newEndDate+"' and a.chargeCode is not null and a.chargeCode!='' and a.revisionRevenueAmount<>0 and (a.actualRevenue=0 or (a.actualRevenue<>0 and a.recPostDate is null)) and a.status=true  and s.status not in ('CNCL','DWNLD','CLSD') and s.corpId = '"+sessionCorpID+"' ").list();
		if(idList!=null && !idList.isEmpty()){
			Iterator it = idList.iterator();
			String ids = "";
			while(it.hasNext()){
				ids = ids +", "+ it.next().toString();
			}
			ids = ids.replaceFirst(",", "");
			String queryString="update AccountLine a set a.accrueRevenue = '"+newEndDate+"', a.updatedBy = 'ACCR_REV', a.updatedOn = sysdate() where a.id in ("+ids+")";
		    getHibernateTemplate().bulkUpdate(queryString);
		}
       
		
		//-------------QUERY FOR EXTRACT EXCEL---------------------------------------------//
		String extractQuery="select s.companyDivision, s.job, s.shipNumber, a.revisionRevenueAmount,a.actualRevenue ,a.chargeCode , date(b.billComplete) as 'billComplete', date(b.revenueRecognition) as 'revRecog', a.recGl,date(a.accrueRevenue) as 'accrueRevenue',a.id from accountline a , serviceorder s, billing b where s.id = a.serviceOrderId and  s.id=b.id and a.chargeCode is not null and a.chargeCode!='' and a.accrueRevenue ='"+newEndDate+"' and s.corpId = '"+sessionCorpID+"' order by s.job, 2";
		List list = this.getSession().createSQLQuery(extractQuery).list();
		Iterator it = list.iterator();
		while(it.hasNext()){
				Object []row= (Object [])it.next();
				AccrualProcessDTO dTO=new AccrualProcessDTO();					
				dTO.setCompanyDivision(row[0]);
				dTO.setJob(row[1]);
				dTO.setShipNumber(row[2]);
				dTO.setRevisionRevenueAmount(row[3]);
				dTO.setActualRevenue(row[4]);				
				dTO.setChargeCode(row[5]);				
				dTO.setBillComplete(row[6]);
				dTO.setRevRecog(row[7]);
				dTO.setRecGl(row[8]);
				dTO.setAccrueRevenue(row[9]);
				dTO.setAccountLineId(row[10]);	
				list1.add(dTO);
	       }
		return list1;
	}
	public List receivableBalance(String sessionCorpID){
	     List list1 = new ArrayList();	
			//-------------QUERY FOR EXTRACT EXCEL---------------------------------------------//
			String extractQuery="select s.companyDivision, s.job, s.shipNumber, a.revisionRevenueAmount,a.actualRevenue ,a.chargeCode , date(b.billComplete) as 'billComplete', date(b.revenueRecognition) as 'revRecog', a.recGl, b.billToCode, b.billToName, a.accrueRevenue,t.deliveryA,b.personBilling,a.id from accountline a , serviceorder s, billing b,trackingstatus t where s.id = a.serviceOrderId and  s.id=b.id and s.id=t.id and a.accrueRevenue is not null and a.accrueRevenueReverse is null and b.revenueRecognition >='2013-01-01' and s.corpId = '"+sessionCorpID+"' order by s.job, 2";
			List list = this.getSession().createSQLQuery(extractQuery).list();
			Iterator it = list.iterator();
			while(it.hasNext()){
					Object []row= (Object [])it.next();
					AccrualProcessDTO dTO=new AccrualProcessDTO();					
					dTO.setCompanyDivision(row[0]);
					dTO.setJob(row[1]);
					dTO.setShipNumber(row[2]);
					dTO.setRevisionRevenueAmount(row[3]);
					dTO.setActualRevenue(row[4]);				
					dTO.setChargeCode(row[5]);			
					dTO.setBillComplete(row[6]);
					dTO.setRevRecog(row[7]);
					dTO.setRecGl(row[8]);				
					dTO.setBillToCode(row[9]);			
					dTO.setBillToName(row[10]);
					dTO.setAccrueRevenue(row[11]);
					dTO.setActualDelivery(row[12]);
					dTO.setPersonBilling(row[13]);
					dTO.setAccountLineId(row[14]);
					list1.add(dTO);
		       }
			return list1;
	}
//	..........End Methods for Accrual Process.............
	
	public  class AccrualProcessDTO {		
			private Object companyDivision; 
			private Object job; 
			private Object shipNumber;
			private Object revisionExpense;
			private Object actualExpense;
			private Object chargeCode;
			private Object billComplete;
			private Object revRecog;
			private Object actStatus;
			private Object status;
			private Object payGl;
			private Object billToCode;
			private Object billToName;
			private Object vendorCode;
			private Object vendorName;
			private Object invoiceDate;
			private Object payingStatus;
			private Object payPostDate;
			private Object accruePayable;
			private Object registrationNumber;
			private Object firstName; 
			private Object lastName;						
			private Object revisionRevenueAmount;			
			private Object recGl;			
			private Object revenueRecognition;
			private Object actualRevenue;
			private Object accrueRevenue;
			private Object actualDelivery;
			private Object personBilling;
			private Object personPayable;
			private Object accountLineId;
			
			public Object getBillToCode() {
				return billToCode;
			}
			public void setBillToCode(Object billToCode) {
				this.billToCode = billToCode;
			}
			public Object getFirstName() {
				return firstName;
			}
			public void setFirstName(Object firstName) {
				this.firstName = firstName;
			}
			public Object getJob() {
				return job;
			}
			public void setJob(Object job) {
				this.job = job;
			}
			public Object getLastName() {
				return lastName;
			}
			public void setLastName(Object lastName) {
				this.lastName = lastName;
			}
			public Object getPayGl() {
				return payGl;
			}
			public void setPayGl(Object payGl) {
				this.payGl = payGl;
			}
			public Object getRecGl() {
				return recGl;
			}
			public void setRecGl(Object recGl) {
				this.recGl = recGl;
			}
			public Object getRegistrationNumber() {
				return registrationNumber;
			}
			public void setRegistrationNumber(Object registrationNumber) {
				this.registrationNumber = registrationNumber;
			}
			public Object getRevenueRecognition() {
				return revenueRecognition;
			}
			public void setRevenueRecognition(Object revenueRecognition) {
				this.revenueRecognition = revenueRecognition;
			}
			public Object getRevisionExpense() {
				return revisionExpense;
			}
			public void setRevisionExpense(Object revisionExpense) {
				this.revisionExpense = revisionExpense;
			}
			public Object getRevisionRevenueAmount() {
				return revisionRevenueAmount;
			}
			public void setRevisionRevenueAmount(Object revisionRevenueAmount) {
				this.revisionRevenueAmount = revisionRevenueAmount;
			}
			public Object getShipNumber() {
				return shipNumber;
			}
			public void setShipNumber(Object shipNumber) {
				this.shipNumber = shipNumber;
			}
			public Object getCompanyDivision() {
				return companyDivision;
			}
			public void setCompanyDivision(Object companyDivision) {
				this.companyDivision = companyDivision;
			}
			public Object getActualExpense() {
				return actualExpense;
			}
			public void setActualExpense(Object actualExpense) {
				this.actualExpense = actualExpense;
			}
			public Object getChargeCode() {
				return chargeCode;
			}
			public void setChargeCode(Object chargeCode) {
				this.chargeCode = chargeCode;
			}
			public Object getBillComplete() {
				return billComplete;
			}
			public void setBillComplete(Object billComplete) {
				this.billComplete = billComplete;
			}
			public Object getRevRecog() {
				return revRecog;
			}
			public void setRevRecog(Object revRecog) {
				this.revRecog = revRecog;
			}
			public Object getActStatus() {
				return actStatus;
			}
			public void setActStatus(Object actStatus) {
				this.actStatus = actStatus;
			}
			public Object getStatus() {
				return status;
			}
			public void setStatus(Object status) {
				this.status = status;
			}
			public Object getBillToName() {
				return billToName;
			}
			public void setBillToName(Object billToName) {
				this.billToName = billToName;
			}
			public Object getVendorCode() {
				return vendorCode;
			}
			public void setVendorCode(Object vendorCode) {
				this.vendorCode = vendorCode;
			}
			public Object getVendorName() {
				return vendorName;
			}
			public void setVendorName(Object vendorName) {
				this.vendorName = vendorName;
			}
			public Object getInvoiceDate() {
				return invoiceDate;
			}
			public void setInvoiceDate(Object invoiceDate) {
				this.invoiceDate = invoiceDate;
			}
			public Object getPayingStatus() {
				return payingStatus;
			}
			public void setPayingStatus(Object payingStatus) {
				this.payingStatus = payingStatus;
			}
			public Object getPayPostDate() {
				return payPostDate;
			}
			public void setPayPostDate(Object payPostDate) {
				this.payPostDate = payPostDate;
			}
			public Object getAccruePayable() {
				return accruePayable;
			}
			public void setAccruePayable(Object accruePayable) {
				this.accruePayable = accruePayable;
			}
			public Object getActualRevenue() {
				return actualRevenue;
			}
			public void setActualRevenue(Object actualRevenue) {
				this.actualRevenue = actualRevenue;
			}
			public Object getAccrueRevenue() {
				return accrueRevenue;
			}
			public void setAccrueRevenue(Object accrueRevenue) {
				this.accrueRevenue = accrueRevenue;
			}
			public Object getActualDelivery() {
				return actualDelivery;
			}
			public void setActualDelivery(Object actualDelivery) {
				this.actualDelivery = actualDelivery;
			}
			public Object getPersonBilling() {
				return personBilling;
			}
			public void setPersonBilling(Object personBilling) {
				this.personBilling = personBilling;
			}
			public Object getPersonPayable() {
				return personPayable;
			}
			public void setPersonPayable(Object personPayable) {
				this.personPayable = personPayable;
			}
			public Object getAccountLineId() {
				return accountLineId;
			}
			public void setAccountLineId(Object accountLineId) {
				this.accountLineId = accountLineId;
			}			
	 }
}
