<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>
<head>

<title>Order Initiation Detail</title>
<meta name="heading"
	content="Order Initiation Detail" />

<style type="text/css">	
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
.upper-case{
	text-transform:capitalize;
}
</style>

<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <script type="text/javascript">
function openadd()
{
	
animatedcollapse.ontoggle=function($, divobj, state){
	
	if (divobj.id=="address"){ //if "peter" DIV is being toggled
				  if (state=="block"){ //if div is expanded
		   animatedcollapse.show("additional");
		}
	 else{
	      animatedcollapse.hide("additional");
		 }
					  
	
	}
}
}
</script>
<script type="text/javascript"> 
function titleCase(element){
	var txt=element.value; var spl=txt.split(" "); 
	var upstring=""; for(var i=0;i<spl.length;i++){ 
	try{ 
	upstring+=spl[i].charAt(0).toUpperCase(); 
	}catch(err){} 
	upstring+=spl[i].substring(1, spl[i].length); 
	upstring+=" ";   
	} 
	element.value=upstring.substring(0,upstring.length-1); 
}
</script> 
<script type="text/javascript"> 
    function validateEmail(target){
    	   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		   var address = target.value;
		   if(address!=''){
		    if(reg.test(address) == false) {		 
		      alert('Invalid Email Address');
		      target.value='';
		      return false;
		    } 
		   }  
      } 
</script>
<!-- Modification closed here -->
<script language="JavaScript" type="text/javascript">

	function setEntitleValue(){
		var entitle = document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value;
		if(entitle.length > 0){
			entitle = entitle.split('_?@').join(' '); 
			entitle = entitle.split('#?@');

			 for ( var i = 0; i < entitle.length; i++ ) {
				 if(entitle[i].length >0){
				 	document.getElementById(entitle[i]).checked=true
				 }
			 }
		}
	}

	
 function getValue(str) {
	 str=str.split(' ').join('_?@'); 
	 var partnerEnti = document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value;
	 if(partnerEnti.length == 0){
		 document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = str+'#?@'+partnerEnti;
	 }else {
		 var currentTagTokens = partnerEnti.split( "#?@" );
		 var existingTags='';
		 var isExist = false;
		  for ( var i = 0; i < currentTagTokens.length; i++ ) {
		    if(currentTagTokens[i] != str){
			    if(currentTagTokens[i].length >0){
		    		existingTags = currentTagTokens[i]+'#?@'+existingTags;
			    }
		    }else{
		    	isExist = true;
		    }
		  }
		  if(isExist == false){
		  	document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = existingTags+'#?@'+str;
		  }else{
			  document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = existingTags;
		  }
	 }
 }
 function openOriginCompanyCodePopUp(){
		window.openWindow('openOrderInitiationBillToCode.html?OADAAccountPortalValidation=yes&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.originCompany&fld_code=customerFile.originCompanyCode');
	}
 function openDestinationCompanyCodePopUp(){
		window.openWindow('openOrderInitiationBillToCode.html?OADAAccountPortalValidation=yes&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.destinationCompany&fld_code=customerFile.destinationCompanyCode');
	}
 function findIkeaPartnerDetails(e, t, n,s) { 
		var $j = jQuery.noConflict();
		var o = s.which ? s.which : s.keyCode;
		if(o != 9){
			var u = document.getElementById(e).value;
			if(u.length <= 3) {
				document.getElementById(t).value = ""
			}
			if(u.length >= 3) {
				$j.get("ikeaOrderPartnerDetailsAutocompleteAjax.html?ajax=1&OADAAccountPortalValidation=yes&decorator=simple&popup=true", {
					partnerNameAutoCopmlete: u,
					partnerNameId: e,
					paertnerCodeId: t,
					autocompleteDivId: n
				}, function(e) {
					document.getElementById(n).style.display = "block";
					$j("#" + n).html(e)
				})
			} else {
				document.getElementById(n).style.display = "none"
			}
		} else {
			document.getElementById(n).style.display = "none"
		}
	}
 function viewPartnerDetailsForBillToCode(position,codeId) {
	    var partnerCode = "";
	    var originalCorpID='${customerFile.corpID}';
	    partnerCode =  document.getElementById(codeId).value;
	    var url="viewPartnerDetailsForBillTo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&sessionCorpID=" + encodeURI(originalCorpID);
	    ajax_showTooltip(url,position);
	    return false
	}
 function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
		lastName=lastName.replace("~","'");
		document.getElementById(partnerNameId).value=lastName;
		document.getElementById(paertnerCodeId).value=partnercode;
		document.getElementById(autocompleteDivId).style.display = "none";
		if(paertnerCodeId=='billToCodeId'){
			findBillToCode();		
		}
		else
			{
		findOrderInitiationBillToCode(autocompleteDivId);
			}
	}
 function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
		document.getElementById(autocompleteDivId).style.display = "none";
		if(document.getElementById(paertnerCodeId).value==''){
			document.getElementById(partnerNameId).value="";	
		}
	}
 function findOriginCompanyCode(){
	 
		var bCode = '';
		bCode = document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value;
		bCode=bCode.trim();
		if(bCode!='') {
		    var url="findOrderInitiationBillToCode.html?ajax=1&OADAAccountPortalValidation=yes&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		    http7771.open("GET", url, true); 
		    http7771.onreadystatechange = function(){findOriginCompanyCodeHttp();};
		    http7771.send(null);	     
	     }else{
	    	
	    		document.forms['customerFileForm'].elements['customerFile.originCompany'].value =""; 
	    	
	     }
		
	 }
	function findOriginCompanyCodeHttp(){
	if (http7771.readyState == 4){    	
		var results = http7771.responseText
		results = results.trim();
		results=results.replace("[","");
	    results=results.replace("]","");
		if(results.length >1){
			
				document.forms['customerFileForm'].elements['customerFile.originCompany'].value = results;
			
		}else{
			var billToCode = document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value;
	    		alert("You are not authorize to select Origin Company code "+billToCode+ ".")
	    		document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value="${customerFile.originCompanyCode}";
	    		document.forms['customerFileForm'].elements['customerFile.originCompany'].value="${customerFile.originCompany}";
			}
		}
	}
	function findDestinationCompanyCode(){ 
		var bCode = '';
		bCode = document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value;
		bCode=bCode.trim();
		if(bCode!='') {
		    var url="findOrderInitiationBillToCode.html?ajax=1&OADAAccountPortalValidation=yes&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		    http8881.open("GET", url, true); 
		    http8881.onreadystatechange = function(){findDestinationCompanyCodeHttp();};
		    http8881.send(null);	     
	     }else{
	    	
	    		document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value =""; 
	    	
	     } 
	 }
	function findDestinationCompanyCodeHttp(){
	if (http8881.readyState == 4){    	
		var results = http8881.responseText
		results = results.trim();
		results=results.replace("[","");
	    results=results.replace("]","");
		if(results.length >1){
			
				document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value = results;
			
		}else{
			var billToCode = document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value;
	    		alert("You are not authorize to select Destination Company code "+billToCode+ ".")
	    		document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value="${customerFile.destinationCompanyCode}";
	    		document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value="${customerFile.destinationCompany}";
			}
		}
	}
</script>
<script type="text/javascript">
	
</script>
<script type="text/javascript" src="scripts/prototype.js"></script>
<script type="text/javascript" src="scripts/effects.js"></script>
<script type="text/javascript" src="scripts/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
window.onload = function() {
	<% int count=0; %>
	getFamilysize();
	setEntitleValue();
var fieldName = document.forms['customerFileForm'].elements['field'].value;
var fieldName1 = document.forms['customerFileForm'].elements['field1'].value;
	if(fieldName!=''){
		document.forms['customerFileForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
		document.forms['customerFileForm'].elements[fieldName1].className = 'rules-textUpper';
	}
}  

function focusDate(target){
	document.forms['customerFileForm'].elements[target].focus();
}
function EmailPortDate(){
	var EmailPort = document.forms['customerFileForm'].elements['customerFile.email'].value;
	document.forms['customerFileForm'].elements['customerFileemail'].value = EmailPort;
	if(document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value==''){
		document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value=EmailPort;
	}	
}

</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script language="JavaScript" type="text/javascript">
	var cal = new CalendarPopup(); 
	cal.showYearNavigation(); 
	cal.showYearNavigationInput();	
function myDate() {
	var HH2;
	var HH1;
	var MM2;
	var MM1;
	var tim1=document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
	var tim2=document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
		tim1=tim1.replace(":","");
		tim2=tim2.replace(":","");
	if((tim1.substring(0,tim1.length-2)).length == 1){
		HH1 = '0'+tim1.substring(0,tim1.length-2);
	}else {
		HH1 = tim1.substring(0,tim1.length-2);
	}
	MM1 = tim1.substring(tim1.length-2,tim1.length);
	if((tim2.substring(0,tim1.length-2)).length == 1){
		HH2 = '0'+tim2.substring(0,tim2.length-2);
	}else {
		HH2 = tim2.substring(0,tim2.length-2);
	}
	MM2 = tim2.substring(tim2.length-2,tim2.length);
	document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = HH1 + ':' + MM1;
	document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = HH2 + ':' + MM2;

	var f = document.getElementById('customerFileForm'); 
	f.setAttribute("autocomplete", "off"); 
} 

function setStatas(){
	if(document.forms['customerFileForm'].elements['customerFile.id'].value ==''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
		
		if(job=='UVL' || job=='MVL' || job=='DOM' || job=='LOC' || job=='OFF' || job=='VAI' || job=='STO' || job=='MLL' || job=='HVY'){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
			
		}
	}	
} 

var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;

function autoPopulate_customerFile_originCountry(targetElement) {		
 	var oriCountry = targetElement.value;
 	var countryCode="";
	<c:forEach var="entry" items="${countryCod}">
	if(oriCountry=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
    var enbState = '${enbState}';
	var index = (enbState.indexOf(oriCountry)> -1);
	if(index != ''){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
			if(oriCountry == 'United States'){
			setTimeout(function() {document.forms['customerFileForm'].elements['customerFile.originZip'].focus(); }, 50);
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.originState'].value = '';
			autoPopulate_customerFile_originCityCode(countryCode,'special');
		}
}

function requiredOriginState(){
    var originCountry = document.forms['customerFileForm'].elements['customerFile.originCountry'].value; 
	var r2=document.getElementById('originStateNotRequired');
	var r1=document.getElementById('originStateRequired');
	if(originCountry == 'United States' || originCountry == 'Canada' || originCountry == 'India'){	
	    var enbState = '${enbState}';
		var index = (enbState.indexOf(originCountry)> -1);
		if(index != ''){
		r1.style.display = 'block';
		r2.style.display = 'none';
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}else{	
		r1.style.display = 'none';
		r2.style.display = 'block';		
	}
}

	function autoPopulate_customerFile_destinationCountry(targetElement) {   
		var dCountry = targetElement.value;
		var countryCode="";
		<c:forEach var="entry" items="${countryCod}">
		if(dCountry=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
	    var enbState = '${enbState}';
		var index = (enbState.indexOf(dCountry)> -1);
		if(index != ''){
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;			
			if(dCountry == 'United States'){
				setTimeout(function() { document.forms['customerFileForm'].elements['customerFile.destinationZip'].focus(); }, 50);
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.destinationState'].value = '';
			autoPopulate_customerFile_destinationCityCode(countryCode,'special');
		}
	}

	function requiredDestinState(){
	    var destinCountry = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value; 
		var r2=document.getElementById('destinationStateNotRequired');
		var r1=document.getElementById('destinationStateRequired');
		if(destinCountry == 'United States' || destinCountry == 'Canada' || destinCountry == 'India'){	
		    var enbState = '${enbState}';
			var index = (enbState.indexOf(destinCountry)> -1);
			if(index != ''){
			r1.style.display = 'block';
			r2.style.display = 'none';
			}else{	
				r1.style.display = 'none';
				r2.style.display = 'block';		
			}
		}else{	
			r1.style.display = 'none';
			r2.style.display = 'block';		
		}
	}
	
	function autoPopulate_customerFile_destinationCityCode(targetElement, w) {
	var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
			if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
			}else{
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+','+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
			}
		}
	}
	function autoPopulate_customerFile_originCityCode(targetElement, w) {
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};

		if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != '')
		{
			if(document.forms['customerFileForm'].elements['customerFile.originState'].value != '')
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+','+document.forms['customerFileForm'].elements['customerFile.originState'].value;
				
			}
			else
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
				
			}
		}
	}
	function autoPopulate_customerFile_statusDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['customerFile.statusDate'].value=datam;
	}
	function autoPopulate_customerFile_approvedDate(targetElement, w) {
		
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['customerFile.billApprovedDate'].value=datam;
	}	
	function openHomeCountryLocation(){
	    var city = document.forms['customerFileForm'].elements['customerFile.homeCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
        var zip = "";
        var address = "";
        if(country == 'United States')
        {
        	//address = document.forms['customerFileForm'].elements['customerFile.originAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        }
        else
        {
        	//address = document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        }
        address = address.replace("#","-");
        var state = "";
       
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	 
   function openOriginLocation() {
        var city = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
        var zip = document.forms['customerFileForm'].elements['customerFile.originZip'].value;
        var address = "";
      /*  if(country == 'United States')
        { Commented out for ticket number: 7022 */
        	address = document.forms['customerFileForm'].elements['customerFile.originAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
       /*  }
        else
        {
        	address = document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        } Commented out for ticket number: 7022 */
        address = address.replace("#","-");
        var state = document.forms['customerFileForm'].elements['customerFile.originState'].value;
       
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	 function openDestinationLocation() {
        var city = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
        var zip = document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
       	var address = "";
        /*if(country == 'United States')
        { Commented out for ticket number: 7022 */
        	address = document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
       /* }
        else
        {
        	address = document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
        } Commented out for ticket number: 7022 */
       	address = address.replace("#","-");
        var state = document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
    	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	function generatePortalId() {
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked = true ;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.secondaryEmailFlag'].checked;
		var emailID = document.forms['customerFileForm'].elements['customerFile.email'].value;
		if(checkPortalBox==true && emailID !='')
		{
		var emailID2 = document.forms['customerFileForm'].elements['customerFile.email2'].value;
		emailID=emailID+','+emailID2;
		}
		var emailTypeFlag=false;
		var emailTypeVOERFlag=false;
		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeflag">
			emailTypeFlag = true;
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeVOERflag">
		  emailTypeVOERFlag=true;
	    </configByCorp:fieldVisibility>
		var portalIdName = document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value;
		
		var checkPortalStatus=document.forms['customerFileForm'].elements['customerFile.status'].value;
		if(checkPortalStatus !='CNCL' && checkPortalStatus !='CLOSED'){
		if(emailID!='')
		{
		if(portalIdName!=''){
		window.open('sendMail.html?id=${customerFile.id}&emailTo='+emailID+'&userID='+portalIdName+'&emailTypeFlag='+emailTypeFlag+'&emailTypeVOERFlag='+emailTypeVOERFlag+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
		}
		else{
		alert("Please create ID for Customer Portal");
		}
		}else
		{
			alert("Please enter primary email");
			document.forms['customerFileForm'].elements['customerFile.email'].focus();
		}
		}
		else{
		alert("You can't Reset password for Customer Portal ID because job is Closed/Cancelled");
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked=false;
		}
	} 
	
	
	function updateOriginSoAdd() {
	    var agree = confirm("Do you want to update address in corresponding S/Os? click OK to proceed or Cancel.");
	    if(agree){
        javascript:openWindow('changeOriginSoAdd.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;	    
	    }
		} 
	function updateOriginAddSoAndTkt() {
	var agree = confirm("Do you want to update address in corresponding S/Os and Tickets? click OK to proceed or Cancel.");
	if(agree){
		javascript:openWindow('updateOriginAddSoAndTkt.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;
		} 
		}
	function updateDestinAddSo() {
	    var agree = confirm("Do you want to update address in corresponding S/Os? click OK to proceed or Cancel.");
	    if(agree){
		javascript:openWindow('updateDestinAddSo.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;
		}
		}
	function updateDestinAddSoAndTkt() {
	var agree = confirm("Do you want to update address in corresponding S/Os and Tickets? click OK to proceed or Cancel.");
	if(agree){
		javascript:openWindow('updateDestinAddSoAndTkt.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;
		}
		}  
         

function notExists(){
	alert("The customer information has not been saved yet, please save customer information to continue");
} 

</script>

<SCRIPT LANGUAGE="JavaScript"> 



function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value;
     
      if(targetField=='OZ'){
       var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
        if(document.forms['customerFileForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }else{
        
         }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
          if( document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             }else{
        
             }
        }  
      }  
  

function findCityState(targetElement,targetField){ 
     var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
     }
     if(zipCode!='' && countryCode=='USA'){
     var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
     }  }
function handleHttpResponseCityState(targetField){
             if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                resu = resu.split(",");
				for(var i = 0; i < resu.length+1; i++) {
                var res = resu[i];
                res = res.split("#");
                if(targetField=='OZ'){
	           	if(res[3]=='P') {	            
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].value=res[0];
	           	 document.forms['customerFileForm'].elements['customerFile.originState'].value=res[2];
	            if (document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=='') {
	           	 document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=(res[4].substring(0,19));
	             } 
	              //document.getElementById('zipCodeList').style.display = 'none';
	             } else if(res[3]!='P') {
	             //document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].value=res[0];
	           	 document.forms['customerFileForm'].elements['customerFile.destinationState'].value=res[2];
	            if (document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=='') {
	           	 document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=(res[4].substring(0,19));
	           }
	            //document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(res[3]!='P') {
	           	//document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].focus();
	           	} } } else { } }
// End Of Method 

function getOriginCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
}
function getDestinationCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpDestinationCountryName;
    http4.send(null);
}

function httpDestinationCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='N';
 					}try{	
 					document.forms['customerFileForm'].elements['customerFile.destinationCountry'].select();
 					}catch (e){}
				}else{
                     
                 }
             }
        }

function getState(targetElement, calledOnLoad) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}
	
function getDestinationState(targetElement){
	var country = targetElement.value; 
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6;
     http3.send(null);
}
function getDestinationStateReset(destinationAbc){
	var countryCode = destinationAbc;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6666666666;
     http3.send(null);
}
function getOriginStateReset(originAbc) {
	 var countryCode = originAbc;
	 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse555555;
     httpState.send(null);
}



String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}  

function handleHttpResponseCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res=results.split('#');
                if(res.length>=1){
                	document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = res[0]; 
                	if(res[1]=='Y'){
                	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y';				
				 	}else{
				 	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='N';
				 	}try{
				 	document.forms['customerFileForm'].elements['customerFile.originCountry'].select();
				 	}catch (e){}		     	 
 				}else{
                     
                 }
             }
}
        
 function handleHttpResponse5(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("originState").value = '';
					}
					else{ document.getElementById("originState").value == '${customerFile.originState}';
					}
             }
        }  
   function handleHttpResponse555555(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("originState").value = '${customerFile.originState}';
             }
        }       
function handleHttpResponse6(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("destinationState").value = '';
					}
					else{ document.getElementById("destinationState").value == '${customerFile.destinationState}';
					   }
        }
}  

function handleHttpResponse6666666666(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("destinationState").value = '${customerFile.destinationState}';
        }
}          


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
                  
        
	var http2 = getHTTPObject();
	var http50 = getHTTPObject50();
	var http51 = getHTTPObject51();
    var http3 = getHTTPObject1();
	var http4 = getHTTPObject3();
	var http5 = getHTTPObject();
	var http6 = getHTTPObject2();
    var http8 = getHTTPObject8();
    var http9 = getHTTPObject9();
    var http7771 = getHTTPObject71();
    var http8881 = getHTTPObject81();
    var httpState = getHTTPObjectState()
    var http22 = getHTTPObject22();
    var http33 = getHTTPObject33();
    var http444 = getHTTPObject33();
    var httpBilltoCode = getHTTPObjectBilltoCode();
    var httpPortalAccess = getHTTPObjectPortalAccess();
    function getHTTPObject71(){
        var xmlhttp;
        if(window.XMLHttpRequest){
            xmlhttp = new XMLHttpRequest();
        }else if (window.ActiveXObject){
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            if (!xmlhttp){
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        return xmlhttp;
    }
    function getHTTPObject81(){
        var xmlhttp;
        if(window.XMLHttpRequest){
            xmlhttp = new XMLHttpRequest();
        }else if (window.ActiveXObject){
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            if (!xmlhttp){
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        return xmlhttp;
    }
    function getHTTPObjectPortalAccess()
    {
        var xmlhttp;
        if(window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else if (window.ActiveXObject)
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            if (!xmlhttp)
            {
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        return xmlhttp;
    }
function getHTTPObjectBilltoCode()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getHTTPObject22()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject50()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject51()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    function getHTTPObject9()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

 var http555 = getHTTPObject555();   
function getHTTPObject555()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
    

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject2(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject3(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
 function getHTTPObject8(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
function changeStatus(){
	document.forms['customerFileForm'].elements['formStatus'].value = '1';
}  
     
function sendEmail1(target){
     	var originEmail = target;
   		var subject = 'C/F# ${customerFile.sequenceNumber}';
   		subject = subject+" ${customerFile.firstName}";
   		subject = subject+" ${customerFile.lastName}";
   		
		var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
} 

function openStandardAddressesPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
if((document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value!="Accepted" )&&(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value!="Rejected")&& (document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value!="Submitted" )){ 
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.originMobile&fld_ninthDescription=customerFile.originCity&fld_eigthDescription=customerFile.originFax&fld_seventhDescription=customerFile.originHomePhone&fld_sixthDescription=customerFile.originDayPhone&fld_fifthDescription=customerFile.originZip&fld_fourthDescription=stdAddOriginState&fld_thirdDescription=customerFile.originCountry&fld_secondDescription=customerFile.originAddress3&fld_description=customerFile.originAddress2&fld_code=customerFile.originAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="originAddSection";
}
}
function openStandardAddressesDestinationPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
if((document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value!="Accepted" )&&(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value!="Rejected")&& (document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value!="Submitted" )){ 
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.destinationMobile&fld_ninthDescription=customerFile.destinationCity&fld_eigthDescription=customerFile.destinationFax&fld_seventhDescription=customerFile.destinationHomePhone&fld_sixthDescription=customerFile.destinationDayPhone&fld_fifthDescription=customerFile.destinationZip&fld_fourthDescription=stdAddDestinState&fld_thirdDescription=customerFile.destinationCountry&fld_secondDescription=customerFile.destinationAddress3&fld_description=customerFile.destinationAddress2&fld_code=customerFile.destinationAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="destinAddSection";
}
}  

var http52 = getHTTPObject52();
	function getHTTPObject52()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http53 = getHTTPObject53();
	function getHTTPObject53()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http54 = getHTTPObject54();
	function getHTTPObject54()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http55 = getHTTPObject55();
	function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpRef = getHTTPObjectRef();
function getHTTPObjectRef(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpRefSale = getHTTPObjectRefSale();
function getHTTPObjectRefSale(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}




  
var httpJob = getHTTPObjectJob();
function getHTTPObjectJob(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}   


</script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
   
<script type="text/javascript">
animatedcollapse.addDiv('address', 'fade=1,hide=1' )
animatedcollapse.addDiv('alternative', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('billing', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('entitlement', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('cportal', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('family', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('benefits', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('hr', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('additional', 'fade=1,hide=1')
animatedcollapse.addDiv('employee', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('transferee', 'fade=1,hide=0,show=1')


animatedcollapse.init()

function cheackVisaRequired()
{   

      var customerFileStatus = document.forms['customerFileForm'].elements['customerFile.visaRequired'].value; 
      if(customerFileStatus=='Yes' ) { 
        document.getElementById("hidVisaStatusReg").style.display="block";
	    document.getElementById("hidVisaStatusReg1").style.display="block"; 
	 }else{
	    document.getElementById("hidVisaStatusReg").style.display="none";
        document.getElementById("hidVisaStatusReg1").style.display="none";
         
     } 
}

function cheackQuotationStatus(temp)
{  

      
      var QuotationStatus = temp; 
      if(QuotationStatus=='QR' ) {
      if(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value=='Submitted'){ 
        document.getElementById("hidQuotationStatus").style.display="block"; 
	 }else{
	    document.getElementById("hidQuotationStatus").style.display="none"; 
	 }
	 }
	 else{
	    document.getElementById("hidQuotationStatus").style.display="none"; 
     } 
     
}
function selectColumn(targetElement) 
   {  
   var rowId=targetElement.value;  
   if(targetElement.checked)
     {
      var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' + rowId;
      document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
      }
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
     var userCheckStatus=document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( rowId , '' );
     document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
     } 
    }


function checkColumn(){
var userCheckStatus ="";
  userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value; 
if(userCheckStatus!=""){ 
document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value="";
var column = userCheckStatus.split(",");  
for(i = 0; i<column.length ; i++)
{    
     var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;  
     var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' +column[i];   
     document.getElementById(column[i]).checked=true;
} 
}
if(userCheckStatus==""){

}
}
 function allReadOnly(temp){ 
     if((document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value=="Accepted" )||(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value=="Rejected")|| (document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value=="Submitted" )){ 
        var numOfElements = document.forms['customerFileForm'].elements.length;  
        for(var i=0; i<numOfElements;i++)
        {
         var textName = document.forms['customerFileForm'].elements[i].name;  
         document.forms['customerFileForm'].elements[i].disabled=true; 
         }
     }
     var QuotationStatus=temp;  
     if(QuotationStatus=='QR' ) {
      if(document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value=='Submitted'){ 
        document.forms['customerFileForm'].elements['acceptQuote'].disabled=false; 
        document.forms['customerFileForm'].elements['rejectQuote'].disabled=false; 
	 }
	 }
     if((document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value=="Submitted" )){ 

     if(document.images) {
 	    var totalImages = document.images.length;
 	    	for (var i=0;i<totalImages;i++) {
 					if(document.images[i].src.indexOf('calender.png')>0) { 
 						var el = document.getElementById(document.images[i].id);  
 						document.images[i].src = 'images/navarrow.gif'; 
 						if((el.getAttribute("id")).indexOf('trigger')>0){ 
 							el.removeAttribute('id');
 						}
 					}
 					if(document.images[i].src.indexOf('open-popup.gif')>0){  
 						var el = document.getElementById(document.images[i].id); 
 						try{
 							el.onclick = false;
 						}catch(e){}
 					    	document.images[i].src = 'images/navarrow.gif';
 					}
 					if(document.images[i].src.indexOf('notes_empty1.jpg')>0) {
 						var el = document.getElementById(document.images[i].id);
 						try{
 							el.onclick = false;
 						}catch(e){}
 				        	document.images[i].src = 'images/navarrow.gif';
 					}
 					if(document.images[i].src.indexOf('notes_open1.jpg')>0) {
 						var el = document.getElementById(document.images[i].id);
 						try{
 							el.onclick =false;
 						}catch(e){}
 							document.images[i].src = 'images/navarrow.gif';
 					} 				
 					if(document.images[i].src.indexOf('images/nav')>0) {
 						var el = document.getElementById(document.images[i].id);
 						try{
 							el.onclick = false;
 						}catch(e){}
 							document.images[i].src = 'images/navarrow.gif';
 					}
 					if(document.images[i].src.indexOf('images/image')>0) {
 						var el = document.getElementById(document.images[i].id);
 						try{
 							el.onclick = false;
 						}catch(e){}
 							document.images[i].src = 'images/navarrow.gif';
 					}
 					if(document.images[i].src.indexOf('email_small.gif')>0) {
 						var el = document.getElementById(document.images[i].id);
 						try{
 							el.onclick =false;
 						}catch(e){}
 							document.images[i].src = 'images/navarrow.gif';
 					}
 					/* if(document.images[i].src.indexOf('globe.png')>0) {
 						var el = document.getElementById(document.images[i].id);
 						try{
 							el.onclick =false;
 						}catch(e){}
 							document.images[i].src = 'images/navarrow.gif';
 					} */
 				} 
 		 	}}
     
    }
  function updateOrderInitiationStatus(temp) { 
     var quotationStatus=temp;
     var id=document.forms['customerFileForm'].elements['customerFile.id'].value;
     window.location.href="updateOrderInitiationStatus.html?id="+id+"&quotationStatus="+quotationStatus;
      
 } 
function checkAllRelo(){ 
var len = document.forms['customerFileForm'].elements['checkV'].length;
var check=true;
for (i = 0; i < len; i++){ 
        if(document.forms['customerFileForm'].elements['checkV'][i].checked == false ){
        check=false;
        i=len+1
        }
    } 
    if(check){
    document.forms['customerFileForm'].elements['checkAllRelo'].checked=true;
    }else{
    document.forms['customerFileForm'].elements['checkAllRelo'].checked=false;
    }
}

function checkAll(temp){
    document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = "";
    var len = document.forms['customerFileForm'].elements['checkV'].length;
    if(temp.checked){
    for (i = 0; i < len; i++){

        document.forms['customerFileForm'].elements['checkV'][i].checked = true ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
    else{
    for (i = 0; i < len; i++){

        document.forms['customerFileForm'].elements['checkV'][i].checked = false ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
}

function submitOrderInitiation(){ 
	var assignmentType = document.forms['customerFileForm'].elements['customerFile.assignmentType'].value;
	var OACompanyCode = document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value;
	var DACompanyCode=	document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value;
	var primaryEmail=	document.forms['customerFileForm'].elements['customerFile.email'].value; 
	assignmentType=assignmentType.trim();
	var assignTypeFlag=false;
	<configByCorp:fieldVisibility componentId="component.customerfile.field.assignTypeFlag">
	assignTypeFlag = true;
	</configByCorp:fieldVisibility>
	if(assignmentType=='' && assignTypeFlag){
		
		return false;
	}
	<c:if test="${hasOADADataSecuritySet}">  
	var  partnerSetList= '<c:out value="${partnerCodeOADADataSecuritySet}"/>'; 
	if(OACompanyCode==''){
		OACompanyCode='No'
	}
	if(DACompanyCode==''){
		DACompanyCode='No'
	} 
    if(!((partnerSetList.indexOf(OACompanyCode)> -1) || (partnerSetList.indexOf(DACompanyCode)> -1))){ 
		return false; 
	}
    </c:if>
    <configByCorp:fieldVisibility componentId="component.orderInitiation.field.primaryEmail.mandatory">
    if(primaryEmail==''){ 
		return false; 
	}
    </configByCorp:fieldVisibility>
	getValue3();
    var showVoer=false;
	<configByCorp:fieldVisibility componentId="component.accportal.editableFields">
	showVoer = true;
	</configByCorp:fieldVisibility>

	if(showVoer)
	{
	var cid=document.forms['customerFileForm'].elements['customerFile.id'].value;

	new Ajax.Request('getDsFamilyDetailsBlankFieldIdAjax.html?ajax=1&decorator=simple&popup=true&cid='+cid,
			{
			method:'get',
			onSuccess: function(transport){
				var response = transport.responseText || "";
				response = response.trim();
				if(response=='Y'){
					alert('Please fill the mandatory fields in family details.');
					return false;
				}else if(response=='N'){
					var familySituation =document.forms['customerFileForm'].elements['customerFile.familySitiation'].value;
			  		var familySize = '${dsFamilyDetailsSize}';
			  		if((familySituation=='Couple' || familySituation=='Married'||familySituation=='Single with children') && familySize < 2){
			  		  	$j('#getErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [2] lines of family details</p>");
						$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
			  		  	return false;
			  	  	}else if ((familySituation=='Married with children' || familySituation=='Couple with children') && familySize < 3){
			  		  	$j('#getErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [3] lines of family details</p>");
						$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
			  		  	return false;
			  	  	}else{
			  	  		document.forms['customerFileForm'].action ='submitOrderInitiation.html';
						document.forms['customerFileForm'].submit();
			  	  	}
				}else{
					document.forms['customerFileForm'].action ='submitOrderInitiation.html';
					document.forms['customerFileForm'].submit();
				}
			},
			onFailure: function(){ 
			}
		});
		}
		
		else
		{
		document.forms['customerFileForm'].action ='submitOrderInitiation.html';
         document.forms['customerFileForm'].submit();  
		
		}
 }  
 function massageFamily(){
 alert("Please Save Order Initiation Detail first before adding Family members.");
 } 
 function onlyNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	
function calcDays() {
 document.forms['customerFileForm'].elements['customerFile.duration'].value="";
 var date2 = document.forms['customerFileForm'].elements['customerFile.contractStart'].value;	 
 var date1 = document.forms['customerFileForm'].elements['customerFile.contractEnd'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   } else if(month == 'Feb')
   {
       month = "02";
   } else if(month == 'Mar')
   {
       month = "03"
   } else if(month == 'Apr')
   {
       month = "04"
   }  else if(month == 'May')
   {
       month = "05"
   } else if(month == 'Jun')
   {
       month = "06"
   }  else if(month == 'Jul')
   {
       month = "07"
   } else if(month == 'Aug')
   {
       month = "08"
   }  else if(month == 'Sep')
   {
       month = "09"
   }  else if(month == 'Oct')
   {
       month = "10"
   }  else if(month == 'Nov')
   {
       month = "11"
   }  else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }  else if(month2 == 'Feb')
   {
       month2 = "02";
   }  else if(month2 == 'Mar')
   {
       month2 = "03"
   }  else if(month2 == 'Apr')
   {
       month2 = "04"
   }  else if(month2 == 'May')
   {
       month2 = "05"
   }  else if(month2 == 'Jun')
   {
       month2 = "06"
   }   else if(month2 == 'Jul')
   {
       month2 = "07"
   }  else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }  else if(month2 == 'Oct')
   {
       month2 = "10"
   }  else if(month2 == 'Nov')
   {
       month2 = "11"
   }  else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  document.forms['customerFileForm'].elements['customerFile.duration'].value = daysApart; 
  if(daysApart<0)
  {
    alert("Contract Start Date must be less than Contract End Date");
    document.forms['customerFileForm'].elements['customerFile.duration'].value=''; 
    //document.forms['customerFileForm'].elements['customerFile.contractEnd'].value='';
    //document.forms['customerFileForm'].elements['customerFile.contractStart'].value='';
  }
  if(document.forms['customerFileForm'].elements['customerFile.duration'].value=='NaN')
   {
     document.forms['customerFileForm'].elements['customerFile.duration'].value = '';
   } 
 document.forms['customerFileForm'].elements['orderInitiationForDaysClick'].value = '';
}



function forDays(){
 document.forms['customerFileForm'].elements['orderInitiationForDaysClick'].value ='1';
} 	

/*Kunal*/
function getPortalDefaultAccess(){
	var partnerCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var url="partnerPrivatePortalDefaultAccess.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
	httpPortalAccess.open("GET", url, true);
	httpPortalAccess.onreadystatechange = httpPortalDefaultAccess;
	httpPortalAccess.send(null);
}
function httpPortalDefaultAccess(){
     if (httpPortalAccess.readyState == 4){
                var results = httpPortalAccess.responseText
                results = results.trim();
                document.getElementById('portalAccess').value=results;
               // var res = results.split("#");
     }
}

/*End Kunal*/
function getHomeCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http444.open("GET", url, true);
    http444.onreadystatechange = httpHomeCountryName;
    http444.send(null);
}
function httpHomeCountryName(){
     if (http444.readyState == 4){
                var results = http444.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['customerFileForm'].elements['customerFile.homeCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['customerFileForm'].elements['HomeCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['customerFileForm'].elements['HomeCountryFlex3Value'].value='N';
 					}try{
 					document.forms['customerFileForm'].elements['customerFile.homeCountry'].select();
 					}catch(e){}
				}else{
                     
                 }
             }
}

function getPortalDefaultPartnerData(){
	var partnerCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var url="partnerPrivatePortalDefaultData.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
	httpPortalAccess.open("GET", url, true);
	httpPortalAccess.onreadystatechange = httpPortalDefaultPartnerData;
	httpPortalAccess.send(null);
}
function httpPortalDefaultPartnerData(){
     if (httpPortalAccess.readyState == 4){
                var results = httpPortalAccess.responseText
                results = results.trim();
                if(results!=""){
                var res = results.split("~"); 
                if(res[0]!="NODATA"){
                 document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[0]; 
                }
                if(res[1]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value = res[1]; 
                }
                if(res[2]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.personPricing'].value = res[2]; 
                }
                if(res[3]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.personBilling'].value = res[3]; 
                }
                if(res[4]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.personPayable'].value = res[4]; 
                }
                if(res[5]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.auditor'].value = res[5]; 
                }
                /* if(res[6]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.coordinator'].value = res[6]; 
                } */
                if(res[7]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.source'].value = res[7]; 
                }
                if(res[8]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = res[8]; 
                }
                if(res[9]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.contract'].value = res[9]; 
                }
                if(res[10]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.accountCode'].value = res[10]; 
                }
                if(res[11]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.accountName'].value = res[11]; 
                }
                }
     }
}


function autoPopulate_customerFile_homeCountry(targetElement) {	
		var dCountry = targetElement.value;
		 var enbState = '${enbState}';
			var index = (enbState.indexOf(dCountry)> -1);
			if(index != ''){
			document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = false;
			//document.getElementById('destinationStateRequired').style.display = 'block';
		    //document.getElementById('destinationStateNotRequired').style.display = 'none';
			if(dCountry == 'United States'){
			//document.forms['customerFileForm'].elements['customerFile.destinationZip'].focus(); 
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.homeState'].value = '';
			//document.getElementById('destinationStateRequired').style.display = 'none';
		    //document.getElementById('destinationStateNotRequired').style.display = 'block';
			//autoPopulate_customerFile_destinationCityCode(dCountryCode,'special');
		}
	}
	
function gethomeState(targetElement){
	var country = targetElement.value; 
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponsehomeState;
     http3.send(null);
}
function handleHttpResponsehomeState(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.homeState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].value = stateVal[0];
					
					if (document.getElementById("homeState").options[i].value == '${customerFile.homeState}'){
					   document.getElementById("homeState").options[i].defaultSelected = true;
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("homeState").value = '';
					}
					else{ document.getElementById("homeState").value == '${customerFile.homeState}';
					   }
        }
}  	
function massageAddress(){
	 alert("Please Save Order Initiation Detail first before adding Additional address.");
}
function enableStateListOrigin(){ 
	var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
	  }	        
}
function enableStateListDestin(){ 
	var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(desCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
	  }	        
}
function enableStateListHome(){ 
	var desCon=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(desCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = true;
	  }	        
}
function findAssignmentFromParentAgent(){	
	   var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value; 
	   if(bCode!='') { 
		   var url="findAssignment.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		   http51.open("GET", url, true); 
		   http51.onreadystatechange = handleHttp90; 
		   http51.send(null); 
	   }
	 }
function handleHttp90(){
	 if (http51.readyState == 4){	 
	         var results = http51.responseText
	         results = results.trim();
	         if(results.length >= 1){
	         	var res = results.split("@"); 
		         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
						targetElement.length = res.length;
						for(i=0;i<res.length;i++){
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].text = res[i];
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].value = res[i];
							if(document.getElementById("assignmentType").options[i].value=='${customerFile.assignmentType}'){
								document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
							}
						} 
		 		}else {/* 
			 		var j=1;
			         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
			         var len='${fn1:length(assignmentTypes)}';
						targetElement.length = parseInt(len)+1;
			 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
						<c:forEach var="assignment1" items="${assignmentTypes}" varStatus="loopStatus">
			 	       	var entKey = "<c:out value="${assignment1.key}"/>";
			 	      	var entvalue = "<c:out value="${assignment1.value}"/>";
			 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey ;
						if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
						}
						j++;
		 	 		</c:forEach>
		 		 */}
	 		}
		}
var http51 = getHTTPObject();
function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
  xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  if (!xmlhttp)
  {
      xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
  }
}
return xmlhttp;
}
</script>
<script type="text/javascript" src="scripts/prototype.js"></script>
<script type="text/javascript" src="scripts/effects.js"></script>
<script type="text/javascript" src="scripts/scriptaculous.js?load=effects"></script>


</head> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<s:hidden name="fileID" id ="fileID" value="%{customerFile.id}" />
<c:set var="fileID" value="%{customerFile.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>

<div class="modal fade" id="showErrorModal" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									    <div class="modal-body" style="font-weight:600;">
										    <div class="alert alert-danger" id="getErrorMsg">
											</div>
									    </div>
									    <div class="modal-footer">
									    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
									     </div>
								</div>
							</div>
						</div>
<s:form id="customerFileForm" name="customerFileForm" action="saveOrderInitiation" onsubmit="return submit_form()" method="post" validate="true" >
<s:hidden name="user.id"/>
<s:hidden name="customerFile.partnerEntitle"/>
<s:hidden name="partnerId" value="${partnerPublic.id}"/>
<s:hidden name="user.version"/>	
 <c:set var="hide" value="false" />
 <c:set var="primaryEmailMandatory" value="false" />
		<configByCorp:fieldVisibility componentId="component.accportal.editableFields">
		<c:set var="hide" value="true" />
        </configByCorp:fieldVisibility>
        <configByCorp:fieldVisibility componentId="component.orderInitiation.field.primaryEmail.mandatory">
        <c:set var="primaryEmailMandatory" value="true" />
        </configByCorp:fieldVisibility>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;">
		<s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
		 <s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
		<s:hidden name="orderInitiationForDaysClick" id="orderInitiationForDaysClick"/>
		<s:hidden  name="customerFile.homeCountryCode" />
		<s:hidden name="HomeCountryFlex3Value" id="HomeCountryFlex3Value" />
		<s:hidden name="controlFlagBnt" value="<%=request.getParameter("controlFlagBnt") %>"/>
		<c:set var="controlFlagBnt" value="<%=request.getParameter("controlFlagBnt") %>" />
		<s:hidden name="customerFile.id" value="%{customerFile.id}" /> 
		<s:hidden name="customerFile.isNetworkRecord" />
		<s:hidden name="custJobType" value="<%=request.getParameter("custJobType") %>" />
		<s:hidden name="contractBillCode" value="<%=request.getParameter("contractBillCode") %>" />
		<s:hidden name="custCreatedOn" value="<%=request.getParameter("custCreatedOn") %>" />
		<s:hidden name="custStatus"/>
		<s:hidden name="oldCustStatus" value="${customerFile.status}"/>
		<s:hidden name="customerFile.corpID" />
	    <s:hidden name="stdAddDestinState" />
	    <s:hidden name="stdAddOriginState" />
	    <s:hidden name="checkConditionForOriginDestin" />
		<s:hidden name="id" value="<%=request.getParameter("id") %>" /> 
		<s:hidden name="gotoPageString" id="gotoPageString" value="" />
		<s:hidden name="idOfWhom" value="${customerFile.id}" /> 
		<s:hidden id="countNotes" name="countNotes" value="<%=request.getParameter("countNotes") %>"/>
		<s:hidden id="countOriginNotes" name="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>"/>
		<s:hidden id="countDestinationNotes" name="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>"/>
		<s:hidden id="countSurveyNotes" name="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>"/>
		<s:hidden id="countVipNotes" name="countVipNotes" value="<%=request.getParameter("countVipNotes") %>"/>
		<s:hidden id="countBillingNotes" name="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<s:hidden id="countSpouseNotes" name="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<s:hidden id="countContactNotes" name="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<s:hidden id="countCportalNotes" name="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<s:hidden id="countEntitlementNotes" name="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<c:set var="countNotes" value="<%=request.getParameter("countNotes") %>" />
		<c:set var="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>" />
		<c:set var="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>" />
		<c:set var="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>" />
		<c:set var="countVipNotes" value="<%=request.getParameter("countVipNotes") %>" />
		<c:set var="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<c:set var="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<c:set var="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<c:set var="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<c:set var="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<s:hidden name="dCountry" value="${dCountry}" id="dCountry" />
		<s:hidden name="oCountry" value="${oCountry}" id="oCountry"/> 
		<s:hidden id="countDSNotes" name="countDSNotes" value="<%=request.getParameter("countDSNotes") %>"/>
		<c:set var="countDSNotes" value="<%=request.getParameter("countDSNotes") %>" /> 
		<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	    <s:hidden name="formStatus" value=""/>  
	    <s:hidden  name="customerFile.quotationStatus"  /> 
	    <s:hidden  name="customerFile.orderIntiationStatus"  /> 
	    <s:hidden name="customerFile.parentAgent" />
	    <s:hidden name="emailTypeFlag" />
	    <s:hidden name="emailTypeVOERFlag" />	   
	    <configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeflag">
	    <s:hidden name="orderInitiationUpload" value="true" />
	    </configByCorp:fieldVisibility>
	    <c:set var="hideSaveButton" value="true" />
		<configByCorp:fieldVisibility componentId="component.field.hideSaveOrderInitiationButton">
		<c:set var="hideSaveButton" value="false" />
		</configByCorp:fieldVisibility>
	<c:choose>
			<c:when test="${gotoPageString == 'gototab.serviceorder' }">
				<c:redirect url="/customerServiceOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.raterequest' }">
				<c:redirect url="/customerRateOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.surveys' }">
				<c:redirect url="/surveysList.html?id1=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.accountpolicy' }">
				<c:redirect url="/showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.imfEntitlements' }">
				<c:redirect url="/editImfEntitlement.html?cid=${customerFile.id}" />
			</c:when> 
			<c:otherwise>
			</c:otherwise>
		</c:choose> 
		<c:set var="from" value="<%=request.getParameter("from") %>"/>
		<c:set var="field" value="<%=request.getParameter("field") %>"/>
		<s:hidden name="field" value="<%=request.getParameter("field") %>" />
		<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
		<c:set var="field1" value="<%=request.getParameter("field1") %>"/> 
	    <s:hidden name="jobNumber" value="%{customerFile.sequenceNumber}"/>
		<c:set var="jobNumber" value="<%=request.getParameter("sequenceNumber") %>" />
		<s:set name="reportss" value="reportss" scope="request"/>  
	

	<c:if test="${not empty customerFile.id}">
		<div id="newmnav">
		  <ul> 
		    <li id="newmnav1" style="background:#FFF "><a href="editOrderInitiation.html?id=${customerFile.id}" class="current"><span>Order Detail</span></a></li>
		    <li><a href="accountPortalFiles.html?id=${customerFile.id}&asmlFlag=order" /><span>File Documents</span></a></li>
		    <li><a href="orderInitiations.html" /><span>Order List</span></a></li>
		 </div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
	</c:if>	
	<c:if test="${empty customerFile.id}">
		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a onmouseover="" onclick=" "  class="current"><span>Order Detail</span></a></li>
		    <li><a onmouseover="" onclick=" "><span>File Documents</span></a></li> 
		    <li><a href="orderInitiations.html" /><span>Order List</span></a></li>  
		 </div><div class="spn">&nbsp;</div>
	</c:if> 
	

<div id="content" align="center">
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
<table cellspacing="0" cellpadding="0" border="0" width="100%" class="detailTabLabel" >
		<tbody>
		<tr>
         <td height="10" width="100%" align="left" style="margin: 0px">
  				
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px" class="detailTabLabel">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Order Instructions
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
				
				 <div id="Initiation" class="switchgroup1">
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
					 <tbody>
					 <tr><td height="10px"></td></tr>
					 <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="" style="margin: 0px;padding: 0px;">
					 <tr>
					  <td align="right" width="110px" class="listwhitetext" valign="bottom">Action Requested&nbsp;<font color="red" size="2">*&nbsp;</font></td>
					  <td width=""></td>
					  <td width="30"></td>
					  <td width=""></td>
					  
					  </tr>
					  <c:choose>
								<c:when test="${customerFile.orderAction == 'IO'}" >
						  		 <tr>
						  			<td></td>
						  			<td class="listwhitetext" width="80px">Order&nbsp;Initiation</td>
						  			<td><INPUT type="radio" name="customerFile.orderAction" checked="checked" value="IO" onchange="cheackQuotationStatus('IO')" tabindex="1" ></td>
						  			<td  align="left" class="listwhitetext" ><fmt:message key='customerFile.moveDate'/>
						  			<c:if test="${not empty customerFile.moveDate}">
							          <s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
							          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
							          <img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							       </c:if>
							       <c:if test="${empty customerFile.moveDate}">
							          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
							          <img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							       </c:if>
						  		  </td>
						  			
						  		 </tr>
						  		 <tr><td height="1"></td></tr>
						  		 <tr>
						  		 <td></td>
						  		 <td class="listwhitetext">Quote Request</td>
						  		 <td><INPUT type="radio" name="customerFile.orderAction"  value="QR" onchange="cheackQuotationStatus('QR')" tabindex="2" ></td>
						  		  <td><table style="margin: 0px;padding: 0px;"><tr>
						  		  <td id="hidQuotationStatus">
						  		  <input type="button" class="cssbutton1" value="Accept Quote" name="acceptQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Accepted')"/> 
						  		  <input type="button" class="cssbutton1" value="Reject Quote" name="rejectQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Rejected')"/> 
						  		  </td>
						  		  </tr>
						  		  </table></td>
						  		  
						  		</tr>
						  		</c:when>
						  		<c:when test="${customerFile.orderAction == 'QR'}" >
						  		 <tr>
						  		   <td></td>
						  		   <td class="listwhitetext" width="80px">Order&nbsp;Initiation</td>
						  		   <td><INPUT type="radio" name="customerFile.orderAction"  value="IO"  onchange="cheackQuotationStatus('IO')" tabindex="1" ></td>
						  		    <td  align="left" class="listwhitetext" ><fmt:message key='customerFile.moveDate'/>
						  			<c:if test="${not empty customerFile.moveDate}">
							          <s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
							          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
							          <img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							       </c:if>
							       <c:if test="${empty customerFile.moveDate}">
							          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
							          <img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							       </c:if>
						  		  </td>
						  		   
						  		  </tr>
						  		  <tr><td height="1"></td></tr>
						  		  <tr>
						  		  <td></td>
						  		  <td class="listwhitetext">Quote Request</td>
						  		  <td><INPUT type="radio" name="customerFile.orderAction" checked="checked" value="QR" onchange="cheackQuotationStatus('QR')" tabindex="2"></td>
						  		   <td><table style="margin: 0px;padding: 0px;"><tr>
						  		  <td id="hidQuotationStatus">
						  		  <input type="button" class="cssbutton1" value="Accept Quote" name="acceptQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Accepted')"/> 
						  		  <input type="button" class="cssbutton1" value="Reject Quote" name="rejectQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Rejected')"/> 
						  		  </td>
						  		  </tr></table></td>
						  		  </tr>
						  		</c:when>
						  		<c:otherwise>
						  		 <tr>
						  			<td></td>
						  			<td class="listwhitetext" width="80px">Order&nbsp;Initiation</td>
						  			<td><INPUT type="radio" name="customerFile.orderAction"  value="IO" onchange="cheackQuotationStatus('IO')" tabindex="1" ></td>
						  		     <td  align="left" class="listwhitetext" ><fmt:message key='customerFile.moveDate'/>
						  			<c:if test="${not empty customerFile.moveDate}">
							          <s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
							          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
							          <img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							       </c:if>
							       <c:if test="${empty customerFile.moveDate}">
							          <s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
							          <img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							       </c:if>
						  		  </td>
						  		    
						  		 </tr>
						  		 <tr><td height="1"></td></tr>
						  		 <tr>
						  		  <td></td>
						  		  <td class="listwhitetext">Quote Request</td>
						  		  <td><INPUT type="radio" name="customerFile.orderAction"  value="QR" onchange="cheackQuotationStatus('QR')" tabindex="2"></td>
						  		  <td><table style="margin: 0px;padding: 0px;"><tr>
						  		  <td id="hidQuotationStatus">
						  		  <input type="button" class="cssbutton1" value="Accept Quote" name="acceptQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Accepted')"/> 
						  		  <input type="button" class="cssbutton1" value="Reject Quote" name="rejectQuote" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderInitiationStatus('Rejected')"/> 
						  		  </td>
						  		  </tr></table></td>
						  		  </tr>
						  		</c:otherwise>
					  		</c:choose> 
    	              </table>
    	              <%-- Modifications Done by kunal for ticket number: 6183 --%>
    	              <table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" style="padding-top: 5px;">
    	             	 <tr>
    	              	 	<td align="right" width="92" class="listwhitetext" style="padding-left: 18px;"></td>
					  		<td class="listwhitetext" valign="bottom">Bill To Code<font color="red">*</font></td>
					  		<td class="listwhitetext" valign="bottom">Bill To Name</td>
					  	 </tr>
    	              	 <tr>
									<td align="right"  class="listwhitetext" style="padding-left: 18px;">Billing</td>
									<td width="95px">
										<s:textfield cssClass="input-textUpper" id="billToCode" name="customerFile.billToCode" cssStyle="width:65px;" maxlength="11" readonly="true" />
										<img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="openPopWindow();" src="<c:url value='/images/open-popup.gif'/>"/>
									</td>
									<td width="90px"><s:textfield id="billToName" cssClass="input-textUpper" name="customerFile.billToName" readonly="true" cssStyle="width:288px;" maxlength="40" tabindex=""/></td>
						 </tr>
    	              </table>
    	              <%-- Modifications Closed here --%>
    	              <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
    	              <tr>
					  <td align="right" width="112"  class="listwhitetext" valign="bottom">Service Requested&nbsp;</td>
					  </tr>
					  <tr>
					  <td></td>
					  <td colspan="11">
					    <fieldset style="margin:2px; padding:5px; width:380px;">
					  <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
					  <tr>	
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Air</td>
					  <td align="left" width="3%" ><s:checkbox name="customerFile.serviceAir" value="${customerFile.serviceAir}" fieldValue="true" disabled="disabled" tabindex="3" /></td>
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Sea</td>
					  <td align="left" width="3%" ><s:checkbox name="customerFile.serviceSurface" value="${customerFile.serviceSurface}" fieldValue="true" disabled="disabled" tabindex="4" /></td>
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Truck</td>
					  <td align="left" width="3%" ><s:checkbox name="customerFile.serviceAuto" value="${customerFile.serviceAuto}" fieldValue="true" disabled="disabled" tabindex="5" /></td>
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Storage</td>
					  <td align="left"  width="3%"><s:checkbox name="customerFile.serviceStorage" value="${customerFile.serviceStorage}" fieldValue="true" disabled="disabled" tabindex="6" /></td>
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Domestic</td>
					  <td align="left"  width="3%"><s:checkbox name="customerFile.serviceDomestic" value="${customerFile.serviceDomestic}" fieldValue="true" disabled="disabled" tabindex="7" /></td>
					  <configByCorp:fieldVisibility componentId="component.tab.customerFile.servicePovCheck">
					  <td align="right" class="listwhitebox" width="2%" valign="middle">POV</td>
					  <td align="left"  width="3%"><s:checkbox name="customerFile.servicePov" value="${customerFile.servicePov}" fieldValue="true" disabled="disabled" tabindex="7" /></td>
					 <td width="2%"></td>
					 </configByCorp:fieldVisibility>					 
					 </tr>
					 </table>
					 </fieldset>
					 </td>
                      </tr></table>
                      <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
                      <tr>
                      <td align="left" height="10"  class="listwhitetext" valign="bottom"></td>
                      <td></td>
                      </tr>
                      <c:if test="${serviceRelosCheck==''}">
    <s:hidden  name="customerFile.serviceRelo" value="${customerFile.serviceRelo}"/>
    </c:if>
                      <c:if test="${serviceRelosCheck!=''}">
                      <tr>
					<td align="right" width="114"  class="listwhitetext" valign="top">Relocation&nbsp;Services&nbsp;</td>
                      <s:hidden  name="customerFile.serviceRelo"   />
                     <td class="listwhitetext" valign="middle"><input type="checkbox" style="margin-left:0px " name="checkAllRelo"  onclick="checkAll(this)" tabindex="8" />Check&nbsp;All&nbsp;</td>
                     </tr>
                     <tr>
                     <td></td>
                      <td colspan="2" valign="top">
                     <fieldset style="margin:3px 0 0 0; padding:2px 0 0 2px; width:60%;">                      
                     <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
                     <tr>
                      <td width="53%" valign="top">
                        <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
                          <c:forEach var="entry" items="${serviceRelos1}">
                           <tr>
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex="9"/> </td> 
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                           
                          </tr>
                          </c:forEach>
                          </table> 
                       </td>
                       <td valign="top">
                       <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
                          <c:forEach var="entry" items="${serviceRelos2}">
                           <tr> 
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex="9"/> </td>
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                          </tr>
                          </c:forEach>
                          </table>                    
                       </td>
                       </tr>
                       </table>
                          </fieldset>
                       </td>
                        
                       </tr>
                       </c:if>
                       <tr>
	                      <td align="left" height="10"  class="listwhitetext" valign="bottom"></td>
	                      <td></td>
                        </tr>
                      </table>
                      	<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin: 0px; paddding: 0px;">
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>

								<td align="right" width="109" class="listwhitetext">Pets Involved</td>
								<td width="5">&nbsp;</td>
								<td align="left" width="3%"><s:checkbox
									name="customerFile.petsInvolved"
									value="${customerFile.petsInvolved}" fieldValue="true"
									disabled="disabled" tabindex="10" cssStyle="margin-left:0px;vertical-align:middle;" /></td>
								<td width="10%">&nbsp;</td>
								<td width="5">&nbsp;</td>
								<td width="">&nbsp;</td>
							</tr>
							
							<tr>
								<td>&nbsp;</td>
							</tr>

							<tr>

								<td align="right" width="108" class="listwhitetext">Visa
								Required&nbsp;</td>
								<td width="5">&nbsp;</td>
								<td align="left" width="115"><s:select cssClass="list-menu"
									name="customerFile.visaRequired" list="%{visaRequiredList}"
									cssStyle="width:100px" headerKey="" headerValue=""
									onchange="cheackVisaRequired();" tabindex="11" /></td>
									<td width="15">&nbsp;</td>
								<td align="right" nowrap="nowrap" class="listwhitetext" id="hidVisaStatusReg" width="10%">Visa
								Status<font color="red" size="2">*&nbsp;</font></td>
								<td width="5">&nbsp;</td>
								<td align="left" id="hidVisaStatusReg1"><s:select
									cssClass="list-menu" name="customerFile.visaStatus"
									list="%{visaStatusList}" cssStyle="width:100px" headerKey=""
									headerValue="" tabindex="12" /></td>
								<c:if test="${hide=='true'}">
								<td align="right" nowrap="nowrap" class="listwhitetext"  width="10%">Family Situation
							    <font color="red" size="2">&nbsp;</font></td>
								<td width="5">&nbsp;</td>
								<td align="left" ><s:select
									cssClass="list-menu" name="customerFile.familySitiation" list="%{familySitiationList}"  cssStyle="width:100px" headerKey=""
									headerValue="" tabindex="12" /></td>
							</c:if>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>

						<table class="detailTabLabel" cellspacing="0" cellpadding="0"
							border="0" style="margin: 0px; paddding: 0px;">
							<tr>
							<td width="108" class="listwhitetext" align="right" valign="top">Entitlement&nbsp;&nbsp;</td>
							<td>
							<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin: 0px; paddding: 0px;">
							<c:forEach var="entitlementMap" items="${entitlementMap}">
							<tr>								
								<td align="left" width="" class="listwhitetext">								
								<s:checkbox name="chk_${entitlementMap.key}" id="chk_${entitlementMap.key}" value="chk_${entitlementMap.key}" fieldValue="true" tabindex="13" onclick="getValue3();" cssStyle="vertical-align:middle;" />
	                           	<s:label title="${entitlementMap.value}" value="${entitlementMap.key}"></s:label>
								</td>
							</tr>
							</c:forEach>
							
						</table>
						</td>
						</tr>
						</table>
                      
                      
                      <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin:0px;paddding:0px;" >
                      <tr>
                      <td align="left" width=""  height="5" class="listwhitetext" valign="bottom"></td>
                      </tr>
                      <tr>
                      <td  align="left" width="113" ></td>             
                      <td  align="left" width="80%" ><s:textarea name="customerFile.orderComment" cssStyle="width:390px;height:90px;" cssClass="textarea" readonly="false" tabindex="14"/></td>
                      </tr>
                      </table>
    	              <tr><td height="10px"></td></tr>
					</tbody></table>
			<tr>
				<td>
				<div onClick="javascript:animatedcollapse.toggle('employee')" style="margin: 0px">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;padding:0px;" >
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center" style="cursor:default; ">&nbsp;&nbsp;Employee Details
</td>
<td width="28" valign="top" class="headtab_bg" style="cursor:default; "></td>
<td class="headtab_bg_center" style="cursor:default; ">
<a href="javascript:animatedcollapse.hide(['address', 'alternative', 'billing', 'entitlement', 'cportal','family','hr','benefits','additional','employee','transferee'])">Collapse All<img src="${pageContext.request.contextPath}/images/section_contract.png" HEIGHT=14 WIDTH=14 align="top"></a> | <a href="javascript:animatedcollapse.show(['address', 'alternative', 'billing', 'entitlement', 'cportal','family','hr','benefits','additional','employee','transferee'])">Expand All<img src="${pageContext.request.contextPath}/images/section_expanded.png" HEIGHT=14 WIDTH=14 align="top"></a>
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>
<div id="employee" class="switchgroup1">
<table width="100%" border="0" cellpadding="4" cellspacing="1" class="detailTabLabel" >
  <tr>
    <td width="90">&nbsp;</td>
    		<td align="left">
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" >
					<tbody>
						<tr>
						</tr>			
						<tr>
				  <td>&nbsp;</td>
				  <td align="left">
					<table width="" border="0" cellpadding="0" cellspacing="" class="detailTabLabel">
					<tbody>																
						<tr>
							<td align="left" width="" class="listwhitetext" valign="bottom" ><fmt:message key='customerFile.prefix'/></td>
							<td align="left" class="listwhitetext" width="10"></td>
							<td align="left" width="" class="listwhitetext" valign="bottom">Title / Rank</td>
							<td align="left" class="listwhitetext" width="10"></td>
							<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.firstName' /></td>
							<td align="left" class="listwhitetext" width="10"></td>
							<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.middleInitial'/></td>
							<td align="left" class="listwhitetext" width="10"></td>
							<td align="left" width="" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.lastName'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"> <font color="red" size="2">*</font></c:if></td>
							<td align="left" class="listwhitetext" width="10" ></td>
							
							<td align="left" width="" class="listwhitetext" ><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.socialSecurityNumber',this);return false" ><fmt:message key='customerFile.socialSecurityNumber'/></a></td>
							<td align="right" width="" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.vip',this);return false" ><fmt:message key='serviceOrder.vip'/></a></td>
						    <td width="15px"></td>
						    <td width="15px"  class="listwhitetext"></td>
						   
						</tr>
						<tr>
						<c:set var="isVipFlag" value="false"/>
								<c:if test="${customerFile.vip}">
									<c:set var="isVipFlag" value="true"/>
								</c:if>
							<td align="left" class="listwhitetext" valign="top" > <s:select cssClass="list-menu" name="customerFile.prefix" cssStyle="width:47px" list="%{preffix}" headerKey="" headerValue="" onchange="changeStatus();" onfocus = "myDate();" tabindex="15"/></td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" valign="top"><s:textfield cssClass="input-text" key="customerFile.rank" cssStyle="width:60px"  maxlength="7" onkeyup="valid(this,'special')" onblur="valid(this,'special')" tabindex="16"/> </td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" valign="top" width="230" > <s:textfield cssClass="input-text upper-case" name="customerFile.firstName" onblur="titleCase(this)" onkeypress="" onchange="noQuote(this);" required="true"
							cssStyle="width:227px" maxlength="80" onfocus = "myDate();" tabindex="17"/> </td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text" key="customerFile.middleInitial"
							required="true" cssStyle="width:13px" maxlength="1" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="18"/> </td>
							<td align="left" class="listwhitetext" ></td>
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text upper-case" key="customerFile.lastName" onblur="titleCase(this)" onkeypress="" onchange="noQuote(this);" required="true"
							cssStyle="width:200px" maxlength="80" onfocus = "myDate();" tabindex="19"/> </td>
							<td align="left" class="listwhitetext" ></td>
							
					        <td align="left" class="listwhitetext" >
					       
							<s:textfield cssClass="input-text" key="customerFile.socialSecurityNumber" cssStyle="width:63px" maxlength="9" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')" tabindex="20"/>
					        
				            </td>
							<td align="right" width="40" valign="bottom" ><s:checkbox key="customerFile.vip" value="${isVipFlag}" fieldValue="true" onclick="changeStatus()" tabindex="21"/></td>
							<td class="listwhitetext"></td> 
							<s:hidden name="customerFile.controlFlag" /> 
							<c:if test="${empty customerFile.id}">
							<td align="right" style="width:137px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countVipNotes == '0' || countVipNotes == '' || countVipNotes == null}">
								<td align="right" style="width:137px"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" style="width:137px"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',755,500);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
					  </tr>
					 <tr>
							<td align="left" class="listwhitetext" colspan="3"><a
								href="#" style="cursor: help"
								onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.billToAuthorization',this);return false"><fmt:message
								key='customerFile.billToAuthorization' /></a></td>
							<td align="left" class="listwhitetext" width=""></td>
							<td colspan="" align="left" class="listwhitetext">Account&nbsp;Reference&nbsp;#</td>
							<td colspan="" align="left" class="listwhitetext"></td>
							<td colspan="" align="left" class="listwhitetext"></td>
							<td colspan="" align="left" class="listwhitetext"></td>
							<td colspan="" align="left" class="listwhitetext">Cost&nbsp;Center&nbsp;</td>
						</tr>
						<tr>
							<td width="100" colspan="3"><s:textfield
								cssClass="input-text"
								name="customerFile.billToAuthorization" maxlength="50"
								onblur="valid(this,'special')" cssStyle="width:121px" tabindex="22" /></td>
							<td align="left" class="listwhitetext" width=""></td>
							<td colspan=""><s:textfield cssClass="input-text"
								name="customerFile.billToReference" maxlength="15"
								onblur="valid(this,'special')" cssStyle="width:227px"
								tabindex="23" /></td>
							<td align="left" class="listwhitetext" width=""></td>
							<td align="left" class="listwhitetext" width=""></td>
							<td align="left" class="listwhitetext" width=""></td>
							<td align="left" width="110" colspan="5"><s:textfield cssClass="input-text" name="customerFile.costCenter" id="orderInitiationCostCenter" cssStyle="width:200px" maxlength="50"  tabindex="24" value="${customerFile.costCenter}"/></td> 
						</tr>
					</tbody>
				</table></td>
			    </tr>
			    
				
				
							<s:hidden  name="customerFile.originCityCode" />
							<s:hidden   name="customerFile.originCountryCode" /> 
							<s:hidden  name="customerFile.destinationCityCode" /> 
							<s:hidden  name="customerFile.destinationCountryCode" />
							<s:hidden  name="customerFile.status" /> 
							<s:hidden  key="customerFile.customerEmployer" /> 
							<s:hidden  name="customerFile.bookingAgentCode" />
							<!--<td width="22"><img class="openpopup" width="17" height="20" onclick="openBookingAgentPopWindow();document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							--><s:hidden  name="customerFile.bookingAgentName" />
							<!--<td align="left">
								<div id="hidBookAgent" style="vertical-align:middle;"><a><img class="openpopup" id="noteCustBANavigations" onclick="getPartnerAlert(document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value,'noteCustBANavigations');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
							</td>
							<script type="text/javascript">
								showPartnerAlert('onload','${customerFile.bookingAgentCode}','hidBookAgent');
							</script>-->
							
							<c:if  test="${companies == 'Yes'}">
							<s:hidden  id="companyDivision" name="customerFile.companyDivision" />
							<!--<td align="left"><configByCorp:fieldVisibility componentId="customerFile.companyDivision"><s:select cssClass="list-menu" id="companyDivision" name="customerFile.companyDivision" list="%{companyDivis}"  cssStyle="width:100px" onchange="changeStatus();" headerKey="" headerValue="" tabindex=""/></configByCorp:fieldVisibility></td>-->
							</c:if>
							
							<c:if  test="${companies != 'Yes'}">
							<s:hidden name="customerFile.companyDivision"/>
							<!--<td align="left"><configByCorp:fieldVisibility componentId="customerFile.companyDivision"><s:select cssClass="list-menu" id="companyDivision" name="customerFile.companyDivision" list="%{companyDivis}"  cssStyle="width:100px" onchange="changeStatus();" headerKey="" headerValue="" tabindex=""/></configByCorp:fieldVisibility></td>-->
							</c:if>
							<!--<td align="left"><s:select cssClass="list-menu" id="companyDivision" name="customerFile.companyDivision" list="%{companyDivis}"  cssStyle="width:100px" onchange="changeStatus();" headerKey="" headerValue="" tabindex=""/></td>
							-->
							
							<c:if test="${not empty customerFile.bookingDate}">
								<s:text id="customerFileBookingFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.bookingDate"/></s:text>
								<s:hidden id="bookingDate" name="customerFile.bookingDate" value="%{customerFileBookingFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.bookingDate}">
								<s:hidden id="bookingDate"  name="customerFile.bookingDate" />
							</c:if>
							
							
							<c:if test="${not empty customerFile.initialContactDate}">
							<s:text id="customerFileInitialFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.initialContactDate"/></s:text>
							<s:hidden id="initialContactDate"  name="customerFile.initialContactDate" value="%{customerFileInitialFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.initialContactDate}">
							<s:hidden id="initialContactDate"  name="customerFile.initialContactDate" />
							</c:if> 
							<s:hidden  id="job" name="customerFile.job"  />
							<s:hidden id="coordinator" name="customerFile.coordinator" /> 
							<s:hidden  name="customerFile.salesStatus" />  
							<s:hidden  name="customerFile.salesMan"    />
				</table> 
				 <s:hidden   name="customerFile.comptetive"  /> 
			     <s:hidden  name="customerFile.source"  /> 
	 </table> </div>
				
							
							<s:hidden  name="customerFile.estimator" id="estimator" /> 
							<c:if test="${not empty customerFile.survey}">
							<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
							<s:hidden  id="survey" name="customerFile.survey" value="%{customerFileSurveyFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.survey}">
							<s:hidden  id="survey" name="customerFile.survey" />
							</c:if> 
							<s:hidden  name="customerFile.surveyTime" id="surveyTime" /> 
							
							<s:hidden name="customerFile.surveyTime2" id="surveyTime2" /> 
							<s:hidden name="timeZone" value="${sessionTimeZone}" />  
							<c:if test="${not empty customerFile.priceSubmissionToAccDate}">
							<s:text id="customerFileSubmissionToAccFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToAccDate"/></s:text>
							<s:hidden id="priceSubmissionToAccDate" name="customerFile.priceSubmissionToAccDate" value="%{customerFileSubmissionToAccFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToAccDate}">
							<s:hidden id="priceSubmissionToAccDate"  name="customerFile.priceSubmissionToAccDate" />
							</c:if>
							
							<c:if test="${not empty customerFile.priceSubmissionToTranfDate}">
							<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToTranfDate"/></s:text>
							<s:hidden id="priceSubmissionToTranfDate" name="customerFile.priceSubmissionToTranfDate" value="%{customerFileSubmissionToTranfFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToTranfDate}">
							<s:hidden id="priceSubmissionToTranfDate" name="customerFile.priceSubmissionToTranfDate" />
							</c:if>
							
							<c:if test="${not empty customerFile.quoteAcceptenceDate}">
							<s:text id="customerFileAcceptenceFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.quoteAcceptenceDate"/></s:text>
							<s:hidden id="quoteAcceptenceDate"  name="customerFile.quoteAcceptenceDate" value="%{customerFileAcceptenceFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.quoteAcceptenceDate}">
							<s:hidden id="quoteAcceptenceDate"  name="customerFile.quoteAcceptenceDate" />
							</c:if>  
				</td>
				</tr>
				
				<tr>
					<td height="10" width="100%" align="left" style="margin: 0px">
  				<div  onClick="javascript:animatedcollapse.toggle('hr')" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;HR Details
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
				<div id="hr" class="switchgroup1">
				<table border="0" class="detailTabLabel">
			<tbody>
			<tr>
	 <td colspan="5" >
		 <table width="100%" class="detailTabLabel">
		 <tr> 
		  <td align="center"  class="listwhitetext" width="58%" ><b><u>Origin</u></b></td>
		 <td width="10px"></td>
		  <td align="center"  class="listwhitetext" width="42%"><b><u>Destination</u></b></td> 
		 </tr>
		 </table>
	 </td>
	 </tr>
	 <tr>
	  
	  <tr> 
	  <td align="right" class="listwhitetext" ">Company</td>
	   <td align="left" class="listwhitetext"  ><s:textfield cssClass="input-text"	id="originCompanyCodeId" name="customerFile.originCompanyCode" tabindex="35" cssStyle="width:60px;" maxlength="8" onchange="valid(this,'special');findOriginCompanyCode();" />
		<img style="vertical-align:top;padding-right:5px;" class="openpopup" id="originCompanyCode.popupImage" width="17" height="20" onclick="openOriginCompanyCodePopUp();document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" />
	    <s:textfield cssClass="input-text" id="originCompanyId" name="customerFile.originCompany"  cssStyle="width:235px;" maxlength="280" tabindex="36" onkeyup="findIkeaPartnerDetails('originCompanyId','originCompanyCodeId','originCompanyNameNameDiv',event);" onchange="findPartnerDetailsByName('originCompanyCodeId','originCompanyId');" />							
            <img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details"	onclick="viewPartnerDetailsForBillToCode(this,'originCompanyCodeId');" src="<c:url value='/images/address2.png'/>" />
	    </td>
		<div id="originCompanyNameNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 226px;"></div>							
								
	    <td align="right" class="listwhitetext" ></td>
	   <td align="right" class="listwhitetext" >Company</td>
	    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"	id="destCompanyCodeID" name="customerFile.destinationCompanyCode" tabindex="42" cssStyle="width:60px;" maxlength="8"
							onchange="valid(this,'special');findDestinationCompanyCode();" />
		<img style="vertical-align:top;padding-right:5px;" class="openpopup" id="destCompanyCode.popupImage" width="17" height="20" onclick="openDestinationCompanyCodePopUp();document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" />
	   <s:textfield cssClass="input-text" id="destinationCompanyId" name="customerFile.destinationCompany" cssStyle="width:225px;" maxlength="280" tabindex="43" onkeyup="findIkeaPartnerDetails('destinationCompanyId','destCompanyCodeID','destCompanyNameNameDiv',event);"
									onchange="findPartnerDetailsByName('destCompanyCodeID','destinationCompanyId');" />
   <img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details"	onclick="viewPartnerDetailsForBillToCode(this,'destCompanyCodeID');" src="<c:url value='/images/address2.png'/>" />	
									</td> 
	    <div id="destCompanyNameNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 226px;"></div>
	  </tr>
	  
	 <tr> 
        <td align="right" class="listwhitetext" width="104px">HR Contact Person</td>
		<td colspan="2"><s:textfield cssClass="input-text" name="customerFile.orderBy" cssStyle="width:230px;" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)" tabindex="25" /> 
		 <configByCorp:fieldVisibility componentId="component.customerFile.HRPersonDetails">
		<img align="right" style="margin-right: 30%;" class="openpopup" width="17" height="20" onclick="getContactForHr();"  src="<c:url value='/images/plus-small.png'/>" />
		</configByCorp:fieldVisibility>
		</td>
		
		
		<td align="right" class="listwhitetext" width="124px" >HR Contact Person</td>
		<td><s:textfield cssClass="input-text" name="customerFile.localHr" cssStyle="width:230px;" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)" tabindex="28" /></td>
	
		
		</tr>
		
	    <tr>
        <td align="right" class="listwhitetext" >HR Email Address</td>
		<td colspan="2"><s:textfield cssClass="input-text" name="customerFile.orderEmail" cssStyle="width:230px;" maxlength="65" tabindex="26"  onkeydown=""/>
		<img class="openpopup" style="vertical-align:top;" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.orderEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.orderEmail}"/></td>
		
		<td align="right" class="listwhitetext" >HR Email Address</td>
		<td ><s:textfield cssClass="input-text" name="customerFile.localHrEmail" cssStyle="width:230px;" maxlength="65" tabindex="29"  onkeydown=""/>
		<img class="openpopup" style="vertical-align:top;" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.localHrEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.localHrEmail}"/></td>
		<td width="10px"></td>

	 </tr>
	 <tr>
		<td align="right" class="listwhitetext">HR Phone</td>   
		<td><s:textfield cssClass="input-text" name="customerFile.orderPhone"  cssStyle="width:130px;" maxlength="20"  tabindex="27"/></td>
		<td width="10px"></td>
		<td align="right" class="listwhitetext">HR Phone</td>   
		<td><s:textfield cssClass="input-text" name="customerFile.localHrPhone"  cssStyle="width:130px;" maxlength="20"  tabindex="30"/></td>
	    <td width="10px"></td>
	    
	    </tr>
	 <tr>
        <td align="right" class="listwhitetext"></td>
     </tr>
		</tbody>
		</table>
		</div>
				</td>
				</tr>	
				
				<tr>
					<td height="10" width="100%" align="left" style="margin: 0px">
  				<div  onClick="return basedOnBillToCode();" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Address Details
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
						<div id="address" class="switchgroup1">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
											<tbody>
											<tr><td>
											<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px; padding: 0px;">
											<tr>
											<td class="subcontenttabChild"><b>&nbsp;Origin</b></td>
											</tr>
											</table>
											</td></tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext" style="padding-left:10px;">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin:0px; padding: 0px;">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																
																<td valign="top" style="margin:0px; padding: 0px;" colspan="2">
																<table style="margin:0px; padding: 0px;">
																<tr>
																<td align="left" class="listwhitetext" style="width:95px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/></td>
																<td width="65px">&nbsp;</td>
																
															
																</tr>
																<tr>
																<td align="left" class="listwhitetext" width="80"></td>
															    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress1" cssStyle="width:230px;" maxlength="100" tabindex="31" onblur="titleCase(this);"/></td>
																
																 <td width="22"><img class="openpopup" width="17" height="20" onclick="openStandardAddressesPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																 
															    </tr>
																</table>
																
																</td>
															</tr>
														
															<tr>
																
																<td valign="top" style="margin:0px; padding: 0px;padding-right:12px;">
																<table style="margin:0px; padding: 0px;" >
															
																<tr>
																<td align="left" class="listwhitetext" style="width:95px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress2" cssStyle="width:230px;" maxlength="100" tabindex="32" onblur="titleCase(this);"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address2</font></td>
																
																</tr>
																<tr>
																<td align="left" class="listwhitetext" style=""></td>
																<td ><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress3" cssStyle="width:230px;" maxlength="100" tabindex="33" onblur="titleCase(this);"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address3</font></td>
																
																</tr>
																</table>
																</td>
																
																<td style="margin:0px; padding: 0px;" >
																
													               </td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
														
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><c:if test="${not validatorPartnerPrivate.portalDefaultAccess}"><font color="red" >*</font></c:if></td>
																<td align="left" class="listwhitetext"></td>
																<td width="57px"></td>
																<td align="left" class="listwhitetext" ><div id="originStateNotRequired"><fmt:message key='customerFile.originState'/></div><div id="originStateRequired"><fmt:message key='customerFile.originState'/><font color="red" size="2">*</font></div></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.originCountry" list="%{ocountry}" id="ocountry" cssStyle="width:235px"  onchange="requiredOriginState();changeStatus();getOriginCountryCode(this);autoPopulate_customerFile_originCountry(this);getState(this);enableStateListOrigin();"  headerKey="" headerValue="" tabindex="34"/></td><td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation();"/></td>
																<td width="57px"></td>
																<td><s:select cssClass="list-menu" id="originState" name="customerFile.originState" list="%{ostates}" cssStyle="width:228px"  onchange="changeStatus();autoPopulate_customerFile_originCityCode(this); " headerKey="" headerValue="" tabindex="35"/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:100px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td class="listwhitetext" style="width:100px"></td>
															<td align="left" class="listwhitetext" onmouseover ="gettooltip('customerFile.originCity');" onmouseout="hideddrivetip();"><fmt:message key='customerFile.originCity'/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" id="zipCodeRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td> 
																</tr>
															<tr>
															<td class="listwhitetext" style="width:100px"></td>
													<td><s:textfield cssClass="input-text upper-case" name="customerFile.originCity" cssStyle="width:150px;" maxlength="30"  onkeypress="" onblur="titleCase(this);autoPopulate_customerFile_originCityCode(this,'special');" tabindex="36"/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originZip" cssStyle="width:64px;" maxlength="10" onchange="findCityStateOfZipCode(this,'OZ');" onkeydown="return onlyAlphaNumericAllowed(event,this,'special');" onblur="valid(this,'special')" tabindex="37"/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originDayPhone'/></td>
																
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.originDayPhoneExt'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originHomePhone'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originMobile'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originFax'/></td>
															</tr>
															<tr>	
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhone" cssStyle="width:150px;" maxlength="20"  tabindex="38" /></td>
																<td style="width:10px"> </td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhoneExt" cssStyle="width:64px;" maxlength="10"   tabindex="39"/></td>
																<td align="left" class="listwhitetext" style="width:26px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originHomePhone" cssStyle="width:130px;" maxlength="20"  tabindex="40" /></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originMobile" cssStyle="width:130px;" maxlength="25"  tabindex="41" /></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originFax" cssStyle="width:130px;" maxlength="20"  tabindex="42" /></td>
															</tr>
														</tbody>
													</table>
													
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left" class="listwhitetext">
																	<fmt:message key='customerFile.email'/>
																	<c:if test="${validatorPartnerPrivate.portalDefaultAccess || hasOADADataSecuritySet || primaryEmailMandatory}">
																		<font color="red" size="2">*</font>
																	</c:if>
																</td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email2'/></td>
																<td align="left" class="listwhitetext" style="width:20px"></td>
																<td></td>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email" cssStyle="width:230px;" maxlength="65" tabindex="43" onchange="EmailPortDate();validateEmail(this);" /></td>
																<td align="center" class="listwhitetext" style="width:25px"><img class="openpopup" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.email'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email}"/></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email2" cssStyle="width:240px;" maxlength="65" tabindex="44" onchange="validateEmail(this);"/></td>
																<td align="left" class="listwhitetext" style="width:30px">&nbsp;&nbsp;<img class="openpopup" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.email2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email2}"/></td>
																<td align="left" class="listwhitetext" style="width:20px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.originPreferredContactTime" cssStyle="width:130px;" maxlength="30" tabindex="45" /></td>
																<c:if test="${empty customerFile.id}">
																<td style="width:238px;" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countOriginNotes == '0' || countOriginNotes == '' || countOriginNotes == null}">
																		<td style="width:238px;" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',755,500);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="width:238px;" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',755,500);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
														</tbody>
													</table>
													</td>
													
												</tr>
												<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="7"></td></tr>
												<tr><td align="center" colspan="15" class="vertlinedata"></td></tr>
												<tr><td>
											<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px; padding: 0px;">
											<tr>
											<td class="subcontenttabChild"><b>&nbsp;Destination</b></td>
											</tr>
												</table>
												</td>
												</tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext" style="padding-left:10px;">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px; padding: 0px;">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
														<tr>
																
																<td valign="top" style="margin:0px; padding: 0px;" colspan="2">
																<table style="margin:0px; padding: 0px;">
																<tr>
																<td align="left" class="listwhitetext" style="width:80px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationAddress1'/></td>
																<td width="65px">&nbsp;</td> 
																</tr>
																<tr>
																<td align="left" class="listwhitetext" width="95px"></td>
															   <td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress1" cssStyle="width:230px;" maxlength="100" tabindex="46" onblur="titleCase(this);"/></td>
																<td width="22"><img class="openpopup" width="17" height="20" onclick="openStandardAddressesDestinationPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																
																 
															    </tr>
																</table>
																
																</td>
															</tr>
														
															<tr>
																
																<td valign="top" style="margin:0px; padding: 0px;padding-right:12px;">
																<table style="margin:0px; padding: 0px;" >
															
																<tr>
																<td align="left" class="listwhitetext" style="width:95px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress2" cssStyle="width:230px;" maxlength="100" tabindex="47" onblur="titleCase(this);"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address2</font></td>
																
																</tr>
																<tr>
																<td align="left" class="listwhitetext" style=""></td>
																<td ><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress3" cssStyle="width:230px;" maxlength="100" tabindex="48" onblur="titleCase(this);"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address3</font></td>
																
																</tr>
																</table>
																</td>
																
																<td style="margin:0px; padding: 0px;" >
																
													               </td>
															</tr>
														
														
														
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext">
																	<fmt:message key='customerFile.destinationCountry'/>
																	<c:if test="${not validatorPartnerPrivate.portalDefaultAccess}">
																		<font color="red" size="2">*</font>
																	</c:if>
																</td>
																<td align="left" class="listwhitetext"></td>
																<td width="57px"></td>
																<td align="left" class="listwhitetext" ><div id="destinationStateNotRequired"><fmt:message key='customerFile.destinationState'/></div><div id="destinationStateRequired"><fmt:message key='customerFile.destinationState'/><font color="red" size="2">*</font></div></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.destinationCountry" list="%{dcountry}" id="dcountry" cssStyle="width:235px"  onchange="requiredDestinState();changeStatus();getDestinationCountryCode(this);autoPopulate_customerFile_destinationCountry(this);getDestinationState(this);enableStateListDestin();"  headerKey="" headerValue="" tabindex="49"/></td><td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openDestinationLocation();"/></td>
																<td width="57px"></td>
																<td><s:select cssClass="list-menu" id="destinationState" name="customerFile.destinationState" list="%{dstates}" cssStyle="width:228px" value="%{customerFile.destinationState}"  onchange="changeStatus();autoPopulate_customerFile_destinationCityCode(this);" headerKey="" headerValue="" tabindex="50"/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
															
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:100px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td class="listwhitetext" style="width:100px"></td>
													        <td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/></td>
														    <td align="left" class="listwhitetext" style="width:5px"></td>
														    <td align="left" id="zipCodeDestinRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.destinationZip' /></td>
															
															</tr>
													        <tr>
													        <td class="listwhitetext" style="width:100px"></td>
													        <td><s:textfield cssClass="input-text upper-case" name="customerFile.destinationCity" cssStyle="width:150px;" maxlength="30"  onkeypress="" onblur="titleCase(this);autoPopulate_customerFile_destinationCityCode(this,'special');" tabindex="51"/></td>
															<td align="left" class="listwhitetext" style="width:10px"></td>
															<td><s:textfield cssClass="input-text" name="customerFile.destinationZip" cssStyle="width:64px;" maxlength="10" onchange="findCityStateOfZipCode(this,'DZ');" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="52"/></td>
															<td align="left" class="listwhitetext" style="width:10px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationDayPhone'/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.destinationDayPhoneExt'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationHomePhone'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationMobile'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationFax'/></td>
															</tr>
															<tr>	
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhone" cssStyle="width:150px;" maxlength="20"  tabindex="53" /></td>
																<td style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhoneExt" cssStyle="width:64px;" maxlength="10"  tabindex="54" /></td>
																<td align="left" class="listwhitetext" style="width:26px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationHomePhone" cssStyle="width:130px;" maxlength="20"  tabindex="55" /></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationMobile" cssStyle="width:130px;" maxlength="25"  tabindex="56" /></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationFax" cssStyle="width:130px;" maxlength="20"  tabindex="57" /></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail2'/></td>
																<td align="left" class="listwhitetext" style="width:20px"></td>
																<td></td>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left"><s:textfield cssClass="input-text" name="customerFile.destinationEmail" cssStyle="width:230px;" maxlength="65" tabindex="58" onchange="validateEmail(this);"/></td>
																<td align="center" class="listwhitetext" style="width:25px"><img class="openpopup" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.destinationEmail}"/></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destinationEmail2" cssStyle="width:240px;" maxlength="65" tabindex="59" onchange="validateEmail(this);"/></td>
																<td align="left" class="listwhitetext" style="width:30px">&nbsp;&nbsp;<img class="openpopup" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.destinationEmail2}"/></td>
																<td align="left" class="listwhitetext" style="width:20px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destPreferredContactTime" cssStyle="width:130px;" maxlength="30" tabindex="60" /></td>
																<c:if test="${empty customerFile.id}">
																<td style="width:238px" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countDestinationNotes == '0' || countDestinationNotes == '' || countDestinationNotes == null}">
																		<td style="width:238px" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',755,500);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="width:238px" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',755,500);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
															<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="7"></td></tr>
																<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px; padding: 0px;">
																<tr>
																	<td class="subcontenttabChild"><b>&nbsp;Spouse Contact</b></td>
																</tr>
															</table>																
													
														<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
															<c:set var="privilegeFlag" value="false"/>
															<c:if test="${customerFile.privileges}">
												 				<c:set var="privilegeFlag" value="true"/>
															</c:if>
															<td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
															<tr></tr>
															<tr></tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerPrefix'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerFirstName'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerLastName'/></td>
															</tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"> <s:select cssClass="list-menu" name="customerFile.custPartnerPrefix" cssStyle="width:47px" list="%{preffix}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="61"/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"> <s:textfield cssClass="input-text" name="customerFile.custPartnerFirstName" required="true"
																cssStyle="width:169px" maxlength="80" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="62"/> </td>
																<td align="left" class="listwhitetext" style="width:25px"></td>
																<td align="left" class="listwhitetext"> <s:textfield cssClass="input-text" key="customerFile.custPartnerLastName" required="true"
																cssStyle="width:278px;" maxlength="80" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="63"/> </td>
																<td align="left">&nbsp;&nbsp;<s:checkbox key="customerFile.privileges" value="${privilegeFlag}" fieldValue="true" onclick="changeStatus()" tabindex="64"/></td>
																<td align="left" class="listwhitetext" style="width:65px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.privileges',this);return false" >Joint&nbsp;Account</a></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerEmail'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerContact'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerMobile'/></td>
															</tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.custPartnerEmail"  cssStyle="width:230px;" maxlength="65" tabindex="65" onchange="validateEmail(this);"/></td>
																<td align="left" class="listwhitetext" style="width:25px"></td>
																<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerContact" cssStyle="width:130px;" maxlength="20"  tabindex="66"/> </td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerMobile" cssStyle="width:130px;" maxlength="25"  tabindex="67"/> </td>
																<c:if test="${empty customerFile.id}">
																<td align="right" style="width:388px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countSpouseNotes == '0' || countSpouseNotes == '' || countSpouseNotes == null}">
																	<td align="right" style="width:388px" valign="bottom"><img id="countSpouseNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',755,500);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td align="right" style="width:388px" valign="bottom"><img id="countSpouseNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',755,500);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:10px;" colspan="7"></td></tr>
														</tbody>
													</table>
										</div>
										</td>
									</tr>
														</tbody>
													</table>
													</td>
												</tr>
											</tbody>
										</table>
										</div>
										</td>
									</tr> 
									
									<tr>
	<td height="10" width="100%" align="left" style="margin:0px">
	<div onClick="javascript:animatedcollapse.toggle('additional')" style="margin:0px">
     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;&nbsp;Additional&nbsp;Addresses
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
     </div>
		<div id="additional" class="switchgroup1">
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr><td align="left" colspan="10">
<div style=" width:100%; overflow-x:auto; overflow-y:auto;">
<s:set name="adAddressesDetailsList" value="adAddressesDetailsList" scope="request"/>
<display:table name="adAddressesDetailsList" class="table" requestURI="" id="adAddressesDetailsList"  defaultsort="1" pagesize="10" style="margin:0px 0px 10px 0px; ">
    <display:column title="Description">
    <a href="javascript:openWindow('editAdAddressesDetails.html?isOrderInitiation=true&id=${adAddressesDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',800,400)">
    <c:out value="${adAddressesDetailsList.description}"></c:out></a>
    </display:column>
    <display:column property="addressType"  title="Address Type" />
    <display:column title="Address" maxLength="15">${adAddressesDetailsList.address1}&nbsp;${adAddressesDetailsList.address2}&nbsp;${adAddressesDetailsList.address3}</display:column>     
    <display:column property="country" title="Country" />
    <display:column property="state" title="State"  /> 
    <display:column property="city"  title="City"  /> 
	<display:column property="phone" title="Phone" />	
	</display:table>
	<c:if test="${not empty customerFile.id}">
	<input type="button" class="cssbutton1" onclick="window.open('editAdAddressesDetails.html?isOrderInitiation=true&decorator=popup&popup=true&customerFileId=${customerFile.id}','customerFileForm','height=500,width=725,top=0, scrollbars=yes,resizable=yes').focus();" 
	        value="Add Addresses" style="width:120px; height:25px" tabindex=""/> 
     </c:if>
     <c:if test="${empty customerFile.id}">
     <input type="button" class="cssbutton1" onclick="massageAddress();"
	       value="Add Addresses" style="width:120px; height:25px" tabindex="83"/> 
     </c:if>
</div>
</td></tr> 
</tbody>
</table></div></td></tr>					
																
                                                                <s:hidden name="customerFile.contactName" /> 
																<s:hidden  name="customerFile.organization"  />  
																<s:hidden name="customerFile.contactEmail"  /> 
																<s:hidden  name="customerFile.contactPhone"   /> 
																<s:hidden  name="customerFile.contactNameExt"  /> 
																<s:hidden  name="customerFile.destinationContactName"   />
																<s:hidden  name="customerFile.destinationOrganization" />  
																<s:hidden  name="customerFile.destinationContactEmail"   /> 
																<s:hidden name="customerFile.destinationContactPhone" /> 
																<s:hidden name="customerFile.destinationContactNameExt"  />  
																
																<s:hidden  id="contract" name="customerFile.contract"  />  
                                                                <s:hidden id="billPayMethod"  name="customerFile.billPayMethod" />  
															    <s:hidden name="customerFile.accountCode"  /> 
																<s:hidden  name="customerFile.accountName" />  
																
																<c:if test="${not empty customerFile.billApprovedDate}">
																<s:text id="customerFileApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.billApprovedDate"/></s:text>
																<s:hidden id="billApprovedDate" name="customerFile.billApprovedDate" value="%{customerFileApprovedDateFormattedValue}" />
																</c:if>
																<c:if test="${empty customerFile.billApprovedDate}">
																<td><s:hidden id="billApprovedDate" name="customerFile.billApprovedDate" />
																</c:if> 
																
															    
                                                                <s:hidden name="customerFile.personPricing"   /> 
																<s:hidden name="customerFile.personBilling" /> 
																<s:hidden name="customerFile.personPayable"  /> 
																<s:hidden name="customerFile.auditor" /> 
																<s:hidden name="customerFile.noCharge" /> 
																<s:hidden name="customerFile.approvedBy" /> 
														    	<c:if test="${not empty customerFile.approvedOn}">
																	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="customerFile.approvedOn"/></s:text>
																    <s:hidden id="dateApprovedOn" name="customerFile.approvedOn" value="%{FormatedInvoiceDate}" />
																</c:if>
																<c:if test="${empty customerFile.approvedOn}">
																	<s:hidden id="dateApprovedOn" name="customerFile.approvedOn" /> 
																</c:if> 
									
 						<tr>
							
							<td height="10" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('entitlement')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>

					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Assignment
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
					<div id="entitlement" class="switchgroup1">
									
<table class="detailTabLabel" cellspacing="0" cellpadding="0"  border="0" style="margin-left:17px;">    
    
    <tr>
      <td height="10" colspan="6" align="right" valign="top" class="listwhitetext" >&nbsp;</td>
    </tr>
    <tr>
    <td colspan="6">
    <table class="detailTabLabel" cellspacing="0" cellpadding="0">
    <tr>
     <td width="95" align="right"  class="listwhitetext" >Contract Start&nbsp;</td> 
     <c:if test="${not empty customerFile.contractStart}">
		 <s:text id="customerFileContractStartFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.contractStart"/></s:text>
		 <td> <s:textfield cssClass="input-text" id="contractStart" name="customerFile.contractStart" value="%{customerFileContractStartFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		 <img id="contractStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
    </td>
    </c:if>
    <c:if test="${empty customerFile.contractStart}">
	<td><s:textfield cssClass="input-text" id="contractStart" name="customerFile.contractStart" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		<img id="contractStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	</td>
	</c:if>
	<td width="95" align="right" class="listwhitetext" >Contract End&nbsp;</td>
    
     <c:if test="${not empty customerFile.contractEnd}">
		 <s:text id="customerFileContractEndFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.contractEnd"/></s:text>
		<td> <s:textfield cssClass="input-text" id="contractEnd" name="customerFile.contractEnd" value="%{customerFileContractEndFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		 <img id="contractEnd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
    </td>
    </c:if>
    <c:if test="${empty customerFile.contractEnd}">
		 <td><s:textfield cssClass="input-text" id="contractEnd" name="customerFile.contractEnd" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		<img id="contractEnd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	</td>
	</c:if>
	<td width="95" align="right" class="listwhitetext" >Duration&nbsp;</td>
    <td><s:textfield cssClass="input-text" name="customerFile.duration" id="orderInitiationDuration" cssStyle="width:65px" maxlength="5" onkeydown="return onlyNumsAllowed(event)" tabindex="68" onselect="" onfocus="calcDays();" /></td>
    </tr>
    </table>
    </td></tr>
    <tr>
      <td height="10" colspan="6" align="right" valign="top" class="listwhitetext" >&nbsp;</td>
    </tr>
    <tr>
      <td width="95" align="right" valign="top" class="listwhitetext" >Comments:&nbsp;</td>
      <td width="520" align="left"  ><textarea name="customerFile.entitled" style="width: 545px; height: 110px;" class="textarea" maxlength="250" tabindex="69"><c:out value='${customerFile.entitled}'/></textarea></td>
      <td width="333" class="listwhitetext" ></td>
      <c:if test="${empty customerFile.id}">
        <td  align="center"  valign="bottom"  ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" height=17 width=50 onclick="notExists();"/></td>
      </c:if>
      <c:if test="${not empty customerFile.id}">
        <c:choose>
          <c:when test="${countEntitlementNotes == '0' || countEntitlementNotes == '' || countEntitlementNotes == null}">
            <td align="center"  valign="bottom"><img id="countEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" height=17 width=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',755,500);" ></a></td>
          </c:when>
          <c:otherwise>
            <td  align="center" valign="bottom"  ><img id="countEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" height=17 width=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',755,500);" ></a></td>
          </c:otherwise>
        </c:choose>
      </c:if>
    </tr>
    <tr>
      <td height="10" colspan="6"></td>
    </tr>
    <tr>
    <td colspan="6">
    <table class="detailTabLabel" cellspacing="0" cellpadding="0">
    <tr>
    <td width="95" align="right"  class="listwhitetext" >Home Country&nbsp;</td> 
    <td style="width:189px;"><s:select cssClass="list-menu" name="customerFile.homeCountry" list="%{ocountry}" cssStyle="width:157px"  onchange="changeStatus(); getHomeCountryCode(this);autoPopulate_customerFile_homeCountry(this); gethomeState(this);enableStateListHome();"  headerKey="" headerValue="" tabindex="70"/><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openHomeCountryLocation();" style="vertical-align: bottom;"/></td>
             <td  align="right" class="listwhitetext" >State&nbsp;</td> 
             <td width="60"><s:select cssClass="list-menu" id="homeState" name="customerFile.homeState" list="%{homeStates}" cssStyle="width:145px"  onchange="changeStatus(); " headerKey="" headerValue="" tabindex=""/></td>
    <td width="38" align="right"  class="listwhitetext" >City&nbsp;</td> 
    <td><s:textfield cssClass="input-text upper-case" name="customerFile.homeCity" cssStyle="width:145px" maxlength="30"  onkeypress="" onblur="titleCase(this)" tabindex="71"/></td>
    </tr></table>
    </td></tr>
    <tr>
      <td height="10" colspan="6"></td>
    </tr>
    <tr>
      <td class="listwhitetext" align="right" >Assignment&nbsp;Type&nbsp;<configByCorp:fieldVisibility componentId="component.customerfile.field.assignTypeFlag"><font color="red" size="2">*&nbsp; </configByCorp:fieldVisibility></font></td>
       <td class="listwhitetext" colspan="5">
       <configByCorp:customDropDown listType="map" list="${assignmentTypes_isactive}" fieldValue="${customerFile.assignmentType}" 
                        attribute="class=list-menu style=width:157px; tabindex=72 id=assignmentType name=customerFile.assignmentType headerKey='' headerValue='' onclick=changeStatus();" /></td>
    </tr>
	 <tr>
      <td height="10" colspan="6"></td>
    </tr> 
</table></div>
   <s:hidden  name="customerFile.sequenceNumber"/> 
 <c:if test="${not empty customerFile.statusDate}">
 <s:text id="customerFileStatusDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.statusDate" /></s:text>
 <s:hidden  name="customerFile.statusDate" value="%{customerFileStatusDateFormattedValue}" />
 </c:if>
 <c:if test="${empty customerFile.statusDate}">
 <s:hidden   name="customerFile.statusDate"  />
 </c:if>
<s:hidden cssClass="list-menu" name="customerFile.statusReason" />	
						
	</tr>
	
	<tr>
	<td height="10" width="100%" align="left" style="margin:0px">
	<div onClick="javascript:animatedcollapse.toggle('family')" style="margin:0px">
     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">

<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;&nbsp;Family&nbsp;Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>

</table>
     </div>
		<div id="family" class="switchgroup1">
		
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr><td align="left" colspan="10">
<div style=" width:90%; overflow-x:auto; overflow-y:auto;">
<s:set name="dsFamilyDetailsList" value="dsFamilyDetailsList" scope="request"/>

<display:table name="dsFamilyDetailsList" class="table" requestURI="" id="dsFamilyDetailsList"  defaultsort="1" pagesize="10">
    <c:if test="${(customerFile.quotationStatus=='Accepted' )||(customerFile.quotationStatus=='Rejected')|| (customerFile.quotationStatus=='Submitted' ) || (customerFile.orderIntiationStatus=='Submitted')}">
     <display:column title="First Name"> 
    <c:out value="${dsFamilyDetailsList.firstName}"></c:out></a>
    </display:column> 
    </c:if>
    <c:if test="${(customerFile.quotationStatus!='Accepted' )&&(customerFile.quotationStatus!='Rejected')&& (customerFile.quotationStatus!='Submitted')&& (customerFile.orderIntiationStatus!='Submitted') }">
    
    <display:column title="First Name">
    <a href="javascript:openWindow('editDsFamilyDetails.html?isOrderInitiation=true&id=${dsFamilyDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',1350,400)">
    <c:out value="${dsFamilyDetailsList.firstName}"></c:out></a>
    </display:column>
    </c:if>
    
    <display:column property="lastName"  title="Last Name" >  <% count++; %> </display:column>
    
    <display:column property="relationship"  title="Relationship" />   
    <display:column property="cellNumber"  title="Mobile Phone"  />
    <display:column property="email" title="Email"  />
    <display:column property="passportNumber" title="Passport #"  /> 
    <display:column property="dateOfBirth" title="Date Of Birth" format="{0,date,dd-MMM-yyyy}" />
	<display:column property="expiryDate" title="Expiry Date" format="{0,date,dd-MMM-yyyy}" />
	<display:column property="countryOfIssue"  title="Country Of Issue" /> 
	<c:if test="${(customerFile.quotationStatus!='Accepted' )&&(customerFile.quotationStatus!='Rejected')&& (customerFile.quotationStatus!='Submitted')&& (customerFile.orderIntiationStatus!='Submitted') }">
	<display:column title="Remove" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="confirmSubmit('${dsFamilyDetailsList.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
	</display:column>
	</c:if>
	</display:table>
	<c:if test="${not empty customerFile.id}">
	<input type="button" class="cssbutton1" onclick="window.open('editDsFamilyDetails.html?isOrderInitiation=true&decorator=popup&popup=true&customerFileId=${customerFile.id}','sqlExtractInputForm','height=500,width=1350,top=0, scrollbars=yes,resizable=yes').focus();" 
	        value="Add Family Details" style="width:120px; height:25px" tabindex=""/> 
     </c:if>
     <c:if test="${empty customerFile.id}">
	<input type="button" class="cssbutton1" onclick="massageFamily();" 
	        value="Add Family Details" style="width:120px; height:25px" tabindex=""/> 
     </c:if>
     <c:if test="${empty dsFamilyDetailsList}">
     <span style="padding-left:20px;" class="listwhitetext">Family Size <s:textfield cssClass="input-text" name="customerFile.familysize" id="customerFile.familysize" onchange="getFamilysize('FS');" size="2" maxlength="2"  tabindex=""/></span>
	</c:if>
	<c:if test="${not empty dsFamilyDetailsList}">
     <span style="padding-left:20px;" class="listwhitetext">Family Size <s:textfield cssClass="input-textUpper" name="customerFile.familysize" id="customerFile.familysize" onchange="getFamilysize('FS');" size="2" maxlength="2"  tabindex=""/></span>
	</c:if>
</div>
</td></tr> 
</tbody>
</table></div>

<!--<c:if test="${empty customerFile.id}">
<s:hidden name="customerFile.portalIdActive" value="false"/>
</c:if>
<c:if test="${not empty customerFile.id}">
<s:hidden name="customerFile.portalIdActive" value="${customerFile.portalIdActive}"/>
</c:if>
<<s:hidden name="customerFileemail"/>
<s:hidden name="customerFile.customerPortalId"/>
<s:head name="customerFile.secondaryEmailFlag"/>
-->

<configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
<tr>						
						<td height="10" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('transferee')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Portal Detail
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
							<div id="transferee" class="switchgroup1">							
						<table cellpadding="1" cellspacing="0" class="detailTabLabel" style="margin-top:5px;">
<tr>													<c:set var="isActiveFlag" value="false"/>
													<c:if test="${customerFile.portalIdActive}">
														<c:set var="isActiveFlag" value="true"/>
													</c:if>
													<td width="120" align="right"  class="listwhitetext" >Customer Portal:</td>
    <td width="16" ><s:checkbox key="customerFile.portalIdActive" id="cportalId" value="${isActiveFlag}" fieldValue="true" onclick="changeStatus();fNameMandatory(this);checkPortalBoxId();" tabindex="73"/></td>
													<td width="95"  align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.portalIdActive',this);return false" >
    <fmt:message key='customerFile.portalIdActive'/></a></td>
													<td width="40" align="right"  class="listwhitetext" >Email:&nbsp;</td>
    <td width="50" ><s:textfield cssClass="input-textUpper" name="customerFileemail"  value="%{customerFile.email}" readonly = "true" cssStyle="width:236px;" maxlength="65" tabindex="74"/></td>
	
	<c:set var="isSecondaryEmailFlag" value="false"/>
													<c:if test="${customerFile.secondaryEmailFlag}">
														<c:set var="isSecondaryEmailFlag" value="true"/>
													</c:if>
													<td width="119" align="right"  class="listwhitetext" >Secondary Email:</td>
    <td width="16" ><s:checkbox key="customerFile.secondaryEmailFlag" value="${isSecondaryEmailFlag}" fieldValue="true" onclick="changeStatus(),checkPortalBoxSecondaryId()" tabindex="75"/></td>
	<td width="100" align="right"  class="listwhitetext" >Email Sent Date:</td>
    <td width="16" ><s:textfield cssClass="input-textUpper" cssStyle="width:65px;" name="customerFile.emailSentDate" readonly="true" /></td>
												   
  </tr></table>
  
 <table  border="0" cellpadding="1" cellspacing="0" class="detailTabLabel">																																									
												<tr>
												  <td width="123" align="right" valign="absmiddle" class="listwhitetext"><fmt:message key='customerFile.customerPortalId'/></td>
													
												  <td align="left" width="155"><s:textfield cssClass="input-textUpper" name="customerFile.customerPortalId" required="true" size="15" maxlength="80" readonly="true" tabindex="76"/></td>
																								
													<c:if test="${not empty customerFile.id}">
													<td ><input type="button" value="Reset password and email to customer" class="cssbuttonB" style="width:240px;" onclick="generatePortalId();" tabindex="77"/></td>
													</c:if>
													<c:if test="${empty customerFile.id}">
													<td  align="left" ><input type="button" disabled value="Reset password and email to customer" class="cssbuttonB" style="width:240px;" onclick="generatePortalId();" tabindex="77"/></td>
													</c:if>
													<c:if test="${empty customerFile.id}">
													<td  align="right"  width="510" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
													</c:if>
													<c:if test="${not empty customerFile.id}">
													<c:choose>
														<c:when test="${countCportalNotes == '0' || countCportalNotes == '' || countCportalNotes == null}">
														<td  align="right"  width="510" valign="bottom"><img id="countCportalNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:when>
														<c:otherwise>
														<td  align="right"  width="510" valign="bottom"><img id="countCportalNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:otherwise>
													</c:choose> 
													</c:if>
												</tr>
										</table>										
						</div>
						</td>
						</tr>

                </configByCorp:fieldVisibility>


	</tbody>
	</table>
	</div>
	
<div class="bottom-header" style="margin-top:50px; z-index:0"><span></span></div>
</div>
	</div>
	
				<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${customerFile.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${customerFile.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{customerFile.createdBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${customerFile.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${customerFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{customerFile.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
				</div>
<s:hidden name="componentId" value="customerFile" /> 
<!--<sec-auth:authComponent componentId="module.customerFileList.edit" replacementHtml=""> 	
<s:submit cssClass="cssbuttonA" method="save" key="button.save" onmouseover="completeTimeString();" onclick="return IsValidTime('save');"/>
				<c:if test="${not empty customerFile.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/editCustomerFile.html"/>'" 
	        value="<fmt:message  key="button.add"/>"/> 
	    </c:if>  
		<s:hidden name="hitFlag" />
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/editCustomerFile.html?id=${customerFile.id}"  />
		</c:if> 
<s:hidden name="submitCnt" value=""/>
<s:hidden name="submitType" value=""/>
<c:set var="submitType" value=""/>
	<c:choose>
		<c:when test="${submitType == 'job'}">
			<s:reset type="button" cssClass="cssbutton1" key="Reset" />
		</c:when>
		<c:otherwise>
		<c:if test="${not empty customerFile.id}">
			<s:reset type="button" cssClass="cssbutton1" key="Reset" onclick="findCompanyDivisionByBookAg();disabledSODestinBtnReset();disabledSOBtnReset(); newFunctionForCountryState(); getContractReset();" />
		</c:if>
		<c:if test="${empty customerFile.id}">
		<s:reset type="button" cssClass="cssbutton1" key="Reset" onclick="findCompanyDivisionByBookAg();" />
		</c:if>
		</c:otherwise>
	 </c:choose>
	 <c:if test="${not empty customerFile.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/addAndCopyCustomerFile.html?cid=${customerFile.id}"/>'" 
	        value="<fmt:message  key="button.addAndCopy"/>" style="width:110px; height:25px"/> 
	    </c:if>  
</sec-auth:authComponent>-->
<s:hidden name="hitFlag" />
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/editOrderInitiation.html?id=${customerFile.id}"  />
		</c:if> 
		<c:if test="${hitFlag == 2}" >
			<c:redirect url="/editOrderInitiation.html?id=${customerFile.id}&familySituationValue=${customerFile.familySitiation}&newData=Yes"  />
		</c:if>
<c:if test="${hideSaveButton=='true'}">
<s:submit cssClass="cssbuttonA" method="saveOrderInitiation" key="button.save" onclick="getValue3();return checkBillToCode();" tabindex="" />	
</c:if>
<input type="button" class="cssbuttonA"  name="submit1" value="Submit" onclick="submitOrderInitiation(); return checkBillToCode();" tabindex=""/>	
<s:hidden name="customerFile.statusNumber" />	
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />




<s:text id="customerFileSystemDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.systemDate" /></s:text>
<td valign="top"><s:hidden name="customerFile.systemDate" value="%{customerFileSystemDateFormattedValue}" /></td>
							
<s:hidden name="sendEmail" />

<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>

<input type="hidden" name="encryptPass" value="true" />
<s:hidden name="description"/>
<s:hidden name="secondDescription"/>
<s:hidden name="thirdDescription"/>
<s:hidden name="fourthDescription"/>
<s:hidden name="fifthDescription"/>
<s:hidden name="sixthDescription" />
<s:hidden id="portalAccess" name="booleanPortalDefaultAccess"/>
</s:form>
<script type="text/javascript">
	function openPopWindow(){
		window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');
	}
	function checkBillToCode(){ 
		var billToCodeValue = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
		var assignmentType = document.forms['customerFileForm'].elements['customerFile.assignmentType'].value;
		var OACompanyCode = document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value;
		var DACompanyCode=	document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value; 
		var primaryEmail=	document.forms['customerFileForm'].elements['customerFile.email'].value; 
		<c:if test="${hasOADADataSecuritySet}">  
		var  partnerSetList= '<c:out value="${partnerCodeOADADataSecuritySet}"/>'; 
		if(OACompanyCode==''){
			OACompanyCode='No'
		}
		if(DACompanyCode==''){
			DACompanyCode='No'
		} 
		if(!((partnerSetList.indexOf(OACompanyCode)> -1) || (partnerSetList.indexOf(DACompanyCode)> -1))){ 
			alert("Please fill Origin Company or Destination Company as "+partnerSetList+" in HR Details"); 
			return false; 
		} 
		</c:if>
		<configByCorp:fieldVisibility componentId="component.orderInitiation.field.primaryEmail.mandatory">
		if(primaryEmail==''){ 
			alert("Primary Email is Required Field."); 
			return false; 
		}
		</configByCorp:fieldVisibility>
		assignmentType=assignmentType.trim();
		var assignTypeFlag=false;
		<configByCorp:fieldVisibility componentId="component.customerfile.field.assignTypeFlag">
		assignTypeFlag = true;
		</configByCorp:fieldVisibility>
		if(billToCodeValue == ''){
			alert('Bill To Code is Required Field.');
			return false;
		}else if(assignmentType=='' && assignTypeFlag){
			alert('Assignment Type is Required Field.');
			return false; 
		} 
		else { 
		 try
		 {
		var lname = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
		lname = lname.trim();
		document.forms['customerFileForm'].elements['customerFile.lastName'].value=lname;
		var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
		email = email.trim();
		document.forms['customerFileForm'].elements['customerFile.email'].value=email;
		}catch(e){};
		}	
	}
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	
function fNameMandatory(target){
	var portalValue = document.forms['customerFileForm'].elements['customerFile.portalIdActive'].value;
	var fname = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
	var pemail = document.forms['customerFileForm'].elements['customerFile.email'].value;
	fname = fname.trim();
	var targetId=target.id;
	if(target.checked==true && portalValue== 'true' && fname == ''){
		alert('First Name is Required For Customer Portal.');
		document.getElementById(targetId).checked=false;
		return false;
	}
	pemail = pemail.trim();
	if(target.checked==true && portalValue== 'true' && pemail == ''){
		alert('Primary Email address is required in the Origin Address section for generating Customer portal ID.');
		document.getElementById(targetId).checked=false;
		return false;
	}
}
</script>
<script type="text/javascript">  
try{
checkColumn();
}
catch(e){}
try{
checkAllRelo();
}
catch(e){}
try{
cheackQuotationStatus('${customerFile.orderAction}');
}
catch(e){}
try{
cheackVisaRequired();
}
catch(e){}
try{
	var f = document.getElementById('customerFileForm'); 
	f.setAttribute("autocomplete", "off");
	}
	catch(e){}
try{
	if('${stateshitFlag}'!= 1){
	getNewCoordAndSale();
	}
	}
	catch(e){}

try{ 
 var ocountry=document.forms['customerFileForm'].elements['ocountry'].value; 
 }
 catch(e){}

try{
 var dcountry=document.forms['customerFileForm'].elements['dcountry'].value;
 }
 catch(e){}
 try{
 var hcountry=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
 }
 catch(e){} 

 		try{
		 	var enbState = '${enbState}';
		 	var oriCon=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
		 	if(oriCon=="") {
					document.getElementById('homeState').disabled = true;
					document.getElementById('homeState').value ="";
		 		}else{	
					if(enbState.indexOf(oriCon)> -1){
						document.getElementById('homeState').disabled = false; 
						document.getElementById('homeState').value='${customerFile.homeState}';									
					}else{
						document.getElementById('homeState').disabled = true;
						document.getElementById('homeState').value ="";						
				}
			}
		 }catch(e){} 		

	try{
	requiredOriginState();	
	autoPopulate_customerFile_originCountry(document.forms['customerFileForm'].elements['customerFile.originCountry']);
	}catch(e){}
	try{
	 	var enbState = '${enbState}';
	 	var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	 		if(oriCon=="") {
				document.getElementById('originState').disabled = true;
				document.getElementById('originState').value ="";
	 		}else{	
				if(enbState.indexOf(oriCon)> -1){
					document.getElementById('originState').disabled = false; 
					document.getElementById('originState').value='${customerFile.originState}';									
				}else{
					document.getElementById('originState').disabled = true;
					document.getElementById('originState').value ="";						
			}
		}
	 }catch(e){}
	 try{
		requiredDestinState();	
		autoPopulate_customerFile_destinationCountry(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
		}catch(e){}
	 
	 try{
		 	var enbState = '${enbState}';
		 	var oriCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
		 		if(oriCon=="") {
					document.getElementById('destinationState').disabled = true;
					document.getElementById('destinationState').value ="";
		 		}else{	
					if(enbState.indexOf(oriCon)> -1){
						document.getElementById('destinationState').disabled = false; 
						document.getElementById('destinationState').value='${customerFile.destinationState}';									
					}else{
						document.getElementById('destinationState').disabled = true;
						document.getElementById('destinationState').value ="";						
				}
			}
		 }catch(e){} 
	 
try{
allReadOnly('${customerFile.orderAction}');
}
catch(e){}
function showOrHide(value) {
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	}
}    
showOrHide(0);

function getValue3() {
	document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value='';
	var parentEnt1='';
	<c:forEach var="entitlementMap" items="${entitlementMap}">
       var entKey = "chk_<c:out value="${entitlementMap.key}"/>";
       if(document.getElementById(entKey) != null && document.getElementById(entKey).checked == true){
        	   entKey = entKey.split(' ').join('_?@'); 
    			parentEnt1 = entKey+'#?@'+parentEnt1;
       }
 	</c:forEach>
	 document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value=parentEnt1;
}

function getFamilysize(str){
	var fSize = document.forms['customerFileForm'].elements['customerFile.familysize'].value;
//	alert(fSize);
	var no='<%= count %>';
	if(no == 0 && str != 'FS'){
		document.forms['customerFileForm'].elements['customerFile.familysize'].readOnly= false;
		if(fSize == ''){
			document.forms['customerFileForm'].elements['customerFile.familysize'].value = no;
		}
		return;
	}
	if(no == 0 && str == 'FS'){
		if(parseInt(fSize) != fSize){
			 alert('Family size is not valid');
			 document.forms['customerFileForm'].elements['customerFile.familysize'].value = '';
			 return false;
		 }else {
			 for(var i = 0;i < fSize.length;i++){
				 if(fSize.charAt(i) == '.' || fSize.charAt(i) == ' '){
					 alert('Family size is not valid');
					 document.forms['customerFileForm'].elements['customerFile.familysize'].value = '';
					 return false;
				 }
			 }
		 }
		document.forms['customerFileForm'].elements['customerFile.familysize'].value = fSize;
		document.forms['customerFileForm'].elements['customerFile.familysize'].readOnly= false;
	}else {
		document.forms['customerFileForm'].elements['customerFile.familysize'].readOnly= true;
		document.forms['customerFileForm'].elements['customerFile.familysize'].value = no;
	}
}
</script>
 
 <script type="text/javascript">
 function getContactForHr()
 {
	try{
	 var partnerCode=document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value;
	 if(partnerCode!=''){
	 	var url = 'searchUserContactCf.html?decorator=popup&popup=true&codeForHr='+partnerCode+'&myFileFor=OI';
	 	window.openWindow(url);
	 }
	}catch(e){}
 }

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
		var url = 'deleteFamilyDetailsOI.html?id=${customerFile.id}&familyId='+targetElement;
		location.href = url;
	 }else{
		return false;
	}
}

function basedOnBillToCode(){
	var bill2Code = document.getElementById('billToCode').value
	if(bill2Code != ''){
		//progressBarAutoSave('1');
		getPortalDefaultAccess();
		animatedcollapse.toggle('address');
		openadd();
		return true;
	}else{
		alert('Please Select Bill To Code');
		document.getElementById('billToCode').focus();
		return false;
	}
}
try{
	findAssignmentFromParentAgent();
}catch(e){}
<c:if test="${not empty customerFile.id}">
	var $j = jQuery.noConflict();
var familySituationVal ="<%=request.getAttribute("familySituationValue")%>";

familySituationVal = familySituationVal.trim();
var firstTimeSaved = '${newData}';

if((familySituationVal==null || familySituationVal=='' || familySituationVal=='null') && (firstTimeSaved=='Yes')){
	
	var familySituation ='${customerFile.familySitiation}';
	var familySize = '${dsFamilyDetailsSize}';
	if((familySituation=='Couple' || familySituation=='Married') && familySize < 2){
		
	  	$j('#getErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [2] lines of family details.<BR>Please add details of the co-worker.</p>");
		$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
  	}
	if((familySituation=='Single with children') && familySize < 2){
	  	$j('#getErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [2] lines of family details.</p>");
		$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
  	}
	if((familySituation=='Married with children' || familySituation=='Couple with children') && familySize < 3){
	  	$j('#getErrorMsg').html("<p>The Family Status is ["+familySituation+"]. For this status we expect [3] lines of family details.<BR>Please add details of the co-worker.</p>");
		$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
  	}
	var dsId = '${dsFamilyDetailsId}';
	
	if((dsId!=null && dsId!='') && (firstTimeSaved=='Yes')){
		javascript:openWindow('editDsFamilyDetails.html?isOrderInitiation=true&id=${dsFamilyDetailsId}&customerFileId=${customerFile.id}&decorator=popup&popup=true',1350,510);
	}
}
</c:if>
var familySituationVal ="<%=request.getAttribute("familySituationValue")%>";

familySituationVal = familySituationVal.trim();
var familySize = '${dsFamilyDetailsSize}';
var firstTimeSaved = '${newData}';
if((familySituationVal!=null && familySituationVal!='' && familySituationVal!='null') && (firstTimeSaved=='Yes')){
	  if((familySituationVal=='Couple' || familySituationVal=='Married') && familySize < 2){
		  	$j('#getErrorMsg').html("<p>The Family Status is ["+familySituationVal+"]. For this status we expect [2] lines of family details.<BR>Please add details of the co-worker.</p>");
			$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	  }
	  if((familySituationVal=='Single with children') && familySize < 2){
		  	$j('#getErrorMsg').html("<p>The Family Status is ["+familySituationVal+"]. For this status we expect [2] lines of family details.</p>");
			$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	  }
	  if((familySituationVal=='Married with children' || familySituation=='Couple with children') && familySize < 3){
		  	$j('#getErrorMsg').html("<p>The Family Status is ["+familySituationVal+"]. For this status we expect [3] lines of family details.<BR>Please add details of the co-worker.</p>");
			$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
	  }
	  var dsId = '${dsFamilyDetailsId}';
	
	  if((dsId!=null && dsId!='') && (firstTimeSaved=='Yes')){
		  javascript:openWindow('editDsFamilyDetails.html?isOrderInitiation=true&id=${dsFamilyDetailsId}&customerFileId=${customerFile.id}&decorator=popup&popup=true',1350,510);
	  }
}
var field = 'newData';
var url = window.location.href;
if(url.indexOf('&' + field + '=') != -1){
	  removeParam("newData");
}
function removeParam(parameter){
	  var url=document.location.href;
	  var urlparts= url.split('?');

	 if (urlparts.length>=2){
	  var urlBase=urlparts.shift(); 
	  var queryString=urlparts.join("?"); 

	  var prefix = encodeURIComponent(parameter)+'=';
	  var pars = queryString.split(/[&;]/g);
	  for (var i= pars.length; i-->0;)               
	      if (pars[i].lastIndexOf(prefix, 0)!==-1)   
	          pars.splice(i, 1);
	  url = urlBase+'?'+pars.join('&');
	  window.history.pushState('',document.title,url);

	}
}

</script>