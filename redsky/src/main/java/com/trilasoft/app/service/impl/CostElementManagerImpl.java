package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;





import com.trilasoft.app.dao.CostElementDao;
import com.trilasoft.app.model.CostElement;
import com.trilasoft.app.service.CostElementManager;

public class CostElementManagerImpl extends GenericManagerImpl<CostElement, Long> implements CostElementManager{
	CostElementDao costElementDao;   
	  
    public CostElementManagerImpl(CostElementDao costElementDao) {   
        super(costElementDao);   
        this.costElementDao = costElementDao;   
    }
    public List getCostListDataSummary(){
    	return costElementDao.getCostListDataSummary();
    }    
    public String costElDetailCode(String costEl){
    	return costElementDao.costElDetailCode(costEl);
    }
    public String costDesDetailCode(String costDes){
    	return costElementDao.costDesDetailCode(costDes);
    }
	public List findCostElementList(String chargeCostElement, String agentCorpId, String jobType, String routing, String soCompanyDivision) {
		return costElementDao.findCostElementList(chargeCostElement, agentCorpId, jobType, routing, soCompanyDivision);
	}
	public List findAgentCostElement(String costElement, String agentCorpId){
		return costElementDao.findAgentCostElement(costElement, agentCorpId);
	}
	public List getCharges(String costElement, String sessionCorpID){
		return costElementDao.getCharges(costElement, sessionCorpID);
	}
	public int updateAccountlineGLByCostElement(String charge, String contract, String sessionCorpID, String recGl, String payGl,Long costElementId){
		return costElementDao.updateAccountlineGLByCostElement(charge, contract, sessionCorpID, recGl, payGl,costElementId);
	}
	public int updateAccountlineGLByGLRateGrid(String charge, String contract, String sessionCorpID, String job, String routing, String recGl, String payGl,String companyDivision){
		return costElementDao.updateAccountlineGLByGLRateGrid(charge, contract, sessionCorpID, job, routing, recGl, payGl,companyDivision);
	}
	public int checkForCostEleUseInCharge(String costElement,String description, String recGl, String payGl, String sessionCorpID){
		return costElementDao.checkForCostEleUseInCharge(costElement, description, recGl, payGl, sessionCorpID);
	}
	public List getCostElementValue(String tableN,String tableD){
		return costElementDao.getCostElementValue(tableN, tableD);
	}
	public List findCostElementListTemplate(String chargeCode,String sessionCorpID, String jobType, String routing,String contract, String soCompanyDivision){
		return costElementDao.findCostElementListTemplate(chargeCode, sessionCorpID, jobType, routing ,contract,soCompanyDivision);
	}
	
	public Map<String, String> costElementDataMap(String agentCorpID){
		return costElementDao.costElementDataMap(agentCorpID);
	}
	
	public List findAgentCostElementData(String sessionCorpID,String agentCorpId){
		return costElementDao.findAgentCostElementData(sessionCorpID,agentCorpId);
	}
}
