package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.QuotationDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.PartnerQuote;
import com.trilasoft.app.model.Quotation;


public class QuotationDaoHibernate extends GenericDaoHibernate<Quotation, Long> implements QuotationDao {   
	private List quotations;
	private HibernateUtil hibernateUtil; 
	
	public QuotationDaoHibernate() {   
	    super(Quotation.class);   
	    } 
	    public List findMaximumId() {
	    	return getHibernateTemplate().find("select max(id) from Quotation");
	    	
	    }
	    
	    public List checkById(Long id) {
	    	return getHibernateTemplate().find("select id from Quotation where id=?",id);
	    }
		public List maxId(String requestedSO) {
			String reqSO =  requestedSO.replaceAll(",", "','");
			return getHibernateTemplate().find("select max(id) from ServiceOrder where shipNumber in ('"+reqSO+"')");
		}
		public List minId(String requestedSO) {
			String reqSO =  requestedSO.replaceAll(",", "','");
			return getHibernateTemplate().find("select min(id) from ServiceOrder where shipNumber in ('"+reqSO+"')");
		}
		public List <PartnerQuote>findByQuotation(String quote, String quoteStatus, String sessionCorpID, String vendorCodeSet) {
			List status = new ArrayList();
			if(vendorCodeSet!=null && (!(vendorCodeSet.trim().equals("")))){
				quotations= getHibernateTemplate().find("from PartnerQuote where quote like '" + quote + "%' AND quoteStatus like '" + quoteStatus + "%' and vendorCode in ("+vendorCodeSet+") and corpID= '"+sessionCorpID+"'");	
			}else{
			quotations= getHibernateTemplate().find("from PartnerQuote where quote like '" + quote + "%' AND quoteStatus like '" + quoteStatus + "%' and corpID= '"+sessionCorpID+"' ");
			}
			return quotations;
		}
		public List checkQuotation(Long quote) {
			
			return getHibernateTemplate().find("select sid from Quotation where pqId=?",quote);
		}
		
		public List<Quotation> getQuotation(Long sID, Long quoteID){
			return getHibernateTemplate().find("from Quotation where sid='"+sID+"' AND pqId='"+quoteID+"'");
		}
		
		public List checkQuotation2(Long sID, Long quoteID){
			return getHibernateTemplate().find("select id from Quotation where sid='"+sID+"' AND pqId='"+quoteID+"'");
		}
		public void submitQuotation(Long quoteID) {
			int i = getHibernateTemplate().bulkUpdate("update Quotation set quoteStatus='Submitted', updatedOn=now()  where pqId=?",quoteID);
			int j = getHibernateTemplate().bulkUpdate("update PartnerQuote set quoteStatus='Submitted', updatedOn=now() where id=?",quoteID);
		}
		public List nextServiceOrder(String requestedSO, Long sid) {
			Long soid;
			if(sid == null  ||  sid.equals("")){
				soid = Long.parseLong("0");
			}else{
				 soid = sid;
			}
			
			String reqSO =  requestedSO.replaceAll(",", "','");
			return getHibernateTemplate().find("select min(id) from ServiceOrder where shipNumber in ('"+reqSO+"') and id>?",soid);
		}
		public List previousServiceOrder(String requestedSO, Long sid) {
			Long soid;
			if(sid == null  ||  sid.equals("")){
				soid = Long.parseLong("0");
			}else{
				 soid = sid;
			}
			
			String reqSO =  requestedSO.replaceAll(",", "','");
			return getHibernateTemplate().find("select max(id) from ServiceOrder where shipNumber in ('"+reqSO+"') and id<?",soid);
		}
		public void processQuotation(Long quoteID) {
			int j = getHibernateTemplate().bulkUpdate("update PartnerQuote set quoteStatus='Processing', updatedOn=now() where id=?",quoteID);
			
		}
	public List  BookingAgentName(String partnerCode, String corpid, Boolean cmmDmmFlag){
		List agentCode= new ArrayList();
		List partnerType=this.getSession().createSQLQuery("select * from partner where partnerCode='"+partnerCode+"' and corpID in ('TSFT','"+getHibernateUtil().getParentCorpID(corpid)+"','"+corpid+"') and isAgent=true").list();
		if(partnerType!=null && (!partnerType.isEmpty())){
			agentCode=this.getSession().createSQLQuery("select lastName, status, companyDivision, if(terminalEmail is null , '' ,terminalEmail), if(trackingUrl is null , '' ,trackingUrl), paymentMethod  from partner where  partnerCode='"+partnerCode+"' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"') and status in('New','Approved')").list();
		}else{
			agentCode=this.getSession().createSQLQuery("select lastName, status, companyDivision, if(terminalEmail is null , '' ,terminalEmail), if(trackingUrl is null , '' ,trackingUrl), paymentMethod  from partner where  partnerCode='"+partnerCode+"' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"') and status='Approved' ").list();	
		}
	 return agentCode;
	}
	public List findBookCodeStatus(String corpId, String bookAg){
		return this.getSession().createSQLQuery("select lastName, status, companyDivision, if(terminalEmail is null , '' ,terminalEmail), if(trackingUrl is null , '' ,trackingUrl), paymentMethod  from partner where  partnerCode='"+bookAg+"' and corpID in ('"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"')").list();
		
	}
	public String findBookCodeStatusAjax(String corpId, String bookAg){
		String []bookCode=bookAg.split("~");
		String record="BookingAgent:"+(getBookingCodeStatus(corpId,bookCode[0]).equalsIgnoreCase("")?"N":"Y")+",BillToCode:"+(getBookingCodeStatus(corpId,bookCode[1]).equalsIgnoreCase("")?"N":"Y");
		return record;
	}
	public String getBookingCodeStatus(String corpId, String bookingCode){
		String status="";
		List al=this.getSession().createSQLQuery("select status  from partner where  partnerCode = '"+bookingCode+"' and corpID in ('"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"')").list();
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().equalsIgnoreCase(""))){
			status= al.get(0).toString();
		}
		return status;
	}
	public HibernateUtil getHibernateUtil() {
		return hibernateUtil;
	}
	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public  Map<String, String> findBookCodeQuotes(String corpId, Long id){
		List quotesStatus=new ArrayList();
		Map<String, String> bookingAgencyMap = new LinkedHashMap<String, String>();
		String BACode="";
		 quotesStatus=this.getSession().createSQLQuery("select bookingAgentCode,shipNumber from serviceorder  where customerFileId='"+id+"' and corpid='"+corpId+"'").list();
		if(quotesStatus!=null && !quotesStatus.isEmpty()){
			Iterator it=quotesStatus.iterator();
			while(it.hasNext())
			  {
			 Object [] row=(Object[])it.next();
			 //BACode=this.getSession().createSQLQuery("select bookingAgentCode from serviceorder  where shipNumber='"+row[0]+"' and corpid='"+corpId+"'").list().get(0).toString();
			List partnerType=this.getSession().createSQLQuery("select * from partner where partnerCode='"+row[0]+"' and corpID in ('"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') and status='Approved'").list();
			if(partnerType==null || partnerType.isEmpty()){
				bookingAgencyMap.put(row[0].toString(), row[1].toString());
			}
			  }
		}
		return bookingAgencyMap;
	}
}
