package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CompensationSetupDao;
import com.trilasoft.app.model.CompensationSetup;
import com.trilasoft.app.model.Contract;

public class CompensationSetupDaoHibernate extends GenericDaoHibernate <CompensationSetup, Long> implements CompensationSetupDao {
	
		public CompensationSetupDaoHibernate() {
			super(CompensationSetup.class);
		}
		
		public List<CompensationSetup> getListFromCompensationSetup(String corpId,Long contractId,String contractName) {
			return getHibernateTemplate().find("from CompensationSetup where contract='"+contractName+"' and contractid='"+contractId+"' order by id  ");
		}

}
