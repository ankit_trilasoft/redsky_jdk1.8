package com.trilasoft.app.model;

import java.util.Date;
import java.math.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="dstemporaryaccommodation")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class DsTemporaryAccommodation extends BaseObject{
	
	private Long id;
	private Long serviceOrderId;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private String vendorCode;
	private String vendorName;
	private String vendorContact;
	private String vendorEmail;
	private Date serviceStartDate;
	private Date serviceEndDate;
	private String nightsAuthorized;
	private String currency;
	private BigDecimal nightlyMaximumAmt;
	private Date checkInDate;
	private Date checkOutDate;
	private Date providerConfirmationReceived;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"serviceOrderId", serviceOrderId).append("corpID", corpID)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("vendorCode", vendorCode).append("vendorName",
						vendorName).append("vendorContact", vendorContact)
				.append("vendorEmail", vendorEmail).append("serviceStartDate",
						serviceStartDate).append("serviceEndDate",
						serviceEndDate).append("nightsAuthorized",
						nightsAuthorized).append("currency",currency).append("nightlyMaximumAmt",
						nightlyMaximumAmt).append("checkInDate", checkInDate)
				.append("checkOutDate", checkOutDate).append(
						"providerConfirmationReceived",
						providerConfirmationReceived).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DsTemporaryAccommodation))
			return false;
		DsTemporaryAccommodation castOther = (DsTemporaryAccommodation) other;
		return new EqualsBuilder().append(id, castOther.id).append(
				serviceOrderId, castOther.serviceOrderId).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(vendorCode,
						castOther.vendorCode).append(vendorName,
						castOther.vendorName).append(vendorContact,
						castOther.vendorContact).append(vendorEmail,
						castOther.vendorEmail).append(serviceStartDate,
						castOther.serviceStartDate).append(serviceEndDate,
						castOther.serviceEndDate).append(nightsAuthorized,
						castOther.nightsAuthorized).append(currency, castOther.currency).append(nightlyMaximumAmt,
						castOther.nightlyMaximumAmt).append(checkInDate,
						castOther.checkInDate).append(checkOutDate,
						castOther.checkOutDate).append(
						providerConfirmationReceived,
						castOther.providerConfirmationReceived).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId).append(
				corpID).append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(vendorCode).append(vendorName)
				.append(vendorContact).append(vendorEmail).append(
						serviceStartDate).append(serviceEndDate).append(
						nightsAuthorized).append(currency).append(nightlyMaximumAmt).append(
						checkInDate).append(checkOutDate).append(
						providerConfirmationReceived).toHashCode();
	}
	
	@Column(length=30)	
	public String getCorpID() {
		return corpID;
	}
	/**
	 * @param corpID the corpID to set
	 */
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	/**
	 * @return the createdBy
	 */
	@Column(length=82)	
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the id
	 */
	@Id
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the notificationDate
	 */
	@Column	
	public Date getProviderConfirmationReceived() {
		return providerConfirmationReceived;
	}
	/**
	 * @param notificationDate the notificationDate to set
	 */
	public void setProviderConfirmationReceived(Date providerConfirmationReceived) {
		this.providerConfirmationReceived = providerConfirmationReceived;
	}
	/**
	 * @return the serviceEndDate
	 */
	@Column
	public Date getServiceEndDate() {
		return serviceEndDate;
	}
	/**
	 * @param serviceEndDate the serviceEndDate to set
	 */
	public void setServiceEndDate(Date serviceEndDate) {
		this.serviceEndDate = serviceEndDate;
	}
	/**
	 * @return the serviceOrderId
	 */
	@Column(length=20)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	/**
	 * @param serviceOrderId the serviceOrderId to set
	 */
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	/**
	 * @return the serviceStartDate
	 */
	@Column
	public Date getServiceStartDate() {
		return serviceStartDate;
	}
	/**
	 * @param serviceStartDate the serviceStartDate to set
	 */
	public void setServiceStartDate(Date serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}
	/**
	 * @return the updatedBy
	 */
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the vendorCode
	 */
	@Column(length=25)	
	public String getVendorCode() {
		return vendorCode;
	}
	/**
	 * @param vendorCode the vendorCode to set
	 */
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	/**
	 * @return the vendorContact
	 */
	@Column(length=225)
	public String getVendorContact() {
		return vendorContact;
	}
	/**
	 * @param vendorContact the vendorContact to set
	 */
	public void setVendorContact(String vendorContact) {
		this.vendorContact = vendorContact;
	}
	/**
	 * @return the vendorEmail
	 */
	@Column(length=65)
	public String getVendorEmail() {
		return vendorEmail;
	}
	/**
	 * @param vendorEmail the vendorEmail to set
	 */
	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}
	/**
	 * @return the vendorName
	 */
	@Column(length=255)	
	public String getVendorName() {
		return vendorName;
	}
	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	@Column(length=3)	
	public String getNightsAuthorized() {
		return nightsAuthorized;
	}
	public void setNightsAuthorized(String nightsAuthorized) {
		this.nightsAuthorized = nightsAuthorized;
	}
	@Column(length=3)	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	
	@Column	
	public BigDecimal getNightlyMaximumAmt() {
		return nightlyMaximumAmt;
	}
	public void setNightlyMaximumAmt(BigDecimal nightlyMaximumAmt) {
		this.nightlyMaximumAmt = nightlyMaximumAmt;
	}	
	
	@Column
	public Date getCheckInDate() {
		return checkInDate;
	}
	/**
	 * @param serviceStartDate the serviceStartDate to set
	 */
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}
	
	@Column
	public Date getCheckOutDate() {
		return checkOutDate;
	}
	/**
	 * @param serviceStartDate the serviceStartDate to set
	 */
	public void setCheckOutDate(Date checkOutDate) {
		this.checkOutDate = checkOutDate;
	}

	
	
}
