<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
</head>

    
<s:form name="sickPersonal" id="sickPersonal" action="updateSickPersonal" method="post" validate="true">
<c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden id="checkDaysClickTicket" name="checkDaysClickTicket" />

<s:hidden name="formStatus" value=""/>
<table> 
		   <tr>
		   <td class="listwhitetext" align="right">Existing Calendar</td> 
	  	  	<td> 	 
		  		<s:textfield cssClass="input-text" id="fromDate" name="fromDate" value="%{fromDate}" size="8" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this);" /> 
		  		<img id="calender3" align="top" src="${pageContext.request.contextPath}/images/calender.png"HEIGHT=20 WIDTH=20onclick=" onclick="cal.select(document.forms['sickPersonal'].fromDate,'calender3',document.forms['sickPersonal'].dateFormat.value);  forDays(); return false;" />
		  	</td>
		   </tr>
	   
		   <tr><td height="50px"></td></tr>   

            <tr> 
            <td class="listwhitetext" align="right">New Calendar</td>             
              <td>
                <s:textfield  cssClass="input-text" id="f_rangeStart" name="f_rangeStart" value="%{f_rangeStart}" size="8"  readonly="true" onkeydown="return onlyDel(event,this);"/>              
                <img id="f_rangeStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
               
                <script type="text/javascript">
                  RANGE_CAL_1 = new Calendar({
                          inputField: "f_rangeStart",
                          dateFormat: "%d-%b-%y",
                          trigger: "f_rangeStart_trigger",
                        
                          
                          bottomBar: true,
                          animation:true,
                          onSelect: function() {                             
                              this.hide();
                      }
                       
                  });
                  
                </script>
              </td>
            </tr>
            
           
            
            
            
          </table>
          
          	</s:form>
          	
          	
          	
          	
          	
          	