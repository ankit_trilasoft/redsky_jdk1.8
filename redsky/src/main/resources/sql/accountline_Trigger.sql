DELIMITER $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_accountline` $$
CREATE
DEFINER=`root`@`%`
TRIGGER `redsky`.`trigger_add_history_accountline`
BEFORE UPDATE ON `redsky`.`accountline`
FOR EACH ROW
BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;
   DECLARE pppartnercode LONGTEXT;
   DECLARE updatedon DATETIME;
  set NEW.updatedon=now();
  
   SET fieldNameList = "";
   SET oldValueList = ""; 
   SET newValueList = "";
   SET pppartnercode = "";
   
IF((NEW.estimateSellQuantity=0.00 or NEW.estimateSellQuantity=0 or NEW.estimateSellQuantity='' or NEW.estimateSellQuantity is null) and NEW.estimateQuantity>0) THEN
 set NEW.estimateSellQuantity=NEW.estimateQuantity;
END IF;

IF((NEW.revisionSellQuantity=0.00 or NEW.revisionSellQuantity=0 or NEW.revisionSellQuantity='' or NEW.revisionSellQuantity is null) and NEW.revisionQuantity>0) THEN
 set NEW.revisionSellQuantity=NEW.revisionQuantity;
END IF;


      IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
       select CONCAT(fieldNameList,'accountline.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
   END IF;
   IF (OLD.invoicereportparameter <> NEW.invoicereportparameter ) THEN
       select CONCAT(fieldNameList,'accountline.invoicereportparameter~') into fieldNameList;
       select CONCAT(oldValueList,OLD.invoicereportparameter,'~') into oldValueList;
       select CONCAT(newValueList,NEW.invoicereportparameter,'~') into newValueList;
   END IF;
IF (OLD.shipNumber <> NEW.shipNumber ) THEN
       select CONCAT(fieldNameList,'accountline.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
   END IF;
  
   IF (OLD.corpID <> NEW.corpID ) THEN
       select CONCAT(fieldNameList,'accountline.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
       select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
   END IF;

IF (OLD.invoiceCreatedBy <> NEW.invoiceCreatedBy ) THEN
       select CONCAT(fieldNameList,'accountline.invoiceCreatedBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.invoiceCreatedBy,'~') into oldValueList;
       select CONCAT(newValueList,NEW.invoiceCreatedBy,'~') into newValueList;
   END IF;

   IF (OLD.creditInvoiceNumber <> NEW.creditInvoiceNumber ) THEN
       select CONCAT(fieldNameList,'accountline.creditInvoiceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.creditInvoiceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.creditInvoiceNumber,'~') into newValueList;
   END IF;

   IF (OLD.truckNumber <> NEW.truckNumber ) THEN
       select CONCAT(fieldNameList,'accountline.truckNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.truckNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.truckNumber,'~') into newValueList;
   END IF;

   IF (OLD.oldBillToCode <> NEW.oldBillToCode ) THEN
       select CONCAT(fieldNameList,'accountline.oldBillToCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.oldBillToCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.oldBillToCode,'~') into newValueList;
   END IF;

   IF (OLD.includeLHF <> NEW.includeLHF ) THEN
       select CONCAT(fieldNameList,'accountline.includeLHF~') into fieldNameList;
       select CONCAT(oldValueList,OLD.includeLHF,'~') into oldValueList;
       select CONCAT(newValueList,NEW.includeLHF,'~') into newValueList;
   END IF;

   IF (OLD.companyDivision <> NEW.companyDivision ) THEN
       select CONCAT(fieldNameList,'accountline.companyDivision~') into fieldNameList;
       select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
       select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
   END IF;

   IF (OLD.displayOnQuote <> NEW.displayOnQuote ) THEN
       select CONCAT(fieldNameList,'accountline.displayOnQuote~') into fieldNameList;
       select CONCAT(oldValueList,OLD.displayOnQuote,'~') into oldValueList;
       select CONCAT(newValueList,NEW.displayOnQuote,'~') into newValueList;
   END IF;

   IF (OLD.revisionValueDate <> NEW.revisionValueDate ) THEN
       select CONCAT(fieldNameList,'accountline.revisionValueDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionValueDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionValueDate,'~') into newValueList;
   END IF;

   IF (OLD.uvlControlNumber <> NEW.uvlControlNumber ) THEN
       select CONCAT(fieldNameList,'accountline.uvlControlNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.uvlControlNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.uvlControlNumber,'~') into newValueList;
   END IF;

   IF (OLD.ddrControlNumber <> NEW.ddrControlNumber ) THEN
       select CONCAT(fieldNameList,'accountline.ddrControlNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ddrControlNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ddrControlNumber,'~') into newValueList;
   END IF;

   IF (OLD.revisionLocalRate <> NEW.revisionLocalRate ) THEN
       select CONCAT(fieldNameList,'accountline.revisionLocalRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionLocalRate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionLocalRate,'~') into newValueList;
   END IF;

   IF (OLD.estimateSellDeviation <> NEW.estimateSellDeviation ) THEN
       select CONCAT(fieldNameList,'accountline.estimateSellDeviation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimateSellDeviation,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimateSellDeviation,'~') into newValueList;
   END IF;
   IF (OLD.estimateDeviation <> NEW.estimateDeviation ) THEN
       select CONCAT(fieldNameList,'accountline.estimateDeviation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimateDeviation,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimateDeviation,'~') into newValueList;
   END IF;

   IF (OLD.revisionSellDeviation <> NEW.revisionSellDeviation ) THEN
       select CONCAT(fieldNameList,'accountline.revisionSellDeviation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionSellDeviation,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionSellDeviation,'~') into newValueList;
   END IF;

   IF (OLD.revisionDeviation <> NEW.revisionDeviation ) THEN
       select CONCAT(fieldNameList,'accountline.revisionDeviation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionDeviation,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionDeviation,'~') into newValueList;
   END IF;
   IF (OLD.groupAccount <> NEW.groupAccount ) THEN
       select CONCAT(fieldNameList,'accountline.groupAccount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.groupAccount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.groupAccount,'~') into newValueList;
   END IF;
   IF (OLD.buyDependSell <> NEW.buyDependSell ) THEN
       select CONCAT(fieldNameList,'accountline.buyDependSell~') into fieldNameList;
       select CONCAT(oldValueList,OLD.buyDependSell,'~') into oldValueList;
       select CONCAT(newValueList,NEW.buyDependSell,'~') into newValueList;
   END IF;
   IF (OLD.additionalService <> NEW.additionalService ) THEN
       select CONCAT(fieldNameList,'accountline.additionalService~') into fieldNameList;
       select CONCAT(oldValueList,OLD.additionalService,'~') into oldValueList;
       select CONCAT(newValueList,NEW.additionalService,'~') into newValueList;
   END IF;
   IF (OLD.settledDate <> NEW.settledDate ) THEN
       select CONCAT(fieldNameList,'accountline.settledDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.settledDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.settledDate,'~') into newValueList;
   END IF;
   IF (OLD.receivableSellDeviation <> NEW.receivableSellDeviation ) THEN
       select CONCAT(fieldNameList,'accountline.receivableSellDeviation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.receivableSellDeviation,'~') into oldValueList;
       select CONCAT(newValueList,NEW.receivableSellDeviation,'~') into newValueList;
   END IF;
   IF (OLD.agentFeesNo <> NEW.agentFeesNo ) THEN
       select CONCAT(fieldNameList,'accountline.agentFeesNo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.agentFeesNo,'~') into oldValueList;
       select CONCAT(newValueList,NEW.agentFeesNo,'~') into newValueList;
   END IF;
   IF (OLD.payableContractRateAmmount <> NEW.payableContractRateAmmount ) THEN
       select CONCAT(fieldNameList,'accountline.payableContractRateAmmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payableContractRateAmmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payableContractRateAmmount,'~') into newValueList;
   END IF;

   IF (OLD.estimateContractValueDate <> NEW.estimateContractValueDate ) THEN
       select CONCAT(fieldNameList,'accountline.estimateContractValueDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimateContractValueDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimateContractValueDate,'~') into newValueList;
   END IF;

   IF (OLD.revisionContractCurrency <> NEW.revisionContractCurrency ) THEN
       select CONCAT(fieldNameList,'accountline.revisionContractCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionContractCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionContractCurrency,'~') into newValueList;
   END IF;

   IF (OLD.revisionContractRate <> NEW.revisionContractRate ) THEN
       select CONCAT(fieldNameList,'accountline.revisionContractRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionContractRate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionContractRate,'~') into newValueList;
   END IF;

   IF (OLD.revisionContractExchangeRate <> NEW.revisionContractExchangeRate ) THEN
       select CONCAT(fieldNameList,'accountline.revisionContractExchangeRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionContractExchangeRate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionContractExchangeRate,'~') into newValueList;
   END IF;

   IF (OLD.revisionContractRateAmmount <> NEW.revisionContractRateAmmount ) THEN
       select CONCAT(fieldNameList,'accountline.revisionContractRateAmmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionContractRateAmmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionContractRateAmmount,'~') into newValueList;
   END IF;

   IF (OLD.revisionContractValueDate <> NEW.revisionContractValueDate ) THEN
       select CONCAT(fieldNameList,'accountline.revisionContractValueDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionContractValueDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionContractValueDate,'~') into newValueList;
   END IF;

   IF (OLD.revisionPayableContractCurrency <> NEW.revisionPayableContractCurrency ) THEN
       select CONCAT(fieldNameList,'accountline.revisionPayableContractCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionPayableContractCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionPayableContractCurrency,'~') into newValueList;
   END IF;

   IF (OLD.revisionPayableContractRate <> NEW.revisionPayableContractRate ) THEN
       select CONCAT(fieldNameList,'accountline.revisionPayableContractRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionPayableContractRate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionPayableContractRate,'~') into newValueList;
   END IF;

   IF (OLD.revisionPayableContractExchangeRate <> NEW.revisionPayableContractExchangeRate ) THEN
       select CONCAT(fieldNameList,'accountline.revisionPayableContractExchangeRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionPayableContractExchangeRate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionPayableContractExchangeRate,'~') into newValueList;
   END IF;

   IF (OLD.revisionPayableContractRateAmmount <> NEW.revisionPayableContractRateAmmount ) THEN
       select CONCAT(fieldNameList,'accountline.revisionPayableContractRateAmmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionPayableContractRateAmmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionPayableContractRateAmmount,'~') into newValueList;
   END IF;

   IF (OLD.revisionPayableContractValueDate <> NEW.revisionPayableContractValueDate ) THEN
       select CONCAT(fieldNameList,'accountline.revisionPayableContractValueDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionPayableContractValueDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionPayableContractValueDate,'~') into newValueList;
   END IF;

   IF (OLD.estimatePayableContractCurrency <> NEW.estimatePayableContractCurrency ) THEN
       select CONCAT(fieldNameList,'accountline.estimatePayableContractCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimatePayableContractCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimatePayableContractCurrency,'~') into newValueList;
   END IF;

   IF (OLD.estimatePayableContractRate <> NEW.estimatePayableContractRate ) THEN
       select CONCAT(fieldNameList,'accountline.estimatePayableContractRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimatePayableContractRate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimatePayableContractRate,'~') into newValueList;
   END IF;

   IF (OLD.estimatePayableContractExchangeRate <> NEW.estimatePayableContractExchangeRate ) THEN
       select CONCAT(fieldNameList,'accountline.estimatePayableContractExchangeRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimatePayableContractExchangeRate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimatePayableContractExchangeRate,'~') into newValueList;
   END IF;

   IF (OLD.estimatePayableContractRateAmmount <> NEW.estimatePayableContractRateAmmount ) THEN
       select CONCAT(fieldNameList,'accountline.estimatePayableContractRateAmmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimatePayableContractRateAmmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimatePayableContractRateAmmount,'~') into newValueList;
   END IF;

   IF (OLD.estimatePayableContractValueDate <> NEW.estimatePayableContractValueDate ) THEN
       select CONCAT(fieldNameList,'accountline.estimatePayableContractValueDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimatePayableContractValueDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimatePayableContractValueDate,'~') into newValueList;
   END IF;

   IF (OLD.estSellCurrency <> NEW.estSellCurrency ) THEN
       select CONCAT(fieldNameList,'accountline.estSellCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estSellCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estSellCurrency,'~') into newValueList;
   END IF;

   IF (OLD.estSellLocalAmount <> NEW.estSellLocalAmount ) THEN
       select CONCAT(fieldNameList,'accountline.estSellLocalAmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estSellLocalAmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estSellLocalAmount,'~') into newValueList;
   END IF;

   IF (OLD.estSellExchangeRate <> NEW.estSellExchangeRate ) THEN
       select CONCAT(fieldNameList,'accountline.estSellExchangeRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estSellExchangeRate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estSellExchangeRate,'~') into newValueList;
   END IF;

   IF (OLD.estSellValueDate <> NEW.estSellValueDate ) THEN
       select CONCAT(fieldNameList,'accountline.estSellValueDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estSellValueDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estSellValueDate,'~') into newValueList;
   END IF;

   IF (OLD.estSellLocalRate <> NEW.estSellLocalRate ) THEN
       select CONCAT(fieldNameList,'accountline.estSellLocalRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estSellLocalRate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estSellLocalRate,'~') into newValueList;
   END IF;

   IF (OLD.costTransferred <> NEW.costTransferred ) THEN
       select CONCAT(fieldNameList,'accountline.costTransferred~') into fieldNameList;
       select CONCAT(oldValueList,OLD.costTransferred,'~') into oldValueList;
       select CONCAT(newValueList,NEW.costTransferred,'~') into newValueList;
   END IF;

   IF (OLD.categoryResource <> NEW.categoryResource ) THEN
       select CONCAT(fieldNameList,'accountline.categoryResource~') into fieldNameList;
       select CONCAT(oldValueList,OLD.categoryResource,'~') into oldValueList;
       select CONCAT(newValueList,NEW.categoryResource,'~') into newValueList;
   END IF;

   IF (OLD.categoryResourceName <> NEW.categoryResourceName ) THEN
       select CONCAT(fieldNameList,'accountline.categoryResourceName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.categoryResourceName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.categoryResourceName,'~') into newValueList;
   END IF;

   IF (OLD.t20Process <> NEW.t20Process ) THEN
       select CONCAT(fieldNameList,'accountline.t20Process~') into fieldNameList;
       select CONCAT(oldValueList,OLD.t20Process,'~') into oldValueList;
       select CONCAT(newValueList,NEW.t20Process,'~') into newValueList;
   END IF;

   IF (OLD.activateAccPortal <> NEW.activateAccPortal ) THEN
       select CONCAT(fieldNameList,'accountline.activateAccPortal~') into fieldNameList;
       select CONCAT(oldValueList,OLD.activateAccPortal,'~') into oldValueList;
       select CONCAT(newValueList,NEW.activateAccPortal,'~') into newValueList;
   END IF;

   IF (OLD.vanlineSettleDate <> NEW.vanlineSettleDate ) THEN
       select CONCAT(fieldNameList,'accountline.vanlineSettleDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.vanlineSettleDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.vanlineSettleDate,'~') into newValueList;
   END IF;

   IF (OLD.onHand <> NEW.onHand ) THEN
       select CONCAT(fieldNameList,'accountline.onHand~') into fieldNameList;
       select CONCAT(oldValueList,OLD.onHand,'~') into oldValueList;
       select CONCAT(newValueList,NEW.onHand,'~') into newValueList;
   END IF;

   IF (OLD.paymentsent <> NEW.paymentsent ) THEN
       select CONCAT(fieldNameList,'accountline.paymentsent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.paymentsent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.paymentsent,'~') into newValueList;
   END IF;

   IF (OLD.download <> NEW.download ) THEN
       select CONCAT(fieldNameList,'accountline.download~') into fieldNameList;
       select CONCAT(oldValueList,OLD.download,'~') into oldValueList;
       select CONCAT(newValueList,NEW.download,'~') into newValueList;
   END IF;

   IF (OLD.invoiceAutoUpload <> NEW.invoiceAutoUpload ) THEN
       select CONCAT(fieldNameList,'accountline.invoiceAutoUpload~') into fieldNameList;
       select CONCAT(oldValueList,OLD.invoiceAutoUpload,'~') into oldValueList;
       select CONCAT(newValueList,NEW.invoiceAutoUpload,'~') into newValueList;
   END IF;

   IF (OLD.invoiceType <> NEW.invoiceType ) THEN
       select CONCAT(fieldNameList,'accountline.invoiceType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.invoiceType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.invoiceType,'~') into newValueList;
   END IF;

   IF (OLD.driverCompanyDivision <> NEW.driverCompanyDivision ) THEN
       select CONCAT(fieldNameList,'accountline.driverCompanyDivision~') into fieldNameList;
       select CONCAT(oldValueList,OLD.driverCompanyDivision,'~') into oldValueList;
       select CONCAT(newValueList,NEW.driverCompanyDivision,'~') into newValueList;
   END IF;

   IF (OLD.recVatGl <> NEW.recVatGl ) THEN
       select CONCAT(fieldNameList,'accountline.recVatGl~') into fieldNameList;
       select CONCAT(oldValueList,OLD.recVatGl,'~') into oldValueList;
       select CONCAT(newValueList,NEW.recVatGl,'~') into newValueList;
   END IF;

   IF (OLD.payVatGl <> NEW.payVatGl ) THEN
       select CONCAT(fieldNameList,'accountline.payVatGl~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payVatGl,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payVatGl,'~') into newValueList;
   END IF;

   IF (OLD.selectiveInvoice <> NEW.selectiveInvoice ) THEN
       select CONCAT(fieldNameList,'accountline.selectiveInvoice~') into fieldNameList;
       select CONCAT(oldValueList,OLD.selectiveInvoice,'~') into oldValueList;
       select CONCAT(newValueList,NEW.selectiveInvoice,'~') into newValueList;
   END IF;

   IF (OLD.serviceTaxInput <> NEW.serviceTaxInput ) THEN
       select CONCAT(fieldNameList,'accountline.serviceTaxInput~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceTaxInput,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceTaxInput,'~') into newValueList;
   END IF;

   IF (OLD.myFileFileName <> NEW.myFileFileName ) THEN
       select CONCAT(fieldNameList,'accountline.myFileFileName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.myFileFileName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.myFileFileName,'~') into newValueList;
   END IF;

   IF (OLD.externalIntegrationReference <> NEW.externalIntegrationReference ) THEN
       select CONCAT(fieldNameList,'accountline.externalIntegrationReference~') into fieldNameList;
       select CONCAT(oldValueList,OLD.externalIntegrationReference,'~') into oldValueList;
       select CONCAT(newValueList,NEW.externalIntegrationReference,'~') into newValueList;
   END IF;

   IF (OLD.estimateSellQuantity <> NEW.estimateSellQuantity ) THEN
       select CONCAT(fieldNameList,'accountline.estimateSellQuantity~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimateSellQuantity,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimateSellQuantity,'~') into newValueList;
   END IF;

   IF (OLD.revisionSellQuantity <> NEW.revisionSellQuantity ) THEN
       select CONCAT(fieldNameList,'accountline.revisionSellQuantity~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionSellQuantity,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionSellQuantity,'~') into newValueList;
   END IF;

   IF (OLD.estimateDiscount <> NEW.estimateDiscount ) THEN
       select CONCAT(fieldNameList,'accountline.estimateDiscount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimateDiscount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimateDiscount,'~') into newValueList;
   END IF;

   IF (OLD.actualDiscount <> NEW.actualDiscount ) THEN
       select CONCAT(fieldNameList,'accountline.actualDiscount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.actualDiscount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.actualDiscount,'~') into newValueList;
   END IF;

   IF (OLD.revisionDiscount <> NEW.revisionDiscount ) THEN
       select CONCAT(fieldNameList,'accountline.revisionDiscount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionDiscount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionDiscount,'~') into newValueList;
   END IF;

   IF (OLD.accountLineCostElement <> NEW.accountLineCostElement ) THEN
       select CONCAT(fieldNameList,'accountline.accountLineCostElement~') into fieldNameList;
       select CONCAT(oldValueList,OLD.accountLineCostElement,'~') into oldValueList;
       select CONCAT(newValueList,NEW.accountLineCostElement,'~') into newValueList;
   END IF;

   IF (OLD.accountLineScostElementDescription <> NEW.accountLineScostElementDescription ) THEN
       select CONCAT(fieldNameList,'accountline.accountLineScostElementDescription~') into fieldNameList;
       select CONCAT(oldValueList,OLD.accountLineScostElementDescription,'~') into oldValueList;
       select CONCAT(newValueList,NEW.accountLineScostElementDescription,'~') into newValueList;
   END IF;

   IF (OLD.dynamicNavisionflag <> NEW.dynamicNavisionflag ) THEN
       select CONCAT(fieldNameList,'accountline.dynamicNavisionflag~') into fieldNameList;
       select CONCAT(oldValueList,OLD.dynamicNavisionflag,'~') into oldValueList;
       select CONCAT(newValueList,NEW.dynamicNavisionflag,'~') into newValueList;
   END IF;

   IF (OLD.deleteDynamicNavisionflag <> NEW.deleteDynamicNavisionflag ) THEN
       select CONCAT(fieldNameList,'accountline.deleteDynamicNavisionflag~') into fieldNameList;
       select CONCAT(oldValueList,OLD.deleteDynamicNavisionflag,'~') into oldValueList;
       select CONCAT(newValueList,NEW.deleteDynamicNavisionflag,'~') into newValueList;
   END IF;

   IF (OLD.oldCompanyDivision <> NEW.oldCompanyDivision ) THEN
       select CONCAT(fieldNameList,'accountline.oldCompanyDivision~') into fieldNameList;
       select CONCAT(oldValueList,OLD.oldCompanyDivision,'~') into oldValueList;
       select CONCAT(newValueList,NEW.oldCompanyDivision,'~') into newValueList;
   END IF;

   IF (OLD.sendDynamicNavisionDate <> NEW.sendDynamicNavisionDate ) THEN
       select CONCAT(fieldNameList,'accountline.sendDynamicNavisionDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sendDynamicNavisionDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.sendDynamicNavisionDate,'~') into newValueList;
   END IF;

   IF (OLD.dynamicNavXfer <> NEW.dynamicNavXfer ) THEN
       select CONCAT(fieldNameList,'accountline.dynamicNavXfer~') into fieldNameList;
       select CONCAT(oldValueList,OLD.dynamicNavXfer,'~') into oldValueList;
       select CONCAT(newValueList,NEW.dynamicNavXfer,'~') into newValueList;
   END IF;

   IF (OLD.estExpVatAmt <> NEW.estExpVatAmt ) THEN
       select CONCAT(fieldNameList,'accountline.estExpVatAmt~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estExpVatAmt,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estExpVatAmt,'~') into newValueList;
   END IF;

   IF (OLD.revisionExpVatAmt <> NEW.revisionExpVatAmt ) THEN
       select CONCAT(fieldNameList,'accountline.revisionExpVatAmt~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionExpVatAmt,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionExpVatAmt,'~') into newValueList;
   END IF;

   IF (OLD.qstRecVatAmt <> NEW.qstRecVatAmt ) THEN
       select CONCAT(fieldNameList,'accountline.qstRecVatAmt~') into fieldNameList;
       select CONCAT(oldValueList,OLD.qstRecVatAmt,'~') into oldValueList;
       select CONCAT(newValueList,NEW.qstRecVatAmt,'~') into newValueList;
   END IF;

   IF (OLD.qstPayVatAmt <> NEW.qstPayVatAmt ) THEN
       select CONCAT(fieldNameList,'accountline.qstPayVatAmt~') into fieldNameList;
       select CONCAT(oldValueList,OLD.qstPayVatAmt,'~') into oldValueList;
       select CONCAT(newValueList,NEW.qstPayVatAmt,'~') into newValueList;
   END IF;

   IF (OLD.qstRecVatGl <> NEW.qstRecVatGl ) THEN
       select CONCAT(fieldNameList,'accountline.qstRecVatGl~') into fieldNameList;
       select CONCAT(oldValueList,OLD.qstRecVatGl,'~') into oldValueList;
       select CONCAT(newValueList,NEW.qstRecVatGl,'~') into newValueList;
   END IF;

   IF (OLD.qstPayVatGl <> NEW.qstPayVatGl ) THEN
       select CONCAT(fieldNameList,'accountline.qstPayVatGl~') into fieldNameList;
       select CONCAT(oldValueList,OLD.qstPayVatGl,'~') into oldValueList;
       select CONCAT(newValueList,NEW.qstPayVatGl,'~') into newValueList;
   END IF;

   IF (OLD.rollUpInInvoice <> NEW.rollUpInInvoice ) THEN
       select CONCAT(fieldNameList,'accountline.rollUpInInvoice~') into fieldNameList;
       select CONCAT(oldValueList,OLD.rollUpInInvoice,'~') into oldValueList;
       select CONCAT(newValueList,NEW.rollUpInInvoice,'~') into newValueList;
   END IF;

   IF (OLD.auditCompleteDate <> NEW.auditCompleteDate ) THEN
       select CONCAT(fieldNameList,'accountline.auditCompleteDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.auditCompleteDate,'~') into oldValueList;
       select CONCAT(newValueList,NEW.auditCompleteDate,'~') into newValueList;
   END IF;
  IF (OLD.accountLineNumber <> NEW.accountLineNumber ) THEN
       select CONCAT(fieldNameList,'accountline.accountLineNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.accountLineNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.accountLineNumber,'~') into newValueList;
   END IF;

   IF (OLD.category <> NEW.category ) THEN
       select CONCAT(fieldNameList,'accountline.category~') into fieldNameList;
       select CONCAT(oldValueList,OLD.category,'~') into oldValueList;
       select CONCAT(newValueList,NEW.category,'~') into newValueList;
   END IF;
   IF (OLD.vendorCode <> NEW.vendorCode ) THEN
       select CONCAT(fieldNameList,'accountline.vendorCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.vendorCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.vendorCode,'~') into newValueList;
   END IF;

   IF (OLD.name <> NEW.name ) THEN
       select CONCAT(fieldNameList,'accountline.name~') into fieldNameList;
       select CONCAT(oldValueList,OLD.name,'~') into oldValueList;
       select CONCAT(newValueList,NEW.name,'~') into newValueList;
   END IF;

   IF (OLD.chargeCode <> NEW.chargeCode ) THEN
       select CONCAT(fieldNameList,'accountline.chargeCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.chargeCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.chargeCode,'~') into newValueList;
   END IF;

   IF (OLD.reference <> NEW.reference ) THEN
       select CONCAT(fieldNameList,'accountline.reference~') into fieldNameList;
       select CONCAT(oldValueList,OLD.reference,'~') into oldValueList;
       select CONCAT(newValueList,NEW.reference,'~') into newValueList;
   END IF;
   IF (OLD.billToCode <> NEW.billToCode ) THEN
       select CONCAT(fieldNameList,'accountline.billToCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.billToCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.billToCode,'~') into newValueList;
   END IF;

   IF (OLD.billToName <> NEW.billToName ) THEN
       select CONCAT(fieldNameList,'accountline.billToName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.billToName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.billToName,'~') into newValueList;
   END IF;
   
   if(OLD.estimateRate is not null and NEW.estimateRate is null) then
   set NEW.estimateRate=OLD.estimateRate;
   end if;
   IF ((OLD.estimateRate <> NEW.estimateRate) or (OLD.estimateRate is null and NEW.estimateRate is not null)) THEN
        select CONCAT(fieldNameList,'accountline.estimateRate~') into fieldNameList;
        IF(OLD.estimateRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateRate is not null) then
            select CONCAT(oldValueList,OLD.estimateRate,'~') into oldValueList;
        END IF;
        IF(NEW.estimateRate is not null) then
            select CONCAT(newValueList,NEW.estimateRate,'~') into newValueList;
        END IF;
    END IF;
   IF ((OLD.estimateSellRate <> NEW.estimateSellRate) or (OLD.estimateSellRate is null and NEW.estimateSellRate is not null)
      or (OLD.estimateSellRate is not null and NEW.estimateSellRate is null)) THEN
        select CONCAT(fieldNameList,'accountline.estimateSellRate~') into fieldNameList;
        IF(OLD.estimateSellRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateSellRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateSellRate is not null) then
            select CONCAT(oldValueList,OLD.estimateSellRate,'~') into oldValueList;
        END IF;
        IF(NEW.estimateSellRate is not null) then
            select CONCAT(newValueList,NEW.estimateSellRate,'~') into newValueList;
        END IF;
    END IF;
    if(OLD.estimateExpense is not null and NEW.estimateExpense is null) then
    set NEW.estimateExpense=OLD.estimateExpense;
    end if;
   IF ((OLD.estimateExpense <> NEW.estimateExpense) or (OLD.estimateExpense is null and NEW.estimateExpense is not null)) THEN
        select CONCAT(fieldNameList,'accountline.estimateExpense~') into fieldNameList;
        IF(OLD.estimateExpense is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateExpense is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateExpense is not null) then
            select CONCAT(oldValueList,OLD.estimateExpense,'~') into oldValueList;
        END IF;
        IF(NEW.estimateExpense is not null) then
            select CONCAT(newValueList,NEW.estimateExpense,'~') into newValueList;
        END IF;
    END IF;

   IF (OLD.estimateVendorName <> NEW.estimateVendorName ) THEN
       select CONCAT(fieldNameList,'accountline.estimateVendorName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimateVendorName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimateVendorName,'~') into newValueList;
   END IF;
   IF ((OLD.estimateRevenueAmount <> NEW.estimateRevenueAmount) or (OLD.estimateRevenueAmount is null and NEW.estimateRevenueAmount is not null)
      or (OLD.estimateRevenueAmount is not null and NEW.estimateRevenueAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.estimateRevenueAmount~') into fieldNameList;
        IF(OLD.estimateRevenueAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateRevenueAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateRevenueAmount is not null) then
            select CONCAT(oldValueList,OLD.estimateRevenueAmount,'~') into oldValueList;
        END IF;
        IF(NEW.estimateRevenueAmount is not null) then
            select CONCAT(newValueList,NEW.estimateRevenueAmount,'~') into newValueList;
        END IF;
    END IF;

   IF ((OLD.estimateQuantity <> NEW.estimateQuantity) or (OLD.estimateQuantity is null and NEW.estimateQuantity is not null)
      or (OLD.estimateQuantity is not null and NEW.estimateQuantity is null)) THEN
        select CONCAT(fieldNameList,'accountline.estimateQuantity~') into fieldNameList;
        IF(OLD.estimateQuantity is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimateQuantity is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimateQuantity is not null) then
            select CONCAT(oldValueList,OLD.estimateQuantity,'~') into oldValueList;
        END IF;
        IF(NEW.estimateQuantity is not null) then
            select CONCAT(newValueList,NEW.estimateQuantity,'~') into newValueList;
        END IF;
    END IF;

    IF ((OLD.estimatePassPercentage <> NEW.estimatePassPercentage) or (OLD.estimatePassPercentage is null and NEW.estimatePassPercentage is not null)
      or (OLD.estimatePassPercentage is not null and NEW.estimatePassPercentage is null)) THEN
        select CONCAT(fieldNameList,'accountline.estimatePassPercentage~') into fieldNameList;
        IF(OLD.estimatePassPercentage is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatePassPercentage is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatePassPercentage is not null) then
            select CONCAT(oldValueList,OLD.estimatePassPercentage,'~') into oldValueList;
        END IF;
        IF(NEW.estimatePassPercentage is not null) then
            select CONCAT(newValueList,NEW.estimatePassPercentage,'~') into newValueList;
        END IF;
    END IF;


   IF ((OLD.revisionRate <> NEW.revisionRate) or (OLD.revisionRate is null and NEW.revisionRate is not null)
      or (OLD.revisionRate is not null and NEW.revisionRate is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionRate~') into fieldNameList;
        IF(OLD.revisionRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionRate is not null) then
            select CONCAT(oldValueList,OLD.revisionRate,'~') into oldValueList;
        END IF;
        IF(NEW.revisionRate is not null) then
            select CONCAT(newValueList,NEW.revisionRate,'~') into newValueList;
        END IF;
    END IF;

    IF ((OLD.revisionSellRate <> NEW.revisionSellRate) or (OLD.revisionSellRate is null and NEW.revisionSellRate is not null)
      or (OLD.revisionSellRate is not null and NEW.revisionSellRate is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionSellRate~') into fieldNameList;
        IF(OLD.revisionSellRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionSellRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionSellRate is not null) then
            select CONCAT(oldValueList,OLD.revisionSellRate,'~') into oldValueList;
        END IF;
        IF(NEW.revisionSellRate is not null) then
            select CONCAT(newValueList,NEW.revisionSellRate,'~') into newValueList;
        END IF;
    END IF;

   IF ((OLD.revisionExpense <> NEW.revisionExpense) or (OLD.revisionExpense is null and NEW.revisionExpense is not null)
      or (OLD.revisionExpense is not null and NEW.revisionExpense is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionExpense~') into fieldNameList;
        IF(OLD.revisionExpense is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionExpense is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionExpense is not null) then
            select CONCAT(oldValueList,OLD.revisionExpense,'~') into oldValueList;
        END IF;
        IF(NEW.revisionExpense is not null) then
            select CONCAT(newValueList,NEW.revisionExpense,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.revisionRevenueAmount <> NEW.revisionRevenueAmount) or (OLD.revisionRevenueAmount is null and NEW.revisionRevenueAmount is not null)
      or (OLD.revisionRevenueAmount is not null and NEW.revisionRevenueAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionRevenueAmount~') into fieldNameList;
        IF(OLD.revisionRevenueAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionRevenueAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionRevenueAmount is not null) then
            select CONCAT(oldValueList,OLD.revisionRevenueAmount,'~') into oldValueList;
        END IF;
        IF(NEW.revisionRevenueAmount is not null) then
            select CONCAT(newValueList,NEW.revisionRevenueAmount,'~') into newValueList;
        END IF;
    END IF;
 IF ((OLD.revisionQuantity <> NEW.revisionQuantity) or (OLD.revisionQuantity is null and NEW.revisionQuantity is not null)
      or (OLD.revisionQuantity is not null and NEW.revisionQuantity is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionQuantity~') into fieldNameList;
        IF(OLD.revisionQuantity is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionQuantity is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionQuantity is not null) then
            select CONCAT(oldValueList,OLD.revisionQuantity,'~') into oldValueList;
        END IF;
        IF(NEW.revisionQuantity is not null) then
            select CONCAT(newValueList,NEW.revisionQuantity,'~') into newValueList;
        END IF;
    END IF;

  IF ((OLD.revisionPassPercentage <> NEW.revisionPassPercentage) or (OLD.revisionPassPercentage is null and NEW.revisionPassPercentage is not null)
      or (OLD.revisionPassPercentage is not null and NEW.revisionPassPercentage is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionPassPercentage~') into fieldNameList;
        IF(OLD.revisionPassPercentage is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionPassPercentage is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionPassPercentage is not null) then
            select CONCAT(oldValueList,OLD.revisionPassPercentage,'~') into oldValueList;
        END IF;
        IF(NEW.revisionPassPercentage is not null) then
            select CONCAT(newValueList,NEW.revisionPassPercentage,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.entitlementAmount <> NEW.entitlementAmount) or (OLD.entitlementAmount is null and NEW.entitlementAmount is not null)
      or (OLD.entitlementAmount is not null and NEW.entitlementAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.entitlementAmount~') into fieldNameList;
        IF(OLD.entitlementAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.entitlementAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;
        IF(OLD.entitlementAmount is not null) then
            select CONCAT(oldValueList,OLD.entitlementAmount,'~') into oldValueList;
        END IF;
        IF(NEW.entitlementAmount is not null) then
            select CONCAT(newValueList,NEW.entitlementAmount,'~') into newValueList;
        END IF;
    END IF;

 IF ((OLD.expense <> NEW.expense) or (OLD.expense is null and NEW.expense is not null)
      or (OLD.expense is not null and NEW.expense is null)) THEN
        select CONCAT(fieldNameList,'accountline.expense~') into fieldNameList;
        IF(OLD.expense is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.expense is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.expense is not null) then
            select CONCAT(oldValueList,OLD.expense,'~') into oldValueList;
        END IF;
        IF(NEW.expense is not null) then
            select CONCAT(newValueList,NEW.expense,'~') into newValueList;
        END IF;
    END IF;
   IF (OLD.status <> NEW.status ) THEN
       select CONCAT(fieldNameList,'accountline.status~') into fieldNameList;
       IF(OLD.status = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.status = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.status = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.status = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   IF (OLD.contract <> NEW.contract ) THEN
       select CONCAT(fieldNameList,'accountline.contract~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
       select CONCAT(newValueList,NEW.contract,'~') into newValueList;
   END IF;

   IF (OLD.basis <> NEW.basis ) THEN
       select CONCAT(fieldNameList,'accountline.basis~') into fieldNameList;
       select CONCAT(oldValueList,OLD.basis,'~') into oldValueList;
       select CONCAT(newValueList,NEW.basis,'~') into newValueList;
   END IF;

   IF (OLD.estCurrency <> NEW.estCurrency ) THEN
       select CONCAT(fieldNameList,'accountline.estCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estCurrency,'~') into newValueList;
   END IF;

   IF ((date_format(OLD.estValueDate,'%Y-%m-%d') <> date_format(NEW.estValueDate,'%Y-%m-%d')) or (date_format(OLD.estValueDate,'%Y-%m-%d') is null and date_format(NEW.estValueDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.estValueDate,'%Y-%m-%d') is not null and date_format(NEW.estValueDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'accountline.estValueDate~') into fieldNameList;
       IF(OLD.estValueDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.estValueDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.estValueDate is not null) THEN
        select CONCAT(oldValueList,OLD.estValueDate,'~') into oldValueList;
     END IF;
      IF(NEW.estValueDate is not null) THEN
        select CONCAT(newValueList,NEW.estValueDate,'~') into newValueList;
      END IF;
   END IF;
   
   IF ((OLD.estExchangeRate <> NEW.estExchangeRate) or (OLD.estExchangeRate is null and NEW.estExchangeRate is not null)
      or (OLD.estExchangeRate is not null and NEW.estExchangeRate is null)) THEN
        select CONCAT(fieldNameList,'accountline.estExchangeRate~') into fieldNameList;
        IF(OLD.estExchangeRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estExchangeRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estExchangeRate is not null) then
            select CONCAT(oldValueList,OLD.estExchangeRate,'~') into oldValueList;
        END IF;
        IF(NEW.estExchangeRate is not null) then
            select CONCAT(newValueList,NEW.estExchangeRate,'~') into newValueList;
        END IF;
    END IF;

     IF ((OLD.estLocalRate <> NEW.estLocalRate) or (OLD.estLocalRate is null and NEW.estLocalRate is not null)
      or (OLD.estLocalRate is not null and NEW.estLocalRate is null)) THEN
        select CONCAT(fieldNameList,'accountline.estLocalRate~') into fieldNameList;
        IF(OLD.estLocalRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estLocalRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estLocalRate is not null) then
            select CONCAT(oldValueList,OLD.estLocalRate,'~') into oldValueList;
        END IF;
        IF(NEW.estLocalRate is not null) then
            select CONCAT(newValueList,NEW.estLocalRate,'~') into newValueList;
        END IF;
    END IF;

   IF (OLD.itemNew <> NEW.itemNew ) THEN
       select CONCAT(fieldNameList,'accountline.itemNew~') into fieldNameList;
       select CONCAT(oldValueList,OLD.itemNew,'~') into oldValueList;
       select CONCAT(newValueList,NEW.itemNew,'~') into newValueList;
   END IF;
IF (OLD.basisnew <> NEW.basisnew ) THEN
       select CONCAT(fieldNameList,'accountline.basisnew~') into fieldNameList;
       select CONCAT(oldValueList,OLD.basisnew,'~') into oldValueList;
       select CONCAT(newValueList,NEW.basisnew,'~') into newValueList;
   END IF;
   IF (OLD.checkNew <> NEW.checkNew ) THEN
       select CONCAT(fieldNameList,'accountline.checkNew~') into fieldNameList;
       select CONCAT(oldValueList,OLD.checkNew,'~') into oldValueList;
       select CONCAT(newValueList,NEW.checkNew,'~') into newValueList;
   END IF;

   IF (OLD.basisNewType <> NEW.basisNewType ) THEN
       select CONCAT(fieldNameList,'accountline.basisNewType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.basisNewType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.basisNewType,'~') into newValueList;
   END IF;
    IF ((OLD.actualExpense <> NEW.actualExpense) or (OLD.actualExpense is null and NEW.actualExpense is not null)
      or (OLD.actualExpense is not null and NEW.actualExpense is null)) THEN
        select CONCAT(fieldNameList,'accountline.actualExpense~') into fieldNameList;
        IF(OLD.actualExpense is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualExpense is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualExpense is not null) then
            select CONCAT(oldValueList,OLD.actualExpense,'~') into oldValueList;
        END IF;
        IF(NEW.actualExpense is not null) then
            select CONCAT(newValueList,NEW.actualExpense,'~') into newValueList;
        END IF;
    END IF;

IF ((OLD.actualRevenue <> NEW.actualRevenue) or (OLD.actualRevenue is null and NEW.actualRevenue is not null)
      or (OLD.actualRevenue is not null and NEW.actualRevenue is null)) THEN
        select CONCAT(fieldNameList,'accountline.actualRevenue~') into fieldNameList;
        IF(OLD.actualRevenue is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualRevenue is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualRevenue is not null) then
            select CONCAT(oldValueList,OLD.actualRevenue,'~') into oldValueList;
        END IF;
        IF(NEW.actualRevenue is not null) then
            select CONCAT(newValueList,NEW.actualRevenue,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.rate <> NEW.rate) or (OLD.rate is null and NEW.rate is not null)
      or (OLD.rate is not null and NEW.rate is null)) THEN
        select CONCAT(fieldNameList,'accountline.rate~') into fieldNameList;
        IF(OLD.rate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rate is not null) then
            select CONCAT(oldValueList,OLD.rate,'~') into oldValueList;
        END IF;
        IF(NEW.rate is not null) then
            select CONCAT(newValueList,NEW.rate,'~') into newValueList;
        END IF;
    END IF;
   IF (OLD.recInvoiceNumber <> NEW.recInvoiceNumber ) THEN
       select CONCAT(fieldNameList,'accountline.recInvoiceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.recInvoiceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.recInvoiceNumber,'~') into newValueList;
   END IF;

  IF ((date_format(OLD.receivedInvoiceDate,'%Y-%m-%d') <> date_format(NEW.receivedInvoiceDate,'%Y-%m-%d')) or (date_format(OLD.receivedInvoiceDate,'%Y-%m-%d') is null and date_format(NEW.receivedInvoiceDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.receivedInvoiceDate,'%Y-%m-%d') is not null and date_format(NEW.receivedInvoiceDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'accountline.receivedInvoiceDate~') into fieldNameList;
          IF(OLD.receivedInvoiceDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.receivedInvoiceDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.receivedInvoiceDate is not null) THEN
        select CONCAT(oldValueList,OLD.receivedInvoiceDate,'~') into oldValueList;
     END IF;
      IF(NEW.receivedInvoiceDate is not null) THEN
        select CONCAT(newValueList,NEW.receivedInvoiceDate,'~') into newValueList;
      END IF;
   END IF;

   IF ((OLD.recQuantity <> NEW.recQuantity) or (OLD.recQuantity is null and NEW.recQuantity is not null)
      or (OLD.recQuantity is not null and NEW.recQuantity is null)) THEN
        select CONCAT(fieldNameList,'accountline.recQuantity~') into fieldNameList;
        IF(OLD.recQuantity is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.recQuantity is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;
        IF(OLD.recQuantity is not null) then
            select CONCAT(oldValueList,OLD.recQuantity,'~') into oldValueList;
        END IF;
        IF(NEW.recQuantity is not null) then
            select CONCAT(newValueList,NEW.recQuantity,'~') into newValueList;
        END IF;
    END IF;
   IF ((OLD.recRate <> NEW.recRate) or (OLD.recRate is null and NEW.recRate is not null)
      or (OLD.recRate is not null and NEW.recRate is null)) THEN
        select CONCAT(fieldNameList,'accountline.recRate~') into fieldNameList;
        IF(OLD.recRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.recRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.recRate is not null) then
            select CONCAT(oldValueList,OLD.recRate,'~') into oldValueList;
        END IF;
        IF(NEW.recRate is not null) then
            select CONCAT(newValueList,NEW.recRate,'~') into newValueList;
        END IF;
    END IF;

   IF (OLD.description <> NEW.description ) THEN
       select CONCAT(fieldNameList,'accountline.description~') into fieldNameList;
       select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
       select CONCAT(newValueList,NEW.description,'~') into newValueList;
   END IF;

   IF (OLD.recGl <> NEW.recGl ) THEN
       select CONCAT(fieldNameList,'accountline.recGl~') into fieldNameList;
       select CONCAT(oldValueList,OLD.recGl,'~') into oldValueList;
       select CONCAT(newValueList,NEW.recGl,'~') into newValueList;
   END IF;

   IF (OLD.payGl <> NEW.payGl ) THEN
       select CONCAT(fieldNameList,'accountline.payGl~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payGl,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payGl,'~') into newValueList;
   END IF;

   IF ((date_format(OLD.invoiceDate,'%Y-%m-%d') <> date_format(NEW.invoiceDate,'%Y-%m-%d')) or (date_format(OLD.invoiceDate,'%Y-%m-%d') is null and date_format(NEW.invoiceDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.invoiceDate,'%Y-%m-%d') is not null and date_format(NEW.invoiceDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'accountline.invoiceDate~') into fieldNameList;
        IF(OLD.invoiceDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.invoiceDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.invoiceDate is not null) THEN
        select CONCAT(oldValueList,OLD.invoiceDate,'~') into oldValueList;
     END IF;
      IF(NEW.invoiceDate is not null) THEN
        select CONCAT(newValueList,NEW.invoiceDate,'~') into newValueList;
      END IF;
   END IF;

  IF ((date_format(OLD.payPostDate,'%Y-%m-%d') <> date_format(NEW.payPostDate,'%Y-%m-%d')) or (date_format(OLD.payPostDate,'%Y-%m-%d') is null and date_format(NEW.payPostDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.payPostDate,'%Y-%m-%d') is not null and date_format(NEW.payPostDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'accountline.payPostDate~') into fieldNameList;
           IF(OLD.payPostDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.payPostDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.payPostDate is not null) THEN
        select CONCAT(oldValueList,OLD.payPostDate,'~') into oldValueList;
     END IF;
      IF(NEW.payPostDate is not null) THEN
        select CONCAT(newValueList,NEW.payPostDate,'~') into newValueList;
      END IF;

   END IF;

  IF ((date_format(OLD.recPostDate,'%Y-%m-%d') <> date_format(NEW.recPostDate,'%Y-%m-%d')) or (date_format(OLD.recPostDate,'%Y-%m-%d') is null and date_format(NEW.recPostDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.recPostDate,'%Y-%m-%d') is not null and date_format(NEW.recPostDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'accountline.recPostDate~') into fieldNameList;
       IF(OLD.recPostDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.recPostDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.recPostDate is not null) THEN
        select CONCAT(oldValueList,OLD.recPostDate,'~') into oldValueList;
     END IF;
      IF(NEW.recPostDate is not null) THEN
        select CONCAT(newValueList,NEW.recPostDate,'~') into newValueList;
      END IF;
   END IF;
   IF ((date_format(OLD.payAccDate,'%Y-%m-%d') <> date_format(NEW.payAccDate,'%Y-%m-%d')) or (date_format(OLD.payAccDate,'%Y-%m-%d') is null and date_format(NEW.payAccDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.payAccDate,'%Y-%m-%d') is not null and date_format(NEW.payAccDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'accountline.payAccDate~') into fieldNameList;
       IF(OLD.payAccDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.payAccDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.payAccDate is not null) THEN
        select CONCAT(oldValueList,OLD.payAccDate,'~') into oldValueList;
     END IF;
      IF(NEW.payAccDate is not null) THEN
        select CONCAT(newValueList,NEW.payAccDate,'~') into newValueList;
      END IF;

   END IF;

   IF ((date_format(OLD.recAccDate,'%Y-%m-%d') <> date_format(NEW.recAccDate,'%Y-%m-%d')) or (date_format(OLD.recAccDate,'%Y-%m-%d') is null and date_format(NEW.recAccDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.recAccDate,'%Y-%m-%d') is not null and date_format(NEW.recAccDate,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.recAccDate~') into fieldNameList;
       IF(OLD.recAccDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.recAccDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.recAccDate is not null) THEN
        select CONCAT(oldValueList,OLD.recAccDate,'~') into oldValueList;
     END IF;
      IF(NEW.recAccDate is not null) THEN
        select CONCAT(newValueList,NEW.recAccDate,'~') into newValueList;
      END IF;

   END IF;

   IF (OLD.recXfer <> NEW.recXfer ) THEN
       select CONCAT(fieldNameList,'accountline.recXfer~') into fieldNameList;
       select CONCAT(oldValueList,OLD.recXfer,'~') into oldValueList;
       select CONCAT(newValueList,NEW.recXfer,'~') into newValueList;
   END IF;

   IF (OLD.payXfer <> NEW.payXfer ) THEN
       select CONCAT(fieldNameList,'accountline.payXfer~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payXfer,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payXfer,'~') into newValueList;
   END IF;

   IF ((date_format(OLD.receivedDate,'%Y-%m-%d') <> date_format(NEW.receivedDate,'%Y-%m-%d')) or (date_format(OLD.receivedDate,'%Y-%m-%d') is null and date_format(NEW.receivedDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.receivedDate,'%Y-%m-%d') is not null and date_format(NEW.receivedDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'accountline.receivedDate~') into fieldNameList;
      IF(OLD.receivedDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.receivedDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.receivedDate is not null) THEN
        select CONCAT(oldValueList,OLD.receivedDate,'~') into oldValueList;
     END IF;
      IF(NEW.receivedDate is not null) THEN
        select CONCAT(newValueList,NEW.receivedDate,'~') into newValueList;
      END IF;

   END IF;

   IF ((date_format(OLD.valueDate,'%Y-%m-%d') <> date_format(NEW.valueDate,'%Y-%m-%d')) or (date_format(OLD.valueDate,'%Y-%m-%d') is null and date_format(NEW.valueDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.valueDate,'%Y-%m-%d') is not null and date_format(NEW.valueDate,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'accountline.valueDate~') into fieldNameList;
       IF(OLD.valueDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.valueDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.valueDate is not null) THEN
        select CONCAT(oldValueList,OLD.valueDate,'~') into oldValueList;
     END IF;
      IF(NEW.valueDate is not null) THEN
        select CONCAT(newValueList,NEW.valueDate,'~') into newValueList;
      END IF;
   END IF;

   IF (OLD.country <> NEW.country ) THEN
       select CONCAT(fieldNameList,'accountline.country~') into fieldNameList;
       select CONCAT(oldValueList,OLD.country,'~') into oldValueList;
       select CONCAT(newValueList,NEW.country,'~') into newValueList;
   END IF;

   IF (OLD.note <> NEW.note ) THEN
       select CONCAT(fieldNameList,'accountline.note~') into fieldNameList;
       select CONCAT(oldValueList,OLD.note,'~') into oldValueList;
       select CONCAT(newValueList,NEW.note,'~') into newValueList;
   END IF;

          IF ((OLD.payingStatus <> NEW.payingStatus)or (OLD.payingStatus is null and NEW.payingStatus is not null)) THEN

       select CONCAT(fieldNameList,'accountline.payingStatus~') into fieldNameList;
        IF(OLD.payingStatus is null) THEN
            select CONCAT(oldValueList,"",'~') into oldValueList;
        END IF;
        IF(OLD.payingStatus is not null) THEN
       select CONCAT(oldValueList,OLD.payingStatus,'~') into oldValueList;
        END IF;
       select CONCAT(newValueList,NEW.payingStatus,'~') into newValueList;
   END IF;


       IF (OLD.invoiceNumber <> NEW.invoiceNumber) THEN
       select CONCAT(fieldNameList,'accountline.invoiceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.invoiceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.invoiceNumber,'~') into newValueList;
   END IF;
   IF ((OLD.localAmount <> NEW.localAmount) or (OLD.localAmount is null and NEW.localAmount is not null)
      or (OLD.localAmount is not null and NEW.localAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.localAmount~') into fieldNameList;
        IF(OLD.localAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.localAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.localAmount is not null) then
            select CONCAT(oldValueList,OLD.localAmount,'~') into oldValueList;
        END IF;
        IF(NEW.localAmount is not null) then
            select CONCAT(newValueList,NEW.localAmount,'~') into newValueList;
        END IF;
    END IF;

  IF ((OLD.exchangeRate <> NEW.exchangeRate) or (OLD.exchangeRate is null and NEW.exchangeRate is not null)
      or (OLD.exchangeRate is not null and NEW.exchangeRate is null)) THEN
        select CONCAT(fieldNameList,'accountline.exchangeRate~') into fieldNameList;
        IF(OLD.exchangeRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.exchangeRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.exchangeRate is not null) then
            select CONCAT(oldValueList,OLD.exchangeRate,'~') into oldValueList;
        END IF;
        IF(NEW.exchangeRate is not null) then
            select CONCAT(newValueList,NEW.exchangeRate,'~') into newValueList;
        END IF;
    END IF;

  IF ((OLD.convertedAmount <> NEW.convertedAmount) or (OLD.convertedAmount is null and NEW.convertedAmount is not null)
      or (OLD.convertedAmount is not null and NEW.convertedAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.convertedAmount~') into fieldNameList;
        IF(OLD.convertedAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.convertedAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.convertedAmount is not null) then
            select CONCAT(oldValueList,OLD.convertedAmount,'~') into oldValueList;
        END IF;
        IF(NEW.convertedAmount is not null) then
            select CONCAT(newValueList,NEW.convertedAmount,'~') into newValueList;
        END IF;
    END IF;

   IF (OLD.glType <> NEW.glType ) THEN
       select CONCAT(fieldNameList,'accountline.glType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.glType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.glType,'~') into newValueList;
   END IF;

   IF (OLD.commission <> NEW.commission ) THEN
       select CONCAT(fieldNameList,'accountline.commission~') into fieldNameList;
       select CONCAT(oldValueList,OLD.commission,'~') into oldValueList;
       select CONCAT(newValueList,NEW.commission,'~') into newValueList;
   END IF;

    IF
    (OLD.externalReference <> NEW.externalReference)
        THEN
       select CONCAT(fieldNameList,'accountline.externalReference~') into fieldNameList;
       select CONCAT(oldValueList,OLD.externalReference,'~') into oldValueList;
       select CONCAT(newValueList,NEW.externalReference,'~') into newValueList;
   END IF;

   IF ((date_format(OLD.statusDate,'%Y-%m-%d') <> date_format(NEW.statusDate,'%Y-%m-%d')) or (date_format(OLD.statusDate,'%Y-%m-%d') is null and date_format(NEW.statusDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.statusDate,'%Y-%m-%d') is not null and date_format(NEW.statusDate,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.statusDate~') into fieldNameList;
           IF(OLD.statusDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.statusDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.statusDate is not null) THEN
        select CONCAT(oldValueList,OLD.statusDate,'~') into oldValueList;
     END IF;
      IF(NEW.statusDate is not null) THEN
        select CONCAT(newValueList,NEW.statusDate,'~') into newValueList;
      END IF;
   END IF;
   IF ((date_format(OLD.storageDateRangeFrom,'%Y-%m-%d') <> date_format(NEW.storageDateRangeFrom,'%Y-%m-%d')) or (date_format(OLD.storageDateRangeFrom,'%Y-%m-%d') is null and date_format(NEW.storageDateRangeFrom,'%Y-%m-%d') is not null)
      or (date_format(OLD.storageDateRangeFrom,'%Y-%m-%d') is not null and date_format(NEW.storageDateRangeFrom,'%Y-%m-%d') is null)) THEN
       select CONCAT(fieldNameList,'accountline.storageDateRangeFrom~') into fieldNameList;
      IF(OLD.storageDateRangeFrom is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.storageDateRangeFrom is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.storageDateRangeFrom is not null) THEN
        select CONCAT(oldValueList,OLD.storageDateRangeFrom,'~') into oldValueList;
     END IF;
      IF(NEW.storageDateRangeFrom is not null) THEN
        select CONCAT(newValueList,NEW.storageDateRangeFrom,'~') into newValueList;
      END IF;

   END IF;

  IF ((date_format(OLD.storageDateRangeTo,'%Y-%m-%d') <> date_format(NEW.storageDateRangeTo,'%Y-%m-%d')) or (date_format(OLD.storageDateRangeTo,'%Y-%m-%d') is null and date_format(NEW.storageDateRangeTo,'%Y-%m-%d') is not null)
      or (date_format(OLD.storageDateRangeTo,'%Y-%m-%d') is not null and date_format(NEW.storageDateRangeTo,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.storageDateRangeTo~') into fieldNameList;
       IF(OLD.storageDateRangeTo is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.storageDateRangeTo is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.storageDateRangeTo is not null) THEN
        select CONCAT(oldValueList,OLD.storageDateRangeTo,'~') into oldValueList;
     END IF;
      IF(NEW.storageDateRangeTo is not null) THEN
        select CONCAT(newValueList,NEW.storageDateRangeTo,'~') into newValueList;
      END IF;
   END IF;

   IF ((OLD.storageDays <> NEW.storageDays) or (OLD.storageDays is null and NEW.storageDays is not null)
      or (OLD.storageDays is not null and NEW.storageDays is null)) THEN
        select CONCAT(fieldNameList,'accountline.storageDays~') into fieldNameList;
        IF(OLD.storageDays is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.storageDays is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.storageDays is not null) then
            select CONCAT(oldValueList,OLD.storageDays,'~') into oldValueList;
        END IF;
        IF(NEW.storageDays is not null) then
            select CONCAT(newValueList,NEW.storageDays,'~') into newValueList;
        END IF;
    END IF;

    IF ((date_format(OLD.sendActualToClient,'%Y-%m-%d') <> date_format(NEW.sendActualToClient,'%Y-%m-%d')) or (date_format(OLD.sendActualToClient,'%Y-%m-%d') is null and date_format(NEW.sendActualToClient,'%Y-%m-%d') is not null)
      or (date_format(OLD.sendActualToClient,'%Y-%m-%d') is not null and date_format(NEW.sendActualToClient,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.sendActualToClient~') into fieldNameList;
           IF(OLD.sendActualToClient is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.sendActualToClient is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.sendActualToClient is not null) THEN
        select CONCAT(oldValueList,OLD.sendActualToClient,'~') into oldValueList;
     END IF;
      IF(NEW.sendActualToClient is not null) THEN
        select CONCAT(newValueList,NEW.sendActualToClient,'~') into newValueList;
      END IF;
   END IF;

   
   IF ((OLD.receivedAmount <> NEW.receivedAmount) or (OLD.receivedAmount is null and NEW.receivedAmount is not null)
      or (OLD.receivedAmount is not null and NEW.receivedAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.receivedAmount~') into fieldNameList;
        IF(OLD.receivedAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.receivedAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.receivedAmount is not null) then
            select CONCAT(oldValueList,OLD.receivedAmount,'~') into oldValueList;
        END IF;
        IF(NEW.receivedAmount is not null) then
            select CONCAT(newValueList,NEW.receivedAmount,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.payPayableAmount <> NEW.payPayableAmount) or (OLD.payPayableAmount is null and NEW.payPayableAmount is not null)
      or (OLD.payPayableAmount is not null and NEW.payPayableAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.payPayableAmount~') into fieldNameList;
        IF(OLD.payPayableAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.payPayableAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.payPayableAmount is not null) then
            select CONCAT(oldValueList,OLD.payPayableAmount,'~') into oldValueList;
        END IF;
        IF(NEW.payPayableAmount is not null) then
            select CONCAT(newValueList,NEW.payPayableAmount,'~') into newValueList;
        END IF;
    END IF;

    IF ((OLD.distributionAmount <> NEW.distributionAmount) or (OLD.distributionAmount is null and NEW.distributionAmount is not null)
      or (OLD.distributionAmount is not null and NEW.distributionAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.distributionAmount~') into fieldNameList;
        IF(OLD.distributionAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.distributionAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.distributionAmount is not null) then
            select CONCAT(oldValueList,OLD.distributionAmount,'~') into oldValueList;
        END IF;
        IF(NEW.distributionAmount is not null) then
            select CONCAT(newValueList,NEW.distributionAmount,'~') into newValueList;
        END IF;
    END IF;
    
    IF (OLD.payPayableStatus <> NEW.payPayableStatus ) THEN
       select CONCAT(fieldNameList,'accountline.payPayableStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payPayableStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payPayableStatus,'~') into newValueList;
   END IF;
    IF ((date_format(OLD.payPayableDate,'%Y-%m-%d')  <> date_format(NEW.payPayableDate,'%Y-%m-%d')) or (date_format(OLD.payPayableDate,'%Y-%m-%d') is null and date_format(NEW.payPayableDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.payPayableDate,'%Y-%m-%d')  is not null and date_format(NEW.payPayableDate,'%Y-%m-%d')  is null)) THEN

       select CONCAT(fieldNameList,'accountline.payPayableDate~') into fieldNameList;
           IF(OLD.payPayableDate  is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.payPayableDate  is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.payPayableDate  is not null) THEN
        select CONCAT(oldValueList,OLD.payPayableDate ,'~') into oldValueList;
     END IF;
      IF(NEW.payPayableDate  is not null) THEN
        select CONCAT(newValueList,NEW.payPayableDate ,'~') into newValueList;
      END IF;
   END IF;

   IF (OLD.payPayableVia <> NEW.payPayableVia ) THEN
       select CONCAT(fieldNameList,'accountline.payPayableVia~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payPayableVia,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payPayableVia,'~') into newValueList;
   END IF;
   IF ((date_format(OLD.sendEstToClient,'%Y-%m-%d')   <> date_format(NEW.sendEstToClient,'%Y-%m-%d')) or (date_format(OLD.sendEstToClient,'%Y-%m-%d') is null and date_format(NEW.sendEstToClient,'%Y-%m-%d') is not null)
      or (date_format(OLD.sendEstToClient,'%Y-%m-%d') is not null and date_format(NEW.sendEstToClient,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.sendEstToClient~') into fieldNameList;
           IF(OLD.sendEstToClient is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.sendEstToClient is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.sendEstToClient is not null) THEN
        select CONCAT(oldValueList,OLD.sendEstToClient ,'~') into oldValueList;
     END IF;
      IF(NEW.sendEstToClient is not null) THEN
        select CONCAT(newValueList,NEW.sendEstToClient ,'~') into newValueList;
      END IF;
   END IF;

   IF ((date_format(OLD.doNotSendtoClient,'%Y-%m-%d')   <> date_format(NEW.doNotSendtoClient,'%Y-%m-%d')) or (date_format(OLD.doNotSendtoClient,'%Y-%m-%d') is null and date_format(NEW.doNotSendtoClient,'%Y-%m-%d') is not null)
      or (date_format(OLD.doNotSendtoClient,'%Y-%m-%d') is not null and date_format(NEW.doNotSendtoClient,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.doNotSendtoClient~') into fieldNameList;
           IF(OLD.doNotSendtoClient is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.doNotSendtoClient is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.doNotSendtoClient is not null) THEN
        select CONCAT(oldValueList,OLD.doNotSendtoClient ,'~') into oldValueList;
     END IF;
      IF(NEW.doNotSendtoClient is not null) THEN
        select CONCAT(newValueList,NEW.doNotSendtoClient ,'~') into newValueList;
      END IF;
   END IF;

   IF (OLD.branchCode <> NEW.branchCode ) THEN
       select CONCAT(fieldNameList,'accountline.branchCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.branchCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.branchCode,'~') into newValueList;
   END IF;
   IF (OLD.authorization <> NEW.authorization ) THEN
       select CONCAT(fieldNameList,'accountline.authorization~') into fieldNameList;
       select CONCAT(oldValueList,OLD.authorization,'~') into oldValueList;
       select CONCAT(newValueList,NEW.authorization,'~') into newValueList;
   END IF;

   IF ((date_format(OLD.accrueRevenue,'%Y-%m-%d')   <> date_format(NEW.accrueRevenue,'%Y-%m-%d')) or (date_format(OLD.accrueRevenue,'%Y-%m-%d') is null and date_format(NEW.accrueRevenue,'%Y-%m-%d') is not null)
      or (date_format(OLD.accrueRevenue,'%Y-%m-%d') is not null and date_format(NEW.accrueRevenue,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.accrueRevenue~') into fieldNameList;
           IF(OLD.accrueRevenue is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.accrueRevenue is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.accrueRevenue is not null) THEN
        select CONCAT(oldValueList,OLD.accrueRevenue ,'~') into oldValueList;
     END IF;
      IF(NEW.accrueRevenue is not null) THEN
        select CONCAT(newValueList,NEW.accrueRevenue ,'~') into newValueList;
      END IF;
   END IF;

    IF ((date_format(OLD.accruePayable,'%Y-%m-%d')   <> date_format(NEW.accruePayable,'%Y-%m-%d')) or (date_format(OLD.accruePayable,'%Y-%m-%d') is null and date_format(NEW.accruePayable,'%Y-%m-%d') is not null)
      or (date_format(OLD.accruePayable,'%Y-%m-%d') is not null and date_format(NEW.accruePayable,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.accruePayable~') into fieldNameList;
           IF(OLD.accruePayable is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.accruePayable is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.accruePayable is not null) THEN
        select CONCAT(oldValueList,OLD.accruePayable ,'~') into oldValueList;
     END IF;
      IF(NEW.accruePayable is not null) THEN
        select CONCAT(newValueList,NEW.accruePayable ,'~') into newValueList;
      END IF;
   END IF;
   IF (OLD.branchName <> NEW.branchName ) THEN
       select CONCAT(fieldNameList,'accountline.branchName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.branchName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.branchName,'~') into newValueList;
   END IF;
    IF (OLD.actgCode <> NEW.actgCode ) THEN
       select CONCAT(fieldNameList,'accountline.actgCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.actgCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.actgCode,'~') into newValueList;
   END IF;
   
   IF ((date_format(OLD.estimateDate,'%Y-%m-%d')   <> date_format(NEW.estimateDate,'%Y-%m-%d')) or (date_format(OLD.estimateDate,'%Y-%m-%d') is null and date_format(NEW.estimateDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.estimateDate,'%Y-%m-%d') is not null and date_format(NEW.estimateDate,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.estimateDate~') into fieldNameList;
           IF(OLD.estimateDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.estimateDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.estimateDate is not null) THEN
        select CONCAT(oldValueList,OLD.estimateDate ,'~') into oldValueList;
     END IF;
      IF(NEW.estimateDate is not null) THEN
        select CONCAT(newValueList,NEW.estimateDate ,'~') into newValueList;
      END IF;
   END IF;
    IF (OLD.quoteDescription <> NEW.quoteDescription ) THEN
       select CONCAT(fieldNameList,'accountline.quoteDescription~') into fieldNameList;
       select CONCAT(oldValueList,OLD.quoteDescription,'~') into oldValueList;
       select CONCAT(newValueList,NEW.quoteDescription,'~') into newValueList;
   END IF;

   IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
       select CONCAT(fieldNameList,'accountline.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
   END IF;

   IF (OLD.recXferUser <> NEW.recXferUser ) THEN
       select CONCAT(fieldNameList,'accountline.recXferUser~') into fieldNameList;
       select CONCAT(oldValueList,OLD.recXferUser,'~') into oldValueList;
       select CONCAT(newValueList,NEW.recXferUser,'~') into newValueList;
   END IF;
   
   IF (OLD.payXferUser <> NEW.payXferUser ) THEN
       select CONCAT(fieldNameList,'accountline.payXferUser~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payXferUser,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payXferUser,'~') into newValueList;
   END IF;
     IF ((date_format(OLD.accrueRevenueReverse,'%Y-%m-%d')   <> date_format(NEW.accrueRevenueReverse,'%Y-%m-%d')) or (date_format(OLD.accrueRevenueReverse,'%Y-%m-%d') is null and date_format(NEW.accrueRevenueReverse,'%Y-%m-%d') is not null)
      or (date_format(OLD.accrueRevenueReverse,'%Y-%m-%d') is not null and date_format(NEW.accrueRevenueReverse,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.accrueRevenueReverse~') into fieldNameList;
           IF(OLD.accrueRevenueReverse is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.accrueRevenueReverse is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.accrueRevenueReverse is not null) THEN
        select CONCAT(oldValueList,OLD.accrueRevenueReverse ,'~') into oldValueList;
     END IF;
      IF(NEW.accrueRevenueReverse is not null) THEN
        select CONCAT(newValueList,NEW.accrueRevenueReverse ,'~') into newValueList;
      END IF;
   END IF;
     IF ((date_format(OLD.accruePayableReverse,'%Y-%m-%d')   <> date_format(NEW.accruePayableReverse,'%Y-%m-%d')) or (date_format(OLD.accruePayableReverse,'%Y-%m-%d') is null and date_format(NEW.accruePayableReverse,'%Y-%m-%d') is not null)
      or (date_format(OLD.accruePayableReverse,'%Y-%m-%d') is not null and date_format(NEW.accruePayableReverse,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.accruePayableReverse~') into fieldNameList;
           IF(OLD.accruePayableReverse is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.accruePayableReverse is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.accruePayableReverse is not null) THEN
        select CONCAT(oldValueList,OLD.accruePayableReverse ,'~') into oldValueList;
     END IF;
      IF(NEW.accruePayableReverse is not null) THEN
        select CONCAT(newValueList,NEW.accruePayableReverse ,'~') into newValueList;
      END IF;
   END IF;
   
   IF (OLD.recRateCurrency <> NEW.recRateCurrency ) THEN
       select CONCAT(fieldNameList,'accountline.recRateCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.recRateCurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.recRateCurrency,'~') into newValueList;
   END IF;

   IF ((OLD.recCurrencyRate <> NEW.recCurrencyRate) or (OLD.recCurrencyRate is null and NEW.recCurrencyRate is not null)
      or (OLD.recCurrencyRate is not null and NEW.recCurrencyRate is null)) THEN
        select CONCAT(fieldNameList,'accountline.recCurrencyRate~') into fieldNameList;
        IF(OLD.recCurrencyRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.recCurrencyRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.recCurrencyRate is not null) then
            select CONCAT(oldValueList,OLD.recCurrencyRate,'~') into oldValueList;
        END IF;
        IF(NEW.recCurrencyRate is not null) then
            select CONCAT(newValueList,NEW.recCurrencyRate,'~') into newValueList;
        END IF;
    END IF;
    IF ((OLD.recRateExchange <> NEW.recRateExchange) or (OLD.recRateExchange is null and NEW.recRateExchange is not null)
      or (OLD.recRateExchange is not null and NEW.recRateExchange is null)) THEN
        select CONCAT(fieldNameList,'accountline.recRateExchange~') into fieldNameList;
        IF(OLD.recRateExchange is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.recRateExchange is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.recRateExchange is not null) then
            select CONCAT(oldValueList,OLD.recRateExchange,'~') into oldValueList;
        END IF;
        IF(NEW.recRateExchange is not null) then
            select CONCAT(newValueList,NEW.recRateExchange,'~') into newValueList;
        END IF;
    END IF;

    IF ((date_format(OLD.racValueDate,'%Y-%m-%d')   <> date_format(NEW.racValueDate,'%Y-%m-%d')) or (date_format(OLD.racValueDate,'%Y-%m-%d') is null and date_format(NEW.racValueDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.racValueDate,'%Y-%m-%d') is not null and date_format(NEW.racValueDate,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.racValueDate~') into fieldNameList;
           IF(OLD.racValueDate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.racValueDate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.racValueDate is not null) THEN
        select CONCAT(oldValueList,OLD.racValueDate ,'~') into oldValueList;
     END IF;
      IF(NEW.racValueDate is not null) THEN
        select CONCAT(newValueList,NEW.racValueDate ,'~') into newValueList;
      END IF;
   END IF;
    IF (OLD.ignoreForBilling <> NEW.ignoreForBilling ) THEN
       select CONCAT(fieldNameList,'accountline.ignoreForBilling~') into fieldNameList;
       IF(OLD.ignoreForBilling = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.ignoreForBilling = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.ignoreForBilling = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.ignoreForBilling = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

    IF ((OLD.actualRevenueForeign <> NEW.actualRevenueForeign) or (OLD.actualRevenueForeign is null and NEW.actualRevenueForeign is not null)
      or (OLD.actualRevenueForeign is not null and NEW.actualRevenueForeign is null)) THEN
        select CONCAT(fieldNameList,'accountline.actualRevenueForeign~') into fieldNameList;
        IF(OLD.actualRevenueForeign is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualRevenueForeign is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualRevenueForeign is not null) then
            select CONCAT(oldValueList,OLD.actualRevenueForeign,'~') into oldValueList;
        END IF;
        IF(NEW.actualRevenueForeign is not null) then
            select CONCAT(newValueList,NEW.actualRevenueForeign,'~') into newValueList;
        END IF;
    END IF;

    IF (OLD.accrueRevenueManual <> NEW.accrueRevenueManual ) THEN
       select CONCAT(fieldNameList,'accountline.accrueRevenueManual~') into fieldNameList;
       IF(OLD.accrueRevenueManual = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.accrueRevenueManual = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.accrueRevenueManual = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.accrueRevenueManual = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   IF (OLD.accruePayableManual <> NEW.accruePayableManual ) THEN
       select CONCAT(fieldNameList,'accountline.accruePayableManual~') into fieldNameList;
       IF(OLD.accruePayableManual = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.accruePayableManual = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.accruePayableManual = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.accruePayableManual = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

   IF ((OLD.receivedAmount <> NEW.receivedAmount) or (OLD.receivedAmount is null and NEW.receivedAmount is not null)
      or (OLD.receivedAmount is not null and NEW.receivedAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.receivedAmount~') into fieldNameList;
        IF(OLD.receivedAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.receivedAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.receivedAmount is not null) then
            select CONCAT(oldValueList,OLD.receivedAmount,'~') into oldValueList;
        END IF;
        IF(NEW.receivedAmount is not null) then
            select CONCAT(newValueList,NEW.receivedAmount,'~') into newValueList;
        END IF;
    END IF;

     IF (trim(both ' ' from OLD.invoiceCreatedBy) <> trim(both ' ' from NEW.invoiceCreatedBy) ) THEN
       select CONCAT(fieldNameList,'accountline.trim(both ' ' from invoiceCreatedBy)~') into fieldNameList;
       select CONCAT(oldValueList,trim(both ' ' from OLD.invoiceCreatedBy),'~') into oldValueList;
       select CONCAT(newValueList,trim(both ' ' from NEW.invoiceCreatedBy),'~') into newValueList;
   END IF;

    IF ((OLD.payPayableAmount <> NEW.payPayableAmount) or (OLD.payPayableAmount is null and NEW.payPayableAmount is not null)
      or (OLD.payPayableAmount is not null and NEW.receivedAmount is null)) THEN
        select CONCAT(fieldNameList,'accountline.payPayableAmount~') into fieldNameList;
        IF(OLD.payPayableAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.payPayableAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.payPayableAmount is not null) then
            select CONCAT(oldValueList,OLD.payPayableAmount,'~') into oldValueList;
        END IF;
        IF(NEW.payPayableAmount is not null) then
            select CONCAT(newValueList,NEW.payPayableAmount,'~') into newValueList;
        END IF;
   END IF;

   IF (OLD.division <> NEW.division ) THEN
       select CONCAT(fieldNameList,'accountline.division~') into fieldNameList;
       select CONCAT(oldValueList,OLD.division,'~') into oldValueList;
       select CONCAT(newValueList,NEW.division,'~') into newValueList;
   END IF;


   IF (OLD.estimateStatus <> NEW.estimateStatus ) THEN
       select CONCAT(fieldNameList,'accountline.estimateStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimateStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimateStatus,'~') into newValueList;
   END IF;
   
 IF((NEW.corpid ='VOER') OR (NEW.corpid ='VOCZ') OR (NEW.corpid ='VORU') OR (NEW.corpid ='VOAO') OR (NEW.corpid ='BONG')) then
 select partnerCode into pppartnercode from partnerprivate where excludeFinancialExtract is true and partnercode=NEW.vendorcode and corpid='VOER';

 IF(NEW.vendorcode in (pppartnercode)) then
       SET NEW.dynamicNavisionflag=false;
       SET NEW.deleteDynamicNavisionflag=false;
       SET NEW.oldCompanyDivision=null;
  ELSEIF((NEW.dynamicNavXfer is not null and NEW.dynamicNavXfer <>'') OR (OLD.dynamicNavXfer is not null and OLD.dynamicNavXfer <>'')) then   
       SET NEW.dynamicNavisionflag=false;
       SET NEW.deleteDynamicNavisionflag=false;
       SET NEW.oldCompanyDivision=null;

 ELSEIF
(
(
(
(OLD.chargeCode <>'' and OLD.chargeCode is not null) 
     or (NEW.chargeCode is not null and NEW.chargeCode <>'')
     or (OLD.chargeCode <> NEW.chargeCode)
) 
  and OLD.status is true
) 
 and
       (
(
(OLD.paygl <>'' and OLD.paygl is not null) 
     or (NEW.paygl is not null and NEW.paygl <>'')
     or (OLD.paygl <> NEW.paygl)
     or OLD.status is true
)
) 
 and
(
(
(OLD.companydivision <>'' and OLD.companydivision is not null) 
     or (NEW.companydivision is not null and NEW.companydivision <>'')
     or (OLD.companydivision <> NEW.companydivision) 
     and OLD.status is true
)
) 
 and
(
(
(OLD.revisionExpense <>'0.00' and OLD.revisionExpense is not null) 
     or (NEW.revisionExpense is not null and NEW.revisionExpense <>'0.00')
     or (OLD.revisionExpense <> NEW.revisionExpense) 
  and OLD.status is true
)
) 
 and
( 
(
(OLD.actualExpense ='0.00' OR OLD.actualExpense is  null) 
     OR (NEW.actualExpense is null OR NEW.actualExpense ='0.00')
     
  and OLD.status is true
)
)
and
(
(
(OLD.vendorcode <>'' and OLD.vendorcode is not null) 
     or (NEW.vendorcode is not null and NEW.vendorcode <>'')
     or (OLD.vendorcode <> NEW.vendorcode) 
     and OLD.status is true
)
) 
and
( 
(
(OLD.actgcode <>'' and OLD.actgcode is not null) 
     or (NEW.actgcode is not null and NEW.actgcode <>'')
     or (OLD.actgcode <> NEW.actgcode) 
     and OLD.status is true
)
)
and NEW.sendDynamicNavisionDate is NULL
)
THEN

   IF(NEW.status is false and OLD.status is true) THEN
       SET NEW.dynamicNavisionflag=false;
       SET NEW.deleteDynamicNavisionflag=true;
       SET NEW.oldCompanyDivision=null;
   ELSEIF(NEW.status is false and OLD.status is false) THEN
       SET NEW.dynamicNavisionflag=false;
       SET NEW.deleteDynamicNavisionflag=false;
       SET NEW.oldCompanyDivision=null;
   ELSEIF ((OLD.companydivision <>'' and OLD.companydivision is not null) and (NEW.companydivision is not null and NEW.companydivision <>'')
    and (OLD.companydivision <> NEW.companydivision)) THEN
       SET NEW.dynamicNavisionflag=true;
       SET NEW.deleteDynamicNavisionflag=true;
       SET NEW.oldCompanyDivision=OLD.companydivision;
   ELSEIF(((OLD.chargeCode <>'' and OLD.chargeCode is not null) and (NEW.chargeCode is not null and NEW.chargeCode <>'') and (OLD.chargeCode <> NEW.chargeCode))
OR
((OLD.paygl <>'' and OLD.paygl is not null) and (NEW.paygl is not null and NEW.paygl <>'') and (OLD.paygl <> NEW.paygl))
OR
((OLD.revisionExpense <>'0.00' and OLD.revisionExpense is not null) and (NEW.revisionExpense is not null and NEW.revisionExpense <>'0.00') and (OLD.revisionExpense <> NEW.revisionExpense))
OR
((OLD.vendorcode <>'' and OLD.vendorcode is not null) and (NEW.vendorcode is not null and NEW.vendorcode <>'') and (OLD.vendorcode <> NEW.vendorcode))
OR
((OLD.actgcode <>'' and OLD.actgcode is not null) and (NEW.actgcode is not null and NEW.actgcode <>'') and (OLD.actgcode <> NEW.actgcode))) THEN
       SET NEW.dynamicNavisionflag=true;
       SET NEW.deleteDynamicNavisionflag=false;
    END IF;
  
 ELSEIF (NEW.sendDynamicNavisionDate is NOT NULL OR OLD.sendDynamicNavisionDate is NOT NULL) then

IF((NEW.status is false and OLD.status is true) OR (NEW.actualExpense<>'0.00' AND NEW.actualExpense is not null 
AND (OLD.actualExpense='0.00' OR OLD.actualExpense is null) AND NEW.payingStatus = 'A')) THEN
       SET NEW.dynamicNavisionflag=false;
       SET NEW.deleteDynamicNavisionflag=true;
       SET NEW.oldCompanyDivision=null;
   ELSEIF(NEW.status is false and OLD.status is false) THEN
       SET NEW.dynamicNavisionflag=false;
       SET NEW.deleteDynamicNavisionflag=false;
       SET NEW.oldCompanyDivision=null;       
   ELSEIF(NEW.actualExpense<>'0.00' AND NEW.actualExpense is not null and OLD.actualExpense<>'0.00' AND OLD.actualExpense is not null AND NEW.payingStatus = 'A' AND OLD.payingStatus = 'A') THEN
       SET NEW.dynamicNavisionflag=false;
       SET NEW.deleteDynamicNavisionflag=false;
       SET NEW.oldCompanyDivision=null;
   ELSEIF(NEW.actualExpense<>'0.00' AND NEW.actualExpense is not null and OLD.actualExpense<>'0.00' AND OLD.actualExpense is not null AND NEW.payingStatus = 'A' AND OLD.payingStatus <> 'A') THEN
       SET NEW.dynamicNavisionflag=false;
       SET NEW.deleteDynamicNavisionflag=true;
       SET NEW.oldCompanyDivision=null;              
   ELSEIF ((OLD.companydivision <>'' and OLD.companydivision is not null) and (NEW.companydivision is not null and NEW.companydivision <>'')
    and (OLD.companydivision <> NEW.companydivision)) THEN
       SET NEW.dynamicNavisionflag=true;
       SET NEW.deleteDynamicNavisionflag=true;
       SET NEW.oldCompanyDivision=OLD.companydivision;
   ELSEIF((OLD.chargeCode <> NEW.chargeCode) 
OR 
(OLD.paygl <> NEW.paygl)
OR
(OLD.revisionExpense <> NEW.revisionExpense)
OR
(OLD.vendorcode <> NEW.vendorcode)
or
(OLD.actgcode <> NEW.actgcode)) THEN
       SET NEW.dynamicNavisionflag=true;
       SET NEW.deleteDynamicNavisionflag=false;
    END IF;


    ELSEIF (((NEW.chargeCode is null OR NEW.chargeCode ='')
OR 
(NEW.paygl is null OR NEW.paygl ='')
OR
(NEW.revisionExpense is null OR NEW.revisionExpense ='0.00')
OR
(NEW.vendorcode is null OR NEW.vendorcode ='')
OR
(NEW.actgcode is null OR NEW.actgcode ='')) AND (NEW.sendDynamicNavisionDate is NULL OR OLD.sendDynamicNavisionDate is NULL)) THEN
       SET NEW.dynamicNavisionflag=false;
       SET NEW.deleteDynamicNavisionflag=false;
   END IF;
   END IF;
    IF (OLD.recvatpercent <> NEW.recvatpercent ) THEN
       select CONCAT(fieldNameList,'accountline.recvatpercent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.recvatpercent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.recvatpercent,'~') into newValueList;
   END IF;

   IF (OLD.recvatdescr <> NEW.recvatdescr ) THEN
       select CONCAT(fieldNameList,'accountline.recvatdescr~') into fieldNameList;
       select CONCAT(oldValueList,OLD.recvatdescr,'~') into oldValueList;
       select CONCAT(newValueList,NEW.recvatdescr,'~') into newValueList;
   END IF;

   IF ((OLD.recvatamt <> NEW.recvatamt) or (OLD.recvatamt is null and NEW.recvatamt is not null)
      or (OLD.recvatamt is not null and NEW.recvatamt is null)) THEN
        select CONCAT(fieldNameList,'accountline.recvatamt~') into fieldNameList;
        IF(OLD.recvatamt is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.recvatamt is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.recvatamt is not null) then
            select CONCAT(oldValueList,OLD.recvatamt,'~') into oldValueList;
        END IF;
        IF(NEW.recvatamt is not null) then
            select CONCAT(newValueList,NEW.recvatamt,'~') into newValueList;
        END IF;
    END IF;

    IF (OLD.payvatpercent <> NEW.payvatpercent ) THEN
       select CONCAT(fieldNameList,'accountline.payvatpercent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payvatpercent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payvatpercent,'~') into newValueList;
   END IF;
   
    IF (OLD.payvatdescr <> NEW.payvatdescr ) THEN
       select CONCAT(fieldNameList,'accountline.payvatdescr~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payvatdescr,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payvatdescr,'~') into newValueList;
   END IF;

   IF ((OLD.payvatamt <> NEW.payvatamt) or (OLD.payvatamt is null and NEW.payvatamt is not null)
      or (OLD.payvatamt is not null and NEW.payvatamt is null)) THEN
        select CONCAT(fieldNameList,'accountline.payvatamt~') into fieldNameList;
        IF(OLD.payvatamt is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.payvatamt is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.payvatamt is not null) then
            select CONCAT(oldValueList,OLD.payvatamt,'~') into oldValueList;
        END IF;
        IF(NEW.payvatamt is not null) then
            select CONCAT(newValueList,NEW.payvatamt,'~') into newValueList;
        END IF;
    END IF;

    IF (OLD.revisioncurrency <> NEW.revisioncurrency ) THEN
       select CONCAT(fieldNameList,'accountline.revisioncurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisioncurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisioncurrency,'~') into newValueList;
   END IF;
   
   IF ((OLD.revisionlocalamount <> NEW.revisionlocalamount) or (OLD.revisionlocalamount is null and NEW.revisionlocalamount is not null)
      or (OLD.revisionlocalamount is not null and NEW.revisionlocalamount is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionlocalamount~') into fieldNameList;
        IF(OLD.revisionlocalamount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionlocalamount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionlocalamount is not null) then
            select CONCAT(oldValueList,OLD.revisionlocalamount,'~') into oldValueList;
        END IF;
        IF(NEW.revisionlocalamount is not null) then
            select CONCAT(newValueList,NEW.revisionlocalamount,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.revisionexchangerate <> NEW.revisionexchangerate) or (OLD.revisionexchangerate is null and NEW.revisionexchangerate is not null)
      or (OLD.revisionexchangerate is not null and NEW.revisionexchangerate is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionexchangerate~') into fieldNameList;
        IF(OLD.revisionexchangerate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionexchangerate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionexchangerate is not null) then
            select CONCAT(oldValueList,OLD.revisionexchangerate,'~') into oldValueList;
        END IF;
        IF(NEW.revisionexchangerate is not null) then
            select CONCAT(newValueList,NEW.revisionexchangerate,'~') into newValueList;
        END IF;
    END IF;

     IF (OLD.vatexclude <> NEW.vatexclude ) THEN
       select CONCAT(fieldNameList,'accountline.vatexclude~') into fieldNameList;
       IF(OLD.vatexclude = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.vatexclude = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.vatexclude = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.vatexclude = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;

    
    IF ((OLD.estvatamt <> NEW.estvatamt) or (OLD.estvatamt is null and NEW.estvatamt is not null)
      or (OLD.estvatamt is not null and NEW.estvatamt is null)) THEN
        select CONCAT(fieldNameList,'accountline.estvatamt~') into fieldNameList;
        IF(OLD.estvatamt is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estvatamt is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estvatamt is not null) then
            select CONCAT(oldValueList,OLD.estvatamt,'~') into oldValueList;
        END IF;
        IF(NEW.estvatamt is not null) then
            select CONCAT(newValueList,NEW.estvatamt,'~') into newValueList;
        END IF;
    END IF;
    
    
    IF (OLD.estvatpercent <> NEW.estvatpercent ) THEN
       select CONCAT(fieldNameList,'accountline.estvatpercent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estvatpercent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estvatpercent,'~') into newValueList;
   END IF;
   
   IF (OLD.estvatdescr <> NEW.estvatdescr ) THEN
       select CONCAT(fieldNameList,'accountline.estvatdescr~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estvatdescr,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estvatdescr,'~') into newValueList;
   END IF;

    IF (OLD.deviation <> NEW.deviation ) THEN
       select CONCAT(fieldNameList,'accountline.deviation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.deviation,'~') into oldValueList;
       select CONCAT(newValueList,NEW.deviation,'~') into newValueList;
   END IF;
   
    IF (OLD.revisionvatpercent <> NEW.revisionvatpercent ) THEN
       select CONCAT(fieldNameList,'accountline.revisionvatpercent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionvatpercent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionvatpercent,'~') into newValueList;
   END IF;

   IF (OLD.revisionvatdescr <> NEW.revisionvatdescr ) THEN
       select CONCAT(fieldNameList,'accountline.revisionvatdescr~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionvatdescr,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionvatdescr,'~') into newValueList;
   END IF;

    IF ((OLD.revisionvatamt <> NEW.revisionvatamt) or (OLD.revisionvatamt is null and NEW.revisionvatamt is not null)
      or (OLD.revisionvatamt is not null and NEW.revisionvatamt is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionvatamt~') into fieldNameList;
        IF(OLD.revisionvatamt is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionvatamt is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionvatamt is not null) then
            select CONCAT(oldValueList,OLD.revisionvatamt,'~') into oldValueList;
        END IF;
        IF(NEW.revisionvatamt is not null) then
            select CONCAT(newValueList,NEW.revisionvatamt,'~') into newValueList;
        END IF;
    END IF;

      IF ((OLD.paydeviation <> NEW.paydeviation) or (OLD.paydeviation is null and NEW.paydeviation is not null)
      or (OLD.paydeviation is not null and NEW.paydeviation is null)) THEN
        select CONCAT(fieldNameList,'accountline.paydeviation~') into fieldNameList;
        IF(OLD.paydeviation is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.paydeviation is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.paydeviation is not null) then
            select CONCAT(oldValueList,OLD.paydeviation,'~') into oldValueList;
        END IF;
        IF(NEW.paydeviation is not null) then
            select CONCAT(newValueList,NEW.paydeviation,'~') into newValueList;
        END IF;
    END IF;

     
   IF (OLD.contractcurrency <> NEW.contractcurrency ) THEN
       select CONCAT(fieldNameList,'accountline.contractcurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contractcurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.contractcurrency,'~') into newValueList;
   END IF;

   IF ((OLD.contractrate <> NEW.contractrate) or (OLD.contractrate is null and NEW.contractrate is not null)
      or (OLD.contractrate is not null and NEW.contractrate is null)) THEN
        select CONCAT(fieldNameList,'accountline.contractrate~') into fieldNameList;
        IF(OLD.contractrate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.contractrate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.contractrate is not null) then
            select CONCAT(oldValueList,OLD.contractrate,'~') into oldValueList;
        END IF;
        IF(NEW.contractrate is not null) then
            select CONCAT(newValueList,NEW.contractrate,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.contractexchangerate <> NEW.contractexchangerate) or (OLD.contractexchangerate is null and NEW.contractexchangerate is not null)
      or (OLD.contractexchangerate is not null and NEW.contractexchangerate is null)) THEN
        select CONCAT(fieldNameList,'accountline.contractexchangerate~') into fieldNameList;
        IF(OLD.contractexchangerate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.contractexchangerate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.contractexchangerate is not null) then
            select CONCAT(oldValueList,OLD.contractexchangerate,'~') into oldValueList;
        END IF;
        IF(NEW.contractexchangerate is not null) then
            select CONCAT(newValueList,NEW.contractexchangerate,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.contractrateammount <> NEW.contractrateammount) or (OLD.contractrateammount is null and NEW.contractrateammount is not null)
      or (OLD.contractrateammount is not null and NEW.contractrateammount is null)) THEN
        select CONCAT(fieldNameList,'accountline.contractrateammount~') into fieldNameList;
        IF(OLD.contractrateammount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.contractrateammount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.contractrateammount is not null) then
            select CONCAT(oldValueList,OLD.contractrateammount,'~') into oldValueList;
        END IF;
        IF(NEW.contractrateammount is not null) then
            select CONCAT(newValueList,NEW.contractrateammount,'~') into newValueList;
        END IF;
    END IF;
   
    IF ((date_format(OLD.contractvaluedate,'%Y-%m-%d')   <> date_format(NEW.contractvaluedate,'%Y-%m-%d')) or (date_format(OLD.contractvaluedate,'%Y-%m-%d') is null and date_format(NEW.contractvaluedate,'%Y-%m-%d') is not null)
      or (date_format(OLD.contractvaluedate,'%Y-%m-%d') is not null and date_format(NEW.contractvaluedate,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.contractvaluedate~') into fieldNameList;
           IF(OLD.contractvaluedate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.contractvaluedate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.contractvaluedate is not null) THEN
        select CONCAT(oldValueList,OLD.contractvaluedate ,'~') into oldValueList;
     END IF;
      IF(NEW.contractvaluedate is not null) THEN
        select CONCAT(newValueList,NEW.contractvaluedate ,'~') into newValueList;
      END IF;
   END IF;

        
   IF (OLD.networksynchedid <> NEW.networksynchedid ) THEN
       select CONCAT(fieldNameList,'accountline.networksynchedid~') into fieldNameList;
       select CONCAT(oldValueList,OLD.networksynchedid,'~') into oldValueList;
       select CONCAT(newValueList,NEW.networksynchedid,'~') into newValueList;
   END IF;

      IF (OLD.payablecontractcurrency <> NEW.payablecontractcurrency ) THEN
       select CONCAT(fieldNameList,'accountline.payablecontractcurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payablecontractcurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payablecontractcurrency,'~') into newValueList;
   END IF;
   
   IF ((OLD.payablecontractexchangerate <> NEW.payablecontractexchangerate) or (OLD.payablecontractexchangerate is null and NEW.payablecontractexchangerate is not null)
      or (OLD.payablecontractexchangerate is not null and NEW.payablecontractexchangerate is null)) THEN
        select CONCAT(fieldNameList,'accountline.payablecontractexchangerate~') into fieldNameList;
        IF(OLD.payablecontractexchangerate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.payablecontractexchangerate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.payablecontractexchangerate is not null) then
            select CONCAT(oldValueList,OLD.payablecontractexchangerate,'~') into oldValueList;
        END IF;
        IF(NEW.payablecontractexchangerate is not null) then
            select CONCAT(newValueList,NEW.payablecontractexchangerate,'~') into newValueList;
        END IF;
    END IF;
  
    IF ((OLD.payablecontractrateammount <> NEW.payablecontractrateammount) or (OLD.payablecontractrateammount is null and NEW.payablecontractrateammount is not null)
      or (OLD.payablecontractrateammount is not null and NEW.payablecontractrateammount is null)) THEN
        select CONCAT(fieldNameList,'accountline.payablecontractrateammount~') into fieldNameList;
        IF(OLD.payablecontractrateammount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.payablecontractrateammount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.payablecontractrateammount is not null) then
            select CONCAT(oldValueList,OLD.payablecontractrateammount,'~') into oldValueList;
        END IF;
        IF(NEW.payablecontractrateammount is not null) then
            select CONCAT(newValueList,NEW.payablecontractrateammount,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((date_format(OLD.payablecontractvaluedate,'%Y-%m-%d')   <> date_format(NEW.payablecontractvaluedate,'%Y-%m-%d')) or (date_format(OLD.payablecontractvaluedate,'%Y-%m-%d') is null and date_format(NEW.payablecontractvaluedate,'%Y-%m-%d') is not null)
      or (date_format(OLD.payablecontractvaluedate,'%Y-%m-%d') is not null and date_format(NEW.payablecontractvaluedate,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.payablecontractvaluedate~') into fieldNameList;
           IF(OLD.payablecontractvaluedate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.payablecontractvaluedate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.payablecontractvaluedate is not null) THEN
        select CONCAT(oldValueList,OLD.payablecontractvaluedate ,'~') into oldValueList;
     END IF;
      IF(NEW.payablecontractvaluedate is not null) THEN
        select CONCAT(newValueList,NEW.payablecontractvaluedate ,'~') into newValueList;
      END IF;
   END IF;
   
   IF (OLD.revisionsellcurrency <> NEW.revisionsellcurrency ) THEN
       select CONCAT(fieldNameList,'accountline.revisionsellcurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionsellcurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionsellcurrency,'~') into newValueList;
   END IF;
   
    IF ((OLD.revisionselllocalamount <> NEW.revisionselllocalamount) or (OLD.revisionselllocalamount is null and NEW.revisionselllocalamount is not null)
      or (OLD.revisionselllocalamount is not null and NEW.revisionselllocalamount is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionselllocalamount~') into fieldNameList;
        IF(OLD.revisionselllocalamount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionselllocalamount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionselllocalamount is not null) then
            select CONCAT(oldValueList,OLD.revisionselllocalamount,'~') into oldValueList;
        END IF;
        IF(NEW.revisionselllocalamount is not null) then
            select CONCAT(newValueList,NEW.revisionselllocalamount,'~') into newValueList;
        END IF;
    END IF;
    
     IF ((OLD.revisionsellexchangerate <> NEW.revisionsellexchangerate) or (OLD.revisionsellexchangerate is null and NEW.revisionsellexchangerate is not null)
      or (OLD.revisionsellexchangerate is not null and NEW.revisionsellexchangerate is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionsellexchangerate~') into fieldNameList;
        IF(OLD.revisionsellexchangerate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionsellexchangerate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionsellexchangerate is not null) then
            select CONCAT(oldValueList,OLD.revisionsellexchangerate,'~') into oldValueList;
        END IF;
        IF(NEW.revisionsellexchangerate is not null) then
            select CONCAT(newValueList,NEW.revisionsellexchangerate,'~') into newValueList;
        END IF;
    END IF;
   
    IF ((date_format(OLD.revisionsellvaluedate,'%Y-%m-%d')   <> date_format(NEW.revisionsellvaluedate,'%Y-%m-%d')) or (date_format(OLD.revisionsellvaluedate,'%Y-%m-%d') is null and date_format(NEW.revisionsellvaluedate,'%Y-%m-%d') is not null)
      or (date_format(OLD.revisionsellvaluedate,'%Y-%m-%d') is not null and date_format(NEW.revisionsellvaluedate,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'accountline.revisionsellvaluedate~') into fieldNameList;
           IF(OLD.revisionsellvaluedate is null) THEN
        select CONCAT(oldValueList," ",'~') into oldValueList;
      END IF;
      IF(NEW.revisionsellvaluedate is null) THEN
        select CONCAT(newValueList," ",'~') into newValueList;
      END IF;

    IF(OLD.revisionsellvaluedate is not null) THEN
        select CONCAT(oldValueList,OLD.revisionsellvaluedate ,'~') into oldValueList;
     END IF;
      IF(NEW.revisionsellvaluedate is not null) THEN
        select CONCAT(newValueList,NEW.revisionsellvaluedate ,'~') into newValueList;
      END IF;
   END IF;
    
    IF ((OLD.revisionselllocalrate <> NEW.revisionselllocalrate) or (OLD.revisionselllocalrate is null and NEW.revisionselllocalrate is not null)
      or (OLD.revisionselllocalrate is not null and NEW.revisionselllocalrate is null)) THEN
        select CONCAT(fieldNameList,'accountline.revisionselllocalrate~') into fieldNameList;
        IF(OLD.revisionselllocalrate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.revisionselllocalrate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.revisionselllocalrate is not null) then
            select CONCAT(oldValueList,OLD.revisionselllocalrate,'~') into oldValueList;
        END IF;
        IF(NEW.revisionselllocalrate is not null) then
            select CONCAT(newValueList,NEW.revisionselllocalrate,'~') into newValueList;
        END IF;
    END IF; 
    
      IF (OLD.estimatecontractcurrency <> NEW.estimatecontractcurrency ) THEN
       select CONCAT(fieldNameList,'accountline.estimatecontractcurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estimatecontractcurrency,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estimatecontractcurrency,'~') into newValueList;
   END IF;

 IF (OLD.paymentStatus <> NEW.paymentStatus ) THEN
       select CONCAT(fieldNameList,'accountline.paymentStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.paymentStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.paymentStatus,'~') into newValueList;
   END IF;

   IF ((OLD.estimatecontractrate <> NEW.estimatecontractrate) or (OLD.estimatecontractrate is null and NEW.estimatecontractrate is not null)
      or (OLD.estimatecontractrate is not null and NEW.estimatecontractrate is null)) THEN
        select CONCAT(fieldNameList,'accountline.estimatecontractrate~') into fieldNameList;
        IF(OLD.estimatecontractrate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatecontractrate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatecontractrate is not null) then
            select CONCAT(oldValueList,OLD.estimatecontractrate,'~') into oldValueList;
        END IF;
        IF(NEW.estimatecontractrate is not null) then
            select CONCAT(newValueList,NEW.estimatecontractrate,'~') into newValueList;
        END IF;
    END IF; 

    
     IF ((OLD.estimatecontractexchangerate <> NEW.estimatecontractexchangerate) or (OLD.estimatecontractexchangerate is null and NEW.estimatecontractexchangerate is not null)
      or (OLD.estimatecontractexchangerate is not null and NEW.estimatecontractexchangerate is null)) THEN
        select CONCAT(fieldNameList,'accountline.estimatecontractexchangerate~') into fieldNameList;
        IF(OLD.estimatecontractexchangerate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatecontractexchangerate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatecontractexchangerate is not null) then
            select CONCAT(oldValueList,OLD.estimatecontractexchangerate,'~') into oldValueList;
        END IF;
        IF(NEW.estimatecontractexchangerate is not null) then
            select CONCAT(newValueList,NEW.estimatecontractexchangerate,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.estimatecontractrateammount <> NEW.estimatecontractrateammount) or (OLD.estimatecontractrateammount is null and NEW.estimatecontractrateammount is not null)
      or (OLD.estimatecontractrateammount is not null and NEW.estimatecontractrateammount is null)) THEN
        select CONCAT(fieldNameList,'accountline.estimatecontractrateammount~') into fieldNameList;
        IF(OLD.estimatecontractrateammount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatecontractrateammount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatecontractrateammount is not null) then
            select CONCAT(oldValueList,OLD.estimatecontractrateammount,'~') into oldValueList;
        END IF;
        IF(NEW.estimatecontractrateammount is not null) then
            select CONCAT(newValueList,NEW.estimatecontractrateammount,'~') into newValueList;
        END IF;
        
    END IF;
    
    IF (OLD.networkBillToCode <> NEW.networkBillToCode ) THEN
       select CONCAT(fieldNameList,'accountline.networkBillToCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.networkBillToCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.networkBillToCode,'~') into newValueList;
   END IF;
   
   IF (OLD.networkBillToName <> NEW.networkBillToName) THEN
       select CONCAT(fieldNameList,'accountline.networkBillToName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.networkBillToName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.networkBillToName,'~') into newValueList;
   END IF;
    
    IF (OLD.varianceExpenseAmount<> NEW.varianceExpenseAmount) THEN
       select CONCAT(fieldNameList,'accountline.varianceExpenseAmount~') into fieldNameList;
       select CONCAT(oldValueList,OLD.varianceExpenseAmount,'~') into oldValueList;
       select CONCAT(newValueList,NEW.varianceExpenseAmount,'~') into newValueList;
   END IF;


  IF (OLD.accrueRevenueVariance <> NEW.accrueRevenueVariance ) THEN
       select CONCAT(fieldNameList,'accountline.accrueRevenueVariance~') into fieldNameList;
       select CONCAT(oldValueList,OLD.accrueRevenueVariance,'~') into oldValueList;
       select CONCAT(newValueList,NEW.accrueRevenueVariance,'~') into newValueList;
   END IF;

  IF (OLD.accrueExpenseVariance <> NEW.accrueExpenseVariance ) THEN
       select CONCAT(fieldNameList,'accountline.accrueExpenseVariance~') into fieldNameList;
       select CONCAT(oldValueList,OLD.accrueExpenseVariance,'~') into oldValueList;
       select CONCAT(newValueList,NEW.accrueExpenseVariance,'~') into newValueList;
   END IF;


  IF (OLD.revisionDescription <> NEW.revisionDescription) THEN
       select CONCAT(fieldNameList,'accountline.revisionDescription~') into fieldNameList;
       select CONCAT(oldValueList,OLD.revisionDescription,'~') into oldValueList;
       select CONCAT(newValueList,NEW.revisionDescription,'~') into newValueList;
   END IF;

IF (OLD.writeOffReason <> NEW.writeOffReason) THEN
       select CONCAT(fieldNameList,'accountline.writeOffReason~') into fieldNameList;
       select CONCAT(oldValueList,OLD.writeOffReason,'~') into oldValueList;
       select CONCAT(newValueList,NEW.writeOffReason,'~') into newValueList;
   END IF;

IF (OLD.authorizedCreditNote <> NEW.authorizedCreditNote ) THEN
       select CONCAT(fieldNameList,'accountline.authorizedCreditNote','~') into fieldNameList;
       IF(OLD.authorizedCreditNote = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.authorizedCreditNote = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.authorizedCreditNote = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.authorizedCreditNote = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
    END IF;

if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') <> date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
CALL add_tblHistory (OLD.id,"accountline", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
end if;
if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') = date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
CALL add_tblHistory (OLD.id,"accountline", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
end if;

END
$$

