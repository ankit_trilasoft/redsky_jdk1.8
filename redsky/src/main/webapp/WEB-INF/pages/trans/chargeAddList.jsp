<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="chargeList.title"/></title>   
    <meta name="heading" content="<fmt:message key='chargeList.heading'/>"/> 
    
<style>
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-18px;
padding:2px 0;
text-align:right;
width:99%;
}
#overlay11 {
	filter:alpha(opacity=70);
	-moz-opacity:0.7;
	-khtml-opacity: 0.7;
	opacity: 0.7;
	position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
	background:url(images/over-load.png);
}
</style>

</head>
<s:form id="chargesListForm" action="" method="post" >
<s:hidden name="contractCopy" value="<%=request.getParameter("contract")%>"/> 
<s:hidden name="contracts.id" value="<%=request.getParameter("id")%>"/> 
<%--<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editCharges.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
--%>
<c:out value="${button}" escapeXml="false" /> 

<div id="otabs">
	 <ul>
	 <li><a class="current"><span>Contract List</span></a></li>
	 </ul>
</div>
<div class="spnblk">&nbsp;</div> 

<s:set name="chargesByContract" value="chargesByContract" scope="request"/>  
<display:table name="chargesByContract" class="table" requestURI="" id="chargesByContract" defaultsort="2" pagesize="16" >   
   <display:column><input type="radio" name="dd"  onclick="countChargesByContract(this,'${chargesByContract.contract}','${chargesByContract.count}');" value="${chargesByContract.id}"/></display:column>
    <display:column property="contract"  title="Contract" />   
	 <display:column property="begin"  title="Begin" format="{0,date,dd-MMM-yyyy}"/> 
	<display:column property="ending" title="Ending" format="{0,date,dd-MMM-yyyy}"/> 
	<display:column property="count" title="Count"/> 
</display:table>  

<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<div id="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait...</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>
</s:form>
<c:if test="${hitFlag == 1}" >
	<c:redirect url="/charge23.html?id=${contracts.id}"  />
</c:if>


<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['chargesListForm'].elements['charges.charge'].value = "";
	document.forms['chargesListForm'].elements['charges.description'].value = "";
}
</script>   
<script language="javascript" type="text/javascript">
function check1(value,value1,capacity){
	if( value1 == 'X' && (capacity == '' || capacity == '1' || capacity == '2')){
		document.forms['assignItemsForm'].elements['forwardBtn'].disabled=true;
		alert('This location is already occupied. Please select any other.');
		  return false;  
	}else{
		document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
	}
	return true;
}
		
function  countChargesByContract(position,contract,count){
	var	agree= confirm(contract+" "+"have"+" "+count+" "+"Charges. Do you want to copy these charges?");
	if(agree){ 
		var copyAgentAccount="No"
		var agent=confirm("Do you want to copy Account and Agent Details?");
		if(agent){
			copyAgentAccount="Yes"
		}
		showOrHide(0);
		
		var url = "copyCharges.html?contract="+ encodeURI(contract)+"&copyAgentAccount="+copyAgentAccount;
		////alert(url);
		document.forms['chargesListForm'].action = url;
		document.forms['chargesListForm'].submit();
		//window.refreshParent()
		setTimeout("window.self.close()", 700);
		showOrHide(1);
	}
	else{
		return false;
	}	
}

function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay11"].visibility='show';
       else
          document.getElementById("overlay11").style.visibility='visible';
   }
}

showOrHide(0);
</script>