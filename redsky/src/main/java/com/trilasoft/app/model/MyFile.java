package com.trilasoft.app.model;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "myfile")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class MyFile extends BaseObject implements Serializable{

	private Long id;

	private String corpID;
	
	@Transient 
    public int fSize;

	//private File file;

	private String fileId;

	private String fileType;

	private String fileContentType;

	private String fileFileName;

	private String location;

	private String customerNumber;

	private String description;

	private Date createdOn;

	private String createdBy;

	private Date updatedOn;

	private String updatedBy;

	private Boolean isBookingAgent;

	private Boolean isNetworkAgent;

	private Boolean isOriginAgent;

	private Boolean isSubOriginAgent;

	private Boolean isDestAgent;
	
	private Boolean isSubDestAgent;
	
	private Boolean isCportal;

	private Boolean isAccportal;

	private Boolean isPartnerPortal;
	
	private Boolean coverPageStripped;

	private Boolean active;

	private String mapFolder;

	private Date docSent;

	private String fileSize;

	private Boolean isSecure;
	
	private String networkLinkId ;
	
	private String transDocStatus ;
	
	private String transferredBy ;
	private String emailStatus;
	private String documentCategory;
	private Boolean isServiceProvider;
	private Boolean invoiceAttachment;
	 private String accountLineVendorCode;
	 private String accountLinePayingStatus;
	 private Boolean isDriver;
	 private String docCode ;
	 
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("fileId", fileId)
				.append("fileType", fileType)
				.append("fileContentType", fileContentType)
				.append("fileFileName", fileFileName)
				.append("location", location)
				.append("customerNumber", customerNumber)
				.append("description", description)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.append("isBookingAgent", isBookingAgent)
				.append("isNetworkAgent", isNetworkAgent)
				.append("isOriginAgent", isOriginAgent)
				.append("isSubOriginAgent", isSubOriginAgent)
				.append("isDestAgent", isDestAgent)
				.append("isSubDestAgent", isSubDestAgent)
				.append("isCportal", isCportal)
				.append("isAccportal", isAccportal)
				.append("isPartnerPortal", isPartnerPortal)
				.append("coverPageStripped", coverPageStripped)
				.append("active", active).append("mapFolder", mapFolder)
				.append("docSent", docSent).append("fileSize", fileSize)
				.append("isSecure", isSecure)
				.append("networkLinkId", networkLinkId)
				.append("transDocStatus", transDocStatus)
				.append("transferredBy", transferredBy)
				.append("emailStatus",emailStatus).append("documentCategory",documentCategory)
				.append("isServiceProvider", isServiceProvider)
				.append("invoiceAttachment", invoiceAttachment)
				.append("accountLineVendorCode", accountLineVendorCode)
				.append("accountLinePayingStatus", accountLinePayingStatus).append("isDriver", isDriver).append("docCode", docCode)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MyFile))
			return false;
		MyFile castOther = (MyFile) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(fileId, castOther.fileId)
				.append(fileType, castOther.fileType)
				.append(fileContentType, castOther.fileContentType)
				.append(fileFileName, castOther.fileFileName)
				.append(location, castOther.location)
				.append(customerNumber, castOther.customerNumber)
				.append(description, castOther.description)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(isBookingAgent, castOther.isBookingAgent)
				.append(isNetworkAgent, castOther.isNetworkAgent)
				.append(isOriginAgent, castOther.isOriginAgent)
				.append(isSubOriginAgent, castOther.isSubOriginAgent)
				.append(isDestAgent, castOther.isDestAgent)
				.append(isSubDestAgent, castOther.isSubDestAgent)
				.append(isCportal, castOther.isCportal)
				.append(isAccportal, castOther.isAccportal)
				.append(isPartnerPortal, castOther.isPartnerPortal)
				.append(coverPageStripped, castOther.coverPageStripped)
				.append(active, castOther.active)
				.append(mapFolder, castOther.mapFolder)
				.append(docSent, castOther.docSent)
				.append(fileSize, castOther.fileSize)
				.append(isSecure, castOther.isSecure)
				.append(networkLinkId, castOther.networkLinkId)
				.append(transDocStatus, castOther.transDocStatus)
				.append(transferredBy, castOther.transferredBy)
				.append(emailStatus, castOther.emailStatus).append(documentCategory, castOther.documentCategory)
				.append(isServiceProvider, castOther.isServiceProvider)
				.append(invoiceAttachment, castOther.invoiceAttachment)
				.append(accountLineVendorCode, castOther.accountLineVendorCode)
				.append(accountLinePayingStatus, castOther.accountLinePayingStatus).append(isDriver, castOther.isDriver).append(docCode, castOther.docCode)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(fileId)
				.append(fileType).append(fileContentType).append(fileFileName)
				.append(location).append(customerNumber).append(description)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).append(isBookingAgent)
				.append(isNetworkAgent).append(isOriginAgent)
				.append(isSubOriginAgent).append(isDestAgent)
				.append(isSubDestAgent).append(isCportal).append(isAccportal)
				.append(isPartnerPortal).append(coverPageStripped)
				.append(active).append(mapFolder).append(docSent)
				.append(fileSize).append(isSecure).append(networkLinkId)
				.append(transDocStatus).append(transferredBy)
				.append(emailStatus).append(documentCategory).append(isServiceProvider)
				.append(invoiceAttachment)
				.append(accountLineVendorCode)
				.append(accountLinePayingStatus)
				.append(isDriver)
				.append(docCode)
				.toHashCode();
	}

	@Column(length = 15)
	public String getNetworkLinkId() {
		return networkLinkId;
	}

	public void setNetworkLinkId(String networkLinkId) {
		this.networkLinkId = networkLinkId;
	}

	/*@Column
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}*/

	@Column(length = 45)
	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	@Column(length = 375)
	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 254)
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column(length = 25)
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@Column(length = 15)
	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	@Column(length = 15)
	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsCportal() {
		return isCportal;
	}

	public void setIsCportal(Boolean isCportal) {
		this.isCportal = isCportal;
	}

	public Boolean getIsAccportal() {
		return isAccportal;
	}

	public void setIsAccportal(Boolean isAccportal) {
		this.isAccportal = isAccportal;
	}

	public Boolean getIsPartnerPortal() {
		return isPartnerPortal;
	}

	public void setIsPartnerPortal(Boolean isPartnerPortal) {
		this.isPartnerPortal = isPartnerPortal;
	}

	public Boolean getCoverPageStripped() {
		return coverPageStripped;
	}

	public void setCoverPageStripped(Boolean coverPageStripped) {
		this.coverPageStripped = coverPageStripped;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getDocSent() {
		return docSent;
	}

	public void setDocSent(Date docSent) {
		this.docSent = docSent;
	}

	@Column(length = 15)
	public String getMapFolder() {
		return mapFolder;
	}

	public void setMapFolder(String mapFolder) {
		this.mapFolder = mapFolder;
	}

	@Column(length = 25)
	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	@Column
	public Boolean getIsSecure() {
		return isSecure;
	}

	public void setIsSecure(Boolean isSecure) {
		this.isSecure = isSecure;
	}
	@Column
	public Boolean getIsBookingAgent() {
		return isBookingAgent;
	}

	public void setIsBookingAgent(Boolean isBookingAgent) {
		this.isBookingAgent = isBookingAgent;
	}
	@Column
	public Boolean getIsNetworkAgent() {
		return isNetworkAgent;
	}

	public void setIsNetworkAgent(Boolean isNetworkAgent) {
		this.isNetworkAgent = isNetworkAgent;
	}
	@Column
	public Boolean getIsOriginAgent() {
		return isOriginAgent;
	}

	public void setIsOriginAgent(Boolean isOriginAgent) {
		this.isOriginAgent = isOriginAgent;
	}
	@Column
	public Boolean getIsSubOriginAgent() {
		return isSubOriginAgent;
	}

	public void setIsSubOriginAgent(Boolean isSubOriginAgent) {
		this.isSubOriginAgent = isSubOriginAgent;
	}
	@Column
	public Boolean getIsDestAgent() {
		return isDestAgent;
	}

	public void setIsDestAgent(Boolean isDestAgent) {
		this.isDestAgent = isDestAgent;
	}
	@Column
	public Boolean getIsSubDestAgent() {
		return isSubDestAgent;
	}

	public void setIsSubDestAgent(Boolean isSubDestAgent) {
		this.isSubDestAgent = isSubDestAgent;
	}
	@Column
	public String getTransDocStatus() {
		return transDocStatus;
	}

	public void setTransDocStatus(String transDocStatus) {
		this.transDocStatus = transDocStatus;
	}
	@Column
	public String getTransferredBy() {
		return transferredBy;
	}

	public void setTransferredBy(String transferredBy) {
		this.transferredBy = transferredBy;
	}
	@Transient
	public int getfSize() {
		return fSize;
	}

	public void setfSize(int fSize) {
		this.fSize = fSize;
	}
	@Column(length=1000)
	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}
	@Column(length=35)
	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public Boolean getIsServiceProvider() {
		return isServiceProvider;
	}

	public void setIsServiceProvider(Boolean isServiceProvider) {
		this.isServiceProvider = isServiceProvider;
	}

	public Boolean getInvoiceAttachment() {
		return invoiceAttachment;
	}

	public void setInvoiceAttachment(Boolean invoiceAttachment) {
		this.invoiceAttachment = invoiceAttachment;
	}
	@Column
	public String getAccountLineVendorCode() {
		return accountLineVendorCode;
	}

	public void setAccountLineVendorCode(String accountLineVendorCode) {
		this.accountLineVendorCode = accountLineVendorCode;
	}
	@Column
	public String getAccountLinePayingStatus() {
		return accountLinePayingStatus;
	}

	public void setAccountLinePayingStatus(String accountLinePayingStatus) {
		this.accountLinePayingStatus = accountLinePayingStatus;
	}

	/**
	 * @return the isDriver
	 */
	@Column
	public Boolean getIsDriver() {
		return isDriver;
	}

	/**
	 * @param isDriver the isDriver to set
	 */
	public void setIsDriver(Boolean isDriver) {
		this.isDriver = isDriver;
	}

	/**
	 * @return the docCode
	 */
	@Column
	public String getDocCode() {
		return docCode;
	}

	/**
	 * @param docCode the docCode to set
	 */
	public void setDocCode(String docCode) {
		this.docCode = docCode;
	}
}
