package com.trilasoft.app.dao;
import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.RemovalRelocationService;

public interface RemovalRelocationServiceDao  extends GenericDao<RemovalRelocationService, Long> {

}
