<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Network Partner Details</title>
<meta name="heading" content="Network Partner Details" />
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
<style type="text/css">

</style>
<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
	for(i=0;i<=5;i++){
		document.forms['networkPartnerForm'].elements[i].value = "";
	}
}

</script>
</head>
<body style="color:#222222;background-color:#dddddd">
<s:form id="networkPartnerForm" name="networkPartnerForm" action="saveNetworkPartner" method="post" validate="true">
	<s:hidden name="selectedCorpId" value="<%=request.getParameter("selectedCorpId") %>"/>
	<s:hidden name="networkPartners.id"/>
	<div id="Layer1" style="width:85%;">
	<div id="newmnav">
     	<ul>
     			<li id="newmnav1" style="background:#FFF"><a class="current"><span>Network Partner Details <img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
     			<li><a href="maintainNetworkPartner.html?selectedCorpId=${selectedCorpId}"><span>Network Partner List</span></a></li>       
      </ul>
  </div>
  <div class="spn">&nbsp;</div>
  <div id="content" align="center">
   <div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:700px;">
			<tbody>				
						<tr>
							<td align="right" width="20px"></td>
							<td align="right" class="listwhitetext" width="400px">Corp Id</td><td width="20px"></td>
                            <td class="listwhitetext"><s:textfield name="networkPartners.corpId" maxlength="20" readonly="true" cssClass="input-textUpper" cssStyle="width:200px" tabindex="1"/></td>
							<td align="right" width="20px"></td>
							
							<td align="right" width="10px"></td>
							<td align="right" class="listwhitetext" width="400px">Agent Partner Code</td><td width="20px"></td>
                            <td class="listwhitetext"><s:textfield name="networkPartners.agentPartnerCode" maxlength="20" cssClass="input-text" cssStyle="width:200px" tabindex="2"/></td>
							<td align="right" width="10px"></td>
						</tr>
						<tr><td height="15px"></td></tr>
                        <tr>
                            <td align="right" width="10px"></td>
                            <td align="right" class="listwhitetext" width="400px">Agent Corp Id</td><td width="20px"></td>
						    <td class="listwhitetext"><s:textfield name="networkPartners.agentCorpId" maxlength="10" readonly="true" cssClass="input-textUpper" cssStyle="width:200px" tabindex="3"/></td>												
						    <td align="right" width="10px"></td>
						    
						    <td align="right" width="10px"></td>
							<td align="right" class="listwhitetext" width="400px">Agent Local Partner Code</td><td width="20px"></td>
                            <td class="listwhitetext"><s:textfield name="networkPartners.agentLocalPartnerCode" maxlength="20" cssClass="input-text" cssStyle="width:200px" tabindex="4"/></td>
							<td align="right" width="10px"></td>
						</tr>
						<tr><td height="15px"></td></tr>
						<tr>
							<td align="right" width="10px"></td>
							<td align="right" class="listwhitetext" width="400px">Agent Name</td><td width="20px"></td>
							<td class="listwhitetext"><s:textfield name="networkPartners.agentName" maxlength="50" cssClass="input-text" cssStyle="width:200px" tabindex="5" /></td>
							<td align="right" width="10px"></td>	
							
							<td align="right" width="10px"></td>
							<td align="right" class="listwhitetext" width="400px">Agent CompanyDivision</td><td width="20px"></td>
                            <td class="listwhitetext"><s:textfield name="networkPartners.agentCompanyDivision" maxlength="15" cssClass="input-text" cssStyle="width:200px" tabindex="6"/></td>
							<td align="right" width="10px"></td>			
						</tr>
						<tr><td height="15px"></td></tr>
			</tbody>
	</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
	   <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:75px; height:25px" tabindex="7" />
	   <s:reset cssClass="cssbutton"  key="button.reset" cssStyle="width:75px; height:25px" onclick="return clear_fields();" tabindex="8"/>

<c:if test="${hitFlag == '1'}" >
	<c:redirect url="/maintainNetworkPartner.html?selectedCorpId=${selectedCorpId}"/>
</c:if>	
</s:form>
</body>