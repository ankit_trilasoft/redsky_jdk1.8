package com.trilasoft.app.model;

import java.util.Date;
import org.appfuse.model.BaseObject;   
import javax.persistence.Column;
import javax.persistence.Entity;   
import javax.persistence.GenerationType;   
import javax.persistence.Id;   
import javax.persistence.GeneratedValue;   
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder; 
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="menu_item_permission")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")


public class MenuItemPermission extends BaseObject
{
	private String corpID;
	private Long id;
	private Long menuItemId;
	private int permission;
	private String recipient;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private MenuItem menuItem;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("menuItemId", menuItemId).append("permission",
				permission).append("recipient", recipient).append("createdBy",
				createdBy).append("createdOn", createdOn).append("updatedBy",
				updatedBy).append("updatedOn", updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MenuItemPermission))
			return false;
		MenuItemPermission castOther = (MenuItemPermission) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(menuItemId, castOther.menuItemId).append(
				permission, castOther.permission).append(recipient,
				castOther.recipient).append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(
				menuItemId).append(permission).append(recipient).append(
				createdBy).append(createdOn).append(updatedBy)
				.append(updatedOn).toHashCode();
	}
	@ManyToOne
	@JoinColumn(name="menuItemId", insertable=false, updatable=false,
	referencedColumnName="id")
	
	public MenuItem getMenuItem() {
		return menuItem;
	}
	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(nullable=false, updatable=false)
	public Long getMenuItemId() {
		return menuItemId;
	}
	
	public void setMenuItemId(Long menuItemId) {
		this.menuItemId = menuItemId;
	}
	@Column
	public int getPermission() {
		return permission;
	}
	public void setPermission(int permission) {
		this.permission = permission;
	}
	
}
