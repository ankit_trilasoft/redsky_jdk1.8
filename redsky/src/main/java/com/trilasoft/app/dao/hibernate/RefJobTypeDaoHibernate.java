package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.RefJobTypeDao;
import com.trilasoft.app.model.RefJobType;

public class RefJobTypeDaoHibernate extends GenericDaoHibernate<RefJobType, Long> implements RefJobTypeDao{
	
	public RefJobTypeDaoHibernate() {
		super(RefJobType.class);
	}

	public List isExisted(String job,String companydivision,String sessionCorpID) {
		return getHibernateTemplate().find("select count(*) from RefJobType where job = '" + job + "' and companyDivision='" + companydivision + "' and corpID='"+sessionCorpID+"' ");
	}
	public List searchResult(String job,String companydivision,String surveytool, String sessionCorpID){			
		//return getHibernateTemplate().find("from RefJobType where job = '" + job + "' and companyDivision='" + companydivision + "' and surveyTool='" + surveytool + "' and corpID='"+sessionCorpID+"' ");
		List check=new ArrayList();
		if(surveytool==null){
			check = getHibernateTemplate().find("from RefJobType where job like '%" + job + "%' and companyDivision like '%" + companydivision + "%'  and corpID='"+sessionCorpID+"' ");	
		}else{
			check = getHibernateTemplate().find("from RefJobType where job like '%" + job + "%' and companyDivision like '%" + companydivision + "%' and surveyTool like '%" + surveytool + "%' and corpID='"+sessionCorpID+"' ");
		}		 
		return check;
	}
}
