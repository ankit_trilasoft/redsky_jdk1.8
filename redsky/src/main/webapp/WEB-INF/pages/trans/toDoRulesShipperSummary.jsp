<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>ToDo Rules Summary</title> 
<meta name="heading" content="ToDo Rules Summary"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
!margin-top:-18px;
padding:0px;
text-align:right;
width:100%;
}
#otabs {
margin-bottom:0;
!margin-bottom:-20px;
margin-left:40px;
position:relative;
}
</style>

<script language="javascript" type="text/javascript">
function getOwnerResult(shipper)
{	
var ship = document.forms['toDoRulesSummary'].elements[shipper].value;
window.opener.document.forms['toDo'].elements['shipperName'].value=ship;
window.opener.getShiperResult();
window.close();
}

</script>
</head>

	<s:form id="toDoRulesSummary" name="toDoRulesSummary" action='' method="post" validate="true">   
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Group By Shipper</span></a></li>
		  </ul>
		  
	</div>
	<!--<span style="padding-left:100px;!padding-left:300px;font-size:13px;color:#003366;">List of Currently Available Tariffs</span>
	--><div class="spnblk">&nbsp;</div>
	
	<display:table name="toDoRulesSummary" class="table" requestURI="" id="recentTariffsId" export="true" defaultsort="" pagesize="10" style="width:100%" >   

	<display:column group="1" title="Shipper"><a href="#" onclick="getOwnerResult('Shipper${recentTariffsId.id}')"/><c:out value="${recentTariffsId.shipper}"/></a>
	<s:hidden name="Shipper${recentTariffsId.id}" value="${recentTariffsId.shipper }"  />
	</display:column>
	<display:column property="current"  sortable="true" title="Current"/>
   	<display:column property="overDue"  sortable="true" title="Over Due"/>
   	<display:column property="total"  sortable="true" title="Total"/>
   	<display:setProperty name="export.excel.filename" value="GroupByShipper.xls"/> 
    <display:setProperty name="export.csv.filename" value="GroupByShipper.csv"/> 
    <display:setProperty name="export.pdf.filename" value="GroupByShipper.pdf"/> 
   	</display:table>
	
	</div>
</s:form> 

<script type="text/javascript"> 

</script> 