package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.T20VisionLogInfo;

public interface T20VisionLogInfoManager extends GenericManager<T20VisionLogInfo, Long> {
	
	List findT20VisionLogList(String sessionCorpID);
	List searcht20VisionLogInfoList(String entryNumber,String invoiceNumber,String orderNumber,String orderNumberLine,String status,String sessionCorpID);

}
