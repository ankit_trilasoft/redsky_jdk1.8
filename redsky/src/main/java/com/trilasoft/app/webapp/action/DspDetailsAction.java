package com.trilasoft.app.webapp.action;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.google.gson.Gson;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.DspDetailsDaoHibernate.TenancyBillingDTO;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.AdAddressesDetails;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ConsigneeInstruction;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsFamilyDetails;
import com.trilasoft.app.model.DspDetails;
import com.trilasoft.app.model.Loss;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.RemovalRelocationService;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TenancyUtilityService;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.AdAddressesDetailsManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CartonManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ConsigneeInstructionManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsFamilyDetailsManager;
import com.trilasoft.app.service.DspDetailsManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.LossManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RemovalRelocationServiceManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.TenancyUtilityServiceManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VehicleManager;

public class DspDetailsAction extends BaseAction implements Preparable {
	private Long id;
	private String sessionCorpID;
	private DspDetails dspDetails;
	private DspDetailsManager dspDetailsManager;
	private Long serviceOrderId;
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private ClaimManager claimManager;
	private LossManager lossManager;
	private RemovalRelocationService removalRelocationService;
	private RemovalRelocationServiceManager removalRelocationServiceManager;
	private  Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private String countDspDetailsNotes;		
	private NotesManager notesManager;
	private String gotoPageString;
	private String validateFormNav;
	private String shipSize;
	private String minShip;
	private String countShip;
	private Map<String, String> relocationServices;
	private RefMasterManager refMasterManager;
    private Map<String, String> viewStatus;
    private Map<String, String> paidStatus;
    private Map<String, String> paymentresponsibility;
    private Map<String, String> dpsservices;
    private Map<String, String> hsmstatus;
    private Map<String, String> pdtservices;
    private Map<String, String> mtsstatus;
    private Map<String, String> utilities;
    private Map<String, String> depositby;    
    private Map<String, String> currency;
    private Map<String, String> timeduration;
    private Map<String, Set<String>>  serviceMandatoryFile;
    private Company company;
    private CompanyManager companyManager;
    private String currencySign;
    private Boolean isNetwork;
    private Boolean costElementFlag;
    private Boolean isNetworkBookingAgent=false;
    private Boolean isNetworkCAR_vendorCode=false;
    private Boolean isNetworkCOL_vendorCode=false;
    private Boolean isNetworkTRG_vendorCode=false;
    private Boolean isNetworkHOM_vendorCode=false;
    private Boolean isNetworkRNT_vendorCode=false;
    private Boolean isNetworkLAN_vendorCode=false;
    private Boolean isNetworkMMG_vendorCode=false;
    private Boolean isNetworkONG_vendorCode=false;
    private Boolean isNetworkPRV_vendorCode=false;
    private Boolean isNetworkAIO_vendorCode=false;
    private Boolean isNetworkEXP_vendorCode=false;
    private Boolean isNetworkRPT_vendorCode=false;
    private Boolean isNetworkTAX_vendorCode=false;
    private Boolean isNetworkTAC_vendorCode=false;
    private Boolean isNetworkTEN_vendorCode=false;
    private Boolean isNetworkVIS_vendorCode=false;
    private Boolean isNetworkWOP_vendorCode=false;
    private Boolean isNetworkREP_vendorCode=false;
    private Boolean isNetworkSET_vendorCode=false;
    private Boolean isNetworkSCH_schoolSelected=false;
    private Boolean isNetworkBookingAgentCode=false;
    private Boolean isNetworkNetworkPartnerCode=false;
    private Boolean isNetworkFRL_vendorCode=false;
    private Boolean isNetworkAPU_vendorCode=false;
    private Boolean isNetworkINS_vendorCode=false;
    private Boolean isNetworkINP_vendorCode=false;
    private Boolean isNetworkEDA_vendorCode=false;
    private Boolean isNetworkTAS_vendorCode=false;
    public String buttonType;
    static final Logger logger = Logger.getLogger(DspDetailsAction.class);
    Date currentdate = new Date();
    private List maxSequenceNumber;
    private Long autoSequenceNumber;
	private String seqNum;
	private List maxShip;
	private String ship;
	private Long autoShip;
	private String shipnumber;
	private boolean networkAgent;
	private List accountLineList;
	private AccountLineManager accountLineManager;
	private AccountLine accountLine;
	private PartnerManager partnerManager;
	private TrackingStatus trackingStatusToRecod;
	private Billing billingRecods;
	private CompanyDivisionManager companyDivisionManager;
	private  ExchangeRateManager exchangeRateManager;
	DecimalFormat decimalFormat = new DecimalFormat("#.####");
	private ContainerManager containerManager;
	private CartonManager cartonManager;
	private VehicleManager vehicleManager;
	private ServicePartnerManager servicePartnerManager;
	private ConsigneeInstructionManager consigneeInstructionManager;
	private DsFamilyDetailsManager dsFamilyDetailsManager;
	private AdAddressesDetailsManager adAddressesDetailsManager;
	private ErrorLogManager errorLogManager;
	private boolean billingContractType;
	private String trackingStatusNetworkPartnerCode;
	private String trackingStatusNetworkPartnerName;
	private String trackingStatusBookingAgentContact;
	private String trackingStatusNetworkContact;
	private String trackingStatusBookingAgentEmail;
	private String trackingStatusNetworkEmail;
	private String trackingStatusBookingAgentExSO; 
	private String trackingStatusNetworkAgentExSO;
	private String usertype;	
    private Map<String, String>  serviceRelos;
    private  Map<String,String>euVatPercentList;
    private  Map<String,String>UTSIeuVatPercentList;
    private  Map<String,String>UTSIpayVatList;
    private  Map<String,String>UTSIpayVatPercentList;
    private  Map<String,String>leaseSignee;
    private  Map<String,String>allowance;
    private  Map<String,String>immigrationStatus;
    private Map<String,String>timeAuthorized;
    private String voxmeIntergartionFlag;
    private int autoIdNumber1;
    private String billToCodeAcc;
    private String rloSetVenderCode;
    private Long userId;
    private EmailSetupManager emailSetupManager;
    private Boolean isNetworkRLS_vendorCode=false;
    private Boolean isNetworkCAT_vendorCode=false;
    private Boolean isNetworkCLS_vendorCode=false;
    private Boolean isNetworkCHS_vendorCode=false;
    private Boolean isNetworkDPS_vendorCode=false;
    private Boolean isNetworkHSM_vendorCode=false;
    private Boolean isNetworkPDT_vendorCode=false;
    private Boolean isNetworkRCP_vendorCode=false;
    private Boolean isNetworkSPA_vendorCode=false;
    private Boolean isNetworkTCS_vendorCode=false;
    private Boolean isNetworkMTS_vendorCode=false;
    private Boolean isNetworkDSS_vendorCode=false;
    private List<SystemDefault> sysDefaultDetail;
    private SystemDefaultManager systemDefaultManager;
    private String systemDefaultCurrency;
    private String baseCurrency;
    private Map<String, String> paymentReferral=new HashMap<String, String>();
    private String surveyEmailList;
    private String jsonServiceForRelo;
    private String isRedSky;
    private String partCode;
    private String contactName;
    private String conEmail;
    private ServiceOrder serviceOrderToRecods;
	private TrackingStatus trackingStatusToRecods;
	private Map<String,String> comDivCodePerCorpIdMap;
	private static Map<String, String> yesno;
	private  Map<String, String> country;
	private  Map<String, String> state;
	private String enbState;
	private Map<String, String> countryCod;
	private Map<String, String> utilitiesIncluded; 
	private Map<String, String> termOfNotice;
	private Map<String, String> billingCycle;
	private Map<String,String>reloTenancyProperty;
	private  Map<String,String>euVatList;
	private String systemDefaultVatCalculation;
	private  Map<String,String>currencyExchangeRate;
	private Map<String, String> countryCodeAndFlex5Map = new LinkedHashMap<String, String>();
	private boolean accountLineAccountPortalFlag=false;
	private boolean cmmDmmAgent;
	
	public void prepare() throws Exception { 
		viewStatus = refMasterManager.findByParameter(sessionCorpID, "VIEWSTATUS");
		paidStatus = refMasterManager.findByParameter(sessionCorpID, "PAIDSTATUS");
		paymentresponsibility = refMasterManager.findByParameter(sessionCorpID, "PAYMENTRESPONSIBILITY");
		paymentReferral = refMasterManager.findByParameter(sessionCorpID, "PAYMENTREFERRAL");
		dpsservices = refMasterManager.findByParameter(sessionCorpID, "DPS_SERVICES");
		hsmstatus = refMasterManager.findByParameter(sessionCorpID, "HSM_STATUS");
		pdtservices = refMasterManager.findByParameter(sessionCorpID, "PDT_SERVICES");
		mtsstatus = refMasterManager.findByParameter(sessionCorpID, "MTS_STATUS");
		utilities = refMasterManager.findByParameter(sessionCorpID, "UTILITIES");
		depositby= refMasterManager.findByParameter(sessionCorpID, "DEPOSITBY");	       	
		currency = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
		leaseSignee = refMasterManager.findCodeOnleByParameter(sessionCorpID, "LEASE_SIGNELIST");
		allowance = refMasterManager.findCodeOnleByParameter(sessionCorpID, "ALLOWANCE");
		immigrationStatus = refMasterManager.findByParameter(sessionCorpID, "IMMIGRATION_STATUS");
		timeduration = refMasterManager.findByParameter(sessionCorpID, "TIMEDURATION");
		relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
		euVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "EUVAT");
		timeAuthorized = refMasterManager.findByParameter(sessionCorpID, "Time_Authorized");
		surveyEmailList= refMasterManager.surveyEmailListForRelo(sessionCorpID, "SERVICE");
		yesno = refMasterManager.findByParameter(sessionCorpID, "YESNO");
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		currencySign=company.getCurrencySign();
		country = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
		state = refMasterManager.findCountry(sessionCorpID, "STATE");
		enbState = customerFileManager.enableStateList(sessionCorpID);
		reloTenancyProperty = refMasterManager.findByParameter(sessionCorpID, "RELOTENANCYPROPERTY");
		termOfNotice = refMasterManager.findByParameter(sessionCorpID, "RELOTENANCYTERMOFNOTICE");
		billingCycle = refMasterManager.findByParameter(sessionCorpID, "RELOTENANCYBILLINGCYCLE");
		euVatList = refMasterManager.findByParameterWithoutParent(sessionCorpID, "EUVAT");
		currencyExchangeRate=exchangeRateManager.getExchangeRateWithCurrency(sessionCorpID);
		countryCodeAndFlex5Map = refMasterManager.findByParameterVatRecGL(sessionCorpID, "COUNTRY");
		systemDefaultVatCalculation="";
		List vatCalculationList=accountLineManager.findVatCalculation(sessionCorpID);
		if(vatCalculationList!=null &&  (!(vatCalculationList.isEmpty())) && vatCalculationList.get(0)!=null){
		systemDefaultVatCalculation =vatCalculationList.get(0).toString();
		}
		
		utilitiesIncluded = new LinkedHashMap();
		utilitiesIncluded.put("Yes", "Yes");
		utilitiesIncluded.put("No", "No");
		
		if(company!=null){
			 if(company.getUTSI()!=null){
				 networkAgent=company.getUTSI();
				}else{
					networkAgent=false;
				}
			 if(company.getVoxmeIntegration()!=null){
					voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
					}
			 if(company.getCmmdmmAgent()!=null){
					cmmDmmAgent = company.getCmmdmmAgent();
				}
			}
	   //costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
	   serviceRelos= refMasterManager.findServiceByJob("RLO",sessionCorpID, "SERVICE");
	   jsonServiceForRelo=new Gson().toJson(serviceRelos);
		rloSetVenderCode=dspDetailsManager.getRloVenderCode( userId ,sessionCorpID);
		sysDefaultDetail = systemDefaultManager.findByCorpID(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getBaseCurrency() !=null ){
					systemDefaultCurrency =systemDefault.getBaseCurrency();
				}
				if(systemDefault.getBaseCurrency() !=null){
					baseCurrency = systemDefault.getBaseCurrency();
				}
				if(systemDefault.getAccountLineAccountPortalFlag() !=null ){
					accountLineAccountPortalFlag = 	systemDefault.getAccountLineAccountPortalFlag();
				}
				if(systemDefault.getCostElement()!=null && systemDefault.getCostElement()){
					costElementFlag = systemDefault.getCostElement();
				}else{
					costElementFlag = false;
				}
			}
		}
		comDivCodePerCorpIdMap = companyDivisionManager.getComDivCodePerCorpIdList();
	}
    public DspDetailsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		usertype = user.getUserType();
		userId=user.getId();
	}	
	private Billing billing; 
	private BillingManager billingManager;
	private List multiplutilities;
	private List multiplutilities1;
	private String moveType;
	private String checkAccessQuotation;
	private String customerSurveyServiceDetails;
	private String resendEmailList;
	private MyFileManager myFileManager; 
	private String serviceCompleteDateVal;
   	public String edit()throws Exception { 
		if (id != null) {
			serviceMandatoryFile=refMasterManager.getMandatoryFileMap(sessionCorpID,"DOCUMENT");
			trackingStatus=trackingStatusManager.get(id);
			serviceOrder=serviceOrderManager.get(id);
			getRequest().setAttribute("soLastName", serviceOrder.getLastName());
	    	customerFile = serviceOrder.getCustomerFile();
	    	miscellaneous = miscellaneousManager.get(id); 
	    	billing=billingManager.get(id);
			moveType = ((customerFile.getMoveType()==null)?"":customerFile.getMoveType());
			checkAccessQuotation=((company.getAccessQuotationFromCustomerFile()==null)?"N":(company.getAccessQuotationFromCustomerFile()?"Y":"N"));
			customerSurveyServiceDetails=dspDetailsManager.getCustomerSurveyDetails(serviceOrder.getShipNumber(),serviceOrder.getId(),sessionCorpID);
	    	try{
	   		 isNetwork=false;
	   		if(serviceOrder.getIsNetworkRecord()==null){
	   			isNetwork=false;
	   		}
	   		else if(serviceOrder.getIsNetworkRecord()){
	   			isNetwork=true;
	   		}
	   		
	   		isNetworkBookingAgent=getIsNetworkBookingAgent(sessionCorpID, serviceOrder.getBookingAgentCode());
	   		billingContractType=trackingStatusManager.getContractType(sessionCorpID ,billing.getContract());
	    	}catch(Exception e ){
	    		e.printStackTrace();
	    	}
	    	
	    	if((dspDetailsManager.checkById(id)).isEmpty())
			{				
	    		dspDetails = new DspDetails(); 
				dspDetails.setCreatedOn(serviceOrder.getCreatedOn());
				dspDetails.setUpdatedOn(serviceOrder.getUpdatedOn());
				dspDetails.setCreatedBy(serviceOrder.getCreatedBy());
				dspDetails.setUpdatedBy(serviceOrder.getUpdatedBy());
				dspDetails.setId(id);
				dspDetails.setServiceOrderId(id);
				dspDetails.setCorpID(sessionCorpID);
				dspDetails.setShipNumber(serviceOrder.getShipNumber());
				dspDetails=dspDetailsManager.save(dspDetails);
			}else{
				dspDetails = dspDetailsManager.get(id);
			}
			if(dspDetails.getId()!=null && surveyEmailList!=null && !surveyEmailList.equalsIgnoreCase("")){
				try{
    			Class<?> cls = dspDetails.getClass();
    			Field chap = null;
    			Object obj=null;	
    			Field chap1 = null;
    			Object obj1=null;		
    			Date dateTemp=null;
    			Long StartTime = System.currentTimeMillis();
    			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
    			
				Date today = new Date();
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(today));
				String dateTemp1=nowYYYYMMDD1.toString();
				dateTemp = dateformatYYYYMMDD1.parse(dateTemp1);
				Calendar c = Calendar.getInstance();
				c.setTime(dateTemp);
				StartTime=c.getTimeInMillis();
    			
    			resendEmailList="";    	
    			String tempSurveyEmailList=surveyEmailList+",NET";
				for(String type:tempSurveyEmailList.split(",")){
		    				try {  
		    					if(type.trim().equalsIgnoreCase("MMG")){
		    						chap = cls.getDeclaredField(type.trim()+"_orginEmailSent");
		    						chap.setAccessible(true);
			    					obj=chap.get(dspDetails);	
			    					if(obj!=null && !resendEmailList.contains("MMG")){
			    						dateTemp=(Date)obj;
			    						 nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dateTemp));
			    						 dateTemp1=nowYYYYMMDD1.toString();
			    						dateTemp = dateformatYYYYMMDD1.parse(dateTemp1);
			    						 c = Calendar.getInstance();
			    						c.setTime(dateTemp);
			    						long StartTime1=c.getTimeInMillis();
			    						long diff=StartTime-StartTime1;
			    						long diffDays = diff / (24 * 60 * 60 * 1000);
			    						if((diffDays)>=1){
			    							if(resendEmailList.equalsIgnoreCase("")){
			    								resendEmailList=type;
			    							}else{
			    								resendEmailList=resendEmailList+","+type;
			    							}
			    						}
			    					}			    			
			    					chap1 = cls.getDeclaredField(type.trim()+"_destinationEmailSent");
			    					chap1.setAccessible(true);
			    					obj1=chap1.get(dspDetails);
			    					if(obj1!=null && !resendEmailList.contains("MMG")){
			    						dateTemp=(Date)obj1;
			    						 nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dateTemp));
			    						 dateTemp1=nowYYYYMMDD1.toString();
			    						dateTemp = dateformatYYYYMMDD1.parse(dateTemp1);
			    						 c = Calendar.getInstance();
			    						c.setTime(dateTemp);
			    						long StartTime1=c.getTimeInMillis();
			    						long diff=StartTime-StartTime1;
			    						long diffDays = diff / (24 * 60 * 60 * 1000);
			    						if((diffDays)>=1){
			    							if(resendEmailList.equalsIgnoreCase("")){
			    								resendEmailList=type;
			    							}else{
			    								resendEmailList=resendEmailList+","+type;
			    							}
			    						}else{
			    							resendEmailList=resendEmailList.replace("MMG,", "");
			    							resendEmailList=resendEmailList.replace(",MMG", "");
			    							resendEmailList=resendEmailList.replace("MMG", "");
			    						}
			    					}			    					
		    					}else{
		    						chap = cls.getDeclaredField(type.trim()+"_emailSent");
			    					chap.setAccessible(true);
			    					obj=chap.get(dspDetails);
			    					if(obj!=null){
			    						dateTemp=(Date)obj;
			    						 nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dateTemp));
			    						 dateTemp1=nowYYYYMMDD1.toString();
			    						dateTemp = dateformatYYYYMMDD1.parse(dateTemp1);
			    						 c = Calendar.getInstance();
			    						c.setTime(dateTemp);
			    						long StartTime1=c.getTimeInMillis();
			    						long diff=StartTime-StartTime1;
			    						long diffDays = diff / (24 * 60 * 60 * 1000);
			    						if((diffDays)>=1){
			    							if(resendEmailList.equalsIgnoreCase("")){
			    								resendEmailList=type;
			    							}else{
			    								resendEmailList=resendEmailList+","+type;
			    							}
			    						}
			    					}		    						
		    					}
		    				}catch (Exception e) {
			    				e.printStackTrace();
			    			}
				}
			}catch(Exception e){e.printStackTrace();}
			}	    	
	    	String serviceType=serviceOrder.getServiceType();
	    	if(serviceOrder.getIsNetworkRecord()){
	    		
	    	}else{
	    	if(serviceType.indexOf("SET")>-1)
	    	{
	    		if((dspDetails.getSET_vendorCode()==null)||(dspDetails.getSET_vendorCode().equalsIgnoreCase("")))
	    		{
	    			String setRecord=dspDetailsManager.getListRecord(serviceOrder.getJob(),sessionCorpID);
	    			if(!setRecord.equalsIgnoreCase(""))
	    			{
	    				String []arr=setRecord.split("~");
	    				dspDetails.setSET_vendorCode(arr[0]);
	    				dspDetails.setSET_vendorName(arr[1]);
	    				dspDetails.setSET_vendorEmail(arr[2]);
	    				dspDetails.setSET_vendorContact(arr[3]);
	    			}
	    		}
	    	}
	    	if(serviceType.indexOf("ONG")>-1)
	    	{
	    		String setRecord=dspDetailsManager.getListRecord(serviceOrder.getJob(),sessionCorpID);
	    		if((dspDetails.getONG_vendorCode()==null)||(dspDetails.getONG_vendorCode().equalsIgnoreCase("")))
	    		{
	    			if(!setRecord.equalsIgnoreCase(""))
	    			{
	    				String []arr=setRecord.split("~");
	    				dspDetails.setONG_vendorCode(arr[0]);
	    				dspDetails.setONG_vendorName(arr[1]);
	    				dspDetails.setONG_vendorEmail(arr[2]);
	    				dspDetails.setONG_vendorContact(arr[3]);
	    			}
	    		}
	    	}
	    	}
	    	//serviceMandatoryFile
	    	try{
	    	String[] emailArr=serviceType.split(",");
	    	Set<String> fileList=new HashSet<String>();
	    	Set<String> tempfileList=null;
		 	   for(int k=0;k<emailArr.length;k++){
		 		   tempfileList=serviceMandatoryFile.get(emailArr[k]);
		 		   if(tempfileList!=null && !tempfileList.isEmpty()){
		 			   for (String s : tempfileList) {
		 		   		fileList.add(s);
		 			   }
		 		   }
		 	   }
		 	  serviceCompleteDateVal="";
		 	   if(fileList!=null && !fileList.isEmpty()){
		 		  serviceCompleteDateVal="N";
		 		 Set<String> myFileList=new HashSet<String>();
		 		 List<MyFile> myList = myFileManager.getAllMyfile(dspDetails.getShipNumber(), sessionCorpID);
		 		if(myList!=null && !myList.isEmpty()){
		 		 for(MyFile myFile:myList){
		 			myFileList.add(myFile.getFileType());
		 		 }
		 		}
		 		for(String myFile:fileList){
		 			if(!myFileList.contains(myFile)){
		 				serviceCompleteDateVal+=","+myFile;
		 			}
		 		 }
		 	   }
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	if((dspDetails.getRNT_utilitiesIncluded()!=null)&&(!dspDetails.getRNT_utilitiesIncluded().equalsIgnoreCase("")))
	    	 {
	    					multiplutilities = new ArrayList();
				String[] ac = dspDetails.getRNT_utilitiesIncluded().split(",");
				int arrayLength2 = ac.length;
				for (int k = 0; k < arrayLength2; k++) {
					String accConJobType = ac[k];
					if(accConJobType!=null && !accConJobType.equalsIgnoreCase("")){
						if (accConJobType.indexOf("(") == 0) {
							accConJobType = accConJobType.substring(1);
						}
						if (accConJobType.lastIndexOf(")") == accConJobType.length() - 1) {
							accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
						}
						if (accConJobType.indexOf("'") == 0) {
							accConJobType = accConJobType.substring(1);
						}
						if (accConJobType.lastIndexOf("'") == accConJobType.length() - 1) {
							accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
						}

					multiplutilities.add(accConJobType.trim());
					}
				}
			} 
	    	 if((dspDetails.getTAC_utilitiesIncluded()!=null)&&(!dspDetails.getTAC_utilitiesIncluded().equalsIgnoreCase("")))
	    	 {
	    		multiplutilities1 = new ArrayList();
				String[] ac = dspDetails.getTAC_utilitiesIncluded().split(",");
				int arrayLength2 = ac.length;
				for (int k = 0; k < arrayLength2; k++) {
					String accConJobType = ac[k];
					if(accConJobType!=null && !accConJobType.equalsIgnoreCase("")){
						if (accConJobType.indexOf("(") == 0) {
							accConJobType = accConJobType.substring(1);
						}
						if (accConJobType.lastIndexOf(")") == accConJobType.length() - 1) {
							accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
						}
						if (accConJobType.indexOf("'") == 0) {
							accConJobType = accConJobType.substring(1);
						}
						if (accConJobType.lastIndexOf("'") == accConJobType.length() - 1) {
							accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
						}

					multiplutilities1.add(accConJobType.trim());
					}
				}
			} 
	    	 if(serviceOrder.getServiceType()!=null && !serviceOrder.getServiceType().equals("")){
		    		Class noparams[] = {};
		    		int count=0;
		    		List<String> vendorCode=new ArrayList<String>();
		    		List<String> vendorCode2=new ArrayList<String>();
		    		Class cls = Class.forName("com.trilasoft.app.model.DspDetails");
		    		String abc[]=serviceOrder.getServiceType().split(",");
		    		for (String string : abc) {
		    		Method method=null;
		    		if(string.trim().equalsIgnoreCase("SCH")){
		    			try {
							method = cls.getDeclaredMethod( "get"+string.trim()+"_schoolSelected", noparams);
							} catch (Exception e) {
								e.printStackTrace();
							}
		    		}else{
		    			try {
						method = cls.getDeclaredMethod( "get"+string.trim()+"_vendorCode", noparams);
						} catch (Exception e) {
							e.printStackTrace();
						}
		    		}
						if(method!=null){
						Object obj1 = method.invoke(dspDetails, null);
		    			if(obj1==null){
		    			count ++;	
		    			break;
		    			}else{
		    				String vendorCode1=obj1+"";
		    				vendorCode.add(vendorCode1);
		    			}
						}
					}
		    		Iterator itr = vendorCode.iterator();
		    		while(itr.hasNext()){
		    			String partCode=(String)itr.next();
		    			String isRedSky1="";
		    			if(partCode!=null && !(partCode.equals(""))){
		    			try {
		    				isRedSky1=trackingStatusManager.checkIsRedSkyUser(partCode);
		    			} catch (Exception e) {
		    				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		    			}
		    			/*if((isRedSky1==null || isRedSky1.equalsIgnoreCase("")) &&trackingStatusManager.getAgentsExistInRedSky(partCode,sessionCorpID,contactName,conEmail)){
		    				isRedSky1="AG";
		    			}*/
		    			
		    			vendorCode2.add(partCode+"~"+isRedSky1);
		    			}
		    			isRedSky=vendorCode2.toString().replace("[","").replace("]", "");
		    	}
		    	}
	    	 utilityServiceDetailList = dspDetailsManager.findUtilityServiceDetail(sessionCorpID,id,"");
		} else {
			dspDetails = new DspDetails(); 
			dspDetails.setCreatedOn(new Date());
			dspDetails.setUpdatedOn(new Date());
			dspDetails.setServiceOrderId(serviceOrderId);
			
		}
		if(usertype.equalsIgnoreCase("PARTNER")){
			shipSize = dspDetailsManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			minShip =  dspDetailsManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();	
		}else{
		shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();		
		}
		
		getNotesForIconChange();
		

		
		
		return SUCCESS;
	}
   	private List utilityServiceDetailList = new ArrayList();
   	private String tenancyManagementFlag;
	private String assignees;
	private String employers;
	private String convenant;
	private String dspURL;
	private String tabType;
	public String save() throws Exception {
		String serviceTypeTemp="";
		boolean isNew = (dspDetails.getId() == null);  
				if (isNew) { 
					dspDetails.setCreatedOn(new Date());
				}else{
		        	dspURL="?id="+dspDetails.getId();
		        }
				if(buttonType==null){
					buttonType="";
				}
				trackingStatus=trackingStatusManager.get(id);
				serviceOrder=serviceOrderManager.get(id);
				getRequest().setAttribute("soLastName", serviceOrder.getLastName());
		    	customerFile = serviceOrder.getCustomerFile();
		    	miscellaneous = miscellaneousManager.get(id); 
		    	billing=billingManager.get(id);
		    	try{
			   		 isNetwork=false;
			   		if(serviceOrder.getIsNetworkRecord()==null){
			   			isNetwork=false;
			   		}
			   		else if(serviceOrder.getIsNetworkRecord()){
			   			isNetwork=true;
			   		}
			   		
			   		isNetworkBookingAgent=getIsNetworkBookingAgent(sessionCorpID, serviceOrder.getBookingAgentCode());
			   		billingContractType=trackingStatusManager.getContractType(sessionCorpID ,billing.getContract());
			    	}catch(Exception e ){e.printStackTrace();}
		    	dspDetails.setCorpID(sessionCorpID);
		    	dspDetails.setUpdatedOn(new Date());
		    	dspDetails.setUpdatedBy(getRequest().getRemoteUser());
		    	dspDetails.setShipNumber(serviceOrder.getShipNumber());
	    		String serviceDoc=assignees.trim().toString();
	    		if(serviceDoc.equals("")||serviceDoc.equals("#"))
	    		{	    	  	  }	    	  	 
	    		else {
	    	  		serviceDoc=serviceDoc.trim();
	    	  	    if(serviceDoc.indexOf("#")==0){
	    	  	    	serviceDoc=serviceDoc.substring(1);
	    	  		 }
	    	  		if(serviceDoc.lastIndexOf("#")==serviceDoc.length()-1){
	    	  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
	    	  		 } 
	    	  	  } 
	    		serviceDoc=serviceDoc.replaceAll("#", ","); 
	    		dspDetails.setVIS_assignees(serviceDoc);
	    		serviceDoc=employers.trim().toString();
	    		if(serviceDoc.equals("")||serviceDoc.equals("#"))
	    		{	    	  	  }	    	  	 
	    		else
	    	  	  {
	    	  		serviceDoc=serviceDoc.trim();
	    	  	    if(serviceDoc.indexOf("#")==0)
	    	  		 {
	    	  	    	serviceDoc=serviceDoc.substring(1);
	    	  		 }
	    	  		if(serviceDoc.lastIndexOf("#")==serviceDoc.length()-1)
	    	  		 {
	    	  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
	    	  		 } 
	    	  	  } 
	    		serviceDoc=serviceDoc.replaceAll("#", ","); 
	    		dspDetails.setVIS_employers(serviceDoc);

	    		serviceDoc=convenant.trim().toString();
	    		if(serviceDoc.equals("")||serviceDoc.equals("#"))
	    		{	    	  	  
	    		}else {
	    	  		serviceDoc=serviceDoc.trim();
	    	  	    if(serviceDoc.indexOf("#")==0){
	    	  	    	serviceDoc=serviceDoc.substring(1);
	    	  		 }
	    	  		if(serviceDoc.lastIndexOf("#")==serviceDoc.length()-1){
	    	  			serviceDoc=serviceDoc.substring(0, serviceDoc.length()-1);
	    	  		 } 
	    	  	  } 
	    		serviceDoc=serviceDoc.replaceAll("#", ","); 
	    		dspDetails.setVIS_convenant(serviceDoc);
	    		try{
		 			List<String> exSoList=new ArrayList();
		 			exSoList.add(dspDetails.getCAR_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getCOL_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getTRG_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getHOM_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getRNT_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getLAN_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getMMG_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getONG_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getPRV_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getAIO_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getEXP_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getRPT_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getSCH_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getTAX_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getTAC_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getTEN_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getVIS_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getSET_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getWOP_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getREP_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getRLS_vendorCodeEXSO());

		 			exSoList.add(dspDetails.getCAT_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getCLS_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getCHS_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getDPS_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getHSM_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getPDT_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getRCP_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getSPA_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getTCS_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getMTS_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getDSS_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getHOB_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getFLB_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getFRL_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getAPU_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getINS_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getINP_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getEDA_vendorCodeEXSO());
		 			exSoList.add(dspDetails.getTAS_vendorCodeEXSO());
		 			
		 			exSoList.add(trackingStatusNetworkAgentExSO);
		 			exSoList.add(trackingStatusBookingAgentExSO);
		 			
		 			List<String> list = new ArrayList<String>(new HashSet<String>(exSoList));
		 			list.remove(null);list.remove("");
					List<String> misMatchList = new ArrayList();
					for (String ship : list) {
						if(list.contains(ship) && list.contains("remove"+ship)){
							misMatchList.add(ship);
							misMatchList.add("remove"+ship);
						}
					}
					list.removeAll(misMatchList);
					checkRemoveLinks(list,trackingStatus);
					
		 			}catch(Exception ex){
		 				ex.printStackTrace();
		 			}
		 			try{	
		 	     		if(dspDetails.getCAR_vendorCodeEXSO()!=null && dspDetails.getCAR_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setCAR_vendorCodeEXSO("");
		 	     		if(dspDetails.getCOL_vendorCodeEXSO()!=null && dspDetails.getCOL_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setCOL_vendorCodeEXSO("");
		 	     		if(dspDetails.getTRG_vendorCodeEXSO()!=null && dspDetails.getTRG_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setTRG_vendorCodeEXSO("");
		 	     		if(dspDetails.getHOM_vendorCodeEXSO()!=null && dspDetails.getHOM_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setHOM_vendorCodeEXSO("");
		 	     		if(dspDetails.getRNT_vendorCodeEXSO()!=null && dspDetails.getRNT_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setRNT_vendorCodeEXSO("");
		 	     		if(dspDetails.getLAN_vendorCodeEXSO()!=null && dspDetails.getLAN_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setLAN_vendorCodeEXSO("");
		 	     		if(dspDetails.getMMG_vendorCodeEXSO()!=null && dspDetails.getMMG_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setMMG_vendorCodeEXSO("");
		 	     		if(dspDetails.getONG_vendorCodeEXSO()!=null && dspDetails.getONG_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setONG_vendorCodeEXSO("");
		 	     		if(dspDetails.getPRV_vendorCodeEXSO()!=null && dspDetails.getPRV_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setPRV_vendorCodeEXSO("");
		 	     		if(dspDetails.getAIO_vendorCodeEXSO()!=null && dspDetails.getAIO_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setAIO_vendorCodeEXSO("");
		 	     		if(dspDetails.getEXP_vendorCodeEXSO()!=null && dspDetails.getEXP_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setEXP_vendorCodeEXSO("");
		 	     		if(dspDetails.getRPT_vendorCodeEXSO()!=null && dspDetails.getRPT_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setRPT_vendorCodeEXSO("");
		 	     		if(dspDetails.getSCH_vendorCodeEXSO()!=null && dspDetails.getSCH_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setSCH_vendorCodeEXSO("");
		 	     		if(dspDetails.getTAX_vendorCodeEXSO()!=null && dspDetails.getTAX_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setTAX_vendorCodeEXSO("");
		 	     		if(dspDetails.getTAC_vendorCodeEXSO()!=null && dspDetails.getTAC_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setTAC_vendorCodeEXSO("");
		 	     		if(dspDetails.getTEN_vendorCodeEXSO()!=null && dspDetails.getTEN_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setTEN_vendorCodeEXSO("");
		 	     		if(dspDetails.getVIS_vendorCodeEXSO()!=null && dspDetails.getVIS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setVIS_vendorCodeEXSO("");
		 	     		if(dspDetails.getWOP_vendorCodeEXSO()!=null && dspDetails.getWOP_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setWOP_vendorCodeEXSO("");
		 	     		if(dspDetails.getREP_vendorCodeEXSO()!=null && dspDetails.getREP_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setREP_vendorCodeEXSO("");
		 	     		if(dspDetails.getSET_vendorCodeEXSO()!=null && dspDetails.getSET_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setSET_vendorCodeEXSO("");
		 	     		if(dspDetails.getRLS_vendorCodeEXSO()!=null && dspDetails.getRLS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setRLS_vendorCodeEXSO("");
		 	     		
		 	     		if(dspDetails.getCAT_vendorCodeEXSO()!=null && dspDetails.getCAT_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setCAT_vendorCodeEXSO("");
		 	     		if(dspDetails.getCLS_vendorCodeEXSO()!=null && dspDetails.getCLS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setCLS_vendorCodeEXSO("");
		 	     		if(dspDetails.getCHS_vendorCodeEXSO()!=null && dspDetails.getCHS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setCHS_vendorCodeEXSO("");
		 	     		if(dspDetails.getDPS_vendorCodeEXSO()!=null && dspDetails.getDPS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setDPS_vendorCodeEXSO("");
		 	     		if(dspDetails.getHSM_vendorCodeEXSO()!=null && dspDetails.getHSM_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setHSM_vendorCodeEXSO("");
		 	     		if(dspDetails.getPDT_vendorCodeEXSO()!=null && dspDetails.getPDT_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setPDT_vendorCodeEXSO("");
		 	     		if(dspDetails.getRCP_vendorCodeEXSO()!=null && dspDetails.getRCP_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setRCP_vendorCodeEXSO("");
		 	     		if(dspDetails.getSPA_vendorCodeEXSO()!=null && dspDetails.getSPA_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setSPA_vendorCodeEXSO("");
		 	     		if(dspDetails.getTCS_vendorCodeEXSO()!=null && dspDetails.getTCS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setTCS_vendorCodeEXSO("");		 	     		
		 	     		if(dspDetails.getMTS_vendorCodeEXSO()!=null && dspDetails.getMTS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setMTS_vendorCodeEXSO("");
		 	     		if(dspDetails.getDSS_vendorCodeEXSO()!=null && dspDetails.getDSS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setDSS_vendorCodeEXSO("");
		 	     		if(dspDetails.getHOB_vendorCodeEXSO()!=null && dspDetails.getHOB_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setHOB_vendorCodeEXSO("");
		 	     		if(dspDetails.getFLB_vendorCodeEXSO()!=null && dspDetails.getFLB_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setFLB_vendorCodeEXSO("");		 	     		
		 	     		if(dspDetails.getFRL_vendorCodeEXSO()!=null && dspDetails.getFRL_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setFRL_vendorCodeEXSO("");
		 	     		if(dspDetails.getAPU_vendorCodeEXSO()!=null && dspDetails.getAPU_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setAPU_vendorCodeEXSO("");
		 	     		if(dspDetails.getINS_vendorCodeEXSO()!=null && dspDetails.getINS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setINS_vendorCodeEXSO("");
		 	     		if(dspDetails.getINP_vendorCodeEXSO()!=null && dspDetails.getINP_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setINP_vendorCodeEXSO("");
		 	     		if(dspDetails.getEDA_vendorCodeEXSO()!=null && dspDetails.getEDA_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setEDA_vendorCodeEXSO("");
		 	     		if(dspDetails.getTAS_vendorCodeEXSO()!=null && dspDetails.getTAS_vendorCodeEXSO().toString().contains("remove"))
		 	     			dspDetails.setTAS_vendorCodeEXSO("");
		 	     		
		 	     		if(trackingStatusNetworkAgentExSO!=null && trackingStatusNetworkAgentExSO.toString().contains("remove")){
		 	     			try{
		 	     			trackingStatusManager.updateNetworkflagDSP(id);
		 	     			trackingStatus.setNetworkAgentExSO("");
		 	     		    trackingStatus.setSoNetworkGroup(false);
		 	     		    trackingStatus.setAccNetworkGroup(false);
		 	     		    trackingStatus.setUtsiNetworkGroup(false);
		 	     		    trackingStatus.setAgentNetworkGroup(false);
		 	     			}catch(Exception ex){
		 	     				
		 	     			}
		 	     		}
		 			}catch(Exception ex){
						ex.printStackTrace();
					}
		 			if((tenancyManagementFlag!=null && !tenancyManagementFlag.equalsIgnoreCase("")) && tenancyManagementFlag.equalsIgnoreCase("true")){
		 				
		 				BigDecimal rentAmount = dspDetails.getTEN_rentAmount();
		 				BigDecimal rentAllowance = dspDetails.getTEN_rentAllowance();
		 				int res;
		 				res = rentAmount.compareTo(rentAllowance);
		 				if( res == -1 ){
		 					dspDetails.setTEN_toBePaidBy("");
		 					dspDetails.setTEN_contributionAmount(new BigDecimal(0.00));
		 					dspDetails.setTEN_directDebit(false);
		 					dspDetails.setTEN_assigneeContributionCurrency("");
		 					dspDetails.setTEN_IBAN_BankAccountNumber("");
		 					dspDetails.setTEN_BIC_SWIFT("");
		 					dspDetails.setTEN_description("");
		 				}
		 				
		 				if((dspDetails.getTEN_utilitiesIncluded()==null || dspDetails.getTEN_utilitiesIncluded().equalsIgnoreCase("")) || dspDetails.getTEN_utilitiesIncluded().equalsIgnoreCase("No")){
		 					dspDetails.setTEN_Gas_Water(false);
		 					dspDetails.setTEN_Gas(false);
		 					dspDetails.setTEN_Electricity(false);
		 					dspDetails.setTEN_TV_Internet_Phone(false);
		 					dspDetails.setTEN_mobilePhone(false);
		 					dspDetails.setTEN_furnitureRental(false);
		 					dspDetails.setTEN_cleaningServices(false);
		 					dspDetails.setTEN_parkingPermit(false);
		 					dspDetails.setTEN_communityTax(false);
		 					dspDetails.setTEN_insurance(false);
		 					dspDetails.setTEN_gardenMaintenance(false);
		 					dspDetails.setTEN_Miscellaneous(false);
		 					dspDetails.setTEN_Gas_Electric(false);
		 					
		 					dspDetails.setTEN_Utility_Gas_Water(false);
		 					dspDetails.setTEN_Utility_Gas(false);
		 					dspDetails.setTEN_Utility_Electricity(false);
		 					dspDetails.setTEN_Utility_TV_Internet_Phone(false);
		 					dspDetails.setTEN_Utility_mobilePhone(false);
		 					dspDetails.setTEN_Utility_furnitureRental(false);
		 					dspDetails.setTEN_Utility_cleaningServices(false);
		 					dspDetails.setTEN_Utility_parkingPermit(false);
		 					dspDetails.setTEN_Utility_communityTax(false);
		 					dspDetails.setTEN_Utility_insurance(false);
		 					dspDetails.setTEN_Utility_gardenMaintenance(false);
		 					dspDetails.setTEN_Utility_Miscellaneous(false);
		 					dspDetails.setTEN_Utility_Gas_Electric(false);
		 				}
		 				
		 			}
		    	  dspDetails=dspDetailsManager.save(dspDetails);
		    	  
		    	  if((dspDetails.getTEN_utilitiesIncluded()==null || dspDetails.getTEN_utilitiesIncluded().equalsIgnoreCase("")) || dspDetails.getTEN_utilitiesIncluded().equalsIgnoreCase("No")){  
			    	  List<TenancyUtilityService> utilityServiceDetail = dspDetailsManager.findUtilityServiceDetail(sessionCorpID,dspDetails.getId(),"");
						if(utilityServiceDetail!=null && !utilityServiceDetail.isEmpty()){
							for(TenancyUtilityService utilityService:utilityServiceDetail){
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_Gas_Water")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_Gas_Water",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_Gas")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_Gas",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_Electricity")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_Electricity",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_TV_Internet_Phone")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_TV_Internet_Phone",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_mobilePhone")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_mobilePhone",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_furnitureRental")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_furnitureRental",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_cleaningServices")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_cleaningServices",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_parkingPermit")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_parkingPermit",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_communityTax")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_communityTax",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_insurance")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_insurance",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_gardenMaintenance")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_gardenMaintenance",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_Miscellaneous")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_Miscellaneous",true,getRequest().getRemoteUser());
		 						}
		 						if(utilityService.getParentFieldName().equalsIgnoreCase("TEN_Utility_Gas_Electric")){
		 							dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,dspDetails.getId(),"TEN_Utility_Gas_Electric",true,getRequest().getRemoteUser());
		 						}
							}
						}
		    	  }
		    	  
		    	  try {
		    		  if(usertype !=null && usertype.equalsIgnoreCase("USER")){
					  serviceOrder.setCAR_vendorCode(dspDetails.getCAR_vendorCode());
					  serviceOrder.setCOL_vendorCode(dspDetails.getCOL_vendorCode());
					  serviceOrder.setTRG_vendorCode(dspDetails.getTRG_vendorCode());
					  serviceOrder.setHOM_vendorCode(dspDetails.getHOM_vendorCode());
					  serviceOrder.setRNT_vendorCode(dspDetails.getRNT_vendorCode());
					  serviceOrder.setSET_vendorCode(dspDetails.getSET_vendorCode());
					  serviceOrder.setLAN_vendorCode(dspDetails.getLAN_vendorCode());
					  serviceOrder.setMMG_vendorCode(dspDetails.getMMG_vendorCode());
					  serviceOrder.setONG_vendorCode(dspDetails.getONG_vendorCode());
					  serviceOrder.setPRV_vendorCode(dspDetails.getPRV_vendorCode());
					  serviceOrder.setAIO_vendorCode(dspDetails.getAIO_vendorCode());
					  serviceOrder.setEXP_vendorCode(dspDetails.getEXP_vendorCode());
					  serviceOrder.setRPT_vendorCode(dspDetails.getRPT_vendorCode());
					  serviceOrder.setSCH_schoolSelected(dspDetails.getSCH_schoolSelected());
					  serviceOrder.setTAX_vendorCode(dspDetails.getTAX_vendorCode());
					  serviceOrder.setTAC_vendorCode(dspDetails.getTAC_vendorCode());
					  serviceOrder.setTEN_vendorCode(dspDetails.getTEN_vendorCode());
					  serviceOrder.setVIS_vendorCode(dspDetails.getVIS_vendorCode());
					  serviceOrder.setWOP_vendorCode(dspDetails.getWOP_vendorCode());
					  serviceOrder.setREP_vendorCode(dspDetails.getREP_vendorCode());
					  serviceOrder.setRLS_vendorCode(dspDetails.getRLS_vendorCode());
					  
					  serviceOrder.setCAT_vendorCode(dspDetails.getCAT_vendorCode());
					  serviceOrder.setCLS_vendorCode(dspDetails.getCLS_vendorCode());
					  serviceOrder.setCHS_vendorCode(dspDetails.getCHS_vendorCode());
					  serviceOrder.setDPS_vendorCode(dspDetails.getDPS_vendorCode());
					  serviceOrder.setHSM_vendorCode(dspDetails.getHSM_vendorCode());
					  serviceOrder.setPDT_vendorCode(dspDetails.getPDT_vendorCode());
					  serviceOrder.setRCP_vendorCode(dspDetails.getRCP_vendorCode());
					  serviceOrder.setSPA_vendorCode(dspDetails.getSPA_vendorCode());
					  serviceOrder.setTCS_vendorCode(dspDetails.getTCS_vendorCode());
					  serviceOrder.setMTS_vendorCode(dspDetails.getMTS_vendorCode());
					  serviceOrder.setDSS_vendorCode(dspDetails.getDSS_vendorCode());
					  serviceOrder.setHOB_vendorCode(dspDetails.getHOB_vendorCode());
					  serviceOrder.setFLB_vendorCode(dspDetails.getFLB_vendorCode());
					  serviceOrder.setFRL_vendorCode(dspDetails.getFRL_vendorCode());
					  serviceOrder.setAPU_vendorCode(dspDetails.getAPU_vendorCode());
					  serviceOrder.setINS_vendorCode(dspDetails.getINS_vendorCode());
					  serviceOrder.setINP_vendorCode(dspDetails.getINP_vendorCode());
					  serviceOrder.setEDA_vendorCode(dspDetails.getEDA_vendorCode());
					  serviceOrder.setTAS_vendorCode(dspDetails.getTAS_vendorCode());
					  serviceOrder=serviceOrderManager.save(serviceOrder);
		    		  }
					  serviceTypeTemp=serviceOrder.getServiceType();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					logger.warn("Exception: "+currentdate+" ## : "+e1.toString());
				}
		    	  
		    	  
		    	  
		    	String serviceOrderStatus="";
		    	int StatusNumber=1;
		    	if(((!serviceOrder.getStatus().equalsIgnoreCase("CLSD")) && (! serviceOrder.getStatus().equalsIgnoreCase("TRNS")) && (!serviceOrder.getStatus().equalsIgnoreCase("2CLS")) && (!serviceOrder.getStatus().equalsIgnoreCase("CNCL"))) || (dspDetails.getCAR_serviceStartDate()!=null|| dspDetails.getCOL_serviceStartDate()!=null || dspDetails.getTRG_serviceStartDate()!=null || dspDetails.getHOM_serviceStartDate()!=null || dspDetails.getRNT_serviceStartDate()!=null || dspDetails.getSET_serviceStartDate()!=null  || dspDetails.getLAN_serviceStartDate()!=null || dspDetails.getMMG_serviceStartDate()!=null|| dspDetails.getONG_serviceStartDate()!=null || dspDetails.getPRV_serviceStartDate()!=null || dspDetails.getAIO_serviceStartDate()!=null || dspDetails.getEXP_serviceStartDate()!=null || dspDetails.getRPT_serviceStartDate()!=null || dspDetails.getSCH_serviceStartDate()!=null || dspDetails.getTAC_serviceStartDate()!=null || dspDetails.getTEN_serviceStartDate()!=null || dspDetails.getVIS_serviceStartDate()!=null || dspDetails.getWOP_serviceStartDate()!=null || dspDetails.getREP_serviceStartDate()!=null || dspDetails.getRLS_serviceStartDate()!=null || dspDetails.getCAT_serviceStartDate()!=null || dspDetails.getCLS_serviceStartDate()!=null || dspDetails.getCHS_serviceStartDate()!=null || dspDetails.getDPS_serviceStartDate()!=null || dspDetails.getHSM_serviceStartDate()!=null || dspDetails.getPDT_serviceStartDate()!=null || dspDetails.getRCP_serviceStartDate()!=null || dspDetails.getSPA_serviceStartDate()!=null || dspDetails.getTCS_serviceStartDate()!=null || dspDetails.getMTS_serviceStartDate()!=null || dspDetails.getFRL_serviceStartDate()!=null || dspDetails.getAPU_serviceStartDate()!=null || dspDetails.getDSS_serviceStartDate()!=null || dspDetails.getHOB_serviceStartDate()!=null || dspDetails.getFLB_serviceStartDate()!=null || dspDetails.getINS_serviceStartDate()!=null || dspDetails.getINP_serviceStartDate()!=null || dspDetails.getEDA_serviceStartDate()!=null || dspDetails.getTAS_serviceStartDate()!=null || dspDetails.getTAX_serviceStartDate()!=null) ){
		    		serviceOrderStatus="TRNS";
		    		StatusNumber=30;
		    	}
		    if(dspDetails.getServiceCompleteDate()==null){
		    	int count=0;
		    	Class noparams[] = {};
		    	TreeSet<Date> dsDate=new TreeSet<Date>();
		    	 SimpleDateFormat df = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		    	if(serviceOrder.getServiceType()!=null && !serviceOrder.getServiceType().equals("") && serviceOrderStatus.equals("")){
		    		Class cls = Class.forName("com.trilasoft.app.model.DspDetails");
		    		String abc[]=serviceOrder.getServiceType().split(",");
		    		for (String string : abc) {
		    		Method method=null;
		    			try {
						method = cls.getDeclaredMethod( "get"+string.trim()+"_serviceEndDate", noparams);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							//method = cls.getDeclaredMethod( "is"+string.trim()+"_serviceEndDate", noparams);
							e.printStackTrace();
						}
		    			Object obj1 = method.invoke(dspDetails, null);
		    			if(obj1==null){
		    			count ++;	
		    			break;
		    			}else{
		    				String date=obj1+"";
		    				
		    				dsDate.add(df.parse(date));
		    			}
					}
		    		if(count==0 && !dsDate.isEmpty() && dsDate.size()!=0 ){
		    			 
		    		  SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		    		  Date latestdate =dsDate.pollLast();
		    		  String strDate=df1.format(latestdate);
		    		  Date dd=df1.parse(strDate);
		    		// String  latestdate11=df11.format(df11.parse(latestdate.toString()));
		    		  if(serviceCompleteDateVal!=null && !serviceCompleteDateVal.equalsIgnoreCase("") && !serviceCompleteDateVal.equalsIgnoreCase("N")){
		    			  if(tabType==null || tabType.equalsIgnoreCase("")){
					    	String msg = "Please upload ("+serviceCompleteDateVal.toString().replaceAll("N,","")+") documents and proceed to fill the Service Complete Date.";
						    errorMessage(msg);		    		
		    			  }
				    		dspDetails.setServiceCompleteDate(null);		    			  
		    		  }else{
		    			  dspDetails.setServiceCompleteDate(dd);
		    		  }
		    		  dspDetails=dspDetailsManager.save(dspDetails);
		    		 // dspDetailsManager.updateServiceCompleteDate(dspDetails.getId(),dspDetails.getShipNumber(),strDate,sessionCorpID);
		    		}
		    	}
		    	}
		       if(dspDetails.getServiceCompleteDate()!=null &&  billing.getBillComplete()!=null && !serviceOrder.getStatus().equalsIgnoreCase("CLSD") && !serviceOrder.getStatus().equalsIgnoreCase("CNCL")){
    			serviceOrderStatus="2CLS";	
    			StatusNumber=500;
    		   }
		    	if(!serviceOrderStatus.equals("")){
		    		dspDetailsManager.updateServiceOrderStatus(serviceOrder.getId(),serviceOrder.getShipNumber(),serviceOrderStatus,sessionCorpID,getRequest().getRemoteUser(),StatusNumber);
		    	}
		    	try{
		    	
		    	if(serviceOrderStatus !=null && (!(serviceOrderStatus.equals(""))) ){
		    		//Bug 11839 - Possibility to modify Current status to Prior status
		    		//serviceOrder.setStatus(serviceOrderStatus);
		    		//serviceOrder.setStatusNumber(StatusNumber);
		    		//serviceOrder.setStatusDate(new Date());
		    	}
		    	}catch(Exception e){
		    	e.printStackTrace();	
		    	}
		    	 if((dspDetails.getRNT_utilitiesIncluded()!=null)&&(!dspDetails.getRNT_utilitiesIncluded().equalsIgnoreCase("")))
		    	 {
		    		multiplutilities = new ArrayList();
					String[] ac = dspDetails.getRNT_utilitiesIncluded().split(",");
					int arrayLength2 = ac.length;
					for (int k = 0; k < arrayLength2; k++) {
						String accConJobType = ac[k];
						if(accConJobType!=null && !accConJobType.equalsIgnoreCase("")){
							if (accConJobType.indexOf("(") == 0) {
								accConJobType = accConJobType.substring(1);
							}
							if (accConJobType.lastIndexOf(")") == accConJobType.length() - 1) {
								accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
							}
							if (accConJobType.indexOf("'") == 0) {
								accConJobType = accConJobType.substring(1);
							}
							if (accConJobType.lastIndexOf("'") == accConJobType.length() - 1) {
								accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
							}

						multiplutilities.add(accConJobType.trim());
						}
					}
					dspDetails.setRNT_utilitiesIncluded(multiplutilities.toString().replace("[", "").replace("]", ""));
				} 
		    	 if((dspDetails.getTAC_utilitiesIncluded()!=null)&&(!dspDetails.getTAC_utilitiesIncluded().equalsIgnoreCase("")))
		    	 {
		    		multiplutilities1 = new ArrayList();
					String[] ac = dspDetails.getTAC_utilitiesIncluded().split(",");
					int arrayLength2 = ac.length;
					for (int k = 0; k < arrayLength2; k++) {
						String accConJobType = ac[k];
						if(accConJobType!=null && !accConJobType.equalsIgnoreCase("")){
							if (accConJobType.indexOf("(") == 0) {
								accConJobType = accConJobType.substring(1);
							}
							if (accConJobType.lastIndexOf(")") == accConJobType.length() - 1) {
								accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
							}
							if (accConJobType.indexOf("'") == 0) {
								accConJobType = accConJobType.substring(1);
							}
							if (accConJobType.lastIndexOf("'") == accConJobType.length() - 1) {
								accConJobType = accConJobType.substring(0, accConJobType.length() - 1);
							}

						multiplutilities1.add(accConJobType.trim());
						}
					}
					dspDetails.setTAC_utilitiesIncluded(multiplutilities1.toString().replace("[", "").replace("]", ""));
				} 
		    	 
		    	 try{  
		 			Boolean isNetworkedCustomerFile=getIsNetworkedCustomerFile(customerFile.getId());
		 			
		 			if(buttonType.equalsIgnoreCase("dspDetailsCAR_vendorCode")){
		 				isNetworkCAR_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCAR_vendorCode() );
		 				if(isNetworkCAR_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getCAR_vendorCode() );
		 						createExternaleEntriesCAR_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesCAR_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsRLS_vendorCode")){
		 				isNetworkRLS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getRLS_vendorCode() );
		 				if(isNetworkRLS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getRLS_vendorCode() );
		 						createExternaleEntriesRLS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesRLS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsCAT_vendorCode")){
		 				isNetworkCAT_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCAT_vendorCode() );
		 				if(isNetworkCAT_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getCAT_vendorCode() );
		 						createExternaleEntriesCAT_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesCAT_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}
	 				else if(buttonType.equalsIgnoreCase("dspDetailsCLS_vendorCode")){
		 				isNetworkCLS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCLS_vendorCode() );
		 				if(isNetworkCLS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getCLS_vendorCode() );
		 						createExternaleEntriesCLS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesCLS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}
	 				else if(buttonType.equalsIgnoreCase("dspDetailsCHS_vendorCode")){
		 				isNetworkCHS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCHS_vendorCode() );
		 				if(isNetworkCHS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getCHS_vendorCode() );
		 						createExternaleEntriesCHS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesCHS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}		 			
	 				else if(buttonType.equalsIgnoreCase("dspDetailsDPS_vendorCode")){
		 				isNetworkDPS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getDPS_vendorCode() );
		 				if(isNetworkDPS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getDPS_vendorCode() );
		 						createExternaleEntriesDPS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesDPS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}
	 				else if(buttonType.equalsIgnoreCase("dspDetailsHSM_vendorCode")){
		 				isNetworkHSM_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getHSM_vendorCode() );
		 				if(isNetworkHSM_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getHSM_vendorCode() );
		 						createExternaleEntriesHSM_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesHSM_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}
	 				else if(buttonType.equalsIgnoreCase("dspDetailsPDT_vendorCode")){
		 				isNetworkPDT_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getPDT_vendorCode() );
		 				if(isNetworkPDT_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getPDT_vendorCode() );
		 						createExternaleEntriesPDT_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesPDT_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}
	 				else if(buttonType.equalsIgnoreCase("dspDetailsRCP_vendorCode")){
		 				isNetworkRCP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getRCP_vendorCode() );
		 				if(isNetworkRCP_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getRCP_vendorCode() );
		 						createExternaleEntriesRCP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesRCP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}
	 				else if(buttonType.equalsIgnoreCase("dspDetailsSPA_vendorCode")){
		 				isNetworkSPA_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getSPA_vendorCode() );
		 				if(isNetworkSPA_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getSPA_vendorCode() );
		 						createExternaleEntriesSPA_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesSPA_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}
	 				else if(buttonType.equalsIgnoreCase("dspDetailsTCS_vendorCode")){
		 				isNetworkTCS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTCS_vendorCode() );
		 				if(isNetworkTCS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getTCS_vendorCode() );
		 						createExternaleEntriesTCS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesTCS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsMTS_vendorCode")){
		 				isNetworkMTS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getMTS_vendorCode() );
		 				if(isNetworkMTS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getMTS_vendorCode() );
		 						createExternaleEntriesMTS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesMTS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsDSS_vendorCode")){
		 				isNetworkDSS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getDSS_vendorCode() );
		 				if(isNetworkDSS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getDSS_vendorCode() );
		 						createExternaleEntriesDSS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesDSS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsCOL_vendorCode")){
		 				isNetworkCOL_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCOL_vendorCode() );
		 				if(isNetworkCOL_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getCOL_vendorCode() );
		 						createExternaleEntriesCOL_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesCOL_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsTRG_vendorCode")){
		 				isNetworkTRG_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTRG_vendorCode() );
		 				if(isNetworkTRG_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getTRG_vendorCode() );
		 						createExternaleEntriesTRG_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesTRG_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsHOM_vendorCode")){
		 				isNetworkHOM_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getHOM_vendorCode() );
		 				if(isNetworkHOM_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getHOM_vendorCode() );
		 						createExternaleEntriesHOM_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesHOM_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsRNT_vendorCode")){
		 				isNetworkRNT_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getRNT_vendorCode() );
		 				if(isNetworkRNT_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getRNT_vendorCode() );
		 						createExternaleEntriesRNT_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesRNT_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsLAN_vendorCode")){
		 				isNetworkLAN_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getLAN_vendorCode() );
		 				if(isNetworkLAN_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getLAN_vendorCode() );
		 						createExternaleEntriesLAN_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesLAN_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsMMG_vendorCode")){
		 				isNetworkMMG_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getMMG_vendorCode() );
		 				if(isNetworkMMG_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getMMG_vendorCode() );
		 						createExternaleEntriesMMG_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesMMG_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsONG_vendorCode")){
		 				isNetworkONG_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getONG_vendorCode() );
		 				if(isNetworkONG_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getONG_vendorCode() );
		 						createExternaleEntriesONG_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesONG_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsPRV_vendorCode")){
		 				isNetworkPRV_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getPRV_vendorCode() );
		 				if(isNetworkPRV_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getPRV_vendorCode() );
		 						createExternaleEntriesPRV_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesPRV_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
		 			}else if(buttonType.equalsIgnoreCase("dspDetailsAIO_vendorCode")){
		 				isNetworkAIO_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getAIO_vendorCode() );
		 				if(isNetworkAIO_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getAIO_vendorCode() );
		 						createExternaleEntriesAIO_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesAIO_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
		 			}else if(buttonType.equalsIgnoreCase("dspDetailsEXP_vendorCode")){
		 				isNetworkEXP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getEXP_vendorCode() );
		 				if(isNetworkEXP_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getEXP_vendorCode() );
		 						createExternaleEntriesEXP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesEXP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
		 			}else if(buttonType.equalsIgnoreCase("dspDetailsRPT_vendorCode")){
		 				isNetworkRPT_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getRPT_vendorCode() );
		 				if(isNetworkRPT_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getRPT_vendorCode() );
		 						createExternaleEntriesRPT_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesRPT_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
		 			}else if(buttonType.equalsIgnoreCase("dspDetailsTAX_vendorCode")){
		 				isNetworkTAX_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTAX_vendorCode() );
		 				if(isNetworkTAX_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getTAX_vendorCode() );
		 						createExternaleEntriesTAX_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesTAX_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsTAC_vendorCode")){
		 				isNetworkTAC_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTAC_vendorCode() );
		 				if(isNetworkTAC_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getTAC_vendorCode() );
		 						createExternaleEntriesTAC_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesTAC_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
		 			}else if(buttonType.equalsIgnoreCase("dspDetailsTEN_vendorCode")){
		 				isNetworkTEN_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTEN_vendorCode() );
		 				if(isNetworkTEN_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getTEN_vendorCode() );
		 						createExternaleEntriesTEN_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesTEN_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
		 			}else if(buttonType.equalsIgnoreCase("dspDetailsVIS_vendorCode")){
		 				isNetworkVIS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getVIS_vendorCode() );
		 				if(isNetworkVIS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getVIS_vendorCode() );
		 						createExternaleEntriesVIS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesVIS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsWOP_vendorCode")){
		 				isNetworkWOP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getWOP_vendorCode() );
		 				if(isNetworkWOP_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getWOP_vendorCode() );
		 						createExternaleEntriesWOP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesWOP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsREP_vendorCode")){
		 				isNetworkREP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getREP_vendorCode() );
		 				if(isNetworkREP_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getREP_vendorCode() );
		 						createExternaleEntriesREP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesREP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsSET_vendorCode")){
		 				isNetworkSET_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getVIS_vendorCode() );
		 				if(isNetworkSET_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getSET_vendorCode() );
		 						createExternaleEntriesSET_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesSET_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsSCH_schoolSelected")){
		 				isNetworkSCH_schoolSelected=getIsNetworkAgent(sessionCorpID,dspDetails.getSCH_schoolSelected() );
		 				if(isNetworkSCH_schoolSelected){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getSCH_schoolSelected() );
		 						createExternaleEntriesSCH_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesSCH_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
		 			}else if(buttonType.equalsIgnoreCase("dspDetailsFRL_vendorCode")){
		 				isNetworkFRL_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getFRL_vendorCode() );
		 				if(isNetworkFRL_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getFRL_vendorCode() );
		 						createExternaleEntriesFRL_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesFRL_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}   
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsAPU_vendorCode")){
		 				isNetworkAPU_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getAPU_vendorCode() );
		 				if(isNetworkAPU_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getAPU_vendorCode() );
		 						createExternaleEntriesAPU_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesAPU_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsINS_vendorCode")){
		 				isNetworkINS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getINS_vendorCode() );
		 				if(isNetworkINS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getINS_vendorCode() );
		 						createExternaleEntriesINS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesINS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsINP_vendorCode")){
		 				isNetworkINP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getINP_vendorCode() );
		 				if(isNetworkINP_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getINP_vendorCode() );
		 						createExternaleEntriesINP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesINP_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsEDA_vendorCode")){
		 				isNetworkEDA_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getEDA_vendorCode() );
		 				if(isNetworkEDA_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getEDA_vendorCode() );
		 						createExternaleEntriesEDA_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesEDA_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}else if(buttonType.equalsIgnoreCase("dspDetailsTAS_vendorCode")){
		 				isNetworkTAS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTAS_vendorCode() );
		 				if(isNetworkTAS_vendorCode){
		 					if(isNetworkedCustomerFile){
		 						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),dspDetails.getTAS_vendorCode() );
		 						createExternaleEntriesTAS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);	
		 					}else{
		 						createExternaleEntriesTAS_vendorCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);
		 					}
		 				}
	 				}
		 			String serviceType=serviceOrder.getServiceType();
			    	if(!buttonType.equalsIgnoreCase("LinkUpNetworkGroup") && !buttonType.equalsIgnoreCase("LinkUpBookingAgent") )
			    	{
			    		if(usertype !=null && usertype.equalsIgnoreCase("USER")){
			    		trackingStatus=trackingStatusManager.get(id);
		 				trackingStatus.setNetworkPartnerCode(trackingStatusNetworkPartnerCode); 
		 				trackingStatus.setNetworkPartnerName(trackingStatusNetworkPartnerName);
		 				trackingStatus.setBookingAgentContact(trackingStatusBookingAgentContact);
		 				trackingStatus.setNetworkContact(trackingStatusNetworkContact);
		 				trackingStatus.setBookingAgentEmail(trackingStatusBookingAgentEmail);
		 				trackingStatus.setNetworkEmail (trackingStatusNetworkEmail);
		 				trackingStatus.setBookingAgentExSO(trackingStatusBookingAgentExSO); 
		 				trackingStatus.setNetworkAgentExSO(trackingStatusNetworkAgentExSO);	
		 				trackingStatus=trackingStatusManager.save(trackingStatus); 
			    		}
			    	}
		 			boolean contractType=trackingStatusManager.getContractType(sessionCorpID ,billing.getContract());
		 			if((buttonType.equalsIgnoreCase("LinkUpNetworkGroup"))){
		 				if(usertype !=null && usertype.equalsIgnoreCase("USER")){
		 				trackingStatus=trackingStatusManager.get(id);
		 				trackingStatus.setNetworkPartnerCode(trackingStatusNetworkPartnerCode); 
		 				trackingStatus.setNetworkPartnerName(trackingStatusNetworkPartnerName);
		 				trackingStatus.setBookingAgentContact(trackingStatusBookingAgentContact);
		 				trackingStatus.setNetworkContact(trackingStatusNetworkContact);
		 				trackingStatus.setBookingAgentEmail(trackingStatusBookingAgentEmail);
		 				trackingStatus.setNetworkEmail (trackingStatusNetworkEmail);
		 				trackingStatus.setBookingAgentExSO(trackingStatusBookingAgentExSO); 
		 				trackingStatus.setNetworkAgentExSO(trackingStatusNetworkAgentExSO);
		 				if(contractType){
			 				trackingStatus.setSoNetworkGroup(true);
							trackingStatus.setAgentNetworkGroup(false);
							if(networkAgent){
								trackingStatus.setAccNetworkGroup(false);	
								trackingStatus.setUtsiNetworkGroup(true);
							}else{
								trackingStatus.setAccNetworkGroup(true);
								trackingStatus.setUtsiNetworkGroup(false);
							}
		 				}
						trackingStatus=trackingStatusManager.save(trackingStatus); 
		 				isNetworkNetworkPartnerCode=getIsNetworkAgent(sessionCorpID,trackingStatus.getNetworkPartnerCode() );
						if(isNetworkNetworkPartnerCode){
							if(isNetworkedCustomerFile){
								String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),trackingStatus.getNetworkPartnerCode() );
								createExternaleEntriesNetworkPartnerCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);
							}else{
								createExternaleEntriesNetworkPartnerCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);	
							}
						} 
					}
		 			}
		 			
		 			if((buttonType.equalsIgnoreCase("LinkUpBookingAgent")) && networkAgent ){
		 				if(usertype !=null && usertype.equalsIgnoreCase("USER")){
		 				trackingStatus=trackingStatusManager.get(id);
		 				trackingStatus.setNetworkPartnerCode(trackingStatusNetworkPartnerCode); 
		 				trackingStatus.setNetworkPartnerName(trackingStatusNetworkPartnerName);
		 				trackingStatus.setBookingAgentContact(trackingStatusBookingAgentContact);
		 				trackingStatus.setNetworkContact(trackingStatusNetworkContact);
		 				trackingStatus.setBookingAgentEmail(trackingStatusBookingAgentEmail);
		 				trackingStatus.setNetworkEmail (trackingStatusNetworkEmail);
		 				trackingStatus.setBookingAgentExSO(trackingStatusBookingAgentExSO); 
		 				trackingStatus.setNetworkAgentExSO(trackingStatusNetworkAgentExSO);
		 				if(contractType){
		 				trackingStatus.setSoNetworkGroup(true);
						trackingStatus.setAgentNetworkGroup(false);
						trackingStatus.setAccNetworkGroup(false);	
					    trackingStatus.setUtsiNetworkGroup(true);
		 				}
					    trackingStatus=trackingStatusManager.save(trackingStatus); 
		 				isNetworkBookingAgentCode=getIsNetworkAgent(sessionCorpID,serviceOrder.getBookingAgentCode() );
						if(isNetworkBookingAgentCode){
							if(isNetworkedCustomerFile){
								String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),serviceOrder.getBookingAgentCode() );
								createExternaleEntriesBookingAgentCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile,dspDetails);
							}else{
								createExternaleEntriesBookingAgentCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile,dspDetails);	
							}
						} 
					}
		 			}
		 	     if(trackingStatusBookingAgentExSO!=null && trackingStatusBookingAgentExSO.toString().contains("remove")){
		 	    	if(usertype !=null && usertype.equalsIgnoreCase("USER")){
		 	    	trackingStatus=trackingStatusManager.get(id);
	 				trackingStatus.setNetworkPartnerCode(trackingStatusNetworkPartnerCode); 
	 				trackingStatus.setNetworkPartnerName(trackingStatusNetworkPartnerName);
	 				trackingStatus.setBookingAgentContact(trackingStatusBookingAgentContact);
	 				trackingStatus.setNetworkContact(trackingStatusNetworkContact);
	 				trackingStatus.setBookingAgentEmail(trackingStatusBookingAgentEmail);
	 				trackingStatus.setNetworkEmail (trackingStatusNetworkEmail);
	 				trackingStatus.setBookingAgentExSO(""); 
	 				trackingStatus.setNetworkAgentExSO(trackingStatusNetworkAgentExSO);	
	 				trackingStatus=trackingStatusManager.save(trackingStatus);  
		 	    	}
		 	    }
		 	     if(trackingStatusNetworkAgentExSO!=null && trackingStatusNetworkAgentExSO.toString().contains("remove")){
		 	    	if(usertype !=null && usertype.equalsIgnoreCase("USER")){
		 	    	trackingStatus=trackingStatusManager.get(id);
	 				trackingStatus.setNetworkPartnerCode(trackingStatusNetworkPartnerCode); 
	 				trackingStatus.setNetworkPartnerName(trackingStatusNetworkPartnerName);
	 				trackingStatus.setBookingAgentContact(trackingStatusBookingAgentContact);
	 				trackingStatus.setNetworkContact(trackingStatusNetworkContact);
	 				trackingStatus.setBookingAgentEmail(trackingStatusBookingAgentEmail);
	 				trackingStatus.setNetworkEmail (trackingStatusNetworkEmail);
	 				trackingStatus.setBookingAgentExSO(trackingStatusBookingAgentExSO); 
	 				trackingStatus.setNetworkAgentExSO("");	
	 				trackingStatus=trackingStatusManager.save(trackingStatus); 
		 	    	}
		 	    }
		    	 }catch(Exception e){e.printStackTrace();}
		    	 
		    	 if(!buttonType.equalsIgnoreCase("dspDetailsCAR_vendorCode")&& !buttonType.equalsIgnoreCase("dspDetailsCOL_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsTRG_vendorCode")&& !buttonType.equalsIgnoreCase("dspDetailsHOM_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsRNT_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsLAN_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsMMG_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsONG_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsPRV_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsAIO_vendorCode")&& !buttonType.equalsIgnoreCase("dspDetailsEXP_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsRPT_vendorCode")&& !buttonType.equalsIgnoreCase("dspDetailsTAX_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsTAC_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsTEN_vendorCode")&& !buttonType.equalsIgnoreCase("dspDetailsVIS_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsRLS_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsCAT_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsCLS_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsCHS_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsDPS_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsHSM_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsPDT_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsRCP_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsSPA_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsTCS_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsMTS_vendorCode")  && !buttonType.equalsIgnoreCase("dspDetailsDSS_vendorCode") && !buttonType.equalsIgnoreCase("dspDetailsSET_vendorCode")&& !buttonType.equalsIgnoreCase("dspDetailsSCH_schoolSelected") && !buttonType.equalsIgnoreCase("LinkUpNetworkGroup") && !buttonType.equalsIgnoreCase("LinkUpBookingAgent")){
		 			try{
			 			if(serviceOrder.getIsNetworkRecord()){
			 				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			 				List<Object> dspDetailsRecords=findDspDetailsRecords(linkedShipNumber,serviceOrder);
			 				Map<String, List> reloServiceFieldMap=refMasterManager.findReloServiceFieldMap(serviceOrder.getServiceType(), serviceOrder.getCorpID());
			 				synchornizeDspDetails(dspDetailsRecords,dspDetails,reloServiceFieldMap);
			 				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder); 
							synchornizeServiceOrder(serviceOrderRecords,serviceOrder,trackingStatus);
							 
			 			}
		 			}catch(Exception ex){
		 				ex.printStackTrace();
		 			}
		 		}
		    	 		    	 
		    	if(!(validateFormNav=="OK")){
		    	String key = (isNew) ? "dspDetails.added" : "dspDetails.updated";
			    saveMessage(getText(key));
		    	}
		    	
		    	if(usertype.equals("ACCOUNT") && serviceOrder.getJob().equals("RLO") && sessionCorpID.equals("VOER")){
		    		try{
		    			String emailTo = "";
		    			List coordEmail = customerFileManager.findCoordEmailAddress(serviceOrder.getCoordinator());
		    			if (!(coordEmail == null) && (!coordEmail.isEmpty())) {
		    				emailTo = coordEmail.get(0).toString().trim();
		    			}
		    			User localUser = userManager.getUserByUsername(getRequest().getRemoteUser());
		    			User coordinatorName = userManager.getUserByUsername(serviceOrder.getCoordinator());
		    			sendMail(emailTo,coordinatorName.getFirstName() + " " + coordinatorName.getLastName(),serviceOrder.getFirstName() + " " + serviceOrder.getLastName(),serviceOrder.getShipNumber(),localUser.getEmail());
		    		}catch(Exception e){
		    			e.printStackTrace();
		    		}
		    	}
		    	
		    	//# 8955 - To change the Sales status of the Relo job type files. Start
		    	if(dspDetails.getId()!=null){
		    		if(serviceTypeTemp!=null&&!serviceTypeTemp.equalsIgnoreCase("")){
		    			Boolean flag=false;
		    			Class<?> cls = dspDetails.getClass();
		    			Field chap = null;
		    			Object obj=null;
		    			for(String serviceT:serviceTypeTemp.split(",")){
		    				try {  
		    					chap = cls.getDeclaredField(serviceT.trim()+"_serviceStartDate");
		    					chap.setAccessible(true);
		    					obj=chap.get(dspDetails);
		    					if(obj!=null){
		    						flag=true;
		    					}
		    				}catch (Exception e) {
			    				e.printStackTrace();
			    			}
		    			}
			    		if(flag){
			    			Class<?> cls1 = serviceOrder.getCustomerFile().getClass(); 
			    			chap = null;
			    			try {
		    					chap = cls1.getDeclaredField("status");
		    					chap.setAccessible(true);
		    					obj=chap.get(serviceOrder.getCustomerFile());
			    				if((obj==null)||((!obj.toString().equalsIgnoreCase("CNCL"))&&(!obj.toString().equalsIgnoreCase("CLSD")))){
			    					chap = cls1.getDeclaredField("salesStatus");
			    					chap.setAccessible(true);
			    					chap.set(serviceOrder.getCustomerFile(), "Booked");
			    					customerFileManager.save(serviceOrder.getCustomerFile());
			    				}
							} catch (Exception e) {e.printStackTrace();
							}
			    		}
		    		}
		    	}
		    	//# 8955 - To change the Sales status of the Relo job type files. End		    	
		    	/*if(serviceOrder.getServiceType()!=null && !serviceOrder.getServiceType().equals("") && serviceOrderStatus.equals("")){
		    		Class noparams[] = {};
		    		int count=0;
		    		List<String> vendorCode=new ArrayList<String>();
		    		Class cls = Class.forName("com.trilasoft.app.model.DspDetails");
		    		String abc[]=serviceOrder.getServiceType().split(",");
		    		for (String string : abc) {
		    		Method method=null;
		    			try {
						method = cls.getDeclaredMethod( "get"+string.trim()+"_vendorCode", noparams);
						} catch (Exception e) {
							e.printStackTrace();
						}
						Object obj1 = method.invoke(dspDetails, null);
		    			if(obj1==null){
		    			count ++;	
		    			break;
		    			}else{
		    				String vendorCode1=obj1+"";
		    				vendorCode.add(vendorCode1);
		    			}
					}
		    		Iterator itr = vendorCode.iterator();
		    		while(itr.hasNext()){
		    			String partCode=(String)itr.next();
		    			try {
		    				isRedSky=trackingStatusManager.checkIsRedSkyUser(partCode);
		    			} catch (Exception e) {
		    				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		    			}
		    			if(trackingStatusManager.getAgentsExistInRedSky(partCode,sessionCorpID,contactName,conEmail)){
		    				isRedSky="AG";
		    		}
		    	}
		    		 isRedSky=isRedSky+"";
		    	}*/	
		    	if(tabType!=null && !tabType.equals("")){
		    		if(tabType.equals("SERVICEORDER")){
						dspURL="?id="+id;
						return "SERVICEORDER";
					}else if(tabType.equals("BILLING")){
						dspURL="?id="+id;
						return "BILLING";
					}else if(tabType.equals("ACCOUNTLINE")){
						dspURL="?sid="+id;
						return "ACCOUNTLINE";
					}else if(tabType.equals("CUSTOMERFILE")){
						dspURL="?id="+customerFile.getId();
						return "CUSTOMERFILE";
					}else if(tabType.equals("COSTING")){
						dspURL="?sid="+id;
						return "COSTING";
					}else if(tabType.equals("DOCUMENT")){
						dspURL="?sid="+id;
						return "DOCUMENT";
					}else if(tabType.equals("SUMMARY")){
						dspURL="?id="+id;
						return "SUMMARY";
					}else if(tabType.equals("DOCUMENT")){
						dspURL="?sid="+id+"&seqNum="+serviceOrder.getSequenceNumber();
						return "DOCUMENT";
					}
				}
				return SUCCESS;
	}
	
	private void checkRemoveLinks(List<String> exSoList, TrackingStatus trackingStatus) {
		for (String exSo : exSoList) {
    		try{
	    		if(exSo.contains("remove")){
	    			trackingStatusManager.removeExternalLink(0L,exSo,getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());	
	    		}
    		}catch(Exception ex){
    			ex.printStackTrace();
    		}
    	}

}
	private String networkVendorCode;
	private String preFixCode;
	private String validationString;
	@SkipValidation
	public String checkNetworkPartnerServiceType(){
		String moveTypeCheck=getRequest().getParameter("moveTypeCheck");
		if((moveTypeCheck==null)||(moveTypeCheck.trim().equalsIgnoreCase(""))){
			moveTypeCheck="N";
		}
		validationString=dspDetailsManager.checkNetworkPartnerServiceType(sessionCorpID, networkVendorCode,preFixCode,moveTypeCheck);	
		return SUCCESS;
	}
	@SkipValidation
	public String checkMoveTypeAgent(){
		validationString=dspDetailsManager.checkMoveTypeAgent(sessionCorpID, networkVendorCode);	
		return SUCCESS;
	}	
	private void synchornizeDspDetails(List<Object> dspDetailsRecords,DspDetails dspDetails,Map<String, List> reloServiceFieldMap) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Iterator  it=dspDetailsRecords.iterator();
		Class<?> cls = dspDetails.getClass(); 
		Field chap = null;
    	while(it.hasNext()){
    		DspDetails dspDetailsToRecods=(DspDetails)it.next();
    		try{
    			if(!(dspDetails.getShipNumber().equals(dspDetailsToRecods.getShipNumber()))){
    				Class<?> cls1 = dspDetailsToRecods.getClass(); 
    				Field chap1 = null;
    				for(Map.Entry<String, List> reloMap:reloServiceFieldMap.entrySet()){
    					List eachTypeFieldList=reloMap.getValue();
    					for(Object object: eachTypeFieldList){
			    			try {
		    					chap = cls.getDeclaredField(object+"".trim());
		    					chap.setAccessible(true);
		    					chap1 = cls1.getDeclaredField(object+"".trim());
		    					chap1.setAccessible(true);
		    					chap1.set(dspDetailsToRecods, chap.get(dspDetails));
							} catch (Exception e) {e.printStackTrace();
							}
    						}
    				}
    				dspDetailsToRecods.setUpdatedBy(dspDetails.getCorpID()+":"+getRequest().getRemoteUser()); 
    				dspDetailsToRecods.setUpdatedOn(new Date());	
    				dspDetailsToRecods.setMailServiceType(dspDetails.getMailServiceType());
    				dspDetailsToRecods.setServiceCompleteDate(dspDetails.getServiceCompleteDate());
    				dspDetailsToRecods=dspDetailsManager.save(dspDetailsToRecods);
    			}	
    		}catch(Exception ex){
    			ex.printStackTrace();
    		}
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
	}
	  private List findLinkerShipNumber(ServiceOrder serviceOrder) {
      	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
      	List linkedShipNumberList= new ArrayList();
      	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
      		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
      	}else{
      		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
      	}
      	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
      	return linkedShipNumberList;
      }
	  
	   private List<Object> findDspDetailsRecords(List linkedShipNumber,	ServiceOrder serviceOrder) {
       	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
       	
       	List<Object> recordList= new ArrayList();
       	Iterator it =linkedShipNumber.iterator();
       	try{
       	while(it.hasNext()){
       		String shipNumber= it.next().toString();
       		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
       		//System.out.println("\n\n\n\n serviceOrderRemote----> "+serviceOrderRemote.getId()+"--"+serviceOrderRemote.getCorpID());
       		DspDetails dspDetailsRemote=dspDetailsManager.getForOtherCorpid(serviceOrderRemote.getId());
       		recordList.add(dspDetailsRemote);
       	}
        }catch(Exception e){
       		e.printStackTrace();
       	}
       	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
       	
       	return recordList;
       	}
	   
	   @SkipValidation
       public List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
       	List<Object> recordList= new ArrayList();
       	Iterator it =linkedShipNumber.iterator();
       	while(it.hasNext()){
       		String shipNumber= it.next().toString();
       		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
       		recordList.add(serviceOrderRemote);
       	}
       	return recordList;
       	}
	   @SkipValidation
	    public void synchornizeServiceOrder(List<Object> records, ServiceOrder serviceOrder,TrackingStatus trackingStatus) {
			try {
				String fieldType="";
				String uniqueFieldType="";
				 
				Iterator  it=records.iterator();
				while(it.hasNext()){
					ServiceOrder serviceOrderToRecods=(ServiceOrder)it.next();
					String soJobType = serviceOrderToRecods.getJob();
					if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
					TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
					if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
						uniqueFieldType=trackingStatusToRecods.getContractType();
					if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
						fieldType="CMM/DMM";
					}
					List fieldToSync=customerFileManager.findFieldToSync("ServiceOrder",fieldType,uniqueFieldType);
					fieldType="";
				    uniqueFieldType=""; 
					Iterator it1=fieldToSync.iterator();
					while(it1.hasNext()){
						String field=it1.next().toString();
						String[] splitField=field.split("~");
						String fieldFrom=splitField[0];
						String fieldTo=splitField[1];
						try{
							//BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
							//beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
							PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
							try{
							beanUtilsBean.setProperty(serviceOrderToRecods,fieldTo.trim(),beanUtilsBean.getProperty(serviceOrder, fieldFrom.trim()));
							}catch(Exception ex){
								ex.printStackTrace();
							}
							
						}catch(Exception ex){
							ex.printStackTrace();
						}
						
					}
					try{
				    if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
				    if(serviceOrder.getJob()!=null && (!(serviceOrder.getJob().toString().equals(""))) && (serviceOrder.getJob().toString().equalsIgnoreCase("RLO")) ) {
				    	String newServicesInLinkOrder="";
				    	if(serviceOrderToRecods.getServiceType()!=null && !serviceOrderToRecods.getServiceType().equalsIgnoreCase("")){
				    		String newServicesInLinkOrderTemp=serviceOrderToRecods.getServiceType();
				    		List al=dspDetailsManager.findAllServicesForRelo(serviceOrder.getCorpID());
				    		if(al!=null && !al.isEmpty() && al.get(0)!=null){
				    			if(newServicesInLinkOrderTemp.indexOf(",")>-1){
				    				for(String ss:newServicesInLinkOrderTemp.split(",")){
										if(!al.contains(ss)){
											if(newServicesInLinkOrder.equalsIgnoreCase("")){
												newServicesInLinkOrder=ss;
											}else{
												newServicesInLinkOrder=newServicesInLinkOrder+","+ss;
											}
										}
									}			    				
				    			}else{
				    				if(!al.contains(newServicesInLinkOrderTemp)){
				    					newServicesInLinkOrder=newServicesInLinkOrderTemp;
				    				}
				    			}
				    		}
				    	}
				    	
						String serviceTypeTemp=serviceOrder.getServiceType();
						String serviceTypeTemp1=serviceOrder.getServiceType();
						if(!newServicesInLinkOrder.equalsIgnoreCase("")){
							serviceTypeTemp1=serviceTypeTemp+","+newServicesInLinkOrder;
						}
						if(serviceOrder.getServiceType()!=null && !serviceOrder.getServiceType().equalsIgnoreCase("")){
							List al=dspDetailsManager.findAllServicesForRelo(serviceOrderToRecods.getCorpID());
							serviceTypeTemp="";
							if(al!=null && !al.isEmpty() && al.get(0)!=null){
								for(String ss:serviceTypeTemp1.split(",")){
									if(al.contains(ss)){
										if(serviceTypeTemp.equalsIgnoreCase("")){
											serviceTypeTemp=ss;
										}else{
											if(serviceTypeTemp.indexOf(ss)<0){
												serviceTypeTemp=serviceTypeTemp+","+ss;
											}
										}
									}
								}
							}
						}				    	
					    	serviceOrderToRecods.setServiceType(serviceTypeTemp);	
					}
					serviceOrderToRecods.setUpdatedBy(serviceOrder.getCorpID()+":"+getRequest().getRemoteUser());
					serviceOrderToRecods.setUpdatedOn(new Date());
				    }}catch( Exception e){
				    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    }
				    try{
				    	if(!networkAgent){
				    	if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
				    		if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
				    		/*List aliasNameList=userManager.findCoordinatorByUsername(serviceOrder.getCoordinator(),sessionCorpID);
							if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
								serviceOrderToRecods.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
							}*/
				    			serviceOrderToRecods.setCoordinator(serviceOrder.getCoordinator());
						}
				    	}
				    	}
				    	if(networkAgent){ 
					    	if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
					    		if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
					    		/*List aliasNameList=userManager.findCoordinatorByNetworkCoordinator(serviceOrder.getCoordinator(),serviceOrderToRecods.getCorpID());
								if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
									serviceOrderToRecods.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
								}*/
					    			serviceOrderToRecods.setCoordinator(serviceOrder.getCoordinator());
				    		}
					    	}
					    	
				    		
				    	}	
				    } catch( Exception e){
				    	e.printStackTrace();
				    }
				    if(serviceOrderToRecods.getCorpID().toString().equalsIgnoreCase("UGWW"))
				    	serviceOrderToRecods.setJob(soJobType);
				    
					 serviceOrderManager.save(serviceOrderToRecods);
				}
				}
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
			}	
	    	}
	  
	private Boolean getIsNetworkAgent(String sessionCorpID,String vendorCode) { 
		Boolean isNetworkAgent=trackingStatusManager.getIsNetworkAgent(sessionCorpID,vendorCode); 
		return isNetworkAgent;
	}
	
	@SkipValidation
	private Boolean getIsNetworkBookingAgent(String sessionCorpID,	String bookingAgentCode) { 
		Boolean isNetworkAgent=trackingStatusManager.getIsNetworkBookingAgent(sessionCorpID,bookingAgentCode); 
		return isNetworkAgent;
	}
	private Boolean getIsNetworkedCustomerFile(Long customerFileId) { 
		return customerFileManager.isNetworkedRecord(customerFileId);
	}
	private String getLinkedCustomerFileNumber(String sequenceNumber,String childAgentCode) {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
   		String linkedCustomerFileNumber="";
    	try{
		List<TrackingStatus> trackingStatusList=trackingStatusManager.getNetworkedTrackingStatus(sequenceNumber);
		for(TrackingStatus trackingStatus:trackingStatusList){
			DspDetails dspDetails=dspDetailsManager.getForOtherCorpid(trackingStatus.getId());
			ServiceOrder serviceOrder=serviceOrderManager.getForOtherCorpid(trackingStatus.getId());
			if(dspDetails.getCAR_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getCAR_vendorCodeEXSO()!=null && !dspDetails.getCAR_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getCAR_vendorCodeEXSO();
			}
			if(dspDetails.getCOL_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getCOL_vendorCodeEXSO()!=null && !dspDetails.getCOL_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getCOL_vendorCodeEXSO();
			}
			if(dspDetails.getTRG_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getTRG_vendorCodeEXSO()!=null && !dspDetails.getTRG_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getTRG_vendorCodeEXSO();
			}
			if(dspDetails.getHOM_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getHOM_vendorCodeEXSO()!=null && !dspDetails.getHOM_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getHOM_vendorCodeEXSO();
			}
			if(dspDetails.getRNT_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getRNT_vendorCodeEXSO()!=null && !dspDetails.getRNT_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getRNT_vendorCodeEXSO();
			}
			if(dspDetails.getLAN_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getLAN_vendorCodeEXSO()!=null && !dspDetails.getLAN_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getLAN_vendorCodeEXSO();
			}
			if(dspDetails.getMMG_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getMMG_vendorCodeEXSO()!=null && !dspDetails.getMMG_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getMMG_vendorCodeEXSO();
			}
			if(dspDetails.getONG_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getONG_vendorCodeEXSO()!=null && !dspDetails.getONG_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getONG_vendorCodeEXSO();
			}
			if(dspDetails.getPRV_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getPRV_vendorCodeEXSO()!=null && !dspDetails.getPRV_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getPRV_vendorCodeEXSO();
			}
			if(dspDetails.getAIO_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getAIO_vendorCodeEXSO()!=null && !dspDetails.getAIO_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getAIO_vendorCodeEXSO();
			}
			if(dspDetails.getEXP_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getEXP_vendorCodeEXSO()!=null && !dspDetails.getEXP_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getEXP_vendorCodeEXSO();
			}
			if(dspDetails.getRPT_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getRPT_vendorCodeEXSO()!=null && !dspDetails.getRPT_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getRPT_vendorCodeEXSO();
			}
			if(dspDetails.getTAX_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getTAX_vendorCodeEXSO()!=null && !dspDetails.getTAX_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getTAX_vendorCodeEXSO();
			}
			if(dspDetails.getTAC_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getTAC_vendorCodeEXSO()!=null && !dspDetails.getTAC_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getTAC_vendorCodeEXSO();
			}
			if(dspDetails.getTEN_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getTEN_vendorCodeEXSO()!=null && !dspDetails.getTEN_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getTEN_vendorCodeEXSO();
			}
			if(dspDetails.getVIS_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getVIS_vendorCodeEXSO()!=null && !dspDetails.getVIS_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getVIS_vendorCodeEXSO();
			}
			if(dspDetails.getWOP_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getWOP_vendorCodeEXSO()!=null && !dspDetails.getWOP_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getWOP_vendorCodeEXSO();
			}
			if(dspDetails.getREP_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getREP_vendorCodeEXSO()!=null && !dspDetails.getREP_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getREP_vendorCodeEXSO();
			}
			if(dspDetails.getSET_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getSET_vendorCodeEXSO()!=null && !dspDetails.getSET_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getSET_vendorCodeEXSO();
			}
			if(dspDetails.getSCH_schoolSelected().equalsIgnoreCase(childAgentCode) && dspDetails.getSCH_vendorCodeEXSO()!=null && !dspDetails.getSCH_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getSCH_vendorCodeEXSO();
			}
			if(dspDetails.getRLS_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getRLS_vendorCodeEXSO()!=null && !dspDetails.getRLS_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getRLS_vendorCodeEXSO();
			}		
			if(dspDetails.getCAT_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getCAT_vendorCodeEXSO()!=null && !dspDetails.getCAT_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getCAT_vendorCodeEXSO();
			}
			if(dspDetails.getCLS_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getCLS_vendorCodeEXSO()!=null && !dspDetails.getCLS_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getCLS_vendorCodeEXSO();
			}
			if(dspDetails.getCHS_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getCHS_vendorCodeEXSO()!=null && !dspDetails.getCHS_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getCHS_vendorCodeEXSO();
			}
			if(dspDetails.getDPS_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getDPS_vendorCodeEXSO()!=null && !dspDetails.getDPS_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getDPS_vendorCodeEXSO();
			}
			if(dspDetails.getHSM_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getHSM_vendorCodeEXSO()!=null && !dspDetails.getHSM_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getHSM_vendorCodeEXSO();
			}
			if(dspDetails.getPDT_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getPDT_vendorCodeEXSO()!=null && !dspDetails.getPDT_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getPDT_vendorCodeEXSO();
			}
			if(dspDetails.getRCP_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getRCP_vendorCodeEXSO()!=null && !dspDetails.getRCP_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getRCP_vendorCodeEXSO();
			}
			if(dspDetails.getSPA_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getSPA_vendorCodeEXSO()!=null && !dspDetails.getSPA_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getSPA_vendorCodeEXSO();
			}
			if(dspDetails.getTCS_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getTCS_vendorCodeEXSO()!=null && !dspDetails.getTCS_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getTCS_vendorCodeEXSO();
			}	
			if(dspDetails.getMTS_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getMTS_vendorCodeEXSO()!=null && !dspDetails.getMTS_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getMTS_vendorCodeEXSO();
			}	
			if(dspDetails.getDSS_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getDSS_vendorCodeEXSO()!=null && !dspDetails.getDSS_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getDSS_vendorCodeEXSO();
			}			
			if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(childAgentCode) && trackingStatus.getBookingAgentExSO()!=null && !trackingStatus.getBookingAgentExSO().equals("")){
				linkedCustomerFileNumber= trackingStatus.getBookingAgentExSO();
			}
			if(dspDetails.getFRL_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getFRL_vendorCodeEXSO()!=null && !dspDetails.getFRL_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getFRL_vendorCodeEXSO();
			}
			if(dspDetails.getAPU_vendorCode().equalsIgnoreCase(childAgentCode) && dspDetails.getAPU_vendorCodeEXSO()!=null && !dspDetails.getAPU_vendorCodeEXSO().equals("")){
				linkedCustomerFileNumber= dspDetails.getAPU_vendorCodeEXSO();
			}						
		}
    	}catch(Exception ex){
    		logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
    	}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return linkedCustomerFileNumber;
	}
	
	private void createExternaleEntriesCAR_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getCAR_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCAR_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CAR_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForCAR_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CAR_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesRLS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getRLS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getRLS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RLS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForRLS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RLS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}	
		
	private void createExternaleEntriesCAT_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getCAT_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCAT_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CAT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForCAT_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CAT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesCLS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getCLS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCLS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CLS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForCLS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CLS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesCHS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getCHS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCHS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CHS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForCHS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CHS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesDPS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getDPS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getDPS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"DPS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForDPS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"DPS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesHSM_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getHSM_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getHSM_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"HSM_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForHSM_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"HSM_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesPDT_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getPDT_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getPDT_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"PDT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForPDT_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"PDT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesRCP_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getRCP_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getRCP_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RCP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForRCP_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RCP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesSPA_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getSPA_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getSPA_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"SPA_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForSPA_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"SPA_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesTCS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getTCS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTCS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TCS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForTCS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TCS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesMTS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getMTS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getMTS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"MTS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForMTS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"MTS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesDSS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getDSS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getDSS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"DSS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForDSS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"DSS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}	
	private void checkRemoveTokenForCAR_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getCAR_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkCAR_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCAR_vendorCode() );
			if(isNetworkCAR_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCAR_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCAR_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CAR_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCAR_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	
	private void checkRemoveTokenForRLS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getRLS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkRLS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getRLS_vendorCode() );
			if(isNetworkRLS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getRLS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getRLS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RLS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getRLS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}	
	
	private void checkRemoveTokenForCAT_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getCAT_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkCAT_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCAT_vendorCode() );
			if(isNetworkCAT_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCAT_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCAT_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CAT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCAT_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForCLS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getCLS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkCLS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCLS_vendorCode() );
			if(isNetworkCLS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCLS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCLS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CLS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCLS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForCHS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getCHS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkCHS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCHS_vendorCode() );
			if(isNetworkCHS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCHS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCHS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"CHS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCHS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForDPS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getDPS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkDPS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getDPS_vendorCode() );
			if(isNetworkDPS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getDPS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getDPS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"DPS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getDPS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForHSM_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getHSM_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkHSM_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getHSM_vendorCode() );
			if(isNetworkHSM_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getHSM_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getHSM_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"HSM_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getHSM_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForPDT_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getPDT_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkPDT_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getPDT_vendorCode() );
			if(isNetworkPDT_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getPDT_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getPDT_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"PDT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getPDT_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForRCP_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getRCP_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkRCP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getRCP_vendorCode() );
			if(isNetworkRCP_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getRCP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getRCP_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RCP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getRCP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForSPA_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getSPA_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkSPA_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getSPA_vendorCode() );
			if(isNetworkSPA_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getSPA_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getSPA_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"SPA_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getSPA_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForTCS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getTCS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkTCS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTCS_vendorCode() );
			if(isNetworkTCS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTCS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTCS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TCS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTCS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForMTS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getMTS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkMTS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getMTS_vendorCode() );
			if(isNetworkMTS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getMTS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getMTS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"MTS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getMTS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}	
	private void checkRemoveTokenForDSS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getDSS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkDSS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getDSS_vendorCode() );
			if(isNetworkDSS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getDSS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getDSS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"DSS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getDSS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}	
	private void createExternaleEntriesCOL_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getCOL_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCOL_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"COL_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForCOL_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"COL_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForCOL_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getCOL_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkCOL_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getCOL_vendorCode() );
			if(isNetworkCOL_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCOL_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getCOL_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"COL_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getCOL_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	
	private void createExternaleEntriesTRG_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getTRG_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTRG_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TRG_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForTRG_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TRG_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForTRG_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getTRG_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkTRG_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTRG_vendorCode() );
			if(isNetworkTRG_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTRG_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTRG_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TRG_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTRG_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesHOM_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getHOM_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getHOM_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"HOM_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForHOM_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"HOM_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForHOM_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getHOM_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkHOM_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getHOM_vendorCode() );
			if(isNetworkHOM_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getHOM_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getHOM_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"HOM_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getHOM_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesRNT_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getRNT_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getRNT_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RNT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForRNT_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RNT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForRNT_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getRNT_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkRNT_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getRNT_vendorCode() );
			if(isNetworkRNT_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getRNT_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getRNT_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RNT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getRNT_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesLAN_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getLAN_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getLAN_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"LAN_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForLAN_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"LAN_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForLAN_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getLAN_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkLAN_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getLAN_vendorCode() );
			if(isNetworkLAN_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getLAN_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getLAN_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"LAN_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getLAN_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesMMG_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getMMG_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getMMG_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"MMG_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForMMG_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"MMG_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForMMG_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getMMG_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkMMG_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getMMG_vendorCode() );
			if(isNetworkMMG_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getMMG_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getMMG_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"MMG_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getMMG_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesONG_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getONG_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getONG_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"ONG_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForONG_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"ONG_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForONG_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getONG_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkONG_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getONG_vendorCode() );
			if(isNetworkONG_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getONG_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getONG_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"ONG_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getONG_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesPRV_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getPRV_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getPRV_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"PRV_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForPRV_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"PRV_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForPRV_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getPRV_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkPRV_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getPRV_vendorCode() );
			if(isNetworkPRV_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getPRV_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getPRV_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"PRV_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getPRV_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesAIO_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getAIO_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getAIO_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"AIO_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForAIO_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"AIO_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForAIO_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getAIO_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkAIO_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getAIO_vendorCode() );
			if(isNetworkAIO_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getAIO_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getAIO_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"AIO_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getAIO_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesEXP_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getEXP_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getEXP_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"EXP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForEXP_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"EXP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForEXP_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getEXP_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkEXP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getEXP_vendorCode() );
			if(isNetworkEXP_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getEXP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getEXP_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"EXP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getEXP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesRPT_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getRPT_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getRPT_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RPT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForRPT_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RPT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForRPT_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getRPT_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkRPT_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getRPT_vendorCode() );
			if(isNetworkRPT_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getRPT_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getRPT_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"RPT_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getRPT_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesTAX_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getTAX_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTAX_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TAX_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForTAX_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TAX_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForTAX_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getTAX_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkTAX_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTAX_vendorCode() );
			if(isNetworkTAX_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTAX_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTAX_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TAX_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTAX_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesTAC_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getTAC_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTAC_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TAC_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForTAC_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TAC_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForTAC_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getTAC_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkTAC_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTAC_vendorCode() );
			if(isNetworkTAC_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTAC_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTAC_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TAC_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTAC_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesTEN_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getTEN_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTEN_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TEN_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForTEN_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TEN_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForTEN_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getTEN_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkTEN_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTEN_vendorCode() );
			if(isNetworkTEN_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTEN_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTEN_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TEN_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTEN_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void createExternaleEntriesVIS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getVIS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getVIS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"VIS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForVIS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"VIS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	
	private void createExternaleEntriesWOP_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getWOP_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getWOP_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"WOP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForWOP_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"WOP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesFRL_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getFRL_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getFRL_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"FRL_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForFRL_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"FRL_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void createExternaleEntriesAPU_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getAPU_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getAPU_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"APU_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForAPU_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"APU_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	
	private void createExternaleEntriesREP_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getREP_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getREP_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"REP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForREP_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"REP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
	}
	
	private void createExternaleEntriesINS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getINS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getINS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"INS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForINS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"INS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
	}
	
	private void checkRemoveTokenForINS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getINS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkINS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getINS_vendorCode() );
			if(isNetworkINS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getINS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getINS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"INS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getINS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
			}
		}
	}
	
	private void createExternaleEntriesINP_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getINP_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getINP_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"INP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForINP_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"INP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
	}
	
	private void checkRemoveTokenForINP_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getINP_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkINP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getINP_vendorCode() );
			if(isNetworkINP_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getINP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getINP_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"INP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getINP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
			}
		}
	}
	
	private void createExternaleEntriesEDA_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getEDA_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getEDA_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"EDA_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForEDA_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"EDA_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
	}
	
	private void checkRemoveTokenForEDA_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getEDA_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkEDA_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getEDA_vendorCode() );
			if(isNetworkEDA_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getEDA_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getEDA_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"EDA_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getEDA_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
			}
		}
	}
	
	private void createExternaleEntriesTAS_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getTAS_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTAS_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TAS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForTAS_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"TAS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
	}
	
	private void checkRemoveTokenForTAS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getTAS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkTAS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getTAS_vendorCode() );
			if(isNetworkTAS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTAS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getTAS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"EDA_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getTAS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
			}
		}
	}
	
	private void checkRemoveTokenForVIS_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getVIS_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkVIS_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getVIS_vendorCode() );
			if(isNetworkVIS_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getVIS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getVIS_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"VIS_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getVIS_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
		}
	}
	private void checkRemoveTokenForWOP_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getWOP_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkWOP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getWOP_vendorCode() );
			if(isNetworkWOP_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getWOP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getWOP_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"WOP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getWOP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForFRL_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getFRL_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkFRL_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getFRL_vendorCode() );
			if(isNetworkFRL_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getFRL_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getFRL_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"FRL_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getFRL_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	private void checkRemoveTokenForAPU_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getAPU_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkAPU_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getAPU_vendorCode() );
			if(isNetworkAPU_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getAPU_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getAPU_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"APU_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getAPU_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	
	private void checkRemoveTokenForREP_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getREP_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkREP_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getREP_vendorCode() );
			if(isNetworkREP_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getREP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getREP_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"REP_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getREP_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	
	private void createExternaleEntriesSET_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getSET_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getSET_vendorCode());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"SET_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForSET_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"SET_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForSET_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getSET_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkSET_vendorCode=getIsNetworkAgent(sessionCorpID,dspDetails.getSET_vendorCode() );
			if(isNetworkSET_vendorCode){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getSET_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getSET_vendorCode());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"SET_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getSET_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	
	private void createExternaleEntriesSCH_vendorCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails  ) {
		Long customerFileId=0L;
		if(dspDetails.getSCH_vendorCodeEXSO().equals("")){
			String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getSCH_schoolSelected());
			customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"SCH_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}else{
			checkRemoveTokenForSCH_vendorCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"SCH_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
		}
		
		
	}
	private void checkRemoveTokenForSCH_vendorCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		if(dspDetails.getSCH_vendorCodeEXSO().contains("remove")){
			Boolean isNetworkSCH_schoolSelected=getIsNetworkAgent(sessionCorpID,dspDetails.getSCH_schoolSelected() );
			if(isNetworkSCH_schoolSelected){
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getSCH_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, dspDetails.getSCH_schoolSelected());
				createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"SCH_vendorCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				trackingStatusManager.removeExternalLink(customerFileId,dspDetails.getSCH_vendorCodeEXSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
				
			}
			
		}
	}
	
	
	   private void createExternaleEntriesBookingAgentCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile,Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails){
       	Long customerFileId=0L;
			if(trackingStatus.getBookingAgentExSO()== null || trackingStatus.getBookingAgentExSO().equals("")){
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, serviceOrder.getBookingAgentCode());
				customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"bookingAgentCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			}else{
				checkRemoveTokenForBookingAgentCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"bookingAgentCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
			} 
       }
	   
		private void checkRemoveTokenForBookingAgentCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
			if(trackingStatus.getBookingAgentExSO().contains("remove")){
				Boolean isNetworkBookingAgentCode=getIsNetworkAgent(sessionCorpID,serviceOrder.getBookingAgentCode() ); 
				if(isNetworkBookingAgentCode){
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getBookingAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, serviceOrder.getBookingAgentCode());
					createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"bookingAgentCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
				}else{
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getBookingAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					
				}
				
			}
		}
		
		  private void createExternaleEntriesNetworkPartnerCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile,Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
				Long customerFileId=0L;
				if(trackingStatus.getBookingAgentExSO()==null || trackingStatus.getNetworkAgentExSO().equals("")){
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode());
					customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"networkPartnerCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
				}else{
					checkRemoveTokenForNetworkPartnerCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"networkPartnerCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
				}
				
			}
			private void checkRemoveTokenForNetworkPartnerCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
				if(trackingStatus.getNetworkAgentExSO().contains("remove")){
					Boolean isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getNetworkPartnerCode() ); 
					if(isNetworkDestinationAgent){
						trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getNetworkAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
						String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode());
						createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"networkPartnerCode", linkedCustomerFileNumber, isNetworkedCustomerFile,dspDetails);
					}else{
						trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getNetworkAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
						
					}
					
				}
			}
	
	private Long createCustomerFile(String externalCorpID,CustomerFile customerFile, ServiceOrder serviceOrder, TrackingStatus trackingStatus, Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile,DspDetails dspDetails) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Long customerFileId=0L;
		 boolean billingCMMContractType=false;
		 boolean billingDMMContractType=false;
		 String fieldType="";
		 String uniqueFieldType="";
		 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
		     billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
		 } 
		 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
		     billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
		 }
		 
		 if(forSection.equalsIgnoreCase("networkPartnerCode") ||forSection.equalsIgnoreCase("bookingAgentCode")){
			 fieldType="CMM/DMM"; 
			 if(billingCMMContractType){
				 uniqueFieldType="CMM"; 
			 }
			 if(billingDMMContractType){
				 uniqueFieldType="DMM"; 
			 }
		 }
		String networkCoordinator=customerFileManager.getNetworkCoordinator(externalCorpID);
		//System.out.println("\n\n\n\n linkedCustomerFileNumber---->"+linkedCustomerFileNumber);
		if(linkedCustomerFileNumber.equalsIgnoreCase("") || linkedCustomerFileNumber.contains("remove")){
		//System.out.println("\n\n\n\n inside blank linkedCustomerFileNumber---->");
		Long remoteCustomerFileId = 0L;
        List linkedSequnceNumberList= new ArrayList();
		if(customerFile.getBookingAgentSequenceNumber()==null || customerFile.getBookingAgentSequenceNumber().equals("") ){
		  linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getSequenceNumber(), "Primary");
	   }else{
		  linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getBookingAgentSequenceNumber(), "Secondary");
	   }  
		Iterator it =linkedSequnceNumberList.iterator();
		while(it.hasNext()){
			String sequenceNumber= it.next().toString();
			if(remoteCustomerFileId.intValue() ==0){
			remoteCustomerFileId=customerFileManager.findNewRemoteCustomerFile(sequenceNumber,externalCorpID);  
			}
		}
		
		if(remoteCustomerFileId.intValue() > 0)	{
			CustomerFile customerFileNew1=customerFileManager.getForOtherCorpid(remoteCustomerFileId);
			customerFileId=customerFileNew1.getId();
			createServiceOrder(customerFileNew1, serviceOrder,trackingStatus,billing,miscellaneous,forSection,customerFile, dspDetails);
		}else{
		List fieldToSync=customerFileManager.findFieldToSyncCreate("CustomerFile",fieldType,uniqueFieldType);
		
		CustomerFile customerFileNew= new CustomerFile();
		try{
			Iterator it1=fieldToSync.iterator();
			while(it1.hasNext()){
				String field=it1.next().toString(); 
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
				beanUtilsBean.setProperty(customerFileNew,fieldTo.trim(),beanUtilsBean.getProperty(customerFile, fieldFrom.trim()));
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			}
		}catch(Exception ex){
			logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
		}
		
		customerFileNew.setId(null);
		if(!(forSection.equalsIgnoreCase("networkPartnerCode"))){
		if(customerFileNew.getComptetive()==null || customerFileNew.getComptetive().trim().equals("")){
			customerFileNew.setComptetive("N");
		}
		}
		try{
			if(customerFileNew.getMoveType()==null || (customerFileNew.getMoveType().toString().trim().equals(""))){
			if(customerFileNew.getControlFlag()==null ||(customerFileNew.getControlFlag().toString().trim().equals("")) || (customerFileNew.getControlFlag().trim().equalsIgnoreCase("Q")  )){
				customerFileNew.setMoveType("Quote");
			}else{
				customerFileNew.setMoveType("BookedMove");
			}
			}
			}catch(Exception e){
				
			}
		customerFileNew.setCorpID(externalCorpID);
		maxSequenceNumber = customerFileManager.findMaximumSequenceNumber(externalCorpID);
		if (maxSequenceNumber.get(0) == null) {
			customerFileNew.setSequenceNumber(externalCorpID + "1000001");
		} else {
			autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(externalCorpID, "")) + 1;
			seqNum = externalCorpID + autoSequenceNumber.toString();
			customerFileNew.setSequenceNumber(seqNum);
		}
		//String bookingAgentCode=trackingStatusManager.getLocalBookingAgentCode(customerFile.getCorpID(),customerFile.getBookingAgentCode() );
		//customerFileNew.setBookingAgentCode(bookingAgentCode);
		customerFileNew.setIsNetworkRecord(true);
		customerFileNew.setBookingAgentSequenceNumber(customerFile.getSequenceNumber());
		customerFileNew.setCreatedBy("Networking");
		customerFileNew.setUpdatedBy("Networking");
		if(customerFileNew.getMoveType() == null || customerFileNew.getMoveType().toString().trim().equals("")){
			customerFileNew.setMoveType("BookedMove");
		}
		customerFileNew.setCreatedOn(new Date());
		customerFileNew.setUpdatedOn(new Date());
		customerFileNew.setSalesStatus("Pending");
		customerFileNew.setIsNetworkGroup(false);
		customerFileNew.setContractType("");
		if(forSection.equalsIgnoreCase("CAR_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getCAR_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("COL_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getCOL_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("TRG_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getTRG_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("HOM_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getHOM_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("RNT_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getRNT_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("LAN_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getLAN_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("MMG_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getMMG_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("ONG_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getONG_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("PRV_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getPRV_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("AIO_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getAIO_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("EXP_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getEXP_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("RPT_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getRPT_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("TAX_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getTAX_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("TAC_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getTAC_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("TEN_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getTEN_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("VIS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getVIS_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("WOP_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getWOP_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("REP_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getREP_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("SET_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getSET_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("SCH_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getSCH_schoolSelected(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("RLS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getRLS_vendorCode(), sessionCorpID,externalCorpID ));
		}	
		if(forSection.equalsIgnoreCase("CAT_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getCAT_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("CLS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getCLS_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("CHS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getCHS_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("DPS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getDPS_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("HSM_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getHSM_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("PDT_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getPDT_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("RCP_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getRCP_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("SPA_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getSPA_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("TCS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getTCS_vendorCode(), sessionCorpID,externalCorpID ));
		}	
		if(forSection.equalsIgnoreCase("MTS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getMTS_vendorCode(), sessionCorpID,externalCorpID ));
		}	
		if(forSection.equalsIgnoreCase("DSS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getDSS_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("FRL_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getFRL_vendorCode(), sessionCorpID,externalCorpID ));
		}		
		if(forSection.equalsIgnoreCase("APU_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getAPU_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("INS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getINS_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("INP_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getINP_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("EDA_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getEDA_vendorCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("TAS_vendorCode")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(dspDetails.getTAS_vendorCode(), sessionCorpID,externalCorpID ));
		}
		
		
		/*if(forSection.equalsIgnoreCase("destinationSub")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getDestinationSubAgentCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("origin")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getOriginAgentCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("originSub")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getOriginSubAgentCode(), sessionCorpID,externalCorpID ));
		}*/
		if(forSection.equalsIgnoreCase("networkPartnerCode")){
			customerFileNew.setIsNetworkGroup(true);
			
			if(billingCMMContractType){
				String[] ParentBillTo=trackingStatusManager.findParentBillTo(customerFile.getBillToCode(), sessionCorpID);
				customerFileNew.setAccountCode(ParentBillTo[0]);
				customerFileNew.setAccountName(ParentBillTo[1]);
				customerFileNew.setContractType("CMM"); 
	        }
	        if(billingDMMContractType){
	        	String[] ParentBillTo=trackingStatusManager.findParentBillTo(customerFile.getAccountCode(), sessionCorpID);
				customerFileNew.setAccountCode(ParentBillTo[0]);
				customerFileNew.setAccountName(ParentBillTo[1]);
	        	customerFileNew.setContractType("DMM");
	        	customerFileNew.setBillToCode(customerFile.getAccountCode());
				customerFileNew.setBillToName(customerFile.getAccountName());
	        	
	        }
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getNetworkPartnerCode(), sessionCorpID,externalCorpID ));
		}
		if(forSection.equalsIgnoreCase("bookingAgentCode")){
				if(billingDMMContractType){
				customerFileNew.setBillToCode(trackingStatus.getNetworkPartnerCode());
				customerFileNew.setBillToName(trackingStatus.getNetworkPartnerName());
				customerFileNew.setAccountCode(customerFile.getBillToCode());
				customerFileNew.setAccountName(customerFile.getBillToName()); 
				}
				if(billingCMMContractType){
					String[] ParentBillTo=trackingStatusManager.findParentBillTo(customerFile.getBillToCode(), sessionCorpID);
					customerFileNew.setAccountCode(ParentBillTo[0]);
					customerFileNew.setAccountName(ParentBillTo[1]); 
			        }
				customerFileNew.setIsNetworkGroup(true);
				if(billingCMMContractType){
					customerFileNew.setContractType("CMM"); 
		        }
		        if(billingDMMContractType){
		        	customerFileNew.setContractType("DMM"); 
		        }
				customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(serviceOrder.getBookingAgentCode(), sessionCorpID,externalCorpID ));
				//customerFileNew.setBookingAgentCode(customerFile.getBookingAgentCode());
			}
		if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
		customerFileNew.setBillToCode(customerFileNew.getBookingAgentCode());
		customerFileNew.setBillToName(customerFileNew.getBookingAgentName());
/*		customerFileNew.setAccountCode(customerFileNew.getBookingAgentCode());
		customerFileNew.setAccountName(customerFileNew.getBookingAgentName());
*/		String companyDivisionNetworkCoordinator=customerFileManager.getCompanyDivisionNetworkCoordinator(customerFileNew.getCompanyDivision(),externalCorpID);
		if(companyDivisionNetworkCoordinator != null && (!(companyDivisionNetworkCoordinator.toString().trim().equals("")))){
			customerFileNew.setCoordinator(companyDivisionNetworkCoordinator);	
		}else{
		customerFileNew.setCoordinator(networkCoordinator);
		}
		customerFileNew.setBillPayMethod("CA");
		}
		if(!networkAgent){
		if(((forSection.equalsIgnoreCase("bookingAgentCode"))) || ((forSection.equalsIgnoreCase("networkPartnerCode")))){
			if(customerFile.getCoordinator()!=null && (!(customerFile.getCoordinator().toString().equals("")))){
			/*List aliasNameList=userManager.findCoordinatorByUsername(customerFile.getCoordinator(),sessionCorpID);
			if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
				customerFileNew.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
			}*/
				customerFileNew.setCoordinator(customerFile.getCoordinator());
		}
		}
		}
		if(networkAgent){
			if(((forSection.equalsIgnoreCase("bookingAgentCode"))) || ((forSection.equalsIgnoreCase("networkPartnerCode")))){
				if(customerFile.getCoordinator()!=null && (!(customerFile.getCoordinator().toString().equals("")))){
				/*List aliasNameList=userManager.findCoordinatorByNetworkCoordinator(customerFile.getCoordinator(),externalCorpID);
				if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
					customerFileNew.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
				}*/
					customerFileNew.setCoordinator(customerFile.getCoordinator());
			}
			}	
		}
		customerFileNew.setSystemDate(new Date());
		customerFileNew.setStatus("NEW");
		customerFileNew.setStatusDate(new Date());
		customerFileNew.setContract("");
		if(forSection.equalsIgnoreCase("networkPartnerCode")||forSection.equalsIgnoreCase("bookingAgentCode")){
			customerFileNew.setContract(customerFile.getContract());
			customerFileNew.setStatus(customerFile.getStatus());
			customerFileNew.setStatusDate(customerFile.getStatusDate());
			customerFileNew.setStatusNumber(customerFile.getStatusNumber());
		}
		
		if(customerFileNew.getBillToCode()!=null && (!(customerFileNew.getBillToCode().equalsIgnoreCase("")))){
			 pricingBillingPaybaleList=accountLineManager.pricingBillingPaybaleName(customerFileNew.getBillToCode(),customerFileNew.getCorpID(),customerFileNew.getJob());
			 if((pricingBillingPaybaleList!=null) && (!(pricingBillingPaybaleList.isEmpty()))){
					String[] str1 = pricingBillingPaybaleList.get(0).toString().split("#");
					if(!(str1[0].equalsIgnoreCase("1"))){
						customerFileNew.setPersonPricing(str1[0]);
						}
					if(!(str1[1].equalsIgnoreCase("1"))){
						customerFileNew.setPersonBilling(str1[1]);
						}
					if(!(str1[2].equalsIgnoreCase("1"))){
						customerFileNew.setPersonPayable(str1[2]);
						}
					if(!(str1[3].equalsIgnoreCase("1"))){
						customerFileNew.setAuditor(str1[3]);
						}
			}
		 }
		
		customerFileNew=customerFileManager.save(customerFileNew);
		customerFileId=customerFileNew.getId();
		createServiceOrder(customerFileNew, serviceOrder,trackingStatus,billing,miscellaneous,forSection, customerFile,dspDetails);
		createFamilyDetail(customerFile, customerFileNew);
		createAdAddressesDetails(customerFile, customerFileNew);
		createRemovalRelocationServiceDetails(customerFile, customerFileNew);
		}
	}
		if(!linkedCustomerFileNumber.equalsIgnoreCase("") && !linkedCustomerFileNumber.contains("remove")){
			//System.out.println("\n\n\n\n inside not blank linkedCustomerFileNumber---->");
			ServiceOrder linkedServiOrder=serviceOrderManager.getForOtherCorpid(Long.parseLong(serviceOrderManager.findRemoteServiceOrder(linkedCustomerFileNumber).toString()));
			//System.out.println("\n\n\n\n linkedServiOrder---->"+linkedServiOrder);
			CustomerFile linkedCustomerFile=customerFileManager.getForOtherCorpid(Long.parseLong(customerFileManager.findRemoteCustomerFile(linkedServiOrder.getSequenceNumber()).toString()));
			createServiceOrder(linkedCustomerFile, serviceOrder,trackingStatus,billing,miscellaneous,forSection,customerFile, dspDetails);
			customerFileId=linkedCustomerFile.getId();
		}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return customerFileId;
	}
	private List pricingBillingPaybaleList;
	private void createServiceOrder(CustomerFile customerFileNew, ServiceOrder serviceOrder, TrackingStatus trackingStatus, Billing billing, Miscellaneous miscellaneous,String forSection,CustomerFile customerFile,DspDetails dspDetails) {
		 boolean billingCMMContractType=false;
		 boolean billingDMMContractType=false;
		 String fieldType="";
		 String uniqueFieldType="";
		 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
		     billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
		 } 
		 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
		      billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
		 }
		 
		 if(forSection.equalsIgnoreCase("networkPartnerCode") ||forSection.equalsIgnoreCase("bookingAgentCode")){
			 fieldType="CMM/DMM";
			 if(billingCMMContractType){
				 uniqueFieldType="CMM"; 
			 }
			 if(billingDMMContractType){
				 uniqueFieldType="DMM"; 
			 }
		 }
		List fieldToSync=customerFileManager.findFieldToSyncCreate("ServiceOrder",fieldType,uniqueFieldType);
		ServiceOrder serviceOrderNew= new ServiceOrder();
				
		try{
			Iterator serviceOrderFields=fieldToSync.iterator();
			while(serviceOrderFields.hasNext()){
				String field=serviceOrderFields.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
				beanUtilsBean.setProperty(serviceOrderNew,fieldTo.trim(),beanUtilsBean.getProperty(serviceOrder, fieldFrom.trim()));
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			}
		serviceOrderNew.setId(null);
		try{
			if(serviceOrderNew.getMoveType()==null || (serviceOrderNew.getMoveType().toString().trim().equals(""))){
			if(serviceOrderNew.getControlFlag()==null ||(serviceOrderNew.getControlFlag().toString().trim().equals("")) || (serviceOrderNew.getControlFlag().trim().equalsIgnoreCase("Q")  )){
				serviceOrderNew.setMoveType("Quote");
			}else{
				serviceOrderNew.setMoveType("BookedMove");
			}
			}
			}catch(Exception e){
				
			}
		serviceOrderNew.setCorpID(customerFileNew.getCorpID());
		if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
		serviceOrderNew.setCoordinator(customerFileNew.getCoordinator());
		}
		if(!networkAgent){
		if(((forSection.equalsIgnoreCase("bookingAgentCode"))) || ((forSection.equalsIgnoreCase("networkPartnerCode")))){
			if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
			/*List aliasNameList=userManager.findCoordinatorByUsername(serviceOrder.getCoordinator(),sessionCorpID);
			if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
				serviceOrderNew.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
			}*/
				serviceOrderNew.setCoordinator(serviceOrder.getCoordinator());
		}
		}
		}
		if(networkAgent){
			if(((forSection.equalsIgnoreCase("bookingAgentCode"))) || ((forSection.equalsIgnoreCase("networkPartnerCode")))){
				if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
					/*List aliasNameList=userManager.findCoordinatorByNetworkCoordinator(serviceOrder.getCoordinator(),customerFileNew.getCorpID());
				if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
					serviceOrderNew.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
				}*/
					serviceOrderNew.setCoordinator(serviceOrder.getCoordinator());
			}
			}
			}
		maxShip = customerFileManager.findMaximumShipExternal(customerFileNew.getSequenceNumber(),customerFileNew.getCorpID());
		if (maxShip.get(0) == null) {
			ship = "01";

		} else {
			autoShip = Long.parseLong((maxShip).get(0).toString()) + 1;
			//System.out.println(autoShip);
			if ((autoShip.toString()).length() == 1) {
				ship = "0" + (autoShip.toString());
			} else {
				ship = autoShip.toString();
			}
	}
		
		shipnumber = customerFileNew.getSequenceNumber() + ship;
		serviceOrderNew.setShip(ship);
		serviceOrderNew.setShipNumber(shipnumber);
		serviceOrderNew.setSequenceNumber(customerFileNew.getSequenceNumber());
		serviceOrderNew.setCustomerFile(customerFileNew);
		if(forSection.equalsIgnoreCase("bookingAgentCode") && billingDMMContractType){
			serviceOrderNew.setBillToCode(trackingStatus.getNetworkPartnerCode());
			serviceOrderNew.setBillToName(trackingStatus.getNetworkPartnerName());	
		}else{
		serviceOrderNew.setBillToCode(customerFileNew.getBillToCode());
		serviceOrderNew.setBillToName(customerFileNew.getBillToName());
		}
		serviceOrderNew.setCreatedBy("Networking");
		serviceOrderNew.setUpdatedBy("Networking");
		serviceOrderNew.setCreatedOn(new Date());
		serviceOrderNew.setUpdatedOn(new Date());
		serviceOrderNew.setNetworkSO(true);
		serviceOrderNew.setCustomerFileId(customerFileNew.getId());
		serviceOrderNew.setBookingAgentCode(serviceOrder.getBookingAgentCode());
		serviceOrderNew.setBookingAgentName(serviceOrder.getBookingAgentName());
		serviceOrderNew.setIsNetworkRecord(true);
		serviceOrderNew.setBookingAgentShipNumber(serviceOrder.getShipNumber());
		serviceOrderNew.setCompanyDivision(customerFileNew.getCompanyDivision());
		//serviceOrderNew.setJob(serviceOrder.getJob());
		if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
		if((serviceOrder.getRouting()!=null) && (serviceOrder.getRouting().equalsIgnoreCase("EXP"))){
			serviceOrderNew.setRouting("IMP");
		}
		else if((serviceOrder.getRouting()!=null) && (serviceOrder.getRouting().equalsIgnoreCase("IMP"))){
			serviceOrderNew.setRouting("EXP");
		}else{
		serviceOrderNew.setRouting(serviceOrder.getRouting());
		} 
		//Bug 11839 - Possibility to modify Current status to Prior status
		//serviceOrderNew.setStatus("NEW");
		//serviceOrderNew.setStatusNumber(1);
		//serviceOrderNew.setStatusDate(new Date());
		}
		String serviceTypeTemp=serviceOrder.getServiceType();
		if(serviceOrder.getServiceType()!=null && !serviceOrder.getServiceType().equalsIgnoreCase("")){
	    	String newServicesInLinkOrder="";
	    	if(serviceOrderNew.getServiceType()!=null && !serviceOrderNew.getServiceType().equalsIgnoreCase("")){
	    		String newServicesInLinkOrderTemp=serviceOrderNew.getServiceType();
	    		List al=dspDetailsManager.findAllServicesForRelo(serviceOrder.getCorpID());
	    		if(al!=null && !al.isEmpty() && al.get(0)!=null){
	    			if(newServicesInLinkOrderTemp.indexOf(",")>-1){
	    				for(String ss:newServicesInLinkOrderTemp.split(",")){
							if(!al.contains(ss)){
								if(newServicesInLinkOrder.equalsIgnoreCase("")){
									newServicesInLinkOrder=ss;
								}else{
									newServicesInLinkOrder=newServicesInLinkOrder+","+ss;
								}
							}
						}			    				
	    			}else{
	    				if(!al.contains(newServicesInLinkOrderTemp)){
	    					newServicesInLinkOrder=newServicesInLinkOrderTemp;
	    				}
	    			}
	    		}
	    	}
			String serviceTypeTemp1=serviceOrder.getServiceType();
			if(!newServicesInLinkOrder.equalsIgnoreCase("")){
				serviceTypeTemp1=serviceTypeTemp+","+newServicesInLinkOrder;
			}
			List al=dspDetailsManager.findAllServicesForRelo(customerFileNew.getCorpID());
			serviceTypeTemp="";
			if(al!=null && !al.isEmpty() && al.get(0)!=null){
				for(String ss:serviceTypeTemp1.split(",")){
					if(al.contains(ss)){
						if(serviceTypeTemp.equalsIgnoreCase("")){
							serviceTypeTemp=ss;
						}else{
							if(serviceTypeTemp.indexOf(ss)<0){
							serviceTypeTemp=serviceTypeTemp+","+ss;
							}
						}
					}
				}
			}
		}
		serviceOrderNew.setServiceType(serviceTypeTemp);
		serviceOrderNew=serviceOrderManager.save(serviceOrderNew);
		
		
		
		
		 Miscellaneous miscellaneousNew = new Miscellaneous();
		 List miscellaneousFieldToSync=customerFileManager.findFieldToSyncCreate("Miscellaneous",fieldType,uniqueFieldType);
			Iterator miscellaneousFields=miscellaneousFieldToSync.iterator();
			while(miscellaneousFields.hasNext()){
				String field=miscellaneousFields.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
				beanUtilsBean.setProperty(miscellaneousNew,fieldTo.trim(),beanUtilsBean.getProperty(miscellaneous, fieldFrom.trim()));
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			}
		 
		 miscellaneousNew.setId(serviceOrderNew.getId());
		 miscellaneousNew.setCorpID(serviceOrderNew.getCorpID());
		 miscellaneousNew.setShipNumber(serviceOrderNew.getShipNumber());
		 miscellaneousNew.setShip(serviceOrderNew.getShip());
		 miscellaneousNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
		 miscellaneousNew.setCreatedBy("Networking");
		 miscellaneousNew.setUpdatedBy("Networking");
		 miscellaneousNew.setCreatedOn(new Date());
		 miscellaneousNew.setUpdatedOn(new Date());
		 try{
			 String  unitData =customerFileManager.getNetworkUnit(serviceOrderNew.getCorpID());
			 String[] splitField=unitData.split("~");
			 String WeightUnit="";
			 String VolumeUnit="";
			 if(splitField[0]!=null && (!(splitField[0].trim().equalsIgnoreCase("No")))){
			 WeightUnit=	 splitField[0].trim();
			 }
			 if(splitField[1]!=null && (!(splitField[1].trim().equalsIgnoreCase("No")))){
			  VolumeUnit=splitField[1].trim();
			 }
			 if(WeightUnit!=null && (!(WeightUnit.equals("")))){
				 miscellaneousNew.setUnit1(WeightUnit)	; 
			 }
			 if(VolumeUnit!=null && (!(VolumeUnit.equals("")))){
				 miscellaneousNew.setUnit2(VolumeUnit)	; 
			 }
			 }catch(Exception e){
				 e.printStackTrace();
			 }
         miscellaneousNew=miscellaneousManager.save(miscellaneousNew);
         
         
		TrackingStatus trackingStatusNew=new TrackingStatus();
		
		List trackingStatusFieldToSync=customerFileManager.findFieldToSyncCreate("TrackingStatus",fieldType,uniqueFieldType);
		Iterator trackingStatusFields=trackingStatusFieldToSync.iterator();
		while(trackingStatusFields.hasNext()){
			String field=trackingStatusFields.next().toString();
			String[] splitField=field.split("~");
			String fieldFrom=splitField[0];
			String fieldTo=splitField[1];
			PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
			try{
			beanUtilsBean.setProperty(trackingStatusNew,fieldTo.trim(),beanUtilsBean.getProperty(trackingStatus, fieldFrom.trim()));
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
		
		trackingStatusNew.setId(serviceOrderNew.getId());
		trackingStatusNew.setContractType("");
		trackingStatusNew.setCorpID(serviceOrderNew.getCorpID());
		trackingStatusNew.setServiceOrder(serviceOrderNew);
		trackingStatusNew.setMiscellaneous(miscellaneousNew);
		trackingStatusNew.setShipNumber(serviceOrderNew.getShipNumber());
		trackingStatusNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
		trackingStatusNew.setSurveyTimeFrom(customerFileNew.getSurveyTime());
		trackingStatusNew.setSurveyTimeTo(customerFileNew.getSurveyTime2());
		trackingStatusNew.setSurveyDate(customerFileNew.getSurvey());
		//trackingStatusNew.setBookingAgentContact(serviceOrder.getCoordinator());
		try{
			//trackingStatusNew.setBookingAgentEmail(customerFileManager.findCoordEmailAddress(serviceOrder.getCoordinator()).get(0).toString());
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		
	/*	if(forSection.equalsIgnoreCase("destination")){
			//String[] destinationAgent=trackingStatusManager.getLocalAgent(serviceOrderNew.getCorpID(), sessionCorpID, trackingStatus.getDestinationAgentCode());
			//System.out.println("\n\n\n\n destinationAgent---> "+destinationAgent[0].toString()+" ---- "+destinationAgent[1].toString());
			//trackingStatusNew.setDestinationAgentCode(destinationAgent[0]);
			//trackingStatusNew.setDestinationAgent(destinationAgent[1]);
			//trackingStatusNew.setDestinationAgentContact(customerFile.getCoordinatorName());
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			try{
			//trackingStatusNew.setDestinationAgentEmail(customerFileManager.findCoordEmailAddress(customerFile.getCoordinator()).get(0).toString());
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
		if(forSection.equalsIgnoreCase("destinationSub")){
			//String[] destinationSubAgent=trackingStatusManager.getLocalAgent(serviceOrderNew.getCorpID(), sessionCorpID, trackingStatus.getDestinationSubAgentCode());
			//trackingStatusNew.setDestinationSubAgentCode(destinationSubAgent[0]);
			//trackingStatusNew.setDestinationSubAgent(destinationSubAgent[1]);
			//trackingStatusNew.setSubDestinationAgentAgentContact(customerFile.getCoordinatorName());
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			try{
			//trackingStatusNew.setSubDestinationAgentEmail(customerFileManager.findCoordEmailAddress(customerFile.getCoordinator()).get(0).toString());
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
		if(forSection.equalsIgnoreCase("origin")){
			//String[] originAgent=trackingStatusManager.getLocalAgent(serviceOrderNew.getCorpID(), sessionCorpID, trackingStatus.getOriginAgentCode());
			//trackingStatusNew.setOriginAgentCode(originAgent[0]);
			//trackingStatusNew.setOriginAgent(originAgent[1]);
			//trackingStatusNew.setOriginAgentContact(customerFile.getCoordinatorName());
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			try{
			//trackingStatusNew.setOriginAgentEmail(customerFileManager.findCoordEmailAddress(customerFile.getCoordinator()).get(0).toString());
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
		if(forSection.equalsIgnoreCase("originSub")){
			//String[] originSubAgent=trackingStatusManager.getLocalAgent(serviceOrderNew.getCorpID(), sessionCorpID, trackingStatus.getOriginSubAgentCode());
			//trackingStatusNew.setOriginSubAgentCode(originSubAgent[0]);
			//trackingStatusNew.setOriginSubAgent(originSubAgent[1]);
			//trackingStatusNew.setSubOriginAgentContact(customerFile.getCoordinatorName());
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			try{
			///trackingStatusNew.setSubOriginAgentEmail(customerFileManager.findCoordEmailAddress(customerFile.getCoordinator()).get(0).toString());
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}*/
		if(forSection.equalsIgnoreCase("NetworkPartnerCode")){
			//String[] networkPartnerCode=trackingStatusManager.getLocalAgent(serviceOrderNew.getCorpID(), sessionCorpID, trackingStatus.getNetworkPartnerCode());
			//trackingStatusNew.setNetworkPartnerCode(networkPartnerCode[0]);
			//trackingStatusNew.setNetworkPartnerName(networkPartnerCode[1]);
			//trackingStatusNew.setOriginAgentContact(customerFile.getCoordinatorName());
			trackingStatusNew.setContractType(uniqueFieldType);
			trackingStatusNew.setSoNetworkGroup(true);
			trackingStatusNew.setUtsiNetworkGroup(false);
			if(networkAgent){
				trackingStatusNew.setAccNetworkGroup(true);
				trackingStatusNew.setAgentNetworkGroup(true);
			}else{
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			}
			try{
			//trackingStatusNew.setNetworkEmail(customerFileManager.findCoordEmailAddress(customerFile.getCoordinator()).get(0).toString());
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
		if(forSection.equalsIgnoreCase("bookingAgentCode")){
			//String[] networkPartnerCode=trackingStatusManager.getLocalAgent(serviceOrderNew.getCorpID(), sessionCorpID, trackingStatus.getNetworkPartnerCode());
			trackingStatusNew.setContractType(uniqueFieldType);
			trackingStatusNew.setNetworkPartnerCode(trackingStatus.getNetworkPartnerCode());
			trackingStatusNew.setNetworkPartnerName(trackingStatus.getNetworkPartnerName());
			//trackingStatusNew.setOriginAgentContact(customerFile.getCoordinatorName());
			//trackingStatusNew.setBookingAgentContact(trackingStatus.getBookingAgentContact());
			try{
				//trackingStatusNew.setBookingAgentEmail(customerFileManager.findCoordEmailAddress(trackingStatus.getBookingAgentContact()).get(0).toString());
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			trackingStatusNew.setSoNetworkGroup(true);
			trackingStatusNew.setUtsiNetworkGroup(false);
			if(networkAgent){
				trackingStatusNew.setAccNetworkGroup(true);
				trackingStatusNew.setAgentNetworkGroup(true);
			}else{
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			}
			try{
			//trackingStatusNew.setNetworkEmail(customerFileManager.findCoordEmailAddress(customerFile.getCoordinator()).get(0).toString());
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
		trackingStatusNew.setCreatedBy("Networking");
		trackingStatusNew.setUpdatedBy("Networking");
		trackingStatusNew.setCreatedOn(new Date());
		trackingStatusNew.setUpdatedOn(new Date());
		try{
			 if(trackingStatusNew.getForwarderCode()!=null && (!(trackingStatusNew.getForwarderCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getForwarderCode());
				if(checkVendorCode){
					
				}else{
					trackingStatusNew.setForwarderCode("");	
				}
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
		 try{
			 if(trackingStatusNew.getBrokerCode()!=null && (!(trackingStatusNew.getBrokerCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getBrokerCode());
				if(checkVendorCode){
					
				}else{
					trackingStatusNew.setBrokerCode("");	
				}
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
		//System.out.println("\n\n\n\n\n destinationAgentName---->"+trackingStatusNew.getDestinationAgent());
		trackingStatusNew=trackingStatusManager.save(trackingStatusNew);
		
		DspDetails dspDetailsNew = new DspDetails(); 
		
		try{
			 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
			 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			 beanUtilsBean2.copyProperties(dspDetailsNew, dspDetails); 
			 dspDetailsNew.setCreatedOn(serviceOrderNew.getCreatedOn());
			 dspDetailsNew.setUpdatedOn(serviceOrderNew.getUpdatedOn());
			 dspDetailsNew.setCreatedBy(serviceOrderNew.getCreatedBy());
			 dspDetailsNew.setUpdatedBy(serviceOrderNew.getUpdatedBy());
			 dspDetailsNew.setId(serviceOrderNew.getId());
			 dspDetailsNew.setServiceOrderId(serviceOrderNew.getId());
			 dspDetailsNew.setCorpID(serviceOrderNew.getCorpID());
			 dspDetailsNew.setShipNumber(serviceOrderNew.getShipNumber());
			 dspDetailsNew=dspDetailsManager.save(dspDetailsNew);
		}catch(Exception e){
			
		}
		billToCodeAcc = billing.getBillToCode();  
		 try{
			 Billing billingNew = new Billing(); 
			 
		        List billingFieldToSync=new ArrayList();
		        /*if(billingCMMContractType){
		        	billingFieldToSync=customerFileManager.findBillingFieldToSyncCreate("Billing","CMM");
		        }
		        if(billingDMMContractType){*/
		        	billingFieldToSync=customerFileManager.findBillingFieldToSyncCreate("Billing",fieldType,uniqueFieldType);
		        //}
				Iterator billingFields=billingFieldToSync.iterator();
				while(billingFields.hasNext()){
					String field=billingFields.next().toString();
					String[] splitField=field.split("~");
 				String fieldFrom=splitField[0];
 				String fieldTo=splitField[1];
					PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					beanUtilsBean.setProperty(billingNew,fieldTo.trim(),beanUtilsBean.getProperty(billing, fieldFrom.trim()));
					}catch(Exception ex){
						logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
					}
				}
				if(billingDMMContractType){
				if(forSection.equalsIgnoreCase("bookingAgentCode")){
					billingNew.setBillToCode(trackingStatus.getNetworkPartnerCode());
					billingNew.setBillToName(trackingStatus.getNetworkPartnerName());
					billingNew.setNetworkBillToCode(customerFileNew.getAccountCode());
					billingNew.setNetworkBillToName(customerFileNew.getAccountName()); 
				}
				else{
				billingNew.setBillToCode(billing.getNetworkBillToCode());
				billingNew.setBillToName(billing.getNetworkBillToName());
					}
				}
				
		 //}
		// BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
		// beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
		// beanUtilsBean2.copyProperties(billingNew, billing);
		 if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
		 billingNew.setBillToCode(customerFileNew.getBillToCode());
		 billingNew.setBillToName(customerFileNew.getBillToName());
		 billingNew.setContract(customerFileNew.getContract());
		 billingNew.setBillTo1Point(customerFileNew.getBillPayMethod());
		 billingNew.setBillingInstructionCodeWithDesc("CON : As per Contract");
		 billingNew.setBillingInstruction("As per Contract");
		 billingNew.setSpecialInstruction("As per Quote");
		 }else{
			 if(billingDMMContractType){
			 try{	 
				 String billingCurrency="";
				 String agentBaseCurrency="";
				 agentBaseCurrency=accountLineManager.searchBaseCurrency(serviceOrderNew.getCorpID()).get(0).toString();
				 billingCurrency=serviceOrderManager.getBillingCurrencyValue(billingNew.getBillToCode(),serviceOrderNew.getCorpID());
				 if(!billingCurrency.equalsIgnoreCase(""))
				  {
					 billingNew.setBillingCurrency(billingCurrency);
				  }else{
				      billingNew.setBillingCurrency(agentBaseCurrency);
				   }
				 }catch(Exception e){
					 logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				 }
			 } 
		 }
		 billingNew.setId(serviceOrderNew.getId());
		 billingNew.setCorpID(serviceOrderNew.getCorpID());
		 billingNew.setShipNumber(serviceOrderNew.getShipNumber());
		 billingNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
		 billingNew.setServiceOrder(serviceOrderNew);
		 billingNew.setCreatedBy("Networking");
		 billingNew.setUpdatedBy("Networking");
		 billingNew.setSystemDate(new Date());
		 billingNew.setCreatedOn(new Date());
		 billingNew.setUpdatedOn(new Date());
		 try{
			 if(trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup() && billingCMMContractType && (((forSection.equalsIgnoreCase("bookingAgentCode"))||(forSection.equalsIgnoreCase("networkPartnerCode")))))	{
				 if(billing.getStorageVatDescr()!=null && (!(billing.getStorageVatDescr().toString().trim().equals("")))){ 
				 billingNew.setStorageVatDescr(sessionCorpID+"_"+billing.getStorageVatDescr()) ;
				 }
				 if(billing.getInsuranceVatDescr()!=null && (!(billing.getInsuranceVatDescr().toString().trim().equals("")))){
				 billingNew.setInsuranceVatDescr(sessionCorpID+"_"+billing.getInsuranceVatDescr()) ;
				 }
				 if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().trim().equals("")))){
				 billingNew.setPrimaryVatCode(sessionCorpID+"_"+billing.getPrimaryVatCode()) ;
				 }
				 if(billing.getSecondaryVatCode()!=null && (!(billing.getSecondaryVatCode().toString().trim().equals("")))){
				 billingNew.setSecondaryVatCode(sessionCorpID+"_"+billing.getSecondaryVatCode()) ;
				 }
				 if(billing.getPrivatePartyVatCode()!=null && (!(billing.getPrivatePartyVatCode().toString().trim().equals("")))){
				 billingNew.setPrivatePartyVatCode(sessionCorpID+"_"+billing.getPrivatePartyVatCode()) ;
				 }
				 billingNew.setBillComplete(billing.getBillComplete());
			 }
			 if(trackingStatus.getSoNetworkGroup() && (!trackingStatus.getAccNetworkGroup()) && billingCMMContractType && (((forSection.equalsIgnoreCase("bookingAgentCode"))||(forSection.equalsIgnoreCase("networkPartnerCode")))))	{
				 if(billing.getStorageVatDescr()!=null && (!(billing.getStorageVatDescr().toString().trim().equals("")))){ 
				 String storageVatDescr=billing.getStorageVatDescr();
				 if(storageVatDescr.contains(serviceOrderNew.getCorpID()+"_")){
					 storageVatDescr=storageVatDescr.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					}
				 billingNew.setStorageVatDescr(storageVatDescr) ;
				 }
				 if(billing.getInsuranceVatDescr()!=null && (!(billing.getInsuranceVatDescr().toString().trim().equals("")))){
				 String insuranceVatDescr=billing.getInsuranceVatDescr();
				 if(insuranceVatDescr.contains(serviceOrderNew.getCorpID()+"_")){
					 insuranceVatDescr=insuranceVatDescr.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					}
				 billingNew.setInsuranceVatDescr(insuranceVatDescr) ; 
				 }
				 if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().trim().equals("")))){
				 String primaryVatCode=billing.getPrimaryVatCode();
				 if(primaryVatCode.contains(serviceOrderNew.getCorpID()+"_")){
					 primaryVatCode=primaryVatCode.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					}
				 billingNew.setPrimaryVatCode(primaryVatCode) ; 
				 }
				 if(billing.getSecondaryVatCode()!=null && (!(billing.getSecondaryVatCode().toString().trim().equals("")))){
				 String secondaryVatCode=billing.getSecondaryVatCode();
				 if(secondaryVatCode.contains(serviceOrderNew.getCorpID()+"_")){
					 secondaryVatCode=secondaryVatCode.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					}
				 billingNew.setSecondaryVatCode(secondaryVatCode) ;
				 }
				 if(billing.getPrivatePartyVatCode()!=null && (!(billing.getPrivatePartyVatCode().toString().trim().equals("")))){
				 String privatePartyVatCode=billing.getPrivatePartyVatCode();
				 if(privatePartyVatCode.contains(serviceOrderNew.getCorpID()+"_")){
					 privatePartyVatCode=privatePartyVatCode.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					} 
				 billingNew.setPrivatePartyVatCode(privatePartyVatCode) ;
				 }
			 }
			 }catch( Exception e){
 	    	
    	 }
		 if(billingNew.getBillToCode()!=null && (!(billingNew.getBillToCode().equalsIgnoreCase("")))){
			 pricingBillingPaybaleList=accountLineManager.pricingBillingPaybaleName(billingNew.getBillToCode(),serviceOrderNew.getCorpID(),serviceOrderNew.getJob());
			 if((pricingBillingPaybaleList!=null) && (!(pricingBillingPaybaleList.isEmpty()))){
					String[] str1 = pricingBillingPaybaleList.get(0).toString().split("#");
					if(!(str1[0].equalsIgnoreCase("1"))){
						billingNew.setPersonPricing(str1[0]);
						}
					if(!(str1[1].equalsIgnoreCase("1"))){
						billingNew.setPersonBilling(str1[1]);
						}
					if(!(str1[2].equalsIgnoreCase("1"))){
						billingNew.setPersonPayable(str1[2]);
						}
					if(!(str1[3].equalsIgnoreCase("1"))){
						billingNew.setAuditor(str1[3]);
						}
			}
		 }
		 try{
			 if(billingNew.getVendorCode()!=null && (!(billingNew.getVendorCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(billingNew.getCorpID(),billingNew.getVendorCode());
				if(checkVendorCode){
					
				}else{
					billingNew.setVendorCode("");	
				}
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
		 if(trackingStatus.getSoNetworkGroup() && (((forSection.equalsIgnoreCase("bookingAgentCode"))||(forSection.equalsIgnoreCase("networkPartnerCode"))))){
		 String externalCorpID = serviceOrderNew.getCorpID();
 		String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
 		if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
				DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
				String estExchangeRate="1"; 
				BigDecimal estExchangeRateBig=new BigDecimal("1.0000");
	            //if(billingNew.getCurrency()!=null && billing.getCurrency()!=null && (!(billingNew.getCurrency().trim().equalsIgnoreCase(billing.getCurrency().trim())))){
	            List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,billingNew.getCurrency());
				 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
				 {
					 estExchangeRate=estRate.get(0).toString();
				 
				 } 
				estExchangeRateBig=new BigDecimal(estExchangeRate);	
				billingNew.setExchangeRate(estExchangeRateBig);
	            //}
				 DecimalFormat decimalFormat = new DecimalFormat("#.##");
	            try{
	            	billingNew.setBaseInsuranceValue(new BigDecimal(decimalFormat.format((billing.getInsuranceValueActual().doubleValue())/(estExchangeRateBig.doubleValue()))));	
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
	            try{
	            	billingNew.setBaseInsuranceTotal((new BigDecimal(decimalFormat.format(((((billingNew.getBaseInsuranceValue())).doubleValue())*(((billing.getInsuranceBuyRate())).doubleValue()) )/(100)))).toString());	
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
	             
 		}
		 }
		 billingNew=billingManager.save(billingNew);
		 }catch(Exception ex){
			 logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
		 }
		 customerFileManager.updateIsNetworkFlag(customerFile.getId(), customerFile.getCorpID(), serviceOrder.getId(),customerFileNew.getContractType(),customerFileNew.getIsNetworkGroup());
		 try{
				if(trackingStatus.getAccNetworkGroup())	{
				if(forSection.equalsIgnoreCase("NetworkPartnerCode")){ 
				if(billingDMMContractType){
				if(billToCodeAcc!=null && (!(billToCodeAcc.toString().equals(""))) && trackingStatus.getNetworkPartnerCode()!=null && (!(trackingStatus.getNetworkPartnerCode().equals(""))) && (!(billToCodeAcc.toString().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode())))){	
				 trackingStatusManager.updateDMMAgentBilltocode(sessionCorpID, serviceOrder.getId(),trackingStatus.getNetworkPartnerCode(),trackingStatus.getNetworkPartnerName()); 		 
				  List accountlineList= accountLineManager.getAllAccountLine(serviceOrder.getId()); 
		      		if(accountlineList!=null && (!(accountlineList.isEmpty()))){
		    			 Iterator accountlineIterator=accountlineList.iterator();
		    			  while (accountlineIterator.hasNext()) {
		    				try{  
		    				Long  Accid = (Long) accountlineIterator.next();
		    				accountLine = accountLineManager.get(Accid);
		    				String billtocode=accountLine.getBillToCode(); 
		    				if(accountLine.getRecInvoiceNumber()==null || accountLine.getRecInvoiceNumber().toString().trim().equals("")){ 
		    					if(billtocode!=null &&  billtocode.equalsIgnoreCase(billToCodeAcc)){	
		    					accountLine.setBillToCode(trackingStatus.getNetworkPartnerCode());
		    					accountLine.setBillToName(trackingStatus.getNetworkPartnerName());
		    					}  
		    				if(billtocode!=null && accountLine.getBillToCode()!=null &&  (!(billtocode.equalsIgnoreCase(accountLine.getBillToCode())))){		
		    				String billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),accountLine.getCorpID());
		    				if(!billingCurrency.equalsIgnoreCase(""))
		    				{
		    					accountLine.setRecRateCurrency(billingCurrency);
		    					accountLine.setEstSellCurrency(billingCurrency);
		    					accountLine.setRevisionSellCurrency(billingCurrency); 
		    					String recExchangeRate="1"; 
		    		            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
		    		            List recRate=exchangeRateManager.findAccExchangeRate(accountLine.getCorpID(),accountLine.getRecRateCurrency());
		    					 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
		    					 {
		    						 recExchangeRate=recRate.get(0).toString();
		    					 }
		    					recExchangeRateBig=new BigDecimal(recExchangeRate);
		    					accountLine.setRecRateExchange(recExchangeRateBig);
		    					accountLine.setRacValueDate(new Date());
		    					if(accountLine.getRecRate()!=null){
		    					accountLine.setRecCurrencyRate(accountLine.getRecRate().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setRecCurrencyRate(new BigDecimal(0.00));	
		    					}
		    					if(accountLine.getActualRevenue()!=null){
		    					accountLine.setActualRevenueForeign(accountLine.getActualRevenue().multiply(recExchangeRateBig)); 
		    					}else{
		    					accountLine.setActualRevenueForeign(new BigDecimal(0.00));	
		    					}
		    					accountLine.setEstSellExchangeRate(recExchangeRateBig);
		    					accountLine.setEstSellValueDate(new Date()); 
		    					if(accountLine.getEstimateSellRate()!=null){
		    					accountLine.setEstSellLocalRate(accountLine.getEstimateSellRate().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setEstSellLocalRate(new BigDecimal(0.00));	
		    					}
		    					if(accountLine.getEstimateRevenueAmount()!=null){
		    					accountLine.setEstSellLocalAmount(accountLine.getEstimateRevenueAmount().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setEstSellLocalAmount(new BigDecimal(0.00));	
		    					}
		    					accountLine.setRevisionSellExchangeRate(recExchangeRateBig);
		    					accountLine.setRevisionSellValueDate(new Date()); 
		    					if(accountLine.getRevisionSellRate()!=null){
		    					accountLine.setRevisionSellLocalRate(accountLine.getRevisionSellRate().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setRevisionSellLocalRate(new BigDecimal(0.00));	
		    					}
		    					if(accountLine.getRevisionRevenueAmount()!=null){
		    					accountLine.setRevisionSellLocalAmount(accountLine.getRevisionRevenueAmount().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setRevisionSellLocalAmount(new BigDecimal(0.00));	
		    					}
		    					try{
		    					    String recVatPercent=accountLine.getRecVatPercent();
		    					    BigDecimal recVatPercentBig=new BigDecimal("0.00");
		    					    BigDecimal recVatAmmountBig=new BigDecimal("0.00");
		    					    BigDecimal revisionVatAmmountBig=new BigDecimal("0.00");
		    					    BigDecimal estVatAmmountBig=new BigDecimal("0.00");
		    					    if(recVatPercent!=null && (!(recVatPercent.trim().equals("")))){
		    					    	recVatPercentBig=new BigDecimal(recVatPercent);
		    					    }
		    					    DecimalFormat decimalFormat = new DecimalFormat("#.####");
		    					   recVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLine.getActualRevenueForeign().multiply(recVatPercentBig)).doubleValue())/100)));
		    					   try{
		    						    estVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLine.getEstSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
		    						    revisionVatAmmountBig = (new BigDecimal(decimalFormat.format(((accountLine.getRevisionSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
		    						    }catch(Exception e){
		    						    	
		    						    }
		    					   accountLine.setRecVatAmt(recVatAmmountBig);
		    					   accountLine.setEstVatAmt(estVatAmmountBig); 
		    					   accountLine.setRevisionVatAmt(revisionVatAmmountBig); 
		    					  
		    					    }catch(Exception e){
		    		                    e.printStackTrace();			    	
		    					    }  
		    				}
		    				accountLine= accountLineManager.save(accountLine);
		    			
		    				}
		    				} 
		    				}catch(Exception e){
		    					e.printStackTrace();
		    				}
		    			  }
		    			  }
		 		}  
				}
				}
				}
			}catch(Exception e){
				
			}
		 try{ 
		 createContainerRecords(serviceOrder, serviceOrderNew);
		 createPieceCountsRecords(serviceOrder, serviceOrderNew);
		 createVehicleRecords(serviceOrder, serviceOrderNew);
		 createRoutingRecords(serviceOrder, serviceOrderNew);
		 createConsigneeInstruction(serviceOrder, serviceOrderNew);
		 
		/* 
		  * Bug 6887 - Claim sync between BA and NA
		  
		 if(trackingStatus.getSoNetworkGroup())	{
			if(forSection.equalsIgnoreCase("NetworkPartnerCode") || forSection.equalsIgnoreCase("bookingAgentCode")){ 
				createClaim(serviceOrder, serviceOrderNew);
			}
		
		}*/
		}catch(Exception ex){
			logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
		}
		try{
			 if(trackingStatus.getAccNetworkGroup())	{
			if(forSection.equalsIgnoreCase("NetworkPartnerCode")){ 
			 if(billingCMMContractType){
				 accountLineList = accountLineManager.getAccountLinesID(serviceOrder.getShipNumber());
			     Iterator it=accountLineList.listIterator();
			     while(it.hasNext()){
					  Long row=(Long)it.next();
					  accountLine = accountLineManager.getForOtherCorpid(row);
					  try{
						  String partnertype="";
                      	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
                      		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
                      	  }
						  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals(""))){	  
						  createAccountLine(serviceOrderNew,serviceOrder,accountLine)  ;
						  }
					  }catch(Exception e){
							
						}
			     }
				 
			 }
			 if(billingDMMContractType){
				 accountLineList = accountLineManager.getAccountLinesID(serviceOrder.getShipNumber());
			     Iterator it=accountLineList.listIterator();
			     while(it.hasNext()){
					  Long row=(Long)it.next();
					  accountLine = accountLineManager.getForOtherCorpid(row);
					  try{
						  //String partnertype="";
						  boolean billtotype=false;
                      	  if(billing.getNetworkBillToCode()!=null && (!(billing.getNetworkBillToCode().toString().equals(""))) && accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))  && accountLine.getNetworkBillToCode()!=null && (!(accountLine.getNetworkBillToCode().toString().equals(""))) ){
                      		  //partnertype=  partnerManager.checkPartnerType(customerFile.getAccountCode());
                      		  billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());
                      	  }
						  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (billtotype)  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals(""))){
						  createDMMAccountLine(serviceOrderNew,serviceOrder,accountLine,billing)  ;
						  }
					  }catch(Exception e){
							
						}
			     }
				 
			 }
			 updateExternalAccountLine(serviceOrderNew,customerFileNew.getCorpID());
		 }
		}
		}catch(Exception e){
			
		}
		DspDetails dspDetailsOLD = new DspDetails(); 
		BeanUtilsBean beanUtilsBean4 = BeanUtilsBean.getInstance();
		 beanUtilsBean4.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
		 beanUtilsBean4.copyProperties(dspDetailsOLD, dspDetails);
		 if(forSection.equalsIgnoreCase("CAR_vendorCode")){
			 dspDetailsOLD.setCAR_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("COL_vendorCode")){
			 dspDetailsOLD.setCOL_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("TRG_vendorCode")){
			 dspDetailsOLD.setTRG_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("HOM_vendorCode")){
			 dspDetailsOLD.setHOM_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("RNT_vendorCode")){
			 dspDetailsOLD.setRNT_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("LAN_vendorCode")){
			 dspDetailsOLD.setLAN_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("MMG_vendorCode")){
			 dspDetailsOLD.setMMG_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("ONG_vendorCode")){
			 dspDetailsOLD.setONG_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			} 
		 if(forSection.equalsIgnoreCase("PRV_vendorCode")){
			 dspDetailsOLD.setPRV_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			} 
		 if(forSection.equalsIgnoreCase("AIO_vendorCode")){
			 dspDetailsOLD.setAIO_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("EXP_vendorCode")){
			 dspDetailsOLD.setEXP_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("RPT_vendorCode")){
			 dspDetailsOLD.setRPT_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("TAX_vendorCode")){
			 dspDetailsOLD.setTAX_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("TAC_vendorCode")){
			 dspDetailsOLD.setTAC_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("TEN_vendorCode")){
			 dspDetailsOLD.setTEN_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("VIS_vendorCode")){
			 dspDetailsOLD.setVIS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("WOP_vendorCode")){
			 dspDetailsOLD.setWOP_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("REP_vendorCode")){
			 dspDetailsOLD.setREP_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("SET_vendorCode")){
			 dspDetailsOLD.setSET_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("SCH_vendorCode")){
			 dspDetailsOLD.setSCH_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("RLS_vendorCode")){
			 dspDetailsOLD.setRLS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}		
		 
		 if(forSection.equalsIgnoreCase("CAT_vendorCode")){
			 dspDetailsOLD.setCAT_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("CLS_vendorCode")){
			 dspDetailsOLD.setCLS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("CHS_vendorCode")){
			 dspDetailsOLD.setCHS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("DPS_vendorCode")){
			 dspDetailsOLD.setDPS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("HSM_vendorCode")){
			 dspDetailsOLD.setHSM_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("PDT_vendorCode")){
			 dspDetailsOLD.setPDT_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("RCP_vendorCode")){
			 dspDetailsOLD.setRCP_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("SPA_vendorCode")){
			 dspDetailsOLD.setSPA_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("TCS_vendorCode")){
			 dspDetailsOLD.setTCS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}		 
		 if(forSection.equalsIgnoreCase("MTS_vendorCode")){
			 dspDetailsOLD.setMTS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}	
		 if(forSection.equalsIgnoreCase("DSS_vendorCode")){
			 dspDetailsOLD.setDSS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("HOB_vendorCode")){
			 dspDetailsOLD.setHOB_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("FLB_vendorCode")){
			 dspDetailsOLD.setFLB_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("FRL_vendorCode")){
			 dspDetailsOLD.setFRL_vendorCodeEXSO(serviceOrderNew.getShipNumber());
			}
		 if(forSection.equalsIgnoreCase("APU_vendorCode")){
			 dspDetailsOLD.setAPU_vendorCodeEXSO(serviceOrderNew.getShipNumber());
		 }
		 if(forSection.equalsIgnoreCase("INS_vendorCode")){
			 dspDetailsOLD.setINS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
		 }
		 if(forSection.equalsIgnoreCase("INP_vendorCode")){
			 dspDetailsOLD.setINP_vendorCodeEXSO(serviceOrderNew.getShipNumber());
		 }
		 if(forSection.equalsIgnoreCase("EDA_vendorCode")){
			 dspDetailsOLD.setEDA_vendorCodeEXSO(serviceOrderNew.getShipNumber());
		 }
		 if(forSection.equalsIgnoreCase("TAS_vendorCode")){
			 dspDetailsOLD.setTAS_vendorCodeEXSO(serviceOrderNew.getShipNumber());
		 }
		 
		 dspDetailsOLD=dspDetailsManager.save(dspDetailsOLD);
	 	 TrackingStatus trackingStatusOLD=new TrackingStatus();
		 BeanUtilsBean beanUtilsBean5 = BeanUtilsBean.getInstance();
		 beanUtilsBean5.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
		 beanUtilsBean5.copyProperties(trackingStatusOLD, trackingStatus); 
		 /*if(forSection.equalsIgnoreCase("destination")){
			trackingStatusOLD.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
		}
		if(forSection.equalsIgnoreCase("destinationSub")){
			trackingStatusOLD.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
		}
		if(forSection.equalsIgnoreCase("origin")){
			trackingStatusOLD.setOriginAgentExSO(serviceOrderNew.getShipNumber());
		}
		if(forSection.equalsIgnoreCase("originSub")){
			trackingStatusOLD.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
		}*/ 
		if(forSection.equalsIgnoreCase("NetworkPartnerCode")){
			trackingStatusOLD.setContractType(uniqueFieldType);
			trackingStatusOLD.setNetworkAgentExSO(serviceOrderNew.getShipNumber()); 
		} 
		if(forSection.equalsIgnoreCase("bookingAgentCode")){
			trackingStatusOLD.setContractType(uniqueFieldType);
			trackingStatusOLD.setBookingAgentExSO(serviceOrderNew.getShipNumber()); 
		}
		
		 trackingStatusOLD=trackingStatusManager.save(trackingStatusOLD);
		}catch(Exception ex){
			logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
		}
		
	}
	private String DSserviceOrderId;
	private String feebackDivId;
	private String serviceName="";
	List CustomerFeebackDetails=new ArrayList();
	@SkipValidation
	public String getCustomerFeebackAjax(){
		
		CustomerFeebackDetails=dspDetailsManager.getFeebackDetails(shipnumber,DSserviceOrderId,serviceName,sessionCorpID);
		
		return SUCCESS;
	}
	@SkipValidation
    private void createAccountLine(ServiceOrder serviceOrderToRecods, ServiceOrder  serviceOrder,AccountLine accountLine) {
		 
    		  
    		try{
    			decimalFormat= new DecimalFormat("#.####");
    		trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
        	if(trackingStatusToRecod.getSoNetworkGroup()){
        		trackingStatus=trackingStatusManager.getForOtherCorpid(serviceOrder.getId()); 
        		//String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode()); 
        		String externalCorpID = trackingStatusToRecod.getCorpID();
        		UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
        		UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
        		String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
        		List fieldToSync=customerFileManager.findAccFieldToSyncCreate("AccountLine","CMM");
        		AccountLine accountLineNew= new AccountLine();
        		try{
    				Iterator it1=fieldToSync.iterator();
    				while(it1.hasNext()){
    				String field=it1.next().toString();
    				String[] splitField=field.split("~");
    				String fieldFrom=splitField[0];
    				String fieldTo=splitField[1];
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
						if(fieldFrom!=null && fieldTo!=null && fieldFrom.trim().equalsIgnoreCase("recRateExchange") && fieldTo.trim().equalsIgnoreCase("exchangeRate"))	{
							BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();
							if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
							payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
							accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
							} 
						}else{	
					       beanUtilsBean.setProperty(accountLineNew,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
						}
					}catch(Exception ex){
						logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
					}
    				
    				}
        		}catch(Exception e){
        			
        		}
        		/*maxLineNumber = serviceOrderManager.findExternalMaximumLineNumber(serviceOrderToRecods.getShipNumber(),externalCorpID); 
	            if ( maxLineNumber.get(0) == null ) {          
	             	accountLineNumber = "001";
	             }else {
	             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
	                 if((autoLineNumber.toString()).length() == 2) {
	                 	accountLineNumber = "0"+(autoLineNumber.toString());
	                 }
	                 else if((autoLineNumber.toString()).length() == 1) {
	                 	accountLineNumber = "00"+(autoLineNumber.toString());
	                 } 
	                 else {
	                 	accountLineNumber=autoLineNumber.toString();
	                 }
	             }*/
	            accountLineNew.setNetworkSynchedId(accountLine.getId());
	            accountLineNew.setAccountLineNumber(accountLine.getAccountLineNumber()); 
	            accountLineNew.setCompanyDivision(serviceOrderToRecods.getCompanyDivision());
	            accountLineNew.setVendorCode(serviceOrderToRecods.getBookingAgentCode());
	            accountLineNew.setEstimateVendorName(serviceOrderToRecods.getBookingAgentName());
	             String actCode="";
	             String  companyDivisionAcctgCodeUnique1="";
	    		 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(externalCorpID);
	    		 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
	    			 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
	    		 }
	            if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
	    			actCode=partnerManager.getAccountCrossReference(serviceOrderToRecods.getBookingAgentCode(),serviceOrderToRecods.getCompanyDivision(),externalCorpID);
	    		}else{
	    			actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrderToRecods.getBookingAgentCode(),externalCorpID);
	    		}
	            accountLineNew.setActgCode(actCode);
	            if(accountLineNew.getEstimateRevenueAmount() !=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0){
	            accountLineNew.setEstimatePassPercentage(100);
	            }
	            if(accountLineNew.getRevisionRevenueAmount() !=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0){
	            accountLineNew.setRevisionPassPercentage(100);
	            }
	            /*if(!costElementFlag){
	            List  agentGLList=accountLineManager.getGLList(accountLineNew.getChargeCode(),billingRecods.getContract(),externalCorpID);
				if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
					try{
					String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
					String recGl=GLarrayData[0];
					String payGl=GLarrayData[1];
					if(!(recGl.equalsIgnoreCase("NO"))){
						accountLineNew.setRecGl(recGl);
					}
					if(!(payGl.equalsIgnoreCase("NO"))){
						accountLineNew.setPayGl(payGl);
					}
					String VATExclude=GLarrayData[2];
					if(VATExclude.equalsIgnoreCase("Y")){
						accountLineNew.setVATExclude(true);
					}else{
						accountLineNew.setVATExclude(false);
					}
				}catch(Exception e){
					
				}
				}
	            }else{*/
	            	String chargeStr="";
					List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),accountLineNew.getChargeCode(),externalCorpID,serviceOrderToRecods.getJob(),serviceOrderToRecods.getRouting(),serviceOrderToRecods.getCompanyDivision());
					if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
						  chargeStr= glList.get(0).toString();
					  }
					   if(!chargeStr.equalsIgnoreCase("")){
						  String [] chrageDetailArr = chargeStr.split("#");
						  accountLineNew.setRecGl(chrageDetailArr[1]);
						  accountLineNew.setPayGl(chrageDetailArr[2]);
						}else{
							accountLineNew.setRecGl("");
							accountLineNew.setPayGl("");
					  }
	            //}
	            accountLineNew.setId(null);
	            accountLineNew.setCorpID(externalCorpID);
	            accountLineNew.setCreatedBy("Networking");
	            accountLineNew.setUpdatedBy("Networking");
	            accountLineNew.setCreatedOn(new Date());
	            accountLineNew.setUpdatedOn(new Date()); 
	            /*accountLineNew.setEstValueDate(new Date());
	             try{
	            BigDecimal estExchangeRateBig=accountLineNew.getEstExchangeRate();
	            accountLineNew.setEstLocalRate(accountLineNew.getEstimateRate().multiply(estExchangeRateBig));
	            accountLineNew.setEstLocalAmount(accountLineNew.getEstimateExpense().multiply(estExchangeRateBig));
	            }catch(Exception e){
	            	
	            }
	            double estExchangeRate=0;
	            BigDecimal estExchangeRateBig=new BigDecimal("1.0000");
	            List eRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstCurrency());
				 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
				 {
				 String exchangeRate1=eRate.get(0).toString();
				 estExchangeRate=Double.parseDouble(exchangeRate1);
				 }else{
					 estExchangeRate=1; 
				 }	
				estExchangeRateBig=new BigDecimal(estExchangeRate);	
			    accountLineNew.setEstExchangeRate(estExchangeRateBig);*/
	            /*accountLineNew.setRevisionValueDate(new Date());
			    try{
		            BigDecimal revisionExchangeRateBig=accountLineNew.getRevisionExchangeRate();
		            accountLineNew.setRevisionLocalRate(accountLineNew.getRevisionRate().multiply(revisionExchangeRateBig));
		            accountLineNew.setRevisionLocalAmount(accountLineNew.getRevisionExpense().multiply(revisionExchangeRateBig));
		            }catch(Exception e){
		            	
		            }
			   double revisionExchangeRate=0; 
	            BigDecimal revisionExchangeRateBig=new BigDecimal("1.0000");
	            List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionCurrency());
				 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
				 {
				 String revisionExchangeRate1=revisionRate.get(0).toString();
				 revisionExchangeRate=Double.parseDouble(revisionExchangeRate1);
				 }else{
					 revisionExchangeRate=1; 
				 }	
				revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
				accountLineNew.setRevisionExchangeRate(revisionExchangeRateBig);*/
				accountLineNew.setRacValueDate(new Date());
				/*String recExchangeRate="1"; 
	            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
	            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
				 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
				 {
					 recExchangeRate=recRate.get(0).toString();
				 }
				recExchangeRateBig=new BigDecimal(recExchangeRate);
				accountLineNew.setRecRateExchange(recExchangeRateBig);
				accountLineNew.setRecRate(accountLineNew.getRecCurrencyRate().divide(recExchangeRateBig,2));
				accountLineNew.setActualRevenue(accountLine.getActualRevenue().divide(recExchangeRateBig,2)); */
				accountLineNew.setContractValueDate(new Date());
			    /*String contractExchangeRate="1"; 
	            BigDecimal contractExchangeRateBig=new BigDecimal("1.0000");
	            List contractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getContractCurrency ());
				 if((contractRate!=null)&&(!contractRate.isEmpty())&& contractRate.get(0)!=null && (!(contractRate.get(0).toString().equals(""))))
				 {
					 contractExchangeRate=contractRate.get(0).toString();
				 
				 } 
				contractExchangeRateBig=new BigDecimal(contractExchangeRate);	
				accountLineNew.setContractExchangeRate(contractExchangeRateBig);
				accountLineNew.setContractRate(accountLine.getContractRate().divide(contractExchangeRateBig,2)); */
				accountLineNew.setValueDate(new Date());
			    /*double payExchangeRate=0; 
	            BigDecimal payExchangeRateBig=new BigDecimal("1.0000");
	            List payRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getCountry ());
				 if((payRate!=null)&&(!payRate.isEmpty())&& payRate.get(0)!=null && (!(payRate.get(0).toString().equals(""))))
				 {
				 String payExchangeRate1=payRate.get(0).toString();
				 payExchangeRate=Double.parseDouble(payExchangeRate1);
				 }else{
					 payExchangeRate=1; 
				 }*/
				BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();
				if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
					payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
					accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
				}
				//BigDecimal payExchangeRateBig=new BigDecimal(accountLineNew.getExchangeRate());	
				//accountLineNew.setExchangeRate(payExchangeRate);
				try{
				//accountLineNew.setActualExpense(accountLineNew.getLocalAmount().divide(accountLine.getRecRateExchange(),2));
				}catch(Exception e){
	    			
	    		}
if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
	DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
	String estExchangeRate="1"; 
    BigDecimal estExchangeRateBig=new BigDecimal("1.0000");
    List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstSellCurrency());
	 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
	 {
		 estExchangeRate=estRate.get(0).toString();
	 
	 } 
	estExchangeRateBig=new BigDecimal(estExchangeRate);	
	accountLineNew.setEstSellExchangeRate(estExchangeRateBig); 
	accountLineNew.setEstExchangeRate(estExchangeRateBig);
	if(accountLine.getEstSellLocalRate()!=null && accountLine.getEstSellLocalRate().doubleValue()!=0){
	accountLineNew.setEstimateSellRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
	accountLineNew.setEstimateRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
	}
	if(accountLine.getEstSellLocalAmount()!=null && accountLine.getEstSellLocalAmount().doubleValue()!=0){
	accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
	accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
	} 
	BigDecimal estcontractExchangeRateBig=new BigDecimal("1");
	try{ 	
		estcontractExchangeRateBig=((new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getEstimateContractRate().doubleValue())/(accountLineNew.getEstimateSellRate().doubleValue()))))); 
	}catch(Exception es){
		
	} 
	accountLineNew.setEstimateContractExchangeRate(estcontractExchangeRateBig);
	accountLineNew.setEstimatePayableContractExchangeRate(estcontractExchangeRateBig);
	
	
	String revisionExchangeRate="1"; 
    BigDecimal revisionExchangeRateBig=new BigDecimal("1.0000");
    List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionSellCurrency());
	 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
	 {
		 revisionExchangeRate=revisionRate.get(0).toString();
	 
	 } 
	 revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
	accountLineNew.setRevisionSellExchangeRate(revisionExchangeRateBig);
	accountLineNew.setRevisionExchangeRate(revisionExchangeRateBig);
	if(accountLine.getRevisionSellLocalRate()!=null && accountLine.getRevisionSellLocalRate().doubleValue()!=0){
	accountLineNew.setRevisionSellRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
	accountLineNew.setRevisionRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
	}
	if(accountLine.getRevisionSellLocalAmount()!=null && accountLine.getRevisionSellLocalAmount().doubleValue()!=0){
	accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
	accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
	}
	
	
	
	BigDecimal revisioncontractExchangeRateBig=new BigDecimal("1");	
	try{ 
		revisioncontractExchangeRateBig=((new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getRevisionContractRate().doubleValue())/(accountLineNew.getRevisionSellRate().doubleValue())))));
		
	}catch(Exception es){
		
	}
	accountLineNew.setRevisionContractExchangeRate(revisioncontractExchangeRateBig);
	accountLineNew.setRevisionPayableContractExchangeRate(revisioncontractExchangeRateBig);
	 
	
	
	
	String recExchangeRate="1"; 
    BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
    List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
	 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
	 {
		 recExchangeRate=recRate.get(0).toString();
	 
	 } 
	recExchangeRateBig=new BigDecimal(recExchangeRate);	
	accountLineNew.setRecRateExchange(recExchangeRateBig);
	payExchangeRateBig=accountLineNew.getRecRateExchange();
	if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
		payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
		accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
	}
	if(accountLine.getRecCurrencyRate()!=null && accountLine.getRecCurrencyRate().doubleValue()!=0){
	accountLineNew.setRecRate((new BigDecimal(decimalFormatExchangeRate.format((accountLine.getRecCurrencyRate().doubleValue())/(recExchangeRateBig.doubleValue())))));
	}
	if(accountLine.getActualRevenueForeign()!=null && accountLine.getActualRevenueForeign().doubleValue()!=0){
	accountLineNew.setActualRevenue((new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue())))));
	try{
		accountLineNew.setActualExpense((new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue())))));
		}catch(Exception e){
			
	}
	
	
	}
	BigDecimal contractExchangeRateBig=new BigDecimal("1");	
	try{
		
		contractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getContractRate().doubleValue())/(accountLineNew.getRecRate().doubleValue()))));
		
	}catch(Exception es){
		
	}
	accountLineNew.setContractExchangeRate(contractExchangeRateBig);
	accountLineNew.setPayableContractExchangeRate(contractExchangeRateBig); 
}
				if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){
				if(UTSIeuVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){		
				accountLineNew.setRecVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
				}else{
					accountLineNew.setRecVatDescr("");
					accountLineNew.setRecVatPercent("0");
					accountLineNew.setRecVatAmt(new BigDecimal(0));
					accountLineNew.setEstVatAmt(new BigDecimal(0));
					accountLineNew.setRevisionVatAmt(new BigDecimal(0));
					}
			    if(UTSIpayVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){ 
				accountLineNew.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
			    }else{
					accountLineNew.setPayVatDescr("");
					accountLineNew.setPayVatPercent("0");
					accountLineNew.setPayVatAmt(new BigDecimal(0));
					accountLineNew.setEstExpVatAmt(new BigDecimal(0)); 
					accountLineNew.setRevisionExpVatAmt(new BigDecimal(0));
					}
				}
	            accountLineNew.setServiceOrderId(serviceOrderToRecods.getId());
	            accountLineNew.setSequenceNumber(serviceOrderToRecods.getSequenceNumber());
	            accountLineNew.setShipNumber(serviceOrderToRecods.getShipNumber());
	            accountLineNew.setServiceOrder(serviceOrderToRecods);
	            accountLineNew=accountLineManager.save(accountLineNew);
	             
        	}
    		}catch(Exception e){
    			
    		}
    	}
	
	
	@SkipValidation
    private void createDMMAccountLine(ServiceOrder serviceOrderToRecods, ServiceOrder  serviceOrder,AccountLine accountLine,Billing billing){


		 
    		try{
    			decimalFormat= new DecimalFormat("#.####");
    		trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
        	if(trackingStatusToRecod.getSoNetworkGroup()){
        		trackingStatus=trackingStatusManager.getForOtherCorpid(serviceOrder.getId()); 
        		String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode());
        		UTSIpayVatList=	refMasterManager.findByParameterUTSIPayVat (sessionCorpID, "PAYVATDESC",externalCorpID);
        		UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
        		UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
        		List fieldToSync=customerFileManager.findAccFieldToSyncCreate("AccountLine","DMM");
        		AccountLine accountLineNew= new AccountLine();
        		try{
    				Iterator it1=fieldToSync.iterator();
    				while(it1.hasNext()){
    				String field=it1.next().toString();
    				String[] splitField=field.split("~");
    				String fieldFrom=splitField[0];
    				String fieldTo=splitField[1];
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					beanUtilsBean.setProperty(accountLineNew,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
					}catch(Exception ex){
						logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
					}
    				
    				}
        		}catch(Exception e){
        			
        		}
        		try{
        			if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){ 
    			    	String companyDivisionUTSI=accountLineManager.getUTSICompanyDivision(externalCorpID,accountLine.getBillToCode()) ;
    			    	accountLineNew.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
    			    	String payVatDescr="";
    			    	if(UTSIpayVatList.containsKey(accountLineNew.getPayVatDescr()+"~"+companyDivisionUTSI)){
    			    		payVatDescr=UTSIpayVatList.get(accountLineNew.getPayVatDescr()+"~"+companyDivisionUTSI);
    			    		accountLineNew.setPayVatDescr(payVatDescr);
    			    		String payVatPercent="0";
    			    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
    	    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
    			    		}
    	    				accountLineNew.setPayVatPercent(payVatPercent);
    			    	}else{
    			    		if(UTSIpayVatList.containsKey(accountLineNew.getPayVatDescr()+"~NO")){
    				    		payVatDescr=UTSIpayVatList.get(accountLineNew.getPayVatDescr()+"~NO");
    				    		accountLineNew.setPayVatDescr(payVatDescr);
    				    		String payVatPercent="0";
    				    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
    		    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
    				    		}
    		    				accountLineNew.setPayVatPercent(payVatPercent);
    				    	}else{
    				    		accountLineNew.setPayVatDescr("");
    				    		accountLineNew.setPayVatPercent("0");
    				    	}
    			    	}
    			    	}
        		}catch(Exception e){
        			
        		}
        		/*maxLineNumber = serviceOrderManager.findExternalMaximumLineNumber(serviceOrderToRecods.getShipNumber(),externalCorpID); 
	            if ( maxLineNumber.get(0) == null ) {          
	             	accountLineNumber = "001";
	             }else {
	             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
	                 if((autoLineNumber.toString()).length() == 2) {
	                 	accountLineNumber = "0"+(autoLineNumber.toString());
	                 }
	                 else if((autoLineNumber.toString()).length() == 1) {
	                 	accountLineNumber = "00"+(autoLineNumber.toString());
	                 } 
	                 else {
	                 	accountLineNumber=autoLineNumber.toString();
	                 }
	             }*/
        		  /*
			     * Added new code for setters some field value as mentioned in Bug# 6124
			     * DMM Payable Mapping reported by : Neha Singh
			     * Code by Gautam Verma
			     */
			    accountLineNew.setPayableContractCurrency(accountLine.getRecRateCurrency());
			    accountLineNew.setCountry(accountLine.getRecRateCurrency());
			    accountLineNew.setEstimatePayableContractCurrency(accountLine.getEstSellCurrency());
			    accountLineNew.setEstCurrency(accountLine.getEstSellCurrency());
			    accountLineNew.setRevisionPayableContractCurrency(accountLine.getRevisionSellCurrency());
			    accountLineNew.setRevisionCurrency(accountLine.getRevisionSellCurrency());
			    String billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLineNew.getBillToCode(),externalCorpID);
	    		if(!billingCurrency.equalsIgnoreCase(""))
	    		{
	    			accountLineNew.setRecRateCurrency(billingCurrency);
	    			accountLineNew.setEstSellCurrency(billingCurrency);
	    			accountLineNew.setRevisionSellCurrency(billingCurrency);
	    		}else{ 
	    			String baseCurrencyExternalCorpID="";
	    			String baseCurrencyCompanyDivisionExternalCorpID=""; 
	    			baseCurrencyExternalCorpID=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
	    			baseCurrencyCompanyDivisionExternalCorpID=	 companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrderToRecods.getCompanyDivision(),externalCorpID);
	    			if(baseCurrencyCompanyDivisionExternalCorpID==null ||baseCurrencyCompanyDivisionExternalCorpID.equals(""))
	    			{
	    				accountLineNew.setRecRateCurrency(baseCurrencyExternalCorpID);
	    				accountLineNew.setEstSellCurrency(baseCurrencyExternalCorpID);
	    				accountLineNew.setRevisionSellCurrency(baseCurrencyExternalCorpID);
	    			}else{
	    				accountLineNew.setRecRateCurrency(baseCurrencyCompanyDivisionExternalCorpID);
	    				accountLineNew.setEstSellCurrency(baseCurrencyCompanyDivisionExternalCorpID);
	    				accountLineNew.setRevisionSellCurrency(baseCurrencyCompanyDivisionExternalCorpID);
	    			}
	    		}
			    //BigDecimal currRate = recExchangeRateBig.multiply(accountLineNew.getRecQuantity());
			   // accountLineNew.setPayableContractRateAmmount(currRate);
			    //accountLineNew.setLocalAmount(currRate);
			    /*
			     * Added new code for setters some field value as mentioned in Bug# 6124
			     * Ends here
			     */
	            accountLineNew.setNetworkSynchedId(accountLine.getId());
	            //accountLineNew.setBillToCode(billing.getNetworkBillToCode());
	            //accountLineNew.setBillToName(billing.getNetworkBillToName());
	            try{
		            if(accountLineNew.getBillToCode()!=null && (!(accountLineNew.getBillToCode().toString().equals(""))) && billingRecods.getBillToCode()!=null && (!(billingRecods.getBillToCode().toString().equals(""))) && accountLineNew.getBillToCode().toString().equals(billingRecods.getBillToCode().toString()) )	{
		            	if(!accountLineNew.getVATExclude()){
		            	accountLineNew.setRecVatDescr(billingRecods.getPrimaryVatCode());
			    		if(billingRecods.getPrimaryVatCode()!=null && (!(billingRecods.getPrimaryVatCode().toString().equals("")))){
				    		String recVatPercent="0";
				    		if(UTSIeuVatPercentList.containsKey(billingRecods.getPrimaryVatCode())){
					    		recVatPercent=UTSIeuVatPercentList.get(billingRecods.getPrimaryVatCode());
					    		}
				    		accountLineNew.setRecVatPercent(recVatPercent);
				    		}
		            	}	
		            }
		            }catch(Exception e){
		            	
		            }

	            accountLineNew.setAccountLineNumber(accountLine.getAccountLineNumber()); 
	            accountLineNew.setCompanyDivision(serviceOrderToRecods.getCompanyDivision());
	            List bookingAgentCodeList=companyDivisionManager.getBookingAgentCode(accountLine.getCompanyDivision(),sessionCorpID);
	            if(bookingAgentCodeList!=null && (!(bookingAgentCodeList.isEmpty())) && bookingAgentCodeList.get(0)!=null && (!(bookingAgentCodeList.get(0).toString().trim().equals(""))))
	            {
	            	accountLineNew.setVendorCode(bookingAgentCodeList.get(0).toString());
	            	List bookingAgentNamelist=	serviceOrderManager.findByBooking(bookingAgentCodeList.get(0).toString(), sessionCorpID);
	                
	            	if(bookingAgentNamelist!=null && (!(bookingAgentNamelist.isEmpty())) && bookingAgentNamelist.get(0)!=null && (!(bookingAgentNamelist.get(0).toString().trim().equals("")))){
		            accountLineNew.setEstimateVendorName(bookingAgentNamelist.get(0).toString());
	                }
	            	
	            }
	            //accountLineNew.setVendorCode(serviceOrderToRecods.getBookingAgentCode());
	            //accountLineNew.setEstimateVendorName(serviceOrderToRecods.getBookingAgentName());
	             String actCode="";
	             String  companyDivisionAcctgCodeUnique1="";
	    		 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(externalCorpID);
	    		 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
	    			 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
	    		 }
	            if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
	    			actCode=partnerManager.getAccountCrossReference(accountLineNew.getVendorCode(),serviceOrderToRecods.getCompanyDivision(),externalCorpID);
	    		}else{
	    			actCode=partnerManager.getAccountCrossReferenceUnique(accountLineNew.getVendorCode(),externalCorpID);
	    		}
	            accountLineNew.setActgCode(actCode);
	            
	            if(!costElementFlag){
	            List  agentGLList=accountLineManager.getGLList(accountLineNew.getChargeCode(),billingRecods.getContract(),externalCorpID);
				if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
					try{
					String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
					String recGl=GLarrayData[0];
					String payGl=GLarrayData[1];
					if(!(recGl.equalsIgnoreCase("NO"))){
						accountLineNew.setRecGl(recGl);
					}
					if(!(payGl.equalsIgnoreCase("NO"))){
						accountLineNew.setPayGl(payGl);
					}
					String VATExclude=GLarrayData[2];
					if(VATExclude.equalsIgnoreCase("Y")){
						accountLineNew.setVATExclude(true);
					}else{
						accountLineNew.setVATExclude(false);
					}
				}catch(Exception e){
					
				}
				}
	            }else{

	            	String chargeStr="";
					List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),accountLineNew.getChargeCode(),externalCorpID,serviceOrderToRecods.getJob(),serviceOrderToRecods.getRouting(),serviceOrderToRecods.getCompanyDivision());
					if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
						  chargeStr= glList.get(0).toString();
					  }
					   if(!chargeStr.equalsIgnoreCase("")){
						  String [] chrageDetailArr = chargeStr.split("#");
						  accountLineNew.setRecGl(chrageDetailArr[1]);
						  accountLineNew.setPayGl(chrageDetailArr[2]);
						}else{
							accountLineNew.setRecGl("");
							accountLineNew.setPayGl("");
					  }
	            
	            }
	            accountLineNew.setId(null);
	            accountLineNew.setCorpID(externalCorpID);
	            accountLineNew.setCreatedBy("Networking");
	            accountLineNew.setUpdatedBy("Networking");
	            accountLineNew.setCreatedOn(new Date());
	            accountLineNew.setUpdatedOn(new Date()); 
	           /* accountLineNew.setEstValueDate(new Date());
	           
	            double estExchangeRate=0;
	            BigDecimal estExchangeRateBig=new BigDecimal("1.0000");
	            List eRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstCurrency());
				 if((eRate!=null)&&(!eRate.isEmpty())&& eRate.get(0)!=null && (!(eRate.get(0).toString().equals(""))))
				 {
				 String exchangeRate1=eRate.get(0).toString();
				 estExchangeRate=Double.parseDouble(exchangeRate1);
				 }else{
					 estExchangeRate=1; 
				 }	
				estExchangeRateBig=new BigDecimal(estExchangeRate);	
			    accountLineNew.setEstExchangeRate(estExchangeRateBig);
	            try{ 
		            accountLineNew.setEstLocalRate(accountLineNew.getEstimateRate().multiply(estExchangeRateBig));
		            accountLineNew.setEstLocalAmount(accountLineNew.getEstimateExpense().multiply(estExchangeRateBig));
		            }catch(Exception e){
		            	
		            }
			    accountLineNew.setRevisionValueDate(new Date());
			    
			    double revisionExchangeRate=0; 
	            BigDecimal revisionExchangeRateBig=new BigDecimal("1.0000");
	            List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionCurrency());
				 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
				 {
				 String revisionExchangeRate1=revisionRate.get(0).toString();
				 revisionExchangeRate=Double.parseDouble(revisionExchangeRate1);
				 }else{
					 revisionExchangeRate=1; 
				 }	
				revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
				accountLineNew.setRevisionExchangeRate(revisionExchangeRateBig);*/
				
				/*try{ 
		            accountLineNew.setRevisionLocalRate(accountLineNew.getRevisionRate().multiply(revisionExchangeRateBig));
		            accountLineNew.setRevisionLocalAmount(accountLineNew.getRevisionExpense().multiply(revisionExchangeRateBig));
		            }catch(Exception e){
		            	
		            }*/
				/*String recExchangeRate="1"; 
	            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
	            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
				 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
				 {
					 recExchangeRate=recRate.get(0).toString();
				 }
				recExchangeRateBig=new BigDecimal(recExchangeRate);
				accountLineNew.setRecRateExchange(recExchangeRateBig);
				accountLineNew.setRecRate(accountLineNew.getRecCurrencyRate().divide(recExchangeRateBig,2));
				accountLineNew.setActualRevenue(accountLine.getActualRevenue().divide(recExchangeRateBig,2)); */
	            
	            
	            String estimatePayableContractExchangeRate="1"; 
	            BigDecimal estimatePayableContractExchangeRateBig=new BigDecimal("1.0000");
	            List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstimatePayableContractCurrency ());
				 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals(""))))
				 {
					 estimatePayableContractExchangeRate=estimatePayableContractRate.get(0).toString();
				 
				 }
				 estimatePayableContractExchangeRateBig=new BigDecimal(estimatePayableContractExchangeRate);	
				 accountLineNew.setEstimatePayableContractExchangeRate(estimatePayableContractExchangeRateBig);
				 accountLineNew.setEstimatePayableContractValueDate(new Date());
				 accountLineNew.setEstExchangeRate(estimatePayableContractExchangeRateBig);
				 accountLineNew.setEstValueDate(new Date());
				 if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
					  accountLineNew.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
				 }else{
				 accountLineNew.setEstimatePayableContractRateAmmount(accountLine.getEstSellLocalAmount());
				 }
				 accountLineNew.setEstimatePayableContractRate(accountLine.getEstSellLocalRate());
				 if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
					 accountLineNew.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
				 }else{
				 accountLineNew.setEstLocalAmount(accountLine.getEstSellLocalAmount());
				 }
				 accountLineNew.setEstLocalRate(accountLine.getEstSellLocalRate());
				 accountLineNew.setEstimateRate(new BigDecimal(decimalFormat.format(accountLine.getEstSellLocalRate().doubleValue()/accountLineNew.getEstimatePayableContractExchangeRate().doubleValue())));
				 accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format(accountLineNew.getEstimatePayableContractRateAmmount().doubleValue()/accountLineNew.getEstimatePayableContractExchangeRate().doubleValue())));
				
				 
				 
				 String estimateContractExchangeRate="1"; 
	            BigDecimal estimateContractExchangeRateBig=new BigDecimal("1.0000");
	            List estimateContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstimateContractCurrency ());
				 if((estimateContractRate!=null)&&(!estimateContractRate.isEmpty())&& estimateContractRate.get(0)!=null && (!(estimateContractRate.get(0).toString().equals(""))))
				 {
					 estimateContractExchangeRate=estimateContractRate.get(0).toString();
				 
				 } 
				 estimateContractExchangeRateBig=new BigDecimal(estimateContractExchangeRate);	
				 accountLineNew.setEstimateContractExchangeRate(estimateContractExchangeRateBig);
				 accountLineNew.setEstimateContractValueDate(new Date()); 
				 accountLineNew.setEstimateSellRate(new BigDecimal(decimalFormat.format(accountLineNew.getEstimateContractRate().doubleValue()/accountLineNew.getEstimateContractExchangeRate().doubleValue())));
				 if(accountLine.getEstimateSellDeviation()!=null && accountLineNew.getEstimateContractRateAmmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLineNew.getEstimateContractRateAmmount().doubleValue()>0){
					  accountLineNew.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateContractRateAmmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
					 }else{
						 
					 }
				 accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format(accountLineNew.getEstimateContractRateAmmount().doubleValue()/ accountLineNew.getEstimateContractExchangeRate().doubleValue())));
				 
				 
				String estSellExchangeRate="1"; 
		        BigDecimal estSellExchangeRateBig=new BigDecimal("1.0000");
	            List estSellRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstSellCurrency());
				 if((estSellRate!=null)&&(!estSellRate.isEmpty())&& estSellRate.get(0)!=null && (!(estSellRate.get(0).toString().equals(""))))
				 {
					 estSellExchangeRate=estSellRate.get(0).toString();
				 }
				 estSellExchangeRateBig=new BigDecimal(estSellExchangeRate);
				accountLineNew.setEstSellExchangeRate(estSellExchangeRateBig);
				accountLineNew.setEstSellValueDate(new Date());
				
				accountLineNew.setEstSellLocalRate(accountLineNew.getEstimateSellRate().multiply(estSellExchangeRateBig));
				accountLineNew.setEstSellLocalAmount(accountLineNew.getEstimateRevenueAmount().multiply(estSellExchangeRateBig));
				 
				 
				
				
				String revisionPayableContractExchangeRate="1"; 
	            BigDecimal revisionPayableContractExchangeRateBig=new BigDecimal("1.0000");
	            List revisionPayableContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionPayableContractCurrency ());
				 if((revisionPayableContractRate!=null)&&(!revisionPayableContractRate.isEmpty())&& revisionPayableContractRate.get(0)!=null && (!(revisionPayableContractRate.get(0).toString().equals(""))))
				 {
					 revisionPayableContractExchangeRate=revisionPayableContractRate.get(0).toString();
				 
				 }
				 revisionPayableContractExchangeRateBig=new BigDecimal(revisionPayableContractExchangeRate);	
				 accountLineNew.setRevisionPayableContractExchangeRate(revisionPayableContractExchangeRateBig);
				 accountLineNew.setRevisionPayableContractValueDate(new Date());
				 accountLineNew.setRevisionExchangeRate(revisionPayableContractExchangeRateBig);
				 accountLineNew.setRevisionValueDate(new Date());
				 if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
					  accountLineNew.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
				 }else{
				 accountLineNew.setRevisionPayableContractRateAmmount(accountLine.getRevisionSellLocalAmount());
				 }
				 accountLineNew.setRevisionPayableContractRate(accountLine.getRevisionSellLocalRate());
				 if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
					 accountLineNew.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
				 }else{
				 accountLineNew.setRevisionLocalAmount(accountLine.getRevisionSellLocalAmount());
				 }
				 accountLineNew.setRevisionLocalRate(accountLine.getRevisionSellLocalRate());
				 accountLineNew.setRevisionRate(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionPayableContractRate().doubleValue()/ accountLineNew.getRevisionPayableContractExchangeRate().doubleValue())));
				 accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionPayableContractRateAmmount().doubleValue()/ accountLineNew.getRevisionPayableContractExchangeRate().doubleValue())));
				
				String revisionContractExchangeRate="1"; 
	            BigDecimal revisionContractExchangeRateBig=new BigDecimal("1.0000");
	            List revisionContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionContractCurrency ());
				 if((revisionContractRate!=null)&&(!revisionContractRate.isEmpty())&& revisionContractRate.get(0)!=null && (!(revisionContractRate.get(0).toString().equals(""))))
				 {
					 revisionContractExchangeRate=revisionContractRate.get(0).toString();
				 
				 } 
				 revisionContractExchangeRateBig=new BigDecimal(revisionContractExchangeRate);	
				 accountLineNew.setRevisionContractExchangeRate(revisionContractExchangeRateBig);
				 accountLineNew.setRevisionContractValueDate(new Date());
				 accountLineNew.setRevisionSellRate(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionContractRate().doubleValue()/  accountLineNew.getRevisionContractExchangeRate().doubleValue())));
				 if(accountLine.getRevisionSellDeviation()!=null && accountLineNew.getRevisionContractRateAmmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLineNew.getRevisionContractRateAmmount().doubleValue()>0){
					  accountLineNew.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionContractRateAmmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue())));	 
					 }else{
						 
					 }
				 accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionContractRateAmmount().doubleValue()/  accountLineNew.getRevisionContractExchangeRate().doubleValue())));
				 
				 
				String revisionSellExchangeRate="1"; 
		        BigDecimal revisionSellExchangeRateBig=new BigDecimal("1.0000");
	            List revisionSellRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionSellCurrency());
				 if((revisionSellRate!=null)&&(!revisionSellRate.isEmpty())&& revisionSellRate.get(0)!=null && (!(revisionSellRate.get(0).toString().equals(""))))
				 {
					 revisionSellExchangeRate=estSellRate.get(0).toString();
				 }
				 revisionSellExchangeRateBig=new BigDecimal(revisionSellExchangeRate);
				 accountLineNew.setRevisionSellExchangeRate(revisionSellExchangeRateBig);
				 accountLineNew.setRevisionSellValueDate(new Date());
				 
				 accountLineNew.setRevisionSellLocalRate(accountLineNew.getRevisionSellRate().multiply(revisionSellExchangeRateBig));
				 accountLineNew.setRevisionSellLocalAmount(accountLineNew.getRevisionRevenueAmount().multiply(revisionSellExchangeRateBig));
				
				 
				  
				String contractPayableExchangeRate="1"; 
		        BigDecimal contractPayableExchangeRateBig=new BigDecimal("1.0000");
		        List contractPayableRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getPayableContractCurrency ());
				if((contractPayableRate!=null)&&(!contractPayableRate.isEmpty())&& contractPayableRate.get(0)!=null && (!(contractPayableRate.get(0).toString().equals(""))))
					 {
					contractPayableExchangeRate=contractPayableRate.get(0).toString();
					 
					 } 
				contractPayableExchangeRateBig=new BigDecimal(contractPayableExchangeRate);
				accountLineNew.setPayableContractExchangeRate(contractPayableExchangeRateBig);
				accountLineNew.setPayableContractValueDate(new Date()); 
				accountLineNew.setValueDate(new Date());
			    double payExchangeRate=0; 
	            //BigDecimal payExchangeRateBig=new BigDecimal("1.0000");  
				payExchangeRate=Double.parseDouble(contractPayableExchangeRate);
				accountLineNew.setExchangeRate(payExchangeRate);
				if(accountLine.getReceivableSellDeviation()!=null && accountLine.getActualRevenueForeign()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLine.getActualRevenueForeign().doubleValue()>0){
					accountLineNew.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));
					accountLineNew.setLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	
				}else{
				accountLineNew.setPayableContractRateAmmount(accountLine.getActualRevenueForeign());
				accountLineNew.setLocalAmount(accountLine.getActualRevenueForeign());
				}
				try{
					accountLineNew.setActualExpense(new BigDecimal(decimalFormat.format(accountLineNew.getPayableContractRateAmmount().doubleValue()/accountLineNew.getPayableContractExchangeRate().doubleValue())));
				}catch(Exception e){
		    			
		    	}
					//payExchangeRateBig=new BigDecimal(payExchangeRate);
				 
				 
			    String contractExchangeRate="1"; 
	            BigDecimal contractExchangeRateBig=new BigDecimal("1.0000");
	            List contractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getContractCurrency ());
				 if((contractRate!=null)&&(!contractRate.isEmpty())&& contractRate.get(0)!=null && (!(contractRate.get(0).toString().equals(""))))
				 {
					 contractExchangeRate=contractRate.get(0).toString();
				 
				 } 
				contractExchangeRateBig=new BigDecimal(contractExchangeRate);	
				accountLineNew.setContractExchangeRate(contractExchangeRateBig);
				accountLineNew.setContractValueDate(new Date()); 
				accountLineNew.setRecRate(new BigDecimal(decimalFormat.format(accountLineNew.getContractRate().doubleValue()/accountLineNew.getContractExchangeRate().doubleValue())));
				if(accountLine.getReceivableSellDeviation()!=null && accountLineNew.getContractRateAmmount()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLineNew.getContractRateAmmount().doubleValue()>0){
					  accountLineNew.setContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getContractRateAmmount().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	 
					 }else{
						 
					 }
				accountLineNew.setActualRevenue(new BigDecimal(decimalFormat.format(accountLineNew.getContractRateAmmount().doubleValue()/accountLineNew.getContractExchangeRate().doubleValue())));
				accountLineNew.setRacValueDate(new Date());
				String recExchangeRate="1"; 
	            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
	            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
				 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
				 {
					 recExchangeRate=recRate.get(0).toString();
				 }
				recExchangeRateBig=new BigDecimal(recExchangeRate);
				accountLineNew.setRecRateExchange(recExchangeRateBig);
				accountLineNew.setRacValueDate(new Date());
				accountLineNew.setRecCurrencyRate(accountLineNew.getRecRate().multiply(recExchangeRateBig));
				accountLineNew.setActualRevenueForeign(accountLineNew.getActualRevenue().multiply(recExchangeRateBig));
				try{
					if(accountLineNew.getEstimateSellDeviation()!=null && accountLineNew.getEstimateSellDeviation().doubleValue()>0 ){
						if(accountLineNew.getEstimateRevenueAmount()!=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0)
							accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateRevenueAmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));
						if(accountLineNew.getEstSellLocalAmount()!=null && accountLineNew.getEstSellLocalAmount().doubleValue()>0)
							accountLineNew.setEstSellLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstSellLocalAmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));
						if(accountLineNew.getEstimateContractRateAmmount()!=null && accountLineNew.getEstimateContractRateAmmount().doubleValue()>0)
							accountLineNew.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateContractRateAmmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getEstimateDeviation()!=null && accountLineNew.getEstimateDeviation().doubleValue()>0 ){
						if(accountLineNew.getEstimateExpense()!=null && accountLineNew.getEstimateExpense().doubleValue()>0)
							accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateExpense().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));
						if(accountLineNew.getEstLocalAmount()!=null && accountLineNew.getEstLocalAmount().doubleValue()>0)
							accountLineNew.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstLocalAmount().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));
						if(accountLineNew.getEstimatePayableContractRateAmmount()!=null && accountLineNew.getEstimatePayableContractRateAmmount().doubleValue()>0)
							accountLineNew.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimatePayableContractRateAmmount().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getRevisionSellDeviation()!=null && accountLineNew.getRevisionSellDeviation().doubleValue()>0 ){
						if(accountLineNew.getRevisionRevenueAmount()!=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0)
							accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionRevenueAmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionSellLocalAmount()!=null && accountLineNew.getRevisionSellLocalAmount().doubleValue()>0)
							accountLineNew.setRevisionSellLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionSellLocalAmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionContractRateAmmount()!=null && accountLineNew.getRevisionContractRateAmmount().doubleValue()>0)
							accountLineNew.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionContractRateAmmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getRevisionDeviation()!=null && accountLineNew.getRevisionDeviation().doubleValue()>0 ){
						if(accountLineNew.getRevisionExpense()!=null && accountLineNew.getRevisionExpense().doubleValue()>0)
							accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionExpense().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionLocalAmount()!=null && accountLineNew.getRevisionLocalAmount().doubleValue()>0)
							accountLineNew.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionLocalAmount().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionPayableContractRateAmmount()!=null && accountLineNew.getRevisionPayableContractRateAmmount().doubleValue()>0)
							accountLineNew.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionPayableContractRateAmmount().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getReceivableSellDeviation()!=null && accountLineNew.getReceivableSellDeviation().doubleValue()>0 ){
						if(accountLineNew.getActualRevenue()!=null && accountLineNew.getActualRevenue().doubleValue()>0)
							accountLineNew.setActualRevenue(new BigDecimal(decimalFormat.format((accountLineNew.getActualRevenue().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));
						if(accountLineNew.getActualRevenueForeign()!=null && accountLineNew.getActualRevenueForeign().doubleValue()>0)
							accountLineNew.setActualRevenueForeign(new BigDecimal(decimalFormat.format((accountLineNew.getActualRevenueForeign().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));
						if(accountLineNew.getContractRateAmmount()!=null && accountLineNew.getContractRateAmmount().doubleValue()>0)
							accountLineNew.setContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getContractRateAmmount().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getPayDeviation()!=null && accountLineNew.getPayDeviation().doubleValue()>0 ){
						if(accountLineNew.getActualExpense()!=null && accountLineNew.getActualExpense().doubleValue()>0)
							accountLineNew.setActualExpense(new BigDecimal(decimalFormat.format((accountLineNew.getActualExpense().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));
						if(accountLineNew.getLocalAmount()!=null && accountLineNew.getLocalAmount().doubleValue()>0)
							accountLineNew.setLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getLocalAmount().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));
						if(accountLineNew.getPayableContractRateAmmount()!=null && accountLineNew.getPayableContractRateAmmount().doubleValue()>0)
							accountLineNew.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getPayableContractRateAmmount().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));	
					} 
				
}catch(Exception e){
	    			
	    		} 
				
				
				  try{
			    		if(accountLineNew.getEstimateRevenueAmount() !=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0 && accountLineNew.getEstimateExpense()!=null && accountLineNew.getEstimateExpense().doubleValue()>0){
			    			Double estimatePassPercentageValue=(accountLineNew.getEstimateRevenueAmount().doubleValue()/accountLineNew.getEstimateExpense().doubleValue())*100;
			    			Integer estimatePassPercentage=estimatePassPercentageValue.intValue();
			    			accountLineNew.setEstimatePassPercentage(estimatePassPercentage);
				            }
				            if(accountLineNew.getRevisionRevenueAmount() !=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0 && accountLineNew.getRevisionExpense()!=null &&  accountLineNew.getRevisionExpense().doubleValue()>0){
				            	Double revisionPassPercentageValue=(accountLineNew.getRevisionRevenueAmount().doubleValue()/accountLineNew.getRevisionExpense().doubleValue())*100;
				            	Integer revisionPassPercentage=revisionPassPercentageValue.intValue();
				            	accountLineNew.setRevisionPassPercentage(revisionPassPercentage);
				            }
			    		}catch(Exception e){
			    			
			    		}	
				 
			    try{
			    	
			    	
			    String payVatPercent=accountLineNew.getPayVatPercent();
			    BigDecimal payVatPercentBig=new BigDecimal("0.00");
			    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
			    BigDecimal estPayVatAmmountBig=new BigDecimal("0.00");
				BigDecimal revisionpayVatAmmountBig=new BigDecimal("0.00");
			    if(payVatPercent!=null && (!(payVatPercent.trim().equals("")))){
			    	payVatPercentBig=new BigDecimal(payVatPercent);
			    }
			    payVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getLocalAmount().multiply(payVatPercentBig).doubleValue())/100))));
			    try{
				    estPayVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getEstLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
				    revisionpayVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getRevisionLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
				    
				  }catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    }
			    accountLineNew.setPayVatAmt(payVatAmmountBig);
			    accountLineNew.setEstExpVatAmt(estPayVatAmmountBig);
			    accountLineNew.setRevisionExpVatAmt(revisionpayVatAmmountBig);
			  
			    }catch(Exception e){
			    	
			    }
	            accountLineNew.setServiceOrderId(serviceOrderToRecods.getId());
	            accountLineNew.setSequenceNumber(serviceOrderToRecods.getSequenceNumber());
	            accountLineNew.setShipNumber(serviceOrderToRecods.getShipNumber());
	            accountLineNew.setServiceOrder(serviceOrderToRecods);
	            try{
	            	String recVatPercent="0";
	            	recVatPercent=accountLineNew.getRecVatPercent();
				    BigDecimal recVatPercentBig=new BigDecimal("0.00");
				    BigDecimal recVatAmmountBig=new BigDecimal("0.00");
				    BigDecimal revisionVatAmmountBig=new BigDecimal("0.00");
				    BigDecimal estVatAmmountBig=new BigDecimal("0.00");
				    if(recVatPercent!=null && (!(recVatPercent.trim().equals("")))){
				    	recVatPercentBig=new BigDecimal(recVatPercent);
				    }
				    DecimalFormat decimalFormat = new DecimalFormat("#.####");
				    recVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getActualRevenueForeign().multiply(recVatPercentBig)).doubleValue())/100)));
				    try{
					    estVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getEstSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
					    revisionVatAmmountBig = (new BigDecimal(decimalFormat.format(((accountLineNew.getRevisionSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
					    }catch(Exception e){
					    	
					    }
				    accountLineNew.setRecVatAmt(recVatAmmountBig); 
				    accountLineNew.setEstVatAmt(estVatAmmountBig); 
				    accountLineNew.setRevisionVatAmt(revisionVatAmmountBig); 
				    }catch(Exception e){
				    	
				    } 
	            accountLineNew=accountLineManager.save(accountLineNew);
	            
        	}
    		}catch(Exception e){
    			
    		}
    	
	}
	
	@SkipValidation
    public void updateExternalAccountLine( ServiceOrder externalServiceOrder,String externalCorpID){ 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		BigDecimal cuDivisormar=new BigDecimal(100);
    	 
    	externalServiceOrder.setEstimatedTotalRevenue(new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
    	externalServiceOrder.setDistributedTotalAmount(new BigDecimal((accountLineManager.getExternaldistributedAmountSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
    	externalServiceOrder.setProjectedDistributedTotalAmount(new BigDecimal((accountLineManager.getExternalProjectedDistributedTotalAmountSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
    	externalServiceOrder.setProjectedActualExpense(new BigDecimal((accountLineManager.getExternalProjectedActualExpenseSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
    	externalServiceOrder.setProjectedActualRevenue(new BigDecimal((accountLineManager.getExternalProjectedActualRevenueSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
		BigDecimal divisormarEst=new BigDecimal(((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())); 
		if(externalServiceOrder.getEstimatedTotalRevenue()==null || externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00")) || externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.0000"))){
			externalServiceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
		} 
		else 
		{
			externalServiceOrder.setEstimatedGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEst,2));
		} 
		externalServiceOrder.setEstimatedGrossMargin((new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
		externalServiceOrder.setRevisedGrossMargin((new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
		externalServiceOrder.setRevisedTotalRevenue(new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
		BigDecimal divisormarEstPer=new BigDecimal(((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
		if(externalServiceOrder.getRevisedTotalRevenue() == null || externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00")) || externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.0000"))){
			externalServiceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
		}
		else{
			externalServiceOrder.setRevisedGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEstPer,2));
		} 
		externalServiceOrder.setRevisedTotalExpense(new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
		externalServiceOrder.setEstimatedTotalExpense(new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
		externalServiceOrder.setEntitledTotalAmount(new BigDecimal((accountLineManager.getExternalEntitleSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
		externalServiceOrder.setActualExpense(new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
		externalServiceOrder.setActualRevenue(new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
		externalServiceOrder.setActualGrossMargin((new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
		BigDecimal divisormar=new BigDecimal(((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
		if(externalServiceOrder.getActualRevenue()== null || externalServiceOrder.getActualRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getActualRevenue().equals(new BigDecimal("0.00")) || externalServiceOrder.getActualRevenue().equals(new BigDecimal("0.0000"))){
			externalServiceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
		}
		else{
			externalServiceOrder.setActualGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormar,2));
		}
		/*# 7784 - Always show actual gross margin in accountline overview End*/


		/*# 7784 - Always show actual gross margin in accountline overview Start*/
		try{
			externalServiceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"EXP")));
		if(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV").doubleValue()!=0){
			externalServiceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((externalServiceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV")).doubleValue())*100));
		}else{
			externalServiceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
		}
		}catch(Exception e){}
		/*# 7784 - Always show actual gross margin in accountline overview End*/
		
		serviceOrderManager.updateFromAccountLine(externalServiceOrder); 
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");  
    } 
	
	private void createContainerRecords(ServiceOrder serviceOrder, ServiceOrder serviceOrderNew) {
		List containerRecordList=containerManager.containerList(serviceOrder.getId());
		Iterator<Container> it=containerRecordList.iterator();
		while(it.hasNext()){
			Container containerFrom=it.next();
			Container containerNew= new Container();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(containerNew, containerFrom);
				
				 containerNew.setId(null);
				 containerNew.setCorpID(serviceOrderNew.getCorpID());
				 containerNew.setShipNumber(serviceOrderNew.getShipNumber());
				 containerNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 containerNew.setServiceOrder(serviceOrderNew);
				 containerNew.setServiceOrderId(serviceOrderNew.getId());
				 containerNew.setCreatedBy("Networking");
				 containerNew.setUpdatedBy("Networking");
				 containerNew.setCreatedOn(new Date());
				 containerNew.setUpdatedOn(new Date());
				 containerNew= containerManager.save(containerNew); 
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
	}
	private void createPieceCountsRecords(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
		List cartonRecordList=cartonManager.cartonList(serviceOrder.getId());
		Iterator<Carton> it=cartonRecordList.iterator();
		while(it.hasNext()){
			Carton cartonFrom=it.next();
			Carton cartonNew= new Carton();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(cartonNew, cartonFrom);
				 cartonNew.setId(null);
				 cartonNew.setCorpID(serviceOrderNew.getCorpID());
				 cartonNew.setShipNumber(serviceOrderNew.getShipNumber());
				 cartonNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 cartonNew.setServiceOrder(serviceOrderNew);
				 cartonNew.setServiceOrderId(serviceOrderNew.getId());
				 cartonNew.setCreatedBy("Networking");
				 cartonNew.setUpdatedBy("Networking");
				 cartonNew.setCreatedOn(new Date());
				 cartonNew.setUpdatedOn(new Date()); 
				 cartonNew=cartonManager.save(cartonNew);
				  
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
	}
	private void createVehicleRecords(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
		List vehicleRecordList=vehicleManager.vehicleList(serviceOrder.getId());
		Iterator<Vehicle> it=vehicleRecordList.iterator();
		while(it.hasNext()){
			Vehicle vehicleFrom=it.next();
			Vehicle vehicleNew= new Vehicle();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(vehicleNew, vehicleFrom);
				 vehicleNew.setId(null);
				 vehicleNew.setCorpID(serviceOrderNew.getCorpID());
				 vehicleNew.setShipNumber(serviceOrderNew.getShipNumber());
				 vehicleNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 vehicleNew.setServiceOrder(serviceOrderNew);
				 vehicleNew.setServiceOrderId(serviceOrderNew.getId());
				 vehicleNew.setCreatedBy("Networking");
				 vehicleNew.setUpdatedBy("Networking");
				 vehicleNew.setCreatedOn(new Date());
				 vehicleNew.setUpdatedOn(new Date()); 
				 vehicleNew= vehicleManager.save(vehicleNew); 
				 
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
		
	}
	private void createRoutingRecords(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
		List routingRecordList=servicePartnerManager.routingList(serviceOrder.getId());
		Iterator<ServicePartner> it=routingRecordList.iterator();
		while(it.hasNext()){
			ServicePartner servicePartnerFrom= it.next();
			ServicePartner servicePartnerNew= new ServicePartner();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(servicePartnerNew, servicePartnerFrom);
				 
				 servicePartnerNew.setId(null);
				 servicePartnerNew.setCorpID(serviceOrderNew.getCorpID());
				 servicePartnerNew.setCarrierCode("T10000");
				 servicePartnerNew.setShipNumber(serviceOrderNew.getShipNumber());
				 servicePartnerNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 servicePartnerNew.setServiceOrder(serviceOrderNew);
				 servicePartnerNew.setServiceOrderId(serviceOrderNew.getId());
				 servicePartnerNew.setCreatedBy("Networking");
				 servicePartnerNew.setUpdatedBy("Networking");
				 servicePartnerNew.setCreatedOn(new Date());
				 servicePartnerNew.setUpdatedOn(new Date()); 
				 servicePartnerNew=servicePartnerManager.save(servicePartnerNew); 
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
	}
	private void createConsigneeInstruction(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
		List consigneeInstructionRecordList=consigneeInstructionManager.consigneeInstructionList(serviceOrder.getId());
		if(!consigneeInstructionRecordList.isEmpty()){
			ConsigneeInstruction consigneeInstructionFrom=(ConsigneeInstruction)consigneeInstructionRecordList.get(0);
			ConsigneeInstruction consigneeInstructionNew= new ConsigneeInstruction();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(consigneeInstructionNew, consigneeInstructionFrom);
				 
				 consigneeInstructionNew.setId(serviceOrderNew.getId());
				 consigneeInstructionNew.setCorpID(serviceOrderNew.getCorpID());
				 consigneeInstructionNew.setShipNumber(serviceOrderNew.getShipNumber());
				 consigneeInstructionNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 consigneeInstructionNew.setShip(serviceOrderNew.getShip());
				 consigneeInstructionNew.setServiceOrderId(serviceOrderNew.getId());
				 consigneeInstructionNew.setCreatedBy("Networking");
				 consigneeInstructionNew.setUpdatedBy("Networking");
				 consigneeInstructionNew.setCreatedOn(new Date());
				 consigneeInstructionNew.setUpdatedOn(new Date());
				 consigneeInstructionNew = consigneeInstructionManager.save(consigneeInstructionNew); 
				 
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
			
		}
		
	}
	
	private void createFamilyDetail(CustomerFile customerFile,CustomerFile customerFileNew) {
		List dsFamilyDetailsList=dsFamilyDetailsManager.getDsFamilyDetailsList(customerFile.getId(),sessionCorpID);
		if(!dsFamilyDetailsList.isEmpty()){ 
			Iterator<DsFamilyDetails> it=dsFamilyDetailsList.iterator();
			while(it.hasNext()){
				DsFamilyDetails dsFamilyDetails = it.next(); 
			try{

				DsFamilyDetails dsFamilyDetailsNew = new DsFamilyDetails();
				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtilsBean.copyProperties(dsFamilyDetailsNew, dsFamilyDetails);
				
				dsFamilyDetailsNew.setCreatedBy("Networking");
				dsFamilyDetailsNew.setUpdatedBy("Networking");
				dsFamilyDetailsNew.setCreatedOn(new Date());
				dsFamilyDetailsNew.setUpdatedOn(new Date());
				dsFamilyDetailsNew.setCustomerFileId(customerFileNew.getId());
				dsFamilyDetailsNew.setCorpID(customerFileNew.getCorpID());
				dsFamilyDetailsNew.setNetworkId(dsFamilyDetails.getId());
				dsFamilyDetailsNew.setId(null);
				dsFamilyDetailsNew = dsFamilyDetailsManager.save(dsFamilyDetailsNew);
				
				//dsFamilyDetails.setNetworkId(dsFamilyDetails.getId());
				//dsFamilyDetails=dsFamilyDetailsManager.save(dsFamilyDetails);
				int i=dsFamilyDetailsManager.updateFamilyDetailsNetworkId(dsFamilyDetails.getId());
			
				 
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
			}
		}
		
	}
	
	public void createClaim(ServiceOrder serviceOrder, ServiceOrder serviceOrderNew){
		trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrderNew.getId());
		if(trackingStatusToRecod.getSoNetworkGroup()){
			List <Claim> claimRecordList = claimManager.claimList(serviceOrder.getId()); 
			for(Claim claimFrom : claimRecordList ){
				
				Claim claimNew= new Claim();
				try{
					Long newClaimNumber = claimManager.findMaximumForOtherCorpid(serviceOrderNew.getCorpID());
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(claimNew, claimFrom);
					
					 claimNew.setId(null);
					 claimNew.setCorpID(serviceOrderNew.getCorpID());
					 claimNew.setShipNumber(serviceOrderNew.getShipNumber());
					 claimNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
					 claimNew.setServiceOrder(serviceOrderNew);
					 claimNew.setServiceOrderId(serviceOrderNew.getId());
					 claimNew.setCreatedBy("Networking");
					 claimNew.setUpdatedBy("Networking");
					 claimNew.setCreatedOn(new Date());
					 claimNew.setUpdatedOn(new Date());
					 claimNew.setClaimNumber(newClaimNumber);
					 try{
					 List  l1 = claimManager.findPartnerCodeOtherCorpID(claimFrom.getInsurerCode(),sessionCorpID);
						if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
							Boolean agentChk   = (Boolean) l1.get(0);
								if(!agentChk){
									claimNew.setInsurerCode("");
									claimNew.setInsurer("");
								}
						}else{
							claimNew.setInsurerCode("");
							claimNew.setInsurer("");
						}
					 }catch(Exception e){
						 logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						 claimNew.setInsurerCode("");
						 claimNew.setInsurer("");
					 }
					 claimNew= claimManager.save(claimNew);
					
					 
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			try{ 
				List <Loss> lossRecordList = lossManager.lossList(claimFrom.getId()); 
				for(Loss lossFrom : lossRecordList ){
					try{
					Loss lossNew= new Loss();
					List  maxIdNumber1 = lossManager.findMaximumIdNumber1(serviceOrderNew.getCorpID());
					if (maxIdNumber1== null ||  maxIdNumber1.get(0) == null ) {          
					String	idNumber1 = "01";
					}else {
						autoIdNumber1 = Integer.parseInt(maxIdNumber1.get(0).toString()) + 1;
					}
					BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				    beanUtilsBean2.copyProperties(lossNew, lossFrom);
				    lossNew.setIdNumber1(autoIdNumber1);
				    
				    lossNew.setCorpID(serviceOrderNew.getCorpID());
				    lossNew.setId(null);
					lossNew.setShipNumber(serviceOrderNew.getShipNumber());
					lossNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
					lossNew.setCreatedBy("Networking");
					lossNew.setWarehouse("");
					lossNew.setUpdatedBy("Networking");
					lossNew.setCreatedOn(new Date());
				    lossNew.setUpdatedOn(new Date()); 
				    lossNew.setClaimId(claimNew.getId().toString());
				    lossNew.setClaimNumber(claimNew.getClaimNumber());
				    lossNew.setClaim(claimNew);
				    lossNew= lossManager.save(lossNew);	
					}catch(Exception ex){
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
				}	
			}catch(Exception ex){
				logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
			}
			}
       	}
	}
	
	private void createAdAddressesDetails(CustomerFile customerFile,CustomerFile customerFileNew) {
		List adAddressesDetailsList=adAddressesDetailsManager.getAdAddressesDetailsList(customerFile.getId(),sessionCorpID);
		if(!adAddressesDetailsList.isEmpty()){
			Iterator<AdAddressesDetails> it=adAddressesDetailsList.iterator();
			while(it.hasNext()){ 
			AdAddressesDetails adAddressesDetails= it.next();
			try{
				AdAddressesDetails adAddressesDetailsNew = new AdAddressesDetails();
				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtilsBean.copyProperties(adAddressesDetailsNew, adAddressesDetails);
				
				adAddressesDetailsNew.setCreatedBy("Networking");
				adAddressesDetailsNew.setUpdatedBy("Networking");
				adAddressesDetailsNew.setCreatedOn(new Date());
				adAddressesDetailsNew.setUpdatedOn(new Date());
				adAddressesDetailsNew.setCustomerFileId(customerFileNew.getId());
				adAddressesDetailsNew.setCorpID(customerFileNew.getCorpID());
				adAddressesDetailsNew.setNetworkId(adAddressesDetails.getId());
				adAddressesDetailsNew.setId(null);
				adAddressesDetailsNew = adAddressesDetailsManager.save(adAddressesDetailsNew);
				
				//adAddressesDetails.setNetworkId(adAddressesDetails.getId());
				//adAddressesDetails=adAddressesDetailsManager.save(adAddressesDetails);
				int i=adAddressesDetailsManager.updateAdAddressesDetailsNetworkId(adAddressesDetails.getId());
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
			}
		}
		
	}
	public void createRemovalRelocationServiceDetails(CustomerFile customerFile,CustomerFile customerFileNew) {
		try{
		String removalReloCheck=customerFileManager.findRemovalReloCheck(customerFile.getId().toString());
		 if(removalReloCheck.equalsIgnoreCase("true")){
		 removalRelocationService=removalRelocationServiceManager.get(customerFile.getId()); 
		 RemovalRelocationService removalRelocationServiceNew = new RemovalRelocationService();
				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtilsBean.copyProperties(removalRelocationServiceNew, removalRelocationService);
				
				removalRelocationServiceNew.setCreatedBy("Networking");
				removalRelocationServiceNew.setUpdatedBy("Networking");
				removalRelocationServiceNew.setCreatedOn(new Date());
				removalRelocationServiceNew.setUpdatedOn(new Date());
				removalRelocationServiceNew.setCustomerFileId(customerFileNew.getId());
				removalRelocationServiceNew.setId(customerFileNew.getId()) ;
				removalRelocationServiceNew.setCorpId(customerFileNew.getCorpID());
				 
				removalRelocationServiceNew = removalRelocationServiceManager.save(removalRelocationServiceNew); 
				 
			
			
			}
		}catch(Exception ex){
			logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
		
	}
	}
	
	@SkipValidation
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
		String s = save();
		return SUCCESS;
	}	
	private String partnerCode="";
	private String userName="";
	private String userEmailId="";
	private String prifix="";
	private List userContactList = new ArrayList();
	@SkipValidation
	public String userContacts() {
		try {
			userContactList = userManager.getContactListByPartnerCode(partnerCode, sessionCorpID);
		} catch (Exception e) {
			
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
//		userContactList = dataSecuritySetManager.getAgentsUsersList(partnerCode, sessionCorpID,"TRACKINGSTATUS");
		return SUCCESS;
	}
	@SkipValidation
	public String searchStatusBookingAgent(){
		try {
			userContactList = userManager.getSearchContactListByPartnerCode(partnerCode, sessionCorpID,userName,userEmailId);
		} catch (Exception e) {			
	    	 e.printStackTrace();	    	
		}
		return SUCCESS;
	}
	private String countDSAutomobileAssistanceNotes;
	private String countDSColaServiceNotes;
	private String countDSCrossCulturalTrainingNotes;
	private String countDSHomeFindingAssistancePurchaseNotes;
	
	private String countDsHomeRentalNotes;
	private String countDSLanguageNotes;
	private String countDSMoveMgmtNotes;
	private String countDSOngoingSupportNotes;
	private String countDSPreviewTripNotes;
	private String countDsRelocationAreaInfoOrientationNotes;
	private String countDSRelocationExpenseManagementNotes;
	private String countDSRepatriationNotes;
	private String countDSSchoolEducationalCounselingNotes;
	private String countdsTaxServicesNotes;
	private String countDSTemporaryAccommodationNotes;
	private String countDSTenancyManagementNotes;
	private String countDSVisaImmigrationNotes;
	private String countDSSettlingInNotes;
	private String countDSWorkPermitNotes;
	private String countDSResidencePermitNotes;
	private String countDSRentalSearchNotes; 
	
	private String countDSCandidateAssessmentNotes; 
	private String countDSClosingServicesNotes; 
	private String countDSComparableHousingStudyNotes; 
	private String countDSDepartureServicesNotes; 
	private String countDSHomeSaleMarketingAssistanceNotes; 
	private String countDSPolicyDevelopmentNotes; 
	private String countDSRelocationCostProjectionsNotes; 
	private String countDSSpousalAssistanceNotes; 
	private String countDSTechnologySolutionsNotes; 
	private String countDSMortgageServicesNotes; 
	private String countDSDestinationsSchoolServicesNotes; 
	private String countDsHotelBookingsNotes;
	private String countDsFlightBookingsNotes;
	
	private String countDSFurnitureRentalNotes;
	private String countDSAirportPickUpNotes;
	@SkipValidation
	public String getNotesForIconChange() {
		List noteDSDsAutomobileAssistan = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsAutomobileAssistan");
		List noteDSDscola = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsCola");
		List noteDSDscrossculturaltrain = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsCrossCulturalTrain");
		
		List noteDSDshomepurchase = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsHomePurchase");
		
		List noteDSDshomerental = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsHomeRental");
		List noteDSDslanguage = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsLanguage");
		List noteDSDsMoveMgmt = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsMoveMgmt");
		List noteDSDsOngoingSupport = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsOngoingSupport");
		List noteDSPreview = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"Preview");
		List noteDSDsorientation = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsOrientation");
		List noteDSDsexpensemanagement = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsExpenseManagement");
		List noteDSDsrepatriation = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsRepatriation");
		List noteDSDsschooling = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsSchooling");
		List noteDSDstaxes = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsTaxes");
		List noteDSDstempaccomodation = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsTempAccomodation");
		List noteDSDsTenancyManagement = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsTenancyManagement");
		List noteDSVisaImmigration = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"VisaImmigration");
		List noteDSSettlingIn = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsSettlingIn");
		List noteDSWorkPermit = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsWorkPermit");
		List noteDSResidencePermit = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsResidencePermit");
		List noteDSRentalSearch = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsRentalSearch");

		
		List noteDSCandidateAssessment = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsCandidateAssessment");
		List noteDSClosingServices = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsClosingServices");
		List noteDSComparableHousingStudy = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsComparableHousingStudy");
		List noteDSDepartureServices = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsDepartureServices");
		List noteDSHomeSaleMarketingAssistance = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsHomeSaleMarketingAssistance");
		List noteDSPolicyDevelopment = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsPolicyDevelopment");
		List noteDSRelocationCostProjections = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsRelocationCostProjections");
		List noteDSSpousalAssistance = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsSpousalAssistance");
		List noteDSTechnologySolutions = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsTechnologySolutions");
		List noteDSMortgageServices = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsMortgageServices");
		List noteDSDestinationsSchoolServices = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsDestinationsSchoolServices");
		List noteDsHotelBookings = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsHotelBookings");
		List noteDsFlightBookings = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsFlightBookings");
		List noteDSDsFurnitureRental = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsFurnitureRental");
		List noteDSDsAirportPickUp = notesManager.countForDspDetailsNotes(serviceOrder.getShipNumber(),"DsAirportPickUp");
		if (noteDSDsAutomobileAssistan.isEmpty()) {
			countDSAutomobileAssistanceNotes = "0";
		} else {
			countDSAutomobileAssistanceNotes = ((noteDSDsAutomobileAssistan).get(0)).toString();
		}
		if (noteDSDscola.isEmpty()) {
			countDSColaServiceNotes = "0";
		} else {
			countDSColaServiceNotes = ((noteDSDscola).get(0)).toString();
		}
		if (noteDSDscrossculturaltrain.isEmpty()) {
			countDSCrossCulturalTrainingNotes = "0";
		} else {
			countDSCrossCulturalTrainingNotes = ((noteDSDscrossculturaltrain).get(0)).toString();
		}
		
		if (noteDSDshomepurchase.isEmpty()) {
			countDSHomeFindingAssistancePurchaseNotes = "0";
		} else {
			countDSHomeFindingAssistancePurchaseNotes = ((noteDSDshomepurchase).get(0)).toString();
		}
		
		if (noteDSDshomerental.isEmpty()) {
			countDsHomeRentalNotes = "0";
		} else {
			countDsHomeRentalNotes = ((noteDSDshomerental).get(0)).toString();
		}
		if (noteDSDslanguage.isEmpty()) {
			countDSLanguageNotes = "0";
		} else {
			countDSLanguageNotes = ((noteDSDslanguage).get(0)).toString();
		}
		if (noteDSDsMoveMgmt.isEmpty()) {
			countDSMoveMgmtNotes = "0";
		} else {
			countDSMoveMgmtNotes = ((noteDSDsMoveMgmt).get(0)).toString();
		}
		if (noteDSDsOngoingSupport.isEmpty()) {
			countDSOngoingSupportNotes = "0";
		} else {
			countDSOngoingSupportNotes = ((noteDSDsOngoingSupport).get(0)).toString();
		}
		if (noteDSPreview.isEmpty()) {
			countDSPreviewTripNotes = "0";
		} else {
			countDSPreviewTripNotes = ((noteDSPreview).get(0)).toString();
		}
		if (noteDSDsorientation.isEmpty()) {
			countDsRelocationAreaInfoOrientationNotes = "0";
		} else {
			countDsRelocationAreaInfoOrientationNotes = ((noteDSDsorientation).get(0)).toString();
		}
		if (noteDSDsexpensemanagement.isEmpty()) {
			countDSRelocationExpenseManagementNotes = "0";
		} else {
			countDSRelocationExpenseManagementNotes = ((noteDSDsexpensemanagement).get(0)).toString();
		}
		if (noteDSDsrepatriation.isEmpty()) {
			countDSRepatriationNotes = "0";
		} else {
			countDSRepatriationNotes = ((noteDSDsrepatriation).get(0)).toString();
		}
		if (noteDSDsschooling.isEmpty()) {
			countDSSchoolEducationalCounselingNotes = "0";
		} else {
			countDSSchoolEducationalCounselingNotes = ((noteDSDsschooling).get(0)).toString();
		}
		if (noteDSDstaxes.isEmpty()) {
			countdsTaxServicesNotes = "0";
		} else {
			countdsTaxServicesNotes = ((noteDSDstaxes).get(0)).toString();
		}
		if (noteDSDstempaccomodation.isEmpty()) {
			countDSTemporaryAccommodationNotes = "0";
		} else {
			countDSTemporaryAccommodationNotes = ((noteDSDstempaccomodation).get(0)).toString();
		}
		if (noteDSDsTenancyManagement.isEmpty()) {
			countDSTenancyManagementNotes = "0";
		} else {
			countDSTenancyManagementNotes = ((noteDSDsTenancyManagement).get(0)).toString();
		}
		if (noteDSVisaImmigration.isEmpty()) {
			countDSVisaImmigrationNotes = "0";
		} else {
			countDSVisaImmigrationNotes = ((noteDSVisaImmigration).get(0)).toString();
		}
		if(noteDSSettlingIn.isEmpty()){
			countDSSettlingInNotes = "0";
		}else{
			countDSSettlingInNotes = ((noteDSSettlingIn).get(0)).toString();
		}
		if(noteDSWorkPermit.isEmpty()){
			countDSWorkPermitNotes = "0";
		}else{
			countDSWorkPermitNotes = ((noteDSWorkPermit).get(0)).toString();
		}
		if(noteDSResidencePermit.isEmpty()){
			countDSResidencePermitNotes = "0";
		}else{
			countDSResidencePermitNotes = ((noteDSResidencePermit).get(0)).toString();
		}	
		if (noteDSRentalSearch.isEmpty()) {
			countDSRentalSearchNotes = "0";
		} else {
			countDSRentalSearchNotes = ((noteDSRentalSearch).get(0)).toString();
		}
		if (noteDSCandidateAssessment.isEmpty()) {
			countDSCandidateAssessmentNotes = "0";
		} else {
			countDSCandidateAssessmentNotes = ((noteDSCandidateAssessment).get(0)).toString();
		}		
		if (noteDSClosingServices.isEmpty()) {
			countDSClosingServicesNotes = "0";
		} else {
			countDSClosingServicesNotes = ((noteDSClosingServices).get(0)).toString();
		}
		if (noteDSComparableHousingStudy.isEmpty()) {
			countDSComparableHousingStudyNotes = "0";
		} else {
			countDSComparableHousingStudyNotes = ((noteDSComparableHousingStudy).get(0)).toString();
		}
		if (noteDSDepartureServices.isEmpty()) {
			countDSDepartureServicesNotes = "0";
		} else {
			countDSDepartureServicesNotes = ((noteDSDepartureServices).get(0)).toString();
		}
		if (noteDSHomeSaleMarketingAssistance.isEmpty()) {
			countDSHomeSaleMarketingAssistanceNotes = "0";
		} else {
			countDSHomeSaleMarketingAssistanceNotes = ((noteDSHomeSaleMarketingAssistance).get(0)).toString();
		}
		if (noteDSPolicyDevelopment.isEmpty()) {
			countDSPolicyDevelopmentNotes = "0";
		} else {
			countDSPolicyDevelopmentNotes = ((noteDSPolicyDevelopment).get(0)).toString();
		}
		if (noteDSRelocationCostProjections.isEmpty()) {
			countDSRelocationCostProjectionsNotes = "0";
		} else {
			countDSRelocationCostProjectionsNotes = ((noteDSRelocationCostProjections).get(0)).toString();
		}
		if (noteDSSpousalAssistance.isEmpty()) {
			countDSSpousalAssistanceNotes = "0";
		} else {
			countDSSpousalAssistanceNotes = ((noteDSSpousalAssistance).get(0)).toString();
		}		
		if (noteDSTechnologySolutions.isEmpty()) {
			countDSTechnologySolutionsNotes = "0";
		} else {
			countDSTechnologySolutionsNotes = ((noteDSTechnologySolutions).get(0)).toString();
		}	
		if (noteDSMortgageServices.isEmpty()) {
			countDSMortgageServicesNotes = "0";
		} else {
			countDSMortgageServicesNotes = ((noteDSMortgageServices).get(0)).toString();
		}	
		if (noteDSDestinationsSchoolServices.isEmpty()) {
			countDSDestinationsSchoolServicesNotes = "0";
		} else {
			countDSDestinationsSchoolServicesNotes = ((noteDSDestinationsSchoolServices).get(0)).toString();
		}
		if(noteDsHotelBookings.isEmpty()){
			countDsHotelBookingsNotes = "0";
		}else{
			countDsHotelBookingsNotes = ((noteDsHotelBookings).get(0)).toString();
		}
		if(noteDsFlightBookings.isEmpty()){
			countDsFlightBookingsNotes = "0";
		}else{
			countDsFlightBookingsNotes = ((noteDsFlightBookings).get(0)).toString();
		}
		
		if (noteDSDsFurnitureRental.isEmpty()) {
			countDSFurnitureRentalNotes = "0";
		} else {
			countDSFurnitureRentalNotes = ((noteDSDsFurnitureRental).get(0)).toString();
		}

		if (noteDSDsAirportPickUp.isEmpty()) {
			countDSAirportPickUpNotes = "0";
		} else {
			countDSAirportPickUpNotes = ((noteDSDsAirportPickUp).get(0)).toString();
		}
		
		return SUCCESS;
	}
	
	@SkipValidation
	public String sendMail(String emailTo,String coordName,String shipperName,String shipNumber,String from) throws AddressException, MessagingException {
		
		try {
			
                String msgText1= "Dear " + coordName + " ,\n\n\t" +
                 	"Relocation status page has been updated via the account portal for the following order: \n\n" +
                 	"Shipper Name:  " + shipperName + "\n" +
                 	"Service Order Number #: " + shipNumber;
                		
                String subject = "Account Portal- Relocation Status Page";
    			String tempRecipient="";
    			String tempRecipientArr[]=emailTo.split(",");
    			for(String str1:tempRecipientArr){
    				if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
    					if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
    					tempRecipient += str1;
    				}
    			}
    			emailTo=tempRecipient;
                emailSetupManager.globalEmailSetupProcess(from, emailTo, "", "", "", msgText1, subject, sessionCorpID,"",shipNumber,"");

        } catch (Exception ex) {
            ex.printStackTrace();
            
        }
       return SUCCESS; 
    }
	private String resendData;
	@SkipValidation
	public String resendEmailUpdateAjax(){
		String resendUpdate=getRequest().getParameter("resendUpdate");
		String soId=getRequest().getParameter("soId");
		String destination=getRequest().getParameter("destination");
		if(destination==null){destination="";}
		String updateQuery="";
		if(resendUpdate!=null){
			for(String type:resendUpdate.split(",")){
				if(!type.equalsIgnoreCase("MMG")){
					 if(updateQuery.equalsIgnoreCase("")){
						 updateQuery = "SET "+type+"_emailSent=now()";
					 }else{
						 updateQuery = updateQuery+","+type+"_emailSent=now()";
					 }
				}else{
					 if(updateQuery.equalsIgnoreCase("")){
						 updateQuery = "SET "+type+"_orginEmailSent=now()";
						 if(!destination.equalsIgnoreCase("") && destination.equalsIgnoreCase("YES")){
							 updateQuery = updateQuery+","+type+"_destinationEmailSent=now()";
						 }
					 }else{
						 updateQuery = updateQuery+","+type+"_orginEmailSent=now()";
						 if(!destination.equalsIgnoreCase("") && destination.equalsIgnoreCase("YES")){
							 updateQuery = updateQuery+","+type+"_destinationEmailSent=now()";
						 }						 
					 }					
				}
			}
		}
		String resendEmailServiceName=getRequest().getParameter("resendEmailServiceName");
		if(resendEmailServiceName==null){
			resendEmailServiceName="";
		}
		if(!resendEmailServiceName.equalsIgnoreCase("")){
			resendUpdate=resendUpdate+","+resendEmailServiceName;
		}
		String resendEmailServices="";
		for(String str:resendUpdate.split(",")){
			if(resendEmailServices.equalsIgnoreCase("") || !resendEmailServices.contains(str)){
				if(resendEmailServices.equalsIgnoreCase("")){
					resendEmailServices=str;
				}else{
					resendEmailServices=resendEmailServices+","+str;
				}
			}
		}
		if(!updateQuery.equalsIgnoreCase("")){
			resendData=resendEmailServices;
			updateQuery=updateQuery+",resendEmailServiceName='"+resendEmailServices+"'";
			dspDetailsManager.resendEmailUpdateAjax(updateQuery,sessionCorpID,Long.parseLong(soId));
		}
		return SUCCESS;
	}
	private String updateFlag;
	@SkipValidation
	public String sendMailVendorAjax(){
		String sid=getRequest().getParameter("sid");
		String vendorCode=getRequest().getParameter("vendorCode");
		String vendorName=getRequest().getParameter("vendorName");
		String serviceName=getRequest().getParameter("serviceName");
		String vendorEmail=getRequest().getParameter("vendorEmail");
		String serviceType=getRequest().getParameter("serviceType");
		ServiceOrder serOrder=serviceOrderManager.get(Long.parseLong(sid));
		DspDetails dspDetails=dspDetailsManager.get(Long.parseLong(sid));
		User coordinatorName = userManager.getUserByUsername(serOrder.getCoordinator());
		String msgText ="Dear "+vendorName+","+"\n\n"+"This is to inform that you have been assigned for "+serviceName+" " +
				"service for Eurohome Relocation Services. \n\nIn your portal you can find this service " +
				"in SO# "+serOrder.getShipNumber()+","+serOrder.getFirstName()+" "+serOrder.getLastName()+". \n\n" +
				"Thanks for your assistance,\n\n"+coordinatorName.getFirstName()+" "+coordinatorName.getLastName()+"\n"+coordinatorName.getEmail()+"\n"+coordinatorName.getUserTitle()+"\n"+coordinatorName.getBranch();
		String subject ="Service assigned from Eurohome Relocation Services";
		updateFlag="FALSE";
		if((serOrder.getStatus()==null)||((!serOrder.getStatus().equalsIgnoreCase("CNCL"))&&(!serOrder.getStatus().equalsIgnoreCase("CLSD")))){
		 emailSetupManager.globalEmailSetupProcess(coordinatorName.getEmail(), vendorEmail, "", "", "", msgText, subject, sessionCorpID,"",serOrder.getShipNumber(),"");
		 String ss=dspDetails.getMailServiceType();
		 if(ss==null){
			 ss="";
		 }
		 if(ss.equalsIgnoreCase("")){
			 	dspDetails.setMailServiceType(serviceType);
		 }else{
			 if(ss.indexOf(serviceType)<0){
			 	dspDetails.setMailServiceType(ss+","+serviceType);
			 }
		 }
		 Class<?> cls = dspDetails.getClass();
 			Field chap = null;
 			try {  
 					if(!serviceType.trim().equalsIgnoreCase("SCH")){
 						chap = cls.getDeclaredField(serviceType.trim()+"_vendorCode");
 					}else{
 						chap = cls.getDeclaredField(serviceType.trim()+"_schoolSelected");
 					}
 					chap.setAccessible(true);
 					chap.set(dspDetails, vendorCode);
 			} catch (Exception e) {
 				e.printStackTrace();
 			}		
 			try { 
 					if(!serviceType.trim().equalsIgnoreCase("SCH")){
 						chap = cls.getDeclaredField(serviceType.trim()+"_vendorName");
 					}else{
 						chap = cls.getDeclaredField(serviceType.trim()+"_schoolName");
					}
					
					chap.setAccessible(true);
					chap.set(dspDetails, vendorName);
			} catch (Exception e) {
				e.printStackTrace();
			}
 			try {  
 					if(!serviceType.trim().equalsIgnoreCase("SCH")){
 						chap = cls.getDeclaredField(serviceType.trim()+"_vendorEmail");
 					}else{
 						chap = cls.getDeclaredField(serviceType.trim()+"_website");
					}
					
					chap.setAccessible(true);
					chap.set(dspDetails, vendorEmail);
			} catch (Exception e) {
				e.printStackTrace();
			}		
			updateFlag="TRUE";
		 dspDetailsManager.save(dspDetails);
	    }
		return SUCCESS; 
	}
	public String findChkPartnerRUC(){
		try {
			isRedSky=trackingStatusManager.checkIsRedSkyUser(partCode);
		} catch (Exception e) {
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		}
		 return SUCCESS;
	}
	public String chkPartnerAgentRUC(){
		//if(trackingStatusManager.getAgentsExistInRedSky(partCode)){
		//isRedSky="AG";
		//}
		if(trackingStatusManager.getAgentsExistInRedSky(partCode,sessionCorpID,contactName,conEmail)){
			isRedSky="AG";
		}else{
			isRedSky="";	
		}
		 return SUCCESS;
	}
	
	private String currencyValue;
	private String fieldName; 
	private boolean fieldValue;
	private boolean updateUtilityServiceTable;
	private TenancyUtilityServiceManager tenancyUtilityServiceManager;
	private TenancyUtilityService tenancyUtilityService;
	@SkipValidation
	public String saveTenancyManagementUtilities(){
		dspDetailsManager.saveTenancyMgmtUtilities(sessionCorpID,serviceOrderId,fieldName,fieldValue,getRequest().getRemoteUser());
		if(updateUtilityServiceTable){
			try{
				List<TenancyUtilityService> utilityServiceDetail = dspDetailsManager.findUtilityServiceDetail(sessionCorpID,serviceOrderId,fieldName);
				if(utilityServiceDetail!=null && !utilityServiceDetail.isEmpty() && utilityServiceDetail.get(0)!=null){
					tenancyUtilityService = utilityServiceDetail.get(0);
					tenancyUtilityServiceManager.remove(tenancyUtilityService.getId());
					
				}else if(fieldValue){
					
					tenancyUtilityService = new TenancyUtilityService(); 
					tenancyUtilityService.setCreatedOn(new Date());
					tenancyUtilityService.setCreatedBy(getRequest().getRemoteUser());
					tenancyUtilityService.setCorpId(sessionCorpID);
					tenancyUtilityService.setUpdatedOn(new Date());
					tenancyUtilityService.setUpdatedBy(getRequest().getRemoteUser());
					tenancyUtilityService.setParentFieldName(fieldName);
					tenancyUtilityService.setParentId(serviceOrderId);
					tenancyUtilityService.setTEN_utilityAllowanceCurrency(currencyValue);
					tenancyUtilityService.setUserUpdatedRow(updateUtilityServiceTable);
					
					tenancyUtilityServiceManager.save(tenancyUtilityService);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		utilityServiceDetailList = dspDetailsManager.findUtilityServiceDetail(sessionCorpID,serviceOrderId,"");
		return SUCCESS;
	}
	private String vendorCodeVal;
	private String updateQuery;
	@SkipValidation
	public void updateUtilityServiceAjax(){
		if(vendorCodeVal!=null && !vendorCodeVal.equalsIgnoreCase("")){
			if(vendorCodeVal.contains("~")){
				vendorCodeVal = vendorCodeVal.replace("~", "&");
			}
			updateQuery = "update TenancyUtilityService set TEN_utilityVendorName='"+vendorCodeVal+"' where id="+serviceOrderId+"";
		}
		dspDetailsManager.updateUtilityService(updateQuery);
	}
	private String returnAjaxStringValue;
	@SkipValidation
	public String findUtilitiesCheckBoxValuesAjax(){
		returnAjaxStringValue = dspDetailsManager.findUtilitiesCheckBoxValues(sessionCorpID,serviceOrderId);
		return SUCCESS;
	}
	
	private  Map<String, String> tenancyBillingGroup;
	@SkipValidation
	public String tenancyBillingList(){
		tenancyBillingGroup = refMasterManager.findByParameter(sessionCorpID, "BILLGRP");
		return SUCCESS;
	}
	private Date billThroughFrom;
	private Date billThroughTo;
	private String billToCode;
	private String tenancyBillingGrpList;
	private List tenancyBillingPreviewList = new ArrayList();
	@SkipValidation
	public String tenancyBillingPreviewSearch(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start TenancyBillingPreviewSearch()");
				tenancyBillingGroup = refMasterManager.findByParameter(sessionCorpID, "BILLGRP");
				tenancyBillingPreviewList = dspDetailsManager.tenancyBillingPreviewList(billThroughFrom,billThroughTo,billToCode,sessionCorpID,tenancyBillingGrpList);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End TenancyBillingPreviewSearch()");
		return SUCCESS;
	}
	Map<String, String> glcodeGridMap= new LinkedHashMap<String, String>();
	Map<String, String> glcodeCostelementMap= new LinkedHashMap<String, String>();
	private List maxLineNumber;
	private String accountLineNumber;
	private Long autoLineNumber;
	private  String companyDivisionAcctgCodeUnique;
	@SkipValidation
	public String createAccountlineByTenancyBilling(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start TenancyBillingAccountlineCreation()");
				tenancyBillingGroup = refMasterManager.findByParameter(sessionCorpID, "BILLGRP");
				tenancyBillingPreviewList = dspDetailsManager.tenancyBillingPreviewList(billThroughFrom,billThroughTo,billToCode,sessionCorpID,tenancyBillingGrpList);
				glcodeGridMap = serviceOrderManager.getGlcodeGridMap();
				glcodeCostelementMap= serviceOrderManager.getGlcodeCostelementMap();
				companyDivisionAcctgCodeUnique="";
				 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
				 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
					 companyDivisionAcctgCodeUnique =companyDivisionAcctgCodeUniqueList.get(0).toString();
				 }
				Iterator it = tenancyBillingPreviewList.iterator();
				while (it.hasNext()) {
					TenancyBillingDTO tenBillingDTO = (TenancyBillingDTO) it.next();
					if(tenBillingDTO.getNotFound()==null || tenBillingDTO.getNotFound().equals("")){
						accountLine = new AccountLine();
						serviceOrder =(ServiceOrder) serviceOrderManager.findShipNumber(tenBillingDTO.getShipNumber().toString()).get(0);
						accountLine.setServiceOrder(serviceOrder);
						accountLine.setServiceOrderId(serviceOrder.getId());
						boolean activateAccPortal =true;
						if(accountLineAccountPortalFlag){	
				    		activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
				    		accountLine.setActivateAccPortal(activateAccPortal);
				    	}
						accountLine.setCreatedBy("TM:"+getRequest().getRemoteUser());
						accountLine.setUpdatedBy(getRequest().getRemoteUser());
						accountLine.setCreatedOn(new Date());
						accountLine.setUpdatedOn(new Date());
						accountLine.setOnHand (new BigDecimal("0.00"));
						accountLine.setCategory("Relocation");
						accountLine.setShipNumber(serviceOrder.getShipNumber());
						accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
						accountLine.setCorpID(sessionCorpID);
						maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
			            if ( maxLineNumber.get(0) == null ) {          
			             	accountLineNumber = "001";
			             }else {
			             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
			                 if((autoLineNumber.toString()).length() == 2) {
			                 	accountLineNumber = "0"+(autoLineNumber.toString());
			                 }
			                 else if((autoLineNumber.toString()).length() == 1) {
			                 	accountLineNumber = "00"+(autoLineNumber.toString());
			                 } 
			                 else {
			                 	accountLineNumber=autoLineNumber.toString();
			                 }
			             }
						accountLine.setAccountLineNumber(accountLineNumber);
						accountLine.setBillToCode(tenBillingDTO.getBillToCode().toString());
						accountLine.setBillToName(tenBillingDTO.getBillToName().toString());
						if(tenBillingDTO.getVendorCode()!=null && !tenBillingDTO.getVendorCode().equals("")){
							accountLine.setVendorCode(tenBillingDTO.getVendorCode().toString());
							accountLine.setEstimateVendorName(tenBillingDTO.getVendorName().toString());
						}
						String actCode=""; 
			            if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
			    			actCode=partnerManager.getAccountCrossReference(accountLine.getVendorCode(),serviceOrder.getCompanyDivision(),serviceOrder.getCorpID());
			    		}else{
			    			actCode=partnerManager.getAccountCrossReferenceUnique(accountLine.getVendorCode(),serviceOrder.getCorpID());
			    		}
			            accountLine.setActgCode(actCode);
			            accountLine.setContract(tenBillingDTO.getBillingContract().toString());
			            accountLine.setBasis("");
						accountLine.setStatus(true);
						accountLine.setChargeCode(tenBillingDTO.getChargeCode().toString());
						accountLine.setDescription(tenBillingDTO.getChargeDescription().toString());
						accountLine.setAuthorization(tenBillingDTO.getBillToAuthority().toString());
						accountLine.setAccountLineCostElement(tenBillingDTO.getCostElement().toString());
						accountLine.setAccountLineScostElementDescription(tenBillingDTO.getCostElementDescription().toString());
						accountLine.setVATExclude(Boolean.parseBoolean(tenBillingDTO.getVatExclude().toString()));
						accountLine.setPayVatPercent(tenBillingDTO.getVatPercent().toString());
		    			accountLine.setPayVatDescr(tenBillingDTO.getVatDesc().toString());
		    			accountLine.setRecVatDescr(tenBillingDTO.getVatDesc().toString());
		    			accountLine.setRecVatPercent(tenBillingDTO.getVatPercent().toString());
		    			
		    			if(!costElementFlag){
							accountLine.setRecGl(tenBillingDTO.getRecGl().toString());
							accountLine.setPayGl(tenBillingDTO.getPayGl().toString()); 
						}else{
							String chargeStr="";
							String costElement="";
							String gridJob="";
							String gridRouting="";
							String companyDivision="";
							if(tenBillingDTO.getCostElement()!=null && (!(tenBillingDTO.getCostElement().toString().trim().equals("")))){
								costElement	= tenBillingDTO.getCostElement().toString().trim();
							}
							if(serviceOrder.getJob()!=null && (!(serviceOrder.getJob().toString().trim().equals("")))){
								gridJob	=serviceOrder.getJob().toString().trim();
							}
							if(serviceOrder.getRouting()!=null && (!(serviceOrder.getRouting().toString().trim().equals("")))){
								gridRouting	=serviceOrder.getRouting().toString().trim();
							}
							if(serviceOrder.getCompanyDivision()!=null && (!(serviceOrder.getCompanyDivision().toString().trim().equals("")))){
								companyDivision	=serviceOrder.getCompanyDivision().toString().trim();
							}
							if(glcodeGridMap.containsKey(sessionCorpID.trim()+"~"+costElement.trim()+"~"+gridJob.trim()+"~"+gridRouting.trim()+"~"+companyDivision.trim())){
								chargeStr=glcodeGridMap.get(sessionCorpID.trim()+"~"+costElement.trim()+"~"+gridJob.trim()+"~"+gridRouting.trim()+"~"+companyDivision.trim());
							}else if(glcodeGridMap.containsKey(sessionCorpID.trim()+"~"+costElement.trim()+"~"+gridJob.trim()+"~"+""+"~"+companyDivision.trim())){
								chargeStr=glcodeGridMap.get(sessionCorpID.trim()+"~"+costElement.trim()+"~"+gridJob.trim()+"~"+""+"~"+companyDivision.trim());
							}else if(glcodeGridMap.containsKey(sessionCorpID.trim()+"~"+costElement.trim()+"~"+""+"~"+gridRouting.trim()+"~"+companyDivision.trim())){
								chargeStr=glcodeGridMap.get(sessionCorpID.trim()+"~"+costElement.trim()+"~"+""+"~"+gridRouting.trim()+"~"+companyDivision.trim());
							}else if (glcodeGridMap.containsKey(sessionCorpID.trim()+"~"+costElement.trim()+"~"+""+"~"+""+"~"+companyDivision.trim())){
								chargeStr=glcodeGridMap.get(sessionCorpID.trim()+"~"+costElement.trim()+"~"+""+"~"+""+"~"+companyDivision.trim());
							}else if(glcodeGridMap.containsKey(sessionCorpID.trim()+"~"+costElement.trim()+"~"+gridJob.trim()+"~"+gridRouting.trim()+"~"+"")){
								chargeStr=glcodeGridMap.get(sessionCorpID.trim()+"~"+costElement.trim()+"~"+gridJob.trim()+"~"+gridRouting.trim()+"~"+"");
							}else if(glcodeGridMap.containsKey(sessionCorpID.trim()+"~"+costElement.trim()+"~"+gridJob.trim()+"~"+""+"~"+"")){
								chargeStr=glcodeGridMap.get(sessionCorpID.trim()+"~"+costElement.trim()+"~"+gridJob.trim()+"~"+""+"~"+"");
							}else if(glcodeGridMap.containsKey(sessionCorpID.trim()+"~"+costElement.trim()+"~"+""+"~"+gridRouting.trim()+"~"+"")){
								chargeStr=glcodeGridMap.get(sessionCorpID.trim()+"~"+costElement.trim()+"~"+""+"~"+gridRouting.trim()+"~"+"");
							}else{
								if(glcodeCostelementMap.containsKey(sessionCorpID.trim()+"~"+costElement.trim())){
									chargeStr=glcodeCostelementMap.get(sessionCorpID.trim()+"~"+costElement.trim());
								} 
							}
								
						    if(!chargeStr.equalsIgnoreCase("")){
							  String [] chrageDetailArr = chargeStr.split("#");
							  accountLine.setRecGl(chrageDetailArr[0]);
							  accountLine.setPayGl(chrageDetailArr[1]);
							}else{
							  accountLine.setRecGl("");
							  accountLine.setPayGl("");
						    }
						}
		    			
		    			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					if((tenBillingDTO.getChargeCode()!=null && !tenBillingDTO.getChargeCode().equals("")) && (tenBillingDTO.getChargeCode().equals("RENT") || tenBillingDTO.getChargeCode().equals("TEN") || tenBillingDTO.getChargeCode().equals("EMG"))){
						accountLine.setEstimatePayableContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setEstimatePayableContractValueDate(new Date());
						accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						
						accountLine.setEstCurrency(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setEstValueDate(new Date());
						accountLine.setEstExchangeRate(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
						
						accountLine.setEstimateContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setEstimateContractValueDate(new Date());
						accountLine.setEstimateContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						
						accountLine.setEstSellCurrency(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setEstSellValueDate(new Date());
						accountLine.setEstSellExchangeRate(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
						
						accountLine.setRevisionContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setRevisionContractValueDate(new Date());
						accountLine.setRevisionContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						
						accountLine.setRevisionSellCurrency(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setRevisionSellValueDate(new Date());
						accountLine.setRevisionSellExchangeRate(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
						
						accountLine.setRevisionPayableContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setRevisionPayableContractValueDate(new Date());
						accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						
						accountLine.setPayableContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setPayableContractValueDate(new Date());
						accountLine.setPayableContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						
						accountLine.setCountry(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setValueDate(new Date());
						accountLine.setExchangeRate(new Double(tenBillingDTO.getBillingExRate().toString()));
						
						accountLine.setRevisionCurrency(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setRevisionValueDate(new Date());
						accountLine.setRevisionExchangeRate(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
						
						accountLine.setRecQuantity(new BigDecimal(tenBillingDTO.getRentBillingCycle().toString()));
						accountLine.setRecRate(new BigDecimal(tenBillingDTO.getBaseRate()));
						accountLine.setActualRevenue(new BigDecimal(tenBillingDTO.getBaseRate()));
						
						accountLine.setRecRateCurrency(tenBillingDTO.getBillingCurrency().toString());
		    			accountLine.setRacValueDate(new Date());
		    			accountLine.setRecRateExchange(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
		    			accountLine.setRecCurrencyRate(new BigDecimal(tenBillingDTO.getBaseRate()));
		    			
		    			accountLine.setRecVatAmt(new BigDecimal(tenBillingDTO.getRentVat().toString()));
		    			accountLine.setActualRevenueForeign(new BigDecimal(tenBillingDTO.getBaseRate()));
						
						accountLine.setContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setContractRate(new BigDecimal(tenBillingDTO.getContractRate()));
						accountLine.setContractValueDate(new Date());
						accountLine.setContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						accountLine.setContractRateAmmount(new BigDecimal(tenBillingDTO.getRentAmount().toString()));
						
						accountLine.setStorageDateRangeFrom(df.parse(tenBillingDTO.getDspBillThroughDate().toString()));
						accountLine.setStorageDateRangeTo(df.parse(tenBillingDTO.getToDate().toString()));
						accountLine.setStorageDays(new Integer(Integer.parseInt(tenBillingDTO.getDateDifference().toString())));
						accountLineManager.save(accountLine);
						if((tenBillingDTO.getChargeCode()!=null && !tenBillingDTO.getChargeCode().equals("")) && (tenBillingDTO.getChargeCode().equals("RENT"))){
							dspDetailsManager.updateBillThroughDate("DspDetails","TEN_billThroughDate",tenBillingDTO.getToDate().toString(),Long.parseLong(tenBillingDTO.getDspId().toString()),getRequest().getRemoteUser());
						}
					}
					if((tenBillingDTO.getChargeCode()!=null && !tenBillingDTO.getChargeCode().equals("")) && (!tenBillingDTO.getChargeCode().equals("RENT") && !tenBillingDTO.getChargeCode().equals("TEN") && !tenBillingDTO.getChargeCode().equals("EMG"))){
						
						accountLine.setEstimatePayableContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setEstimatePayableContractValueDate(new Date());
						accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						
						accountLine.setEstCurrency(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setEstValueDate(new Date());
						accountLine.setEstExchangeRate(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
						
						accountLine.setEstimateContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setEstimateContractValueDate(new Date());
						accountLine.setEstimateContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						
						accountLine.setEstSellCurrency(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setEstSellValueDate(new Date());
						accountLine.setEstSellExchangeRate(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
						
						accountLine.setRevisionContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setRevisionContractValueDate(new Date());
						accountLine.setRevisionContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						
						accountLine.setRevisionSellCurrency(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setRevisionSellValueDate(new Date());
						accountLine.setRevisionSellExchangeRate(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
						
						accountLine.setPayableContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setPayableContractValueDate(new Date());
						accountLine.setPayableContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						
						accountLine.setCountry(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setValueDate(new Date());
						accountLine.setExchangeRate(new Double(tenBillingDTO.getBillingExRate().toString()));
						
						accountLine.setRecQuantity(new BigDecimal(tenBillingDTO.getRentBillingCycle().toString()));
						accountLine.setRecRate(new BigDecimal(tenBillingDTO.getBaseRate()));
						accountLine.setActualRevenue(new BigDecimal((tenBillingDTO.getBaseRate())*(tenBillingDTO.getRentBillingCycle())));
						
						accountLine.setRecRateCurrency(tenBillingDTO.getBillingCurrency().toString());
		    			accountLine.setRacValueDate(new Date());
		    			accountLine.setRecRateExchange(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
		    			accountLine.setRecCurrencyRate(new BigDecimal(tenBillingDTO.getBaseRate()));
		    			
		    			accountLine.setRecVatAmt(new BigDecimal(tenBillingDTO.getRentVat().toString()));
		    			accountLine.setActualRevenueForeign(new BigDecimal((tenBillingDTO.getBillingRate())*(tenBillingDTO.getRentBillingCycle())));
						
						accountLine.setContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setContractRate(new BigDecimal(tenBillingDTO.getContractRate()));
						accountLine.setContractValueDate(new Date());
						accountLine.setContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						accountLine.setContractRateAmmount(new BigDecimal(tenBillingDTO.getRentAmount().toString()));
						
						accountLine.setRevisionPayableContractCurrency(tenBillingDTO.getContractCurrency().toString());
						accountLine.setRevisionPayableContractValueDate(new Date());
						accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(tenBillingDTO.getContractExRate().toString()));
						accountLine.setRevisionPayableContractRate(new BigDecimal(tenBillingDTO.getContractRate()));
						accountLine.setRevisionPayableContractRateAmmount(new BigDecimal(tenBillingDTO.getRentAmount().toString()));
						
						accountLine.setRevisionCurrency(tenBillingDTO.getBillingCurrency().toString());
						accountLine.setRevisionValueDate(new Date());
						accountLine.setRevisionExchangeRate(new BigDecimal(tenBillingDTO.getBillingExRate().toString()));
						accountLine.setRevisionLocalRate(new BigDecimal(tenBillingDTO.getBillingRate().toString()));
						accountLine.setRevisionLocalAmount(new BigDecimal((tenBillingDTO.getBillingRate())*(tenBillingDTO.getRentBillingCycle())));
						
						accountLine.setRevisionQuantity(new BigDecimal(tenBillingDTO.getRentBillingCycle().toString()));
						accountLine.setRevisionRate(new BigDecimal(tenBillingDTO.getBaseRate().toString()));
						
						accountLine.setRevisionExpense(new BigDecimal((tenBillingDTO.getBaseRate())*(tenBillingDTO.getRentBillingCycle())));
						accountLine.setRevisionExpVatAmt(new BigDecimal(tenBillingDTO.getRentVat().toString()));
						if(tenBillingDTO.getUtilityBillThroughDate()!=null && !tenBillingDTO.getUtilityBillThroughDate().equals("")){
							accountLine.setStorageDateRangeFrom(df.parse(tenBillingDTO.getUtilityBillThroughDate().toString()));
						}
						if(tenBillingDTO.getToDate()!=null && !tenBillingDTO.getToDate().equals("")){
							accountLine.setStorageDateRangeTo(df.parse(tenBillingDTO.getToDate().toString()));
						}
						if(tenBillingDTO.getDateDifference()!=null && !tenBillingDTO.getDateDifference().equals("")){
							accountLine.setStorageDays(new Integer(Integer.parseInt(tenBillingDTO.getDateDifference().toString())));
						}
						accountLineManager.save(accountLine);
						dspDetailsManager.updateBillThroughDate("TenancyUtilityService","TEN_utilityBillThrough",tenBillingDTO.getToDate().toString(),Long.parseLong(tenBillingDTO.getUtilityId().toString()),getRequest().getRemoteUser());
					}
				}
			}
			String key = "Accountline Created Successfully";   
			saveMessage(getText(key));
			tenancyBillingPreviewList = dspDetailsManager.tenancyBillingPreviewList(billThroughFrom,billThroughTo,billToCode,sessionCorpID,tenancyBillingGrpList);
			}catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	e.printStackTrace();
		    return CANCEL;
		}
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End TenancyBillingAccountlineCreation()");
		return SUCCESS;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public DspDetails getDspDetails() {
		return dspDetails;
	}
	public void setDspDetails(DspDetails dspDetails) {
		this.dspDetails = dspDetails;
	}
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	
	public Map<String, String> getServiceRelos() {
		return serviceRelos;
	}
	
	
	public void setServiceRelos(Map<String, String> serviceRelos) {
		this.serviceRelos = serviceRelos;
	}
	
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public String getCountDspDetailsNotes() {
		return countDspDetailsNotes;
	}
	public void setCountDspDetailsNotes(String countDspDetailsNotes) {
		this.countDspDetailsNotes = countDspDetailsNotes;
	}
	public String getGotoPageString() {
		return gotoPageString;
	}
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}
	public String getValidateFormNav() {
		return validateFormNav;
	}
	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public String getShipSize() {
		return shipSize;
	}
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}
	public String getMinShip() {
		return minShip;
	}
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}
	public String getCountShip() {
		return countShip;
	}
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}
	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}
	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}
	public void setDspDetailsManager(DspDetailsManager dspDetailsManager) {
		this.dspDetailsManager = dspDetailsManager;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getViewStatus() {
		return viewStatus;
	}
	public void setViewStatus(Map<String, String> viewStatus) {
		this.viewStatus = viewStatus;
	}
	public Map<String, String> getPaidStatus() {
		return paidStatus;
	}
	public void setPaidStatus(Map<String, String> paidStatus) {
		this.paidStatus = paidStatus;
	}
	public Map<String, String> getCurrency() {
		return currency;
	}
	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}
	public String getCountDSAutomobileAssistanceNotes() {
		return countDSAutomobileAssistanceNotes;
	}
	public void setCountDSAutomobileAssistanceNotes(
			String countDSAutomobileAssistanceNotes) {
		this.countDSAutomobileAssistanceNotes = countDSAutomobileAssistanceNotes;
	}
	public String getCountDSColaServiceNotes() {
		return countDSColaServiceNotes;
	}
	public void setCountDSColaServiceNotes(String countDSColaServiceNotes) {
		this.countDSColaServiceNotes = countDSColaServiceNotes;
	}
	public String getCountDSCrossCulturalTrainingNotes() {
		return countDSCrossCulturalTrainingNotes;
	}
	public void setCountDSCrossCulturalTrainingNotes(
			String countDSCrossCulturalTrainingNotes) {
		this.countDSCrossCulturalTrainingNotes = countDSCrossCulturalTrainingNotes;
	}
	public String getCountDSHomeFindingAssistancePurchaseNotes() {
		return countDSHomeFindingAssistancePurchaseNotes;
	}
	public void setCountDSHomeFindingAssistancePurchaseNotes(
			String countDSHomeFindingAssistancePurchaseNotes) {
		this.countDSHomeFindingAssistancePurchaseNotes = countDSHomeFindingAssistancePurchaseNotes;
	}
	public String getCountDsHomeRentalNotes() {
		return countDsHomeRentalNotes;
	}
	public void setCountDsHomeRentalNotes(String countDsHomeRentalNotes) {
		this.countDsHomeRentalNotes = countDsHomeRentalNotes;
	}
	public String getCountDSLanguageNotes() {
		return countDSLanguageNotes;
	}
	public void setCountDSLanguageNotes(String countDSLanguageNotes) {
		this.countDSLanguageNotes = countDSLanguageNotes;
	}
	public String getCountDSMoveMgmtNotes() {
		return countDSMoveMgmtNotes;
	}
	public void setCountDSMoveMgmtNotes(String countDSMoveMgmtNotes) {
		this.countDSMoveMgmtNotes = countDSMoveMgmtNotes;
	}
	public String getCountDSOngoingSupportNotes() {
		return countDSOngoingSupportNotes;
	}
	public void setCountDSOngoingSupportNotes(String countDSOngoingSupportNotes) {
		this.countDSOngoingSupportNotes = countDSOngoingSupportNotes;
	}
	public String getCountDSPreviewTripNotes() {
		return countDSPreviewTripNotes;
	}
	public void setCountDSPreviewTripNotes(String countDSPreviewTripNotes) {
		this.countDSPreviewTripNotes = countDSPreviewTripNotes;
	}
	public String getCountDsRelocationAreaInfoOrientationNotes() {
		return countDsRelocationAreaInfoOrientationNotes;
	}
	public void setCountDsRelocationAreaInfoOrientationNotes(
			String countDsRelocationAreaInfoOrientationNotes) {
		this.countDsRelocationAreaInfoOrientationNotes = countDsRelocationAreaInfoOrientationNotes;
	}
	public String getCountDSRelocationExpenseManagementNotes() {
		return countDSRelocationExpenseManagementNotes;
	}
	public void setCountDSRelocationExpenseManagementNotes(
			String countDSRelocationExpenseManagementNotes) {
		this.countDSRelocationExpenseManagementNotes = countDSRelocationExpenseManagementNotes;
	}
	public String getCountDSRepatriationNotes() {
		return countDSRepatriationNotes;
	}
	public void setCountDSRepatriationNotes(String countDSRepatriationNotes) {
		this.countDSRepatriationNotes = countDSRepatriationNotes;
	}
	public String getCountDSSchoolEducationalCounselingNotes() {
		return countDSSchoolEducationalCounselingNotes;
	}
	public void setCountDSSchoolEducationalCounselingNotes(
			String countDSSchoolEducationalCounselingNotes) {
		this.countDSSchoolEducationalCounselingNotes = countDSSchoolEducationalCounselingNotes;
	}
	public String getCountdsTaxServicesNotes() {
		return countdsTaxServicesNotes;
	}
	public void setCountdsTaxServicesNotes(String countdsTaxServicesNotes) {
		this.countdsTaxServicesNotes = countdsTaxServicesNotes;
	}
	public String getCountDSTemporaryAccommodationNotes() {
		return countDSTemporaryAccommodationNotes;
	}
	public void setCountDSTemporaryAccommodationNotes(
			String countDSTemporaryAccommodationNotes) {
		this.countDSTemporaryAccommodationNotes = countDSTemporaryAccommodationNotes;
	}
	public String getCountDSTenancyManagementNotes() {
		return countDSTenancyManagementNotes;
	}
	public void setCountDSTenancyManagementNotes(
			String countDSTenancyManagementNotes) {
		this.countDSTenancyManagementNotes = countDSTenancyManagementNotes;
	}
	public String getCountDSVisaImmigrationNotes() {
		return countDSVisaImmigrationNotes;
	}
	public void setCountDSVisaImmigrationNotes(String countDSVisaImmigrationNotes) {
		this.countDSVisaImmigrationNotes = countDSVisaImmigrationNotes;
	}
	public Billing getBilling() {
		return billing;
	}
	public void setBilling(Billing billing) {
		this.billing = billing;
	}
	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public String getCurrencySign() {
		return currencySign;
	}
	public void setCurrencySign(String currencySign) {
		this.currencySign = currencySign;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public Map<String, String> getUtilities() {
		return utilities;
	}
	public void setUtilities(Map<String, String> utilities) {
		this.utilities = utilities;
	}
	public Map<String, String> getDepositby() {
		return depositby;
	}
	public void setDepositby(Map<String, String> depositby) {
		this.depositby = depositby;
	}
	public List getMultiplutilities() {
		return multiplutilities;
	}
	public void setMultiplutilities(List multiplutilities) {
		this.multiplutilities = multiplutilities;
	}
	public List getMultiplutilities1() {
		return multiplutilities1;
	}
	public void setMultiplutilities1(List multiplutilities1) {
		this.multiplutilities1 = multiplutilities1;
	}
	public String getCountDSSettlingInNotes() {
		return countDSSettlingInNotes;
	}
	public void setCountDSSettlingInNotes(String countDSSettlingInNotes) {
		this.countDSSettlingInNotes = countDSSettlingInNotes;
	}
	public String getAssignees() {
		return assignees;
	}
	public void setAssignees(String assignees) {
		this.assignees = assignees;
	}
	public String getEmployers() {
		return employers;
	}
	public void setEmployers(String employers) {
		this.employers = employers;
	}
	public String getConvenant() {
		return convenant;
	}
	public void setConvenant(String convenant) {
		this.convenant = convenant;
	}
	public Map<String, String> getTimeduration() {
		return timeduration;
	}
	public void setTimeduration(Map<String, String> timeduration) {
		this.timeduration = timeduration;
	}
	public Boolean getIsNetwork() {
		return isNetwork;
	}
	public void setIsNetwork(Boolean isNetwork) {
		this.isNetwork = isNetwork;
	}
	public Boolean getIsNetworkBookingAgent() {
		return isNetworkBookingAgent;
	}
	public void setIsNetworkBookingAgent(Boolean isNetworkBookingAgent) {
		this.isNetworkBookingAgent = isNetworkBookingAgent;
	}
	public Boolean getIsNetworkCAR_vendorCode() {
		return isNetworkCAR_vendorCode;
	}
	public void setIsNetworkCAR_vendorCode(Boolean isNetworkCAR_vendorCode) {
		this.isNetworkCAR_vendorCode = isNetworkCAR_vendorCode;
	}
	public List getAccountLineList() {
		return accountLineList;
	}
	public void setAccountLineList(List accountLineList) {
		this.accountLineList = accountLineList;
	}
	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}
	public AccountLine getAccountLine() {
		return accountLine;
	}
	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public TrackingStatus getTrackingStatusToRecod() {
		return trackingStatusToRecod;
	}
	public void setTrackingStatusToRecod(TrackingStatus trackingStatusToRecod) {
		this.trackingStatusToRecod = trackingStatusToRecod;
	}
	public Billing getBillingRecods() {
		return billingRecods;
	}
	public void setBillingRecods(Billing billingRecods) {
		this.billingRecods = billingRecods;
	}
	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}
	public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
		this.exchangeRateManager = exchangeRateManager;
	}
	public void setContainerManager(ContainerManager containerManager) {
		this.containerManager = containerManager;
	}
	public void setCartonManager(CartonManager cartonManager) {
		this.cartonManager = cartonManager;
	}
	public void setVehicleManager(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
	}
	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}
	public void setConsigneeInstructionManager(
			ConsigneeInstructionManager consigneeInstructionManager) {
		this.consigneeInstructionManager = consigneeInstructionManager;
	}
	public void setDsFamilyDetailsManager(
			DsFamilyDetailsManager dsFamilyDetailsManager) {
		this.dsFamilyDetailsManager = dsFamilyDetailsManager;
	}
	public void setAdAddressesDetailsManager(
			AdAddressesDetailsManager adAddressesDetailsManager) {
		this.adAddressesDetailsManager = adAddressesDetailsManager;
	}
	public Boolean getIsNetworkCOL_vendorCode() {
		return isNetworkCOL_vendorCode;
	}
	public void setIsNetworkCOL_vendorCode(Boolean isNetworkCOL_vendorCode) {
		this.isNetworkCOL_vendorCode = isNetworkCOL_vendorCode;
	}
	public Boolean getIsNetworkTRG_vendorCode() {
		return isNetworkTRG_vendorCode;
	}
	public void setIsNetworkTRG_vendorCode(Boolean isNetworkTRG_vendorCode) {
		this.isNetworkTRG_vendorCode = isNetworkTRG_vendorCode;
	}
	public Boolean getIsNetworkHOM_vendorCode() {
		return isNetworkHOM_vendorCode;
	}
	public void setIsNetworkHOM_vendorCode(Boolean isNetworkHOM_vendorCode) {
		this.isNetworkHOM_vendorCode = isNetworkHOM_vendorCode;
	}
	public Boolean getIsNetworkRNT_vendorCode() {
		return isNetworkRNT_vendorCode;
	}
	public void setIsNetworkRNT_vendorCode(Boolean isNetworkRNT_vendorCode) {
		this.isNetworkRNT_vendorCode = isNetworkRNT_vendorCode;
	}
	public Boolean getIsNetworkLAN_vendorCode() {
		return isNetworkLAN_vendorCode;
	}
	public void setIsNetworkLAN_vendorCode(Boolean isNetworkLAN_vendorCode) {
		this.isNetworkLAN_vendorCode = isNetworkLAN_vendorCode;
	}
	public Boolean getIsNetworkMMG_vendorCode() {
		return isNetworkMMG_vendorCode;
	}
	public void setIsNetworkMMG_vendorCode(Boolean isNetworkMMG_vendorCode) {
		this.isNetworkMMG_vendorCode = isNetworkMMG_vendorCode;
	}
	public Boolean getIsNetworkONG_vendorCode() {
		return isNetworkONG_vendorCode;
	}
	public void setIsNetworkONG_vendorCode(Boolean isNetworkONG_vendorCode) {
		this.isNetworkONG_vendorCode = isNetworkONG_vendorCode;
	}
	public Boolean getIsNetworkPRV_vendorCode() {
		return isNetworkPRV_vendorCode;
	}
	public void setIsNetworkPRV_vendorCode(Boolean isNetworkPRV_vendorCode) {
		this.isNetworkPRV_vendorCode = isNetworkPRV_vendorCode;
	}
	public Boolean getIsNetworkAIO_vendorCode() {
		return isNetworkAIO_vendorCode;
	}
	public void setIsNetworkAIO_vendorCode(Boolean isNetworkAIO_vendorCode) {
		this.isNetworkAIO_vendorCode = isNetworkAIO_vendorCode;
	}
	public Boolean getIsNetworkEXP_vendorCode() {
		return isNetworkEXP_vendorCode;
	}
	public void setIsNetworkEXP_vendorCode(Boolean isNetworkEXP_vendorCode) {
		this.isNetworkEXP_vendorCode = isNetworkEXP_vendorCode;
	}
	public Boolean getIsNetworkRPT_vendorCode() {
		return isNetworkRPT_vendorCode;
	}
	public void setIsNetworkRPT_vendorCode(Boolean isNetworkRPT_vendorCode) {
		this.isNetworkRPT_vendorCode = isNetworkRPT_vendorCode;
	}
	public Boolean getIsNetworkTAX_vendorCode() {
		return isNetworkTAX_vendorCode;
	}
	public void setIsNetworkTAX_vendorCode(Boolean isNetworkTAX_vendorCode) {
		this.isNetworkTAX_vendorCode = isNetworkTAX_vendorCode;
	}
	public Boolean getIsNetworkTAC_vendorCode() {
		return isNetworkTAC_vendorCode;
	}
	public void setIsNetworkTAC_vendorCode(Boolean isNetworkTAC_vendorCode) {
		this.isNetworkTAC_vendorCode = isNetworkTAC_vendorCode;
	}
	public Boolean getIsNetworkTEN_vendorCode() {
		return isNetworkTEN_vendorCode;
	}
	public void setIsNetworkTEN_vendorCode(Boolean isNetworkTEN_vendorCode) {
		this.isNetworkTEN_vendorCode = isNetworkTEN_vendorCode;
	}
	public Boolean getIsNetworkVIS_vendorCode() {
		return isNetworkVIS_vendorCode;
	}
	public void setIsNetworkVIS_vendorCode(Boolean isNetworkVIS_vendorCode) {
		this.isNetworkVIS_vendorCode = isNetworkVIS_vendorCode;
	}
	public Boolean getIsNetworkSET_vendorCode() {
		return isNetworkSET_vendorCode;
	}
	public void setIsNetworkSET_vendorCode(Boolean isNetworkSET_vendorCode) {
		this.isNetworkSET_vendorCode = isNetworkSET_vendorCode;
	}
	public Boolean getIsNetworkSCH_schoolSelected() {
		return isNetworkSCH_schoolSelected;
	}
	public void setIsNetworkSCH_schoolSelected(Boolean isNetworkSCH_schoolSelected) {
		this.isNetworkSCH_schoolSelected = isNetworkSCH_schoolSelected;
	}
	public boolean isBillingContractType() {
		return billingContractType;
	}
	public void setBillingContractType(boolean billingContractType) {
		this.billingContractType = billingContractType;
	}
	public Boolean getIsNetworkBookingAgentCode() {
		return isNetworkBookingAgentCode;
	}
	public void setIsNetworkBookingAgentCode(Boolean isNetworkBookingAgentCode) {
		this.isNetworkBookingAgentCode = isNetworkBookingAgentCode;
	}
	public String getTrackingStatusNetworkPartnerCode() {
		return trackingStatusNetworkPartnerCode;
	}
	public void setTrackingStatusNetworkPartnerCode(
			String trackingStatusNetworkPartnerCode) {
		this.trackingStatusNetworkPartnerCode = trackingStatusNetworkPartnerCode;
	}
	public String getTrackingStatusNetworkPartnerName() {
		return trackingStatusNetworkPartnerName;
	}
	public void setTrackingStatusNetworkPartnerName(
			String trackingStatusNetworkPartnerName) {
		this.trackingStatusNetworkPartnerName = trackingStatusNetworkPartnerName;
	}
	public String getTrackingStatusBookingAgentContact() {
		return trackingStatusBookingAgentContact;
	}
	public void setTrackingStatusBookingAgentContact(
			String trackingStatusBookingAgentContact) {
		this.trackingStatusBookingAgentContact = trackingStatusBookingAgentContact;
	}
	public String getTrackingStatusNetworkContact() {
		return trackingStatusNetworkContact;
	}
	public void setTrackingStatusNetworkContact(String trackingStatusNetworkContact) {
		this.trackingStatusNetworkContact = trackingStatusNetworkContact;
	}
	public String getTrackingStatusBookingAgentEmail() {
		return trackingStatusBookingAgentEmail;
	}
	public void setTrackingStatusBookingAgentEmail(
			String trackingStatusBookingAgentEmail) {
		this.trackingStatusBookingAgentEmail = trackingStatusBookingAgentEmail;
	}
	public String getTrackingStatusNetworkEmail() {
		return trackingStatusNetworkEmail;
	}
	public void setTrackingStatusNetworkEmail(String trackingStatusNetworkEmail) {
		this.trackingStatusNetworkEmail = trackingStatusNetworkEmail;
	}
	public String getTrackingStatusNetworkAgentExSO() {
		return trackingStatusNetworkAgentExSO;
	}
	public void setTrackingStatusNetworkAgentExSO(
			String trackingStatusNetworkAgentExSO) {
		this.trackingStatusNetworkAgentExSO = trackingStatusNetworkAgentExSO;
	}
	public Boolean getIsNetworkNetworkPartnerCode() {
		return isNetworkNetworkPartnerCode;
	}
	public void setIsNetworkNetworkPartnerCode(Boolean isNetworkNetworkPartnerCode) {
		this.isNetworkNetworkPartnerCode = isNetworkNetworkPartnerCode;
	}
	public boolean isNetworkAgent() {
		return networkAgent;
	}
	public void setNetworkAgent(boolean networkAgent) {
		this.networkAgent = networkAgent;
	}
	public String getDspURL() {
		return dspURL;
	}
	public void setDspURL(String dspURL) {
		this.dspURL = dspURL;
	}
	public Boolean getCostElementFlag() {
		return costElementFlag;
	}
	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}
	public String getTrackingStatusBookingAgentExSO() {
		return trackingStatusBookingAgentExSO;
	}
	public void setTrackingStatusBookingAgentExSO(
			String trackingStatusBookingAgentExSO) {
		this.trackingStatusBookingAgentExSO = trackingStatusBookingAgentExSO;
	}
	public String getTabType() {
		return tabType;
	}
	public void setTabType(String tabType) {
		this.tabType = tabType;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}
	public Map<String, String> getEuVatPercentList() {
		return euVatPercentList;
	}
	public void setEuVatPercentList(Map<String, String> euVatPercentList) {
		this.euVatPercentList = euVatPercentList;
	}
	public Map<String, String> getUTSIpayVatList() {
		return UTSIpayVatList;
	}
	public void setUTSIpayVatList(Map<String, String> uTSIpayVatList) {
		UTSIpayVatList = uTSIpayVatList;
	}
	public Map<String, String> getUTSIpayVatPercentList() {
		return UTSIpayVatPercentList;
	}
	public void setUTSIpayVatPercentList(Map<String, String> uTSIpayVatPercentList) {
		UTSIpayVatPercentList = uTSIpayVatPercentList;
	}
	public Map<String, String> getUTSIeuVatPercentList() {
		return UTSIeuVatPercentList;
	}
	public void setUTSIeuVatPercentList(Map<String, String> uTSIeuVatPercentList) {
		UTSIeuVatPercentList = uTSIeuVatPercentList;
	}
	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}
	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}
	public void setLossManager(LossManager lossManager) {
		this.lossManager = lossManager;
	}
	public RemovalRelocationService getRemovalRelocationService() {
		return removalRelocationService;
	}
	public void setRemovalRelocationService(
			RemovalRelocationService removalRelocationService) {
		this.removalRelocationService = removalRelocationService;
	}
	public void setRemovalRelocationServiceManager(
			RemovalRelocationServiceManager removalRelocationServiceManager) {
		this.removalRelocationServiceManager = removalRelocationServiceManager;
	}
	public String getButtonType() {
		return buttonType;
	}
	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}
	public String getBillToCodeAcc() {
		return billToCodeAcc;
	}
	public void setBillToCodeAcc(String billToCodeAcc) {
		this.billToCodeAcc = billToCodeAcc;
	}
	public String getRloSetVenderCode() {
		return rloSetVenderCode;
	}
	public void setRloSetVenderCode(String rloSetVenderCode) {
		this.rloSetVenderCode = rloSetVenderCode;
	}
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}
	public String getUpdateFlag() {
		return updateFlag;
	}
	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}
	public String getCountDSWorkPermitNotes() {
		return countDSWorkPermitNotes;
	}
	public void setCountDSWorkPermitNotes(String countDSWorkPermitNotes) {
		this.countDSWorkPermitNotes = countDSWorkPermitNotes;
	}
	public String getCountDSResidencePermitNotes() {
		return countDSResidencePermitNotes;
	}
	public void setCountDSResidencePermitNotes(String countDSResidencePermitNotes) {
		this.countDSResidencePermitNotes = countDSResidencePermitNotes;
	}
	public Boolean getIsNetworkWOP_vendorCode() {
		return isNetworkWOP_vendorCode;
	}
	public void setIsNetworkWOP_vendorCode(Boolean isNetworkWOP_vendorCode) {
		this.isNetworkWOP_vendorCode = isNetworkWOP_vendorCode;
	}
	public Boolean getIsNetworkREP_vendorCode() {
		return isNetworkREP_vendorCode;
	}
	public void setIsNetworkREP_vendorCode(Boolean isNetworkREP_vendorCode) {
		this.isNetworkREP_vendorCode = isNetworkREP_vendorCode;
	}
	public String getCountDSRentalSearchNotes() {
		return countDSRentalSearchNotes;
	}
	public void setCountDSRentalSearchNotes(String countDSRentalSearchNotes) {
		this.countDSRentalSearchNotes = countDSRentalSearchNotes;
	}
	public Boolean getIsNetworkRLS_vendorCode() {
		return isNetworkRLS_vendorCode;
	}
	public void setIsNetworkRLS_vendorCode(Boolean isNetworkRLS_vendorCode) {
		this.isNetworkRLS_vendorCode = isNetworkRLS_vendorCode;
	}
	public String getCountDSCandidateAssessmentNotes() {
		return countDSCandidateAssessmentNotes;
	}
	public void setCountDSCandidateAssessmentNotes(
			String countDSCandidateAssessmentNotes) {
		this.countDSCandidateAssessmentNotes = countDSCandidateAssessmentNotes;
	}
	public String getCountDSClosingServicesNotes() {
		return countDSClosingServicesNotes;
	}
	public void setCountDSClosingServicesNotes(String countDSClosingServicesNotes) {
		this.countDSClosingServicesNotes = countDSClosingServicesNotes;
	}
	public String getCountDSComparableHousingStudyNotes() {
		return countDSComparableHousingStudyNotes;
	}
	public void setCountDSComparableHousingStudyNotes(
			String countDSComparableHousingStudyNotes) {
		this.countDSComparableHousingStudyNotes = countDSComparableHousingStudyNotes;
	}
	public String getCountDSDepartureServicesNotes() {
		return countDSDepartureServicesNotes;
	}
	public void setCountDSDepartureServicesNotes(
			String countDSDepartureServicesNotes) {
		this.countDSDepartureServicesNotes = countDSDepartureServicesNotes;
	}
	public String getCountDSHomeSaleMarketingAssistanceNotes() {
		return countDSHomeSaleMarketingAssistanceNotes;
	}
	public void setCountDSHomeSaleMarketingAssistanceNotes(
			String countDSHomeSaleMarketingAssistanceNotes) {
		this.countDSHomeSaleMarketingAssistanceNotes = countDSHomeSaleMarketingAssistanceNotes;
	}
	public String getCountDSPolicyDevelopmentNotes() {
		return countDSPolicyDevelopmentNotes;
	}
	public void setCountDSPolicyDevelopmentNotes(
			String countDSPolicyDevelopmentNotes) {
		this.countDSPolicyDevelopmentNotes = countDSPolicyDevelopmentNotes;
	}
	public String getCountDSRelocationCostProjectionsNotes() {
		return countDSRelocationCostProjectionsNotes;
	}
	public void setCountDSRelocationCostProjectionsNotes(
			String countDSRelocationCostProjectionsNotes) {
		this.countDSRelocationCostProjectionsNotes = countDSRelocationCostProjectionsNotes;
	}
	public String getCountDSSpousalAssistanceNotes() {
		return countDSSpousalAssistanceNotes;
	}
	public void setCountDSSpousalAssistanceNotes(
			String countDSSpousalAssistanceNotes) {
		this.countDSSpousalAssistanceNotes = countDSSpousalAssistanceNotes;
	}
	public String getCountDSTechnologySolutionsNotes() {
		return countDSTechnologySolutionsNotes;
	}
	public void setCountDSTechnologySolutionsNotes(
			String countDSTechnologySolutionsNotes) {
		this.countDSTechnologySolutionsNotes = countDSTechnologySolutionsNotes;
	}
	public Boolean getIsNetworkCAT_vendorCode() {
		return isNetworkCAT_vendorCode;
	}
	public void setIsNetworkCAT_vendorCode(Boolean isNetworkCAT_vendorCode) {
		this.isNetworkCAT_vendorCode = isNetworkCAT_vendorCode;
	}
	public Boolean getIsNetworkCLS_vendorCode() {
		return isNetworkCLS_vendorCode;
	}
	public void setIsNetworkCLS_vendorCode(Boolean isNetworkCLS_vendorCode) {
		this.isNetworkCLS_vendorCode = isNetworkCLS_vendorCode;
	}
	public Boolean getIsNetworkCHS_vendorCode() {
		return isNetworkCHS_vendorCode;
	}
	public void setIsNetworkCHS_vendorCode(Boolean isNetworkCHS_vendorCode) {
		this.isNetworkCHS_vendorCode = isNetworkCHS_vendorCode;
	}
	public Boolean getIsNetworkDPS_vendorCode() {
		return isNetworkDPS_vendorCode;
	}
	public void setIsNetworkDPS_vendorCode(Boolean isNetworkDPS_vendorCode) {
		this.isNetworkDPS_vendorCode = isNetworkDPS_vendorCode;
	}
	public Boolean getIsNetworkHSM_vendorCode() {
		return isNetworkHSM_vendorCode;
	}
	public void setIsNetworkHSM_vendorCode(Boolean isNetworkHSM_vendorCode) {
		this.isNetworkHSM_vendorCode = isNetworkHSM_vendorCode;
	}
	public Boolean getIsNetworkPDT_vendorCode() {
		return isNetworkPDT_vendorCode;
	}
	public void setIsNetworkPDT_vendorCode(Boolean isNetworkPDT_vendorCode) {
		this.isNetworkPDT_vendorCode = isNetworkPDT_vendorCode;
	}
	public Boolean getIsNetworkRCP_vendorCode() {
		return isNetworkRCP_vendorCode;
	}
	public void setIsNetworkRCP_vendorCode(Boolean isNetworkRCP_vendorCode) {
		this.isNetworkRCP_vendorCode = isNetworkRCP_vendorCode;
	}
	public Boolean getIsNetworkSPA_vendorCode() {
		return isNetworkSPA_vendorCode;
	}
	public void setIsNetworkSPA_vendorCode(Boolean isNetworkSPA_vendorCode) {
		this.isNetworkSPA_vendorCode = isNetworkSPA_vendorCode;
	}
	public Boolean getIsNetworkTCS_vendorCode() {
		return isNetworkTCS_vendorCode;
	}
	public void setIsNetworkTCS_vendorCode(Boolean isNetworkTCS_vendorCode) {
		this.isNetworkTCS_vendorCode = isNetworkTCS_vendorCode;
	}
	public Map<String, String> getPaymentresponsibility() {
		return paymentresponsibility;
	}
	public void setPaymentresponsibility(Map<String, String> paymentresponsibility) {
		this.paymentresponsibility = paymentresponsibility;
	}
	public Boolean getIsNetworkMTS_vendorCode() {
		return isNetworkMTS_vendorCode;
	}
	public void setIsNetworkMTS_vendorCode(Boolean isNetworkMTS_vendorCode) {
		this.isNetworkMTS_vendorCode = isNetworkMTS_vendorCode;
	}
	public String getCountDSMortgageServicesNotes() {
		return countDSMortgageServicesNotes;
	}
	public void setCountDSMortgageServicesNotes(String countDSMortgageServicesNotes) {
		this.countDSMortgageServicesNotes = countDSMortgageServicesNotes;
	}
	public Map<String, String> getDpsservices() {
		return dpsservices;
	}
	public void setDpsservices(Map<String, String> dpsservices) {
		this.dpsservices = dpsservices;
	}
	public Map<String, String> getHsmstatus() {
		return hsmstatus;
	}
	public void setHsmstatus(Map<String, String> hsmstatus) {
		this.hsmstatus = hsmstatus;
	}
	public Map<String, String> getPdtservices() {
		return pdtservices;
	}
	public void setPdtservices(Map<String, String> pdtservices) {
		this.pdtservices = pdtservices;
	}
	public Map<String, String> getMtsstatus() {
		return mtsstatus;
	}
	public void setMtsstatus(Map<String, String> mtsstatus) {
		this.mtsstatus = mtsstatus;
	}
	public String getNetworkVendorCode() {
		return networkVendorCode;
	}
	public void setNetworkVendorCode(String networkVendorCode) {
		this.networkVendorCode = networkVendorCode;
	}
	public String getPreFixCode() {
		return preFixCode;
	}
	public void setPreFixCode(String preFixCode) {
		this.preFixCode = preFixCode;
	}
	public String getMoveType() {
		return moveType;
	}
	public void setMoveType(String moveType) {
		this.moveType = moveType;
	}
	public String getCheckAccessQuotation() {
		return checkAccessQuotation;
	}
	public void setCheckAccessQuotation(String checkAccessQuotation) {
		this.checkAccessQuotation = checkAccessQuotation;
	}
	public String getValidationString() {
		return validationString;
	}
	public void setValidationString(String validationString) {
		this.validationString = validationString;
	}
	public Map<String, String> getLeaseSignee() {
		return leaseSignee;
	}
	public void setLeaseSignee(Map<String, String> leaseSignee) {
		this.leaseSignee = leaseSignee;
	}
	public Map<String, String> getAllowance() {
		return allowance;
	}
	public void setAllowance(Map<String, String> allowance) {
		this.allowance = allowance;
	}
	public Map<String, String> getImmigrationStatus() {
		return immigrationStatus;
	}
	public void setImmigrationStatus(Map<String, String> immigrationStatus) {
		this.immigrationStatus = immigrationStatus;
	}
	public String getCountDSDestinationsSchoolServicesNotes() {
		return countDSDestinationsSchoolServicesNotes;
	}
	public void setCountDSDestinationsSchoolServicesNotes(
			String countDSDestinationsSchoolServicesNotes) {
		this.countDSDestinationsSchoolServicesNotes = countDSDestinationsSchoolServicesNotes;
	}
	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}
	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}
	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}
	public String getSystemDefaultCurrency() {
		return systemDefaultCurrency;
	}
	public void setSystemDefaultCurrency(String systemDefaultCurrency) {
		this.systemDefaultCurrency = systemDefaultCurrency;
	}
	public String getCustomerSurveyServiceDetails() {
		return customerSurveyServiceDetails;
	}
	public void setCustomerSurveyServiceDetails(String customerSurveyServiceDetails) {
		this.customerSurveyServiceDetails = customerSurveyServiceDetails;
	}
	public String getDSserviceOrderId() {
		return DSserviceOrderId;
	}
	public void setDSserviceOrderId(String dSserviceOrderId) {
		DSserviceOrderId = dSserviceOrderId;
	}
	public String getFeebackDivId() {
		return feebackDivId;
	}
	public void setFeebackDivId(String feebackDivId) {
		this.feebackDivId = feebackDivId;
	}
	public String getShipnumber() {
		return shipnumber;
	}
	public void setShipnumber(String shipnumber) {
		this.shipnumber = shipnumber;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public List getCustomerFeebackDetails() {
		return CustomerFeebackDetails;
	}
	public void setCustomerFeebackDetails(List customerFeebackDetails) {
		CustomerFeebackDetails = customerFeebackDetails;
	}
	public Map<String, String> getTimeAuthorized() {
		return timeAuthorized;
	}
	public void setTimeAuthorized(Map<String, String> timeAuthorized) {
		this.timeAuthorized = timeAuthorized;
	}
	public List getPricingBillingPaybaleList() {
		return pricingBillingPaybaleList;
	}
	public void setPricingBillingPaybaleList(List pricingBillingPaybaleList) {
		this.pricingBillingPaybaleList = pricingBillingPaybaleList;
	}
	public List getUserContactList() {
		return userContactList;
	}
	public void setUserContactList(List userContactList) {
		this.userContactList = userContactList;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserEmailId() {
		return userEmailId;
	}
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}
	public String getPrifix() {
		return prifix;
	}
	public void setPrifix(String prifix) {
		this.prifix = prifix;
	}
	public Map<String, String> getPaymentReferral() {
		return paymentReferral;
	}
	public void setPaymentReferral(Map<String, String> paymentReferral) {
		this.paymentReferral = paymentReferral;
	}
	public String getSurveyEmailList() {
		return surveyEmailList;
	}
	public void setSurveyEmailList(String surveyEmailList) {
		this.surveyEmailList = surveyEmailList;
	}
	public String getJsonServiceForRelo() {
		return jsonServiceForRelo;
	}
	public void setJsonServiceForRelo(String jsonServiceForRelo) {
		this.jsonServiceForRelo = jsonServiceForRelo;
	}
	public String getResendEmailList() {
		return resendEmailList;
	}
	public void setResendEmailList(String resendEmailList) {
		this.resendEmailList = resendEmailList;
	}
	public String getResendData() {
		return resendData;
	}
	public void setResendData(String resendData) {
		this.resendData = resendData;
	}
	public String getIsRedSky() {
		return isRedSky;
	}
	public void setIsRedSky(String isRedSky) {
		this.isRedSky = isRedSky;
	}
	public String getPartCode() {
		return partCode;
	}
	public void setPartCode(String partCode) {
		this.partCode = partCode;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getConEmail() {
		return conEmail;
	}
	public void setConEmail(String conEmail) {
		this.conEmail = conEmail;
	}
	public String getCountDsHotelBookingsNotes() {
		return countDsHotelBookingsNotes;
	}
	public Map<String, String> getComDivCodePerCorpIdMap() {
		return comDivCodePerCorpIdMap;
	}
	public void setComDivCodePerCorpIdMap(Map<String, String> comDivCodePerCorpIdMap) {
		this.comDivCodePerCorpIdMap = comDivCodePerCorpIdMap;
	}	
	public void setCountDsHotelBookingsNotes(String countDsHotelBookingsNotes) {
		this.countDsHotelBookingsNotes = countDsHotelBookingsNotes;
	}
	public String getCountDsFlightBookingsNotes() {
		return countDsFlightBookingsNotes;
	}
	public void setCountDsFlightBookingsNotes(String countDsFlightBookingsNotes) {
		this.countDsFlightBookingsNotes = countDsFlightBookingsNotes;
	}
	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}
	public static Map<String, String> getYesno() {
		return yesno;
	}
	public static void setYesno(Map<String, String> yesno) {
		DspDetailsAction.yesno = yesno;
	}
	public Map<String, Set<String>> getServiceMandatoryFile() {
		return serviceMandatoryFile;
	}
	public void setServiceMandatoryFile(
			Map<String, Set<String>> serviceMandatoryFile) {
		this.serviceMandatoryFile = serviceMandatoryFile;
	}
	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}
	public String getServiceCompleteDateVal() {
		return serviceCompleteDateVal;
	}
	public void setServiceCompleteDateVal(String serviceCompleteDateVal) {
		this.serviceCompleteDateVal = serviceCompleteDateVal;
	}
	public String getCountDSFurnitureRentalNotes() {
		return countDSFurnitureRentalNotes;
	}
	public void setCountDSFurnitureRentalNotes(String countDSFurnitureRentalNotes) {
		this.countDSFurnitureRentalNotes = countDSFurnitureRentalNotes;
	}
	public String getCountDSAirportPickUpNotes() {
		return countDSAirportPickUpNotes;
	}
	public void setCountDSAirportPickUpNotes(String countDSAirportPickUpNotes) {
		this.countDSAirportPickUpNotes = countDSAirportPickUpNotes;
	}
	public Boolean getIsNetworkFRL_vendorCode() {
		return isNetworkFRL_vendorCode;
	}
	public void setIsNetworkFRL_vendorCode(Boolean isNetworkFRL_vendorCode) {
		this.isNetworkFRL_vendorCode = isNetworkFRL_vendorCode;
	}
	public Boolean getIsNetworkAPU_vendorCode() {
		return isNetworkAPU_vendorCode;
	}
	public void setIsNetworkAPU_vendorCode(Boolean isNetworkAPU_vendorCode) {
		this.isNetworkAPU_vendorCode = isNetworkAPU_vendorCode;
	}
	public Map<String, String> getCountry() {
		return country;
	}
	public void setCountry(Map<String, String> country) {
		this.country = country;
	}
	public Map<String, String> getState() {
		return state;
	}
	public void setState(Map<String, String> state) {
		this.state = state;
	}
	public String getEnbState() {
		return enbState;
	}
	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}
	public Map<String, String> getCountryCod() {
		return countryCod;
	}
	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}
	
	public Map<String, String> getUtilitiesIncluded() {
		return utilitiesIncluded;
	}
	public void setUtilitiesIncluded(Map<String, String> utilitiesIncluded) {
		this.utilitiesIncluded = utilitiesIncluded;
	}
	public Map<String, String> getTermOfNotice() {
		return termOfNotice;
	}
	public void setTermOfNotice(Map<String, String> termOfNotice) {
		this.termOfNotice = termOfNotice;
	}
	public Map<String, String> getBillingCycle() {
		return billingCycle;
	}
	public void setBillingCycle(Map<String, String> billingCycle) {
		this.billingCycle = billingCycle;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public boolean isFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(boolean fieldValue) {
		this.fieldValue = fieldValue;
	}
	public Map<String, String> getReloTenancyProperty() {
		return reloTenancyProperty;
	}
	public void setReloTenancyProperty(Map<String, String> reloTenancyProperty) {
		this.reloTenancyProperty = reloTenancyProperty;
	}
	public String getTenancyManagementFlag() {
		return tenancyManagementFlag;
	}
	public void setTenancyManagementFlag(String tenancyManagementFlag) {
		this.tenancyManagementFlag = tenancyManagementFlag;
	}
	public boolean isUpdateUtilityServiceTable() {
		return updateUtilityServiceTable;
	}
	public void setUpdateUtilityServiceTable(boolean updateUtilityServiceTable) {
		this.updateUtilityServiceTable = updateUtilityServiceTable;
	}
	public TenancyUtilityService getTenancyUtilityService() {
		return tenancyUtilityService;
	}
	public void setTenancyUtilityService(TenancyUtilityService tenancyUtilityService) {
		this.tenancyUtilityService = tenancyUtilityService;
	}
	public void setTenancyUtilityServiceManager(
			TenancyUtilityServiceManager tenancyUtilityServiceManager) {
		this.tenancyUtilityServiceManager = tenancyUtilityServiceManager;
	}
	public List getUtilityServiceDetailList() {
		return utilityServiceDetailList;
	}
	public void setUtilityServiceDetailList(List utilityServiceDetailList) {
		this.utilityServiceDetailList = utilityServiceDetailList;
	}
	public String getUpdateQuery() {
		return updateQuery;
	}
	public void setUpdateQuery(String updateQuery) {
		this.updateQuery = updateQuery;
	}
	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}
	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}
	public Map<String, String> getEuVatList() {
		return euVatList;
	}
	public void setEuVatList(Map<String, String> euVatList) {
		this.euVatList = euVatList;
	}
	public String getSystemDefaultVatCalculation() {
		return systemDefaultVatCalculation;
	}
	public void setSystemDefaultVatCalculation(String systemDefaultVatCalculation) {
		this.systemDefaultVatCalculation = systemDefaultVatCalculation;
	}
	public Map<String, String> getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}
	public void setCurrencyExchangeRate(Map<String, String> currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}
	public Map<String, String> getCountryCodeAndFlex5Map() {
		return countryCodeAndFlex5Map;
	}
	public void setCountryCodeAndFlex5Map(Map<String, String> countryCodeAndFlex5Map) {
		this.countryCodeAndFlex5Map = countryCodeAndFlex5Map;
	}
	public String getCurrencyValue() {
		return currencyValue;
	}
	public void setCurrencyValue(String currencyValue) {
		this.currencyValue = currencyValue;
	}
	public Map<String, String> getTenancyBillingGroup() {
		return tenancyBillingGroup;
	}
	public void setTenancyBillingGroup(Map<String, String> tenancyBillingGroup) {
		this.tenancyBillingGroup = tenancyBillingGroup;
	}
	public Date getBillThroughFrom() {
		return billThroughFrom;
	}
	public void setBillThroughFrom(Date billThroughFrom) {
		this.billThroughFrom = billThroughFrom;
	}
	public Date getBillThroughTo() {
		return billThroughTo;
	}
	public void setBillThroughTo(Date billThroughTo) {
		this.billThroughTo = billThroughTo;
	}
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	public List getTenancyBillingPreviewList() {
		return tenancyBillingPreviewList;
	}
	public void setTenancyBillingPreviewList(List tenancyBillingPreviewList) {
		this.tenancyBillingPreviewList = tenancyBillingPreviewList;
	}
	public String getTenancyBillingGrpList() {
		return tenancyBillingGrpList;
	}
	public void setTenancyBillingGrpList(String tenancyBillingGrpList) {
		this.tenancyBillingGrpList = tenancyBillingGrpList;
	}
	public boolean isAccountLineAccountPortalFlag() {
		return accountLineAccountPortalFlag;
	}
	public void setAccountLineAccountPortalFlag(boolean accountLineAccountPortalFlag) {
		this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
	}
	public List getMaxLineNumber() {
		return maxLineNumber;
	}
	public void setMaxLineNumber(List maxLineNumber) {
		this.maxLineNumber = maxLineNumber;
	}
	public String getAccountLineNumber() {
		return accountLineNumber;
	}
	public void setAccountLineNumber(String accountLineNumber) {
		this.accountLineNumber = accountLineNumber;
	}
	public Long getAutoLineNumber() {
		return autoLineNumber;
	}
	public void setAutoLineNumber(Long autoLineNumber) {
		this.autoLineNumber = autoLineNumber;
	}
	public Map<String, String> getGlcodeGridMap() {
		return glcodeGridMap;
	}
	public void setGlcodeGridMap(Map<String, String> glcodeGridMap) {
		this.glcodeGridMap = glcodeGridMap;
	}
	public Map<String, String> getGlcodeCostelementMap() {
		return glcodeCostelementMap;
	}
	public void setGlcodeCostelementMap(Map<String, String> glcodeCostelementMap) {
		this.glcodeCostelementMap = glcodeCostelementMap;
	}
	public String getVendorCodeVal() {
		return vendorCodeVal;
	}
	public void setVendorCodeVal(String vendorCodeVal) {
		this.vendorCodeVal = vendorCodeVal;
	}
	public boolean isCmmDmmAgent() {
		return cmmDmmAgent;
	}
	public void setCmmDmmAgent(boolean cmmDmmAgent) {
		this.cmmDmmAgent = cmmDmmAgent;
	}
	public Boolean getIsNetworkINS_vendorCode() {
		return isNetworkINS_vendorCode;
	}
	public void setIsNetworkINS_vendorCode(Boolean isNetworkINS_vendorCode) {
		this.isNetworkINS_vendorCode = isNetworkINS_vendorCode;
	}
	public Boolean getIsNetworkINP_vendorCode() {
		return isNetworkINP_vendorCode;
	}
	public void setIsNetworkINP_vendorCode(Boolean isNetworkINP_vendorCode) {
		this.isNetworkINP_vendorCode = isNetworkINP_vendorCode;
	}
	public Boolean getIsNetworkEDA_vendorCode() {
		return isNetworkEDA_vendorCode;
	}
	public void setIsNetworkEDA_vendorCode(Boolean isNetworkEDA_vendorCode) {
		this.isNetworkEDA_vendorCode = isNetworkEDA_vendorCode;
	}
	public Boolean getIsNetworkTAS_vendorCode() {
		return isNetworkTAS_vendorCode;
	}
	public void setIsNetworkTAS_vendorCode(Boolean isNetworkTAS_vendorCode) {
		this.isNetworkTAS_vendorCode = isNetworkTAS_vendorCode;
	}
}
