<%@ include file="/common/taglibs.jsp"%>

<head>
<title>Storage Library</title>
<meta name="heading" content="Storage Library" />
<script language="javascript" type="text/javascript">
function clear_fields()
{  		    	
	document.forms['storageLibraryList'].elements['storageLibrary.storageId'].value = "";
	document.forms['storageLibraryList'].elements['storageLibrary.storageType'].value = "";
	document.forms['storageLibraryList'].elements['storageLibrary.storageMode'].value ="";
	
}
function confirmSubmit(targetElement){
    
	var agree=confirm("Are you sure you want to remove this Storage Unit?");
	var did = targetElement;
	if (agree){
		 location.href="storageLibraryDelete.html?id="+did;
	}else{
		return false;
	}
}
</script>

<style>
 span.pagelinks {display:block;font-size:0.85em;margin-bottom:3px;!margin-bottom:2px;margin-top:-19px;!margin-top:-18px;padding:2px 0px;text-align:right;width:100%;}
</style>
</head>
<c:set var="storagelibrarybuttons"/>
<s:form id="storageLibraryList" name="storageLibraryList" action="storageLibrarySearch.html" method='post'>
<div id="Layer1" style="width:100%">
	 <div id="otabs" class="form_magn">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
	</div>
	<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
 <div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">	
	<table class="table" width="98%">
		<thead>
			<tr>
			    <th>Storage Id</th>
				<th>Storage Type</th>
				<th>Storage Mode</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
			    <td><s:textfield name="storageLibrary.storageId" size="35" tabindex="1" cssClass="input-text" /></td>
				<td><s:select name="storageLibrary.storageType" cssClass="list-menu" cssStyle="width:200px" list="%{storageType}" headerKey=""  headerValue="" tabindex="2"/></td>
	            <td><s:select name="storageLibrary.storageMode" cssClass="list-menu" cssStyle="width:200px" list="%{storageMode}" headerKey=""  headerValue="" tabindex="3"/></td>
	           	<td style="border-left: hidden; align:right"><s:submit cssClass="cssbutton" method="search" cssStyle="width:60px; height:25px;" key="button.search"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:60px;" onclick="clear_fields();"/></td> 
			</tr>
		</tbody>
	</table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${storagelibrarybuttons}" escapeXml="false" />

	<div id="Layer5" style="width:100%;">
	
	<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Storage Library List</span></a></li>
		    <li><a href="uploadStorageFileForm.html"><span>Storage Library Maintenance</span></a></li>
		  </ul>
	</div>
	<div class="spn" style="!margin-bottom:4px;">&nbsp;</div>
	<s:set name="storageLibraries" value="storageLibraries" scope="request" />
	<display:table name="storageLibraries" class="table" requestURI="" id="storageLibrariesList" export="true" pagesize="10" style="margin-top:1px;!margin-top:-1px;">
	<display:column property="storageId" sortable="true" title="Storage Id" href="editStorageLibrary.html" paramId="id" paramProperty="id" style="width:80px"/>
	<display:column property="storageDes" sortable="true" title="Storage Type" maxLength="15" style="width:110px"/>	
	<display:column property="storageDescription" sortable="true" title="Storage Description" maxLength="25" style="width:150px"/>
    <display:column headerClass="containeralign" sortable="true" sortProperty="volumeCbm" title="Volume<br>Cbm" style="width:50px; text-align: right"><div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageLibrariesList.volumeCbm}" /></div>
	</display:column>
	<display:column headerClass="containeralign" sortable="true" sortProperty="usedVolumeCbm" title="Used&nbsp;Vol<br>Cbm" style="width:55px; text-align: right"><div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageLibrariesList.usedVolumeCbm}" /></div>
	</display:column>
	<display:column headerClass="containeralign" sortable="true" sortProperty="availVolumeCbm" title="Avail&nbsp;Vol<br>Cbm" style="width:55px; text-align: right"><div align="right">
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${storageLibrariesList.availVolumeCbm}" /></div>
	</display:column>
	<c:if test="${storageLibrariesList.storageLocked =='true' }">
		<display:column  title="Storage Locked" style="width:20px;"  sortable="true">
			<img src="${pageContext.request.contextPath}/images/tick01.gif"/>&nbsp;
		</display:column> 
	</c:if>
	<c:if test="${storageLibrariesList.storageLocked =='false' }">
		<display:column  title="Storage Locked" style="width:20px; color:red;" sortable="true">
		  <img src="${pageContext.request.contextPath}/images/cancel001.gif"/>&nbsp;
		</display:column> 
	</c:if>
	
	<c:if test="${storageLibrariesList.storageAssigned =='true' }">
		<display:column title="Storage Assigned" style="width:20px;"  sortable="true">
			<img src="${pageContext.request.contextPath}/images/tick01.gif"/>&nbsp;
		</display:column> 
	</c:if>
	<c:if test="${storageLibrariesList.storageAssigned =='false' }">
		<display:column title="Storage Assigned" style="width:20px; color:red;" sortable="true">
		 <img src="${pageContext.request.contextPath}/images/cancel001.gif"/>&nbsp;
		</display:column> 
	</c:if>
		<display:column property="storageMode" sortable="true" title="Storage Mode" maxLength="5" style="width:20px"/>
		<display:column title="Remove" style="text-align:center; width:10px;">
				<a><img align="middle" onclick="confirmSubmit(${storageLibrariesList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>	
	
	<display:setProperty name="export.excel.filename" value="StorageLibrary List.xls"/>
	<display:setProperty name="export.csv.filename" value="StorageLibrary List.csv"/>
	</display:table>
	<c:set var="buttons"/>
	<input type="button" class="cssbuttonA" value="Add"  style="width:65px;" onclick="location.href='<c:url value="/saveStorageLibraryForm.html"/>'" /></td>
	  
	<c:out value="${buttons}" escapeXml="false" />
	</div>
	</div>
</s:form>
<script type="text/javascript"> 
    highlightTableRows("storageLibraryList"); 
</script>
