<head>   
    <title>Partner Merge Confirmation</title>   
    <meta name="heading" content="Partner Merge Confirmation"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">

</head> 
<s:form id="addPartner" action="mergePartner.html?decorator=popup&popup=true" method="post" >  
<s:hidden name="userCheck"/>
<s:hidden name="userCheckMaster"/>
<s:hidden name="partnerCorpid"/>
</s:form>


<script type="text/javascript">
function closeWindow(){
	window.close(); 
    window.opener.location.reload(); 
}
</script>   
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
<script type="text/javascript" src="scripts/ajax.js"></script>
<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>

<script type="text/javascript"> 
try{
closeWindow();
}
catch(e){}
</script> 