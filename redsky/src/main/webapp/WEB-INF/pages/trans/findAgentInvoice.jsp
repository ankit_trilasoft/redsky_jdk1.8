<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="findVendorInviceList.title"/></title>   
    <meta name="heading" content="<fmt:message key='findVendorInviceList.heading'/>"/> 
 <script language="javascript" type="text/javascript">
  function clear_fields(){
  
			    
			    document.forms['searchForm'].elements['accountLine.invoiceNumber'].value = "";
			    document.forms['searchForm'].elements['accountLine.shipNumber'].value = "";
			    document.forms['searchForm'].elements['serviceOrder.registrationNumber'].value = "";
}
</script> 
<style>
span.pagelinks
{display:block;
font-size:0.85em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-17px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:75%;
!width:76%;
position:relative;
}

div.error, span.error, li.error, div.message {
background:#BCD2EF none repeat scroll 0%;
border:1px solid #000000;
color:#000000;
font-family:Arial,Helvetica,sans-serif;
font-weight:bold;
margin:10px auto;
padding:3px;
text-align:center;
vertical-align:bottom;
width:450px;
}


</style>       
</head>

<s:form cssClass="form_magn" id="searchForm"  name="searchForm" action="searchAgentInviceList" method="post" validate="true" >
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
		<table class="table" style="width:100%"  >
		<thead>
		<tr>
		<th><fmt:message key="accountLine.invoiceNumbers"/></th>
		<th>Service Order #</th>
		<th>Reg No. #</th>
		<th>&nbsp;</th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="20" align="left">
					    <s:textfield name="accountLine.invoiceNumber" required="true" cssClass="input-text" size="30"/>
				</td>
				<td width="20" align="left">
					    <s:textfield name="accountLine.shipNumber" required="true" cssClass="input-text" size="40"/>
				</td>

				<td width="20" align="left">
					    <s:textfield name="serviceOrder.registrationNumber" required="true" cssClass="input-text" size="30"/>
				</td>
				<td width="130px" align="left">
					    <c:out value="${searchbuttons}" escapeXml="false" />
				</td>
				</tr>
	
				</tbody>
			</table>
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
			<s:set name="findVendorInviceList" value="findVendorInviceList" scope="request"/>
			
<div id="Layer1" style="width:100%;overflow-x:scroll;" >
	<div id="newtabs" style="margin-bottom:-10px;">
		  <ul>
		    <li><a class="current"><span>Find Vendor Invoice List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<table class="" cellspacing="0" cellpadding="1" border="0" style="width:100%">
 <tbody> 
   <tr>
	 <td>  
<display:table name="findVendorInviceList" class="table" requestURI="" id="findVendorInviceListId" defaultsort="2" defaultorder="descending" pagesize="10">
<display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
    <a href="costingDetail.html?sid=${findVendorInviceListId.sid}">
    <c:out value="${findVendorInviceListId.shipNumber}" />
    </a> 
</display:column>
<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:50px"/>  
<display:column property="registrationNumber" sortable="true" titleKey="serviceOrder.registrationNumber" style="width:50px"/>  
<display:column property="lastName" sortable="true" titleKey="serviceOrder.lastName" style="width:75px" />
<display:column property="firstname" sortable="true" titleKey="serviceOrder.firstName" style="width:75px" />
<display:column property="status" sortable="true" titleKey="serviceOrder.status" style="width:20px" />
<display:column property="job" sortable="true" titleKey="serviceOrder.job" style="width:50px"/>  
<display:column property="routing" sortable="true" titleKey="serviceOrder.routing" style="width:20px"/>  
<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity" style="width:20px"/>  
<display:column property="billToName" sortable="true" titleKey="serviceOrder.billToCode" style="width:110px" maxLength="19"/>
<display:column property="mode" sortable="true" titleKey="serviceOrder.mode" style="width:20px"/>   
<display:column property="originCountry" sortable="true" titleKey="serviceOrder.originCountry" style="width:20px"/>
<display:column property="originCity" sortable="true" titleKey="serviceOrder.originCity1" style="width:60px"/>    
<display:column property="destinationCountry" sortable="true" titleKey="serviceOrder.destinationCountry" style="width:20px"/> 
<display:column property="destinationCity" sortable="true" titleKey="serviceOrder.destinationCity1" style="width:60px"/>   
<display:column property="coordinator" sortable="true" titleKey="serviceOrder.coordinator" style="width:20px"/>  
<display:column property="createdOn" sortable="true" titleKey="serviceOrder.createdOn" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>  
		
</display:table>


 </td>
  </tr>
  </tbody>
  </table>
</div>
</s:form>

<script type="text/javascript"> 
try{
document.forms['searchForm'].elements['accountLine.invoiceNumber'].focus(); 
}
catch(e){}
highlightTableRows("findVendorInviceList");  
try{
<c:if test="${detailPage == true}" >
	 <c:redirect url="/costingDetail.html?sid=${findVendorInviceListId.sid}"/>
</c:if>
}
catch(e){}
</script>  
		  	