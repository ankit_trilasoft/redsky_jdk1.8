package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.Role;

import com.trilasoft.app.dao.RoleCorpManagementDao;

public class RoleCorpManagementDaoHibernate extends GenericDaoHibernate<Role, Long>
		implements RoleCorpManagementDao {

	public RoleCorpManagementDaoHibernate() {
		super(Role.class);
		// TODO Auto-generated constructor stub
	}
	
	public List<String> onlyRoles(String corpIDList){
		List roleList =  this.getSession().createSQLQuery("select distinct name from role where corpid in(" + corpIDList + ") order by name asc").list();
		Set<String> set = new HashSet<String>(roleList);
		List<String> newRoleList = new ArrayList<String>(set);
		return newRoleList;
	}
	public List<Role> corpRoles(String corpIdList){
		List roleList =  this.getSession().createSQLQuery("select id,corpid,name,category from role where corpid in(" + corpIdList + ")").list();
		List<Role> newRoleList = new ArrayList<Role>();
		Iterator it = roleList.iterator();
		while(it.hasNext()){
			Object[] object = (Object[])(it.next());
			Role role = new Role();
			role.setId(Long.parseLong(object[0].toString()));
			role.setCorpID(object[1].toString());
			role.setName(object[2].toString());
			role.setCategory(((object[3] == null)?"":object[3].toString()));
			newRoleList.add(role);
		}
		return newRoleList;
	}
	
	public Role fetchRoleDetail(Long id,String corpID){
		List roleList =  this.getSession().createSQLQuery("select id,corpid,name,category,description from role where id=" + id).list();
		Role role = new Role();
		Iterator it = roleList.iterator();
		while(it.hasNext()){
			Object[] object = (Object[])(it.next());
			role.setId(Long.parseLong(object[0].toString()));
			role.setCorpID(object[1].toString());
			role.setName(object[2].toString());
			role.setCategory(((object[3] == null)?"":object[3].toString()));
			role.setDescription(((object[4] == null)?"":object[4].toString()));
		}
		return role;
	}
	
	public List<Role> searchRole(String name, String corpID, String category) {
		List roleList  = new ArrayList();
		String condition = " where";
		if(name.equals("") && corpID.equals("") && category.equals("")){
			condition = "";
		}
		if(!name.equals("")){
			condition += " name like '%" + name + "%'";
			if(!corpID.equals("") || !category.equals("")){
				condition += " and";
			}
		}
		if(!corpID.equals("")){
			condition += " corpid like '%" + corpID + "%'";
			if(!category.equals("")){
				condition += " and";
			}
		}
		if(!category.equals("")){
			condition += " category like '%" + category + "%'";
		}
		roleList =  this.getSession().createSQLQuery("select id,corpid,name,category,description from role" + condition).list();
		List<Role> newList = new ArrayList<Role>();
		Iterator it = roleList.iterator();
		while(it.hasNext()){
			Object[] object = (Object[])(it.next());
			Role role = new Role();
			role.setId(Long.parseLong(object[0].toString()));
			role.setCorpID(object[1].toString());
			role.setName(object[2].toString());
			role.setCategory(((object[3] == null)?"":object[3].toString()));
			role.setDescription(((object[4] == null)?"":object[4].toString()));
			newList.add(role);
		}
		return newList;
	}
	
	public boolean checkRoleForExistance(String name, String corpID, String category){
		boolean result = false;
		String query = "select corpid,name,category from role where corpid='" + corpID + "' and name='" + name +"' and category='" + category +"'";
		List resultList = this.getSession().createSQLQuery(query).list();
		if(!resultList.isEmpty() && resultList.size() > 0){
			result = true;
		}
		return result;
	}
}
