package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsRepatriation;

public interface DsRepatriationDao extends GenericDao<DsRepatriation, Long> {
	List getListById(Long id);
}
