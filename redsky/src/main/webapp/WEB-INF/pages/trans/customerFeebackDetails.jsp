<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<c:forEach var="CustomerFeeback" items="${CustomerFeebackDetails}">
<table width="100%" cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;background:url(images/feedbak-bg.png) repeat-x scroll 0 0 #BCD2EF; border-bottom:1px solid #6AC6F0;">
<tr>
<c:if test="${empty CustomerFeeback.serviceDetails}">
<td class="listwhitetext" style="padding:3px;padding-bottom:4px;font-size: 12px;"><b>&nbsp;Rating&nbsp;&nbsp;${CustomerFeeback.customerRating}</b></td>
</c:if>
<c:if test="${not empty CustomerFeeback.serviceDetails}">
<td class="listwhitetext" style="padding:3px;padding-bottom:4px;font-size: 12px;"><b>${CustomerFeeback.serviceDetails}&nbsp;Services&nbsp;Ranking&nbsp;${CustomerFeeback.customerRating}</b></td>
</c:if>
<td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMyDivrat('${feebackDivId}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin: 0px; width: 100%; padding: 5px 10px;line-height:16px;font-family: arial,verdana;">
<tr>
<td valign="top" >${CustomerFeeback.feedback}</td>
</tr>
</table>

</c:forEach>
