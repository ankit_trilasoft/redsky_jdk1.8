<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="MissingEstimatedWeight" pageWidth="792" pageHeight="612" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="756" leftMargin="18" rightMargin="18" topMargin="18" bottomMargin="18" uuid="b336f871-9640-4cd1-89f3-ef3babf0b8f7">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="begindate" class="java.util.Date">
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="enddate" class="java.util.Date">
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	'MissingEstimatedWeight-R-PDF-DAFL.jrxml',
	serviceorder.shipNumber AS serviceorder_shipNumber,
	DATE_FORMAT(now(),'%d-%b-%y') AS currentdate,
	if(serviceorder.companyDivision is null || serviceorder.companyDivision='','',serviceorder.companyDivision) AS serviceorder_companyDivision,
   	serviceorder.job AS serviceorder_job,
	if((serviceorder.firstName is null || trim(serviceorder.firstName)=''),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName)))AS Shipper,
	DATE_FORMAT(workticket.date1,'%d-%b-%y') AS workticket_date1,
	workticket.shipNumber AS workticket_shipNumber,
	workticket.service AS workticket_service,
	workticket.ticket AS workticket_ticket,
	workticket.warehouse AS workticket_warehouse,
	serviceorder.coordinator AS serviceorder_coordinator,
	DATE_FORMAT(customerfile.survey,'%d-%b-%y') AS customerfile_survey,
	customerfile.estimator AS customerfile_estimator
FROM workticket workticket
	INNER JOIN serviceorder serviceorder ON workticket.serviceOrderId = serviceorder.id and serviceorder.corpID = $P{Corporate ID}
	INNER JOIN customerfile customerfile ON serviceorder.customerFileId = customerfile.id and customerfile.corpID = $P{Corporate ID}
WHERE workticket.corpID = $P{Corporate ID}
	AND workticket.targetActual <> 'C'
	AND (workticket.estimatedWeight = 0 or workticket.estimatedWeight IS null)
AND (
       (workticket.date1 between $P{begindate}  AND  $P{enddate})
    OR (workticket.date2 between $P{begindate}  AND $P{enddate})
    OR (workticket.date1 < $P{begindate} AND workticket.date2 > $P{enddate})
)
order by serviceorder.companyDivision]]>
	</queryString>
	<field name="MissingEstimatedWeight-R-PDF-DAFL.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="workticket_date1" class="java.lang.String"/>
	<field name="workticket_shipNumber" class="java.lang.String"/>
	<field name="workticket_service" class="java.lang.String"/>
	<field name="workticket_ticket" class="java.lang.Long"/>
	<field name="workticket_warehouse" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<field name="customerfile_survey" class="java.lang.String"/>
	<field name="customerfile_estimator" class="java.lang.String"/>
	<sortField name="workticket_date1"/>
	<sortField name="Shipper"/>
	<sortField name="serviceorder_shipNumber"/>
	<variable name="tktcount" class="java.lang.Integer" resetType="Group" resetGroup="date1" calculation="DistinctCount">
		<variableExpression><![CDATA[$F{workticket_ticket}]]></variableExpression>
	</variable>
	<group name="date1">
		<groupExpression><![CDATA[$F{workticket_date1}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="27" splitType="Stretch">
				<textField pattern="#,##0" isBlankWhenNull="true">
					<reportElement key="textField" x="249" y="8" width="64" height="18" forecolor="#0000CC" uuid="8bd1c769-9da2-454a-b52d-81c52deb1998"/>
					<textElement>
						<font isBold="true" isItalic="true" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{tktcount}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-22" x="33" y="8" width="148" height="18" forecolor="#0000CC" uuid="4eebd4ad-a5b2-4944-bf35-c5ecc63a0598"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<text><![CDATA[Number of tickets for ]]></text>
				</staticText>
				<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
					<reportElement key="textField-4" x="184" y="8" width="65" height="18" forecolor="#0000CC" uuid="6056d0f1-99bf-41bc-8f21-d812f70c197f"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement>
						<font fontName="Arial" size="10" isBold="true" isItalic="true" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{workticket_date1}]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement key="rectangle-1" x="0" y="7" width="756" height="0" uuid="25585890-94bf-408a-8545-ba4dd3ffc13e"/>
				</rectangle>
				<line>
					<reportElement key="line-2" x="0" y="4" width="756" height="1" uuid="db671737-be8b-4286-a05c-3469ddd4db89"/>
				</line>
			</band>
		</groupFooter>
	</group>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[$F{serviceorder_companyDivision}]]></groupExpression>
		<groupHeader>
			<band height="20" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="102" y="0" width="200" height="20" uuid="5c16409b-e737-4b36-879f-ac1e7d226257"/>
					<textElement>
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-24" x="0" y="0" width="102" height="20" uuid="3949beb8-fe70-4392-afac-b2445eb8f290"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="57" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="185" y="0" width="412" height="23" forecolor="#663300" uuid="acf01d49-612f-4e77-ad5c-4d7e81448ac7"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="18" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[WorkTicket Missing Estimated Weight]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-2" x="464" y="23" width="133" height="21" uuid="8a7868aa-d021-41a8-8f84-271c8964e7a7"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd-MMM-yy").format($P{enddate})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-3" x="324" y="23" width="98" height="21" uuid="9a7a7e7f-3661-4a2a-9c13-8b99bd221875"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd-MMM-yy").format($P{begindate})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-19" x="428" y="23" width="36" height="21" uuid="f7ae4695-9305-418e-bb9f-ef97089f35f6"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<text><![CDATA[To]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-23" x="197" y="23" width="147" height="21" uuid="68586900-43a0-487f-a95d-f5f6685c0e91"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="14"/>
				</textElement>
				<text><![CDATA[WorkTicket Date  ]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="21" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-4" x="1" y="2" width="92" height="16" uuid="1acc92b1-ef50-4825-a2a4-61711896732c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="539" y="2" width="58" height="16" uuid="6b0bda67-4aa3-4c33-aff5-3130764dbc68"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Service]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="268" y="2" width="52" height="14" uuid="5fef38a1-d796-4cd3-97e6-f62ef3abbb53"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[S/O #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="384" y="2" width="71" height="16" uuid="2cf40f55-d3c6-48ad-a6e6-d987ae607ac6"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Ticket #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="451" y="2" width="43" height="16" uuid="43972a6b-64d9-4f0a-a699-f2bd61ec2740"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[WH]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" x="497" y="2" width="34" height="16" uuid="1bf0519e-7d8c-4c44-ab72-63675208c186"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Job]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" x="611" y="2" width="64" height="16" uuid="363fbac4-68ff-4f69-b569-d05c56421f1c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Coordinator]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-15" x="675" y="2" width="81" height="16" uuid="f934fea6-5dbc-470f-beb2-5c47d238f48f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Sales Person]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" x="188" y="2" width="69" height="15" uuid="e224889f-fdf0-4a90-9f6d-e6048a79aa46"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font isBold="true" isItalic="true" isUnderline="false" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<text><![CDATA[Survey Date]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="19" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="539" y="0" width="58" height="18" uuid="71f390cb-2cee-4edc-992e-6fb5c95d6d18"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_service}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="267" y="0" width="102" height="18" uuid="489e1796-b8b8-487d-9f2f-d578f379160a"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="384" y="0" width="61" height="18" uuid="ccd8fa57-54db-49fe-ad02-94b33add1173"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_ticket}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="451" y="0" width="27" height="18" uuid="6c4a0a7f-c610-4f6e-9cd9-303073080674"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_warehouse}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="0" width="171" height="18" uuid="05c1c551-94ac-48df-b89f-81458380cd83"/>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="497" y="0" width="33" height="18" uuid="46ff89b1-8cdb-446c-8b7a-ff150eca2ead"/>
				<textElement textAlignment="Left">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="193" y="0" width="75" height="18" uuid="391ba818-63ae-44e0-93f1-48c81f9b578c"/>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_survey}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="674" y="0" width="81" height="18" uuid="84fd9c7a-3aa7-4174-9e9d-5f511ae91ea7"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_estimator}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="596" y="0" width="78" height="18" uuid="d6a3dad8-c8c0-4248-8da2-fdc0f9fe5ac7"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_coordinator}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="18" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="602" y="4" width="115" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="37206015-93af-427a-b6c2-3c96e6c37629"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="720" y="4" width="36" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="49f51f65-a515-4447-97ff-06df57cbc75d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="4" width="209" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="78ba9e4e-9055-4738-aad0-dea9fe89c90f"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currentdate}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-20" x="266" y="4" width="224" height="14" uuid="423930f5-027d-471f-b7ea-febbf8d47efd"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font isBold="false" isItalic="false" isUnderline="false" pdfFontName="Helvetica"/>
				</textElement>
				<text><![CDATA[MissingEstimatedWeight.jrxml]]></text>
			</staticText>
			<line>
				<reportElement key="line-3" x="0" y="1" width="754" height="1" uuid="1c60eecf-e1f9-4524-9eee-86dcbb2a62ca"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
