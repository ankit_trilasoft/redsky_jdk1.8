<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
 <head>   
    <title><fmt:message key="universalSearchList.title"/></title>   
    <meta name="heading" content="<fmt:message key='universalSearchList.heading' />"/>   
<script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['universalSearchForm'].elements['activeStatus'].checked = false;
	document.forms['universalSearchForm'].elements['job'].value  = "";
	document.forms['universalSearchForm'].elements['coordinator'].value  = "";
	document.forms['universalSearchForm'].elements['status'].value  = "";
	document.forms['universalSearchForm'].elements['sequenceNumber'].value  = ""; 
	document.forms['universalSearchForm'].elements['lastName'].value  = "";
	document.forms['universalSearchForm'].elements['firstName'].value  = "";
	document.forms['universalSearchForm'].elements['billToName'].value  = "";
	document.forms['universalSearchForm'].elements['companyDivision'].value ="";
	document.forms['universalSearchForm'].elements['registrationNumber'].value ="";
	document.forms['universalSearchForm'].elements['personPricing'].value ="";
	document.forms['universalSearchForm'].elements['email'].value ="";
	document.forms['universalSearchForm'].elements['accountName'].value ="";
	try{
		document.forms['universalSearchForm'].elements['originCityOrZip'].value ="";
		document.forms['universalSearchForm'].elements['destinationCityOrZip'].value ="";
		document.forms['universalSearchForm'].elements['originCountry'].value ="";
		document.forms['universalSearchForm'].elements['destinationCountry'].value ="";
		document.forms['universalSearchForm'].elements['cityCountryZipOption'].value ="City";
	}catch(e){}
		
}
function universalSearchList(){	
	var originCityCountryOrZip="";
	var destinationCityCountryOrZip="";
	var activeStatus=document.forms['universalSearchForm'].elements['activeStatus'].checked;
	var job=document.forms['universalSearchForm'].elements['job'].value;
	var coordinator=document.forms['universalSearchForm'].elements['coordinator'].value;
	var status=document.forms['universalSearchForm'].elements['status'].value;
	var sequenceNumber=document.forms['universalSearchForm'].elements['sequenceNumber'].value; 
	var lastName=document.forms['universalSearchForm'].elements['lastName'].value;
	var firstName=document.forms['universalSearchForm'].elements['firstName'].value;
	var billToName=document.forms['universalSearchForm'].elements['billToName'].value;
	var companyDivis=document.forms['universalSearchForm'].elements['companyDivision'].value;
	var reg = document.forms['universalSearchForm'].elements['registrationNumber'].value;
	var personPricing = document.forms['universalSearchForm'].elements['personPricing'].value;
	//var originCity = document.forms['universalSearchForm'].elements['originCity'].value;
	///var destinationCity = document.forms['universalSearchForm'].elements['destinationCity'].value;
	var email = document.forms['universalSearchForm'].elements['email'].value;
	var originCityOrZip = document.forms['universalSearchForm'].elements['originCityOrZip'].value;
	var destinationCityOrZip = document.forms['universalSearchForm'].elements['destinationCityOrZip'].value;
	var cityCountryZipVal = document.forms['universalSearchForm'].elements['cityCountryZipOption'].value;
	if(cityCountryZipVal== 'City' || cityCountryZipVal == 'Zip'){
	   originCityCountryOrZip = document.forms['universalSearchForm'].elements['originCityOrZip'].value;
	   destinationCityCountryOrZip = document.forms['universalSearchForm'].elements['destinationCityOrZip'].value;
	}else{
	   originCityCountryOrZip = document.forms['universalSearchForm'].elements['originCountry'].value;
	   destinationCityCountryOrZip = document.forms['universalSearchForm'].elements['destinationCountry'].value;
	  }
	var accountName = document.forms['universalSearchForm'].elements['accountName'].value;
	if( reg=='' && job=='' &&  coordinator=='' && status=='' &&  sequenceNumber=='' && lastName=='' &&  firstName==''  && billToName=='' && companyDivis=='' && personPricing=='' && activeStatus==false  && email=='' && accountName=='' && originCityOrZip=='' && destinationCityOrZip=='' && originCityCountryOrZip=='' && destinationCityCountryOrZip=='')
		{
			alert('Please select any one of the search!');	
			return false;	
		}
	else{
		
		document.forms['universalSearchForm'].action = 'universalSearch.html';
		
		document.forms['universalSearchForm'].submit();
	}	
}
function validateRegNumber(abc){
	var reg=abc.value;
	var job = document.forms['universalSearchForm'].elements['job'].value;
	     reg =reg.trim();
	     job =job.trim();
	      if(job=='UVL' || job=='MVL'){
	       if(reg!=''){
	        var firstIndex=reg.indexOf("-"); 
	        var lastIndex=reg.lastIndexOf("-");
	        if(firstIndex!=5||lastIndex!=11||reg.length!=13){
	       alert('Registration Number format is incorrect');
	       document.forms['universalSearchForm'].elements['registrationNumber'].value='';
	       }
	      }
	     } 
	}

function serviceOrderBySeqNumber1(sequenceNumber,controlFlag,position){
	var url="serviceOrderBySeqNumber.html?sequenceNumber="+sequenceNumber+"&controlFlag="+controlFlag+"&decorator=simple&popup=true";
	ajax_SoTooltip(url,position);	
}

function showHideCityCountry(){
	var cityCountryZipVal = document.forms['universalSearchForm'].elements['cityCountryZipOption'].value;
	var el = document.getElementById('showHideCityOrZip');
	var el1 = document.getElementById('showHideCountry');
	if(cityCountryZipVal == 'City' || cityCountryZipVal == 'Zip'){
		el.style.display = 'block';		
		el1.style.display = 'none';	
	}else{
		el.style.display = 'none';
		el1.style.display = 'block';	
	}
	document.forms['universalSearchForm'].elements['originCityOrZip'].value='';
	document.forms['universalSearchForm'].elements['destinationCityOrZip'].value='';
	document.forms['universalSearchForm'].elements['originCountry'].value ="";
	document.forms['universalSearchForm'].elements['destinationCountry'].value ="";
}

</script>


<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-20px;
!margin-top:-19px;
padding:2px 0px;
!padding-bottom:0px;
text-align:right;
width:100%;
!width:100%;
}

div.error, span.error, li.error, div.message {

width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;/*jitendra*/
!margin-top:-10px;

}
#ajaxtooltipObj #ajaxtooltip_content form#universalSoForm{margin-top:-28px;}

div#main {
margin:-5px 0 0;

}

 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

}
</style>
</head>   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:52px; height:25px;"  key="button.search" onclick="return universalSearchList();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px; height:25px;" onclick="clear_fields();"/> 
</c:set>   
<s:hidden name="fileID"  id= "fileID" value=""/>    
<s:form id="universalSearchForm" method="post" validate="true">  
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content" style="padding-left:15px;">
<table class="table" style="width:100%"  >
<thead>
<tr>
   <th><fmt:message key="customerFile.sequenceNumber"/></th>
   <th><fmt:message key="customerFile.lastName"/></th>
   <th><fmt:message key="customerFile.firstName"/></th>
   <th><fmt:message key="customerFile.customerEmployer"/></th>
   <th><fmt:message key="customerFile.job"/></th>
   <th><fmt:message key="customerFile.status"/></th>
   <th><fmt:message key="customerFile.coordinator"/></th>
</tr>
</thead>	
		<tbody>
		<tr>		
		    <td width="20" align=" left">
			    <s:textfield name="sequenceNumber" required="true" cssClass="input-text" cssStyle="width:100px" size="15" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
		    <td width="20" align="left">
			    <s:textfield name="lastName" required="true" cssClass="input-text" size="16" cssStyle="width:100px" />
			</td>
		    <td width="20" align="left">
			    <s:textfield name="firstName" required="true" cssClass="input-text" size="10" cssStyle="width:100px" />
			</td>
			<td width="20" align="left">
			    <s:textfield name="billToName" required="true" cssClass="input-text" cssStyle="width:100px" size="20"/>
			</td>
			<td width="20" align="left">
                <s:select name="job" list="%{jobs}" cssClass="list-menu" cssStyle="width:100px" headerKey="" headerValue=""/>
			</td>
			<td width="20px" align="left">
                 <s:select name="status" list="%{custStatus}" cssClass="list-menu" cssStyle="width:100px" headerKey="" headerValue=""/>
			</td>
			<td width="30px" align="left">
                <s:select name="coordinator" list="%{coord}" cssClass="list-menu" cssStyle="width:100px" headerKey="" headerValue="" />
			</td>
			</tr>
					
			<tr><td style="text-align:left;border-right:none;margin:0px;padding:0px;" class="listwhitetext" colspan="7">
			<table style="margin:0px;padding:0px;border:none; float:left;" class="listwhitetext">
				<tr>
			<td style="border:none;" class="listwhitetext">
			   Company Division<br>
			  <s:select name="companyDivision" list="%{companyDivis}" cssClass="list-menu" cssStyle="width:100px;margin-top:2px;margin-right:10px;!vertical-align:middle;" headerKey="" headerValue="" />
			   </td>
			   <td style="border:none; width:129px;" class="listwhitetext">
			   <fmt:message key='serviceOrder.registrationNumber' />
			   &nbsp;<br><s:textfield cssClass="input-text" cssStyle="!vertical-align:middle;margin-top:2px;width:100px;" name="registrationNumber" onchange="validateRegNumber(this)" onkeypress="valid(this,'special')" size="9" maxlength="20"  />
			   </td>
			   <td style="border:none;" class="listwhitetext">
			  Pricing Person<br>
			  <s:select name="personPricing" list="%{pricing}" cssClass="list-menu" cssStyle="width:100px;margin-top:2px;!vertical-align:middle;" headerKey="" headerValue="" />
			   </td>
			   <td style="border:none; width:134px;" class="listwhitetext">
			O/D Options&nbsp;<br>
			<s:select name="cityCountryZipOption" id="cityCountryZipOption" list="%{cityCountryZipSearchOption}" cssClass="list-menu" cssStyle="width:100px;margin-right:0px;margin-top:2px;" onchange="showHideCityCountry();"/>
			</td>
			<td style="border:none;padding:0px;">
			<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;border:none; float:left;" id="showHideCityOrZip">
				<tr>
					<td style="border:none; width:114px;" class="listwhitetext">
					Origin&nbsp;<br>
					<s:textfield name="originCityOrZip" cssClass="input-text" cssStyle="width:100px;margin-right:5px;margin-top:2px;"/>
					</td>
					<td style="border:none;width: 122px;" class="listwhitetext">
					Destination&nbsp;<br>
					<s:textfield name="destinationCityOrZip" cssClass="input-text" cssStyle="width:100px;margin-right:0px;margin-top:2px;"/>
					</td>
				</tr>
			</table>
			</td>
			<td style="border:none;padding:0px;">
			<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;border:none; float:left;" id="showHideCountry">
				<tr>
					<td style="border:none; width:114px;" class="listwhitetext">
					Origin&nbsp;<br>
					<s:select name="originCountry" list="%{ocountry}" cssClass="list-menu" cssStyle="width:100px;margin-right:5px;margin-top:2px;" headerKey="" headerValue="" /></td>
					<td style="border:none; width: 122px;" class="listwhitetext">
					Destination&nbsp;<br>
					<s:select name="destinationCountry" list="%{dcountry}" cssClass="list-menu" cssStyle="width:100px;margin-right:0px;margin-top:2px;" headerKey="" headerValue="" /></td>
				</tr>
			</table>
			</td>
			 <td style="border:none;" class="listwhitetext">
			Account Name<br>
			<s:textfield name="accountName" cssClass="input-text" cssStyle="width:100px;!width:70px; margin-right:2px;margin-top:2px;!vertical-align:middle;"/>
			 </td>
			 <td style="border:none;" class="listwhitetext">
			 E-Mail<br>
			    <s:textfield name="email" cssClass="input-text" size="15" cssStyle="width:100px;!width:70px; margin-right:2px;margin-top:2px;!vertical-align:middle;"/>
			</td>
			 
			 <td style="border:none;vertical-align:middle; width:70px;" class="listwhitetext">
			Active Status<br><s:checkbox key="activeStatus" cssStyle="vertical-align:middle; margin:5px 25px 3px 25px;"/>
			</td>
			  <td style="border:none; width:433px; class="listwhitetext">		
			Search Options<br>
			<s:select name="serviceOrderSearchVal" list="%{serviceOrderSearchType}" cssClass="list-menu" cssStyle="margin-top:2px;margin-top:2px;width:100px;"/>
			</td>
			  <td style="border:none;vertical-align:middle;padding-top:15px; " class="listwhitetext">
			    <c:out value="${searchbuttons}" escapeXml="false"  />   
			   </td>
		   </tr>
		   </table>
		   </td>
		   </tr>
						   
		</tbody>
	</table>
</div>
<div class="bottom-header" style="margin-top:30px;!margin-top:50px;"><span></span></div>
</div>
</div> 
<div style=" width:100%;margin-top:-8px; ">
<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs" style="margin-bottom:0px;!margin-bottom:5px;">
		  <ul>
		    <li><a class="current"><span>Common Search List</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>

<s:set name="universalSearchList" value="universalSearchList" scope="request"/>     
 
<c:if test="${universalSearchList!='[]'}">
<c:choose> 
<c:when test="${usertype =='ACCOUNT'}">
<display:table name="universalSearchList" class="table" requestURI="" id="universalSearchList"  pagesize="15" style="width:100%;" > 
  
  <c:if test="${universalSearchList.type == 'CF' && universalSearchList.moveType=='Quote'}">
  <display:column value="QF" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'CF' &&  universalSearchList.moveType=='BookedMove'}">
  <display:column value="CF" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'CF' && (universalSearchList.moveType=='' || universalSearchList.moveType==null)}">
  <display:column value="CF" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'QF'}">
  <display:column value="QF" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'OR'}">
  <display:column value="OR" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'LC'}">
  <display:column value="LC" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == ''}">
  <display:column value="" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'CF'}">
  <display:column  sortable="true" style="width:5%" >
  <img align="top" onclick="serviceOrderBySeqNumber1('${universalSearchList.sequencenumber}','C',this);" src="${pageContext.request.contextPath}/images/plus-small.png"/>
  </display:column>
  </c:if> 
  <c:if test="${universalSearchList.type == 'OR'}">
  <display:column  sortable="true" style="width:5%">
  <img align="top" onclick="serviceOrderBySeqNumber1('${universalSearchList.sequencenumber}','O',this);" src="${pageContext.request.contextPath}/images/plus-small.png"/>
  </display:column>
  </c:if>  
  <c:if test="${universalSearchList.type == 'CF'}">
  <display:column  sortable="true" title="CF#"  style="width:15%">
  <a href="editCustomerFile.html?id=${universalSearchList.id}"><c:out value="${universalSearchList.sequencenumber}" /></a>
  </display:column>
  </c:if> 
  <c:if test="${universalSearchList.type == 'OR'}">
  <display:column  sortable="true" title="CF#"  style="width:15%">
  <a href="${accountOrderURL}?id=${universalSearchList.id}"><c:out value="${universalSearchList.sequencenumber}" /></a>
  </display:column>
  </c:if> 
   <c:if test="${universalSearchList.type == 'LC'}">
  <display:column  sortable="true" title="CF#"  style="width:15%">
  <a href="editLeadCapture.html?id=${universalSearchList.id}"><c:out value="${universalSearchList.sequencenumber}" /></a>
  </display:column>
  </c:if> 
  <display:column property="registrationnumber" sortable="true" title="Reg#"  style="width:10%"/> 
  <display:column  title="Status"  style="width:15%">
	  <c:if test="${universalSearchList.type == 'CF'}"> 
	  	<c:out value="${universalSearchList.status}"  />  
	  </c:if>
	   
	  <c:if test="${universalSearchList.type == 'OR'}"> 
	  	<c:out value="${universalSearchList.status}"  />   
	  </c:if> 
  </display:column>
  <display:column property="lastname" sortable="true" title="Last Name" style="width:15%"/>
  <display:column property="firstname" sortable="true" title="First Name" style="width:15%"/>  
  <display:column property="job" sortable="true" title="Job"  style="width:15%"/> 
  <display:column property="billtoname" sortable="true" title="Billing Party"  style="width:15%"/>
  <display:column property="accountName" sortable="true" title="Account&nbsp;Name"  style="width:15%"/>
  <display:column property="coordinator" sortable="true" title="Coordinator"  style="width:15%"/>
  <display:column property="origincountry" sortable="true" title="Origin Country"  style="width:15%"/>
  <display:column property="origincity" sortable="true" title="Origin city"  style="width:15%"/>
  <display:column property="destinationcountry" sortable="true" title="Destination Country"  style="width:15%"/>
  <display:column property="destinationcity" sortable="true" title="Destination city"  style="width:15%"/>
  <display:column property="personPricing" sortable="true" title="Pricing&nbsp;Person" style="width:15%"/>
  <display:setProperty name="export.excel.filename" value="universalSearch List.xls"/>   
  <display:setProperty name="export.csv.filename" value="universalSearch List.csv"/>   
  <display:setProperty name="export.pdf.filename" value="universalSearch List.pdf"/>
    
</display:table> 
</c:when>
<c:otherwise> 
<display:table name="universalSearchList" class="table" requestURI="" id="universalSearchList" export="true"  pagesize="15" style="width:100%;" > 
  
  <c:if test="${universalSearchList.type == 'CF' && universalSearchList.moveType=='Quote'}">
  <display:column value="QF" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'CF' && universalSearchList.moveType=='BookedMove'}">
  <display:column value="CF" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'CF' && (universalSearchList.moveType=='' || universalSearchList.moveType==null)}">
  <display:column value="CF" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'QF'}">
  <display:column value="QF" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'OR'}">
  <display:column value="OR" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
   <c:if test="${universalSearchList.type == 'GR'}">
  <display:column value="GR" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'LC'}">
  <display:column value="LC" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == ''}">
  <display:column value="" sortable="true" title="Type"  style="width:5%"/>
  </c:if>
  <c:if test="${universalSearchList.type == 'CF'}">
  <display:column  sortable="true" style="width:5%" >
  <img align="top" onclick="serviceOrderBySeqNumber1('${universalSearchList.sequencenumber}','C',this);" src="${pageContext.request.contextPath}/images/plus-small.png"/>
  </display:column>
  </c:if>
  <c:if test="${universalSearchList.type == 'QF'}">
  <display:column  sortable="true" style="width:5%" >
  <img align="top" onclick="serviceOrderBySeqNumber1('${universalSearchList.sequencenumber}','Q',this);" src="${pageContext.request.contextPath}/images/plus-small.png"/>
  </display:column>
  </c:if>
  <c:if test="${universalSearchList.type == 'OR'}">
  <display:column  sortable="true" style="width:5%">
  <img align="top" onclick="serviceOrderBySeqNumber1('${universalSearchList.sequencenumber}','O',this);" src="${pageContext.request.contextPath}/images/plus-small.png"/>
  </display:column>
  </c:if>
  <c:if test="${universalSearchList.type == 'GR'}">
  <display:column  sortable="true" style="width:5%">
  <img align="top" onclick="serviceOrderBySeqNumber1('${universalSearchList.sequencenumber}','G',this);" src="${pageContext.request.contextPath}/images/plus-small.png"/>
  </display:column>
  </c:if>
  <c:if test="${universalSearchList.type == 'LC'}">
  <display:column  sortable="true" style="width:5%">
  <img align="top" onclick="serviceOrderBySeqNumber1('${universalSearchList.sequencenumber}','L',this);" src="${pageContext.request.contextPath}/images/plus-small.png"/>
  </display:column>
  </c:if>
  <c:if test="${universalSearchList.type == ''}">
  <display:column  sortable="true" style="width:5%">
  <img align="top" src="${pageContext.request.contextPath}/images/plus-small.png"/>
  </display:column>
  </c:if>
  
  <c:if test="${universalSearchList.type == 'CF'}">
  <display:column  sortable="true" title="CF#"  style="width:15%">
  <a href="editCustomerFile.html?id=${universalSearchList.id}"><c:out value="${universalSearchList.sequencenumber}" /></a>
  </display:column>
  </c:if>
  <c:if test="${universalSearchList.type == 'QF'}">
  <display:column  sortable="true" title="CF#"  style="width:15%">
  <a href="QuotationFileForm.html?id=${universalSearchList.id}"><c:out value="${universalSearchList.sequencenumber}" /></a>
  </display:column>
  </c:if>
  <c:if test="${universalSearchList.type == 'OR'}">
  <display:column  sortable="true" title="CF#"  style="width:15%">
  <a href="editOrderManagement.html?id=${universalSearchList.id}"><c:out value="${universalSearchList.sequencenumber}" /></a>
  </display:column>
  </c:if>
  <c:if test="${universalSearchList.type == 'GR'}">
  <display:column  sortable="true" title="CF#"  style="width:15%">
  <a href="editCustomerFile.html?id=${universalSearchList.id}"><c:out value="${universalSearchList.sequencenumber}" /></a>
  </display:column>
  </c:if>
  <c:if test="${universalSearchList.type == 'LC'}">
  <display:column  sortable="true" title="CF#"  style="width:15%">
  <a href="editLeadCapture.html?id=${universalSearchList.id}"><c:out value="${universalSearchList.sequencenumber}" /></a>
  </display:column>
  </c:if> 
  <c:if test="${universalSearchList.type == ''}">
  <display:column property="sequencenumber" sortable="true" title="CF#"  style="width:15%"/>
  </c:if>
  <display:column property="registrationnumber" sortable="true" title="Reg#"  style="width:10%"/> 
  <display:column sortProperty="createdon" sortable="true" titleKey="serviceOrder.createdOn" style="width:80px">
  <fmt:timeZone value="${sessionTimeZone}" >
<fmt:formatDate value="${universalSearchList.createdon}" pattern="dd-MMM-yyyy"/>
</fmt:timeZone></display:column>
  <display:column  title="Status"  style="width:15%">
	  <c:if test="${universalSearchList.type == 'CF'}"> 
	  	<c:out value="${universalSearchList.status}"  />  
	  </c:if>
	  <c:if test="${universalSearchList.type == 'QF'}"> 
	  	<c:out value="${universalSearchList.quotationStatus}"  />   
	  </c:if>
	  <c:if test="${universalSearchList.type == 'OR'}"> 
	  	<c:out value="${universalSearchList.status}"  />   
	  </c:if>
	  <c:if test="${universalSearchList.type == 'GR'}"> 
	  	<c:out value="${universalSearchList.status}"  />   
	  </c:if>
	  <c:if test="${universalSearchList.type == 'LC'}"> 
	  	<c:out value="${universalSearchList.status}"  />  
	  </c:if>
	  <c:if test="${universalSearchList.type == ''}"> 
	  	<c:out value="${universalSearchList.status}"  />   
	  </c:if>
  </display:column>
  <display:column property="lastname" sortable="true" title="Last Name" style="width:15%"/>
  <display:column property="firstname" sortable="true" title="First Name" style="width:15%"/>  
  <display:column property="job" sortable="true" title="Job"  style="width:15%"/> 
  <display:column property="billtoname" sortable="true" title="Billing Party"  style="width:15%"/>
   <display:column property="accountName" sortable="true" title="Account&nbsp;Name"  style="width:15%"/>
  <display:column property="coordinator" sortable="true" title="Coordinator"  style="width:15%"/>
  <display:column property="origincountry" sortable="true" title="Origin Country"  style="width:15%"/>
  <display:column property="origincity" sortable="true" title="Origin city"  style="width:15%"/>
  <display:column property="destinationcountry" sortable="true" title="Destination Country"  style="width:15%"/>
  <display:column property="destinationcity" sortable="true" title="Destination city"  style="width:15%"/>
  <display:column property="personPricing" sortable="true" title="Pricing&nbsp;Person" style="width:15%"/>
  <display:setProperty name="export.excel.filename" value="universalSearch List.xls"/>   
  <display:setProperty name="export.csv.filename" value="universalSearch List.csv"/>   
  <display:setProperty name="export.pdf.filename" value="universalSearch List.pdf"/>   
</display:table>
</c:otherwise>
</c:choose>
</c:if>

<c:if test="${universalSearchList=='[]'}"> 
<display:table name="universalSearchList" class="table" requestURI="" id="universalSearchList" export="true"  pagesize="15" style="width:100%;" > 
  <display:column sortable="true" title="Type"  style="width:5%"/>
  <display:column sortable="true" title="CF#"  style="width:15%"/>
  <display:column sortable="true" title="Reg#"  style="width:10%"/> 
  <display:column  title="Status"  style="width:15%" />
  <display:column sortable="true" title="Last Name" style="width:15%"/>
  <display:column sortable="true" title="First Name" style="width:15%"/>  
  <display:column sortable="true" title="Job"  style="width:15%"/> 
  <display:column sortable="true" title="Billing Party"  style="width:15%"/>
  <display:column sortable="true" title="Account Name"  style="width:15%"/>
  <display:column sortable="true" title="Coordinator"  style="width:15%"/>
  <display:column sortable="true" title="Origin Country"  style="width:15%"/>
  <display:column sortable="true" title="Origin city"  style="width:15%"/>
  <display:column sortable="true" title="Destination Country"  style="width:15%"/>
  <display:column sortable="true" title="Destination city"  style="width:15%"/>
  <display:column sortable="true" title="Pricing&nbsp;Person" style="width:15%"/>
  <display:setProperty name="export.excel.filename" value="universalSearch List.xls"/>   
  <display:setProperty name="export.csv.filename" value="universalSearch List.csv"/>   
  <display:setProperty name="export.pdf.filename" value="universalSearch List.pdf"/>   
</display:table>
</c:if>

</div>
<c:set var="isTrue" value="false" scope="session"/>
</s:form>
<script type="text/javascript"> 

</script>
<script type="text/javascript">  
try{
	var companyDivision='${defaultCompanyCode}';
	if(companyDivision!=''){
	 document.forms['universalSearchForm'].elements['companyDivision'].value='${companyCode}';
	}
}catch(e){}
try{
	var cityCountryZipVal = document.forms['universalSearchForm'].elements['cityCountryZipOption'].value;
	var el = document.getElementById('showHideCityOrZip');
	var el1 = document.getElementById('showHideCountry');
	if(cityCountryZipVal == 'City' || cityCountryZipVal == 'Zip'){
		el.style.display = 'block';		
		el1.style.display = 'none';	
	}else{
		el.style.display = 'none';
		el1.style.display = 'block';	
	}
}catch(e){}
</script> 