delimiter $$

CREATE trigger redsky.trigger_add_history_frequentlyaskedquestions
BEFORE UPDATE on redsky.frequentlyaskedquestions
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";






   IF (OLD.corpId <> NEW.corpId ) THEN
      select CONCAT(fieldNameList,'frequentlyaskedquestions.corpId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpId,'~') into newValueList;
  END IF;

    




   IF (OLD.partnerCode <> NEW.partnerCode ) THEN
      select CONCAT(fieldNameList,'frequentlyaskedquestions.partnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerCode,'~') into newValueList;
  END IF;

    




   IF (OLD.question <> NEW.question ) THEN
      select CONCAT(fieldNameList,'frequentlyaskedquestions.question~') into fieldNameList;
      select CONCAT(oldValueList,OLD.question,'~') into oldValueList;
      select CONCAT(newValueList,NEW.question,'~') into newValueList;
  END IF;

    




   IF (OLD.answer <> NEW.answer ) THEN
      select CONCAT(fieldNameList,'frequentlyaskedquestions.answer~') into fieldNameList;
      select CONCAT(oldValueList,OLD.answer,'~') into oldValueList;
      select CONCAT(newValueList,NEW.answer,'~') into newValueList;
  END IF;

    




   IF (OLD.partnerId <> NEW.partnerId ) THEN
      select CONCAT(fieldNameList,'frequentlyaskedquestions.partnerId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerId,'~') into newValueList;
  END IF;

    




   IF (OLD.language <> NEW.language ) THEN
      select CONCAT(fieldNameList,'frequentlyaskedquestions.language~') into fieldNameList;
      select CONCAT(oldValueList,OLD.language,'~') into oldValueList;
      select CONCAT(newValueList,NEW.language,'~') into newValueList;
  END IF;

    




   IF (OLD.parentId <> NEW.parentId ) THEN
      select CONCAT(fieldNameList,'frequentlyaskedquestions.parentId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.parentId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.parentId,'~') into newValueList;
  END IF;

    


   IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
      select CONCAT(fieldNameList,'frequentlyaskedquestions.sequenceNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
  END IF;

    




   IF (OLD.jobType <> NEW.jobType ) THEN
      select CONCAT(fieldNameList,'frequentlyaskedquestions.jobType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.jobType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.jobType,'~') into newValueList;
  END IF;

    

    


 CALL add_tblHistory (OLD.id,"frequentlyaskedquestions", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;