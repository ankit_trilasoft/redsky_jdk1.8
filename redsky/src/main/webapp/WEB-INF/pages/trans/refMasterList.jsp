<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>

<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />

  <title><fmt:message key="reference.title" /></title>
  <meta name="heading"
	content="<fmt:message key='reference.heading'/>" />
	<style>
<%@ include file="/common/calenderStyle.css"%>
</style>



<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
</script>
<script language="javascript" type="text/javascript">
function myDate() {
	
	
		if(document.forms['refMasterForm'].elements['refMaster.stopDate'].value == "null" ){
		document.forms['refMasterForm'].elements['refMaster.stopDate'].value="";
	}
	

	}
	
function clear_fields()
{
	
			document.forms['refMasterForm'].elements['nameonform'].value = "";
	
			if(document.forms['refMasterForm'].elements['refMaster.parameter'].value!='')
			 {
			    document.forms['refMasterForm'].elements['refMaster.parameter'].value = "";
			 }
			else
			{
			   document.forms['refMasterForm'].elements['parameter'].value = "";
			}
			
			
}


function confirmSubmits(targetElement){
var parameter = targetElement;
var url="refMastermenus.html?parameter=" + encodeURI(parameter);
location.href =	 url;   	    
return true;
}

function confirmSubmit(targetElement){
	var parameter = targetElement;
    var url="refMasterListByParameter.html?ajax=1&decorator=simple&popup=true&parameter=" + encodeURI(parameter);
     http2.open("GET", url, true);
     http2.onreadystatechange = function(){ handleHttpResponse2(parameter);};
     http2.send(null);
    } 

function handleHttpResponse2(parameter){
 			if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
               if(results > 0){ 
               confirmSubmits(parameter);   
             }else{
             alert("Reference List is Empty for this parameter"); 
               
      }
}
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:1px;
margin-right:0px;
margin-top:-15px;
margin-left:px;
padding:2px 0px;
text-align:right;
float:right;
width:100%;

}
</style>

</head>

<s:form name="refMasterForm" id="refMasterForm" action="refMastersParameter" method="post" validate="true">
<c:set var="sessionCorpID" value="<%=request.getParameter("sessionCorpID")%>" />
<s:hidden name="sessionCorpID" value="<%=request.getParameter("sessionCorpID")%>"/>
<div id="Layer1" style="width:100%">
	<s:hidden name="refMaster.id" value="%{refMaster.id}" />
	<c:set var="pmt" value="<%=request.getParameter("parameter")%>" scope="session"/>		
	
		<div id="newmnav">
		  <ul>
		  	<li id="newmnav1" style="background:#FFF "><a href="refMasters.html" class="current"><span>Reference<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="vanLineGLTypeList.html"><span>GL Type</span></a></li>
		    <li><a href="vanLineCommissionTypeList.html"><span>Commission Type</span></a></li>  
		     <c:if test="${costElementFlag==true}">
				<li><a href="costElementList.html"><span>Cost Element</span></a></li>  
			</c:if>
		  </ul>
		</div><div class="spn">&nbsp;</div>
		
<c:set var="searchbuttons">   
	     <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="refMasterParameterList" key="button.search"/>   
         <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
      </c:set>
      <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="0" cellpadding="0" border="0">
	  <tbody>	  
	  <tr>
	  <td rowspan="3"></td>
	  </tr>
	  <tr>
	  <td align="right"class="listwhitetext" width="90px"><b>&nbsp;Parameter:&nbsp;</b></td>
	  <td align="left" style="width:150px"><s:textfield name="refMaster.parameter" required="true" cssClass="input-text" size="30"/></td>
	  <td width="10px"></td>
	  <td></td>
	  <td align="right"class="listwhitetext" width="115px"><b>&nbsp;Name&nbsp;of&nbsp;Field:&nbsp;</b></td>
	  <td align="left" style="width:150px"><s:textfield name="nameonform" value="${nameonform}" required="true" cssClass="input-text" size="50"/></td>
	  <td width="10px"></td>
	  <td  align="left"><c:out value="${searchbuttons}" escapeXml="false" /></td>
	  </tr>
	  </tbody>
	  </table>	 
	  </div>
<div class="bottom-header" style="margin-top:35px;"><span></span></div>
</div>
</div>

      <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Parameter List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div> 
	  <display:table name="ls" class="table" requestURI=""  id="refMasterList"  pagesize="10"  >
	   <c:if test="${hitFlag==1}">
	    <c:set var="lowerStr" value="${refMasterList.description}"/>
		<display:column   sortable="true" style="width:100px" ><c:out value="${fn:toUpperCase(lowerStr)}"/></display:column>
        <%-- <display:column style="width:100px" ><img src="images/viewlist.gif"/><a  href="refMastermenus.html?parameter=${fn:toUpperCase(lowerStr)}" /><font color="blue">View List</font></a></display:column> --%>
		  <display:column property="comments" sortable="true" title="Comments" style="width:100px"/>
		  <display:column property="abbreviation" sortable="true" title="Description" style="width:100px"/>
		 <display:column style="width:100px;font-size: 9px;"><a><img align="top" onclick="confirmSubmit('${refMasterList.description}');" style="margin: 0px 0px 0px 8px;" src="images/viewlist.gif"/></a><a href="#" onclick="confirmSubmit('${refMasterList.description}');">View List</a></display:column>
		
	  
		 <display:column style="width:100px;font-size: 9px;" ><img align="top" style="margin: 0px 0px 0px 8px;" src="images/addetail.gif"/>
		<a href="editRefMastermenu.html?parameter=${fn:toUpperCase(lowerStr)}&l=${refMasterList.shipNumber}" />Add Details</a>
		</display:column>	
		</c:if>	
		
		<c:if test="${hitFlag==2}">
	     
	    <c:set var="lowerStr" value="${refMasterList.description}"/>
		<display:column   sortable="true" style="width:100px" ><c:out value="${fn:toUpperCase(lowerStr)}"/></display:column>
        <%-- <display:column style="width:100px" ><img src="images/viewlist.gif"/><a  href="refMastermenus.html?parameter=${fn:toUpperCase(lowerStr)}" /><font color="blue">View List</font></a></display:column> --%>
		 <display:column style="width:100px;font-size: 9px;"><a><img align="top" onclick="confirmSubmit('${refMasterList.description}');" style="margin: 0px 0px 0px 8px;" src="images/viewlist.gif"/></a><a href="#" onclick="confirmSubmit('${refMasterList.description}');">View List</a></display:column>
		
	  
		 <display:column style="width:100px;font-size: 9px;" ><img align="top" style="margin: 0px 0px 0px 8px;" src="images/addetail.gif"/>
		<a href="editRefMastermenu.html?parameter=${fn:toUpperCase(lowerStr)}&l=${refMasterList.shipNumber}" />Add Details</a>
		</display:column>
		</c:if>
	</display:table>
	
	
	</div>
	 <c:if test="${sessionCorpID=='TSFT'}" >
       <input type="button" class="cssbuttonA" value="Add"  style="width:65px; height:25px" onclick="location.href='<c:url value="/editRefMastermenu.html"/>'" /></td>
     </c:if>

</s:form>	
<script type="text/javascript"> 
    Form.focusFirstElement($("refMasterForm")); 
</script>