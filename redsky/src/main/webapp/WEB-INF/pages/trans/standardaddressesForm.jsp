<%@ include file="/common/taglibs.jsp"%>   
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<head>
 
<title><fmt:message key="standardAddressesForm.title" /></title>
<meta name="heading"
	content="<fmt:message key='standardAddressesForm.heading'/>" />

	<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	<link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
<style type="text/css">
 .ui-autocomplete {
    max-height: 200px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }	
  
  .upper-case{
    text-transform:capitalize;
}
</style>	
<script language="javascript" type="text/javascript">

function getState(targetElement){
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6;
     http3.send(null);
}

function handleHttpResponse6(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['standardAddressesForm'].elements['standardAddresses.state'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['standardAddressesForm'].elements['standardAddresses.state'].options[i].text = '';
						document.forms['standardAddressesForm'].elements['standardAddresses.state'].options[i].value = '';
					}else{
						stateVal = res[i].split("#");
						document.forms['standardAddressesForm'].elements['standardAddresses.state'].options[i].text = stateVal[1];
						document.forms['standardAddressesForm'].elements['standardAddresses.state'].options[i].value = stateVal[0];
					
						if (document.getElementById("state").options[i].value == '${standardAddresses.state}'){
						   document.getElementById("state").options[i].defaultSelected = true;
						}
					}
				}
				document.getElementById("state").value = '${standardAddresses.state}';
        }
}

var http3 = getHTTPObject();
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getState1(targetElement){
	var country = document.forms['standardAddressesForm'].elements['standardAddresses.countryCode'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = handleHttpResponse66;
     http33.send(null);
}

function handleHttpResponse66(){
		if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['standardAddressesForm'].elements['standardAddresses.state'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.getElementById("state1").options[i].text = '';
						document.getElementById("state1").options[i].value = '';
					}else{
						stateVal = res[i].split("#");
						document.getElementById("state1").options[i].text = stateVal[1];
						document.getElementById("state1").options[i].value = stateVal[0];
					
						if (document.getElementById("state1").options[i].value == '${standardAddresses.state}'){
						   document.getElementById("state1").options[i].defaultSelected = true;
						}
					}
				}
				document.getElementById("state1").value = '${standardAddresses.state}';
        }
}
var http33 = getHTTPObject();

function autoPopulate_Country(targetElement) {		
	var oriCon=document.forms['standardAddressesForm'].elements['standardAddresses.countryCode'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['standardAddressesForm'].elements['standardAddresses.state'].disabled = false;
	  }else{
		  document.forms['standardAddressesForm'].elements['standardAddresses.state'].disabled = true;
	  }	 
}

function trim(str) {
        return str.replace(/^\s+|\s+$/g,"");
    }
function confirmValidate(){
var add1Len=document.forms['standardAddressesForm'].elements['standardAddresses.addressLine1'].value;
var mystr1;
mystr1 = trim(add1Len); 
mystr1=mystr1.length;
var add1Value=document.forms['standardAddressesForm'].elements['standardAddresses.addressLine1'].value;
var mystr;
mystr = trim(add1Value); 
document.forms['standardAddressesForm'].elements['standardAddresses.addressLine1'].value=mystr;
if(mystr=='')
{
alert('Please fill the mandatory field');
return false;
}
if(mystr1==0){
alert('AddressLine1 cannot be blank');
return false;
}

else{
return true;
}
}
function enableStateListOrigin(){ 
	var oriCon=document.forms['standardAddressesForm'].elements['standardAddresses.countryCode'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['standardAddressesForm'].elements['standardAddresses.state'].disabled = false;
	  }else{
		  document.forms['standardAddressesForm'].elements['standardAddresses.state'].disabled = true;
	  }	        
}

function newFunctionForCountryState(){	
	 var originAbc ='${standardAddresses.countryCode}';
	 var enbState = '${enbState}';
	 var index = (enbState.indexOf(originAbc)> -1);	
	 if(index != ''){
		 document.forms['standardAddressesForm'].elements['standardAddresses.state'].disabled =false;
	 getOriginStateReset(originAbc);
	 }
	 else{
		 document.forms['standardAddressesForm'].elements['standardAddresses.state'].disabled =true;
	 }	 
	 }

	function getOriginStateReset(target) {
		var country = target;
	 	var countryCode = "";
		<c:forEach var="entry" items="${countryCod}">
		if(country=="${entry.value}"){
			countryCode="${entry.key}";
		}
		</c:forEach>
		 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	     httpState1.open("GET", url, true);
	     httpState1.onreadystatechange = handleHttpResponse555555;
	     httpState1.send(null);
	}
	          
	 function handleHttpResponse555555(){
        if (httpState1.readyState == 4){
           var results = httpState1.responseText
           results = results.trim();
           res = results.split("@");
           targetElement = document.forms['standardAddressesForm'].elements['standardAddresses.state'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++) {
				if(res[i] == ''){
				document.forms['standardAddressesForm'].elements['standardAddresses.state'].options[i].text = '';
				document.forms['standardAddressesForm'].elements['standardAddresses.state'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				document.forms['standardAddressesForm'].elements['standardAddresses.state'].options[i].text = stateVal[1];
				document.forms['standardAddressesForm'].elements['standardAddresses.state'].options[i].value = stateVal[0];
				
				if (document.getElementById("state").options[i].value == '${standardAddresses.state}'){
				   document.getElementById("state").options[i].defaultSelected = true;
				} } }
				document.getElementById("state").value = '${standardAddresses.state}';
        }
   }  
	  var httpState1 = getHTTPObjectState1();	  
 	function getHTTPObjectState1()	 {
	     var xmlhttp;
	     if(window.XMLHttpRequest)
	     {
	         xmlhttp = new XMLHttpRequest();
	     }
	     else if (window.ActiveXObject)
	     {
	         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	         if (!xmlhttp)
	         {
	             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	         }
	     }
	     return xmlhttp;
	 } 
 	function autoCompleterAjaxCity(){
 		var countryName = document.forms['standardAddressesForm'].elements['standardAddresses.countryCode'].value;
 		var stateNameDes = document.forms['standardAddressesForm'].elements['standardAddresses.state'].value;
 		var cityNameDes = document.forms['standardAddressesForm'].elements['standardAddresses.city'].value;		
 		var countryNameDes = "";
 		<c:forEach var="entry" items="${countryCod}">
 		if(countryName=="${entry.value}"){
 			countryNameDes="${entry.key}";
 		}
 		</c:forEach>
 		if(cityNameDes!=''){
 		var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
 		$( "#city" ).autocomplete({				 
 		      source: data		      
 		    });
 		}
 	}
 	function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode; 
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
	}
 	function validate_email(targetElement,fieldName){
 	if(targetElement.value!=''){
 	if(targetElement.value.match(/\ /)){
 		alert("Please enter valid email address.");
 		document.forms['standardAddressesForm'].elements[fieldName].value ='';
 		return false
 	}
 	with (targetElement){
 	apos=value.indexOf("@")
 	dotpos=value.lastIndexOf(".")
 	if (apos<1||dotpos-apos<2){
 		alert("Please enter valid email address.");
 		document.forms['standardAddressesForm'].elements[fieldName].value ='';
 		return false
 	}else{
 		return true
 	}
 	}
 	}
 	}
</script>	
</head>

<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>

<s:form name="standardAddressesForm" action="saveStandardAddress">
<s:hidden  name="standardAddresses.id"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden name="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
	<s:hidden name="fld_eigthDescription" value="${param.fld_eigthDescription}" />	
	<s:hidden name="fld_ninthDescription" value="${param.fld_ninthDescription}" />
	<s:hidden name="fld_tenthDescription" value="${param.fld_tenthDescription}" />
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	<c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	<c:set var="fld_eigthDescription" value="${param.fld_eigthDescription}" />
	<c:set var="fld_ninthDescription" value="${param.fld_ninthDescription}" />
	<c:set var="fld_tenthDescription" value="${param.fld_tenthDescription}" />
</c:if>
<s:hidden  name="standardAddresses.createdBy"/>
<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${standardAddresses.updatedOn}" 
pattern="${displayDateTimeEditFormat}"/>
<s:hidden name="standardAddresses.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>

<fmt:formatDate var="customerFilecreatedOnFormattedValue" value="${standardAddresses.createdOn}" 
pattern="${displayDateTimeEditFormat}"/>
<s:hidden name="standardAddresses.createdOn" value="${customerFilecreatedOnFormattedValue}"/>

<s:hidden  name="standardAddresses.updatedBy"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="residenceManagement" value="N" />
<configByCorp:fieldVisibility componentId="component.field.StandardAddress.residenceManagement">		
	<c:set var="residenceManagement" value="Y" />
</configByCorp:fieldVisibility>
<c:if test="${!param.popup}">
	<div id="Layer1" style="width:100%">
	<div id="layer4" style="width:100%;">
		<div id="newmnav" style="!margin-top:15px;">
			<ul>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Standard Address Form</span></a></li>
			<li><a href="standardaddresses.html"><span>Standard Address List</span></a></li>
			<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${standardAddresses.id}&tableName=standardaddresses&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			</ul>
		</div>
		<div class="spn">&nbsp;</div>
			<div id="content" align="center" >
			<div id="liquid-round-top">
			<div class="top" style=""><span></span></div>
				<div class="center-content">						
					<table cellspacing="3" cellpadding="2" border="0">
						<tbody>
						<tr><td  class="bgblue" colspan="6">Job Type Detail</td></tr>
						<tr><td align="right" class="listwhitetext"><fmt:message key='standardAddresses.job'/></td>							
							<td style="padding-bottom:7px;" colspan="4"><s:select cssClass="list-menu"  name="standardAddresses.job" list="%{jobs}" cssStyle="width:260px"  onchange="changeStatus();" headerKey="" headerValue="" tabindex="1"/>
							</td></tr>							
							
							<tr><td  class="bgblue" colspan="6">Address Detail</td></tr>
							<!--<tr><td colspan="6" class="listwhitetext" style="line-height:5px;padding-bottom:7px;"><b>Address Details</b></td></tr>-->
						
							<tr>
							<c:if test="${residenceManagement=='N'}">
							<td width="95px" class="listwhitetext" align="right">Address Line 1<font size="2" color="red">*</td>
							</c:if>
							<c:if test="${residenceManagement=='Y'}">
							<td width="95px" class="listwhitetext" align="right">Residence Name<font size="2" color="red">*</td>
							</c:if>
							<td><s:textfield tabindex="2" cssClass="input-text upper-case" name="standardAddresses.addressLine1" onblur="titleCase(this)" size="38" maxlength="50" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.countryCode'/></td>
							<td ><s:select  cssClass="list-menu" tabindex="3" id="country" name="standardAddresses.countryCode" list="%{country}" cssStyle="width:162px"  headerKey="" headerValue=""  onchange="getState(this);autoPopulate_Country(this);enableStateListOrigin();"/></td>
							<td align="right" class="listwhitetext">Day Phone</td>
							<td><s:textfield tabindex="4" cssClass="input-text" name="standardAddresses.dayPhone" maxlength="20" size="30" onkeydown="return checkPhone(this);"  /></td>
							</tr>
							
							<tr>
								<td class="listwhitetext" align="right">Address Line 2</td>
								<td colspan=""><s:textfield tabindex="5" cssClass="input-text upper-case" onblur="titleCase(this)" name="standardAddresses.addressLine2"  size="38" maxlength="50"  /></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.state'/></td>
								<td align="left" class="listwhitetext" style="width:3px">
								<s:select cssClass="list-menu" id="state" tabindex="6" name="standardAddresses.state" list="%{states}" cssStyle="width:162px"  onchange="changeStatus();" headerKey="" headerValue="" /></td>
								<td align="right" class="listwhitetext">Home Phone</td>
								<td><s:textfield  tabindex="7" cssClass="input-text" name="standardAddresses.homePhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)" /></td>
							</tr>
							
							<tr>
								<td class="listwhitetext" align="right">Address Line 3</td>
								<td colspan=""><s:textfield tabindex="8" cssClass="input-text upper-case" onblur="titleCase(this)" name="standardAddresses.addressLine3" size="38" maxlength="50" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.zip'/></td>
								<td><s:textfield tabindex="9" cssClass="input-text" name="standardAddresses.zip" cssStyle="width:158px"  maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)"  /></td>													
								<td align="right" class="listwhitetext">Fax Number</td>
								<td><s:textfield tabindex="10" cssClass="input-text" name="standardAddresses.faxPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)"  /></td>
							</tr>
							
							<tr>
								<td class="listwhitetext" align="right">Email</td>
								<td><s:textfield tabindex="10" cssClass="input-text" name="standardAddresses.email" maxlength="60" size="38" onchange="validate_email(this,'standardAddresses.email')"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.city'/></td>
								<td><s:textfield tabindex="11" cssClass="input-text upper-case" name="standardAddresses.city" id="city" maxlength="20" cssStyle="width:158px" onblur="titleCase(this)" onkeyup="autoCompleterAjaxCity();" /></td>
								<td align="right" class="listwhitetext">Mob Phone</td>
								<td><s:textfield tabindex="12" cssClass="input-text" name="standardAddresses.mobPhone" maxlength="20" size="30" onkeydown="return onlyPhoneNumsAllowed(event)"  /></td>
							</tr>
							<c:if test="${residenceManagement=='Y'}">
							<tr><td  class="bgblue" colspan="6">Residence Management</td></tr>
							<tr>
							<td class="listwhitetext" align="right">Type</td>
							<td ><s:select  cssClass="list-menu" name="standardAddresses.residenceType" list="%{residenceType}" cssStyle="width:187px"  headerKey="" headerValue="" tabindex="13"/></td>
							<td align="right" class="listwhitetext">Residence Management&nbsp;Name</td>
							<td><s:textfield cssClass="input-text" name="standardAddresses.residenceMgmtName" maxlength="200" size="50" tabindex="14" /></td>
							<td class="listwhitetext" align="right">Contact Person</td>
							<td><s:textfield cssClass="input-text" name="standardAddresses.contactPerson" maxlength="80" size="38" /></td>
							</tr>
							<tr>
							<td class="listwhitetext" align="right">Deposit 1</td>
							<td colspan="1">
							<table style="margin:0px;" cellspacing="0" cellpadding="0" border="0">
							<tr>
							<td ><s:select  cssClass="list-menu" name="standardAddresses.residenceDeposit1" list="%{residenceDeposit}" cssStyle="width:100px"  headerKey="" headerValue="" tabindex="15"/></td>
							<td align="right" class="listwhitetext">&nbsp;Amount&nbsp;</td>
							<td><s:textfield cssClass="input-text" name="standardAddresses.depositAmount1" onkeydown="return onlyFloatNumsAllowed(event)" maxlength="8" size="4" tabindex="16" /></td>
							</tr>
							</table>
							</td>
							<td align="right" class="listwhitetext">Purpose</td>
							<td><s:textfield cssClass="input-text" name="standardAddresses.residencePurpose1" maxlength="100" size="50" tabindex="16" /></td>
							<td class="listwhitetext" align="right">Contact Phone</td>
							<td><s:textfield tabindex="10" cssClass="input-text" name="standardAddresses.contactPersonPhone" maxlength="20" size="38" /></td>
							</tr>
							<tr>
							<td class="listwhitetext" align="right">Deposit 2</td>
							<td colspan="1">
							<table style="margin:0px;" cellspacing="0" cellpadding="0" border="0">
							<tr>
							<td ><s:select  cssClass="list-menu" name="standardAddresses.residenceDeposit2" list="%{residenceDeposit}" cssStyle="width:100px"  headerKey="" headerValue="" tabindex="17"/></td>
							<td align="right" class="listwhitetext">&nbsp;Amount&nbsp;</td>
							<td><s:textfield cssClass="input-text" name="standardAddresses.depositAmount2" onkeydown="return onlyFloatNumsAllowed(event)" maxlength="8" size="4" tabindex="16" /></td>
							</tr>
							</table>
							</td>
							<td align="right" class="listwhitetext">Purpose</td>
							<td><s:textfield cssClass="input-text" name="standardAddresses.residencePurpose2" maxlength="100" size="50" tabindex="18" /></td>
							<td class="listwhitetext" align="right">Contact Email</td>
							<td><s:textfield tabindex="10" cssClass="input-text" name="standardAddresses.contactPersonEmail" maxlength="60" size="38" onchange="validate_email(this,'standardAddresses.contactPersonEmail')"/></td>
							</tr>
							<tr>
							<td width="105px" class="listwhitetext" align="right">Methods Of&nbsp;Payment</td>
							<td ><s:select  cssClass="list-menu" name="standardAddresses.residencePayType" list="%{payType}" cssStyle="width:187px"  headerKey="" headerValue="" tabindex="19"/></td>
							<td align="right" class="listwhitetext">Notes</td>
							<td><s:textarea name="standardAddresses.residenceNotes" cssStyle="width:270px;height:25px;" cssClass="textarea" /></td>
							</tr>
							</c:if>
							<tr>
								<td style="!height:10px;height:1px;">&nbsp;</td>
							</tr>
							</tbody>
					</table>
	  			</div>
				<div class="bottom-header"><span></span></div>
				</div>
				</div>		
					<table border="0" width="760px">
					<tbody>
							<tr>					
								
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn' /></b></td>
							<td style="width:120px"><s:label name="createdOn" value="${customerFilecreatedOnFormattedValue}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty standardAddresses.id}">
								
								<td style="width:85px"><s:label name="createdBy" value="%{standardAddresses.createdBy}"/></td>
							</c:if>
							<c:if test="${empty standardAddresses.id}">
								<td style=""><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn' /></b></td>
							<td style="width:120px" ><s:label name="updatedOn" value="${customerFileupdatedOnFormattedValue}"/></td>
						
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty standardAddresses.id}">
								
								<td style="width:85px"><s:label name="updatedBy" value="%{standardAddresses.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty standardAddresses.id}">
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
			
				</table>	
	<s:submit name="Save"  cssClass="cssbuttonA" value="Save" title="Save" onclick="return confirmValidate();"></s:submit>
	<input type="button"  tabindex="13" class="cssbuttonA"  onclick="location.href='<c:url value="/addStandardaddress.html"/>'" value="Add"/>
	<s:reset cssClass="cssbutton" key="Reset" cssStyle="width:70px; height:25px " onclick="newFunctionForCountryState()"/>
	</c:if>
	<c:if test="${param.popup}">

	<div id="Layer1" style="width:100%">
	<div id="layer4" style="width:100%;">
		<div id="newmnav" style="!margin-top:15px;">
			<ul>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Standard Address Form</span></a></li>
			<li><a href="standardaddresses.html?decorator=popup&popup=true&jobType=${standardAddresses.job}&fld_tenthDescription=${fld_tenthDescription}&fld_ninthDescription=${fld_ninthDescription}&fld_eigthDescription=${fld_eigthDescription}&fld_seventhDescription=${fld_seventhDescription}&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Standard Address List</span></a></li>
			 </ul>
			</div>
			<div class="spn">&nbsp;</div>
			
			<div id="content" align="center" >
				<div id="liquid-round-top">
				<div class="top" style=""><span></span></div>
					<div class="center-content">
					<table cellspacing="3" cellpadding="2" border="0">
						<tbody>
						<tr><td  class="bgblue" colspan="6">Job Type Detail</td></tr>
						<tr><td align="right" class="listwhitetext"><fmt:message key='standardAddresses.job'/></td>							
							<td style="padding-bottom:7px;" colspan="4"><s:select cssClass="list-menu"  name="standardAddresses.job" list="%{jobs}" cssStyle="width:260px" disabled="true" onchange="changeStatus();" headerKey="" headerValue="" />
							</td></tr>
							<tr><td  class="bgblue" colspan="6">Address Detail</td></tr>
							<!--<tr><td colspan="6" class="listwhitetext" style="line-height:5px;padding-bottom:7px;"><b>Address Details</b></td></tr>
						
							--><tr><td width="95px" class="listwhitetext" align="right">Address Line 1</td>
								<td colspan=""><s:textfield cssClass="input-text" name="standardAddresses.addressLine1" size="40" maxlength="30" disabled="true"  /></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.countryCode'/></td>
								<td ><s:select cssClass="list-menu" id="country" name="standardAddresses.countryCode" list="%{country}" disabled="true" cssStyle="width:162px"  headerKey="" headerValue=""  onchange="getState1(this);autoPopulate_Country(this);enableStateListOrigin();"/></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.dayPhone'/></td>
								<td><s:textfield cssClass="input-text" name="standardAddresses.dayPhone" maxlength="20" size="30" disabled="true" onkeydown="return checkPhone(this);"  /></td>
							</tr>
							
							<tr>
								<td class="listwhitetext" align="right">Address Line 2</td>
								<td colspan=""><s:textfield cssClass="input-text" name="standardAddresses.addressLine2"  size="40" disabled="true" maxlength="30"  /></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.state'/></td>
								<td align="left" class="listwhitetext" style="width:3px">
								<s:select cssClass="list-menu" id="state1" name="standardAddresses.state" list="%{states}" cssStyle="width:162px" disabled="true" onchange="changeStatus();" headerKey="" headerValue="" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.homePhone'/></td>
								<td><s:textfield cssClass="input-text" name="standardAddresses.homePhone" maxlength="20" size="30" disabled="true" onkeydown="return onlyPhoneNumsAllowed(event)" /></td>
							</tr>
							
							<tr>
								<td class="listwhitetext" align="right">Address Line 3</td>
								<td colspan=""><s:textfield cssClass="input-text" name="standardAddresses.addressLine3" size="40" disabled="true" maxlength="30" /></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.zip'/></td>
								<td><s:textfield cssClass="input-text" name="standardAddresses.zip" cssStyle="width:160px"  maxlength="10" disabled="true" onkeydown="return onlyAlphaNumericAllowed(event)"  /></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.faxPhone'/></td>
								<td><s:textfield cssClass="input-text" name="standardAddresses.faxPhone" maxlength="20" size="30" disabled="true" onkeydown="return onlyPhoneNumsAllowed(event)" /></td>
							</tr>
							
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.city'/></td>
								<td><s:textfield cssClass="input-text" name="standardAddresses.city" maxlength="20" size="26" disabled="true"  /></td>
								<td align="right" class="listwhitetext"><fmt:message key='standardAddresses.mobPhone'/></td>
								<td><s:textfield  cssClass="input-text" name="standardAddresses.mobPhone" maxlength="20" size="30" disabled="true" onkeydown="return onlyPhoneNumsAllowed(event)"  /></td>
							</tr>
							<tr>
								<td style="!height:10px;height:1px;">&nbsp;</td>
							</tr>
						</tbody>
					</table>
	  			</div>
				<div class="bottom-header"><span></span></div>
				</div>
			</div>	
	</c:if>
	</s:form>
				<c:if test="${param.popup}">
					<table border="0" width="760px">
					<tbody>
							<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn' /></b></td>
							<td style="width:120px"><s:label name="createdOn" value="${customerFilecreatedOnFormattedValue}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty standardAddresses.id}">
								
							<td style="width:85px"><s:label name="createdBy" value="%{standardAddresses.createdBy}"/></td>
							</c:if>
							<c:if test="${empty standardAddresses.id}">
								<td style=""><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn' /></b></td>
							<td style="width:120px" ><s:label name="updatedOn" value="${customerFileupdatedOnFormattedValue}"/></td>
						
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty standardAddresses.id}">
								
							<td style="width:85px"><s:label name="updatedBy" value="%{standardAddresses.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty standardAddresses.id}">
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
				</c:if>	
<SCRIPT LANGUAGE="JavaScript">
try{
if(document.getElementById('state').value==""){
document.getElementById('state').disabled = true;
 }
}
catch(e){}
try{
 var country=document.getElementById('country').value;
 }
 catch(e){}
 try{
	 var enbState = '${enbState}';
	 var oriCon=document.forms['standardAddressesForm'].elements['standardAddresses.countryCode'].value;
	 if(oriCon=="")	 {
			document.getElementById('state').disabled = true;
			document.getElementById('state').value ="";
	 }else{
	  		if(enbState.indexOf(oriCon)> -1){
				document.getElementById('state').disabled = false; 
				document.getElementById('state').value='${standardAddresses.state}';
			}else{
				document.getElementById('state').disabled = true;
				document.getElementById('state').value ="";
			} 
		}
	 }catch(e){}

		try{
			var enbState = '${enbState}';
			 var oriCon=document.forms['standardAddressesForm'].elements['standardAddresses.countryCode'].value;
			 if(oriCon=="")	 {
					document.getElementById('state1').disabled = true;
					document.getElementById('state1').value ="";
			 }else{
			  		if(enbState.indexOf(oriCon)> -1){
						document.getElementById('state1').disabled = false; 
						document.getElementById('state1').value='${standardAddresses.state}';
					}else{
						document.getElementById('state1').disabled = true;
						document.getElementById('state1').value ="";
					} 
				}		
			}catch(e){}
if(window.addEventListener)
				window.addEventListener('load',function(){
					getState(document.forms['standardAddressesForm'].elements['standardAddresses.countryCode']);
					getState1(document.forms['standardAddressesForm'].elements['standardAddresses.countryCode']);
					},false);
try{
	<c:if test="${param.popup}">
	if(document.getElementById('country').disabled = true){
		document.getElementById('state1').disabled = true;
	}
	</c:if>
}catch(e){}
</script>
