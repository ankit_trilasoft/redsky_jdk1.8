package com.trilasoft.app.service;

import java.util.List;

import com.trilasoft.app.model.TruckTrailers;
import org.appfuse.service.GenericManager;

public interface TruckTrailersManager extends GenericManager<TruckTrailers, Long>{

	public List getPrimaryFlagStatus(String truckNumber, String corpId);
	public List  findTruckTrailersList(String truckNumber,String corpId);
	public List trailersPopup();
	public List searchTrailerPopupList(String truckNumber,String ownerPayTo, String corpId);
	
}
