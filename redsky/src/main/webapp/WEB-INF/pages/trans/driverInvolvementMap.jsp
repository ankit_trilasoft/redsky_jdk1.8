<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/common/tooltip.jsp"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>   
    <title>Driver Involvement Map</title><!--  
    <META HTTP-EQUIV='Pragma' CONTENT='no-cache'">
	<META HTTP-EQUIV="Expires" CONTENT="-1"> 
    --><meta name="heading" content="Driver Involvement Map"/>   
    <script language="javascript" type="text/javascript">
</script> 
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:3px;
margin-top:-9px;
!margin-top:-7px;
padding:2px 0px;
text-align:right;
width:100%;
}
form {
margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}
.listheadred {    
 color: red;
 font-family: arial,verdana;
 font-size: 11px;
 font-weight: bold;    
 text-decoration: none;
}
#content {
color:#003366;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
z-index:901;
}

#content-containertable.override{
width:212px;
}

#content-containertable.override div{
height:460px;
overflow:scroll;
}
</style> 
<style>
.rounded-top {
   -moz-border-radius-topleft: .8em;
   -webkit-border-top-left-radius:.8em;
   -moz-border-radius-topright: .8em;
   -webkit-border-top-right-radius:.8em;
   border:1px solid #AFAFAF;
   border-bottom:none;   
   }
.rounded-bottom {
   -moz-border-radius-bottomleft: .8em;
   -webkit-border-bottom-left-radius:.8em;
   -moz-border-radius-bottomright: .8em;
   -webkit-border-bottom-right-radius:.8em;
   border:1px solid #AFAFAF;
   border-top:none;   
}

</style> 
<!--<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>-->
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>" djConfig="parseOnLoad:true, isDebug:false"></script>
<script>
function initLoader() { 
	 var script = document.createElement("script");
     script.type = "text/javascript";
  	 script.src = "http://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=loadMap";
     document.body.appendChild(script); 
}
var geocoder;
var map;
var letter1 = "";
var letter = "";
var letter3 = "";
var flag1="0";
var count1=-1;
var k=1;
var letterMap = new Object();
var driverDataMap = new Object();
var driverHomeDataMap = new Array();
var varMap = new Object();
var input = new Array();

function loadMap() { 
	var loc=document.forms['driverInvolvementForm'].elements['vLocation'].value;
	if(loc!=''){	
	var locArray=loc.split(",");
	var address1="";
	var terminalCity="";
	var terminalZip="";
	var terminalState="";
	var terminalCountry ="";
	if(locArray!="")
	{
	terminalCity=locArray[0];
	terminalState=locArray[1];
	}
	var addressL = address1+","+terminalCity+" , "+terminalZip+","+terminalState+","+terminalCountry;
	map = new GMap2(document.getElementById('map'));
	geocoder = new GClientGeocoder();
	geocoder.getLocations(addressL, addToMap);}
	else{
    map = new GMap2(document.getElementById('map'));
	geocoder = new GClientGeocoder();
	loadHomeAddress();
	}
}
 
function addToMap(response)
   {
      if(!response.Placemark) return;
      place = response.Placemark[0];
      point = new GLatLng(place.Point.coordinates[1],
                          place.Point.coordinates[0]);

		document.forms['driverInvolvementForm'].elements['geoLatitude'].value = place.Point.coordinates[1];
		document.forms['driverInvolvementForm'].elements['geoLongitude'].value = place.Point.coordinates[0];
	  addBasicInfoToMap(point, null); 
	  map.setUIToDefault();
	  loadHomeAddress();
	 
      
   }
 function loadHomeAddress()
 {
 var homeloc=document.forms['driverInvolvementForm'].elements['homeAddress'].value;	
	driverHomeDataMap[0]=homeloc;
 geocoder.getLocations(homeloc, addToMap1);
 }  

function addToMap1(response)
   {
      if(!response.Placemark) return;
      place = response.Placemark[0];
      point = new GLatLng(place.Point.coordinates[1],
                          place.Point.coordinates[0]);
		document.forms['driverInvolvementForm'].elements['geoLatitude'].value = place.Point.coordinates[1];
		document.forms['driverInvolvementForm'].elements['geoLongitude'].value = place.Point.coordinates[0];
	  addBasicInfoToMap1(point, null); 
	  map.setUIToDefault();
	  var add=driverHomeDataMap[0];
	  var dName=document.forms['driverInvolvementForm'].elements['driverName'].value;
	   var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Driver Name:</b> '+dName+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Address:</b> '+add+' </h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
       		var fn1 = markerClickHomeFnPorts(point,infoHtml);
				GEvent.addListener(marker1, "mouseover", fn1);
	  getPortsPoint();     
   }
   	  function markerClickHomeFnPorts(point, portDetails) {
	   return function() {
	   map.openInfoWindowHtml(point, portDetails);
	}
	}
         function addBasicInfoToMap1(point, partnerDetails){
		var targetIcon = new GIcon(G_DEFAULT_ICON);
		targetIcon.image = "${pageContext.request.contextPath}/images/1307701944_Home.png";
			targetIcon.iconSize = new GSize(27, 28);		
		markerOptions = { icon:targetIcon };
	     marker1 = new GMarker(point, markerOptions); 
		 map.addOverlay(marker1, markerOptions);	
		 map.setCenter(point, 8);	
		  map.setUIToDefault();
	  }

         function addBasicInfoToMap(point, partnerDetails){
		var targetIcon = new GIcon(G_DEFAULT_ICON);
		targetIcon.image = "${pageContext.request.contextPath}/images/lorrygreen2.png";		             
		targetIcon.iconSize = new GSize(27, 28);		
		markerOptions = { icon:targetIcon };
	     marker = new GMarker(point, markerOptions); 
		 map.addOverlay(marker, markerOptions);
		  map.setCenter(point, 8);
		  map.setUIToDefault();
	 	
	  }
function getPortsPoint() { 
	var address1="";
	var city="";
	var zip="";
	var state="";
	var country = "";
		dojo.xhrPost({
	       form: "driverInvolvementForm",
	       url:"driverInvolvementPoint.html",
	       timeout: 900000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){
	       			var j = 0;
	           for(var i=0; i < jsonData.ports.length; i++){
				   var address = jsonData.ports[i].address;	    
				   var addressDriverCell = jsonData.ports[i].driverCell;				   
				   document.forms['driverInvolvementForm'].elements['driverCell'].value=addressDriverCell;
	        	   console.log("address: " + address);
	        	   driverDataMap[jsonData.ports[i].address] = jsonData.ports[i];				   
	        	   letterMap[jsonData.ports[i].address] = k;
	        	   k++;
	        	   varMap[jsonData.ports[i].address] = i;
	               geocoder.getLocations(address, addDriverToMap); 
    	           
	        	}
	       }	       
	   });
}
     var j = 0; 
     
   function addDriverToMap(response)
   {
         if(!response.Placemark) return;
   			   place = response.Placemark[0];
		      point = new GLatLng(place.Point.coordinates[1], place.Point.coordinates[0]);
	          map.setZoom(4);
	     	  var baseIcon = new GIcon(G_DEFAULT_ICON);
  	          var letteredIconR = new GIcon(baseIcon);
  	          var pinColor=driverDataMap[response.name].loadType;
  	          if(pinColor=='LD')
  	          {
				    letteredIconR.image = "http://chart.apis.google.com/chart?chst=d_map_spin&chld=4.5|0|00FF00|100|b|"+letterMap[response.name];			  			   						    
			  }
  	          else if(pinColor=='DL')
  	          {
				    letteredIconR.image = "http://chart.apis.google.com/chart?chst=d_map_spin&chld=4.5|0|FF0000|100|b|"+letterMap[response.name];			  			   						    
			  }else
			  {
			  letteredIconR.image = "http://chart.apis.google.com/chart?chst=d_map_spin&chld=4.5|0|2E64FE|100|b|"+letterMap[response.name];	
			  }
			  

				    markerOptions = { icon:letteredIconR };
				    marker = new GMarker(point, markerOptions); 
					map.addOverlay(marker, markerOptions);
					if(point!='')
					{					 					
					 input[parseInt(varMap[response.name])]=point;
					}
					map.setUIToDefault();								
	  			    map.disableScrollWheelZoom();

			var fn = markerClickFnPorts(point,driverDataMap[response.name]);

				GEvent.addListener(marker, "mouseover", fn);
				GEvent.addListener(marker, "mouseout", onMouseOutFunc);
				GEvent.addListener(marker, "click", Moveover_Ident);
				
   }
 function onMouseOutFunc() {
                      var point = this;                     
                      setTimeout("map.closeInfoWindow()",2000);
                    }
   
	function drawLine()
	{
	var flag2=0;
		for(k=0;k<input.length;k++)
		{
			if(input[k]!=undefined)
			{
				var tempP2=input[k];								
					if(flag2!=0)
					{
						polyline = new GPolyline([  tempP1,  tempP2], "#0099CC", 5,1);					 
	   					map.addOverlay(polyline);
						var tempP1=tempP2;	   					 
					
	   				}
					if(flag2==0)
					{
  					    var tempP1=tempP2;
						polyline = new GPolyline([  tempP1,  tempP2], "#0099CC", 5,1);					 
	   					map.addOverlay(polyline);
	   					flag2=1; 
	   				}
			
			}
		}
	}
	  function markerClickFnPorts(point, portDetails) {
	   return function() {
		   if (!portDetails) return;
		   var driverid = portDetails.driverid
		   var regNo = portDetails.regNo
		   var location = portDetails.location
		   var loadType = portDetails.loadType
		   var keyDate = portDetails.keyDate
		   var name = portDetails.name
		   var estWt = portDetails.estWt
		   var estCube = portDetails.estCube
		   var actWt = portDetails.actWt
		   var actCube = portDetails.actCube
		   var shipNumber = portDetails.shipNumber
		   
		   if(driverid=='null')		   {		   driverid='';		   }
		   if(regNo=='null')		   {		   regNo='';		   }
		   if(location=='null')		   {		   location='';		   }
		   if(loadType=='null')		   {		   loadType='';		   }
		   if(keyDate=='null')		   {		   keyDate='';		   }
		   if(name=='null')		       {		   name='';		       }
		   if(estWt=='null')		   {		   estWt='';		   }
		   if(estCube=='null')		   {		   estCube='';		   }
		   if(actWt=='null')		   {		   actWt='';		   }
		   if(actCube=='null')		   {		   actCube='';		   }
   		   if(shipNumber=='null')	   {		   shipNumber='';	   }
		   
		   if((estWt!='null')||(estWt!=''))		   {		   estWt=estWt.substring(0,estWt.indexOf('.'));		   }
		   if((estCube!='null')||(estCube!=''))		   {		   estCube=estCube.substring(0,estCube.indexOf('.'));		   }
		   if((actWt!='null')||(actWt!=''))		   {		   actWt=actWt.substring(0,actWt.indexOf('.'));		   }
		   if((actCube!='null')||(actCube!=''))		   {		   actCube=actCube.substring(0,actCube.indexOf('.'));		   }

if((estWt!='')&&(estCube!=''))
	{
		   if(loadType=='LD'){
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Loading Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b> <span style="width:60px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px; padding-left:5px;">'+actWt+'</span> </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px; padding-left:9px;">'+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }
		   else if(loadType=='DL'){
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Delivery Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b><span style="width:60px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px; padding-left:5px;">'+actWt+'</span></h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px; padding-left:5px;">'+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }else
	       {
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Delivery Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b><span style="width:60px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px; padding-left:5px;">'+actWt+'</span></h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px; padding-left:5px;">'+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }
	 }
	if((estWt!='')&&(estCube==''))
	{
		   if(loadType=='LD'){
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Loading Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b> <span style="width:60px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px; padding-left:5px;">'+actWt+' </span></h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px;  margin-right:34px; padding-left:9px;">'+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }
		   if(loadType=='DL'){
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Delivery Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b><span style="width:60px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px; padding-left:5px;">'+actWt+'</span></h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px;  margin-right:34px; padding-left:5px;">'+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }else
	       {
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Delivery Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b><span style="width:60px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px; padding-left:5px;">'+actWt+'</span></h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px; padding-left:5px;">'+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }
	 }
	if((estWt=='')&&(estCube!=''))
	{
		   if(loadType=='LD'){
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Loading Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b> <span style="width:60px;  margin-right:34px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px;  padding-left:5px;">'+actWt+' </span></h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px; padding-left:9px;"> '+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }
		   if(loadType=='DL'){
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Delivery Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b><span style="width:60px;  margin-right:34px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px;  padding-left:5px;">'+actWt+' </span></h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px; padding-left:5px;"> '+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }else
	       {
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Delivery Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b><span style="width:60px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px; padding-left:5px;">'+actWt+'</span></h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px; padding-left:5px;">'+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }
	 }
	if((estWt=='')&&(estCube==''))
	{
		   if(loadType=='LD'){
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Loading Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b> <span style="width:60px;  margin-right:34px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px;  padding-left:5px;">'+actWt+'</span> </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px;  margin-right:34px; padding-left:9px;"> '+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }
		   if(loadType=='DL'){
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Delivery Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b><span style="width:60px;  margin-right:34px; padding-left:5px;">'+estWt+'</span><b> Act:</b><span style="width:60px; padding-left:5px;">'+actWt+'</span> </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px; margin-right:34px; padding-left:5px;"> '+estCube+' </span> <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }else
	       {
	       var infoHtml = '<div style="width:100%;"><h4 style="line-height:0px;"><b>Reg#:</b> '+regNo+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>SO#:</b> '+shipNumber+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Location:</b> '+location+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Delivery Date:</b>  '+keyDate+' </h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Wt. Est:</b><span style="width:60px; padding-left:5px;">'+estWt+'</span> <b>Act:</b><span style="width:60px; padding-left:5px;">'+actWt+'</span></h4><br style="line-height:11px;"><h4 style="line-height:0px;"><b>Vol. Est:</b><span style="width:60px; padding-left:5px;">'+estCube+' </span>  <b> Act:</b> <span style="width:60px; padding-left:5px;">'+actCube+' </span></h4>'+
	       '<br style="line-height:11px;"><span style="padding-left:120px;"></span>';
	       infoHtml += '</div></div>';
	       }
	 }	       map.openInfoWindowHtml(point, infoHtml);
	   }
   }

function serviceOrderBySeqNumber1(sequenceNumber,position){
	var url="trackingInfoByShipNumber.html?sequenceNumber="+sequenceNumber+"&decorator=simple&popup=true";
	ajax_SoTooltip(url,position);	
}   

function goNext() {
showOrHide(0);
	var driverIdData =document.forms['driverInvolvementForm'].elements['driverIdData'].value;
	if(driverIdData!=''){
	var url="editNextDriverLocationList.html?ajax=1&decorator=simple&popup=true&driverId="+encodeURI(driverIdData);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null);
     }else{
     alert('Please select Driver.');
     }
   }
   
  function handleHttpResponseOtherShip(){
  var driverIdData =document.forms['driverInvolvementForm'].elements['driverIdData'].value;
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               var res=results.split("~");              
               var driverNameDup = res[0]+" "+res[1];
               var vLocationDup = res[2];
               var driverVanId =res[3];
               location.href =  "driverInvolvementMap.html?driverId="+driverIdData+"&driverName="+driverNameDup+"&vLocation="+vLocationDup+"&driverVanId="+driverVanId+"&decorator=popup&popup=true";              
               showOrHide(1);
             }
       }    
   var http5 = getHTTPObject5();
	function getHTTPObject5()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}	
function findTripNumber(tripNumber,position){
	var url="findTripNumberList.html?ajax=1&decorator=simple&popup=true&tripNumber=" + encodeURI(tripNumber);
	ajax_showTooltip(url,position);
}
</script>  
</head>
<s:form id="driverInvolvementForm" action="driverInvolvementPoint" method="post" validate="true" >
<s:hidden name="geoLatitude" />
<s:hidden name="geoLongitude"/>
<s:hidden name="driverId"/>
<s:hidden name="homeAddress"/>
<s:hidden name="vLocation"/>
<s:hidden name="driverName" />

<s:hidden name="driverVanId"/>

<div id="layer1" style="width:100%;">
<div style="float:left;width:100%;margin-top:-8px;!margin-top:8px;">
<table style="margin:0px;padding:0px;width:100%;" border="0" cellpadding="0" cellspacing="0"> 
    <tr>
      <td align="left" class="listwhitetext" width="156">
		<b>Current assigned loads for:&nbsp;</b>   
      </td>     
      <td align="right" class="listwhitetext"><b>Driver&nbsp;</b></td>
   <td align="left" width="80px"><s:select cssClass="list-menu" name="driverIdData" value="%{driverId}" list="%{driverDropDownList}" headerKey="" headerValue="" cssStyle="width:145px"/></td>
 	<td align="right" width="2px"></td>
   <td><input type="button" class="cssbutton" style="width:50px; height:21px;padding:0 0 2px;cursor:pointer;" name="goto" value="Select" onclick="goNext();" /></td>
       <td align="left" class="listwhitetext" width="">
		<b>Driver#</b>&nbsp;<font color='red'>(<c:out value="${driverVanId}"/>)</font>&nbsp;
      </td>
      <td align="left" class="listwhitetext" width="">
		<b> Cell:&nbsp;</b>&nbsp;<s:textfield cssClass="balanceBox" name="driverCell"  readonly="true" size="15" cssStyle="text-align:left"/>
      </td>
      
   <td align="right" style=""></td>
   <td align="right">
   <input type="button" class="cssbutton" style="width:85px; height:21px;padding:0 0 2px;cursor:pointer;" name="showLine" value="Show route" onclick="drawLine();" />
   </td>
   <td width="" colspan="5" align="right">
   <table class="detailTabLabel">
   <tr>
   	<td><img  src="http://chart.apis.google.com/chart?chst=d_map_spin&chld=0.4|0|2E64FE|20|b|" /></td><td class="listwhitetext" style="font-size:12px;">Contractor</td><td></td><td><img src="http://chart.apis.google.com/chart?chst=d_map_spin&chld=0.4|0|00FF00|20|b|" /></td><td class="listwhitetext" style="font-size:12px;"> Origin</td><td></td><td><img src="http://chart.apis.google.com/chart?chst=d_map_spin&chld=0.4|0|FF0000|20|b|" /></td><td class="listwhitetext" style="font-size:12px;"> Destination</td>
   	</tr>
   	</table>
   </td>
   </tr>
  </table>
  </div> 
<div class="spnblk" style="clear:both;">&nbsp;</div>
<table  width="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
	<tbody>
	<tr>
	<td>
	<% int count=0; %> 
    <% String driverName=request.getParameter("driverName"); %> 
    
<s:set name="driverInvolvementList" value="driverInvolvementList" scope="request"/>  
<display:table defaultsort="9" defaultorder="ascending"  name="driverInvolvementList" class="table" id="driverInvolvementDtoList" export="false" requestURI="" pagesize="30" style="width:100%;margin-bottom:8px;"> 
<display:column  property="regNo" title="Reg#" sortable="true" style="width:85px"/>
<display:column  property="job" title="Job" sortable="true" style="width:25px"/>
<display:column  title="SO#" sortable="true" style="width:105px">
<c:if test="${driverInvolvementDtoList.shipNumber!=''}">
<img align="top" onclick="serviceOrderBySeqNumber1('${driverInvolvementDtoList.shipNumber}',this);" src="${pageContext.request.contextPath}/images/plus-small.png"/>
<a onclick=window.open('editServiceOrderUpdate.html?id=${driverInvolvementDtoList.id}',"width=250,height=70,scrollbars=1");>
<c:out value="${driverInvolvementDtoList.shipNumber}"/>
</a>
</c:if>
</display:column>
<display:column  property="name" title="Shipper" sortable="true" style="width:125px"/>
<display:column  property="location" title="Location" sortable="true" style="width:135px"/>
<display:column  value="<%=++count %>" title="Step" sortable="true" style="width:20px;text-align: right;"/>
<display:column title="Trip#" sortable="true" style="width:135px">
<c:out value="${driverInvolvementDtoList.tripNumber}"></c:out>
<c:if test="${driverInvolvementDtoList.tripNumber!=''}">
<img id="target" onclick ="findTripNumber('${driverInvolvementDtoList.tripNumber}',this);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
</c:if>
</display:column>

<display:column  title="Action" sortable="true" style="width:50px">
<c:if test="${driverInvolvementDtoList.loadType=='LD'}">
<c:out value="Loading"/>
</c:if>
<c:if test="${driverInvolvementDtoList.loadType=='DL'}">
<c:out value="Delivery"/>
</c:if>

<c:if test="${driverInvolvementDtoList.loadType=='CPK'}">
<c:out value="Cont-Packing"/>
</c:if>

<c:if test="${driverInvolvementDtoList.loadType=='CLD'}">
<c:out value="Cont-Loading"/>
</c:if>

<c:if test="${driverInvolvementDtoList.loadType=='CDL'}">
<c:out value="Cont-Delivery"/>
</c:if>

<c:if test="${driverInvolvementDtoList.loadType=='SIT'}">
<c:out value="Cont-Delv.SIT"/>
</c:if>
<c:if test="${driverInvolvementDtoList.loadType=='EV'}">
<c:out value="EV"/>
</c:if>
</display:column>
<display:column property="listDate" title="Date" sortable="true" format="{0,date,dd MMM, yyyy}"  style="width:85px;text-align: left;"/>
<display:column  property="vanLineNationalCode" title="Nat'l Act" sortable="true" style="width:45px"/>
<display:column  property="selfHaul" title="Haul" sortable="true" style="width:40px"/>
<display:column  property="estimatedRevenue" title="Est Rev." sortable="true" style="width:45px"/>
<display:column  property="mile" title="Mile" sortable="true" style="width:25px"/>
<display:column headerClass="containeralign" title="Weight" sortable="true"    style="width:40px;text-align: right;" >	
	<c:if test="${driverInvolvementDtoList.actWt!=''}">
		<c:out value="${driverInvolvementDtoList.actWt}"/><b class="listheadred">(A)</b>
	</c:if>
	<c:if test="${driverInvolvementDtoList.actWt==''}">
		<c:if test="${driverInvolvementDtoList.estWt!=''}">
			<c:out value="${driverInvolvementDtoList.estWt}"/><b class="listheadred">(E)</b>
		</c:if>
		<c:if test="${driverInvolvementDtoList.estWt==''}">
			<c:out value=""/>
		</c:if>
	</c:if>
</display:column>
<display:column headerClass="containeralign" title="Volume" sortable="true"  style="width:40px;text-align: right;">	
	<c:if test="${driverInvolvementDtoList.actCube!=''}">
		<c:out value="${driverInvolvementDtoList.actCube}"/><b class="listheadred">(A)</b>
	</c:if>
	<c:if test="${driverInvolvementDtoList.actCube==''}">
		<c:if test="${driverInvolvementDtoList.estCube!=''}">
			<c:out value="${driverInvolvementDtoList.estCube}"/><b class="listheadred">(E)</b>
		</c:if>
		<c:if test="${driverInvolvementDtoList.estCube==''}">
			<c:out value=""/>
		</c:if>
	</c:if>
</display:column>
<display:column headerClass="containeralign"  property="cumVol" title="Cum Vol" style="width:30px;text-align: right;"/>	
<display:setProperty name="paging.banner.item_name" value="Driver Involvement"/>   
<display:setProperty name="paging.banner.items_name" value="Driver Involvement"/> 
<display:setProperty name="export.excel.filename" value="Driver Involvement List.xls"/>   
<display:setProperty name="export.csv.filename" value="Driver Involvement List.csv"/>   
<display:setProperty name="export.pdf.filename" value="Driver Involvement List.pdf"/> 
</display:table>
</td>
</tr>
</tbody>
</table>
</div> 
<div id="layerH" style="position:absolute;left:30%;!100px;top:25%; z-index:910; ">
		<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
				<td align="center">
					<table cellspacing="0" cellpadding="3" align="center">
						<tr>
							<td height="200px"></td>
						</tr>
						<tr>
				       		<td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
				           		<font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait, Map is loading.......</font>
				       		</td>
				       </tr>
       					<tr>
     			 			<td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           						<img src="<c:url value='/images/ajax-loader.gif'/>" />     
       						</td>
       					</tr>
       				</table>
       			</td>
      		</tr>
       	</table>
	</div>
	<table width="100%" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0"> 
    <tr>
      <td align="left" width="80%" valign="top">
		   <div id="map" style="width:100%;border:1px dotted #219DD1; height: 550px"></div>
      </td>
      <B>Note</B>:-If you are unable to view the map, this may be due to Firefox security. Please
click on the grey shield icon on the left hand corner of the browsers address
bar and accept to view "Mixed Mode Content" for this page.
   </tr>
  </table>
</s:form>
<script>
setTimeout("showOrHide(0)",6000);
</script>

<script type="text/javascript">
try{
<c:if test="${not empty driverInvolvementDtoList}">
initLoader();
</c:if>
<c:if test="${empty driverInvolvementDtoList}">
		document.forms['driverInvolvementForm'].elements['showLine'].disabled=true;
</c:if>

}
catch(e){}
</script>