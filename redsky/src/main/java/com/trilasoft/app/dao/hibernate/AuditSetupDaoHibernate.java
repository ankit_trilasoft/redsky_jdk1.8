//----Created By Bibhash---

package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.AuditSetupDao;
import com.trilasoft.app.model.AuditSetup;
import com.trilasoft.app.model.DataCatalog;


public class AuditSetupDaoHibernate extends GenericDaoHibernate<AuditSetup, Long> implements AuditSetupDao{

	Logger logger = Logger.getLogger(AuditSetupDaoHibernate.class);
	
	private List auditSetups;
	public AuditSetupDaoHibernate() {   
        super(AuditSetup.class);   
    }  
	
	public List findMaximumId() {
		try{
    	return getHibernateTemplate().find("select max(id) from AuditSetup");
		 }catch (Exception e) {
				logger.error("Error executing query"+ e,e);
			}
			return null;
    }
	
	static String replaceWord(String original, String find, String replacement)
	{
		int i = original.indexOf(find);
	    if (i < 0) {
	        return original;  // return original if 'find' is not in it.
	    }
	  
	    String partBefore = original.substring(0, i);
	    String partAfter  = original.substring(i + find.length());
	  
	    return partBefore + replacement + partAfter;

	}
	
	public List findById(Long id) {
		try{
    	return getHibernateTemplate().find("select id from AuditSetup where id=?",id);
		 }catch (Exception e) {
				logger.error("Error executing query"+ e,e);
			}
			return null;
    }
	
	public List<AuditSetup> searchAuditSetup(String corpID, String tableName, String fieldName,String auditable, String isAlertValue) {
		try{
			if (!isAlertValue.equalsIgnoreCase("")) {
				String query="from AuditSetup where tableName like '%" +tableName.replaceAll("'", "''")+"%' AND fieldName like '%" +  fieldName.replaceAll("'", "''") + "%' AND corpID like '" + corpID.replaceAll("'", "''") + "%' AND auditable like '"+auditable+"%' and alertValue like '%"+isAlertValue.replaceAll("'", "''")+"%'";
				return getHibernateTemplate().find(query);
			}else {
				String query1="from AuditSetup where tableName like '%" +tableName.replaceAll("'", "''")+"%' AND fieldName like '%" +  fieldName.replaceAll("'", "''") + "%' AND corpID like '" + corpID.replaceAll("'", "''") + "%' AND auditable like '"+auditable+"%' and (alertValue like '%"+isAlertValue.replaceAll("'", "''")+"%' or alertValue is null)";
				return getHibernateTemplate().find(query1);
			}
			}catch (Exception e) {
				logger.error("Error executing query"+ e,e);
			}
			return null;
	}
	public List auditSetupTable(String b)
	{  try{
		return getHibernateTemplate().find("SELECT distinct(tableName)  FROM DataCatalog  where auditable='true'");
	}catch (Exception e) {
		logger.error("Error executing query"+ e,e);
	}
	return null;
}
	public List auditSetupField(String tableName,String b)
	{ try{
		return getHibernateTemplate().find("SELECT distinct(fieldName) from DataCatalog where tableName="+"'"+tableName+"'"+"AND auditable='true' order by fieldName asc ");
	}catch (Exception e) {
		logger.error("Error executing query"+ e,e);
	}
	return null;
}

	public List getList(String corpID) {
       try{
		return getHibernateTemplate().find("from AuditSetup where corpID='"+corpID+"' order by tableName");
	}catch (Exception e) {
		logger.error("Error executing query"+ e,e);
	}
	return null;
}

	public List checkData(String tableName, String fieldName, String sessionCorpID) {
try{
		return getHibernateTemplate().find("from AuditSetup where tableName='"+tableName+"' and fieldName='"+fieldName+"' and corpID='"+sessionCorpID+"'");
	}catch (Exception e) {
		logger.error("Error executing query"+ e,e);
	}
	return null;
}
}
