package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.Costing;


import java.util.*;

	
public interface CostingDao extends GenericDao<Costing, Long>{
    public List findMaxId();
    public List checkById(Long id);
    public int genrateInvoiceNumber(Long newInvoiceNumber,String shipNumber);
    public List findInvoiceNumber();
   
}