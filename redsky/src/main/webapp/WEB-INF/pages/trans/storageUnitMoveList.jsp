<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="storageList.title" /></title>
<meta name="heading" content="<fmt:message key='storageList.heading'/>" />
<script type="text/javascript">
		function check(targetElement) {
			  //document.forms['assignItemsForm'].elements['id'].value=targetElement.value;
			  //document.forms['assignItemsreForm'].elements['id'].value=targetElement.value;
			  document.forms['assignItemsmoForm'].elements['id1'].value=targetElement.value;
			  //document.forms['assignItemsForm'].elements['locate'].value=document.forms['assignItemsForm'].elements['id'].value;
			  //document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
			  //document.forms['assignItemsreForm'].elements['forwardBtn1'].disabled = false ;
			  document.forms['assignItemsmoForm'].elements['forwardBtn2'].disabled = false ;
			  //document.forms['assignItemsForm'].elements['id1'].value=targetElement.value;
		  }
</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="storageListForm" action="searchStorages" method="post">
<div id="Layer1" style="width:100%">
    <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="serviceOrder.id" />
			<div id="newmnav">
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <c:choose>
			     <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
					 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>  
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose>
			     
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
					 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			    <c:otherwise> 
		          <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>
		      </sec-auth:authComponent>
			  <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
			  <c:if test="${serviceOrder.job =='INT'}">
			    <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  <c:if test="${serviceOrder.job =='RLO'}">
			  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <c:if test="${serviceOrder.job !='RLO'}">
			  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div><div style="padding-bottom:3px;"></div>
	
	<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
		<div id="newmnav">
		   			  <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
					  	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Storage List</span></a></li>
						<li><a href="searchUnitLocates.html?locationId=&type=&occupied=&warehouse=${workTicket.warehouse}&id=${workTicket.id}"><span>Add Storage</span></a></li>
					  	<li><a href="storageUnit.html?id=${workTicket.id}"><span>Access/Release Storage</span></a></li>
					  	<li><a href="storageUnitMove.html?id=${workTicket.id}"><span>Move Storage Location</span></a></li>
					  	<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					  	
					</ul>
		</div><div class="spn">&nbsp;</div>


	
	
	<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			<td>
				<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
				<table>
					<tbody>
						<tr>
							<td width="10px"></td>
							<td class="listwhite">Ticket Number</td>
							<td><s:textfield name="workTicket.ticket" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
							<td width="10px"></td>
							<td class="listwhite">Warehouse</td>
							<td><s:textfield name="workTicket.warehouse" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
						</tr>
					</tbody>
				</table>
				</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<s:set name="storages" value="storages" scope="request" />
	<div id="otabs">
		<ul>
			<li><a class=""><span>List</span></a></li>
		</ul>
	</div>
	<div class="spnblk">&nbsp;</div> 
			<display:table name="storages" class="table" requestURI="" id="storageList" export="true" pagesize="50">
				<c:if test="${storageList.releaseDate==null}">
				<display:column><input type="radio"  name="dd" onclick="check(this)" value="${storageList.id}" /></display:column>
				</c:if>
				<c:if test="${storageList.releaseDate!=null}">
				<display:column><input type="radio" disabled="disabled" name="dd" onclick="check(this)" value="${storageList.id}" /></display:column>
				</c:if>
				<display:column property="ticket" sortable="true" titleKey="storage.ticket" />
				<display:column property="description" sortable="true" titleKey="storage.description" maxLength="15"/>
		 		<display:column property="locationId" sortable="true" titleKey="storage.locationId" />
				<display:column property="releaseDate" sortable="true" titleKey="storage.releaseDate" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
				<display:column property="containerId" sortable="true" titleKey="storage.containerId" />
				<display:column property="itemTag" sortable="true" titleKey="storage.itemTag"  style="width:120px"/>
				<display:column property="model" sortable="true" titleKey="storage.model" />
				<display:column property="serial" sortable="true" titleKey="storage.serial" />
				<display:column property="pieces" sortable="true" titleKey="storage.pieces" />
				
				<display:setProperty name="paging.banner.item_name" value="storage" />
				<display:setProperty name="paging.banner.items_name" value="storage" />
				
				<display:setProperty name="export.excel.filename" value="Storage List.xls" />
				<display:setProperty name="export.csv.filename" value="Storage List.csv" />
				<display:setProperty name="export.pdf.filename" value="Storage List.pdf" />
			</display:table>

			</td>
		</tr>
	</tbody>
</table>
</div>   
	<c:out value="${buttons}" escapeXml="false" />
	<s:hidden name="locate"  />
		
</s:form>
<table>
	<tr>
		<td>
			<s:form id="assignItemsmoForm" action="locationmoves.html?locationId=&type=&occupied=&warehouse=${workTicket.warehouse}" method="post">
				<s:hidden name="id" value="%{workTicket.id}"/> 
				<s:hidden name="id1"/>
				
				<s:hidden name="ticket" value="%{workTicket.ticket}" />
				
				<input type="submit" class="cssbutton1" name="forwardBtn2"  disabled value="Move" style="width:55px; height:25px" />	
			</s:form>
		</td>
	</tr>
</table>
<script type="text/javascript"> 
    highlightTableRows("storageList");
</script>