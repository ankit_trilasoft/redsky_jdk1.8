<%@ include file="/common/taglibs.jsp"%> 
 
<head>
<meta name="heading" content="<fmt:message key='dataCatalogForm.title'/>"/> 
<title><fmt:message key="dataCatalogForm.title"/></title> 
<style>
input[type="checkbox"]{
margin:0px;
}
</style>
</head>

<script language="javascript" type="text/javascript">

function imposeMaxLength(Object, MaxLen)
{
return (Object.value.length <= MaxLen);
}

function openCatalogList(){
	action('hi');
	document.forms['dataCatalogForm'].action='searchDataCatalog.html';
	document.forms['dataCatalogForm'].submit();
}
</script>

<script language="JavaScript">
	function check(){
	
	
	if(document.forms['dataCatalogForm'].elements['dataCatalog.tableName'].value=="")
	{
	if(document.forms['dataCatalogForm'].elements['dataCatalog.fieldName'].value=="")
	{
		alert("Field Name is the required field");
		
		return false;
		
	}
	
	else
		{
			alert("Table Name is the required field");
			return false;
		}
		
	}
	}
	
	function changeStatus(){
    document.forms['dataCatalogForm'].elements['formStatus'].value = '1';
}
	
	function autoSave(clickType){	
	if(!(clickType == 'save')){
	if ('${autoSavePrompt}' == 'No'){
		var id1 = document.forms['dataCatalogForm'].elements['dataCatalog.id'].value;
		if (document.forms['dataCatalogForm'].elements['formStatus'].value == '1'){
		document.forms['dataCatalogForm'].action = 'savedataCatalog!saveOnTabChange.html';
		document.forms['dataCatalogForm'].submit();
		}
		else
		{
			if(document.forms['dataCatalogForm'].elements['gotoPageString'].value == 'gototab.fieldConfigurationList'){
				location.href = 'catalogs.html';
				}
		}
	}
	else{
	var id1 = document.forms['dataCatalogForm'].elements['dataCatalog.id'].value;
	
	if (document.forms['dataCatalogForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='dataCatalogForm.title'/>");
		if(agree){
			document.forms['dataCatalogForm'].action = 'savedataCatalog!saveOnTabChange.html';
			document.forms['dataCatalogForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['dataCatalogForm'].elements['gotoPageString'].value == 'gototab.fieldConfigurationList'){
				location.href = 'catalogs.html';
				}
			
					
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['dataCatalogForm'].elements['gotoPageString'].value == 'gototab.fieldConfigurationList'){
				location.href = 'catalogs.html';
				}
			
			
			
	}
	}
}
}
}

</script>
<s:form id="dataCatalogForm" name="dataCatalogForm" action="savedataCatalog" method="post" validate="true">
       <s:hidden name="tableName" value="<%=request.getParameter("tableName") %>"/>
       <s:hidden name="fieldName" value="<%=request.getParameter("fieldName") %>"/>
       <s:hidden name="description" value="<%=request.getParameter("description") %>"/>
       <s:hidden name="auditable" value="<%=request.getParameter("auditable") %>"/>
       <s:hidden name="defineByToDoRule" value="<%=request.getParameter("defineByToDoRule") %>"/>
       <s:hidden name="isdateField" value="<%=request.getParameter("isdateField") %>"/>
       <s:hidden name="visible" value="<%=request.getParameter("visible") %>"/>
       <s:hidden name="charge" value="<%=request.getParameter("charge") %>"/>
		<s:hidden name="dataCatalog.rulesFiledName"/>
	
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value=""/>
	<c:if test="${validateFormNav == 'OK'}" >
	<c:choose>
	<c:when test="${gotoPageString == 'gototab.fieldConfigurationList' }">
	    <c:redirect url="/catalogs.html"/>
	</c:when>

	<c:otherwise>
	</c:otherwise>
	</c:choose>
	</c:if>
<div id="Layer1" style="width:100%" onkeydown="changeStatus();"> 

		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Field Configuration<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <!--<li><a onclick="setReturnString('gototab.fieldConfigurationList');return autoSave();"><span>Field Configuration List</span></a></li>-->
		    <li><a onclick="openCatalogList();"><span>Field Configuration List</span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div><br>
<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top" style="margin-top: -10px;"><span></span></div>
    <div class="center-content">

  	<table class="detailTabLabel" border="0" width="750px">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" class="listwhitetext"><fmt:message key="dataCatalog.tableName"/><font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="dataCatalog.tableName"  maxlength="50" size="18" cssClass="input-text" readonly="false"/></td>
		  		<td align="right" class="listwhitetext"><fmt:message key="dataCatalog.fieldName"/><font color="red" size="2">*</font></td>
		  		<td align="left" ><s:textfield name="dataCatalog.fieldName"  maxlength="50" size="29" cssClass="input-text" readonly="false"/></td>		  		
		  		<td align="right" class="listwhitetext"><fmt:message key="dataCatalog.charge"/></td>
		  		<td align="left"><s:select cssClass="list-menu"   name="dataCatalog.charge" list="{'Quantity','Price'}" headerKey="" headerValue="" cssStyle="width:119px" onchange="changeStatus();"/></td> 		  		
		  	</tr>
		  	<tr><td colspan="6"></td></tr>
		  	<tr>
		  		<td align="right" class="listwhitetext">Field Type</td>
		  		<td align="left"><s:select cssClass="list-menu"   name="dataCatalog.usDomestic" list="%{usDomesticList}" headerKey="" headerValue="" cssStyle="width:119px" onchange="changeStatus();"/></td> 
		  		<td align="right" class="listwhitetext">Reference Table</td>
		  		<td align="left"><s:textfield name="dataCatalog.referenceTable"  maxlength="40" size="29" cssClass="input-text" readonly="false"/></td>
		  		<td align="right" class="listwhitetext">Parameter Linking Code</td>
		  		<td align="left"><s:textfield name="dataCatalog.parameterLinkingCode"  maxlength="40" size="18" cssClass="input-text" readonly="false"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" ></td>
		  		<td colspan="6">
		  		<table class="detailTabLabel" style="width:58%;!width:54%;">
		  		<tr>
		  		<td height="3px" align="left"></td>
		  		</tr>
		  		<tr>
		  	    <td align="left"><s:checkbox key="dataCatalog.auditable" fieldValue="true"/></td>
		  		<td align="left"  class="listwhitetext"><fmt:message key="dataCatalog.auditable"/></td>
		  	    <td align="left"><s:checkbox key="dataCatalog.defineByToDoRule" fieldValue="true"/></td>
		  		<td align="left" class="listwhitetext"><fmt:message key="dataCatalog.defineByToDoRule"/></td>
		  		<td align="left" valign="bottom"><s:checkbox key="dataCatalog.isdateField" fieldValue="true"/></td>
		  		<td align="left" class="listwhitetext"><fmt:message key="dataCatalog.isdateField"/></td>	
		  		<td align="left" valign="bottom"><s:checkbox key="dataCatalog.visible" fieldValue="true"/></td>
		  		<td align="left" class="listwhitetext"><fmt:message key="dataCatalog.configerable"/></td>

		  		</tr>
		  		</table>
		  		</td>
		  	</tr>	
				  <tr>
				  <td align="right" class="listwhitetext"><fmt:message key="dataCatalog.description"/></td>				  
		  			<td align="left" colspan="10"><s:textarea name="dataCatalog.description"  onkeypress="return imposeMaxLength(this,1000);" rows="3" cols="67" readonly="false" cssClass="textarea"/></td>
		  			</tr>
				<tr>
		  		<td align="left" height="20"></td>
		  		</tr>
		  	</tbody>
	</table>
				  </div>
<div class="bottom-header"><span></span></div>
</div>
</div> 	  
		 
	
 		  <table class="detailTabLabel" border="0" style="width:700px">
				<tbody>
					<tr>
					   <td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='auditSetup.createdOn'/></b></td>
							<s:hidden name="dataCatalog.createdOn" />
							<td style="width:160px"><s:date name="dataCatalog.createdOn" format="dd-MMM-yyyy HH:mm"/></td>		
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='auditSetup.createdBy' /></b></td>
							<c:if test="${not empty dataCatalog.id}">
								<s:hidden name="dataCatalog.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{dataCatalog.createdBy}"/></td>
							</c:if>
							<c:if test="${empty dataCatalog.id}">
								<s:hidden name="dataCatalogcreatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width85px"><b><fmt:message key='auditSetup.updatedOn'/></b></td>
							<s:hidden name="dataCatalog.updatedOn"/>
							<td style="width:160px"><s:date name="dataCatalog.updatedOn" format="dd-MMM-yyyy HH:mm"/></td>
							<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='auditSetup.updatedBy' /></b></td>
							<c:if test="${not empty dataCatalog.id}">
								<s:hidden name="dataCatalog.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{dataCatalog.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty dataCatalog.id}">
								<s:hidden name="dataCatalog.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:85px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table>
		  
		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save"/>  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		<td align="right">
        		<c:if test="${not empty dataCatalog.id}">
		       <input type="button" class="cssbutton1" value="Add New Record" style="width:120px;" onclick="location.href='<c:url value="/dataCatalogForm.html"/>'" />   
	            </c:if>
	            </td>
       	  	</tr>		  	
		  </tbody>
		  </table>
</div>
<s:hidden name="dataCatalog.id"/>

</s:form>