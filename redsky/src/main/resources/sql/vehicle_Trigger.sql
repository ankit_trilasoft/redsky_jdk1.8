DELIMITER $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_vehicle` $$
create trigger redsky.trigger_add_history_vehicle BEFORE UPDATE on redsky.vehicle
for each row BEGIN
	DECLARE fieldNameList LONGTEXT;
	DECLARE oldValueList LONGTEXT;
	DECLARE newValueList LONGTEXT;

	SET fieldNameList = " ";
	SET oldValueList = " ";
	SET newValueList = " ";
	
	IF ((OLD.length <> NEW.length) or (OLD.length is null and NEW.length is not null) 
	  or (OLD.length is not null and NEW.length is null)) THEN
		select CONCAT(fieldNameList,'vehicle.length~') into fieldNameList;
		IF(OLD.length is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.length is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.length is not null) then
			select CONCAT(oldValueList,OLD.length,'~') into oldValueList;
		END IF;
		IF(NEW.length is not null) then
			select CONCAT(newValueList,NEW.length,'~') into newValueList;
		END IF; 
	END IF;
	IF (OLD.year <> NEW.year ) THEN
	    select CONCAT(fieldNameList,'vehicle.year~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.year,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.year,'~') into newValueList;
	END IF;
	IF (OLD.container <> NEW.container ) THEN
	    select CONCAT(fieldNameList,'vehicle.container~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.container,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.container,'~') into newValueList;
	END IF;
	IF (OLD.model <> NEW.model ) THEN
	    select CONCAT(fieldNameList,'vehicle.model~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.model,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.model,'~') into newValueList;
	END IF;
	IF (OLD.auto <> NEW.auto ) THEN
	    select CONCAT(fieldNameList,'vehicle.auto~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.auto,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.auto,'~') into newValueList;
	END IF;
	IF (OLD.cntnrNumber <> NEW.cntnrNumber ) THEN
	    select CONCAT(fieldNameList,'vehicle.cntnrNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.cntnrNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.cntnrNumber,'~') into newValueList;
	END IF;
	IF (OLD.color <> NEW.color ) THEN
	    select CONCAT(fieldNameList,'vehicle.color~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.color,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.color,'~') into newValueList;
	END IF;
	IF (OLD.cylinders <> NEW.cylinders ) THEN
	    select CONCAT(fieldNameList,'vehicle.cylinders~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.cylinders,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.cylinders,'~') into newValueList;
	END IF;
	IF (OLD.doors <> NEW.doors ) THEN
	    select CONCAT(fieldNameList,'vehicle.doors~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.doors,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.doors,'~') into newValueList;
	END IF;
	IF (OLD.inventory <> NEW.inventory ) THEN
	    select CONCAT(fieldNameList,'vehicle.inventory~') into fieldNameList;
	    IF(OLD.inventory is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
	IF(NEW.inventory is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;
    	IF(OLD.inventory is not null) then
        	select CONCAT(oldValueList,OLD.inventory,'~') into oldValueList;
     	END IF;
      	IF(NEW.inventory is not null) then
        	select CONCAT(newValueList,NEW.inventory,'~') into newValueList;
      	END IF;
	END IF;

	IF (OLD.licNumber <> NEW.licNumber ) THEN
	    select CONCAT(fieldNameList,'vehicle.licNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.licNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.licNumber,'~') into newValueList;
	END IF;
	IF (OLD.make <> NEW.make ) THEN
	    select CONCAT(fieldNameList,'vehicle.make~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.make,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.make,'~') into newValueList;
	END IF;
	IF (OLD.proNumber <> NEW.proNumber ) THEN
	    select CONCAT(fieldNameList,'vehicle.proNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.proNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.proNumber,'~') into newValueList;
	END IF;
	IF (OLD.serial <> NEW.serial ) THEN
	    select CONCAT(fieldNameList,'vehicle.serial~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.serial,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.serial,'~') into newValueList;
	END IF;
	IF (OLD.title <> NEW.title ) THEN
	    select CONCAT(fieldNameList,'vehicle.title~') into fieldNameList;
	    IF(OLD.title is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.title is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;
    	IF(OLD.title is not null) then
        	select CONCAT(oldValueList,OLD.title,'~') into oldValueList;
     	END IF;
      	IF(NEW.title is not null) then
        	select CONCAT(newValueList,NEW.title,'~') into newValueList;
      	END IF;
	END IF;
	IF (OLD.titleNumber <> NEW.titleNumber ) THEN
	    select CONCAT(fieldNameList,'vehicle.titleNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.titleNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.titleNumber,'~') into newValueList;
	END IF;
	IF ((OLD.volume <> NEW.volume) or (OLD.volume is null and NEW.volume is not null) 
	  or (OLD.volume is not null and NEW.volume is null)) THEN
		select CONCAT(fieldNameList,'vehicle.volume~') into fieldNameList;
		IF(OLD.volume is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.volume is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.volume is not null) then
			select CONCAT(oldValueList,OLD.volume,'~') into oldValueList;
		END IF;
		IF(NEW.volume is not null) then
			select CONCAT(newValueList,NEW.volume,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.weight <> NEW.weight) or (OLD.weight is null and NEW.weight is not null) 
	  or (OLD.weight is not null and NEW.weight is null)) THEN
		select CONCAT(fieldNameList,'vehicle.weight~') into fieldNameList;
		IF(OLD.weight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.weight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.weight is not null) then
			select CONCAT(oldValueList,OLD.weight,'~') into oldValueList;
		END IF;
		IF(NEW.weight is not null) then
			select CONCAT(newValueList,NEW.weight,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.width <> NEW.width) or (OLD.width is null and NEW.width is not null) 
	  or (OLD.width is not null and NEW.width is null)) THEN
		select CONCAT(fieldNameList,'vehicle.width~') into fieldNameList;
		IF(OLD.width is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.width is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.width is not null) then
			select CONCAT(oldValueList,OLD.width,'~') into oldValueList;
		END IF;
		IF(NEW.width is not null) then
			select CONCAT(newValueList,NEW.width,'~') into newValueList;
		END IF; 
	END IF;
	IF (OLD.unit1 <> NEW.unit1 ) THEN
	    select CONCAT(fieldNameList,'vehicle.unit1~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.unit1,'~') into newValueList;
	END IF;
	IF (OLD.unit2 <> NEW.unit2 ) THEN
	    select CONCAT(fieldNameList,'vehicle.unit2~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.unit2,'~') into newValueList;
	END IF;
	IF (OLD.totalLine <> NEW.totalLine ) THEN
	    select CONCAT(fieldNameList,'vehicle.totalLine~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.totalLine,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.totalLine,'~') into newValueList;
	END IF;
	IF (OLD.unit3 <> NEW.unit3 ) THEN
	    select CONCAT(fieldNameList,'vehicle.unit3~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.unit3,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.unit3,'~') into newValueList;
	END IF;
	IF (OLD.idNumber <> NEW.idNumber ) THEN
	    select CONCAT(fieldNameList,'vehicle.idNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.idNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.idNumber,'~') into newValueList;
	END IF;
	IF (OLD.height <> NEW.height ) THEN
	    select CONCAT(fieldNameList,'vehicle.height~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.height,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.height,'~') into newValueList;
	END IF;
	IF (OLD.classEPA <> NEW.classEPA ) THEN
	    select CONCAT(fieldNameList,'vehicle.classEPA~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.classEPA,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.classEPA,'~') into newValueList;
	END IF;
	IF (OLD.ugwIntId <> NEW.ugwIntId ) THEN
	    select CONCAT(fieldNameList,'vehicle.ugwIntId~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.ugwIntId,'~') into newValueList;
	END IF;
	IF (OLD.vehicleType <> NEW.vehicleType ) THEN
	    select CONCAT(fieldNameList,'vehicle.vehicleType~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.vehicleType,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.vehicleType,'~') into newValueList;
	END IF;
        IF (OLD.valuation <> NEW.valuation ) THEN
            select CONCAT(fieldNameList,'vehicle.valuation~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.valuation,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.valuation,'~') into newValueList;
	END IF;
	
	IF (OLD.engineNumber <> NEW.engineNumber ) THEN
            select CONCAT(fieldNameList,'vehicle.engineNumber~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.engineNumber,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.engineNumber,'~') into newValueList;
	END IF;
	IF (OLD.valuationCurrency <> NEW.valuationCurrency ) THEN
            select CONCAT(fieldNameList,'vehicle.valuationCurrency~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.valuationCurrency,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.valuationCurrency,'~') into newValueList;
	END IF;
	IF (OLD.type <> NEW.type ) THEN
            select CONCAT(fieldNameList,'vehicle.type~') into fieldNameList;
	    select CONCAT(oldValueList,OLD.type,'~') into oldValueList;
	    select CONCAT(newValueList,NEW.type,'~') into newValueList;
	END IF;
	
	IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'vehicle.status~') into fieldNameList;
      IF(OLD.status = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.status = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(OLD.status is null) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(NEW.status = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.status = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
       IF(NEW.status is null) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
  END IF;
  
update serviceorder set isSOExtract=false,updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.serviceorderid;	
CALL add_tblHistory (OLD.id,"vehicle", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END
$$