package com.trilasoft.app.dao.hibernate.dto;

public class HibernateDTO {

private Object shipNumber;
private Object bookingAgentCode;
private Object job;
private Object routing;
private Object mode;
private Object packingMode;
private Object commodity;
private Object firstName;
private Object lastName;
private Object coordinator;
private Object salesMan;
private Object originCity;
private Object originCountryCode;
private Object destinationCity;
private Object destinationCountryCode;
private Object serviceType;
private Object beginLoad;
private Object deliveryA;
private Object sitOriginA;
private Object sitDestinationA;
private Object originAgent;
private Object destinationAgent;
private Object contract;
private Object billToCode;
private Object billCompleteA;
private Object  Expires;
private Object vendorCode;
private Object bookingAgentName;
private Object comptetive;
private Object actualNetWeight;
private Object actualGrossWeight;
private Object netWeight;
private Object diffActualRevenue;
private Object diffActualExpense;
private Object actualRevenue;
private Object estimatedTotalRevenue;
private Object revisedTotalRevenue;
private Object actualGrossMargin;
private Object actualGrossMarginPercentage;
private Object categoryType;
//for active shipments
private Object beginPacking;
private Object packA;
private Object activate;
private Object createdon;
private Object status;
private Object serviceOrderETA;
private Object serviceOrderATA;
/* Added By kunal for ticket number 6406 */
private Object servicePartnerCarrierDeparture;
private Object servicePartnerBookNumber;
private Object servicePartnerBlNumber;
private Object servicePartnerAesNumber;
private Object servicePartnerCarrierVessels;
/* Addition complete */
private Object density;
private Object originAgentCode;
private Object destinationAgentCode;
private Object actualCubicFeet;
private Object deliveryShipper;
//for dynamic financial
private Object topier;
private Object actualWeight;
private Object chargeCode;
private Object note;
private Object recGl;
private Object aBillToCode;
private Object AactualRevenue;
private Object description;
private Object billTo1Point ;//PAYEE_TYP
private Object aActualExpense;//TOTAL_INV
private Object estimateVendorName; //Fin Amts detail Report Extract_IAGENTS_NAME
private Object companyDivision;
private Object revisedDynamicRevenue;
private Object estimatedDynamicRevenue;
private Object revisedDynamicExpense;
private Object estimatedDynamicExpense;
private Object registrationNumber;
private Object beginLoadTarget;
//for Domestic extract
private Object originState;
private Object destinationState;
private Object haulingAgentCode;
private Object haulerName;
private Object driverId;
private Object driverName;
private Object revenueRecognition;
private Object DYN_REV;
private Object DYN_EXP;
private Object billToAuthority;
private Object billToReference;
private Object insuranceValueEntitle;
private Object insuranceValueActual;
private Object certnum;
private Object billToName;
private Object sendActualToClient;
private Object DynamicRevenue;
private Object DynamicExpense; 
private Object requestGBL; 
private Object recivedGBL; 
private Object actualRevenueSum; 
private Object actualExpenseSum; 
private Object notesSubject; 
private Object accountLineNumber;
private Object recInvoiceNumber;
private Object receivedInvoiceDate;
private Object invoiceNumber;
private Object invoiceDate;
private Object serviceOrderETD;
private Object serviceOrderATD;
private Object servicePartnerCarrierArrival;
private Object recPostDate;
private Object payPostDate;
private Object sumDistributionAmount;
private Object distributionAmount;
private Object estimateGrossWeight;
private Object estimatedNetWeight;
private Object netEstWeight;
private Object leftWHOn;
private Object billGroup;
private Object abbreviation;
private Object millitaryShipment;
private Object dp3;
private Object salesStatus;
private Object gblNumber; 
private Object instructionsToForwarder; 
private Object preliminaryNotification;
private Object finalNotification;
private Object source;
private Object personBilling;
private Object  billingId;
private Object  billComplete;
private Object  auditComplete;

private Object  emailSent;
private Object clientOverallResponse;

private Object  socialSecurityNumber;
private Object year;
private Object oaeValuationP;
private Object oaeValuationN;
private Object oaeValuation;
private Object feedback;
private Object oaCount;
private Object comments;
private Object corpID;
private Object quoteStatus;
private Object sid;
private Object accrualReadyDate;
private Object minimumMargin;
private Object accrueRevenue;
private Object accrueProfit;
private Object marginPercentage;

public Object getYear() {
	return year;
}
public void setYear(Object year) {
	this.year = year;
}

public Object getOaeValuationN() {
	return oaeValuationN;
}
public void setOaeValuationN(Object oaeValuationN) {
	this.oaeValuationN = oaeValuationN;
}

public Object getOaeValuationP() {
	return oaeValuationP;
}
public void setOaeValuationP(Object oaeValuationP) {
	this.oaeValuationP = oaeValuationP;
}

public Object getBillingId() {
	return billingId;
}
public Object getClientOverallResponse() {
	return clientOverallResponse;
}
public void setClientOverallResponse(Object clientOverallResponse) {
	this.clientOverallResponse = clientOverallResponse;
}
public Object getEmailSent() {
	return emailSent;
}
public void setEmailSent(Object emailSent) {
	this.emailSent = emailSent;
}
public void setBillingId(Object billingId) {
	this.billingId = billingId;
}
public Object getSource() {
	return source;
}
public void setSource(Object source) {
	this.source = source;
}
public Object getActualGrossMargin() {
	return actualGrossMargin;
}
public void setActualGrossMargin(Object actualGrossMargin) {
	this.actualGrossMargin = actualGrossMargin;
}
public Object getActualGrossMarginPercentage() {
	return actualGrossMarginPercentage;
}
public void setActualGrossMarginPercentage(Object actualGrossMarginPercentage) {
	this.actualGrossMarginPercentage = actualGrossMarginPercentage;
}
public Object getActualGrossWeight() {
	return actualGrossWeight;
}
public void setActualGrossWeight(Object actualGrossWeight) {
	this.actualGrossWeight = actualGrossWeight;
}
public Object getActualNetWeight() {
	return actualNetWeight;
}
public void setActualNetWeight(Object actualNetWeight) {
	this.actualNetWeight = actualNetWeight;
}
public Object getActualRevenue() {
	return actualRevenue;
}
public void setActualRevenue(Object actualRevenue) {
	this.actualRevenue = actualRevenue;
}
public Object getBeginLoad() {
	return beginLoad;
}
public void setBeginLoad(Object beginLoad) {
	this.beginLoad = beginLoad;
}
public Object getBillCompleteA() {
	return billCompleteA;
}
public void setBillCompleteA(Object billCompleteA) {
	this.billCompleteA = billCompleteA;
}
public Object getBillToCode() {
	return billToCode;
}
public void setBillToCode(Object billToCode) {
	this.billToCode = billToCode;
}
public Object getBookingAgentCode() {
	return bookingAgentCode;
}
public void setBookingAgentCode(Object bookingAgentCode) {
	this.bookingAgentCode = bookingAgentCode;
}
public Object getBookingAgentName() {
	return bookingAgentName;
}
public void setBookingAgentName(Object bookingAgentName) {
	this.bookingAgentName = bookingAgentName;
}
public Object getCommodity() {
	return commodity;
}
public void setCommodity(Object commodity) {
	this.commodity = commodity;
}
public Object getComptetive() {
	return comptetive;
}
public void setComptetive(Object comptetive) {
	this.comptetive = comptetive;
}
public Object getContract() {
	return contract;
}
public void setContract(Object contract) {
	this.contract = contract;
}
public Object getCoordinator() {
	return coordinator;
}
public void setCoordinator(Object coordinator) {
	this.coordinator = coordinator;
}
public Object getDeliveryA() {
	return deliveryA;
}
public void setDeliveryA(Object deliveryA) {
	this.deliveryA = deliveryA;
}
public Object getDestinationAgent() {
	return destinationAgent;
}
public void setDestinationAgent(Object destinationAgent) {
	this.destinationAgent = destinationAgent;
}
public Object getDestinationCity() {
	return destinationCity;
}
public void setDestinationCity(Object destinationCity) {
	this.destinationCity = destinationCity;
}
public Object getDestinationCountryCode() {
	return destinationCountryCode;
}
public void setDestinationCountryCode(Object destinationCountryCode) {
	this.destinationCountryCode = destinationCountryCode;
}
public Object getDiffActualExpense() {
	return diffActualExpense;
}
public void setDiffActualExpense(Object diffActualExpense) {
	this.diffActualExpense = diffActualExpense;
}
public Object getDiffActualRevenue() {
	return diffActualRevenue;
}
public void setDiffActualRevenue(Object diffActualRevenue) {
	this.diffActualRevenue = diffActualRevenue;
}
public Object getEstimatedTotalRevenue() {
	return estimatedTotalRevenue;
}
public void setEstimatedTotalRevenue(Object estimatedTotalRevenue) {
	this.estimatedTotalRevenue = estimatedTotalRevenue;
}
public Object getExpires() {
	return Expires;
}
public void setExpires(Object expires) {
	Expires = expires;
}
public Object getFirstName() {
	return firstName;
}
public void setFirstName(Object firstName) {
	this.firstName = firstName;
}
public Object getJob() {
	return job;
}
public void setJob(Object job) {
	this.job = job;
}
public Object getLastName() {
	return lastName;
}
public void setLastName(Object lastName) {
	this.lastName = lastName;
}
public Object getMode() {
	return mode;
}
public void setMode(Object mode) {
	this.mode = mode;
}
public Object getNetWeight() {
	return netWeight;
}
public void setNetWeight(Object netWeight) {
	this.netWeight = netWeight;
}
public Object getOriginAgent() {
	return originAgent;
}
public void setOriginAgent(Object originAgent) {
	this.originAgent = originAgent;
}
public Object getOriginCity() {
	return originCity;
}
public void setOriginCity(Object originCity) {
	this.originCity = originCity;
}
public Object getOriginCountryCode() {
	return originCountryCode;
}
public void setOriginCountryCode(Object originCountryCode) {
	this.originCountryCode = originCountryCode;
}
public Object getPackingMode() {
	return packingMode;
}
public void setPackingMode(Object packingMode) {
	this.packingMode = packingMode;
}
public Object getRevisedTotalRevenue() {
	return revisedTotalRevenue;
}
public void setRevisedTotalRevenue(Object revisedTotalRevenue) {
	this.revisedTotalRevenue = revisedTotalRevenue;
}
public Object getRouting() {
	return routing;
}
public void setRouting(Object routing) {
	this.routing = routing;
}
public Object getSalesMan() {
	return salesMan;
}
public void setSalesMan(Object salesMan) {
	this.salesMan = salesMan;
}
public Object getServiceType() {
	return serviceType;
}
public void setServiceType(Object serviceType) {
	this.serviceType = serviceType;
}
public Object getShipNumber() {
	return shipNumber;
}
public void setShipNumber(Object shipNumber) {
	this.shipNumber = shipNumber;
}
public Object getSitDestinationA() {
	return sitDestinationA;
}
public void setSitDestinationA(Object sitDestinationA) {
	this.sitDestinationA = sitDestinationA;
}
public Object getSitOriginA() {
	return sitOriginA;
}
public void setSitOriginA(Object sitOriginA) {
	this.sitOriginA = sitOriginA;
}
public Object getVendorCode() {
	return vendorCode;
}
public void setVendorCode(Object vendorCode) {
	this.vendorCode = vendorCode;
}
public Object getCategoryType() {
	return categoryType;
}
public void setCategoryType(Object categoryType) {
	this.categoryType = categoryType;
}
public Object getActivate() {
	return activate;
}
public void setActivate(Object activate) {
	this.activate = activate;
}
public Object getBeginPacking() {
	return beginPacking;
}
public void setBeginPacking(Object beginPacking) {
	this.beginPacking = beginPacking;
}
public Object getCreatedon() {
	return createdon;
}
public void setCreatedon(Object createdon) {
	this.createdon = createdon;
} 
public Object getPackA() {
	return packA;
}
public void setPackA(Object packA) {
	this.packA = packA;
}
public Object getStatus() {
	return status;
}
public void setStatus(Object status) {
	this.status = status;
}
public Object getDensity() {
	return density;
}
public void setDensity(Object density) {
	this.density = density;
}
public Object getAactualRevenue() {
	return AactualRevenue;
}
public void setAactualRevenue(Object aactualRevenue) {
	AactualRevenue = aactualRevenue;
}
public Object getABillToCode() {
	return aBillToCode;
}
public void setABillToCode(Object billToCode) {
	aBillToCode = billToCode;
}
public Object getActualWeight() {
	return actualWeight;
}
public void setActualWeight(Object actualWeight) {
	this.actualWeight = actualWeight;
}
public Object getChargeCode() {
	return chargeCode;
}
public void setChargeCode(Object chargeCode) {
	this.chargeCode = chargeCode;
}
public Object getNote() {
	return note;
}
public void setNote(Object note) {
	this.note = note;
}
public Object getRecGl() {
	return recGl;
}
public void setRecGl(Object recGl) {
	this.recGl = recGl;
}
public Object getTopier() {
	return topier;
}
public void setTopier(Object topier) {
	this.topier = topier;
}
public Object getDescription() {
	return description;
}
public void setDescription(Object description) {
	this.description = description;
}
public Object getBillTo1Point() {
	return billTo1Point;
}
public void setBillTo1Point(Object billTo1Point) {
	this.billTo1Point = billTo1Point;
}
public Object getAActualExpense() {
	return aActualExpense;
}
public void setAActualExpense(Object actualExpense) {
	aActualExpense = actualExpense;
}
public Object getDestinationAgentCode() {
	return destinationAgentCode;
}
public void setDestinationAgentCode(Object destinationAgentCode) {
	this.destinationAgentCode = destinationAgentCode;
}
public Object getOriginAgentCode() {
	return originAgentCode;
}
public void setOriginAgentCode(Object originAgentCode) {
	this.originAgentCode = originAgentCode;
}
public Object getActualCubicFeet() {
	return actualCubicFeet;
}
public void setActualCubicFeet(Object actualCubicFeet) {
	this.actualCubicFeet = actualCubicFeet;
}
public Object getDeliveryShipper() {
	return deliveryShipper;
}
public void setDeliveryShipper(Object deliveryShipper) {
	this.deliveryShipper = deliveryShipper;
}
public Object getEstimateVendorName() {
	return estimateVendorName;
}
public void setEstimateVendorName(Object estimateVendorName) {
	this.estimateVendorName = estimateVendorName;
}
public Object getCompanyDivision() {
	return companyDivision;
}
public void setCompanyDivision(Object companyDivision) {
	this.companyDivision = companyDivision;
}
public Object getEstimatedDynamicRevenue() {
	return estimatedDynamicRevenue;
}
public void setEstimatedDynamicRevenue(Object estimatedDynamicRevenue) {
	this.estimatedDynamicRevenue = estimatedDynamicRevenue;
}
public Object getRevisedDynamicRevenue() {
	return revisedDynamicRevenue;
}
public void setRevisedDynamicRevenue(Object revisedDynamicRevenue) {
	this.revisedDynamicRevenue = revisedDynamicRevenue;
}
public Object getEstimatedDynamicExpense() {
	return estimatedDynamicExpense;
}
public void setEstimatedDynamicExpense(Object estimatedDynamicExpense) {
	this.estimatedDynamicExpense = estimatedDynamicExpense;
}
public Object getRevisedDynamicExpense() {
	return revisedDynamicExpense;
}
public void setRevisedDynamicExpense(Object revisedDynamicExpense) {
	this.revisedDynamicExpense = revisedDynamicExpense;
}
public Object getBeginLoadTarget() {
	return beginLoadTarget;
}
public void setBeginLoadTarget(Object beginLoadTarget) {
	this.beginLoadTarget = beginLoadTarget;
}
public Object getRegistrationNumber() {
	return registrationNumber;
}
public void setRegistrationNumber(Object registrationNumber) {
	this.registrationNumber = registrationNumber;
}
public Object getDestinationState() {
	return destinationState;
}
public void setDestinationState(Object destinationState) {
	this.destinationState = destinationState;
}
public Object getDriverId() {
	return driverId;
}
public void setDriverId(Object driverId) {
	this.driverId = driverId;
}
public Object getDriverName() {
	return driverName;
}
public void setDriverName(Object driverName) {
	this.driverName = driverName;
}
public Object getHaulerName() {
	return haulerName;
}
public void setHaulerName(Object haulerName) {
	this.haulerName = haulerName;
}
public Object getHaulingAgentCode() {
	return haulingAgentCode;
}
public void setHaulingAgentCode(Object haulingAgentCode) {
	this.haulingAgentCode = haulingAgentCode;
}
public Object getOriginState() {
	return originState;
}
public void setOriginState(Object originState) {
	this.originState = originState;
}
/**
 * @return the revenueRecognition
 */
public Object getRevenueRecognition() {
	return revenueRecognition;
}
/**
 * @param revenueRecognition the revenueRecognition to set
 */
public void setRevenueRecognition(Object revenueRecognition) {
	this.revenueRecognition = revenueRecognition;
}
/**
 * @return the dYN_EXP
 */
public Object getDYN_EXP() {
	return DYN_EXP;
}
/**
 * @param dyn_exp the dYN_EXP to set
 */
public void setDYN_EXP(Object dyn_exp) {
	DYN_EXP = dyn_exp;
}
/**
 * @return the dYN_REV
 */
public Object getDYN_REV() {
	return DYN_REV;
}
/**
 * @param dyn_rev the dYN_REV to set
 */
public void setDYN_REV(Object dyn_rev) {
	DYN_REV = dyn_rev;
}
public Object getBillToAuthority() {
	return billToAuthority;
}
public void setBillToAuthority(Object billToAuthority) {
	this.billToAuthority = billToAuthority;
}
public Object getBillToReference() {
	return billToReference;
}
public void setBillToReference(Object billToReference) {
	this.billToReference = billToReference;
}
public Object getCertnum() {
	return certnum;
}
public void setCertnum(Object certnum) {
	this.certnum = certnum;
}
public Object getInsuranceValueActual() {
	return insuranceValueActual;
}
public void setInsuranceValueActual(Object insuranceValueActual) {
	this.insuranceValueActual = insuranceValueActual;
}
public Object getInsuranceValueEntitle() {
	return insuranceValueEntitle;
}
public void setInsuranceValueEntitle(Object insuranceValueEntitle) {
	this.insuranceValueEntitle = insuranceValueEntitle;
}
public Object getBillToName() {
	return billToName;
}
public void setBillToName(Object billToName) {
	this.billToName = billToName;
}
public Object getSendActualToClient() {
	return sendActualToClient;
}
public void setSendActualToClient(Object sendActualToClient) {
	this.sendActualToClient = sendActualToClient;
}
public Object getDynamicExpense() {
	return DynamicExpense;
}
public void setDynamicExpense(Object dynamicExpense) {
	DynamicExpense = dynamicExpense;
}
public Object getDynamicRevenue() {
	return DynamicRevenue;
}
public void setDynamicRevenue(Object dynamicRevenue) {
	DynamicRevenue = dynamicRevenue;
}
public Object getRecivedGBL() {
	return recivedGBL;
}
public void setRecivedGBL(Object recivedGBL) {
	this.recivedGBL = recivedGBL;
}
public Object getRequestGBL() {
	return requestGBL;
}
public void setRequestGBL(Object requestGBL) {
	this.requestGBL = requestGBL;
}
public Object getActualExpenseSum() {
	return actualExpenseSum;
}
public void setActualExpenseSum(Object actualExpenseSum) {
	this.actualExpenseSum = actualExpenseSum;
}
public Object getActualRevenueSum() {
	return actualRevenueSum;
}
public void setActualRevenueSum(Object actualRevenueSum) {
	this.actualRevenueSum = actualRevenueSum;
}
public Object getNotesSubject() {
	return notesSubject;
}
public void setNotesSubject(Object notesSubject) {
	this.notesSubject = notesSubject;
}
public Object getAccountLineNumber() {
	return accountLineNumber;
}
public void setAccountLineNumber(Object accountLineNumber) {
	this.accountLineNumber = accountLineNumber;
}
public Object getInvoiceDate() {
	return invoiceDate;
}
public void setInvoiceDate(Object invoiceDate) {
	this.invoiceDate = invoiceDate;
}
public Object getInvoiceNumber() {
	return invoiceNumber;
}
public void setInvoiceNumber(Object invoiceNumber) {
	this.invoiceNumber = invoiceNumber;
}
public Object getReceivedInvoiceDate() {
	return receivedInvoiceDate;
}
public void setReceivedInvoiceDate(Object receivedInvoiceDate) {
	this.receivedInvoiceDate = receivedInvoiceDate;
}
public Object getRecInvoiceNumber() {
	return recInvoiceNumber;
}
public void setRecInvoiceNumber(Object recInvoiceNumber) {
	this.recInvoiceNumber = recInvoiceNumber;
}
public Object getServiceOrderETD() {
	return serviceOrderETD;
}
public void setServiceOrderETD(Object serviceOrderETD) {
	this.serviceOrderETD = serviceOrderETD;
}
public Object getServiceOrderATD() {
	return serviceOrderATD;
}
public void setServiceOrderATD(Object serviceOrderATD) {
	this.serviceOrderATD = serviceOrderATD;
}

public Object getServiceOrderETA() {
	return serviceOrderETA;
}
public void setServiceOrderETA(Object serviceOrderETA) {
	this.serviceOrderETA = serviceOrderETA;
}
public Object getServiceOrderATA() {
	return serviceOrderATA;
}
public void setServiceOrderATA(Object serviceOrderATA) {
	this.serviceOrderATA = serviceOrderATA;
}
public Object getServicePartnerCarrierDeparture() {
	return servicePartnerCarrierDeparture;
}
public void setServicePartnerCarrierDeparture(
		Object servicePartnerCarrierDeparture) {
	this.servicePartnerCarrierDeparture = servicePartnerCarrierDeparture;
}
public Object getServicePartnerbookNumber() {
	return servicePartnerBookNumber;
}
public void setServicePartnerbookNumber(Object servicePartnerbookNumber) {
	this.servicePartnerBookNumber = servicePartnerbookNumber;
}
public Object getServicePartnerblNumber() {
	return servicePartnerBlNumber;
}
public void setServicePartnerblNumber(Object servicePartnerblNumber) {
	this.servicePartnerBlNumber = servicePartnerblNumber;
}
public Object getServicePartneraesNumber() {
	return servicePartnerAesNumber;
}
public void setServicePartneraesNumber(Object servicePartneraesNumber) {
	this.servicePartnerAesNumber = servicePartneraesNumber;
}
public Object getServicePartnercarrierVessels() {
	return servicePartnerCarrierVessels;
}
public void setServicePartnercarrierVessels(Object servicePartnercarrierVessels) {
	this.servicePartnerCarrierVessels = servicePartnercarrierVessels;
}
public Object getRecPostDate() {
	return recPostDate;
}
public void setRecPostDate(Object recPostDate) {
	this.recPostDate = recPostDate;
}
public Object getPayPostDate() {
	return payPostDate;
}
public void setPayPostDate(Object payPostDate) {
	this.payPostDate = payPostDate;
}
public Object getSumDistributionAmount() {
	return sumDistributionAmount;
}
public void setSumDistributionAmount(Object sumDistributionAmount) {
	this.sumDistributionAmount = sumDistributionAmount;
}
public Object getDistributionAmount() {
	return distributionAmount;
}
public void setDistributionAmount(Object distributionAmount) {
	this.distributionAmount = distributionAmount;
}
public Object getEstimatedNetWeight() {
	return estimatedNetWeight;
}
public void setEstimatedNetWeight(Object estimatedNetWeight) {
	this.estimatedNetWeight = estimatedNetWeight;
}
public Object getEstimateGrossWeight() {
	return estimateGrossWeight;
}
public void setEstimateGrossWeight(Object estimateGrossWeight) {
	this.estimateGrossWeight = estimateGrossWeight;
}
public Object getNetEstWeight() {
	return netEstWeight;
}
public void setNetEstWeight(Object netEstWeight) {
	this.netEstWeight = netEstWeight;
}
public Object getLeftWHOn() {
	return leftWHOn;
}
public void setLeftWHOn(Object leftWHOn) {
	this.leftWHOn = leftWHOn;
}
public Object getAbbreviation() {
	return abbreviation;
}
public void setAbbreviation(Object abbreviation) {
	this.abbreviation = abbreviation;
}
public Object getBillGroup() {
	return billGroup;
}
public void setBillGroup(Object billGroup) {
	this.billGroup = billGroup;
}
public Object getDp3() {
	return dp3;
}
public void setDp3(Object dp3) {
	this.dp3 = dp3;
}
public Object getMillitaryShipment() {
	return millitaryShipment;
}
public void setMillitaryShipment(Object millitaryShipment) {
	this.millitaryShipment = millitaryShipment;
}
public Object getSalesStatus() {
	return salesStatus;
}
public void setSalesStatus(Object salesStatus) {
	this.salesStatus = salesStatus;
}
public Object getGblNumber() {
	return gblNumber;
}
public void setGblNumber(Object gblNumber) {
	this.gblNumber = gblNumber;
}
public Object getFinalNotification() {
	return finalNotification;
}
public void setFinalNotification(Object finalNotification) {
	this.finalNotification = finalNotification;
}
public Object getInstructionsToForwarder() {
	return instructionsToForwarder;
}
public void setInstructionsToForwarder(Object instructionsToForwarder) {
	this.instructionsToForwarder = instructionsToForwarder;
}
public Object getPreliminaryNotification() {
	return preliminaryNotification;
}
public void setPreliminaryNotification(Object preliminaryNotification) {
	this.preliminaryNotification = preliminaryNotification;
}
public Object getPersonBilling() {
	return personBilling;
}
public void setPersonBilling(Object personBilling) {
	this.personBilling = personBilling;
}
public Object getAuditComplete() {
	return auditComplete;
}
public void setAuditComplete(Object auditComplete) {
	this.auditComplete = auditComplete;
}
public Object getBillComplete() {
	return billComplete;
}
public void setBillComplete(Object billComplete) {
	this.billComplete = billComplete;
}
public Object getSocialSecurityNumber() {
	return socialSecurityNumber;
}
public void setSocialSecurityNumber(Object socialSecurityNumber) {
	this.socialSecurityNumber = socialSecurityNumber;
}
public Object getFeedback() {
	return feedback;
}
public void setFeedback(Object feedback) {
	this.feedback = feedback;
}
public Object getOaeValuation() {
	return oaeValuation;
}
public void setOaeValuation(Object oaeValuation) {
	this.oaeValuation = oaeValuation;
}
public Object getOaCount() {
	return oaCount;
}
public void setOaCount(Object oaCount) {
	this.oaCount = oaCount;
}
public Object getComments() {
	return comments;
}
public void setComments(Object comments) {
	this.comments = comments;
}
public Object getCorpID() {
	return corpID;
}
public void setCorpID(Object corpID) {
	this.corpID = corpID;
}
public Object getServicePartnerCarrierArrival() {
	return servicePartnerCarrierArrival;
}
public void setServicePartnerCarrierArrival(Object servicePartnerCarrierArrival) {
	this.servicePartnerCarrierArrival = servicePartnerCarrierArrival;
}
public Object getQuoteStatus() {
	return quoteStatus;
}
public void setQuoteStatus(Object quoteStatus) {
	this.quoteStatus = quoteStatus;
}
public Object getSid() {
	return sid;
}
public void setSid(Object sid) {
	this.sid = sid;
}
public Object getAccrualReadyDate() {
	return accrualReadyDate;
}
public void setAccrualReadyDate(Object accrualReadyDate) {
	this.accrualReadyDate = accrualReadyDate;
}
public Object getMinimumMargin() {
	return minimumMargin;
}
public void setMinimumMargin(Object minimumMargin) {
	this.minimumMargin = minimumMargin;
}
public Object getAccrueRevenue() {
	return accrueRevenue;
}
public void setAccrueRevenue(Object accrueRevenue) {
	this.accrueRevenue = accrueRevenue;
}
public Object getAccrueProfit() {
	return accrueProfit;
}
public void setAccrueProfit(Object accrueProfit) {
	this.accrueProfit = accrueProfit;
}
public Object getMarginPercentage() {
	return marginPercentage;
}
public void setMarginPercentage(Object marginPercentage) {
	this.marginPercentage = marginPercentage;
}


}
