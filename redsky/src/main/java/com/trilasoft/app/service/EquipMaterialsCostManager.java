package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.EquipMaterialsCost;


public interface EquipMaterialsCostManager extends GenericManager<EquipMaterialsCost, Long>{
	public EquipMaterialsCost getCostById(long id);
	public EquipMaterialsCost getByMaterailId(long id);

}
