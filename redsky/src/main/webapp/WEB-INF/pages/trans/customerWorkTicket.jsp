<%@ include file="/common/taglibs.jsp"%>
<head>
<title><fmt:message key="workTicketList.title" /></title>
<meta name="heading"
	content="<fmt:message key='workTicketList.heading'/>" />
<style type="text/css">
		h2 {background-color: #FBBFFF}
	</style>
	<script language="javascript" type="text/javascript"
	SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>

<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
</script>
<script language="javascript" type="text/javascript">  
function requestedWorkTickets(targetElement){
	// alert(targetElement.checked);
	var requestedWT = document.forms['form2'].elements['requestedCLT'].value;
	if(targetElement.checked){
		if(document.forms['form2'].elements['requestedCLT'].value == '' ){
			document.forms['form2'].elements['requestedCLT'].value = targetElement.value;
		}else{
			if(requestedWT.indexOf(targetElement.value)>=0){}
			else{
				requestedWT = requestedWT + ',' +targetElement.value;
			}
			document.forms['form2'].elements['requestedCLT'].value = requestedWT.replace(',,',",");
		}
	}else{
		if(requestedWT.indexOf(targetElement.value)>=0){
			requestedWT = requestedWT.replace(targetElement.value,"");
		}
		document.forms['form2'].elements['requestedCLT'].value = requestedWT.replace(',,',",");
	}
	
	if(document.forms['form2'].elements['requestedCLT'].value == ',' ){
			document.forms['form2'].elements['requestedCLT'].value = '';
	}
	if((document.forms['form2'].elements['requestedCLT'].value).indexOf(",") == 0){
			document.forms['form2'].elements['requestedCLT'].value = (document.forms['form2'].elements['requestedCLT'].value).substring(1);
	}
	if(document.forms['form2'].elements['requestedCLT'].value.lastIndexOf(",") == document.forms['form2'].elements['requestedCLT'].value.length -1){
			document.forms['form2'].elements['requestedCLT'].value = (document.forms['form2'].elements['requestedCLT'].value).substring(0,document.forms['form2'].elements['requestedCLT'].value.length-1)
	}
	
	if(document.forms['form2'].elements['requestedCLT'].value == ''){
		document.forms['form2'].elements['assignTicketsToLoss'].disabled = true;
	}else{
		document.forms['form2'].elements['assignTicketsToLoss'].disabled = false;
	}
}
 </script>
<SCRIPT LANGUAGE="JavaScript">
	var inputCount = 0;
      function addRow(id,ids){
      //alert(ids);
        	var tmpString = eval("String(document.workTicketList.CLT_"+ids+".value)"); 
			var tmp = eval("String(document.workTicketList.CLT_"+ids+".checked)"); 
      
			if(tmp.length==4)
			{
            inputCount++;
			var tbody = window.opener.document.getElementById(id).getElementsByTagName("TBODY")[0];
            var row = window.opener.document.createElement("TR");
            var td1 = window.opener.document.createElement("TD");
            var td2 = window.opener.document.createElement("TD");
            var td3 = window.opener.document.createElement("TD");
            var td4 = window.opener.document.createElement("TD");
            var td5 = window.opener.document.createElement("TD");
            var td6 = window.opener.document.createElement("TD");
            var aa = window.opener.document.createElement('INPUT');
           	aa.type = 'checkbox';
           	aa.setAttribute('name','requestedACLT');
           	aa.onclick = window.opener.requestedWorkTickets;
           	aa.value = ids;
           	td1.appendChild(aa) 
           /// td1.appendChild(window.opener.document.createTextNode("<input type=\"checkbox\" name=\"lossTicket\" Value="+ids+" onclick=\"requestedWorkTickets(this)\">"));
            var link = document.createElement('a');
          // link.setAttribute('href','editClaimLossTicket.html');
         // link.setAttribute('href','editClaimLossTicket.html?id=${claimLossTicketList.ticket}');
          //link.setAttribute('href','<a onclick="javascript:openWindow('editClaimLossTicket.html?id=${claimLossTicketList.id}&decorator=popup&popup=true',300,200);"> <c:out value="${claimLossTicketList.ticket}" /></a>');
                      var linkText=document.createTextNode(ids);
           link.appendChild(linkText);
                     // td2.appendChild(link); 
             td2.appendChild(window.opener.document.createTextNode(ids));
            
            td3.appendChild(window.opener.document.createTextNode(""));
           	td4.appendChild(window.opener.document.createTextNode(""));
          	td5.appendChild (window.opener.document.createTextNode("<%=request.getParameter("claimNum") %>"));
          	td6.appendChild (window.opener.document.createTextNode("<%=request.getParameter("lossNum") %>"));
            
            row.appendChild(td1);
           	row.appendChild(td2);
            row.appendChild(td3);
            row.appendChild(td4);
           	row.appendChild(td5);
            row.appendChild(td6);
            
            row.setAttribute("id",ids);
			tbody.setAttribute("id","OK");
            
            tbody.appendChild(row);
             //alert(window.opener.document.forms[ "lossForm" ].elements["requestedCLT"].value);
            var parentValue = window.opener.document.forms[ "lossForm" ].elements["requestedCLT"].value;
           	//alert(parentValue+"--"+ids);
            window.opener.document.forms[ "lossForm" ].elements["requestedCLT"].value = (parentValue +""+ ids+",");
            //alert("Work ticket ID: "+ids+" has been successfully added.");
            
            }
			else
			{
				 var d = window.opener.document.getElementById('OK');
				  var olddiv = window.opener.document.getElementById(ids);
				  d.removeChild(olddiv);
			}
			var olddiv = window.opener.document.getElementById('empty');
           if(olddiv != null)
           {
               var d1 = window.opener.document.getElementById('OK');
               //alert(window.opener.document.getElementById('empty'));
               d1.removeChild(olddiv);
           } 
            
      }
</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value=""/>
<s:hidden name="fileID"  id= "fileID" value=""/>
<s:form id="form2" action="assignTicketsToLoss" method="post" name="workTicketList">
	<s:hidden name="id" value="<%=request.getParameter("id") %>" />
	<s:hidden name="assignTickets" />
	<s:hidden name="requestedCLT" />
	<s:hidden name="claimNum" value="<%=request.getParameter("claimNum") %>"/>
	<s:hidden name="lossNum" value="<%=request.getParameter("lossNum") %>"/>
	<s:hidden name="decorator" value="<%=request.getParameter("decorator")%>"/>
	<s:hidden name="popup" value="<%=request.getParameter("popup")%>"/>
	<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	</c:if>

<div id="Layer1" style="width:100%;">
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Work Ticket List</span></a></li>
		  </ul>
		</div>
<div class="spnblk">&nbsp;</div>
<s:set name="workTickets" value="workTickets" scope="request" />
<display:table name="workTickets" class="table" requestURI="" export="${empty param.popup}" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'	id="customerWorkTicket" defaultsort="4" pagesize="10">
<display:column title="Select">
<s:checkbox name="CLT_${customerWorkTicket.ticket}" fieldValue="${customerWorkTicket.ticket}"  onclick="addRow('claimLossTicketList',${customerWorkTicket.ticket})"/></display:column>
<%--<c:if test="${param.popup}">  
<display:column property="ticket" sortable="true" titleKey="workTicket.ticket" url="/editWorkTicketUpdate.html?from=list&decorator=popup&popup=true" paramId="id" paramProperty="id"/>   
</c:if>--%> 
<c:if test="${param.popup}"> 
<display:column sortable="true" title="ticket" style="width:260px">
<a href="javascript:window.open('editWorkTicketUpdate.html?from=list&id=${customerWorkTicket.id}'),self.close();"><c:out value="${customerWorkTicket.ticket}" /></a>
</display:column> 
</c:if> 
<display:column property="shipNumber" sortable="true" titleKey="workTicket.shipNumber"  />
<display:column property="registrationNumber" sortable="true" titleKey="workTicket.registrationNumber" />
<display:column property="lastName" sortable="true" titleKey="workTicket.lastName" />
<display:column property="firstName" sortable="true" titleKey="workTicket.firstName" />
<display:column property="jobType" sortable="true" titleKey="workTicket.jobType" />
<display:column property="jobMode" sortable="true" titleKey="workTicket.jobMode" />
<display:column property="service" sortable="true" titleKey="workTicket.service" />
<display:column property="city" sortable="true" titleKey="workTicket.city" />
<display:column property="destinationCity" sortable="true" titleKey="workTicket.destinationCity" />
<display:column property="date1" sortable="true" titleKey="workTicket.date1" format="{0,date,dd-MMM-yyyy}"/>
<display:column property="date2" sortable="true" titleKey="workTicket.date2" format="{0,date,dd-MMM-yyyy}"/>
<display:column property="warehouse" sortable="true" titleKey="workTicket.warehouse" />				
<display:column property="targetActual" sortable="true" titleKey="workTicket.targetActual" />

</display:table>
</div>
<!--<input type="submit" name="assignTicketsToLoss" class="cssbutton" value="Assign Tickets To Losses" style="width:170px; height:25px" onclick="requestedWorkTickets()" />-->
	   <input type="button" name="closeWin" class="cssbutton" value="Close Window" style="width:100px; height:25px" onclick="window.close()" />
</s:form>
<script type="text/javascript"> 
	try{
	document.forms['form2'].elements['assignTicketsToLoss'].disabled = true;
   }
   catch(e){}
   highlightTableRows("customerWorkTicket");
</script>
