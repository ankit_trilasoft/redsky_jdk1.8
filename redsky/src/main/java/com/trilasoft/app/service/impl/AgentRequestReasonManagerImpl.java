package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AgentRequestDao;
import com.trilasoft.app.dao.AgentRequestReasonDao;
import com.trilasoft.app.model.AgentRequestReason;
import com.trilasoft.app.service.AgentBaseManager;
import com.trilasoft.app.service.AgentRequestManager;
import com.trilasoft.app.service.AgentRequestReasonManager;


public class AgentRequestReasonManagerImpl  extends GenericManagerImpl<AgentRequestReason, Long> implements AgentRequestReasonManager {
	    AgentRequestReasonDao agentRequestReasondao;
		public AgentRequestReasonManagerImpl(AgentRequestReasonDao agentRequestReasondao) { 
	        super(agentRequestReasondao); 
	        this.agentRequestReasondao = agentRequestReasondao; 
	    } 
		public Map<String, String> getCountryWithBranch(String parameter,String sessionCorpID){
			return agentRequestReasondao.getCountryWithBranch(parameter,sessionCorpID);
		}
		public List getPartnerPublicList(String corpID,String lastName,String aliasName, String countryCode, String country, String stateCode, String status, Boolean isAgt, Boolean isIgnoreInactive, String fidiNumber,String OMNINumber,String AMSANumber,String WERCNumber,String IAMNumber,String utsNumber ,String eurovanNetwork ,String PAIMA,String LACMA,String counter) 
			{
				return agentRequestReasondao.getPartnerPublicList( corpID, lastName,aliasName,countryCode, country,stateCode,status,isAgt, isIgnoreInactive,fidiNumber ,OMNINumber,AMSANumber,WERCNumber,IAMNumber,utsNumber,eurovanNetwork, PAIMA, LACMA,counter);
			}
		
		public List getAgentRequestDetailList(String sessionCorpID,String lastName,String aliasName,String billingCountry,String billingCity,String billingState,String status,String billingEmail, boolean isAgt,String billingZip,String billingAddress1)
		{
			
			
			return agentRequestReasondao.getAgentRequestDetailList(sessionCorpID,lastName,aliasName,billingCountry,billingCity,billingState,status,billingEmail,isAgt, billingZip,billingAddress1);
		}
		public String updateAgentRecord(String sessionCorpID,String lastName, String aliasName, String billingCountry,String billingCity,String billingState,String status,String billingEmail,boolean isAgt,String billingZip,String billingAddress1,String updatedBy,Long id,String createdBy)
		{
			return agentRequestReasondao.updateAgentRecord(sessionCorpID, lastName, aliasName, billingCountry, billingCity, billingState, status, billingEmail, isAgt, billingZip, billingAddress1, updatedBy, id,createdBy);
			
			
		}
		
		public String  updateAgentRejected(Long id,String updatedBy){
			return agentRequestReasondao.updateAgentRejected(id,updatedBy);
		}
		 public List checkById(Long id) {
			 return agentRequestReasondao.checkById(id);
		 
		 }
			public List getAgentRequestList(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String status,Boolean isAgt, Boolean isIgnoreInactive) {
			return agentRequestReasondao.getAgentRequestList(partnerType, corpID, lastName, aliasName, partnerCode, countryCode, country, stateCode, status, isAgt, isIgnoreInactive);
					
			}
			public String getUser(String createdBy)
			{
				return agentRequestReasondao.getUser(createdBy);
			}
			public AgentRequestReason getByID(Long id) {
				return agentRequestReasondao.getByID(id);
			}
			public List findAgentFeedbackList(Long id)
			{
				
				return agentRequestReasondao.findAgentFeedbackList(id);
			}
			
			}
		 
