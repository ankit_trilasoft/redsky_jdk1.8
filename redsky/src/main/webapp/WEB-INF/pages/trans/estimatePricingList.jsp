 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Estimate Pricing</title>   
    <meta name="heading" content="Estimate Pricing List"/> 
 
     
</head> 
<s:form id="serviceForm1" > 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="sid" id ="sid" value="%{serviceOrder.id}" />
<div id="Layer1" style="width:100%; ">
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<div id="newmnav" style="float: left;margin-bottom:0px;">
  <ul>
  <li>
  <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
  <a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a>
  </sec-auth:authComponent>
  </li>
  <sec-auth:authComponent componentId="module.tab.container.billingTab">
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  	<li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.accountingTab">
  <c:choose>
	<%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
	   <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
	</c:when> --%>
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose> 
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <c:choose> 
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.forwardingTab">
  		<c:if test="${forwardingTabVal!='Y'}"> 
			<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
		</c:if>
		<c:if test="${forwardingTabVal=='Y'}">
			<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
		</c:if>
   </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.domesticTab">
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
  <c:if test="${serviceOrder.job =='INT'}">
   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.statusTab">
  <c:if test="${serviceOrder.job =='RLO'}">
  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.ticketTab">
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  </sec-auth:authComponent>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
   <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
   </sec-auth:authComponent>
   </configByCorp:fieldVisibility>
   <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.reportTab">
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Container&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
       <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
 </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
    <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
</sec-auth:authComponent>
</div>
<div class="spn">&nbsp;</div>
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
<div id="newmnav">
		  <ul>
		  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Estimate Pricing</span></a></li>		  
		  <li><a onclick="openActiveList();"><span>Active List</span></a></li>
		  	    
  </ul>
		</div>

		<div class="spn">&nbsp;</div>
<div id="content" align="center" style="width:100%">
<div id="liquid-round-top">
    <div class="top" ><span></span></div>
    <div class="center-content" style="padding-left:25px;">
     <div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" >
<tr>
<td style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;text-align:center;font-size: 1em;font-weight: bold;background: #d7e9f5;">
<b><div align="right">Total Estimate Amount:&nbsp;$${miscellaneous.estimatedRevenue}</div></b>
</td>
</tr>
<tr><td height="4"></td></tr>
</table>
</div>
<div class="spn">&nbsp;</div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" width="530" >
<tr>
<td style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;background: #d7e9f5;">TRANSPORTATION</td>
</tr>
<tr>
<td>

<s:set name="underLyingHaulList" value="underLyingHaulList" scope="request"/>
<display:table name="underLyingHaulList" class="table" requestURI="" id="underLyingHaulList"  defaultsort="1" defaultorder="descending" pagesize="10">
     <display:column property="description"  title="Description" />
   <display:column title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${underLyingHaulList.rate}" /></div></display:column>
   <display:column  title="Discount">
   <c:if test="${underLyingHaulList.discount!=''}">
   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${underLyingHaulList.discount}" />%</div>
             </c:if>
             </display:column>
   <display:column title="Charge" style="width:10%;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${underLyingHaulList.charge}" /></div></display:column>
   <display:footer>
	<tr> 
 	  	 <td align="right" colspan="3"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${transportSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${transportSum}" /></div></c:if></td>             
      </tr>
	</display:footer>
	
	</display:table>
	</td>
	</tr>
	</table>
	
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
<thead>
<tr>
<th colspan="3" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;background: #d7e9f5;">PACKING</th>
</tr>
</thead>
<tr>
<td>
<div style="float:left;">
<div style="float:left;">
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" >
<tr>
<td style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;background: #d7e9f5;">

<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;width:100%;text-align:right;padding-right:8%;">
<tr>
<td style="padding-left:32%;">Container</td>
<td style="padding-left:28%;">Packing</td>
<td style="padding-left:15%;">Unpacking</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<s:set name="packingList" value="packingList" scope="request"/>
<c:if test="${packingList!='[]'}">
<display:table name="packingList" class="table" requestURI="" id="packingList"  pagesize="10" >
	<display:column property="mainDescr"  title="Description" />
	<display:column title="Qty" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctQty}" /></div></display:column>
   <display:column title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctRate}" /></div></display:column>
             <c:if test="${packingList.ctDisc!=''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctDisc}" />%</div></display:column></c:if>
             <c:if test="${packingList.ctDisc==''}">
             <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctDisc}" /></div></display:column>
             </c:if>
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;">   
	<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctCharge}" /></div></display:column>
	<display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkQty}" /></div></display:column>
   <display:column title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkRate}" /></div></display:column>
   <c:if test="${packingList.pkDisc!=''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkDisc}" />%</div></display:column></c:if>
             <c:if test="${packingList.pkDisc==''}">
             <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkDisc}" /></div></display:column>
             </c:if>
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;">
      <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkCharge}" /></div></display:column>
	<display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upQty}" /></div></display:column>
   <display:column  title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upRate}" /></div></display:column>
             <c:if test="${packingList.upDisc==''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upDisc}" /></div></display:column>
        </c:if>
        <c:if test="${packingList.upDisc!=''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upDisc}" />%</div></display:column>
        </c:if>     
   <display:column title="Charge"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upCharge}" /></div></display:column>
   <display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${packContSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packContSum}" /></div></c:if></td>
             <td colspan="3"></td>
 	  	 <td align="right">
 	  	 <c:if test="${packingSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packingSum}" /></div></c:if></td>
 	  	  	    <td colspan="3"></td><td align="right">
 	  	  	    <c:if test="${packUnpackSum!='None'}">
 	  	  	    <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packUnpackSum}" /></div>
                  </c:if>
                  </td>
      </tr>
	</display:footer>
	</display:table>
</c:if>
<c:if test="${packingList=='[]'}">
<display:table name="packingList" class="table" requestURI="" id="packingList"  pagesize="10" >
   <display:column title="Description" />
   <display:column title="Qty" />
   <display:column title="Rate"/>
   <display:column title="Discount"/>
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;"/>  
   <display:column title="Qty"/>
   <display:column title="Rate"/>
   <display:column title="Discount"/>
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;"/>
   <display:column title="Qty"/>
   <display:column  title="Rate"/>
   <display:column title="Discount"/>   
   <display:column title="Charge"/>
   <display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${packContSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packContSum}" /></div></c:if></td>
             <td colspan="3"></td>
 	  	 <td align="right">
 	  	 <c:if test="${packingSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packingSum}" /></div></c:if></td>
 	  	  	    <td colspan="3"></td><td align="right">
 	  	  	    <c:if test="${packUnpackSum!='None'}">
 	  	  	    <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packUnpackSum}" /></div>
                  </c:if>
                  </td>
      </tr>
	</display:footer>
	</display:table>
</c:if>
	</td>
	</tr>
	</table>
	</div>
	
	</div>
	</td>
	
	</tr>
	</table>
	
	<div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;"  width="530" >
<tr>
<td style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;background: #d7e9f5;">OTHER</td>
</tr>
<tr>
<td>
<s:set name="otherMiscellaneousList" value="otherMiscellaneousList" scope="request"/>
<c:if test="${otherMiscellaneousList!='[]'}">
<display:table name="otherMiscellaneousList" class="table" requestURI="" id="otherMiscellaneousList"  pagesize="10">
    <display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.qty}" /></div></display:column>
    <display:column property="rateType"  title="Rate Type" />
   <display:column property="description"  title="Description" />
   <display:column title="Rate" >
   <c:if test="${otherMiscellaneousList.qty!=''}">
   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.rate}" /></div></c:if>
             <c:if test="${otherMiscellaneousList.qty==''}">
   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.charge}" /></div></c:if>
             </display:column>
   <display:column title="Discount" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.discount}" />%</div></display:column>
   <display:column title="Charge" style="width:10%;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.charge}" /></div></display:column>   
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="5"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${othersSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${othersSum}" /></div>
             </c:if></td>             
      </tr>
	</display:footer>
        
	</display:table>
</c:if>
<c:if test="${otherMiscellaneousList=='[]'}">
<display:table name="otherMiscellaneousList" class="table" requestURI="" id="otherMiscellaneousList"  defaultsort="1" defaultorder="descending" pagesize="10">
   <display:column title="Qty"/>
   <display:column title="Rate Type" />
   <display:column title="Description" />
   <display:column title="Rate" />
   <display:column title="Discount" />
   <display:column title="Charge" style="width:10%;"/> 
   <display:footer>
	<tr> 
 	  	 <td align="right" colspan="5"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${othersSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${othersSum}" /></div>
             </c:if></td>             
      </tr>
	</display:footer>        
	</display:table>
</c:if>
	</td>
	</tr>
	</table>	
	</div>
	
		<div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;"  width="530" >
<tr>
<td style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;background: #d7e9f5;">VALUATION</td>
</tr>
<tr>
<td>
<s:set name="valuationList" value="valuationList" scope="request"/>
<c:if test="${valuationList!='[]'}">
<display:table name="valuationList" class="table" requestURI="" id="valuationList"  defaultsort="1" defaultorder="descending"  pagesize="10">
    <display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.qty}" /></div></display:column>
   <display:column property="description"  title="Description" />
   <display:column title="Rate" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.rate}" /></div></display:column>
   <display:column title="Discount" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.discount}" />%</div></display:column>
   <display:column title="Charge" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.charge}" /></div></display:column>   
	
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${valuationSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationSum}" /></div></c:if></td>             
      </tr>
	</display:footer>	
	</display:table>
</c:if>
<c:if test="${valuationList=='[]'}">
<display:table name="valuationList" class="table" requestURI="" id="valuationList"  defaultsort="1" defaultorder="descending"  pagesize="10">
   <display:column title="Qty"/>
   <display:column title="Description" />
   <display:column title="Rate" />
   <display:column title="Discount" />
   <display:column title="Charge" />	
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${valuationSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationSum}" /></div></c:if></td>             
      </tr>
	</display:footer>	
	</display:table>
</c:if>
	</td>
	</tr>      
    </table>	
	</div>	
</div>
</td></tr> 
		                     
							</tbody>
						</table>
		</div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
</div>
</s:form>
<script type="text/javascript">
function openActiveList() {
	<c:if test="${vanLineAccountView =='category'}">
		location.href = "accountCategoryViewList.html?sid=${serviceOrder.id}&vanLineAccountView=${vanLineAccountView}&accountLineStatus=true";
	</c:if>
	<c:if test="${vanLineAccountView !='category'}">
	    location.href = "accountLineList.html?sid=${serviceOrder.id}";
	</c:if>
}
</script>