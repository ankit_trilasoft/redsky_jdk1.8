<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
  
<head>   
    <title><fmt:message key="claimLossTicketDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='claimLossTicketDetail.heading'/>"/>
    <style type="text/css">h2 {background-color: #FBBFFF}</style>

</head>
<s:form id="claimLossTicketForm" action="saveClaimLossTicket.html?from=list&decorator=popup&popup=true" method="post" validate="true">   
<s:hidden name="claimLossTicket.id" />
<s:hidden name="lossNumber" value="<%=request.getParameter("lossNumber") %>"/>
<s:hidden name="updateCharge" value="YES"/>
<s:hidden name="claimLossTicket.corpID" />
<s:hidden name="claimLossTicket.shipNumber"/>
<table class="detailTabLabel" border="0">
<tbody>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claimLossTicket.ticket"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claimLossTicket.ticket" size="18" maxlength="16" readonly="true"/></td><s:hidden name="serviceOrder.id"/><s:hidden name="claim.id"/><s:hidden name="loss.id"/>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claimLossTicket.chargeBack"/></td>
<td align="left"><s:select cssClass="list-menu" name="claimLossTicket.chargeBack" value="%{claimLossTicket.chargeBack}" list="%{yesno}" cssStyle="width:75px"required="true" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="claimLossTicket.percent"/></td>
<td align="left"><s:textfield cssClass="input-text" name="claimLossTicket.percent" size="18" maxlength="3" required="true"  onkeydown="return onlyNumsAllowed(event)"/></td>
<s:hidden name="claimLossTicket.claimNumber" />
<s:hidden name="claimLossTicket.lossNumber" />
<s:hidden name="claimLossTicket.sequenceNumber" />
<s:hidden name="claimLossTicket.isAppliedToAllLoss" />
</tr> 
</tbody>
</table>
<c:out value="${button1}" escapeXml="false" />  

<c:set var="button1">
         <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:70px; height:25px"/>
<!--       <s:reset cssClass="cssbutton" key="Reset" cssStyle="width:70px; height:25px"/>   -->
 <input type="button" name="closeWin" class="cssbutton" value="Close Window" style="width:120px; height:25px" onclick="window.close()" />
</c:set>
    
<c:out value="${button1}" escapeXml="false" /> 
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
</s:form>

<script language="JavaScript">
	function notExists(){
	alert("The Claim Item is not been saved yet");
}	
</script>
<script>
  function pick() { }
</script>

<script type="text/javascript">  
	<c:if test="${!param.popup}"> 
		parent.window.opener.document.location.reload();
	  	window.close();
  	</c:if>
    Form.focusFirstElement($("claimLossTicketForm"));
</script>  		