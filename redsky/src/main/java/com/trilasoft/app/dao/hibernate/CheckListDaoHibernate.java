package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CheckListDao;
import com.trilasoft.app.model.CheckList;
import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.model.CheckListResultDTO;
import com.trilasoft.app.model.ToDoRule;

public class CheckListDaoHibernate extends GenericDaoHibernate<CheckList, Long> implements CheckListDao{

	public CheckListDaoHibernate() {
		super(CheckList.class);
	}

	public List getById() {
		// TODO Auto-generated method stub
		return null;
	}

	public List getControlDateList(String sessionCorpID) {
		return getHibernateTemplate().find("select rulesFiledName from DataCatalog where isdateField='true' and corpID in ('TSFT','" + sessionCorpID + "') group by rulesFiledName order by rulesFiledName");
	}

	public List execute(String sessionCorpID) {
		// TODO Auto-generated method stub
		return null;
	}

	public List executeExpression(String ruleExpression) {
		return this.getSession().createSQLQuery(ruleExpression).list();	
	}

	public List<CheckList> getAllCheckListRules(String sessionCorpID) {
		return getHibernateTemplate().find("from CheckList where corpID='"+sessionCorpID+"' and enable=true and status=true");
	}

	public void deleteThisResult(Long checkListId, String corpID) {
		if(checkListId != 0L){
			getHibernateTemplate().bulkUpdate("delete from CheckListResult where checkListId="+checkListId+" and corpID='"+corpID+"'");
		}else{
			getHibernateTemplate().bulkUpdate("delete from CheckListResult where  corpID='"+corpID+"'");
		}
	}

	public List <CheckListResult> getResultList(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed) {
		if(owner==null){owner="";}
		if(ruleId==null){ruleId="";}
		String owners = owner.replaceAll("'", "");
		List result= new ArrayList();
		String query="";
		query="select tr.id, tr.owner, tr.url,tr.corpID, tr.duration,tr.doctype,tr.messagedisplayed, tr.resultId,tr.checklistid, "
				+ "tr.shipper, tr.rolelist, tr.checkedUserName, "
				+ "tr.resultNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType,n.id as nid, "
				+ "if(n.note='',n.subject,n.note),tr.billToName,tr.ruleNumber "
				+ "from checklistresult tr left outer join notes n on tr.resultId = n.notesKeyId "
				+ "and (tr.noteType = n.noteType or n.notetype = 'Activity') and n.noteStatus not in('CNCL','CMP') "
				+ "and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+sessionCorpID+"' ,'TSFT') "
				+ "and (tr.checklistid=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) ";
		if(ruleId.equalsIgnoreCase("")){
		           if(duration.equals("today")){
		                 if(owner.equalsIgnoreCase("UNASSIGNED")){					
						query=query+" where owner ='' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"' group by tr.resultId,tr.ruleNumber";
				}else{
					query=query+" where owner like '%"+owners+"%' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"' group by tr.resultId,tr.ruleNumber";
				}
				}
			if(duration.equals("overDue")){
				if(owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where owner ='' and tr.duration >0 and tr.corpID='"+sessionCorpID+"' group by tr.resultId,tr.ruleNumber";
				}else{
					query=query+" where owner like '%"+owners+"%' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration > 0 and tr.corpID='"+sessionCorpID+"' group by tr.resultId,tr.ruleNumber";
				}
				
			}
		}else{
			if(duration.equals("today")){
				if(owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where owner ='' and ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"' group by tr.resultId,tr.ruleNumber";
				}else{
					query=query+" where owner like '%"+owners+"%' and messageDisplayed like '%"+messageDisplayed+"%' and ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"' group by tr.resultId,tr.ruleNumber";
				}
				}
			if(duration.equals("overDue")){
				if(owner.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where owner ='' and ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"' group by tr.resultId,tr.ruleNumber";
				}else{
					query=query+" where owner like '%"+owners+"%' and ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"' group by tr.resultId,tr.ruleNumber";
				}
				
			}
		}
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			CheckListRuleDTO  toDoRuleDTO= new CheckListRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDuration("");}else{toDoRuleDTO.setDuration(row[4]);}
			if(row[5] == null){toDoRuleDTO.setDocType("");}else{toDoRuleDTO.setDocType(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessageDisplayed("");}else{toDoRuleDTO.setMessageDisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setResultId("");}else{toDoRuleDTO.setResultId(row[7]);}
			if(row[8] == null){toDoRuleDTO.setCheckListId("");}else{toDoRuleDTO.setCheckListId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[9]);}
			if(row[10] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[10]);}
			if(row[11] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[11]);}
			if(row[12] == null){toDoRuleDTO.setResultNumber("");}else{toDoRuleDTO.setResultNumber(row[12]);}
			if(row[13] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[14] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[14]);}
			if(row[15] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[15]);}
			if(row[16] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[16]);}
			if(row[17] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[17]);}
			if(row[18] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[18]);}
			if(row[19] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[19]);}
			result.add(toDoRuleDTO);
		  }
		
		return result;
	}
	public  class CheckListRuleDTO
	{
		
		private Object id;
		private Object owner;
		private Object url;
		private Object corpID;	
		private Object duration;
		private Object docType;
		private Object messageDisplayed;		
		private Object resultId;
		private Object checkListId;
		private Object shipper;
		private Object rolelist;
		private Object checkedUserName;
		private Object resultNumber;
		private Object isNotesAdded;		
		private Object noteType;
		private Object noteSubType;		
		private Object noteId;
		private Object note;
		private Object billToName;		
		private Object ruleNumber;
		
		private Object current;
		private Object overDue;
		private Object total;		
		private Object emailNotification;
		
		public Object getCheckedUserName() {
			return checkedUserName;
		}
		public void setCheckedUserName(Object checkedUserName) {
			this.checkedUserName = checkedUserName;
		}
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getDuration() {
			return duration;
		}
		public void setDuration(Object duration) {
			this.duration = duration;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getOwner() {
			return owner;
		}
		public void setOwner(Object owner) {
			this.owner = owner;
		}
		public Object getUrl() {
			return url;
		}
		public void setUrl(Object url) {
			this.url = url;
		}
		public Object getDocType() {
			return docType;
		}
		public void setDocType(Object docType) {
			this.docType = docType;
		}
		public Object getMessageDisplayed() {
			return messageDisplayed;
		}
		public void setMessageDisplayed(Object messageDisplayed) {
			this.messageDisplayed = messageDisplayed;
		}
		public Object getResultId() {
			return resultId;
		}
		public void setResultId(Object resultId) {
			this.resultId = resultId;
		}
		public Object getCheckListId() {
			return checkListId;
		}
		public void setCheckListId(Object checkListId) {
			this.checkListId = checkListId;
		}
		public Object getShipper() {
			return shipper;
		}
		public void setShipper(Object shipper) {
			this.shipper = shipper;
		}
		public Object getRolelist() {
			return rolelist;
		}
		public void setRolelist(Object rolelist) {
			this.rolelist = rolelist;
		}
		public Object getResultNumber() {
			return resultNumber;
		}
		public void setResultNumber(Object resultNumber) {
			this.resultNumber = resultNumber;
		}
		public Object getIsNotesAdded() {
			return isNotesAdded;
		}
		public void setIsNotesAdded(Object isNotesAdded) {
			this.isNotesAdded = isNotesAdded;
		}
		public Object getNoteType() {
			return noteType;
		}
		public void setNoteType(Object noteType) {
			this.noteType = noteType;
		}
		public Object getNoteSubType() {
			return noteSubType;
		}
		public void setNoteSubType(Object noteSubType) {
			this.noteSubType = noteSubType;
		}
		public Object getNoteId() {
			return noteId;
		}
		public void setNoteId(Object noteId) {
			this.noteId = noteId;
		}
		public Object getNote() {
			return note;
		}
		public void setNote(Object note) {
			this.note = note;
		}
		public Object getBillToName() {
			return billToName;
		}
		public void setBillToName(Object billToName) {
			this.billToName = billToName;
		}
		public Object getRuleNumber() {
			return ruleNumber;
		}
		public void setRuleNumber(Object ruleNumber) {
			this.ruleNumber = ruleNumber;
		}
		public Object getCurrent() {
			return current;
		}
		public void setCurrent(Object current) {
			this.current = current;
		}
		public Object getOverDue() {
			return overDue;
		}
		public void setOverDue(Object overDue) {
			this.overDue = overDue;
		}
		public Object getTotal() {
			return total;
		}
		public void setTotal(Object total) {
			this.total = total;
		}
		public Object getEmailNotification() {
			return emailNotification;
		}
		public void setEmailNotification(Object emailNotification) {
			this.emailNotification = emailNotification;
		}
		
	}
	public List findNumberOfResultEntered(String sessionCorpID, Long id) {
		return getHibernateTemplate().find("from CheckListResult where checkListId="+id+" and corpID='"+sessionCorpID+"'");
	}

	public Integer findNumberOfResult(String sessionCorpID) {
		Integer count=0;
		List resultList=getHibernateTemplate().find("from CheckListResult where corpID='"+sessionCorpID+"'");
		if(!resultList.isEmpty()){
			count=resultList.size();
		}
		return count;
	}
	public List executeTdrCheckList(String sessionCorpID){
		return null;
	}
	public HashMap<String, CheckListResultDTO> getAllToDoResultListForCheckList(String sessionCorpID,Long currentRuleNumber){
		List recordList = new ArrayList();
		String query="";
		if(currentRuleNumber!=null){
			query="select t.id,tr.resultrecordid,tr.corpid,t.durationAddSub, " +
			" tr.messagedisplayed,tr.shipper,tr.owner,tr.filenumber,t.doctype,tr.id as toDoResultId,t.ruleNumber " +
					" from todorule t inner join todoresult tr on t.rulenumber=tr.rulenumber and tr.corpId='"+sessionCorpID+"' "
					+ " left outer join myfile m on tr.filenumber = m.fileid and m.corpid = '"+sessionCorpID+"' where t.doctype<>'' "
							+ " and t.doctype not in (select filetype from myfile where fileid=m.fileid and m.corpid = '"+sessionCorpID+"' and active is true) "
									+ " and t.corpid='"+sessionCorpID+"' and tr.rulenumber = '"+currentRuleNumber+"' "
											+ "	 group by filenumber ";
		}else{/*
			query="select t.id,tr.resultrecordid,tr.corpid,t.durationAddSub, " +
			"tr.messagedisplayed,tr.shipper,tr.owner,tr.filenumber,t.doctype,tr.id as toDoResultId,t.ruleNumber " +
			"from todorule t inner join todoresult tr on t.rulenumber=tr.rulenumber right outer join myfile m on  m.fileid = tr.filenumber " +
			"where t.doctype<>'' " +
			"and m.corpid = tr.corpid and t.corpid=tr.corpid " +
			"and t.corpid=m.corpid and m.filetype <> t.doctype and t.corpid='"+sessionCorpID+"' " +
			"and m.corpid = '"+sessionCorpID+"' and tr.corpid='"+sessionCorpID+"' group by filenumber ";
		*/}
		//System.out.println("\n\n\n\n\n\n check list result query >> "+query);
		List list = this.getSession().createSQLQuery(query).list();
		Iterator it1 = list.iterator();
		CheckListResultDTO checkListResult = null;
		HashMap<String, CheckListResultDTO> checkListMap = new HashMap<String, CheckListResultDTO>();
		while(it1.hasNext()){
			Object [] row=(Object[])it1.next();
			checkListResult = new CheckListResultDTO();
			checkListResult.setCheckListId(Long.parseLong(row[0].toString()));
			checkListResult.setResultId(Long.parseLong(row[1].toString()));
			checkListResult.setCorpID(row[2].toString());
			checkListResult.setDuration(Long.parseLong(row[3].toString()));
			checkListResult.setMessageDisplayed(row[4].toString()+ " - " +row[8].toString()+" is missing");
			checkListResult.setShipper(row[5].toString());
			checkListResult.setOwner(row[6].toString());
			checkListResult.setResultNumber(row[7].toString());
			checkListResult.setDocType(row[8].toString());
			checkListResult.setToDoResultId(Long.parseLong(row[9].toString()));
			checkListResult.setRuleNumber(row[10].toString());
			checkListResult.setId(null);
			checkListMap.put(row[9].toString(), checkListResult);
			recordList.add(checkListResult);
			
		}
		query="select t.checkListId,t.resultId,t.corpID,t.duration," +
				"t.MessageDisplayed,t.Shipper,t.Owner,t.ResultNumber,t.DocType,t.ToDoResultId,t.ruleNumber,t.id " +
				" from checklistresult t left outer join myfile m on  m.fileid = t.resultNumber" +
				" where t.doctype<>''" +
				" and m.corpid = t.corpid " +
				" and t.corpid=m.corpid and t.doctype not in (select filetype from myfile where fileid=m.fileid and m.corpid = '"+sessionCorpID+"' and active is true) and t.corpid='"+sessionCorpID+"'" +
				" and m.corpid = '"+sessionCorpID+"' group by resultNumber ";
		
		List list1 = this.getSession().createSQLQuery(query).list();
		Iterator it2 = list1.iterator();
		while(it2.hasNext()){
			Object [] row=(Object[])it2.next();
			checkListResult = new CheckListResultDTO();
			checkListResult.setCheckListId(Long.parseLong(row[0].toString()));
			checkListResult.setResultId(Long.parseLong(row[1].toString()));
			checkListResult.setCorpID(row[2].toString());
			checkListResult.setDuration(Long.parseLong(row[3].toString()));
			checkListResult.setMessageDisplayed(row[4].toString());
			checkListResult.setShipper(row[5].toString());
			checkListResult.setOwner(row[6].toString());
			checkListResult.setResultNumber(row[7].toString());
			checkListResult.setDocType(row[8].toString());
			checkListResult.setToDoResultId(Long.parseLong(row[9].toString()));
			checkListResult.setRuleNumber(row[10].toString());
			checkListResult.setId(Long.parseLong(row[11].toString()));
			checkListMap.put(row[9].toString(), checkListResult);
			recordList.add(checkListResult);
			
		}
		return checkListMap;
	}
	
	public void updateAndDel(String sessionCorpID,Long currentRuleNumber,Long ruleId){
		getHibernateTemplate().bulkUpdate("delete from CheckListResult where checkResultForDel='0' and corpID = '"+sessionCorpID+"' and ruleNumber = '"+currentRuleNumber+"' ");
		getHibernateTemplate().bulkUpdate("update CheckListResult set checkResultForDel='0'  where checkResultForDel='1' and corpID = '"+sessionCorpID+"' and ruleNumber = '"+currentRuleNumber+"' ");
	}

	public List<ToDoRule> getAllRules(String sessionCorpID) {
		List <ToDoRule> toDoRules= getHibernateTemplate().find("from ToDoRule where status='Tested' and checkEnable=true and corpID in ('TSFT','"+sessionCorpID+"') and docType is not null and docType !='' ");
		return toDoRules;
	}
	
	public List <CheckListResult> getResultListExtCordinator(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed,String bookingAgentPopup,String billToCodePopup,String userType) {
		if(owner==null ||owner.equalsIgnoreCase("")){owner="''";}
		if(ruleId==null){ruleId="";}
		//String owners = owner.replaceAll("'", "");
		 String user = owner.replaceAll("'", "");	
		List result= new ArrayList();
		String query="";
		List userList=new ArrayList();
		List tempuserfilter=new ArrayList();
		String billtocodecondition="";
		String bookingAgentcondition="";
		String billtocodenotecondition="";
		String bookingAgentnotecondition="";
		String acc="";
		if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
		{
			
			billtocodecondition= "and tr.billToCode in ("+billToCodePopup+")";
		}
	
		if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
		{
			bookingAgentcondition= "and tr.bookingAgentCode in ("+bookingAgentPopup+")";
        }
		if(userType.equalsIgnoreCase("ACCOUNT")){
			acc=" and isAccount is true ";
		}
		query="select tr.id, tr.owner, tr.url,tr.corpID, tr.duration,tr.doctype,tr.messagedisplayed, tr.resultId,tr.checklistid, "
				+ "tr.shipper, tr.rolelist, tr.checkedUserName, "
				+ "tr.resultNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType,n.id as nid, "
				+ "if(n.note='',n.subject,n.note),tr.billToName,tr.ruleNumber "
				+ "from checklistresult tr left outer join notes n on tr.resultId = n.notesKeyId "
				+ "and (tr.noteType = n.noteType or n.notetype = 'Activity') and n.noteStatus not in('CNCL','CMP') "
				+ "and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+sessionCorpID+"' ,'TSFT') "
				+ "and (tr.checklistid=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) ";
		if(ruleId.equalsIgnoreCase("")){
		           if(duration.equals("today")){
		                 if(user.equalsIgnoreCase("UNASSIGNED")){					
						query=query+" where owner ='' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}else if(user!=null && !user.equals("")){
					query=query+" where owner in ("+owner+") and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}
				else
				{
					query=query+" where messageDisplayed like '%"+messageDisplayed+"%' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";

				}
				}
			if(duration.equals("overDue")){
				if(user.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where owner ='' and tr.duration >0 and tr.corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}else if(user!=null && !user.equals("")){
					query=query+" where owner in ("+owner+") and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration > 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}
				else
				{
					query=query+" where  messageDisplayed like '%"+messageDisplayed+"%' and tr.duration > 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";

				}
				
			}
		}else{
			if(duration.equals("today")){
				if(user.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where owner ='' and ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}else if(user!=null && !user.equals("")){
					query=query+" where owner in ("+owner+") and messageDisplayed like '%"+messageDisplayed+"%' and ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}
				else
				{
					query=query+" where  messageDisplayed like '%"+messageDisplayed+"%' and ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";

				}
				}
			if(duration.equals("overDue")){
				if(user.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where owner ='' and ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}else if(user!=null && !user.equals("")){
					query=query+" where owner in ("+owner+") and ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+"  group by tr.resultId,tr.ruleNumber";
				}
				else
				{
					query=query+" where ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";

				}
				
			}
		}
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			CheckListRuleDTO  toDoRuleDTO= new CheckListRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDuration("");}else{toDoRuleDTO.setDuration(row[4]);}
			if(row[5] == null){toDoRuleDTO.setDocType("");}else{toDoRuleDTO.setDocType(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessageDisplayed("");}else{toDoRuleDTO.setMessageDisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setResultId("");}else{toDoRuleDTO.setResultId(row[7]);}
			if(row[8] == null){toDoRuleDTO.setCheckListId("");}else{toDoRuleDTO.setCheckListId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[9]);}
			if(row[10] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[10]);}
			if(row[11] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[11]);}
			if(row[12] == null){toDoRuleDTO.setResultNumber("");}else{toDoRuleDTO.setResultNumber(row[12]);}
			if(row[13] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[14] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[14]);}
			if(row[15] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[15]);}
			if(row[16] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[16]);}
			if(row[17] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[17]);}
			if(row[18] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[18]);}
			if(row[19] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[19]);}
			result.add(toDoRuleDTO);
		  }
		
		return result;
	}
	public List <CheckListResult> getResultListExtCordinatorForTeam(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed,String bookingAgentPopup,String billToCodePopup,String username,String allradio,String userType) {
		if(owner==null ||owner.equalsIgnoreCase("")){owner="''";}
		if(ruleId==null){ruleId="";}
		//String owners = owner.replaceAll("'", "");
		List result= new ArrayList();
		String query="";
		List userList=new ArrayList();
		List tempuserfilter=new ArrayList();
		String billtocodecondition="";
		String bookingAgentcondition="";
		String billtocodenotecondition="";
		String bookingAgentnotecondition="";
		String userName="'"+username+"'";
		String finaluser=userName+","+owner;
		 String user = owner.replaceAll("'", "");	
		 String acc="";
		if(billToCodePopup!=null && !billToCodePopup.toString().trim().equals(""))
		{
			
			billtocodecondition= "and tr.billToCode in ("+billToCodePopup+")";
		}
	
		if(bookingAgentPopup!=null && !bookingAgentPopup.toString().trim().equals(""))
		{
			bookingAgentcondition= "and tr.bookingAgentCode in ("+bookingAgentPopup+")";
        }
		
		if(userType.equalsIgnoreCase("ACCOUNT")){
			acc=" and isAccount is true ";
		}
		if(allradio.equalsIgnoreCase("Team"))
		{
		query="select tr.id, tr.owner, tr.url,tr.corpID, tr.duration,tr.doctype,tr.messagedisplayed, tr.resultId,tr.checklistid, "
				+ "tr.shipper, tr.rolelist, tr.checkedUserName, "
				+ "tr.resultNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType,n.id as nid, "
				+ "if(n.note='',n.subject,n.note),tr.billToName,tr.ruleNumber "
				+ "from checklistresult tr left outer join notes n on tr.resultId = n.notesKeyId "
				+ "and (tr.noteType = n.noteType or n.notetype = 'Activity') and n.noteStatus not in('CNCL','CMP') "
				+ "and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+sessionCorpID+"' ,'TSFT') "
				+ "and (tr.checklistid=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) ";
		if(ruleId.equalsIgnoreCase("")){
		           if(duration.equals("today")){
		                 if(user.equalsIgnoreCase("UNASSIGNED")){					
						query=query+" where owner ='' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+"  "+acc+"  group by tr.resultId,tr.ruleNumber";
				}else if(user!=null && !user.equals("")){
					query=query+" where owner in ("+finaluser+") and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";
				}
				else
				{
					query=query+" where  messageDisplayed like '%"+messageDisplayed+"%' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";

				}
				}
			if(duration.equals("overDue")){
				if(user.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where owner ='' and tr.duration >0 and tr.corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";
				}else if(user!=null && !user.equals("")){ 
					query=query+" where owner in ("+finaluser+") and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration > 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";
				}
				else
				{
					query=query+" where messageDisplayed like '%"+messageDisplayed+"%' and tr.duration > 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";

				}
				
			}
		}else{
			if(duration.equals("today")){
				if(user.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where owner ='' and ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";
				}else if(user!=null && !user.equals("")){ 
					query=query+" where owner in ("+finaluser+") and messageDisplayed like '%"+messageDisplayed+"%' and ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";
				}
				else
				{
					query=query+" where  messageDisplayed like '%"+messageDisplayed+"%' and ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";

				}
				}
			if(duration.equals("overDue")){
				if(user.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where owner ='' and ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";
				}else if(user!=null && !user.equals("")){ 
					query=query+" where owner in ("+finaluser+") and ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";
				}
				else
				{
					query=query+" where  ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";

				}
				
			}
		}
		}
		if(allradio.equalsIgnoreCase("All"))
		{
			query="select tr.id, tr.owner, tr.url,tr.corpID, tr.duration,tr.doctype,tr.messagedisplayed, tr.resultId,tr.checklistid, "
				+ "tr.shipper, tr.rolelist, tr.checkedUserName, "
				+ "tr.resultNumber, tr.isNotesAdded, tr.noteType, tr.noteSubType,n.id as nid, "
				+ "if(n.note='',n.subject,n.note),tr.billToName,tr.ruleNumber "
				+ "from checklistresult tr left outer join notes n on tr.resultId = n.notesKeyId "
				+ "and (tr.noteType = n.noteType or n.notetype = 'Activity') and n.noteStatus not in('CNCL','CMP') "
				+ "and (tr.noteSubType = n.noteSubType or n.noteSubtype = 'ActivityManagement' or n.noteSubtype = 'AgentNotes') and n.corpID in ('"+sessionCorpID+"' ,'TSFT') "
				+ "and (tr.checklistid=n.todoRuleId or n.todoRuleId='' or n.todoRuleId is null) ";
		if(ruleId.equalsIgnoreCase("")){
		           if(duration.equals("today")){
		                 if(user.equalsIgnoreCase("UNASSIGNED")){					
						query=query+" where  tr.duration = 0 and tr.corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+"  group by tr.resultId,tr.ruleNumber";
				}else{
					query=query+" where  messageDisplayed like '%"+messageDisplayed+"%' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}
				}
			if(duration.equals("overDue")){
				if(user.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where   tr.duration >0 and tr.corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+"   "+acc+"  group by tr.resultId,tr.ruleNumber";
				}else{
					query=query+" where  messageDisplayed like '%"+messageDisplayed+"%' and tr.duration > 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}
				
			}
		}else{
			if(duration.equals("today")){
				if(user.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where  ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"' "+billtocodecondition+""+bookingAgentcondition+" "+acc+"  group by tr.resultId,tr.ruleNumber";
				}else{
					query=query+" where   messageDisplayed like '%"+messageDisplayed+"%' and ruleNumber='"+ruleId+"' and tr.duration = 0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}
				}
			if(duration.equals("overDue")){
				if(user.equalsIgnoreCase("UNASSIGNED")){
					query=query+" where   ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}else{
					query=query+" where  ruleNumber='"+ruleId+"' and messageDisplayed like '%"+messageDisplayed+"%' and tr.duration >0 and tr.corpID='"+sessionCorpID+"'  "+billtocodecondition+""+bookingAgentcondition+" "+acc+" group by tr.resultId,tr.ruleNumber";
				}
				
			}
		}	
			
			
		}
		List list= this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext())
		  {
			Object [] row=(Object[])it.next(); 
			CheckListRuleDTO  toDoRuleDTO= new CheckListRuleDTO();
			if(row[0] == null){toDoRuleDTO.setId("");}else{toDoRuleDTO.setId(row[0]);}
			if(row[1] == null){toDoRuleDTO.setOwner("");}else{toDoRuleDTO.setOwner(row[1]);}
			if(row[2] == null){toDoRuleDTO.setUrl("");}else{toDoRuleDTO.setUrl(row[2]);}
			if(row[3] == null){toDoRuleDTO.setCorpID("");}else{toDoRuleDTO.setCorpID(row[3]);}
			if(row[4] == null){toDoRuleDTO.setDuration("");}else{toDoRuleDTO.setDuration(row[4]);}
			if(row[5] == null){toDoRuleDTO.setDocType("");}else{toDoRuleDTO.setDocType(row[5]);}
			if(row[6] == null){toDoRuleDTO.setMessageDisplayed("");}else{toDoRuleDTO.setMessageDisplayed(row[6]);}
			if(row[7] == null){toDoRuleDTO.setResultId("");}else{toDoRuleDTO.setResultId(row[7]);}
			if(row[8] == null){toDoRuleDTO.setCheckListId("");}else{toDoRuleDTO.setCheckListId(row[8]);}
			if(row[9] == null){toDoRuleDTO.setShipper("");}else{toDoRuleDTO.setShipper(row[9]);}
			if(row[10] == null){toDoRuleDTO.setRolelist("");}else{toDoRuleDTO.setRolelist(row[10]);}
			if(row[11] == null){toDoRuleDTO.setCheckedUserName("");}else{toDoRuleDTO.setCheckedUserName(row[11]);}
			if(row[12] == null){toDoRuleDTO.setResultNumber("");}else{toDoRuleDTO.setResultNumber(row[12]);}
			if(row[13] == null){toDoRuleDTO.setIsNotesAdded(false);}else{toDoRuleDTO.setIsNotesAdded(true);}
			if(row[14] == null){toDoRuleDTO.setNoteType("");}else{toDoRuleDTO.setNoteType(row[14]);}
			if(row[15] == null){toDoRuleDTO.setNoteSubType("");}else{toDoRuleDTO.setNoteSubType(row[15]);}
			if(row[16] == null){toDoRuleDTO.setNoteId("");}else{toDoRuleDTO.setNoteId(row[16]);}
			if(row[17] == null){toDoRuleDTO.setNote("");}else{toDoRuleDTO.setNote(row[17]);}
			if(row[18] == null){toDoRuleDTO.setBillToName("");}else{toDoRuleDTO.setBillToName(row[18]);}
			if(row[19] == null){toDoRuleDTO.setRuleNumber("");}else{toDoRuleDTO.setRuleNumber(row[19]);}
			result.add(toDoRuleDTO);
		  }
		
		return result;
	}
	
}
