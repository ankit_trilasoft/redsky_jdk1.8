<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>    
	<authz:authorize ifAnyGranted="ROLE_SALE" >
    	<title><fmt:message key="myCalendarList.title"/></title>   
    	<meta name="heading" content="<fmt:message key='myCalendarList.heading'/>"/>   
 	</authz:authorize>
	<authz:authorize ifAnyGranted="ROLE_ADMIN" ifNotGranted="ROLE_SALE" >
    	<title>Company Events</title>   
    	<meta name="heading" content="Company Events"/>   
 	</authz:authorize>
<script language="javascript" type="text/javascript">

function clear_fields(){
document.forms['myCalendarList'].elements['surveyFrom'].value = "";
document.forms['myCalendarList'].elements['surveyTo'].value = "";
document.forms['myCalendarList'].elements['consult'].value = "";
document.forms['myCalendarList'].elements['surveyCity'].value = "";
document.forms['myCalendarList'].elements['surveyJob'].value = "";

}
</script>
 <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
    <script>
    function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this Event?");
	var id1 = targetElement;
	
	if (agree){
			document.forms['myCalendarList'].action="deleteMyCalendar.html?id1="+id1;
			document.forms['myCalendarList'].submit();
	}else{
		return false;
	}
	}
	
	function getSurveyList(){
		var id = document.forms['myCalendarList'].elements['id'].value;
		document.forms['myCalendarList'].action = "surveysList.html?id1="+id+"";
		document.forms['myCalendarList'].submit();
	}
	
function myCalenderforDays(){
 document.forms['myCalendarList'].elements['checkSurveyToClick'].value = '1';
}
	
function myCalenderCalcDays()
{ 
 var date1 = document.forms['myCalendarList'].elements['surveyFrom'].value;	 
 var date2 = document.forms['myCalendarList'].elements['surveyTo'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(document.forms['myCalendarList'].elements['surveyFrom'].value=='' && document.forms['myCalendarList'].elements['surveyTo'].value!=''){
  alert("Please select From Date.");
  return false;
  }
  if(document.forms['myCalendarList'].elements['surveyTo'].value=='' && document.forms['myCalendarList'].elements['surveyFrom'].value!=''){
  alert("Please select To Date.");
  return false;
  }
  if(daysApart<0)
  {
    alert("To Date should be greater than From Date.");
    document.forms['myCalendarList'].elements['surveyTo'].value='';
    return false;
  } 
  else{
   return true;
  }
}
	
	
	
    </script>
<style>    
    form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>
</head>


<s:form id="myCalendarList" action="searchMyEvents" method="post" >  
<c:set var="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden id="checkSurveyToClick" name ="checkSurveyToClick"/>
<s:hidden name="formStatus" value=""/>
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:65px; height:25px"  
        onclick="location.href='<c:url value="/editMyCalendars.html?id1=${id}"/>'"  
        value="Add Event"/>   
</c:set> 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom:10px;" align="top" key="button.search" onclick="return myCalenderCalcDays();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set>   
<c:if test="${not empty id}" >
		<div id="newmnav">
		  <ul>
		    <li><a href="editCustomerFile.html?id=<%=request.getParameter("id") %> "><span>Customer File</span></a></li>
		    <li><a href="customerServiceOrders.html?id=<%=request.getParameter("id") %> "><span>Service Orders</span></a></li>
		    <li><a href="customerRateOrders.html?id=<%=request.getParameter("id") %>"><span>Rate Request</span></a></li>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Surveys<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onclick="openWindow('subModuleReports.html?id=<%=request.getParameter("id") %>&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=customerFile&reportSubModule=Survey&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&decorator=popup&popup=true',750,400)"><span>Forms</span></a></li>
		    <li><a onclick="openWindow('myFiles.html?id=<%=request.getParameter("id") %>&myFileFor=CF&decorator=popup&popup=true',740,400);" ><span>Docs</span></a></li> 
		 </ul>
		</div><div class="spn">&nbsp;</div>
	
</c:if>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>


<div id="Layer1" style="width:100%">


				<div id="otabs" >
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
				<table class="table" style="width:98%" >
				<thead>
				<tr>
				<authz:authorize ifAnyGranted="ROLE_SALE"><th><fmt:message key="customerFile.estimator"/></th></authz:authorize>
				<th>From&nbsp;Date</th>
				<th>To&nbsp;Date</th>
				<th>&nbsp;</th>
				
				</tr></thead>	
						<tbody>
						<tr>
							<authz:authorize ifAnyGranted="ROLE_SALE">
							<td>
								<s:textfield id="consultDay" name="consult" cssStyle="width:65px" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
							</td>
							</authz:authorize>
							<td>
							    <s:textfield name="surveyFrom" cssClass="input-text" id="date1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" /><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
							<td>
							    <s:textfield name="surveyTo" cssClass="input-text" id="date2" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" /><img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</td>
							<td>
							    <c:out value="${searchbuttons}" escapeXml="false" />   
							</td>
							</tr>							
						</tbody>
					</table>
					</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
				<s:set name="calendarFiles" value="calendarFiles" scope="request"/>  
				<authz:authorize ifAnyGranted="ROLE_SALE">
				<div id="newmnav" style="margin-top:-15px; ">
						  <ul>
						  <c:if test="${not empty id}" >
						  	 <li><a href="surveysList.html?id1=<%=request.getParameter("id") %>"><span>Survey List</span></a></li>
						   	 <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Events<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						  </c:if>
						  <c:if test="${empty id}" >
						  	 <li id="newmnav1" style="background:#FFF"><a class="current" ><span>Events<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						  </c:if>
						  </ul>
				</div><div class="spn">&nbsp;</div>
				
				
				<display:table name="calendarFiles" class="table" requestURI="" id="myCalendarList" export="true" defaultorder="descending" style="margin-top:3px;">   
				    <display:column property="userName" titleKey="calendarFile.userName" url='/editMyCalendars.html?from=list&id1=${id}' paramId="id" paramProperty="id"/>
				    <display:column property="surveyDate" sortable="true" titleKey="calendarFile.surveyDate" format="{0,date,dd-MMM-yyyy}"/>
				    <display:column headerClass="containeralign" style="text-align: right;" property="fromTime" titleKey="calendarFile.fromTime" />
				    <display:column headerClass="containeralign" style="text-align: right;" property="toTime" titleKey="calendarFile.toTime" />
				    <display:column titleKey="calendarFile.fullDay" >
				    <c:choose>
				    <c:when test="${myCalendarList.fullDay == true}" >
				    	Yes
				    </c:when>
				    <c:otherwise >
				    	
				    </c:otherwise>
				    </c:choose>
				    </display:column>
				    <display:column property="activityName" titleKey="calendarFile.activityName" />
				    
				    <display:column title="Remove" style="text-align: center;">
				    <a><img align="middle" onclick="confirmSubmit(${myCalendarList.id});" style="margin: 0px 0px 0px 1px;" src="images/recyle-trans.gif"/></a>
				    </display:column>
				    
				    <display:setProperty name="paging.banner.item_name" value="calendar"/>   
				    <display:setProperty name="paging.banner.items_name" value="calendars"/>   
				  
				    <display:setProperty name="export.excel.filename" value="My Calendar List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="My Calendar List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="My Calendar List.pdf"/>   
				</display:table> 
				</authz:authorize>
				<authz:authorize ifAnyGranted="ROLE_ADMIN" ifNotGranted="ROLE_SALE" >
				<div id="newmnav" style="margin-top:-15px; ">
						  <ul>
						  <c:if test="${not empty id}" >
						  	 <li><a href="surveysList.html?id1=<%=request.getParameter("id") %>"><span>Survey List</span></a></li>
						   	 <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Events<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						  </c:if>
						  <c:if test="${empty id}" >
						  	 <li id="newmnav1" style="background:#FFF"><a class="current" ><span>Company Events<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
						  </c:if>
						  </ul>
				</div><div class="spn">&nbsp;</div>
				
				<display:table name="calendarFiles" class="table" requestURI="" id="myCalendarList" export="true" defaultorder="descending" style="margin-top:3px;!margin-top:10px ">   
				    <display:column property="surveyDate" sortable="true" title="Event&nbsp;Date" format="{0,date,dd-MMM-yyyy}"/>
				    <display:column property="userName" titleKey="calendarFile.userName" url='/editMyCalendars.html?from=list&id1=${id}' paramId="id" paramProperty="id"/>
				    <display:column headerClass="containeralign" style="text-align:right;width:80px;" property="fromTime" titleKey="calendarFile.fromTime" />
				    <display:column headerClass="containeralign" style="text-align:right;" property="toTime" titleKey="calendarFile.toTime" />
				    <display:column property="activityName" title="Event" />
				    
				    <display:column title="Remove" style="text-align:center;width:50px;">
				    <a><img align="middle" onclick="confirmSubmit(${myCalendarList.id});" style="margin: 0px 0px 0px 1px;" src="images/recyle-trans.gif"/></a>
				    </display:column>
				    
				    <display:setProperty name="paging.banner.item_name" value="calendar"/>   
				    <display:setProperty name="paging.banner.items_name" value="calendars"/>   
				  
				    <display:setProperty name="export.excel.filename" value="My Calendar List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="My Calendar List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="My Calendar List.pdf"/>   
				</display:table> 
				</authz:authorize>

</div>
<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<c:out value="${buttons}" escapeXml="false" />   
</s:form>

<script type="text/javascript">   
   	//highlightTableRows("myCalendarList");   
   	Form.focusFirstElement($("myCalendarList")); 
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
