delimiter $$

CREATE  trigger redsky.trigger_add_history_myfile_add

after insert on redsky.myfile

for each row

BEGIN

  DECLARE fieldNameList LONGTEXT;

  DECLARE oldValueList LONGTEXT;

  DECLARE newValueList LONGTEXT;

  DECLARE oldCorpid LONGTEXT;

  DECLARE newUser LONGTEXT;

  DECLARE newRoleName LONGTEXT;

 

  SET fieldNameList = " ";

  SET oldValueList = " ";

  SET newValueList = " ";

  SET oldCorpid = " ";

  SET newUser = " ";

  SET newRoleName = " ";

 




    IF (NEW.isCportal <>'' or NEW.isCportal is not null) THEN

       select CONCAT(fieldNameList,'myfile.isCportal~') into fieldNameList;
	
	    IF(NEW.isCportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isCportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






    IF (NEW.isAccportal <>'' or NEW.isAccportal is not null) THEN

       select CONCAT(fieldNameList,'myfile.isAccportal~') into fieldNameList;

	    IF(NEW.isAccportal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isAccportal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.isServiceProvider <>'' or NEW.isServiceProvider is not null) THEN

       select CONCAT(fieldNameList,'myfile.isServiceProvider~') into fieldNameList;
	
	    IF(NEW.isServiceProvider = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isServiceProvider = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   

    IF (NEW.isPartnerPortal <>'' or NEW.isPartnerPortal is not null) THEN

       select CONCAT(fieldNameList,'myfile.isPartnerPortal~') into fieldNameList;
	
	    IF(NEW.isPartnerPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isPartnerPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.isBookingAgent <>'' or NEW.isBookingAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isBookingAgent~') into fieldNameList;
	
	    IF(NEW.isBookingAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isBookingAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isNetworkAgent <>'' or NEW.isNetworkAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isNetworkAgent~') into fieldNameList;
	
	    IF(NEW.isNetworkAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isNetworkAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





    IF (NEW.isOriginAgent <>'' or NEW.isOriginAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isOriginAgent~') into fieldNameList;
	
	    IF(NEW.isOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   


    IF (NEW.isSubOriginAgent <>'' or NEW.isSubOriginAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isSubOriginAgent~') into fieldNameList;
	
	    IF(NEW.isSubOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isSubOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   


    IF (NEW.isDestAgent <>'' or NEW.isDestAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isDestAgent~') into fieldNameList;

	    IF(NEW.isDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.isDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   
    IF (NEW.invoiceAttachment <>'' or NEW.invoiceAttachment is not null) THEN

       select CONCAT(fieldNameList,'myfile.invoiceAttachment~') into fieldNameList;

	    IF(NEW.invoiceAttachment = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;
	   
	    IF(NEW.invoiceAttachment = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   


    IF (NEW.isSubDestAgent <>'' or NEW.isSubDestAgent is not null) THEN

       select CONCAT(fieldNameList,'myfile.isSubDestAgent~') into fieldNameList;

	    IF(NEW.isSubDestAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	   	   END IF;

	    IF(NEW.isSubDestAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






CALL add_tblHistory (NEW.id,"myfile", fieldNameList, oldValueList, newValueList, NEW.updatedby, NEW.corpID, now());



END $$
delimiter;

 

