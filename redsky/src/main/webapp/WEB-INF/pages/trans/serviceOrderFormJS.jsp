<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<SCRIPT LANGUAGE="JavaScript"> 
//A function for auto save funtionality.
function totalOfestimate(){
	  // Code Gross Section
	  try{
  	var estimatedTotalRevenue=0.00;
	var estimatedTotalExpense=0.00;
	var tempEstimatedTotalExpense=0.00;
	var tempEstimatedTotalRevenue=0.00;
	var estimatedGrossMargin=0.00;
	var estimatedGrossMarginPercentage=0.00;
	var aidListIds=document.forms['serviceOrderForm'].elements['aidListIds'].value;
	var accArr=aidListIds.split(",");
	for(var g=0;g<accArr.length;g++){
		if(document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accArr[g]].value!=""){
 			tempEstimatedTotalRevenue=document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accArr[g]].value;
		}
 		estimatedTotalRevenue=estimatedTotalRevenue+parseFloat(tempEstimatedTotalRevenue);
 		estimatedTotalRevenue=Math.round(estimatedTotalRevenue*10000)/10000;
 		if(document.forms['serviceOrderForm'].elements['estimateExpense'+accArr[g]].value!=""){
 			tempEstimatedTotalExpense=document.forms['serviceOrderForm'].elements['estimateExpense'+accArr[g]].value;
 		}
 		estimatedTotalExpense=estimatedTotalExpense+parseFloat(tempEstimatedTotalExpense);
 		estimatedTotalExpense=Math.round(estimatedTotalExpense*10000)/10000;
	}
estimatedGrossMargin=estimatedTotalRevenue-estimatedTotalExpense;
try{
	if((estimatedTotalRevenue==0)||(estimatedTotalRevenue==0.0)||(estimatedTotalRevenue==0.00)){
		estimatedGrossMarginPercentage=0.00;
	}else{
		estimatedGrossMarginPercentage=(estimatedGrossMargin*100)/estimatedTotalRevenue;
	}
}catch(e){
	estimatedGrossMarginPercentage=0.00;
	}
 <c:if test="${not empty serviceOrder.id}">
document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=Math.round(estimatedTotalRevenue*100)/100;
document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=Math.round(estimatedTotalExpense*100)/100;
document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=Math.round(estimatedGrossMargin*100)/100;
document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(estimatedGrossMarginPercentage*100)/100;
</c:if>	
	  }catch(e){}
}
function disableRevenue(type){
	if(type==''){
		var aidListIds=document.forms['serviceOrderForm'].elements['aidListIds'].value;
		var accArr=aidListIds.split(",");
		for(var g=0;g<accArr.length;g++){
			var rev1=document.forms['serviceOrderForm'].elements['category'+accArr[g]].value;
			if(rev1 == "Internal Cost"){
				document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accArr[g]].readOnly=true;
				document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accArr[g]].value=0;
				document.forms['serviceOrderForm'].elements['estimateSellRate'+accArr[g]].readOnly=true;
				document.forms['serviceOrderForm'].elements['estimateSellRate'+accArr[g]].value=0;
				document.forms['serviceOrderForm'].elements['estimatePassPercentage'+accArr[g]].readOnly=true;
				document.forms['serviceOrderForm'].elements['estimatePassPercentage'+accArr[g]].value=0; 
				<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+accArr[g]].readOnly=true;
				document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+accArr[g]].value=0;
				document.forms['serviceOrderForm'].elements['revisionSellRate'+accArr[g]].readOnly=true;
				document.forms['serviceOrderForm'].elements['revisionSellRate'+accArr[g]].value=0;
				document.forms['serviceOrderForm'].elements['revisionPassPercentage'+accArr[g]].readOnly=true;
				document.forms['serviceOrderForm'].elements['revisionPassPercentage'+accArr[g]].value=0;
				calculateRevisionBlock(accArr[g],'','','','');
				</c:if>
				<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].readOnly=true;
					document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value=0;
					document.forms['serviceOrderForm'].elements['recRate'+accArr[g]].readOnly=true;
					document.forms['serviceOrderForm'].elements['recRate'+accArr[g]].value=0;
					document.forms['serviceOrderForm'].elements['recQuantity'+accArr[g]].readOnly=true;
					document.forms['serviceOrderForm'].elements['recQuantity'+accArr[g]].value=0;
						<c:if test="${multiCurrency=='Y'}"> 
						document.forms['serviceOrderForm'].elements['recCurrencyRate'+accArr[g]].readOnly=true;
						document.forms['serviceOrderForm'].elements['recCurrencyRate'+accArr[g]].value=0;
						</c:if> 
						document.forms['serviceOrderForm'].elements['payingStatus'+accArr[g]].value='I';
						calculateActualRevenue(accArr[g],'','BAS','','','');
				</c:if>
			}else{ 
				//document.forms['serviceOrderForm'].elements['estimateSellRate'+accArr[g]].readOnly=false;
				//document.forms['serviceOrderForm'].elements['estimatePassPercentage'+accArr[g]].readOnly=false; 
				<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}"> 
				document.forms['serviceOrderForm'].elements['revisionSellRate'+accArr[g]].readOnly=false;
				document.forms['serviceOrderForm'].elements['revisionPassPercentage'+accArr[g]].readOnly=false;
				</c:if>
				<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					if(document.forms['serviceOrderForm'].elements['utsiRecAccDateFlag'+accArr[g]].value==false && document.forms['serviceOrderForm'].elements['utsiPayAccDateFlag'+accArr[g]].value==false){
				    document.forms['serviceOrderForm'].elements['recRate'+accArr[g]].readOnly=false;
					document.forms['serviceOrderForm'].elements['recQuantity'+accArr[g]].readOnly=false;
						<c:if test="${multiCurrency=='Y'}"> 
						document.forms['serviceOrderForm'].elements['recCurrencyRate'+accArr[g]].readOnly=false;
						</c:if> 
					}
				</c:if> 
			} }
		totalOfestimate();
	}else{
		var rev1=document.forms['serviceOrderForm'].elements['category'+type].value;
		if(rev1 == "Internal Cost"){
			document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+type].readOnly=true;
			document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+type].value=0;
			document.forms['serviceOrderForm'].elements['estimateSellRate'+type].readOnly=true;
			document.forms['serviceOrderForm'].elements['estimateSellRate'+type].value=0;
			document.forms['serviceOrderForm'].elements['estimatePassPercentage'+type].readOnly=true;
			document.forms['serviceOrderForm'].elements['estimatePassPercentage'+type].value=0; 
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+type].readOnly=true;
			document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+type].value=0;
			document.forms['serviceOrderForm'].elements['revisionSellRate'+type].readOnly=true;
			document.forms['serviceOrderForm'].elements['revisionSellRate'+type].value=0;
			document.forms['serviceOrderForm'].elements['revisionPassPercentage'+type].readOnly=true;
			document.forms['serviceOrderForm'].elements['revisionPassPercentage'+type].value=0;
			calculateRevisionBlock(type,'','','','');
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				document.forms['serviceOrderForm'].elements['actualRevenue'+type].readOnly=true;
				document.forms['serviceOrderForm'].elements['actualRevenue'+type].value=0;
				document.forms['serviceOrderForm'].elements['recRate'+type].readOnly=true;
				document.forms['serviceOrderForm'].elements['recRate'+type].value=0;
				document.forms['serviceOrderForm'].elements['recQuantity'+type].readOnly=true;
				document.forms['serviceOrderForm'].elements['recQuantity'+type].value=0;
					<c:if test="${multiCurrency=='Y'}"> 
					document.forms['serviceOrderForm'].elements['recCurrencyRate'+type].readOnly=true;
					document.forms['serviceOrderForm'].elements['recCurrencyRate'+type].value=0;
					</c:if> 
					document.forms['serviceOrderForm'].elements['payingStatus'+type].value='I';
					calculateActualRevenue(type,'','BAS','','','');
			</c:if>
		}else{ 
			document.forms['serviceOrderForm'].elements['estimateSellRate'+type].readOnly=false;
			document.forms['serviceOrderForm'].elements['estimatePassPercentage'+type].readOnly=false; 
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}"> 
			document.forms['serviceOrderForm'].elements['revisionSellRate'+type].readOnly=false;
			document.forms['serviceOrderForm'].elements['revisionPassPercentage'+type].readOnly=false;
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(document.forms['serviceOrderForm'].elements['utsiRecAccDateFlag'+accArr[g]].value==false && document.forms['serviceOrderForm'].elements['utsiPayAccDateFlag'+accArr[g]].value==false){	
			    document.forms['serviceOrderForm'].elements['recRate'+type].readOnly=false;
				document.forms['serviceOrderForm'].elements['recQuantity'+type].readOnly=false;
					<c:if test="${multiCurrency=='Y'}"> 
					document.forms['serviceOrderForm'].elements['recCurrencyRate'+type].readOnly=false;
					</c:if> 
			}
			</c:if> 
		}
		totalOfestimate();
	} }
function findExchangeRateRateOnActualization(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function currentDateRateOnActualization(){
	 var mydate=new Date();
   var daym;
   var year=mydate.getFullYear()
   var y=""+year;
   if (year < 1000)
   year+=1900
   var day=mydate.getDay()
   var month=mydate.getMonth()+1
   if(month == 1)month="Jan";
   if(month == 2)month="Feb";
   if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function rateOnActualizationDate(field1,field2,field3){
	<c:if test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
    <c:if test="${contractType}">	
	var currency=""; 
		var currentDate=currentDateRateOnActualization();
		try{
		currency=document.forms['serviceOrderForm'].elements[field1.trim().split('~')[0]].value;
		document.forms['serviceOrderForm'].elements[field2.trim().split('~')[0]].value=currentDate;
		document.forms['serviceOrderForm'].elements[field3.trim().split('~')[0]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}
		try{
		currency=document.forms['serviceOrderForm'].elements[field1.trim().split('~')[1]].value;
		document.forms['serviceOrderForm'].elements[field2.trim().split('~')[1]].value=currentDate;
		document.forms['serviceOrderForm'].elements[field3.trim().split('~')[1]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}		
	</c:if>
	</c:if>
}
function ContainerAutoSave(clickType){
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	<c:set var="checkPropertyAmountComponent" value="N" />
	<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
		<c:set var="checkPropertyAmountComponent" value="Y" />
	</configByCorp:fieldVisibility>
	var check=false;
	 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
		 var check = true;
	 }else{
		 var check =saveValidation('autoTab');
	 }
 if(check){ 	 
        progressBarAutoSave('1');
		var elementsLen=document.forms['serviceOrderForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++) {
			
					document.forms['serviceOrderForm'].elements[i].disabled=false;
		}
        var id1 = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    	var jobNumber = document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
    	var idc = document.forms['serviceOrderForm'].elements['customerFile.id'].value;
	if (document.forms['serviceOrderForm'].elements['formStatus'].value == '1'){
	if ('${autoSavePrompt}' == 'No'){
			var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                noSaveAction = 'customerServiceOrders.html?id='+idc;
              }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                //noSaveAction = 'accountLineList.html?sid='+id1;
                <c:if test="${checkPropertyAmountComponent!='Y'}">
                	noSaveAction = 'accountLineList.html?sid='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		noSaveAction = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
    }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
        noSaveAction = 'pricingList.html?sid='+id1;
        }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.OI'){
        //noSaveAction = 'operationResource.html?id='+id1;
        <c:if test="${checkPropertyAmountComponent!='Y'}">
	    	noSaveAction = 'operationResource.html?id='+id1;
		</c:if>
	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
	  		noSaveAction = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
	  	</c:if>
    } 
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.operationResourceOI'){
        noSaveAction = 'operationResourceFromAcPortal.html?id='+id1;
        } 
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.containers'){
				 <c:if test="${forwardingTabVal!='Y'}">
					noSaveAction = 'containers.html?id='+id1;
				</c:if>
			  	<c:if test="${forwardingTabVal=='Y'}">
			  		noSaveAction = 'containersAjaxList.html?id='+id1;
			  </c:if>
         	}
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.billing'){
                //noSaveAction = 'editBilling.html?id='+id1;
			<c:if test="${checkPropertyAmountComponent!='Y'}">
				noSaveAction = 'editBilling.html?id='+id1;
			</c:if>
		  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  		noSaveAction = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  	</c:if>
    }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                //noSaveAction = 'editMiscellaneous.html?id='+id1;
		<c:if test="${checkPropertyAmountComponent!='Y'}">
			noSaveAction = 'editMiscellaneous.html?id='+id1;
		</c:if>
	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
	  		noSaveAction = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
	  	</c:if>
    }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.status'){
                var rel=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
                if(rel=='RLO'){ 
                	noSaveAction = 'editDspDetails.html?id='+id1;
                  relo="yes";
                }else{
                	//noSaveAction =  'editTrackingStatus.html?id='+id1;
                	<c:if test="${checkPropertyAmountComponent!='Y'}">
						noSaveAction = 'editTrackingStatus.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		noSaveAction = 'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
                    }   }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                //noSaveAction = 'customerWorkTickets.html?id='+id1;
                <c:if test="${checkPropertyAmountComponent!='Y'}">
	    	    	noSaveAction = 'customerWorkTickets.html?id='+id1;
	    		</c:if>
	    	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
	    	  		noSaveAction = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
	    	  	</c:if>
    }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.claims'){
                //noSaveAction = 'claims.html?id='+id1;
                <c:if test="${checkPropertyAmountComponent!='Y'}">
	    	    	noSaveAction = 'claims.html?id='+id1;
	    		</c:if>
	    	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
	    	  		noSaveAction = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
	    	  	</c:if>
    }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.surveydetail'){
        		noSaveAction = 'inventoryDataList.html?cid='+idc+'&id='+id1;      
        		} 
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                noSaveAction = 'editCustomerFile.html?id='+cidVal;
                }
	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
        noSaveAction = 'soAdditionalDateDetails.html?sid='+id1;
        }
          processAutoSave(document.forms['serviceOrderForm'], 'saveServiceOrder!saveOnTabChange.html', noSaveAction);
	}
else{
     if(!(clickType == 'save')){
      var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='serviceOrderDetail.heading'/>");
         if(agree){
           document.forms['serviceOrderForm'].action = 'saveServiceOrder!saveOnTabChange.html';
           document.forms['serviceOrderForm'].submit();
       }else{
           if(id1 != ''){
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
               location.href = 'customerServiceOrders.html?id='+idc;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
               //location.href = 'accountLineList.html?sid='+id1;
               <c:if test="${checkPropertyAmountComponent!='Y'}">
               		location.href = 'accountLineList.html?sid='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
               location.href = 'pricingList.html?sid='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.OI'){
        	   //location.href = 'operationResource.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	          		location.href = 'operationResource.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.operationResourceOI'){
               location.href = 'operationResourceFromAcPortal.html?id='+id1;
               } 
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.containers'){
	        	   <c:if test="${forwardingTabVal!='Y'}">
				 		location.href = 'containers.html?id='+id1;
		  			</c:if>
			  		<c:if test="${forwardingTabVal=='Y'}">
			  			//location.href = 'containersAjaxList.html?id='+id1;
			  			<c:if test="${checkPropertyAmountComponent!='Y'}">
			          		location.href = 'containersAjaxList.html?id='+id1;
				   		</c:if>
				   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				   	 		location.href = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				   	  	</c:if>
			  		</c:if>
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.billing'){
				//location.href = 'editBilling.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
        	   		location.href = 'editBilling.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
			}
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.domestic'){
               //location.href = 'editMiscellaneous.html?id='+id1;
        	   	<c:if test="${checkPropertyAmountComponent!='Y'}">
        	   		location.href = 'editMiscellaneous.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.status'){ 
                var rel=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
                if(rel=='RLO'){ 
                	location.href = 'editDspDetails.html?id='+id1;
                  relo="yes";
                }else{
                	//location.href =  'editTrackingStatus.html?id='+id1;
                	<c:if test="${checkPropertyAmountComponent!='Y'}">
                		location.href = 'editTrackingStatus.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
                    }  }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.ticket'){
               //location.href = 'customerWorkTickets.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	         		location.href = 'customerWorkTickets.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.claims'){
               //location.href = 'claims.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	        		location.href = 'claims.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.surveydetail'){
               location.href = 'inventoryDataList.html?cid='+idc+'&id='+id1;      
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
        	   var cidVal='${customerFile.id}';
               location.href = 'editCustomerFile.html?id='+cidVal;
               }
       	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
       		location.href = 'soAdditionalDateDetails.html?sid='+id1;
            } } }
   }else{
   if(id1 != ''){
       if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
               location.href = 'customerServiceOrders.html?id='+idc;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
               //location.href = 'accountLineList.html?sid='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	       			location.href = 'accountLineList.html?sid='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
               location.href = 'pricingList.html?sid='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.OI'){
        	   //location.href = 'operationResource.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	      			location.href = 'operationResource.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           } 
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.operationResourceOI'){
               location.href = 'operationResourceFromAcPortal.html?id='+id1;
           } 
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.containers'){
	        	   <c:if test="${forwardingTabVal!='Y'}">
				 		location.href = 'containers.html?id='+id1;
		  			</c:if>
			  		<c:if test="${forwardingTabVal=='Y'}">
			  			//location.href = 'containersAjaxList.html?id='+id1;
			  			<c:if test="${checkPropertyAmountComponent!='Y'}">
			      			location.href = 'containersAjaxList.html?id='+id1;
				   		</c:if>
				   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				   	 		location.href = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				   	  	</c:if>
			  		</c:if>
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.billing'){
				//location.href = 'editBilling.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	   	   			location.href = 'editBilling.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
			}
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.domestic'){
               //location.href = 'editMiscellaneous.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	   	   			location.href = 'editMiscellaneous.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.status'){
                var rel=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
                if(rel=='RLO') { 
                	location.href = 'editDspDetails.html?id='+id1;
                  relo="yes";
                }else{
                	//location.href =  'editTrackingStatus.html?id='+id1;
                	<c:if test="${checkPropertyAmountComponent!='Y'}">
	            		location.href = 'editTrackingStatus.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
                    }  }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.ticket'){
               //location.href = 'customerWorkTickets.html?id='+id1;
               <c:if test="${checkPropertyAmountComponent!='Y'}">
		      		location.href = 'customerWorkTickets.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.claims'){
               //location.href = 'claims.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
		      		location.href = 'claims.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.surveydetail'){
               location.href = 'inventoryDataList.html?cid='+idc+'&id='+id1;   
               }           
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
        	   var cidVal='${customerFile.id}';
               location.href = 'editCustomerFile.html?id='+cidVal;
               }
       	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
       		location.href = 'soAdditionalDateDetails.html?sid='+id1;
            }  } }  } }
  else{	  
  if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
               location.href = 'customerServiceOrders.html?id='+idc;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
               //location.href = 'accountLineList.html?sid='+id1;
               <c:if test="${checkPropertyAmountComponent!='Y'}">
		      		location.href = 'accountLineList.html?sid='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
               location.href = 'pricingList.html?sid='+id1;
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.OI'){
        	   //location.href = 'operationResource.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
		      		location.href = 'operationResource.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           } 
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.operationResourceOI'){
               location.href = 'operationResourceFromAcPortal.html?id='+id1;
           } 
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.containers'){
		        	   <c:if test="${forwardingTabVal!='Y'}">
					 		location.href = 'containers.html?id='+id1;
			  			</c:if>
				  		<c:if test="${forwardingTabVal=='Y'}">
				  			//location.href = 'containersAjaxList.html?id='+id1;
				  			<c:if test="${checkPropertyAmountComponent!='Y'}">
					      		location.href = 'containersAjaxList.html?id='+id1;
					   		</c:if>
					   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
					   	 		location.href = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
					   	  	</c:if>
				  		</c:if>
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.billing'){
				//location.href = 'editBilling.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	   	   			location.href = 'editBilling.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
			}
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.domestic'){
               //location.href = 'editMiscellaneous.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	   	   			location.href = 'editMiscellaneous.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'editMiscellaneous.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.status'){               
                   var rel=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
                   if(rel=='RLO'){ 
                     	location.href = 'editDspDetails.html?id='+id1;
                     relo="yes";
                   }else{
                	   //location.href =  'editTrackingStatus.html?id='+id1;
                	   <c:if test="${checkPropertyAmountComponent!='Y'}">
	               			location.href = 'editTrackingStatus.html?id='+id1;
						</c:if>
					  	<c:if test="${checkPropertyAmountComponent=='Y'}">
					  		location.href = 'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
					  	</c:if>
                       } 
                }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.ticket'){
               //location.href = 'customerWorkTickets.html?id='+id1;
        	   <c:if test="${checkPropertyAmountComponent!='Y'}">
		      		location.href = 'customerWorkTickets.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.claims'){
               //location.href = 'claims.html?id='+id1;
               <c:if test="${checkPropertyAmountComponent!='Y'}">
		      		location.href = 'claims.html?id='+id1;
		   		</c:if>
		   	  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		   	 		location.href = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		   	  	</c:if>
           }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.surveydetail'){
               location.href = 'inventoryDataList.html?cid='+idc+'&id='+id1;               
               }
           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
        	   var cidVal='${customerFile.id}';
               location.href = 'editCustomerFile.html?id='+cidVal;
               }
       	if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
       		location.href = 'soAdditionalDateDetails.html?sid='+id1;
            } 
       	
       	else
       		{
       		showOrHide(0);
       		
       		}} 
	
 
 } }
function changeStatus1() {
	for (var i=0; i < document.forms['serviceOrderForm'].elements['serviceOrder.customerFeedback'].length; i++) 	{
   if (document.forms['serviceOrderForm'].elements['serviceOrder.customerFeedback'][i].checked) {
      		var rad_val = document.forms['serviceOrderForm'].elements['serviceOrder.customerFeedback'][i].value;
	      if(rad_val=='No')  {
	      	document.getElementById('COA').style.display = 'none';
	      	document.getElementById('OAE').style.display = 'none';
	      	document.getElementById('DAE').style.display = 'none';
			document.getElementById('Comments').style.display = 'none';
			document.forms['serviceOrderForm'].elements['serviceOrder.feedBack'].style.display = 'none';
			document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].style.display = 'none';
			document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].style.display = 'none';
			document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].style.display = 'none';
	     	document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].value=''
			document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].value=''
			document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].checked=false
	      }
		   else  if(rad_val=='Yes'){
		   		document.getElementById('COA').style.display = '';
	      		document.getElementById('OAE').style.display = '';
	      		document.getElementById('DAE').style.display = '';
		   		document.getElementById('Comments').style.display = '';
		   		document.forms['serviceOrderForm'].elements['serviceOrder.feedBack'].style.display = '';
		   		document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].style.display = '';
				document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].style.display = '';
				document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].style.display = '';
		   } } } }
function changeStatus(){
   document.forms['serviceOrderForm'].elements['formStatus'].value='1';
}
function validate(targetElement){
if (document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].value=='Y'){
document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].value='1'
document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].value='1'
}
else{
   	if (document.forms['serviceOrderForm'].elements['serviceOrder.clientOverallResponse'].value=='N'){
document.forms['serviceOrderForm'].elements['serviceOrder.OAEvaluation'].value='-1'
document.forms['serviceOrderForm'].elements['serviceOrder.DAEvaluation'].value='-1'
    }  } }
function myFunction(){
 var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
 	 if(bookCode != '' || bookCode != null){
 	    	checkBookingAgent();  	
		   	setTimeout(function(){findBookingAgentName();}, 500);  		
	 }	 }    
 function checkBookingAgent() {
	var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
    var url="bookingAgent.html?ajax=1&decorator=simple&popup=true&companyDivision=" + encodeURI(companyDivision);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse222;
     http2.send(null);
     setTimeout(function(){findBookingAgentName();},1000);
}
function handleHttpResponse222() { 
          if (http2.readyState == 4)  {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
                 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value= results;                   
                 }   }   }
function getCompanyDivisionByJob() {
	var custJobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	var url="findCompanyDivisionByJob.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponse3333;
    http6.send(null);
    setTimeout(function(){checkBookingAgent();},400); 
}
function handleHttpResponse3333() {
		    if (http6.readyState == 4)  {
                var results = http6.responseText
                results = results.trim();
                var res = results.split("@");
                var targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'];
					targetElement.length = res.length;
 					for(i=1;i<res.length;i++)
 					{
 						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].text = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].value = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=true;
					} }	 }  
function getCommodity(){
    var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
    var url;    
    if(jobType=='STO'|| jobType=='STF'|| jobType=='STL' || jobType=='TPS' ){
      url="getCommodity.html?ajax=1&decorator=simple&popup=true&commodityParameter=" + encodeURI('COMMODITS');
     }
     else
     {
       url="getCommodity.html?ajax=1&decorator=simple&popup=true&jobType="+encodeURI(jobType)+"&commodityParameter=" + encodeURI('COMMODIT');      
     }
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse100;
     http2.send(null);
} 
function handleHttpResponse100() {
             if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                res = results.replace("{",'');
                res = res.replace("}",'');
                res = res.split(",");
                targetElementValue=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'];
				targetElement.length = res.length; 
				var loc=0;
				var flag=0;
				var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
 				for(i=0;i<res.length;i++) { 
					  if(res[i] == ''){
					  document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].text = '';
					  document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].value = '';
					} else { 
					   stateVal = res[i].replace("=",'+');
					   stateVal=stateVal.split("+");
					   document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].text=stateVal[1];
					   document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].value=stateVal[0].trim();
					   if(job == "")
					   {
					   	document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[0].selected=true;
					  	document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[0].selected=true;
					   } }  }
 			if(targetElementValue==''){
 				targetElementValue = '${serviceOrder.commodity}';
 			}
           document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value=targetElementValue;
       }  } 
function putDefault(){
	document.forms['serviceOrderForm'].elements['serviceOrder.job'].value = '${serviceOrder.job}';
	document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value = '${serviceOrder.commodity}';
	document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value = '${serviceOrder.serviceType}';
	findService();
	getCommodity();
}
function findBookingAgentName(){
    var bookingAgentCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
    if(bookingAgentCode == ''){
    	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = "";
    }
    if(bookingAgentCode!=''){
        try{    	
    document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].value = "";
        }catch(e){}
    if(bookingAgentCode != ''){
    showOrHide(1)
     var url="BookingAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
    }   } }
function handleHttpResponse4(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
                		showOrHide(0);
	           			document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = res[1];
                   		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
                	}else{
                		showOrHide(0);
	           			alert("Booking Agent code is not approved" ); 
					    document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
	           		}  
                }else{
                	showOrHide(0);
                     alert("Booking Agent code not valid");
                     document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
               } } }  
function getKeyByValue(myData,value){
	for (key in myData) {
	    if (myData.hasOwnProperty(key)) {
	    	if(value==myData[key]){
	    		return key;
	    	} } } }
 function getState(targetElement) {
	 var country = targetElement.value;
	 var countryList = JSON.parse('${jsonCountryCodeText}');
	 var countryCode =getKeyByValue(countryList,country);
	 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpHVY.open("GET", url, true);
     httpHVY.onreadystatechange = handleHttpResponse7;
     httpHVY.send(null);
	}
function getDestinationState(targetElement) {
	var country = targetElement.value; 
	var countryList = JSON.parse('${jsonCountryCodeText}');
	var countryCode = getKeyByValue(countryList,country); 
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse8;
     http3.send(null);
	}
function findWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid); 
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse3;
     http2.send(null);
} }
function handleHttpResponse3() {
             if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00") {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){ 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value = '${miscellaneous.actualGrossWeight}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value = '${miscellaneous.actualNetWeight}';
                 	calcNetWeight3();
                 } } } }  
function findNetWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }else{
     var url="containerNetWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = netWtCheckFromContCart;
     http2.send(null);
     } }
function netWtCheckFromContCart() {
             if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00") {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){ 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value = '${miscellaneous.actualNetWeight}';
                 } } } } 
function findTareWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     } else{
     var url="containerTareWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse6;
     http2.send(null);
} }
function handleHttpResponse6() {
             if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){ 
                 }else{ 
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value = '${miscellaneous.actualTareWeight}';
                 	calcNetWeight3();	
                 } } }  }     
function findVolumeWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerVolumeWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);
} }
function handleHttpResponse5() {
             if (http2.readyState == 4) {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){ 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value = '${miscellaneous.actualCubicFeet}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value = '${miscellaneous.actualCubicMtr}';
                 } } }  } 
function findWeightKilo(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerWeightKiloList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid); 
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse33;
     http4.send(null);
}
}
function handleHttpResponse33() {
             if (http4.readyState == 4) {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00") {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){ 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value = '${miscellaneous.actualGrossWeightKilo}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value = '${miscellaneous.actualNetWeightKilo}';
                 	calcNetWeightKgs3();
                 } } } }  
function findNetWeightKilo(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }else{
     var url="containerNetWeightKiloList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse22;
     http4.send(null);
     } }
function handleHttpResponse22() { 
             if (http4.readyState == 4) {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){ 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value = '${miscellaneous.actualNetWeightKilo}';
                 } }  } }  
function findTareWeightKilo(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerTareWeightKiloList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse66;
     http4.send(null);
} }
function handleHttpResponse66() { 
             if (http4.readyState == 4) {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00")
                {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value = '${miscellaneous.actualTareWeightKilo}';
                 	calcNetWeightKgs3();	
                 } } }  }     
function findVolumeWeightCbm(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerVolumeWeightCbmList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http4.open("GET", url, true);
     http4.onreadystatechange = handleHttpResponse55;
     http4.send(null);
} } 
function handleHttpResponse55() {
             if (http4.readyState == 4)  {
                var results = http4.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null" && results!="0" && results!="0.00") {
                 var agree = confirm("Weights/Volume is defaulted from Container/Piece Count screen ,do you still want to edit? ");
                 if(agree){ 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value = '${miscellaneous.actualCubicMtr}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value = '${miscellaneous.actualCubicFeet}';
                 } } }  }    
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function handleHttpResponse7() {
             if (httpHVY.readyState == 4)  {
                var results = httpHVY.responseText
                results = results.trim();                
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++) {
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
					} } }
					if ('${stateshitFlag}'=="1"){
					document.getElementById("originState").value = '';
					}
					else{ document.getElementById("originState").value == '${serviceOrder.originState}';
					 }	 } 
function handleHttpResponse8() {
             if (http3.readyState == 4) {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++) {
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
					} } }
					if ('${stateshitFlag}'=="1"){
					document.getElementById("destinationState").value = '';
					}
					else{ document.getElementById("destinationState").value == '${serviceOrder.destinationState}';
					   }  }
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
    var http2 = getHTTPObject(); 
function getHTTPObject1() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    var httpNetworkAgent= getHTTPObject1();
function getHTTPObject4() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
    var http4 = getHTTPObject4();
    function statusCheck() {
    	var status = document.forms['serviceOrderForm'].elements['serviceOrder.status1'].value; 
    }
    function myDate() {
    	var mydate=new Date()
    	var year=mydate.getYear()
    	if (year < 1000)
    	year+=1900
    	var day=mydate.getDay()
    	var month=mydate.getMonth()+1
    	if (month<10)
    	month="0"+month
    	var daym=mydate.getDate()
    	if (daym<10)
    	daym="0"+daym
    	var datam = month+"/"+daym+"/"+year;
    	var tim1=document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value;  
    	if(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value == "null" ){
    		document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value="";
    		}
    		if(document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value == '0.0' ){
    		document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value="";
    	}
    	if(document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value == '0.0' ){
    		document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value="";
    	}
    	if(document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value == '0.0' ){
    		document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value="";
    	}
    	if(document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value == '0.0' ){
    		document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value="";
    	}
    	if(document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value == '0.0' ){
    		document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value="";
    	} 
    	if(document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value == '0.0' ){
    		document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value="";
    	}
    	 var f = document.getElementById('serviceOrderForm'); 
    		f.setAttribute("autocomplete", "off");
    	var partnerType=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
    		var el = document.getElementById('hid');
    		if(partnerType == 'AUTO' || partnerType == 'HHG/A' || partnerType == 'BOAT'){
    		el.style.visibility = 'visible';
    		}else{
    		el.style.visibility = 'collapse';
    		}
    		var distanceUnit=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
    		var el = document.getElementById('hidDM');
    		if(distanceUnit == 'Overland' || distanceUnit == 'Truck'){
    		el.style.display = 'block';
    		el.style.visibility = 'visible';		
    		}else{
    		el.style.display = 'collapse';
    		} } 
    function waitForMapToLoad(){
    }
    function loadMaps() { 
      google.load("maps", "2", {"callback" : waitForMapToLoad}); 
    }
    function validAutoBoat() {
          var partnerType=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
    		var el = document.getElementById('hid');
    		if(partnerType == 'AUTO' || partnerType == 'HHG/A' || partnerType == 'BOAT'){
    		el.style.display = 'block';
    		el.style.visibility = 'visible';
    		}else{
    			if(el != null && el != undefined){
    				el.style.display = 'none';
    			} } }
    function validateDistanceUnit() {
          var distanceUnit=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
    		var el = document.getElementById('hidDM');
    		if(distanceUnit == 'Overland' || distanceUnit == 'Truck'){
    		initLoader();
    		el.style.display = 'block';
    		el.style.visibility = 'visible';		
    		}else{
    		el.style.display = 'none';
    		} }
	function autoPopulate_customerFile_statusDate(targetElement) { 
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec"; 
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['serviceOrder.statusDate'].value=datam; 
	}
	function copyCompanyToDestination(targetElement){
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationCompany'].value = targetElement.value;
	} 
	function validate_email(field) { 
		with (field) {
		apos=value.indexOf("@")
		dotpos=value.lastIndexOf(".")
		if (apos<1||dotpos-apos<2) 
		  {alert("Not a valid e-mail address!");return false}
		else {return true}
		} }
		function calcNetWeight1(){
			var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value;
			var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value; 
			Q1 = Math.round(Q1*100)/100
			   Q2 = Math.round(Q2*100)/100
			var E1='';
				if(Q1<Q2) {
					alert("Grossweight should be greater than Tareweight");
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
				}else if(Q1 != undefined && Q2 != undefined) {
					E1=Q1-Q2;
					var E2=Math.round(E1*100)/100;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
					var E3 = E2*0.4536;
					var E4=Math.round(E3*100)/100;
					var Q3 = Q1*0.4536;
					var Q4=Math.round(Q3*100)/100;
					var Q5 = Q2*0.4536;
					var Q6=Math.round(Q5*100)/100;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=Q4;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=Q6;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
				}else if(Q1 == undefined && Q2 == undefined){
					E1=0;
					var E2=Math.round(E1*100)/100;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
					var E3 = E2*0.4536;
					var E4=Math.round(E3*100)/100;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=0;
				}else if(Q2 == undefined) {
					E1=Q1;
					var E2=Math.round(E1*100)/100;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
					var E3 = E2*0.4536;
					var E4=Math.round(E3*100)/100;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=E4;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
				}else if(Q1 == undefined) {
					alert("Grossweight should be greater than Tareweight");
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=0;
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
			        document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
				} }
		//  End of function.
		//  function used to calculate the estimate net weight. 
		function calcNetWeight2(){
		var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;
		var Q2 =document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value; 
		Q1 = Math.round(Q1*100)/100
		   Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2) {
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){
				E1=Q1-Q2;
				var E2=Math.round(E1*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*100)/100;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*100)/100;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=Q4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=Q6;
			}else if(Q1 == undefined && Q2 == undefined) {
				E1=0;
				var E2=Math.round(E1*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=0;
			}else if(Q2 == undefined) {
				E1=Q1;
				var E2=Math.round(E1*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*100)/100;
		        document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
			}else if(Q1 == undefined) {
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
			}
		}
		//  End of function.
		//  function used to calculate the actual net weight. 
		function calcNetWeight3(){ 
				var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;
		     	var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value;
		     	Q1 = Math.round(Q1*100)/100
		        Q2 = Math.round(Q2*100)/100
				var E1='';
					if(Q1<Q2) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
				        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
					}else if(Q1 != undefined && Q2 != undefined){
						E1=Q1-Q2;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E2;
						var E3 = E2*0.4536;
				        var E4=Math.round(E3*100)/100;
						var Q3 = Q1*0.4536;
						var Q4=Math.round(Q3*100)/100;
						var Q5 = Q2*0.4536;
						var Q6=Math.round(Q5*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=Q4;
				        document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
				        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=Q6;
					}else if(Q1 == undefined && Q2 == undefined) {
						E1=0;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E2;
						var E3 = E2*0.4536;
				        var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=0;
					}else if(Q2 == undefined) {
						E1=Q1;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E2;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
						var E3 = E2*0.4536;
				        var E4=Math.round(E3*100)/100;
				        document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
					}else if(Q1 == undefined) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
				        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
					}
		}
		//  End of function.
		//  function used to calculate the rwgh net weight. 
		function calcNetWeight4(){ 
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value;
				var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value; 
				Q1 = Math.round(Q1*100)/100
				   Q2 = Math.round(Q2*100)/100
				var E1='';
					if(Q1<Q2) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
					}else if(Q1 != undefined && Q2 != undefined){	
						E1=Q1-Q2;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E2;
						var E3 = E2*0.4536;
						var E4=Math.round(E3*100)/100;
						var Q3 = Q1*0.4536;
						var Q4=Math.round(Q3*100)/100;
						var Q5 = Q2*0.4536;
						var Q6=Math.round(Q5*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=Q4;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=Q6;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
					}else if(Q1 == undefined && Q2 == undefined) {
						E1=0;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E2;
						var E3 = E2*0.4536;
						var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=0;
					}else if(Q2 == undefined) {
						E1=Q1;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E2;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
						var E3 = E2*0.4536;
						var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
					}else if(Q1 == undefined) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
					}
		}
		//  End of function.
		function calcNetWeight5(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value;
				var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value;
				Q1 = Math.round(Q1*100)/100
				   Q2 = Math.round(Q2*100)/100
				var E1='';
					if(Q1<Q2){
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
					}else if(Q1 != undefined && Q2 != undefined) {	
					     E1=Q1-Q2;
						 var E2=Math.round(E1*100)/100;
						 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
						 var E3 = E2*0.4536;
						var E4=Math.round(E3*100)/100;
						var Q3 = Q1*0.4536;
						var Q4=Math.round(Q3*100)/100;
						var Q5 = Q2*0.4536;
						var Q6=Math.round(Q5*100)/100;
						 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
						 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=Q4;
						 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=Q6; 
					}else if(Q1 == undefined && Q2 == undefined) {
						E1=0;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
						var E3 = E2*0.4536;
						var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=0;
					}else if(Q2 == undefined) {
						E1=Q1;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
						var E3 = E2*0.4536;
						var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
					}else if(Q1 == undefined) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
					} } 
		//  End of function.
		function calcGrossVolume1(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicMtr'].value=0;
					}
			}
		//  End of function.    
		function calcGrossVolume2(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
		        var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value=0;
					} }
		//  End of function. 
		function calcGrossVolume3(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value=0;
					}
			}
		//  End of function. 
		function calcGrossVolume4(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicMtr'].value=0;
					}
			}
		//  End of function. 
		function calcGrossVolume5(){
		        var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicMtr'].value=0;
					}
			}
		//  End of function. 
		function calcNetVolume1(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicMtr'].value=0;
					}
			}
		//  End of function.  
		function calcNetVolume2(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) 	{
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value=0;
					}
			}
		//  End of function. 
		function calcNetVolume3(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value=0;
					}
			}
		//  End of function. 
		function calcNetVolume4(){
		        var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicMtr'].value=0;
					}
			}
		//  End of function. 
		function calcNetVolume5(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.0283;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicMtr'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicMtr'].value=0;
					}
			}
		//  End of function. 
		function calcNetVolumeMtr1(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value=0;
					}
			}
		//  End of function. 
		function calcNetVolumeMtr2(){
		        var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value=0;
					}
			}
		//  End of function. 
		function calcNetVolumeMtr3(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value=0;
					}
			}
		//  End of function. 
		function calcNetVolumeMtr4(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].value=0;
					}
			}
		//  End of function. 
		function calcNetVolumeMtr5(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].value=0;
					}
			}
		//  End of function. 
		function calcGrossVolumeMtr1(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value=0;
					}
			}
		//  End of function. 
		function calcGrossVolumeMtr2(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined){
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value=E3;
					}else{
						document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value=0;
					} }
		//  End of function. 
		function calcGrossVolumeMtr3(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined){
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value=E3;
					}else{
						document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value=0;
					} }
		//  End of function. 
		function calcGrossVolumeMtr4(){
		        var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
		        var E1='';
					if(Q1 != undefined)
					{
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].value=0;
					} }
		//  End of function. 
		function calcGrossVolumeMtr5(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicMtr'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*35.3147;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].value=E3;
					}
					else{
					 document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].value =0;
					} }
		//  End of function. 
		function calcNetWeightKgs1(){
		var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value;
		var Q2 =document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value;
		Q1 = Math.round(Q1*100)/100
		   Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2) {
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){
				E1=Q1-Q2;
				var E2=Math.round(E1*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
				        var E3 = E2*2.2046;
						var E4=Math.round(E3*100)/100;
						var Q3 = Q1*2.2046;
						var Q4=Math.round(Q3*100)/100;
						var Q5 = Q2*2.2046;
						var Q6=Math.round(Q5*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=Q4;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=Q6;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
			}else if(Q1 == undefined && Q2 == undefined) {
				E1=0;
				var E2=Math.round(E1*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=0;
			}else if(Q2 == undefined) {
				E1=Q1;
				var E2=Math.round(E1*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
			}else if(Q1 == undefined) {
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
			} 
			}
		//  End of function.
		function calcNetWeightKgs2(){
		var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
		var Q2 =document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value; 
		Q1 = Math.round(Q1*100)/100
		   Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2) {
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined) {
				E1=Q1-Q2;
				var E2=Math.round(E1*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
				        var E3 = E2*2.2046;
						var E4=Math.round(E3*100)/100;
						var Q3 = Q1*2.2046;
						var Q4=Math.round(Q3*100)/100;
						var Q5 = Q2*2.2046;
						var Q6=Math.round(Q5*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=Q4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=Q6;
			}else if(Q1 == undefined && Q2 == undefined) {
				E1=0;
				var E2=Math.round(E1*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=0;
			}else if(Q2 == undefined) {
				E1=Q1;
				var E2=Math.round(E1*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*100)/100;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
			}else if(Q1 == undefined) {
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
			} } 
		function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
			document.getElementById(autocompleteDivId).style.display = "none";
			if(document.getElementById(paertnerCodeId).value==''){
				document.getElementById(partnerNameId).value="";	
			} } 
			function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
				lastName=lastName.replace("~","'");
				document.getElementById(partnerNameId).value=lastName;
				document.getElementById(paertnerCodeId).value=partnercode;
				document.getElementById(autocompleteDivId).style.display = "none";	
				if(paertnerCodeId=='soBookingAgentCodeId'){
					findBookingAgentName();
					findCompanyDivisionByBookAg('onchange');
					findCoordinatorByBA();
				} } 
			function findPartnerDetailsPriceing(nameId,CodeId,DivId,partnerTypeWhereClause,id,evt){
				 var keyCode = evt.which ? evt.which : evt.keyCode;  
				 if(keyCode!=9){
				var nameValue=document.getElementById(nameId).value;
				if(nameValue.length<=1){
					document.getElementById(CodeId).value="";	
					} 
				if(nameValue.length>=3){
					$.get("partnerDetailsPricingAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", 
							{partnerNameAutoCopmlete: nameValue,partnerNameId:nameId,paertnerCodeId:CodeId,autocompleteDivId:DivId,autocompleteCondition:partnerTypeWhereClause,pricingAccountLineId:id},
							function(data){
								 document.getElementById(DivId).style.display = "block";
								$("#"+DivId).html(data); 
						});	
				     }else{
					document.getElementById(DivId).style.display = "none";	
				    }
				 }else{
					 document.getElementById(DivId).style.display = "none";
				 }
				}
			 function closeMypricingDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
					document.getElementById(autocompleteDivId).style.display = "none";	
					if(document.getElementById(paertnerCodeId).value==''){
						document.getElementById(partnerNameId).value="";	
					} } 
					function copyPartnerPricingDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId,pricingAccountLineId){
						lastName=lastName.replace("~","'");
						document.getElementById(partnerNameId).value=lastName;
						document.getElementById(paertnerCodeId).value=partnercode;
						document.getElementById(autocompleteDivId).style.display = "none";	 
					}  
					function fillPayVatByVendorCode(aid) {
						fillDefaultInvoiceNumber(aid);
						<c:if test="${systemDefaultVatCalculationNew=='Y'}">
						var vatDesc=document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value;
						if(document.forms['serviceOrderForm'].elements['payPostDate'+aid].value=='' && document.forms['serviceOrderForm'].elements['payAccDate'+aid].value=='') {
						if(document.forms['serviceOrderForm'].elements['VATExclude'+aid].value!='true'){
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${trackingStatus.originAgentCode}'){
						document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.originAgentVatCode}';
						}
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${trackingStatus.destinationAgentCode}'){
							document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.destinationAgentVatCode}';
							}
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${trackingStatus.originSubAgentCode}'){
							document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.originSubAgentVatCode}';
							} 
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${trackingStatus.destinationSubAgentCode}'){
							document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.destinationSubAgentVatCode}';
							}
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${trackingStatus.forwarderCode}'){
							document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.forwarderVatCode}';
							}
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${trackingStatus.brokerCode}'){
							document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.brokerVatCode}';
							}
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${trackingStatus.networkPartnerCode}'){
							document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.networkPartnerVatCode}';
							}
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${serviceOrder.bookingAgentCode}'){
							try{
							document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.bookingAgentVatCode}';
							}catch(e){}
							}
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${billing.vendorCode}'){
							try{
							document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.vendorCodeVatCode}';
							}catch(e){}
							}
						if(document.forms['serviceOrderForm'].elements['vendorCode'+aid].value =='${billing.vendorCode1}'){
							try{
							document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value='${billing.vendorCodeVatCode1}';
							}catch(e){}
							}
						<c:if test="${empty accountLine.id}">	
					        var syspayable='${systemDefault.payableVat}';
					        if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value =='' && syspayable!=''){
					    		try{
					    		document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=syspayable;
					    		}catch(e){}
					    		}
							</c:if>
					        if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value ==''){
					        	document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value;
					        }	
					        if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value ==''){
					        	document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=vatDesc;
					        }						        
						getPayPercent(aid);
						}  }
						</c:if>
					}
					function getPayPercent(aid){ 
						<c:if test="${systemDefaultVatCalculation=='true'}">
						var payAccDate='';
						  <c:if test="${systemDefaultVatCalculationNew=='Y'}">
						  payAccDate=document.forms['serviceOrderForm'].elements['payAccDate'+aid].value;
					     </c:if>
					       if (payAccDate!='') { 
					        alert("You can not change the VAT as Sent To Acc has been already filled");
					        document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value;
					        } else{
						var relo='';
						<c:forEach var="entry" items="${payVatPercentList}" > 
						if(relo==''){
						  if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry.key}'){
						      document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value='${entry.value}';
						      calculateVatSectionAll('EXPENSE',aid,'BAS');
						      relo="yes";
						      }   }
						     </c:forEach>
						     <configByCorp:fieldVisibility componentId="accountLine.payVatGl">
						     var relo1='';
							 	<c:forEach var="entry1" items="${payVatPayGLMap}" > 
							 	if(relo1 ==''){
							 	  if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
							 	      document.forms['serviceOrderForm'].elements['payVatGl'+aid].value='${entry1.value}'; 
							 	      relo1 ="yes";
							 	      }   }
							 	     </c:forEach>
							 	    relo1='';
								 	<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
								 	if(relo1 ==''){
								 	  if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
								 	      document.forms['serviceOrderForm'].elements['qstPayVatGl'+aid].value='${entry1.value}'; 
								 	      relo1 ="yes";
								 	      }   }
								 	     </c:forEach>		 	     
						 </configByCorp:fieldVisibility> 
						     if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value==''){
					            document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
					            calculateVatSectionAll('EXPENSE',aid,'BAS');
					      }  }
					      </c:if>
						}
		function calcNetWeightKgs3(){ 
				var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value;
		     	var Q2 =document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value;
		     	Q1 = Math.round(Q1*100)/100
		        Q2 = Math.round(Q2*100)/100
				var E1='';
					if(Q1<Q2) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
				        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
					}else if(Q1 != undefined && Q2 != undefined) {
						E1=Q1-Q2;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
						var E3 = E2*2.2046;
						var E4=Math.round(E3*100)/100;
						var Q3 = Q1*2.2046;
						var Q4=Math.round(Q3*100)/100;
						var Q5 = Q2*2.2046;
						var Q6=Math.round(Q5*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=Q4;
				        document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E4;
				        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=Q6;
					}else if(Q1 == undefined && Q2 == undefined) {
						E1=0;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
						var E3 = E2*2.2046;
						var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=0;
					}else if(Q2 == undefined) {
						E1=Q1;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
						var E3 = E2*2.2046;
						var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E4;
					}else if(Q1 == undefined) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
					}
		}
		function calcNetWeightKgs4(){ 
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value;
				var Q2 =document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value; 
				Q1 = Math.round(Q1*100)/100
				   Q2 = Math.round(Q2*100)/100
				   var E1='';
					if(Q1<Q2) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
					}else if(Q1 != undefined && Q2 != undefined){	
						E1=Q1-Q2;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
						var E3 = E2*2.2046;
						var E4=Math.round(E3*100)/100;
						var Q3 = Q1*2.2046;
						var Q4=Math.round(Q3*100)/100;
						var Q5 = Q2*2.2046;
						var Q6=Math.round(Q5*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=Q4;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=Q6;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E4;
					}else if(Q1 == undefined && Q2 == undefined) {
						E1=0;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
						var E3 = E2*2.2046;
						var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=0;
					}else if(Q2 == undefined) {
						E1=Q1;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
						var E3 = E2*2.2046;
						var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E4;
					}else if(Q1 == undefined) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
					} } 
		function calcNetWeightKgs5(){
		        var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value;
				var Q2 =document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value;
				Q1 = Math.round(Q1*100)/100
				   Q2 = Math.round(Q2*100)/100
				var E1='';
					if(Q1<Q2) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
					}else if(Q1 != undefined && Q2 != undefined) {	
					     E1=Q1-Q2;
						 var E2=Math.round(E1*100)/100;
						 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
						 var E3 = E2*2.2046;
						 var E4=Math.round(E3*100)/100;
						var Q3 = Q1*2.2046;
						var Q4=Math.round(Q3*100)/100;
						var Q5 = Q2*2.2046;
						var Q6=Math.round(Q5*100)/100;
						 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
						 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=Q4;
						 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=Q6;

					}else if(Q1 == undefined && Q2 == undefined) {
						E1=0;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
						var E3 = E2*2.2046;
						 var E4=Math.round(E3*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=0;
					}else if(Q2 == undefined) {
						E1=Q1;
						var E2=Math.round(E1*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
						var E3 = E2*2.2046;
						 var E4=Math.round(E3*100)/100;
						 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=E4;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
					}else if(Q1 == undefined) {
						alert("Grossweight should be greater than Tareweight");
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
					}
			}
		function calcNetWeightLbs1(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.4536;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
					} } 
		function calcNetWeightLbs2(){
		        var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.4536;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
					} }
			function calcNetWeightLbs3(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.4536;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
					} }
			function calcNetWeightLbs4(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.4536;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
					}
			}
			function calcNetWeightLbs5(){
		        var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*0.4536;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
					}
			}

		function calcNetWeightOnlyKgs1(){
		        var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*2.2046;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
					}
			}
		function calcNetWeightOnlyKgs2(){
		        var Q1 =document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
		        Q1 = Math.round(Q1*100)/100
		        var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*2.2046;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
					}
			}
			function calcNetWeightOnlyKgs3(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value;
		        Q1 = Math.round(Q1*100)/100
		       
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*2.2046;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
					}
			}
			
			function calcNetWeightOnlyKgs4(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value;
		        Q1 = Math.round(Q1*100)/100
				var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*2.2046;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
					}
			}
			
			function calcNetWeightOnlyKgs5(){
		        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value;
		        Q1 = Math.round(Q1*100)/100
		        
		        var E1='';
					if(Q1 != undefined) {
					    var E1=Q1;
					    var E2 = Q1*2.2046;
						var E3=Math.round(E2*100)/100;
						document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E3;
					}
					else{
					document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
					}
			}
			 var digits = "0123456789";
			  var phoneNumberDelimiters = "()- ";
			  var validWorldPhoneChars = phoneNumberDelimiters + "+";
			  var minDigitsInIPhoneNumber = 10; 
			function isInteger(s)
			{   var i;
			    for (i = 0; i < s.length; i++)
			    {   
			        var c = s.charAt(i);
			        if (((c < "0") || (c > "9"))) return false;
			    }
			    return true;
			} 
			function stripCharsInBag(s, bag)
			{   var i;
			    var returnString = "";
			    for (i = 0; i < s.length; i++)
			    {   
			    
			        var c = s.charAt(i);
			        if (bag.indexOf(c) == -1) returnString += c;
			    }
			    return returnString;
			} 
			function checkInternationalPhone(strPhone){
			var s=stripCharsInBag(strPhone,validWorldPhoneChars);
			return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
			} 
			function validatePhone(targetElelemnt){
				var Phone=targetElelemnt.value;
				
				if (checkInternationalPhone(Phone)==false){
					alert("Please Enter a Valid Phone Number")
					targetElelemnt.value="";
					return false;
				}
				return true;
			 }
			 // End of function.
			 
	function onlyNumsAllowed(evt) {
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36) ; 
	}
	function onlyFloatNumsAllowed(evt) {
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==190) || (keyCode==110) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36) ; 
	}
	
	function onlyCharsAllowed(evt) {
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt) {
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)  {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    return true;
	}
	function onlyTimeFormatAllowed(evt) {
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}

function changeOriginTicketAdd() {
        var agree = confirm("Do you want to update address in corresponding Tickets? click OK to proceed or Cancel.");
	    if(agree){
		javascript:openWindow('changeOriginTicketAdd.html?id=${serviceOrder.id}&decorator=popup&popup=true&from=file',650,170) ;
		} 
		}
function changeDestinTicketAdd() {
	    var agree = confirm("Do you want to update address in corresponding Tickets? click OK to proceed or Cancel.");
	    if(agree){
		javascript:openWindow('changeDestinTicketAdd.html?id=${serviceOrder.id}&decorator=popup&popup=true&from=file',650,170) ;
		} 
}
function disabledOriginTicketBtn() {
			document.forms['serviceOrderForm'].changeOriginTicket.disabled=true;
		}
function disabledDestinTicketBtn() {
			document.forms['serviceOrderForm'].changeDestinTicket.disabled=true;
		}	
function disabledOriginTicketBtnReset()
		{	 <c:if test="${serviceOrder.job !='RLO'}">
			document.forms['serviceOrderForm'].changeOriginTicket.disabled=false;	 </c:if>
		}
function disabledDestinTicketBtnReset()
		{	<c:if test="${serviceOrder.job !='RLO'}">
			document.forms['serviceOrderForm'].changeDestinTicket.disabled=false;	</c:if>
		}	
function autoPopulate_serviceOrder_originCountry(targetElement) {
	var originCountryCode=targetElement.options[targetElement.selectedIndex].value;
	document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value=originCountryCode.substring(0,originCountryCode.indexOf(":"));
	targetElement.form.elements['serviceOrder.originCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+1,originCountryCode.length);
}
function autoPopulate_serviceOrder_destinationCountry(targetElement) {
	var destinationCountryCode=targetElement.options[targetElement.selectedIndex].value;
	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value=destinationCountryCode.substring(0,destinationCountryCode.indexOf(":"));
	targetElement.form.elements['serviceOrder.destinationCountry'].value=destinationCountryCode.substring(destinationCountryCode.indexOf(":")+1,destinationCountryCode.length);
}
function wrongValue(field, event) {
	var key, keyChar;
	if(window.event) {
		key = window.event.keyCode;
	}else if(event) {
		key = event.which;
	}
	else
		return true; 
	keyChar = String.fromCharCode(key); 
	if((key==null) || (key==0) || (key==8) ||(key== 9) || (key==13) || (key==27)) {
		window.status="";
		return true;
	}
	else {
	if((("01234567890").indexOf(keyChar)>-1)) { 
		window.status="";
		return true;
	}else { 
		window.status="It accepts only numeric value";
		alert("It accepts only numeric value");
		return false;
	} } }
function openOriginLocation() {
	var city = document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
	var country = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	var zip = document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
	var address = document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value;
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+country+','+city+','+zip+','+address);
} 
function openDestinationLocation() {
	var city = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
	var country = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	var zip = document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
	var address = document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value;
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+country+','+city+','+zip+','+address);
}
function forwardToMyMessage1(){
	 
    if(${serviceOrder.job!='RLO' && serviceOrder.job!='OFF' })  {
		document.forms['serviceOrderForm'].elements['serviceOrder.actualNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.actualGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.actualCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.estimateCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.estimateGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.estimatedNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;		
	}
	}     
function forwardToMyMessage2(){
	autoPopulate_customerFile_originCityCode();
	autoPopulate_customerFile_destinationCityCode();
	
	
	var elementsLen=document.forms['serviceOrderForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++)			{
				document.forms['serviceOrderForm'].elements[i].disabled=false;
	} 
 	if(${serviceOrder.job=='RLO' })		 	{
				document.forms['serviceOrderForm'].changeOriginTicket.disabled=true;
				document.forms['serviceOrderForm'].changeDestinTicket.disabled=true;
 	}
	var check =saveValidation('saveButton');
	if(check){
    if(${serviceOrder.job!='RLO'  && serviceOrder.job!='OFF'})		    {
		document.forms['serviceOrderForm'].elements['serviceOrder.actualNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.actualGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.actualCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.estimateCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.estimateGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;		
	document.forms['serviceOrderForm'].elements['serviceOrder.estimatedNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;		
	}
	return true;
	}else{
     return false;
		}
}	
function notExists(){
alert(" The Order has not been saved yet, please save Order to continue");
}
function resetAfterSubmit(){
var goCnt = '-'+document.forms['serviceOrderForm'].elements['submitCnt'].value * 1;
history.go(goCnt);
}
function forGST(targetElement) {
	document.forms['serviceOrderForm'].elements['serviceOrder.job'].value=targetElement.options[targetElement.selectedIndex].value;
	if(targetElement.value == 'GST' ){
		document.forms['serviceOrderForm'].elements['serviceOrder.specificParameter'].disabled = false;
	}else{
		document.forms['serviceOrderForm'].elements['serviceOrder.specificParameter'].value="";
		document.forms['serviceOrderForm'].elements['serviceOrder.specificParameter'].disabled = true;
	} }
function autoPopulate_customerFile_originCountry(targetElement) {
	var oriCountry = targetElement.value;
	var oriCountryCode = '';
	if(oriCountry == 'United States') {
		oriCountryCode = "USA";
	}
	if(oriCountry == 'India') {
		oriCountryCode = "IND";
	}
	if(oriCountry == 'Canada') {
		oriCountryCode = "CAN";
	}
	if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = false;
		if(oriCountry == 'United States'){
		document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].focus();
		}
	}else{
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = true;
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = '';
		if(oriCountry == 'Netherlands'|| oriCountry == 'Austria' ||oriCountry == 'Belgium' ||oriCountry == 'Bulgaria' ||oriCountry == 'Croatia' ||oriCountry == 'Cyprus' ||oriCountry == 'Czech Republic' ||
		oriCountry == 'Denmark' ||oriCountry == 'Estonia' ||oriCountry == 'Finland' ||oriCountry == 'France' ||oriCountry == 'Germany' ||oriCountry == 'Greece' ||oriCountry == 'Hungary' ||
		oriCountry == 'Iceland' ||oriCountry == 'Ireland' ||oriCountry == 'Italy' ||oriCountry == 'Latvia' ||oriCountry == 'Lithuania' ||oriCountry == 'Luxembourg' ||oriCountry == 'Malta' ||
		oriCountry == 'Poland' ||oriCountry == 'Portugal' ||oriCountry == 'Romania' ||oriCountry == 'Slovakia' ||oriCountry == 'Slovenia' ||oriCountry == 'Spain' ||oriCountry == 'Sweden' ||
		oriCountry == 'Turkey' ||oriCountry == 'United Kingdom' ){
			document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].focus();
		}
		autoPopulate_customerFile_originCityCode(oriCountryCode,'special');
	}
}
function autoPopulate_customerFile_destinationCountry(targetElement) {
	var dCountry = targetElement.value;
	var dCountryCode = '';
	if(dCountry == 'United States') {
		dCountryCode = "USA";
	}
	if(dCountry == 'India') {
		dCountryCode = "IND";
	}
	if(dCountry == 'Canada') {
		dCountryCode = "CAN";
	}
	if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = false;
		if(dCountry == 'United States'){
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].focus();
		}
	}else{
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = true;
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = '';
		if(dCountry== 'Netherlands'|| dCountry== 'Austria' ||dCountry== 'Belgium' ||dCountry== 'Bulgaria' ||dCountry== 'Croatia' ||dCountry== 'Cyprus' ||dCountry== 'Czech Republic' ||
		dCountry== 'Denmark' ||dCountry== 'Estonia' ||dCountry== 'Finland' ||dCountry== 'France' ||dCountry== 'Germany' ||dCountry== 'Greece' ||dCountry== 'Hungary' ||
		dCountry== 'Iceland' ||dCountry== 'Ireland' ||dCountry== 'Italy' ||dCountry== 'Latvia' ||dCountry== 'Lithuania' ||dCountry== 'Luxembourg' ||dCountry== 'Malta' ||
		dCountry== 'Poland' ||dCountry== 'Portugal' ||dCountry== 'Romania' ||dCountry== 'Slovakia' ||dCountry== 'Slovenia' ||dCountry== 'Spain' ||dCountry== 'Sweden' ||
		dCountry== 'Turkey' ||dCountry== 'United Kingdom' ){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].focus();
		}
		autoPopulate_customerFile_destinationCityCode(dCountryCode,'special');
	}
}
function autoPopulate_customerFile_destinationCityCode(targetElement) {
	if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value != ''){
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value+', '+document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
	}
	if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value == ''){
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
	}
		}
function autoPopulate_customerFile_originCityCode(targetElement) {
	if(document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value != ''){
		document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value+', '+document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
	}
	if(document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value == ''){
		document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
	}
	}
function autoFilter_PartnerType(targetElement) {
	var partnerType=targetElement.options[targetElement.selectedIndex].value; 
	var el = document.getElementById('hid');
	if(partnerType == 'AUTO' || partnerType == 'HHG/A' || partnerType == 'BOAT'){
	el.style.visibility = 'visible';
	}else{
	el.style.visibility = 'collapse';
	} }
function openBookingAgentPopWindow(){
<configByCorp:fieldVisibility componentId="component.field.vanline">
	var job =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
    if(job=='DOM'||job=='UVL'||job=='MVL'){
    	if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value!="yes"){ 
			var first = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
			var last = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
			javascript:openWindow('destinationPartnersVanline.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=serviceOrder.bookingAgentVanlineCode&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode',1000,550);
		}
		else if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){
			var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
			javascript:openWindow('bookingAgentCompanyDivisionVanline.html?partnerType=AG&companyDivision='+companyDivision+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=serviceOrder.bookingAgentVanlineCode&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode',1000,550);
		}
    }else{
		if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value!="yes"){ 
			var first = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
			var last = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
			javascript:openWindow('bookingAgentPopup.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
		}
		else if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){
			var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
			javascript:openWindow('bookingAgentCompanyDivision.html?partnerType=AG&companyDivision='+companyDivision+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
		}
	}
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
	if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value!="yes"){ 
			var first = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
			var last = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
			javascript:openWindow('bookingAgentPopup.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
	}
	else if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){
		var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
		javascript:openWindow('bookingAgentCompanyDivision.html?partnerType=AG&companyDivision='+companyDivision+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
	}
</configByCorp:fieldVisibility>			
}
function openBookingAgentBookingAgentSetPopWindow(){
	var first = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
	var last = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
	javascript:openWindow('bookingAgentPopupWithBookingAgentSet.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
	} 
function displayStatusReason(targetElement)  {
  var statusValue=targetElement.value;
  var serviceOrderStatus1 = document.forms['serviceOrderForm'].elements['serviceOrder.status1'].value;
  if(serviceOrderStatus1 == 'NEW' && (statusValue == '2CLS' || statusValue == 'CLSD'))  {
   		alert("You can not change status NEW to Closed or Ready To Closed");
   		document.forms['serviceOrderForm'].elements['serviceOrder.status'].value = document.forms['serviceOrderForm'].elements['serviceOrder.status1'].value;
   }
  if(statusValue=='CNCL') {
     document.getElementById("reasonHid").style.display="block";
     document.getElementById("reasonHid1").style.display="block";
   } else  {
    document.getElementById("reasonHid").style.display="none";
    document.getElementById("reasonHid1").style.display="none";
  } 
  if(serviceOrderStatus=='HOLD'){
   		 document.getElementById("reasonHid2").style.display="block";
         document.getElementById("reasonHid3").style.visibility="visible";
   	}else{ 
   		document.getElementById("reasonHid2").style.display="none";
     	document.getElementById("reasonHid3").style.visibility="hidden"; 
  }  }
// End of function

function findTicketStatus() { 
 var serviceOrderStatus = document.forms['serviceOrderForm'].elements['serviceOrder.status'].value;
      var oldStatus=document.forms['serviceOrderForm'].elements['oldStatus'].value; 
     if((serviceOrderStatus=='CNCL' || serviceOrderStatus=='TCNL') && oldStatus!='CNCL' && oldStatus!='CLSD' && oldStatus!='TCNL' ){
     var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     var url="findTicketStatus.html?ajax=1&decorator=simple&popup=true&sid="+encodeURI(serviceOrderId);
     http99.open("GET", url, true);
     http99.onreadystatechange = handleHttpResponse9999;
     http99.send(null);
     }else {
     if((serviceOrderStatus=='CNCL' || serviceOrderStatus=='TCNL')&& oldStatus=='CLSD'){ 
                 	<c:if test="${closedCompanyDivision=='Y'}">
  					findClosedCompanyDivision('${serviceOrder.companyDivision}',oldStatus,status);
  				</c:if>
  				<c:if test="${closedCompanyDivision!='Y'}"> 
  				   if(status !='REOP'){
  					alert("You can select only Reopen status");
			 		document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=oldStatus;
			 		}
				</c:if>
     		}
     else{
     cheackStatusReason();
     }
     } } 
function handleHttpResponse9999()  {
		 var oldStatus=document.forms['serviceOrderForm'].elements['oldStatus'].value; 	
             if (http99.readyState == 4)  {
                var results = http99.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results >0)   {	
                	alert("You can't change current status to cancel as tickets are still open");
		            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=oldStatus; 
		            cheackStatusReason();
                }
                else { 
                cheackStatusReason();
                }   }  }	
function cheackStatusReason() {
      var serviceOrderStatus = document.forms['serviceOrderForm'].elements['serviceOrder.status'].value; 
      var oldStatus=document.forms['serviceOrderForm'].elements['oldStatus'].value; 
     if((serviceOrderStatus=='CNCL' || serviceOrderStatus=='TCNL') && oldStatus!='CNCL' && oldStatus!='TCNL'){
     var agree = confirm("Are you sure you want to cancel this Service Order?");
     if(agree){ 
     document.getElementById("reasonHid").style.display="block";
	 document.getElementById("reasonHid1").style.display="block";
	 autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
	 }else{document.getElementById("reasonHid").style.display="none";
         	document.getElementById("reasonHid1").style.display="none";
         	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';}
     }  else{
      if(serviceOrderStatus=='CNCL' || serviceOrderStatus=='TCNL'){
      document.getElementById("reasonHid").style.display="block";
	  document.getElementById("reasonHid1").style.display="block";
      }else {
            document.getElementById("reasonHid").style.display="none";
         	document.getElementById("reasonHid1").style.display="none";
         	}
         	}
      if(serviceOrderStatus=='HOLD'){
       		 document.getElementById("reasonHid2").style.display="block";
	         document.getElementById("reasonHid3").style.visibility="visible";
       	}else{ 
       		document.getElementById("reasonHid2").style.display="none";
         	document.getElementById("reasonHid3").style.visibility="hidden";
         	document.forms['serviceOrderForm'].elements['serviceOrder.nextCheckOn'].value=""; 
      } } 
function cheackStatusReasonLoad(){
      var serviceOrderStatus = document.forms['serviceOrderForm'].elements['serviceOrder.status'].value; 
        if(serviceOrderStatus=='CNCL'  || serviceOrderStatus=='TCNL'){
	         document.getElementById("reasonHid").style.display="block";
	         document.getElementById("reasonHid1").style.display="block";
       	}else{ 
         	document.getElementById("reasonHid").style.display="none";
         	document.getElementById("reasonHid1").style.display="none";
      }
      if(serviceOrderStatus=='HOLD'){
       		 document.getElementById("reasonHid2").style.display="block";
	         document.getElementById("reasonHid3").style.visibility="visible";
       	}else{ 
       		document.getElementById("reasonHid2").style.display="none";
         	document.getElementById("reasonHid3").style.visibility="hidden";
      } }
// End of function
function cheackStatusReasonReset(){
      var serviceOrderStatus = document.forms['serviceOrderForm'].elements['oldStatus'].value; 
        if(serviceOrderStatus=='CNCL'){
	         document.getElementById("reasonHid").style.display="block";
	         document.getElementById("reasonHid1").style.display="block";
       	}else{ 
         	document.getElementById("reasonHid").style.display="none";
         	document.getElementById("reasonHid1").style.display="none";
      }
      if(serviceOrderStatus=='HOLD'){
       		 document.getElementById("reasonHid2").style.display="block";
	         document.getElementById("reasonHid3").style.visibility="visible";
       	}else{ 
       		document.getElementById("reasonHid2").style.display="none";
         	document.getElementById("reasonHid3").style.visibility="hidden"; 
      } }
function addServiceOrder(){
	 var check =saveValidation('Add');
	   var listSize='${shipSize}';
	   if(!check){
		   return false; 
	   }else if(listSize == 99){
			alert('Maximum limit of service order is 99 for a customer.');
		  return false;  
		}
	var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
	var url = "editServiceOrder.html?cid="+${customerFile.id};	 
	document.forms['serviceOrderForm'].action= url;	 
	document.forms['serviceOrderForm'].submit();
	return true;
}

function addCopyServiceOrder(){
	var check =saveValidation('Add');
	   var listSize='${shipSize}';
	   if(!check){
		   return false; 
	   }else if(listSize == 99){
		alert('Maximum limit of service order is 99 for a customer.');
	  return false;  
		}
 var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
 var url = "addAndCopyServiceOrder.html?cid=${customerFile.id}&sid=${serviceOrder.id}"; 
	document.forms['serviceOrderForm'].action= url;	 
document.forms['serviceOrderForm'].submit();
return true;
} 
function addCopyForwardingServiceOrder(){
var listSize='${shipSize}';
if(listSize == 99){
	alert('Maximum limit of service order is 99 for a customer.');
 return false;  
}
var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
var url = "addAndCopyServiceOrder.html?cid=${customerFile.id}&sid=${serviceOrder.id}&forwarding=fwd"; 
document.forms['serviceOrderForm'].action= url;	 
document.forms['serviceOrderForm'].submit();
return true;
} 
function checkJobType(targetElement){
	try{
	var str="${systemDefaultmiscVl}";
	var n=str.indexOf(targetElement.value);
	 if(targetElement.value.length == 0){
			document.getElementById("hidRegNum").style.display="none";
	      	document.getElementById("hidVanLine").style.display="none";
		}else if(n >= 0 ){
			document.getElementById("hidRegNum").style.display="block";
	   		document.getElementById("hidVanLine").style.display="block";
		}else {
			document.getElementById("hidRegNum").style.display="none";
	      	document.getElementById("hidVanLine").style.display="none";
		}
	}catch(e){}} 
function checkJobTypeOnLoad(){   
    var job='${serviceOrder.job}';
    var str="${systemDefaultmiscVl}";
	var n=str.indexOf(job);
	 if(job.length == 0){
			document.getElementById("hidRegNum").style.display="none";
	      	document.getElementById("hidVanLine").style.display="none";
		}else if(n >= 0 ){
			document.getElementById("hidRegNum").style.display="block";
	   		document.getElementById("hidVanLine").style.display="block";
		}else {
			document.getElementById("hidRegNum").style.display="none";
	      	document.getElementById("hidVanLine").style.display="none";
		} } 
 function checkJobTypeOnReset(){
	 try{
    var job =document.forms['serviceOrderForm'].elements['jobtypeSO'].value; 
    var str="${systemDefaultmiscVl}";
	var n=str.indexOf(job);
	 if(job.length == 0){
			document.getElementById("hidRegNum").style.display="none";
	      	document.getElementById("hidVanLine").style.display="none";
		}else if(n >= 0 ){
			document.getElementById("hidRegNum").style.display="block";
	   		document.getElementById("hidVanLine").style.display="block";
		}else {
			document.getElementById("hidRegNum").style.display="none";
	      	document.getElementById("hidVanLine").style.display="none";
		}}catch(e){}  } 
// Function for getting booking agent code through magnifying glass.
  function findJobSeq() { 
     var registrationNumber= document.forms['serviceOrderForm'].elements['serviceOrder.registrationNumber'].value
     var bookingAgentCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value
     var SOjob = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
     registrationNumber =registrationNumber.trim();
     bookingAgentCode =bookingAgentCode.trim();
     if(registrationNumber==''&& bookingAgentCode=='') {
        alert("Please select Booking Agent Code");
     }
     if(registrationNumber==''&& bookingAgentCode!='')  { 
     var url="getJobSeqNumber.html?ajax=1&decorator=simple&popup=true&bookingAgentCode="+encodeURI(bookingAgentCode)+"&jobType="+SOjob;
     http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponse14; 
     http5.send(null); 
     }
  } 
  function handleHttpResponse14() {
             if (http5.readyState == 4) {
               var results = http5.responseText
               results = results.trim();
               var res = results.split(",");  
               if(results.length >= 2){ 
               if(res[1]=='')  {
                  res[1]='00001';
               } 
               var mydate=new Date();
		       var daym;
		       var year1 = mydate.getFullYear();
		       var str = ""+year1;
		       var yearLastChar=str.substring( str.length-1, str.length);
                if(res[0]!='') {
                  document.forms['serviceOrderForm'].elements['serviceOrder.registrationNumber'].value=res[0]+'-'+ res[1]+'-'+yearLastChar;
                  document.forms['serviceOrderForm'].elements['vanlineseqNum'].value=res[1];
                  document.forms['serviceOrderForm'].elements['vanlineCodeForRef'].value=res[0]; 
                } } } }
  function validateRegistrationNumber(){
	  <c:if test="${empty sysDefSequenceNum}">
     var regNum= document.forms['serviceOrderForm'].elements['serviceOrder.registrationNumber'].value;
     var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
 	 regNum =regNum.trim();
     job =job.trim();
     if(job=='UVL' || job=='MVL')
     {
     regNum =regNum.trim();
     var url="validateRegistrationNumber.html?ajax=1&decorator=simple&popup=true&regNum="+encodeURI(regNum);
     http6.open("GET", url, true); 
     http6.onreadystatechange = handleHttpResponse74; 
     http6.send(null); 
     }
     </c:if>
  }
  function validateRegistraionNumberAC(x){
	  var corpID= document.forms['serviceOrderForm'].elements['serviceOrder.corpID'].value;
	  if(corpID=='JKMS') {
		  document.forms['serviceOrderForm'].elements['regUpdate'].value="yes";
	  }
	  <c:if test="${empty sysDefSequenceNum}">
     var  regNum= document.getElementById(x).value;
      var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
 	  regNum =regNum.trim();
      job =job.trim();
      if(job=='UVL' || job=='MVL') {
         if((regNum == null)||(regNum =="")){ }
         else{
         var checkFirst =regNum.indexOf("-");
         var temp=regNum.replace("-","~");
         var checkLast  =temp.indexOf("-");         
         if(regNum.charAt(0).toUpperCase()=="U"||regNum.charAt(0).toUpperCase()=="M"){
              if(checkFirst == 5 &&  checkLast == 11 && regNum.length >= 13 ){
              }else{
              alert('Registration Number format is incorrect');
             }         } 
         }  }
		</c:if>
       }
    function handleHttpResponse74() {
             if (http6.readyState == 4) {
               var results = http6.responseText
               results = results.trim(); 
               if(results > 0){ 
                alert("Reg # is already assigned Please write another");  
                  document.forms['serviceOrderForm'].elements['serviceOrder.registrationNumber'].value='';
                } else {
              }
     }  }
     function checkStatus() {
     	var status=document.forms['serviceOrderForm'].elements['serviceOrder.status'].value; 
     	var oldStatus=document.forms['serviceOrderForm'].elements['oldStatus'].value;
     	var oldStatusNumber =document.forms['serviceOrderForm'].elements['oldStatusNumber'].value;
     	var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
     	<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
			<c:set var="closedCompanyDivision" value="Y" />
		</configByCorp:fieldVisibility>
     	if((jobType!='STO')&& (jobType!='TPS')&& (jobType!='STF')&&(jobType!='STL')){
     	if(status=='CNCL' || status=='HOLD' || status=='REOP' || status=='2CLS'|| status=='CLSD' || status=='TCNL'){
     	if(status=='CLSD' || status=='2CLS'){
     	if(oldStatus=='2CLS'||oldStatus=='CNCL'||oldStatus=='DWNLD' || oldStatus=='TCNL'){
     	var agree = confirm("Are you sure you want to close this Service Order?");
         if(agree){
         autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
          if(status=='2CLS'){
          document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
          else if(status=='CLSD'){
          document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
        }else{
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          } 
     	}
     	else if(oldStatus =='CLSD'){
     	         if(status !='REOP'){
	  				alert("You can select only Reopen status");
    			 	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=oldStatus;
    			 	}
     	}
     	else if(oldStatus!='CNCL' && oldStatus!='DWNLD' && jobType!='RLO' ){
     	<c:if test="${not empty serviceOrder.id}">
     	if(document.forms['serviceOrderForm'].elements['billing.billCompleteA'].value!='A' && document.forms['serviceOrderForm'].elements['billing.billComplete'].value=='' && status =='CLSD' ){ 
         		alert('Job cannot be closed as billing has not been completed');
                document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	}else{
     	  if(document.forms['serviceOrderForm'].elements['claimCount'].value!='0'){
     	   alert("There are open claim against the Service Order.") 
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}'; 
     	} else {
     	if(document.forms['serviceOrderForm'].elements['trackingStatus.deliveryA'].value==''){
     	var agree = confirm("The service order has not been delivered, do you want to close this Service Order?");
          if(agree){
          autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
            if(status=='2CLS'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
            }
            else if(status=='CLSD'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
          }else{
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          }  
     	} else { 
     	if(document.forms['serviceOrderForm'].elements['trackingStatus.deliveryA'].value!=''){ 
     	var date1 = document.forms['serviceOrderForm'].elements['trackingStatus.deliveryA'].value;	 
        var mySplitResult = date1.split("-");
        var day = mySplitResult[0];
        var month = mySplitResult[1];
        var year = mySplitResult[2]; 
        if(month == 'Jan') {
         month = "01";
        }else if(month == 'Feb') {
          month = "02";
        }else if(month == 'Mar') {
         month = "03"
        }else if(month == 'Apr') {
         month = "04"
        }else if(month == 'May') {
          month = "05"
        }else if(month == 'Jun') {
          month = "06"
        }else if(month == 'Jul') {
          month = "07"
        }else if(month == 'Aug') {
         month = "08"
        }else if(month == 'Sep') {
         month = "09"
        }else if(month == 'Oct')  {
         month = "10"
        }else if(month == 'Nov') {
         month = "11"
        }else if(month == 'Dec') {
         month = "12";
        }
        year="20"+year; 
        var finalDate = month+"-"+day+"-"+year;  
        date1 = finalDate.split("-"); 
        var deliveryADate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);  
        var sysDate = new Date();  
        var daysApart = Math.round((deliveryADate-sysDate)/86400000); 
     	 if(document.forms['serviceOrderForm'].elements['trackingStatus.deliveryA'].value!='' && daysApart>0){
     	 var agree = confirm("It's been "+daysApart+ "days after delivery,do you want to close this Service Order?");
          if(agree){
          autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
            if(status=='2CLS'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
            }
            else if(status=='CLSD'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
          }else{
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          } 
     	
     	}     	}     	}     	}     	}
     	</c:if>
     	<c:if test="${empty serviceOrder.id}">
     	var agree = confirm("This is new service order, do you  want to cloase this service Order?");
          if(agree){
          autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
            if(status=='2CLS'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
            }
            else if(status=='CLSD'){
            document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
          }
          }else{
           document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
           document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
          }  
     	</c:if>
     	} else if(oldStatus!='CNCL' && oldStatus!='DWNLD' && jobType=='RLO'){
     		<c:if test="${not empty serviceOrder.id}">
     		if(document.forms['serviceOrderForm'].elements['billing.billCompleteA'].value!='A' && document.forms['serviceOrderForm'].elements['billing.billComplete'].value=='' && status =='CLSD'){
     			alert('Job cannot be closed as billing has not been completed');
                document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
         	}else if(document.forms['serviceOrderForm'].elements['billing.billCompleteA'].value!='A' && document.forms['serviceOrderForm'].elements['dspDetails.serviceCompleteDate'].value==''){
         		alert('Job cannot be closed as Service complete date is not present');
                document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
         	}
     		</c:if>
     	}  
     	}
     	if(status=='HOLD') {
        if(eval(oldStatusNumber)<10){
        document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
        }
     	if(!(eval(oldStatusNumber)<10)){
     		var corpID= document.forms['serviceOrderForm'].elements['serviceOrder.corpID'].value;
     	if(corpID=='VOER') {
     		if(oldStatus=='TRNS'){
     			document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
     		}else{
     			alert("You can't change current status to HOLD");
     	     	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     		}
     	}else{
     	alert("You can't change current status to HOLD");
     	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	}
     	}
     	}
     	if(status=='REOP'){
     	if(oldStatus=='HOLD'||oldStatus=='CNCL'||oldStatus=='2CLS' || oldStatus=='TCNL'){
     	<c:if test='${customerFile.status == "CNCL" || customerFile.status == "CLOSED" || customerFile.status == "CANCEL"}'>  
     	alert("Cannot change current status to Reopen as the Customer File is not active.");
     	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	</c:if>
     	<c:if test='${customerFile.status != "CNCL" && customerFile.status != "CLOSED" && customerFile.status != "CANCEL"}'> 
     	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status; 
     	</c:if>
     	} 
     	else if (oldStatus!='HOLD' && oldStatus!='CNCL' && oldStatus!='2CLS'){
     		if(oldStatus=='CLSD'){
     			<c:if test="${closedCompanyDivision=='Y'}">
	  				findClosedCompanyDivision('${serviceOrder.companyDivision}',oldStatus,status);
	  			</c:if>
	  			<c:if test="${closedCompanyDivision!='Y'}">
	  			if(status !='REOP'){
	  				alert("You can select only Reopen status");
    			 	document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=oldStatus;
    			 	}
  				</c:if>
     		}else{
     	  		alert("You can't change current status to Reopen");
     	  		document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
     	} } } }
     	else { 
     	if(status =='NEW' && oldStatus == 'DWNLD'){ 
     		autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
     		document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=status;
     		document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=1;
     	} else {
     		if(oldStatus=='CLSD'){
     			<c:if test="${closedCompanyDivision=='Y'}">
  					findClosedCompanyDivision('${serviceOrder.companyDivision}',oldStatus,status);
  				</c:if>
  				<c:if test="${closedCompanyDivision!='Y'}">
  				 if(status !='REOP'){
  					alert("You can select only Reopen status");
			 		document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=oldStatus;
			 		}
				</c:if>
     		}else{
     			alert("You can select only Cancelled, To cancel, Hold, Reopen, Ready To Close and Closed status");
     			document.forms['serviceOrderForm'].elements['serviceOrder.status'].value='${serviceOrder.status}';
        		document.forms['serviceOrderForm'].elements['serviceOrder.statusNumber'].value=oldStatusNumber;
     	}
        }}
     	}else if((jobType=='STO')|| (jobType=='TPS')|| (jobType=='STF')||(jobType=='STL')){
     		if(oldStatus=='CLSD'){
     			<c:if test="${closedCompanyDivision=='Y'}">
  					findClosedCompanyDivision('${serviceOrder.companyDivision}',oldStatus,status);
  				</c:if>
     		}
	     	if(status !='CNCL'){
	     	 	autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
	     	}
     	}
       findTicketStatus();
     }
     
     function findClosedCompanyDivision(companyCode,oldStatus,status){ 
		 new Ajax.Request('/redsky/findClosedCompanyDivisionAjax.html?ajax=1&companyCode='+companyCode+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      if(response.trim()=="false"){
				      if(status !='REOP'){
				    	     alert("You can select only Reopen status");
			     			 document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=oldStatus;
			     			 }
				      }else{
				    	  alert('You can not change Status, As Company Division is closed.')
				    	  document.forms['serviceOrderForm'].elements['serviceOrder.status'].value = oldStatus;
				      }
				    },
				    onFailure: function(){ 
					    }
				  });
	}
     
  function  changebAgentCheckedBy() {
  document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCheckedBy'].value="";
  }
    function sendEmail(target) {
   		var originEmail = target;
   		var subject = 'S/O# ${serviceOrder.shipNumber}';
   		subject = subject+" ${serviceOrder.firstName}";
   		subject = subject+" ${serviceOrder.lastName}";   		
		var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close();  
		}
		function copyToEmail1(target) {
			if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail'].value=='') {
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail'].value=target;
			} } 
		function copyToEmail2(target) {
			if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail2'].value=='') {
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail2'].value=target;
			} }
// End of Function 
    function chkSelect() {
		if (checkFloat('serviceOrderForm','miscellaneous.entitleGrossWeight','Invalid data in entitleGrossWeight') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.estimateGrossWeight','Invalid data in estimateGrossWeight') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.actualGrossWeight','Invalid data in actualGrossWeight') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableGrossWeight','Invalid data in chargeableGrossWeight') == false)  {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghGross','Invalid data in grossreweight') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.entitleTareWeight','Invalid data in entitleTareWeight') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.estimateTareWeight','Invalid data in estimateTareWeight') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.actualTareWeight','Invalid data in actualTareWeight') == false)  {
              document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableTareWeight','Invalid data in chargeableTareWeight') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghTare','Invalid data in reweighttare') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.entitleNetWeight','Invalid data in entitleNetWeight') == false)  {
              document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.estimatedNetWeight','Invalid data in estimatedNetWeight') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.actualNetWeight','Invalid data in actualNetWeight') == false)  {
              document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableNetWeight','Invalid data in chargeableNetWeight') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghNet','Invalid data in rwghNet') == false)  {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.entitleCubicFeet','Invalid data in entitleCubicFeet') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.estimateCubicFeet','Invalid data in estimateCubicFeet') == false)  {
              document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.actualCubicFeet','Invalid data in actualCubicFeet') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableCubicFeet','Invalid data in chargeableCubicFeet') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghCubicFeet','Invalid data in rwghCubicFeet') == false)  {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.netEntitleCubicFeet','Invalid data in netEntitleCubicFeet') == false)  {
              document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.netEstimateCubicFeet','Invalid data in netEstimateCubicFeet') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.netActualCubicFeet','Invalid data in netActualCubicFeet') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.chargeableNetCubicFeet','Invalid data in chargeableNetCubicFeet') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].focus();
              return false
           }
           if (checkFloat('serviceOrderForm','miscellaneous.rwghNetCubicFeet','Invalid data in rwghNetCubicFeet') == false) {
              document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].focus();
              return false
           }
	} 
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceOrderForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   } 
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceOrderForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   } 
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4) {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editServiceOrderUpdate.html?id='+results;
             }   }     
function findCustomerOtherSO(position) {
 var sid=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	  function goToUrl(id) {
		location.href = "editServiceOrderUpdate.html?id="+id;
	}
function checkTransferDate(){
	var companyDivision=document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	var dbCompanyDivision='${serviceOrder.companyDivision}';
	var msgAlertFlag = "0";
	if(dbCompanyDivision!= ""){
	if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){
	  alert("You cannot change company division as accounting data has been transferred to accounting system.");
               document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value='${serviceOrder.companyDivision}';
               msgAlertFlag = "1";
	} }
	if(companyDivision!="" && msgAlertFlag == "0"){
		<c:if test="${not empty serviceOrder.id}">
		  	<c:if test="${cmmDmmFlag}">
	      	var linkedFile = '${trackingStatus.soNetworkGroup}';
		        if((linkedFile==true || linkedFile=='true') && (companyDivision!=dbCompanyDivision)){
		        	var agree = confirm("Changing of Company Division may leads to change of contract, Do you Agree ?");
		    			if(agree){
		    			}else{
		    				document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value = '${serviceOrder.companyDivision}';
		    			}
		        }
	  		</c:if>
		</c:if>
	}
}
	function onLoadcheckTransferDate(){
	if(document.forms['serviceOrderForm'].elements['transferDateFlag'].value=="yes"){
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].readOnly=true;
        document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].className = 'input-textUpper';
        document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].className = 'input-textUpper'; 
        document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].readOnly = true;
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].className = 'input-textUpper';
		}
	}
function check(targetElement){
  var t = targetElement.value;
  if(t=='lbscft'){
  document.forms['serviceOrderForm'].elements['miscellaneous.unit1'].value='Lbs';
  document.forms['serviceOrderForm'].elements['miscellaneous.unit2'].value='Cft';
  }
  if(t=='kgscbm'){
  document.forms['serviceOrderForm'].elements['miscellaneous.unit1'].value='Kgs';
  document.forms['serviceOrderForm'].elements['miscellaneous.unit2'].value='Cbm';
  }
} 
 function openStandardAddressesPopWindow(){
 	var jobType =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
	javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=serviceOrder.originMobile&fld_ninthDescription=serviceOrder.originCity&fld_eigthDescription=serviceOrder.originFax&fld_seventhDescription=serviceOrder.originHomePhone&fld_sixthDescription=serviceOrder.originDayPhone&fld_fifthDescription=serviceOrder.originZip&fld_fourthDescription=stdAddOriginState&fld_thirdDescription=serviceOrder.originCountry&fld_secondDescription=serviceOrder.originAddressLine3&fld_description=serviceOrder.originAddressLine2&fld_code=serviceOrder.originAddressLine1');
document.forms['serviceOrderForm'].elements['checkConditionForOriginDestin'].value="originAddSection";
}
function openStandardAddressesDestinationPopWindow(){
 	var jobType =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=serviceOrder.destinationMobile&fld_ninthDescription=serviceOrder.destinationCity&fld_eigthDescription=serviceOrder.destinationFax&fld_seventhDescription=serviceOrder.destinationHomePhone&fld_sixthDescription=serviceOrder.destinationDayPhone&fld_fifthDescription=serviceOrder.destinationZip&fld_fourthDescription=stdAddDestinState&fld_thirdDescription=serviceOrder.destinationCountry&fld_secondDescription=serviceOrder.destinationAddressLine3&fld_description=serviceOrder.destinationAddressLine2&fld_code=serviceOrder.destinationAddressLine1');
document.forms['serviceOrderForm'].elements['checkConditionForOriginDestin'].value="destinAddSection";
} 
 function enableState(){
var id=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
var AddSection = document.forms['serviceOrderForm'].elements['checkConditionForOriginDestin'].value;
if(AddSection=='originAddSection'){
if(id!=''){
document.forms['serviceOrderForm'].changeOriginTicket.disabled=true;
}
	var originCountry = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value
	getOriginCountryCode();
	 var countryList = JSON.parse('${jsonCountryCodeText}');
	 var countryCode =getKeyByValue(countryList,originCountry);
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse12;
     http9.send(null);
}
if(AddSection=='destinAddSection'){
if(id!=''){
document.forms['serviceOrderForm'].changeDestinTicket.disabled=true;
}
	var country = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value
	getDestinationCountryCode();
	var countryList = JSON.parse('${jsonCountryCodeText}');
	 var countryCode =getKeyByValue(countryList,country);
var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse11;
     http9.send(null);
}
}
function handleHttpResponse11(){ 
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
                var stdAddDestinState=document.forms['serviceOrderForm'].elements['stdAddDestinState'].value;
                targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${serviceOrder.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					} } }
					document.getElementById("destinationState").value = stdAddDestinState;
					if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value != ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value+', '+document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
		}
		if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
		} 
        }dstateMandatory();
}  
function handleHttpResponse12(){ 
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
                var stdAddOriginState=document.forms['serviceOrderForm'].elements['stdAddOriginState'].value;
                targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${serviceOrder.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					} } }
					document.getElementById("originState").value = stdAddOriginState;
        if(document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value != ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value+', '+document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
		}
		if(document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		}
        }ostateMandatory();zipCode();
}  
function getVanlineCode(targetField, position) {
	var partnerCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
	<configByCorp:fieldVisibility componentId="component.field.vanline">
		if(partnerCode != ''){
			var job =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
    		if(job=='DOM'||job=='UVL'||job=='MVL'){
				var partnerCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
			 	var url="findVanLineList.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode) +"&agentType="+ encodeURI(targetField);
			  	position = document.getElementById('navigations');
			  	ajax_showTooltip(url,position);
			}  	
		}
	</configByCorp:fieldVisibility>	
	setTimeout("getBigPartnerAlert(position,'noteNavigations')",2000);
}
function goToUrlVanline(id, targetField){
	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].value = id;
} 
function getVanlineAgent(targetField){
	var vanlineCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].value;
	if(vanlineCode != ''){
		var url="findVanLineAgent.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vanlineCode);
    	http9.open("GET", url, true);
    	http9.onreadystatechange = function() {handleHttpResponseVanlineAgent(targetField);};
    	http9.send(null);
	} }
function handleHttpResponseVanlineAgent(targetField){
	if (http9.readyState == 4){
		var results = http9.responseText
        results = results.trim();
        if(results.length>1){
	        res = results.split("@");
	        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'];
	        targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res.length == 2){
					stateVal = res[i].split("~");
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = '';
				}else{
					stateVal = res[i].split("~");
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value = stateVal[0];
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
				}
			}
        }else{
        	alert("Invalid vanline code");
        	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentVanlineCode'].value='';
       	} } } 
var http50 = getHTTPObject50();
	function getHTTPObject50() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http51 = getHTTPObject51();
	function getHTTPObject51() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http52 = getHTTPObject52();
	function getHTTPObject52() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http54 = getHTTPObject54();
	function getHTTPObject54() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http77 = getHTTPObject77();
var http777 = getHTTPObject77();
function getHTTPObject77() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getNewCoordAndSale(){
	 document.getElementById("coordinator").value='';   
    var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
    var soNetworkGrp =false;
    var accNetworkGrp = false;
    var checkUTSI="${company.UTSI}";
    <c:if test="${not empty serviceOrder.id}">
        soNetworkGrp= ${trackingStatus.soNetworkGroup};
        accNetworkGrp= ${trackingStatus.accNetworkGroup};
    </c:if>
    var checkSoFlag = true;
    var bookAgCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
   if(job!=''){
     	var url = "findCoord.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job)+"&soNetworkGrp="+encodeURI(soNetworkGrp)+"&accNetworkGrp="+encodeURI(accNetworkGrp)+"&checkSoFlag="+encodeURI(checkSoFlag)+"&bookAgCode="+encodeURI(bookAgCode)+"&checkUTSI="+encodeURI(checkUTSI);
     	http50.open("GET", url, true);
     	http50.onreadystatechange = handleHttpResponse50;
     	http50.send(null);
    }  }
function handleHttpResponse50(){ 
		if (http50.readyState == 4){
                var results = http50.responseText
                results = results.trim();
                res = results.split("~");
              	targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("coordinator").value = '${serviceOrder.coordinator}';
					getConsultant();
        } }
function getConsultant(){  
	    var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	    var soNetworkGrp =false;
	    var accNetworkGrp = false;
	    var checkUTSI="${company.UTSI}";
	    <c:if test="${not empty serviceOrder.id}">
	        soNetworkGrp= ${trackingStatus.soNetworkGroup};
	        accNetworkGrp= ${trackingStatus.accNetworkGroup};
	    </c:if>
	    var checkSoFlag = true;
	    var bookAgCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
        
   	if(job!=''){
     	var url = "findSale.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job)+"&soNetworkGrp="+encodeURI(soNetworkGrp)+"&accNetworkGrp="+encodeURI(accNetworkGrp)+"&checkSoFlag="+encodeURI(checkSoFlag)+"&bookAgCode="+encodeURI(bookAgCode)+"&checkUTSI="+encodeURI(checkUTSI);
     	http52.open("GET", url, true);
     	http52.onreadystatechange = handleHttpResponse52;
     	http52.send(null);
    } }
function handleHttpResponse52(){
		if (http52.readyState == 4){ 
                var results = http52.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++) {
					try{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].value = stateVal[0];
					}
					}catch(e){}
					}
					document.getElementById("salesMan").value = '${serviceOrder.salesMan}';
        }  }
  function fillCheckJob(temp){ 
  document.forms['serviceOrderForm'].elements['jobtypeCheck'].value=temp.value;
  } 
   function findService(){
	try{     
	var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	<c:if test="${not empty serviceOrder.id}">
	  <c:if test="${cmmDmmFlag}">
        var linkedFile = '${trackingStatus.soNetworkGroup}';
        var dbJob = '${serviceOrder.job}';
	        if((linkedFile==true || linkedFile=='true') && (job!=dbJob)){
	        	var agree = confirm("Changing of Job Type may leads to change of contract, Do you Agree ?");
	    			if(agree){
	    			}else{
	    				document.forms['serviceOrderForm'].elements['serviceOrder.job'].value = '${serviceOrder.job}';
	    			}
	        }
    	</c:if>
	</c:if>
	var mode = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
    var hidStatusReg5 = document.getElementById('Service2');
    var hidStatusReg4 = document.getElementById('Service1');
    var hidStatusEstimator1 = document.getElementById('EstimatorService1');
    var labellabelEstimator1 = document.getElementById('labelEstimator');
    var labelserviceVar = document.getElementById('labelservice'); 
    var modehidStatusReg = document.getElementById('modeFild');
    var modelabelserviceVar = document.getElementById('modeLabel');
    var routinghidStatusReg = document.getElementById('routingFild');
    var routinglabelserviceVar = document.getElementById('routingLabel');
    var packhidStatusReg = document.getElementById('packFild');
    var packlabelserviceVar = document.getElementById('packLabel');
    var weightserviceVar = document.getElementById('weightVolSection');
    var hidStatusCommoditText1=document.getElementById('hidStatusCommoditText');
    var hidStatusCommoditLabel1=document.getElementById('hidStatusCommoditLabel'); 
    var hidStatusSalesText11=document.getElementById('hidStatusSalesText');   
    var hidStatusSalesLabel11=document.getElementById('hidStatusSalesLabel');
    var registrationHide1=document.getElementById('registrationHide');
    var registrationHide11=document.getElementById('registrationHideLabel');
    var carrierFildLabel1=document.getElementById('carrierFildLabel');
    var carrierFild1=document.getElementById('carrierFild');
   	if(job!=''){   		if(job=='RLO'){
   		   hidStatusReg5.style.display = 'block';
   		   hidStatusReg4.style.display = 'none';
   		   hidStatusEstimator1.style.display = 'none';
  	       labellabelEstimator1.style.display= 'none';
   		   labelserviceVar.style.display = 'none';
   		   modehidStatusReg.style.display = 'none';
   		   modelabelserviceVar.style.display = 'none';
   		   routinghidStatusReg.style.display = 'none';
   		   routinglabelserviceVar.style.display = 'none';
   		   packhidStatusReg.style.display = 'none';
   		   packlabelserviceVar.style.display = 'none';
   		   weightserviceVar.style.display = 'none';
   		   hidStatusCommoditText1.style.display = 'none';
   		   hidStatusCommoditLabel1.style.display = 'none';
   		   hidStatusSalesText11.style.display = 'none';
   		   hidStatusSalesLabel11.style.display = 'none';
   		   registrationHide1.style.display = 'block';
   		   registrationHide11.style.display = 'block';
   		<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
   		   carrierFildLabel1.style.display = 'none';
   	     carrierFild1.style.display = 'none';
   	  </configByCorp:fieldVisibility>
   		var hidStatusSalesLabel11=document.getElementById('hidStatusSalesLabel');
   	    var hidStatusSalesLabeTextl11=document.getElementById('hidStatusSalesText');
   		 <configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.city">
  	   hidStatusSalesLabel11.style.display = 'block';
  	   hidStatusSalesLabeTextl11.style.display = 'block';
  	   </configByCorp:fieldVisibility>
  	   <configByCorp:fieldVisibility componentId="component.serviceorderList.CWMS.notshowcity">
  	   hidStatusSalesLabel11.style.display = 'none';
  	   </configByCorp:fieldVisibility> 
   		   <c:if test="${not empty serviceOrder.id}">
   		   document.forms['serviceOrderForm'].elements['changeOriginTicket'].disabled=true;
   		   document.forms['serviceOrderForm'].elements['changeDestinTicket'].disabled=true;
   		   </c:if>   		      		   
   		      }else{   		   	   hidStatusReg4.style.display = 'block';
   		   hidStatusEstimator1.style.display = 'block';
   		   labellabelEstimator1.style.display= 'block';
   			   hidStatusReg5.style.display = 'none';
   			   labelserviceVar.style.display = 'block';
   			   modehidStatusReg.style.display = 'block';
   			   modelabelserviceVar.style.display = 'block';
   			   routinghidStatusReg.style.display = 'block';
   			   routinglabelserviceVar.style.display = 'block';
   			   packhidStatusReg.style.display = 'block';
   			   packlabelserviceVar.style.display = 'block';	
   			   weightserviceVar.style.display = 'block'; 
   	 	       hidStatusCommoditText1.style.display = 'block';
   		   	   hidStatusCommoditLabel1.style.display = 'block';	 
   			   hidStatusSalesText11.style.display = 'block';
   			   hidStatusSalesLabel11.style.display = 'block';
   			   registrationHide1.style.display = 'block';
   			   registrationHide11.style.display = 'block';
   			<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
   			if(mode=='Air' || mode=='Sea'){
   			 carrierFildLabel1.style.display = 'block';
             carrierFild1.style.display = 'block';
   			}
   			   </configByCorp:fieldVisibility>
   			   <c:if test="${not empty serviceOrder.id}">
   			   document.forms['serviceOrderForm'].elements['changeOriginTicket'].disabled=false;
   			   document.forms['serviceOrderForm'].elements['changeDestinTicket'].disabled=false;
   			   </c:if>		   	        	    }    }
	  }catch(e){}
}
function handleHttpResponse54(){
if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();  
                res = results.split("@"); 
              	targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value = '';
					}else{
					stateVal = res[i].split("#"); 
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value = stateVal[0];
					}
					}
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value = '';
        } 
  }  
 var httpState = getHTTPObjectState();
 var httpDState = getHTTPObjectDState();
   function getHTTPObjectState() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
  function getHTTPObjectDState() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
function newFunctionForCountryState(){
var destinationAbc ='${serviceOrder.destinationCountry}';
var originAbc ='${serviceOrder.originCountry}';
var enbState = '${enbState}';
var index = (enbState.indexOf(originAbc)> -1);
var index1 = (enbState.indexOf(destinationAbc)> -1);
if(index != ''){
document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =false;
getOriginStateReset(originAbc);
}
else{
document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =true;
}
if(index1 != ''){
document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =false;
getDestinationStateReset(destinationAbc);
}
else{
document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =true;
}
}
function getDestinationStateReset(target){
	var country = target;
	 var countryList = JSON.parse('${jsonCountryCodeText}');
	 var countryCode =getKeyByValue(countryList,country);
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpDState.open("GET", url, true);
     httpDState.onreadystatechange = handleHttpResponse6666666666;
     httpDState.send(null);
}
function getOriginStateReset(target) {
	var country = target;
	var countryList = JSON.parse('${jsonCountryCodeText}');
	 var countryCode =getKeyByValue(countryList,country);

	 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse555555;
     httpState.send(null);
}
function handleHttpResponse6666666666(){
		if (httpDState.readyState == 4){
                var results = httpDState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${serviceOrder.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					} } }
					document.getElementById("destinationState").value = '${serviceOrder.destinationState}';
        }
}          
   function handleHttpResponse555555(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${serviceOrder.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					} } }
					document.getElementById("originState").value = '${serviceOrder.originState}';
             }   } 
 function showPartnerAlert(display, code, position){
     if(code == ''){ 	
     	document.getElementById(position).style.display="none";
     	ajax_hideTooltip();
     }else if(code != ''){
     	getBAPartner(display,code,position);
     }   }
function getBAPartner(display,code,position){
	var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
     httpPA.open("GET", url, true);
     httpPA.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
     httpPA.send(null);
}
function handleHttpResponsePartner(display,code,position){
 	if (httpPA.readyState == 4){
		var results = httpPA.responseText
        results = results.trim(); 
     if(results.indexOf("Nothing found to display")>1){  
     exit();
      	}else{
      	      if(results.length > 555){
        	document.getElementById(position).style.display="block";
           	if(display != 'onload'){
      		getPartnerAlert(code,position);
          	}
      	}else{
      	  document.getElementById(position).style.display="none";
      		ajax_hideTooltip();
      	}
      	} } }
var httpPA = getHTTPObjectPA();
function getHTTPObjectPA(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
var httpJob = getHTTPObjectJob();
function getHTTPObjectJob(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function getJobList(loadParam){
	var comDiv = "";
	if(document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value != null){
		comDiv = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	}
  	var url = "findJobs.html?ajax=1&decorator=simple&popup=true&compDivision="+encodeURI(comDiv);
   	httpJob.open("GET", url, true);
   	httpJob.onreadystatechange = function() {handleHttpResponseJob(loadParam)};
   	httpJob.send(null);
}
function handleHttpResponseJob(loadParamRes){
	if (httpJob.readyState == 4){
    	var results = httpJob.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.job'];
        targetElement.length = res.length;
        
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].text = '';
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].text = stateVal[1];
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].value = stateVal[0];
			}
		}
		document.forms['serviceOrderForm'].elements['serviceOrder.job'].value = '${serviceOrder.job}';
		
		if(loadParamRes!='onload'){
			getCommodity();
			getServiceTypeByJob();
			populateOpsPersonByJob();
	}}}     
function toCheckWeightnVolumeRadio1()
{
     var weightTabSub1  =  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub1'].value);
     var weightTabSub2  =  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub2'].value);
	if(document.forms['serviceOrderForm'].elements['weightnVolumeRadio'].checked == true)
	{  if(weightTabSub1%2==0){
        animatedcollapse.toggle('weightnvolumesub1');
        weightTabSub1 =weightTabSub1+1;
        document.forms['serviceOrderForm'].elements['weightTabSub1'].value=String(weightTabSub1);
        }    
    }
	else if(document.forms['serviceOrderForm'].elements['weightnVolumeRadio2'].checked ==true)
	{ if(weightTabSub2%2==0){
         animatedcollapse.toggle('weightnvolumesub2');
         weightTabSub2 =weightTabSub2+1;
         document.forms['serviceOrderForm'].elements['weightTabSub2'].value=String(weightTabSub2);
       }	
	}
}
function toCheckWeightnVolumeRadio(){
 var weightTab      =  parseInt(document.forms['serviceOrderForm'].elements['weightTab'].value);
 var weightTabSub1  =  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub1'].value);
 var weightTabSub2  =  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub2'].value);
 if(weightTab%2==1){ 
 if(document.forms['serviceOrderForm'].elements['weightnVolumeRadio'].checked==true){
   animatedcollapse.toggle('weightnvolumesub1');
   weightTabSub1 =weightTabSub1+1;
   document.forms['serviceOrderForm'].elements['weightTabSub1'].value=String(weightTabSub1);
  }
if(document.forms['serviceOrderForm'].elements['weightnVolumeRadio2'].checked==true){
  animatedcollapse.toggle('weightnvolumesub2');
    weightTabSub2 =weightTabSub2+1;
   document.forms['serviceOrderForm'].elements['weightTabSub2'].value=String(weightTabSub2);
  }
  weightTab =weightTab+1;
  document.forms['serviceOrderForm'].elements['weightTab'].value=String(weightTab);
  }else{
       if(weightTabSub1%2==0){
       animatedcollapse.toggle('weightnvolumesub1');
       weightTabSub1 =weightTabSub1+1;
       document.forms['serviceOrderForm'].elements['weightTabSub1'].value=String(weightTabSub1);
       }
       if(weightTabSub2%2==0){
         animatedcollapse.toggle('weightnvolumesub2');
         weightTabSub2 =weightTabSub2+1;
         document.forms['serviceOrderForm'].elements['weightTabSub2'].value=String(weightTabSub2);
       }
        weightTab =weightTab+1;
      document.forms['serviceOrderForm'].elements['weightTab'].value=String(weightTab);
     }}

function toIncrementWeightTabSub1(){ 
	 var weightTabSub1=  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub1'].value);
	      weightTabSub1 =weightTabSub1+1;
	      document.forms['serviceOrderForm'].elements['weightTabSub1'].value=String(weightTabSub1);
	 }
	function  toIncrementWeightTabSub2(){
	         var weightTabSub2=  parseInt(document.forms['serviceOrderForm'].elements['weightTabSub2'].value);
	          weightTabSub2 =weightTabSub2+1;
	         document.forms['serviceOrderForm'].elements['weightTabSub2'].value=String(weightTabSub2); 
	}
	function toCheckCountryFields(){
		var incExcype = document.forms['serviceOrderForm'].elements['userQuoteServices'].value;		 			
		 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value!='CNCL'){
	    if(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value=='' || document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value=='')
	    {
	     if(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value==''){
	      alert("Origin country is required field");
	      return false;
	      }
	     if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value==''){
	       alert("Destination country is required field");
	       return false;
	    }
	    }
		else{
			if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Separate section'){
			pickCheckBoxData('');
			 }else if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Single section'){
				pickCheckBoxDataSec();
			}else{
			 }
	    submit_form();
	    }
		 }else{
			 if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Separate section'){
				pickCheckBoxData('');
			 }else if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Single section'){
				 pickCheckBoxDataSec();
			 }else{
			 }
			 submit_form();
		 }
		// alert("hi");
		 //var res=document.getElementById('aqid');
		 
		 document.forms[0].quesansIds.value=document.forms[1].quesansId.value
		 alert(document.forms[0].elements['quesansIds'].value);
	}
	function openPricing(){
	form_submitted = false;
	var checkValidation =  submit_form(); 
	 if(checkValidation == true){
	document.forms['serviceOrderForm'].action = 'saveServiceOrderPricing.html?pricingButton=yes&sid=${serviceOrder.id}';
	var check=false;
	if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
		 var check = true;
	}else{
		var check =saveValidation('saveButton');
	}
	if(check){
	document.forms['serviceOrderForm'].submit();
	setTimeout("animatedcollapse.toggle('pricing')", 5000);
	}}}
	function checkAccountLineNumber(temp) { 
	       var accountLineNumber=document.forms['serviceOrderForm'].elements[temp].value; 
	       var accountLineNumber1=document.forms['serviceOrderForm'].elements[temp].value;
	       accountLineNumber=accountLineNumber.trim();
	       accountLineNumber1=accountLineNumber1.trim();
	       if(accountLineNumber!=""){
	       if(accountLineNumber>999)  {
	         alert("Manually you can not Enter Line # greater than 999");
	         document.forms['serviceOrderForm'].elements[temp].value=0;
	       }
	       else if(accountLineNumber1.length<2)  { 
	       	document.forms['serviceOrderForm'].elements[temp].value='00'+document.forms['serviceOrderForm'].elements[temp].value;
	       }
	       else if(accountLineNumber1.length==2)  { 
	       	var aa = document.forms['serviceOrderForm'].elements[temp].value;
	       	document.forms['serviceOrderForm'].elements[temp].value='0'+aa;
	       }} }  
	function onlyNumberAllowed(evt){
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39); 
		}
	var chargeCodeMsgPromt="0";
	function fillDiscription(discription,chargeCode,category,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate,chargeCodeTemp,aid)
	{ 
		var chargeCodeValidationVal=document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value;
		if(chargeCodeValidationVal=='' || chargeCodeValidationVal==null){
		chargeCodeMsgPromt++;	
		var category = document.forms['serviceOrderForm'].elements[category].value;
		var quotationContract = document.forms['serviceOrderForm'].elements['billing.contract'].value;
		var chargeCode1 = document.forms['serviceOrderForm'].elements[chargeCode].value;
		var discriptionValue = document.forms['serviceOrderForm'].elements[discription].value; 
		var jobtype = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
	    var routing = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value; 
	    var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	    var sid='${serviceOrder.id}'; 
			var url = "findQuotationDiscription.html?ajax=1&contract="+quotationContract+"&chargeCode="+chargeCode1+"&decorator=simple&popup=true&jobType="+jobtype+"&routing1="+routing+"&soId="+sid+"&companyDivision="+companyDivision;
	     	http50.open("GET", url, true);
	     	http50.onreadystatechange =  function(){fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate,chargeCode,chargeCodeTemp,aid,chargeCodeMsgPromt);};
	     	http50.send(null);
		}
	}
	function fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate,chargeCode,chargeCodeTemp,aid,chargeCodeMsgPromt){ 
	   if (http50.readyState == 4)
	       {
	       	   var result= http50.responseText
	           result = result.trim();   
	           result = result.replace('[','');
	           result=result.replace(']','');
	           var res = result.split("#");
	           var discriptionValue = document.forms['serviceOrderForm'].elements[discription].value;
	           var chargeCodeNew = document.forms['serviceOrderForm'].elements[chargeCode].value;
	           var chargeCodeOld = document.forms['serviceOrderForm'].elements[chargeCodeTemp].value;
				
	           if(chargeCodeNew.trim()!=chargeCodeOld.trim()){
		    		rateOnActualizationDate('estSellCurrencyNew'+aid,'estSellValueDateNew'+aid,'estSellExchangeRateNew'+aid);
		    		rateOnActualizationDate('revisionSellCurrency'+aid,'revisionSellValueDate'+aid,'revisionSellExchangeRate'+aid);
		    		rateOnActualizationDate('recRateCurrency'+aid,'racValueDate'+aid,'recRateExchange'+aid);
		    		}	
		           if(chargeCodeNew.trim()!=chargeCodeOld.trim()){
				           <configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
				           if(result.indexOf('NoDiscription')<0) {
				           if(document.forms['serviceOrderForm'].elements[discription].value.trim()==''){
				           if(res[8]==''){ 
				        	   document.forms['serviceOrderForm'].elements[discription].value = res[0];
					           }else{
					        	   res[8]=res[8].trim();
					        	   document.forms['serviceOrderForm'].elements[discription].value = res[8];
					           }
				           }}
				           </configByCorp:fieldVisibility>
				           <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
				           if(res[8]==''){
				        	   document.forms['serviceOrderForm'].elements[discription].value = res[0];
					           }else{
					        	   res[8]=res[8].trim();
					        	   document.forms['serviceOrderForm'].elements[discription].value = res[8];
					           }	
				       		</configByCorp:fieldVisibility>
				       					res[2]=res[2].trim();
					       		        document.forms['serviceOrderForm'].elements[recGl].value=res[1];
					       		        document.forms['serviceOrderForm'].elements[payGl].value=res[2];
				       		           var ch=chargeCode;
				       		           var aid=ch.replace("chargeCode","");
				       		       	<c:if test="${chargeDiscountFlag}">
				       		       	checkChargeDiscount(chargeCode,'',aid);
				       		       	</c:if>
				       		     fillDefaultInvoiceNumber(aid);
		           }
		           <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">		           
		           if(res[5]=='Y'){
			            	document.forms['serviceOrderForm'].elements['rollUpInInvoice'+aid].checked=true;
			            }else{
			            	document.forms['serviceOrderForm'].elements['rollUpInInvoice'+aid].checked=false;          
			            }
		           </configByCorp:fieldVisibility>
		         <c:if test="${costElementFlag}">
		        	try{
		                var ctype= document.forms['serviceOrderForm'].elements['category'+aid].value;
		          		 ctype=ctype.trim();
		          		if(ctype==''){
		          			document.forms['serviceOrderForm'].elements['category'+aid].value = res[6];
		          		}
		        	}catch(e){}
		        	</c:if> 
		        	try{
		                var VATExclude=res[7];
		                VATExclude=VATExclude.trim();
		                var oldChargeCode=document.forms['serviceOrderForm'].elements['accChargeCodeTemp'+aid].value;
		                var newChargeCode=document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
		                if(oldChargeCode!=newChargeCode){		
		                if(VATExclude=='Y'){
		          			document.forms['serviceOrderForm'].elements['VATExclude'+aid].value = true;
		  				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value="";
						  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value="";						  		          			
		  				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = true;
						  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = true;
						 
						  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=0;
			
			</c:if>
			document.forms['serviceOrderForm'].elements['vatAmt'+aid].value=0;			
			document.forms['serviceOrderForm'].elements['estExpVatAmt'+aid].value=0;			
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['revisionVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['revisionExpVatAmt'+aid].value=0;			
			</c:if>
						  
		          		}else{
		          			document.forms['serviceOrderForm'].elements['VATExclude'+aid].value = false;
		  				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = false;
						  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = false;
		          		}
		                }
		        	}catch(e){}
		        	<c:if test="${cmmDmmFlag}">
		        	try{
		        		var chargeCodeNew = document.forms['serviceOrderForm'].elements[chargeCode].value;
		        		chargeCodeNew=chargeCodeNew.trim();
		        		if((chargeCodeNew=='DMMFXFEE' || chargeCodeNew=='DMMFEE' || chargeCodeNew=='MGMTFEE') && (chargeCodeMsgPromt=='1' || chargeCodeMsgPromt=='2')){
		        			alert("You are not allowed to select this charge code");
		        			document.forms['serviceOrderForm'].elements[recGl].value="";
		       		        document.forms['serviceOrderForm'].elements[payGl].value="";
		       		     document.forms['serviceOrderForm'].elements[chargeCode].value="";
		        		}
		        	}catch(e){}
		        	</c:if>
		    		        	   
	       }  }
	 function checkChargeDiscount(chargeCode1,sid,aid){
		 var quotationContract = document.forms['serviceOrderForm'].elements['billing.contract'].value;
		 var chargeCode=document.forms['serviceOrderForm'].elements[chargeCode1].value;
		 if(chargeCode!=''){
		 var url="checkChargeDiscount.html?ajax=1&decorator=simple&popup=true&contract="+quotationContract+"&chargeCode="+chargeCode; 
	     httpChargeDiscount.open("GET", url, true);
	     httpChargeDiscount.onreadystatechange =function(){handleHttpResponseChargeDiscount(chargeCode,sid,aid);} ;
	     httpChargeDiscount.send(null);
		 }
	 }
	 function handleHttpResponseChargeDiscount(chargeCode,sid,aid){
		 if (httpChargeDiscount.readyState == 4) {
		        var results = httpChargeDiscount.responseText
		        results = results.trim(); 
		        if(results.length>3){
					        results = results.replace("{",'');
					        results = results.replace("}",'');  
					        var discountLength=results.split(",")         
				            targetElement = document.forms['serviceOrderForm'].elements['estimateDiscount'+aid];
							targetElement.length = discountLength.length;
							for(i=0;i<discountLength.length;i++)
								{
								  		document.forms['serviceOrderForm'].elements['estimateDiscount'+aid].options[i].value=discountLength[i].trim().split("=")[0];
								  		document.forms['serviceOrderForm'].elements['estimateDiscount'+aid].options[i].text=discountLength[i].trim().split("=")[1];
				         		}
				       document.forms['serviceOrderForm'].elements['estimateDiscount'+aid].value=document.forms['serviceOrderForm'].elements['oldEstimateDiscount'+aid].value;

						var flagDiscountTemp='false';
						try{
							<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								flagDiscountTemp='true';
							</c:if>
						}catch(e){}
						if(flagDiscountTemp=='true'){		       
			            targetElement = document.forms['serviceOrderForm'].elements['revisionDiscount'+aid];
						targetElement.length = discountLength.length;
						for(i=0;i<discountLength.length;i++)
							{
							  		document.forms['serviceOrderForm'].elements['revisionDiscount'+aid].options[i].value=discountLength[i].trim().split("=")[0];
							  		document.forms['serviceOrderForm'].elements['revisionDiscount'+aid].options[i].text=discountLength[i].trim().split("=")[1];
			         		}
			      			 document.forms['serviceOrderForm'].elements['revisionDiscount'+aid].value=document.forms['serviceOrderForm'].elements['oldRevisionDiscount'+aid].value;
						}
				       
						flagDiscountTemp='false';
						try{
							<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								flagDiscountTemp='true';
							</c:if>
						}catch(e){}
						if(flagDiscountTemp=='true'){		       
			            targetElement = document.forms['serviceOrderForm'].elements['actualDiscount'+aid];
						targetElement.length = discountLength.length;
						for(i=0;i<discountLength.length;i++)
							{
							  		document.forms['serviceOrderForm'].elements['actualDiscount'+aid].options[i].value=discountLength[i].trim().split("=")[0];
							  		document.forms['serviceOrderForm'].elements['actualDiscount'+aid].options[i].text=discountLength[i].trim().split("=")[1];
			         		}
			      			 document.forms['serviceOrderForm'].elements['actualDiscount'+aid].value=document.forms['serviceOrderForm'].elements['oldActualDiscount'+aid].value;
						} 
		        }
		 }
	 }
	 function checkChargeCode(chargeCode,category,aid){
		 var chargeCodeValidationVal=document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value;
		 if(chargeCodeValidationVal=='' || chargeCodeValidationVal==null){
		 var charge= document.forms['serviceOrderForm'].elements[chargeCode].value;
		 var accChargeCodeTemp = document.forms['serviceOrderForm'].elements['accChargeCodeTemp'+aid].value;
		 if(charge.trim()!="" && charge != accChargeCodeTemp){
		 progressBarAutoSave('1');
	    var val = document.forms['serviceOrderForm'].elements[category].value;
	    var jobType='${serviceOrder.job}'; 
	    var routing='${serviceOrder.routing}'; 
	    var soCompanyDivision =	'${serviceOrder.companyDivision}'; 
	    var sid='${serviceOrder.id}'; 
	    var accountCompanyDivision = document.forms['serviceOrderForm'].elements['companyDivision'+aid].value;
		var quotationContract = document.forms['serviceOrderForm'].elements['billing.contract'].value;
		var myRegxp = /^[a-zA-Z0-9-_\'('&'\)']+$/;
		if(charge.match(myRegxp)==false){
		   alert( "Please enter a valid charge code." );
	       document.forms['serviceOrderForm'].elements[chargeCode].value='';
	       progressBarAutoSave('0');
		}
		else{
		if(val=='Internal Cost'){
		 quotationContract="Internal Cost Management";
		 var url="checkInternalCostChargeCode.html?ajax=1&decorator=simple&popup=true&accountCompanyDivision="+encodeURIComponent(accountCompanyDivision)+"&chargeCode="+encodeURIComponent(charge)+"&jobType="+encodeURIComponent(jobType)+"&routing="+encodeURIComponent(routing)+"&soId="+sid+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
		 http5.open("GET", url, true);
	     http5.onreadystatechange =function(){handleHttpResponseChargeCode(chargeCode,category,aid);} ;
	     http5.send(null);
		}
		else{
		if(quotationContract=='' || quotationContract==' '){
		alert("There is no pricing contract in billing: Please select.");
		document.forms['serviceOrderForm'].elements[chargeCode].value = "";
		progressBarAutoSave('0');
		}
		else{
		 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+quotationContract+"&chargeCode="+charge+"&jobType="+jobType+"&routing="+routing+"&soCompanyDivision="+soCompanyDivision; 
	     http5.open("GET", url, true);
	     http5.onreadystatechange =function(){handleHttpResponseChargeCode(chargeCode,category,aid);} ;
	     http5.send(null);
		}}}}}}
	 function handleHttpResponseChargeCode(chargeCode,category,aid) {
	     if (http5.readyState == 4) {
	        var results = http5.responseText
	        results = results.trim(); 
	        var res = results.split("#"); 
	        if(results.length>4)  {
	        	<c:if test="${costElementFlag}">
	        	try{
	                var ctype= document.forms['serviceOrderForm'].elements['category'+aid].value;
	          		 ctype=ctype.trim();
	          		if(ctype==''){
	          			document.forms['serviceOrderForm'].elements['category'+aid].value = res[12];
	          		}
	        	}catch(e){}
	        	</c:if>
	        	populateCostElementData(aid);
	        	<c:if test="${contractType}">  
	           document.forms['serviceOrderForm'].elements['contractCurrencyNew'+aid].value=res[9];
	           document.forms['serviceOrderForm'].elements['payableContractCurrencyNew'+aid].value=res[10];
	           accountLineIdScript=aid;
	           checkChargesDataFromSO(); 
	       	</c:if> 
	       	<configByCorp:fieldVisibility componentId="component.accountLine.BillingCurrencyFromCharge">
        	try{
        	if(res[9] !='' && res[9]!=undefined &&  res[9]!="undefined"){
        		document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value=res[9];
				document.forms['serviceOrderForm'].elements['revisionSellCurrency'+aid].value=res[9];
				document.forms['serviceOrderForm'].elements['recRateCurrency'+aid].value=res[9];
			}
			}catch(e){}
	  	    try{
        	if(res[10] !='' && res[10]!=undefined &&  res[10]!="undefined"){
				document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value=res[10];
				document.forms['serviceOrderForm'].elements['revisionCurrency'+aid].value=res[10];
				document.forms['serviceOrderForm'].elements['country'+aid].value=res[10];
			}
			}catch(e){}
        	</configByCorp:fieldVisibility>
	       	<c:if test="${chargeDiscountFlag}">
	       	checkChargeDiscount(chargeCode,'',aid);
	       	</c:if>
	            }
	         else{
	        	document.getElementById("ChargeNameDivId"+aid).style.display = "none";
	        	document.forms['serviceOrderForm'].elements[chargeCode].focus();
	        	var chCode= document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
	        	if(chCode.trim()!='DMMFXFEE' && chCode.trim()!='DMMFEE' && chCode.trim()!='MGMTFEE'){
	            	alert("Charge code does not exist according the pricing contract in Pricing Detail, Please select another" );
	            	document.forms['serviceOrderForm'].elements[chargeCode].value = "";
	         	}
			   } }
	     progressBarAutoSave('0');
	 }
	 var populateCostElementDatahttp=getHTTPObject77(); 
	 function populateCostElementData(aid){
		 if(aid==''){
			 aid=accountLineIdScript;
		 }
	 	<c:if test="${costElementFlag}">
	 	var chargeCode= document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
	 	var billingContract = document.forms['serviceOrderForm'].elements['billing.contract'].value;
	 	var sid='${serviceOrder.id}';
	 	var url="populateCostElementDataAjax.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(billingContract)+"&chargeCode="+encodeURIComponent(chargeCode)+"&soId="+sid;
	 	populateCostElementDatahttp.open("GET", url, true);
	 	populateCostElementDatahttp.onreadystatechange = function(){handleHttpResponsePopulateCostElementData(aid);} ;
	 	populateCostElementDatahttp.send(null);
	 	</c:if>
	 }
	 function handleHttpResponsePopulateCostElementData(aid) { 
	 	if (populateCostElementDatahttp.readyState == 4) {
	 		var results = populateCostElementDatahttp.responseText
	 		results = results.trim();
	 	    if(results.length>1){
	 	    	document.forms['serviceOrderForm'].elements['accountLineCostElement'+aid].value=results.split("~")[0];
	 		    document.forms['serviceOrderForm'].elements['accountLineScostElementDescription'+aid].value=results.split("~")[1];
	 	    }
	 	    
	     }
	 }
	 function checkAccountInterface(vendorCode,estimateVendorName,actgCode){			                	  
		 <c:if test="${accountInterface!='Y'}">
		 	checkVendorName(vendorCode,estimateVendorName);
	     </c:if>
	     <c:if test="${accountInterface=='Y'}">
	       vendorCodeForActgCode(vendorCode,estimateVendorName,actgCode);
	     </c:if>
	 }
	 var httpvandor = getHTTPObject(); 
	function vendorCodeForActgCode(vendorCode,estimateVendorName,actgCode){
	    var vendorId = document.forms['serviceOrderForm'].elements[vendorCode].value;
		if(vendorId == ''){
			document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
		}
		if(vendorId != ''){
			var myRegxp = /([a-zA-Z0-9_-]+)$/;
			if(myRegxp.test(vendorId)==false){
				alert("Vendor code not valid" );
	            document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
			 	 document.forms['serviceOrderForm'].elements[vendorCode].value="";
			}else{
				var aid=vendorCode.replace('vendorCode','');
				aid=aid.trim();
				var companyDivision=document.forms['serviceOrderForm'].elements['companyDivision'+aid].value;
			     var url=""
			    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
			    var actgCodeFlag='';
			    try{
			    	actgCodeFlag=document.forms['serviceOrderForm'].elements['actgCodeFlag'].value;
			    }catch(e){} 
			     url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision)+"&actgCodeFlag="+encodeURI(actgCodeFlag);	     
			    </c:if>
			    <c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}"> 
			     url="vendorNameForUniqueActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision);
			    </c:if>
		        httpvandor.open("GET", url, true);
			    httpvandor.onreadystatechange = function(){handleHttpResponse22219(vendorCode,estimateVendorName,actgCode);} ;
			    httpvandor.send(null);
	    } }	    
	}
	function handleHttpResponse22219(vendorCode,estimateVendorName,actgCode){
		if (httpvandor.readyState == 4){
                var results = httpvandor.responseText
                results = results.trim(); 
               
                var res = results.split("#");  
                if(res.length >= 2){ 
		                if(res[3]=='Approved'){
		                	if(res[2] != "null" && res[2] !=''){
		    					document.forms['serviceOrderForm'].elements[estimateVendorName].value = res[1];
		    					document.forms['serviceOrderForm'].elements[actgCode].value = res[2];
		    				    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
		    				    try{
		    				    	var aid=vendorCode.replace('vendorCode',''); aid=aid.trim();
		    				    	if(res[8] != "null" && res[8].trim() !=''){
		    							document.forms['serviceOrderForm'].elements['companyDivision'+aid].value=res[8].trim();
		    							document.forms['serviceOrderForm'].elements['companyDivision'+aid].disabled=true;
		    							document.forms['serviceOrderForm'].elements['vendorCodeOwneroperator'+aid].value='Y'; 
		    				    	}else{
		    				    		document.forms['serviceOrderForm'].elements['companyDivision'+aid].disabled=false;
		    				    		document.forms['serviceOrderForm'].elements['vendorCodeOwneroperator'+aid].value=''; 
		    				    	}
		    				    	}catch(e){} 
		    				     </c:if>
		    			    }  else { 
				                document.forms['serviceOrderForm'].elements[estimateVendorName].value=res[1]; 
								document.forms['serviceOrderForm'].elements[actgCode].value="";
								document.forms['serviceOrderForm'].elements[vendorCode].select(); 
				            }
				        }else{
				            alert("Partner code is not approved" ); 
							document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
							document.forms['serviceOrderForm'].elements[vendorCode].value="";
							document.forms['serviceOrderForm'].elements[actgCode].value="";
							document.forms['serviceOrderForm'].elements[vendorCode].select(); 
				        }     
		            }else{
		                alert("Partner code not valid" ); 
						document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
						document.forms['serviceOrderForm'].elements[vendorCode].value="";
						document.forms['serviceOrderForm'].elements[actgCode].value="";
						document.forms['serviceOrderForm'].elements[vendorCode].select(); 
					}  } }    
	function checkVendorName(vendorCode,estimateVendorName){
	    var vendorId = document.forms['serviceOrderForm'].elements[vendorCode].value;
		if(vendorId == ''){
			document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
		}
		if(vendorId != ''){
			var myRegxp = /([a-zA-Z0-9_-]+)$/;
			if(myRegxp.test(vendorId)==false){
				alert("Vendor code not valid" );
	            document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
			 	 document.forms['serviceOrderForm'].elements[vendorCode].value="";
			}else{
	    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	     http5.open("GET", url, true);
	     http5.onreadystatechange =function(){handleHttpResponsevendorCode(vendorCode,estimateVendorName);} ;
	     http5.send(null);
	    } }}
	function handleHttpResponsevendorCode(vendorCode,estimateVendorName){
			if (http5.readyState == 4){
	                var results = http5.responseText
	                results = results.trim();
	                var res = results.split("#"); 
			           if(res.size() >= 2){ 
			           		if(res[2] == 'Approved'){
			           			document.forms['serviceOrderForm'].elements[estimateVendorName].value = res[1];
			           			document.forms['serviceOrderForm'].elements[vendorCode].select();
			           		}else{
			           			alert("Vendor code is not approved" ); 
							    document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
							    document.forms['serviceOrderForm'].elements[vendorCode].value="";
							    document.forms['serviceOrderForm'].elements[vendorCode].select();
			           		}
	                }else{
	                     alert("Vendor code not valid" );
	                     document.forms['serviceOrderForm'].elements[estimateVendorName].value="";
					 	 document.forms['serviceOrderForm'].elements[vendorCode].value="";
					 	 document.forms['serviceOrderForm'].elements[vendorCode].select();
				   }}}
	function winOpenForActgCode(vendorCode,vendorName,category,aid){
	
		   accountLineIdScript=aid;
		    var sid='${serviceOrder.id}';
		 	var val = document.forms['serviceOrderForm'].elements[category].value;
		 	var finalVal = "OK";
		 	var indexCheck = val.indexOf('Origin'); 
		    openWindow('findVendorCode.html?sid='+sid+'&defaultListFlag=show&partnerType=PA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+vendorName+'&fld_code='+vendorCode+'&fld_seventhDescription=seventhDescription', 1300,500);
		 	document.forms['serviceOrderForm'].elements[vendorCode].select(); 
	 }
	function changeBasis(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,targetElement){  
		  var quantityValue = document.forms['serviceOrderForm'].elements[quantity].value;
		  var basisValue = document.forms['serviceOrderForm'].elements[basis].value; 
		  var oldBasis="old"+basis;
		  var oldQuantity="old"+quantity;
		  var oldQuantityValue = document.forms['serviceOrderForm'].elements[oldQuantity].value;
		  var oldBasisValue = document.forms['serviceOrderForm'].elements[oldBasis].value;  
		  if((document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value=="AIR")|| (document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value=="Air")){
		  if(basisValue=='cwt'){
		  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;
		  </c:if>
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;
			  <c:if test="${contractType}">
			  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;
			  </c:if>		  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
			document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;
		  }
		  </c:if>
		  }else if(basisValue=='kg'){
		  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value;
		  </c:if>		  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value;
		  }
		  </c:if>
		  }
		  else if(basisValue=='cft'){
		  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;
		  </c:if>		  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;
		  }
		  </c:if>
		  }
		  else if(basisValue=='cbm'){
		  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value;
		  </c:if>		  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value;
		  }
		  </c:if>
		  }
		  else if(basisValue=='each'){
		  document.forms['serviceOrderForm'].elements[quantity].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=1;
		  </c:if>		  		  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=1;
		  }
		  </c:if>
		  }
		  else if(basisValue=='value'){
		  document.forms['serviceOrderForm'].elements[quantity].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=1;
		  </c:if>		  	  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=1;
		  }
		  </c:if>
		  }
		  else if(basisValue=='flat'){
		  document.forms['serviceOrderForm'].elements[quantity].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=1;
		  </c:if>		  		  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=1;
		  }
		  </c:if>
		  }
		  }
		  else{
		  if(basisValue=='cwt'){
		  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;
		  </c:if>		  		  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;
		  }
		  </c:if>
		  }else if(basisValue=='kg'){
		  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value;
		  </c:if>		  		  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value;
		  }
		  </c:if>
		  }
		  else if(basisValue=='cft'){
		  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value;
		  </c:if>		  	  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value;
		  }
		  </c:if>
		  }
		  
		  else if(basisValue=='cbm'){
		  document.forms['serviceOrderForm'].elements[quantity].value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value;
		  </c:if>		  	  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value;
		  }
		  </c:if>
		  }
		  else if(basisValue=='each'){
		  document.forms['serviceOrderForm'].elements[quantity].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=1;
		  </c:if>		  	  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=1;
		  }
		  </c:if>
		  }
		  else if(basisValue=='value'){
		  document.forms['serviceOrderForm'].elements[quantity].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=1;
		  </c:if>		  		  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=1;
		  }
		  </c:if>
		  }
		  else if(basisValue=='flat'){
		  document.forms['serviceOrderForm'].elements[quantity].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
		  </c:if>
		  
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=1;
		  <c:if test="${contractType}">
		  document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=1;
		  </c:if>		  	  
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=1;
		  }
		  </c:if>
		  } }
		  calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid);
		  calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,'','','');
		  <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  calculateRevisionBlock(aid,'BAS','','','');
		  </c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  if(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value==0) {
		  calculateActualRevenue(aid,targetElement,'BAS','','','');
		  }
		  </c:if>
			}
	 function calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid){ 
		  var expenseValue=0;
		    var expenseRound=0;
		    var oldExpense=0;
		    var oldExpenseSum=0;
		    var balanceExpense=0;
		    oldExpense=eval(document.forms['serviceOrderForm'].elements[expense].value);
		    <c:if test="${not empty serviceOrder.id}">
		    oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
		    </c:if>
		    var basisValue = document.forms['serviceOrderForm'].elements[basis].value;
		    checkFloatNew('serviceOrderForm',quantity,'Nothing');    
		    var quantityValue = document.forms['serviceOrderForm'].elements[quantity].value;
		    checkFloatNew('serviceOrderForm',buyRate,'Nothing');    
		    var buyRateValue = document.forms['serviceOrderForm'].elements[buyRate].value;
		    if(basisValue=="cwt" || basisValue== "%age"){
		    expenseValue=(quantityValue*buyRateValue)/100 ;
		    }
		    else if(basisValue=="per 1000"){
		    expenseValue=(quantityValue*buyRateValue)/1000 ;
		    }else{
		    expenseValue=(quantityValue*buyRateValue);
		    }
		    expenseRound=Math.round(expenseValue*10000)/10000;
		    balanceExpense=expenseRound-oldExpense;
		    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
		    balanceExpense=Math.round(balanceExpense*10000)/10000; 
		    oldExpenseSum=oldExpenseSum+balanceExpense;
		    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
		    var expenseRoundNew=""+expenseRound;
		    if(expenseRoundNew.indexOf(".") == -1){
			    expenseRoundNew=expenseRoundNew+".0000";
			    } 
		    while((expenseRoundNew.indexOf(".")+5 != expenseRoundNew.length)){
		    	expenseRoundNew=expenseRoundNew+"0";
		    } 
		    document.forms['serviceOrderForm'].elements[expense].value=expenseRoundNew; 
		    try{
		        var buyRate1=document.forms['serviceOrderForm'].elements['estimateRate'+aid].value;
		        var estCurrency1=document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value;
				if(estCurrency1.trim()!=""){
						var Q1=0;
						var Q2=0;
						Q1=buyRate1;
						Q2=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
			            var E1=Q1*Q2;
			            E1=Math.round(E1*10000)/10000;
			            document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=E1;
			            var estLocalAmt1=0;
			            if(basisValue=="cwt" || basisValue== "%age"){
			            	estLocalAmt1=(quantityValue*E1)/100 ;
			                }
			                else if(basisValue=="per 1000"){
			                	estLocalAmt1=(quantityValue*E1)/1000 ;
			                }else{
			                	estLocalAmt1=(quantityValue*E1);
			                }	 
			            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
			            document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estLocalAmt1;     
				}
				<c:if test="${contractType}">
				try{
				     var estimatePayableContractExchangeRate=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
				     var estLocalRate=document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value;
				     var estExchangeRate=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value
				     var recCurrencyRate=(estimatePayableContractExchangeRate*estLocalRate)/estExchangeRate;
				     var roundValue=Math.round(recCurrencyRate*10000)/10000;
				     document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=roundValue ; 
					var Q11=0;
					var Q21=0;
					Q11=document.forms['serviceOrderForm'].elements[quantity].value;
					Q21=document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
		            var E11=Q11*Q21;
		            var estLocalAmt11=0;
		            if(basisValue=="cwt" || basisValue== "%age"){
		            	estLocalAmt11=E11/100 ;
		                }
		                else if(basisValue=="per 1000"){
		                	estLocalAmt11=E11/1000 ;
		                }else{
		                	estLocalAmt11=E11;
		                }	 
		            estLocalAmt11=Math.round(estLocalAmt11*10000)/10000;    
		            document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value=estLocalAmt11;
				}catch(e){}     
			</c:if>
		    }catch(e){}
		    <c:if test="${not empty serviceOrder.id}">
		    var newExpenseSum=""+oldExpenseSum;
		    if(newExpenseSum.indexOf(".") == -1){
		    newExpenseSum=newExpenseSum+".00";
		    } 
		    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
		    newExpenseSum=newExpenseSum+"0";
		    } 
		    document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
		    </c:if>
		    if(expenseRoundNew>0){
		    document.forms['serviceOrderForm'].elements[displayOnQuots].checked = true;
		    }
		    try{
		        <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		        calculateVatAmt(estVatPer,revenue,estAmt);
		        </c:if>
		        }catch(e){}
		    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
		       }
	 function checkFloatNew(formName,fieldName,message)
	 {   var i;
	 	var s = document.forms[formName].elements[fieldName].value;
	 	var count = 0;
	 	if(s.trim()=='.') {
	 		document.forms[formName].elements[fieldName].value='0.00';
	 	}else{
	     for (i = 0; i < s.length; i++)
	     {   
	         var c = s.charAt(i); 
	         if(c == '.') {
	         	count = count+1
	         }
	         if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) && (c != '-') ) 
	         {
	 	        document.forms[formName].elements[fieldName].value='0.00';
	         } }    } }
	 function findExchangeRateGlobal(currency){
		 var rec='1';
			<c:forEach var="entry" items="${currencyExchangeRate}">
				if('${entry.key}'==currency.trim()){
					rec='${entry.value}';
				}
			</c:forEach>
			return rec;
	 }
	 function currentDateGlobal(){
		 var mydate=new Date();
	     var daym;
	     var year=mydate.getFullYear()
	     var y=""+year;
	     if (year < 1000)
	     year+=1900
	     var day=mydate.getDay()
	     var month=mydate.getMonth()+1
	     if(month == 1)month="Jan";
	     if(month == 2)month="Feb";
	     if(month == 3)month="Mar";
		  if(month == 4)month="Apr";
		  if(month == 5)month="May";
		  if(month == 6)month="Jun";
		  if(month == 7)month="Jul";
		  if(month == 8)month="Aug";
		  if(month == 9)month="Sep";
		  if(month == 10)month="Oct";
		  if(month == 11)month="Nov";
		  if(month == 12)month="Dec";
		  var daym=mydate.getDate()
		  if (daym<10)
		  daym="0"+daym
		  var datam = daym+"-"+month+"-"+y.substring(2,4); 
		  return datam;
	 }

	 function revisionBuyFX(aid){
		 var basis=document.forms['serviceOrderForm'].elements['basis'+aid].value;
		 var baseRateVal=1;
		 var roundVal=0.00;
		 var revisionQuantity=document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value;
		 if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
			 revisionQuantity = document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=1;
	         } 
	     if( basis=="cwt" || basis=="%age"){
	       	 baseRateVal=100;
	     }else if(basis=="per 1000"){
	       	 baseRateVal=1000;
	     } else {
	       	 baseRateVal=1;  	
		 }
	     var revisionDiscount=0.00;
	     try{
	    	 revisionDiscount=document.forms['serviceOrderForm'].elements['revisionDiscount'+aid].value;
	     }catch(e){}
		 var revisionRate=0.00;
		 revisionRate=document.forms['serviceOrderForm'].elements['revisionRate'+aid].value;
		 if(revisionRate=='') {
			 revisionRate = document.forms['serviceOrderForm'].elements['revisionRate'+aid].value=0.00;
	         }
		  <c:if test="${contractType}">
			var revisionPayableContractCurrency=document.forms['serviceOrderForm'].elements['revisionPayableContractCurrency'+aid].value;		
			var revisionPayableContractExchangeRate=document.forms['serviceOrderForm'].elements['revisionPayableContractExchangeRate'+aid].value;
			var revisionPayableContractRate=document.forms['serviceOrderForm'].elements['revisionPayableContractRate'+aid].value;
			var revisionPayableContractRateAmmount=document.forms['serviceOrderForm'].elements['revisionPayableContractRateAmmount'+aid].value;
			var revisionPayableContractValueDate=document.forms['serviceOrderForm'].elements['revisionPayableContractValueDate'+aid].value;
		 	if(revisionPayableContractCurrency!=''){
		 		var ss=findExchangeRateGlobal(revisionPayableContractCurrency);
		 		revisionPayableContractExchangeRate=parseFloat(ss);
		 		document.forms['serviceOrderForm'].elements['revisionPayableContractExchangeRate'+aid].value=revisionPayableContractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['revisionPayableContractValueDate'+aid].value=currentDateGlobal();
		 		revisionPayableContractRate=revisionRate*revisionPayableContractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['revisionPayableContractRate'+aid].value=revisionPayableContractRate;
		 		revisionPayableContractRateAmmount=(revisionQuantity*revisionPayableContractRate)/baseRateVal;
		  	    document.forms['serviceOrderForm'].elements['revisionPayableContractRateAmmount'+aid].value=revisionPayableContractRateAmmount;	 		
		 	}
		  </c:if>
		  
			var revisionCurrency=document.forms['serviceOrderForm'].elements['revisionCurrency'+aid].value;		
			var revisionExchangeRate=document.forms['serviceOrderForm'].elements['revisionExchangeRate'+aid].value;
			var revisionLocalRate=document.forms['serviceOrderForm'].elements['revisionLocalRate'+aid].value;
			var revisionLocalAmount=document.forms['serviceOrderForm'].elements['revisionLocalAmount'+aid].value;
			var revisionValueDate=document.forms['serviceOrderForm'].elements['revisionValueDate'+aid].value;
		 	if(revisionCurrency!=''){
		 		var ss=findExchangeRateGlobal(revisionCurrency);
		 		revisionExchangeRate=parseFloat(ss);
		 		document.forms['serviceOrderForm'].elements['revisionExchangeRate'+aid].value=revisionExchangeRate;
		 		document.forms['serviceOrderForm'].elements['revisionValueDate'+aid].value=currentDateGlobal();
		 		revisionLocalRate=revisionRate*revisionExchangeRate;
		 		document.forms['serviceOrderForm'].elements['revisionLocalRate'+aid].value=revisionLocalRate;
		 		revisionLocalAmount=(revisionQuantity*revisionLocalRate)/baseRateVal;
		  	   document.forms['serviceOrderForm'].elements['revisionLocalAmount'+aid].value=revisionLocalAmount;	 		
		 	}
	 }
	 function revisionSellFX(aid){
		 var basis=document.forms['serviceOrderForm'].elements['basis'+aid].value;
		 var baseRateVal=1;
		 var roundVal=0.00;
		 var revisionQuantity=document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value;
		 if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
			 revisionQuantity = document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=1;
	         }
		 var revisionSellQuantity=0.00;
		 <c:if test="${contractType}">
		 revisionSellQuantity=document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value;
		 if(revisionSellQuantity=='' ||revisionSellQuantity=='0' ||revisionSellQuantity=='0.0' ||revisionSellQuantity=='0.00') {
			 revisionSellQuantity = document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=1;
	         }		 
		 </c:if> 
	     if( basis=="cwt" || basis=="%age"){
	       	 baseRateVal=100;
	     }else if(basis=="per 1000"){
	       	 baseRateVal=1000;
	     } else {
	       	 baseRateVal=1;  	
		 }
	     var revisionDiscount=0.00;
	     try{
	    	 revisionDiscount=document.forms['serviceOrderForm'].elements['revisionDiscount'+aid].value;
	     }catch(e){}
		 var revisionSellRate=0.00;
		 revisionSellRate=document.forms['serviceOrderForm'].elements['revisionSellRate'+aid].value;
		 if(revisionSellRate=='') {
			 revisionSellRate = document.forms['serviceOrderForm'].elements['revisionSellRate'+aid].value=0.00;
	         } 
		  <c:if test="${contractType}">
			var revisionContractCurrency=document.forms['serviceOrderForm'].elements['revisionContractCurrency'+aid].value;		
			var revisionContractExchangeRate=document.forms['serviceOrderForm'].elements['revisionContractExchangeRate'+aid].value;
			var revisionContractRate=document.forms['serviceOrderForm'].elements['revisionContractRate'+aid].value;
			var revisionContractRateAmmount=document.forms['serviceOrderForm'].elements['revisionContractRateAmmount'+aid].value;
			var revisionContractValueDate=document.forms['serviceOrderForm'].elements['revisionContractValueDate'+aid].value;
		 	if(revisionContractCurrency!=''){
		 		var ss=findExchangeRateGlobal(revisionContractCurrency);
		 		revisionContractExchangeRate=parseFloat(ss);
		 		document.forms['serviceOrderForm'].elements['revisionContractExchangeRate'+aid].value=revisionContractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['revisionContractValueDate'+aid].value=currentDateGlobal();
		 		revisionContractRate=revisionSellRate*revisionContractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['revisionContractRate'+aid].value=revisionContractRate;
		 		 <c:if test="${contractType}">
		 		revisionContractRateAmmount=(revisionSellQuantity*revisionContractRate)/baseRateVal;
		 		</c:if>
		 		 <c:if test="${!contractType}">
			 		revisionContractRateAmmount=(revisionQuantity*revisionContractRate)/baseRateVal;
			 	</c:if>
			    if(revisionDiscount!=0.00){
			    	revisionContractRateAmmount=(revisionContractRateAmmount*revisionDiscount)/100;
			    }
		  	    document.forms['serviceOrderForm'].elements['revisionContractRateAmmount'+aid].value=revisionContractRateAmmount;	 		
		 	}
		  </c:if>
			var revisionSellCurrency=document.forms['serviceOrderForm'].elements['revisionSellCurrency'+aid].value;		
			var revisionSellExchangeRate=document.forms['serviceOrderForm'].elements['revisionSellExchangeRate'+aid].value;
			var revisionSellLocalRate=document.forms['serviceOrderForm'].elements['revisionSellLocalRate'+aid].value;
			var revisionSellLocalAmount=document.forms['serviceOrderForm'].elements['revisionSellLocalAmount'+aid].value;
			var revisionSellValueDate=document.forms['serviceOrderForm'].elements['revisionSellValueDate'+aid].value;
		 	if(revisionSellCurrency!=''){
		 		var ss=findExchangeRateGlobal(revisionSellCurrency);
		 		revisionSellExchangeRate=parseFloat(ss);
		 		document.forms['serviceOrderForm'].elements['revisionSellExchangeRate'+aid].value=revisionSellExchangeRate;
		 		document.forms['serviceOrderForm'].elements['revisionSellValueDate'+aid].value=currentDateGlobal();
		 		revisionSellLocalRate=revisionSellRate*revisionSellExchangeRate;
		 		document.forms['serviceOrderForm'].elements['revisionSellLocalRate'+aid].value=revisionSellLocalRate;
		 		 <c:if test="${contractType}">
		 		revisionSellLocalAmount=(revisionSellQuantity*revisionSellLocalRate)/baseRateVal;
			 		</c:if>
			 		 <c:if test="${!contractType}">
			 		revisionSellLocalAmount=(revisionQuantity*revisionSellLocalRate)/baseRateVal;
				 	</c:if>
			    if(revisionDiscount!=0.00){
			    	revisionSellLocalAmount=(revisionSellLocalAmount*revisionDiscount)/100;
			    }	 		
		  	   document.forms['serviceOrderForm'].elements['revisionSellLocalAmount'+aid].value=revisionSellLocalAmount;	 		
		 	}
	 }
	 function calculateRevisionBlock(aid,type1,field1,field2,field3) {
		 if(field1!='' && field2!='' & field3!=''){
			 rateOnActualizationDate(field1,field2,field3);
			 }	   
		 var basis=document.forms['serviceOrderForm'].elements['basis'+aid].value;
		 var baseRateVal=1;
		 var roundVal=0.00;
		 var revisionSellQuantity=0.00;
			 <c:if test="${contractType}">
			 revisionSellQuantity=document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value;
			 if(revisionSellQuantity=='' ||revisionSellQuantity=='0' ||revisionSellQuantity=='0.0' ||revisionSellQuantity=='0.00') {
				 revisionSellQuantity = document.forms['serviceOrderForm'].elements['revisionSellQuantity'+aid].value=1;
		         }			 
			 </c:if>
		 var revisionQuantity=document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value;
		 if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
			 revisionQuantity = document.forms['serviceOrderForm'].elements['revisionQuantity'+aid].value=1;
	         } 
	     if( basis=="cwt" || basis=="%age"){
	       	 baseRateVal=100;
	     }else if(basis=="per 1000"){
	       	 baseRateVal=1000;
	     } else {
	       	 baseRateVal=1;  	
		 }
	     var revisionDiscount=0.00;
	     try{
	    	 revisionDiscount=document.forms['serviceOrderForm'].elements['revisionDiscount'+aid].value;
	     }catch(e){}

	     //Code For revision Est	 
		 var revisionRate=0.00;
		 revisionRate=document.forms['serviceOrderForm'].elements['revisionRate'+aid].value;
		 if(revisionRate=='') {
			 revisionRate = document.forms['serviceOrderForm'].elements['revisionRate'+aid].value=0.00;
	         }
		 revisionBuyFX(aid);
		 var revisionExpense=0.00;
		 revisionExpense=document.forms['serviceOrderForm'].elements['revisionExpense'+aid].value;
		 if(revisionExpense=='') {
			 revisionExpense = document.forms['serviceOrderForm'].elements['revisionExpense'+aid].value=0.00;
	         }
		 revisionExpense=(revisionRate*revisionQuantity)/baseRateVal;
		    document.forms['serviceOrderForm'].elements['revisionExpense'+aid].value=revisionExpense;

	     //Code For revision Sell	 
		 var revisionSellRate=0.00;
		 revisionSellRate=document.forms['serviceOrderForm'].elements['revisionSellRate'+aid].value;
		 if(revisionSellRate=='') {
			 revisionSellRate = document.forms['serviceOrderForm'].elements['revisionSellRate'+aid].value=0.00;
	         }
		  <c:if test="${multiCurrency=='Y'}">
		  revisionSellFX(aid);
		  </c:if>
		 var revisionRevenueAmount=0.00;
		 revisionRevenueAmount=document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+aid].value;
		 if(revisionRevenueAmount=='') {
			 revisionRevenueAmount = document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+aid].value=0.00;
	         }
		 <c:if test="${contractType}">
		 revisionRevenueAmount=(revisionSellRate*revisionSellQuantity)/baseRateVal;
		 </c:if>
		 <c:if test="${!contractType}">
		 revisionRevenueAmount=(revisionSellRate*revisionQuantity)/baseRateVal;
		 </c:if>
		 
		    if(revisionDiscount!=0.00){
		    	revisionRevenueAmount=(revisionRevenueAmount*revisionDiscount)/100;
		    }
		    document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+aid].value=revisionRevenueAmount;	
		  //Code For Revision Pass Percentage   
		    calculateRevisionPassPercentage('revisionRevenueAmount'+aid,'revisionExpense'+aid,'revisionPassPercentage'+aid);
		  // Code Gross Section
	 	  	var revisedTotalRevenue=0.00;
	 		var revisedTotalExpense=0.00;
	 		var tempRevisedTotalExpense=0.00;
	 		var tempRevisedTotalRevenue=0.00;
	 		var revisedGrossMargin=0.00;
	 		var revisedGrossMarginPercentage=0.00;
	 		var aidListIds=document.forms['serviceOrderForm'].elements['aidListIds'].value;
			var accArr=aidListIds.split(",");
			for(var g=0;g<accArr.length;g++){
				if(document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+accArr[g]].value!=""){
		 			tempRevisedTotalRevenue=document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+accArr[g]].value;
				}
		 		revisedTotalRevenue=revisedTotalRevenue+parseFloat(tempRevisedTotalRevenue);
		 		if(document.forms['serviceOrderForm'].elements['revisionExpense'+accArr[g]].value!=""){
		 			tempRevisedTotalExpense=document.forms['serviceOrderForm'].elements['revisionExpense'+accArr[g]].value;
		 		}
		 		revisedTotalExpense=revisedTotalExpense+parseFloat(tempRevisedTotalExpense);
			}
		revisedGrossMargin=revisedTotalRevenue-revisedTotalExpense;
		try{
			if((revisedTotalRevenue==0)||(revisedTotalRevenue==0.0)||(revisedTotalRevenue==0.00)){
				revisedGrossMarginPercentage=0.00;
			}else{
				revisedGrossMarginPercentage=(revisedGrossMargin*100)/revisedTotalRevenue;
			}
		}catch(e){
			revisedGrossMarginPercentage=0.00;
			}
		 <c:if test="${not empty serviceOrder.id}">
		document.forms['serviceOrderForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value=Math.round(revisedTotalRevenue*10000)/10000;
		document.forms['serviceOrderForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value=Math.round(revisedTotalExpense*10000)/10000;
		document.forms['serviceOrderForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value=Math.round(revisedGrossMargin*10000)/10000;
		document.forms['serviceOrderForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(revisedGrossMarginPercentage*10000)/10000;
		</c:if>	 
		 <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		 <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		 calculateVatSection('REV',aid,type1); 
		 </c:if>
		 </c:if>   
	 }
	 function calculateRevisionPassPercentage(revenue,expense,estimatePassPercentage){
		    var revenueValue=0;
		    var expenseValue=0;
		    var estimatePassPercentageValue=0; 
		    checkFloatNew('serviceOrderForm',estimatePassPercentage,'Nothing'); 
		    revenueValue=eval(document.forms['serviceOrderForm'].elements[revenue].value);
		    expenseValue=eval(document.forms['serviceOrderForm'].elements[expense].value); 
		    try{
		    	estimatePassPercentageValue=(revenueValue/expenseValue)*100;
		    }catch(e){
		    	estimatePassPercentageValue=0.00;
		        } 
		    estimatePassPercentageValue=Math.round(estimatePassPercentageValue);
		    if(document.forms['serviceOrderForm'].elements[expense].value == '' || document.forms['serviceOrderForm'].elements[expense].value == 0||document.forms['serviceOrderForm'].elements[expense].value == 0.00){
		  	   document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
		  	   }else{ 
		  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value = estimatePassPercentageValue;
		  	   } 
		    } 
	 function calculateActualRevenueFromFX (aid,targetElement,type,field1,field2,field3,billingCurrency,contractCurrency,FXType) {
		 if(field1!='' && field2!='' & field3!=''){
			 rateOnActualizationDate(field1,field2,field3);
			 }	   
		  var chargeCode=document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
		  var vendorCode=document.forms['serviceOrderForm'].elements['vendorCode'+aid].value; 
		  var recGl=document.forms['serviceOrderForm'].elements['recGl'+aid].value;
		  var payGl=document.forms['serviceOrderForm'].elements['payGl'+aid].value;   
		  var actgCode=document.forms['serviceOrderForm'].elements['actgCode'+aid].value;
		  var acref="${accountInterface}";
		  acref=acref.trim();
		  if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
			  alert("Vendor Code Missing. Please select");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
			  alert("Charge code needs to be selected");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY' && acref =='Y'){
			  alert("Pay GL missing. Please select another charge code.");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC' && acref =='Y'){
			  alert("Receivable GL missing. Please select another charge code.");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
			  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }else{		   
		 	 
		 var basis=document.forms['serviceOrderForm'].elements['basis'+aid].value;
		 var baseRateVal=1;
		 var roundVal=0.00;
		 var recQuantity=document.forms['serviceOrderForm'].elements['recQuantity'+aid].value;
		 var recRate=0.00;
		 recRate=document.forms['serviceOrderForm'].elements['recRate'+aid].value;
		 if(recRate=='') {
			 recRate = document.forms['serviceOrderForm'].elements['recRate'+aid].value=0.00;
	         }
		 var actualRevenue=document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value;
		 var actualExpense=document.forms['serviceOrderForm'].elements['actualExpense'+aid].value;
	     var actualDiscount=0.00;
	     try{
	    	 actualDiscount=document.forms['serviceOrderForm'].elements['actualDiscount'+aid].value;
	     }catch(e){}
		 if(recQuantity=='' ||recQuantity=='0' ||recQuantity=='0.0' ||recQuantity=='0.00') {
			 recQuantity = document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=1;
	         } 
	     if( basis=="cwt" || basis=="%age"){
	       	 baseRateVal=100;
	     }else if(basis=="per 1000"){
	       	 baseRateVal=1000;
	     } else {
	       	 baseRateVal=1;  	
		 }
	     actualRevenue=(recQuantity*recRate)/baseRateVal;
	 	    if(actualDiscount!=0.00){
	 	    	actualRevenue=(actualRevenue*actualDiscount)/100;
		    }
	 	   document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value=actualRevenue;
	 	  <c:if test="${contractType}">
			var contractCurrency=document.forms['serviceOrderForm'].elements['contractCurrency'+aid].value;		
			var contractExchangeRate=document.forms['serviceOrderForm'].elements['contractExchangeRate'+aid].value;
			var contractRate=document.forms['serviceOrderForm'].elements['contractRate'+aid].value;
			var contractRateAmmount=document.forms['serviceOrderForm'].elements['contractRateAmmount'+aid].value;
			var contractValueDate=document.forms['serviceOrderForm'].elements['contractValueDate'+aid].value;
		 	if(contractCurrency!=''){
		 		if(type=='FX' && FXType=='Rec' && contractCurrency){
		 		var ss=findExchangeRateGlobal(contractCurrency);
		 		contractExchangeRate=parseFloat(ss);
		 		document.forms['serviceOrderForm'].elements['contractExchangeRate'+aid].value=contractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['contractValueDate'+aid].value=currentDateGlobal();
		 		}
		 		contractRate=recRate*contractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['contractRate'+aid].value=contractRate;
		 		contractRateAmmount=(recQuantity*contractRate)/baseRateVal;
		  	    if(actualDiscount!=0.00){
		  	    	contractRateAmmount=(contractRateAmmount*actualDiscount)/100;
		 	    }
		  	   document.forms['serviceOrderForm'].elements['contractRateAmmount'+aid].value=contractRateAmmount;	 		
		 	}
	 	  </c:if>
			var recRateCurrency=document.forms['serviceOrderForm'].elements['recRateCurrency'+aid].value;		
			var recRateExchange=document.forms['serviceOrderForm'].elements['recRateExchange'+aid].value;
			var recCurrencyRate=document.forms['serviceOrderForm'].elements['recCurrencyRate'+aid].value;
			var actualRevenueForeign=document.forms['serviceOrderForm'].elements['actualRevenueForeign'+aid].value;
			var racValueDate=document.forms['serviceOrderForm'].elements['racValueDate'+aid].value;
		 	if(recRateCurrency!=''){
		 		if(type=='FX' && FXType=='Rec' && billingCurrency){
		 		var ss=findExchangeRateGlobal(recRateCurrency);
		 		recRateExchange=parseFloat(ss);
		 		document.forms['serviceOrderForm'].elements['recRateExchange'+aid].value=recRateExchange;
		 		document.forms['serviceOrderForm'].elements['racValueDate'+aid].value=currentDateGlobal();
		 		}
		 		recCurrencyRate=recRate*recRateExchange;
		 		document.forms['serviceOrderForm'].elements['recCurrencyRate'+aid].value=recCurrencyRate;
		 		actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
		  	    if(actualDiscount!=0.00){
		  	    	actualRevenueForeign=(actualRevenueForeign*actualDiscount)/100;
		 	    }
		  	   document.forms['serviceOrderForm'].elements['actualRevenueForeign'+aid].value=actualRevenueForeign;	 		
		 	}

	<c:if test="${contractType}">
	var payableContractCurrency=document.forms['serviceOrderForm'].elements['payableContractCurrency'+aid].value;		
	var payableContractExchangeRate=document.forms['serviceOrderForm'].elements['payableContractExchangeRate'+aid].value;
	var payableContractRateAmmount=document.forms['serviceOrderForm'].elements['payableContractRateAmmount'+aid].value;
	var payableContractValueDate=document.forms['serviceOrderForm'].elements['payableContractValueDate'+aid].value;
		if(payableContractCurrency!=''){
			if(type=='FX' && FXType=='Pay' && contractCurrency){
			var ss=findExchangeRateGlobal(payableContractCurrency);
			payableContractExchangeRate=parseFloat(ss);
			document.forms['serviceOrderForm'].elements['payableContractExchangeRate'+aid].value=payableContractExchangeRate;
			document.forms['serviceOrderForm'].elements['payableContractValueDate'+aid].value=currentDateGlobal();
			}
			payableContractRateAmmount=actualExpense*payableContractExchangeRate;
		   document.forms['serviceOrderForm'].elements['payableContractRateAmmount'+aid].value=payableContractRateAmmount;	 		
		}
	</c:if>
	var country=document.forms['serviceOrderForm'].elements['country'+aid].value;		
	var exchangeRate=document.forms['serviceOrderForm'].elements['exchangeRate'+aid].value;
	var localAmount=document.forms['serviceOrderForm'].elements['localAmount'+aid].value;
	var valueDate=document.forms['serviceOrderForm'].elements['valueDate'+aid].value;
		if(country!=''){
			if(type=='FX'&& FXType=='Pay' && billingCurrency){
			var ss=findExchangeRateGlobal(country);
			exchangeRate=parseFloat(ss);
			document.forms['serviceOrderForm'].elements['exchangeRate'+aid].value=exchangeRate;
			document.forms['serviceOrderForm'].elements['valueDate'+aid].value=currentDateGlobal();
			}
			localAmount=actualExpense*exchangeRate;
		   document.forms['serviceOrderForm'].elements['localAmount'+aid].value=localAmount;	 		
		}
	 	  	var actualTotalRevenue=0.00;
	 		var actualTotalExpense=0.00;
	 		var tempActualTotalExpense=0.00;
	 		var tempActualTotalRevenue=0.00;
	 		var actualGrossMargin=0.00;
	 		var actualGrossMarginPercentage=0.00;
	 		var aidListIds=document.forms['serviceOrderForm'].elements['aidListIds'].value;
			var accArr=aidListIds.split(",");
			for(var g=0;g<accArr.length;g++){
			 	tempActualTotalRevenue="0.00";
			 	if(document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value!=''){
			 		tempActualTotalRevenue=document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value;
			 	}			 	
			 	actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
			 	tempActualTotalExpense="0.00";
			 	if(document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value!=''){
			 		tempActualTotalExpense=document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value;
			 	}
			 	actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
			}
		actualGrossMargin=actualTotalRevenue-actualTotalExpense;
		try{
			if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
				actualGrossMarginPercentage=0.00;
			}else{
				actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
			}
		}catch(e){
			actualGrossMarginPercentage=0.00;
			}
		 <c:if test="${not empty serviceOrder.id}">
		document.forms['serviceOrderForm'].elements['projectedActualRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*10000)/10000;
		document.forms['serviceOrderForm'].elements['projectedActualExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*10000)/10000;		
		</c:if>
 	  	 actualTotalRevenue=0.00;
 		 actualTotalExpense=0.00;
 		 tempActualTotalExpense=0.00;
 		 tempActualTotalRevenue=0.00;
 		 actualGrossMargin=0.00;
 		 actualGrossMarginPercentage=0.00;
 		var tempRecInvoiceNumber='';
		var tempPayingStatus=''; 		 
 		for(var g=0;g<accArr.length;g++){
	 		tempRecInvoiceNumber=document.forms['serviceOrderForm'].elements['recInvoiceNumber'+accArr[g]].value; 
	 	 	if(tempRecInvoiceNumber!=undefined && tempRecInvoiceNumber!=null && tempRecInvoiceNumber.trim()!=''){
			 	if(document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value!=""){
			 		tempActualTotalRevenue=document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value;
			 	}
			 	actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
	 	 	}
	 	 	tempPayingStatus=document.forms['serviceOrderForm'].elements['payingStatus'+accArr[g]].value;
	 	 	if(tempPayingStatus!=undefined && tempPayingStatus!=null && tempPayingStatus.trim()!='' &&( tempPayingStatus.trim()=='A' || tempPayingStatus.trim()=='I')){
			 	if(document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value!=""){
			 		tempActualTotalExpense=document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value;
			 	}
		 	actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
	 	 	} 	
 		}
	actualGrossMargin=actualTotalRevenue-actualTotalExpense;
	try{
		if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
			actualGrossMarginPercentage=0.00;
		}else{
			actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
		}
	}catch(e){
		actualGrossMarginPercentage=0.00;
		}		
	 <c:if test="${not empty serviceOrder.id}">
		document.forms['serviceOrderForm'].elements['actualTotalRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*10000)/10000;
		document.forms['serviceOrderForm'].elements['actualTotalExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*10000)/10000;
		document.forms['serviceOrderForm'].elements['actualGrossMargin'+${serviceOrder.id}].value=Math.round(actualGrossMargin*10000)/10000;
		document.forms['serviceOrderForm'].elements['actualGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(actualGrossMarginPercentage*10000)/10000;
		</c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		   calculateVatSection('REC',aid,type);
		  </c:if>
		  <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		   calculateVatSection('PAY',aid,type);
		  </c:if>
		  </c:if>
		  }	
	 }
	 function calculateActualRevenue(aid,targetElement,type,field1,field2,field3) {
		 if(field1!='' && field2!='' & field3!=''){
			 rateOnActualizationDate(field1,field2,field3);
			 }	   
		  var chargeCode=document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
		  var vendorCode=document.forms['serviceOrderForm'].elements['vendorCode'+aid].value; 
		  var recGl=document.forms['serviceOrderForm'].elements['recGl'+aid].value;
		  var payGl=document.forms['serviceOrderForm'].elements['payGl'+aid].value;   
		  var actgCode=document.forms['serviceOrderForm'].elements['actgCode'+aid].value;
		  var acref="${accountInterface}";
		  acref=acref.trim();
		  if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
			  alert("Vendor Code Missing. Please select");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
			  alert("Charge code needs to be selected");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY' && acref =='Y'){
			  alert("Pay GL missing. Please select another charge code.");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC' && acref =='Y'){
			  alert("Receivable GL missing. Please select another charge code.");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
			  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
			  document.forms['serviceOrderForm'].elements[targetElement.name].value='';
		  }else{		   
		 	 
		 var basis=document.forms['serviceOrderForm'].elements['basis'+aid].value;
		 var baseRateVal=1;
		 var roundVal=0.00;
		 var recQuantity=document.forms['serviceOrderForm'].elements['recQuantity'+aid].value;
		 var recRate=0.00;
		 recRate=document.forms['serviceOrderForm'].elements['recRate'+aid].value;
		 if(recRate=='') {
			 recRate = document.forms['serviceOrderForm'].elements['recRate'+aid].value=0.00;
	         }
		 var actualRevenue=document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value;
		 var actualExpense=document.forms['serviceOrderForm'].elements['actualExpense'+aid].value;
	     var actualDiscount=0.00;
	     try{
	    	 actualDiscount=document.forms['serviceOrderForm'].elements['actualDiscount'+aid].value;
	     }catch(e){}
		 if(recQuantity=='' ||recQuantity=='0' ||recQuantity=='0.0' ||recQuantity=='0.00') {
			 recQuantity = document.forms['serviceOrderForm'].elements['recQuantity'+aid].value=1;
	         } 
	     if( basis=="cwt" || basis=="%age"){
	       	 baseRateVal=100;
	     }else if(basis=="per 1000"){
	       	 baseRateVal=1000;
	     } else {
	       	 baseRateVal=1;  	
		 }
	     actualRevenue=(recQuantity*recRate)/baseRateVal;
	 	    if(actualDiscount!=0.00){
	 	    	actualRevenue=(actualRevenue*actualDiscount)/100;
		    }
	 	   document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value=actualRevenue;
	 	  <c:if test="${contractType}">
			var contractCurrency=document.forms['serviceOrderForm'].elements['contractCurrency'+aid].value;		
			var contractExchangeRate=document.forms['serviceOrderForm'].elements['contractExchangeRate'+aid].value;
			var contractRate=document.forms['serviceOrderForm'].elements['contractRate'+aid].value;
			var contractRateAmmount=document.forms['serviceOrderForm'].elements['contractRateAmmount'+aid].value;
			var contractValueDate=document.forms['serviceOrderForm'].elements['contractValueDate'+aid].value;
		 	if(contractCurrency!=''){
		 		if(type=='FX'){
		 		var ss=findExchangeRateGlobal(contractCurrency);
		 		contractExchangeRate=parseFloat(ss);
		 		document.forms['serviceOrderForm'].elements['contractExchangeRate'+aid].value=contractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['contractValueDate'+aid].value=currentDateGlobal();
		 		}
		 		contractRate=recRate*contractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['contractRate'+aid].value=contractRate;
		 		contractRateAmmount=(recQuantity*contractRate)/baseRateVal;
		  	    if(actualDiscount!=0.00){
		  	    	contractRateAmmount=(contractRateAmmount*actualDiscount)/100;
		 	    }
		  	   document.forms['serviceOrderForm'].elements['contractRateAmmount'+aid].value=contractRateAmmount;	 		
		 	}
	 	  </c:if>
			var recRateCurrency=document.forms['serviceOrderForm'].elements['recRateCurrency'+aid].value;		
			var recRateExchange=document.forms['serviceOrderForm'].elements['recRateExchange'+aid].value;
			var recCurrencyRate=document.forms['serviceOrderForm'].elements['recCurrencyRate'+aid].value;
			var actualRevenueForeign=document.forms['serviceOrderForm'].elements['actualRevenueForeign'+aid].value;
			var racValueDate=document.forms['serviceOrderForm'].elements['racValueDate'+aid].value;
		 	if(recRateCurrency!=''){
		 		if(type=='FX'){
		 		var ss=findExchangeRateGlobal(recRateCurrency);
		 		recRateExchange=parseFloat(ss);
		 		document.forms['serviceOrderForm'].elements['recRateExchange'+aid].value=recRateExchange;
		 		document.forms['serviceOrderForm'].elements['racValueDate'+aid].value=currentDateGlobal();
		 		}
		 		recCurrencyRate=recRate*recRateExchange;
		 		document.forms['serviceOrderForm'].elements['recCurrencyRate'+aid].value=recCurrencyRate;
		 		actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
		  	    if(actualDiscount!=0.00){
		  	    	actualRevenueForeign=(actualRevenueForeign*actualDiscount)/100;
		 	    }
		  	   document.forms['serviceOrderForm'].elements['actualRevenueForeign'+aid].value=actualRevenueForeign;	 		
		 	}

	<c:if test="${contractType}">
	var payableContractCurrency=document.forms['serviceOrderForm'].elements['payableContractCurrency'+aid].value;		
	var payableContractExchangeRate=document.forms['serviceOrderForm'].elements['payableContractExchangeRate'+aid].value;
	var payableContractRateAmmount=document.forms['serviceOrderForm'].elements['payableContractRateAmmount'+aid].value;
	var payableContractValueDate=document.forms['serviceOrderForm'].elements['payableContractValueDate'+aid].value;
		if(payableContractCurrency!=''){
			if(type=='FX'){
			var ss=findExchangeRateGlobal(payableContractCurrency);
			payableContractExchangeRate=parseFloat(ss);
			document.forms['serviceOrderForm'].elements['payableContractExchangeRate'+aid].value=payableContractExchangeRate;
			document.forms['serviceOrderForm'].elements['payableContractValueDate'+aid].value=currentDateGlobal();
			}
			payableContractRateAmmount=actualExpense*payableContractExchangeRate;
		   document.forms['serviceOrderForm'].elements['payableContractRateAmmount'+aid].value=payableContractRateAmmount;	 		
		}
	</c:if>
	var country=document.forms['serviceOrderForm'].elements['country'+aid].value;		
	var exchangeRate=document.forms['serviceOrderForm'].elements['exchangeRate'+aid].value;
	var localAmount=document.forms['serviceOrderForm'].elements['localAmount'+aid].value;
	var valueDate=document.forms['serviceOrderForm'].elements['valueDate'+aid].value;
		if(country!=''){
			if(type=='FX'){
			var ss=findExchangeRateGlobal(country);
			exchangeRate=parseFloat(ss);
			document.forms['serviceOrderForm'].elements['exchangeRate'+aid].value=exchangeRate;
			document.forms['serviceOrderForm'].elements['valueDate'+aid].value=currentDateGlobal();
			}
			localAmount=actualExpense*exchangeRate;
		   document.forms['serviceOrderForm'].elements['localAmount'+aid].value=localAmount;	 		
		}
	 	  	var actualTotalRevenue=0.00;
	 		var actualTotalExpense=0.00;
	 		var tempActualTotalExpense=0.00;
	 		var tempActualTotalRevenue=0.00;
	 		var actualGrossMargin=0.00;
	 		var actualGrossMarginPercentage=0.00;
	 		var aidListIds=document.forms['serviceOrderForm'].elements['aidListIds'].value;
			var accArr=aidListIds.split(",");
			for(var g=0;g<accArr.length;g++){
			 	tempActualTotalRevenue="0.00";
			 	if(document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value!=''){
			 		tempActualTotalRevenue=document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value;
			 	}			 	
			 	actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
			 	tempActualTotalExpense="0.00";
			 	if(document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value!=''){
			 		tempActualTotalExpense=document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value;
			 	}
			 	actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
			}
		actualGrossMargin=actualTotalRevenue-actualTotalExpense;
		try{
			if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
				actualGrossMarginPercentage=0.00;
			}else{
				actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
			}
		}catch(e){
			actualGrossMarginPercentage=0.00;
			}
		 <c:if test="${not empty serviceOrder.id}">
		document.forms['serviceOrderForm'].elements['projectedActualRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*10000)/10000;
		document.forms['serviceOrderForm'].elements['projectedActualExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*10000)/10000;		
		</c:if>
 	  	 actualTotalRevenue=0.00;
 		 actualTotalExpense=0.00;
 		 tempActualTotalExpense=0.00;
 		 tempActualTotalRevenue=0.00;
 		 actualGrossMargin=0.00;
 		 actualGrossMarginPercentage=0.00;
 		var tempRecInvoiceNumber='';
		var tempPayingStatus=''; 		 
 		for(var g=0;g<accArr.length;g++){
	 		tempRecInvoiceNumber=document.forms['serviceOrderForm'].elements['recInvoiceNumber'+accArr[g]].value; 
	 	 	if(tempRecInvoiceNumber!=undefined && tempRecInvoiceNumber!=null && tempRecInvoiceNumber.trim()!=''){
			 	if(document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value!=""){
			 		tempActualTotalRevenue=document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value;
			 	}
			 	actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
	 	 	}
	 	 	tempPayingStatus=document.forms['serviceOrderForm'].elements['payingStatus'+accArr[g]].value;
	 	 	if(tempPayingStatus!=undefined && tempPayingStatus!=null && tempPayingStatus.trim()!='' &&( tempPayingStatus.trim()=='A' || tempPayingStatus.trim()=='I')){
			 	if(document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value!=""){
			 		tempActualTotalExpense=document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value;
			 	}
		 	actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
	 	 	} 	
 		}
	actualGrossMargin=actualTotalRevenue-actualTotalExpense;
	try{
		if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
			actualGrossMarginPercentage=0.00;
		}else{
			actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
		}
	}catch(e){
		actualGrossMarginPercentage=0.00;
		}		
	 <c:if test="${not empty serviceOrder.id}">
		document.forms['serviceOrderForm'].elements['actualTotalRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*10000)/10000;
		document.forms['serviceOrderForm'].elements['actualTotalExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*10000)/10000;
		document.forms['serviceOrderForm'].elements['actualGrossMargin'+${serviceOrder.id}].value=Math.round(actualGrossMargin*10000)/10000;
		document.forms['serviceOrderForm'].elements['actualGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(actualGrossMarginPercentage*10000)/10000;
		</c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		   calculateVatSection('REC',aid,type);
		  </c:if>
		  <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		   calculateVatSection('PAY',aid,type);
		  </c:if>
		  </c:if>
		  }	
	 }
	 function calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,field1,field2,field3) { 
		 if(field1!='' && field2!='' & field3!=''){
			 rateOnActualizationDate(field1,field2,field3);
			 }	   
		   var revenueValue=0;
		    var revenueRound=0;
		    var oldRevenue=0;
		    var oldRevenueSum=0;
		    var balanceRevenue=0;
		    try{
			 <c:if test="${contractType}">
			var estimateSellQuantity=document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
			 if(estimateSellQuantity=='' ||estimateSellQuantity=='0' ||estimateSellQuantity=='0.0' ||estimateSellQuantity=='0.00') {
				 document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value=1;
		         }			 
			 </c:if>
			}catch(e){}
		    oldRevenue=eval(document.forms['serviceOrderForm'].elements[revenue].value);
		    <c:if test="${not empty serviceOrder.id}">
		    oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
		    </c:if>
		    var basisValue = document.forms['serviceOrderForm'].elements[basis].value;
		    checkFloatNew('serviceOrderForm',quantity,'Nothing');
		    var quantityValue = document.forms['serviceOrderForm'].elements[quantity].value;
		    <c:if test="${contractType}"> 
		    checkFloatNew('serviceOrderForm','estimateSellQuantity'+aid,'Nothing');
		    quantityValue = document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
		    </c:if>
		    checkFloatNew('serviceOrderForm',sellRate,'Nothing');
		    var sellRateValue = document.forms['serviceOrderForm'].elements[sellRate].value;
		    if(basisValue=="cwt" || basisValue== "%age"){
		    revenueValue=(quantityValue*sellRateValue)/100 ;
		    }
		    else if(basisValue=="per 1000"){
		    revenueValue=(quantityValue*sellRateValue)/1000 ;
		    }else{
		    revenueValue=(quantityValue*sellRateValue);
		    } 
		    var estimateDiscount=0.00;
		    try{
		    	 estimateDiscount=document.forms['serviceOrderForm'].elements['estimateDiscount'+aid].value;
		    }catch(e){}
		    if(estimateDiscount!=0.00){
		    	revenueValue=(revenueValue*estimateDiscount)/100;
		    }	    
		    revenueRound=Math.round(revenueValue*10000)/10000;
		    balanceRevenue=revenueRound-oldRevenue;
		    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
		    balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
		    oldRevenueSum=oldRevenueSum+balanceRevenue; 
		    oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
		    var revenueRoundNew=""+revenueRound;
		    if(revenueRoundNew.indexOf(".") == -1){
		    revenueRoundNew=revenueRoundNew+".00";
		    } 
		    if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
		    revenueRoundNew=revenueRoundNew+"0";
		    }

		    document.forms['serviceOrderForm'].elements[revenue].value=revenueRoundNew;
		    try{
		        var buyRate1=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value;
		        var estCurrency1=document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value;
				if(estCurrency1.trim()!=""){
						var Q1=0;
						var Q2=0;
						Q1=buyRate1;
						Q2=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
			            var E1=Q1*Q2;
			            E1=Math.round(E1*10000)/10000;
			            document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=E1;
			            var estLocalAmt1=0;
			            if(basisValue=="cwt" || basisValue== "%age"){
			            	estLocalAmt1=(quantityValue*E1)/100 ;
			                }
			                else if(basisValue=="per 1000"){
			                	estLocalAmt1=(quantityValue*E1)/1000 ;
			                }else{
			                	estLocalAmt1=(quantityValue*E1);
			                }	 
			            estLocalAmt1=Math.round(estLocalAmt1*10000)/10000;    
			            document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
				}
				<c:if test="${contractType}"> 
					try{
					     var estimatePayableContractExchangeRate=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value;
					     var estLocalRate=document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;
					     var estExchangeRate=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value
					     var recCurrencyRate=(estimatePayableContractExchangeRate*estLocalRate)/estExchangeRate;
					     var roundValue=Math.round(recCurrencyRate*10000)/10000;
					     document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value=roundValue ;
						var Q11=0;
						var Q21=0;
						Q11=document.forms['serviceOrderForm'].elements[quantity].value;
					    <c:if test="${contractType}"> 
					    Q11 = document.forms['serviceOrderForm'].elements['estimateSellQuantity'+aid].value;
					    </c:if>
						Q21=document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
			            var E11=Q11*Q21;
			            var estLocalAmt11=0;
			            if(basisValue=="cwt" || basisValue== "%age"){
			            	estLocalAmt11=E11/100 ;
			                }
			                else if(basisValue=="per 1000"){
			                	estLocalAmt11=E11/1000 ;
			                }else{
			                	estLocalAmt11=E11;
			                }	 
			            estLocalAmt11=Math.round(estLocalAmt11*10000)/10000;    
			            document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value=estLocalAmt11;
					}catch(e){}     
				</c:if>
		    }catch(e){}
		    <c:if test="${not empty serviceOrder.id}">
		    var newRevenueSum=""+oldRevenueSum;
		    if(newRevenueSum.indexOf(".") == -1){
		    newRevenueSum=newRevenueSum+".00";
		    } 
		    if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
		    newRevenueSum=newRevenueSum+"0";
		    } 
		    document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
		    </c:if>
		    if(revenueRoundNew>0){
		    document.forms['serviceOrderForm'].elements[displayOnQuots].checked = true;
		    }
		    try{
		    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		    calculateVatAmt(estVatPer,revenue,estAmt);
		    </c:if>
		    }catch(e){}
		    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
		   }
	  function onlyRateAllowed(evt) { 
		  var keyCode = evt.which ? evt.which : evt.keyCode; 
		  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
		 }
	  function enterNotAllow(evt){
	  	var keyCode = evt.which ? evt.which : evt.keyCode;
	  	if(keyCode==16)
	  	{
	      	return false;
	  	} else {
	      	return true;
	  	}
	  }
		function checkFloat1(temp) {  
			  negativeCount = 0;
	    var check='';  
	    var i;
	    var dotcheck=0;
		var s = temp.value;
		if(s.indexOf(".") == -1){
		dotcheck=eval(s.length)
		}else{
		dotcheck=eval(s.indexOf(".")-1)
		}
		var fieldName = temp.name;  
		var count = 0;
		var countArth = 0;
	    for (i = 0; i < s.length; i++) {   
	        var c = s.charAt(i); 
	        if(c == '.') {
	        	count = count+1
	        }
	        if(c == '-') {
	        	countArth = countArth+1
	        }
	        if((i!=0)&&(c=='-')) {
	       	  alert("Invalid data." ); 
	          document.forms['serviceOrderForm'].elements[fieldName].select();
	          document.forms['serviceOrderForm'].elements[fieldName].value='';
	       	  return false;
	       	} 	
	        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))  {
	        	check='Invalid'; 
	        }
	    }
	    if(dotcheck>15){
	    check='tolong';
	    }
	    if(check=='Invalid'){ 
	    alert("Invalid data." ); 
	    document.forms['serviceOrderForm'].elements[fieldName].select();
	    document.forms['serviceOrderForm'].elements[fieldName].value='';
	    return false;
	    } else if(check=='tolong'){ 
	    alert("Data Too Long." ); 
	     document.forms['serviceOrderForm'].elements[fieldName].select();
	    document.forms['serviceOrderForm'].elements[fieldName].value='';
	    return false;
	    } else{
	    s=Math.round(s*10000)/10000; 
	    var value=""+s;
	    if(value.indexOf(".") == -1){
	    value=value+".0000";
	    }  
	    while((value.indexOf(".")+5 != value.length)){
	    value=value+"0";
	    } 
	    document.forms['serviceOrderForm'].elements[fieldName].value=value;
	    return true;
	    }
	    }
	    function checkFloatForQuantity(temp) {  
			  negativeCount = 0;
	    var check='';  
	    var i;
	    var dotcheck=0;
		var s = temp.value;
		if(s.indexOf(".") == -1){
		dotcheck=eval(s.length)
		}else{
		dotcheck=eval(s.indexOf(".")-1)
		}
		var fieldName = temp.name;  
		var count = 0;
		var countArth = 0;
	    for (i = 0; i < s.length; i++) {   
	        var c = s.charAt(i); 
	        if(c == '.') {
	        	count = count+1
	        }
	        if(c == '-') {
	        	countArth = countArth+1
	        }
	        if((i!=0)&&(c=='-')) {
	       	  alert("Invalid data." ); 
	          document.forms['serviceOrderForm'].elements[fieldName].select();
	          document.forms['serviceOrderForm'].elements[fieldName].value='';
	       	  return false;
	       	} 	
	        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))  {
	        	check='Invalid'; 
	        }
	    }
	    if(dotcheck>15){
	    check='tolong';
	    }
	    if(check=='Invalid'){ 
	    alert("Invalid data." ); 
	    document.forms['serviceOrderForm'].elements[fieldName].select();
	    document.forms['serviceOrderForm'].elements[fieldName].value='';
	    return false;
	    } else if(check=='tolong'){ 
	    alert("Data Too Long." ); 
	     document.forms['serviceOrderForm'].elements[fieldName].select();
	    document.forms['serviceOrderForm'].elements[fieldName].value='';
	    return false;
	    } else{
	    s=Math.round(s*100)/100; 
	    var value=""+s;
	    if(value.indexOf(".") == -1){
	    value=value+".00";
	    }  
	    while((value.indexOf(".")+3 != value.length)){
	    value=value+"0";
	    } 
	    document.forms['serviceOrderForm'].elements[fieldName].value=value;
	    return true;
	    }
	    }
	    function getAccountlineField(aid,basis,quantity,chargeCode,contract,vendorCode){
	    	var oForm = document.forms[0].name; 

	    	  accountLineBasis=document.forms['serviceOrderForm'].elements[basis].value;
	    	  accountLineEstimateQuantity=document.forms['serviceOrderForm'].elements[quantity].value;
	    	  chargeCode=document.forms['serviceOrderForm'].elements[chargeCode].value;
	    	  contract=document.forms['serviceOrderForm'].elements[contract].value;
	    	  vendorCode=document.forms['serviceOrderForm'].elements[vendorCode].value;
	    	  <c:if test="${contractType}">
	    	  var estimatePayableContractCurrencyTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value; 
	    	  var estimatePayableContractValueDateTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractValueDateNew'+aid].value;
	    	  var estimatePayableContractExchangeRateTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
	    	  var estimatePayableContractRateTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
	    	  var estimatePayableContractRateAmmountTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value;
			  </c:if>
	    	  var estCurrencyTemp=document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value; 
	    	  var estValueDateTemp=document.forms['serviceOrderForm'].elements['estValueDateNew'+aid].value;
	    	  var estExchangeRateTemp=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
	    	  var estLocalRateTemp=document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value;
	    	  var estLocalAmountTemp=document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value;

	    	  var estimateQuantityTemp=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
	    	  var estimateRateTemp=document.forms['serviceOrderForm'].elements['estimateRate'+aid].value;
	    	  var estimateExpenseTemp=document.forms['serviceOrderForm'].elements['estimateExpense'+aid].value;
	    	  var basisTemp=document.forms['serviceOrderForm'].elements['basis'+aid].value;
	    	  <c:if test="${contractType}">
	    	  openWindow('getAccountlineField.html?formName='+oForm+'&aid='+aid+'&basisTemp='+encodeURI(basisTemp)+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estimatePayableContractCurrencyTemp='+estimatePayableContractCurrencyTemp+'&estimatePayableContractValueDateTemp='+estimatePayableContractValueDateTemp+'&estimatePayableContractExchangeRateTemp='+estimatePayableContractExchangeRateTemp+'&estimatePayableContractRateTemp='+estimatePayableContractRateTemp+'&estimatePayableContractRateAmmountTemp='+estimatePayableContractRateAmmountTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	    	  </c:if>
	    	  <c:if test="${!contractType}"> 
	    	  openWindow('getAccountlineField.html?formName='+oForm+'&aid='+aid+'&basisTemp='+encodeURI(basisTemp)+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	    	  </c:if>
	    	  }
	  function changeEstimatePassPercentage(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt){
	       var estimate=0; 
	       var estimateround=0;
	       var estimateRevenue=0;
	       var estimateRevenueRound=0;
	       var sellRateValue=0;
	       var sellRateround=0;
	       var	estimatepasspercentage =0;
	       var quantityValue = 0;
	       var oldRevenue=0;
	       var oldRevenueSum=0;
	       var balanceRevenue=0; 
	       var buyRateValue=0;  
		   oldRevenue=eval(document.forms['serviceOrderForm'].elements[revenue].value); 
		    <c:if test="${not empty serviceOrder.id}">
	        oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	       </c:if> 
	       quantityValue = eval(document.forms['serviceOrderForm'].elements[quantity].value); 
	       buyRateValue = eval(document.forms['serviceOrderForm'].elements[buyRate].value); 
	       estimatepasspercentage= document.forms['serviceOrderForm'].elements[estimatePassPercentage].value;
	       estimate = (quantityValue*buyRateValue); 
	   	   if(quantityValue>0 && estimate>0){
	   	   if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age"){
	       		estimate = estimate/100; 	  	
	       	} 
	       	if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000"){
	       		estimate = estimate/1000; 	  	
	       	}
	       	estimateRevenue=((estimate)*estimatepasspercentage/100)
	       	estimateRevenueRound=Math.round(estimateRevenue*10000)/10000; 
	  	    document.forms['serviceOrderForm'].elements[revenue].value=estimateRevenueRound;   
		    if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age"){
	       	  sellRateValue=(estimateRevenue/quantityValue)*100;
	       	  sellRateround=Math.round(sellRateValue*10000)/10000;
	       	  
	       	  document.forms['serviceOrderForm'].elements[sellRate].value=sellRateround;	  	
	       	}
		    else if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000"){
	       	  sellRateValue=(estimateRevenue/quantityValue)*1000;
	       	  sellRateround=Math.round(sellRateValue*10000)/10000; 
	       	  document.forms['serviceOrderForm'].elements[sellRate].value=sellRateround;	  	
	       	} else if(document.forms['serviceOrderForm'].elements[basis].value!="cwt" || document.forms['serviceOrderForm'].elements[basis].value!="%age"||document.forms['serviceOrderForm'].elements[basis].value!="per 1000")
	        {
	          sellRateValue=(estimateRevenue/quantityValue);
	       	  sellRateround=Math.round(sellRateValue*10000)/10000; 
	          document.forms['serviceOrderForm'].elements[sellRate].value=sellRateround;	  	
		    }
		    balanceRevenue=estimateRevenueRound-oldRevenue;
	        oldRevenueSum=Math.round(oldRevenueSum*10000)/10000;
	        balanceRevenue=Math.round(balanceRevenue*10000)/10000; 
	        oldRevenueSum=oldRevenueSum+balanceRevenue;  
	        <c:if test="${not empty serviceOrder.id}">
	        var newRevenueSum=""+oldRevenueSum;
	        if(newRevenueSum.indexOf(".") == -1){
	        newRevenueSum=newRevenueSum+".00";
	        } 
	        if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
	        newRevenueSum=newRevenueSum+"0";
	        } 
	        document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
	        </c:if>
	       if(estimateRevenueRound>0){
	        document.forms['serviceOrderForm'].elements[displayOnQuots].checked = true;
	       }
	       try{
	    	    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
	    	    calculateVatAmt(estVatPer,revenue,estAmt);
	    	    </c:if>
	    	    }catch(e){}
		 calculateGrossMargin();
	}
	else{
	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=0;}} 	
	 	function onlyNumsAllowed1(evt){
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
		}
	function discriptionView(discription){ 
	 document.getElementById(discription).style.height="50px"; 
	 } 
	 function discriptionView1(discription){ 
	 document.getElementById(discription).style.height="15px"; 
	 SetCursorToTextEnd(discription);
	 }
	 function SetCursorToTextEnd(textControlID){ 
		var text = document.getElementById(textControlID); 
		text.scrollTop = 1;
	   }
	 function selectiveInvoiceCheck(rowId,targetElement){
		 progressBarAutoSave('1');
	     var selectiveInvoice=true;
		 if(targetElement.checked==false)
	     {
			 selectiveInvoice=false;
	     }
		 var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	     var url="updateSelectiveInvoice.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&id="+rowId+"&selectiveInvoice="+selectiveInvoice;
	     httpCMMAgentInvoiceCheck.open("GET", url, true);
	     httpCMMAgentInvoiceCheck.onreadystatechange = selectiveInvoiceCheckResponse;
	     httpCMMAgentInvoiceCheck.send(null);
	 }
	 function selectiveInvoiceCheckResponse(){
		  if (httpCMMAgentInvoiceCheck.readyState == 4)
	     {
	        var results = httpCMMAgentInvoiceCheck.responseText  
	     }
		  progressBarAutoSave('0');
	 }
	 function selectAllActive(str){ 
		 <c:set var="DMMContractTypeValue" value="false"/> 
			var billingDMMContractTypePage1 = document.forms['serviceOrderForm'].elements['billingDMMContractTypePage'].value; 
			if(billingDMMContractTypePage1=='true'){
				<c:set var="DMMContractTypeValue" value="true"/> 
			}
			<c:choose> 
			 <c:when test="${((trackingStatus.accNetworkGroup) && (DMMContractTypeValue) && (trackingStatus.soNetworkGroup) )}"> 
			 alert("You cannot deactivate all accountlines because this is a DMM order. May be in UTSI instance, few accountlines have been invoiced.")
			 document.getElementById('selectallActive').checked=true;
			 </c:when>
		     <c:otherwise> 
			var id = idList.split(",");
			  for (i=0;i<id.length;i++){
				  if(str.checked == true){
					  try{
						  if(!document.getElementById('statusCheck'+id[i]).disabled){
							  document.getElementById('statusCheck'+id[i].trim()).checked = true;
							  if(document.getElementById('Inactivate')!=undefined){
							  document.getElementById('Inactivate').disabled = true; 
							  } 
						  }
					  }catch(e){}
					
				  }else{
					  try{
						  if(!document.getElementById('statusCheck'+id[i]).disabled){
							  document.getElementById('statusCheck'+id[i].trim()).checked = false;
							  if(document.getElementById('Inactivate')!=undefined){
							  document.getElementById('Inactivate').disabled = false; 
							  } 
						  }
					  }catch(e){}
					 
				  } }</c:otherwise></c:choose> }
	function inactiveStatusCheck(rowId,targetElement){
		<c:set var="DMMContractTypeValue" value="false"/> 
		var billingDMMContractTypePage1 = document.forms['serviceOrderForm'].elements['billingDMMContractTypePage'].value; 
		if(billingDMMContractTypePage1=='true'){
			<c:set var="DMMContractTypeValue" value="true"/> 
		}
		<c:choose> 
		 <c:when test="${((trackingStatus.accNetworkGroup) && (DMMContractTypeValue) && (trackingStatus.soNetworkGroup) )}"> 
		 progressBarAutoSave('1');
	     var url="checkNetworkAgentInvoice.html?ajax=1&decorator=simple&popup=true&networkAgentId=" + encodeURI(rowId);
	     httpNetworkAgent.open("GET", url, true);
	     httpNetworkAgent.onreadystatechange = function(){ checkNetworkAgentInvoiceResponse(rowId,targetElement);}; 
	     httpNetworkAgent.send(null);
	     </c:when>
	     <c:otherwise>  
		   if(targetElement.checked==false){
		      var userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
		      if(userCheckStatus == ''){
			  	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = rowId;
		      }else{
		       var userCheckStatus=	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus + ',' + rowId;
		      document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
		      }}
		   if(targetElement.checked){
		     var userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
		     var userCheckStatus=document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( rowId , '' );
		     userCheckStatus=userCheckStatus.replace( ',,' , ',' )
		     var len=userCheckStatus.length-1;
		     if(len==userCheckStatus.lastIndexOf(",")){
		    	 userCheckStatus=userCheckStatus.substring(0,len);
		         }if(userCheckStatus.indexOf(",")==0){
		    	 userCheckStatus=userCheckStatus.substring(1,userCheckStatus.length);}
		     document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus;} 
		     accountIdCheck(); </c:otherwise></c:choose>}
	function checkNetworkAgentInvoiceResponse(rowId,targetElement){
		
		  if (httpNetworkAgent.readyState == 4)
	      {
			
	       var results = httpNetworkAgent.responseText
	       results = results.trim(); 
	       if(results=='true'){
	    	   alert("This account line cannot be deactivated as either it has been transferred to Accounting and/or Invoiced in UTSI Instance "); 
	    	   document.getElementById('statusCheck'+rowId).checked=true;
	       }else{
	    	   if(targetElement.checked==false){
	 		      var userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
	 		      if(userCheckStatus == ''){
	 			  	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = rowId;
	 		      }else{
	 		       var userCheckStatus=	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus + ',' + rowId;
	 		      document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
	 		      }}
	 		   if(targetElement.checked){
	 		     var userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
	 		     var userCheckStatus=document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( rowId , '' );
	 		     userCheckStatus=userCheckStatus.replace( ',,' , ',' )
	 		     var len=userCheckStatus.length-1;
	 		     if(len==userCheckStatus.lastIndexOf(",")){
	 		    	 userCheckStatus=userCheckStatus.substring(0,len);
	 		         }if(userCheckStatus.indexOf(",")==0){
	 		    	 userCheckStatus=userCheckStatus.substring(1,userCheckStatus.length);}
	 		     document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus;} 
	 		     accountIdCheck();
	       }
	    }
		  
		  progressBarAutoSave('0');
	}
	 function accountIdCheck(){
	    var accountIdCheck = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
	    accountIdCheck=accountIdCheck.trim(); 
	    if(accountIdCheck==''){
	    	 if(document.getElementById('Inactivate')!=undefined){
	        document.forms['serviceOrderForm'].elements['Inactivate'].disabled=true;
	    	 }
	        document.getElementById("selectallActive").checked=true;
	    }else if(accountIdCheck==','){
	    	 if(document.getElementById('Inactivate')!=undefined){
	    document.forms['serviceOrderForm'].elements['Inactivate'].disabled=true;
	    	 }
	    document.getElementById("selectallActive").checked=true;
	    }else if(accountIdCheck!=''){
	    	 if(document.getElementById('Inactivate')!=undefined){
	      document.forms['serviceOrderForm'].elements['Inactivate'].disabled=false;
	    	 }
	      document.getElementById("selectallActive").checked=false;
	    }}   
	 function findDefaultLine(){
	     try{ 
		document.forms['serviceOrderForm'].elements['jobtypeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
		document.forms['serviceOrderForm'].elements['routingSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
		document.forms['serviceOrderForm'].elements['commoditySO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
		document.forms['serviceOrderForm'].elements['serviceTypeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
		document.forms['serviceOrderForm'].elements['companyDivisionSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
		document.forms['serviceOrderForm'].elements['modeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
		document.forms['serviceOrderForm'].elements['packingModeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
		} 
	catch(e){}
		var customerFileContract=document.forms['serviceOrderForm'].elements['billing.contract'].value;
		if(customerFileContract==''){ 
		 		alert("Please select  Pricing Contract from billing Detail form"); 
		 }
		else{
		  document.forms['serviceOrderForm'].action = 'addServiceOrderPricingDefaultLine.html?pricingButton=yes&sid=${serviceOrder.id}';
		  showOrHide(1);
			var check=false;
			 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
				 var check = true;
			 }else{
				var check =saveValidation('saveButton');
			 }
	 	  if(check){
	      document.forms['serviceOrderForm'].submit();
	      }
		}}
	function defaultLineMassage(){
	alert("Default pricing template have already been added.");
	}
	 function addPricingLine() {
	    document.forms['serviceOrderForm'].action ='addServiceOrderPricingLine.html?pricingButton=yes&sid=${serviceOrder.id}';
		var check=false;
		 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
			 var check = true;
		 }else{
			var check =saveValidation('saveButton');
		 }
	 	if(check){
	    document.forms['serviceOrderForm'].submit();
	    } 
	    }
	function savePricingLine() {
	    document.forms['serviceOrderForm'].action ='savePricingLineSO.html?pricingButton=yes&sid=${serviceOrder.id}';
		var check=false;
		 if(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value=='CNCL'){
			 var check = true;
		 }else{
			var check =saveValidation('saveButton');
		 }
	    if(check){
	    document.forms['serviceOrderForm'].submit();
	    } 
	    }    
	  function updateAccInactive() {
		 var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
		 document.forms['serviceOrderForm'].action ='deletePricingLineSO.html?pricingButton=yes&sid=${serviceOrder.id}';
	     document.forms['serviceOrderForm'].submit(); 
		    } 	
	  function checkCompanyDiv(id){
		  var companyDivision = validateFieldValue('companyDivision'+id);
		  if(companyDivision==null || companyDivision==undefined || companyDivision.trim()==''){
			  alert("Company Division is required field");
			  companyDivision = validateFieldValue('tempCompanyDivision'+id);
			 document.forms['serviceOrderForm'].elements['companyDivision'+id].value=companyDivision;
		  }else if(validateFieldValue('payAccDate'+id)!='' || validateFieldValue('recAccDate'+id)!='') { 	 
			  	alert('You can not change the CompanyDivision as Sent To Acc has been already filled');
			 	 companyDivision = validateFieldValue('tempCompanyDivision'+id);
				 document.forms['serviceOrderForm'].elements['companyDivision'+id].value=companyDivision;
		  }else{
		  }
	  }
	  function saveValidation(type){
		  var ChargesMandatory='${checkContractChargesMandatory}';
		  var contractType = '${contractType}';
		  var routingflag=false;
		  var checkAccessQuo='${checkAccessQuotation}';
		  var moveTypeVal = '${serviceOrder.moveType}';	
		   var qualityFlag='${customerFeedbackFlag1}';
		   var billingContract = '${billing.contract}';
		   var deliveryDate="";
		   var oaCoordinatoreval="";
		   var daCoordinatoreval="";
		   var CoordinatorEvalValidation="";
		   <configByCorp:fieldVisibility componentId="component.QualitySurvey.CoordinatorEval.validation">
		   if(qualityFlag == 'Quality Survey'){
			  oaCoordinatoreval=document.forms['serviceOrderForm'].elements['serviceOrder.oaCoordinatorEval'].value;
			  daCoordinatoreval=document.forms['serviceOrderForm'].elements['serviceOrder.daCoordinatorEval'].value;
			  CoordinatorEvalValidation=document.forms['serviceOrderForm'].elements['CoordinatorEvalValidation'].value;
			  }
		   </configByCorp:fieldVisibility> 
		   <c:if test="${not empty trackingStatus.deliveryA}">
		   deliveryDate="Y";
		   </c:if>
		   
		  <c:if test="${costElementFlag}">
		  routingflag=true;
		  </c:if>
	      var routing="";
	      try{
	    	  routing=document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
	      }catch(e){}		  
	  	document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].disabled=false;
	      var lastName="";
	      var ChargesMandatory='${checkContractChargesMandatory}';
	      try{
	      	lastName=document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
	      }catch(e){}
	      var originCountry="";
	      try{
	      	originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	      }catch(e){}
	      var companyDivision="";
	      try{
	      	companyDivision=document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	      }catch(e){}
	      var destinationCountry="";
	      try{
	      	destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	      }catch(e){}
	      var status="";
	      try{
	      	status=document.forms['serviceOrderForm'].elements['serviceOrder.status'].value;
	      }catch(e){}
	      var job="";
	      try{
	      	job=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	      }catch(e){}
	      var coordinator="";
	      try{
	      	coordinator=document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].value;
	      }catch(e){}
	      var commodity="";
	      try{
	      	commodity=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
	      }catch(e){}
	      var originState="";
	      try{
	      	originState=document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
	      }catch(e){}
	      var originCity="";
	      try{
	      	originCity=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
	      }catch(e){}
	      var originZip="";
	      try{
	      	originZip=document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
	      }catch(e){}
	      var destinationState="";
	      try{
	      	destinationState=document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
	      }catch(e){}
	      var destinationCity="";
	      try{
	      	destinationCity=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
	      }catch(e){}
	      var distance="";
	      try{
	      	distance=document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value;
	      }catch(e){}
	      var distanceInKmMiles="";
	      try{
	      	distanceInKmMiles=document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].value;
	      }catch(e){}
	      var mode="";
	      try{
	      	mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
	      }catch(e){}   
    if(status!='CNCL' && billingContract=='' && (type=='AddLine' || type=='AddDefault')){
		alert("There is no pricing contract in billing: Please select.");
		 showOrHideAutoSave('0');
		 return false;		
	}else if((lastName=='')&&(status='CNCL')&&(type=='saveButton')){
		   alert("Last name is required field");
		   document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].disabled=true;
		   showOrHideAutoSave('0');
		   return false;
	}else if((routing=='')&&(job!='RLO')&&(status!='CNCL') && routingflag && inactiveCheck!='Inactive'){
		 alert("Routing is required field.");
		  showOrHideAutoSave('0');
		  inactiveCheck="";
		 return false;
  	}else if((originCountry=='')&&(status!='CNCL')){
	 alert("Origin country is required field. ");
	 showOrHideAutoSave('0');
	 return false;
	}else if((companyDivision=='')&&(status!='CNCL')){
	 alert("Company Division is required field.");
	 showOrHideAutoSave('0');
	 return false;
	}else if((destinationCountry=='')&&(status!='CNCL')){
	 alert("Destination country is required field.");
	 showOrHideAutoSave('0');
	 return false;
	}else if((job=='')&&(status!='CNCL')){
	 alert("Job Type is required field.");
	 showOrHideAutoSave('0');
	 return false;
	}else if(CoordinatorEvalValidation =='Y' && (qualityFlag =='Quality Survey') && (deliveryDate =='Y') && (oaCoordinatoreval ==''))
		{
					alert("Coord Eval OA is a required field in Quality Survey.");
				  	showOrHideAutoSave('0');
				 	return false;  	 
		}
	else if(CoordinatorEvalValidation =='Y' && (qualityFlag =='Quality Survey') && (deliveryDate =='Y') && (daCoordinatoreval =='')){
		
				  alert("Coord Eval DA is a required field in Quality Survey.");
				  showOrHideAutoSave('0');
				  return false;
	} 
	else if((coordinator=='')&&(status!='CNCL')){
		if((checkAccessQuo==true || checkAccessQuo=='true') && moveTypeVal=='Quote'){
			return true;
		}else{
			alert("Coordinator is required field.");
			 showOrHideAutoSave('0');
			 return false;
		}
	}else if((commodity=='')&&(job!='RLO')&&(status!='CNCL')){
	 alert("Commodity is required field.");
	 showOrHideAutoSave('0');
	 return false;
	}else if((originState=='')&&(originCountry=="United States")&&(status!='CNCL')){
	 alert("State is a required field in Origin.");
	 showOrHideAutoSave('0');
	 return false;
	}else if((originCity=='')&&(status!='CNCL')){
	 alert("Origin city is required field.");
	 showOrHideAutoSave('0');
	 return false;
	}else if((originZip=='')&&(originCountry=="United States")&&(status!='CNCL')){
	 alert("Origin Postal Code is required field.");
	 showOrHideAutoSave('0');
	 return false;
	}else if((destinationState=='')&&(destinationCountry=="United States")&&(status!='CNCL')){
	 alert("State is a required field in Destination.");
	 showOrHideAutoSave('0');
	 return false;
	}else if((destinationCity=='')&&(status!='CNCL')){
	 alert("Destination city is required field.");
	 showOrHideAutoSave('0');
	 return false;
	}else if((distance!='')&& (distanceInKmMiles=='') && (mode=='Overland'||mode=='Truck')){
		 alert("Distance Unit is required field.");
		  showOrHideAutoSave('0');
		 return false;  
	}else if((ChargesMandatory=='1' || contractType=='true') && type!='Inactive'){
		if(idList!=''){			
			 var id = idList.split(",");
			 for (var i=0;i<id.length;i++){
				var test=document.forms['serviceOrderForm'].elements['chargeCode'+id[i].trim()].value;
				if(test.trim()==''){
					var lineNo=document.forms['serviceOrderForm'].elements['accountLineNumber'+id[i].trim()].value;
					 alert("Please select Charge Code for Pricing line # "+lineNo);
					 showOrHideAutoSave('0');
					 return false;
				}
			 }
			 return true; 
			}else{
				return true;	
			}
    }else{
    <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.salesManMandatory">
	       var salesman="";
	     salesman=document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].value;
	     if(salesman==''){
		 alert("Sales Person is required field.");
		 showOrHideAutoSave('0');
		 return false;
		}
	 </configByCorp:fieldVisibility>
	 return true;
	    }
	}  
	  function calculateestimatePassPercentage(revenue,expense,estimatePassPercentage){
	    var revenueValue=0;
	    var expenseValue=0;
	    var estimatePassPercentageValue=0; 
	    checkFloatNew('serviceOrderForm',estimatePassPercentage,'Nothing'); 
	    revenueValue=eval(document.forms['serviceOrderForm'].elements[revenue].value);
	    expenseValue=eval(document.forms['serviceOrderForm'].elements[expense].value); 
	    try{
	    	estimatePassPercentageValue=(revenueValue/expenseValue)*100;
	    }catch(e){
	    	estimatePassPercentageValue=0.00;
	        } 
	    estimatePassPercentageValue=Math.round(estimatePassPercentageValue);
	    if(document.forms['serviceOrderForm'].elements[expense].value == '' || document.forms['serviceOrderForm'].elements[expense].value == 0||document.forms['serviceOrderForm'].elements[expense].value == 0.00){
	  	   document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
	  	   }else{ 
	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value = estimatePassPercentageValue;
	  	   } 
	    calculateGrossMargin();
	    } 
	     function calculateEstimateRate(){
	    
	    var accountlineId=document.forms['serviceOrderForm'].elements['accId'].value
	    var accEstimateQuantity=document.forms['serviceOrderForm'].elements['accEstimateQuantity'].value
	    var accEstimateRate=document.forms['serviceOrderForm'].elements['accEstimateRate'].value 
	    var accEstimateExpense=document.forms['serviceOrderForm'].elements['accEstimateExpense'].value 
	    var expenseValue=0;
	    var expenseRound=0;
	    var oldExpense=0;
	    var oldExpenseSum=0;
	    var balanceExpense=0;
	    oldExpense=eval(document.forms['serviceOrderForm'].elements['estimateExpense'+accountlineId].value);
	    <c:if test="${not empty serviceOrder.id}">
	    oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
	    </c:if> 
	    expenseValue=accEstimateExpense;
	    expenseRound=Math.round(expenseValue*10000)/10000;
	    balanceExpense=expenseRound-oldExpense;
	    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
	    balanceExpense=Math.round(balanceExpense*10000)/10000; 
	    oldExpenseSum=oldExpenseSum+balanceExpense;
	    oldExpenseSum=Math.round(oldExpenseSum*10000)/10000;
	    var expenseRoundNew=""+expenseRound;
	    if(expenseRoundNew.indexOf(".") == -1){
	    expenseRoundNew=expenseRoundNew+".00";
	    } 
	    if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
	    expenseRoundNew=expenseRoundNew+"0";
	    } 
	    document.forms['serviceOrderForm'].elements['estimateExpense'+accountlineId].value=expenseRoundNew;
	    document.forms['serviceOrderForm'].elements['estimateRate'+accountlineId].value=accEstimateRate;
	    estimateQuantity=document.forms['serviceOrderForm'].elements['estimateQuantity'+accountlineId].value;
	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {   
	     document.forms['serviceOrderForm'].elements['estimateQuantity'+accountlineId].value=accEstimateQuantity;
	     }
	    <c:if test="${not empty serviceOrder.id}">
	    var newExpenseSum=""+oldExpenseSum;
	    if(newExpenseSum.indexOf(".") == -1){
	    newExpenseSum=newExpenseSum+".00";
	    } 
	    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
	    newExpenseSum=newExpenseSum+"0";
	    } 
	    document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
	    </c:if>
	    if(expenseRoundNew>0){
	    document.forms['serviceOrderForm'].elements['displayOnQuote'+accountlineId].checked = true;
	    }
	    try{
		    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		    calculateVatAmtSection('EST',accountlineId);
		    </c:if>
		    }catch(e){}
	    
	    var revenue='estimateRevenueAmount'+accountlineId;
	    var expense='estimateExpense'+accountlineId;
	    var estimatePassPercentage='estimatePassPercentage'+accountlineId;
	    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage); 
	    } 
	     function calculateGrossMargin(){
	    <c:if test="${not empty serviceOrder.id}">
	    var revenueValue=0;
	    var expenseValue=0;
	    var grossMarginValue=0; 
	    var grossMarginPercentageValue=0; 
	    revenueValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	    expenseValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
	    grossMarginValue=revenueValue-expenseValue;
	    grossMarginValue=Math.round(grossMarginValue*10000)/10000; 
	    document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
	    if(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
	  	    document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
	  	   }else{
	  	     grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
	  	     grossMarginPercentageValue=Math.round(grossMarginPercentageValue*10000)/10000; 
	  	   	document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
	  	   }
	  	</c:if>    
	    }
	     var accountLineIdScript=""; 
	  function chk(chargeCode,category,aid){
		  accountLineIdScript=aid;
			var val = document.forms['serviceOrderForm'].elements[category].value;
			var quotationContract = document.forms['serviceOrderForm'].elements['billing.contract'].value; 
			var accountCompanyDivision = document.forms['serviceOrderForm'].elements['companyDivision'+aid].value;
			var originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
			var destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
			var mode;		
			if(document.forms['serviceOrderForm'].elements['serviceOrder.mode']!=undefined){
				 mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value; }else{mode="";}
			 		
				recGlVal = 'secondDescription';
				payGlVal = 'thirdDescription'; 
				var estimateContractCurrency='tenthDescription';
				var estimatePayableContractCurrency='fourthDescription';
				<c:if test="${contractType}">  
					estimateContractCurrency='contractCurrencyNew'+aid;
					estimatePayableContractCurrency='payableContractCurrencyNew'+aid;
				</c:if>
			if(val=='Internal Cost'){
				quotationContract="Internal Cost Management";
				javascript:openWindow('internalCostChargess.html?accountCompanyDivision='+accountCompanyDivision+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode);
			    document.forms['serviceOrderForm'].elements[chargeCode].select();
			}
			else {
			if(quotationContract=='' || quotationContract==' '){
				alert("There is no pricing contract in billing: Please select.");
			}
			else{
				javascript:openWindow('chargess.html?contract='+quotationContract+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode+'&categoryType='+val);
			}
		}
		document.forms['serviceOrderForm'].elements[chargeCode].focus();
	}  	
	  function checkBillingComplete() { 
	      if(document.forms['serviceOrderForm'].elements['billing.billComplete'].value!=''){ 
	          var agree = confirm("The billing has been completed, do you still want to add lines?");
	           if(agree){
	        	   findAllPricingLineId('AddLine');
	             }else {         }
	      }else{
	    	  findAllPricingLineId('AddLine');    
	      }
	  } 
	  var decimalValueCount = 0 ;
	  var count = 0;
	  var negativeCount=0;
	  
	  function clearDecimal(){
			count = 0;
			decimalValueCount = 0;
		}
		function numbersonly(myfield, e) {
			var key;
			var keychar;
			var val = myfield.value;
			if(count > 0){	decimalValueCount++;    if(decimalValueCount > 2){ return true; }		}
					if(val.indexOf(".")<0)
					{  count=0;	}
			if (window.event)
			   key = window.event.keyCode;
			else if (e)
			   key = e.which;
			else
			   return true;
				keychar = String.fromCharCode(key);
			if ((key==null) || (key==0) || (key==8) ||     (key==9) || (key==13) || (key==27) )
			   return true;
			else if ((("0123456789").indexOf(keychar) > -1)){
			   return true;
			}
			else if ((count<1)&&(keychar == ".")){
			   count++;
			   return true;
			}
			else if ((negativeCount<1)&&(keychar == "-"))
			{
				negativeCount++;
			   return true;
			}
			else if ((negativeCount>0)&&(keychar == "-"))
			{
			   return false;
			}
			else if ((count>0)&&(keychar == ".")){
			   return false;
			}
			else
				return false;
		}		
	    function changeVatAmt(estVatDec,estVatPer,revenue,estAmt,estSellAmt) {
    	    var aid=estVatDec.replace('vatDecr','');
    	    aid=aid.trim();
	    	calculateVatSection('EST',aid,'EST');
	     }  
		function calculateVatAmt(estVatPer,revenue,estAmt){
    	    var aid=estVatPer.replace('vatPers' ,'');
    	    aid=aid.trim();
			calculateVatAmtSection('EST',aid);		 
		} 
		function calculateVatSectionAll(type,aid,type1){
			  var vatExclude=document.forms['serviceOrderForm'].elements['VATExclude'+aid].value;
			  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
			  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = false;
			  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = false;
			  var chargeCode=document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
			  var vendorCode=document.forms['serviceOrderForm'].elements['vendorCode'+aid].value; 
			  var recGl=document.forms['serviceOrderForm'].elements['recGl'+aid].value;
			  var payGl=document.forms['serviceOrderForm'].elements['payGl'+aid].value;   
			  var actgCode=document.forms['serviceOrderForm'].elements['actgCode'+aid].value;
			  var acref="${accountInterface}";
			  acref=acref.trim();
			  if(vendorCode.trim()=='' && type1!='BAS' && type1=='PAY'){
				  alert("Vendor Code Missing. Please select");
			  }else if(chargeCode.trim()=='' && type1!='BAS' && type1!='FX' && (type1=='REC' || type1=='PAY')){
				  alert("Charge code needs to be selected");
			  }	else if(payGl.trim()=='' && type1!='BAS' && type1=='PAY' && acref =='Y'){
				  alert("Pay GL missing. Please select another charge code.");
			  }	else if(recGl.trim()=='' && type1!='BAS' && type1=='REC' && acref =='Y'){
				  alert("Receivable GL missing. Please select another charge code.");
			  }else if(actgCode.trim()=='' && type1!='BAS' && acref =='Y' && type1=='PAY'){
				  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
			  }else if(type=='EXPENSE'){
				       <c:forEach var="entry" items="${payVatPercentList}">
				       if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
				     document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
				     calculateVatAmtSectionAll(type,aid);
				    }
				   </c:forEach>
				        if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value==''){
				         document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
				         calculateVatAmtSectionAll(type,aid);
				        } 
					     <configByCorp:fieldVisibility componentId="accountLine.payVatGl">
					     var relo1='';
						 	<c:forEach var="entry1" items="${payVatPayGLMap}" > 
						 	if(relo1 ==''){
						 	  if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
						 		 document.forms['serviceOrderForm'].elements['payVatGl'+aid].value='${entry1.value}'; 
						 	      relo1 ="yes";
						 	      }   }
						 	     </c:forEach>
						 	    relo1='';
							 	<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
							 	if(relo1 ==''){
							 	  if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
							 		 document.forms['serviceOrderForm'].elements['qstPayVatGl'+aid].value='${entry1.value}'; 
							 	      relo1 ="yes";
							 	      }   }
							 	     </c:forEach>		 	     
					 </configByCorp:fieldVisibility>
				          
			        
			  }else if(type=='REVENUE'){
			       <c:forEach var="entry" items="${estVatPersentList}">
			       if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
			     document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
			     calculateVatAmtSectionAll(type,aid);
			    }
			   </c:forEach>
			        if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value==''){
			         document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			         calculateVatAmtSectionAll(type,aid);
			        }
			        <configByCorp:fieldVisibility componentId="accountLine.recVatGL"> 
			        var relo1=""  
			            <c:forEach var="entry1" items="${euvatRecGLMap}">
			               if(relo1==""){ 
			            	   
			               if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry1.key}') {
			            	   document.forms['serviceOrderForm'].elements['recVatGl'+aid].value='${entry1.value}';  
			               relo1="yes"; 
			              }  }
			           </c:forEach>
			           relo1='';
			    	 	<c:forEach var="entry1" items="${qstEuvatRecGLMap}" > 
			    	 	if(relo1 ==''){
			    	 	  if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry1.key}'){
			    	 		 document.forms['serviceOrderForm'].elements['qstRecVatGl'+aid].value='${entry1.value}'; 
			    	 	      relo1 ="yes";
			    	 	      }   }
			    	 	     </c:forEach>        
			          </configByCorp:fieldVisibility> 
			        
			  }else {
			  }
			  }else{
  				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value="";
				  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value="";			  
				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = true;
				  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = true;
				  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=0;
			
			</c:if>
			document.forms['serviceOrderForm'].elements['vatAmt'+aid].value=0;			
			document.forms['serviceOrderForm'].elements['estExpVatAmt'+aid].value=0;			
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['revisionVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['revisionExpVatAmt'+aid].value=0;			
			</c:if>
			  }
			 } 
			 function calculateVatAmtSectionAll(type,aid){
				  var vatExclude=document.forms['serviceOrderForm'].elements['VATExclude'+aid].value;
				  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = false;
				  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = false;
				 
				 if(type=='EXPENSE'){
					  try{	 
						   var payVatAmt=0.00;
						   var actualExpense=0.00;
						   if(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value!=''){
						    var payVatPercent=document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value;
						    actualExpense = document.forms['serviceOrderForm'].elements['actualExpense'+aid].value;
						    <c:if test="${contractType}"> 
							actualExpense=  document.forms['serviceOrderForm'].elements['localAmount'+aid].value;
							</c:if>
							  
							var firstPersent="";
							var secondPersent=""; 
							var relo="" 
							     <c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
							        if(relo==""){ 
							        if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry.key}') {
								        var arr='${entry.value}';
								        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
								        	firstPersent=arr.split(':')[0];
								        	secondPersent=arr.split(':')[1];
								        }
							        relo="yes"; 
							       }  }
							    </c:forEach> 
							    if(firstPersent!='' && secondPersent!=''){
								    var p1=parseFloat(firstPersent);
								    var p2=parseFloat(secondPersent);
								    var temp=0.00;
								    payVatAmt=(actualExpense*p1)/100;
								    temp=payVatAmt;
								    payVatAmt=Math.round(payVatAmt*10000)/10000; 
								    document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=payVatAmt;
									payVatAmt=(actualExpense*p2)/100;
									temp=temp+payVatAmt;
									payVatAmt=Math.round(payVatAmt*10000)/10000; 
									document.forms['serviceOrderForm'].elements['qstPayVatAmt'+aid].value=payVatAmt;
									document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=temp;
							    }else{
								    payVatAmt=(actualExpense*payVatPercent)/100;
								    payVatAmt=Math.round(payVatAmt*10000)/10000; 
								    document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=payVatAmt;
								    document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=payVatAmt;
								    document.forms['serviceOrderForm'].elements['qstPayVatAmt'+aid].value=0.00;						    
							    }
						   }
						 }catch(e){}  

						  try{
						   var estExpVatAmt=0;
						   if(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value!=''){
						    var payVatPercent=eval(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value);
						    var estimateExpense= eval(document.forms['serviceOrderForm'].elements['estimateExpense'+aid].value);
						    <c:if test="${contractType}">
						    estimateExpense= eval(document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value); 
							</c:if>
							estExpVatAmt=(estimateExpense*payVatPercent)/100;
							estExpVatAmt=Math.round(estExpVatAmt*10000)/10000; 
						    document.forms['serviceOrderForm'].elements['estExpVatAmt'+aid].value=estExpVatAmt;
						    }
							}catch(e){} 

							  try{
						   var revisionExpVatAmt=0;
						   if(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value!=''){
						    var payVatPercent=eval(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value);
						    var revisionExpense= eval(document.forms['serviceOrderForm'].elements['revisionExpense'+aid].value);
						    <c:if test="${contractType}">
						    revisionExpense= eval(document.forms['serviceOrderForm'].elements['revisionLocalAmount'+aid].value); 
							</c:if>
							revisionExpVatAmt=(revisionExpense*payVatPercent)/100;
							revisionExpVatAmt=Math.round(revisionExpVatAmt*10000)/10000; 
						    document.forms['serviceOrderForm'].elements['revisionExpVatAmt'+aid].value=revisionExpVatAmt;
						    }
							}catch(e){} 
					 
				 }else if(type=='REVENUE'){
					   try{
						   var recVatAmt=0;
						   if(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value!=''){
						    var recVatPercent=eval(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value);
						    var actualRevenue= eval(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value);
						    <c:if test="${contractType}"> 
						    actualRevenue=  document.forms['serviceOrderForm'].elements['actualRevenueForeign'+aid].value;
							</c:if>
							var firstPersent="";
							var secondPersent=""; 
							var relo="" 
							     <c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
							        if(relo==""){ 
							        if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
								        var arr='${entry.value}';
								        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
								        	firstPersent=arr.split(':')[0];
								        	secondPersent=arr.split(':')[1];
								        }
							        relo="yes"; 
							       }  }
							    </c:forEach> 
							    if(firstPersent!='' && secondPersent!=''){
								    var p1=parseFloat(firstPersent);
								    var p2=parseFloat(secondPersent);
								    var temp=0.00;
									recVatAmt=(actualRevenue*p1)/100;
									temp=recVatAmt;
									recVatAmt=Math.round(recVatAmt*10000)/10000; 
									document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=recVatAmt;
									recVatAmt=(actualRevenue*p2)/100;
									temp=temp+recVatAmt;
									recVatAmt=Math.round(recVatAmt*10000)/10000;
									document.forms['serviceOrderForm'].elements['qstRecVatAmt'+aid].value=recVatAmt;
									document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=temp;
							    }else{
								    recVatAmt=(actualRevenue*recVatPercent)/100;
								    recVatAmt=Math.round(recVatAmt*10000)/10000; 
								    document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=recVatAmt;
								    document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=recVatAmt;
								    document.forms['serviceOrderForm'].elements['qstRecVatAmt'+aid].value=0.00;
							    }
						    }
						   }catch(e){}  
					  try{
						   var estVatAmt=0;
						   if(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value!=''){
						    var estVatPercent=eval(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value);
						    var estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value);
						    <c:if test="${contractType}">
						    estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value); 
							</c:if>
							estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
							estVatAmt=Math.round(estVatAmt*10000)/10000; 
						    document.forms['serviceOrderForm'].elements['vatAmt'+aid].value=estVatAmt;
						    }
							}catch(e){} 


							  try{
						   var revisionVatAmt=0;
						   if(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value!=''){
						    var revisionVatPercent=eval(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value);
						    var revisionRevenueAmount= eval(document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+aid].value);
						    <c:if test="${contractType}">
						    revisionRevenueAmount= eval(document.forms['serviceOrderForm'].elements['revisionSellLocalAmount'+aid].value); 
							</c:if>
						    revisionVatAmt=(revisionRevenueAmount*revisionVatPercent)/100;
						    revisionVatAmt=Math.round(revisionVatAmt*10000)/10000; 
						    document.forms['serviceOrderForm'].elements['revisionVatAmt'+aid].value=revisionVatAmt;
						    }
							}catch(e){} 


					 
				 }else{
				 }
				  }else{
	  				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value="";
					  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value="";				  
					  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = true;
					  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = true;					  
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=0;
			
			</c:if>
			document.forms['serviceOrderForm'].elements['vatAmt'+aid].value=0;			
			document.forms['serviceOrderForm'].elements['estExpVatAmt'+aid].value=0;			
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['revisionVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['revisionExpVatAmt'+aid].value=0;			
					  </c:if>
				  }
			 }
		function calculateVatSection(type,aid,type1){
			  var vatExclude=document.forms['serviceOrderForm'].elements['VATExclude'+aid].value;
			  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
			  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = false;
			  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = false;
			
			  var chargeCode=document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
			  var vendorCode=document.forms['serviceOrderForm'].elements['vendorCode'+aid].value; 
			  var recGl=document.forms['serviceOrderForm'].elements['recGl'+aid].value;
			  var payGl=document.forms['serviceOrderForm'].elements['payGl'+aid].value;   
			  var actgCode=document.forms['serviceOrderForm'].elements['actgCode'+aid].value;
			  var acref="${accountInterface}";
			  acref=acref.trim();
			  if(vendorCode.trim()=='' && type1!='BAS' && type1=='PAY'){
				  alert("Vendor Code Missing. Please select");
			  }else if(chargeCode.trim()=='' && type1!='BAS' && type1!='FX' && (type1=='REC' || type1=='PAY')){
				  alert("Charge code needs to be selected");
			  }	else if(payGl.trim()=='' && type1!='BAS' && type1=='PAY' && acref =='Y'){
				  alert("Pay GL missing. Please select another charge code.");
			  }	else if(recGl.trim()=='' && type1!='BAS' && type1=='REC' && acref =='Y'){
				  alert("Receivable GL missing. Please select another charge code.");
			  }else if(actgCode.trim()=='' && type1!='BAS' && acref =='Y' && type1=='PAY'){
				  alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
			  }else if(type=='EST'){
			       <c:forEach var="entry" items="${estVatPersentList}">
			       if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
			     document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
			     calculateVatAmtSection(type,aid);
			    }
			   </c:forEach>
			        if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value==''){
			         document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			         calculateVatAmtSection(type,aid);
			        }
				       <c:forEach var="entry" items="${payVatPercentList}">
				       if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
				     document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
				     calculateVatAmtSection(type,aid);
				    }
				   </c:forEach>
				        if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value==''){
				         document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
				         calculateVatAmtSection(type,aid);
				        }   
			        
			  }else if(type=='REV'){
			       <c:forEach var="entry" items="${estVatPersentList}">
			       if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
			     document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
			     calculateVatAmtSection(type,aid);
			    }
			   </c:forEach>
			        if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value==''){
			         document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			         calculateVatAmtSection(type,aid);
			        }
				       <c:forEach var="entry" items="${payVatPercentList}">
				       if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
				     document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
				     calculateVatAmtSection(type,aid);
				    }
				   </c:forEach>
				        if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value==''){
				         document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
				         calculateVatAmtSection(type,aid);
				        }   
			        
			  }else if(type=='REC'){
			       <c:forEach var="entry" items="${estVatPersentList}">
			       if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
			     document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
			     calculateVatAmtSection(type,aid);
			    }
			   </c:forEach>
			        if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value==''){
			         document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			         calculateVatAmtSection(type,aid);
			        } 

			        <configByCorp:fieldVisibility componentId="accountLine.recVatGL"> 
			        var relo1=""  
			            <c:forEach var="entry1" items="${euvatRecGLMap}">
			               if(relo1==""){ 
			            	   
			               if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry1.key}') {
			            	   document.forms['serviceOrderForm'].elements['recVatGl'+aid].value='${entry1.value}';  
			               relo1="yes"; 
			              }  }
			           </c:forEach>
			           relo1='';
			    	 	<c:forEach var="entry1" items="${qstEuvatRecGLMap}" > 
			    	 	if(relo1 ==''){
			    	 	  if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry1.key}'){
			    	 		 document.forms['serviceOrderForm'].elements['qstRecVatGl'+aid].value='${entry1.value}'; 
			    	 	      relo1 ="yes";
			    	 	      }   }
			    	 	     </c:forEach>        
			          </configByCorp:fieldVisibility> 
			          
			  }else if(type=='PAY'){
			       <c:forEach var="entry" items="${payVatPercentList}">
			       if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
			     document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
			     calculateVatAmtSection(type,aid);
			    }
			   </c:forEach>
			        if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value==''){
			         document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
			         calculateVatAmtSection(type,aid);
			        } 

				     <configByCorp:fieldVisibility componentId="accountLine.payVatGl">
				     var relo1='';
					 	<c:forEach var="entry1" items="${payVatPayGLMap}" > 
					 	if(relo1 ==''){
					 	  if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
					 		 document.forms['serviceOrderForm'].elements['payVatGl'+aid].value='${entry1.value}'; 
					 	      relo1 ="yes";
					 	      }   }
					 	     </c:forEach>
					 	    relo1='';
						 	<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
						 	if(relo1 ==''){
						 	  if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
						 		 document.forms['serviceOrderForm'].elements['qstPayVatGl'+aid].value='${entry1.value}'; 
						 	      relo1 ="yes";
						 	      }   }
						 	     </c:forEach>		 	     
				 </configByCorp:fieldVisibility>
			          
			  }else{
			  }
			  }else{
  				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value="";
				  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value="";			  
				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = true;
				  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = true;
				  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=0;
			
			</c:if>
			document.forms['serviceOrderForm'].elements['vatAmt'+aid].value=0;			
			document.forms['serviceOrderForm'].elements['estExpVatAmt'+aid].value=0;			
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['revisionVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['revisionExpVatAmt'+aid].value=0;			
				  </c:if>
			  }
			 } 
			 function calculateVatAmtSection(type,aid){
				  var vatExclude=document.forms['serviceOrderForm'].elements['VATExclude'+aid].value;
				  if(vatExclude==null || vatExclude==false || vatExclude=='false'){
				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = false;
				  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = false;
			  
			 if(type=='EST'){
					  try{
				   var estVatAmt=0;
				   if(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value!=''){
				    var estVatPercent=eval(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value);
				    var estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value);
				    <c:if test="${contractType}">
				    estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value); 
					</c:if>
					estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
					estVatAmt=Math.round(estVatAmt*10000)/10000; 
				    document.forms['serviceOrderForm'].elements['vatAmt'+aid].value=estVatAmt;
				    }
					}catch(e){} 
				  try{
				   var estExpVatAmt=0;
				   if(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value!=''){
				    var payVatPercent=eval(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value);
				    var estimateExpense= eval(document.forms['serviceOrderForm'].elements['estimateExpense'+aid].value);
				    <c:if test="${contractType}">
				    estimateExpense= eval(document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value); 
					</c:if>
					estExpVatAmt=(estimateExpense*payVatPercent)/100;
					estExpVatAmt=Math.round(estExpVatAmt*10000)/10000; 
				    document.forms['serviceOrderForm'].elements['estExpVatAmt'+aid].value=estExpVatAmt;
				    }
					}catch(e){} 
					
			  }else if(type=='REV'){ 
				  try{
			   var revisionVatAmt=0;
			   if(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value!=''){
			    var revisionVatPercent=eval(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value);
			    var revisionRevenueAmount= eval(document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+aid].value);
			    <c:if test="${contractType}">
			    revisionRevenueAmount= eval(document.forms['serviceOrderForm'].elements['revisionSellLocalAmount'+aid].value); 
				</c:if>
			    revisionVatAmt=(revisionRevenueAmount*revisionVatPercent)/100;
			    revisionVatAmt=Math.round(revisionVatAmt*10000)/10000; 
			    document.forms['serviceOrderForm'].elements['revisionVatAmt'+aid].value=revisionVatAmt;
			    }
				}catch(e){} 
				  try{
			   var revisionExpVatAmt=0;
			   if(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value!=''){
			    var payVatPercent=eval(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value);
			    var revisionExpense= eval(document.forms['serviceOrderForm'].elements['revisionExpense'+aid].value);
			    <c:if test="${contractType}">
			    revisionExpense= eval(document.forms['serviceOrderForm'].elements['revisionLocalAmount'+aid].value); 
				</c:if>
				revisionExpVatAmt=(revisionExpense*payVatPercent)/100;
				revisionExpVatAmt=Math.round(revisionExpVatAmt*10000)/10000; 
			    document.forms['serviceOrderForm'].elements['revisionExpVatAmt'+aid].value=revisionExpVatAmt;
			    }
				}catch(e){} 
			  }else if(type=='REC'){				  
			   try{
			   var recVatAmt=0;
			   if(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value!=''){
			    var recVatPercent=eval(document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value);
			    var actualRevenue= eval(document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value);
			    <c:if test="${contractType}"> 
			    actualRevenue=  document.forms['serviceOrderForm'].elements['actualRevenueForeign'+aid].value;
				</c:if>
				var firstPersent="";
				var secondPersent=""; 
				var relo="" 
				     <c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
				        if(relo==""){ 
				        if(document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
					        var arr='${entry.value}';
					        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
					        	firstPersent=arr.split(':')[0];
					        	secondPersent=arr.split(':')[1];
					        }
				        relo="yes"; 
				       }  }
				    </c:forEach> 
				    if(firstPersent!='' && secondPersent!=''){
					    var p1=parseFloat(firstPersent);
					    var p2=parseFloat(secondPersent);
					    var temp=0.00;
						recVatAmt=(actualRevenue*p1)/100;
						temp=recVatAmt;
						recVatAmt=Math.round(recVatAmt*10000)/10000; 
						document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=recVatAmt;
						recVatAmt=(actualRevenue*p2)/100;
						temp=temp+recVatAmt;
						recVatAmt=Math.round(recVatAmt*10000)/10000;
						document.forms['serviceOrderForm'].elements['qstRecVatAmt'+aid].value=recVatAmt;
						document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=temp;
				    }else{
					    recVatAmt=(actualRevenue*recVatPercent)/100;
					    recVatAmt=Math.round(recVatAmt*10000)/10000; 
					    document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=recVatAmt;
					    document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=recVatAmt;
					    document.forms['serviceOrderForm'].elements['qstRecVatAmt'+aid].value=0.00;
				    }
			    }
			   }catch(e){}    
			  }else if(type=='PAY'){
				  try{	 
				   var payVatAmt=0.00;
				   var actualExpense=0.00;
				   if(document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value!=''){
				    var payVatPercent=document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value;
				    actualExpense = document.forms['serviceOrderForm'].elements['actualExpense'+aid].value;
				    <c:if test="${contractType}"> 
					actualExpense=  document.forms['serviceOrderForm'].elements['localAmount'+aid].value;
					</c:if>
					  
					var firstPersent="";
					var secondPersent=""; 
					var relo="" 
					     <c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
					        if(relo==""){ 
					        if(document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value=='${entry.key}') {
						        var arr='${entry.value}';
						        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
						        	firstPersent=arr.split(':')[0];
						        	secondPersent=arr.split(':')[1];
						        }
					        relo="yes"; 
					       }  }
					    </c:forEach> 
					    if(firstPersent!='' && secondPersent!=''){
						    var p1=parseFloat(firstPersent);
						    var p2=parseFloat(secondPersent);
						    var temp=0.00;
						    payVatAmt=(actualExpense*p1)/100;
						    temp=payVatAmt;
						    payVatAmt=Math.round(payVatAmt*10000)/10000; 
						    document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=payVatAmt;
							payVatAmt=(actualExpense*p2)/100;
							temp=temp+payVatAmt;
							payVatAmt=Math.round(payVatAmt*10000)/10000; 
							document.forms['serviceOrderForm'].elements['qstPayVatAmt'+aid].value=payVatAmt;
							document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=temp;
					    }else{
						    payVatAmt=(actualExpense*payVatPercent)/100;
						    payVatAmt=Math.round(payVatAmt*10000)/10000; 
						    document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=payVatAmt;
						    document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=payVatAmt;
						    document.forms['serviceOrderForm'].elements['qstPayVatAmt'+aid].value=0.00;						    
					    }
				   }
				 }catch(e){}  
				  
			  }else{
			  }
				  }else{
	  				  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].value="";
					  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].value="";					  				  
					  document.forms['serviceOrderForm'].elements['payVatDescr'+aid].disabled = true;
					  document.forms['serviceOrderForm'].elements['recVatDescr'+aid].disabled = true;
					  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['recVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatPercent'+aid].value=0;
			document.forms['serviceOrderForm'].elements['recVatAmtTemp'+aid].value=0;
			document.forms['serviceOrderForm'].elements['payVatAmtTemp'+aid].value=0;
			
			</c:if>
			document.forms['serviceOrderForm'].elements['vatAmt'+aid].value=0;			
			document.forms['serviceOrderForm'].elements['estExpVatAmt'+aid].value=0;			
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['serviceOrderForm'].elements['revisionVatAmt'+aid].value=0;
			document.forms['serviceOrderForm'].elements['revisionExpVatAmt'+aid].value=0;			
			</c:if>
					  
				  }
			    
			 }
			 	
		function onlyNumsAllowedPersent(evt){
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
		} 

		function fillCommodity(){		
		}
		function getOnGroup(label) {
			if(label=='dropDown') {
				if(document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value=='GRP') {
				document.forms['serviceOrderForm'].elements['serviceOrder.grpPref'].checked=true;
				}
			}
			if(label=='checkBox') {
				if(document.forms['serviceOrderForm'].elements['serviceOrder.grpPref'].checked==true)
				{
				document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value='GRP';
				} } }    
		function selectColumn(targetElement)  {  
		     var rowId=targetElement.value;  
		     if(targetElement.checked) {
		        var userCheckStatus = document.forms['serviceOrderForm'].elements['reloServiceType'].value;
		        if(userCheckStatus == '') {
		  	  	document.forms['serviceOrderForm'].elements['reloServiceType'].value = rowId;
		        } else {
		         var userCheckStatus=	document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus + '#' + rowId;
		        document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus.replace( '##' , '#' );
		        }
		      }
		     if(targetElement.checked==false)
		      {
		       var userCheckStatus = document.forms['serviceOrderForm'].elements['reloServiceType'].value;
		       var userCheckStatus=document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus.replace( rowId , '' );
		       document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus.replace( '##' , '#' );
		       } 
		    }
		function checkColumn(){
			var userCheckStatus ="";
			userCheckStatus = '${serviceOrder.serviceType}';		  
			if(userCheckStatus!=""){
				var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
				document.forms['serviceOrderForm'].elements['reloServiceType'].value="";
				var column = userCheckStatus.split(",");  
				userCheckStatus = document.forms['serviceOrderForm'].elements['reloServiceType'].value;
				for(i = 0; i<column.length ; i++){    
			        	if(column[i]!=""){
				        	if(job=='RLO' && document.getElementById(column[i])!=undefined && document.getElementById(column[i])!=null){
			     				document.getElementById(column[i]).checked=true;
			     				userCheckStatus=userCheckStatus + '#' +column[i];
			        		}
						} 
				}
				document.forms['serviceOrderForm'].elements['reloServiceType'].value =userCheckStatus; 
			}
		}
		function enableStateListOrigin(){ 
			var oriCon=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
			  var enbState = '${enbState}';
			  var index = (enbState.indexOf(oriCon)> -1);
			  if(index != ''){
			  		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = false;
			  }else{
				  document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = true;
			  }	        
		}
		function enableStateListDestin(){ 
			var desCon=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
			  var enbState = '${enbState}';
			  var index = (enbState.indexOf(desCon)> -1);
			  if(index != ''){
			  		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = false;
			  }else{
				  document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = true;
			  }	        
		}
		  function getServiceTypeByJob() {
			    var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
			    if(jobType!='RLO'){
				var url="getServiceTypeByJob.html?ajax=1&decorator=simple&popup=true&jobType="+encodeURI(jobType);
				http5412.open("GET", url, true); 
				http5412.onreadystatechange = handleHttpResponse5412; 
				http5412.send(null);
			    } 
			   }	
			function handleHttpResponse5412(){
				if (http5412.readyState == 4){
				            var results = http5412.responseText
				            results = results.trim();
				            res = results.replace("{",'');
				            res = res.replace("}",'');
				            res = res.split(",");
							targetElementValue=document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
							targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'];
							targetElement.length = res.length;
							var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
								for(i=0;i<res.length;i++){
								  if(res[i] == ''){
								  document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text = '';
								  document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value = '';
								  }else{
									   stateVal = res[i].replace("=",'+');
									   stateVal=stateVal.split("+");
									   document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text=stateVal[1];
									   document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value=stateVal[0].trim();
									   if(job == "")  {
									   	document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[0].selected=true;
									  	document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[0].selected=true;
									   }   } }
							document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value=targetElementValue;
				        } 
				  }	
		    var http5412 = getHTTPObject(); 
		    function findCustomInfo(position) {
		    	 var serviceid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
		    	 var url="serviceInfo.html?ajax=1&decorator=simple&popup=true&serviceid=" + encodeURI(serviceid)
		    	  ajax_showTooltip(url,position);	
		    	  }
		    function findDocInfo(position) {
		    	 var serviceid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
		    	 var url="servicedocInfo.html?ajax=1&decorator=simple&popup=true&serviceid=" + encodeURI(serviceid)
		    	  ajax_showTooltip(url,position);	
		    	  } 
		    function viewcustom(){
		    	 alert("No details available for this Country");
		    	 } 
		     function checkJobTypes(){
		    	 <configByCorp:fieldVisibility componentId="component.button.CWMS.commission"> 
		    	var jobType=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
		    	var ComidityType= document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
		    	if(document.forms['serviceOrderForm'].elements['invoiceCount'].value!='0'){ 
		      	   alert("Invoice Already Generated.Cannot change the Job."); 
		           document.forms['serviceOrderForm'].elements['serviceOrder.job'].value='${serviceOrder.job}';
		           getCommodity();
		           getNewCoordAndSale();
		          
		    	}
		    	 </configByCorp:fieldVisibility> 
		      }	     
		   	      
		    function checkSalesMan(){
		    	 <configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
		    	 var abc = '${roles}' ;
		    	 if(!(abc.indexOf("ROLE_ADMIN") > -1)){
		    	var saleManType=document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].value;
		    		if(document.forms['serviceOrderForm'].elements['invoiceCount'].value!='0'){
		 	      	   alert("Invoice Already Generated.Cannot change the Sales Person.") 
		      	 	document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].value='${serviceOrder.salesMan}';  
		      	}}
		    	</configByCorp:fieldVisibility> 
		    }	    
		   
		   function showMmCounselorByJob(){
			   var str="0";
			   <configByCorp:fieldVisibility componentId="component.field.Alternative.mmCounselor">
			   str="1";
			   </configByCorp:fieldVisibility>
					var jobValue=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
					if((str=="1") && (jobValue=='VIS' || jobValue=='GST' || jobValue=='INT' || jobValue=='STO' || jobValue=='UVL')){
						document.getElementById('mmCounselor').style.display="block";	
						document.getElementById('mmCounselorLabel').style.display="block";	
					}else{
						document.getElementById('mmCounselor').style.display="none";
						document.getElementById('mmCounselorLabel').style.display="none";	
					}				
			}
		   function titleCase(element){
				var txt=element.value; var spl=txt.split(" "); 
				var upstring=""; for(var i=0;i<spl.length;i++){ 
				try{ 
				upstring+=spl[i].charAt(0).toUpperCase(); 
				}catch(err){} 
				upstring+=spl[i].substring(1, spl[i].length); 
				upstring+=" ";   
				} 
				element.value=upstring.substring(0,upstring.length-1); 
			}
		   function getCarrier(){
				 <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
				 var modeType = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
			  if(modeType=='Air'|| modeType=='Sea'){
			   document.getElementById('carrierFild').style.display="block";
			   document.getElementById('carrierFildLabel').style.display="block";
			  }else{
			   document.getElementById('carrierFild').style.display="none";
			   document.getElementById('carrierFildLabel').style.display="none";
			  }
			  </configByCorp:fieldVisibility>
			}
		   function fillUsaFlag(){
				<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
			 		var modeType = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
			  		if(modeType=='Air'|| modeType=='Sea'){
						var flagBillTocode = document.forms['serviceOrderForm'].elements['serviceOrder.billToCode'].value;
						if(flagBillTocode!='') {
						    var url="getUsaFlagAjax.html?ajax=1&decorator=simple&popup=true&flagBillTocode="+encodeURI(flagBillTocode);
						    http511.open("GET", url, true); 
						   	http511.onreadystatechange = handleHttp905; 
						   	http511.send(null);	     
			    		 }
			   		}
			  </configByCorp:fieldVisibility>
			} 
			   function handleHttp905(){
			    if (http511.readyState == 4){    	
			    	var results = http511.responseText
					if(results!=null && results.trim()!='false'){
						document.forms['serviceOrderForm'].elements['serviceOrder.serviceOrderFlagCarrier'].value='USA';
						}
			    	}
			       }  
			var http511 = getHTTPObject();
			
			function autoCompleterAjaxCallOriCity(){
				var countryName = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
				var stateNameOri = document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
				var cityNameOri = document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
				var countryNameOri = "";
				var countryList = JSON.parse('${jsonCountryCodeText}');
				var countryNameOri = getKeyByValue(countryList,countryName);
				if(cityNameOri!=''){
					var data = 'leadCaptureAutocompleteOriAjax.html?ajax=1&stateNameOri='+stateNameOri+'&countryNameOri='+countryNameOri+'&cityNameOri='+cityNameOri+'&decorator=simple&popup=true';
					$("#originCity").autocomplete({				 
					      source: data		      
					    });
				}
			}
			
			function findCoordinatorByBA(){
			    var custJobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
			    var soNetworkGrp =false;
			    var accNetworkGrp = false;
			    var checkUTSI="${company.UTSI}";
			    <c:if test="${not empty serviceOrder.id}">
			        soNetworkGrp= ${trackingStatus.soNetworkGroup};
			        accNetworkGrp= ${trackingStatus.accNetworkGroup};
			    </c:if>
			    var checkSoFlag = true;
			    var bookAgCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
			    
			    if(custJobType!='' && bookAgCode!=''){
			    $.ajax({
			        type: "POST",
			        url: "checkNetworkCoordinatorForCf.html?ajax=1&decorator=simple&popup=true",
			        data: {custJobType:custJobType,soNetworkGrp:soNetworkGrp,accNetworkGrp:accNetworkGrp,bookAgCode:bookAgCode,checkSoFlag:checkSoFlag,checkUTSI:checkUTSI},
			        success: function (data, textStatus, jqXHR) {
			           results = data.trim();
			           res = results.split("~");
			           targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'];
		                targetElement.length = res.length;
		                for(i=0;i<res.length;i++)
		                    {
		                    if(res[i] == ''){
		                    document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].text = '';
		                    document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].value = '';
		                    }else{
		                    stateVal = res[i].split("#");
		                    document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].text = stateVal[1];
		                    document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].value = stateVal[0];
		                    }
		                    }
		                    document.getElementById("coordinator").value = '${serviceOrder.coordinator}';
			        },
			      error: function (data, textStatus, jqXHR) {
			              }
			      });
			}else{
			    document.forms['customerFileForm'].elements['customerFile.coordinator'].value='';
			}}
			
			function isNumber(targetElement) {
				var i;
			    var s = targetElement.value;
			    for (i = 0; i < s.length; i++){   
			        var c = s.charAt(i);
			        if (((c < "0") || (c > "9"))) {
			        alert("Enter valid Number");
			        targetElement.value="";
			        return false;
			        } }
			    return true;
			}
			
			function saveMarkupPercentage(){
				var shipNumber=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
				var markupVal = document.forms['serviceOrderForm'].elements['serviceOrder.estimateOverallMarkupPercentage'].value;
				if(markupVal==''){
					document.forms['serviceOrderForm'].elements['serviceOrder.estimateOverallMarkupPercentage'].value = '${serviceOrder.estimateOverallMarkupPercentage}';
					markupVal = document.forms['serviceOrderForm'].elements['serviceOrder.estimateOverallMarkupPercentage'].value;
				}else if(markupVal!=''){
					new Ajax.Request('updateEstimateOverallMarkupPercentageAjax.html?ajax=1&decorator=simple&popup=true&markupVal='+markupVal+'&shipNumber='+shipNumber,
							{
						method:'get',
						onSuccess: function(transport){
							var response = transport.responseText || "";
							response = response.trim();
						},
						onFailure: function(){ 
						}
			        });
				}
			}
			
			function caculateMarkUpPercentage(){
				var markupVal = document.forms['serviceOrderForm'].elements['serviceOrder.estimateOverallMarkupPercentage'].value;
				if(markupVal==''){
					alert('Please fill MK %');
				}else{
					var shipNumber=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
					  //openWindow('findEstimateExpenseForMarkUpCalculation.html?shipNumber='+shipNumber+'&decorator=popup&popup=true',680,300);
					  var $j = jQuery.noConflict();
						$j("#markUpDetails").modal({show:true,backdrop: 'static'});
						 $j.ajax({
				            url: 'findEstimateExpenseForMarkUpCalculation.html?shipNumber='+shipNumber+'&markupVal='+markupVal+'&decorator=modal&popup=true',
				            success: function(data){
									$j("#markUpDetails").html(data);
				            },
				            error: function () {
				                alert('Some error happens');
				                $j("#markUpDetails").modal("hide");
				             }   
				        });  
				}
			}
			
			function markUpModalClose(){
		    	var $j = jQuery.noConflict();
		    	$j("#markUpDetails").modal("hide");
		    }
			
			function populateOpsPersonByJob(){
				var str="0";
				<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.opsPerson">
				   str="1";
				</configByCorp:fieldVisibility>
				var jobValue=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
				var companyDivisionValue = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
				if(jobValue!=''){
					new Ajax.Request('populateOpsPersonByJobAjax.html?ajax=1&decorator=simple&popup=true&jobValue='+jobValue+'&companyDivisionValue='+companyDivisionValue,
							{
						method:'get',
						onSuccess: function(transport){
							var response = transport.responseText || "";
							response = response.trim();
							var opsPerson = document.forms['serviceOrderForm'].elements['serviceOrder.opsPerson'].value;
							if(opsPerson==null || opsPerson==''){
								document.forms['serviceOrderForm'].elements['serviceOrder.opsPerson'].value = response;
							}
						},
						onFailure: function(){ 
						}
			        });
				}
			}
			function sendSurveyEmail(){
				
		    	if(document.forms['serviceOrderForm'].elements['serviceOrder.email'].value !=''){
		    	var emailSurveyFlag=false;
		    		
		  		  if (validate_email(document.forms['serviceOrderForm'].elements['serviceOrder.email'])== true) {
		           var estimator = document.forms['serviceOrderForm'].elements['serviceOrder.estimator'].value;
		           var survey = document.forms['serviceOrderForm'].elements['serviceOrder.survey'].value;
		          
		           var surveyTime = document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value;
		         
		           var surveyTime2 = document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value;
		           var email = document.forms['serviceOrderForm'].elements['serviceOrder.email'].value;
		           var hour = surveyTime.substring(0, surveyTime.indexOf(":"));
		           var hour2 = surveyTime2.substring(0, surveyTime.indexOf(":"));
		           var min = surveyTime.substring(surveyTime.indexOf(":")+1, surveyTime.length);
		           var min2 = surveyTime2.substring(surveyTime2.indexOf(":")+1, surveyTime2.length);
		           if (hour<12){
		           		surveyTime=hour+":"+min+"AM";
		           } else if ((surveyTime.substring(0, surveyTime.indexOf(":")))>12) {
		               surveyTime=(hour-12)+":"+min+"PM";
		           }else{
		        	 //Putting if block for #7598(14 Feb 2013)        	   
		        	   var emaitime=true;
		        	   
		        	   if(emaitime){
		        		   if(hour==12){
		                  	  hour=0;                    	  
		                    }  
		        	   }
		        	   surveyTime=hour+":"+min+"PM";        	   
		           }
		            if (hour2<12){
		           		surveyTime2=hour2+":"+min2+"AM";
		           } else if (hour2>12) {
		               surveyTime2=(hour2-12)+":"+min2+"PM";
		           }else{
		        	 //Putting if block for #7598(14 Feb 2013)
		        	   var emaitime1=true;
		        	  
		        	   if(emaitime1){
		        	   if(hour2==12){
		              	  hour2=0;              	
		                }
		        	   }
		               surveyTime2=hour2+":"+min2+"PM";
		           }
		           if(estimator=='' || survey=='' || surveyTime =='00:00AM' || surveyTime2=='00:00AM' || surveyTime =='' || surveyTime2=='') { 
		           alert("Survey is not yet Scheduled - Cannot send Survey Email to the customer");
		           } else if (survey !='' && surveyTime!='00:00AM' && surveyTime2 !='00:00AM' && estimator !='' && surveyTime2 !='' && surveyTime2 !=''){
		    	    var systemDate = new Date();
		    	 // For English mail except BOUR
					var mySplitResult = survey.split("-");
				   	var day = mySplitResult[0];
				   	var month = mySplitResult[1];
				   	var month13 = mySplitResult[1];
				   	var year = mySplitResult[2];
				 	if(month == 'Jan'){ month = "01";
				   	}else if(month == 'Feb'){ month = "02";
				   	}else if(month == 'Mar'){ month = "03"
				   	}else if(month == 'Apr'){ month = "04"
				   	}else if(month == 'May'){ month = "05"
				   	}else if(month == 'Jun'){ month = "06"
				   	}else if(month == 'Jul'){ month = "07"
				   	}else if(month == 'Aug'){ month = "08"
				   	}else if(month == 'Sep'){ month = "09"
				   	}else if(month == 'Oct'){ month = "10"
				   	}else if(month == 'Nov'){ month = "11"
				   	}else if(month == 'Dec'){ month = "12";
				   	}
				   	var weekday=new Array(7);
					weekday[0]="Sunday";
					weekday[1]="Monday";
					weekday[2]="Tuesday";
					weekday[3]="Wednesday";
					weekday[4]="Thursday";
					weekday[5]="Friday";
					weekday[6]="Saturday";
					var finalDate = month+"/"+day+"/"+year;
					var finalDate11 = month13+"/"+day+"/"+year;
					//alert("initial:"+finalDate11)
				   	survey = finalDate.split("/");
				   	var enterDate = new Date(survey[0]+"/"+survey[1]+"/20"+survey[2]);
				  	var sendDate = weekday[enterDate.getDay()]+" "+finalDate;
					var sentMonthDate= weekday[enterDate.getDay()]+" "+finalDate11;
					//alert("final format:"+sentMonthDate)
									   
				  	var newsystemDate = new Date(systemDate.getMonth()+1+"/"+systemDate.getDate()+"/"+systemDate.getFullYear());
				  	var daysApart = Math.round((enterDate-newsystemDate)/86400000);
				  	if(daysApart < 0){
				    	alert("The Survey Schedule date is past the current date-The Survey is already over or Please Schedule a new survey.");
				    	return false;
				  	}
				  	if(survey != ''){
			  		if(daysApart == 0){
			  			var time = document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value;
						var hour = time.substring(0, time.indexOf(":"))
						var min = time.substring(time.indexOf(":")+1, time.length);
			  			if (hour < 0  || hour > 23) {
							document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].focus();
							return false;
						}
						if (min<0 || min > 59) {
							document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].focus();
							return false;
						}
						if(systemDate.getHours() > hour){
							alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
							return false;
						} else if(systemDate.getHours() == hour && systemDate.getMinutes() > min){
							alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
							return false;
					}  }   }
				   
				    try{
				  	var checkEnglish = document.forms['serviceOrderForm'].elements['checkEnglish'].checked;
		           
				    }catch(e){} 
		            var agree= confirm("Press OK to Send Survey Email, else press Cancel.");	  	   
		            if(agree){
		            	
		            var email = document.forms['serviceOrderForm'].elements['serviceOrder.email'].value;
		            var email2 = document.forms['serviceOrderForm'].elements['serviceOrder.email2'].value;
					var firstName = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
					var lastName = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
					var surveyDate = document.forms['serviceOrderForm'].elements['serviceOrder.survey'].value; 
					var updatedBy = document.forms['serviceOrderForm'].elements['serviceOrder.updatedBy'].value;
					var subject = "Confirmation for "+firstName+"'s survey on "+surveyDate;
					if(checkEnglish==true ){
						if(email !='' && email2 !='' ){
							 email = email+","+email2;
						}else if(email2 !=''){
							 email =email2;
						}else{
							email =email;
						}
						
						window.open('sendSurveySoEmail.html?id=${serviceOrder.id}&emailTo='+email+'&estimator='+estimator+'&surveyDates='+sendDate+'&surveyMonths='+sentMonthDate+'&firstName='+firstName+'&surveyTime='+surveyTime+'&surveyTime2='+surveyTime2+'&emailSurveyFlag='+emailSurveyFlag+'&checkEnglish='+checkEnglish+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
					}else{
						alert("Please select Language!");
						}
					  } else {             
			  	    }	  	   
		            } else {
		            alert("Survey is not yet Scheduled - Cannot send Survey Email to the customer");
		            }            
		            } else {
		             alert("Invalid Origin's Primary Email Id");
		            } 
		            } else {
		            alert("Primary Email is not yet entered - Cannot send Survey Email to the customer");
		            }   
		        }
			 
			  
			  
			  
			  
			  function calendarICSFunction(){	
					 
					var soId = '${serviceOrder.id}';
					var surveyType = 'SO';
				    var surveyDateSO = document.forms['serviceOrderForm'].elements['serviceOrder.survey'].value;
				    var consultantName = document.forms['serviceOrderForm'].elements['serviceOrder.estimator'].value;
				    var surveyTimeFrom = document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value;
				    var surveyTimeTo = document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value;
				 
					    if(soId==''){
					    	alert('Please save Service File to continue.');
					    }else if(consultantName==''){
					    	alert('Please select Consultant to continue.');
					    }else if(surveyDateSO==''){
					    	alert('Please select Survey date to continue.');
					    }else if(surveyTimeFrom==''){
					    	alert('Please fill Survey From Time to continue.');
					    }else if(surveyTimeTo==''){
					        	alert('Please fill Survey To Time to continue.');
					    }else{
					 		var url = "appointmentICSSO.html?id="+soId+"&consultantName="+consultantName+"&surveyDateSO="+surveyDateSO+"&surveyTimeFrom="+surveyTimeFrom+"&surveyTimeTo="+surveyTimeTo+"&surveyType="+surveyType+"";
					    	location.href=url;
					    	}
			  }
			  
			  
			  function scheduleSurvey()   {
		           var jobTypeSchedule =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
		           var addressLine1=document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value;
		           var addressLine2=document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value;
		          	addressLine1= addressLine1.replace('#','');
		          	addressLine2= addressLine2.replace('#','');
		          	if(addressLine2!=''){
		          	addressLine1=addressLine1+', '+addressLine2;}
		           var city=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		           var country=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
		           var zipCode=document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
		           var state = document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
		           var survey=document.forms['serviceOrderForm'].elements['serviceOrder.survey'].value;
		           var targetAddress=addressLine1+', '+city+', '+state+', '+zipCode+', '+country;
		           var surveyTime="";
			       var surveyTime2="";
			       var estimated="";
			       if(country=='Hong Kong'){
		             zipCode=' ';
		             targetAddress=addressLine1+', '+city+', '+state+','+country; 
		                }	       
		           if(jobTypeSchedule=='' || addressLine1=='' || city =='' || country=='' || zipCode=='')
		           { 
		           alert("Please select job type, address line1, country, city, postal code in origin section.");
		           }
		           else if(survey !='')
		           {
		           var agree= confirm("WARNING: Survey already scheduled, do you want to reschedule? Hit OK to reschedule and Cancel to keep current date.");
		           if(agree){
		           window.open("sendSurveySOSchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
		           }           
		           }  else{
		           window.open("sendSurveySOSchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
		           }   }

				function showPartnerAlert(display, code, position){
					try{
					    if(code == ''){ 
					        if(document.getElementById(position) != null){
					    		document.getElementById(position).style.display="none";
					        }
					    	ajax_hideTooltip();
					    }else if(code != ''){
					    	getBAPartner(display,code,position);
					    }  }catch(e){} }
				
				function findOriginAgentName(){
				    var originAgentCode = document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].value; 
				    if(originAgentCode==''){
				    	document.forms['serviceOrderForm'].elements['serviceOrder.originAgentName'].value="";
				    }
				    if(originAgentCode!=''){
				     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(originAgentCode);
				     http2.open("GET", url, true);
				     http2.onreadystatechange = handleHttpResponse44;
				     http2.send(null);
				    } 
				}
				function handleHttpResponse44(){
					<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">		
						<c:set var="classifiedAgentOriginValidation" value="Y" />
					</configByCorp:fieldVisibility>
					<configByCorp:fieldVisibility componentId="component.field.customerFile.notClassified">		
						<c:set var="classifiedAgentOriginValidation" value="N" />
					</configByCorp:fieldVisibility>
					if (http2.readyState == 4) {
				          var results = http2.responseText
				          results = results.trim();
				          var res = results.split("#"); 
				          if(res.length>2){
				            	if(res[2] == 'Approved'){
				            		<c:if test="${classifiedAgentOriginValidation=='Y'}">
				            		if(res[11]=='' && res[8]=='T'){
				            			alert("This Agent cannot be processed as it is not classified.")
				            			document.forms['serviceOrderForm'].elements['serviceOrder.originAgentName'].value="";
				            			document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].value="";
				            			document.forms['serviceOrderForm'].elements['serviceOrder.originAgentEmail'].value ="";
				            			document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].select();
				            			return false;
				            		}else{
				            			document.forms['serviceOrderForm'].elements['serviceOrder.originAgentName'].value = res[1];
				            			document.forms['serviceOrderForm'].elements['serviceOrder.originAgentEmail'].value = res[4];
				            			document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].select();
				                   	   getCustomerContactInfo();
				                   }
				            		</c:if>
				            		<c:if test="${classifiedAgentOriginValidation!='Y'}">
					            		document.forms['serviceOrderForm'].elements['serviceOrder.originAgentName'].value = res[1];
					           			document.forms['serviceOrderForm'].elements['serviceOrder.originAgentEmail'].value = res[4];
					               		document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].select();
					               	   getCustomerContactInfo();
				             		</c:if>
				            	}else{
				           			alert("Origin Agent code is not approved" ); 
								    document.forms['serviceOrderForm'].elements['serviceOrder.originAgentName'].value="";
								    document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].value="";
								    document.forms['serviceOrderForm'].elements['serviceOrder.originAgentEmail'].value ="";
								    document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].select();
								 showPartnerAlert('onchange','','hidTSOriginAgent');
				           		}  
				            }else{
				                 alert("Origin Agent code not valid");
				                 document.forms['serviceOrderForm'].elements['serviceOrder.originAgentName'].value="";
								 document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].value="";
								 document.forms['serviceOrderForm'].elements['serviceOrder.originAgentEmail'].value ="";
								 document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].select();
								 showPartnerAlert('onchange','','hidTSOriginAgent');
				           }
				  }
				}   
				function getCustomerContactInfo()
				{      
				        var originAgentCode =document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].value; 
					    var url="getContactInfo.html?ajax=1&decorator=simple&popup=true&originAgentInfo="+originAgentCode;
					    http7.open("GET", url, true);
					    http7.onreadystatechange =   handleHttpResponseDefaultContact;
				        http7.send(null);
				}
				function handleHttpResponseDefaultContact() {  
					
					  if (http7.readyState == 4) {
						
						       var results = http7.responseText ;  
					           results = results.trim();   
				               var res = results.split("~"); 
				            if(res.length>1 )
				          	   {
				              if(res[0]!=undefined &&  res[0]!="undefined" && res[0] !=""  ){
				              	   document.forms['serviceOrderForm'].elements['serviceOrder.originAgentContact'].value = res[0];
				         		}if(res[1]!=undefined &&  res[1]!="undefined" &&  res[1]!="" ){
				              	   document.forms['serviceOrderForm'].elements['serviceOrder.originAgentEmail'].value = res[1];
				         		} 
				         		if(res[2]!=undefined &&  res[2]!="undefined" && res[2]!=""){
				              	   document.forms['serviceOrderForm'].elements['serviceOrder.originAgentPhoneNumber'].value = res[2];
				         		}
				         		}
				          	   
					       	}}	
					
					var http7 = getHTTPObjectst();
					function getHTTPObjectst() {
				    var xmlhttp;
				    if(window.XMLHttpRequest) {
				      xmlhttp = new XMLHttpRequest();
				  }
				  else if (window.ActiveXObject)  {
				      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				      if (!xmlhttp)  {
				          xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
				      }   } 
				  return xmlhttp;
				} 
					function showPartnerAlert(display, code, position){
					    if(code == ''){ 
					    	
					 	document.getElementById(position).style.display="none";
					 
					    }else if(code != ''){

					    }  }
					function resetContactEmail(agentType){
						if(agentType == 'OA'){
							document.forms['serviceOrderForm'].elements['serviceOrder.originAgentContact'].value = '';
							document.forms['serviceOrderForm'].elements['serviceOrder.originAgentEmail'].value = '';
							document.forms['serviceOrderForm'].elements['serviceOrder.originAgentPhoneNumber'].value = '';
							
						}}
					function reseEmails(agentType,str){
						str=str.trim();
						if(str.length == 0){
							resetContactEmail(agentType);
						}  
							var con=document.forms['serviceOrderForm'].elements['serviceOrder.originAgentContact'].value ;
							con=con.trim(); 
							var email=document.forms['serviceOrderForm'].elements['serviceOrder.originAgentEmail'].value ;
							var pcode=document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].value ;
							if(con!=null &&  con.length>0){			
								var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
								http234.open("GET", url, true);
								http234.onreadystatechange = handleHttpchkPartnerAgentRUC;
								http234.send(null);
							}else{
								document.getElementById('hidActiveAP').style.display="none";
								document.forms['serviceOrderForm'].elements['serviceOrder.originAgentContactImgFlag'].value='';
							} } 
					function handleHttpchkPartnerAgentRUC(){ 
						if (http234.readyState == 4){
							var results = http234.responseText
					        results = results.trim();		
								if(results=='AG'){	
									document.getElementById("hidActiveAP").style.display="block";	
									document.forms['serviceOrderForm'].elements['serviceOrder.originAgentContactImgFlag'].value='true';
								}else{ 
									document.getElementById("hidActiveAP").style.display="none";
									document.forms['serviceOrderForm'].elements['serviceOrder.originAgentContactImgFlag'].value='';	
								}  } }
					
					function getContactInfo(agentType,partnerCode){
						 if(agentType.length > 0 && partnerCode.length >0){
						 openWindow('userServiceContacts.html?agentType='+agentType+'&partnerCode='+partnerCode+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=serviceOrder.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=serviceOrder.originAgentVanlineCode&fld_description=serviceOrder.originAgent&fld_code=serviceOrder.originAgentCode',1000,550);
					}} 
					function checkEmail(fieldId) {
						var email = document.forms['serviceOrderForm'].elements[fieldId];
						var filter = /^([a-zA-Z0-9_\.\-\'])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
							if (!filter.test(email.value)) {
								alert('Please provide a valid email address');
								document.forms['serviceOrderForm'].elements[fieldId].value='';
								document.forms['serviceOrderForm'].elements[fieldId].focus();
								return false;
							} }
					function winOpenOrigin(){
					        openWindow('ClassifiedAgentPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=serviceOrder.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.originAgentName&fld_code=serviceOrder.originAgentCode'); 
                            document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].select(); 						
					}
					function showAddressImage(){
					
					    var agentCode=document.forms['serviceOrderForm'].elements['serviceOrder.originAgentCode'].value; 
					    if(agentCode==''){ 
					    
					    document.getElementById("hidImagePlus").style.display="none";
					    document.forms['serviceOrderForm'].elements['serviceOrder.originAgentContact'].value='';
					    document.forms['serviceOrderForm'].elements['serviceOrder.originAgentEmail'].value='';
					    document.forms['serviceOrderForm'].elements['serviceOrder.originAgentPhoneNumber'].value='';
					   
					    } else if(agentCode!='') {
					     document.getElementById("hidImagePlus").style.display="block";
					    } }
					
					function completeTimeString() {	
						stime1 = document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value;
						stime2 = document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value;
						stime3="";
						stime4="";
						if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
							if(stime1.length==1 || stime1.length==2){
								if(stime1.length==2){
									document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value = stime1 + ":00";
								}
								if(stime1.length==1){
									document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value = "0" + stime1 + ":00";
								}
							}else{
								document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value = stime1 + "00";
							}
						}else{
							if(stime1.indexOf(":") == -1 && stime1.length==3){
								document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
							}
							if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
								document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
							}
							if(stime1.indexOf(":") == 1){
								document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value = "0" + stime1;
							}
						}
						if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
							if(stime2.length==1 || stime2.length==2){
								if(stime2.length==2){
									document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value = stime2 + ":00";
								}
								if(stime2.length==1){
									document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value = "0" + stime2 + ":00";
								}
							}else{
								document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value = stime2 + "00";
							}
						}else{
							if(stime2.indexOf(":") == -1 && stime2.length==3){
								document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
							}
							if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
								document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
							}
							if(stime2.indexOf(":") == 1){
								document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value = "0" + stime2;
							} } 
					}
					function IsValidTime(clickType) {
						

						var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
							var timeStr = document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value;
							var matchArray = timeStr.match(timePat);
							if (matchArray == null){
								alert("Time is not in a valid format. Please use HH:MM format");
								document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].focus();
								return false;
							}
						hour = matchArray[1];
						minute = matchArray[2];
						second = matchArray[4];
						ampm = matchArray[6];
						if (second=="") { second = null; }
						if (ampm=="") { ampm = null }
						if (hour < 0  || hour > 23) {
						alert("'Pre Move Survey' time must between 0 to 23:59 (Hrs)");
						document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].focus();
						return false;
						}
						if (minute<0 || minute > 59) {
						alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
						document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].focus();
						return false;
						}
						if (second != null && (second < 0 || second > 59)) {
						alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
						document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].focus();
						return false;
						}
						// **************Check for Survey Time2*************************
						var time2Str = document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value;
						var matchTime2Array = time2Str.match(timePat);
						if (matchTime2Array == null) {
						alert("Time is not in a valid format. please Use HH:MM format");
						document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].focus();
						return false;
						}
						hourTime2 = matchTime2Array[1];
						minuteTime2 = matchTime2Array[2];
						secondTime2 = matchTime2Array[4];
						ampmTime2 = matchTime2Array[6];
						if (hourTime2 < 0  || hourTime2 > 23) {
							
						alert("'Pre Move Survey' time must between 0 to 23:59 (Hrs)");
						document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].focus();
						return false;
						}
						
					     validatePricing('Save');
				        }
					
					function surveyDateTime() {
					
						var HH2;
						var HH1;
						var MM2;
						var MM1;
						var tim1=document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value;
						var tim2=document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value;
							tim1=tim1.replace(":","");
							tim2=tim2.replace(":","");
						if((tim1.substring(0,tim1.length-2)).length == 1){
							HH1 = '0'+tim1.substring(0,tim1.length-2);
						}else {
							HH1 = tim1.substring(0,tim1.length-2);
						}
						MM1 = tim1.substring(tim1.length-2,tim1.length);
						if((tim2.substring(0,tim1.length-2)).length == 1){
							HH2 = '0'+tim2.substring(0,tim2.length-2);
						}else {
							HH2 = tim2.substring(0,tim2.length-2);
						}
						MM2 = tim2.substring(tim2.length-2,tim2.length);
						document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime'].value = HH1 + ':' + MM1;
						document.forms['serviceOrderForm'].elements['serviceOrder.surveyTime2'].value = HH2 + ':' + MM2;

						var f = document.getElementById('serviceOrderForm'); 
						f.setAttribute("autocomplete", "off"); 
					} 
			   
	</script> 