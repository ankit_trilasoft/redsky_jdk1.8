package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsRelocationExpenseManagement;

public interface DsRelocationExpenseManagementDao extends GenericDao<DsRelocationExpenseManagement, Long>{
	List getListById(Long id);
}

