package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CrewCapacity;

public interface CrewCapacityManager extends GenericManager<CrewCapacity,Long>{
	
	public List<CrewCapacity> findRecordByHub(String crewGroupval, String sessionCorpID);
	public List findAllCrewGroup(String corpId);
	public List findOldGroupName(String sessionCorpID, String gName);
	public Map<String,String> findExistingJobName(String sessionCorpID);
	public List findParentHubByWarehouse(String warehouse,String sessionCorpID);
	public CrewCapacity findGroupNameFromCrewCapacity(String jobType, String sessionCorpID);
}
