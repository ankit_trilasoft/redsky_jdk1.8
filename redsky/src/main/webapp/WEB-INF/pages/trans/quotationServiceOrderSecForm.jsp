<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<script type="text/JavaScript">

  function pricingOverFlow() { 
	  var elem = document.getElementById("para1");
	  
	  if (screen.width > 1600 ){
				 elem.style.overflow = 'none';
				 elem.style.width = '100%';
		 }else if (screen.width == 1440 ){
				 elem.style.overflow = 'auto';
				 elem.style.width = '1320px';
		 }else if (screen.width >= 1360 ){
				 elem.style.overflow = 'auto';
				 elem.style.width = '1250px';
		 }else if(screen.width == 1280) {
	    	  elem.style.overflow = 'auto';
	    	  elem.style.width = '1160px';
          }else if(screen.width == 1152) {
	    	  elem.style.overflow = 'auto';
	    	  elem.style.width = '1035px';
          }else if(screen.width == 1024){
	    	  elem.style.overflow = 'auto';
	    	  elem.style.width = '910px';
          }
}
</script>
<script type="text/JavaScript">
function callPostalCode(){
	window.open('http://www.canadapost.ca/cpo/mc/personal/postalcode/fpc.jsf', '_blank');
}
</script>
  <script type="text/javascript">
    var location1;
  	var location2;	
  	var address1;
  	var address2;
  	var latlng;
  	var geocoder;
  	var map;	
  	var distance;
  	var gDir;	
  	function initLoader() { 
  		 var script = document.createElement("script");
  	     script.type = "text/javascript";
  		script.src = "https://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=initialize";
  	     document.body.appendChild(script);
  	}
	function initialize() {
	 geocoder = new GClientGeocoder();
		 gDir = new GDirections();
		 GEvent.addListener(gDir, "load", function() {
		 var drivingDistanceMiles = gDir.getDistance().meters / 1609.344;
		 var drivingDistanceKilometers = gDir.getDistance().meters / 1000;
		 var chkSelected=document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].value;
		 var sysUnit=document.forms['serviceOrderForm'].elements['unit'].value;
		
		if(chkSelected==''){
		chkSelected=sysUnit;
		}
		if(chkSelected=='Mile'){
		document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value=Math.round(drivingDistanceMiles*100)/100;
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].text = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].value = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].selected=true;
		checkUnitValidation();
		}
		if(chkSelected=='KM'){
		document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value=Math.round(drivingDistanceKilometers*100)/100;
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[1].text = 'KM';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[1].value = 'KM';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[1].selected=true;	
		checkUnitValidation();
		}
	});
//setTimeout('',6000);
}



function calculateDistance() {
		var origAddress1=document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value;
		var origAddress2=document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value;
		var origAddress3=document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value;
		var origCity=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		var origZip=document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
		var origState=document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
		var dd = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].selectedIndex;
		var origCountry = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[dd].text;
   	var address1 = origAddress1+" "+origAddress2+" "+origAddress3+","+origCity+" , "+origZip+","+origState+","+origCountry;
		var destAddress1=document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value;
		var destAddress2=document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value;
		var destAddress3=document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value;				
		var destCity=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
		var destZip=document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
		var destState=document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
		var dd = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].selectedIndex;
		var destCountry = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[dd].text;
   		var address2 = destAddress1+" "+destAddress2+" "+destAddress3+","+destCity+" , "+destZip+","+destState+","+destCountry;
   	    //geocoder = new GClientGeocoder();
   		geocoder.getLocations(address1, function (response) {
			if (!response || response.Status.code != 200)
				{
					alert("Sorry, we were unable to geocode the first address");
				}
			else
				{
					location1 = {lat: response.Placemark[0].Point.coordinates[1], lon: response.Placemark[0].Point.coordinates[0], address: response.Placemark[0].address};
					geocoder.getLocations(address2, function (response) {
					if (!response || response.Status.code != 200)
					{
						alert("Sorry, we were unable to geocode the second address");
					}
					else
					{
						location2 = {lat: response.Placemark[0].Point.coordinates[1], lon: response.Placemark[0].Point.coordinates[0], address: response.Placemark[0].address};
						gDir.load('from: ' + location1.address + ' to: ' + location2.address);
					}
				});
			}
		});
	}

<%--
 function findRevisedEstimateQuantitys(chargeCode, category, estimateExpense, basis, estimateQuantity, estimateRate, estimateSellRate, estimateRevenueAmount, estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
{
var charge = document.forms['serviceOrderForm'].elements[chargeCode].value;
var billingContract = document.forms['serviceOrderForm'].elements['customerFile.contract'].value;
var shipNumber=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
var rev=document.forms['serviceOrderForm'].elements[category].value;
var comptetive = document.forms['serviceOrderForm'].elements['inComptetive'].value;
if(charge!=''){
if(rev == "Internal Cost"){
    if(charge=="VANPAY" ){
     	getVanPayCharges("Estimate");
    }else if(charge=="DOMCOMM" ){
     	getDomCommCharges("Estimate");
    }else if(charge=="SALESCM" && comptetive=="Y") { 
	    var totalExpense = eval(document.forms['serviceOrderForm'].elements['inEstimatedTotalExpense'].value);
	    var totalRevenue =eval(document.forms['serviceOrderForm'].elements['inEstimatedTotalRevenue'].value); 
	    var commisionRate =eval(document.forms['serviceOrderForm'].elements['salesCommisionRate'].value); 
	    var grossMargin = eval(document.forms['serviceOrderForm'].elements['grossMarginThreshold'].value);
	    var commision=((totalRevenue*commisionRate)/100);
	    var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
	    calGrossMargin=calGrossMargin*100;
	    var calGrossMargin=Math.round(calGrossMargin);
	    if(calGrossMargin>=grossMargin)  {
	    	document.forms['serviceOrderForm'].elements[estimateExpense].value=Math.round(commision*100)/100; 
	    	document.forms['serviceOrderForm'].elements[basis].value='each';
	    	document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
	    } else { 
		    commision=((.8*totalRevenue)-totalExpense); 
		    commision=Math.round(commision*100)/100;
		    if(commision<0) { 
		    	document.forms['serviceOrderForm'].elements[estimateExpense].value=0; 
		    	document.forms['serviceOrderForm'].elements[basis].value='each';
		    	document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
		    } else {
		    	document.forms['serviceOrderForm'].elements[estimateExpense].value=Math.round(commision*100)/100;
		    	document.forms['serviceOrderForm'].elements[basis].value='each';
		    	document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
		    } 
	    }  
    }else if(charge=="SALESCM" && comptetive!="Y") {
    var agree =confirm("This job is not competitive; do you still want to calculate commission?");
    if(agree) {
    var accountCompanyDivision =  document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value 
    var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id};
    http88.open("GET", url, true);
    http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate,basis);};
    http88.send(null);
    } else {
    return false;
    }
    } else {
    var accountCompanyDivision =  document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value 
    var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id};
    http88.open("GET", url, true);
    http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate, basis);};
    http88.send(null);
    }
} else{	
	var url="invoiceExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id};
    http88.open("GET", url, true);
    http88.onreadystatechange = function(){ handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote, deviation,estimateDeviation, estimateSellDeviation,aid);};
    http88.send(null);
	} 
   }else{
    alert("Please select chargeCode!");
   }
} 



  	function handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate, basis) {
          if (http88.readyState == 4)  {
                  var results = http88.responseText
                  results = results.trim(); 
                  var res = results.split("#");
                  if(res[3]=="AskUser1"){
                  var reply = prompt("Please Enter Quantity", "");
                  if(reply)   {
                  if((reply>0 || reply<9)|| reply=='.') {
               	    estimateQuantity.value = Math.round(reply*100)/100;
               	}  else  {
               		alert("Please Enter legitimate Quantity");
               	} }
               	 else{
               		document.forms['serviceOrderForm'].elements[estimateQuantity].value = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
               }  }
               else{
               if(res[1] == undefined){
            	   document.forms['serviceOrderForm'].elements[estimateQuantity].value ="0.00";
               }else{
            	   document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(res[1]*100)/100 ;
               }  }
               if(res[5]=="AskUser"){
               var reply = prompt("Please Enter the value of Rate", "");
               if(reply)  {
               if((reply>0 || reply<9)|| reply=='.')  {
            	   document.forms['serviceOrderForm'].elements[estimateRate].value =Math.round(reply*100)/100 ;
               	} else {
               		alert("Please Enter legitimate Rate");
               	} 	}
               	 else{
               		document.forms['serviceOrderForm'].elements[estimateRate].value = document.forms['serviceOrderForm'].elements[estimateRate].value;
               } 
               }else{
               if(res[4] == undefined){
            	   document.forms['serviceOrderForm'].elements[estimateRate].value ="0.00";
               }else{
            	   document.forms['serviceOrderForm'].elements[estimateRate].value = Math.round(res[4]*100)/100 ;
               }  }
               if(res[5]=="BuildFormula")
               {
               if(res[6] == undefined){
            	   document.forms['serviceOrderForm'].elements[estimateRate].value ="0.00";
               }else{
            	   document.forms['serviceOrderForm'].elements[estimateRate].value = Math.round(res[6]*100)/100;
               }  }
              var Q1 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
              var Q2 = document.forms['serviceOrderForm'].elements[estimateRate].value;
              var E1=Q1*Q2;
              var E2="";
              E2=Math.round(E1*100)/100;
              document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
              var type = res[8];
                var typeConversion=res[10];
                if(type == 'Division')  {
                	 if(typeConversion==0)  {
                	   E1=0*1;
                	   document.forms['serviceOrderForm'].elements[estimateExpense].value = E1;
                	 } else  {	
                		E1=(Q1*Q2)/typeConversion;
                		E2=Math.round(E1*100)/100;
                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
                	 }  }
                if(type == ' ')  {
                		E1=(Q1*Q2);
                		E2=Math.round(E1*100)/100;
                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
                }
                if(type == 'Multiply')  {
                		E1=(Q1*Q2*typeConversion);
                		E2=Math.round(E1*100)/100;
                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
                } 
                basis.value=res[9];

               document.forms['serviceOrderForm'].elements['oldEstimateSellDeviation'].value=0;
               document.forms['serviceOrderForm'].elements['estimateDeviation'].value=0;
               document.forms['serviceOrderForm'].elements['oldEstimateDeviation'].value=0;
               document.forms['serviceOrderForm'].elements['estimateSellDeviation'].value=0; 
            }  
      }
  	
  	function handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote, deviation,estimateDeviation, estimateSellDeviation,aid)
  	{
  	     if (http88.readyState == 4)  {
  	        var results = http88.responseText;
  	        results = results.trim(); 

  	      <c:if test="${!contractType}">
  	        var res = results.split("#");
  	        var multquantity=res[14];
            if(res[17] == undefined){             
         	   document.forms['serviceOrderForm'].elements[estimateRate].value ="0.00";
            }else{                  
         	   document.forms['serviceOrderForm'].elements[estimateRate].value = Math.round(res[17]*100)/100 ;
            }
  	        if(res[3]=="AskUser1"){ 
  	           var reply = prompt("Please Enter Quantity", "");
  	             if(reply) {
	  	             if((reply>0 || reply<9)|| reply=='.')  {
	  	            	document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(reply*100)/100;
	  	             }else {
	  	                alert("Please Enter legitimate Quantity");
	  	             } 
  	             }else{
  	             	document.forms['serviceOrderForm'].elements[estimateQuantity].value = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
  	             }
           	}else{             
	            if(res[1] == undefined){
	         	 	document.forms['serviceOrderForm'].elements[estimateQuantity].value ="0.00";
	            }else{
	         	 	document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(res[1]*100)/100;
	            }  
           	}
  	       	if(res[5]=="AskUser"){ 
               var reply = prompt("Please Enter the value of Rate", "");
               if(reply)  {
	               if((reply>0 || reply<9)|| reply=='.') {
	            	 	document.forms['serviceOrderForm'].elements[estimateSellRate].value = Math.round(reply*100)/100;
	               } else {
	               		alert("Please Enter legitimate Rate");
	               	} 
	            }else{
               		document.forms['serviceOrderForm'].elements[estimateRate].value = document.forms['serviceOrderForm'].elements[estimateRate].value;
               	} 
  	       	}else{
               if(res[4] == undefined){
            	 document.forms['serviceOrderForm'].elements[estimateSellRate].value ="0.00";
               }else{
            	 document.forms['serviceOrderForm'].elements[estimateSellRate].value = Math.round(res[4]*100)/100 ;
               }  
  	      	 }
  	        if(res[5]=="BuildFormula"){
  	          if(res[6] == undefined){
  	           		document.forms['serviceOrderForm'].elements[estimateSellRate].value ="0.00";
  	           }else{
  	            	document.forms['serviceOrderForm'].elements[estimateSellRate].value = Math.round(res[6]*100)/100 ;
  	           }  
	         }

  	          
            var Q1 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
            var Q2 = document.forms['serviceOrderForm'].elements[estimateSellRate].value;
            var oldRevenue = eval(document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value);
            var oldExpense=eval(document.forms['serviceOrderForm'].elements[estimateExpense].value);
            var E1=Q1*Q2;
            var E2="";
            E2=Math.round(E1*100)/100;
            document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E2;
            var type = res[8];
              var typeConversion=res[10];   
              if(type == 'Division')   {
              	 if(typeConversion==0) 	 {
              	   E1=0*1;
              	 	document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E1;
              	 } else {	
              		E1=(Q1*Q2)/typeConversion;
              		E2=Math.round(E1*100)/100;
              		document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E2;
              	 } }
              if(type == ' ') {
              		E1=(Q1*Q2);
              		E2=Math.round(E1*100)/100;
              		document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E2;
              }
              if(type == 'Multiply')  {
              		E1=(Q1*Q2*typeConversion);
              		E2=Math.round(E1*100)/100;
              		document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value = E2;
              }

              var Q11 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
              var Q22 = document.forms['serviceOrderForm'].elements[estimateRate].value;
              var E11=Q11*Q22;
              var E22="";
              E22=Math.round(E11*100)/100;
              document.forms['serviceOrderForm'].elements[estimateExpense].value = E22;
                if(type == 'Division')  {
                	 if(typeConversion==0)  {
                	   E11=0*1;
                	   document.forms['serviceOrderForm'].elements[estimateExpense].value = E11;
                	 } else  {	
                		E11=(Q11*Q22)/typeConversion;
                		E22=Math.round(E11*100)/100;
                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E22;
                	 }  }
                if(type == ' ')  {
                		E11=(Q11*Q22);
                		E22=Math.round(E11*100)/100;
                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E22;
                }
                if(type == 'Multiply')  {
                		E11=(Q11*Q22*typeConversion);
                		E22=Math.round(E11*100)/100;
                		document.forms['serviceOrderForm'].elements[estimateExpense].value = E22;
                }
  	             
  	         document.forms['serviceOrderForm'].elements[basis].value=res[9];
             var chargedeviation=res[15];
             if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
            	 var buyDependSellAccount=res[11];
	             var estimateRevenueAmt=0;
	             var finalEstimateRevenueAmount=0; 
	             var finalEstimateExpenceAmount=0;
	             var estimateSellRates=0; 
	             var sellDeviation=0;
	             var estimatepasspercentages=0;
	             var estimatepasspercentageRound=0;
	             var buyDeviation=0;
	             var oldEstDeviation =  document.forms['serviceOrderForm'].elements[estimateDeviation].value;
	             var oldEstSellDeviation = document.forms['serviceOrderForm'].elements[estimateSellDeviation].value;
	             sellDeviation=res[12];
	             buyDeviation=res[13]; 
            	 document.forms['serviceOrderForm'].elements[estimateSellDeviation].value=sellDeviation;
		         document.forms['serviceOrderForm'].elements[estimateDeviation].value=buyDeviation;
		        
            	 document.forms['serviceOrderForm'].elements[deviation].value=chargedeviation; 
	             document.forms['serviceOrderForm'].elements['buyDependSell'].value=res[11];
	            
	             estimateRevenueAmt=document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value; 
	             if(sellDeviation==0 && sellDeviation==undefined){
	            	 sellDeviation = document.forms['serviceOrderForm'].elements[oldEstSellDeviation].value;
		         }

	             finalEstimateRevenueAmount=Math.round((estimateRevenueAmt*(sellDeviation/100))*100)/100; 
	             document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=finalEstimateRevenueAmount;
	             if(buyDeviation==0 && buyDeviation==undefined){
	            	 buyDeviation = document.forms['serviceOrderForm'].elements[oldEstDeviation].value;
		         }

	             if(buyDependSellAccount=="Y"){
	              	finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation/100))*100)/100;
	             }else{
	              	finalEstimateExpenceAmount= Math.round((estimateRevenueAmt*(buyDeviation/100))*100)/100;
	             }
	             document.forms['serviceOrderForm'].elements[estimateExpense].value=finalEstimateExpenceAmount;
	             estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
	 	         estimatepasspercentageRound=Math.round(estimatepasspercentages);
	 	         if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
	 	   	       document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
	 	         }else{
	 	   	       document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
	 	         }
	 	         // calculate estimate rate 
	 	        var estimateQuantity1 =0;
	 		    var estimateDeviation1=100;
	 		    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
	 		    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
	 		    var estimateExpense1 =eval(document.forms['serviceOrderForm'].elements[estimateExpense].value);
	 		    //alert(estimateExpense1) 
	 		    var estimateRate1=0;
	 		    var estimateRateRound1=0; 
	 		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
	 	         estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
	 	         } 
	 	        if( basis1=="cwt" || basis1=="%age"){
	 	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
	 	       	  estimateRateRound1=Math.round(estimateRate1*100)/100;
	 	       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound1;	  	
	 	       	}  else if(basis1=="per 1000"){
	 	       	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
	 	       	  estimateRateRound1=Math.round(estimateRate1*100)/100;
	 	       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound1;		  	
	 	       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
	 	          estimateRate1=(estimateExpense1/estimateQuantity1); 
	 	       	  estimateRateRound1=Math.round(estimateRate1*100)/100; 
	 	       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound1;		  	
	 		    }
	 		    
	 		    estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value 
	 		    if(estimateDeviation1!='' && estimateDeviation1>0){
		 		    estimateRate1=estimateRateRound1*100/estimateDeviation1;
		 		    estimateRateRound1=Math.round(estimateRate1*100)/100;
		 		    document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound1;		
	 		    }
             }else {
            	 document.forms['serviceOrderForm'].elements[estimateDeviation].value=0.00;
	             document.forms['serviceOrderForm'].elements[estimateSellDeviation].value=0.00;
	             var bayRate=0
	             bayRate = res[17] ;
	             if(bayRate!=0 && bayRate!=undefined){
	               var estimateQuantityPay=0;
	               estimateQuantityPay= document.forms['serviceOrderForm'].elements[estimateQuantity].value;
	               Q1=estimateQuantityPay;
	               Q2=bayRate; 
	               if(type == 'Division')   {
	               	 if(typeConversion==0) 	 {
	               	   E1=0*1;
	               	   document.forms['serviceOrderForm'].elements[estimateExpense].value = E1;
	               	 } else {	
	               		E1=(Q1*Q2)/typeConversion;
	               		E2=Math.round(E1*100)/100;
	               		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
	               	 } }
	               if(type == ' ') {
	               		E1=(Q1*Q2);
	               		E2=Math.round(E1*100)/100;
	               		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
	               }
	               if(type == 'Multiply')  {
	               		E1=(Q1*Q2*typeConversion);
	               		E2=Math.round(E1*100)/100;
	               		document.forms['serviceOrderForm'].elements[estimateExpense].value = E2;
	               }
	               document.forms['serviceOrderForm'].elements[estimateRate].value = Q2;
	               var  estimatepasspercentages=0
	               var estimatepasspercentageRound=0;
	               var finalEstimateRevenueAmount=0; 
	               var finalEstimateExpenceAmount=0;
	               finalEstimateRevenueAmount=  document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
	               finalEstimateExpenceAmount=document.forms['serviceOrderForm'].elements[estimateExpense].value;
	               estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
	  	           estimatepasspercentageRound=Math.round(estimatepasspercentages);
	  	           if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
	  	   	           document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
	  	           }else{
	  	   	           document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
	  	           } 
	             }else{	           	   
	               var quantity =0;
	               var rate =0;
	               var estimatepasspercentages=0;
	               var estimatepasspercentageRound=0;
	               var finalEstimateRevenueAmount=0; 
	               var finalEstimateExpenceAmount=0;
	               quantity= document.forms['serviceOrderForm'].elements[estimateQuantity].value;	               
	               rate=document.forms['serviceOrderForm'].elements[estimateRate].value;	               
	               finalEstimateExpenceAmount = (quantity*rate); 
	               if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age")  {
	      	          finalEstimateExpenceAmount = finalEstimateExpenceAmount/100; 
	               }
	               if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000")  {
	      	        	finalEstimateExpenceAmount = finalEstimateExpenceAmount/1000; 
	               }
	               finalEstimateExpenceAmount= Math.round((finalEstimateExpenceAmount)*100)/100;
	               finalEstimateRevenueAmount=  document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
	               estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
	  	           estimatepasspercentageRound=Math.round(estimatepasspercentages);
       
	  	           if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
	  	   	           document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
	  	           }else{
	  	   	           document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
	  	           }
	            }
	             
            }

           //#7639 - Issue with Compute functionality in Account line and Pricing section start
      	    var basisValue = document.forms['serviceOrderForm'].elements[basis].value;
	            var estLocalAmt1=0;         
              try{
			            estLocalAmt1= document.forms['serviceOrderForm'].elements[estimateExpense].value;
			            if(basisValue=="cwt" || basisValue== "%age"){
			            	estLocalAmt1=estLocalAmt1/100 ;
			                }
			                else if(basisValue=="per 1000"){
			                	estLocalAmt1=estLocalAmt1/1000 ;
			                }else{
			                	estLocalAmt1=estLocalAmt1;
			                }
			            estLocalAmt1=Math.round(estLocalAmt1*100)/100;
		                document.forms['serviceOrderForm'].elements[estimateExpense].value=estLocalAmt1;
              }catch(e){}
              try{
			            estLocalAmt1= document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
			            if(basisValue=="cwt" || basisValue== "%age"){
			            	estLocalAmt1=estLocalAmt1/100 ;
			                }
			                else if(basisValue=="per 1000"){
			                	estLocalAmt1=estLocalAmt1/1000 ;
			                }else{
			                	estLocalAmt1=estLocalAmt1;
			                }
			            estLocalAmt1=Math.round(estLocalAmt1*100)/100;
		                document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=estLocalAmt1;
               }catch(e){}                 
               //#7639 - Issue with Compute functionality in Account line and Pricing section End
               
/////////changes
             try{
		     	    var basisValue = document.forms['serviceOrderForm'].elements[basis].value;
		    	    checkFloatNew('serviceOrderForm',estimateQuantity,'Nothing');
		    	    var quantityValue = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
		    	    		             
		 	        var buyRate1=document.forms['serviceOrderForm'].elements['estimateRate'+aid].value;
			        var estCurrency1=document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value;
					if(estCurrency1.trim()!=""){
							var Q1=0;
							var Q2=0;
							Q1=buyRate1;
							Q2=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
				            var E1=Q1*Q2;
				            E1=Math.round(E1*100)/100;
				            document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=E1;
				            var estLocalAmt1=0;
				            if(basisValue=="cwt" || basisValue== "%age"){
				            	estLocalAmt1=(quantityValue*E1)/100 ;
				                }
				                else if(basisValue=="per 1000"){
				                	estLocalAmt1=(quantityValue*E1)/1000 ;
				                }else{
				                	estLocalAmt1=(quantityValue*E1);
				                }	 
				            estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
				            document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estLocalAmt1;     
					}

			        buyRate1=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value;
			        estCurrency1=document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value;
					if(estCurrency1.trim()!=""){
							var Q1=0;
							var Q2=0;
							Q1=buyRate1;
							Q2=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
				            var E1=Q1*Q2;
				            E1=Math.round(E1*100)/100;
				            document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=E1;
				            var estLocalAmt1=0;
				            if(basisValue=="cwt" || basisValue== "%age"){
				            	estLocalAmt1=(quantityValue*E1)/100 ;
				                }
				                else if(basisValue=="per 1000"){
				                	estLocalAmt1=(quantityValue*E1)/1000 ;
				                }else{
				                	estLocalAmt1=(quantityValue*E1);
				                }	 
				            estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
				            document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
					}
					var type01 = res[8];
				     var typeConversion01=res[10]; 					
					//#7298 - Copy esimates not calculating preset calculations in charges start
					  var tempData= document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value;
					               if(type01 == 'Division')   {
						               try{
						            	   tempData=tempData/typeConversion01; 
						               }catch(e){
						            	   tempData=0;
							               }
					               }else if(type01 == 'Multiply')   {
					            	   tempData=tempData*typeConversion01; 
					               }else{
					            	   tempData=tempData;
					               }
		       				            tempData=Math.round(tempData*100)/100;
					  document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=tempData;
		 	                 //#7298 - Copy esimates not calculating preset calculations in charges End	 					
			          //#7298 - Copy esimates not calculating preset calculations in charges start
					  var tempData= document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value;
					               if(type01 == 'Division')   {
						               try{
						            	   tempData=tempData/typeConversion01; 
						               }catch(e){
						            	   tempData=0;
							               }
					               }else if(type01 == 'Multiply')   {
					            	   tempData=tempData*typeConversion01; 
					               }else{
					            	   tempData=tempData;
					               }
		       				            tempData=Math.round(tempData*100)/100;
					  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=tempData;
		 	                 //#7298 - Copy esimates not calculating preset calculations in charges End						
					}catch(e){}
			        <c:if test="${estVatFlag=='Y'}">
					try{
			        var estVatP='vatPers'+aid;
			        var revenueP='estimateRevenueAmount'+aid;
			        var estVatA='vatAmt'+aid;
			   
			        calculateVatAmt(estVatP,revenueP,estVatA);
					}catch(e){}
			        </c:if> 					
//////////changes
                //--------------------Start--estimateRevenueAmount-------------------------//
	               var revenueValue=document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
	  	           var revenueRound=0;
	  	           var oldRevenueSum=0;
	  	           var balanceRevenue=0;
	  	           <c:if test="${not empty serviceOrder.id}">
	  	           	oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	  	           </c:if> 	  	          
	  	           revenueRound=Math.round(revenueValue*100)/100;
	  	           balanceRevenue=revenueRound-oldRevenue;
	  	           oldRevenueSum=Math.round(oldRevenueSum*100)/100;
	  	           balanceRevenue=Math.round(balanceRevenue*100)/100; 
	  	           oldRevenueSum=oldRevenueSum+balanceRevenue; 
	  	           oldRevenueSum=Math.round(oldRevenueSum*100)/100;
	  	           var revenueRoundNew=""+revenueRound;
	  	           if(revenueRoundNew.indexOf(".") == -1){
	  	           revenueRoundNew=revenueRoundNew+".00";
	  	           } 
	  	           if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
	  	           revenueRoundNew=revenueRoundNew+"0";
	  	           }
	  	           document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=revenueRoundNew;
	  	           <c:if test="${not empty serviceOrder.id}">
	  	  	           var newRevenueSum=""+oldRevenueSum;
	  	  	           if(newRevenueSum.indexOf(".") == -1){
	  	  	           	newRevenueSum=newRevenueSum+".00";
	  	  	           } 
	  	  	           if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
	  	  	           	newRevenueSum=newRevenueSum+"0";
	  	  	           } 
	  	  	           document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
	  	           </c:if>
	  	           if(revenueRoundNew>0){
	  	           	document.forms['serviceOrderForm'].elements[displayOnQuote].checked = true;
	  	           }
	  	       //--------------------End--estimateRevenueAmount-------------------------//
	  	       
               //-------------------Start---estimateExpense-------------------------//                
	              var expenseValue=document.forms['serviceOrderForm'].elements[estimateExpense].value;
	              var expenseRound=0;
	              var oldExpenseSum=0;
	              var balanceExpense=0;
	              <c:if test="${not empty serviceOrder.id}">
	              	oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
	              </c:if>
	              expenseRound=Math.round(expenseValue*100)/100;
	              balanceExpense=expenseRound-oldExpense;
	              oldExpenseSum=Math.round(oldExpenseSum*100)/100;
	              balanceExpense=Math.round(balanceExpense*100)/100; 
	              oldExpenseSum=oldExpenseSum+balanceExpense;
	              oldExpenseSum=Math.round(oldExpenseSum*100)/100;
	              var expenseRoundNew=""+expenseRound;
	              if(expenseRoundNew.indexOf(".") == -1){
	              	expenseRoundNew=expenseRoundNew+".00";
	              } 
	              if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
	              	expenseRoundNew=expenseRoundNew+"0";
	              }
	              document.forms['serviceOrderForm'].elements[estimateExpense].value=expenseRoundNew;
	              <c:if test="${not empty serviceOrder.id}">
		              var newExpenseSum=""+oldExpenseSum;
		              if(newExpenseSum.indexOf(".") == -1){
		              	newExpenseSum=newExpenseSum+".00";
		              } 
		              if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
		              	newExpenseSum=newExpenseSum+"0";
		              } 
		              document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
	              </c:if>
	              if(expenseRoundNew>0){
	              	document.forms['serviceOrderForm'].elements[displayOnQuote].checked = true;
	              }
	              //-------------------End---estimateExpense-------------------------//
	              			            
	              calculateGrossMargin();
		            </c:if>
		            <c:if test="${contractType}">
		            
			 	       var res = results.split("#"); 
		               document.forms['serviceOrderForm'].elements['estimateContractCurrencyNew'+aid].value=res[18];
		               var rec='1';
			   			<c:forEach var="entry" items="${currencyExchangeRate}">
			   				if('${entry.key}'==res[18]){
								rec='${entry.value}';
			   				}
						</c:forEach>
						document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value=rec;
		  	              var mydate=new Date();
		  		          var daym;
		  		          var year=mydate.getFullYear()
		  		          var y=""+year;
		  		          if (year < 1000)
		  		          year+=1900
		  		          var day=mydate.getDay()
		  		          var month=mydate.getMonth()+1
		  		          if(month == 1)month="Jan";
		  		          if(month == 2)month="Feb";
		  		          if(month == 3)month="Mar";
		  				  if(month == 4)month="Apr";
		  				  if(month == 5)month="May";
		  				  if(month == 6)month="Jun";
		  				  if(month == 7)month="Jul";
		  				  if(month == 8)month="Aug";
		  				  if(month == 9)month="Sep";
		  				  if(month == 10)month="Oct";
		  				  if(month == 11)month="Nov";
		  				  if(month == 12)month="Dec";
		  				  var daym=mydate.getDate()
		  				  if (daym<10)
		  				  daym="0"+daym
		  				  var datam = daym+"-"+month+"-"+y.substring(2,4);
	  					  document.forms['serviceOrderForm'].elements['estimateContractValueDateNew'+aid].value=datam;
		               document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value=res[19];
		               var rec1='1';
			   			<c:forEach var="entry" items="${currencyExchangeRate}">
		   				if('${entry.key}'==res[19]){
							rec1='${entry.value}';
		   				}
					    </c:forEach>
					   document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value=rec1
		  	              mydate=new Date();
		  		          year=mydate.getFullYear()
		  		          y=""+year;
		  		          if (year < 1000)
		  		          year+=1900
		  		          var day=mydate.getDay()
		  		          var month=mydate.getMonth()+1
		  		          if(month == 1)month="Jan";
		  		          if(month == 2)month="Feb";
		  		          if(month == 3)month="Mar";
		  				  if(month == 4)month="Apr";
		  				  if(month == 5)month="May";
		  				  if(month == 6)month="Jun";
		  				  if(month == 7)month="Jul";
		  				  if(month == 8)month="Aug";
		  				  if(month == 9)month="Sep";
		  				  if(month == 10)month="Oct";
		  				  if(month == 11)month="Nov";
		  				  if(month == 12)month="Dec";
		  				  daym=mydate.getDate()
		  				  if (daym<10)
		  				  daym="0"+daym
		  				  datam = daym+"-"+month+"-"+y.substring(2,4);
		  				  document.forms['serviceOrderForm'].elements['estimatePayableContractValueDateNew'+aid].value=datam;
	//            	   findExchangeContractRateByChargesNew('payable',aid);
		               var multquantity=res[14]
		              if(res[3]=="AskUser1"){ 
		               var reply = prompt("Please Enter Quantity", "");
		               if(reply) {
		               if((reply>0 || reply<9)|| reply=='.')  {
		            	document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(reply*100)/100;
		            	} else {
		            		alert("Please Enter legitimate Quantity");
		            	} }
		            	else{
		            		document.forms['serviceOrderForm'].elements[estimateQuantity].value =document.forms['serviceOrderForm'].elements[estimateQuantity].value ;
		            	}
		            }else{             
		            if(res[1] == undefined){
		            document.forms['serviceOrderForm'].elements[estimateQuantity].value ="";
		            }else{
		            document.forms['serviceOrderForm'].elements[estimateQuantity].value = Math.round(res[1]*100)/100;
		            }  }
		            if(res[5]=="AskUser"){ 
		            var reply = prompt("Please Enter the value of Rate", "");
		            if(reply)  {
		            if((reply>0 || reply<9)|| reply=='.') {
		            	document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value = Math.round(reply*100)/100;
		            	} else {
		            		alert("Please Enter legitimate Rate");
		            	} }
		            	else{
		            	document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value = document.forms['serviceOrderForm'].elements[estimateRate].value;
		            	} 
		            }else{
		            if(res[4] == undefined){
		            document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value ="";
		            }else{
		            document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value =Math.round(res[4]*100)/100 ;
		            }  }
		            if(res[5]=="BuildFormula")   {
		            if(res[6] == undefined){
		            document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value ="";
		            }else{
		             document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value =Math.round(res[6]*100)/100 ;
		            }  }
		           var Q1 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
		           var Q2 = document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		            var oldRevenue = eval(document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value);
		            var oldExpense=eval(document.forms['serviceOrderForm'].elements[estimateExpense].value);		           
		           var E1=Q1*Q2;
		           var E2="";
		           E2=Math.round(E1*100)/100;
		           document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
		           var type = res[8];
		             var typeConversion=res[10];   
		             if(type == 'Division')   {
		             	 if(typeConversion==0) 	 {
		             	   E1=0*1;
		             	   document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E1;
		             	 } else {	
		             		E1=(Q1*Q2)/typeConversion;
		             		E2=Math.round(E1*100)/100;
		             		document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
		             	 } }
		             if(type == ' ') {
		             		E1=(Q1*Q2);
		             		E2=Math.round(E1*100)/100;
		             		document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
		             }
		             if(type == 'Multiply')  {
		             		E1=(Q1*Q2*typeConversion);
		             		E2=Math.round(E1*100)/100;
		             		document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
		             } 
		             document.forms['serviceOrderForm'].elements[basis].value=res[9];
		             var chargedeviation=res[15];
		             document.forms['serviceOrderForm'].elements[deviation].value=chargedeviation;
		             if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){ 
		             document.forms['serviceOrderForm'].elements['buyDependSellNew'+aid].value=res[11];
		             var buyDependSellAccount=res[11];
		             var estimateRevenueAmount1=0;
		             var finalEstimateRevenueAmount=0; 
		             var finalEstimateExpenceAmount=0;
		             var finalEstimateContractRateAmmount=0;
		             var estimateContractRateAmmount1=0;
		             var estimatePayableContractRateAmmount1=0;
		             var estimateSellRate1=0; 
		             var sellDeviation1=0;
		             var  estimatepasspercentage=0
		             var estimatepasspercentageRound=0;
		             var buyDeviation1=0;
		             sellDeviation1=res[12]
		             buyDeviation1=res[13] 
		             var estimateContractExchangeRate1=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value
		             estimateContractRateAmmount1 =document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value
		             estimateRevenueAmount1=estimateContractRateAmmount1/estimateContractExchangeRate1;
		             estimateRevenueAmount1=Math.round(estimateRevenueAmount1*100)/100;
		             estimateRevenueAmount1=document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value;  
		             finalEstimateRevenueAmount=Math.round((estimateRevenueAmount1*(sellDeviation1/100))*100)/100;
		             finalEstimateContractRateAmmount=Math.round((estimateContractRateAmmount1*(sellDeviation1/100))*100)/100;
		             document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value=finalEstimateContractRateAmmount; 
		             if(buyDependSellAccount=="Y"){
		              finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation1/100))*100)/100;
		             }else{
		              finalEstimateExpenceAmount= Math.round((estimateRevenueAmount1*(buyDeviation1/100))*100)/100;
		             }
		             document.forms['serviceOrderForm'].elements[estimateSellDeviation].value=sellDeviation1;
		             document.forms['serviceOrderForm'].elements['oldEstimateSellDeviationNew'+aid].value=sellDeviation1;
		             document.forms['serviceOrderForm'].elements[estimateDeviation].value=buyDeviation1;
		             document.forms['serviceOrderForm'].elements['oldEstimateDeviationNew'+aid].value=buyDeviation1;
			    	 var estimatePayableContractExchangeRate=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value
			 	    estimatePayableContractRateAmmount1=finalEstimateExpenceAmount*estimatePayableContractExchangeRate;
		             estimatePayableContractRateAmmount1=Math.round(estimatePayableContractRateAmmount1*100)/100;
		             document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value=estimatePayableContractRateAmmount1;
		             calEstimatePayableContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		             }else {
		             document.forms['serviceOrderForm'].elements[estimateSellDeviation].value=0;
		             document.forms['serviceOrderForm'].elements['oldEstimateSellDeviationNew'+aid].value=0;
		             document.forms['serviceOrderForm'].elements[estimateDeviation].value=0;
		            document.forms['serviceOrderForm'].elements['oldEstimateDeviationNew'+aid].value=0;
		             var bayRate=0
		             bayRate = res[17] ;
		             if(bayRate!=0){
		             var estimateQuantityPay=0;
		             estimateQuantityPay= document.forms['serviceOrderForm'].elements[estimateQuantity].value;
		             Q1=estimateQuantityPay;
		             Q2=bayRate; 
		             if(type == 'Division')   {
		             	 if(typeConversion==0) 	 {
		             	   E1=0*1;
		             	   document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E1;
		             	 } else {	
		             		E1=(Q1*Q2)/typeConversion;
		             		E2=Math.round(E1*100)/100;
		             		document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E2;
		             	 } }
		             if(type == ' ') {
		             		E1=(Q1*Q2);
		             		E2=Math.round(E1*100)/100;
		             		document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E2;
		             }
		             if(type == 'Multiply')  {
		             		E1=(Q1*Q2*typeConversion);
		             		E2=Math.round(E1*100)/100;
		             		document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E2;
		             }
		               document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value = Q2;
		             } }
		             calculateEstimateSellRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
		             calculateEstimateRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		                //--------------------Start--estimateRevenueAmount-------------------------//
		               var revenueValue=document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
		  	           var revenueRound=0;
		  	           var oldRevenueSum=0;
		  	           var balanceRevenue=0;
		  	           <c:if test="${not empty serviceOrder.id}">
		  	           	oldRevenueSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
		  	           </c:if> 	  	          
		  	           revenueRound=Math.round(revenueValue*100)/100;
		  	           balanceRevenue=revenueRound-oldRevenue;
		  	           oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		  	           balanceRevenue=Math.round(balanceRevenue*100)/100; 
		  	           oldRevenueSum=oldRevenueSum+balanceRevenue; 
		  	           oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		  	           var revenueRoundNew=""+revenueRound;
		  	           if(revenueRoundNew.indexOf(".") == -1){
		  	           revenueRoundNew=revenueRoundNew+".00";
		  	           } 
		  	           if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
		  	           revenueRoundNew=revenueRoundNew+"0";
		  	           }
		  	           document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=revenueRoundNew;

		  	         var type01 = res[8];
		             var typeConversion01=res[10];  
		             //#7298 - Copy esimates not calculating preset calculations in charges start
					  var tempData= document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value;
					               if(type01 == 'Division')   {
						               try{
						            	   tempData=tempData/typeConversion01; 
						               }catch(e){
						            	   tempData=0;
							               }
					               }else if(type01 == 'Multiply')   {
					            	   tempData=tempData*typeConversion01; 
					               }else{
					            	   tempData=tempData;
					               }
		      				            tempData=Math.round(tempData*100)/100;
					  document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=tempData;
			                 //#7298 - Copy esimates not calculating preset calculations in charges End	 					
			          //#7298 - Copy esimates not calculating preset calculations in charges start
					  var tempData= document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value;
					               if(type01 == 'Division')   {
						               try{
						            	   tempData=tempData/typeConversion01; 
						               }catch(e){
						            	   tempData=0;
							               }
					               }else if(type01 == 'Multiply')   {
					            	   tempData=tempData*typeConversion01; 
					               }else{
					            	   tempData=tempData;
					               }
		      				            tempData=Math.round(tempData*100)/100;
					  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=tempData;
			                 //#7298 - Copy esimates not calculating preset calculations in charges End    

		 					//#7298 - Copy esimates not calculating preset calculations in charges start
					  var tempData= document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value;
					               if(type01 == 'Division')   {
						               try{
						            	   tempData=tempData/typeConversion01; 
						               }catch(e){
						            	   tempData=0;
							               }
					               }else if(type01 == 'Multiply')   {
					            	   tempData=tempData*typeConversion01; 
					               }else{
					            	   tempData=tempData;
					               }
		      				            tempData=Math.round(tempData*100)/100;
					  document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value=tempData;
			                 //#7298 - Copy esimates not calculating preset calculations in charges End	 					
			          //#7298 - Copy esimates not calculating preset calculations in charges start
					  var tempData= document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value;
					               if(type01 == 'Division')   {
						               try{
						            	   tempData=tempData/typeConversion01; 
						               }catch(e){
						            	   tempData=0;
							               }
					               }else if(type01 == 'Multiply')   {
					            	   tempData=tempData*typeConversion01; 
					               }else{
					            	   tempData=tempData;
					               }
		      				            tempData=Math.round(tempData*100)/100;
					  document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value=tempData;
			                 //#7298 - Copy esimates not calculating preset calculations in charges End   
		  //#7298 - Copy esimates not calculating preset calculations in charges start
					  var tempData= document.forms['serviceOrderForm'].elements[estimateExpense].value;
					               if(type01 == 'Division')   {
						               try{
						            	   tempData=tempData/typeConversion01; 
						               }catch(e){
						            	   tempData=0;
							               }
					               }else if(type01 == 'Multiply')   {
					            	   tempData=tempData*typeConversion01; 
					               }else{
					            	   tempData=tempData;
					               }
		      				            tempData=Math.round(tempData*100)/100;
					  document.forms['serviceOrderForm'].elements[estimateExpense].value=tempData;
			                 //#7298 - Copy esimates not calculating preset calculations in charges End 
		 	//#7298 - Copy esimates not calculating preset calculations in charges start
					  var tempData= document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value;
					               if(type01 == 'Division')   {
						               try{
						            	   tempData=tempData/typeConversion01; 
						               }catch(e){
						            	   tempData=0;
							               }
					               }else if(type01 == 'Multiply')   {
					            	   tempData=tempData*typeConversion01; 
					               }else{
					            	   tempData=tempData;
					               }
		      				            tempData=Math.round(tempData*100)/100;
					  document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=tempData;
			                 //#7298 - Copy esimates not calculating preset calculations in charges End 
			        <c:if test="${estVatFlag=='Y'}">
			        try{
			        var estVatP='vatPers'+aid;
			        var revenueP='estimateRevenueAmount'+aid;
			        var estVatA='vatAmt'+aid;
			   
			        calculateVatAmt(estVatP,revenueP,estVatA);
			        }catch(e){}
			        </c:if>   		  	           
		  	           <c:if test="${not empty serviceOrder.id}">
		  	  	           var newRevenueSum=""+oldRevenueSum;
		  	  	           if(newRevenueSum.indexOf(".") == -1){
		  	  	           	newRevenueSum=newRevenueSum+".00";
		  	  	           } 
		  	  	           if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
		  	  	           	newRevenueSum=newRevenueSum+"0";
		  	  	           } 
		  	  	           document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
		  	           </c:if>
		  	           if(revenueRoundNew>0){
		  	           	document.forms['serviceOrderForm'].elements[displayOnQuote].checked = true;
		  	           }
		  	       //--------------------End--estimateRevenueAmount-------------------------//
		  	       
	               //-------------------Start---estimateExpense-------------------------//                
		              var expenseValue=document.forms['serviceOrderForm'].elements[estimateExpense].value;
		              var expenseRound=0;
		              var oldExpenseSum=0;
		              var balanceExpense=0;
		              <c:if test="${not empty serviceOrder.id}">
		              	oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
		              </c:if>
		              expenseRound=Math.round(expenseValue*100)/100;
		              balanceExpense=expenseRound-oldExpense;
		              oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		              balanceExpense=Math.round(balanceExpense*100)/100; 
		              oldExpenseSum=oldExpenseSum+balanceExpense;
		              oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		              var expenseRoundNew=""+expenseRound;
		              if(expenseRoundNew.indexOf(".") == -1){
		              	expenseRoundNew=expenseRoundNew+".00";
		              } 
		              if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
		              	expenseRoundNew=expenseRoundNew+"0";
		              }
		              document.forms['serviceOrderForm'].elements[estimateExpense].value=expenseRoundNew;
		              
		              <c:if test="${not empty serviceOrder.id}">
			              var newExpenseSum=""+oldExpenseSum;
			              if(newExpenseSum.indexOf(".") == -1){
			              	newExpenseSum=newExpenseSum+".00";
			              } 
			              if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
			              	newExpenseSum=newExpenseSum+"0";
			              } 
			              document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
		              </c:if>
		              if(expenseRoundNew>0){
		              	document.forms['serviceOrderForm'].elements[displayOnQuote].checked = true;
		              }
		              //-------------------End---estimateExpense-------------------------//
		              //calculateGrossMargin();
		             <c:if test="${not empty serviceOrder.id}">
		              var revenueValue=0;
		              var expenseValue=0;
		              var grossMarginValue=0; 
		              var grossMarginPercentageValue=0; 
		              revenueValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
		              expenseValue=eval(document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
		              grossMarginValue=revenueValue-expenseValue;
		              grossMarginValue=Math.round(grossMarginValue*100)/100; 
		              document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
		              if(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
		            	    document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
		            	   }else{
		            	     grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
		            	     grossMarginPercentageValue=Math.round(grossMarginPercentageValue*100)/100; 
		            	   	document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
		            	   }
		            </c:if> 
			            </c:if>
  	        }  
  	    }
  	
  	--%>
  		function getVanPayCharges(temp){
  			 var sid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
  			 var url="getVanPayCharges.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
  			      http88.open("GET", url, true);
  			      http88.onreadystatechange =function(){ handleHttpResponseVanPayCharges(temp);}; ;
  			      http88.send(null);
  			}
			
	    function handleHttpResponseVanPayCharges(temp,estimateExpense,estimateRate,basis,estimateQuantity){
	           if (http88.readyState == 4)   {
	                var results = http88.responseText
	                results = results.trim(); 
	                results = results.replace('[','');
	                results=results.replace(']',''); 
	                if(results.length > 0) {
	                }else{
	                results=0;
	                }
	                if(temp=='Estimate'){
	                	document.forms['serviceOrderForm'].elements[estimateExpense].value = results; 
	                	document.forms['serviceOrderForm'].elements[estimateRate].value = results; 
	                	document.forms['serviceOrderForm'].elements[basis].value = 'each'; 
	                	document.forms['serviceOrderForm'].elements[estimateQuantity].value = 1; 
	                }
	              } 
               }

		function getDomCommCharges(temp,estimateExpense,estimateRate,basis,estimateQuantity){
			var comptetive = document.forms['serviceOrderForm'].elements['inComptetive'].value;
			if(comptetive=="Y"){
			 var sid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
			 var job=document.forms['serviceOrderForm'].elements['serviceOrder.Job'].value;
			 var url="getDomCommCharges.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&job="+encodeURI(job);
			      http88.open("GET", url, true);
			      http88.onreadystatechange =function(){ handleHttpResponseVanPayCharges(temp,estimateExpense,estimateRate,basis,estimateQuantity);}; ;
			      http88.send(null);
		      } 
		   }
		function handleHttpResponseDomCommCharges(temp,estimateExpense,estimateRate,basis,estimateQuantity){
	           if (http88.readyState == 4)   {
	                var results = http88.responseText
	                results = results.trim(); 
	                results = results.replace('[','');
	                results=results.replace(']',''); 
	                if(results.length > 0)  {
	                }else{
	                results=0;
	                }
	                if(temp=='Estimate'){
	                	document.forms['serviceOrderForm'].elements[estimateExpense].value = results; 
	                	document.forms['serviceOrderForm'].elements[estimateRate].value = results; 
	                	document.forms['serviceOrderForm'].elements[basis].value = 'each'; 
	                	document.forms['serviceOrderForm'].elements[estimateQuantity].value = 1; 
	                }
	             }  
               }
	    			    
		//differnt method added for pricing  8-31-2012  Start

<%--		
				  	 function calEstimatePayableContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) 
		{
		  		  var estimateQuantity1 =0;
		  		    var estimateDeviation1=100;
		  		    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
		  		    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
		  		    var estimateExpense1 =eval(document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value); 
		  	        var estimateRate1=0;
		  		    var estimateRateRound=0; 
		  		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
		  	       estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
		  	       } 
		  	      if( basis1=="cwt" || basis1=="%age"){
		  	     	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
		  	     	  estimateRateRound=Math.round(estimateRate1*100)/100;
		  	     	  document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=estimateRateRound;	  	
		  	     	}  else if(basis1=="per 1000"){
		  	     	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
		  	     	  estimateRateRound=Math.round(estimateRate1*100)/100;
		  	     	  document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=estimateRateRound;		  	
		  	     	} else  {
		  	        estimateRate1=(estimateExpense1/estimateQuantity1); 
		  	     	  estimateRateRound=Math.round(estimateRate1*100)/100; 
		  	     	  document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=estimateRateRound;		  	
		  		    }
		  		    estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value 
		  		    if(estimateDeviation1!='' && estimateDeviation1>0){
		  		    estimateRate1=estimateRateRound*100/estimateDeviation1;
		  		    estimateRateRound=Math.round(estimateRate1*100)/100;
		  		  document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=estimateRateRound;		
		  		    } 
		  	 } 	  

		  	function  calculateEstimateSellRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
		  	{
		  		
		 	   <c:if test="${contractType}">
		 	        var country1 =document.forms['serviceOrderForm'].elements['estimateContractCurrencyNew'+aid].value; 
		 	        if(country1=='') {
		 	          alert("Please select Contract Currency ");
		 	          document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value=0;
		 	          document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value=1;
		 	        } else if(country1!=''){
		 	        var estimateContractExchangeRate1=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value
		 	        var estimateContractRate1=document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value
		 	        estimateContractRate1=Math.round(estimateContractRate1*100)/100;
		 	        var estimateSellRate1=estimateContractRate1/estimateContractExchangeRate1;
		 	        var roundValue=Math.round(estimateSellRate1*100)/100;
		 	        document.forms['serviceOrderForm'].elements[estimateSellRate].value=roundValue ; 
		 	        calEstSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		 	        calEstimateContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		 	        }   
		 	   </c:if>
		 	   }
		  	

		  	function calEstSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
		  	{
		  		<c:if test="${multiCurrency=='Y'}">
		  		          var country1 =document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value; 
		  		          if(country1=='') {
		  		          alert("Please select billing currency "); 
		  		          document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=0; 
		  		        } else if(country1!=''){  
		  		        var recRateExchange1=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value
		  		        var recRate1=document.forms['serviceOrderForm'].elements[estimateSellRate].value
		  		        var recCurrencyRate1=recRate1*recRateExchange1;
		  		        var roundValue=Math.round(recCurrencyRate1*100)/100;
		  		        document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=roundValue ; 
		  		        calculateEstimateSellLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		  		        } 
		  	     </c:if>		        
		  		  }
		  	

		  	function calEstimateContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) 
		  	{

		 	   <c:if test="${contractType}">
		 	   var estimate=0;
		 	   var estimaternd=0;
		 	   var receivableSellDeviation=0;
		 	       var quantity = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
		 	       var rate = document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		 	   	   if(quantity<999999999999&& rate<999999999999) {
		 	  	 	estimate = (quantity*rate);
		 	  	 	estimaternd=Math.round(estimate*100)/100;
		 	  	 	} else { 
		 	  	 	 estimate=0;
		 	  	 	 estimaternd=Math.round(estimate*100)/100;
		 	  	 	} 
		 	         if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age") {
		 	      	    estimate = estimate/100; 
		 	      	    estimaternd=Math.round(estimate*100)/100;	 
		 	      	    document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = estimaternd; 
		 	      	      }
		 	          else if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000")  {
		 	      	    estimate = estimate/1000; 
		 	      	    estimaternd=Math.round(estimate*100)/100;	 
		 	      	    document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = estimaternd; 
		 	      	    } else{
		 	            document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = estimaternd;
		 	              } 
		 	           estimateDeviation=document.forms['serviceOrderForm'].elements[estimateSellDeviation].value;
		 	             if(estimateDeviation!='' && estimateDeviation>0){
		 	               estimaternd=estimaternd*estimateDeviation/100;
		 	               estimaternd=Math.round(estimaternd*100)/100;
		 	               document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value = estimaternd;	 
		 	           }
		 	             calculateEstimateRevenueAmountByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)  
		 	   </c:if>
		 	  		}


		    function calculateEstimateSellLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
		    {
		    	   var estimateQuantity1 =0;
		    	   var estLocalRate1 =0;
		    	   	    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
		    	   	    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
		    	   	    estSellLocalRate1 =document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;  
		    	   	    var estimateSellLocalAmount1=0;
		    	   	    var estimateSellLocalAmountRound=0; 
		    	   	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
		    	            estimateQuantity =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
		    	            } 
		    	           if( basis1=="cwt" || basis1=="%age"){
		    	        	   estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/100;
		    	        	   estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*100)/100;
		    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;	  	
		    	          	}  else if(basis1=="per 1000"){
		    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/1000;
		    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*100)/100;
		    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
		    	          	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
		    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1); 
		    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*100)/100; 
		    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
		    	   	    }
		    	           estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateSellDeviation].value;
		  	             if(estimateDeviation1!='' && estimateDeviation1>0){
		  	            	estimateSellLocalAmountRound=estimateSellLocalAmountRound*estimateDeviation1/100;
		  	            	estimateSellLocalAmountRound=Math.round(estimateSellLocalAmountRound*100)/100;
		  	               document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value = estimateSellLocalAmountRound;	 
		  	           }  
		  	    	   var contractRate=0;
		 			  <c:if test="${contractType}">
		         contractRate = document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		         </c:if>
		       if(contractRate==0){
		    	           calculateEstimateRevenueNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		       }
		    	   }

		  	function calculateEstimateRevenueAmountByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) 
		  	{
			  	 
		 	   var estSellLocalAmount1= document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value ;
		 	   var estSellExchangeRate1= document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value*1 ;
		 	   var roundValue=0; 
		 	  	 if(estSellExchangeRate1 ==0) {
		 	         document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=0*1;
		 	     } else if(estSellExchangeRate1 >0) {
		 	       var amount1=estSellLocalAmount1/estSellExchangeRate1; 
		 	       roundValue=Math.round(amount1*100)/100;
		 	       document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=roundValue ; 
		 	     }
		 	     var estimatepasspercentageRound=0;
		 	     var estimatepasspercentage1 = 0;
		 	     var estimateExpense1 = document.forms['serviceOrderForm'].elements[estimateExpense].value; 
		 	     estimatepasspercentage1 = (roundValue/estimateExpense1)*100;
		 	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
		 	  	 if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
		 	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
		 	  	   }else{
		 	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
		 	  	   }  
		 } 	  		

		       function  calculateEstimateRevenueNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
		       {
		    	   var contractRate=0;
					  <c:if test="${contractType}">
		        contractRate = document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		        </c:if>
		      if(contractRate==0){
		    	   var estSellLocalAmount1= document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value ;
		    	   var estSellExchangeRate1= document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value*1 ;
		    	   var roundValue=0; 
		    	  	 if(estSellExchangeRate1 ==0) {
		    	         document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=0*1;
		    	     } else if(estSellExchangeRate1 >0) {
		    	       var amount1=estSellLocalAmount1/estSellExchangeRate1; 
		    	       roundValue=Math.round(amount1*100)/100;
		    	       document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value=roundValue ; 
		    	     }
		    	     var estimatepasspercentageRound=0;
		    	     var estimatepasspercentage1 = 0;
		    	     var estimateExpense1 = document.forms['serviceOrderForm'].elements[estimateExpense].value; 
		    	     estimatepasspercentage1 = (roundValue/estimateExpense1)*100;
		    	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
		    	  	 if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
		    	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
		    	  	   }else{
		    	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
		    	  	   } 
		    	  	 calculateSellEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
		    	  	calculateEstimateSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		            }else{
		       	        calEstSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
		             }
		  	  	
		           } 
		       
		       
		       function calculateSellEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
		       {
		      	    var estimateQuantity1 =0;
		      	    var estimateDeviation1=100;
		      	    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
		      	    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
		      	    var estimateRevenueAmount1 =eval(document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value); 
		      	    var estimateSellRate1=0;
		      	    var estimateSellRateRound=0; 
		      	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
		               estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
		               } 
		              if( basis1=="cwt" || basis1=="%age"){
		           	   estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*100;
		           	   estimateSellRateRound=Math.round(estimateSellRate1*100)/100;
		             	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;	  	
		             	}  else if(basis1=="per 1000"){
		             		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*1000;
		             		estimateSellRateRound=Math.round(estimateSellRate1*100)/100;
		             	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		  	
		             	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
		             		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1); 
		             		estimateSellRateRound=Math.round(estimateSellRate1*100)/100; 
		             	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		  	
		      	    }
		              estimateSellDeviation1=document.forms['serviceOrderForm'].elements[estimateSellDeviation].value 
		      	    if(estimateSellDeviation1!='' && estimateSellDeviation1>0){
		      	    	estimateSellRateRound=estimateSellRateRound*100/estimateSellDeviation1;
		      	    	estimateSellRateRound=Math.round(estimateSellRateRound*100)/100;
		      	    document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		
		      	    }  }	
			    
		       function calculateEstimateSellLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
		       {
		    	    var estimateQuantity1 =0;
		    	    var estimateRevenueAmount1 =0;
		    		    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
		    		    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
		    		    estimateRevenueAmount1 =document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value; 
		    		    var estimateSellRate1=0;
		    		    var estimateSellRateRound=0; 
		    		    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
		    	         estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
		    	         } 
		    	        if( basis1=="cwt" || basis1=="%age"){
		    	        	estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*100;
		    	        	estimateSellRateRound=Math.round(estimateSellRate1*100)/100;
		    	       	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;	  	
		    	       	}  else if(basis1=="per 1000"){
		    	       		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1)*1000;
		    	       		estimateSellRateRound=Math.round(estimateSellRate1*100)/100;
		    	       	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		  	
		    	       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
		    	       		estimateSellRate1=(estimateRevenueAmount1/estimateQuantity1); 
		    	       		estimateSellRateRound=Math.round(estimateSellRate1*100)/100; 
		    	       	  document.forms['serviceOrderForm'].elements[estimateSellRate].value=estimateSellRateRound;		  	
		    		    }
		    	        estimateSellDeviation1=document.forms['serviceOrderForm'].elements[estimateSellDeviation].value 
		    	   	    if(estimateSellDeviation1!='' && estimateSellDeviation1>0){
		    	   	    	estimateSellRate1=estimateSellRateRound*100/estimateSellDeviation1;
		    	   	    	estimateSellRateRound=Math.round(estimateSellRate1*100)/100;
		    	   	    document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=estimateSellRateRound;		
		    	   	    }  
		    	 }	

		  	 function  calculateEstimateRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) {
		  	   <c:if test="${contractType}">
		  	        var country1 =document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value; 
		  	        if(country1=='') {
		  	          alert("Please select Contract Currency ");
		  	          document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value=0;
		  	          document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value=1;
		  	        } else if(country1!=''){
		  	        var estimatePayableContractExchangeRate1=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value
		  	        var estimatePayableContractRate1=document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value
		  	        estimatePayableContractRate1=Math.round(estimatePayableContractRate1*100)/100;
		  	        var estimateRate1=estimatePayableContractRate1/estimatePayableContractExchangeRate1;
		  	        var roundValue=Math.round(estimateRate1*100)/100;
		  	        document.forms['serviceOrderForm'].elements[estimateRate].value=roundValue ;  
		  	        calEstLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		  	        //calRecCurrencyRate();
		  	        calEstimatePayableContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		  	        }   
		  	   </c:if>
		  	   }

		  	 function calEstLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
		  	 {
		  	   
		          var country1 =document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value; 
		          if(country1=='') {
		          alert("Please select currency "); 
		          document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=0; 
		        } else if(country1!=''){  
		        var recRateExchange1=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value
		        var recRate1=document.forms['serviceOrderForm'].elements[estimateRate].value
		        var recCurrencyRate1=recRate1*recRateExchange1;
		        var roundValue=Math.round(recCurrencyRate1*100)/100;
		        document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=roundValue ; 
		         calculateEstimateLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		        } 
		  }	

		  	 function calEstimatePayableContractRateAmmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid) 
		  	 {
		  	   <c:if test="${contractType}">
		  	   var estimate1=0;
		  	   var estimaternd=0;
		  	   var receivableSellDeviation1=0;
		  	       var quantity1 = document.forms['serviceOrderForm'].elements[estimateQuantity].value;
		  	       var rate1 = document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
		  	   	   if(quantity1<999999999999&& rate1<999999999999) {
		  	  	 	estimate1 = (quantity1*rate1);
		  	  	 	estimaternd=Math.round(estimate1*100)/100;
		  	  	 	} else { 
		  	  	 	 estimate1=0;
		  	  	 	 estimaternd=Math.round(estimate1*100)/100;
		  	  	 	} 
		  	         if( document.forms['serviceOrderForm'].elements[basis].value=="cwt" || document.forms['serviceOrderForm'].elements[basis].value=="%age") {
		  	      	    estimate1 = estimate1/100; 
		  	      	    estimaternd=Math.round(estimate1*100)/100;	 
		  	      	    document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = estimaternd; 
		  	      	      }
		  	          else if( document.forms['serviceOrderForm'].elements[basis].value=="per 1000")  {
		  	      	    estimate1 = estimate1/1000; 
		  	      	    estimaternd=Math.round(estimate1*100)/100;	 
		  	      	    document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = estimaternd; 
		  	      	    } else{
		  	            document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = estimaternd;
		  	              } 
		  	         estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value;
		  	             if(estimateDeviation1!='' && estimateDeviation1>0){
		  	               estimaternd=estimaternd*estimateDeviation1/100;
		  	               estimaternd=Math.round(estimaternd*100)/100;
		  	               document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = estimaternd;	 
		  	           }
		  	             calculateEstimateExpenseByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);   
		  	   </c:if>
		  	  		}	
		  	
		  	function calculateEstimateLocalAmountNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
		  	{
		  		var estimateQuantity1 =0;
		  		var estLocalRate1 =0;
		  			    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
		  			    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
		  			    estLocalRate1 =document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value;  
		  			    var estimateLocalAmount1=0;
		  			    var estimateLocalAmountRound=0; 
		  			    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
		  		         estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
		  		         } 
		  		        if( basis1=="cwt" || basis1=="%age"){
		  		       	  estimateLocalAmount1=(estLocalRate1*estimateQuantity1)/100;
		  		       	  estimateLocalAmountRound=Math.round(estimateLocalAmount1*100)/100;
		  		       	  document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estimateLocalAmountRound;	  	
		  		       	}  else if(basis1=="per 1000"){
		  		       	  estimateLocalAmount1=(estLocalRate1*estimateQuantity1)/1000;
		  		       	  estimateLocalAmountRound=Math.round(estimateLocalAmount1*100)/100;
		  		       	  document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estimateLocalAmountRound;		  	
		  		       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
		  		          estimateLocalAmount1=(estLocalRate1*estimateQuantity1); 
		  		       	  estimateLocalAmountRound=Math.round(estimateLocalAmount1*100)/100; 
		  		       	  document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estimateLocalAmountRound;		  	
		  			    }
		  		        estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value;
		  		        if(estimateDeviation1!='' && estimateDeviation1>0){
		  		        	estimateLocalAmountRound=estimateLocalAmountRound*estimateDeviation1/100;
		  		        	estimateLocalAmountRound=Math.round(estimateLocalAmountRound*100)/100;
		  		          document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value = estimateLocalAmountRound;	 
		  		      }
		  		        var contractRate=0;
		  		  	  <c:if test="${contractType}">
		  		        contractRate = document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
		  		        </c:if>  
		  		        if(contractRate==0){   
		  			    calculateEstimateExpenseNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		  		        }
		  		}	

  function calculateEstimateExpenseNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
  {
		  	  var contractRate=0;
		  	  <c:if test="${contractType}">
		        contractRate = document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
		        </c:if> 
		        if(contractRate==0){  
		     var estLocalAmount1= document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value ;
		     var estExchangeRate1= document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value*1 ;
		     var roundValue=0; 
		    	 if(estExchangeRate1 ==0) {
		           document.forms['serviceOrderForm'].elements[estimateExpense].value=0*1;
		       } else if(estExchangeRate1 >0) {
		         var amount1=estLocalAmount1/estExchangeRate1; 
		         roundValue=Math.round(amount1*100)/100;
		         document.forms['serviceOrderForm'].elements[estimateExpense].value=roundValue ; 
		       }
		       var estimatepasspercentageRound=0;
		       var estimatepasspercentage1 = 0;
		       var estimateRevenue1 = document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value; 
		       estimatepasspercentage1 = (estimateRevenue1/roundValue)*100;
		    	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
		    	 if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
		    	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
		    	   }else{
		    	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
		    	   } 
		       calculateEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		       calculateEstimateLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);
		        }else{
		      	  calEstLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);    
		        }
		    } 

 function calculateEstimateRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
 {
			    var estimateQuantity1 =0;
			    var estimateDeviation1=100;
			    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
			    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
			    var estimateExpense1 =eval(document.forms['serviceOrderForm'].elements[estimateExpense].value); 
			    var estimateRate1=0;
			    var estimateRateRound=0; 
			    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
		         estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
		         } 
		        if( basis1=="cwt" || basis1=="%age"){
		       	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
		       	  estimateRateRound=Math.round(estimateRate1*100)/100;
		       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound;	  	
		       	}  else if(basis1=="per 1000"){
		       	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
		       	  estimateRateRound=Math.round(estimateRate1*100)/100;
		       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound;		  	
		       	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
		          estimateRate1=(estimateExpense1/estimateQuantity1); 
		       	  estimateRateRound=Math.round(estimateRate1*100)/100; 
		       	  document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound;		  	
			    }
			    estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value 
			    if(estimateDeviation1!='' && estimateDeviation1>0){
			    	estimateRateRound=estimateRateRound*100/estimateDeviation1;
			    	estimateRateRound=Math.round(estimateRateRound*100)/100;
			    document.forms['serviceOrderForm'].elements[estimateRate].value=estimateRateRound;		
			    }  }


		    function calculateEstimateLocalRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
		    {
		        var estimateQuantity1 =0;
		        var estimateExpense1 =0;
		    	    estimateQuantity1=document.forms['serviceOrderForm'].elements[estimateQuantity].value; 
		    	    var basis1 =document.forms['serviceOrderForm'].elements[basis].value; 
		    	    estimateExpense1 =document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value; 
		    	    var estimateRate1=0;
		    	    var estimateRateRound=0; 
		    	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
		             estimateQuantity1 =document.forms['serviceOrderForm'].elements[estimateQuantity].value=1;
		             } 
		            if( basis1=="cwt" || basis1=="%age"){
		           	  estimateRate1=(estimateExpense1/estimateQuantity1)*100;
		           	  estimateRateRound=Math.round(estimateRate1*100)/100;
		           	  document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=estimateRateRound;	  	
		           	}  else if(basis1=="per 1000"){
		           	  estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
		           	  estimateRateRound=Math.round(estimateRate1*100)/100;
		           	  document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=estimateRateRound;		  	
		           	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
		              estimateRate1=(estimateExpense1/estimateQuantity1); 
		           	  estimateRateRound=Math.round(estimateRate1*100)/100; 
		           	  document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=estimateRateRound;		  	
		    	    }  
		            estimateDeviation1=document.forms['serviceOrderForm'].elements[estimateDeviation].value 
		    	    if(estimateDeviation1!='' && estimateDeviation1>0){
		    	    	estimateRateRound=estimateRateRound*100/estimateDeviation1;
		    	    	estimateRateRound=Math.round(estimateRateRound*100)/100;
		    	    document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=estimateRateRound;		
		    	    }
		     }


function calculateEstimateExpenseByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid)
{ 
		  	   var estLocalAmount1= document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value ;
		  	   var estExchangeRate1= document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value*1 ;
		  	   var roundValue=0; 
		  	  	 if(estExchangeRate1 ==0) {
		  	         document.forms['serviceOrderForm'].elements[estimateExpense].value=0*1;
		  	     } else if(estExchangeRate1 >0) {
		  	       var amount1=estLocalAmount1/estExchangeRate1; 
		  	       roundValue=Math.round(amount1*100)/100;
		  	       document.forms['serviceOrderForm'].elements[estimateExpense].value=roundValue ; 
		  	     }
		  	     var estimatepasspercentageRound=0;
		  	     var estimatepasspercentage1 = 0;
		  	     var estimateRevenue1 = document.forms['serviceOrderForm'].elements[estimateRevenueAmount].value; 
		  	     estimatepasspercentage1 = (estimateRevenue1/roundValue)*100;
		  	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage1);
		  	  	 if(document.forms['serviceOrderForm'].elements[estimateExpense].value == '' || document.forms['serviceOrderForm'].elements[estimateExpense].value == 0){
		  	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value='';
		  	  	   }else{
		  	  	   	document.forms['serviceOrderForm'].elements[estimatePassPercentage].value=estimatepasspercentageRound;
		  	  	   }  
		  		 }	

--%>
 /*function calculateEstimateSellRate(){

		    var accountlineId=document.forms['serviceOrderForm'].elements['accId'].value
		    var accEstimateQuantity=document.forms['serviceOrderForm'].elements['accEstimateQuantity'].value
		    var accEstimateSellRate=document.forms['serviceOrderForm'].elements['accEstimateSellRate'].value 
		    var accEstimateRevenueAmount=document.forms['serviceOrderForm'].elements['accEstimateRevenueAmount'].value 
		    var expenseValue=0;
		    var expenseRound=0;
		    var oldExpense=0;
		    var oldExpenseSum=0;
		    var balanceExpense=0;
		    oldExpense=eval(document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accountlineId].value);
		    <c:if test="${not empty serviceOrder.id}">
		    oldExpenseSum=eval(document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
		    </c:if> 
		    expenseValue=accEstimateRevenueAmount;
		    expenseRound=Math.round(expenseValue*100)/100;
		    balanceExpense=expenseRound-oldExpense;
		    oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		    balanceExpense=Math.round(balanceExpense*100)/100; 
		    oldExpenseSum=oldExpenseSum+balanceExpense;
		    oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		    var expenseRoundNew=""+expenseRound;
		    if(expenseRoundNew.indexOf(".") == -1){
		    expenseRoundNew=expenseRoundNew+".00";
		    } 
		    if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
		    expenseRoundNew=expenseRoundNew+"0";
		    } 
		    document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accountlineId].value=expenseRoundNew;
		    document.forms['serviceOrderForm'].elements['estimateSellRate'+accountlineId].value=accEstimateSellRate;
		    estimateQuantity=document.forms['serviceOrderForm'].elements['estimateQuantity'+accountlineId].value;
		    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {   
		     document.forms['serviceOrderForm'].elements['estimateQuantity'+accountlineId].value=accEstimateQuantity;
		     }
		    <c:if test="${not empty serviceOrder.id}">
		    var newExpenseSum=""+oldExpenseSum;
		    if(newExpenseSum.indexOf(".") == -1){
		    newExpenseSum=newExpenseSum+".00";
		    } 
		    if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
		    newExpenseSum=newExpenseSum+"0";
		    } 
		    document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newExpenseSum;
		    </c:if>
		    if(expenseRoundNew>0){
		    document.forms['serviceOrderForm'].elements['displayOnQuote'+accountlineId].checked = true;
		    }
		    var revenue='estimateRevenueAmount'+accountlineId;
		    var expense='estimateExpense'+accountlineId;
		    var estimatePassPercentage='estimatePassPercentage'+accountlineId;
		    calculateestimatePassPercentage(revenue,expense,estimatePassPercentage); 
		    	  
	  }*/
/*
function calculateVatAmtEstTemp(vatPer,vatAmt){
	<c:if test="${estVatFlag=='Y'}">
	var estVatAmt=0;
	if(document.forms['serviceOrderForm'].elements[vatPer].value!=''){
	var estVatPercent=eval(document.forms['serviceOrderForm'].elements[vatPer].value);
	var estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['accEstimateRevenueAmount'].value);
	<c:if test="${contractType}">
	  estimateRevenueAmount= eval(document.forms['serviceOrderForm'].elements['accEstSellLocalAmount'].value);
	</c:if>
	estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
	estVatAmt=Math.round(estVatAmt*100)/100; 
	document.forms['serviceOrderForm'].elements[vatAmt].value=estVatAmt;
	}
    </c:if>
}*/

		/*  	 function calculateAccountingACToBillingCurrency(accLineList,currency,extRate,valueDate){
				if(accLineList.trim()!=""){
			  	 var str=accLineList.split(",");
			  	 for(i=0;i<str.length;i++){
					var aid=str[i];
			  		document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value=currency;
			  		document.forms['serviceOrderForm'].elements['estSellValueDateNew'+aid].value=valueDate;
			  		document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value=extRate;
			  		<c:if test="${contractType}">
			  		var extSellRateBelow=0;
			  		var extEstimateContractRateEXAbove=0;
			  		var extEstimateContractRateAbove=0;
			  		var estSellLocalRate1=0;
			  		var roundValue=0;
			  		var estimateQuantity1=0;
			  		extSellRateBelow=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
			  		extEstimateContractRateEXAbove=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value;
			  		extEstimateContractRateAbove=document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
			  		try{
						  	if((extEstimateContractRateAbove!='')&&(extEstimateContractRateAbove!='0.00')&&(extEstimateContractRateAbove!='0')&&(extEstimateContractRateAbove!='0.0')){
						  		estSellLocalRate1=(extSellRateBelow*extEstimateContractRateAbove)/extEstimateContractRateEXAbove; 
						        roundValue=Math.round(estSellLocalRate1*100)/100;
							    document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=roundValue;
						    }else{
				  		        var recRateExcha1=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value
				  		        var recRat1=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value
				  		        var recCurrencyR1=recRat1*recRateExcha1;
				  		        var roundValue4=Math.round(recCurrencyR1*100)/100;
				  		        document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=roundValue4 ; 
						    }
			  		}catch(e){
			  			document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value='0.00';
				  		}
			  		
			  		try{
			  			estSellLocalRate1=document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;
				  		estimateQuantity1=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
				  	  	var basis1 =document.forms['serviceOrderForm'].elements['basis'+aid].value; 
				        var estimateSellLocalAmountRound=0;
				        
		    	   	    if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
		    	   	    	estimateQuantity1 =document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value=1;
		    	            } 
		    	        if(basis1=="cwt" || basis1=="%age"){
		    	        	estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/100;
		    	        	   estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*100)/100;
		    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;	  	
		    	          	}  else if(basis1=="per 1000"){
		    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/1000;
		    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*100)/100;
		    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
		    	          	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
		    	          		estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1); 
		    	          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*100)/100; 
		    	          	  document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
		    	     	    }
		    	           estimateDeviation1=document.forms['serviceOrderForm'].elements['estimateSellDeviation'+aid].value;
		    	           if(estimateDeviation1!='' && estimateDeviation1>0){
		    	          	estimateSellLocalAmountRound=estimateSellLocalAmountRound*estimateDeviation1/100;
		    	          	estimateSellLocalAmountRound=Math.round(estimateSellLocalAmountRound*100)/100;
		    	             document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value = estimateSellLocalAmountRound;	 
		    	         } 
			  		}catch(e){
			  			document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value = '0.00';
					  		}
			  		</c:if>
			  		<c:if test="${!contractType}">
			  		var estSellLocalRate1=0;
  						extSellRateBelow=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
		  				estSellLocalRate1=document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;
		  			var estimateQuantity1=0;
			    	   var roundValue=0; 
			    	   try{
			    	  	 if(extSellRateBelow==0) {
			    	         document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value=0*1;
			    	     } else{
			    	       var amount1=estSellLocalRate1/extSellRateBelow; 
			    	       roundValue=Math.round(amount1*100)/100;
			    	       document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value=roundValue ; 
			    	     }}catch(e){
			    	    	 document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value='0.00' ;
				    	     }
			    	     var estSelRate=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value;
			    	     estimateQuantity1=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
			    	     var basis1 =document.forms['serviceOrderForm'].elements['basis'+aid].value;
				    	   try{
					    	      var amount1=estSelRate*estimateQuantity1;
					    	        if(basis1=="cwt" || basis1=="%age"){
					    	        	amount1=(amount1)/100;
					    	          	}  else if(basis1=="per 1000"){
					    	          		amount1=(amount1)/1000;
					    	          	} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
					    	          		amount1=amount1;
					    	     	    }
					    	       roundValue=Math.round(amount1*100)/100;
					    	       document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value=roundValue ; 
					    	   }catch(e){
					    	    	 document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value='0.00' ;
						    	     }
					    	try{
						    	  var estimateRevenue=document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value;
						    	  var estimate=document.forms['serviceOrderForm'].elements['estimateExpense'+aid].value;
					    		  var estimatepasspercentage1 = (estimateRevenue/estimate)*100;
					    		  roundValue=Math.round(estimatepasspercentage1);
					    		  document.forms['serviceOrderForm'].elements['estimatePassPercentage'+aid].value=roundValue;
					    	}catch(e){
					    		document.forms['serviceOrderForm'].elements['estimatePassPercentage'+aid].value='0';
					    		 	}
			  		</c:if>
			  	}
				}
		  	 }    */



		  	function checkChargesDataFromSO() {
			  	var aid=accountLineIdScript;
		  		<c:if test="${contractType}"> 
		  		var contractCurrency=document.getElementById('contractCurrencyNew'+aid).value;		  		
		  		if(contractCurrency.trim()!="")
		  		{ 
		  			document.getElementById('estimateContractCurrencyNew'+aid).value=contractCurrency.trim();
		  			document.getElementById('revisionContractCurrencyNew'+aid).value=contractCurrency.trim();
		  			document.getElementById('contractCurrencyNew'+aid).value=contractCurrency.trim();
			  		 var rec='1';
		   			<c:forEach var="entry" items="${currencyExchangeRate}">
		   				if('${entry.key}'==contractCurrency.trim()){
							rec='${entry.value}';
		   				}
					</c:forEach>
					document.getElementById('estimateContractExchangeRateNew'+aid).value=rec;
					document.getElementById('revisionContractExchangeRateNew'+aid).value=rec;
					document.getElementById('contractExchangeRateNew'+aid).value=rec;
					
					var mydate=new Date();
				          var daym;
				          var year=mydate.getFullYear()
				          var y=""+year;
				          if (year < 1000)
				          year+=1900
				          var day=mydate.getDay()
				          var month=mydate.getMonth()+1
				          if(month == 1)month="Jan";
				          if(month == 2)month="Feb";
				          if(month == 3)month="Mar";
						  if(month == 4)month="Apr";
						  if(month == 5)month="May";
						  if(month == 6)month="Jun";
						  if(month == 7)month="Jul";
						  if(month == 8)month="Aug";
						  if(month == 9)month="Sep";
						  if(month == 10)month="Oct";
						  if(month == 11)month="Nov";
						  if(month == 12)month="Dec";
						  var daym=mydate.getDate()
						  if (daym<10)
						  daym="0"+daym
						  var datam = daym+"-"+month+"-"+y.substring(2,4);
						  document.getElementById('estimateContractValueDateNew'+aid).value=datam;
						  document.getElementById('revisionContractValueDateNew'+aid).value=datam;
						  document.getElementById('contractValueDateNew'+aid).value=datam;
		  		}
		  		var payableContractCurrency=document.getElementById('payableContractCurrencyNew'+aid).value;
		  		if(payableContractCurrency.trim()!="")
		  		{ 
		  			document.getElementById('estimatePayableContractCurrencyNew'+aid).value=payableContractCurrency.trim();
		  			document.getElementById('revisionPayableContractCurrencyNew'+aid).value=payableContractCurrency.trim();
		  			document.getElementById('payableContractCurrencyNew'+aid).value=payableContractCurrency.trim();
			  		 var rec='1';
		   			<c:forEach var="entry" items="${currencyExchangeRate}">
		   				if('${entry.key}'==payableContractCurrency.trim()){
							rec='${entry.value}';
		   				}
					</c:forEach>
					document.getElementById('estimatePayableContractExchangeRateNew'+aid).value=rec;
					document.getElementById('revisionPayableContractExchangeRateNew'+aid).value=rec;
					document.getElementById('payableContractExchangeRateNew'+aid).value=rec;
					
					var mydate=new Date();
				          var daym;
				          var year=mydate.getFullYear()
				          var y=""+year;
				          if (year < 1000)
				          year+=1900
				          var day=mydate.getDay()
				          var month=mydate.getMonth()+1
				          if(month == 1)month="Jan";
				          if(month == 2)month="Feb";
				          if(month == 3)month="Mar";
						  if(month == 4)month="Apr";
						  if(month == 5)month="May";
						  if(month == 6)month="Jun";
						  if(month == 7)month="Jul";
						  if(month == 8)month="Aug";
						  if(month == 9)month="Sep";
						  if(month == 10)month="Oct";
						  if(month == 11)month="Nov";
						  if(month == 12)month="Dec";
						  var daym=mydate.getDate()
						  if (daym<10)
						  daym="0"+daym
						  var datam = daym+"-"+month+"-"+y.substring(2,4);
						  document.getElementById('estimatePayableContractValueDateNew'+aid).value=datam;
						  document.getElementById('revisionPayableContractValueDateNew'+aid).value=datam;
						  document.getElementById('payableContractValueDateNew'+aid).value=datam;
		  		}
		  		calculateEstimateByChargeCodeBlock(aid,'BAS');
		  		</c:if>		
		  	}

		  	 <%--
		  	function fillCurrencyByChargeCode(aid) {
			  	if(aid=='temp'){
			  		aid=accountLineIdScript;
			  	}
		  		<c:if test="${contractType}"> 
		  	    var vendorCode = document.getElementById('vendorCode'+aid).value;  
		  	    var url="fillCurrencyByChargeCode.html?ajax=1&decorator=simple&popup=true&vendorCode="+vendorCode;  
		  	     http5513.open("GET", url, true); 
		  	     http5513.onreadystatechange = function(){ handleHttpResponse4113(aid);};
		  	     http5513.send(null); 
		  	     </c:if> 
		  	} 
		  	function handleHttpResponse4113(aid) {
		  	    if (http5513.readyState == 4)  {
		  			       var results = http5513.responseText
		  			       results = results.trim(); 
		  			       if(results!=""){
		  		                var res = results.split("~");
		  		            if(res[0].trim()!=''){
		  		                document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value=res[0];		                	
		  		                document.forms['serviceOrderForm'].elements['revisionCurrencyNew'+aid].value=res[0];
		  		                document.forms['serviceOrderForm'].elements['countryNew'+aid].value=res[0]; 
		  		                document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value=res[1];
		  		                document.forms['serviceOrderForm'].elements['revisionExchangeRateNew'+aid].value=res[1];
		  		                document.forms['serviceOrderForm'].elements['exchangeRateNew'+aid].value=res[1];
		  						var mydate=new Date();
		  	  		          var daym;
		  	  		          var year=mydate.getFullYear()
		  	  		          var y=""+year;
		  	  		          if (year < 1000)
		  	  		          year+=1900
		  	  		          var day=mydate.getDay()
		  	  		          var month=mydate.getMonth()+1
		  	  		          if(month == 1)month="Jan";
		  	  		          if(month == 2)month="Feb";
		  	  		          if(month == 3)month="Mar";
		  	  				  if(month == 4)month="Apr";
		  	  				  if(month == 5)month="May";
		  	  				  if(month == 6)month="Jun";
		  	  				  if(month == 7)month="Jul";
		  	  				  if(month == 8)month="Aug";
		  	  				  if(month == 9)month="Sep";
		  	  				  if(month == 10)month="Oct";
		  	  				  if(month == 11)month="Nov";
		  	  				  if(month == 12)month="Dec";
		  	  				  var daym=mydate.getDate()
		  	  				  if (daym<10)
		  	  				  daym="0"+daym
		  	  				  var datam = daym+"-"+month+"-"+y.substring(2,4);
		  		                document.forms['serviceOrderForm'].elements['estValueDateNew'+aid].value=datam;
		  		                document.forms['serviceOrderForm'].elements['revisionValueDateNew'+aid].value=datam;
		  		                document.forms['serviceOrderForm'].elements['valueDateNew'+aid].value=datam;
		  		            }

		  		          <c:if test="${contractType}">
		    		        try{
		    	  		        var buyRate1=document.forms['serviceOrderForm'].elements['estimateRate'+aid].value;
		    	  		        var estCurrency1=document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value;
		    	  		        var basisValue=document.forms['serviceOrderForm'].elements['basis'+aid].value;
		    	    	  		var quantityValue1=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
		    		  		    if(quantityValue1=='' ||quantityValue1=='0' ||quantityValue1=='0.0' ||quantityValue1=='0.00') {   
		    		  		      document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value=1;
		    		  		      }
		    	  		       var quantityValue=0;
		    	  		     	quantityValue=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
		    	  				if(estCurrency1.trim()!=""){
		    		  		          try{
		    			  			        var val1=0.00;
		    			  			        val1=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
		    			  			        var val2=0.00;
		    			  			        val2=document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
		    			  			        var val3=0.00;
		    			  			        val3=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
		    								if((val2!='')&&(val2!='0.00')&&(val2!='0.0')&&(val2!='0')){
		    				  			    var recCurrencyRate1=(val1*val2)/val3;
		    			  			        var roundValue=Math.round(recCurrencyRate1*100)/100;
		    			  			        document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=roundValue;
		    								}else{
		    		  						var Q1=0;
		    		  						var Q2=0;
		    		  						Q1=buyRate1;
		    		  						Q2=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
		    		  			            var E1=Q1*Q2;
		    		  			            E1=Math.round(E1*100)/100;
		    		  			            document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=E1;
		    								}			  			          			        
		    			  		          }catch(e){ 
		    			  	  		          }
		    	  			            var E1=document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value;
		    	  			            var estLocalAmt1=0;
		    	  			            if(basisValue=="cwt" || basisValue== "%age"){
		    	  			            	estLocalAmt1=(quantityValue*E1)/100 ;
		    	  			                }
		    	  			                else if(basisValue=="per 1000"){
		    	  			                	estLocalAmt1=(quantityValue*E1)/1000 ;
		    	  			                }else{
		    	  			                	estLocalAmt1=(quantityValue*E1);
		    	  			                }	 
		    	  			            estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
		    	  			            document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=estLocalAmt1;     
		    	  				}
		    	  		      }catch(e){}
		  		          </c:if>		  		            
		  			       }
		  	        } 
		  	    
		  	    }	  	
		  	
			   var http5513 = getHTTPObject88();
			   --%>		  	 
		//differnt method added for pricing  8-31-2012  End

  		 function getHTTPObject88() {
    	    var xmlhttp;
    	    if(window.XMLHttpRequest) {
    	        xmlhttp = new XMLHttpRequest();
    	    }
    	    else if (window.ActiveXObject)  {
    	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    	        if (!xmlhttp)   {
    	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    	        }  }
    	    return xmlhttp;
    	   }
    	 var http88 = getHTTPObject88();      
</script>
<script type="text/javascript"> 
      function getAddress(targetElement,temp){
           	 var address=targetElement.value;
        	 address=address.trim();
        	 var cid='${customerFile.id}';
        	 if(address!='')
        	 {
            	if(address=='Origin Address')
            	{
            		   var url="getAddress.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&addType="+ encodeURI(temp);
            		   httpst.open("GET",url,true);
            		   httpst.onreadystatechange = function(){ handleHttpResponseState(temp,address);};
            		   httpst.send(null);
                	
            	}
            	else if(address=='Destination Address')
            	{
            		   var url="getAddress.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&addType="+ encodeURI(temp);
            		   httpst.open("GET",url,true);
            		   httpst.onreadystatechange = function(){ handleHttpResponseState(temp,address);};
            		   httpst.send(null);
                	
            	}
            	else
            	{
                	var tempAddress = address.split('^');
                	if(tempAddress[1]=='add'){
            		   var url="getAddressDescription.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&descType="+ encodeURI(tempAddress[0]);
            		   httpst.open("GET",url,true);
            		   httpst.onreadystatechange = function(){ handleHttpResponseState(temp,address);};
            		   httpst.send(null);
                	}
                	if(tempAddress[1]=='std'){
                       var url="getStandardAddressDescription.html?ajax=1&decorator=simple&popup=true&custId=" + encodeURI(cid)+"&stdAddId="+ encodeURI(tempAddress[0]);
					   httpst.open("GET",url,true);
               		   httpst.onreadystatechange = function(){ handleHttpResponseState(temp,address);};
               		   httpst.send(null);
                   }
                }            	
        	 }        	 
        }
      function handleHttpResponseState(temp,address){
  		if (httpst.readyState == 4){
      			var results = httpst.responseText                  
                  results = results.trim();   
                  var res = results.split("~");                                                                   
                 	if(address=='Origin Address')  	{      
                     	document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value = res[0];
						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value = res[1];
						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value = res[2];
						//document.forms['serviceOrderForm'].elements['serviceOrder.originCompany'].value = res[3];
						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value = res[4];
						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = res[5];
						document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = res[6];
						document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value = res[7];
						document.forms['serviceOrderForm'].elements['serviceOrder.originDayPhone'].value = res[8];
						document.forms['serviceOrderForm'].elements['serviceOrder.originDayExtn'].value = res[9];
						document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value = res[10];
						document.forms['serviceOrderForm'].elements['serviceOrder.originMobile'].value = res[11];
						document.forms['serviceOrderForm'].elements['serviceOrder.originFax'].value = res[12];
						document.forms['serviceOrderForm'].elements['addAddressState'].value='';
						getADDState('OA');
  					} 
                 	else if(address=='Destination Address') 	{
                     	document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value = res[0];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value = res[1];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value = res[2];
						//document.forms['serviceOrderForm'].elements['serviceOrder.destinationCompany'].value = res[3];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value = res[4];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = res[5];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = res[6];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value = res[7];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayPhone'].value = res[8];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayExtn'].value = res[9];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value = res[10];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationMobile'].value = res[11];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationFax'].value = res[12];
						document.forms['serviceOrderForm'].elements['addAddressState'].value='';
						getADDState('DA');
					}
                 	else 
                	{
                 		var tempAddress = address.split('^');
                 		if(tempAddress[1]=='add'){
                 	if(temp=='origin'){ 
                     	if(res!=''){                    		
	                 			document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value = res[0];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value = res[1];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value = res[2];
	                 			//document.forms['serviceOrderForm'].elements['serviceOrder.originCompany'].value = '';
	                     			    						   
	                 			targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'];            				
	             				for(i=0;i<targetElement.length;i++)
	            				{
	             					
	                				if(res[3] == targetElement.options[i].value)
	                				{	
	                				
	                 			document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].text = res[3];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].value = res[3];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].selected=true;
	         					}
	            				} 
	             														
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = res[4];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = res[5];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value = res[6];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayPhone'].value = res[7];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayExtn'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originMobile'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originFax'].value = '';
	    						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[4];
	    						getADDState('OA');
	                     	}else{
	                     		document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value = '';
	    						//document.forms['serviceOrderForm'].elements['serviceOrder.originCompany'].value = res[3];
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =true;
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayPhone'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayExtn'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value = '';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originMobile'].value ='';
	    						document.forms['serviceOrderForm'].elements['serviceOrder.originFax'].value = '';
	    						document.forms['serviceOrderForm'].elements['addAddressState'].value='';
	                         }
    					}
                 		if(temp=='dest'){
                     		if(res!=''){
                 			document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value = res[0];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value = res[1];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value = res[2];
    						//document.forms['serviceOrderForm'].elements['serviceOrder.destinationCompany'].value = '';
							targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'];            				
             				for(i=0;i<targetElement.length;i++)
            				{
                				if(res[3] == targetElement.options[i].value)
                				{	
                				
                 			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].text = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].value = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].selected=true;
         					}
            				} 
             				document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = res[4];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = res[5];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value = res[6];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayPhone'].value = res[7];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayExtn'].value = '';
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value = '';
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationMobile'].value = '';
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationFax'].value = '';
    						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[4];
    						getADDState('DA');
                     		}else{
                     			document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value = '';
        						//document.forms['serviceOrderForm'].elements['serviceOrder.destinationCompany'].value = res[3];
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =true;
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayPhone'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayExtn'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationMobile'].value = '';
        						document.forms['serviceOrderForm'].elements['serviceOrder.destinationFax'].value = '';
        						document.forms['serviceOrderForm'].elements['addAddressState'].value='';
                         	}
    					}
                	}
                 	if(tempAddress[1]=='std'){
                     	if(temp=='origin'){                     		
                 			document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value = res[0];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value = res[1];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine3'].value = res[2];
                 			//document.forms['serviceOrderForm'].elements['serviceOrder.originCompany'].value = '';
                     			    						   
                 			targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'];            				
             				for(i=0;i<targetElement.length;i++)
            				{
             					
                				if(res[3] == targetElement.options[i].value)
                				{	
                				
                 			document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].text = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].value = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].options[i].selected=true;
         					}
            				} 
             														
    						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = res[4];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = res[5];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value = res[6];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayPhone'].value = res[7];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originDayExtn'].value = '';
    						document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value = res[8];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originMobile'].value = res[9];
    						document.forms['serviceOrderForm'].elements['serviceOrder.originFax'].value =res[10];
    						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[4];
    						getADDState('OA');
    					}
                 		if(temp=='dest'){
                 			document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value = res[0];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value = res[1];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine3'].value = res[2];
    						//document.forms['serviceOrderForm'].elements['serviceOrder.destinationCompany'].value = '';
							targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'];            				
             				for(i=0;i<targetElement.length;i++)
            				{
                				if(res[3] == targetElement.options[i].value)
                				{	
                				
                 			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].text = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].value = res[3];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].options[i].selected=true;
         					}
            				} 
             				document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = res[4];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = res[5];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value = res[6];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayPhone'].value = res[7];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationDayExtn'].value = '';
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value = res[8];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationMobile'].value = res[9];
    						document.forms['serviceOrderForm'].elements['serviceOrder.destinationFax'].value = res[10];
    						document.forms['serviceOrderForm'].elements['addAddressState'].value=res[4];
    						getADDState('DA');
    					}

                     }
                	}					
                    }
  				}      		      
	var httpst = getHTTPObjectst();
	function getHTTPObjectst()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
	
    return xmlhttp;
}	
	
</script>
<script language="javascript">
function getHTTPObject21()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var httpADDDetail = getHTTPObject21();
    
function getADDState(from) {
var country = "";
var getStateList = "";
var countryCode = "";
if(from == 'OA')
{
	country = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	if(country == 'United States')
	{
		countryCode = "USA";
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled=false;
	}
	else if(country == 'India')
	{
		countryCode = "IND";
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled=false;
	}
	else if(country == 'Canada')
	{
		countryCode = "CAN";
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled=false;
	}
	else
	{
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = true;
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = '';
	}
	getStateList = handleHttpResponseAddOrg;
}
if(from == 'DA')
{
	country = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	if(country == 'United States')
	{
		countryCode = "USA";
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled=false;
	}
	else if(country == 'India')
	{
		countryCode = "IND";
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled=false;
	}
	else if(country == 'Canada')
	{
		countryCode = "CAN";
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled=false;
	}
	else
	{
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = true;
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = '';
	}

	getStateList = handleHttpResponseAddDest;
}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2="+encodeURI(countryCode);
	httpADDDetail.open("GET", url, true);
	httpADDDetail.onreadystatechange = getStateList;
	httpADDDetail.send(null);
}

function handleHttpResponseAddOrg()
{
	 if (httpADDDetail.readyState == 4)
     {
    	var tempState = document.forms['serviceOrderForm'].elements['addAddressState'].value;
    	var results = httpADDDetail.responseText;
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
       	targetElement.length = res.length;
			for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
			
			if (tempState=='' && document.getElementById("originState").value == '${serviceOrder.originState}' ){
				document.getElementById("originState").options[i].selectedIndex = true;
			}
			if(tempState!='' && document.getElementById("originState").options[i].value == tempState ){
				document.getElementById("originState").options[i].selected = true;
			}
			}
			}
			var oCountry1=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
			
			if(oCountry1 == 'India' || oCountry1 == 'Canada' || oCountry1 == 'United States'){
				
				document.getElementById('originState').disabled = false;

			}else{
			document.getElementById('originState').disabled = true;
			document.getElementById('originState').value ="";
			}
			}
     }
 

function handleHttpResponseAddDest()
{
	if (httpADDDetail.readyState == 4)
     {
    	var tempState = document.forms['serviceOrderForm'].elements['addAddressState'].value;
    	var results = httpADDDetail.responseText;
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
       targetElement.length = res.length;
			for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
			}else{
			stateVal = res[i].split("#");
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
			if (tempState=='' && document.getElementById("destinationState").value == '${serviceOrder.destinationState}'){
				  document.getElementById("destinationState").options[i].selectedIndex = true;
			}
			if (document.getElementById("destinationState").options[i].value == tempState && tempState!='' ){
				   document.getElementById("destinationState").options[i].selected = true;
				}					
			}
			}
			var oCountry1=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
			
			if(oCountry1 == 'India' || oCountry1 == 'Canada' || oCountry1 == 'United States'){						
				document.getElementById('destinationState').disabled = false;		
			}else{
			document.getElementById('destinationState').disabled = true;
			document.getElementById('destinationState').value ="";
			}
     }
}
<%--
function selectAll5(str){
	 <c:forEach var="accountLineList" items="${accountLineList}">
	  if(str.checked==true){
	   document.getElementById("displayOnQuote${accountLineList.id}").checked=true;
	  }else{
	   document.getElementById("displayOnQuote${accountLineList.id}").checked=false;
	  }
	 </c:forEach>
	}

function selectAdd(str){
	 <c:forEach var="accountLineList" items="${accountLineList}">
	  if(str.checked==true){
	   document.getElementById("additionalService${accountLineList.id}").checked=true;
	  }else{
	   document.getElementById("additionalService${accountLineList.id}").checked=false;
	  }
	 </c:forEach>
	}
function selectAllAct(str){
	 <c:forEach var="accountLineList" items="${accountLineList}">
	  if(str.checked==true){
	   document.getElementById("statusCheck${accountLineList.id}").checked=true;
	  }else{
	   document.getElementById("statusCheck${accountLineList.id}").checked=false;
	  }
	 </c:forEach>
}
--%>
function checkUnitValidation(){
	var e4 = document.getElementById('hid4');
	if(document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value!=''){
		e4.style.visibility = 'visible';	
	}else{
		e4.style.visibility = 'collapse';
	}
	
}
</script>
 		  
 		  
<tr>
             		  <td height="10" width="100%" align="left" >
             		  <div  onClick="javascript:animatedcollapse.toggle('address')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Address Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
             				
					<div id="address">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
	<tbody>
	<tr><td class="subcontenttabChild"><font color="black"><b>&nbsp;Origin</b></font></td></tr>
	<tr>
 	<td>
 	<table class="detailTabLabel" border="0" width="" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
									<td align="left" height="6px"></td></tr>
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originAddress1' /></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext" ></td>
												<td align="left" class="listwhitetext-scope"><fmt:message	key='serviceOrder.originCompany' /></td>
												<td align="left" class="listwhitetext" style="width:23px"></td>
												<td align="left" class="listwhitetext" colspan="3">Address Book</td>
												</tr>
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><s:textfield	cssClass="input-text upper-case" name="serviceOrder.originAddressLine1" cssStyle="width:230px;" maxlength="100" tabindex="201" />
												</td>
												<td align="left">
												<img align="top" class="openpopup" width="17" height="20" onclick="openStandardAddressesPopWindow();" src="<c:url value='/images/open-popup.gif'/>" />
												</td>											
												<td align="left" class="listwhitetext" style="width:24px"></td>
												<td align="left" class="listwhitetext"><s:hidden cssClass="input-text" name="serviceOrder.originCompany" /></td>
												<td align="left" class="listwhitetext" width="5px"></td>
												<td align="left" class="listwhitetext" colspan="3"><s:select cssClass="list-menu" cssStyle="width:183px;" list="%{originAddress}" headerKey="" headerValue="" onchange="getAddress(this,'origin');" name="addOriginAdd" id="addOriginAdd" tabindex="" /></td>
											</tr>
											<tr>
									<td align="left" height="3px"></td></tr>
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="serviceOrder.originAddressLine2" cssStyle="width:230px;" maxlength="100" tabindex="202" /></td>
												<td align="left" class="listwhitetext" ><font color="gray">&nbsp;&nbsp;Address2</font></td>
												<td align="left" class="listwhitetext" style="width:58px"></td>
												<td align="left" class="listwhitetext" style="padding-top:5px" colspan="4">Port Of Lading</td>
											</tr>
											<tr>
											<td align="left" height="3px"></td></tr>
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="serviceOrder.originAddressLine3" cssStyle="width:230px;" maxlength="100"  tabindex="203" /></td>
												<td align="left" class="listwhitetext" ><font color="gray">&nbsp;&nbsp;Address3</font></td>
												<td align="left" class="listwhitetext" style="width:123px"></td>
												<td align="left" class="listwhitetext" colspan="4"><s:textfield cssClass="input-text" name="serviceOrder.portOfLading" cssStyle="width:275px;" maxlength="45" tabindex="204" /></td>
											</tr>
										</tbody>
									</table>
									</td>
									</tr>
									<tr>
									<td>
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0">
										<tbody>
											<tr>
												<td style="width:50px; height:5px;" colspan="7"></td>
											</tr>
<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originCountry' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext"></td>

												
												<td align="left" id="originStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originState' /></td>
												<td align="left" id="originStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originState' /><font color="red" size="2">*</font></td>
												
												
												<td align="left" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originCity' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originZip' /></td>

											</tr>
<tr>
												<td align="right" class="listwhitetext" style="width:25px"></td>
												<td>
												<c:if test="${not empty serviceOrder.id}">
												<s:select cssClass="list-menu" name="serviceOrder.originCountry" list="%{ocountry}" cssStyle="width:234px" onchange="appliedState(); getOriginCountryCode(this);autoPopulate_customerFile_originCountry(this);getState(this),changeStatus();enableStateListOrigin();" headerKey="" headerValue="" tabindex="205" />
												</c:if>
												<c:if test="${empty serviceOrder.id}">
												<s:select cssClass="list-menu" name="serviceOrder.originCountry" list="%{ocountry}" cssStyle="width:234px" onchange="appliedState(); getOriginCountryCode(this);autoPopulate_customerFile_originCountry(this);populateInclusionExclusionData('default','1','F');getState(this),changeStatus();enableStateListOrigin();" headerKey="" headerValue="" tabindex="205" />
												</c:if>
												</td>
												<td width="32px"><img src="${pageContext.request.contextPath}/images/globe.jpg" HEIGHT=20 WIDTH=20 onclick="openOriginLocation();" /></td>
												<td><s:select cssClass="list-menu" name="serviceOrder.originState" id="originState" list="%{ostates}" cssStyle="width:131px" onchange="autoPopulate_customerFile_originCityCode(this),changeStatus();" headerKey="" headerValue="" tabindex="206"/></td>
												<td align="left" class="listwhitetext" style="width:15px"></td>
												<td><s:textfield cssClass="input-text upper-case" name="serviceOrder.originCity" id="originCity" cssStyle="width:128px;" maxlength="30" onkeypress="" onblur="titleCase(this)" onchange="autoPopulate_customerFile_originCityCode(this);" onkeyup="autoCompleterAjaxCallOriCity();"  tabindex="207"/></td>
												<td align="left" class="listwhitetext" style="width:10px"><div id="zipCodeList" style="vertical-align:middle;"><a><img class="openpopup" id="navigation8" onclick="findCityStateNotPrimary('OZ', this)" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></td>
												<td align="right" class="listwhitetext" style="width:5px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originZip" cssStyle="width:113px;" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" onblur="findCityStateOfZipCode(this,'OZ');" tabindex="208" /></td>
                                                <configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
																<td class="listwhitetext">
                                                            <a id="myUniqueLinkId" onclick="callPostalCode();"> &nbsp;&nbsp;<font style="font-size:11px;font-weight:bold;text-decoration:underline;cursor: pointer;">Look-Up Canadian Postal Code</font></a>
                                                            </td>
                                                            </configByCorp:fieldVisibility>
											</tr>
											</tbody>
									</table>
									</td>
									</tr>

<tr><td>
<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="0">
<tbody>
<tr>    
                   <td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originDayPhone' /></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originDayPhoneExt' /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originHomePhone' /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originMobile' /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originFax' /></td>
											</tr>
<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originDayPhone" cssStyle="width:160px;" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex="209" /></td>
												<td align="right" class="listwhitetext" style="width:11px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originDayExtn" cssStyle="width:54px;" maxlength="4"	onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex="210" /></td>
												<td align="right" class="listwhitetext" style="width:33px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originHomePhone" cssStyle="width:128px;" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex="211" /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originMobile" cssStyle="width:128px;" maxlength="25" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex="212" /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originFax" cssStyle="width:128px;" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')"	onkeyup="valid(this,'special')" tabindex="213" /></td>

											</tr>
										</tbody>
									</table>
									</td>
									</tr>
									<tr>
									<td>

<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr> 
                   <td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.email' /></td>
												<td align="left" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.email2' /></td>
												<td align="left" class="listwhitetext"></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
												<td align="left" class="listwhitetext" style="width:10px"></td>
											</tr>
	<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.email" cssStyle="width:229px;" maxlength="65"  tabindex="214"/></td>
												<td align="right" class="listwhitetext" style="width:25px"><img class="openpopup" onclick="sendEmail(document.forms['serviceOrderForm'].elements['serviceOrder.email'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${serviceOrder.email}"/></td>
												<td align="right" class="listwhitetext" style="width:8px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.email2" cssStyle="width:248px;" maxlength="65"  tabindex="215"/></td>
												<td align="right" class="listwhitetext" style="width:25px">&nbsp;&nbsp;<img class="openpopup" onclick="sendEmail(document.forms['serviceOrderForm'].elements['serviceOrder.email2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${serviceOrder.email2}"/></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td ><s:textfield cssClass="input-text" name="serviceOrder.originPreferredContactTime" cssStyle="width:131px;" maxlength="30"  tabindex="216"/></td>
												<td></td>
												
												<td align="right" class="listwhitetext" style="width:50px"></td>
												
											</tr>
</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td>
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0">
				<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" ></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.originLoadSite',this);return false" onmouseout="ajax_hideTooltip()">
												<fmt:message key='serviceOrder.originLoadSite' /></a></td>
												<td align="right" class="listwhitetext" style="width:18px"></td>
												<configByCorp:fieldVisibility componentId="component.field.serviceOrder.originMilitary">
												<td align="left" class="listwhitetext" colspan="3"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.originMilitary',this);return false" onmouseout="ajax_hideTooltip()">
												<fmt:message key='serviceOrder.originMilitary' /></a></td>
												<td align="right" class="listwhitetext" style="width:5px"></td>
												</configByCorp:fieldVisibility>
												</tr>
												<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
												<td><s:select cssClass="list-menu" name="serviceOrder.originLoadSite" list="%{loadsite}" cssStyle="width:233px" onchange="changeStatus();"  tabindex="217"/></td>
												<td align="right" class="listwhitetext" style="width:32px"></td>
												<configByCorp:fieldVisibility componentId="component.field.serviceOrder.originMilitary">
												<td colspan="3"><s:select cssClass="list-menu" name="serviceOrder.originMilitary" list="%{military}"
													cssStyle="width:134px" onchange="changeStatus();" tabindex="218"
													 /></td>
												</configByCorp:fieldVisibility>
												</tr>
												<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
												<tr>
												<td align="right" class="listwhitetext" style="width:92px" ></td>
												<td align="left" class="listwhitetext"><fmt:message	key='serviceOrder.originContactName' /></td>
												<td align="right" class="listwhitetext" style="width:18px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='serviceOrder.originContactWork' /></td>
												<td align="right" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.originDayPhoneExt' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='serviceOrder.originContactPhone' /></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originContactName" cssStyle="width:230px;" maxlength="35"  tabindex="219"/></td>
												<td align="right" class="listwhitetext" style="width:18px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originContactWork" cssStyle="width:128px;" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')"  tabindex="220"/></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originContactNameExt" cssStyle="width:54px;" maxlength="4"	onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')"  tabindex="221"/></td>
												<td align="right" class="listwhitetext" style="width:5px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.originContactEmail" cssStyle="width:207px;" maxlength="65"  tabindex="222"/></td>
												<td align="right" class="listwhitetext" style=""></td>
<c:if test="${empty serviceOrder.id}">
													<td style="width:117px" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
												</c:if>
												<c:if test="${not empty serviceOrder.id}">
													<c:choose>
														<c:when
															test="${countOriginDetailNotes == '0' || countOriginDetailNotes == '' || countOriginDetailNotes == null}">
															<td style="width:117px" align="right"><img id="countOriginDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50
																onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=Origin&imageId=countOriginDetailNotesImage&fieldId=countOriginDetailNotes&decorator=popup&popup=true',800,600);" /><a
																onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=Origin&imageId=countOriginDetailNotesImage&fieldId=countOriginDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
														</c:when>
														<c:otherwise>
															<td style="width:117px" align="right"><img id="countOriginDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50
																onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=Origin&imageId=countOriginDetailNotesImage&fieldId=countOriginDetailNotes&decorator=popup&popup=true',800,600);" /><a
																onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=Origin&imageId=countOriginDetailNotesImage&fieldId=countOriginDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
														</c:otherwise>
													</c:choose>
												</c:if>
											</tr>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
										</tbody>
										
</table>
									
								    <tr>
									<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
										<tbody>
											<tr>
												<tr><td align="center" colspan="10" class="vertlinedata"></td></tr>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								
								<tr>
									<td valign="top" align="left" class="listwhitetext">
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
										<tbody>

											<tr>
												<td class="subcontenttabChild" colspan="7"><font color="black"><b>&nbsp;Destination</b></font></td>
											</tr>
											<tr>
											<td>
											<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
											
											<tr><td height="6px"></td></tr>
											<tr>
												<td align="left" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationAddress1' /></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext-scope"><fmt:message key='serviceOrder.destinationCompany' /></td>
												<td align="left" class="listwhitetext" style="width:23px"></td>
												<td align="left" class="listwhitetext" colspan="3">Address Book</td>
											</tr>
<tr>
					<td align="left" style="width:25px"></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="serviceOrder.destinationAddressLine1" cssStyle="width:230px;" maxlength="100"  tabindex="224" />
					</td>
					<td>
					<img align="top" class="openpopup" width="17" height="20" onclick="openStandardAddressesDestinationPopWindow();" src="<c:url value='/images/open-popup.gif'/>" />
					</td>
					
					<td align="left" style="width:24px"></td>
      				<td align="left" class="listwhitetext"><s:hidden cssClass="input-text" name="serviceOrder.destinationCompany"/></td>
      				<td align="left" width="5px" ></td>
      				<td align="left" class="listwhitetext" colspan="3"><s:select cssClass="list-menu" cssStyle="width:183px;"  list="%{destinationAddress}" onchange="getAddress(this,'dest');" headerKey="" headerValue=""  name="addDestAdd" id="addDestAdd" tabindex="" /></td>
</tr>
<tr>
                    <td style="width:50px; height:5px;" colspan="7"></td>
</tr>
<tr>
					<td align="left" class="listwhitetext" style="width:90px"></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="serviceOrder.destinationAddressLine2" cssStyle="width:230px;" maxlength="100" tabindex="225" /></td>
					<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address2</font></td>
					<td align="left" class="listwhitetext" style="width:58px"></td>
					<td align="left" class="listwhitetext" colspan="4" style="width:35px; padding-top: 5px">Port&nbsp;Of&nbsp;Entry</td>
</tr>
<tr>
<td style="width:50px; height:5px;" colspan="7"></td>
</tr>
<tr>
     				<td align="left" class="listwhitetext" style="width:90px"></td>
					<td ><s:textfield cssClass="input-text upper-case" name="serviceOrder.destinationAddressLine3" cssStyle="width:230px;" maxlength="100" tabindex="226" /></td>
	     			<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address3</font></td>
					<td align="left" style="width:123px"></td>
					<td align="left" class="listwhitetext" colspan="4"><s:textfield cssClass="input-text" name="serviceOrder.portOfEntry" cssStyle="width:275px;" maxlength="45"  tabindex="227" /></td>
</tr>
											</table>
											</td>
											</tr>
										
										</tbody>
									</table>
																		
									</td>
									</tr>
									
									
									<tr>
									<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
															
<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationCountry' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" id="destinationStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.destinationState' /></td>
												<td align="left" id="destinationStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.destinationState' /><font color="red" size="2">*</font></td>
												
												<!--<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationState' /></td>
												--><td align="left" class="listwhitetext" style="width:10px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationCity' /><font color="red" size="2">*</font></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationZip' /></td>

											</tr>
<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td>
												<c:if test="${not empty serviceOrder.id}">
												<s:select cssClass="list-menu" name="serviceOrder.destinationCountry" list="%{dcountry}" cssStyle="width:234px" onchange="destinationState11(); getDestinationCountryCode(this);autoPopulate_customerFile_destinationCountry(this);changeStatus();getDestinationState(this);enableStateListDestin();" headerKey="" headerValue=""  tabindex="228"/>
												</c:if>
												<c:if test="${empty serviceOrder.id}">
												<s:select cssClass="list-menu" name="serviceOrder.destinationCountry" list="%{dcountry}" cssStyle="width:234px" onchange="destinationState11(); getDestinationCountryCode(this);autoPopulate_customerFile_destinationCountry(this);populateInclusionExclusionData('default','1','F');changeStatus();getDestinationState(this);enableStateListDestin();" headerKey="" headerValue=""  tabindex="228"/>
												</c:if>
												</td>
												<td width="32px"><img src="${pageContext.request.contextPath}/images/globe.jpg" HEIGHT=20 WIDTH=20 onclick="openDestinationLocation();" /></td>
												<td><s:select cssClass="list-menu" name="serviceOrder.destinationState" id="destinationState" list="%{dstates}" cssStyle="width:131px" onchange="autoPopulate_customerFile_destinationCityCode(this),changeStatus();" headerKey="" headerValue=""  tabindex="229"/></td>
												<td align="left" class="listwhitetext" style="width:15px"></td>
												<td><s:textfield cssClass="input-text upper-case" name="serviceOrder.destinationCity" id="destinationCity" cssStyle="width:128px;" maxlength="30" onkeypress="" onblur="titleCase(this)" onchange="autoPopulate_customerFile_destinationCityCode(this);" onkeyup="autoCompleterAjaxCallDestCity();"  tabindex="230"/></td>
												<td align="left"><div id="zipDestCodeList" style="vertical-align:middle;"><a><img id="navigation9" class="openpopup" onclick="findCityStateNotPrimary('DZ', this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></div></td>
												<td align="left" class="listwhitetext" style="width:5px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationZip" cssStyle="width:113px;" maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" onblur="findCityStateOfZipCode(this,'DZ');"  tabindex="231"/></td>
                                                <configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
																<td class="listwhitetext">
                                                            <a id="myUniqueLinkId" onclick="callPostalCode();">&nbsp;&nbsp;<font style="font-size:11px;font-weight:bold;text-decoration:underline;cursor: pointer;">Look-Up Canadian Postal Code</font></a>
                                                            </td>
                                                            </configByCorp:fieldVisibility>
											</tr>
										</tbody>
									</table>
								</tr>
								<tr>
									<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationDayPhone' /></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationDayPhoneExt' /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationHomePhone' /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationMobile' /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationFax' /></td>

											</tr>

<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationDayPhone" cssStyle="width:160px;" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')"  tabindex="232"/></td>
												<td align="right" class="listwhitetext" style="width:11px"></td>
												<td align="left"><s:textfield cssClass="input-text" name="serviceOrder.destinationDayExtn" cssStyle="width:54px;" maxlength="4" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex="233" /></td>
												<td align="right" class="listwhitetext" style="width:33px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationHomePhone" cssStyle="width:128px;" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex="234" /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationMobile" cssStyle="width:128px;" maxlength="25" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex="235" /></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationFax" cssStyle="width:128px;" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex="236" /></td>
											</tr>
										</tbody>
									</table>
									</td>
									</tr>
									<tr>
									<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
										<tbody>
											<tr>
												<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='serviceOrder.email' /></td>
												<td align="left" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext"></td>
												<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationEmail2' /></td>
												<td align="left" class="listwhitetext"></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
												<td align="left" class="listwhitetext" style="width:10px"></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationEmail" cssStyle="width:229px;" maxlength="65" tabindex="237" /></td>
												<td align="right" class="listwhitetext" style="width:25px"><img class="openpopup" onclick="sendEmail(document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${serviceOrder.destinationEmail}"/></td>
												<td align="right" class="listwhitetext" style="width:8px"></td>
												<td><s:textfield cssClass="input-text" name="serviceOrder.destinationEmail2" cssStyle="width:248px;" maxlength="65" tabindex="238" /></td>
												<td align="right" class="listwhitetext" style="width:25px">&nbsp;&nbsp;<img class="openpopup" onclick="sendEmail(document.forms['serviceOrderForm'].elements['serviceOrder.destinationEmail2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${serviceOrder.destinationEmail2}"/></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
												<td ><s:textfield cssClass="input-text" name="serviceOrder.destPreferredContactTime" cssStyle="width:131px;" maxlength="30" tabindex="239" /></td>
												<td></td>
												<td align="right" class="listwhitetext" style="width:15px"></td>
											</tr>
</tbody>
									</table>
									</td>
								</tr>
								
								<tr>
								<td>

								<table class="detailTabLabel" cellspacing="0" cellpadding="0" >
									<tbody>
										<tr>
											<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
										</tr>
<tr>
											<td align="right" class="listwhitetext" style="width:92px"></td>
											<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.destinationLoadSite',this);return false" onmouseout="ajax_hideTooltip()">
												<fmt:message key='serviceOrder.destinationLoadSite' /></a></td>
											<td align="left" class="listwhitetext" style="width:18px"></td>
											<configByCorp:fieldVisibility componentId="component.field.serviceOrder.destinationMilitary">
											<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.destinationMilitary',this);return false" onmouseout="ajax_hideTooltip()">
												<fmt:message key='serviceOrder.destinationMilitary' /></a></td>
												</configByCorp:fieldVisibility>	
												<td align="right" class="listwhitetext" style=""></td>										
											<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.consignee',this);return false" onmouseout="ajax_hideTooltip()">
												<fmt:message key='serviceOrder.consignee' /></a></td>
										</tr>
										<tr>
											<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
											<td><s:select cssClass="list-menu" name="serviceOrder.destinationLoadSite" list="%{loadsite}" cssStyle="width:233px" onchange="changeStatus();" tabindex="240"/></td>
											<td align="right" class="listwhitetext" style="width:32px"></td>
											<configByCorp:fieldVisibility componentId="component.field.serviceOrder.destinationMilitary">
											<td><s:select cssClass="list-menu" name="serviceOrder.destinationMilitary" list="%{military}" cssStyle="width:134px" onchange="changeStatus();" tabindex="241"/></td>
											<td align="right" class="listwhitetext" style="width:15px"></td>
											</configByCorp:fieldVisibility>
											<td colspan="2"><s:textfield cssClass="input-text" name="serviceOrder.consignee" cssStyle="width:128px;" maxlength="30"  tabindex="242"/></td>
											<td align="right" class="listwhitetext" style=""></td>
										</tr>
									</tbody>
								</table>
								</td>
								</tr>
								<tr>						
						<td>
						<div id="hidDM">
						<div id="map" style="width:0px; height:0px; display:none"></div>
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
							<tbody>
	                            <center><div id="map_canvas" style="width:0%; height:0%"></div></center>
							
							<tr>
							<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
							</tr>
								<tr>
								<td align="right" class="listwhitetext" style="width:90px"></td>
								<td align="right" class="listwhitetext" style="width:60px"></td>
										<td align="left" class="listwhitetext" style="width:10px"></td>										
										<td align="left" class="listwhitetext" style="width:10px">Distance&nbsp;(Est.)</td>
										<td align="left" class="listwhitetext" style="width:10px"></td>	
										<td align="left" class="listwhitetext">Unit<div id="hid4" style="margin: 0px;float:right;width:32px;"><font color="red" size="2">*</font></div></td>									
										</tr>
								<tr>
								<td align="right" class="listwhitetext"></td>
										<td  align="left" class="listwhitetext">
										<input type="button" name="Calc Distance" value="Calc Distance" class="cssbuttonB" onclick="calculateDistance();" tabindex="243" /></td>
										<td align="right" class="listwhitetext"></td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" id="distance"
										name="serviceOrder.distance" size="8" onblur="checkUnitValidation();"
										maxlength="10" required="true" tabindex="244"/></td>
										<td align="left" class="listwhitetext" style="width:10px"></td>	
                                       <td align="left" class="listwhitetext"><s:select cssClass="list-menu"  name="serviceOrder.distanceInKmMiles" list="{'KM','Mile'}"  headerKey="" headerValue=""  tabindex="245"/></td>									
								
								
								</tr>								
							</tbody>
						</table>
						</div>						
						</td>						
						</tr>
								<tr>
								<td>
								<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
									<tbody>
										<tr>
											<td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td>
										</tr>
<tr>
											<td align="right" class="listwhitetext" style="width:92px"></td>
											<td align="left" class="listwhitetext">
											<fmt:message	key='serviceOrder.contactName' /></td>
											<td align="left" class="listwhitetext" style="width:18px"></td>
											<td align="left" class="listwhitetext">
											<fmt:message key='serviceOrder.contactPhone' /></td>
											
											<td align="left" class="listwhitetext" style="width:5px"></td>
											<td align="left" class="listwhitetext"><fmt:message	key='customerFile.destinationDayPhoneExt' /></td>
												<td align="right" class="listwhitetext" style="width:10px"></td>
											<td align="left" class="listwhitetext">
											<fmt:message key='serviceOrder.contactFax' /></td>
										</tr>
										<tr>
											<td align="right" class="listwhitetext" style="width:92px">&nbsp;</td>
											<td><s:textfield cssClass="input-text" name="serviceOrder.contactName" cssStyle="width:230px;" maxlength="35" onkeydown="return onlyCharsAllowed(event)"  tabindex=""/></td>
											<td align="right" class="listwhitetext" style="width:31px"></td>
											<td><s:textfield cssClass="input-text" name="serviceOrder.contactPhone" cssStyle="width:128px;" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex=""/></td>
											<td align="right" class="listwhitetext" style="width:18px"></td>
											<td align="left"><s:textfield cssClass="input-text" name="serviceOrder.contactNameExt" cssStyle="width:54px;" maxlength="4" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" tabindex=""/></td>
												<td align="right" class="listwhitetext" style="width:5px"></td>
											<td><s:textfield cssClass="input-text" name="serviceOrder.destinationContactEmail" cssStyle="width:207px;" maxlength="65"  tabindex=""/></td>
											<td align="right" class="listwhitetext" style=""></td>
											
											<c:if test="${empty serviceOrder.id}">
												<td align="right" width="117px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
											</c:if>
											<c:if test="${not empty serviceOrder.id}">
												<c:choose>
													<c:when
														test="${countDestinationDetailNotes == '0' || countDestinationDetailNotes == '' || countDestinationDetailNotes == null}">
														<td align="right" width="117px"><img id="countDestinationDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50
															onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=Destination&imageId=countDestinationDetailNotesImage&fieldId=countDestinationDetailNotes&decorator=popup&popup=true',800,600);" /><a
															onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=Destination&imageId=countDestinationDetailNotesImage&fieldId=countDestinationDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
													</c:when>
													<c:otherwise>
														<td align="right" width="117px"><img id="countDestinationDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50
															onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=Destination&imageId=countDestinationDetailNotesImage&fieldId=countDestinationDetailNotes&decorator=popup&popup=true',800,600);" /><a
															onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=Destination&imageId=countDestinationDetailNotesImage&fieldId=countDestinationDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
													</c:otherwise>
												</c:choose>
											</c:if>
										</tr>
										
	<tr><td align="left" height="5px"></td></tr>
	</tbody>
								</table>								
								</td>			
											</tr>
										</tbody>
									</table>
									</div>
									</td>
								</tr> 
		<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">						
 			<tr>
				<td  width="100%" align="left" style="margin:0px">
					<div id="pricingShow" tabindex="200">
					<div onClick="javascript:animatedcollapse.toggle('pricing1');loadPricing();" style="margin:0px">
						<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
							<tr>
								<td class="headtab_left"></td>
								<td NOWRAP class="headtab_center">&nbsp;Pricing</td>
								<td width="28" valign="top" class="headtab_bg"></td>
								<td class="headtab_bg_center">&nbsp;</td>
								<td class="headtab_right"></td>
							</tr>
						</table>
					</div>
					
					<div id="pricing1" class="switchgroup1">
				 			<div id="pricingAjax">
				 	
				 			</div>
				 			
			 			
					</div>
					</div>
				</td>
			</tr>
			
					
			<%-- <tr>
							<td   align="left" class="listwhitetext">							
							<div id="pricingShow">	
							<c:if test="${not empty serviceOrder.id}">
							<div  onClick="javascript:animatedcollapse.toggle('pricing')" style="margin: 0px">
      						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
							<tr>
							<td class="headtab_left">
							</td>
							<td NOWRAP class="headtab_center">&nbsp;Pricing 
							</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_center">&nbsp;
							</td>
							<td class="headtab_right">
							</td>
							</tr>
						</table>
							</div>
							</c:if>
							<c:if test="${empty serviceOrder.id}">
							<div  onClick = "openPricing();" style="margin: 0px">
      						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
							<tr>
							<td class="headtab_left">
							</td>
							<td NOWRAP class="headtab_center">&nbsp;Pricing  
							</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_center">&nbsp;
							</td>
							<td class="headtab_right">
							</td>
							</tr>
						</table>
							</div>
							</c:if> 
							<div class="dspcont" id="pricing" >
							<div id="para1" >
							<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;"><tr><td>
		                     <display:table name="accountLineList"  class="table" requestURI="" id="accountLineList" style="width:1800px;margin-top:2px;margin-left:0px;margin-right:0px;" defaultsort="2" pagesize="100" >
		                     	<display:column title="#" style="width:10px;"><c:out value="${accountLineList_rowNum}"/></display:column>      
								<display:column sortProperty="accountLineNumber"  sortable="true" titleKey="accountLine.accountLineNumber"  style="width:10px; font-size:12px;" > 
								<input type="text" style="text-align:right" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber }" size="1" maxlength="3" class="input-text" onkeydown="return onlyNumberAllowed(event)" onchange="checkAccountLineNumber('accountLineNumber${accountLineList.id}');" />
								</display:column>
								<display:column   titleKey="accountLine.category" style="width:70px" > 
								<select name ="category${accountLineList.id}" style="width:70px" onchange="" class="list-menu"> 
								<c:forEach var="chrms" items="${category}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.category}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								 </display:column>
								<display:column title="Charge &nbsp;Code" style="width:100px;!width:135px;" >
								<input type="text" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" size="20"  maxlength="25"  class="input-text" onblur="fillDiscription('quoteDescription${accountLineList.id}','chargeCode${accountLineList.id}','category${accountLineList.id}','recGl${accountLineList.id}','payGl${accountLineList.id}','contractCurrency${accountLineList.id}','contractExchangeRate${accountLineList.id}','contractValueDate${accountLineList.id}','payableContractCurrency${accountLineList.id}','payableContractExchangeRate${accountLineList.id}','payableContractValueDate${accountLineList.id}');checkChargeCode('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')"/>
								<img class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="chk('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');"  src="<c:url value='/images/open-popup.gif'/>" />
								<input type="hidden"  name="contractCurrency${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="recGl${accountLineList.id}" value="${accountLineList.recGl}"/>
								<input type="hidden"  name="payGl${accountLineList.id}" value="${accountLineList.payGl}"/>
								<input type="hidden"  name="contractExchangeRate${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
								<input type="hidden"  name="contractValueDate${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="payableContractCurrency${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractExchangeRate${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								<input type="hidden"  name="payableContractValueDate${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="division${accountLineList.id}" value="${accountLineList.division}"/>
								<c:if test="${estVatFlag!='Y'}">
								<input type="hidden"  name="vatDecr${accountLineList.id}" value="${accountLineList.estVatDescr}"/>								
								<input type="hidden"  name="vatPers${accountLineList.id}" value="${accountLineList.estVatPercent}"/>
								<input type="hidden"  name="vatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>								
								</c:if>
								<c:if test="${contractType}">
								<input type="hidden"  name="estimatePayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimatePayableContractCurrency}"/>
								<input type="hidden"  name="estimatePayableContractValueDateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractValueDate}"/>
								<input type="hidden"  name="estimatePayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractExchangeRate}"/>
								<input type="hidden"  name="estimatePayableContractRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRate}"/>
								<input type="hidden"  name="estimatePayableContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRateAmmount}"/>
								<input type="hidden"  name="estimateContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimateContractCurrency}"/>
								<input type="hidden"  name="estimateContractValueDateNew${accountLineList.id}" value="${accountLineList.estimateContractValueDate}"/>
								<input type="hidden"  name="estimateContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimateContractExchangeRate}"/>
								<input type="hidden"  name="estimateContractRateNew${accountLineList.id}" value="${accountLineList.estimateContractRate}"/>
								<input type="hidden"  name="estimateContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimateContractRateAmmount}"/>
								<input type="hidden"  name="revisionContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionContractCurrency}"/>
								<input type="hidden"  name="revisionContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionContractValueDate}"/>
								<input type="hidden"  name="revisionContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionContractExchangeRate}"/>

								<input type="hidden"  name="revisionPayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.revisionPayableContractCurrency}"/>
								<input type="hidden"  name="revisionPayableContractValueDateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractValueDate}"/>
								<input type="hidden"  name="revisionPayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionPayableContractExchangeRate}"/>

								<input type="hidden"  name="contractCurrencyNew${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="contractValueDateNew${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="contractExchangeRateNew${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>

								<input type="hidden"  name="payableContractCurrencyNew${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractValueDateNew${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="payableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								
								</c:if>						
								<input type="hidden"  name="estCurrencyNew${accountLineList.id}" value="${accountLineList.estCurrency}"/>
								<input type="hidden"  name="estValueDateNew${accountLineList.id}" value="${accountLineList.estValueDate}"/>
								<input type="hidden"  name="estExchangeRateNew${accountLineList.id}" value="${accountLineList.estExchangeRate}"/>
								<input type="hidden"  name="estLocalRateNew${accountLineList.id}" value="${accountLineList.estLocalRate}"/>
								<input type="hidden"  name="estLocalAmountNew${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
								<input type="hidden"  name="estSellCurrencyNew${accountLineList.id}" value="${accountLineList.estSellCurrency}"/>
								<input type="hidden"  name="estSellValueDateNew${accountLineList.id}" value="${accountLineList.estSellValueDate}"/>
								<input type="hidden"  name="estSellExchangeRateNew${accountLineList.id}" value="${accountLineList.estSellExchangeRate}"/>
								<input type="hidden"  name="estSellLocalRateNew${accountLineList.id}" value="${accountLineList.estSellLocalRate}"/>
								<input type="hidden"  name="estSellLocalAmountNew${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
								<input type="hidden"  name="buyDependSellNew${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
								<input type="hidden"  name="oldEstimateSellDeviationNew${accountLineList.id}" />								
								<input type="hidden"  name="oldEstimateDeviationNew${accountLineList.id}" />


								<input type="hidden"  name="revisionCurrencyNew${accountLineList.id}" value="${accountLineList.revisionCurrency}"/>
								<input type="hidden"  name="revisionValueDateNew${accountLineList.id}" value="${accountLineList.revisionValueDate}"/>
								<input type="hidden"  name="revisionExchangeRateNew${accountLineList.id}" value="${accountLineList.revisionExchangeRate}"/>
								<input type="hidden"  name="countryNew${accountLineList.id}" value="${accountLineList.country}"/>
								<input type="hidden"  name="valueDateNew${accountLineList.id}" value="${accountLineList.valueDate}"/>
								<input type="hidden"  name="exchangeRateNew${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
								</display:column>
								<display:column   title="Vendor Code" style="width:80px;!width:100px;" > 
								<input type="text" name="vendorCode${accountLineList.id}"  value="${accountLineList.vendorCode }" size="6" class="input-text" onchange="checkVendorName('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}');fillCurrencyByChargeCode('${accountLineList.id}');"/>
								<img  class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="javascript:winOpenForActgCode('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');" src="<c:url value='/images/open-popup.gif'/>" />
								</display:column>
								<display:column  title="Vendor &nbsp;Name" style="width:50px" >
								<input type="text" name="estimateVendorName${accountLineList.id}"  value="${accountLineList.estimateVendorName }" size="15" class="input-text" /> 
								</display:column>
								<display:column   title="Basis"  style="width:70px;!width:85px;"> 
								<select name ="basis${accountLineList.id}" style="width:50px" class="list-menu" onchange="changeBasis('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}')"> 
								<c:forEach var="chrms" items="${basis}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.value == accountLineList.basis}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								<input type="hidden"  name="oldbasis${accountLineList.id}" value="${accountLineList.basis}"/><img id="rateImage"  class="openpopup" width="17" height="20" align="top" src="${pageContext.request.contextPath}/images/image.jpg" onclick="findRevisedEstimateQuantitys('chargeCode${accountLineList.id}','category${accountLineList.id}','estimateExpense${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','deviation${accountLineList.id}','estimateDeviation${accountLineList.id}','estimateSellDeviation${accountLineList.id}','${accountLineList.id}');"/>
								<input type="hidden" name ="deviation${accountLineList.id}"  value="${accountLineList.deviation}" />
								<input type="hidden" name ="estimateDeviation${accountLineList.id}"  value="${accountLineList.estimateDeviation}" />
								<input type="hidden" name ="estimateSellDeviation${accountLineList.id}"  value="${accountLineList.estimateSellDeviation}" /> 		               		               
								 
								 </display:column>
								 <display:column title="Quantity"  style="width:18px" >
								<input type="text" style="text-align:right" name="estimateQuantity${accountLineList.id}"  value="${accountLineList.estimateQuantity }" size="4" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')"/>
								<input type="hidden"  name="oldestimateQuantity${accountLineList.id}" value="${accountLineList.estimateQuantity }"/>
								</display:column>
								
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateRate${accountLineList.id}" value="${accountLineList.estimateRate }" size="5" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat(this)" onchange="calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}')"/>
								</display:column>  
								<display:column title="FX" headerClass="RemoveBorder" style="width:20px; border-left:medium hidden;!border-left-style:none;">
									<c:choose>
			                        <c:when test="${accountLineList.estLocalAmount>0  }">
			                           
			                           <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column> 
								
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateSellRate${accountLineList.id}" value="${accountLineList.estimateSellRate }" size="5" class="input-text" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="return checkFloat(this)" onchange="clearDecimal();calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')"/>
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estSellLocalAmount>0  }">			                           
			                           <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="14" height="14" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>					
								</c:if>			
								<display:column title="Expense"  style="width:20px" >
								<input type="text" style="text-align:right" name="estimateExpense${accountLineList.id}" value="${accountLineList.estimateExpense }" readonly="readonly" size="6" class="input-textUpper"/>
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								<input type="text" style="text-align:right" name="estimatePassPercentage${accountLineList.id}" value="${accountLineList.estimatePassPercentage }" maxlength="4"  size="2" class="input-text" onkeydown="return onlyNumsAllowedPersent(event)"  onchange="changeEstimatePassPercentage('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}');calculateRevenueNew('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}')" />
								</display:column> 
								<display:column title="Revenue" style="width:20px" >
								<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper"/>
								</display:column>

								<c:if test="${estVatFlag=='Y'}">
								<display:column   title="VAT Desc"  style="width:50px"> 
								<select name ="vatDecr${accountLineList.id}" style="width:75px" class="list-menu" onchange="changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}');">
								<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.estVatDescr}">
	                                  <c:set var="selectedInd" value="selected"></c:set>	                                  
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach>                                 
								</select>
								<input type="hidden"  name="vatPers${accountLineList.id}" value="${accountLineList.estVatPercent}"/>
								</display:column>
								<display:column   title="VAT Amt"  style="width:50px">
								<input type="text" name ="vatAmt${accountLineList.id}"  value="${accountLineList.estVatAmt}" style="width:50px" class="input-textUpper" readonly="true"> 
								</display:column>
								</c:if>
								
	                            <display:column title="Description" style="width:100px" >
								<textarea  name="quoteDescription${accountLineList.id}"  id="quoteDescription${accountLineList.id}"   style="overflow:auto;" class="input-text" onselect="discriptionView('quoteDescription${accountLineList.id}')" onblur="discriptionView1('quoteDescription${accountLineList.id}')" value ="${accountLineList.quoteDescription }"  cols="15" rows="1">${accountLineList.quoteDescription }</textarea>
								</display:column> 
								<display:column style="width:85px;padding:0px;text-align:center;"
								 title="Display&nbsp;On&nbsp;Quote <input type='checkbox' style='margin-left:54px; vertical-align:middle;' name='selectall' onclick='selectAll(this);'/>All" media="html" >  
                                 <c:if test="${accountLineList.displayOnQuote}"> 
                                  <input type="checkbox" style="text-align:center;" name="displayOnQuote${accountLineList.id}" id="displayOnQuote${accountLineList.id}" checked="checked"  value="${accountLineList.status}" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.displayOnQuote==false}">
                                 <input type="checkbox" style="text-align:center;" name="displayOnQuote${accountLineList.id}" id="displayOnQuote${accountLineList.id}" value="${accountLineList.status}" /> 
                                 </c:if>
                                 </display:column>
    							<configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">	
								<display:column title="Additional Services <input type='checkbox' style='margin-left:29px; vertical-align:middle;' name='selectall' onclick='selectAdd(this);'/>All" media="html"    style="width:50px;padding:0px;text-align:center;" > 
                                 <c:if test="${accountLineList.additionalService}"> 
                                  <input type="checkbox"  name="additionalService${accountLineList.id}"  id="additionalService${accountLineList.id}" checked="checked"  value="${accountLineList.status}" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.additionalService==false}">
                                 <input type="checkbox"  name="additionalService${accountLineList.id}"  id="additionalService${accountLineList.id}"  value="${accountLineList.status}" /> 
                                 </c:if>
                                 </display:column>
								</configByCorp:fieldVisibility>

	                            <display:column style="width:65px;padding:0px;text-align:center;"  title="<font style='padding-left:30px;'>Act </font><br><input type='checkbox' style='margin-left:26px; vertical-align:middle;' checked='checked' id='selectallActive' name='selectallActive' onclick='selectAllActive(this);'/>All"  media="html">
				                 <c:if test="${accountLineList.status}"> 
				                 <input type="checkbox"  name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}" checked="checked"  value="${accountLineList.status}" onchange="inactiveStatusCheck(${accountLineList.id},this)"/> 
				                 </c:if>
				               </display:column>	                            
		
                               <display:footer>
                                 <tr> 
				 	  	          <td align="right" colspan="11"><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
				 	  	        <c:if test="${multiCurrency=='Y'}">
		  	                    <td align="left" ></td>
		  	                    </c:if>
						          <td align="left" width="40px">
						          <input type="text" style="text-align:right" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper"/>
						          </td>
						          <td align="left"> 
						  	      </td>
						  	      <td align="left" width="70px">
						  	      <input type="text" style="text-align:right" name="estimatedTotalRevenue${serviceOrder.id}" value="${serviceOrder.estimatedTotalRevenue}" readonly="readonly" size="6" class="input-textUpper"/>
						  	      </td>
						  	      <c:if test="${estVatFlag=='Y'}">
		  	                     <td align="left" colspan="2" ></td>
		  	                     </c:if> 
		  	                    <td></td>
		  	                    <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                     <td></td>	
		  	                    </configByCorp:fieldVisibility>
		  	                    <td colspan="2" align="right"><input type="button" class="cssbuttonA" style="width:60px; height:25px; margin-top:-4px;"  name="Inactivate"  value="Inactivate" onclick="updateAccInactive();"/></td>
		  	                    
		  	                    </tr>
		  	                    <tr>
		  	                     <td align="right" colspan="11"><b><div align="right">Gross&nbsp;Margin</div></b></td>
		  	       	  	         <c:if test="${multiCurrency=='Y'}">
		  	                    <td align="left" ></td>
		  	                    </c:if>
						          <td align="left" width="40px">
						          </td>
						          </td>
						  	      <td align="left"> 
						          <td align="left" width="70px" >
						  	      <input type="text" style="text-align:right" name="estimatedGrossMargin${serviceOrder.id}" value="${serviceOrder.estimatedGrossMargin}" readonly="readonly" size="6" class="input-textUpper"/>
						  	      </td>
						  	      <c:if test="${estVatFlag=='Y'}">
		  	                     <td align="left" colspan="2" ></td>
		  	                     </c:if>  
		  	                    <td colspan="3"></td>
		  	                     <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                     <td></td>	
		  	                    </configByCorp:fieldVisibility>
		  	                    </tr>
		  	                    <tr>
		  	                     <td align="right" colspan="11"><b><div align="right">Gross&nbsp;Margin%</div></b></td>
		  	       	  	         <c:if test="${multiCurrency=='Y'}">
		  	                    <td align="left" ></td>
		  	                    </c:if>
						          <td align="left" width="40px">
						          </td>
						          </td>
						  	      <td align="left"> 
						          <td align="left" width="40px">
						  	      <input type="text" style="text-align:right" name="estimatedGrossMarginPercentage${serviceOrder.id}" value="${serviceOrder.estimatedGrossMarginPercentage}" readonly="readonly" size="6" class="input-textUpper"/>
						  	      
						  	      </td> 
						  	      <c:if test="${estVatFlag=='Y'}">
		  	                     <td align="left" colspan="2" ></td>
		  	                     </c:if> 
		  	                    <td colspan="3"></td>
		  	                     <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                     <td></td>	
		  	                    </configByCorp:fieldVisibility>
		  	                    </tr>
		  	                    
                             </display:footer> 
                          </display:table> 
						</td></tr></table>
						
						
						</div>
						<table>
						<tr>
						<td>
						<input type="button" class="cssbuttonA" style="width:65px; height:25px" name="pricingAdd" value="Add Line"  onclick="addPricingLine();" /> 
						<input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="savePricingLine();"/> 
						<c:if test="${serviceOrder.defaultAccountLineStatus=='false'}">
                         <input type="button" class="cssbuttonA" style="width:140px; height:25px" onclick="findDefaultLine();" 	value="Add Default Template"/>
	                   </c:if>
	                   <c:if test="${serviceOrder.job == 'OFF'}">
	                   		<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
                        	 	<input type="button" class="cssbutton1" onclick="createPrice()" value="Create Pricing" style="width:120px; height:25px; margin-left:5px;margin-bottom:5px;" />
                         	</configByCorp:fieldVisibility>
                       </c:if>
	                   <c:if test="${serviceOrder.defaultAccountLineStatus=='true'}">
                         <input type="button" class="cssbuttonA" style="width:140px; height:25px" onclick="defaultLineMassage()" value="Add Default Template" />
	                   </c:if>
						</td>
						</tr></table> 
		                    </div></div></td></tr> --%>
		                    
   <tr>
	<td   width="100%" align="left" style="margin:0px">
	<div id="estimatePricingShow">
	<div onClick="javascript:animatedcollapse.toggle('estpricing')" style="margin:0px">
     <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">

<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;Estimate&nbsp;Pricing
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>

</table>
     </div>
		<div id="estpricing" class="switchgroup1">
		
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
 		  <tr>
 		  <td colspan="5">
 		  <div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" >
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;text-align:center;font-size: 1em;font-weight: bold;">
<b><div align="right">Total Estimate Amount:&nbsp;$${miscellaneous.estimatedRevenue}</div></b>
</td>
</tr>
</table>
</div>
</td>
</tr>
<tr><td align="left" colspan="10">

<div>
<div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" width="530">
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">TRANSPORTATION</td>
</tr>
<tr>
<td>
<s:set name="underLyingHaulList" value="underLyingHaulList" scope="request"/>
<c:if test="${underLyingHaulList!='[]'}"> 
	<display:table name="underLyingHaulList" class="table" requestURI="" id="underLyingHaulList"  defaultsort="1" defaultorder="descending" pagesize="10">
    <display:column property="description"  title="Description" />
   	<display:column title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${underLyingHaulList.rate}" /></div></display:column>
   	<display:column  title="Discount">
  	 <c:if test="${underLyingHaulList.discount!=''}">
   	<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${underLyingHaulList.discount}" />%</div>
             </c:if>
             </display:column>
   	<display:column title="Charge" style="width:10%;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${underLyingHaulList.charge}" /></div></display:column>
   	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="3"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${transportSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${transportSum}" /></div></c:if></td>             
      </tr>
	</display:footer>
	</display:table>
</c:if>
	
<c:if test="${underLyingHaulList=='[]'}"> 
   <display:table name="underLyingHaulList" class="table" requestURI="" id="underLyingHaulList"  defaultsort="1" defaultorder="descending" pagesize="10">
   <display:column  title="Description" />
   <display:column title="Rate"/>
   <display:column  title="Discount"/>
   <display:column title="Charge" />
   <display:footer>
	 <tr> 
 	  	 <td align="right" colspan="3"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${transportSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${transportSum}" /></div></c:if></td>             
     </tr>
   </display:footer>
	</display:table>
</c:if>
	</td>
	</tr>
	</table>
	</div>

<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
<thead>
<tr>
<th colspan="3" class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">PACKING</th>
</tr>
</thead>
<tr>
<td>
<div style="float:left;">
<div style="float:left;">
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" >
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">

<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;width:100%;text-align:right;padding-right:8%;">
<tr>
<td style="padding-left:32%;">Container</td>
<td style="padding-left:28%;">Packing</td>
<td style="padding-left:15%;">Unpacking</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<s:set name="packingList" value="packingList" scope="request"/>
<c:if test="${packingList!='[]'}"> 
<display:table name="packingList" class="table" requestURI="" id="packingList"  pagesize="10" >
	<display:column property="mainDescr"  title="Description" />
	<display:column title="Qty" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctQty}" /></div></display:column>
   <display:column title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctRate}" /></div></display:column>
             <c:if test="${packingList.ctDisc!=''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctDisc}" />%</div></display:column></c:if>
             <c:if test="${packingList.ctDisc==''}">
             <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctDisc}" /></div></display:column>
             </c:if>
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;">   
	<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.ctCharge}" /></div></display:column>
	<display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkQty}" /></div></display:column>
   <display:column title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkRate}" /></div></display:column>
   <c:if test="${packingList.pkDisc!=''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkDisc}" />%</div></display:column></c:if>
             <c:if test="${packingList.pkDisc==''}">
             <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkDisc}" /></div></display:column>
             </c:if>
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;">
      <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.pkCharge}" /></div></display:column>
	<display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upQty}" /></div></display:column>
   <display:column  title="Rate"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upRate}" /></div></display:column>
             <c:if test="${packingList.upDisc==''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upDisc}" /></div></display:column>
        </c:if>
        <c:if test="${packingList.upDisc!=''}">
   <display:column title="Discount"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upDisc}" />%</div></display:column>
        </c:if>     
   <display:column title="Charge"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packingList.upCharge}" /></div></display:column>
   <display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${packContSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packContSum}" /></div></c:if></td>
             <td colspan="3"></td>
 	  	 <td align="right">
 	  	 <c:if test="${packingSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packingSum}" /></div></c:if></td>
 	  	  	    <td colspan="3"></td><td align="right">
 	  	  	    <c:if test="${packUnpackSum!='None'}">
 	  	  	    <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packUnpackSum}" /></div>
                  </c:if>
                  </td>
      </tr>
	</display:footer>	
	</display:table>
</c:if>
<c:if test="${packingList=='[]'}"> 
<display:table name="packingList" class="table" requestURI="" id="packingList"  pagesize="10" >
	<display:column title="Description" />
	<display:column title="Qty" />
   	<display:column title="Rate" />
   	<display:column title="Discount" />
   	<display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;" />   
	<display:column title="Qty" />
   <display:column title="Rate" />
   <display:column title="Discount" />
   <display:column title="Charge" style="border-right:2px solid #74B3DC;padding:0px;"/>
	<display:column title="Qty"/>
   <display:column  title="Rate"/>
   <display:column title="Discount"/>   
   <display:column title="Charge"/>
   <display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right"><c:if test="${packContSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${packContSum}" /></div></c:if></td>
             <td colspan="3"></td>
 	  	 <td align="right">
 	  	 <c:if test="${packingSum!='None'}"><div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packingSum}" /></div></c:if></td>
 	  	  	    <td colspan="3"></td><td align="right">
 	  	  	    <c:if test="${packUnpackSum!='None'}">
 	  	  	    <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${packUnpackSum}" /></div>
                  </c:if>
                  </td>
      </tr>
	</display:footer>	
	</display:table>
</c:if>
	</td>
	</tr>
	</table>
	</div>
	
	</div>
	</td>
	
	</tr>
	</table>
	
	<div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" width="530" >
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">OTHER</td>
</tr>
<tr>
<td>
<s:set name="otherMiscellaneousList" value="otherMiscellaneousList" scope="request"/>
<c:if test="${otherMiscellaneousList!='[]'}">
<display:table name="otherMiscellaneousList" class="table" requestURI="" id="otherMiscellaneousList"  pagesize="10">
    <display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.qty}" /></div></display:column>
    <display:column property="rateType"  title="Rate Type" />
   <display:column property="description"  title="Description" />
   <display:column title="Rate" >
   <c:if test="${otherMiscellaneousList.qty!=''}">
   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.rate}" /></div></c:if>
             <c:if test="${otherMiscellaneousList.qty==''}">
   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.charge}" /></div></c:if>
             </display:column>
   <display:column title="Discount" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.discount}" />%</div></display:column>
   <display:column title="Charge" style="width:10%;"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${otherMiscellaneousList.charge}" /></div></display:column>   
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="5"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${othersSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${othersSum}" /></div>
             </c:if></td>             
      </tr>
	</display:footer>
    
	</display:table>
</c:if>
<c:if test="${otherMiscellaneousList=='[]'}">
<display:table name="otherMiscellaneousList" class="table" requestURI="" id="otherMiscellaneousList"  defaultsort="1" defaultorder="descending" pagesize="10">
    <display:column title="Qty"/>
    <display:column title="Rate Type" />
   <display:column title="Description" />
   <display:column title="Rate" />
   <display:column title="Discount" />
   <display:column title="Charge" style="width:10%;" />
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="5"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${othersSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${othersSum}" /></div>
             </c:if></td>             
      </tr>
	</display:footer>
    
	</display:table>
</c:if>
	</td>
	</tr>
	</table>	
	</div>
	
		<div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;"  width="530" >
<tr>
<td class="Estheading-bg" style="margin:0px;padding:0.5em;border: 2px solid #74B3DC;border-bottom:none;text-align:center;font-size: 1em;font-weight: bold;">VALUATION</td>
</tr>
<tr>
<td>
<s:set name="valuationList" value="valuationList" scope="request"/>
<c:if test="${valuationList!='[]'}">
<display:table name="valuationList" class="table" requestURI="" id="valuationList"  defaultsort="1" defaultorder="descending"  pagesize="10">
    <display:column title="Qty"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.qty}" /></div></display:column>
   <display:column property="description"  title="Description" />
   <display:column title="Rate" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.rate}" /></div></display:column>
   <display:column title="Discount" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.discount}" />%</div></display:column>
   <display:column title="Charge" ><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationList.charge}" /></div></display:column>   
	
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${valuationSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationSum}" /></div></c:if></td>             
      </tr>
	</display:footer>	
	</display:table>
</c:if>
<c:if test="${valuationList=='[]'}">
<display:table name="valuationList" class="table" requestURI="" id="valuationList"  defaultsort="1" defaultorder="descending"  pagesize="10">
    <display:column title="Qty"/>
   <display:column title="Description" />
   <display:column title="Rate" />
   <display:column title="Discount" />
   <display:column title="Charge" />	
	<display:footer>
	<tr> 
 	  	 <td align="right" colspan="4"><b><div align="right">Total:</div></b></td>
		 <td align="right">
		 <c:if test="${valuationSum!='None'}">
		 <div align="right">$<fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${valuationSum}" /></div></c:if></td>             
      </tr>
	</display:footer>	
	</display:table>
</c:if>
	</td>
	</tr>      
    
	</table>	
	</div>	
</div>
</td></tr></table></div></div></td></tr> 
</sec-auth:authComponent>


			<!-- &nbsp;Inclusions and Exclusions  START-->
		<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices!='N/A'}">
				<tr>
				<td width="100%" align="left" style="margin:0px">
					<div onClick="javascript:divtoggle('incexc');" style="margin:0px">
						<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
							<tr>
								<td class="headtab_left"></td>
								<td NOWRAP class="headtab_center">&nbsp;Inclusions and Exclusions</td>
								<td width="28" valign="top" class="headtab_bg"></td>
								<td class="headtab_bg_center">&nbsp;</td>
								<td class="headtab_right"></td>
							</tr>
						</table>
					</div>
					<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px" id="langS">
					<tr>
							<td class="listwhitetext" align="left" colspan="4">Language
							<c:if test="${not empty serviceOrder.id}">
						    <s:select list="%{languageList}" name="serviceOrder.incExcServiceTypeLanguage" cssStyle="width:75px;height:19px;" cssClass="list-menu" onchange="" tabindex=""/>
						    </c:if>
						    <c:if test="${empty serviceOrder.id}">
						    <s:select list="%{languageList}" name="serviceOrder.incExcServiceTypeLanguage" cssStyle="width:75px;height:19px;" cssClass="list-menu" onchange="populateInclusionExclusionData('default','1','F');" tabindex=""/>
						    </c:if>
				    </td>
					<td><input type="button" id="resetServices" name="resetServices" onclick="getConfirmationMessage();" class="cssbuttonA" style="width: 100px; height: 20px; margin: 2px;" value="Reset Services" tabindex="" /></td>
							</tr>
					</table>
					 
					<div style="text-align: center; display: none; left: 25%; margin-top: 5%;" id="loader"><img src="${pageContext.request.contextPath}/images/ajax-loader.gif" /></div>
					<div id="incexc">
					</div> 
				</td>
			</tr>	
		</c:if>		
			<!-- &nbsp;Inclusions and Exclusions  END-->
		                    
		                  </tbody>
						</table>
</div>

<div class="bottom-header"><span></span></div>
</div>
</div>
<script type="text/javascript">


try{
	<c:if test="${quotesToValidate!='QTG'}"> 
	document.getElementById("pricingShow").style.display="block"; 
	document.getElementById("estimatePricingShow").style.display="none";  
	</c:if>
	<c:if test="${quotesToValidate=='QTG'}"> 
	document.getElementById("pricingShow").style.display="none";  
	document.getElementById("estimatePricingShow").style.display="block";  
	</c:if>
	}
	catch(e){}
	try{
		checkUnitValidation();
	}
	catch(e){}	
	</script>
	<script type="text/javascript">
	function getHTTPObjectSO23()
	{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}
	function getConfirmationMessage(){
		var agree = confirm("Click 'OK' to reset the service list or click 'Cancel' to continue using the current list.");
	    if(agree){
	      populateInclusionExclusionData('noDefault','1','T');
	  }
	}
	var httpMergeSO23 = getHTTPObjectSO23();
	var httpMergeSO24 = getHTTPObjectSO23();
	function populateInclusionExclusionData(target,val,flag){
		var incExcype = document.forms['serviceOrderForm'].elements['userQuoteServices'].value;
		 var copyWithoutPricingFlag45="${copyWithoutPricingFlag}";
			<c:if test="${empty serviceOrder.id}">
			 if((copyWithoutPricingFlag45 != undefined)&&(copyWithoutPricingFlag45 != null)&&(copyWithoutPricingFlag45 != '')&&(incExcype.trim()=='Separate section')){
			 }else{
				target="noDefault";
			 }
			</c:if>
		if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){ 
			 	document.getElementById("loader").style.display = "block";
			 	var job=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
			 	var mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
			 	var routing=document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
			 	var language=document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceTypeLanguage'].value;
			 	var companyDivision=document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
			 	var commodity=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
			 	var packingMode=document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
			 	var originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
			 	var destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
			 	if((job==null)||(job==undefined)||(job.trim()=="")){
			 		job="";
			 	}
			 	if((mode==null)||(mode==undefined)||(mode.trim()=="")){
			 		mode="";
			 	}
			 	if((routing==null)||(routing==undefined)||(routing.trim()=="")){
			 		routing="";
			 	}
			 	if((companyDivision==null)||(companyDivision==undefined)||(companyDivision.trim()=="")){
			 		companyDivision="";
			 	}
			 	if((commodity==null)||(commodity==undefined)||(commodity.trim()=="")){
			 		commodity="";
			 	}
			 	if((packingMode==null)||(packingMode==undefined)||(packingMode.trim()=="")){
			 		packingMode="";
			 	}
			 	if((originCountry==null)||(originCountry==undefined)||(originCountry.trim()=="")){
			 		originCountry="";
			 	}
			 	if((destinationCountry==null)||(destinationCountry==undefined)||(destinationCountry.trim()=="")){
			 		destinationCountry="";
			 	}
			    var id="${serviceOrder.id}";
			    if((language==null)||(language==undefined)||(language.trim()=="")){
			     if((target=='default')&&(id==null)){
			      language="en";
			     }else{
			      language="";
			     }  
			    }
			    var url="inclusionExlusionQOlist.html?ajax=1&decorator=simple&popup=true&jobIncExc="+job+"&modeIncExc="+mode+"&routingIncExc="+routing+"&id="+id+"&languageIncExc="+language+"&companyDivisionIncExc="+companyDivision+"&commodityIncExc="+commodity+"&packingModeIncExc="+packingMode+"&originCountryIncExc="+originCountry+"&destinationCountryIncExc="+destinationCountry+"&checkDefaultIncExc="+target+"&userQuoteServices="+encodeURIComponent(incExcype)+"&incExcResetCheck="+encodeURIComponent(flag);		    
			 	httpMergeSO23.open("GET", url, true);
			 	httpMergeSO23.onreadystatechange =  function(){ handleHttpResponseMergeSoList23(target,incExcype,val);};
			 	httpMergeSO23.send(null);	
		}
	}
	function handleHttpResponseMergeSoList23(target,incExcype,val){
		if (httpMergeSO23.readyState == 4){
		    var results=httpMergeSO23.responseText; 
		    var soDiv = document.getElementById("incexc"); 
		    if(val=='0'){  
			    soDiv.style.display = 'none';
					document.getElementById("langS").style.display="none";
			    }
		    document.getElementById("loader").style.display = "none";
		    soDiv.innerHTML = results;
		    <c:if test="${empty serviceOrder.id}">
		    if(target=='default'){
		    	var copyWithoutPricingFlag="${copyWithoutPricingFlag}";
		    	if(copyWithoutPricingFlag==''){
		   	 		 document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceTypeLanguage'].value='en';	  	  
		    	}	  
		    }else{
		    	if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Separate section'){ 
		    		pickCheckBoxData1(target);
		    	}else{
		    		pickCheckBoxDataSec1(target);
		    	}
		    }
		    </c:if>
		    <c:if test="${not empty serviceOrder.id}">
		    if(target=='default'){
		    }else{
		    	if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Separate section'){ 
		    		pickCheckBoxData1(target);
		    	}else{
		    		pickCheckBoxDataSec1(target);
		    	}
		    }
		    </c:if>	    
		    }
	}
	function pickCheckBoxDataSec1(target){  
		if(target!='default'){
		var payingStatusListServer="";
	    if(document.getElementsByName("checkVC")!=undefined){
	    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
	 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){	        	           
	 	             if(payingStatusListServer==''){
	 	 	             if(document.getElementsByName("checkVC")[i].checked==true){
	 	                   payingStatusListServer=document.getElementsByName("checkVC")[i].id;
	 	 	             }
	 	                 }else{
	 	                	if(document.getElementsByName("checkVC")[i].checked==true){
	 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("checkVC")[i].id;
	 	                	}
	 	                   }
	 	       }	
	 	     }else{	      
	 	    	if(document.getElementsByName("checkVC").checked==true){ 
	 	           payingStatusListServer=document.getElementsByName("checkVC").id;
	 	    	}
	 	         }	        
	 	     }
	    document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value =payingStatusListServer;
		}

	}

	function pickCheckBoxData1(target){  
		if(target!='default'){
		var payingStatusListServer="";
	    if(document.getElementsByName("checkVC")!=undefined){
	    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
	 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){	        	           
	 	             if(payingStatusListServer==''){
	 	 	             if(document.getElementsByName("checkVC")[i].checked==true){
	 	                   payingStatusListServer=document.getElementsByName("checkVC")[i].id+":"+document.getElementsByName("checkVC")[i].value;
	 	 	             }
	 	                 }else{
	 	                	if(document.getElementsByName("checkVC")[i].checked==true){
	 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("checkVC")[i].id+":"+document.getElementsByName("checkVC")[i].value;
	 	                	}
	 	                   }
	 	       }	
	 	     }else{	      
	 	    	if(document.getElementsByName("checkVC").checked==true){ 
	 	           payingStatusListServer=document.getElementsByName("checkVC").id+":"+document.getElementsByName("checkVC").value;
	 	    	}
	 	         }	        
	 	     }
	    document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value =payingStatusListServer;
		}

	}
	function checkAllInxExc(target,type){
		try{
			if(target.checked==true){
			    if(document.getElementsByName("checkVC")!=undefined){
			    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
			 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){
				 	      if(document.getElementsByName("checkVC")[i].value==type){
			 	    	 document.getElementsByName("checkVC")[i].checked=true;	
				 	      }        	           
		 		       }	
		 	     	}else{
		 	     		if(document.getElementsByName("checkVC").value==type){
		 	    		document.getElementsByName("checkVC").checked==true;
		 	     		}		      
		 	         }	        
		 	    }
			}else{
			    if(document.getElementsByName("checkVC")!=undefined){
			    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
			 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){
			 	    	 if(document.getElementsByName("checkVC")[i].value==type){
			 	    	 document.getElementsByName("checkVC")[i].checked=false;	  
			 	    	 }      	           
		 		       }	
		 	     	}else{
		 	     		if(document.getElementsByName("checkVC").value==type){
		 	    		document.getElementsByName("checkVC").checked==false;
		 	     		}		      
		 	         }	        
		 	    }			
			}
		}catch(e){}
		pickCheckBoxData(type);
	}	
	function checkAllINCEXCFlag(type){
		  var checkAllF='T';
			    if(document.getElementsByName("checkVC")!=undefined){
			    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
			 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){
				 	      if(document.getElementsByName("checkVC")[i].value==type && document.getElementsByName("checkVC")[i].checked==false && checkAllF=='T'){
				 	    	 checkAllF='F';
				 	      }        	           
		 		       }	
		 	     	}else{
		 	     		if(document.getElementsByName("checkVC").value==type && document.getElementsByName("checkVC").checked==false && checkAllF=='T'){
		 	     			checkAllF='F';
		 	     		}		      
		 	         }	        
		 	    }

		  if(checkAllF=='T'){
			  document.getElementById(type+'All').checked=true;
		  }else{
			  document.getElementById(type+'All').checked=false;
		  }	
		var incCount=0;
		var excCount=0;
	    if(document.getElementsByName("checkVC")!=undefined){
	    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
	 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){
		 	      if(document.getElementsByName("checkVC")[i].value=='INC'){
		 	    	 incCount++;
		 	      }else{
		 	    	 excCount++;
		 	      }        	           
		       }	
	     	}else{
	     		if(document.getElementsByName("checkVC").value=='INC'){
	     			incCount++;
	     		}else{
		 	    	excCount++;
		 	    }		      
	        }	        
	    }
	  if(incCount<1){
		  document.getElementById('INCAll').checked=false;
	  } 
	  if(excCount<1){
		  document.getElementById('EXCAll').checked=false;
	  } 	  
}
	
	function pickCheckBoxData(type){
		try{
		var payingStatusListServer="";
	    if(document.getElementsByName("checkVC")!=undefined){
	    	
	    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
	 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){	        	           
	 	             if(payingStatusListServer==''){
	 	 	             if(document.getElementsByName("checkVC")[i].checked==true){
	 	                   payingStatusListServer=document.getElementsByName("checkVC")[i].id+":"+document.getElementsByName("checkVC")[i].value;
	 	 	             }
	 	                 }else{
	 	                	if(document.getElementsByName("checkVC")[i].checked==true){
	 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("checkVC")[i].id+":"+document.getElementsByName("checkVC")[i].value;
	 	                	}
	 	                   }
	 	       }	
	 	     }else{	      
	 	    	if(document.getElementsByName("checkVC").checked==true){ 
	 	           payingStatusListServer=document.getElementsByName("checkVC").id+":"+document.getElementsByName("checkVC").value;
	 	    	}
	 	         }	        
	 	     }
	    document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value =payingStatusListServer;
	    checkAllINCEXCFlag(type);
	    autoSaveIncusionExclusion();
		}catch(e){}

	}
	function pickCheckBoxDataSec(){
		try{
		var payingStatusListServer="";
	    if(document.getElementsByName("checkVC")!=undefined){
	    	if((document.getElementsByName("checkVC").size!=0)&&(document.getElementsByName("checkVC").length!=undefined)){
	 	      for (i=0; i<document.getElementsByName("checkVC").length; i++){	        	           
	 	             if(payingStatusListServer==''){
	 	 	             if(document.getElementsByName("checkVC")[i].checked==true){
	 	                   payingStatusListServer=document.getElementsByName("checkVC")[i].id;
	 	 	             }
	 	                 }else{
	 	                	if(document.getElementsByName("checkVC")[i].checked==true){
	 	                  payingStatusListServer= payingStatusListServer+"#"+document.getElementsByName("checkVC")[i].id;
	 	                	}
	 	                   }
	 	       }	
	 	     }else{	      
	 	    	if(document.getElementsByName("checkVC").checked==true){ 
	 	           payingStatusListServer=document.getElementsByName("checkVC").id;
	 	    	}
	 	         }	        
	 	     }
	    document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value =payingStatusListServer;
	    autoSaveIncusionExclusion();
		}catch(e){}
	}
	function autoSaveIncusionExclusion(){
		 <c:if test="${not empty serviceOrder.id}">
			var incExcServiceType=document.forms['serviceOrderForm'].elements['serviceOrder.incExcServiceType'].value;
			document.forms['serviceOrderForm'].elements['servIncExcDefault'].value=incExcServiceType;
			var sid="${serviceOrder.id}";
			var url="autoSaveIncusionExclusionAjax.html?ajax=1&decorator=simple&popup=true&incExcServiceType="+encodeURIComponent(incExcServiceType)+"&sid="+sid;		    
		 	httpMergeSO24.open("GET", url, true);
		 	httpMergeSO24.onreadystatechange = handleHttpResponseMergeSoList24;
		 	httpMergeSO24.send(null);
		 </c:if>
	}
	function handleHttpResponseMergeSoList24(){
		if (httpMergeSO24.readyState == 4){
		    var results=httpMergeSO24.responseText; 
		}
	}		
	function divtoggle(divid)
	{
		if(togle=='0'){
			togle='1';
		document.getElementById(divid).style.display= "block";
		document.getElementById("langS").style.display="block";
		checkAllINCEXCFlag('INC');
		checkAllINCEXCFlag('EXC');				
		}else{
			togle='0';
			document.getElementById(divid).style.display= "none";
			document.getElementById("langS").style.display="none";
			
		}

	}


	var togle="0";	
	populateInclusionExclusionData('default','0','F');	
	</script>	