package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.model.Role;
import org.appfuse.service.GenericManager;

public interface RoleCorpManagementManager extends GenericManager<Role, Long> {
	public List<String> onlyRoles(String corpIDList);
	public List<Role> corpRoles(String corpIdList);
	public Role fetchRoleDetail(Long id,String corpID);
	public List<Role> searchRole(String name,String corpID,String category);
	public boolean checkRoleForExistance(String name, String corpID, String category);
}
