package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.RoleBasedComponentPermission;

public interface RoleBasedComponentPermissionDao extends GenericDao<RoleBasedComponentPermission, Long>{

	public List findMaximumId();
	public List searchcompanyPermission(String componentId, String description,String role);
	public List getRole(String corpID);
	public boolean isAccessAllowed(String componentId, String roles);
	public int getPermission(String componentId, String roles);
	public String getUserRoleInTransaction(String transId,String partnerIdList);
	public List getModulePermissions(String[] tableList, String roles);
	public String getPartnerIdList(String userName);
	public void updateCurrentRolePermission(String corpId);
}