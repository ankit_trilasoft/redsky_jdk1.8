package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.SurveyResponseDao;
import com.trilasoft.app.model.SurveyResponse;

public class SurveyResponseDaoHibernate extends GenericDaoHibernate<SurveyResponse, Long> implements SurveyResponseDao{
public SurveyResponseDaoHibernate()
{
	super(SurveyResponse.class);
}
}
