ALTER TABLE `redsky`.`container` ADD COLUMN `vgmCutoff` DATETIME NULL  AFTER `status` ;

ALTER TABLE `redsky`.`dspdetails`
ADD COLUMN `WOP_leaseExNeeded` VARCHAR(3) AFTER `APU_carColor`,
ADD COLUMN `REP_leaseExNeeded` VARCHAR(3) AFTER `WOP_leaseExNeeded`;

ALTER TABLE `redsky`.`claim` 
ADD COLUMN `claimantCountry` VARCHAR(3) , 
ADD COLUMN `claimantCity` VARCHAR(100) ;

trigger-trigger_add_history_dspdetails  will be update on prod 
find file path - Sql> dspdetails_Trigger.sql

trigger- trigger_add_history_trackingstatus will be update on prod
find file path - Sql> trackingstatus_Trigger.sql

trigger- trigger_add_history_refjobtype will be update on prod
find file path - Sql> refjobtype_Trigger.sql

trigger- trigger_add_history_claim will be update on prod
find file path - Sql> trigger trigger_add_history_claim.sql



ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `passwordPolicyForExternalUser` bit(1) DEFAULT b'0' ;

insert into dspnetworkfield(corpID,createdBy,updatedBy,createdOn,updatedOn,tableName,fieldName,type)
select 'TSFT','Backend_Upd','Backend_Upd',now(),now(),'dspdetails','WOP_leaseExNeeded','WOP'
union
select 'TSFT','Backend_Upd','Backend_Upd',now(),now(),'dspdetails','REP_leaseExNeeded','REP';