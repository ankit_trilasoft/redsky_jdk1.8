package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="workticketinventorydata")
public class WorkTicketInventoryData  {
	private Long id;
	private String item;
	private String itemDesc;
	private BigDecimal  weight = new BigDecimal("0.00");
	private BigDecimal volume = new BigDecimal("0.00");
	private Integer quantity = new Integer(1);
	private String corpId;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private Long  locationId;
	private String barCode;
	private String shipNumber;
	private Long  oItemRoomId;
	private String oItemCondition;
	private String oItemComment;
	private Long oItemImageId;
	private String oPacker;
	private Long  dItemRoomId;
	private String dItemCondition;
	private String dItemComment;
	private Long dItemImageId;
	private String dPacker;
	private String delivered;
	private String cartonContent;
	private String packingCode;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("item", item)
				.append("itemDesc", itemDesc).append("weight", weight)
				.append("volume", volume).append("quantity", quantity)
				.append("corpId", corpId).append("createdOn", createdOn)
				.append("createdBy", createdBy).append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy)
				.append("locationId", locationId).append("barCode", barCode)
				.append("shipNumber", shipNumber)
				.append("oItemRoomId", oItemRoomId)
				.append("oItemCondition", oItemCondition)
				.append("oItemComment", oItemComment)
				.append("oItemImageId", oItemImageId)
				.append("oPacker", oPacker).append("dItemRoomId", dItemRoomId)
				.append("dItemCondition", dItemCondition)
				.append("dItemComment", dItemComment)
				.append("dItemImageId", dItemImageId)
				.append("dPacker", dPacker).append("delivered", delivered)
				.append("cartonContent", cartonContent)
				.append("packingCode", packingCode).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof WorkTicketInventoryData))
			return false;
		WorkTicketInventoryData castOther = (WorkTicketInventoryData) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(item, castOther.item)
				.append(itemDesc, castOther.itemDesc)
				.append(weight, castOther.weight)
				.append(volume, castOther.volume)
				.append(quantity, castOther.quantity)
				.append(corpId, castOther.corpId)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(locationId, castOther.locationId)
				.append(barCode, castOther.barCode)
				.append(shipNumber, castOther.shipNumber)
				.append(oItemRoomId, castOther.oItemRoomId)
				.append(oItemCondition, castOther.oItemCondition)
				.append(oItemComment, castOther.oItemComment)
				.append(oItemImageId, castOther.oItemImageId)
				.append(oPacker, castOther.oPacker)
				.append(dItemRoomId, castOther.dItemRoomId)
				.append(dItemCondition, castOther.dItemCondition)
				.append(dItemComment, castOther.dItemComment)
				.append(dItemImageId, castOther.dItemImageId)
				.append(dPacker, castOther.dPacker)
				.append(delivered, castOther.delivered)
				.append(cartonContent, castOther.cartonContent)
				.append(packingCode, castOther.packingCode).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(item).append(itemDesc)
				.append(weight).append(volume).append(quantity).append(corpId)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).append(locationId).append(barCode)
				.append(shipNumber).append(oItemRoomId).append(oItemCondition)
				.append(oItemComment).append(oItemImageId).append(oPacker)
				.append(dItemRoomId).append(dItemCondition)
				.append(dItemComment).append(dItemImageId).append(dPacker)
				.append(delivered).append(cartonContent).append(packingCode)
				.toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	@Column
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	@Column
	public BigDecimal getWeight() {
		return weight;
	}
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}
	@Column
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	@Column
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	@Column
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public Long getoItemRoomId() {
		return oItemRoomId;
	}
	public void setoItemRoomId(Long oItemRoomId) {
		this.oItemRoomId = oItemRoomId;
	}
	@Column
	public String getoItemCondition() {
		return oItemCondition;
	}
	public void setoItemCondition(String oItemCondition) {
		this.oItemCondition = oItemCondition;
	}
	@Column
	public String getoItemComment() {
		return oItemComment;
	}
	public void setoItemComment(String oItemComment) {
		this.oItemComment = oItemComment;
	}
	@Column
	public Long getoItemImageId() {
		return oItemImageId;
	}
	public void setoItemImageId(Long oItemImageId) {
		this.oItemImageId = oItemImageId;
	}
	@Column
	public String getoPacker() {
		return oPacker;
	}
	public void setoPacker(String oPacker) {
		this.oPacker = oPacker;
	}
	@Column
	public Long getdItemRoomId() {
		return dItemRoomId;
	}
	public void setdItemRoomId(Long dItemRoomId) {
		this.dItemRoomId = dItemRoomId;
	}
	@Column
	public String getdItemCondition() {
		return dItemCondition;
	}
	public void setdItemCondition(String dItemCondition) {
		this.dItemCondition = dItemCondition;
	}
	@Column
	public String getdItemComment() {
		return dItemComment;
	}
	public void setdItemComment(String dItemComment) {
		this.dItemComment = dItemComment;
	}
	@Column
	public Long getdItemImageId() {
		return dItemImageId;
	}
	public void setdItemImageId(Long dItemImageId) {
		this.dItemImageId = dItemImageId;
	}
	@Column
	public String getdPacker() {
		return dPacker;
	}
	public void setdPacker(String dPacker) {
		this.dPacker = dPacker;
	}
	@Column
	public String getDelivered() {
		return delivered;
	}
	public void setDelivered(String delivered) {
		this.delivered = delivered;
	}
	@Column
	public String getCartonContent() {
		return cartonContent;
	}
	public void setCartonContent(String cartonContent) {
		this.cartonContent = cartonContent;
	}
	public String getPackingCode() {
		return packingCode;
	}
	public void setPackingCode(String packingCode) {
		this.packingCode = packingCode;
	}

	
}
