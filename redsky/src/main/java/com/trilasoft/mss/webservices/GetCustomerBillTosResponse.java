
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerBillTosResult" type="{http://www.mss1.com/webservices/}ArrayOfMSSBillTo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getCustomerBillTosResult"
})
@XmlRootElement(name = "GetCustomerBillTosResponse")
public class GetCustomerBillTosResponse {

    @XmlElement(name = "GetCustomerBillTosResult")
    protected ArrayOfMSSBillTo getCustomerBillTosResult;

    /**
     * Gets the value of the getCustomerBillTosResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSBillTo }
     *     
     */
    public ArrayOfMSSBillTo getGetCustomerBillTosResult() {
        return getCustomerBillTosResult;
    }

    /**
     * Sets the value of the getCustomerBillTosResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSBillTo }
     *     
     */
    public void setGetCustomerBillTosResult(ArrayOfMSSBillTo value) {
        this.getCustomerBillTosResult = value;
    }

}
