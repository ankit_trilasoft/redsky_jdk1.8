package com.trilasoft.app.service.impl;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.SSOLoginTokenDao;
import com.trilasoft.app.model.Userlogfile;
import com.trilasoft.app.service.SSOLoginTokenManager;

public class SSOLoginTokenManagerImpl extends GenericManagerImpl<Userlogfile, Long> implements SSOLoginTokenManager {

	private SSOLoginTokenDao ssoLoginTokenDao;

	public SSOLoginTokenManagerImpl(GenericDao<Userlogfile, Long> genericDao) {
		super(genericDao);
	}
	
	public SSOLoginTokenDao getSsoLoginTokenDao() {
		return ssoLoginTokenDao;
	}

	public void setSsoLoginTokenDao(SSOLoginTokenDao ssoLoginTokenDao) {
		this.ssoLoginTokenDao = ssoLoginTokenDao;
	}

	public void registerLoginToken(String sessionId, String loginToken, String userName) throws Exception {
		
		try {
			ssoLoginTokenDao.registerLoginToken(sessionId,  loginToken,  userName);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error Creating the SSO Login token for User : "+userName+" and sessionId : "+sessionId);
		}
	}

}
