// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`refmaster` MODIFY COLUMN `description` VARCHAR(5000) DEFAULT NULL;

ALTER TABLE `redsky`.`companydivision` ADD COLUMN `childAgentCode` VARCHAR(50) DEFAULT NULL AFTER `ugwwAgentCodes`;

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `confirmationOfDelievery` DATETIME NULL  AFTER `contractReceived` , ADD COLUMN `podToBooker` DATETIME NULL  AFTER `confirmationOfDelievery` ;

ALTER TABLE `redsky`.`company` ADD COLUMN `accessRedSkyCportalapp` BIT(1) DEFAULT b'0';
	
ALTER TABLE `redsky`.`app_user` ADD COLUMN `mobile_access` VARCHAR(45) DEFAULT '' AFTER `macid`;

ALTER TABLE `redsky`.`app_user` MODIFY COLUMN `mobile_access` BIT(1) ;

ALTER TABLE `redsky`.`companydivision` MODIFY COLUMN `childAgentCode` TEXT  DEFAULT NULL;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('UTSI', 'KeySurvey2013!', 'w.heijden@uts-intl.com', 50, 'QUALITYUSERPARAM','en',now(),'dkumar',now(),'dkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QUALITYUSERPARAM',50,true,now(),'dkumar',now(),'dkumar',1,1,'');


