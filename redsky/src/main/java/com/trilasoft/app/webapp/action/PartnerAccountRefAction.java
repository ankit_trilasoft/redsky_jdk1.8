package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.PartnerAccountRef;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.PartnerAccountRefManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.SystemDefaultManager;

public class PartnerAccountRefAction  extends BaseAction implements Preparable{
	
	private String sessionCorpID;
	private List partnerAccountRefs;
	private Long id;
	private String partnerCodeForRef;
	public List findCompanyDivisionByCorpId;
	private PartnerAccountRef partnerAccountRef;
	private PartnerAccountRefManager partnerAccountRefManager;
	private SystemDefaultManager systemDefaultManager;
	private PartnerPublic partner;
	private List companyCodeList=new ArrayList();
	private String companyDivision;
	private String partnerCode;
	private String hitFlag="";
	private SystemDefault systemDefault;
	private Company company;
	private CompanyManager companyManager;

	private String companyDivisionAcctgCodeUnique;
	private RefMasterManager refMasterManager;
	private Map<String,String> accountRefTypeList;
    private String refType;
    private String accountCrossReference;
    private String recSeq;
    private String paySeq;
    private String suspenseNum;
    private String partnerType;
    private String errMessage;
    private String usertype;
    private Boolean checkTransfereeInfopackage;
    private Boolean checkUpdateCorpId=false;
    
	public String getCompanyDivisionAcctgCodeUnique() {
		return companyDivisionAcctgCodeUnique;
	}

	public void setCompanyDivisionAcctgCodeUnique(
			String companyDivisionAcctgCodeUnique) {
		this.companyDivisionAcctgCodeUnique = companyDivisionAcctgCodeUnique;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public PartnerAccountRefAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        usertype = user.getUserType();
        this.sessionCorpID = user.getCorpID();
	}

	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(PartnerAccountRefAction.class);
    public void prepare() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){
	    	companyDivisionAcctgCodeUnique=partnerAccountRefManager.findAccountInterface(sessionCorpID); 
	    	getComboList(sessionCorpID);
		}
	}
    
    public String getComboList(String corpId){
    	findCompanyDivisionByCorpId=partnerAccountRefManager.findCompanyDivisionByCorpId(sessionCorpID);
    	accountRefTypeList = refMasterManager.findByParameter(corpId, "ACCOUNTREFTYPE");
    	return SUCCESS;
	}
    
    public String list(){
    	company= companyManager.findByCorpID(sessionCorpID).get(0);
    	checkTransfereeInfopackage = company.getTransfereeInfopackage();
		systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
		errMessage="" ;
		partner = (PartnerPublic) partnerAccountRefManager.getPartnerList(partnerCodeForRef, sessionCorpID).get(0);
    	partnerAccountRefs = partnerAccountRefManager.getPartnerAccountReferenceList(sessionCorpID, partnerCodeForRef);
    	 if(partnerType != null && (partnerType.equalsIgnoreCase("AC") || partnerType.equalsIgnoreCase("AG") || partnerType.equalsIgnoreCase("PP") || partnerType.equalsIgnoreCase("VN") || partnerType.equalsIgnoreCase("CR") || partnerType.equalsIgnoreCase("OO")) ){
	        	if(company.getAutoGenerateAccRef()!= null && company.getAutoGenerateAccRef().equalsIgnoreCase("Y")){
	        	   		if(systemDefault.getMinRec()== null ){
	        	   			errMessage = "The Limit for Payable and Receivable is blank in System Default. Please fill in the limit in System Default.";   
	        	   		}
	        	}
    	 }
    	return SUCCESS; 
	}
    
	public String edit() {
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage = company.getTransfereeInfopackage();
		systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
		//getComboList(sessionCorpID);
        if (id != null) {
        	partnerAccountRef = partnerAccountRefManager.get(id);
     
        } else {
        	partnerAccountRef = new PartnerAccountRef();
        	partnerAccountRef.setCorpID(sessionCorpID);    
        	partnerAccountRef.setRefType("P");
        	partnerAccountRef.setPartnerCode(partnerCodeForRef);
        	partnerAccountRef.setUpdatedOn(new Date());
        	partnerAccountRef.setCreatedOn(new Date());
        	
        	//recSeq =systemDefault.getCurrentRec();
        	//paySeq = systemDefault.getCurrentPay();
        	//suspenseNum = systemDefault.getSuspenseDefault();
        	 if(partnerType != null && (partnerType.equalsIgnoreCase("AC") || partnerType.equalsIgnoreCase("AG") || partnerType.equalsIgnoreCase("PP") || partnerType.equalsIgnoreCase("VN") || partnerType.equalsIgnoreCase("CR") || partnerType.equalsIgnoreCase("OO")) ){
 	        	if(company.getAutoGenerateAccRef()!= null && company.getAutoGenerateAccRef().equalsIgnoreCase("Y")){
 	        		
 	        		if(systemDefault.getCurrentRec()==null || systemDefault.getCurrentRec().equalsIgnoreCase("")  ){
 	        			recSeq=systemDefault.getMinRec();
 	        		}else{
 	        			Long tempCurr = new Long(systemDefault.getCurrentRec());
 	        			Long tempLow = new Long(systemDefault.getMinRec());
 	        			if(tempCurr<tempLow){
 	        				recSeq = systemDefault.getMinRec();
 	        			}else{
 	        				tempCurr= tempCurr+1;
 	        				String seq = tempCurr.toString();
 	        				recSeq = seq.toString() ;
 	        			}
 	        		}
 	        		if(systemDefault.getCurrentPay()==null || systemDefault.getCurrentPay().equalsIgnoreCase("")  ){
 	        			paySeq=systemDefault.getMinPay();
 	        		}else{
 	        			Long tempCurr = new Long(systemDefault.getCurrentPay());
 	        			Long tempLow = new Long(systemDefault.getMinPay());
 	        			if(tempCurr<tempLow){
 	        				paySeq=systemDefault.getMinPay();
 	        			}else{
	 	        			tempCurr= tempCurr+1;
	 	        			String seq =tempCurr.toString();
	 	        			paySeq = seq.toString() ;
 	        			}
 	        		}
 	        		if(systemDefault.getSuspenseDefault()==null || systemDefault.getSuspenseDefault().equalsIgnoreCase("")  ){
 	        			suspenseNum="";
 	        		}else{
 	        			suspenseNum = systemDefault.getSuspenseDefault();
 	        		}
 	        		
 	        		partnerAccountRef.setAccountCrossReference(paySeq);
 	        	}
        	 }
        	/*if(company.getAutoGenerateAccRef()!= null && company.getAutoGenerateAccRef().equalsIgnoreCase("Y")){
        		partnerAccountRef.setAccountCrossReference(paySeq);
        	}*/
        	
        }
        partner = (PartnerPublic) partnerAccountRefManager.getPartnerList(partnerAccountRef.getPartnerCode(), sessionCorpID).get(0);
        return SUCCESS;
    }
	@SkipValidation
	public String deleteAcctRef(){
		
		if (id != null) 
        {
        	partnerAccountRef = partnerAccountRefManager.get(id);
        	company= companyManager.findByCorpID(sessionCorpID).get(0);
        
        	if(company.getParentCorpId().equals(sessionCorpID))
        	{
        		try{
        			List enableChildCorpId = partnerAccountRefManager.getAccrefSameParentForChild(sessionCorpID);
        			if(enableChildCorpId!=null && !enableChildCorpId.isEmpty())
        			{
        				Iterator listItr = enableChildCorpId.listIterator();
        				while (listItr.hasNext()) 
        				{
        					String checkEnableChildCorpId = (String) listItr.next();
        					String[] a=checkEnableChildCorpId.split(",");
        					String childCorpId=a[0].toString();
        					String parentCorpId=a[1].toString();
        					partnerAccountRefManager.deletePartnerAccountRefFromChild(partnerAccountRef.getAccountCrossReference(),childCorpId,partnerAccountRef.getRefType(),partnerAccountRef.getPartnerCode());
        				}
        			}
        		}
        		catch(Exception ex)
        		{
        			ex.printStackTrace();
        		}
        	}
        }
		
    	String key = "Acct Ref has been deleted successfully.";   
        saveMessage(getText(key));
        partnerAccountRefManager.updateDeleteStatus(id);
        
       	hitFlag="1";
		list();
		return SUCCESS;	
	}
	
	
    public String save() throws Exception {
    	company= companyManager.findByCorpID(sessionCorpID).get(0);
    	checkTransfereeInfopackage = company.getTransfereeInfopackage();
		systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
		//getComboList(sessionCorpID);

		if((partnerAccountRef.getRefType().trim()!= null) && (!partnerAccountRef.getRefType().trim().equalsIgnoreCase(""))){
			List tempCount=partnerAccountRefManager.checkAccountReferenceForChild(partnerAccountRef.getAccountCrossReference().trim(),sessionCorpID,partnerAccountRef.getPartnerCode(),partnerAccountRef.getRefType());
			  if(!tempCount.get(0).toString().trim().equalsIgnoreCase("0")){
				  String key="";
				  if(partnerAccountRef.getRefType().equalsIgnoreCase("P"))
					  key = "The Payable is already added for this partner code.";
				  if(partnerAccountRef.getRefType().equalsIgnoreCase("R"))
					  key = "The Receivable is already added for this partner code.";
				  if(partnerAccountRef.getRefType().equalsIgnoreCase("S"))
					  key = "The Suspense is already added for this partner code.";
	        	partnerAccountRef.setAccountCrossReference(partnerAccountRef.getAccountCrossReference());
				errorMessage(getText(key));
				return INPUT;
	        }
		  }
		
		
		
		  if(partnerType != null && (partnerType.equalsIgnoreCase("AC") || partnerType.equalsIgnoreCase("AG") || partnerType.equalsIgnoreCase("CR")) ){
	        	if(company.getAutoGenerateAccRef()!= null && company.getAutoGenerateAccRef().equalsIgnoreCase("Y")){
	        		if(systemDefault.getCurrentRec()==null || systemDefault.getCurrentRec().equalsIgnoreCase("")  ){
	            		recSeq=systemDefault.getMinRec();
	            	}else{
	            		Long temp = new Long(systemDefault.getCurrentRec());
	            		   	temp= temp+1;
	    	        		String seq = temp.toString();
	    	        		recSeq = seq.toString() ;
	            	}
	            	if(systemDefault.getCurrentPay()==null || systemDefault.getCurrentPay().equalsIgnoreCase("")  ){
	            		paySeq=systemDefault.getMinPay();
	            	}else{
	            		Long temp = new Long(systemDefault.getCurrentPay());
	            		temp= temp+1;
	    	        	String seq = temp.toString();
	    	        	paySeq = seq.toString() ;
	            	}
	            	if(systemDefault.getSuspenseDefault()==null || systemDefault.getSuspenseDefault().equalsIgnoreCase("")  ){
	            		suspenseNum="";
	            	}else{
	            		suspenseNum = systemDefault.getSuspenseDefault();
	            	}
	        		if(partnerAccountRef.getRefType().equalsIgnoreCase("P") || partnerAccountRef.getRefType().equalsIgnoreCase("R") ){
	        		Long seqUpdate = Long.valueOf(partnerAccountRef.getAccountCrossReference());
	        		Long tempPay = Long.valueOf(systemDefault.getMaxPay());
	        		Long tempRec = Long.valueOf(systemDefault.getMaxRec());
	        		if(partnerAccountRef.getRefType().equalsIgnoreCase("P") && (seqUpdate > tempPay)){
	        			String key = "The Payable High Limit Has Been Reached. Please Update in System Default.";
	        			partnerAccountRef.setAccountCrossReference(partnerAccountRef.getAccountCrossReference());
	        			errorMessage(getText(key));
	        			return INPUT;
	        		}
	        		if(partnerAccountRef.getRefType().equalsIgnoreCase("R") && (seqUpdate > tempRec)){
	        			String key = "The Receivable High Limit Has Been Reached. Please Update in System Default.";
	        			partnerAccountRef.setAccountCrossReference(partnerAccountRef.getAccountCrossReference());
	        			errorMessage(getText(key));
	        			return INPUT;
	        		}
	        	 }
	          }
	        }
		  if(company.getAutoGenerateAccRef()!= null && company.getAutoGenerateAccRef().equalsIgnoreCase("Y")){
			  List tempCount = partnerAccountRefManager.countChkAccountRef(partnerAccountRef.getPartnerCode(),partnerAccountRef.getRefType(),sessionCorpID);
			  if(!tempCount.get(0).toString().trim().equalsIgnoreCase("0")){
				  String key="";
				  if(partnerAccountRef.getRefType().equalsIgnoreCase("P"))
					  key = "The Payable is already added for this partner code.";
				  if(partnerAccountRef.getRefType().equalsIgnoreCase("R"))
					  key = "The Receivable is already added for this partner code.";
				  if(partnerAccountRef.getRefType().equalsIgnoreCase("S"))
					  key = "The Suspense is already added for this partner code.";
	        	partnerAccountRef.setAccountCrossReference(partnerAccountRef.getAccountCrossReference());
				errorMessage(getText(key));
				return INPUT;
	        }
		  }
        if (cancel != null) {
            return "cancel";
        }
        boolean isNew = (partnerAccountRef.getId() == null);
        if(isNew){
        	checkUpdateCorpId=true;
        }else{
        	checkUpdateCorpId=false;
        }
        partnerAccountRef.setUpdatedOn(new Date());
        partnerAccountRef.setUpdatedBy(getRequest().getRemoteUser());
        partnerAccountRef.setCorpID(sessionCorpID);     	
        partnerAccountRef = partnerAccountRefManager.save(partnerAccountRef);
       
        if(partnerType != null && (partnerType.equalsIgnoreCase("AC") || partnerType.equalsIgnoreCase("AG") || partnerType.equalsIgnoreCase("CR")) ){
	        	if(company.getAutoGenerateAccRef()!= null && company.getAutoGenerateAccRef().equalsIgnoreCase("Y")){
	        		if(partnerAccountRef.getRefType().equalsIgnoreCase("P") || partnerAccountRef.getRefType().equalsIgnoreCase("R")){
		        		Long seqUpdate = Long.valueOf(partnerAccountRef.getAccountCrossReference());
				        String tempRefType = partnerAccountRef.getRefType();
				        seqUpdate = seqUpdate;
						String seq =seqUpdate.toString();
						int i = systemDefaultManager.updateSeqNumber(tempRefType,seq,sessionCorpID);
	        		}
	        	}
        }
        if(company.getParentCorpId().equals(sessionCorpID)){
        	try{
        List enableChildCorpId = partnerAccountRefManager.getAccrefSameParentForChild(sessionCorpID);
        if(enableChildCorpId!=null && !enableChildCorpId.isEmpty()){
        	Iterator listItr = enableChildCorpId.listIterator();
        	String count="";
			while (listItr.hasNext()) {
				String checkEnableChildCorpId = (String) listItr.next();
				String[] a=checkEnableChildCorpId.split(",");
				String childCorpId=a[0].toString();
				String parentCorpId=a[1].toString();
				companyCodeList=partnerAccountRefManager.checkAccountReferenceForChild(partnerAccountRef.getAccountCrossReference().trim(),childCorpId,partnerAccountRef.getPartnerCode(),partnerAccountRef.getRefType());
				if(companyCodeList!=null && !companyCodeList.isEmpty() && companyCodeList.get(0)!=null ){
					count = companyCodeList.get(0).toString();
				}
				if(count.equals("0")){
				createPartnerAccountRef(partnerAccountRef,childCorpId,parentCorpId);
				}
        }
        }
        	}catch(Exception ex){
        		ex.printStackTrace();
        	}
        }
        
        
    	hitFlag="1";
        partner=(PartnerPublic)partnerAccountRefManager.getPartnerList(partnerAccountRef.getPartnerCode(), sessionCorpID).get(0);
    	String key = (isNew) ? "partnerAccountRef.added" : "partnerAccountRef.updated";
        saveMessage(getText(key));
        if (!isNew) {
            return INPUT;
        } else {
            return SUCCESS;
        }
    }
        
        
        public void createPartnerAccountRef(PartnerAccountRef partnerAccountRefOld,String childCorpId,String parentCorpId) throws Exception {
        	company= companyManager.findByCorpID(childCorpId).get(0);
    		//systemDefault = systemDefaultManager.findByCorpID(childCorpId).get(0);
    		partnerAccountRef = new PartnerAccountRef();
    		Long childId=null;
    		
    		if(checkUpdateCorpId){
    			partnerAccountRef.setCreatedBy(getRequest().getRemoteUser());
    			partnerAccountRef.setCreatedOn(new Date());
    			
    		}else{
    			childId=partnerAccountRefOld.getId()+1;
    			partnerAccountRef.setId(childId);
    		}
    		partnerAccountRef.setUpdatedOn(new Date());
            partnerAccountRef.setUpdatedBy(getRequest().getRemoteUser());
    		partnerAccountRef.setRefType(partnerAccountRefOld.getRefType());
    		partnerAccountRef.setPartnerCode(partnerAccountRefOld.getPartnerCode());
    		partnerAccountRef.setAccountCrossReference(partnerAccountRefOld.getAccountCrossReference());
    		partnerAccountRef.setCompanyDivision(partnerAccountRefOld.getCompanyDivision());
            
            partnerAccountRef.setCorpID(childCorpId);     	
            partnerAccountRef = partnerAccountRefManager.save(partnerAccountRef);
           
        	
        }
    
    @SkipValidation
	public String checkCompanyDivisonExists(){
    	companyCodeList=partnerAccountRefManager.checkCompayCodeAccount(companyDivision,sessionCorpID,partnerCode,refType);
    	return SUCCESS; 
	}
    @SkipValidation
	public String checkAccountCrossReference(){
    	companyCodeList=partnerAccountRefManager.checkAccountReference(accountCrossReference.trim(),sessionCorpID,partnerCode,refType);
    	return SUCCESS; 
	}
    
	public List getPartnerAccountRefs() {
		return partnerAccountRefs;
	}
	
	public void setPartnerAccountRefs(List partnerAccountRefs) {
		this.partnerAccountRefs = partnerAccountRefs;
	}

	public PartnerAccountRef getPartnerAccountRef() {
		return partnerAccountRef;
	}

	public void setPartnerAccountRef(PartnerAccountRef partnerAccountRef) {
		this.partnerAccountRef = partnerAccountRef;
	}

	public void setPartnerAccountRefManager(
			PartnerAccountRefManager partnerAccountRefManager) {
		this.partnerAccountRefManager = partnerAccountRefManager;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getPartnerCodeForRef() {
		return partnerCodeForRef;
	}

	public void setPartnerCodeForRef(String partnerCodeForRef) {
		this.partnerCodeForRef = partnerCodeForRef;
	}

	public List getFindCompanyDivisionByCorpId() {
		return findCompanyDivisionByCorpId;
	}

	public void setFindCompanyDivisionByCorpId(List findCompanyDivisionByCorpId) {
		this.findCompanyDivisionByCorpId = findCompanyDivisionByCorpId;
	}
	
	public List getCompanyCodeList() {
		return companyCodeList;
	}

	public void setCompanyCodeList(List companyCodeList) {
		this.companyCodeList = companyCodeList;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	
	public PartnerPublic getPartner() {
		return partner;
	}
	
	public void setPartner(PartnerPublic partner) {
		this.partner = partner;
	}

	public Map<String, String> getAccountRefTypeList() {
		return accountRefTypeList;
	}

	public void setAccountRefTypeList(Map<String, String> accountRefTypeList) {
		this.accountRefTypeList = accountRefTypeList;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getRefType() {
		return refType;
	}

	public void setRefType(String refType) {
		this.refType = refType;
	}

	public String getAccountCrossReference() {
		return accountCrossReference;
	}

	public void setAccountCrossReference(String accountCrossReference) {
		this.accountCrossReference = accountCrossReference;
	}

	public String getRecSeq() {
		return recSeq;
	}

	public void setRecSeq(String recSeq) {
		this.recSeq = recSeq;
	}

	public String getPaySeq() {
		return paySeq;
	}

	public void setPaySeq(String paySeq) {
		this.paySeq = paySeq;
	}

	public String getSuspenseNum() {
		return suspenseNum;
	}

	public void setSuspenseNum(String suspenseNum) {
		this.suspenseNum = suspenseNum;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public Boolean getCheckTransfereeInfopackage() {
		return checkTransfereeInfopackage;
	}

	public void setCheckTransfereeInfopackage(Boolean checkTransfereeInfopackage) {
		this.checkTransfereeInfopackage = checkTransfereeInfopackage;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Boolean getCheckUpdateCorpId() {
		return checkUpdateCorpId;
	}

	public void setCheckUpdateCorpId(Boolean checkUpdateCorpId) {
		this.checkUpdateCorpId = checkUpdateCorpId;
	}
	
}
