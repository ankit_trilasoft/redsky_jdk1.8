package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
@Entity
@Table(name = "refquoteservices")
public class RefQuoteServices extends BaseObject{
	private Long id;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String langauge;
	private String mode;
	private String job;
	private String routing;
/*	private String code;*/
	private String description;

	private String checkIncludeExclude ;
	private Boolean refQuoteServicesDefault;
	private Integer displayOrder=new Integer(0);
	private String companyDivision;
	private String commodity;
	private String packingMode;
	private String originCountry;
	private String destinationCountry;
	private Long englishId;
	private Long germanId;
	private Long frenchId;
	private Long dutchId;
	private String serviceType;
	private String contract;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("langauge", langauge)
				.append("mode", mode).append("job", job)
				.append("routing", routing)
				.append("description", description)
				.append("checkIncludeExclude", checkIncludeExclude)
				.append("refQuoteServicesDefault", refQuoteServicesDefault)
				.append("displayOrder",displayOrder)
				.append("companyDivision",companyDivision)
				.append("commodity",commodity)
				.append("packingMode",packingMode)
				.append("originCountry",originCountry)
				.append("destinationCountry",destinationCountry)
				.append("englishId",englishId)
				.append("germanId",germanId)
				.append("frenchId",frenchId)
				.append("dutchId",dutchId)
				.append("serviceType",serviceType)
				.append("contract",contract)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RefQuoteServices))
			return false;
		RefQuoteServices castOther = (RefQuoteServices) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(langauge, castOther.langauge)
				.append(mode, castOther.mode)
				.append(job, castOther.job)
				.append(routing, castOther.routing)
				.append(description, castOther.description)
				.append(checkIncludeExclude, castOther.checkIncludeExclude)
				.append(refQuoteServicesDefault, castOther.refQuoteServicesDefault)
				.append(displayOrder, castOther.displayOrder)
				.append(companyDivision, castOther.companyDivision)
				.append(commodity, castOther.commodity)
				.append(packingMode, castOther.packingMode)
				.append(originCountry, castOther.originCountry)
				.append(destinationCountry, castOther.destinationCountry)
				.append(englishId, castOther.englishId)
				.append(germanId, castOther.germanId)
				.append(frenchId, castOther.frenchId)
				.append(dutchId, castOther.dutchId)
				.append(serviceType, castOther.serviceType)
				.append(contract, castOther.contract)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(createdOn).append(updatedBy)
				.append(updatedOn).append(langauge).append(mode).append(job)
				.append(routing).append(description)
				.append(checkIncludeExclude).append(refQuoteServicesDefault)
				.append(displayOrder)
				.append(companyDivision)
				.append(commodity)
				.append(packingMode)
				.append(originCountry)
				.append(destinationCountry)
				.append(englishId)
				.append(germanId)
				.append(frenchId)
				.append(dutchId)
				.append(serviceType)
				.append(contract)
				.toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getLangauge() {
		return langauge;
	}
	public void setLangauge(String langauge) {
		this.langauge = langauge;
	}
	@Column
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	@Column
	public String getRouting() {
		return routing;
	}
	public void setRouting(String routing) {
		this.routing = routing;
	}
/*	@Column
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}*/
	@Column
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column
	public String getCheckIncludeExclude() {
		return checkIncludeExclude;
	}
	public void setCheckIncludeExclude(String checkIncludeExclude) {
		this.checkIncludeExclude = checkIncludeExclude;
	}
	@Column
	public Boolean getRefQuoteServicesDefault() {
		return refQuoteServicesDefault;
	}
	public void setRefQuoteServicesDefault(Boolean refQuoteServicesDefault) {
		this.refQuoteServicesDefault = refQuoteServicesDefault;
	}
	@Column
	public Integer getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	@Column
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	@Column
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	@Column
	public String getPackingMode() {
		return packingMode;
	}
	public void setPackingMode(String packingMode) {
		this.packingMode = packingMode;
	}
	@Column
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	@Column
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	@Column
	public Long getEnglishId() {
		return englishId;
	}
	public void setEnglishId(Long englishId) {
		this.englishId = englishId;
	}
	@Column
	public Long getGermanId() {
		return germanId;
	}
	public void setGermanId(Long germanId) {
		this.germanId = germanId;
	}
	@Column
	public Long getFrenchId() {
		return frenchId;
	}
	public void setFrenchId(Long frenchId) {
		this.frenchId = frenchId;
	}
	@Column
	public Long getDutchId() {
		return dutchId;
	}
	public void setDutchId(Long dutchId) {
		this.dutchId = dutchId;
	}
	
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
}
