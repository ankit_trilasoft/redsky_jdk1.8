package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.KeySurveyQuestionMapDao;
import com.trilasoft.app.model.KeySurveyQuestionMap;
import com.trilasoft.app.service.KeySurveyQuestionMapManager;

public class KeySurveyQuestionMapManagerImpl extends GenericManagerImpl<KeySurveyQuestionMap, Long> implements KeySurveyQuestionMapManager{
	KeySurveyQuestionMapDao keySurveyQuestionMapDao;
	public KeySurveyQuestionMapManagerImpl(KeySurveyQuestionMapDao keySurveyQuestionMapDao) {
		super(keySurveyQuestionMapDao);
		this.keySurveyQuestionMapDao=keySurveyQuestionMapDao;
	}

}
