package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.SurveyResponseDtl;

public interface SurveyResponseDtlDao extends GenericDao<SurveyResponseDtl, Long> {

}
