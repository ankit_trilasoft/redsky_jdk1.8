package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.IntelLead;


public interface IntelLeadManager extends GenericManager<IntelLead, Long> {  
	
	public List<IntelLead> getIntelLeadLists(String corpId);

	public void updateActionValue(Long id, String sessionCorpID, String shipnumber, String seqNum);

}
