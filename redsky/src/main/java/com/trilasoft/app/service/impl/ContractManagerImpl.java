package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.dao.ContractDao;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.service.ContractManager;

import org.appfuse.service.impl.GenericManagerImpl;

public class ContractManagerImpl extends GenericManagerImpl<Contract, Long> implements ContractManager {
	ContractDao contractDao;

	public ContractManagerImpl(ContractDao contractDao) {
		super(contractDao);
		this.contractDao = contractDao;
	}
	
	public Contract getContactListByContarct(String corpid,String contact){
		return contractDao.getContactListByContarct(corpid,contact);
	}

	public List<Contract> findByJobType(String jobType) {
		return contractDao.findByJobType(jobType);
	}

	public List<Contract> findByBillingInstructionCode(String billingInstructionCode) {
		return contractDao.findByBillingInstructionCode(billingInstructionCode);
	}

	public Map<String, String> findForContract(String corpID, String jobType) {
		return contractDao.findForContract(corpID, jobType);
	}

	public List findContracts(String contractType, String description, String jobType,String companyDivision, Boolean activeCheck,String corpId) {
		return contractDao.findContracts(contractType, description, jobType,companyDivision, activeCheck, corpId);
	}

	public List findForJobContract(String corpID, String jobType) {
		return contractDao.findForJobContract(corpID, jobType);
	}

	public List findMaximumId() {
		return contractDao.findMaximumId();
	}

	public List findAllContract(String corpID) {
		return contractDao.findAllContract(corpID);
	}

	public List calculateCommission(Date openfromdate, Date opentodate, String corpId) {
		return contractDao.calculateCommission(openfromdate, opentodate, corpId);
	}
	
	public List findComanyDivision(String corpID){		
		return contractDao.findComanyDivision(corpID);
	}
	public List agentContract(String sessionCorpID,  String contract){
		return contractDao.agentContract(sessionCorpID,contract);
		
	}
	public List countRow(String contractName, String agentID){
		return contractDao.countRow(contractName, agentID);
	}
	public List findCostElement(String sessionCorpID){
		return contractDao.findCostElement(sessionCorpID);
	}
	public String getOwnerContract(Long id){
		return contractDao.getOwnerContract(id);
	}
	public List countRowOwner(String owner, Long Orginid, String agentID){
		return contractDao.countRowOwner(owner, Orginid, agentID);
	}
	public int findContractCount(String corpId, String contract, String agentsCorpId, Long id){
		return contractDao.findContractCount(corpId, contract, agentsCorpId, id);
	}
	public List findAgentCodeList(String agentCorpId){
		return contractDao.findAgentCodeList(agentCorpId);
	}
	public List checkContract(String sessionCorpID, String contract){
		return contractDao.checkContract(sessionCorpID,contract);
	}
	public List getAllContractbyCorpId(String sessionCorpID){
		return contractDao.getAllContractbyCorpId(sessionCorpID);
	}
	public void findDeleteContractOfOtherCorpId(String contractName, String agentCorpId){
		contractDao.findDeleteContractOfOtherCorpId(contractName,agentCorpId);
	}
	public String callPublishContractProcedure(String contractName,String sessionCorpID,String agentCorpId,String updatedBy){
		return contractDao.callPublishContractProcedure(contractName,sessionCorpID,agentCorpId,updatedBy);
	}
	public List findCmmDmmAgentDromCompany(){
		return contractDao.findCmmDmmAgentDromCompany();
	}
	public Map<String, String> getContractAndContractTypeMap(String sessionCorpID){
		return contractDao.getContractAndContractTypeMap(sessionCorpID);
	}
}