<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
   <head>   
    <title>Release Date</title>
   <!--A.Dash  --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here --> 
<script type="text/javascript">
function winLoad(){
	parent.window.opener.document.location='bookStorages.html?id=${workTicket.id}'; 
	window.close();
}
function storagewinLoad(){
	parent.window.opener.document.location='storages.html?id=${workTicket.id}'; 
	window.close();
}
function cancelToClose(){
	window.close();
}
function autoPopulate_Date() {
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym;
	var datam = daym+"-"+month+"-"+y.substring(2,4);
	//targetElement.form.elements['surveyListForm.surveyFrom'].value=datam;
	document.forms['releaseDate'].elements['workTicketrelease'].value=datam;
}
</script>
    </head>
 <s:form name="releaseDate" id="releaseDate" action="editStoragere.html?id=${workTicket.id}" method="post" onsubmit="return checkPartialData();">
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="idCheck" value="<%= request.getParameter("idCheck")%>" /> 
<s:hidden name="id1"  value="%{workTicket.id}" />
<s:hidden name="ticket" value="%{workTicket.ticket}" />

<s:hidden name="balanceWeight" value="" />
<s:hidden name="balanceVolume" value="" />
<s:hidden name="balancePieces" value="" />

<table border="0" class="mainDetailTable" cellpadding="0" cellspacing="0" style="width:380px;margin:0 0 10px;"> 
	<tr>
	<td>
	<table  border="0" align="left"  cellpadding="3" cellspacing="0" style="margin:0px" width="95%">
	<tr>
	<!--<td align="left" colspan="3" class="listwhitetext"><b>The containers shall be released on the Work Ticket end date. <br />Press OK to continue or select date manually. 
	</b></td>  
	--></tr>
	<tr><td height="10px"></td></tr>
	<tr>
	<td align="left" class="listwhitetext" width="33%"><b>Select release date:</b></td> 
	<td align="left" width="60"><s:textfield cssClass="input-text" id="date1" name="workTicketrelease"  size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" /><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	</tr>
	<tr><td height="10px"></td></tr>	
	</table> 
	</td></tr>	
	</table>
	<div id="releaseTable">
	<table border="0" class="mainDetailTable" cellpadding="0" cellspacing="0" style="width:380px;height:auto;margin:15px 0px 12px 0px;padding:5px;">
		<tr>
			<td align="right" class="listwhitetext" width="">Select Release:</td>
			<td colspan="" class="listwhitetext" width=""><s:radio id="releaseType" cssStyle="margin:2px;vertical-align:text-bottom;" name="releaseType" list="%{releaseTypeRadio}" onclick="showHide();" /></td>
		</tr>
		<tr>		
		<td colspan="5">
		<div id="releaseShow">
		<table cellpadding="3" cellspacing="0" style="width:320px;margin:3px 0px;">
		<tr>
			<td class="listwhitetext" align="right">Weight:&nbsp;</td>
			<td><s:textfield name="releaseWeight" size="15" maxlength="10" cssClass="input-text" cssStyle="text-align:right" onchange="onlyFloat(this);calculateWeight();"/></td>
			<td align="left" valign="middle" class="listwhitetext"><s:textfield name="originalWeight" readonly="true" size="10" cssClass="input-textUpper" value="${storage.measQuantity}" cssStyle="text-align:right"/></td>
		</tr>
		<tr>
			<td class="listwhitetext" align="right">Volume:&nbsp;</td>
			<td><s:textfield name="releaseVolume" size="15" maxlength="10" cssClass="input-text" cssStyle="text-align:right" onchange="onlyFloat(this);calculateVolume();"/></td>
			<td align="left" valign="middle" class="listwhitetext"><s:textfield name="originalVolume" readonly="true" size="10" cssClass="input-textUpper" value="${storage.volume}" cssStyle="text-align:right"/></td>
		</tr>
		<tr>
			<td class="listwhitetext" align="right">Pieces:&nbsp;</td>
			<td><s:textfield name="releasePieces" size="15" maxlength="7"  cssClass="input-text" cssStyle="text-align:right" onchange="onlyNumeric(this);calculatePieces();"/></td>
			<td align="left" valign="middle" class="listwhitetext"><s:textfield name="originalPieces" readonly="true" size="10" cssClass="input-textUpper" value="${storage.pieces}" cssStyle="text-align:right"/></td>
			<td></td>
		</tr>
		</table></div>
		</td>
		</tr>
	</table></div>
	<table style="margin:0px;padding:0px;" cellpadding="0" cellspacing="0">
	<tr>
	<td><input type="submit" class="cssbutton1" name="forwardBtn"   value="Ok" style="width:55px;"  /></td> 
    <td width="5px"></td>
    <td><input type="button" class="cssbutton1" name="forwardBtn"   value="Cancel" onclick="cancelToClose()" style="width:55px;" /></td>
	</tr>
	</table>
 </s:form>
 <script type="text/javascript">
	setCalendarFunctionality();
</script>
  <script type="text/javascript">
try{
	 autoPopulate_Date();
	<c:if test="${hitFlag == 1}" >
	winLoad();
</c:if>
<c:if test="${hitFlag == 0}" >
storagewinLoad();
</c:if>		
}catch(e){}
document.getElementById('releaseTypeC').checked=true;
function calculateWeight(){
	var oriWt = parseFloat(document.forms['releaseDate'].elements['originalWeight'].value);
	var relWt = parseFloat(document.forms['releaseDate'].elements['releaseWeight'].value);
	var originalWt = document.forms['releaseDate'].elements['originalWeight'].value;
	var restWt = "";
	if(originalWt==''){
		alert('Location Weight is empty.');
		document.forms['releaseDate'].elements['releaseWeight'].value = "";
	}else{
		if((relWt > oriWt) || (relWt == oriWt)){
			alert('Kindly put Weight less than'+" "+document.forms['releaseDate'].elements['originalWeight'].value);
			document.forms['releaseDate'].elements['releaseWeight'].value = "";
		}else{
			restWt = oriWt-relWt;
			restWt = Math.round(restWt*100)/100;
			document.forms['releaseDate'].elements['balanceWeight'].value = restWt;
		}
	}
}

function calculateVolume(){
	var oriVol = parseFloat(document.forms['releaseDate'].elements['originalVolume'].value);
	var relVol = parseFloat(document.forms['releaseDate'].elements['releaseVolume'].value);
	var originalVol = document.forms['releaseDate'].elements['originalVolume'].value;
	var restVol = "";
	if(originalVol==''){
		alert('Location Volume is empty.');
		document.forms['releaseDate'].elements['releaseVolume'].value = "";
	}else{
		if((relVol > oriVol) || (relVol == oriVol)){
			alert('Kindly put Volume less than'+" "+document.forms['releaseDate'].elements['originalVolume'].value);
			document.forms['releaseDate'].elements['releaseVolume'].value = "";
		}else{
			restVol = oriVol-relVol;
			restVol = Math.round(restVol*100)/100;
			document.forms['releaseDate'].elements['balanceVolume'].value = restVol;
		}
	}
}

function calculatePieces(){
	var oriPiece = parseInt(document.forms['releaseDate'].elements['originalPieces'].value);
	var relPiece = parseInt(document.forms['releaseDate'].elements['releasePieces'].value);
	var originalPiece = document.forms['releaseDate'].elements['originalPieces'].value;
	var restPiece = "";
	if(originalPiece=='' || originalPiece==0){
		alert('Location Pieces is empty.');
		document.forms['releaseDate'].elements['releasePieces'].value = "";
	}else{
		if((relPiece > oriPiece) || (relPiece == oriPiece)){
			alert('Kindly put Pieces less than'+" "+oriPiece);
			document.forms['releaseDate'].elements['releasePieces'].value = "";
		}else{
			restPiece = oriPiece-relPiece;
			document.forms['releaseDate'].elements['balancePieces'].value = restPiece;
		}
	}
}
function showHide(){
	var releaseTypeVal = document.forms['releaseDate'].elements['releaseType'].value;
	if(releaseTypeVal=='P'){
		document.getElementById('releaseShow').style.display="block";
	}else{
		document.getElementById('releaseShow').style.display="none";
	}
}

function checkPartialData(){
	var releaseTypeVal = document.forms['releaseDate'].elements['releaseType'].value;
	if(releaseTypeVal=='P'){
		var relWt = document.forms['releaseDate'].elements['releaseWeight'].value;
		var relVol = document.forms['releaseDate'].elements['releaseVolume'].value;
		var relPiece = document.forms['releaseDate'].elements['releasePieces'].value;
		
		if((relWt=='' || relWt==0) && (relVol=='' || relVol==0) && (relPiece=='')){
			alert('Kindly put value for either Weight or Volume or Pieces.');
			return false;
		}else{
			return true;
		}
	}else{
		return true;
	}
}
showHide();
var idVal = document.forms['releaseDate'].elements['idCheck'].value;
	if(idVal.indexOf(",") == -1) {
		document.getElementById('releaseTable').style.display="block";
	}else{
		document.getElementById('releaseTable').style.display="none";
	}
 </script>