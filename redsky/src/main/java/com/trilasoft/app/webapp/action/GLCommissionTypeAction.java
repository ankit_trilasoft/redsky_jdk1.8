package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.GLCommissionType;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.model.VanLineGLType;

import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.GLCommissionTypeManager;
import com.trilasoft.app.service.VanLineCommissionTypeManager;
import com.trilasoft.app.service.VanLineGLTypeManager;

public class GLCommissionTypeAction extends BaseAction {

	private List gLCommissionTypeList;

	private Long id;

	private GLCommissionTypeManager glCommissionTypeManager;

	private String sessionCorpID;



	private GLCommissionType glCommissionType;
	
	private String contract;
	private Charges charges;
	private String charge;
	private List glList;
	
    private ChargesManager chargesManager;
    private String chargesGl;
    private Long glId;
    private Long  contractId;
	private List glTypeList;
	private List glCodeList;
	private String hitFlag;
	private VanLineGLTypeManager vanLineGLTypeManager; 
    private VanLineCommissionTypeManager vanLineCommissionTypeManager;
	public Long getGlId() {
		return glId;
	}

	public void setGlId(Long glId) {
		this.glId = glId;
	}

	public GLCommissionTypeAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	   public String list() { 
		charges =chargesManager.get(glId);
		contractId=this.contractId;
		gLCommissionTypeList = glCommissionTypeManager.gLCommissionTypeList(contract, charge); 
		
		return SUCCESS;
	}
	   @SkipValidation
	   public String deleteGLCommissionType(){ 
		   glCommissionTypeManager.remove(id);
		   charges =chargesManager.get(glId);
			contractId=this.contractId;
			gLCommissionTypeList=new ArrayList();
			gLCommissionTypeList = glCommissionTypeManager.gLCommissionTypeList(contract, charge); 
			list();
			hitFlag="5";
			return SUCCESS;
	   }
	
	public String edit() {
		contractId=this.contractId;
		charges =chargesManager.get(glId);
		glList= glCommissionTypeManager.glList(charges.getGl()); 
		if (id != null) {
			glCommissionType = glCommissionTypeManager.get(id);
		} else {
			glCommissionType = new GLCommissionType();			
			glCommissionType.setGlCode(charges.getGl());
			//glList= vanLineCommissionTypeManager.glList(charges.getGl());
			glCommissionType.setCreatedOn(new Date());
			glCommissionType.setUpdatedOn(new Date());
			glCommissionType.setUpdatedBy(getRequest().getRemoteUser());
		}
		glCommissionType.setCorpID(sessionCorpID);
		
		return SUCCESS;
	}
	
	
	public String save() throws Exception {	
		
		charges =chargesManager.get(glId);
		glList= glCommissionTypeManager.glList(charges.getGl());
		contractId=this.contractId;
		boolean isNew=(glCommissionType.getId()==null);
		 if(isNew)
	        {
			 glCommissionType.setCreatedOn(new Date());
	        }
		 glCommissionType.setUpdatedOn(new Date());
		 glCommissionType.setUpdatedBy(getRequest().getRemoteUser());
		glCommissionType.setCorpID(sessionCorpID);
		//gLCommissionType.setUpdatedOn(new Date());
		glCommissionType=glCommissionTypeManager.save(glCommissionType);	
		
		String key = (isNew) ? "GL Type has been added successfully." : "GL Type has been updated successfully.";
		saveMessage(getText(key));
		
		return SUCCESS;
		
	}
	
	public String glTypePopupList(){
		
		glTypeList=vanLineGLTypeManager.getGLPopupList(chargesGl);
		return SUCCESS;
	}
	
public String glCodePopupList(){
		
		glCodeList=vanLineCommissionTypeManager.getGLCodePopupList(chargesGl);
		return SUCCESS;
	}
	/*public String getComboList(String corpId) {
		glCode = refMasterManager.findByParameter(corpId, "GLCODES");
		return SUCCESS;
	}

	public String edit() {

		if (id != null) {
			vanLineGLType = vanLineGLTypeManager.get(id);
			vanLineGLType.setUpdatedOn(new Date());
		} else {
			vanLineGLType = new VanLineGLType();
			vanLineGLType.setCreatedOn(new Date());
			vanLineGLType.setUpdatedOn(new Date());
		}
		getComboList(sessionCorpID);
		vanLineGLType.setCorpID(sessionCorpID);
		return SUCCESS;
	}

	public String save() throws Exception {
		boolean isNew = (vanLineGLType.getId() == null);
		if (isNew) {
			vanLineGLType.setCreatedOn(new Date());
		} 

		vanLineGLType.setUpdatedOn(new Date());
		vanLineGLType.setCorpID(sessionCorpID);
		vanLineGLType = vanLineGLTypeManager.save(vanLineGLType);
		getComboList(sessionCorpID);
		String key = (isNew) ? "Van Line GL Type has been added successfully" : "Van Line GL Type has been updated successfully";
		saveMessage(getText(key));
		if (!isNew) {
			return INPUT;
		} else {
			//maxId = Long.parseLong(vanLineManager.findMaximumId().get(0).toString());
			//vanLine = vanLineManager.get(maxId);
			return SUCCESS;
		}
	}
	
	@SkipValidation 
	  public String searchGLType(){     
		boolean glType = (vanLineGLType.getGlType() == null);
	    boolean desc = (vanLineGLType.getDescription() == null);
	    boolean glCode = (vanLineGLType.getGlCode() == null);
	    
          if(!glType ||!desc ||!glCode ) {
        	  vanLineGLTypeList =  vanLineGLTypeManager.searchVanLineGLType(vanLineGLType.getGlType(), vanLineGLType.getDescription(), vanLineGLType.getGlCode());
        	  
          }
        return SUCCESS;     
	}
	*/
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public List getGLCommissionTypeList() {
		return gLCommissionTypeList;
	}

	public void setGLCommissionTypeList(List commissionTypeList) {
		gLCommissionTypeList = commissionTypeList;
	}

	

	public void setGlCommissionTypeManager(GLCommissionTypeManager glCommissionTypeManager) {
		this.glCommissionTypeManager = glCommissionTypeManager;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public List getGlList() {
		return glList;
	}

	public void setGlList(List glList) {
		this.glList = glList;
	}

	

	public String getChargesGl() {
		return chargesGl;
	}

	public void setChargesGl(String chargesGl) {
		this.chargesGl = chargesGl;
	}

	public Charges getCharges() {
		return charges;
	}

	public void setCharges(Charges charges) {
		this.charges = charges;
	}

	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}

	public GLCommissionType getGlCommissionType() {
		return glCommissionType;
	}

	public void setGlCommissionType(GLCommissionType glCommissionType) {
		this.glCommissionType = glCommissionType;
	}

	

	public List getGlTypeList() {
		return glTypeList;
	}

	public void setGlTypeList(List glTypeList) {
		this.glTypeList = glTypeList;
	}

	public void setVanLineGLTypeManager(VanLineGLTypeManager vanLineGLTypeManager) {
		this.vanLineGLTypeManager = vanLineGLTypeManager;
	}

	public List getGlCodeList() {
		return glCodeList;
	}

	public void setGlCodeList(List glCodeList) {
		this.glCodeList = glCodeList;
	}

	public void setVanLineCommissionTypeManager(
			VanLineCommissionTypeManager vanLineCommissionTypeManager) {
		this.vanLineCommissionTypeManager = vanLineCommissionTypeManager;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}	
}
