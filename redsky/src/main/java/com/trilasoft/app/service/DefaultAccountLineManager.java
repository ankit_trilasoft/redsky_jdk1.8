package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.DefaultAccountLine;

public interface DefaultAccountLineManager extends GenericManager<DefaultAccountLine, Long>  {
	public List<DefaultAccountLine> findDefaultAccountLineList();

	public List getDefaultList(String jobType, String routing, String mode);
	public List<DefaultAccountLine> findDefaultAccountLines(String jobType, String route, String mode, String serviceType, String categories, String billToCode, String contract,String packMode,String commodity, String compDiv, String oCountry, String dCountry,String equipment);
	public List defaultALUpdate(Long id);
	public List findAccountInterface(String corpID); 
	public List getDefaultAccountList(String jobType, String routing, String mode,String contract,String billToCode, String packingMode, String commodity, String serviceType, String companyDivision,String originCountry,String destinationCountry,String equipment,String originCity,String destinationCity);
    public List findBillToCode(String corpID);

	public List findContractList(String corpID);

	public List getAcctTemplateList(String contract, String sessionCorpID); 
	public List findChargesByTemplate(String sessionCorpID);
	public String saveBulkUpdates(String id,String changedValue );
	public List getChargeTemplate(String contract, String sessionCorpID);
	public List getAllValues(String sessionCorpID);
	public List findContracOnJobBasis(String billToCode, String job, Date createdon,String corpID);
	public void saveUpdatesList(String AllValue,String corpId,String updateby);
	public List findSearchNameAutocompleteListFromDefaultAccountList(String searchName,String corpId,String populateDataField);
	public List findSearchChargeCodeAutocompleteList(String searchName,String corpId,String contract,String formtype,String compDivision,String originCountry,String destinCountry,String mode,String category); 
	public List findContractListNEW(String corpID);
	public List findContractListFromContract(String corpID);
	public List findContractListFromTemplate(String corpID);
	public List<Charges> findChargeCodeForMissingDefaultAccountLineContract(String fromcontract,String corpID,String toContract);
	public int getChargeTemplateCopy(String fromContract, String sessionCorpID,String id,String toContract);
	public List findSameDefaultAccountLineContract(String fromcontract,String corpID,String toContract);
	public List findChargeCodeForMisMatchDefaultAccountLine(String fromcontract,String corpID,String toContract);
	public List findChargeCodeForMisMatchFromCharges(String fromcontract,String corpID,String toContract);
	public List getDuplicatedData(String corpID);
	public String deleteDuplicateTemplateData(String corpID,String listedId);
	public int findChargeCodeForMissingDefaultAccountLineContractWithoutValidation(String userName,String fromcontract,String corpID,String toContract);

}
