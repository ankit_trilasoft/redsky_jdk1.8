
package com.trilasoft.app.service.impl;

import com.trilasoft.app.dao.PartnerQuoteDao;
import com.trilasoft.app.model.PartnerQuote;   
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.PartnerQuoteManager;

import org.appfuse.service.impl.GenericManagerImpl;   

import java.math.BigDecimal;   
import java.util.List;   
  
public class PartnerQuoteManagerImpl extends GenericManagerImpl<PartnerQuote, Long> implements PartnerQuoteManager {   
	PartnerQuoteDao partnerQuoteDao;   
	
    public PartnerQuoteManagerImpl(PartnerQuoteDao partnerQuoteDao) {   
        super(partnerQuoteDao);   
        this.partnerQuoteDao = partnerQuoteDao;   
    } 
    
    public List findMaximumId(){
     	return partnerQuoteDao.findMaximumId(); 
     }
    public int updateQuoteStatus(String mail, String reqSO){
     	return partnerQuoteDao.updateQuoteStatus(mail, reqSO); 
     }
    public int updateQuoteReminder(String mail )
    {
    	return partnerQuoteDao.updateQuoteReminder(mail); 
    }
    public List findPartnerCode(String partnerCode){
    	return partnerQuoteDao.findPartnerCode(partnerCode);
    }
	
    
// 	@@@@@@@@@@@@@@@@@@@@@ By Madhu - For Quation Management @@@@@@@@@@@@@@@@@@@@@@@@
	public List findByQuoteType(String quoteType,String sequenceNumber)
	{
		return partnerQuoteDao.findByQuoteType(quoteType,sequenceNumber);
	}
	
	public List findBySequenceNumber(String sequenceNumber)
	{
		return partnerQuoteDao.findBySequenceNumber(sequenceNumber);
	}
// 	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	public List <PartnerQuote> findByStatus(String quoteStatus,String sessionCorpID, String vendorCodeSet) {
		return partnerQuoteDao.findByStatus(quoteStatus,sessionCorpID,vendorCodeSet);
	}
	public List <PartnerQuote> findByStatusSubmitted(String quoteStatus,String searchByCode,String searchByName,String shipNumber) {
		return partnerQuoteDao.findByStatusSubmitted(quoteStatus, searchByCode, searchByName,shipNumber);
	}
	public List findByVendorEmail(String email, String seqNum){
		return partnerQuoteDao.findByVendorEmail(email,seqNum);
	}
	public List getGeneralListVendor(String searchByCode,String searchByName,String searchByCountryCode,String searchByState,String searchByCity){
		return partnerQuoteDao.getGeneralListVendor(searchByCode,searchByName,searchByCountryCode,searchByState,searchByCity);
	}

	public List findServiceOrders(String requestedSO,String sessionCorpID) {
		return partnerQuoteDao.findServiceOrders(requestedSO,sessionCorpID);
	}
	
	public List vendorNamenEmail(String partnerCode){
		return partnerQuoteDao.vendorNamenEmail(partnerCode); 
	}

	public List findSoList(String sequenceNumber, String corpId) {
		 
		return partnerQuoteDao.findSoList(sequenceNumber, corpId);
	}

	public List findSoDetail(String shipNumber, String corpId) {
		return partnerQuoteDao.findSoDetail(shipNumber, corpId);
		
	}

}

