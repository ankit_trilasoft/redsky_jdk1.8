package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CommissionDao;
import com.trilasoft.app.model.Commission;



public class CommissionDaoHibernate  extends GenericDaoHibernate<Commission, Long> implements CommissionDao{

	public CommissionDaoHibernate () {
		super(Commission.class);
	}
	public Map<Long, Double> getAutoCommisssionMap(String sessionCorpID, String shipNumber) {
		Map <Long,Double > commissionMap  = new   HashMap<Long,Double >();
		List orderList=getHibernateTemplate().find("from Commission where corpId='"+sessionCorpID+"' and shipNumber='"+shipNumber+"'");
		Iterator<Object> it =orderList.iterator();
		while (it.hasNext()){
			try{
			Commission commission	=(Commission)it.next();
			if(commission !=null && commission.getAid() !=null && commission.getSalesPersonAmount()!=null){
			commissionMap.put(commission.getAid(),Double.parseDouble(commission.getSalesPersonAmount().toString()));
			} 
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return commissionMap;
	}

	public BigDecimal findTotalExpenseOfChargeACC(String sessionCorpID, String shipNumber, String tempAccId) {
		BigDecimal totalCommExp = new BigDecimal(0);
		System.out.println("\n\n\n\n select sum(salesPersonAmount) from commission where shipNumber='"+shipNumber+"' and corpID= '"+sessionCorpID+"' and aid in ("+tempAccId+") ");
		List l1 = this.getSession().createSQLQuery("select sum(salesPersonAmount) from commission where shipNumber='"+shipNumber+"' and corpID= '"+sessionCorpID+"' and aid in ("+tempAccId+") " ).list();
		if(l1!=null && !l1.isEmpty() && l1.get(0)!=null ){
			totalCommExp = new BigDecimal(l1.get(0).toString().trim());
		}	
		return totalCommExp;
	}
}
