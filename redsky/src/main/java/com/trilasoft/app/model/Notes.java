package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

@Entity
@Table ( name="notes")
@FilterDefs({
	@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")}),
	@FilterDef(name = "notesAccountPortalFilter", parameters = { @ParamDef(name = "accportal", type = "boolean")}),
	@FilterDef(name = "notesPartnerPortalFilter", parameters = { @ParamDef(name = "partnerportal", type = "boolean")})
})
@Filters( {
	@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)"),
	@Filter(name = "notesAccountPortalFilter", condition = " (accPortal = :accportal) "),
	@Filter(name = "notesPartnerPortalFilter", condition = " (partnerPortal = :partnerportal) ")
} )
public class Notes extends BaseObject {
	
	private String notesId;
	private String corpID;
	private Long id;
	private String noteType;
	private String noteSubType;
	private String subject;
	private String note;
	private String forwardToUser;
	private Date forwardDate;
	private String forwardStatus;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String followUpId;
	//private CustomerFile customerFile;
	
	// Modifications
	
	private String noteStatus;
	private String remindTime;
	private Boolean displaySurvey;
	private Boolean displayTicket;
	private Boolean displayInvoice;
	private Boolean displayPortal;
	private Boolean displayQuote;
	private String customerNumber;
	private Date systemDate;
	private Date complitionDate;
	private String reminderStatus;
//	 Modifications after 30 May 2008
	private String followUpFor;
	private String remindInterval;
	
	private Boolean accPortal;
	private Boolean partnerPortal;
	
	private String toDoRuleId;
	
	private Long notesKeyId ;
	
	private String noteGroup;
	//Enhancement in Account Contact Notes Functionality...
	private Date dateOfContact;
	private String notesActivity;
	private String name;
	private String myFileId;
	private String linkedTo;
	private String issueType;
	private String supplier;
	private String grading;
	
	private Boolean bookingAgent;
	private Boolean originAgent;
	private Boolean destinationAgent;
	private Boolean subOriginAgent;
	private Boolean subDestinationAgent;
	private Boolean networkAgent;
	private String networkLinkId;
	private String category;
	private String uvlSentStatus;
	private String memoUploadStatus;
	private Date   memoLastUploadedDate;
	private Date requestDate;
	private Date submittedDate;
    private BigDecimal actualValue=new BigDecimal("0.00");
    private BigDecimal requestedValue=new BigDecimal("0.00");
    private Boolean negativeComment;
    private Boolean reportToAgent;
    private String issue;
    private String resolution;
    private String roleCausedBy;
    private String billToCode;
	private String bookingAgentCode;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("notesId", notesId)
				.append("corpID", corpID).append("id", id)
				.append("noteType", noteType)
				.append("noteSubType", noteSubType).append("subject", subject)
				.append("note", note).append("forwardToUser", forwardToUser)
				.append("forwardDate", forwardDate)
				.append("forwardStatus", forwardStatus)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.append("followUpId", followUpId)
				.append("noteStatus", noteStatus)
				.append("remindTime", remindTime)
				.append("displaySurvey", displaySurvey)
				.append("displayTicket", displayTicket)
				.append("displayInvoice", displayInvoice)
				.append("displayPortal", displayPortal)
				.append("displayQuote", displayQuote)
				.append("customerNumber", customerNumber)
				.append("systemDate", systemDate)
				.append("complitionDate", complitionDate)
				.append("reminderStatus", reminderStatus)
				.append("followUpFor", followUpFor)
				.append("remindInterval", remindInterval)
				.append("accPortal", accPortal)
				.append("partnerPortal", partnerPortal)
				.append("toDoRuleId", toDoRuleId)
				.append("notesKeyId", notesKeyId)
				.append("noteGroup", noteGroup)
				.append("dateOfContact", dateOfContact)
				.append("notesActivity", notesActivity).append("name", name)
				.append("myFileId", myFileId).append("linkedTo", linkedTo)
				.append("issueType", issueType).append("supplier", supplier)
				.append("grading", grading)
				.append("bookingAgent", bookingAgent)
				.append("originAgent", originAgent)
				.append("destinationAgent", destinationAgent)
				.append("subOriginAgent", subOriginAgent)
				.append("subDestinationAgent", subDestinationAgent)
				.append("networkAgent", networkAgent)
				.append("networkLinkId", networkLinkId)
				.append("category",category).append("uvlSentStatus",uvlSentStatus).append("memoUploadStatus",memoUploadStatus)
				.append("memoLastUploadedDate",memoLastUploadedDate)
				.append("requestDate",requestDate)
				.append("submittedDate",submittedDate)
				.append("actualValue",actualValue)
				.append("requestedValue",requestedValue)
				.append("negativeComment",negativeComment)
				.append("reportToAgent",reportToAgent)
				.append("issue",issue)
				.append("resolution",resolution)
				.append("rollCausedBy",roleCausedBy)
				.append("billToCode",billToCode)
				.append("bookingAgentCode",bookingAgentCode)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Notes))
			return false;
		Notes castOther = (Notes) other;
		return new EqualsBuilder().append(notesId, castOther.notesId)
				.append(corpID, castOther.corpID).append(id, castOther.id)
				.append(noteType, castOther.noteType)
				.append(noteSubType, castOther.noteSubType)
				.append(subject, castOther.subject)
				.append(note, castOther.note)
				.append(forwardToUser, castOther.forwardToUser)
				.append(forwardDate, castOther.forwardDate)
				.append(forwardStatus, castOther.forwardStatus)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(followUpId, castOther.followUpId)
				.append(noteStatus, castOther.noteStatus)
				.append(remindTime, castOther.remindTime)
				.append(displaySurvey, castOther.displaySurvey)
				.append(displayTicket, castOther.displayTicket)
				.append(displayInvoice, castOther.displayInvoice)
				.append(displayPortal, castOther.displayPortal)
				.append(displayQuote, castOther.displayQuote)
				.append(customerNumber, castOther.customerNumber)
				.append(systemDate, castOther.systemDate)
				.append(complitionDate, castOther.complitionDate)
				.append(reminderStatus, castOther.reminderStatus)
				.append(followUpFor, castOther.followUpFor)
				.append(remindInterval, castOther.remindInterval)
				.append(accPortal, castOther.accPortal)
				.append(partnerPortal, castOther.partnerPortal)
				.append(toDoRuleId, castOther.toDoRuleId)
				.append(notesKeyId, castOther.notesKeyId)
				.append(noteGroup, castOther.noteGroup)
				.append(dateOfContact, castOther.dateOfContact)
				.append(notesActivity, castOther.notesActivity)
				.append(name, castOther.name)
				.append(myFileId, castOther.myFileId)
				.append(linkedTo, castOther.linkedTo)
				.append(issueType, castOther.issueType)
				.append(supplier, castOther.supplier)
				.append(grading, castOther.grading)
				.append(bookingAgent, castOther.bookingAgent)
				.append(originAgent, castOther.originAgent)
				.append(destinationAgent, castOther.destinationAgent)
				.append(subOriginAgent, castOther.subOriginAgent)
				.append(subDestinationAgent, castOther.subDestinationAgent)
				.append(networkAgent, castOther.networkAgent)
				.append(networkLinkId, castOther.networkLinkId)
				.append(category,castOther.category).append(uvlSentStatus,castOther.uvlSentStatus).append(memoUploadStatus,castOther.memoUploadStatus)
				.append(memoLastUploadedDate,castOther.memoLastUploadedDate)
				.append(requestDate,castOther.requestDate)
				.append(submittedDate,castOther.submittedDate)
				.append(actualValue,castOther.actualValue)
				.append(requestedValue,castOther.requestedValue)
				.append(negativeComment,castOther.negativeComment)
				.append(reportToAgent,castOther.reportToAgent)
				.append(issue,castOther.issue)
				.append(resolution,castOther.resolution)
				.append(roleCausedBy,castOther.roleCausedBy)
				.append("billToCode",castOther.billToCode)
				.append("bookingAgentCode",castOther.bookingAgentCode)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(notesId).append(corpID).append(id)
				.append(noteType).append(noteSubType).append(subject)
				.append(note).append(forwardToUser).append(forwardDate)
				.append(forwardStatus).append(createdOn).append(createdBy)
				.append(updatedOn).append(updatedBy).append(followUpId)
				.append(noteStatus).append(remindTime).append(displaySurvey)
				.append(displayTicket).append(displayInvoice)
				.append(displayPortal).append(displayQuote)
				.append(customerNumber).append(systemDate)
				.append(complitionDate).append(reminderStatus)
				.append(followUpFor).append(remindInterval).append(accPortal)
				.append(partnerPortal).append(toDoRuleId).append(notesKeyId)
				.append(noteGroup).append(dateOfContact).append(notesActivity)
				.append(name).append(myFileId).append(linkedTo)
				.append(issueType).append(supplier).append(grading)
				.append(bookingAgent).append(originAgent)
				.append(destinationAgent).append(subOriginAgent)
				.append(subDestinationAgent).append(networkAgent)
				.append(networkLinkId)
				.append(category).append(uvlSentStatus).append(memoUploadStatus)
				.append(memoLastUploadedDate)
				.append(requestDate)
				.append(submittedDate)
				.append(actualValue)
				.append(requestedValue)
				.append(negativeComment)
				.append(reportToAgent)
				.append(issue)
				.append(resolution)
				.append(roleCausedBy)
				.append(billToCode)
				.append(bookingAgentCode)
				.toHashCode();
	}
	@Column( length=12 )
	public String getFollowUpFor() {
		return followUpFor;
	}
	public void setFollowUpFor(String followUpFor) {
		this.followUpFor = followUpFor;
	}
	@Column( length=12 )
	public String getRemindInterval() {
		return remindInterval;
	}
	public void setRemindInterval(String remindInterval) {
		this.remindInterval = remindInterval;
	}
	/*	@ManyToOne
	@JoinColumn(name="notesId", nullable=false, updatable=false,
	referencedColumnName="sequenceNumber")
	public CustomerFile getCustomerFile() {
	        return customerFile;
	    }
	    
		 public void setCustomerFile(CustomerFile customerFile) {
				this.customerFile = customerFile;
		} 
*/		 
	@Column( length=15 )
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getForwardDate() {
		return forwardDate;
	}
	public void setForwardDate(Date forwardDate) {
		this.forwardDate = forwardDate;
	}
	@Column( length=12 )
	public String getForwardStatus() {
		return forwardStatus;
	}
	public void setForwardStatus(String forwardStatus) {
		this.forwardStatus = forwardStatus;
	}
	@Column( length=50)
	public String getForwardToUser() {
		return forwardToUser;
	}
	public void setForwardToUser(String forwardToUser) {
		this.forwardToUser = forwardToUser;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	@Column( length=30 )
	public String getNoteSubType() {
		return noteSubType;
	}
	public void setNoteSubType(String noteSubType) {
		this.noteSubType = noteSubType;
	}
	@Column( length=20 )
	public String getNoteType() {
		return noteType;
	}
	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}
	@Column( length=100)
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(length=25 )
	public String getNotesId() {
		return notesId;
	}
	public void setNotesId(String notesId) {
		this.notesId = notesId;
	}
	@Column
	public Boolean getDisplayInvoice() {
		return displayInvoice;
	}
	public void setDisplayInvoice(Boolean displayInvoice) {
		this.displayInvoice = displayInvoice;
	}
	@Column
	public Boolean getDisplayPortal() {
		return displayPortal;
	}
	public void setDisplayPortal(Boolean displayPortal) {
		this.displayPortal = displayPortal;
	}
	@Column
	public Boolean getDisplaySurvey() {
		return displaySurvey;
	}
	public void setDisplaySurvey(Boolean displaySurvey) {
		this.displaySurvey = displaySurvey;
	}
	@Column
	public Boolean getDisplayTicket() {
		return displayTicket;
	}
	public void setDisplayTicket(Boolean displayTicket) {
		this.displayTicket = displayTicket;
	}
	@Column
	public String getNoteStatus() {
		return noteStatus;
	}
	public void setNoteStatus(String noteStatus) {
		this.noteStatus = noteStatus;
	}
	@Column( length=5 )
	public String getRemindTime() {
		return remindTime;
	}
	public void setRemindTime(String remindTime) {
		this.remindTime = remindTime;
	}
	@Column
	public Boolean getDisplayQuote() {
		return displayQuote;
	}
	public void setDisplayQuote(Boolean displayQuote) {
		this.displayQuote = displayQuote;
	}
	@Column( length=11 )
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	@Column( length=12 )
	public String getReminderStatus() {
		return reminderStatus;
	}
	public void setReminderStatus(String reminderStatus) {
		this.reminderStatus = reminderStatus;
	}
	@Column
	public Date getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(Date systemDate) {
		this.systemDate = systemDate;
	}
	@Column( length=5 )
	public String getFollowUpId() {
		return followUpId;
	}
	public void setFollowUpId(String followUpId) {
		this.followUpId = followUpId;
	}
	@Column
	public Boolean getAccPortal() {
		return accPortal;
	}
	public void setAccPortal(Boolean accPortal) {
		this.accPortal = accPortal;
	}
	@Column
	public Boolean getPartnerPortal() {
		return partnerPortal;
	}
	public void setPartnerPortal(Boolean partnerPortal) {
		this.partnerPortal = partnerPortal;
	}
	@Column( length=20 )
	public String getToDoRuleId() {
		return toDoRuleId;
	}
	public void setToDoRuleId(String toDoRuleId) {
		this.toDoRuleId = toDoRuleId;
	}
	
	@Column(length=20)
	public Long getNotesKeyId() {
		return notesKeyId;
	}
	public void setNotesKeyId(Long notesKeyId) {
		this.notesKeyId = notesKeyId;
	}
	@Column(length=20)
	public String getNoteGroup() {
		return noteGroup;
	}
	public void setNoteGroup(String noteGroup) {
		this.noteGroup = noteGroup;
	}
	/**
	 * @return the dateOfContact
	 */
	@Column
	public Date getDateOfContact() {
		return dateOfContact;
	}
	/**
	 * @param dateOfContact the dateOfContact to set
	 */
	public void setDateOfContact(Date dateOfContact) {
		this.dateOfContact = dateOfContact;
	}
	/**
	 * @return the notesActivity
	 */
	@Column(length=60)
	public String getNotesActivity() {
		return notesActivity;
	}
	/**
	 * @param notesActivity the notesActivity to set
	 */
	public void setNotesActivity(String notesActivity) {
		this.notesActivity = notesActivity;
	}
	@Column(length=161)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column
	public String getMyFileId() {
		return myFileId;
	}
	public void setMyFileId(String myFileId) {
		this.myFileId = myFileId;
	}
	
	@Column
	public String getLinkedTo() {
		return linkedTo;
	}
	public void setLinkedTo(String linkedTo) {
		this.linkedTo = linkedTo;
	}
	@Column
	public String getIssueType() {
		return issueType;
	}
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	@Column
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	@Column
	public String getGrading() {
		return grading;
	}
	public void setGrading(String grading) {
		this.grading = grading;
	}
	public Boolean getBookingAgent() {
		return bookingAgent;
	}
	public void setBookingAgent(Boolean bookingAgent) {
		this.bookingAgent = bookingAgent;
	}
	public Boolean getOriginAgent() {
		return originAgent;
	}
	public void setOriginAgent(Boolean originAgent) {
		this.originAgent = originAgent;
	}
	public Boolean getDestinationAgent() {
		return destinationAgent;
	}
	public void setDestinationAgent(Boolean destinationAgent) {
		this.destinationAgent = destinationAgent;
	}
	public Boolean getSubOriginAgent() {
		return subOriginAgent;
	}
	public void setSubOriginAgent(Boolean subOriginAgent) {
		this.subOriginAgent = subOriginAgent;
	}
	public Boolean getSubDestinationAgent() {
		return subDestinationAgent;
	}
	public void setSubDestinationAgent(Boolean subDestinationAgent) {
		this.subDestinationAgent = subDestinationAgent;
	}
	public Boolean getNetworkAgent() {
		return networkAgent;
	}
	public void setNetworkAgent(Boolean networkAgent) {
		this.networkAgent = networkAgent;
	}
	public String getNetworkLinkId() {
		return networkLinkId;
	}
	public void setNetworkLinkId(String networkLinkId) {
		this.networkLinkId = networkLinkId;
	}
	public Date getComplitionDate() {
		return complitionDate;
	}
	public void setComplitionDate(Date complitionDate) {
		this.complitionDate = complitionDate;
	}
	@Column(length=10)
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Column(length=10)
	public String getUvlSentStatus() {
		return uvlSentStatus;
	}
	public void setUvlSentStatus(String uvlSentStatus) {
		this.uvlSentStatus = uvlSentStatus;
	}
	@Column(length=20)
	public String getMemoUploadStatus() {
		return memoUploadStatus;
	}
	public void setMemoUploadStatus(String memoUploadStatus) {
		this.memoUploadStatus = memoUploadStatus;
	}
	@Column
	public Date getMemoLastUploadedDate() {
		return memoLastUploadedDate;
	}
	public void setMemoLastUploadedDate(Date memoLastUploadedDate) {
		this.memoLastUploadedDate = memoLastUploadedDate;
	}
	@Column
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	@Column
	public Date getSubmittedDate() {
		return submittedDate;
	}
	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}
	@Column
	public BigDecimal getActualValue() {
		return actualValue;
	}
	public void setActualValue(BigDecimal actualValue) {
		this.actualValue = actualValue;
	}
	@Column
	public BigDecimal getRequestedValue() {
		return requestedValue;
	}
	public void setRequestedValue(BigDecimal requestedValue) {
		this.requestedValue = requestedValue;
	}
	@Column
	public Boolean getNegativeComment() {
		return negativeComment;
	}
	public void setNegativeComment(Boolean negativeComment) {
		this.negativeComment = negativeComment;
	}
	@Column
	public Boolean getReportToAgent() {
		return reportToAgent;
	}
	public void setReportToAgent(Boolean reportToAgent) {
		this.reportToAgent = reportToAgent;
	}
	@Column
	public String getIssue() {
		return issue;
	}
	public void setIssue(String issue) {
		this.issue = issue;
	}
	@Column
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	@Column
	public String getRoleCausedBy() {
		return roleCausedBy;
	}
	public void setRoleCausedBy(String roleCausedBy) {
		this.roleCausedBy = roleCausedBy;
	}
	public String getBillToCode() {
		return billToCode;
	}
	
	public String getBookingAgentCode() {
		return bookingAgentCode;
	}
	public void setBookingAgentCode(String bookingAgentCode) {
		this.bookingAgentCode = bookingAgentCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}

}
