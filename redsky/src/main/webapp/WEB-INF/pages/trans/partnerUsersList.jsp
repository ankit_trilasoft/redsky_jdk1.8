<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="userList.title"/></title>
    <meta name="heading" content="<fmt:message key='userList.heading'/>"/>
    
  <script language="javascript" type="text/javascript">
  function clear_fields(){
	var i;
		for(i=0;i<=5;i++){
			document.forms['searchForm'].elements[i].value = "";
		} 
	}
	
		function userStatusCheck(targetElement){
	   var userCheckStatus;
    	if(targetElement.checked){
      		 userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['searchForm'].elements['userCheck'].value = targetElement.value;
      		}else{
      			 userCheckStatus=	document.forms['searchForm'].elements['userCheck'].value = userCheckStatus + ',' + targetElement.value;
      			document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
    	}
  	 	if(targetElement.checked==false){
     		 userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
     		 userCheckStatus=document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( targetElement.value , '' );
     		document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
   		}
   	var userCheckStatus1=	document.forms['searchForm'].elements['userCheck'].value;
   		if(userCheckStatus1!=''  ){
   		document.getElementById("disableUsers").disabled=false;
   		document.getElementById("enableUsers").disabled=false;
   		document.getElementById("deleteUsers").disabled=false;
   		document.getElementById("assignAdmins").disabled=false;
   		}
   		else{

   		document.getElementById("disableUsers").disabled=true;
   		document.getElementById("enableUsers").disabled=true;
   		document.getElementById("deleteUsers").disabled=true;
   		document.getElementById("assignAdmins").disabled=true;
   		}
}

function disableUser()
{
	var userCheck=document.forms['searchForm'].elements['userCheck'].value;
	var partnerId=document.forms['searchForm'].elements['partnerId'].value;
	agree= confirm("Are you sure you want to disable User?");
		if(agree){
		location.href = 'disableFromPartner.html?id=${id}&partnerType=${partnerType}&partnerId='+partnerId+'&userCheck='+userCheck;
		}
		else{
			return false;
		}
}

function enableUser1()
{
    
	var userCheck=document.forms['searchForm'].elements['userCheck'].value;
	var partnerId=document.forms['searchForm'].elements['partnerId'].value;
	agree= confirm("Are you sure you want to enable User?");
		if(agree){
		location.href = 'enableFromPartner.html?id=${id}&partnerType=${partnerType}&partnerId='+partnerId+'&userCheck='+userCheck;
		}
		else{
			return false;
		}
}

function deleteUser1()
{
	var userCheck=document.forms['searchForm'].elements['userCheck'].value;
	var partnerId=document.forms['searchForm'].elements['partnerId'].value;
	agree= confirm("Are you sure you want to delete User?");
		if(agree){
		location.href = 'deleteUserFromPartner.html?id=${id}&partnerType=${partnerType}&partnerId='+partnerId+'&userCheck='+userCheck;
		}
		else{
			return false;
		}
}

function assignAdmin1()
{
	var userCheck=document.forms['searchForm'].elements['userCheck'].value;
	var partnerId=document.forms['searchForm'].elements['partnerId'].value;
	agree= confirm("Are you sure you want to assign admin role?");
		if(agree){
		location.href = 'assignAdminRoleFromPartner.html?id=${id}&partnerType=${partnerType}&partnerId='+partnerId+'&userCheck='+userCheck;
		}
		else{
			return false;
		}
}

function findUserRole(setName,position) { 
  var url="findUserRole.html?ajax=1&decorator=simple&popup=true&buttonType=invoice&userName=" + encodeURI(setName);
  ajax_showTooltip(url,position);	
  }
 function findUserPermission(userName,position) { 
  var url="findUserPermission.html?ajax=1&decorator=simple&popup=true&buttonType=invoice&userNamePermission=" + encodeURI(userName);
  ajax_showTooltip(url,position);	
  }
 function pickAllList(){
	 
	 var userCheck = document.getElementById("userList");
	 var contactCheck = document.getElementById("contactList");
	 if(userCheck.checked==true)
	 {
		 if(contactCheck.checked==true)
		 { 	
			 document.forms['searchForm'].elements['checkOption'].value='BOTH';
			 document.forms['searchForm'].elements['id'].value='${partnerPublic.id}';
	         document.forms['searchForm'].action ='partnerUsersList.html';
		     document.forms['searchForm'].submit();			 
		 }else{
			 document.forms['searchForm'].elements['checkOption'].value='USER';
			 document.forms['searchForm'].elements['id'].value='${partnerPublic.id}';
	         document.forms['searchForm'].action ='partnerUsersList.html';
		     document.forms['searchForm'].submit();				 
		 }
	 }else{
		 if(contactCheck.checked==true)
		 {	
			 document.forms['searchForm'].elements['checkOption'].value='CONTAC';
			 document.forms['searchForm'].elements['id'].value='${partnerPublic.id}';
	         document.forms['searchForm'].action ='partnerUsersList.html';
		     document.forms['searchForm'].submit();				 
		 }else{
			 document.forms['searchForm'].elements['checkOption'].value='';
			 document.forms['searchForm'].elements['id'].value='${partnerPublic.id}';
	         document.forms['searchForm'].action ='partnerUsersList.html';
		     document.forms['searchForm'].submit();				 
		 }
	 }
 }
 function disabledAll(){
		var elementsLen=document.forms['searchForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['searchForm'].elements[i].type=='text'){
				document.forms['searchForm'].elements[i].readOnly =true;
				document.forms['searchForm'].elements[i].className = 'input-textUpper';
			}else if(document.forms['searchForm'].elements[i].type=='textarea'){
				document.forms['searchForm'].elements[i].readOnly =true;
				document.forms['searchForm'].elements[i].className = 'textareaUpper';
			}else{
				document.forms['searchForm'].elements[i].disabled=true;
			} 
		}
		
	}
</script>

<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-12px;
padding:0px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head>

<c:set var="buttons">
<c:if test="${partnerType == 'AC'}">
    <input type="button" class="cssbuttonA" style="width: 100px" onclick="location.href='<c:url value="/addUsersFromPartner.html?id=${id}&partnerType=${partnerType}&method=Add&from=list"/>'" value="Add New User"/>
</c:if>
 <sec-auth:authComponent componentId="module.tab.partner.AgentDEV">		
	<c:if test="${partnerType == 'AG' }">
	    <c:if test="${disableUserContract }">
	    <input type="button" class="cssbuttonA" style="width: 150px" onclick="" disabled="disabled" value="Add New User/Contact"/>
	    </c:if>
	    <c:if test="${!disableUserContract }">
	    <input type="button" class="cssbuttonA" style="width: 150px" onclick="location.href='<c:url value="/addUsersFromPartner.html?id=${id}&partnerType=${partnerType}&method=Add&from=list"/>'" value="Add New User/Contact"/>
	    </c:if>
	</c:if>
 </sec-auth:authComponent>
  <configByCorp:fieldVisibility componentId="component.user.venderAccess.showHSRG">		
	<c:if test="${partnerType == 'VN' }">
	    <input type="button" class="cssbuttonA" style="width: 150px" onclick="location.href='<c:url value="/addUsersFromPartner.html?id=${id}&partnerType=${partnerType}&method=Add&from=list&agentContact=true"/>'" value="Add New Vender Contact"/>
  </c:if>
 </configByCorp:fieldVisibility>
    <sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
	    <input type="button" class="cssbuttonA" style="width: 100px" onclick="return disableUser();" value="Disable User" id="disableUsers" disabled="disabled"/>
	    <input type="button" class="cssbuttonA" style="width: 100px" onclick="return enableUser1()" value="Enable User" name="enableUser" id="enableUsers" disabled="disabled"/>
	    <%-- <c:if test="${partnerType != 'AG'}">
	    <input type="button" class="cssbuttonA" style="width: 100px" onclick="return deleteUser1();" value="Delete User" disabled="disabled" name="deleteUsers" id="deleteUsers"/>
	    </c:if>
	    <c:if test="${partnerType == 'AG'}">
	    <input type="button" class="cssbuttonA" style="width: 150px" onclick="return deleteUser1();" value="Delete New User/Contact" disabled="disabled" name="deleteUsers" id="deleteUsers"/>
	    </c:if> --%>
	    <c:if test="${partnerType == 'AG'}">
	    	<input type="button" class="cssbuttonA" style="width: 100px" onclick="return assignAdmin1();" value="Assign Admin" name="assignAdmin" disabled="disabled" id="assignAdmins" />
   		</c:if>
    </sec-auth:authComponent>
    <c:if test="${partnerType == 'AG'}">
     <input type="button" name="AGContact" class="cssbutton" style="margin-right: 5px; width:150px;" 
        onclick="location.href='<c:url value="/addUsersFromPartner.html?id=${id}&partnerType=${partnerType}&method=Add&from=list&agentContact=true"/>'" 
        value="Add New Agent Contacts"/> 
   </c:if>
</c:set>

<s:form id="searchForm" action="" method="post" validate="true">
<c:set var="id" value="<%= request.getParameter("id")%>" />
<s:hidden name="id" id="testId" value="<%= request.getParameter("id")%>" />	
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" id="partnerType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="partnerId" value="<%= request.getParameter("id")%>" />
<s:hidden name="partnerId" value="<%= request.getParameter("id")%>" />
<s:hidden name="userCheck"/>	
<!-- <c:out value="${buttons}" escapeXml="false" /> -->
<s:hidden name="checkOption" id="checkOption" value="${checkOption}"/>
<c:if test="${partnerType == 'AG'}">
<div id="priceinfotab_bg" style="border:2px solid #9BBEF7;height:25px;margin-bottom:5px;width:100%;">
		  	<table cellpadding="0" cellspacing="0" style="margin:2px 0px 0px 10px;padding:0px;">
		  	<tr>  	
		  	<td align="right"><input type="checkbox" id="contactList"  value="${isContact1}" onclick="pickAllList();" fieldValue="true"/></td>
		  	<td class="listwhitetext" align="left">Display Contacts Only</td>
		  	<td class="listwhitetext" align="left" width="20"></td>
		  	<td align="right"><input type="checkbox" id="userList"  value="${isUser1}" onclick="pickAllList();" fieldValue="true"/></td>
		  	<td class="listwhitetext" align="left">Display Users Only</td>
		  	</tr>		  
		  	</table></div>
		  	</c:if>
<div id="newmnav" style="margin-left:20px;">
<ul>
	  	<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
	  		<c:if test="${partnerType == 'AG'}">
	  		<li><a href="editPartnerPublic.html?partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Detail</span></a></li>
	  		</c:if>
	  		<c:if test="${partnerType == 'AC'}">
	  		<li><a href="editPartnerPublic.html?partnerType=${partnerType}&id=${partnerPublic.id}"><span>Account Detail</span></a></li>
	  		</c:if>
 			<c:if test="${partnerType == 'VN'}">
	  		<li><a href="editPartnerPublic.html?partnerType=${partnerType}&id=${partnerPublic.id}"><span>Vendor Detail</span></a></li>
	  		</c:if>
	  		<%-- Added By kunal for ticket number: 6176 --%>
	  			<c:if test="${not empty partnerPublic.id}">
					<configByCorp:fieldVisibility componentId="component.button.partnverPublicScript">
						<c:if test="${partnerType == 'AC'}">
							<li><a href="editNewAccountProfile.html?id=${partnerPublic.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
						</c:if>
						<c:if test="${partnerType == 'AG'}">
							<li><a href="findPartnerProfileList.html?code=${partnerPublic.partnerCode}&partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Profile</span></a></li>
						</c:if>
						<%-- Working default means before working on ticket number: 6176 --%>
						<c:if test="${sessionCorpID!='TSFT' }">
						<c:if test="${partnerType == 'AC' || partnerType == 'AG' || partnerType == 'VN'}">
							<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
						</c:if>
						</c:if>
					</configByCorp:fieldVisibility>
				</c:if>
				<%-- Added By Kunal Sharma for ticket number: 6176 --%>
					<c:if test="${partnerType == 'AC'}">
					<c:if test="${not empty partnerPublic.id}">
						<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
						<configByCorp:fieldVisibility componentId="component.standard.accountContactTab"><li><a href="accountContactList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Account Contact</span></a></li></configByCorp:fieldVisibility>
						<c:if test="${checkTransfereeInfopackage==true}">
						<li><a href="editContractPolicy.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
						</c:if>
					</c:if>
				
				</c:if>
			<%-- Modification closed here for ticket number: 6176 --%>
		  	<c:if test="${partnerType == 'AC'}">
		  	<%-- working as default means before working on ticket number: 6176 --%>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Portal Users<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	<%-- Added By kunal for ticket number: 6176 --%>
		  		<c:if test="${paramValue == 'View'}">
					<li><a href="searchPartnerView.html"><span>Public List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC'}">
					<li><a href="partnerReloSvcs.html?id=${partnerId}&partnerType=${partnerType}"><span>Services</span></a></li>
				</c:if>
				 <c:if test="${not empty partnerPublic.id}">
				<c:url value="frequentlyAskedQuestionsList.html" var="url">
				<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
				<c:param name="partnerType" value="AC"/>
				<c:param name="partnerId" value="${partnerPublic.id}"/>
				<c:param name="lastName" value="${partnerPublic.lastName}"/>
				<c:param name="status" value="${partnerPublic.status}"/>
				</c:url>				 
				<li><a href="${url}"><span>FAQ</span></a></li>
				  </c:if>
			<%-- Modification closed here for ticket number: 6176 --%>
		  	<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
		  	<li><a href="partnerUserUnassignedList.html?id=${id}&partnerType=${partnerType}"><span>Unassigned Portal Users</span></a></li>
		  	</c:if>
		  	<c:if test="${partnerType == 'AG'}">
				<c:if test="${not empty partnerPublic.id}">
					<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
					<li><a href="baseList.html?id=${partnerPublic.id}"><span>Base</span></a></li>
					<c:if test="${paramValue == 'View'}">
						<li><a href="partnerView.html"><span>Partner List</span></a></li>
					</c:if>
					<c:if test="${paramValue != 'View'}">
						<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
					</c:if>	
					<!--<c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
						<li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>
					</c:if>
					<c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
						<li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
					</c:if>
				  	-->
				  	<li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
			   
			   </c:if>
			  	<li id="newmnav1" style="background:#FFF;"><a class="current" style="margin-bottom:0px;"><span>Portal Users & Contacts<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
			  	<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
			  	</configByCorp:fieldVisibility>
			  	<!--<li><a href="partnerUserUnassignedList.html?id=${id}&partnerType=${partnerType}"><span>Unassigned Agent Users</span></a></li>
			  	-->
			  <sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
			  	<li><a href="requestedPartnerAccessList.html?id=${id}&partnerType=${partnerType}"><span>Requested Partner Access</span></a></li>
	  	      </sec-auth:authComponent>
	  	    </c:if>
	  	   
	  	</sec-auth:authComponent>
	   <configByCorp:fieldVisibility componentId="component.user.venderAccess.showHSRG">		
	    <c:if test="${partnerType == 'VN' }">
	    <li id="newmnav1" style="background:#FFF;"><a class="current" style="margin-bottom:0px;"><span>Portal Users & Contacts<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	   
	            <c:if test="${paramValue == 'View'}">
					<li><a href="searchPartnerView.html"><span>Public List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC'}">
					<li><a href="partnerReloSvcs.html?id=${partnerId}&partnerType=${partnerType}"><span>Services</span></a></li>
				</c:if>
	   
	   </c:if>
	   </configByCorp:fieldVisibility>
	  	<sec-auth:authComponent componentId="module.tab.partner.AgentListTab">
		   	<li><a href="editPartnerPublic.html?partnerType=AG&id=${partnerPublic.id}"><span>Agent Details</span></a></li>
		   	<li><a href="findPartnerProfileList.html?code=${partnerPublic.partnerCode}&partnerType=AG&id=${partnerPublic.id}"><span>Agent Profile</span></a></li>
 			<c:if test="${partnerType == 'AC'}">
 			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Portal Users<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		   	</c:if><!--
		   	<c:if test="${partnerType == 'AG'}">
		   	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Portal Users & contacts<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		   	</c:if>
		   	--><!--<c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
              	<li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
            </c:if>
            <c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
             	<li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
            </c:if>
		   	
		    --><li><a href="partnerAgent.html"><span>Agent List</span></a></li>
		</sec-auth:authComponent>
	</ul>
	</div><div class="spn">&nbsp;</div>
<display:table name="partnerUsersList" requestURI="" defaultsort="2" id="userList" class="table" pagesize="10" style="width:100%;!margin-top:8px;" >
    <sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
    	<display:column title=" " style="width:5%"><input type="checkbox" style="margin-left:10px;" id="checkboxId" name="DD" value="${userList.id}" onclick="userStatusCheck(this)"/></display:column>
    </sec-auth:authComponent>
    <display:column  sortable="true" title="User Name" style="width: 15%">
    	<a href="addUsersFromPartner.html?id=${id}&userId=${userList.id}&partnerType=${partnerType}"><c:out value="${userList.username}"></c:out></a>
   </display:column>
    <display:column property="first_name"  sortable="true" title="First Name" style="width: 15%"/>
    <display:column property="last_name"  sortable="true" title="Last Name" style="width: 15%"/>
    <display:column  sortable="true" title="Portal Access Enabled" style="width: 15%">
    <c:if test="${userList.account_enabled==true }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
	<c:if test="${userList.account_enabled==false }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>
	<c:if test="${partnerType=='AC'}">
	<display:column title="Filter Sets" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserPermission('${userList.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	<display:column title="Roles" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserRole('${userList.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	</c:if>
	<c:if test="${partnerType=='AG' && sessionCorpID=='TSFT'}">
	<display:column title="Filter Sets" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserPermission('${userList.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	<display:column title="Roles" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserRole('${userList.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	</c:if>
	<c:if test="${partnerType=='AG' && sessionCorpID!='TSFT'}">
	<display:column title="Filter Sets" style="width: 15px;">
		<a><img align="middle" title="User List"  style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	<display:column title="Roles" style="width: 15px;">
		<a><img align="middle" title="User List"  style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	</c:if>
	 <display:column property="userType"  sortable="true" title="User Type" style="width: 15%"/>
	</display:column>
	 <configByCorp:fieldVisibility componentId="component.user.venderAccess.showHSRG">		
	<display:column sortable="true" title="Relocation Contact" style="width: 15%">
	    <c:if test="${userList.relocationContact==true }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
	<c:if test="${userList.relocationContact==false }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>
	 </display:column>
	 </configByCorp:fieldVisibility>
	<c:if test="${partnerType=='AG' && sessionCorpID=='TSFT' }">
	<display:column title="Email" style="width:45px;font-size: 9px;" >
			<a href="sendEmailToAgents.html?partnerId=${id}&userId=${userList.id}&partnerType=AG"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Email</a>    
	</display:column>
	</c:if>
	<c:if test="${partnerType=='AG' && sessionCorpID!='TSFT' }">
	<display:column title="Email" style="width:45px;font-size: 9px;" >
			<img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Email    
	</display:column>
	</c:if>
    <display:setProperty name="export.excel.filename" value="User List.xls"/>
    <display:setProperty name="export.csv.filename" value="User List.csv"/>
    <display:setProperty name="export.pdf.filename" value="User List.pdf"/>
</display:table>
<c:out value="${buttons}" escapeXml="false" /> 
</s:form>
<script type="text/javascript">
   // highlightTableRows("users");
			<c:if test="${checkOption==null || checkOption==''}">
							document.getElementById("userList").checked=false;
							document.getElementById("contactList").checked=false;							
			</c:if>
			<c:if test="${checkOption=='BOTH'}">
							document.getElementById("userList").checked=true;
							document.getElementById("contactList").checked=true;							
			</c:if>
		  	<c:if test="${checkOption=='CONTAC'}">
							document.getElementById("userList").checked=false;
							document.getElementById("contactList").checked=true;							
			</c:if>
		  	<c:if test="${checkOption=='USER'}">
							document.getElementById("userList").checked=true;
							document.getElementById("contactList").checked=false;							
			</c:if>
			
</script>
<script type="text/javascript">
try{
	<c:if test="${partnerType=='AG'}">
	var checkCorpId = 0;
	<configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
	checkCorpId  = 14;
	</configByCorp:fieldVisibility>
	if(checkCorpId < 14){
		disabledAll();
	document.forms['searchForm'].elements['testId'].disabled=false;
	document.forms['searchForm'].elements['contactList'].disabled=false;
	document.forms['searchForm'].elements['userList'].disabled=false;
	document.forms['searchForm'].elements['AGContact'].disabled=false;
	document.forms['searchForm'].elements['checkOption'].disabled=false;
	document.forms['searchForm'].elements['partnerType'].disabled=false;
	
	}else{
	document.forms['searchForm'].elements['AGContact'].disabled=true;
	}	
}catch(e){
	
}


</c:if>
</script>
