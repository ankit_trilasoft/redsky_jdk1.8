package com.trilasoft.app.service;


import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.StorageLibrary;

public interface StorageLibraryManager  extends GenericManager<StorageLibrary, Long>{

public List storageLibrarySearch(String storageId, String storageType,String storageMode);

public List storageLibraryList(String sessionCorpID);

public List findStorageLibraryList(String sessionCorpID);

public List storageLibraryUnitSearch(String storageId, String storageType,String storageMode);
public StorageLibrary getByStorageId(String storageId,String sessionCorpID);

public List findStoLocRearrangeList(String sessionCorpID);

public List searchStoLocRearrangeList(String sessionCorpID, String locationId, String ticket);

}
