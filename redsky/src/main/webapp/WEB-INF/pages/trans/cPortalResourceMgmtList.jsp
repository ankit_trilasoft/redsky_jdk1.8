<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="cPortalResourceMgmtList.title"/></title>   
    <meta name="heading" content="<fmt:message key='cPortalResourceMgmtList.heading'/>"/>  
    <style>
span.pagelinks {display:block;font-size:0.85em;margin-bottom:1px;margin-top:-12px;!margin-top:-10px;padding:0px;text-align:right;width:100%;}
form {margin-top:-40px;!margin-top:0px;}
div#main {margin:-5px 0 0;}

 </style> 

</head>
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editCPortalResourceMgmt.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  

<s:form name="cPortalResourceMgmts" action="searchcPortalResourceMgmts">
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:58px;" align="top"  key="button.search" />   
    <input type="button" class="cssbutton1" value="Clear" style="width:58px;" onclick="clear_fields();"/> 
</c:set>
<div id="otabs"><ul>
	<c:url value="frequentlyAskedList.html" var="url">
	</c:url>
	<c:url value="contractFilePolicy.html" var="url11"/>
<li><a class="current"><span>Cportal Docs</span></a></li>
<li><a href="${url11}"><span>Policy</span></a></li>
<li><a href="${url}"><span>FAQ</span></a></li>
</ul></div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 11px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
	<table class="table" style="width:100%;"  >
	<thead>
<tr>
<th>Document Type</th>
<th>Description</th>
<th>Document</th>
<th>Origin Country</th>
<th>Destination Country</th>
<th>Bill To Code</th>
<th>Bill To Name</th>
</tr></thead>	
		<tbody>
			<tr>	
			<td width="">
			    <s:textfield name="cportalResource.documentType" id="documentType" size="15" required="true" cssClass="input-text"/>
			</td>
			<td width="">
			    <s:textfield name="cportalResource.documentName" id="documentName" size="15" required="true" cssClass="input-text" />
			</td>
			<td width="">
			    <s:textfield name="cportalResource.fileFileName" id="fileFileName" size="15" required="true" cssClass="input-text" />
			</td>
			<td width="">
			   <s:select cssClass="list-menu" name="cportalResource.originCountry" id="originCountry" list="%{ocountry}" cssStyle="width:130px"  headerKey="" headerValue=""   />
			</td>
			<td width="">
			  <s:select cssClass="list-menu" name="cportalResource.destinationCountry" id="destinationCountry" list="%{dcountry}" cssStyle="width:130px"  headerKey="" headerValue=""   />
			</td>	
				<td width="">
			    <s:textfield name="cportalResource.billToCode" size="15" required="true"  id="billToCode" cssClass="input-text"/>
			</td>
			<td width="">
			    <s:textfield name="cportalResource.billToName" size="15" required="true" id="billToName" cssClass="input-text" />
			</td>
		</tr>
		<tr>
		<td  colspan="6" ></td>
			
			<td width="" ><c:out value="${searchbuttons}" escapeXml="false"/></td>
		
		</tr>
			
		</tbody>
	</table>
	</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
<div id="layer1" style="width:100%;">
<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF"><a class="current"><span>CPortal&nbsp;ResourceMgmt&nbsp;List</span></a></li>
		    <li><a href="resourceExtractInfo.html" /><span>Resource&nbsp;Extract</span></a></li>
		    <configByCorp:fieldVisibility componentId="component.field.document.documentbundleview">
		    <li><a href="documentBundleList.html?tabId=setUp" /><span>Document&nbsp;Bundle</span></a></li>
		    </configByCorp:fieldVisibility>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
<s:set name="cportalResourceList" value="cportalResourceList" scope="request"/> 
<display:table name="cportalResourceList" class="table" requestURI="" id="cPortalResourceMgmtList" defaultsort="1" pagesize="10" style="100%;margin-top:1px;!margin-top:5px;">
	    
	    <display:column sortable="true" property="documentType" title="Document Type" href="editCPortalResourceMgmt.html" paramId="id" paramProperty="id" style="width:200px" >
	    </display:column>
	    <display:column sortable="true" property="documentName" title="Description" maxLength="20" style="width:200px;"/>
	     <display:column sortable="true" property="fileFileName" title="Document" maxLength="20" style="width:120px;"/>
	     <display:column sortable="true" property="language" title="Language" maxLength="20" style="width:120px;"/>
	     <display:column sortable="true" property="infoPackage" title="Info Package" maxLength="20" style="width:120px;"/>
	   	<display:column sortable="true" title="Job&nbsp;Type"  property="jobType" style="width:120px;">
			<%-- <c:forEach var="entry" items="${jobType}">
				<c:if test="${cPortalResourceMgmtList.jobType==entry.key}">
					<c:out value="${entry.value}" />
				</c:if>
			</c:forEach> --%>
		</display:column>
	    <display:column sortable="true" property="originCountry" titleKey="cPortalResourceMgmt.originCountry" maxLength="20" style="width:120px;"/>
	    <display:column sortable="true" property="destinationCountry" titleKey="cPortalResourceMgmt.destinationCountry" maxLength="20" style="width:120px;"/>
	    
	    <display:column sortable="true" property="billToCode" titleKey="cPortalResourceMgmt.billToCode" maxLength="20" style="width:150px;"/>
	    <display:column sortable="true" property="billToName" titleKey="cPortalResourceMgmt.billToName" maxLength="20" style="width:150px;"/>
	    <display:column title="Remove" style="width:45px;">
				    <a><img align="middle" onclick="confirmSubmit(${cPortalResourceMgmtList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
				    </display:column>				
					
    </display:table>  
</div>
</s:form>
<c:out value="${buttons}" escapeXml="false" />   

<SCRIPT type="text/javascript">
  function clear_fields(){  		    	
	document.getElementById('documentType').value= "";
   	document.getElementById('documentName').value ="";
    document.getElementById('fileFileName').value = "";
    document.getElementById('originCountry').value ="";
  document.getElementById('destinationCountry').value ="";
  document.getElementById('billToCode').value ="";
  document.getElementById('billToName').value ="";
}
    function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this CPortal ResourceMgmt Doc?");
	var did = targetElement;
	if (agree){
		location.href="deleteResourceDoc.html?id="+did+"";
	}
	else{
		return false;
	}
}

function autoPopulate_Country(targetElement) {		
		var country = targetElement.value;
		if(country == 'India' || country == 'Canada' || country == 'United States' ){
			
			document.getElementById('originCountry').disabled = false;
		}else{
			document.getElementById('originCountry').disabled = true;
			document.getElementById('originCountry').value ="";
		}
}
</SCRIPT>