package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;


import com.trilasoft.app.model.RedskyBilling;

public interface RedskyBillingManager extends GenericManager<RedskyBilling, Long> {

	public List putRedskyBillingList(String sessionCorpID);

	public List gettotalCountOfSo(String corpID, int year);

	public List getRedskyBillingList(String corpID, int year);

	public List getRedskyBillingListDetails(String corpID, int month, int year);
	public String checkByShipNumber(String corpID,String shipNumber);
	
	public int getDetailsByProcedure(int proc, Date date);
}
