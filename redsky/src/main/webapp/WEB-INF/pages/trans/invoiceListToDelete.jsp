<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="invoiceToDeleteList.title"/></title>   
    <meta name="heading" content="<fmt:message key='invoiceToDeleteList.heading'/>"/> 
    <style type="text/css"> 

.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style> 
	
<script type="text/javascript">

    function pick() {
  		parent.window.opener.document.location.reload();
  		window.close();
	} 
	
	function fillReverseInvoice(){
	   var sid = document.forms['invoiceToDeleteList'].elements['sid'].value; 
       var invoiceNumber = valButton(document.forms['invoiceToDeleteList'].elements['radiobilling']);   
	    if (invoiceNumber == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       } 
	       else 
	       { 
	        invoiceNumber = invoiceNumber.replace('/',''); 
	        document.forms['invoiceToDeleteList'].elements['invoiceToDelete'].value=invoiceNumber; 
	        var agree = confirm("Press OK to proceed, or press Cancel.");
            if(agree)
             { 
               document.forms['invoiceToDeleteList'].elements['idCheck'].value="";
               document.forms['invoiceToDeleteList'].action ='getInvoiceToDeleteDitail.html?&detailListButton=yes&decorator=popup&popup=true';
               document.forms['invoiceToDeleteList'].submit(); 
             }
             else {
             return false; 
             } 
           }  
      }
   
   
    function pick() {
  		parent.window.opener.document.location.reload();
  		window.close();
	}
   
   function valButton(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
    	return document.forms['invoiceToDeleteList'].elements['radiobilling'].value; 
    } 
  }
   
   function invoiceStatusCheck(rowId,targetElement) 
   {  
   //alert(rowId);
   //alert(targetElement);
   var checkboxCount = 0; 
   checkboxCount = document.forms['invoiceToDeleteList'].elements['checkboxCount'].value;
   if(targetElement.checked)
     {
      var userCheckStatus = document.forms['invoiceToDeleteList'].elements['idCheck'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['invoiceToDeleteList'].elements['idCheck'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['invoiceToDeleteList'].elements['idCheck'].value = userCheckStatus + ',' + rowId;
      document.forms['invoiceToDeleteList'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
      }
      checkboxCount = eval(checkboxCount+"+1");
      document.forms['invoiceToDeleteList'].elements['checkboxCount'].value = checkboxCount;
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['invoiceToDeleteList'].elements['idCheck'].value;
     var userCheckStatus=document.forms['invoiceToDeleteList'].elements['idCheck'].value = userCheckStatus.replace( rowId , '' );
     document.forms['invoiceToDeleteList'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
     document.forms['invoiceToDeleteList'].elements['checkboxCount'].value=eval(checkboxCount+"-1")
     }
     invoiceCheck();
     
    }
    
    function invoiceCheck(){
    var invoiceCheck = document.forms['invoiceToDeleteList'].elements['idCheck'].value;
    invoiceCheck=invoiceCheck.trim(); 
    if(invoiceCheck=='')
    {
    document.forms['invoiceToDeleteList'].elements['deleteSelectedInvoices'].disabled=true;
    }
    else if(invoiceCheck==',')
    {
    document.forms['invoiceToDeleteList'].elements['deleteSelectedInvoices'].disabled=true;
    }
    else if(invoiceCheck!='')
    {
      document.forms['invoiceToDeleteList'].elements['deleteSelectedInvoices'].disabled=false;
    }
    }
      
  function check_all(chk) {
   var checkboxCount = document.forms['invoiceToDeleteList'].elements['checkboxCount'].value; 
   if(chk.length==undefined || checkboxCount==chk.length){
   alert("You can not delete all invoice line.")
   }
   else if(checkboxCount!=chk.length){
           document.forms['invoiceToDeleteList'].action ='updateInvoiceToDelete.html?&btntype=yes&decorator=popup&popup=true';
           document.forms['invoiceToDeleteList'].submit(); 
   
   }
    
}
  </script>  
</head>
<s:form id="invoiceToDeleteList" name="invoiceToDeleteList" action="" method="post">  
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/> 
<s:hidden name="detailListButton" value="<%=request.getParameter("detailListButton") %>"/> 
<s:hidden name="idCheck" />  
<s:hidden name="invoiceToDelete" /> 
<s:hidden name="checkboxCount" /> 
<s:hidden name="shipNumber" value="${shipNumber}"/> 
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:set name="invoiceToDeleteList" value="invoiceToDeleteList" scope="request"/> 
<display:table name="invoiceToDeleteList" class="table" requestURI="" id="invoiceToDeleteList" style="width:300px" defaultsort="1" pagesize="100" >   
 		
		<display:column property="recInvoiceNumber" sortable="true" title="Invoice#" style="width:70px" />
		 <display:column property="billToName" sortable="true" title="Bill To" maxLength="15"  style="width:120px"/> 
	   <display:column   title="" style="width:10px" >
 		<input style="vertical-align:bottom;" type="radio" name="radiobilling" id=${invoiceToDeleteList.recInvoiceNumber} value=${invoiceToDeleteList.recInvoiceNumber}/>
 		</display:column>
	     
</display:table> 
<table> 
<tr>
<td>
<input type="button" name="Submit" onclick="return fillReverseInvoice();" value="Check Invoice Detail" class="cssbuttonA" style="width:170px; height:25px" >
</td>
<td>
<input type="button" name="Submit"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();">
</td>
</tr>
</table> 
<div id="invoiceDetailListDiv" style="align:center" >

<c:if test="${invoiceDetailList!='[]'}">  
<s:set name="invoiceDetailList" value="invoiceDetailList" scope="request"/>  
<display:table name="invoiceDetailList" class="table" requestURI="" id="invoiceDetailList" style="width:100%"  pagesize="100" >   
 		<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:70px" />
		<display:column  sortable="true"   title="Actual Revenue" style="width:30px">
		    <div align="right"><fmt:formatNumber type="number"  
            value="${invoiceDetailList.actualRevenue}"  maxFractionDigits="2" groupingUsed="true" minFractionDigits="2"/>
           </div>
       </display:column> 
       <display:column property="recRateCurrency" sortable="true" title="Currency"  style="width:50px" /> 
	   <display:column property="receivedInvoiceDate" sortable="true" title="Invoice Date"  style="width:100px"  format="{0,date,dd-MMM-yy}"/>
       <display:column  property="billToName"  sortable="true" title="Bill&nbsp;To" maxLength="28" style="width:100px"/>
       <display:column property="description" sortable="true" title="Description"  maxLength="20"  style="width:60px;"/>
       <display:column title=""   style="width:10px"> 
	     <input type="checkbox"  style="margin-left:10px;" name="check_list"  onclick="invoiceStatusCheck('${invoiceDetailList.id}',this)" />
        </display:column>
 </display:table>  
</c:if> 
<input type="button" name="deleteSelectedInvoices"  value="Delete Selected Invoices" class="cssbuttonA" style="width:170px; height:25px"  onclick="check_all(document.invoiceToDeleteList.check_list);">
<input type="button" name="Submit"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();"> </td>
</div> 
</s:form>
<script type="text/javascript">   


if(document.forms['invoiceToDeleteList'].elements['detailListButton'].value=='yes'){ 
document.getElementById("invoiceDetailListDiv").style.display="block"; 
}
if(document.forms['invoiceToDeleteList'].elements['detailListButton'].value!='yes'){ 
document.getElementById("invoiceDetailListDiv").style.display="none"; 
}
invoiceCheck();
if(document.forms['invoiceToDeleteList'].elements['btntype'].value=='yes'){
pick();
}   
document.forms['invoiceToDeleteList'].elements['radiobilling'].checked=true;
</script>  
		  	