<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="findVendorInviceList.title"/></title>   
    <meta name="heading" content="<fmt:message key='findVendorInviceList.heading'/>"/> 
 <script language="javascript" type="text/javascript"> 
  function clear_fields(){			    
   document.forms['searchForm'].elements['accountLine.invoiceNumber'].value = "";
   document.forms['searchForm'].elements['accountLine.estimateVendorName'].value = "";
   document.forms['searchForm'].elements['accountLine.shipNumber'].value = "";
   <configByCorp:fieldVisibility componentId="component.finance.recInvoiceDate">
   document.forms['searchForm'].elements['accountLine.invoiceDate'].value = "";
   </configByCorp:fieldVisibility>
}
function lineInvoice(lineElement){
lineElement.value=lineElement.value.trim();
}
</script> 
 <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<style>
span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-19px;
!margin-top:-20px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
font-size:.85em;
}

div.error, span.error, li.error, div.message {
width:450px;
}
form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
.listwhitetextWhite {
    background-color: #FFFFFF;
    color: #003366;
    font-family: arial,verdana;
    font-size: 11px;
   }
</style>       
</head>
<script type="text/javascript">
try{	
	 <c:set var="externalRoleId" value="0"/>
	<c:set var="internalRoleId" value="0"/> 
	<sec-auth:authComponent componentId="module.link.vendorInvoice.external"> 
		<c:set var="externalRoleId" value="14"/>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.link.vendorInvoice.internal"> 
		<c:set var="internalRoleId" value="14"/>
	</sec-auth:authComponent>
} catch(e){}

</script>
<s:form id="searchForm"  name="searchForm" action="searchAgentPayInviceList" method="post" validate="true" >
<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/> 
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px;" align="top" method="searchAgentPayInviceList" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
		<table class="table" style="width:100%;">
		<thead>
		<tr>
		<th>Vendor Invoice Number</th>
		<th>Vendor's Name</th> 
		<th>SO Number</th>
	    <th>&nbsp;</th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="accountLine.invoiceNumber" required="true" cssClass="input-text" size="30" onchange="lineInvoice(this)"  />
					</td>
					<td width="" align="left">
					    <s:textfield name="accountLine.estimateVendorName" required="true"  cssClass="input-text" size="30" onchange="lineInvoice(this)" />
					</td>
					
					<td width="" align="left">
					    <s:textfield name="accountLine.shipNumber" required="true" cssClass="input-text" size="30" onchange="lineInvoice(this)" />
					</td>
					<configByCorp:fieldVisibility componentId="component.finance.recInvoiceDate">
					<c:if test="${not empty accountLine.invoiceDate}">
				<s:text id="invoiceDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.invoiceDate" /></s:text>
				<td align="left" style="">
				<s:textfield id="invoiceDate" name="accountLine.invoiceDate" value="%{invoiceDateFormattedValue}" cssStyle="width:65px" cssClass="input-text" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"  />
				
				<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
				</c:if>
				<c:if test="${empty accountLine.invoiceDate}">
					<td width="" align="left">
				        <s:textfield name="accountLine.invoiceDate" cssClass="input-text" id="invoiceDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" />
					 	<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					</td>
					</c:if>
					</configByCorp:fieldVisibility>
					<td>
					<c:out value="${searchbuttons}" escapeXml="false" />
					</td>
					</tr>
					
				</tbody>
			</table>
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
			<s:set name="findVendorInviceList" value="findVendorInviceList" scope="request"/>
			
<div id="Layer1" style="width:100%; overflow-x:auto;" >
	<div id="newtabs" >
		  <ul>
		    <li><a class="current"><span>Vendor Invoice List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<table class="" cellspacing="0" cellpadding="1" border="0" style="width:100%">
 <tbody> 
   <tr>
	 <td>  
 <c:choose>
 <c:when test="${findVendorInviceList!=''}">
<display:table name="findVendorInviceList" class="table" requestURI="" id="findVendorInviceList" defaultsort="1" pagesize="10" style="width:100%">
     <display:column property="invoiceNumber" group="1" sortable="true" title="Vendor Invoice Number"  style="width:7%"/>
    <display:column sortable="true"  title="SO Number" style="width:10%" sortProperty="shipNumber">
	   <a href="editTrackingStatus.html?id=${findVendorInviceList.sid}"><c:out value="${findVendorInviceList.shipNumber}" /></a>
     </display:column> 
     <display:column property="lastName" sortable="true" title="Shipper Name"  style="width:12%"/>
     <display:column property="invoiceDate" sortable="true" title="Invoice Date"  style="width:12%"/>
      <display:column  sortable="true" title="Invoice Status" style="width:12%" >
   <c:if test="${findVendorInviceList.payingStatus==''}">
     </c:if>
         <c:if test="${findVendorInviceList.payingStatus=='A'}">
          Approved
        </c:if>
        <c:if test="${findVendorInviceList.payingStatus=='N'}">
          Not Authorized
        </c:if>
        <c:if test="${findVendorInviceList.payingStatus=='P'}">
          Pending with note
        </c:if>
        <c:if test="${findVendorInviceList.payingStatus=='S'}">
          Short pay with notes
        </c:if>
         <c:if test="${findVendorInviceList.payingStatus=='I'}">
          Internal Cost
         </c:if>
         <c:if test="${findVendorInviceList.payingStatus=='R'}">
          Rejected
         </c:if>
         <c:if test="${findVendorInviceList.payingStatus=='D'}">
          Awaiting Approval
         </c:if>
         <c:if test="${findVendorInviceList.payingStatus=='W'}">
            Awaiting approval (amount/currency changed)
         </c:if></display:column>
     <display:column property="localAmount" sortable="true" title="Local Amount"  style="width:12%"/>
	 <display:column property="country" sortable="true" title="Currency"  style="width:7%"/>
	<display:column property="vendorCode" sortable="true" title="Vendor Code"  style="width:7%"/>
	<display:column property="estimateVendorName" sortable="true" title="Vendor's Name"  style="width:7%"/>
</display:table>
</c:when>
	<c:otherwise>
<div style="height:400px;overflow:auto;">
<table class="table" id="dataTable" style="width:100%;margin-top:0px;">
 <thead>					 
	<tr>
	<th>Inv Number  </th>
	<th>So Number</th>
	<th>Shipper Name </th>
	<th>Invoice Date </th>
	<th>Inv Status</th>
	  <th>Local Amount</th>
	<th>Currency </th>
	<th>Vendor Code</th>
	<th>Vendor Name </th>
</tr>
</thead>
<tbody>
  <td  class="listwhitetextWhite" colspan="16">Nothing Found To Display.</td>
</tbody>
</table>
</div>				
	</c:otherwise>
</c:choose>

 </td>
  </tr>
  </tbody>
  </table>
</div>
</s:form>
 <script type="text/javascript">
	setCalendarFunctionality();
	setOnSelectBasedMethods([]);
</script> 
<script type="text/javascript"> 
try{
document.forms['searchForm'].elements['accountLine.invoiceNumber'].focus(); 
}
catch(e){}


highlightTableRows("findVendorInviceList");  

</script>  
		  	