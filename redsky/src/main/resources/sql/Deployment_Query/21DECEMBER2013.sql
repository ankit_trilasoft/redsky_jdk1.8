// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`datasecurityset` ADD COLUMN `createdBy` VARCHAR(45) DEFAULT NULL AFTER `corpID`,
 ADD COLUMN `updatedBy` VARCHAR(45) DEFAULT NULL AFTER `createdBy`,
 ADD COLUMN `createdOn` DATETIME DEFAULT NULL AFTER `updatedBy`,
 ADD COLUMN `updatedOn` DATETIME DEFAULT NULL AFTER `createdOn`;
 
ALTER TABLE `redsky`.`miscellaneous` CHANGE COLUMN `haulerName` `haulerName` VARCHAR(255) NULL DEFAULT NULL  ,
CHANGE COLUMN `driverName` `driverName` VARCHAR(255) NULL DEFAULT NULL  ;

ALTER TABLE `redsky`.`company` ADD COLUMN `enablePricePoint` BIt(1) DEFAULT b'0' ;

ALTER TABLE `redsky`.`company` ADD COLUMN `priceStartDate` DATETIME 
AFTER `enablePricePoint`,
  ADD COLUMN `priceEndDate` DATETIME AFTER `priceStartDate`,
  ADD COLUMN `subscriptionAmt` DECIMAL(19,2) AFTER `priceEndDate`;
 
ALTER TABLE `redsky`.`customerfile` ADD COLUMN `leadStatus` VARCHAR(10) NULL  AFTER `noInsurance` , ADD COLUMN `leadStatusDate` DATETIME NULL  AFTER `leadStatus` ;

INSERT INTO refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,language) 
values('TSFT', 'NEW', 'New', 10, 'LEAD_STATUS',now(),'ravi',now(),'ravi','en'),
('TSFT', 'ACCEPTED', 'Accepted', 10, 'LEAD_STATUS',now(),'ravi',now(),'ravi','en'),
('TSFT', 'REJECTED', 'Rejected', 10, 'LEAD_STATUS',now(),'ravi',now(),'ravi','en');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) 
VALUES('TSFT','LEAD_STATUS',10,'',now(),'ravi',now(),'ravi','0','0','');

INSERT INTO refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,language) 
values('TSFT', 'LeadCapture', 'LeadCapture', 25, 'PENTITY',now(),'ravi',now(),'ravi','en');

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `customerRating` BIGINT(2) AFTER `serviceOrderFlagCarrier`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `fileCabinetView` VARCHAR(20) AFTER `loginFailureReason`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `customerSurveyEmail` BIT(1) DEFAULT false ;

ALTER TABLE `redsky`.`myfile` ADD COLUMN `isServiceProvider` BIT(1) NULL DEFAULT NULL  AFTER `documentCategory` ;

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `isServiceProvider` BIT(1) NULL DEFAULT NULL  AFTER `isSubDestAgent` ;

DROP TABLE IF EXISTS `redsky`.`equipmaterialslimits`;
CREATE TABLE  `redsky`.`equipmaterialslimits` (
  `id` bigint(15) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(25) DEFAULT NULL,
  `resource` varchar(45) DEFAULT NULL,
  `corpId` varchar(11) DEFAULT NULL,
  `maxResourceLimit` decimal(19,2) DEFAULT NULL,
  `minResourceLimit` decimal(19,2) DEFAULT NULL,
   `qty` INTEGER DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `branch` varchar(20) DEFAULT NULL,
   `division` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `redsky`.`equipmaterialscost`;
CREATE TABLE  `redsky`.`equipmaterialscost` (
  `id` bigint(15) unsigned NOT NULL AUTO_INCREMENT,
  `equipMaterialsId` bigint(15) unsigned NOT NULL,
  `unitCost` DOUBLE,
  `salestCost` DOUBLE,
  `ownerOpCost` DOUBLE,
  `corpId` varchar(11) DEFAULT NULL,  
   `availableQty` INTEGER,
  `createdBy` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,  
  PRIMARY KEY (`id`)  
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

ALTER TABLE `redsky`.`documentaccesscontrol` ADD COLUMN `invoiceAttachment` BIT(1) NULL DEFAULT b'0'  AFTER `isServiceProvider` ;

ALTER TABLE `redsky`.`myfile` ADD COLUMN `invoiceAttachment` BIT(1) NULL DEFAULT b'0'  AFTER `isServiceProvider` ;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `recVatGl` VARCHAR(20) DEFAULT '' ,ADD COLUMN `payVatGl` VARCHAR(20) DEFAULT '' AFTER `recVatGl`;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('UTSI', 'PIR','Pre-Invoice Report', 20, 'PAY_STATUS','en',now(),'dkumar',now(),'dkumar');

ALTER TABLE `redsky`.`todorule` ADD COLUMN `docType` VARCHAR(25) NULL AFTER `publishRule` ;

ALTER TABLE `redsky`.`todoresult` ADD COLUMN `reassignedOwner` VARCHAR(50) NULL AFTER `mailStauts` ;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `selectiveInvoice` bit(1) DEFAULT b'1' AFTER `payVatGl`;

update accountline set selectiveInvoice =true;

ALTER TABLE `redsky`.`customerfile` MODIFY COLUMN `assignmentType` VARCHAR(80) DEFAULT NULL;
 
ALTER TABLE `redsky`.`history` MODIFY COLUMN `alertFileNumber` VARCHAR(50) ;

ALTER TABLE `redsky`.`contract` ADD COLUMN `recClearing` VARCHAR(20) DEFAULT '',ADD COLUMN `payClearing` VARCHAR(20) DEFAULT '' AFTER `recClearing`;

ALTER TABLE `redsky`.`accountcontact` MODIFY COLUMN `salutation` VARCHAR(20) DEFAULT NULL;

ALTER TABLE `redsky`.`vehicle` MODIFY COLUMN `model` VARCHAR(25);

update refmaster set flex5='LDA' where parameter='HOUSE' and corpid='SSCW' and flex3 ='P2N';

update refmaster set flex5='LDA' where parameter='HOUSE' and corpid='SSCW' and flex3 ='7MU';

update refmaster set flex5='LCY' where parameter='HOUSE' and corpid='SSCW' and flex3 ='PUF';

update refmaster set flex5='LDA' where parameter='HOUSE' and corpid='SSCW' and flex3 ='6MU';

update refmaster set flex5='' where parameter='HOUSE' and corpid='SSCW' and flex5 is null;

ALTER TABLE `redsky`.`serviceorder` add COLUMN `isSOExtract` bit(1) default false;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `soExtractSeq` VARCHAR(10) AFTER `sortOrder`;

INSERT INTO 
parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','MARKETAREA',200,'',now(),'subrat',now(),'subrat','1','1','');

ALTER TABLE `redsky`.`servicepartner` ADD COLUMN `caed` VARCHAR(45) AFTER `houseBillNumber`;

ALTER TABLE equipmaterialslimits ADD CONSTRAINT uc_resource UNIQUE (resource);
     
ALTER TABLE equipmaterialslimits DROP INDEX uc_resource;

ALTER TABLE equipmaterialslimits ADD CONSTRAINT uc_resource UNIQUE (resource,branch);

delete from refmaster where parameter='LEAD' and corpid='HSRG';

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Account', 'Account', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'Agent', 'Agent', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'BBB', 'Better Business Bureau (BBB)', 10, 'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),

('HSRG', 'BD', 'Business Development', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'ER', 'Employer Referral', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'FB', 'Frog Box', 10, 'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),

('HSRG', 'Google', 'Google', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'Homestars', 'Homestars', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'IO', 'Internet - Other', 10, 'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),

('HSRG', 'Mailer', 'Mailer', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'MovesOn', 'MovesOnline', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'MVF', 'MVF', 10, 'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),

('HSRG', 'Other', 'Other', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'Referral', 'Referral', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'RC', 'Repeat Customer', 10, 'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),

('HSRG', 'SM', 'Social Media', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'TMILeads', 'TMI Leads', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'TradeShow', 'Trade Show', 10, 'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),

('HSRG', 'Trucks', 'Trucks', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'UVL', 'United Van Lines', 10,'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'YP', 'Yellow Pages', 10, 'LEAD','en',now(),'adminhsrg',now(),'adminhsrg'),
('HSRG', 'YPW', 'Yellow Pages Website', 10, 'LEAD','en',now(),'adminhsrg',now(),'adminhsrg');

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `refferedBy` VARCHAR(45) AFTER `estHour`;

ALTER TABLE `redsky`.`itemsjbkequip` ADD COLUMN `equipMaterialsId` BIGINT AFTER `refferedBy`;

ALTER TABLE `redsky`.`emailsetup` MODIFY COLUMN `attchedFileLocation` TEXT;

ALTER TABLE `redsky`.`company` ADD COLUMN `clientID` VARCHAR(10) AFTER `subscriptionAmt`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `customerSurveyEmail` VARCHAR(45) AFTER `ownbillingBilltoCodes`;

DROP TABLE IF EXISTS `redsky`.`dynamicpricing`;
CREATE TABLE  `redsky`.`dynamicpricing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpID` varchar(30) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `dynamicPricingType` varchar(45) DEFAULT NULL,
  `noOfEmployees` int(11) DEFAULT '0',
  `minRange1Men` int(11) DEFAULT '0',
  `maxRange1Men` int(11) DEFAULT '0',
  `minRange2Men` int(11) DEFAULT '0',
  `maxRange2Men` int(11) DEFAULT '0',
  `minRange3Men` int(11) DEFAULT '0',
  `maxRange3Men` int(11) DEFAULT '0',
  `minRange4Men` int(11) DEFAULT '0',
  `maxRange4Men` int(11) DEFAULT '0',
  `minRange5Men` int(11) DEFAULT '0',
  `maxRange5Men` int(11) DEFAULT '0',
  `minRange6Men` int(11) DEFAULT '0',
  `maxRange6Men` int(11) DEFAULT '0',
  `regularHourlyRatePer2Men` decimal(19,2) DEFAULT '0.00',
  `regularHourlyRatePer3Men` decimal(19,2) DEFAULT '0.00',
  `regularHourlyRatePer4Men` decimal(19,2) DEFAULT '0.00',
  `monthEndHourlyRatePer2Men` decimal(19,2) DEFAULT '0.00',
  `monthEndHourlyRatePer3Men` decimal(19,2) DEFAULT '0.00',
  `monthEndHourlyRatePer4Men` decimal(19,2) DEFAULT '0.00',
  `extraMenRate` decimal(19,2) DEFAULT '0.00',
  `saturdayRatePersent` decimal(19,2) DEFAULT '0.00',
  `sundayRatePersent` decimal(19,2) DEFAULT '0.00',
  `deviationRange1forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange2forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange3forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange4forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange5forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange6forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange1forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange2forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange3forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange4forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange5forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange6forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange1forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange2forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange3forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange4forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange5forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange6forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange1forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange2forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange3forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange4forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange5forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `deviationRange6forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange1forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange1forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange1forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange2forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange2forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange2forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange3forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange3forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange3forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange4forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange4forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange4forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange5forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange5forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange5forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange6forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange6forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange6forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange1forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange1forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange1forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange2forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange2forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange2forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange3forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange3forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange3forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange4forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange4forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange4forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange5forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange5forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange5forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange6forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange6forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange6forMoreThan21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange1forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange1forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange1forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange2forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange2forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange2forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange3forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange3forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange3forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange4forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange4forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange4forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange5forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange5forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange5forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange6forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange6forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange6forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange1forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange1forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange1forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange2forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange2forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange2forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange3forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange3forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange3forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange4forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange4forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange4forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange5forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange5forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange5forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange6forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange6forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange6forBetween14to21Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange1forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange1forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange1forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange2forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange2forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange2forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange3forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange3forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange3forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange4forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange4forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange4forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange5forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange5forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange5forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange6forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange6forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange6forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange1forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange1forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange1forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange2forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange2forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange2forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange3forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange3forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange3forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange4forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange4forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange4forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange5forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange5forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange5forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange6forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange6forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange6forBetween7to13Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange1forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange1forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange1forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange2forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange2forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange2forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange3forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange3forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange3forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange4forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange4forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange4forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange5forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange5forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange5forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer2MenRange6forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer3MenRange6forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `regularRatePer4MenRange6forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange1forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange1forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange1forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange2forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange2forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange2forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange3forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange3forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange3forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange4forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange4forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange4forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange5forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange5forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange5forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer2MenRange6forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer3MenRange6forLessThan7Days` decimal(19,2) DEFAULT '0.00',
  `monthEndRatePer4MenRange6forLessThan7Days` decimal(19,2) DEFAULT '0.00',
   PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `redsky`.`holidaymaintenance`;
CREATE TABLE  `redsky`.`holidaymaintenance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpID` varchar(30) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `holiDayDate` datetime DEFAULT NULL,
  `holiDayRate` decimal(19,2) DEFAULT '0.00',
  `holiDaybranch` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'EDMSUMMER', 'Edmonton - Summer', 50, 'DYNAMICPRICINGCATEGORY','en',now(),'HSRG_SETUP',now(),'HSRG_SETUP'),
('HSRG', 'EDMWINTER', 'Edmonton - Winter', 50, 'DYNAMICPRICINGCATEGORY','en',now(),'HSRG_SETUP',now(),'HSRG_SETUP'),
('HSRG', 'CALSUMMER', 'Calgary - Summer', 50, 'DYNAMICPRICINGCATEGORY','en',now(),'HSRG_SETUP',now(),'HSRG_SETUP'),
('HSRG', 'CALWINTER', 'Calgary - Winter', 50, 'DYNAMICPRICINGCATEGORY','en',now(),'HSRG_SETUP',now(),'HSRG_SETUP');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('HSRG','DYNAMICPRICINGCATEGORY',50,true,now(),'HSRG_SETUP',now(),'HSRG_SETUP',1,1,'');






