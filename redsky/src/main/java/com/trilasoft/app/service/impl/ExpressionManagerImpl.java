package com.trilasoft.app.service.impl;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.trilasoft.app.service.ExpressionManager;

public class ExpressionManagerImpl implements ExpressionManager{
	
	private static String formulaImports = "import java.text.SimpleDateFormat\r\n"
		+ "import java.util.Calendar\r\n"
		+ "import java.util.GregorianCalendar\r\n"
		+ "import java.text.SimpleDateFormat\r\n";
	
	private static String dowFunction =  "def dow(d) { \r\n"
	+ "Calendar calendar = new GregorianCalendar()\r\n"
	+ "calendar.setTime(d)\r\n"
	+ "return calendar.get(Calendar.DAY_OF_WEEK)\r\n" + " }\r\n";

	private static String iifFunction =  "def iif(def test, valYes, valNo) { \r\n"
		+ "return test? valYes: valNo\r\n" + " }\r\n";

	
	private static String formulaGlobalVariables = "df1 = new SimpleDateFormat(\"yyyy-MM-dd\")\r\n";

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	
	public Object executeExpression(String baseExpression, Map values) {
		//System.err.println("\n\n\n\n IN Expression Manager IMPL------1------>>"+baseExpression);
		String processedExpression= migrateFormula(baseExpression, values);		
		//System.err.println("\n\n\n\n IN Expression Manager IMPL------2------>>"+processedExpression);
		String expression = buildExpression(processedExpression);
		//System.err.println("\n\n\n\n IN Expression Manager IMPL------3------>>"+expression);
		Binding binding = new Binding();
		GroovyShell shell = new GroovyShell(binding);
		Object value = shell.evaluate(expression);
		//System.err.println("\n\n\n\n IN value Expression Manager IMPL------4------>>"+value);
		return value;		
	}
	
	public String executeTestWording(String baseExpression, Map values) {
		String message = "";
		String processedExpression= migrateFormula(baseExpression, values);		
		String expression = buildExpression(processedExpression);
		Binding binding = new Binding();
		GroovyShell shell = new GroovyShell(binding);
		Object value;
		try{
			value = shell.evaluate(expression);
			//System.out.println("\n\n  value-->>"+value);
			return value.toString();
		}catch(Exception ex){
			message = "Formula is Incorrect";
			//System.out.println("\n\n  message-->>"+message+"\n"+ex.getMessage());
			return message;
		}		
				
	}
	
	public String executeTestExpression(String baseExpression, Map values) {
		String message = "";
		String processedExpression= migrateFormula(baseExpression, values);
		String expression = buildExpression(processedExpression);
		Binding binding = new Binding();
		GroovyShell shell = new GroovyShell(binding);
		try{
		Object value = shell.evaluate(expression);
		//System.out.println("\n\n value-->>"+value);
		}catch(Exception ex){
		message = "Formula is Incorrect\n\n"+ex;
		//System.out.println("\n\n message-->>"+message+"\n"+ex);
		return message;
		}
		return null;
		}

	
	public static String migrateFormula(String formula, Map values){
		formula = formula.replaceAll("\\.or\\.", "||");
		formula = formula.replaceAll("DOW", "dow");
		formula = formula.replaceAll("=", "==");
		formula = formula.replaceAll("FORM.QTY", "form.qty");
		formula = formula.replaceAll("form.qty", "@form.qty@");
		formula = formula.replaceAll("timeSheet.workDate", "timeSheet.workDate");
		formula = formula.replaceAll("timeSheet.workDate", "timeSheet.workDate");
		formula = formula.replaceAll("workTicket.actualWeight", "workTicket.actualWeight");
		formula = formula.replaceAll("workTicket.revenue", "workTicket.revenue");
		
		/********** FOR GL TYPE **************/
		formula = formula.replaceAll("accountLine.actualRevenue", "accountLine.actualRevenue");
		formula = formula.replaceAll("accountline.branch", "accountline.branchCode");
		formula = formula.replaceAll("billing.discount", "billing.discount");
		formula = formula.replaceAll("billing.othdisc", "billing.otherDiscount");
		formula = formula.replaceAll("imisc.actnet", "miscellaneous.actualNetWeight");
		formula = formula.replaceAll("accountline.actualRevenue", "accountline.actualRevenue");
		formula = formula.replaceAll("accountline.description", "accountline.description");
		formula = formula.replaceAll("partner.driveragency", "partner.driverAgency");
		formula = formula.replaceAll("accountline.chargeCode", "accountline.chargeCode");
		formula = formula.replaceAll("accountline.type", "accountline.chargeCode");
		formula = formula.replaceAll("<>", "!=");
		formula = formula.replaceAll("IIF", "iif");
		//formula = formula.replaceAll("<", "<");
		//System.out.println("\n\n\n formula"+formula);
		//formula = formula.replaceAll(">", ">=");
		
		
		/********** FOR GL TYPE COMM**************/
		formula = formula.replaceAll("accountline.distributionAmount", "accountline.distributionAmount");
		formula = formula.replaceAll("vanLineCommissionType.pc", "vanLineCommissionType.pc");
		formula = formula.replaceAll("serviceorder.bookingAgentCode", "serviceorder.bookingAgentCode");
		//formula = formula.replaceAll("vanlinegltype.glType","058");

		/********** FOR Billing Contract **************/
		formula = formula.replaceAll("billing.onHand", "billing.onHand");
		formula = formula.replaceAll("billing.cycle", "billing.cycle");
		formula = formula.replaceAll("billing.insuranceRate", "billing.insuranceRate");
		formula = formula.replaceAll("billing.insuranceValueActual", "billing.insuranceValueActual");
		formula = formula.replaceAll("billing.baseInsuranceValue", "billing.baseInsuranceValue");
		formula = formula.replaceAll("charges.minimum", "charges.minimum");
		formula = formula.replaceAll("billing.postGrate", "billing.postGrate");
		formula = formula.replaceAll("billing.vendorStoragePerMonth", "billing.vendorStoragePerMonth");
		formula = formula.replaceAll("billing.storagePerMonth", "billing.storagePerMonth");
		formula = formula.replaceAll("billing.totalInsrVal", "billing.totalInsrVal");
		formula = formula.replaceAll("billing.insurancePerMonth", "billing.insurancePerMonth");
		formula = formula.replaceAll("billing.insuranceValueEntitle", "billing.insuranceValueEntitle");
		formula = formula.replaceAll("billing.payableRate", "billing.payableRate");
		
		formula = formula.replaceAll("billing.packDiscount", "billing.packDiscount");
		formula = formula.replaceAll("miscellaneous.actualAutoWeight", "miscellaneous.actualAutoWeight");
		formula = formula.replaceAll("miscellaneous.actualGrossWeightKilo", "miscellaneous.actualGrossWeightKilo");
		formula = formula.replaceAll("miscellaneous.actualNetWeightKilo", "miscellaneous.actualNetWeightKilo");
		formula = formula.replaceAll("miscellaneous.actualGrossWeight", "miscellaneous.actualGrossWeight");
		formula = formula.replaceAll("miscellaneous.actualNetWeight", "miscellaneous.actualNetWeight");
		formula = formula.replaceAll("billing.storageMeasurement", "billing.storageMeasurement");
		
		/********** FOR Standard Deductions **************/
		formula = formula.replaceAll("standardDeductions.maxDeductAmt", "standardDeductions.maxDeductAmt");
		formula = formula.replaceAll("standardDeductions.rate", "standardDeductions.rate");
				
		formula = migrateDecimals(formula);
		formula = migrateFieldNames(formula, values);
		return formula; 
	}
	
	
	private static String migrateFieldNames(String formula, Map values){
		Pattern fiedDelimiterPattern = Pattern.compile("@[0-9a-zA-Z_%$]+.[0-9a-zA-Z_%$]+@");
		StringBuffer formulaVariables = new StringBuffer();
		StringBuffer processedFormula = new StringBuffer();
		Matcher matcher = fiedDelimiterPattern.matcher(formula);
		while (matcher.find()) {
			String fieldName = formula.substring(matcher.start() + 1, matcher.end() -1);
			if (formulaVariables.indexOf(fieldName.replace('.', '_') ) < 0)
				formulaVariables.append(generateFormulaVariable(fieldName, values));
			    matcher.appendReplacement(processedFormula, fieldName.replace('.','_'));
		}
		matcher.appendTail(processedFormula);		
		return formulaVariables.toString() + "expressionValue = " + processedFormula.toString() + "\r\n" + "return expressionValue\r\n";		
	}
	
	private static String migrateDecimals(String formula) {
		Pattern fiedDelimiterPattern = Pattern.compile("[\\D]\\.[\\d]");
		StringBuffer processedFormula = new StringBuffer();
		Matcher matcher = fiedDelimiterPattern.matcher(formula);
		while (matcher.find()) {
			String replacementText = formula.substring(matcher.start(), matcher.start()+1) +  "0" + formula.substring(matcher.start()+1, matcher.end());
			matcher.appendReplacement(processedFormula, replacementText);
		}
		matcher.appendTail(processedFormula);
		return processedFormula.toString();

	}

	private static String generateFormulaVariable(String fieldName, Map values) {
		StringBuffer variableDeclaration = new StringBuffer();
		String variableName = fieldName.replace('.', '_');
		String variableNameInput = variableName + "Str";
		Object value = values.get(fieldName);

		// Define the format pattern in a string
	    String fmt = "0.00";

	    // Create a DecimalFormat instance for this format
	    DecimalFormat decimalFormat = new DecimalFormat (fmt);
	    
		if (value instanceof Date) {
			variableDeclaration.append(variableNameInput).append(" = \"").append(sdf.format(value)).append("\"\r\n");
			variableDeclaration.append(variableName).append(" = df1.parse(").append(variableNameInput).append(")\r\n");
		} else if (value instanceof String) {
			variableDeclaration.append(variableName).append(" = \"").append(value).append("\"\r\n");
		} else if (value instanceof Double) {
			variableDeclaration.append(variableName).append(" = ").append(decimalFormat.format(value)).append("\r\n");
		} else {
			variableDeclaration.append(variableName).append(" = ").append(value).append("\r\n");
		}
		return variableDeclaration.toString();
	}

	public String buildExpression(String formula ){
		return formulaImports + formulaGlobalVariables + dowFunction  + iifFunction + formula;
	}
	
}
