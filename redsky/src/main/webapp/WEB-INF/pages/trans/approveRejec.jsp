
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ApproveRejectList</title>
</head>

<s:form id="sodashbord" name="" action="" method="post" validate="true"onsubmit="">
<s:set name="accountLineList" value="accountLineList" scope="request"/>
<display:table name="accountLineList" class="table" requestURI="" id="accountLineList" style="margin-top:-2px;" defaultsort="1" pagesize="1000" >
 <display:setProperty name="paging.banner.placement" value="both" /> 
 	      <%--  <display:column property="accId" sortable="true" title="Accid#" /> --%>
		  <display:column sortProperty="invoiceNumber" sortable="true" title="Invoice Number" style="width:100px;"  >
		  <c:choose>
		  <c:when test="${accountLineList.myFileFileName !=''}">
		  <a onclick="downloadSelectedFile('${accountLineList.myFileFileName}','${accountLineList.shipNumber}');">
		    <c:out value="${accountLineList.invoiceNumber}" escapeXml="false"/>
	     </a> 
	     </c:when>
	      <c:otherwise>
	       <c:out value="${accountLineList.invoiceNumber}" escapeXml="false"/>
	      </c:otherwise>
	     </c:choose>
	     </display:column>
	     <display:column property="invoiceDate" sortable="true" title="Invoice Date" format="{0,date,dd-MMM-yyyy}"   />
          <display:column property="vendorCode" sortable="true" title="Vendor Code" />
          <display:column property="estimateVendorName" sortable="true" title="Vendor Name" />
          <display:column property="localAmount" sortable="true" title="Billing Amount" />
          <display:column property="country" sortable="true" title="Billing Currency" />
          <display:column property="actualExpense" sortable="true" title="Actual Expense"  /> 
  <%--         <display:column title="Status" > 
	  	  <configByCorp:customDropDown 	listType="map" list="${payingStatus}" fieldValue=""  attribute="id='payingStatus ${accountLineList.invoiceNumber}' tabindex='43'class=list-menu  style=width:102px  headerKey='' headerValue='' onchange=checkPayingStatus(${accountLineList.invoiceNumber},'${accountLineList.accId}',this),changeStatus();"/>
	     </display:column>  --%>
	     
	     <display:column title="Status" >
  <s:select cssClass="list-menu" headerKey="" headerValue="" id="payingStatus ${accountLineList.invoiceNumber}" name="payingStatus ${accountLineList.invoiceNumber}" cssStyle="width:107px" list="%{payingStatusPopUpList}" tabindex="44" onchange="checkPayingStatus('${accountLineList.invoiceNumber}','${accountLineList.accId}',this)" />
</display:column>
</display:table>
		
		 <script type="text/javascript">


	
var httpTemplate = getHTTPObject8(); 
var httpTemplate = getHTTPObject9(); 
function getHTTPObject8(){
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
} 

function getHTTPObject9(){
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
} 

function checkPayingStatus(invoiveNumber,accId,target){
var payingStatus = target.value
var payingStatusName = target.name
   if(payingStatus=='R' || payingStatus=='P') {
	   var url="notesStatus.html?ajax=1&decorator=simple&popup=true&accId="+encodeURI(accId);
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange =function(){  getHTTPObject19(payingStatus,accId,payingStatusName);};
		httpTemplate.send(null);
	   
   }else{
	   if(payingStatus=='A' && invoiveNumber.trim()==''){
			 alert("Please fill invoice# in payable detail.")  
			 document.getElementById('payingStatus'+id).value="";
		   }
	   else{
	   var url="updatePayingStatus.html?ajax=1&decorator=simple&popup=true&accId="+encodeURI(accId)+"&payingStatusValue="+payingStatus;
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = getHTTPObject18;
		httpTemplate.send(null); 
	   }
   }

}
function getHTTPObject18(){
if (httpTemplate.readyState == 4){
   var results= httpTemplate.responseText;
   parent.window.opener.pageReload();
}
}
function getHTTPObject19(payingStatus,accId,payingStatusName){
	if (httpTemplate.readyState == 4){
	   var results= httpTemplate.responseText;
	   var results=results.trim();
	   var results=results.split("~");
	   var count= results[0];
	    var id= results[1];
   
       if(count> 0) {
    	   var url="updatePayingStatus.html?ajax=1&decorator=simple&popup=true&accId="+encodeURI(accId)+"&payingStatusValue="+payingStatus;
      		httpTemplate.open("GET", url, true);
      		httpTemplate.onreadystatechange = getHTTPObject18;
      		httpTemplate.send(null);
       }else{
    	   alert("Cannot select status pending with note/rejected as there is no note available");
    	   document.getElementById(payingStatusName).value="";
    	   window.location.reload();
       }
	}
	}
function downloadSelectedFile(id,shipNumber){
	var fieldVal = "AgentPayableProcessing"; 
	var url ="";
	url="ImageServletAction.html?id="+id+"&shipNumber="+shipNumber+"&myFileForVal="+fieldVal+"";
	location.href=url;
}
 </script>
 <s:hidden name="status"  id= "status" />
</s:form>
</html>