package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CompensationSetup;

public interface CompensationSetupDao extends GenericDao <CompensationSetup,Long> {
	
	public List getListFromCompensationSetup(String corpId, Long contractId,String contractName);

}
