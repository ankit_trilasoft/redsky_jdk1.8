<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="bookStorageList.title" /></title>
<meta name="heading"
	content="<fmt:message key='bookStorageList.heading'/>" />
</head>


<c:set var="buttons">
</c:set>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="0" align="right"><img width="0" height="20"
				src="<c:url value='/images/head-left.jpg'/>" alt="" /></td>
			<td width="100" class="content-tab"><a href="storageLibraries.html">Work
			On Ticket</a></td>
			<td width="0" align="left"><img width="0" height="20"
				src="<c:url value='/images/head-right.jpg'/>" alt="" /></td>

			<td width="4"></td>
			<td width="1" align="right"><img width="1" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" alt="" /></td>
			<td width="90" class="content-tab"><a href="storageLibraries.html">Detail</a></td>
			<td width="1" align="left"><img width="1" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" alt="" /></td>
			<td width="4"></td>
			<td width="1" align="right"><img width="1" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" alt="" /></td>
			<td width="130" class="content-tab"><a href="storages.html">Add Storage</a></td>
			<td width="1" align="left"><img width="1" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" alt="" /></td>

			<td width="4"></td>
			<td width="1" align="right"><img width="1" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" alt="" /></td>
			<td width="160" class="detailActiveTabLabel content-tab"><a
				href="bookStorageLibraries.html">Access/Release Storage </a></td>
			<td width="1" align="left"><img width="1" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" alt="" /></td>
			<td width="4"></td>

		</tr>
	</tbody>
</table>


<c:out value="${buttons}" escapeXml="false" />

<s:set name="bookStorages" value="bookStorages" scope="request" />
<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="7"><img width="7" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td id="searchLabelCenter" width="90">
			<div align="center" class="content-head">List</div>
			</td>
			<td width="7"><img width="7" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td></td>
		</tr>
	</tbody>
</table>
<display:table name="bookStorages" class="table" requestURI=""
	id="bookStorageList" export="true" pagesize="25">
	<display:column property="id" sortable="true"
		href="editBookStorage.html" paramId="id" paramProperty="id"
		titleKey="bookStorage.id" />
	<display:column property="shipNumber" sortable="true"
		titleKey="bookStorage.shipNumber" />
	<display:column property="containerId" sortable="true"
		titleKey="bookStorage.containerId" />
	<display:column property="description" sortable="true"
		titleKey="bookStorage.description" />
	<display:column property="itemNumber" sortable="true"
		titleKey="bookStorage.itemNumber" />
	<display:column property="jobNumber" sortable="true"
		titleKey="bookStorage.jobNumber" />
	<display:column property="location" sortable="true"
		titleKey="bookStorage.location" />
	<display:column property="pieces" sortable="true"
		titleKey="bookStorage.pieces" />
	<display:column property="price" sortable="true"
		titleKey="bookStorage.price" />


	<display:setProperty name="paging.banner.item_name" value="bookStorage" />
	<display:setProperty name="paging.banner.items_name"
		value="bookStorage" />
	<display:setProperty name="export.excel.filename"
		value="BookStorage List.xls" />
	<display:setProperty name="export.csv.filename"
		value="BookStorage List.csv" />
	<display:setProperty name="export.pdf.filename"
		value="BookStorage List.pdf" />
</display:table>
<c:set var="buttons">
	<input type="button" style="margin-right: 5px"
		onclick="location.href='<c:url value="/editBookStorageLibraries.html"/>'"
		value="<fmt:message key="button.add"/>" />

	<input type="button"
		onclick="location.href='<c:url value="/mainMenu.html"/>'"
		value="<fmt:message key="button.done"/>" />
</c:set>
<c:out value="${buttons}" escapeXml="false" />

<script type="text/javascript"> 
    highlightTableRows("searchbookStorageList"); 
</script>
