<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="accountContactList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountContactList.heading'/>"/>  
    
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
} 
</style>

<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id") %>" />
<c:set var="fileID" value="<%=request.getParameter("id") %>"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>

<s:form id="accountContactListForm" action="" method="post" validate="true">
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="pID" value="${partner.id}"/>
<div id="Layer1" style="width: 100%">
<div id="newmnav">
				<ul>
				  <c:if test="${param.popup}" >
				    <%-- <li><a href='${empty param.popup?"partners.html":"javascript:history.go(-4)"}' ><span>List</span></a></li>  --%>
				    <li><a href="searchPartner.html?partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account List</span></a></li>
			  	    <li><a href="editPartnerAddFormPopup.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Detail</span></a></li>
			  	    <li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Profile</span></a></li>
			  	    <li><a href="accountInfoPage.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Info</span></a></li>
			  		
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Contact<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  		<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Policy</span></a></li>
			  	  </c:if>
			  	  <c:if test="${empty param.popup}" >
				    <li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=AC"><span>Account Detail</span></a></li>
				    <li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Profile</span></a></li>
				    <c:if test="${sessionCorpID!='TSFT' }">
				    <li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
			  		</c:if>
			  		<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Contact</span></a></li>
			  		<c:if test="${checkTransfereeInfopackage==true}">
			  			<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
			  		</c:if>
			  		<%-- Added Portal Users Tab By kunal for ticket number: 6176 --%>
			  		<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
						<c:if test="${usertype!='ACCOUNT'}">
							<c:if test="${partner.partnerPortalActive == true}">
								<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
							</c:if>
						</c:if>
					</sec-auth:authComponent>
					<%-- Modification closed here for ticket number: 6176 --%>
			  		<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					
					<c:if test='${paramValue == "View"}'>
						<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
					</c:if>
					<c:if test='${paramValue != "View"}'>
						<li><a href="partnerPublics.html?partnerType=AC"><span>Partner List</span></a></li>
					</c:if>
					<li><a href="partnerReloSvcs.html?id=${partner.id}&partnerType=${partnerType}"><span>Services</span></a></li>
					
				<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partner.id}">
					<c:url value="frequentlyAskedQuestionsList.html" var="url">
					<c:param name="partnerCode" value="${partner.partnerCode}"/>
					<c:param name="partnerType" value="AC"/>
					<c:param name="partnerId" value="${partner.id}"/>
					<c:param name="lastName" value="${partner.lastName}"/>
					<c:param name="status" value="${partner.status}"/>
					</c:url>				 
				 <li><a href="${url}"><span>FAQ</span></a></li>
				 <c:if test="${partner.partnerPortalActive == true}">
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
				</c:if>
				  </c:if>
				</c:if>						
			  	</c:if>
			  	<li><a onclick="window.open('auditList.html?id=${accountContact.id}&tableName=accountcontact&partnerCode=${partner.partnerCode}&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			  	</ul>
		</div>
		<div class="spn">&nbsp;</div>
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:25px" 
        onclick="location.href='<c:url value="/editNewAccountContact.html?pID=${partner.id}&partnerType=${partnerType}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  

<table class="" cellspacing="1" cellpadding="0" border="0" style="width: 100%;">
	<tbody>
		<tr>
			<td>
			<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top"><span></span></div>
<div class="center-content">
				<table class='detailTabLabel' cellspacing="1" cellpadding="2" border="0">
					<tbody>
						<tr>
							<td width="10px" ><s:label label="  "/></td>
							<td class="listwhitetext" >Account&nbsp;Name</td>
							<td><s:hidden name="partner.id" />
							    <s:textfield cssClass="input-textUpper" name="partner.lastName" required="true" size="35" readonly="true"/>
							</td>
							<td align="right" class="listwhitetext" width="50px" ><fmt:message key='partner.partnerCode' /></td>
							<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="8" readonly="true" /></td>
							<td align="right" class="listwhitetext" width="50px" >Status</td>
							<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.status" required="true" cssClass="input-textUpper" size="15" readonly="true" /></td>
		  	
						</tr>
						<tr><td height="20px"></td></tr>
					</tbody>
				</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<s:set name="accountContacts" value="accountContacts" scope="request"/>
	      <div id="otabs">
		  <ul>
		    <li><a class="current"><span>List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk" style="margin-bottom:-2px;">&nbsp;</div>	
<display:table name="accountContacts" class="table" requestURI="" id="accountContactList" export="true" defaultsort="1" pagesize="10" >   
	 <c:if test="${empty param.popup}" >
	<display:column sortable="true" titleKey="accountContact.jobType">
	<a onclick="goToDetail1(${accountContactList.id});" style="cursor:pointer"> 
     <c:out value="${accountContactList.jobType}" /></a></display:column>	
	</c:if>
	
	<c:if test="${param.popup}" >
	<display:column property="jobType" sortable="true" titleKey="accountContact.jobType"
		href="editAccountContact.html?pID=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}" paramId="id" paramProperty="id" />   
	</c:if>
	<display:column property="jobTypeOwner" sortable="true" titleKey="accountContact.jobTypeOwner" />
	<display:column property="contractType" sortable="true" titleKey="accountContact.contractType" />
	<display:column sortable="true" titleKey="accountContact.contactName" >
		<c:out value="${accountContactList.contactName} ${accountContactList.contactLastName}" />
	</display:column>
	<display:column property="department" sortable="true" titleKey="accountContact.department" />
	<display:column headerClass="containeralign" style="text-align: right" property="primaryPhone" sortable="true" titleKey="accountContact.primaryPhone" />
	<c:if test="${accountContactList.status=='true'}">
	<display:column title="Active"> <img src="${pageContext.request.contextPath}/images/tick01.gif" /> </display:column>
	</c:if>
	<c:if test="${accountContactList.status!='true'}">
	<display:column title="Active"> <img src="${pageContext.request.contextPath}/images/cancel001.gif" /> </display:column>
	</c:if>
	<display:column title="Remove" style="text-align:center; width:20px;">
    <a><img align="middle" onclick="confirmSubmit(${accountContactList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>

	
    <display:setProperty name="paging.banner.item_name" value="accountContact"/>   
    <display:setProperty name="paging.banner.items_name" value="accountContacts"/>   
  
    <display:setProperty name="export.excel.filename" value="AccountContacts List.xls"/>   
    <display:setProperty name="export.csv.filename" value="AccountContacts List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="AccountContacts List.pdf"/>   
</display:table>  
 
<c:if test="${empty param.popup}" >
<c:out value="${buttons}" escapeXml="false" />  
</c:if>
			</td>
		</tr>
	</tbody>
</table> 
</div>
</s:form>

<%-- Script Shifted from Top to Botton on 06-Sep-2012 By Kunal --%>
<script type="text/javascript"> 
	function confirmSubmit(targetElement){  
		var agree=confirm("Are you sure you want to remove this Account Contact ?");
		var did = targetElement;
		var pID=document.forms['accountContactListForm'].elements['pID'].value;
		if (agree){
			 location.href="accountContactDelete.html?id="+did+"&pID="+pID;
		}else{
			return false;
		}
	}
	function goToDetail1(targetValue){
        //document.forms['accountContactListForm'].elements['id'].value = targetValue;
        var id=targetValue;
        var pID=document.forms['accountContactListForm'].elements['pID'].value;
        var partnerType=document.forms['accountContactListForm'].elements['partnerType'].value;                
        location.href = 'editAccountContact.html?id='+encodeURI(id)+'&partnerType='+partnerType+'&pID='+encodeURI(pID);
	}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">  
	<c:if test="${hitFlag == 1}" >
		<c:redirect url="/accountContactList.html?id=${partner.id}&partnerType=AC"></c:redirect>
	</c:if>
</script>