<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<head>
<meta name="heading" content="<fmt:message key='contractAccountList.title'/>"/> 
<title><fmt:message key="contractAccountList.heading"/></title> 


</head>

<s:form id="contractAccountList" name="contractAccountList" action=""  validate="true">   
<div id="Layer1" style="width:875px;">
<s:hidden name="countContractAccount"/>
<s:hidden name="contractid" value="<%=request.getParameter("id")%>"/>
<c:set var="contractid" value="<%=request.getParameter("id")%>"/> 
<s:hidden name="btnType" value="<%= request.getParameter("btnType")%>" />
<c:set var="btnType" value="<%= request.getParameter("btnType")%>" />
<s:hidden name="btnRefType" value="<%= request.getParameter("btnRefType")%>" />
<c:set var="btnRefType" value="<%= request.getParameter("btnRefType")%>" />
<s:hidden name="contract" value="<%=request.getParameter("contract") %>" />
<c:set var="contract" value="<%=request.getParameter("contract") %>" />
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" name="add" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editcontractAccountForm.html?contractid=${contractid}"/>'"  
        value="<fmt:message key="button.add"/>"/> 
         <c:if test="${countContractAccount == 0}">
     <input type="button" name="copyContractAccount" class="cssbutton1" style="width:275px; height:25px" onclick="location.href='<c:url   value="/addAndCopyContractAccount.html?id=${contract.id}&contractCopy=${contract.contract}"/>'" value="<fmt:message  key="button.CopyContractAccount"/>" style="width:110px; height:25px"/> 
 </c:if>       
</c:set>

<div id="newmnav">
		  <ul>
		    <li><a href="editContract.html?id=${contract.id}"><span>Contract Details</span></a></li>
		    <li><a href="charge23.html?id=${contract.id}"><span>Charge List</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Account List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  		</ul>
		</div><div class="spn">&nbsp;</div>
			<div id="content" align="center">
   <div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
    <table style="margin-bottom: 3px">
    <tr>
        <td align="right" class="listwhitetext">Contract</td>
  <td><s:textfield   value="${contract.contract}" required="true" readonly="true" size="30" cssClass="input-textUpper"/></td>
  </tr>
  </table>
  
  </div>
<div class="bottom-header" style="margin-top:35px;"><span></span></div>
</div>
</div>
		<div style="padding-bottom:3px;!padding-bottom:5px;"></div>		
		<s:set name="contractAccountList" value="contractAccountList" scope="request"/>
		<display:table name="contractAccountList" class="table" requestURI="" id="contractAccountListId" export="true" defaultsort="1" pagesize="10" style="width:875px" >   
		<display:column property="accountCode"  href="editcontractAccountForm.html?contractid=${contractid}" paramId="id" paramProperty="id" sortable="true" titleKey="contractAccount.accountCode"/>
		<display:column property="accountName"  sortable="true" titleKey="contractAccount.accountName" />
		<c:if test ="${contractOrgId == contract.owner}">
		<display:column style="width:105px;">
		 <input type="button" class="cssbutton1" style="width:110px; height:25px" name="Refresh Child" value="Refresh Child" onclick="refreshChild('${contractAccountListId.accountCode}');"   /> 
		</display:column>
		</c:if>
		<sec-auth:authComponent componentId="module.contractAccount.delete">
		 <c:if test ="${contractOrgId == contract.owner}">
		<display:column title="Remove" style="width: 50px;">
			 <img align="middle" title="" onclick="confirmSubmit('${contractAccountListId.id}','${contractid}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
		</display:column>
		</c:if>	
		</sec-auth:authComponent>
		<display:setProperty name="export.excel.filename" value="Account List.xls"/>
		<display:setProperty name="export.csv.filename" value="Account List.csv"/>
		</display:table>
		
		<c:out value="${buttons}" escapeXml="false" /> 
</div>
</s:form>

<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
			for(i=0;i<=1;i++)
			{
					document.forms['contractAccountList'].elements[i].value = "";
			}
}

function confirmSubmit(targetElement, targetElement2){
	var agree=confirm("Please confirm that you want to delete this contract account ?");
	var id = targetElement;
	var contractid=targetElement2;
	if (agree){
        window.location="deleteContractAccountID.html?delAccountId="+id+"&id="+contractid+"&btnType=del";
	}
}
function show(){
	var redirect = document.forms['contractAccountList'].elements['btnType'].value;
	if(redirect=='del'){
		location.href = "contractAccountList.html?id=${contract.id}"
	}
}

function checkReadOnly(){
    var chkCorpId= '${contract.owner}';
    var corpId='${contractOrgId}'; 
    if(corpId!=chkCorpId){
    		document.forms['contractAccountList'].elements['add'].disabled = true;
    		document.forms['contractAccountList'].elements['copyContractAccount'].disabled = true;
    }
}
function refreshChild(targetValue){
	//alert(targetValue);
	var currentContract='${contract.contract}';
	var idValue='${contract.id}';
	window.location= "searchParentCode.html?childContract="+encodeURI(currentContract) +"&btnRefType=Ref&id="+idValue+"&parentCode="+ encodeURI(targetValue);
	///document.forms['contractAccountList'].action = url;
	//document.forms['contractAccountList'].submit();
}
</script>

<script type="text/javascript">
try{ show();
}
catch(e){}
try{checkReadOnly();}
	catch(e){}
	
</script>
