
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetServiceCategoryTypesResult" type="{http://www.mss1.com/webservices/}ArrayOfMSSServiceCategoryType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getServiceCategoryTypesResult"
})
@XmlRootElement(name = "GetServiceCategoryTypesResponse")
public class GetServiceCategoryTypesResponse {

    @XmlElement(name = "GetServiceCategoryTypesResult")
    protected ArrayOfMSSServiceCategoryType getServiceCategoryTypesResult;

    /**
     * Gets the value of the getServiceCategoryTypesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfMSSServiceCategoryType }
     *     
     */
    public ArrayOfMSSServiceCategoryType getGetServiceCategoryTypesResult() {
        return getServiceCategoryTypesResult;
    }

    /**
     * Sets the value of the getServiceCategoryTypesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfMSSServiceCategoryType }
     *     
     */
    public void setGetServiceCategoryTypesResult(ArrayOfMSSServiceCategoryType value) {
        this.getServiceCategoryTypesResult = value;
    }

}
