// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`charges` ADD COLUMN `status` bit(1) DEFAULT b'1' AFTER `rollUpInInvoice`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `service` VARCHAR(30) , ADD COLUMN `warehouse` VARCHAR(20);

ALTER TABLE `redsky`.`partnerprivate`
 ADD COLUMN `privateCurrency` varchar(4) DEFAULT NULL AFTER `vatBillingGroup` ,
 ADD COLUMN `privateBankCode` varchar(20) DEFAULT NULL AFTER `privateCurrency` ,
 ADD COLUMN `privateBankAccountNumber` varchar(40) DEFAULT NULL  AFTER `privateBankCode` ,
 ADD COLUMN `privateVatNumber` varchar(25) AFTER `privateBankAccountNumber`;
 
 ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `opsPerson` VARCHAR(82) DEFAULT NULL AFTER `isUpdater`;
 
 
 // as per ashish sir
 insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('TrackingStatus','originGivenCode' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','originGivenName' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','originReceivedCode' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','originReceivedName' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','destinationGivenCode' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','destinationGivenName' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','destinationReceivedCode' ,'Update','amishra',now(),'amishra',now(),false,'','',''),
('TrackingStatus','destinationReceivedName' ,'Update','amishra',now(),'amishra',now(),false,'','','');


CREATE  TABLE `redsky`.`updaterule` (
 `id` BIGINT(20)   NOT NULL AUTO_INCREMENT ,
 `tableName` VARCHAR(100)  DEFAULT NULL ,
 `fieldToUpdate` VARCHAR(225) DEFAULT NULL ,
 `validationContion` TEXT DEFAULT NULL ,
  `updateFieldValue` VARCHAR(225) DEFAULT  NULL ,
  `ruleName` VARCHAR(225) DEFAULT  NULL ,
  `checkEnable` BIT(1)  DEFAULT  NULL ,
  `ruleStatus` VARCHAR(50) DEFAULT NULL ,
  `remarks` TEXT DEFAULT NULL ,
  `updatedBy` VARCHAR(225) DEFAULT NULL ,
  `updatedOn` DATETIME DEFAULT NULL ,
  `createdBy` VARCHAR(225) DEFAULT NULL ,
  `createdOn` DATETIME DEFAULT NULL ,
  `corpID` VARCHAR(15) DEFAULT NULL,
   PRIMARY KEY (`id`) );


  CREATE  TABLE `redsky`.`updateruleresult` (
  `id` BIGINT(20)   NOT NULL AUTO_INCREMENT ,
  `tableName` VARCHAR(100)  DEFAULT NULL ,
  `fieldName` VARCHAR(225) DEFAULT NULL ,
  `ruleName`  VARCHAR(225) DEFAULT NULL ,
  `valueOfField` VARCHAR(225) DEFAULT  NULL ,
  `updatefieldValue` VARCHAR(225) DEFAULT  NULL ,
  `updatedBy` VARCHAR(225) DEFAULT NULL ,
  `updatedOn` DATETIME   DEFAULT NULL ,
  `createdBy` VARCHAR(225) DEFAULT NULL ,
  `createdOn` DATETIME  DEFAULT NULL ,
  `corpID` VARCHAR(15) DEFAULT NULL,
  PRIMARY KEY (`id`) );
  
  // do not run this. It is for reference only
  
  ALTER TABLE `redsky`.`updaterule` ADD COLUMN `corpID` VARCHAR(15) DEFAULT NULL  AFTER `createdOn` ;
  ALTER TABLE `redsky`.`updateruleresult` ADD COLUMN `corpID` VARCHAR(15) DEFAULT NULL  AFTER `createdOn` ;
  
  // as per sunil sir
  ALTER TABLE `redsky`.`todorule` CHANGE COLUMN `remarks` `remarks` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT '' ;
  
  insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
('UTSI','CAT','Candidate Assessment', 5, 'SERVICE','en','Relo','dspDetails.CAT_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','CLS','Closing Services', 5, 'SERVICE','en','Relo','dspDetails.CLS_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','CHS','Comparable Housing Study', 5, 'SERVICE','en','Relo','dspDetails.CHS_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','DPS','Departure Services', 5, 'SERVICE','en','Relo','dspDetails.DPS_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','HSM','Home Sale Marketing Assistance', 5, 'SERVICE','en','Relo','dspDetails.HSM_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','PDT','Policy Development', 5, 'SERVICE','en','Relo','dspDetails.PDT_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','RCP','Relocation Cost Projections', 5, 'SERVICE','en','Relo','dspDetails.RCP_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','SPA','Spousal Assistance', 5, 'SERVICE','en','Relo','dspDetails.SPA_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','TCS','Technology Solutions', 5, 'SERVICE','en','Relo','dspDetails.TCS_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','MTS','Mortgage Assistance', 5, 'SERVICE','en','Relo','dspDetails.MTS_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','VIS','Visa', 5, 'SERVICE','en','Relo','dspDetails.VIS_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','RNT','Home Finding / Rental Search', 5, 'SERVICE','en','Relo','dspDetails.RNT_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','REP','Residence Permit', 5, 'SERVICE','en','Relo','dspDetails.REP_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','WOP','Work Permit', 5, 'SERVICE','en','Relo','dspDetails.WOP_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','DSS','Destination School Services', 5, 'SERVICE','en','Relo','dspDetails.DSS_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','HOB','Hotel Bookings', 5, 'SERVICE','en','Relo','dspDetails.HOB_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),
('UTSI','FLB','Flight Bookings', 5, 'SERVICE','en','Relo','dspDetails.FLB_serviceEndDate',now(),'UTSI_SETUP',now(),'UTSI_SETUP'),

    ('UTSI', 'Company', 'Company', 25, 'DEPOSITBY','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Assignee', 'Assignee', 25, 'DEPOSITBY','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Agent', 'Agent', 25, 'DEPOSITBY','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Others', 'Others', 25, 'DEPOSITBY','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Employee', 'Employee', 25, 'DEPOSITBY','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Employer', 'Employer', 25, 'DEPOSITBY','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Highstar Relocation', 'Highstar Relocation', 25, 'DEPOSITBY','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Managed Lump Sum', 'Managed Lump Sum', 25, 'DEPOSITBY','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Gas', 'Gas', 25, 'UTILITIES','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Water', 'Water', 25, 'UTILITIES','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Electricity', 'Electricity', 25, 'UTILITIES','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'TV', 'TV', 25, 'UTILITIES','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Internet', 'Internet', 25, 'UTILITIES','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Others', 'Others', 25, 'UTILITIES','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'None', 'None', 25, 'UTILITIES','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Initial Contact', 'Initial Contact', 25, 'VIEWSTATUS','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'House hunting', 'House hunting', 25, 'VIEWSTATUS','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Offer', 'Offer', 25, 'VIEWSTATUS','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Closed', 'Closed', 25, 'VIEWSTATUS','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'accepted', 'accepted', 25, 'VIEWSTATUS','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'discussed', 'discussed', 25, 'VIEWSTATUS','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'rejected', 'rejected', 25, 'VIEWSTATUS','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'shortlist', 'shortlist', 25, 'VIEWSTATUS','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'visited', 'visited', 25, 'VIEWSTATUS','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Days', 'Days', 25, 'TIMEDURATION','en','','',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'Hours', 'Hours', 25, 'TIMEDURATION','en','','',now(),'dkumar',now(),'dkumar');
  


insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_startDate'),        
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_endDate'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_hotelName'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_city'),         
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorCode'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorName'),      
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorContact'),      
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorEmail'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_serviceStartDate'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_serviceEndDate'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorCodeEXSO'),        
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_emailSent'),             
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_displyOtherVendorCode'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_displyOtherVendorCode'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_arrivalDate'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_departureDate'),       
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_additionalBookingReminderDate'),         
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_arrivalDate1'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_departureDate1'),      
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_additionalBookingReminderDate1'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_addOn'),      
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorCode'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorName'),             
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorContact'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorEmail'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_serviceStartDate'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_serviceEndDate'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorCodeEXSO'),                    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_emailSent'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_vendorCode'),        
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_vendorName'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_serviceStartDate'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_vendorCodeEXSO'),         
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_vendorContact'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_serviceEndDate'),      
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_vendorEmail'),      
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_paymentResponsibility'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_displyOtherVendorCode'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_lender'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_initialContact'),        
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_status'),             
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_mortgageAmount'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_mortgageRate'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_mortgageTerm'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','MTS','MTS_comment'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_vendorCode'),         
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_vendorName'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_serviceStartDate'),      
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_vendorCodeEXSO'),    
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_vendorContact'),      
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_serviceEndDate'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_vendorEmail'),             
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_paymentResponsibility'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_displyOtherVendorCode'),
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_initialContactDate'),          
            ('UTSI', now(),'dkumar', now(),'dkumar','dspdetails','DSS','DSS_comment'),
            
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_startDate'),        
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_endDate'),          
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_hotelName'),
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_city'),         
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorCode'),    
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorName'),      
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorContact'),      
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorEmail'),    
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_serviceStartDate'),          
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_serviceEndDate'),    
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_vendorCodeEXSO'),        
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_emailSent'),             
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','HOB','HOB_displyOtherVendorCode'),
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_displyOtherVendorCode'),          
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_arrivalDate'),          
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_departureDate'),       
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_additionalBookingReminderDate'),         
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_arrivalDate1'),    
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_departureDate1'),      
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_additionalBookingReminderDate1'),    
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_addOn'),      
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorCode'),
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorName'),             
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorContact'),
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorEmail'),
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_serviceStartDate'),          
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_serviceEndDate'),
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_vendorCodeEXSO'),                    
            ('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FLB','FLB_emailSent');            
     
 