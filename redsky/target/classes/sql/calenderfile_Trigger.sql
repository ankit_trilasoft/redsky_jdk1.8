
delimiter $$

CREATE trigger redsky.trigger_add_history_calendarfile
BEFORE UPDATE on redsky.calendarfile
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";

   IF (OLD.corpId <> NEW.corpId ) THEN
      select CONCAT(fieldNameList,'calendarfile.corpId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpId,'~') into newValueList;
  END IF;
   IF (OLD.state <> NEW.state ) THEN
      select CONCAT(fieldNameList,'calendarfile.state~') into fieldNameList;
      select CONCAT(oldValueList,OLD.state,'~') into oldValueList;
      select CONCAT(newValueList,NEW.state,'~') into newValueList;
  END IF;
   IF (OLD.userName <> NEW.userName ) THEN
      select CONCAT(fieldNameList,'calendarfile.userName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.userName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.userName,'~') into newValueList;
  END IF;

 IF (OLD.city <> NEW.city ) THEN
      select CONCAT(fieldNameList,'calendarfile.city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.city,'~') into newValueList;
  END IF;

   IF (OLD.activityName <> NEW.activityName ) THEN
      select CONCAT(fieldNameList,'calendarfile.activityName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.activityName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.activityName,'~') into newValueList;
  END IF;

  IF (OLD.billTo <> NEW.billTo ) THEN
      select CONCAT(fieldNameList,'calendarfile.billTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billTo,'~') into newValueList;
  END IF;
IF (OLD.fromTime <> NEW.fromTime ) THEN
      select CONCAT(fieldNameList,'calendarfile.fromTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fromTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fromTime,'~') into newValueList;
  END IF;
IF (OLD.toTime <> NEW.toTime ) THEN
      select CONCAT(fieldNameList,'calendarfile.toTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.toTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.toTime,'~') into newValueList;
  END IF;

  IF (OLD.eventType <> NEW.eventType ) THEN
      select CONCAT(fieldNameList,'calendarfile.eventType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.eventType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.eventType,'~') into newValueList;
  END IF;
IF (OLD.category <> NEW.category ) THEN
      select CONCAT(fieldNameList,'calendarfile.category~') into fieldNameList;
      select CONCAT(oldValueList,OLD.category,'~') into oldValueList;
      select CONCAT(newValueList,NEW.category,'~') into newValueList;
  END IF;
 IF (OLD.personId <> NEW.personId ) THEN
      select CONCAT(fieldNameList,'calendarfile.personId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.personId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.personId,'~') into newValueList;
  END IF;
 IF (OLD.firstName <> NEW.firstName ) THEN
      select CONCAT(fieldNameList,'calendarfile.firstName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.firstName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.firstName,'~') into newValueList;
  END IF;
 IF (OLD.lastName <> NEW.lastName ) THEN
      select CONCAT(fieldNameList,'calendarfile.lastName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastName,'~') into newValueList;
  END IF;

 
IF ((OLD.surveyDate <> NEW.surveyDate) or (OLD.surveyDate is null and NEW.surveyDate is not null)
      or (OLD.surveyDate is not null and NEW.surveyDate is null)) THEN

       select CONCAT(fieldNameList,'calendarfile.surveyDate~') into fieldNameList;
        IF(OLD.surveyDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.surveyDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.surveyDate is not null) THEN
        	select CONCAT(oldValueList,OLD.surveyDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.surveyDate is not null) THEN
        	select CONCAT(newValueList,NEW.surveyDate,'~') into newValueList;
      	END IF;

   END IF;

   IF ((OLD.surveyTillDate <> NEW.surveyTillDate) or (OLD.surveyTillDate is null and NEW.surveyTillDate is not null)
      or (OLD.surveyTillDate is not null and NEW.surveyTillDate is null)) THEN
       select CONCAT(fieldNameList,'calendarfile.surveyTillDate~') into fieldNameList;
       IF(OLD.surveyTillDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.surveyTillDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.surveyTillDate is not null) THEN
        	select CONCAT(oldValueList,OLD.surveyTillDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.surveyTillDate is not null) THEN
        	select CONCAT(newValueList,NEW.surveyTillDate,'~') into newValueList;
      	END IF;
   END IF;

  IF (OLD.fullDay <> NEW.fullDay ) THEN
       select CONCAT(fieldNameList,'calendarfile.fullDay~') into fieldNameList;
       IF(OLD.fullDay = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.fullDay = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.fullDay = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.fullDay = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

IF (OLD.surveyDays <> NEW.surveyDays ) THEN
      select CONCAT(fieldNameList,'calendarfile.surveyDays~') into fieldNameList;
      select CONCAT(oldValueList,OLD.surveyDays,'~') into oldValueList;
      select CONCAT(newValueList,NEW.surveyDays,'~') into newValueList;
  END IF;


 CALL add_tblHistory (OLD.id,"calendarfile", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;

