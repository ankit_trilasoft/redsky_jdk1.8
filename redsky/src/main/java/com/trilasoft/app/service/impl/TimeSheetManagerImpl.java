package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.TimeSheetDao;
import com.trilasoft.app.model.AvailableCrew;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Payroll;
import com.trilasoft.app.model.PayrollAllocation;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TimeSheet;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.TimeSheetManager;

public class TimeSheetManagerImpl  extends GenericManagerImpl<TimeSheet, Long> implements TimeSheetManager {
	 TimeSheetDao timeSheetDao;   
	  
    public TimeSheetManagerImpl(TimeSheetDao timeSheetDao) {   
        super(timeSheetDao);   
        this.timeSheetDao = timeSheetDao;   
    }
    
    public List<WorkTicket> findTickets(String warehouse,String service,String date1, String shipper, String tkt, String sessionCorpID){
    	return timeSheetDao.findTickets(warehouse, service, date1, shipper, tkt,sessionCorpID);
    }
    public List<WorkTicket> findTicket(Long tkt){
    	return timeSheetDao.findTicket(tkt);
    }
    public List<Billing> getBilling(String shipNum){
    	return timeSheetDao.getBilling(shipNum);
    }
    public List<PayrollAllocation> findDistAndCalcCode(String job, String service, String sessionCorpID,String mode,String comDiv){
    	return timeSheetDao.findDistAndCalcCode(job, service, sessionCorpID,mode,comDiv);
    }
    
    public List<AvailableCrew> searchAvailableCrews(String warehouse,String date1, String crewNm, String sessionCorpID){
    	return timeSheetDao.searchAvailableCrews(warehouse, date1, crewNm, sessionCorpID);
    }
    
    public List<Payroll> findTimeSheets(String warehouse,String date1, String shipper, String tkt, String crewNm, String sessionCorpID){
    	return timeSheetDao.findTimeSheets(warehouse, date1, shipper, tkt, crewNm,  sessionCorpID);
    }
    
    public List<SystemDefault> findSystemDefault(String corpID){
    	return timeSheetDao.findSystemDefault(corpID);
    }
    public List getTimeHoursFormSysDefault(String crewCorpId){
    	return timeSheetDao.getTimeHoursFormSysDefault(crewCorpId);
    }
    public List<AvailableCrew> findCrewList(){
    	return timeSheetDao.findCrewList();
    }
    public List existsTimeSheet(Date wDate, String uName, String wHouse){
    	return timeSheetDao.existsTimeSheet(wDate, uName, wHouse);
    }
    public List findTimeSheetByTicket(Long ticket){
    	return timeSheetDao.findTimeSheetByTicket(ticket);
    }
    /*public List<DayDistributionHoursDTO> findDayDistributionHoursList(Long ticket, String actWgt, String totRev){
    	return timeSheetDao.findDayDistributionHoursList(ticket, actWgt, totRev);
    }*/
    public List findDayDistributionHoursList(Long ticket, String actWgt){
    	return timeSheetDao.findDayDistributionHoursList(ticket, actWgt);
    }
    public String findExpression(String calc, String sessionCorpID,String compDivision){
    	return timeSheetDao.findExpression(calc,sessionCorpID,compDivision);
    }
    public List findWorkDays(Long ticket){
    	return timeSheetDao.findWorkDays(ticket);
    }
    public List findTotalHoursWorked(Long ticket){
    	return timeSheetDao.findTotalHoursWorked(ticket);
    }
    public List findNormalDist(Long ticket, Date workDt, String job, String service, String compDivision){
    	return timeSheetDao.findNormalDist(ticket, workDt, job, service,compDivision);
    }
    public List findTimeSheetByTicketAndDate(Long ticket, Date wrkDt){
    	return timeSheetDao.findTimeSheetByTicketAndDate(ticket, wrkDt);
    }
    
    public Boolean totalRevenueShare(Long ticket){
    	return timeSheetDao.totalRevenueShare(ticket);
    }
    
    public TimeSheet findTimeSheetDetail(){
    	return timeSheetDao.findTimeSheetDetail();
    }
    
    public void updateWorkTicketData(Long ticket, String totalRevenue, String calcCode){
    	timeSheetDao.updateWorkTicketData( ticket, totalRevenue, calcCode);
    }
    public List findTimeSheetsForOT(String beginDt, String endDt, String wareHse, String sessionCorpID){
    	return timeSheetDao.findTimeSheetsForOT(beginDt, endDt, wareHse,sessionCorpID);
    }
    public List checkRevenueSharing(String crewNm, String workDt, String wareHse,String sessionCorpID, String endDt){
    	return timeSheetDao.checkRevenueSharing(crewNm, workDt, wareHse,sessionCorpID, endDt);
    }
    public List findTimeSheetsForNonUnionOT(String beginDt, String endDt, String wareHse, String sessionCorpID){
    	return timeSheetDao.findTimeSheetsForNonUnionOT(beginDt, endDt, wareHse,sessionCorpID);
    }
    public List findSummerTimeSheetsForOT(String beginDt, String endDt, String wareHse, String sessionCorpID){
    	return timeSheetDao.findSummerTimeSheetsForOT(beginDt, endDt, wareHse,sessionCorpID);
    }
    public List findNonRevenueHrs(String crewNm, String workDt, String wareHse){
    	return timeSheetDao.findNonRevenueHrs(crewNm, workDt, wareHse);
    }
    public List findOTHrs(String crewNm, String workDt, String wareHse){
    	return timeSheetDao.findOTHrs(crewNm, workDt, wareHse);
    }
    public List findAllTimeSheets(String beginDt, String endDt, String wareHse, String sessionCorpID ){
    	return timeSheetDao.findAllTimeSheets(beginDt, endDt, wareHse,sessionCorpID);
    }
    public List checkExtraPaidTimeSheet(String workDt,String userName){
    	return timeSheetDao.checkExtraPaidTimeSheet( workDt, userName);
    }
    public List checkActionOTimeSheet(String workDt,String userName){
    	return timeSheetDao.checkActionOTimeSheet( workDt, userName);
    }

	public List getTimeFromEndHours(Long ticket, String crewName, Date date1) {
		return timeSheetDao.getTimeFromEndHours(ticket, crewName,date1);
	}

	public List getBiginHoursTime(Long ticket, String crewName, Date workDate) {
		return timeSheetDao.getBiginHoursTime(ticket, crewName, workDate);
	}

	public List findRegularHours(Long ticket, String crewName, Date workDate) {
		return timeSheetDao.findRegularHours(ticket, crewName, workDate);
	}

	public void updateDoneForTheDay(String crewName, Date workDate, String DFD) {
		timeSheetDao.updateDoneForTheDay(crewName, workDate, DFD);
		
	}

	public List findOverTimeList(String beginDt, String endDt, String wareHse) {
		return timeSheetDao.findOverTimeList(beginDt, endDt, wareHse);
	}

	public void updateCrewType(String ticket) {
		timeSheetDao.updateCrewType(ticket);
		
	}

	public void deleteFromTimeSheet(String beginDt, String endDt, String wareHse, String sessionCorpID) {
		timeSheetDao.deleteFromTimeSheet(beginDt, endDt, wareHse, sessionCorpID);
	}

	public List getNumberOfCrew(String beginDt, String endDt, String wareHse, String sessionCorpID) {
		return timeSheetDao.getNumberOfCrew(beginDt, endDt, wareHse,sessionCorpID);
	}

	public List getMaxId(String userName) {
		return timeSheetDao.getMaxId(userName);
	}

	

	public List doneForDay(String wareHse, String newWrkDt, String tkt, String crewNm) {
		
		return timeSheetDao.doneForDay(wareHse, newWrkDt, tkt, crewNm);
	}

	public List excludeEearningOppertunity(String wareHse, String newWrkDt, String tkt, String crewNm) {
		
		return timeSheetDao.excludeEearningOppertunity(wareHse, newWrkDt, tkt, crewNm);
	}

	public List filterLunch(String wareHse, String newWrkDt, String tkt, String crewNm) {
		
		return timeSheetDao.filterLunch(wareHse, newWrkDt, tkt, crewNm);
	}

	public void updateOtherTimeSheet(String bhours, String eHours, String regHours, String doneForDay, String pRevShare, String id, String action, String lunch, String reasonForAdjustment, String overTime, String doubleOverTime, String differentPayBasis, String adjustmentToRevenue, String adjustedBy, String calcCode, String distCode) {
		timeSheetDao.updateOtherTimeSheet(bhours, eHours, regHours, doneForDay, pRevShare, id, action, lunch, reasonForAdjustment, overTime, doubleOverTime, differentPayBasis, adjustmentToRevenue, adjustedBy,calcCode,distCode);
		
	}

	public List getParentTimeSheet(String userName, Date workDate, String lunch) {
		return timeSheetDao.getParentTimeSheet(userName, workDate,lunch);
	}

	public void updateRegHours(String regHours, String userName, Date workDate, String lunch) {
		timeSheetDao.updateRegHours(regHours, userName, workDate, lunch);		
	}

	public List openTimeSheets(String wareHse, String newWrkDt, String tkt, String crewNm) {
		return timeSheetDao.openTimeSheets(wareHse, newWrkDt, tkt, crewNm);
	}

	public List getWorkTicket(Long ticket, String sessionCorpID) {
		return timeSheetDao.getWorkTicket(ticket,  sessionCorpID);
	}

	public void deleteLunchTicket(Date date, String name, String house) {
		timeSheetDao.deleteLunchTicket(date, name, house);
		
	}

	public List checkRegularHours(String beginDt, String endDt, String wareHse) {
		return timeSheetDao.checkRegularHours(beginDt, endDt, wareHse);
	}

	public List getBillingDiscount(String shipNumber, String sessionCorpID) {
		return timeSheetDao.getBillingDiscount(shipNumber, sessionCorpID);
	}

	public List getCompanyHolidays(String sessionCorpID, String workDate) {
		return timeSheetDao.getCompanyHolidays(sessionCorpID, workDate);
	}

	public List findDistributionCode(String actionCode, String sessionCorpID) {
		return timeSheetDao.findDistributionCode(actionCode, sessionCorpID);
	}

	public void updateRefmasterStopDate(String beginDt, String sessionCorpID, String wareHse) {
		timeSheetDao.updateRefmasterStopDate(beginDt, sessionCorpID, wareHse);
		
	}

	public List checkStopDate(String workDt, String sessionCorpID, String wareHse) {
		return timeSheetDao.checkStopDate(workDt, sessionCorpID, wareHse);
	}

	public List getLockPayrollList(String sessionCorpID) {
		return timeSheetDao.getLockPayrollList(sessionCorpID);
	}

	public List getcompanyDivision(String calcCode, String sessionCorpID) {
		return timeSheetDao.getcompanyDivision(calcCode, sessionCorpID);
	}

	public List getWarehouse(String id) {
		return timeSheetDao.getWarehouse(id);
	}

	public List checkStopDateForCrew(Date date1, String sessionCorpID, String warehouse) {
		return timeSheetDao.checkStopDateForCrew(date1, sessionCorpID, warehouse);
	}

	public List checkCH(Long ticket, Date workdtTmp, String sessionCorpID) {
		return timeSheetDao.checkCH(ticket, workdtTmp, sessionCorpID);
	}

	public List findTimeSheetsForNonUnionOTPost(String beginDt, String endDt, String wareHse, String sessionCorpID, String userName) {
		return timeSheetDao.findTimeSheetsForNonUnionOTPost(beginDt, endDt, wareHse, sessionCorpID, userName);
	}

	public List findTimeSheetsForUnionOTPost(String workDate, String wareHse2, String sessionCorpID, String userName) {
		return timeSheetDao.findTimeSheetsForUnionOTPost(workDate, wareHse2, sessionCorpID, userName);
	}

	public List findOTTimeSheetsForUnionOTPost(String workDate, String wareHse2, String sessionCorpID, String userName) {
		return timeSheetDao.findOTTimeSheetsForUnionOTPost(workDate, wareHse2, sessionCorpID, userName);
	}

	public List findWorkdate(String beginDt, String endDt, String wareHse2, String userName, String sessionCorpID2) {
		return timeSheetDao.findWorkdate(beginDt, endDt, wareHse2, userName, sessionCorpID2);
	}

	public List searchAvailableTrucks(String wareHse, String newWrkDt, String sessionCorpID,Boolean visibilityForCorpId) {
		return timeSheetDao.searchAvailableTrucks(wareHse, newWrkDt, sessionCorpID,visibilityForCorpId);
	}

	public List findSecheDuledResource(String wareHse, String newWrkDt,	String string, String tkt, String string2, String sessionCorpID) {
		return timeSheetDao.findSecheDuledResource(wareHse, newWrkDt, string, tkt, string2, sessionCorpID);
	}
	public List findFullCityAdd(String shipNumberForCity,String ocityForCity,String sessionCorpID){
		return timeSheetDao.findFullCityAdd(shipNumberForCity, ocityForCity, sessionCorpID);
	}
	public List findFullDestinationCityAdd(String shipNumberForCity,String dcityForCity,String sessionCorpID){
		return timeSheetDao.findFullDestinationCityAdd(shipNumberForCity, dcityForCity, sessionCorpID);
	}

	public List findResourceTickets(String tktWareHse, String newWrkDt,String sessionCorpID, String workticketService, String workTicketStatus) {
		return timeSheetDao.findResourceTickets(tktWareHse, newWrkDt, sessionCorpID, workticketService, workTicketStatus);
	}

	public List searchAvailableResourceCrews(String wareHse, String newWrkDt, String sessionCorpID, String showAbsent,String defaultSortValueFlag) {
		return timeSheetDao.searchAvailableResourceCrews(wareHse, newWrkDt, sessionCorpID, showAbsent, defaultSortValueFlag);
	}


	public List getCountFromTimeSheet(String ticket,  String date11, String wareHse,	String sessionCorpID) {
		return timeSheetDao.getCountFromTimeSheet(ticket, date11, wareHse, sessionCorpID);
	}
	
	public List findSecheDuledResourceOperation(String wareHse,	String newWrkDt, String string, String tkt, String string2,String sessionCorpID){
		return timeSheetDao.findSecheDuledResourceOperation(wareHse,newWrkDt, string, tkt, string2, sessionCorpID);
		
	}
	public List getCountForTruckOpsId(String ticket, String date11,String wareHse, String sessionCorpID, Long truckOpsId){
		return timeSheetDao.getCountForTruckOpsId(ticket, date11,wareHse, sessionCorpID,truckOpsId);
	}
	public List getCountForTimeSheetId(String ticket, String date11,String wareHse, String sessionCorpID, Long timeSheetId){
		return timeSheetDao.getCountForTimeSheetId(ticket, date11,wareHse, sessionCorpID,timeSheetId);
	}

	public void updateWorkTicketCrew(Long ticket, String updatedBy,String sessionCorpID) {
          timeSheetDao.updateWorkTicketCrew(ticket, updatedBy,sessionCorpID);
	}

	public Integer selectedWorkTicketSize(String tktWareHse, String newWrkDt,String sessionCorpID, String workticketService, String workTicketStatus) {
		return timeSheetDao.selectedWorkTicketSize(tktWareHse, newWrkDt, sessionCorpID, workticketService, workTicketStatus);
	}

	public Integer selectedAvailableCrewSize(String wareHse, String newWrkDt,String sessionCorpID) {
		return timeSheetDao.selectedAvailableCrewSize(wareHse, newWrkDt,sessionCorpID);
	}

	public Integer selectedAvailableTrucksSize(String truckWareHse,String newWrkDt, String sessionCorpID) {
		return timeSheetDao.selectedAvailableTrucksSize(truckWareHse,newWrkDt,sessionCorpID);
	}

	public List findNonSelectedResourceTickets(String tktWareHse,String newWrkDt, String sessionCorpID, String workticketService, String workTicketStatus) {
		return timeSheetDao.findNonSelectedResourceTickets(tktWareHse, newWrkDt, sessionCorpID, workticketService, workTicketStatus);
	}
	public void updateDriver(String driverId, Boolean driverChecked){
		timeSheetDao.updateDriver(driverId, driverChecked);
	}
	public List findResourceItemsList(String tktWareHse, String newWrkDt, String sessionCorpID){
		return timeSheetDao.findResourceItemsList(tktWareHse, newWrkDt, sessionCorpID);
	}
	public List findToolTipResourceDetail(String sessionCorpID,String ticket, String type){
		return timeSheetDao.findToolTipResourceDetail(sessionCorpID, ticket, type);
	}
	public List findPreviewOverTimeList(String beginDt, String endDt, String wareHse){
		return timeSheetDao.findPreviewOverTimeList(beginDt, endDt, wareHse);
	}
	 public List findTimeSheetsForOTPreview(String beginDt, String endDt, String wareHse, String sessionCorpID){
		 return timeSheetDao.findTimeSheetsForOTPreview(beginDt, endDt, wareHse, sessionCorpID);
	 }
	 
	 public List findScheduleResourceSeparateCrewList(String wareHse, String newWrkDt,String tkt, String sessionCorpID,String projectName,String projectNumber){
		 return timeSheetDao.findScheduleResourceSeparateCrewList(wareHse, newWrkDt,tkt,sessionCorpID,projectName,projectNumber);
	 }
	 
	 public List findScheduleResourceSeparateTruckList(String wareHse, String newWrkDt,String tkt, String sessionCorpID,String projectName,String projectNumber){
		 return timeSheetDao.findScheduleResourceSeparateTruckList(wareHse, newWrkDt,tkt,sessionCorpID,projectName,projectNumber);
	 }
	 public void updateAssignedCrewForTruck(String updateQuery){
		 timeSheetDao.updateAssignedCrewForTruck(updateQuery);
	 }
	 public Map<Map, Map> findSecheDuledResourceMap(String wareHse, String newWrkDt,String sessionCorpID,String whereClauseSecheDuledResourceMap,String whereClauseSecheDuledResourceMap1){
		 return timeSheetDao.findSecheDuledResourceMap(wareHse, newWrkDt,sessionCorpID, whereClauseSecheDuledResourceMap, whereClauseSecheDuledResourceMap1);
	 }
	 public List findDistinctFieldValue(String wareHse,String newWrkDt,String fieldName,String sessionCorpID){
		 return timeSheetDao.findDistinctFieldValue(wareHse, newWrkDt, fieldName, sessionCorpID);
	 }
	 public Boolean getPlannedMove(String code,String parameter,String corpId){
		 return timeSheetDao.getPlannedMove(code, parameter, corpId);
	 }
	 public List findCrewResourceDetail(String sessionCorpID,String ticket){
		 return timeSheetDao.findCrewResourceDetail(sessionCorpID, ticket);
	 }
	 public List findVehicleResourceDetail(String sessionCorpID,String ticket){
		 return timeSheetDao.findVehicleResourceDetail(sessionCorpID, ticket);
	 }
	 public List findEquipmentResourceDetail(String sessionCorpID,String ticket){
		 return timeSheetDao.findEquipmentResourceDetail(sessionCorpID, ticket);
	 }
	 public List findMaterialResourceDetail(String sessionCorpID,String ticket){
		 return timeSheetDao.findMaterialResourceDetail(sessionCorpID, ticket);
	 }
	 public String findIssuedDriverCheck(String sessionCorpID,Long ticket){
		 return timeSheetDao.findIssuedDriverCheck(sessionCorpID, ticket);
	 }
}
