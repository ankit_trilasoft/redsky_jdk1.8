package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.InventoryArticle;

public interface InventoryArticleDao extends GenericDao<InventoryArticle,Long>{
   List	getArticleAutoComplete(String sessionCorpID,String article);
}
