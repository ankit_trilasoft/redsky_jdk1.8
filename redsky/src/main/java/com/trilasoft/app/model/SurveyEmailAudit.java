package com.trilasoft.app.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table ( name="surveyemailaudit") 
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class SurveyEmailAudit extends BaseObject{
	private Long id;
	private String accountId;
	private Long surveyEmailId;
	private Date lastSent;
	private Integer numberSent;
	private Boolean recievedResponse;
	private String corpId;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String shipNumber;
	private String sequenceNumber;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("accountId", accountId)
				.append("surveyEmailId", surveyEmailId)
				.append("lastSent", lastSent).append("numberSent", numberSent)
				.append("recievedResponse", recievedResponse)
				.append("corpId", corpId).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("shipNumber",shipNumber).append("sequenceNumber",sequenceNumber).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SurveyEmailAudit))
			return false;
		SurveyEmailAudit castOther = (SurveyEmailAudit) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(accountId, castOther.accountId)
				.append(surveyEmailId, castOther.surveyEmailId)
				.append(lastSent, castOther.lastSent)
				.append(numberSent, castOther.numberSent)
				.append(recievedResponse, castOther.recievedResponse)
				.append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn).append(shipNumber, castOther.shipNumber).append(sequenceNumber, castOther.sequenceNumber).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(accountId)
				.append(surveyEmailId).append(lastSent).append(numberSent)
				.append(recievedResponse).append(corpId).append(createdBy)
				.append(createdOn).append(updatedBy).append(updatedOn).append(shipNumber).append(sequenceNumber)
				.toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=8)
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public Long getSurveyEmailId() {
		return surveyEmailId;
	}
	public void setSurveyEmailId(Long surveyEmailId) {
		this.surveyEmailId = surveyEmailId;
	}
	@Column
	public Date getLastSent() {
		return lastSent;
	}
	public void setLastSent(Date lastSent) {
		this.lastSent = lastSent;
	}
	public Integer getNumberSent() {
		return numberSent;
	}
	public void setNumberSent(Integer numberSent) {
		this.numberSent = numberSent;
	}
	@Column
	public Boolean getRecievedResponse() {
		return recievedResponse;
	}
	public void setRecievedResponse(Boolean recievedResponse) {
		this.recievedResponse = recievedResponse;
	}
	@Column(length=15)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
}
