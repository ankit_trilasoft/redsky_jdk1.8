// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`billing` MODIFY COLUMN `vendorStorageVatPercent` DECIMAL(19,2) DEFAULT 0.00; 

ALTER TABLE `redsky`.`billing` ADD COLUMN `fXRateOnActualizationDate` bit(1) DEFAULT b'0' AFTER `recSignedDisclaimerDate`;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('Billing','fXRateOnActualizationDate' ,'Create','dkumar',now(),'dkumar',now(),false,'','','CMM/DMM'),
('Billing','fXRateOnActualizationDate' ,'Update','dkumar',now(),'dkumar',now(),false,'','','CMM/DMM');

// As per ravi
DROP VIEW IF EXISTS `redsky`.`partner`;
CREATE VIEW `partner` AS select `a`.`id` AS `id`,`a`.`parent` AS `parent`,`a`.`middleInitial` AS `middleInitial`,`a`.`billingAddress1` AS `billingAddress1`,`a`.`billingAddress2` AS `billingAddress2`,`a`.`billingAddress3` AS `billingAddress3`,`a`.`billingAddress4` AS `billingAddress4`,`a`.`billingCity` AS `billingCity`,`a`.`billingCountry` AS `billingCountry`,`a`.`billingCountryCode` AS `billingCountryCode`,`a`.`billingEmail` AS `billingEmail`,`a`.`billingFax` AS `billingFax`,`a`.`billingPhone` AS `billingPhone`,`a`.`billingState` AS `billingState`,`a`.`billingTelex` AS `billingTelex`,`a`.`billingZip` AS `billingZip`,`a`.`effectiveDate` AS `effectiveDate`,`a`.`isAccount` AS `isAccount`,`a`.`isAgent` AS `isAgent`,`a`.`isCarrier` AS `isCarrier`,`a`.`isVendor` AS `isVendor`,`a`.`mailingAddress1` AS `mailingAddress1`,`a`.`mailingAddress2` AS `mailingAddress2`,`a`.`mailingAddress3` AS `mailingAddress3`,`a`.`mailingAddress4` AS `mailingAddress4`,`a`.`mailingCity` AS `mailingCity`,`a`.`mailingCountry` AS `mailingCountry`,`a`.`mailingCountryCode` AS `mailingCountryCode`,`a`.`mailingEmail` AS `mailingEmail`,`a`.`mailingFax` AS `mailingFax`,`a`.`mailingPhone` AS `mailingPhone`,`a`.`mailingState` AS `mailingState`,`a`.`mailingTelex` AS `mailingTelex`,`a`.`mailingZip` AS `mailingZip`,`a`.`partnerPrefix` AS `partnerPrefix`,`a`.`partnerSuffix` AS `partnerSuffix`,`a`.`terminalAddress1` AS `terminalAddress1`,`a`.`terminalAddress2` AS `terminalAddress2`,`a`.`terminalAddress3` AS `terminalAddress3`,`a`.`terminalAddress4` AS `terminalAddress4`,`a`.`terminalCity` AS `terminalCity`,`a`.`terminalCountry` AS `terminalCountry`,`a`.`terminalCountryCode` AS `terminalCountryCode`,`a`.`terminalEmail` AS `terminalEmail`,`a`.`terminalFax` AS `terminalFax`,`a`.`terminalPhone` AS `terminalPhone`,`a`.`terminalState` AS `terminalState`,`a`.`terminalTelex` AS `terminalTelex`,`a`.`terminalZip` AS `terminalZip`,`a`.`isPrivateParty` AS `isPrivateParty`,`a`.`air` AS `air`,`a`.`sea` AS `sea`,`a`.`surface` AS `surface`,`a`.`isOwnerOp` AS `isOwnerOp`,`a`.`typeOfVendor` AS `typeOfVendor`,`a`.`agentParent` AS `agentParent`,`a`.`location1` AS `location1`,`a`.`location2` AS `location2`,`a`.`location3` AS `location3`,`a`.`location4` AS `location4`,`a`.`companyProfile` AS `companyProfile`,`a`.`url` AS `url`,`a`.`latitude` AS `latitude`,`a`.`longitude` AS `longitude`,`a`.`trackingUrl` AS `trackingUrl`,`a`.`yearEstablished` AS `yearEstablished`,`a`.`companyFacilities` AS `companyFacilities`,`a`.`companyCapabilities` AS `companyCapabilities`,`a`.`companyDestiantionProfile` AS `companyDestiantionProfile`,`a`.`serviceRangeKms` AS `serviceRangeKms`,`a`.`serviceRangeMiles` AS `serviceRangeMiles`,`a`.`fidiNumber` AS `fidiNumber`,`a`.`OMNINumber` AS `OMNINumber`,`a`.`IAMNumber` AS `IAMNumber`,`a`.`AMSANumber` AS `AMSANumber`,`a`.`WERCNumber` AS `WERCNumber`,`a`.`facilitySizeSQFT` AS `facilitySizeSQFT`,`a`.`qualityCertifications` AS `qualityCertifications`,`a`.`vanLineAffiliation` AS `vanLineAffiliation`,`a`.`serviceLines` AS `serviceLines`,`a`.`facilitySizeSQMT` AS `facilitySizeSQMT`,`b`.`corpid` AS `corpid`,`b`.`warehouse` AS `warehouse`,`a`.`createdBy` AS `createdBy`,`a`.`createdOn` AS `createdOn`,`a`.`updatedBy` AS `updatedBy`,`a`.`updatedOn` AS `updatedOn`,`a`.`firstName` AS `firstName`,`a`.`lastName` AS `lastName`,if((`a`.`isPrivateParty` is true),`a`.`status`,`b`.`status`) AS `status`,`a`.`nextVanLocation` AS `nextVanLocation`,`a`.`nextReportOn` AS `nextReportOn`,`b`.`billingInstruction` AS `billingInstruction`,`b`.`billingInstructionCode` AS `billingInstructionCode`,`b`.`coordinator` AS `coordinator`,`b`.`salesMan` AS `salesMan`,`b`.`qc` AS `qc`,`b`.`abbreviation` AS `abbreviation`,`b`.`billPayOption` AS `billPayOption`,`b`.`billPayType` AS `billPayType`,`b`.`billToGroup` AS `billToGroup`,`b`.`longPercentage` AS `longPercentage`,`b`.`needAuth` AS `needAuth`,`a`.`partnerCode` AS `partnerCode`,`b`.`storageBillingGroup` AS `storageBillingGroup`,`b`.`accountHolder` AS `accountHolder`,`b`.`paymentMethod` AS `paymentMethod`,`b`.`payOption` AS `payOption`,`b`.`multiAuthorization` AS `multiAuthorization`,`b`.`payableUploadCheck` AS `payableUploadCheck`,`b`.`companyDivision` AS `companyDivision`,`b`.`invoiceUploadCheck` AS `invoiceUploadCheck`,`b`.`billingUser` AS `billingUser`,`b`.`payableUser` AS `payableUser`,`b`.`pricingUser` AS `pricingUser`,`b`.`partnerPortalActive` AS `partnerPortalActive`,`b`.`partnerPortalId` AS `partnerPortalId`,`b`.`associatedAgents` AS `associatedAgents`,`b`.`viewChild` AS `viewChild`,`b`.`creditTerms` AS `creditTerms`,`b`.`accountingDefault` AS `accountingDefault`,`b`.`acctDefaultJobType` AS `acctDefaultJobType`,`b`.`stopNotAuthorizedInvoices` AS `stopNotAuthorizedInvoices`,`b`.`doNotCopyAuthorizationSO` AS `doNotCopyAuthorizationSO`,`b`.`partnerPublicId` AS `partnerPublicId`,`b`.`driverAgency` AS `driverAgency`,`b`.`validNationalCode` AS `validNationalCode`,`b`.`cardNumber` AS `cardNumber`,`b`.`cardStatus` AS `cardStatus`,`b`.`accountId` AS `accountId`,`b`.`customerId` AS `customerId`,`b`.`licenseNumber` AS `licenseNumber`,`b`.`licenseState` AS `licenseState`,`b`.`cardSettelment` AS `cardSettelment`,`b`.`directDeposit` AS `directDeposit`,`b`.`fuelPurchase` AS `fuelPurchase`,`b`.`expressCash` AS `expressCash`,`b`.`creditCheck` AS `creditCheck`,`b`.`creditAmount` AS `creditAmount`,`b`.`creditCurrency` AS `creditCurrency`,`b`.`yruAccess` AS `yruAccess`,`b`.`taxId` AS `taxId`,`b`.`taxIdType` AS `taxIdType`,`b`.`extReference` AS `extReference`,`a`.`startDate` AS `startDate`,`b`.`accountManager` AS `accountManager`,`b`.`classcode` AS `classcode`,`a`.`vatNumber` AS `vatNumber`,`a`.`utsNumber` AS `utsNumber`,`a`.`isNetworkPartner` AS `isNetworkPartner`,`a`.`vanLastLocation` AS `vanLastLocation`,`a`.`vanLastReportOn` AS `vanLastReportOn`,`a`.`vanAvailCube` AS `vanAvailCube`,`a`.`vanLastReportTime` AS `vanLastReportTime`,`a`.`currentVanAgency` AS `currentVanAgency`,`a`.`currentVanID` AS `currentVanID`,`a`.`currentTractorAgency` AS `currentTractorAgency`,`a`.`currentTractorID` AS `currentTractorID`,`b`.`driverType` AS `driverType`,`b`.`noDispatch` AS `noDispatch`,`a`.`aliasName` AS `aliasName`,`a`.`networkGroup` AS `networkGroup`,`b`.`insuranceAuthorized` AS `insuranceAuthorized`,`b`.`mc` AS `mc`,`b`.`soAllDrivers` AS `soAllDrivers`,`a`.`contactName` AS `contactName`,`a`.`billingCurrency` AS `billingCurrency`,`a`.`bankCode` AS `bankCode`,`a`.`bankAccountNumber` AS `bankAccountNumber`,`a`.`agentParentName` AS `agentParentName`,`a`.`ugwwNetworkGroup` AS `ugwwNetworkGroup`,`b`.`collection` AS `collection`,`a`.`isPartnerExtract` AS `isPartnerExtract`,`a`.`agentGroup` AS `agentGroup`,`a`.`partnerType` AS `partnerType`,`b`.`noInsurance` AS `noInsurance`,`b`.`lumpSum` AS `lumpSum`,`b`.`detailedList` AS `detailedList` from (`partnerpublic` `a` join `partnerprivate` `b`) where (`a`.`id` = `b`.`partnerPublicId`);
//

ALTER TABLE `redsky`.`proposalmanagement` 
ADD COLUMN `releaseDate` DATETIME NULL DEFAULT NULL  AFTER `billTillDate` ;

ALTER TABLE `redsky`.`company` ADD COLUMN `enableMoveForYou` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`company` ADD COLUMN `enableMSS` BIt(1) DEFAULT b'0'; 

insert into redsky.corp_comp_permission(corp_id, mask,componentId, description,  createdBy, createdOn, updatedBy, updatedOn)
values
('AATL', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','AATL_SETUP', now(), 'AATL_SETUP', now()),
('ADAM', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','ADAM_SETUP', now(), 'ADAM_SETUP', now()),
('BONG', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','BONG_SETUP', now(), 'BONG_SETUP', now()),
('BOUR', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','BOUR_SETUP', now(), 'BOUR_SETUP', now()),
('BRSH', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','BRSH_SETUP', now(), 'BRSH_SETUP', now()),
('CAMP', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','CAMP_SETUP', now(), 'CAMP_SETUP', now()),
('CMLK', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','CMLK_SETUP', now(), 'CMLK_SETUP', now()),
('CWMS', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','CWMS_SETUP', now(), 'CWMS_SETUP', now()),
('DECA', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','DECA_SETUP', now(), 'DECA_SETUP', now()),
('GERS', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','GERS_SETUP', now(), 'GERS_SETUP', now()),
('HIGH', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','HIGH_SETUP', now(), 'HIGH_SETUP', now()),
('HOLL', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','HOLL_SETUP', now(), 'HOLL_SETUP', now()),
('ICMG', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','ICMG_SETUP', now(), 'ICMG_SETUP', now()),
('INTM', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','INTM_SETUP', now(), 'INTM_SETUP', now()),
('JOHN', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','JOHN_SETUP', now(), 'JOHN_SETUP', now()),
('KOTA', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','KOTA_SETUP', now(), 'KOTA_SETUP', now()),
('KTMS', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','KTMS_SETUP', now(), 'KTMS_SETUP', now()),
('PUTT', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','PUTT_SETUP', now(), 'PUTT_SETUP', now()),
('RSKY', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','RSKY_SETUP', now(), 'RSKY_SETUP', now()),
('SSCW', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','SSCW_SETUP', now(), 'SSCW_SETUP', now()),
('STAR', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','STAR_SETUP', now(), 'STAR_SETUP', now()),
('TFMS', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','TFMS_SETUP', now(), 'TFMS_SETUP', now()),
('TSFT', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','TSFT_SETUP', now(), 'TSFT_SETUP', now()),
('UGCA', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','UGCA_SETUP', now(), 'UGCA_SETUP', now()),
('UGCN', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','UGCN_SETUP', now(), 'UGCN_SETUP', now()),
('UGHK', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','UGHK_SETUP', now(), 'UGHK_SETUP', now()),
('UGJP', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','UGJP_SETUP', now(), 'UGJP_SETUP', now()),
('UGMY', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','UGMY_SETUP', now(), 'UGMY_SETUP', now()),
('UGSG', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','UGSG_SETUP', now(), 'UGSG_SETUP', now()),
('UGWW', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','UGWW_SETUP', now(), 'UGWW_SETUP', now()),
('UTSI', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','UTSI_SETUP', now(), 'UTSI_SETUP', now()),
('VOAO', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','VOAO_SETUP', now(), 'VOAO_SETUP', now()),
('VOCZ', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','VOCZ_SETUP', now(), 'VOCZ_SETUP', now()),
('VOER', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','VOER_SETUP', now(), 'VOER_SETUP', now()),
('VORU', 2,'component.serviceOrder.section.customerFeedback.view', 'component.serviceOrder.section.customerFeedback.view','VORU_SETUP', now(), 'VORU_SETUP', now());



ALTER TABLE `redsky`.`dspdetails`
ADD COLUMN `PRV_emailSent` DATETIME AFTER `MMG_destinationAgentEmail`,
ADD COLUMN `TAX_emailSent` DATETIME AFTER `PRV_emailSent`,
ADD COLUMN `AIO_emailSent` DATETIME AFTER `TAX_emailSent`,
ADD COLUMN `TRG_emailSent` DATETIME AFTER `AIO_emailSent`,
ADD COLUMN `LAN_emailSent` DATETIME AFTER `TRG_emailSent`,
ADD COLUMN `RPT_emailSent` DATETIME AFTER `LAN_emailSent`,
ADD COLUMN `COL_emailSent` DATETIME AFTER `RPT_emailSent`,
ADD COLUMN `TAC_emailSent` DATETIME AFTER `COL_emailSent`,
ADD COLUMN `HOM_emailSent` DATETIME AFTER `TAC_emailSent`,
ADD COLUMN `SCH_emailSent` DATETIME AFTER `HOM_emailSent`,
ADD COLUMN `EXP_emailSent` DATETIME AFTER `SCH_emailSent`,
ADD COLUMN `ONG_emailSent` DATETIME AFTER `EXP_emailSent`,
ADD COLUMN `TEN_emailSent` DATETIME AFTER `ONG_emailSent`,
ADD COLUMN `MMG_emailSent` DATETIME AFTER `TEN_emailSent`,
ADD COLUMN `CAR_emailSent` DATETIME AFTER `MMG_orginEmailSent`,
ADD COLUMN `CAR_emailSent` DATETIME AFTER `MMG_destinationEmailSent`,
ADD COLUMN `SET_emailSent` DATETIME AFTER `CAR_emailSent`,
ADD COLUMN `CAT_emailSent` DATETIME AFTER `SET_emailSent`,
ADD COLUMN `CLS_emailSent` DATETIME AFTER `CAT_emailSent`,
ADD COLUMN `CHS_emailSent` DATETIME AFTER `CLS_emailSent`,
ADD COLUMN `DPS_emailSent` DATETIME AFTER `CHS_emailSent`,
ADD COLUMN `HSM_emailSent` DATETIME AFTER `DPS_emailSent`,
ADD COLUMN `PDT_emailSent` DATETIME AFTER `HSM_emailSent`,
ADD COLUMN `RCP_emailSent` DATETIME AFTER `PDT_emailSent`,
ADD COLUMN `SPA_emailSent` DATETIME AFTER `RCP_emailSent`,
ADD COLUMN `TCS_emailSent` DATETIME AFTER `SPA_emailSent`,
ADD COLUMN `MTS_emailSent` DATETIME AFTER `TCS_emailSent`,
ADD COLUMN `DSS_emailSent` DATETIME AFTER `MTS_emailSent`,
ADD COLUMN `VIS_emailSent` DATETIME AFTER `DSS_emailSent`,
ADD COLUMN `RNT_emailSent` DATETIME AFTER `VIS_emailSent`,
ADD COLUMN `NET_emailSent` DATETIME AFTER `RNT_emailSent`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `commodity` VARCHAR(5) ;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `surveyorEvaluation` VARCHAR(3) AFTER `moveType`,
ADD COLUMN `coordinatorEvaluation` VARCHAR(3) AFTER `surveyorEvaluation`;

// As per manish and sunil sir 
select concat("insert into menu_item( parentName, name, description, url, corpID, title, sequenceNum, createdBy, updatedBy, UpdatedOn, createdOn)
values ('Administration ', 'Integration Centre', 'Integration Centre', '/integrationCenterList.html', '",c.corpid,"', 'Integration Centre', 7.00000, '",concat(c.corpid,'_SETUP'),"', '",concat(c.corpid,'_SETUP'),"', now(), now());")
from company c
where c.corpid not in (SELECT corpid FROM menu_item where name='Integration Centre');
//

//As per manish and giten sir
select concat("insert into corp_comp_permission(mask, description, corp_id, createdBy, createdOn, updatedBy, updatedOn, componentId)
values ( 2, 'component.field.forwardingTabAjax', '",c.corpid,"', '",concat(c.corpid,'_SETUP'),"', now(), '",concat(c.corpid,'_SETUP'),"', now(), 'component.field.forwardingTabAjax');")
from company c
where c.corpid not in (Select corp_id from corp_comp_permission where componentid='component.field.forwardingTabAjax');
//


ALTER TABLE `redsky`.`extractqueryfile` MODIFY COLUMN `selectColumnId` VARCHAR(1000);




