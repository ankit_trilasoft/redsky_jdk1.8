// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;


#already ran on prod

ALTER TABLE `redsky`.`emailsetuptemplate` CHANGE COLUMN `emailFrom` `emailFrom` VARCHAR(60) NULL DEFAULT NULL  ,
CHANGE COLUMN `emailTo` `emailTo` VARCHAR(250) NULL DEFAULT NULL  ,
CHANGE COLUMN `emailCc` `emailCc` VARCHAR(250) NULL DEFAULT NULL  ,
CHANGE COLUMN `emailBcc` `emailBcc` VARCHAR(250) NULL DEFAULT NULL  ,
CHANGE COLUMN `emailSubject` `emailSubject` VARCHAR(500) NULL DEFAULT NULL  ;

ALTER TABLE `redsky`.`emailsetuptemplate` CHANGE COLUMN `others` `others` VARCHAR(1000) NULL DEFAULT NULL  ;
//

ALTER TABLE sqlextract ADD COLUMN sqlQueryScheduler  VARCHAR(1) NULL  ,ADD COLUMN perWeekScheduler  VARCHAR(1) NULL ,ADD COLUMN perMonthScheduler VARCHAR(2) NULL 
ALTER TABLE `redsky`.`emailsetup` ADD COLUMN `saveAs` VARCHAR(100) NULL  AFTER `module` ;
ALTER TABLE `redsky`.`billing` ADD COLUMN `paymentReceived` BIT(1) NULL DEFAULT b'0'  AFTER `autoInternalRate` ;
ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `thirdPartyServiceRequired` BIT(1) NULL DEFAULT b'0';
ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `packConfirmCallPriorOneDay` DATETIME NULL  AFTER `destinationLocationType` , ADD COLUMN `packConfirmCallPriorThreeDay` DATETIME NULL  AFTER `packConfirmCallPriorOneDay` ;
ALTER TABLE redsky.partnerprivate add COLUMN contactName varchar(82) NULL,
add COLUMN phone varchar(30) NULL,
add COLUMN email varchar(255) NULL ;