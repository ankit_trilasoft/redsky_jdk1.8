<%@ include file="/common/taglibs.jsp"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_ADMIN")){
		userRole=role.getName();
		break;
	}
	
}
%>
 
 
 
<head>
<title><fmt:message key="sqlExtractList.title" /></title>
<meta name="heading" content="<fmt:message key='sqlExtractList.heading'/>" />
<script language="javascript" type="text/javascript">


function clear_fields1()
{  		    	
				document.forms['sqlExtractList'].elements['fileName'].value = "";
				if('${sessionCorpID}' == 'TSFT')
				{
				    document.forms['sqlExtractList'].elements['corpID'].value ="";
			    }
}
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this SQL Extract?");
	var did = targetElement;
		if (agree){
			location.href="deleteSQLExtract.html?id="+did;
		}else{
			return false;
		}
}

function extractData(targetElement){
	var did = targetElement;
	location.href="extractSQLData.html?id="+did;
}

function extractData1(targetElement){
	alert('Invalid query, Make it correct to extract.');

return false;
}
function sendMail(){
	alert('Invalid query, Make it correct to send mail.');
}
function dataExtractMail(tarVal,tarid){ 
	var tarIdd=tarid;
	var extractVal=tarVal;
	var tar=tarVal;
	openWindow('dataExtractSendMail.html?id='+tarIdd+'&decorator=popup&popup=true',400,200);

	}
</script>

<style>
 form{margin-top:-30px;}
 span.pagelinks {display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:2px;margin-top:-18px;padding:2px 0px;text-align:right;width:100%;}
</style>
</head>
<s:form id="sqlExtractList" name="sqlExtractList" action="extractSearch" method="post">
<div id="Layer1" style="width:100%"> 
<c:if test="${sessionCorpID!='TSFT'}">
<s:hidden name="corpID"/>
</c:if>
<div id="otabs"><ul><li><a class="current"><span>Search</span></a></li></ul></div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
	<table class="table" style="width:99%;"  >
	<thead>
		<tr>
			<th align="center"><fmt:message key="sqlExtract.fileName"/></th>	 		
			<th align="center" colspan="2">
			<c:if test="${sessionCorpID=='TSFT'}">
			<fmt:message key="sqlExtract.corpID"/>
			</c:if>
			</th>			
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td align="left"><s:textfield name="fileName" required="true" cssClass="input-text" size="50"/></td>
	  		<c:if test="${sessionCorpID=='TSFT'}">
			<td width="20%" align="left">
				<s:textfield name="corpID" required="true" cssClass="input-text" size="50"/>
			</td>
			<td width="140px;" align="right">
				<s:submit cssClass="cssbuttonA" cssStyle="width:60px;" key="button.search"/>
		   	 	<input type="button" class="cssbuttonA" value="Clear" style="width:60px;" onclick="clear_fields1();"/>
		   	</td> 
			</c:if>
	  		<c:if test="${sessionCorpID!='TSFT'}">			
			<td width="140px;" align="right" colspan="2">
				<s:submit cssClass="cssbuttonA" cssStyle="width:60px;" key="button.search"/>
		   	 	<input type="button" class="cssbuttonA" value="Clear" style="width:60px;" onclick="clear_fields1();"/>
		   	</td> 
		   	</c:if>

		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${searchresults}" escapeXml="false" />
	
	<div id="otabs">
		<ul>
		    <li><a class="current"><span>SQL Extract List</span></a></li>
		</ul>
	</div>
	<div class="spnblk">&nbsp;</div>
	
	<s:set name="extractList" value="extractList" scope="request" />
	
	<display:table name="extractList" class="table" requestURI="" id="extractListRow"  pagesize="10">
		<display:column property="fileName" sortable="true" titleKey="sqlExtract.fileName"  href="editSQLExtract.html?from=main" paramId="id" paramProperty="id" style="width:180px"/>
		<display:column property="description" sortable="true" titleKey="sqlExtract.description"/>
		<display:column property="fileType" sortable="true" titleKey="sqlExtract.fileType" style="width:80px"/>
		<display:column property="tested" sortable="true" title="Status" style="width:100px"/>
		<display:column property="corpID" sortable="true" title="corpID" titleKey="sqlExtract.corpID" style="width:60px"/>
	
		<display:column title="Extract" style="width:50px">
			<c:if test="${extractListRow.tested == 'Tested'}">
				<c:choose>
					<c:when test="${empty extractListRow.whereClause1 and empty extractListRow.whereClause2 and empty extractListRow.whereClause3 and empty extractListRow.whereClause4 and empty extractListRow.whereClause4 and empty extractListRow.orderBy and empty extractListRow.groupBy}">
						<a><img title="Extract" align="middle"  style="margin: 0px 0px 0px 8px;" onclick="javascript:window.location='sqlExtractInputForm.html?decorator=popup&popup=true&id=${extractListRow.id}&whereClause1=${extractListRow.whereClause1}&whereClause2=${extractListRow.whereClause2}&whereClause3=${extractListRow.whereClause3}&whereClause4=${extractListRow.whereClause4}&whereClause5=${extractListRow.whereClause5}&orderBy=${extractListRow.orderBy}&groupBy=${extractListRow.groupBy}&isBlank=yes';" src="images/extract-para.png"/></a>
					</c:when>
					<c:otherwise>
						<a><img title="Extract" align="middle"  style="margin: 0px 0px 0px 8px;" onclick="window.open('sqlExtractInputForm.html?decorator=popup&popup=true&id=${extractListRow.id}&whereClause1=${extractListRow.whereClause1}&whereClause2=${extractListRow.whereClause2}&whereClause3=${extractListRow.whereClause3}&whereClause4=${extractListRow.whereClause4}&whereClause5=${extractListRow.whereClause5}&orderBy=${extractListRow.orderBy}&groupBy=${extractListRow.groupBy}','sqlExtractInputForm','height=500,width=575,top=0, left=610, scrollbars=yes,resizable=yes').focus();" src="images/extract-para.png"/></a>
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:if test="${extractListRow.tested != 'Tested'}">
				<a><img title="Extract" align="middle" onclick="extractData1();" style="margin: 0px 0px 0px 8px;"  src="images/extract-para.png"/></a>
			</c:if>		
		
		</display:column>
		<display:column title="Send Mail" style="width:50px">
			<c:if test="${extractListRow.tested == 'Tested'}">
				<c:if test="${not empty extractListRow.email}">
					<a onClick="window.open('sqlExtractListSendMail.html?decorator=popup&popup=true&id=${extractListRow.id}&whereClause1=${extractListRow.whereClause1}&whereClause2=${extractListRow.whereClause2}&whereClause3=${extractListRow.whereClause3}&whereClause4=${extractListRow.whereClause4}&whereClause5=${extractListRow.whereClause5}&orderBy=${extractListRow.orderBy}&groupBy=${extractListRow.groupBy}','sqlExtractInputForm','height=500,width=575,top=0, left=610, scrollbars=yes,resizable=yes').focus();">
					<img title="Email" align="middle"  style="margin: 0px 0px 0px 8px;"  src="images/email_go.png"/></a>
				</c:if>
				<c:if test="${empty extractListRow.email}">					
					<img title="Email" align="middle" style="margin: 0px 0px 0px 8px;" src="images/email_disable.png"/>
				</c:if>			
		    </c:if>
			<c:if test="${extractListRow.tested != 'Tested'}">
				<img title="Email" align="middle" onclick="sendMail();" style="margin: 0px 0px 0px 8px;"  src="images/email_go.png"/>
			</c:if>
		</display:column>
			<display:column title="Remove" style="width:50px">
			<a><img title="Remove" align="middle" onclick="confirmSubmit(${extractListRow.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>	
		
	</display:table>
	<c:set var="buttons"/>
	<input type="button" class="cssbuttonA" value="Add"  style="width:65px;" onclick="location.href='<c:url value="/editSQLExtract.html"/>'" /></td>
	
	<c:out value="${buttons}" escapeXml="false" />
</div>
</s:form>
 <script type="text/javascript"> 
try{
		<c:if test="${sessionCorpID!='TSFT'}">
			document.forms['sqlExtractList'].elements['corpID'].value='${sessionCorpID}';
		</c:if>  
}
catch(e){}		
</script>

