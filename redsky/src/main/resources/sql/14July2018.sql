// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;


ALTER TABLE `redsky`.`serviceorder` ADD COLUMN  priceSubmissionToAccDate datetime DEFAULT NULL ,ADD COLUMN  priceSubmissionToTranfDate datetime DEFAULT NULL,ADD COLUMN   priceSubmissionToBookerDate datetime DEFAULT NULL,ADD COLUMN  quoteAcceptenceDate datetime DEFAULT NULL,ADD COLUMN originAgentName varchar(255)  DEFAULT NULL,ADD COLUMN originAgentContact varchar(200)  DEFAULT NULL,ADD COLUMN originAgentEmail varchar(65)  DEFAULT NULL, ADD COLUMN originAgentPhoneNumber varchar(30)  DEFAULT NULL;
ALTER TABLE `redsky`.`emailsetup` ADD COLUMN `lastSentMailBody` TEXT NULL  AFTER `saveAs` ;
ALTER TABLE `redsky`.`refmaster` ADD COLUMN `serviceWarehouse` VARCHAR(200) NULL  AFTER `globallyUniqueIdentifier` ;
