<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="refZipGeoCodeMapList.title"/></title>   
    <meta name="heading" content="<fmt:message key='refZipGeoCodeMapList.heading'/>"/>   
    <script language="javascript" type="text/javascript">
  function clear_fields(){
  
			    
			    document.forms['searchForm'].elements['zipCode'].value = "";
			    document.forms['searchForm'].elements['city'].value = "";
		        document.forms['searchForm'].elements['state'].value = "";
		         document.forms['searchForm'].elements['country'].value = "";
}
</script> 
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:3px;
margin-top:-18px;
!margin-top:-15px;
padding:2px 0px;
text-align:right;
width:99%;
}
form {
margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}
</style> 
</head>
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="location.href='<c:url value="editRefZipGeoCodeMap.html"/>'" value="<fmt:message key="button.add"/>"/> 
</c:set> 
<s:form id="searchForm" action="searchRefZipGeoCodeMap.html" method="post" validate="true" >
<div id="layer1" style="width:100%; ">

<div id="otabs" style="">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		  <div class="spnblk">&nbsp;</div>
		</div>
	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;"  method="searchRefZipGeoCodeMap" key="button.search"/>   
 	<input type="button" class="cssbutton" value="Clear" style="width:57px;" onclick="clear_fields();"/>   
</c:set>
<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:-20px;!margin-top:-2px; "><span></span></div>
    <div class="center-content">
		<table class="table" style="width:100%">
		<thead>
		<tr>
		<th><fmt:message key="refZipGeoCodeMap.zipcode"/></th>
		<th><fmt:message key="refZipGeoCodeMap.city"/></th>
		<th><fmt:message key="refZipGeoCodeMap.state"/></th>
		<th><fmt:message key="refZipGeoCodeMap.country"/></th>
		<th></th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="zipCode" required="true" cssClass="input-text" size="25"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="city" required="true" cssClass="input-text" size="25"/>
					</td>
					
					<td width="" align="left">
					    <s:textfield name="state" required="true" cssClass="input-text" size="25"/>
					</td>
					
					<td width="" align="left">
					    <s:textfield name="country" required="true" cssClass="input-text" size="30"/>
					</td>
					
					<td width="" align="right">
					    <c:out value="${searchbuttons}" escapeXml="false" />
					</tr>
				</tbody>
			</table>
			
				</div>
<div class="bottom-header" style="!margin-top:45px;" ><span></span></div>
</div>
</div> 
<s:set name="refZipGeoCodeMaps" value="refZipGeoCodeMaps" scope="request"/>  
<div id="otabs" style="margin-top: -15px;">
		  <ul>
		    <li><a class="current"><span>Ref Zip Geo Code Map&nbsp;List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<display:table name="refZipGeoCodeMaps" class="table" requestURI="" id="refZipGeoCodeMapList" pagesize="20" style="width:99%; margin-left:5px;margin-top: 2px;!margin-top:-5px;">
     <display:column property="zipcode" sortable="true" titleKey="refZipGeoCodeMap.zipcode"  url="/editRefZipGeoCodeMap.html" paramId="id" paramProperty="id">
    <c:out value="${refZipGeoCodeMapList.zipcode}" />
    </display:column>
    <display:column property="city" sortable="true" titleKey="refZipGeoCodeMap.city"/>
    <display:column property="state" sortable="true" titleKey="refZipGeoCodeMap.state"/>
    <display:column property="country" sortable="true" titleKey="refZipGeoCodeMap.country"/>
    <display:column property="latitude" sortable="true" titleKey="refZipGeoCodeMap.latitude"/>
    <display:column property="longitude" sortable="true" titleKey="refZipGeoCodeMap.longitude"/>
    <display:column property="primaryRecord" sortable="true" title="Primary"/>
</display:table> 
</div>
<c:out value="${buttons}" escapeXml="false" />  
<s:hidden name="id"/>
</s:form>
<script>   
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/refZipGeoCodeMaps.html" ></c:redirect>
		</c:if>
		<c:if test="${detailPage == true}" >
			   <c:redirect url="/editRefZipGeoCodeMap.html?id=${refZipGeoCodeMapList.id}"/>
	   </c:if>
</script>