package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "operationsresourcelimits")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")

public class OperationsResourceLimits extends BaseObject {
	
	private Long id;

	private String hubId;

	private String category;
	
	private String resource;
	
	private BigDecimal resourceLimit;
	
	private String corpId;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("hubId", hubId).append("category", category)
				.append("resource", resource)
				.append("resourceLimit", resourceLimit)
				.append("corpId", corpId).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof OperationsResourceLimits))
			return false;
		OperationsResourceLimits castOther = (OperationsResourceLimits) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(hubId, castOther.hubId)
				.append(category, castOther.category)
				.append(resource, castOther.resource)
				.append(resourceLimit, castOther.resourceLimit)
				.append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(hubId).append(category)
				.append(resource).append(resourceLimit).append(corpId)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).toHashCode();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(length = 15)
	public String getHubId() {
		return hubId;
	}

	public void setHubId(String hubId) {
		this.hubId = hubId;
	}
	
	@Column(length = 55)
	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}
	@Column
	public BigDecimal getResourceLimit() {
		return resourceLimit;
	}

	public void setResourceLimit(BigDecimal resourceLimit) {
		this.resourceLimit = resourceLimit;
	}
	@Column(length = 15)
	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}


}
