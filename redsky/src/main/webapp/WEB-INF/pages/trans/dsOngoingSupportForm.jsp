<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
    <title>Ongoing Support</title>
    <meta name="heading" content="Ongoing Support"/>
    <style><%@ include file="/common/calenderStyle.css"%></style>
   
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	

<script language="javascript" type="text/javascript">
	var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>

<script language="JavaScript" type="text/javascript">
<sec-auth:authComponent componentId="module.script.form.corpAccountScript">

	window.onload = function() { 
		trap();
		var elementsLen=document.forms['dsOngoingSupportForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['dsOngoingSupportForm'].elements[i].type=='text')
					{
						document.forms['dsOngoingSupportForm'].elements[i].readOnly =true;
						document.forms['dsOngoingSupportForm'].elements[i].className = 'input-textUpper'; 
					}
					else
					{
						document.forms['dsOngoingSupportForm'].elements[i].disabled=true;
					} 
			} 
	}
</sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.partnerScript"> 
window.onload = function() { 
		trap();
		var elementsLen=document.forms['dsOngoingSupportForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['dsOngoingSupportForm'].elements[i].type=='text')
					{
						document.forms['dsOngoingSupportForm'].elements[i].readOnly =true;
						document.forms['dsOngoingSupportForm'].elements[i].className = 'input-textUpper'; 
					}
					else
					{ 
						document.forms['dsOngoingSupportForm'].elements[i].disabled=true;
					}
			if(document.forms['dsOngoingSupportForm'].elements[i].type=='radio')
			{
				var n=document.forms['dsOngoingSupportForm'].elements[i].id;
				document.forms['dsOngoingSupportForm'].elements[i].disabled=false;
			} 		
			} 
			if(document.forms['dsOngoingSupportForm'].elements['saveButton'])
			{
				document.forms['dsOngoingSupportForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['dsOngoingSupportForm'].elements['Reset'])
			{ 
				document.forms['dsOngoingSupportForm'].elements['Reset'].disabled=false;
			} 
	}
		
 </sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.vendettiScript">
window.onload = function() {
       trap();
		var elementsLen=document.forms['dsOngoingSupportForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['dsOngoingSupportForm'].elements[i].type=='text')
					{
						document.forms['dsOngoingSupportForm'].elements[i].readOnly =true;
						document.forms['dsOngoingSupportForm'].elements[i].className = 'input-textUpper'; 
					}
					else
					{ 
						document.forms['dsOngoingSupportForm'].elements[i].disabled=true;
					} 
			} 
			if(document.forms['dsOngoingSupportForm'].elements['saveButton'])
			{
				document.forms['dsOngoingSupportForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['dsOngoingSupportForm'].elements['Reset'])
			{ 
				document.forms['dsOngoingSupportForm'].elements['Reset'].disabled=false;
			} 
	}
		
</sec-auth:authComponent>
 
<sec-auth:authComponent componentId="module.script.form.partnerScript">
window.onload = function() {
		trap();
		var elementsLen=document.forms['dsOngoingSupportForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['dsOngoingSupportForm'].elements[i].type=='text')
					{
						document.forms['dsOngoingSupportForm'].elements[i].readOnly =true;
						document.forms['dsOngoingSupportForm'].elements[i].className = 'input-textUpper'; 
					}
					else
					{ 
						document.forms['dsOngoingSupportForm'].elements[i].disabled=true;
					} 
			} 
			if(document.forms['dsOngoingSupportForm'].elements['saveButton'])
			{
				document.forms['dsOngoingSupportForm'].elements['saveButton'].disabled=false;
			}
			if(document.forms['dsOngoingSupportForm'].elements['Reset'])
			{ 
				document.forms['dsOngoingSupportForm'].elements['Reset'].disabled=false;
			} 
	}
</sec-auth:authComponent>
function test()
		{
			return false;
		}

function trap() 
		  {
		  if(document.images)
		    {
		      var totalImages = document.images.length;
		      	for (var i=0;i<totalImages;i++)
					{
						if(document.images[i].src.indexOf('calender.png')>0)
						{
							
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('open-popup.gif')>0)
						{ 
							
								var el = document.getElementById(document.images[i].id);
								el.onclick = test;
							    document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('notes_empty1.jpg')>0)
						{
								var el = document.getElementById(document.images[i].id);
								el.onclick = test;
						        document.images[i].src = 'images/navarrow.gif';
						}
						if(document.images[i].src.indexOf('notes_open1.jpg')>0)
						{
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
						
											
						if(document.images[i].src.indexOf('images/nav')>0)
						{
							var el = document.getElementById(document.images[i].id);
							el.onclick = test;
							document.images[i].src = 'images/navarrow.gif';
						}
					
					}
		    }
		  }

</script>


<script>
function findAgent(position,agentType)
{
	var partnerCode = "";
	var shipNumberA="";
	if(agentType=='OA')
	{
		partnerCode = document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].value; 
		var url="customerAddress.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
	ajax_showTooltip(url,position);	
	}
	
}
function winOpenDest(){ 
	    	openWindow('destinationPartnersRelo.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&jobRelo=${serviceOrder.serviceType}&decorator=popup&popup=true&fld_sixthDescription=dsOngoingSupport.vendorContact&fld_fifthDescription=thirdDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=dsOngoingSupport.vendorEmail&fld_secondDescription=secondDescription&fld_description=dsOngoingSupport.vendorName&fld_code=dsOngoingSupport.vendorCode');
 }

function checkVendorName(){ 
    var vendorId = document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].value;
    	if(vendorId == ''){
		document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorName'].value="";
	}
	if(vendorId != ''){
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    } 
} 

function checkVendorNameRelo(){
  var vendorId=document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].value;
  var job='${serviceOrder.serviceType}';
  
  if(vendorId == '')
  {
   document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorName'].value="";
   document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorContact'].value="";
   document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorEmail'].value="";
   }
  if(vendorId != '')
  {
   var url="vendorNameRelo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId)+"&jobRelo="+ encodeURI(job);
   http3.open("GET",url,true);
   http3.onreadystatechange = handleHttpResponse3;
   http3.send(null);
  }
  }
function handleHttpResponse3(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();                
                var res = results.split("#");                        
		           if(res.size() >= 2){ 
		           		if(res[2] == 'Approved'){
		           		        document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorName'].value = res[1];
		           				document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorContact'].value = res[3];
		           					document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorEmail'].value = res[4];
		           			//document.forms['dsOngoingSupportForm'].elements['trackingUrl'].value = res[5];
		           			document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].select();
		           		}else{
		           		    alert("Vendor Code is not approved" ); 
						    document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorName'].value="";
						    document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].value="";
						    document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorContact'].value ="";
		           			document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorEmail'].value ="";
						    document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].select();
						    
		           		}
                }else{
                     alert("Vendor Code not valid" );
                     document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorName'].value=""; 
					 document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].value="";
					 document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorContact'].value ="";
		           	 document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorEmail'].value ="";
					 document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].select();
			   }
     }
}
function handleHttpResponse2(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();                
                var res = results.split("#");                
		           if(res.size() >= 2){ 
		           		if(res[2] == 'Approved'){
		           			document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorName'].value = res[1];
		           			//document.forms['dsOngoingSupportForm'].elements['trackingUrl'].value = res[5];
		           			document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].select();
		           		}else{
		           			alert("Vendor Code is not approved" ); 
						    document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorName'].value="";
						    document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].value="";
						    document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].select();
		           		}
                }else{
                     alert("Vendor Code not valid" );
                     document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorName'].value=""; 
					 document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].value="";
					 document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.vendorCode'].select();
			   }
     }
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http3 = getHTTPObject();
    
 function IsValidTime(clickType) {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].value;
timeStr=timeStr.trim();
if(timeStr!=''){
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("Flight Arrival Time must between 0 to 23 (Hrs)");
document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("Flight Arrival Time must between 0 to 59 (Min)");
document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("Flight Arrival Time must between 0 to 59 (Sec)");
document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].focus();
return false;
} 
}
}

function completeTimeString() {
	
		var stime1 = document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].value;
		
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.flightArrivalTime'].value = "0" + stime1;
			}
		} 
	} 
	
	 
function autoSaveFunc(clickType)
{
var seqNum=document.forms['dsOngoingSupportForm'].elements['seqNum'].value;
progressBarAutoSave('1');
var id1 =document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.id'].value;
		if ('${autoSavePrompt}'=='No'){
	
			var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
      		var id1 =document.forms['dsOngoingSupportForm'].elements['dsOngoingSupport.id'].value;
	  		      		
	           if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.serviceorder')
		           {
			             noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
		           }
			  if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.billing')
		           {
			             noSaveAction = 'editBilling.html?id='+id1;
		           }
	          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.accounting')
		           {
			              noSaveAction = 'accountLineList.html?sid='+id1;
				   }
	          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.customerfile')
			       {
    	  		     var cidVal='${customerFile.id}';
		             noSaveAction = 'editCustomerFile.html?id='+cidVal;
		           }
	          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.document')
			       {
			             noSaveAction = 'accountFiles.html?sid='+id1+'&seqNum='+seqNum;
		           }
	          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.costing')
		           {
				         noSaveAction = 'costingDetail.html?sid='+id1;
		           }
			          processAutoSave(document.forms['dsOngoingSupportForm'], 'saveDsOngoingSupport!saveOnTabChange.html', noSaveAction);	
		}
		else{
	
			   if(!(clickType=='save'))
				   {
				     var id1 =document.forms['dsOngoingSupportForm'].elements['serviceOrder.id'].value;
					     if (document.forms['dsOngoingSupportForm'].elements['formStatus'].value == '1')
						     {
						       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the ");
						       if(agree)
						        {
						           document.forms['dsOngoingSupportForm'].action ='saveDsOngoingSupport!saveOnTabChange.html';
						           document.forms['dsOngoingSupportForm'].submit();
						        }
						       else
						        {
						         if(id1 != '')
						          {
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.serviceorder')
							           {
							             location.href = 'editServiceOrderUpdate.html?id='+id1;
							           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.billing')
							           {
							             location.href = 'editBilling.html?id='+id1;
							           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.accounting')
							           {
							              location.href = 'accountLineList.html?sid='+id1;
							           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.customerfile')
							           {
						  				     var cidVal='${customerFile.id}';
										     location.href = 'editCustomerFile.html?id='+cidVal;
							           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.document')
							           {
							             location.href = 'accountFiles.html?sid='+id1+'&seqNum='+seqNum;
							           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.costing')
							           {
							             location.href = 'costingDetail.html?sid='+id1;
							           }
          
						          }
						       }
						  }
						   else
							   {
							     if(id1 != '')
							      {
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.serviceorder')
								           {
									             location.href = 'editServiceOrderUpdate.html?id='+id1;
								           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.billing')
								           {
									             location.href = 'editBilling.html?id='+id1;
								           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.accounting')
								           {
									              location.href = 'accountLineList.html?sid='+id1;
								           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.customerfile')
								           {
						  				     var cidVal='${customerFile.id}';
										     location.href = 'editCustomerFile.html?id='+cidVal;
								           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.document')
								           {
									             location.href = 'accountFiles.html?sid='+id1+'&seqNum='+seqNum;
								           }
							          if(document.forms['dsOngoingSupportForm'].elements['gotoPageString'].value =='gototab.costing')
								           {
									             location.href = 'costingDetail.html?sid='+id1;
								           }
            
									}
							   }
				 }
		  }
 }
	 
</script>
<script type="text/javascript">
 function changeStatus(){
    document.forms['dsOngoingSupportForm'].elements['formStatus'].value ='1';
}
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['dsOngoingSupportForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['dsOngoingSupportForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURIComponent(soIdNum)+"&seqNm="+encodeURIComponent(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['dsOngoingSupportForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['dsOngoingSupportForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURIComponent(soIdNum)+"&seqNm="+encodeURIComponent(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }

  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
          		 var results = http5.responseText
                 results = results.trim();
				 var id1=results;	 
	             findOtherServiceType(id1);
             }
             
       }     
	function findOtherServiceType(id1)      
	{
		  var soIdNum=id1;
		  var url="findOtherServiceType.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum);
		  http10.open("GET", url, true); 
          http10.onreadystatechange =function(){ handleHttpResponseOtherShipType(id1);}; 
          http10.send(null); 
	}
  function handleHttpResponseOtherShipType(id1){
             if (http10.readyState == 4)
             {
             			  var results = http10.responseText
			               results = results.trim();
						if(results=="")	{
						location.href = 'editTrackingStatus.html?id='+id1;
						}
						else{
						location.href = results+id1;
						} 
             } 
       }	
    var http10 = getHTTPObject();         
     var http5 = getHTTPObject();  
  function goToUrl(id)
	{
	findOtherServiceType(id);
	}
function findCustomerOtherSO(position) {
 var sid=document.forms['dsOngoingSupportForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['dsOngoingSupportForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURIComponent(sid)+"&soIdNum="+encodeURIComponent(soIdNum);
  ajax_showTooltip(url,position);	
  } 

</script>    
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<c:set var="fileID" value="%{serviceOrder.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="dsOngoingSupportForm" action="saveDsOngoingSupport.html" method="post" validate="true">
<s:hidden name="serviceOrder.sequenceNumber" value="%{serviceOrder.sequenceNumber}"/> 
<s:hidden name="minShip" />
<s:hidden name="shipSize" />
<s:hidden name="countShip" />
<s:hidden name="customerFile.id" value="%{customerFile.id}" />

<c:if test="${validateFormNav == 'OK'}">
<c:choose>
	<c:when test="${gotoPageString == 'gototab.serviceorder' }">
		<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.billing' }">
		<c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.accounting' }">
		<c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.customerfile' }">
				     <c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.document' }">
		<c:redirect url="/accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.costing' }">
		<c:redirect url="/costingDetail.html?sid=${serviceOrder.id}"/>
	</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<c:set var="field" value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>


<s:hidden name="seqNum" id ="seqNum" value="%{serviceOrder.sequenceNumber}" />
<div id="Layer1"  style="width:100%;">
		<div id="newmnav" style="float: left;"> 
		    <ul>
		    <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
		      <li><a onClick="setReturnString('gototab.serviceorder');return autoSaveFunc('none');"><span>S/O Details</span></a></li>
		    </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
			  <li><a onClick="setReturnString('gototab.billing');return autoSaveFunc('none');"/><span>Billing</span></a></li>
			  </sec-auth:authComponent>
            </sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">  
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			   </c:when> --%>
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			   <c:otherwise> 
		         <li><a onclick="setReturnString('gototab.accounting');return autoSaveFunc('none');"><span>Accounting</span></a></li>
		      </c:otherwise>
		     </c:choose> 
		    </sec-auth:authComponent>
			  <li  id="newmnav1" style="background:#FFF"><a class="current"><span>Status</span></a></li>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">  
			  <li><a onclick="setReturnString('gototab.customerfile');return autoSaveFunc('none');"><span>Customer File</span></a></li>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.trackingStatus.reportTab">  
			  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Billing&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.trackingStatus.auditTab">
			  <li><a onclick="window.open('auditList.html?id=${billing.id}&tableName=dspreviewtrip&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">			 
           	 	<li><a onclick="setReturnString('gototab.costing');return autoSaveFunc('none');"><span>Costing</span></a></li>
           	  </sec-auth:authComponent>
           	  <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	  
           	 	<li><a onclick="setReturnString('gototab.document');return autoSaveFunc('none');"><span>Document</span></a></li>
           	  </sec-auth:authComponent>
			 </ul> 
		</div>
		
       	<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;height:22px;float: none; "><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}"  >
  		<a><img align="middle" id="navigation1" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" id="navigation2" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" id="navigation3" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" id="navigation4" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" id="navigation5" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" id="navigation6" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if>
		</tr>
		</table>
		
		
 <div class="spn">&nbsp;</div>
     <div style="!margin-top:8px; ">
      <%@ include file="/WEB-INF/pages/trans/serviceOrderJobHeader.jsp"%>
     </div>
      </div>
      
      
      
      
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
	<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" /> 
	<s:hidden name="firstDescription" />
<s:hidden name="dsOngoingSupport.id" />
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="validateFormNav" />
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}" />
<s:hidden name="id" value="${dsOngoingSupport.id}"/>
<s:hidden name="dsOngoingSupport.serviceOrderId" /> 
<s:hidden id="countDSOngoingSupportNotes" name="countDSOngoingSupportNotes" value="<%=request.getParameter("countDSOngoingSupportNotes") %>"/> 

<c:set var="countDSOngoingSupportNotes" value="<%=request.getParameter("countDSOngoingSupportNotes") %>" />
<div id="layer1" style="width:750px; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ongoing Support<img id="imgId3"  src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" >
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dsOngoingSupport.vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorNameRelo(),changeStatus();"  /></td>
	<td align="left"width="10"><img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest(),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dsOngoingSupport.vendorName" readonly="true" size="35" maxlength="200" onchange="changeStatus();" />

	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dsOngoingSupport.serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dsOngoingSupport.serviceStartDate"/></s:text>
			 <td><s:textfield id="serviceStartDate" cssClass="input-text" name="dsOngoingSupport.serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsOngoingSupportForm'].serviceStartDate,'calender',document.forms['dsOngoingSupportForm'].dateFormat.value); return false;"/></td>
		</c:if>
	    <c:if test="${empty dsOngoingSupport.serviceStartDate}">
		<td><s:textfield id="serviceStartDate" cssClass="input-text" name="dsOngoingSupport.serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="calender" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsOngoingSupportForm'].serviceStartDate,'calender',document.forms['dsOngoingSupportForm'].dateFormat.value); return false;"/></td>
		</c:if>



<c:if test="${empty dsOngoingSupport.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dsOngoingSupport.id}">
<c:choose>
<c:when test="${countDSOngoingSupportNotes == '0' || countDSOngoingSupportNotes == '' || countDSOngoingSupportNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSOngoingSupportNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',755,500);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSOngoingSupportNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',755,500);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>

</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dsOngoingSupport.vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dsOngoingSupport.serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dsOngoingSupport.serviceEndDate"/></s:text>
			 <td><s:textfield id="serviceEndDate" cssClass="input-text" name="dsOngoingSupport.serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender1" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsOngoingSupportForm'].serviceEndDate,'calender1',document.forms['dsOngoingSupportForm'].dateFormat.value); return false;"/></td>
		</c:if>
	    <c:if test="${empty dsOngoingSupport.serviceEndDate}">
		<td><s:textfield id="serviceEndDate" cssClass="input-text" name="dsOngoingSupport.serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="calender1" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['dsOngoingSupportForm'].serviceEndDate,'calender1',document.forms['dsOngoingSupportForm'].dateFormat.value); return false;"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dsOngoingSupport.vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>

</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table width="100%">
<tr><td>



</table>

</div></div><div class="bottom-header"><span></span></div></div>
<table width="700px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${dsOngoingSupport.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="dsOngoingSupport.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${dsOngoingSupport.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty dsOngoingSupport.id}">
								<s:hidden name="dsOngoingSupport.createdBy"/>
								<td ><s:label name="createdBy" value="%{dsOngoingSupport.createdBy}"/></td>
							</c:if>
							<c:if test="${empty dsOngoingSupport.id}">
								<s:hidden name="dsOngoingSupport.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${dsOngoingSupport.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="dsOngoingSupport.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${dsOngoingSupport.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty dsOngoingSupport.id}">
							<s:hidden name="dsOngoingSupport.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{dsOngoingSupport.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty dsOngoingSupport.id}">
							<s:hidden name="dsOngoingSupport.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
			</div>
<table><tr><td>   
<sec-auth:authComponent componentId="module.button.trackingStatus.saveButton">
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick="return IsValidTime('save');"/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
        </sec-auth:authComponent>
	</td></tr></table>
</s:form>

<script language="JavaScript" type="text/javascript">
	var fieldName = document.forms['dsOngoingSupportForm'].elements['field'].value;
	var fieldName1 = document.forms['dsOngoingSupportForm'].elements['field1'].value;
	if(fieldName!=''){
	document.forms['dsOngoingSupportForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
	document.forms['dsOngoingSupportForm'].elements[fieldName1].className = 'rules-textUpper';
	}
 </script>