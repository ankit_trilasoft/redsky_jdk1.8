package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.CheckListResult;

public interface CheckListResultDao extends GenericDao<CheckListResult, Long>{
	public List getById();
	
}
