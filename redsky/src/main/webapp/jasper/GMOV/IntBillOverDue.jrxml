<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="IntBillOverDue"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="849"
		 pageHeight="842"
		 columnWidth="789"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.zoom" value="1.0" />
	<property name="ireport.x" value="0" />
	<property name="ireport.y" value="0" />
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[select
	"IntBillOverDue-R-GMOV.jrxml",
	serviceorder.shipnumber AS serviceorder_shipnumber,
	serviceorder.companyDivision AS serviceorder_companyDivision,
	if((serviceorder.firstName is null || trim(serviceorder.firstName)=''),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))) AS 'Shipper',
	billing.billtocode AS billing_billtocode,
	serviceorder.status AS serviceorder_status,
	serviceorder.routing AS serviceorder_routing,
	date_format(trackingstatus.loadA,'%d-%b-%y') AS trackingstatus_loadA,
	date_format(trackingstatus.deliveryA,'%d-%b-%y')AS trackingstatus_deliveryA,
	lower(billing.personbilling) AS billing_personbilling,
	ifnull(notes.subject,'') As notes_subject,
	date_format(now(),'%d-%b-%y') AS date
FROM serviceorder serviceorder
	INNER JOIN trackingstatus trackingstatus on serviceorder.id = trackingstatus.id	AND trackingstatus.corpid = $P{Corporate ID}
	INNER JOIN billing billing on serviceorder.id = billing.id AND billing.corpid = $P{Corporate ID}
	LEFT OUTER JOIN notes notes ON serviceorder.id = notes.noteskeyId AND notes.noteSubType ='Accounting Issue' AND notes.notestatus <> 'CMP' and notetype = 'Service Order' AND notes.corpid = $P{Corporate ID}
WHERE  serviceorder.job = 'INT'
	AND billing.billComplete is null
	AND serviceorder.status not in ('CNCL','CLSD','DWNLD','OSIT')
	AND serviceorder.corpid = $P{Corporate ID}
	AND trackingstatus.loadA is not null
order by serviceorder.companyDivision,billing.billtocode, trackingstatus.loadA, trackingstatus.deliveryA]]></queryString>

	<field name="IntBillOverDue-R-GMOV.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipnumber" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="billing_billtocode" class="java.lang.String"/>
	<field name="serviceorder_status" class="java.lang.String"/>
	<field name="serviceorder_routing" class="java.lang.String"/>
	<field name="trackingstatus_loadA" class="java.lang.String"/>
	<field name="trackingstatus_deliveryA" class="java.lang.String"/>
	<field name="billing_personbilling" class="java.lang.String"/>
	<field name="notes_subject" class="java.lang.String"/>
	<field name="date" class="java.lang.String"/>


		<group  name="CompDiv" isStartNewPage="true" >
			<groupExpression><![CDATA[$F{serviceorder_companyDivision}]]></groupExpression>
			<groupHeader>
			<band height="20"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="102"
						y="0"
						width="100"
						height="18"
						key="textField"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="102"
						height="18"
						key="staticText-12"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="31"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Transparent"
						x="0"
						y="0"
						width="789"
						height="30"
						key="staticText-1"/>
					<box topPadding="5"></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="14" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[International Billing Overdue Report]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="30"
						width="789"
						height="1"
						forecolor="#999999"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="19"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="0"
						width="95"
						height="18"
						backcolor="#D2CCCC"
						key="staticText-2"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[S/O #]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="95"
						y="0"
						width="124"
						height="18"
						backcolor="#D2CCCC"
						key="staticText-3"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Shipper]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="219"
						y="0"
						width="81"
						height="18"
						backcolor="#D2CCCC"
						key="staticText-4"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Bill Code]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="300"
						y="0"
						width="50"
						height="18"
						backcolor="#D2CCCC"
						key="staticText-5"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Status]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="350"
						y="0"
						width="58"
						height="18"
						backcolor="#D2CCCC"
						key="staticText-6"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Routing]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="408"
						y="0"
						width="65"
						height="18"
						backcolor="#D2CCCC"
						key="staticText-7"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Loading]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="473"
						y="0"
						width="65"
						height="18"
						backcolor="#D2CCCC"
						key="staticText-8"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Delivery]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="538"
						y="0"
						width="85"
						height="18"
						backcolor="#D2CCCC"
						key="staticText-9"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Billing Person]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="623"
						y="0"
						width="166"
						height="18"
						backcolor="#D2CCCC"
						key="staticText-10"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Note]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="18"
						width="789"
						height="1"
						forecolor="#999999"
						backcolor="#999999"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="0"
						width="95"
						height="18"
						backcolor="#E2E1E1"
						key="textField"/>
					<box leftPadding="3" topPadding="5"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipnumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="95"
						y="0"
						width="124"
						height="18"
						key="textField"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Shipper}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="219"
						y="0"
						width="81"
						height="18"
						backcolor="#E2E1E1"
						key="textField"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_billtocode}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="300"
						y="0"
						width="50"
						height="18"
						key="textField"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_status}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="350"
						y="0"
						width="58"
						height="18"
						backcolor="#E2E1E1"
						key="textField"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_routing}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="408"
						y="0"
						width="65"
						height="18"
						key="textField"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{trackingstatus_loadA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="473"
						y="0"
						width="65"
						height="18"
						backcolor="#E2E1E1"
						key="textField"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{trackingstatus_deliveryA}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="538"
						y="0"
						width="85"
						height="18"
						key="textField"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{billing_personbilling}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="623"
						y="0"
						width="166"
						height="18"
						backcolor="#E2E1E1"
						key="textField"/>
					<box leftPadding="3" topPadding="2"></box>
					<textElement>
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{notes_subject}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="16"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="647"
						y="2"
						width="85"
						height="14"
						forecolor="#666666"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="730"
						y="2"
						width="59"
						height="14"
						forecolor="#666666"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
						<font fontName="Arial" pdfFontName="Helvetica" size="9" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="236"
						y="2"
						width="317"
						height="14"
						forecolor="#666666"
						key="staticText-11"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="9"/>
					</textElement>
				<text><![CDATA[IntBillOverDue.jrxml]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="2"
						width="150"
						height="14"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{date}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="1"
						width="788"
						height="1"
						key="line-5"/>
					<graphicElement stretchType="NoStretch" fill="Solid" />
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
