package com.trilasoft.app.webapp.action;

import org.apache.poi.hssf.util.HSSFColor;

public class ExcelCreatorColor {
	 public static short DEFAULT   =   (short)1;
	 public static short AQUA	=	HSSFColor.AQUA.index;
	 public static short BLACK	=	 HSSFColor.BLACK.index;
	 public static short BLUE	=	 HSSFColor.BLUE.index;
	 public static short BLUE_GREY	=	 HSSFColor.BLUE_GREY.index;
	 public static short BRIGHT_GREEN	=	 HSSFColor.BRIGHT_GREEN.index;
	 public static short BROWN	=	 HSSFColor.BROWN.index;
	 public static short CORAL	=	 HSSFColor.CORAL.index;
	 public static short CORNFLOWER_BLUE	=	 HSSFColor.CORNFLOWER_BLUE.index;
	 public static short DARK_BLUE	=	 HSSFColor.DARK_BLUE.index;
	 public static short DARK_GREEN	=	 HSSFColor.DARK_GREEN.index;
	 public static short DARK_RED	=	 HSSFColor.DARK_RED.index;
	 public static short DARK_TEAL	=	 HSSFColor.DARK_TEAL.index;
	 public static short DARK_YELLOW	=	 HSSFColor.DARK_YELLOW.index;
	 public static short GOLD	=	 HSSFColor.GOLD.index;
	 public static short GREEN	=	 HSSFColor.GREEN.index;
	 public static short GREY_25_PERCENT	=	 HSSFColor.GREY_25_PERCENT.index;
	 public static short GREY_40_PERCENT	=	 HSSFColor.GREY_40_PERCENT.index;
	 public static short GREY_50_PERCENT	=	 HSSFColor.GREY_50_PERCENT.index;
	 public static short GREY_80_PERCENT	=	 HSSFColor.GREY_80_PERCENT.index;
	 public static short INDIGO	=	 HSSFColor.INDIGO.index;
	 public static short LAVENDER	=	 HSSFColor.LAVENDER.index;
	 public static short LEMON_CHIFFON	=	 HSSFColor.LEMON_CHIFFON.index;
	 public static short LIGHT_BLUE	=	 HSSFColor.LIGHT_BLUE.index;
	 public static short LIGHT_CORNFLOWER_BLUE	=	 HSSFColor.LIGHT_CORNFLOWER_BLUE.index;
	 public static short LIGHT_GREEN	=	 HSSFColor.LIGHT_GREEN.index;
	 public static short LIGHT_ORANGE	=	 HSSFColor.LIGHT_ORANGE.index;
	 public static short LIGHT_TURQUOISE	=	 HSSFColor.LIGHT_TURQUOISE.index;
	 public static short LIGHT_YELLOW	=	 HSSFColor.LIGHT_YELLOW.index;
	 public static short LIME	=	 HSSFColor.LIME.index;
	 public static short MAROON	=	 HSSFColor.MAROON.index;
	 public static short OLIVE_GREEN	=	 HSSFColor.OLIVE_GREEN.index;
	 public static short ORANGE	=	 HSSFColor.ORANGE.index;
	 public static short ORCHID	=	 HSSFColor.ORCHID.index;
	 public static short PALE_BLUE	=	 HSSFColor.PALE_BLUE.index;
	 public static short PINK	=	 HSSFColor.PINK.index;
	 public static short PLUM	=	 HSSFColor.PLUM.index;
	 public static short RED	=	 HSSFColor.RED.index;
	 public static short ROSE	=	 HSSFColor.ROSE.index;
	 public static short ROYAL_BLUE	=	 HSSFColor.ROYAL_BLUE.index;
	 public static short SEA_GREEN	=	 HSSFColor.SEA_GREEN.index;
	 public static short SKY_BLUE	=	 HSSFColor.SKY_BLUE.index;
	 public static short TAN	=	 HSSFColor.TAN.index;
	 public static short TEAL	=	 HSSFColor.TEAL.index;
	 public static short TURQUOISE	=	 HSSFColor.TURQUOISE.index;
	 public static short VIOLET	=	 HSSFColor.VIOLET.index;
	 public static short WHITE	=	 HSSFColor.WHITE.index;
	 public static short YELLOW	=	 HSSFColor.YELLOW.index;

}
