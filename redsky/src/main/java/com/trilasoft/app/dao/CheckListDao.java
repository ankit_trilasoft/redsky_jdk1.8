package com.trilasoft.app.dao;

import java.util.HashMap;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CheckList;
import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.model.CheckListResultDTO;
import com.trilasoft.app.model.ToDoRule;

public interface CheckListDao extends GenericDao<CheckList, Long>{
	public List getById();
	public List getControlDateList(String sessionCorpID);
	public List execute(String sessionCorpID);
	public List<CheckList> getAllCheckListRules(String sessionCorpID);
	public List executeExpression(String ruleExpression);
	public void deleteThisResult(Long checkListId, String corpID); 
	public List <CheckListResult> getResultList(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed);
	public List findNumberOfResultEntered(String sessionCorpID, Long id);
	public Integer findNumberOfResult(String sessionCorpID); 
	public List executeTdrCheckList(String sessionCorpID);
	public HashMap<String, CheckListResultDTO> getAllToDoResultListForCheckList(String sessionCorpID,Long currentRuleNumber);
	public void updateAndDel(String sessionCorpID,Long currentRuleNumber,Long ruleId);
	public List <ToDoRule> getAllRules(String sessionCorpID);
	public List <CheckListResult> getResultListExtCordinator(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed,String bookingAgentPopup,String billToCodePopup,String userType);
	public List <CheckListResult> getResultListExtCordinatorForTeam(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed,String bookingAgentPopup,String billToCodePopup,String userName,String allradio,String userType);
}
