package org.apache.jsp.common;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.appfuse.model.User;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.service.CompanyManager;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public final class header_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.Vector _jspx_dependants;

  static {
    _jspx_dependants = new java.util.Vector(1);
    _jspx_dependants.add("/common/taglibs.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_set_var_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_set_var;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_fmt_message_key_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_if_test;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_url_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_fmt_setLocale_value_scope_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_sec$1auth_authComponent_componentId;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_textfield_value_name_id_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_configByCorp_fieldVisibility_componentId;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_configByCorp_userGuideVisibility_userType_corpId;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_appfuse_constants_scope_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_hidden_value_name_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_remove_var_scope_nobody;

  private org.apache.jasper.runtime.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_set_var_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_set_var = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_fmt_message_key_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_url_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_fmt_setLocale_value_scope_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_sec$1auth_authComponent_componentId = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_textfield_value_name_id_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_configByCorp_fieldVisibility_componentId = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_configByCorp_userGuideVisibility_userType_corpId = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_appfuse_constants_scope_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_hidden_value_name_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_remove_var_scope_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_set_var_value_nobody.release();
    _jspx_tagPool_c_set_var.release();
    _jspx_tagPool_fmt_message_key_nobody.release();
    _jspx_tagPool_c_if_test.release();
    _jspx_tagPool_c_url_value_nobody.release();
    _jspx_tagPool_fmt_setLocale_value_scope_nobody.release();
    _jspx_tagPool_sec$1auth_authComponent_componentId.release();
    _jspx_tagPool_s_textfield_value_name_id_nobody.release();
    _jspx_tagPool_configByCorp_fieldVisibility_componentId.release();
    _jspx_tagPool_configByCorp_userGuideVisibility_userType_corpId.release();
    _jspx_tagPool_appfuse_constants_scope_nobody.release();
    _jspx_tagPool_s_hidden_value_name_nobody.release();
    _jspx_tagPool_c_remove_var_scope_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=utf-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"/error.htm", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      String resourceInjectorClassName = config.getInitParameter("com.sun.appserv.jsp.resource.injector");
      if (resourceInjectorClassName != null) {
        _jspx_resourceInjector = (org.apache.jasper.runtime.ResourceInjector) Class.forName(resourceInjectorClassName).newInstance();
        _jspx_resourceInjector.setContext(application);
      }

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write(" \r\n");
      out.write(" \r\n");
      out.write(" \n");
      if (_jspx_meth_c_set_0(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_c_set_1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("  \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User quickUser = (User) auth.getPrincipal();
String quickCorpId = quickUser.getCorpID();
String quickUserType = quickUser.getUserType();
String defaultOperationCalendarVal = quickUser.getDefaultOperationCalendar();
if(defaultOperationCalendarVal==null || defaultOperationCalendarVal.equalsIgnoreCase("")){
	defaultOperationCalendarVal="Ops Calendar";
}
Boolean newsUpdateFlag=quickUser.isNewsUpdateFlag();
String quickFileCabinetView = quickUser.getFileCabinetView();

      out.write('\r');
      out.write('\n');

WebApplicationContext  context=WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
CompanyManager a = (CompanyManager) context.getBean("companyManager");
Company c=(Company)a.findByCorpID(quickCorpId).get(0);
Boolean accessQuotationFrom = c.getAccessQuotationFromCustomerFile(); 
Boolean forceDocCenter=c.getForceDocCenter();
Boolean activityMgmtVersion2 = c.getActivityMgmtVersion2();

      out.write(" \r\n");
      out.write("\r\n");
      out.write("<style type=\"text/css\">\r\n");
      out.write("body {\r\n");
      out.write("margin-top: 0px;\r\n");
      out.write("margin-bottom: 0px;\r\n");
      out.write("}\r\n");
      out.write("#overlayMoodal {\r\n");
      out.write("display:none;\r\n");
      out.write("filter:alpha(opacity=60);\r\n");
      out.write("opacity:0.6;\r\n");
      out.write("position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:991; margin:0px auto -1px auto;\r\n");
      out.write("background:url(images/over-load.png);\r\n");
      out.write("}\r\n");
      out.write("#moodal {\r\n");
      out.write("display:none;\r\n");
      out.write("opacity: 0;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("#TopSearchBox {position:absolute;top:54px;float:right;right:4%;z-index:1;}\r\n");
      out.write("\r\n");
      out.write("#searchBox {\r\n");
      out.write("    background-image: url(\"images/searchblue-left.png\");\r\n");
      out.write("    background-position: right top;\r\n");
      out.write("    background-repeat: no-repeat;\r\n");
      out.write("    float: left;\r\n");
      out.write("    height: 23px;    \r\n");
      out.write("    padding: 0;\r\n");
      out.write("    width: 158px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".ajaxSearch_input {\r\n");
      out.write("    background-color: transparent;\r\n");
      out.write("    border: medium none;\r\n");
      out.write("    color: #444444;\r\n");
      out.write("    float: left;\r\n");
      out.write("    font-size: 11px;\r\n");
      out.write("    height: 17px;\r\n");
      out.write("    margin: 2px 2px 0 14px;\r\n");
      out.write("    width: 132px;\r\n");
      out.write("}\r\n");
      out.write("#searchBox .ajaxSearch_input:hover{\r\n");
      out.write("    background-color: transparent;       \r\n");
      out.write("}\r\n");
      out.write("input[type=text]:hover\r\n");
      out.write("{\r\n");
      out.write("  background: none;\r\n");
      out.write("}\r\n");
      out.write("input::-webkit-input-placeholder {\r\n");
      out.write("    color:#999;\r\n");
      out.write("}\r\n");
      out.write("input:-moz-placeholder {\r\n");
      out.write("    color:#999;\r\n");
      out.write("}\r\n");
      out.write("input:-ms-input-placeholder {\r\n");
      out.write("    color:#999;\r\n");
      out.write("}\r\n");
      out.write(".ajaxSearch_submit {\r\n");
      out.write("    margin: 5px 0 0;\r\n");
      out.write("    width: 25px;\r\n");
      out.write("    border:none;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("/* The hint to Hide and Show */\r\n");
      out.write("#hint {\r\n");
      out.write("   \tdisplay: none;\r\n");
      out.write("    position: absolute;\r\n");
      out.write("\tcolor:#999;\r\n");
      out.write("    top:36px;\r\n");
      out.write("\tleft:5px;\r\n");
      out.write("    width: 215px;\r\n");
      out.write("    margin-top: -4px;\r\n");
      out.write("    border: 1px solid #d7d7d7;\r\n");
      out.write("\t-moz-border-radius: 3px;\r\n");
      out.write("  \tborder-radius: 3px;\r\n");
      out.write("    padding: 5px 5px;\r\n");
      out.write("\tfont:normal 11px/16px Arial;\r\n");
      out.write("    /* to fix IE6, I can't just declare a background-color,\r\n");
      out.write("    I must do a bg image, too!  So I'm duplicating the pointer.gif\r\n");
      out.write("    image, and positioning it so that it doesn't show up\r\n");
      out.write("    within the box */\r\n");
      out.write("    background: #ffffff ;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("/* The pointer image is hadded by using another span */\r\n");
      out.write("#hint .hint-pointer {\r\n");
      out.write("    position: absolute;\r\n");
      out.write("    top: -10px;\r\n");
      out.write("    width: 19px;\r\n");
      out.write("    height: 10px;\r\n");
      out.write("    background: url(images/pointer2.gif) left top no-repeat;\r\n");
      out.write("}\r\n");
      out.write("#qsearch ul\r\n");
      out.write("{\r\n");
      out.write("list-style-type:none;\r\n");
      out.write("padding:0px;\r\n");
      out.write("margin:0px;\r\n");
      out.write("}\r\n");
      out.write("#qsearch ul li\r\n");
      out.write("{\r\n");
      out.write("background-image:url(images/hint-arrow.gif);\r\n");
      out.write("background-repeat:no-repeat;\r\n");
      out.write("background-position:0px 5px; \r\n");
      out.write("padding-left:10px;\r\n");
      out.write("padding-bottom:4px;\r\n");
      out.write("}\r\n");
      out.write("#qsearch ul li b{color:#7c7c7c;}\r\n");
      out.write("</style>  \r\n");
      if (_jspx_meth_c_if_0(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_c_set_2(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_sec$1auth_authComponent_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("<div id=\"branding\">\r\n");
      out.write("<!--");
      //  s:textfield
      org.apache.struts2.views.jsp.ui.TextFieldTag _jspx_th_s_textfield_0 = (org.apache.struts2.views.jsp.ui.TextFieldTag) _jspx_tagPool_s_textfield_value_name_id_nobody.get(org.apache.struts2.views.jsp.ui.TextFieldTag.class);
      _jspx_th_s_textfield_0.setPageContext(_jspx_page_context);
      _jspx_th_s_textfield_0.setParent(null);
      _jspx_th_s_textfield_0.setName("fileID");
      _jspx_th_s_textfield_0.setId("fileID");
      _jspx_th_s_textfield_0.setValue(request.getParameter("id") );
      int _jspx_eval_s_textfield_0 = _jspx_th_s_textfield_0.doStartTag();
      if (_jspx_th_s_textfield_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_s_textfield_value_name_id_nobody.reuse(_jspx_th_s_textfield_0);
        return;
      }
      _jspx_tagPool_s_textfield_value_name_id_nobody.reuse(_jspx_th_s_textfield_0);
      out.write('\r');
      out.write('\n');
      //  c:set
      org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_4 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
      _jspx_th_c_set_4.setPageContext(_jspx_page_context);
      _jspx_th_c_set_4.setParent(null);
      _jspx_th_c_set_4.setVar("fileID");
      _jspx_th_c_set_4.setValue(request.getParameter("id") );
      int _jspx_eval_c_set_4 = _jspx_th_c_set_4.doStartTag();
      if (_jspx_th_c_set_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_4);
        return;
      }
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_4);
      out.write(" -->\r\n");
      out.write("\r\n");
      out.write("<div id=\"main-top\">    \r\n");
      out.write("          <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\">\r\n");
      out.write("          <tr>\r\n");
      out.write("          <td width=\"17\">&nbsp;</td>\r\n");
      out.write("          <td width=\"11%\"><img src=\"");
      if (_jspx_meth_c_url_1(_jspx_page_context))
        return;
      out.write("\" width=\"112\" height=\"63\" /></td>\t\t  \r\n");
      out.write("            ");
      //  c:if
      org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
      _jspx_th_c_if_1.setPageContext(_jspx_page_context);
      _jspx_th_c_if_1.setParent(null);
      _jspx_th_c_if_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${!pwdreset}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
      int _jspx_eval_c_if_1 = _jspx_th_c_if_1.doStartTag();
      if (_jspx_eval_c_if_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("    \t\t");
          //  c:if
          org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_2 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
          _jspx_th_c_if_2.setPageContext(_jspx_page_context);
          _jspx_th_c_if_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_1);
          _jspx_th_c_if_2.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${!isTrue}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
          int _jspx_eval_c_if_2 = _jspx_th_c_if_2.doStartTag();
          if (_jspx_eval_c_if_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
            do {
              out.write("\r\n");
              out.write("    \t\t <td width=\"9%\">\r\n");
              out.write("   \t \t  ");
              if (_jspx_meth_sec$1auth_authComponent_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td>\r\n");
              out.write("          <td width=\"8%\">\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td> \r\n");
              out.write("          <td width=\"6%\">\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_5((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("           ");
              if (_jspx_meth_sec$1auth_authComponent_6((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td>\r\n");
              out.write("          \r\n");
              out.write("         \r\n");
              out.write("          <td width=\"6%\">\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_7((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write(" \r\n");
              out.write("          </td>\r\n");
              out.write("       \r\n");
              out.write("          <td width=\"8%\">\r\n");
              out.write("          \t");
              if (_jspx_meth_sec$1auth_authComponent_8((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("         </td>\r\n");
              out.write("         \r\n");
              out.write("         ");
              if (_jspx_meth_configByCorp_fieldVisibility_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("                 \r\n");
              out.write("          \r\n");
              out.write("          <td width=\"9%\">\r\n");
              out.write("           <a href=\"");
              out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
              out.write("/editProfile.html\"><img src=\"");
              if (_jspx_meth_c_url_14((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\" width=\"83\" height=\"63\" border=\"0\" /></a>\r\n");
              out.write("           </td>\r\n");
              out.write("           ");
              //  c:if
              org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_9 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
              _jspx_th_c_if_9.setPageContext(_jspx_page_context);
              _jspx_th_c_if_9.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
              _jspx_th_c_if_9.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType!='DRIVER'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
              int _jspx_eval_c_if_9 = _jspx_th_c_if_9.doStartTag();
              if (_jspx_eval_c_if_9 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
                do {
                  out.write("\r\n");
                  out.write("          ");
                  //  configByCorp:userGuideVisibility
                  com.trilasoft.app.webapp.tags.UserGuideVisibility _jspx_th_configByCorp_userGuideVisibility_0 = (com.trilasoft.app.webapp.tags.UserGuideVisibility) _jspx_tagPool_configByCorp_userGuideVisibility_userType_corpId.get(com.trilasoft.app.webapp.tags.UserGuideVisibility.class);
                  _jspx_th_configByCorp_userGuideVisibility_0.setPageContext(_jspx_page_context);
                  _jspx_th_configByCorp_userGuideVisibility_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_9);
                  _jspx_th_configByCorp_userGuideVisibility_0.setUserType(quickUserType );
                  _jspx_th_configByCorp_userGuideVisibility_0.setCorpId(quickCorpId );
                  int _jspx_eval_configByCorp_userGuideVisibility_0 = _jspx_th_configByCorp_userGuideVisibility_0.doStartTag();
                  if (_jspx_eval_configByCorp_userGuideVisibility_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
                    do {
                      out.write("  \r\n");
                      out.write("          \r\n");
                      out.write("          <td width=\"6%\">\r\n");
                      out.write("          \r\n");
                      out.write("          ");
                      if (_jspx_meth_c_if_10((javax.servlet.jsp.tagext.JspTag) _jspx_th_configByCorp_userGuideVisibility_0, _jspx_page_context))
                        return;
                      out.write("\r\n");
                      out.write("         ");
                      if (_jspx_meth_c_if_11((javax.servlet.jsp.tagext.JspTag) _jspx_th_configByCorp_userGuideVisibility_0, _jspx_page_context))
                        return;
                      out.write("\r\n");
                      out.write("           <img src=\"");
                      if (_jspx_meth_c_url_15((javax.servlet.jsp.tagext.JspTag) _jspx_th_configByCorp_userGuideVisibility_0, _jspx_page_context))
                        return;
                      out.write("\" width=\"55\" height=\"63\" border=\"0\"/>\r\n");
                      out.write("            </td>\r\n");
                      out.write("           ");
                      int evalDoAfterBody = _jspx_th_configByCorp_userGuideVisibility_0.doAfterBody();
                      if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
                        break;
                    } while (true);
                  }
                  if (_jspx_th_configByCorp_userGuideVisibility_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
                    _jspx_tagPool_configByCorp_userGuideVisibility_userType_corpId.reuse(_jspx_th_configByCorp_userGuideVisibility_0);
                    return;
                  }
                  _jspx_tagPool_configByCorp_userGuideVisibility_userType_corpId.reuse(_jspx_th_configByCorp_userGuideVisibility_0);
                  out.write("\r\n");
                  out.write("           ");
                  int evalDoAfterBody = _jspx_th_c_if_9.doAfterBody();
                  if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
                    break;
                } while (true);
              }
              if (_jspx_th_c_if_9.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
                _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_9);
                return;
              }
              _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_9);
              out.write("      \r\n");
              out.write("        ");
              out.write("\r\n");
              out.write("          \r\n");
              out.write("          <td width=\"5%\">\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_10((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("            ");
              if (_jspx_meth_sec$1auth_authComponent_11((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("           </td>\r\n");
              out.write("           \r\n");
              out.write("           <td width=\"8%\">\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_12((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td>\r\n");
              out.write("          ");
              int evalDoAfterBody = _jspx_th_c_if_2.doAfterBody();
              if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
                break;
            } while (true);
          }
          if (_jspx_th_c_if_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
            _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
            return;
          }
          _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
          out.write("\r\n");
          out.write("          \r\n");
          out.write("          \r\n");
          out.write("             \r\n");
          out.write("          ");
          //  c:if
          org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_12 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
          _jspx_th_c_if_12.setPageContext(_jspx_page_context);
          _jspx_th_c_if_12.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_1);
          _jspx_th_c_if_12.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${isTrue}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
          int _jspx_eval_c_if_12 = _jspx_th_c_if_12.doStartTag();
          if (_jspx_eval_c_if_12 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
            do {
              out.write("\r\n");
              out.write("          <td width=\"9%\">\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_13((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("           ");
              if (_jspx_meth_sec$1auth_authComponent_14((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td>\r\n");
              out.write("          \r\n");
              out.write("          <td width=\"8%\">\r\n");
              out.write("           ");
              if (_jspx_meth_sec$1auth_authComponent_15((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("           ");
              if (_jspx_meth_sec$1auth_authComponent_16((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td>\r\n");
              out.write("          \r\n");
              out.write("          <td width=\"6%\">\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_17((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("            ");
              if (_jspx_meth_sec$1auth_authComponent_18((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td>\r\n");
              out.write("          <td width=\"6%\">\r\n");
              out.write("           ");
              if (_jspx_meth_sec$1auth_authComponent_19((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write(" \r\n");
              out.write("          </td>\r\n");
              out.write("             \r\n");
              out.write("          <td width=\"8%\">\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_20((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td>\r\n");
              out.write("          \r\n");
              out.write("          ");
              if (_jspx_meth_configByCorp_fieldVisibility_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          \r\n");
              out.write("          <td width=\"9%\">\r\n");
              out.write("          <a href=\"");
              out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
              out.write("/editProfile.html\"><img src=\"");
              if (_jspx_meth_c_url_33((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\" width=\"83\" height=\"63\" border=\"0\" /></a>\r\n");
              out.write("          </td>\r\n");
              out.write("          ");
              //  c:if
              org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_23 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
              _jspx_th_c_if_23.setPageContext(_jspx_page_context);
              _jspx_th_c_if_23.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
              _jspx_th_c_if_23.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType!='DRIVER'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
              int _jspx_eval_c_if_23 = _jspx_th_c_if_23.doStartTag();
              if (_jspx_eval_c_if_23 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
                do {
                  out.write("\r\n");
                  out.write("          ");
                  //  configByCorp:userGuideVisibility
                  com.trilasoft.app.webapp.tags.UserGuideVisibility _jspx_th_configByCorp_userGuideVisibility_1 = (com.trilasoft.app.webapp.tags.UserGuideVisibility) _jspx_tagPool_configByCorp_userGuideVisibility_userType_corpId.get(com.trilasoft.app.webapp.tags.UserGuideVisibility.class);
                  _jspx_th_configByCorp_userGuideVisibility_1.setPageContext(_jspx_page_context);
                  _jspx_th_configByCorp_userGuideVisibility_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_23);
                  _jspx_th_configByCorp_userGuideVisibility_1.setUserType(quickUserType );
                  _jspx_th_configByCorp_userGuideVisibility_1.setCorpId(quickCorpId );
                  int _jspx_eval_configByCorp_userGuideVisibility_1 = _jspx_th_configByCorp_userGuideVisibility_1.doStartTag();
                  if (_jspx_eval_configByCorp_userGuideVisibility_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
                    do {
                      out.write(" \r\n");
                      out.write("          <td width=\"6%\">\r\n");
                      out.write("          \r\n");
                      out.write("          ");
                      if (_jspx_meth_c_if_24((javax.servlet.jsp.tagext.JspTag) _jspx_th_configByCorp_userGuideVisibility_1, _jspx_page_context))
                        return;
                      out.write("\r\n");
                      out.write("         ");
                      if (_jspx_meth_c_if_25((javax.servlet.jsp.tagext.JspTag) _jspx_th_configByCorp_userGuideVisibility_1, _jspx_page_context))
                        return;
                      out.write("\r\n");
                      out.write("           <img src=\"");
                      if (_jspx_meth_c_url_34((javax.servlet.jsp.tagext.JspTag) _jspx_th_configByCorp_userGuideVisibility_1, _jspx_page_context))
                        return;
                      out.write("\" width=\"55\" height=\"63\" border=\"0\"/>\r\n");
                      out.write("            </td>\r\n");
                      out.write("          ");
                      int evalDoAfterBody = _jspx_th_configByCorp_userGuideVisibility_1.doAfterBody();
                      if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
                        break;
                    } while (true);
                  }
                  if (_jspx_th_configByCorp_userGuideVisibility_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
                    _jspx_tagPool_configByCorp_userGuideVisibility_userType_corpId.reuse(_jspx_th_configByCorp_userGuideVisibility_1);
                    return;
                  }
                  _jspx_tagPool_configByCorp_userGuideVisibility_userType_corpId.reuse(_jspx_th_configByCorp_userGuideVisibility_1);
                  out.write("\r\n");
                  out.write("          ");
                  int evalDoAfterBody = _jspx_th_c_if_23.doAfterBody();
                  if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
                    break;
                } while (true);
              }
              if (_jspx_th_c_if_23.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
                _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_23);
                return;
              }
              _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_23);
              out.write("\r\n");
              out.write("          ");
              out.write("\r\n");
              out.write("          \r\n");
              out.write("\t\t  <td width=\"5%\"> \r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_22((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_23((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td> \r\n");
              out.write("          <td width=\"8%\">\r\n");
              out.write("          ");
              if (_jspx_meth_sec$1auth_authComponent_24((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_12, _jspx_page_context))
                return;
              out.write("\r\n");
              out.write("          </td> \r\n");
              out.write("          ");
              int evalDoAfterBody = _jspx_th_c_if_12.doAfterBody();
              if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
                break;
            } while (true);
          }
          if (_jspx_th_c_if_12.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
            _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_12);
            return;
          }
          _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_12);
          out.write("\r\n");
          out.write("             \r\n");
          out.write("         <td>\r\n");
          out.write("          ");
          if (_jspx_meth_c_if_26((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_1, _jspx_page_context))
            return;
          out.write("\r\n");
          out.write("          ");
          if (_jspx_meth_c_if_27((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_1, _jspx_page_context))
            return;
          out.write("\r\n");
          out.write("           ");
          int evalDoAfterBody = _jspx_th_c_if_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_if_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
        return;
      }
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
      out.write("    \r\n");
      out.write(" \t\t\r\n");
      out.write("         </td>          \r\n");
      out.write("          </tr>\r\n");
      out.write("          </table>\r\n");
      out.write("          </div>\r\n");
      out.write(" \r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write(" ");
if(quickUserType.equalsIgnoreCase("user")){ 
      out.write('\r');
      out.write('\n');
      //  c:if
      org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_28 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
      _jspx_th_c_if_28.setPageContext(_jspx_page_context);
      _jspx_th_c_if_28.setParent(null);
      _jspx_th_c_if_28.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${salesPortalAccess=='false'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
      int _jspx_eval_c_if_28 = _jspx_th_c_if_28.doStartTag();
      if (_jspx_eval_c_if_28 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("<div id=\"TopSearchBox\">\r\n");
          out.write("<div id=\"searchBox\"> \r\n");
          out.write(" <div id=\"hint\">\r\n");
          out.write(" <table style=\"margin:0px;padding:0px;\">\r\n");
          out.write("<th align=\"left\">HINT</th>\r\n");
          out.write("<tr>\r\n");
          out.write("<td>\r\n");
          out.write("<div id=\"qsearch\">\r\n");
          out.write("<ul>\r\n");
          out.write("\r\n");
          out.write("<li>job#<b>db</b> -> DashBoard Details page</li>\r\n");
          out.write("<li>job#<b>cf</b> -> Customer File page</li>\r\n");
          out.write("<li>job#<b>so</b> -> S/O Details page</li>\r\n");
          out.write("<li>job#<b>ts</b> -> Status page</li>\r\n");
          if (_jspx_meth_configByCorp_fieldVisibility_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_28, _jspx_page_context))
            return;
          out.write("\r\n");
          out.write("<li>job#<b>bl</b> -> Billing page</li>\r\n");
          out.write("<li>job#<b>ac</b> -> Accounting page</li>\r\n");
          out.write("<li>job#<b>wt</b> -> Ticket page</li>\r\n");
          out.write("<li>job#<b>fo</b> -> Forwarding page</li>\r\n");
if(accessQuotationFrom==false){ 
          out.write(" \r\n");
          out.write("<li>job#<b>qf</b> -> Quotation File page</li>\r\n");
          out.write("<li>job#<b>qu</b> -> Quote page</li>\r\n");
          out.write(" ");
} 
          out.write("\r\n");
          out.write("<li>job#<b>or</b> -> Order Management</li>\r\n");
          out.write("<li>job#<b>pr</b> -> Partner view page</li>\r\n");
          out.write("<li>job#<b>fc</b> -> File Cabinet on CF / So level</li> \r\n");
          out.write("<li>job#<b>uf</b> -> Upload document screen on CF / SO level</li> \r\n");
          out.write("<li>job#<b>nt</b> -> Notes Overview on CF / So level</li> \r\n");
          out.write("<li>job#<b>nn</b> -> New Note on CF / SO level</li> \r\n");
          out.write("<li>Ext.job#<b>so</b> -> SO Details page own SO#</li>\r\n");
          out.write("<li>Ext.job#<b>ts</b> -> Status page own SO#</li> \r\n");
          out.write("<li>Ext.job#<b>bl</b> -> Billing page own SO#</li>\r\n");
          out.write("<li>Ext.job#<b>ac</b> -> Accounting page own SO#</li>\r\n");
          out.write("<li>Ext.job#<b>wt</b> -> Ticket page own SO#</li>\r\n");
          out.write("<li>Ext.job#<b>fo</b> -> Forwarding page own SO#</li>\r\n");
          out.write("<li>Ext.job#<b>fc</b> -> File Cabinet own SO#</li>\r\n");
          out.write("<li>Ext.job#<b>uf</b> -> Upload document screen own SO#</li>\r\n");
          out.write("<li>Ext.job#<b>nt</b> -> Notes overview own SO#</li>\r\n");
          out.write("<li>Ext.job#<b>nn</b> -> New Note own SO#</li>\r\n");
          out.write("</ul>\r\n");
          out.write("</div>\r\n");
          out.write("</td>\r\n");
          out.write("</tr>\r\n");
          out.write("</table>\r\n");
          out.write(" <div class=\"hint-pointer\"></div></div>\r\n");
          out.write(" <input type=\"text\" value=\"\" onclick=\"searchDisplay();\" onblur=\"searchDisplay1();\" onkeyup=\"validCheck(this,'special1')\" placeholder=\"Quick Search...\" class=\"ajaxSearch_input\" id=\"quickSearch\" >\r\n");
          out.write("  </div>\r\n");
          out.write(" <img src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("/images/searchblue-right.png\" border=\"0\" style=\"vertical-align:bottom;\" onclick=\"return testPattern();\" />\r\n");
          out.write("</div>\r\n");
          int evalDoAfterBody = _jspx_th_c_if_28.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_if_28.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_28);
        return;
      }
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_28);
      out.write("\r\n");
      out.write("\t");
} 
      out.write("\t\r\n");
      out.write("\r\n");
      out.write("<hr/>\r\n");
      out.write('\r');
      out.write('\n');
      //  appfuse:constants
      org.appfuse.webapp.taglib.ConstantsTag _jspx_th_appfuse_constants_0 = (org.appfuse.webapp.taglib.ConstantsTag) _jspx_tagPool_appfuse_constants_scope_nobody.get(org.appfuse.webapp.taglib.ConstantsTag.class);
      _jspx_th_appfuse_constants_0.setPageContext(_jspx_page_context);
      _jspx_th_appfuse_constants_0.setParent(null);
      _jspx_th_appfuse_constants_0.setScope("request");
      int _jspx_eval_appfuse_constants_0 = _jspx_th_appfuse_constants_0.doStartTag();
      if (_jspx_th_appfuse_constants_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_appfuse_constants_scope_nobody.reuse(_jspx_th_appfuse_constants_0);
        return;
      }
      _jspx_tagPool_appfuse_constants_scope_nobody.reuse(_jspx_th_appfuse_constants_0);
      java.lang.String BUNDLE_KEY = null;
      java.lang.String ENC_ALGORITHM = null;
      java.lang.String ENCRYPT_PASSWORD = null;
      java.lang.String FILE_SEP = null;
      java.lang.String USER_HOME = null;
      java.lang.String CONFIG = null;
      java.lang.String PREFERRED_LOCALE_KEY = null;
      java.lang.String USER_KEY = null;
      java.lang.String USER_LIST = null;
      java.lang.String REGISTERED = null;
      java.lang.String ADMIN_ROLE = null;
      java.lang.String USER_ROLE = null;
      java.lang.String USER_ROLES = null;
      java.lang.String AVAILABLE_ROLES = null;
      java.lang.String CSS_THEME = null;
      BUNDLE_KEY = (java.lang.String) _jspx_page_context.findAttribute("BUNDLE_KEY");
      ENC_ALGORITHM = (java.lang.String) _jspx_page_context.findAttribute("ENC_ALGORITHM");
      ENCRYPT_PASSWORD = (java.lang.String) _jspx_page_context.findAttribute("ENCRYPT_PASSWORD");
      FILE_SEP = (java.lang.String) _jspx_page_context.findAttribute("FILE_SEP");
      USER_HOME = (java.lang.String) _jspx_page_context.findAttribute("USER_HOME");
      CONFIG = (java.lang.String) _jspx_page_context.findAttribute("CONFIG");
      PREFERRED_LOCALE_KEY = (java.lang.String) _jspx_page_context.findAttribute("PREFERRED_LOCALE_KEY");
      USER_KEY = (java.lang.String) _jspx_page_context.findAttribute("USER_KEY");
      USER_LIST = (java.lang.String) _jspx_page_context.findAttribute("USER_LIST");
      REGISTERED = (java.lang.String) _jspx_page_context.findAttribute("REGISTERED");
      ADMIN_ROLE = (java.lang.String) _jspx_page_context.findAttribute("ADMIN_ROLE");
      USER_ROLE = (java.lang.String) _jspx_page_context.findAttribute("USER_ROLE");
      USER_ROLES = (java.lang.String) _jspx_page_context.findAttribute("USER_ROLES");
      AVAILABLE_ROLES = (java.lang.String) _jspx_page_context.findAttribute("AVAILABLE_ROLES");
      CSS_THEME = (java.lang.String) _jspx_page_context.findAttribute("CSS_THEME");
      out.write("\r\n");
      out.write("<div id=\"overlayMoodal\"></div>\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("var r1={\r\n");
      out.write("\t\t 'special1':/['#'&'$'&'\\~'&'\\!'&'\\@'&'\\+'&'\\\\'&'\\/'&'%'&'\\^'&'\\&'&'\\*'&'\\:'&'\\;'&'\\>'&'\\<'&'\\?'&'\\{'&'\\}'&'\\|'&'\\['&'\\]'&'\\,'&'\\`'&'\\='&'('&'\\)']/g,\r\n");
      out.write("\t\t 'quotes':/['\\''&'\\\"']/g,\r\n");
      out.write("\t\t 'notnumbers':/[^\\d]/g\r\n");
      out.write("\t\t};\r\n");
      out.write("\t\t\r\n");
      out.write("function validCheck(targetElement,w){\r\n");
      out.write("\t targetElement.value = targetElement.value.replace(r1[w],'');\r\n");
      out.write("\t}\r\n");
      out.write("</script>\r\n");
      out.write("<script>\r\n");
      out.write("function displayMoodal()\r\n");
      out.write("  {\r\n");
      out.write("\twindow.open('editScrachCard.html?&decorator=popup&popup=true','test','height=700,width=725,top=0');   \r\n");
      out.write("  \r\n");
      out.write("  }\r\n");
      out.write("  \r\n");
      out.write("  function hideMoodal()\r\n");
      out.write("  {\r\n");
      out.write("\t\r\n");
      out.write("  \r\n");
      out.write("   }\r\n");
      out.write("</script>\r\n");
      out.write("</script>\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write("    function searchDisplay() {\r\n");
      out.write("       var e = document.getElementById('hint');\r\n");
      out.write("       if(e.style.display == 'block')\r\n");
      out.write("          e.style.display = 'none';\r\n");
      out.write("       else\r\n");
      out.write("          e.style.display = 'block';\r\n");
      out.write("    }\r\n");
      out.write("    function searchDisplay1() {\r\n");
      out.write("        var e = document.getElementById('hint');\r\n");
      out.write("           e.style.display = 'none';\r\n");
      out.write("     }\r\n");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\">\r\n");
      out.write("function keyHandler(e)\r\n");
      out.write("{\r\n");
      out.write(" var pressedKey;\r\n");
      out.write(" if(document.all) {e = window.event}\r\n");
      out.write(" if(document.layers || e.which){pressedKey = 27; }\r\n");
      out.write(" hideMoodal();\r\n");
      out.write("}\r\n");
      out.write("document.onkeypress = keyHandler;\r\n");
      out.write("</script>\r\n");
      out.write("<script type=\"text/javascript\">\r\n");
      out.write(" window.onkeypress = getActiveHTMLElement;\r\n");
      out.write(" function getActiveHTMLElement(keyevent) {\r\n");
      out.write("  keyevent = (keyevent) ? keyevent : ((window.event) ? event : null);\r\n");
      out.write("  if (keyevent) {\r\n");
      out.write("  if(keyevent.keyCode == 13) {\r\n");
      out.write("\t \r\n");
      out.write("  if(document.activeElement.id =='quickSearch'){\r\n");
      out.write("\t       testPattern();\r\n");
      out.write("\t }\r\n");
      out.write("\t\r\n");
      out.write("   }\r\n");
      out.write("  }\r\n");
      out.write(" }\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<script type=\"text/JavaScript\">\r\n");
      out.write("\r\n");
      out.write("Array.prototype.findSuffix = function(searchStr) {\r\n");
      out.write("\t  var returnArray = false;\r\n");
      out.write("\t  for (i=0; i<this.length; i++) {\r\n");
      out.write("\t    if (typeof(searchStr) == 'function') {\r\n");
      out.write("\t      if (searchStr.test(this[i])) {\r\n");
      out.write("\t        if (!returnArray) { \r\n");
      out.write("\t        \treturnArray = [];\r\n");
      out.write("\t        \t}\r\n");
      out.write("\t        returnArray.push(i);\r\n");
      out.write("\t      }\r\n");
      out.write("\t    } else {\r\n");
      out.write("\t      if (this[i]===searchStr) {\r\n");
      out.write("\t        if (!returnArray) {\r\n");
      out.write("\t        \treturnArray = [];\r\n");
      out.write("\t        \t}\r\n");
      out.write("\t        returnArray.push(i);\r\n");
      out.write("\t      }\r\n");
      out.write("\t    }\r\n");
      out.write("\t  }\r\n");
      out.write("\t  if(returnArray==false){\r\n");
      out.write("\t\t  \r\n");
      out.write("\t\t  return 0;\r\n");
      out.write("\t  }\r\n");
      out.write("\t\t  return 1;\r\n");
      out.write("\t}\r\n");
      out.write("function testPattern(){\r\n");
      out.write("     var corpID='");
      out.print(quickCorpId );
      out.write("';\r\n");
      out.write("     var accessQuotationFromCustomerfi = '");
      out.print(accessQuotationFrom );
      out.write("';\r\n");
      out.write("     ");
 String b1="false"; 
      out.write("\r\n");
      out.write("     ");
      //  configByCorp:fieldVisibility
      com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag _jspx_th_configByCorp_fieldVisibility_3 = (com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag) _jspx_tagPool_configByCorp_fieldVisibility_componentId.get(com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag.class);
      _jspx_th_configByCorp_fieldVisibility_3.setPageContext(_jspx_page_context);
      _jspx_th_configByCorp_fieldVisibility_3.setParent(null);
      _jspx_th_configByCorp_fieldVisibility_3.setComponentId("component.standard.claimTab");
      int _jspx_eval_configByCorp_fieldVisibility_3 = _jspx_th_configByCorp_fieldVisibility_3.doStartTag();
      if (_jspx_eval_configByCorp_fieldVisibility_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("   \t\t ");
 b1="true"; 
          out.write("\r\n");
          out.write("     ");
          int evalDoAfterBody = _jspx_th_configByCorp_fieldVisibility_3.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_configByCorp_fieldVisibility_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_3);
        return;
      }
      _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_3);
      out.write("\r\n");
      out.write("     if(accessQuotationFromCustomerfi=='true'){\r\n");
      out.write("\t var suffixArray = ['invalid','db','so','bl','ac','fo','ts','wt','cf','or','pr','fc','uf','nt','nn'];\r\n");
      out.write("\t ");
 if(b1=="true"){ 
      out.write("\r\n");
      out.write("\t\tsuffixArray = ['invalid','db','so','bl','ac','cl','fo','ts','wt','cf','or','pr','fc','uf','nt','nn'];\r\n");
      out.write("         ");
 }
      out.write("\r\n");
      out.write("     }else {\r\n");
      out.write("    var suffixArray = ['invalid','db','so','bl','ac','fo','ts','wt','cf','qf','or','qu','pr','fc','uf','nt','nn'];\r\n");
      out.write("\t ");
 if(b1=="true"){ 
      out.write("\r\n");
      out.write("\t\t suffixArray = ['invalid','db','so','bl','ac','cl','fo','ts','wt','cf','qf','or','qu','pr','fc','uf','nt','nn'];\r\n");
      out.write("         ");
 }
      out.write("\r\n");
      out.write("    \t }\r\n");
      out.write("var myText = document.getElementById('quickSearch');\r\n");
      out.write("\r\n");
      out.write("if(myText.value != \"\"){\r\n");
      out.write("\tvar str1 = myText.value;\r\n");
      out.write("\t var str= str1.toLowerCase(); \r\n");
      out.write("\t str = str.replace('-','');\r\n");
      out.write("\tstr = str.replace(/\\s/g,'');\r\n");
      out.write("\t\r\n");
      out.write("\tvar n=str.indexOf(corpID);\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("\tif(n>0){\r\n");
      out.write("\t \tvar atStart= str.substring(0,n);\r\n");
      out.write("\t\r\n");
      out.write("\t \tif(suffixArray.findSuffix(atStart)){\r\n");
      out.write("\t \t\tif(suffixArray.findSuffix(str.substring(str.length-2,str.length))){\r\n");
      out.write("\t \t\t\talert(\"invalid suffix on both side\");\r\n");
      out.write("\t\t\t       var e1 = document.getElementById('quickSearch');\r\n");
      out.write("\t\t\t       e1.focus();\r\n");
      out.write("\t \t\t\treturn;\r\n");
      out.write("\t \t\t}else{\r\n");
      out.write("\t \t\t\t\r\n");
      out.write("\t \t\t\tlocation.href='globalSearchProcess.html?searchId='+str;\r\n");
      out.write("\t\t\t\treturn ;\r\n");
      out.write("\t \t\t\r\n");
      out.write("\t \t\t}\r\n");
      out.write("\t \t}else{\r\n");
      out.write("\t \t\tif(suffixArray.findSuffix(str.substring(str.length-2,str.length))){\r\n");
      out.write("\t \t\t\tif(suffixArray.findSuffix(str.substring(o,2))){\r\n");
      out.write("\t \t\t\t\talert(\"invalid without corpID and code on both side\");\r\n");
      out.write("\t \t\t       var e1 = document.getElementById('quickSearch');\r\n");
      out.write("\t\t\t       e1.focus();\r\n");
      out.write("\t \t\t\t}else{\r\n");
      out.write("\t \t\t\t\talert(\"valid found at last in case corpID is absent\");\r\n");
      out.write("\t \t\t       var e1 = document.getElementById('quickSearch');\r\n");
      out.write("\t\t\t       e1.focus();\r\n");
      out.write("\t \t\t\t}\r\n");
      out.write("\t \t\t}else{\r\n");
      out.write("\t \t\t\talert(\"enter valid suffix\");\r\n");
      out.write("\t\t\t       var e1 = document.getElementById('quickSearch');\r\n");
      out.write("\t\t\t       e1.focus();\r\n");
      out.write("\t \t\t\treturn ;\r\n");
      out.write("\t \t\t}\r\n");
      out.write("\t \t\t\t\r\n");
      out.write("\t \t\t\r\n");
      out.write("\t \t\t\t\r\n");
      out.write("\t \t\t\r\n");
      out.write("\t \t}\r\n");
      out.write("\t\t\r\n");
      out.write("\t}else{\r\n");
      out.write("        \r\n");
      out.write("\t\tif(suffixArray.findSuffix(str.substring(str.length-2,str.length))){\r\n");
      out.write("\t\t\tif(suffixArray.findSuffix(str.substring(0,2))){\r\n");
      out.write("\t\t\t\talert(\"invalid suffix on both side\");\r\n");
      out.write("\t\t\t       var e1 = document.getElementById('quickSearch');\r\n");
      out.write("\t\t\t       e1.focus();\r\n");
      out.write("\t\t\t\treturn;\r\n");
      out.write("\t\t\t}else{\r\n");
      out.write("\t\t\t\tlocation.href='globalSearchProcess.html?searchId='+str;\r\n");
      out.write("\t\t\t       var e1 = document.getElementById('quickSearch');\r\n");
      out.write("\t\t\t       e1.focus();\r\n");
      out.write("\t\t\t\treturn ;\r\n");
      out.write("\t\t\t}\r\n");
      out.write(" \t\t\t\r\n");
      out.write("\t}else{\r\n");
      out.write("\t\tif(suffixArray.findSuffix(str.substring(0,2))){\r\n");
      out.write("\t\t\tlocation.href='globalSearchProcess.html?searchId='+str;\r\n");
      out.write("\t\t\treturn ;\r\n");
      out.write("\t\t}else{\r\n");
      out.write("\t\t\talert(\"Enter valid suffix\");\r\n");
      out.write("\t\t       var e1 = document.getElementById('quickSearch');\r\n");
      out.write("\t\t       e1.focus();\r\n");
      out.write("\t\t\treturn false;\r\n");
      out.write("\t\t}\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t\r\n");
      out.write("\t}\r\n");
      out.write("\t\t}\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("\tvar atLast = str.substring(str.length-2,str.length);\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("   \r\n");
      out.write("  \tvar atBegin= str.substring(0,2);\r\n");
      out.write("  \t\r\n");
      out.write("\t\r\n");
      out.write("\r\n");
      out.write("var resultAtLast = suffixArray.findSuffix(atLast);\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("var resultAtBegin = suffixArray.findSuffix(atBegin);\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("if(resultAtBegin==1){\r\n");
      out.write("\t\r\n");
      out.write("}else if(resultAtLast==1){\r\n");
      out.write("\t\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("if((resultAtBegin==1) && (resultAtLast==1)){\r\n");
      out.write("\t\r\n");
      out.write("    str1 = str.substring(2,str.length-2);\r\n");
      out.write("    str2=str1.substring(0,4);\r\n");
      out.write("    alert(str2);\r\n");
      out.write("\tif(str2==corpID){\r\n");
      out.write(" \t\talert(\"invalid suffix\");\r\n");
      out.write(" \t\treturn;\r\n");
      out.write("\t}else{\r\n");
      out.write("\t\talert(\"invalid suffix\");\r\n");
      out.write("\t\treturn;\r\n");
      out.write("\t}\r\n");
      out.write(" \t}else{\r\n");
      out.write(" \t\talert(\"LAst Elseif  resultAtLast----\"   + atLast);\r\n");
      out.write(" \t}\r\n");
      out.write("\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t\r\n");
      out.write("else{ \r\n");
      out.write("\talert(\"Would you please enter some text?\");\r\n");
      out.write("\treturn;\r\n");
      out.write("}\r\n");
      out.write("}\r\n");
      out.write("</script>\r\n");
      //  c:set
      org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_5 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
      _jspx_th_c_set_5.setPageContext(_jspx_page_context);
      _jspx_th_c_set_5.setParent(null);
      _jspx_th_c_set_5.setVar("sessionCorpID");
      _jspx_th_c_set_5.setValue(request.getParameter("sessionCorpID") );
      int _jspx_eval_c_set_5 = _jspx_th_c_set_5.doStartTag();
      if (_jspx_th_c_set_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_5);
        return;
      }
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_5);
      out.write("\r\n");
      out.write("\t");
      //  s:hidden
      org.apache.struts2.views.jsp.ui.HiddenTag _jspx_th_s_hidden_0 = (org.apache.struts2.views.jsp.ui.HiddenTag) _jspx_tagPool_s_hidden_value_name_nobody.get(org.apache.struts2.views.jsp.ui.HiddenTag.class);
      _jspx_th_s_hidden_0.setPageContext(_jspx_page_context);
      _jspx_th_s_hidden_0.setParent(null);
      _jspx_th_s_hidden_0.setName("sessionCorpID");
      _jspx_th_s_hidden_0.setValue(request.getParameter("sessionCorpID") );
      int _jspx_eval_s_hidden_0 = _jspx_th_s_hidden_0.doStartTag();
      if (_jspx_th_s_hidden_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_s_hidden_value_name_nobody.reuse(_jspx_th_s_hidden_0);
        return;
      }
      _jspx_tagPool_s_hidden_value_name_nobody.reuse(_jspx_th_s_hidden_0);
      out.write("\r\n");
      out.write("\t\r\n");
      out.write("\r\n");
      out.write("<script>\r\n");
      out.write("function message(){\t\r\n");
      out.write("\tvar fileId=document.getElementById( \"fileID\" ).value;\r\n");
      out.write("\tvar fileCabView = '");
      out.print( quickFileCabinetView );
      out.write("';\r\n");
      out.write("\t//10457 - Company Table\r\n");
      out.write("\tvar forceDocCenter='");
      out.print(forceDocCenter );
      out.write("';\r\n");
      out.write("\tvar noteFor=\"\";\r\n");
      out.write("\tvar PPID =\"\";\r\n");
      out.write("\tif(document.getElementById( \"noteFor\" )==null || document.getElementById( \"noteFor\" )=='null'){\r\n");
      out.write("\t\tnoteFor=\"\";\r\n");
      out.write("\t}else{\r\n");
      out.write("\t   noteFor=document.getElementById( \"noteFor\" ).value;\r\n");
      out.write("\t}\r\n");
      out.write("\tif(fileId == '' ){\r\n");
      out.write("\t\talert('Please choose a valid record before opening the File Cabinet.');\r\n");
      out.write("\t}else{\r\n");
      out.write("\t\tvar fileNameFor=document.getElementById( \"fileNameFor\" ).value;\r\n");
      out.write("\t\tvar fileNameFor=document.getElementById( \"fileNameFor\" ).value;\r\n");
      out.write("\t\tvar ppType=document.getElementById( \"ppType\" ).value;\r\n");
      out.write("\t\tvar from = \"\";\r\n");
      out.write("\t\tif(document.getElementById( \"from\" ) == 'null' || document.getElementById( \"from\" ) == null){\r\n");
      out.write("\t\t\tfrom = \"\";\r\n");
      out.write("\t\t}else{\r\n");
      out.write("\t\t\tfrom = document.getElementById( \"from\" ).value;\r\n");
      out.write("\t\t}\r\n");
      out.write("\t\tvar forQuotationCheck=document.getElementById( \"forQuotation\" );\r\n");
      out.write("\t\tif(forQuotationCheck!=null){\r\n");
      out.write("\t\tvar forQuotation =forQuotationCheck.value; \r\n");
      out.write("\t\t}else{\r\n");
      out.write("\t\tvar forQuotation =\"\";\r\n");
      out.write("\t\t}\r\n");
      out.write("\t  \tif(fileId != '' && fileNameFor ==\"\"){\r\n");
      out.write("\t    \talert('Please choose a valid record before opening the File Cabinet.');\r\n");
      out.write("\t  \t}\r\n");
      out.write("\t\tif(fileId != '' && fileNameFor !=\"\" && ppType ==\"\" && forQuotation==\"\"){\r\n");
      out.write("\t\t\tif(fileCabView == 'Document Centre' || forceDocCenter == \"true\" ){\r\n");
      out.write("\t\t\t\tlocation.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false';\r\n");
      out.write("\t\t\t}else{\r\n");
      out.write("\t\t\t\tlocation.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false'; \r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\t//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); \r\n");
      out.write("\t\t}\r\n");
      out.write("\t\tif(fileId != '' && fileNameFor !=\"\" && ppType ==\"\" && forQuotation==\"QC\"){\r\n");
      out.write("\t\t\tif(fileCabView == 'Document Centre' || forceDocCenter == \"true\" ){\r\n");
      out.write("\t\t\t\tlocation.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&forQuotation=QC&active=true&secure=false'; \r\n");
      out.write("\t\t\t}else{\r\n");
      out.write("\t\t\t\tlocation.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&forQuotation=QC&active=true&secure=false';  \r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t//\tlocation.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&forQuotation=QC&active=true&secure=false'; \r\n");
      out.write("\t\t\t//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); \r\n");
      out.write("\t\t}\r\n");
      out.write("\t\tif(fileId != '' && fileNameFor !=\"\" && fileNameFor ==\"Truck\"){\r\n");
      out.write("\t\t\tif(fileCabView == 'Document Centre' || forceDocCenter == \"true\" ){\r\n");
      out.write("\t\t\t\tlocation.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false'; \r\n");
      out.write("\t\t\t}else{\r\n");
      out.write("\t\t\t\tlocation.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false';  \r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\t//location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false'; \r\n");
      out.write("\t\t\t//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); \r\n");
      out.write("\t\t}\r\n");
      out.write("\t\tif(fileId != '' && fileNameFor !=\"\" && ppType !=\"\" && from != 'View'){\r\n");
      out.write("\t\t\ttry{\r\n");
      out.write("\t\t\t\tPPID = document.getElementById(\"PPID\").value;\r\n");
      out.write("\t\t\t}catch(e){\r\n");
      out.write("\t\t\t\tPPID=document.getElementById( \"fileID\" ).value;\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\tif(fileCabView == 'Document Centre' || forceDocCenter == \"true\" ){\r\n");
      out.write("\t\t\t\tlocation.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&PPID='+PPID;  \r\n");
      out.write("\t\t\t}else{\r\n");
      out.write("\t\t\t\tlocation.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&PPID='+PPID; \r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\t//location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&PPID='+PPID; \r\n");
      out.write("\t\t\t//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); \r\n");
      out.write("\t\t}\r\n");
      out.write("\t\tif(fileId != '' && fileNameFor !=\"\" && ppType !=\"\" && from == 'View'){\r\n");
      out.write("\t\t\ttry{\r\n");
      out.write("\t\t\t    PPID = document.getElementById(\"PPID\").value;\r\n");
      out.write("\t\t\t}catch(e){\r\n");
      out.write("\t\t\t\tPPID=document.getElementById( \"fileID\" ).value;\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\tif(fileCabView == 'Document Centre' || forceDocCenter == \"true\" ){\r\n");
      out.write("\t\t\t\tlocation.href='myFilesDocType.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&from=View&ppCode='+ppCode; \r\n");
      out.write("\t\t\t}else{\r\n");
      out.write("\t\t\t\tlocation.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&from=View&ppCode='+ppCode; \r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\t//location.href='myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor+'&active=true&secure=false&ppType='+ppType+'&from=View&ppCode='+ppCode; \r\n");
      out.write("\t\t\t//window.open('myFiles.html?id='+fileId+'&myFileFor='+fileNameFor+'&active=true&decorator=popup&popup=true','docs','left=0,top=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=1,status=1,toolbar=0'); \r\n");
      out.write("\t\t}\r\n");
      out.write("\t}\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("function logOut(){\r\n");
      out.write("\tlocation.href=\"userLogOut.html\";\r\n");
      out.write("}\r\n");
      out.write("function accountUserlogOut(){\r\n");
      out.write("\tlocation.href=\"accountUserLogOut.html\";\r\n");
      out.write("}\r\n");
      out.write("function alertList(){\r\n");
      out.write("\tlocation.href=\"alertHistoryList.html\";\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("function openNotes(){\r\n");
      out.write("\tvar from = \"\";\r\n");
      out.write("\tvar PPID=\"\";\r\n");
      out.write("\tif(document.getElementById( \"from\" ) == 'null' || document.getElementById( \"from\" ) == null){\r\n");
      out.write("\t\tfrom = \"\";\r\n");
      out.write("\t}else{\r\n");
      out.write("\t\tfrom = document.getElementById( \"from\" ).value;\r\n");
      out.write("\t}\r\n");
      out.write("\tvar ppType=document.getElementById( \"ppType\" ).value;\r\n");
      out.write("\tif(ppType !=\"\"){\r\n");
      out.write("\t\tvar ppType=document.getElementById( \"ppType\" ).value;\r\n");
      out.write("\t\ttry{\r\n");
      out.write("\t\tPPID = document.getElementById(\"PPID\").value;\r\n");
      out.write("\t\t}catch(e){\r\n");
      out.write("\t\t\tPPID='");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfWhom}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("';\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t    location.href=\"notess.html?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfWhom }", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&notesId=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteID }", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&noteFor=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteFor }", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&from=\"+from+\"&PPID=\"+PPID+\"&ppType=\"+ppType;\r\n");
      out.write("\t}else{\r\n");
      out.write("\t\tlocation.href=\"notess.html?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfWhom }", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&notesId=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteID }", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&noteFor=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteFor }", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\";\r\n");
      out.write("\t}\r\n");
      out.write("}\r\n");
      out.write("function openTasks(){\r\n");
      out.write("\tvar corpID='");
      out.print(quickCorpId );
      out.write("';\r\n");
      out.write("\tvar flagActivityMgmtVersion2 =  ");
      out.print(activityMgmtVersion2 );
      out.write(";\r\n");
      out.write("\tvar idOfTasks='");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfTasks}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("';\r\n");
      out.write("\tif(idOfTasks ==''){\r\n");
      out.write("\t\talert('Please choose a valid record before opening the Tasks.');\r\n");
      out.write("\t}else{\r\n");
      out.write("\t\t\r\n");
      out.write("\t\tif(!flagActivityMgmtVersion2 || ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName=='customerfile'}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(")\r\n");
      out.write("\t\t{\r\n");
      out.write("\t\tif(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName=='workticket'}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write('|');
      out.write('|');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName=='claim'}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("){\r\n");
      out.write("\t\t\tlocation.href=\"tasks.html?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfTasks}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&tableName=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&soId=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tasksSoId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\";\r\n");
      out.write("\t\t}else{\r\n");
      out.write("\t\t\tlocation.href=\"tasks.html?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfTasks}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&tableName=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\";\r\n");
      out.write("\t\t}}\r\n");
      out.write("\t\telse{\r\n");
      out.write("\t\t\t  if(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName=='workticket'}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write('|');
      out.write('|');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName=='claim'}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("){\r\n");
      out.write("\t\t\t\t  location.href=\"tasksCheckList.html?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tasksSoId}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&tableName=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\";\t  \r\n");
      out.write("\t\t\t  }else{\r\n");
      out.write("\t\t\tlocation.href=\"tasksCheckList.html?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfTasks}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&tableName=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\";\r\n");
      out.write("\t\t\t//location.href=\"tasks.html?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfTasks}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&tableName=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${tableName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\";\r\n");
      out.write("\t\t\t//<a href=\"tasksCheckList.html?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${soObj.id}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&shipnumber=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${soObj.shipNumber}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" ><span>Tasks Check List</span></a>\r\n");
      out.write("\t\t\t  }\r\n");
      out.write("\t\t}\r\n");
      out.write("\t}\r\n");
      out.write("\t");
      if (_jspx_meth_c_remove_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\t");
      if (_jspx_meth_c_remove_1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\t}\r\n");
      out.write("function openRelatedNotes(){\r\n");
      out.write("\tvar notefor='");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteFor}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("';\r\n");
      out.write("\tif(notefor!='Partner'){\t\t\r\n");
      out.write("\tlocation.href=\"raleventNotess.html?id=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfWhom }", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&customerNumber=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteID }", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&noteFor=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteFor}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("&noteFrom=");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteFor}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\";\t\r\n");
      out.write("\t}\r\n");
      out.write("}\r\n");
      out.write("function defaultOpsCalendar(){\r\n");
      out.write("\tvar defaultOperationCalendarVal='';\r\n");
      out.write("\t");
      //  configByCorp:fieldVisibility
      com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag _jspx_th_configByCorp_fieldVisibility_4 = (com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag) _jspx_tagPool_configByCorp_fieldVisibility_componentId.get(com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag.class);
      _jspx_th_configByCorp_fieldVisibility_4.setPageContext(_jspx_page_context);
      _jspx_th_configByCorp_fieldVisibility_4.setParent(null);
      _jspx_th_configByCorp_fieldVisibility_4.setComponentId("component.field.Resource.OpsCalendarView");
      int _jspx_eval_configByCorp_fieldVisibility_4 = _jspx_th_configByCorp_fieldVisibility_4.doStartTag();
      if (_jspx_eval_configByCorp_fieldVisibility_4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\r\n");
          out.write("\t\tdefaultOperationCalendarVal='");
          out.print(defaultOperationCalendarVal );
          out.write("';\r\n");
          out.write("\t");
          int evalDoAfterBody = _jspx_th_configByCorp_fieldVisibility_4.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_configByCorp_fieldVisibility_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_4);
        return;
      }
      _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_4);
      out.write("\r\n");
      out.write("\t if(defaultOperationCalendarVal==\"Planning Calendar\"){ \r\n");
      out.write("\t\t\twindow.open('planningCalendarList.html?decorator=popup&popup=true','planingCalendar','height=600,width=1100,top=90, left=50, scrollbars=yes,resizable=yes');\r\n");
      out.write("\t }else if(defaultOperationCalendarVal==\"Crew Calendar\"){ \r\n");
      out.write("\t\t \twindow.open('crewCalenderView.html?decorator=popup&popup=true','crewCalendar','height=600,width=1100,top=90, left=50, scrollbars=yes,resizable=yes');\r\n");
      out.write("\t }else if(defaultOperationCalendarVal==\"Work Planning\"){ \r\n");
      out.write("\t \t\twindow.open('workPlan.html?decorator=popup&popup=true','workPlanning','height=600,width=1100,top=90, left=50, scrollbars=yes,resizable=yes');\r\n");
      out.write("\t }else{ \r\n");
      out.write("\t \t\twindow.open('opsCalendar.html?decorator=popup&popup=true','opsCalendar','height=600,width=1100,top=90, left=50, scrollbars=yes,resizable=yes');\r\n");
      out.write("\t } \r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("</script>\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_set_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_0.setPageContext(_jspx_page_context);
    _jspx_th_c_set_0.setParent(null);
    _jspx_th_c_set_0.setVar("ctx");
    _jspx_th_c_set_0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_0 = _jspx_th_c_set_0.doStartTag();
    if (_jspx_th_c_set_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
      return true;
    }
    _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
    return false;
  }

  private boolean _jspx_meth_c_set_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_1.setPageContext(_jspx_page_context);
    _jspx_th_c_set_1.setParent(null);
    _jspx_th_c_set_1.setVar("datePattern");
    int _jspx_eval_c_set_1 = _jspx_th_c_set_1.doStartTag();
    if (_jspx_eval_c_set_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_c_set_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_c_set_1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_c_set_1.doInitBody();
      }
      do {
        if (_jspx_meth_fmt_message_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_set_1, _jspx_page_context))
          return true;
        int evalDoAfterBody = _jspx_th_c_set_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_c_set_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_c_set_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var.reuse(_jspx_th_c_set_1);
      return true;
    }
    _jspx_tagPool_c_set_var.reuse(_jspx_th_c_set_1);
    return false;
  }

  private boolean _jspx_meth_fmt_message_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_set_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_0 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_0.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_set_1);
    _jspx_th_fmt_message_0.setKey("date.format");
    int _jspx_eval_fmt_message_0 = _jspx_th_fmt_message_0.doStartTag();
    if (_jspx_th_fmt_message_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_0);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_0);
    return false;
  }

  private boolean _jspx_meth_c_if_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_0.setPageContext(_jspx_page_context);
    _jspx_th_c_if_0.setParent(null);
    _jspx_th_c_if_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.locale.language != 'en'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_0 = _jspx_th_c_if_0.doStartTag();
    if (_jspx_eval_c_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("    <div id=\"switchLocale\"><a href=\"");
        if (_jspx_meth_c_url_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_0, _jspx_page_context))
          return true;
        out.write('"');
        out.write('>');
        if (_jspx_meth_fmt_message_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_0, _jspx_page_context))
          return true;
        out.write(" in English</a></div>\r\n");
        out.write("    ");
        if (_jspx_meth_fmt_setLocale_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        int evalDoAfterBody = _jspx_th_c_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
    return false;
  }

  private boolean _jspx_meth_c_url_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_0 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_0.setPageContext(_jspx_page_context);
    _jspx_th_c_url_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_0);
    _jspx_th_c_url_0.setValue("/?locale=en");
    int _jspx_eval_c_url_0 = _jspx_th_c_url_0.doStartTag();
    if (_jspx_th_c_url_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_0);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_0);
    return false;
  }

  private boolean _jspx_meth_fmt_message_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_1 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_1.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_0);
    _jspx_th_fmt_message_1.setKey("webapp.name");
    int _jspx_eval_fmt_message_1 = _jspx_th_fmt_message_1.doStartTag();
    if (_jspx_th_fmt_message_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_1);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_1);
    return false;
  }

  private boolean _jspx_meth_fmt_setLocale_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:setLocale
    org.apache.taglibs.standard.tag.rt.fmt.SetLocaleTag _jspx_th_fmt_setLocale_0 = (org.apache.taglibs.standard.tag.rt.fmt.SetLocaleTag) _jspx_tagPool_fmt_setLocale_value_scope_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.SetLocaleTag.class);
    _jspx_th_fmt_setLocale_0.setPageContext(_jspx_page_context);
    _jspx_th_fmt_setLocale_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_0);
    _jspx_th_fmt_setLocale_0.setValue(new String("en_US"));
    _jspx_th_fmt_setLocale_0.setScope("session");
    int _jspx_eval_fmt_setLocale_0 = _jspx_th_fmt_setLocale_0.doStartTag();
    if (_jspx_th_fmt_setLocale_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_setLocale_value_scope_nobody.reuse(_jspx_th_fmt_setLocale_0);
      return true;
    }
    _jspx_tagPool_fmt_setLocale_value_scope_nobody.reuse(_jspx_th_fmt_setLocale_0);
    return false;
  }

  private boolean _jspx_meth_c_set_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_2 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_2.setPageContext(_jspx_page_context);
    _jspx_th_c_set_2.setParent(null);
    _jspx_th_c_set_2.setVar("salesPortalAccess");
    _jspx_th_c_set_2.setValue(new String("false"));
    int _jspx_eval_c_set_2 = _jspx_th_c_set_2.doStartTag();
    if (_jspx_th_c_set_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_2);
      return true;
    }
    _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_2);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_0 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_0.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_0.setParent(null);
    _jspx_th_sec$1auth_authComponent_0.setComponentId("module.script.form.corpSalesScript");
    int _jspx_eval_sec$1auth_authComponent_0 = _jspx_th_sec$1auth_authComponent_0.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write('\r');
        out.write('\n');
        if (_jspx_meth_c_set_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_0, _jspx_page_context))
          return true;
        out.write('\r');
        out.write('\n');
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_0);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_0);
    return false;
  }

  private boolean _jspx_meth_c_set_3(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_3 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_3.setPageContext(_jspx_page_context);
    _jspx_th_c_set_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_0);
    _jspx_th_c_set_3.setVar("salesPortalAccess");
    _jspx_th_c_set_3.setValue(new String("true"));
    int _jspx_eval_c_set_3 = _jspx_th_c_set_3.doStartTag();
    if (_jspx_th_c_set_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_3);
      return true;
    }
    _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_3);
    return false;
  }

  private boolean _jspx_meth_c_url_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_1 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_1.setPageContext(_jspx_page_context);
    _jspx_th_c_url_1.setParent(null);
    _jspx_th_c_url_1.setValue("/images/logo_redsky.png");
    int _jspx_eval_c_url_1 = _jspx_th_c_url_1.doStartTag();
    if (_jspx_th_c_url_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_1);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_1);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_1 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_1.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_1.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_1 = _jspx_th_sec$1auth_authComponent_1.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          \t<a href=\"toDos.html?fromUser=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.remoteUser}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\"><img src=\"");
        if (_jspx_meth_c_url_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_1, _jspx_page_context))
          return true;
        out.write("\" width=\"94\" height=\"63\" border=\"0\"/></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_1);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_1);
    return false;
  }

  private boolean _jspx_meth_c_url_2(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_2 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_2.setPageContext(_jspx_page_context);
    _jspx_th_c_url_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_1);
    _jspx_th_c_url_2.setValue("/images/activity-trans.png");
    int _jspx_eval_c_url_2 = _jspx_th_c_url_2.doStartTag();
    if (_jspx_th_c_url_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_2);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_2);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_2(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_2 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_2.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_2.setComponentId("module.section.header.topActivityMgtForAgent");
    int _jspx_eval_sec$1auth_authComponent_2 = _jspx_th_sec$1auth_authComponent_2.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("        \t <a href=\"toDos.html?fromUser=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.remoteUser}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\"><img src=\"");
        if (_jspx_meth_c_url_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_2, _jspx_page_context))
          return true;
        out.write("\" width=\"94\" height=\"63\" border=\"0\"/></a>\r\n");
        out.write("         ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_2);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_2);
    return false;
  }

  private boolean _jspx_meth_c_url_3(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_3 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_3.setPageContext(_jspx_page_context);
    _jspx_th_c_url_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_2);
    _jspx_th_c_url_3.setValue("/images/activity-trans.png");
    int _jspx_eval_c_url_3 = _jspx_th_c_url_3.doStartTag();
    if (_jspx_th_c_url_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_3);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_3);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_3(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_3 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_3.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_3.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_3 = _jspx_th_sec$1auth_authComponent_3.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_3, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_3, _jspx_page_context))
          return true;
        out.write(" \r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_3);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_3);
    return false;
  }

  private boolean _jspx_meth_c_if_3(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_3 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_3.setPageContext(_jspx_page_context);
    _jspx_th_c_if_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_3);
    _jspx_th_c_if_3.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${fileID == ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_3 = _jspx_th_c_if_3.doStartTag();
    if (_jspx_eval_c_if_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t\t<a href=\"#\"><img src=\"");
        if (_jspx_meth_c_url_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_3, _jspx_page_context))
          return true;
        out.write("\" width=\"85\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\" onclick=\"message(); return false;\"/></a>\r\n");
        out.write("\t\t\t");
        int evalDoAfterBody = _jspx_th_c_if_3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_3);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_3);
    return false;
  }

  private boolean _jspx_meth_c_url_4(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_4 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_4.setPageContext(_jspx_page_context);
    _jspx_th_c_url_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_3);
    _jspx_th_c_url_4.setValue("/images/file-cabinet-trans.png");
    int _jspx_eval_c_url_4 = _jspx_th_c_url_4.doStartTag();
    if (_jspx_th_c_url_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_4);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_4);
    return false;
  }

  private boolean _jspx_meth_c_if_4(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_4 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_4.setPageContext(_jspx_page_context);
    _jspx_th_c_if_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_3);
    _jspx_th_c_if_4.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${fileID != ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_4 = _jspx_th_c_if_4.doStartTag();
    if (_jspx_eval_c_if_4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\t\t          \r\n");
        out.write("\t          <a href=\"#\"><img src=\"");
        if (_jspx_meth_c_url_5((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_4, _jspx_page_context))
          return true;
        out.write("\" width=\"85\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\" onclick=\"message(); return false;\"/></a>\r\n");
        out.write("\t      ");
        int evalDoAfterBody = _jspx_th_c_if_4.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_4);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_4);
    return false;
  }

  private boolean _jspx_meth_c_url_5(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_4, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_5 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_5.setPageContext(_jspx_page_context);
    _jspx_th_c_url_5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_4);
    _jspx_th_c_url_5.setValue("/images/file-cabinet-trans.png");
    int _jspx_eval_c_url_5 = _jspx_th_c_url_5.doStartTag();
    if (_jspx_th_c_url_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_5);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_5);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_4(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_4 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_4.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_4.setComponentId("module.section.header.driver");
    int _jspx_eval_sec$1auth_authComponent_4 = _jspx_th_sec$1auth_authComponent_4.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_5((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_4, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_6((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_4, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("         ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_4.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_4);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_4);
    return false;
  }

  private boolean _jspx_meth_c_if_5(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_4, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_5 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_5.setPageContext(_jspx_page_context);
    _jspx_th_c_if_5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_4);
    _jspx_th_c_if_5.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${fileID == ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_5 = _jspx_th_c_if_5.doStartTag();
    if (_jspx_eval_c_if_5 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t\t<a href=\"#\"><img src=\"");
        if (_jspx_meth_c_url_6((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_5, _jspx_page_context))
          return true;
        out.write("\" width=\"85\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\" onclick=\"message(); return false;\"/></a>\r\n");
        out.write("\t\t\t");
        int evalDoAfterBody = _jspx_th_c_if_5.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_5);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_5);
    return false;
  }

  private boolean _jspx_meth_c_url_6(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_5, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_6 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_6.setPageContext(_jspx_page_context);
    _jspx_th_c_url_6.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_5);
    _jspx_th_c_url_6.setValue("/images/file-cabinet-trans.png");
    int _jspx_eval_c_url_6 = _jspx_th_c_url_6.doStartTag();
    if (_jspx_th_c_url_6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_6);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_6);
    return false;
  }

  private boolean _jspx_meth_c_if_6(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_4, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_6 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_6.setPageContext(_jspx_page_context);
    _jspx_th_c_if_6.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_4);
    _jspx_th_c_if_6.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${fileID != ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_6 = _jspx_th_c_if_6.doStartTag();
    if (_jspx_eval_c_if_6 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\t\t          \r\n");
        out.write("\t          <a href=\"#\"><img src=\"");
        if (_jspx_meth_c_url_7((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_6, _jspx_page_context))
          return true;
        out.write("\" width=\"85\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\" onclick=\"message(); return false;\"/></a>\r\n");
        out.write("\t      ");
        int evalDoAfterBody = _jspx_th_c_if_6.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_6);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_6);
    return false;
  }

  private boolean _jspx_meth_c_url_7(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_6, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_7 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_7.setPageContext(_jspx_page_context);
    _jspx_th_c_url_7.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_6);
    _jspx_th_c_url_7.setValue("/images/file-cabinet-trans.png");
    int _jspx_eval_c_url_7 = _jspx_th_c_url_7.doStartTag();
    if (_jspx_th_c_url_7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_7);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_7);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_5(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_5 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_5.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_5.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_5 = _jspx_th_sec$1auth_authComponent_5.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_5 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("        \t <img src=\"");
        if (_jspx_meth_c_url_8((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_5, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" />\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_5.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_5);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_5);
    return false;
  }

  private boolean _jspx_meth_c_url_8(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_5, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_8 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_8.setPageContext(_jspx_page_context);
    _jspx_th_c_url_8.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_5);
    _jspx_th_c_url_8.setValue("/images/notes-trans-new.png");
    int _jspx_eval_c_url_8 = _jspx_th_c_url_8.doStartTag();
    if (_jspx_th_c_url_8.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_8);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_8);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_6(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_6 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_6.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_6.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_6.setComponentId("module.section.header.topImagesExternal");
    int _jspx_eval_sec$1auth_authComponent_6 = _jspx_th_sec$1auth_authComponent_6.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_6 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("        \t <img src=\"");
        if (_jspx_meth_c_url_9((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_6, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" />\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_6.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_6);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_6);
    return false;
  }

  private boolean _jspx_meth_c_url_9(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_6, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_9 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_9.setPageContext(_jspx_page_context);
    _jspx_th_c_url_9.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_6);
    _jspx_th_c_url_9.setValue("/images/notes-trans-new.png");
    int _jspx_eval_c_url_9 = _jspx_th_c_url_9.doStartTag();
    if (_jspx_th_c_url_9.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_9);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_9);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_7(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_7 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_7.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_7.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_7.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_7 = _jspx_th_sec$1auth_authComponent_7.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_7 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_7((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_7, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_8((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_7, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_7.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_7);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_7);
    return false;
  }

  private boolean _jspx_meth_c_if_7(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_7, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_7 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_7.setPageContext(_jspx_page_context);
    _jspx_th_c_if_7.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_7);
    _jspx_th_c_if_7.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfTasks!=''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_7 = _jspx_th_c_if_7.doStartTag();
    if (_jspx_eval_c_if_7 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <a onclick=\"openTasks();\"><img src=\"");
        if (_jspx_meth_c_url_10((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_7, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" /></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_c_if_7.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_7);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_7);
    return false;
  }

  private boolean _jspx_meth_c_url_10(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_7, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_10 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_10.setPageContext(_jspx_page_context);
    _jspx_th_c_url_10.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_7);
    _jspx_th_c_url_10.setValue("/images/task-trans-new.png");
    int _jspx_eval_c_url_10 = _jspx_th_c_url_10.doStartTag();
    if (_jspx_th_c_url_10.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_10);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_10);
    return false;
  }

  private boolean _jspx_meth_c_if_8(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_7, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_8 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_8.setPageContext(_jspx_page_context);
    _jspx_th_c_if_8.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_7);
    _jspx_th_c_if_8.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfTasks== ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_8 = _jspx_th_c_if_8.doStartTag();
    if (_jspx_eval_c_if_8 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <img src=\"");
        if (_jspx_meth_c_url_11((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_8, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" />\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_c_if_8.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_8.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_8);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_8);
    return false;
  }

  private boolean _jspx_meth_c_url_11(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_8, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_11 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_11.setPageContext(_jspx_page_context);
    _jspx_th_c_url_11.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_8);
    _jspx_th_c_url_11.setValue("/images/task-trans-new.png");
    int _jspx_eval_c_url_11 = _jspx_th_c_url_11.doStartTag();
    if (_jspx_th_c_url_11.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_11);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_11);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_8(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_8 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_8.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_8.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_8.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_8 = _jspx_th_sec$1auth_authComponent_8.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_8 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("          \r\n");
        out.write("          \t\t<a onclick=\"window.open('surveysList.html?decorator=popup&popup=true&from=survey','surveysList','height=600,width=875,top=20, left=60, scrollbars=yes,resizable=yes').focus();\" />\r\n");
        out.write("          \t\t\t<img src=\"");
        if (_jspx_meth_c_url_12((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_8, _jspx_page_context))
          return true;
        out.write("\" width=\"70\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false;\"/>\r\n");
        out.write("          \t\t</a>\r\n");
        out.write("         \t");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_8.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_8.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_8);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_8);
    return false;
  }

  private boolean _jspx_meth_c_url_12(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_8, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_12 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_12.setPageContext(_jspx_page_context);
    _jspx_th_c_url_12.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_8);
    _jspx_th_c_url_12.setValue("/images/survery-new.png");
    int _jspx_eval_c_url_12 = _jspx_th_c_url_12.doStartTag();
    if (_jspx_th_c_url_12.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_12);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_12);
    return false;
  }

  private boolean _jspx_meth_configByCorp_fieldVisibility_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  configByCorp:fieldVisibility
    com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag _jspx_th_configByCorp_fieldVisibility_0 = (com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag) _jspx_tagPool_configByCorp_fieldVisibility_componentId.get(com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag.class);
    _jspx_th_configByCorp_fieldVisibility_0.setPageContext(_jspx_page_context);
    _jspx_th_configByCorp_fieldVisibility_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_configByCorp_fieldVisibility_0.setComponentId("component.tab.header.operations");
    int _jspx_eval_configByCorp_fieldVisibility_0 = _jspx_th_configByCorp_fieldVisibility_0.doStartTag();
    if (_jspx_eval_configByCorp_fieldVisibility_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t         <td width=\"8%\">\r\n");
        out.write("\t          \t");
        if (_jspx_meth_sec$1auth_authComponent_9((javax.servlet.jsp.tagext.JspTag) _jspx_th_configByCorp_fieldVisibility_0, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("\t         </td>\r\n");
        out.write("         ");
        int evalDoAfterBody = _jspx_th_configByCorp_fieldVisibility_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_configByCorp_fieldVisibility_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_0);
      return true;
    }
    _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_0);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_9(javax.servlet.jsp.tagext.JspTag _jspx_th_configByCorp_fieldVisibility_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_9 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_9.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_9.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_configByCorp_fieldVisibility_0);
    _jspx_th_sec$1auth_authComponent_9.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_9 = _jspx_th_sec$1auth_authComponent_9.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_9 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("          \r\n");
        out.write("\t          \t\t<a onclick=\"defaultOpsCalendar();\" />\r\n");
        out.write("\t          \t\t\t<img src=\"");
        if (_jspx_meth_c_url_13((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_9, _jspx_page_context))
          return true;
        out.write("\" width=\"82\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false;\"/>\r\n");
        out.write("\t          \t\t</a>\r\n");
        out.write("\t         \t");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_9.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_9.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_9);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_9);
    return false;
  }

  private boolean _jspx_meth_c_url_13(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_9, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_13 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_13.setPageContext(_jspx_page_context);
    _jspx_th_c_url_13.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_9);
    _jspx_th_c_url_13.setValue("/images/ops-calendar.png");
    int _jspx_eval_c_url_13 = _jspx_th_c_url_13.doStartTag();
    if (_jspx_th_c_url_13.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_13);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_13);
    return false;
  }

  private boolean _jspx_meth_c_url_14(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_14 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_14.setPageContext(_jspx_page_context);
    _jspx_th_c_url_14.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_c_url_14.setValue("/images/myaccount-new.png");
    int _jspx_eval_c_url_14 = _jspx_th_c_url_14.doStartTag();
    if (_jspx_th_c_url_14.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_14);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_14);
    return false;
  }

  private boolean _jspx_meth_c_if_10(javax.servlet.jsp.tagext.JspTag _jspx_th_configByCorp_userGuideVisibility_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_10 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_10.setPageContext(_jspx_page_context);
    _jspx_th_c_if_10.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_configByCorp_userGuideVisibility_0);
    _jspx_th_c_if_10.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType == 'USER'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_10 = _jspx_th_c_if_10.doStartTag();
    if (_jspx_eval_c_if_10 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("            <a onclick=\"window.open('salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=ALL/ General','guideList','height=600,width=995,top=20, left=60, scrollbars=yes,resizable=yes').focus();\" />\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_c_if_10.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_10.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_10);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_10);
    return false;
  }

  private boolean _jspx_meth_c_if_11(javax.servlet.jsp.tagext.JspTag _jspx_th_configByCorp_userGuideVisibility_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_11 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_11.setPageContext(_jspx_page_context);
    _jspx_th_c_if_11.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_configByCorp_userGuideVisibility_0);
    _jspx_th_c_if_11.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType != 'USER'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_11 = _jspx_th_c_if_11.doStartTag();
    if (_jspx_eval_c_if_11 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("           <a onclick=\"window.open('salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=Portals','guideList','height=600,width=995,top=20, left=60, scrollbars=yes,resizable=yes').focus();\" />\r\n");
        out.write("     \t");
        int evalDoAfterBody = _jspx_th_c_if_11.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_11.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_11);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_11);
    return false;
  }

  private boolean _jspx_meth_c_url_15(javax.servlet.jsp.tagext.JspTag _jspx_th_configByCorp_userGuideVisibility_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_15 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_15.setPageContext(_jspx_page_context);
    _jspx_th_c_url_15.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_configByCorp_userGuideVisibility_0);
    _jspx_th_c_url_15.setValue("/images/guide-trans-new.png");
    int _jspx_eval_c_url_15 = _jspx_th_c_url_15.doStartTag();
    if (_jspx_th_c_url_15.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_15);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_15);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_10(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_10 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_10.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_10.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_10.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_10 = _jspx_th_sec$1auth_authComponent_10.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_10 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("         <a href=\"#\"><img oncontextmenu=\"alert('Right click is disabled!');return false\" src=\"");
        if (_jspx_meth_c_url_16((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_10, _jspx_page_context))
          return true;
        out.write("\" width=\"45\" height=\"63\" border=\"0\" \r\n");
        out.write("          onclick=\"window.open('");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/resources/help/");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/Help.htm?decorator=popup&popup=true','help','height=800,width=750,top=0, left=610, scrollbars=yes,resizable=yes').focus();\"/></a>\r\n");
        out.write("           ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_10.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_10.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_10);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_10);
    return false;
  }

  private boolean _jspx_meth_c_url_16(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_10, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_16 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_16.setPageContext(_jspx_page_context);
    _jspx_th_c_url_16.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_10);
    _jspx_th_c_url_16.setValue("/images/help-new.png");
    int _jspx_eval_c_url_16 = _jspx_th_c_url_16.doStartTag();
    if (_jspx_th_c_url_16.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_16);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_16);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_11(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_11 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_11.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_11.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_11.setComponentId("module.section.header.topImagesExternal");
    int _jspx_eval_sec$1auth_authComponent_11 = _jspx_th_sec$1auth_authComponent_11.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_11 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("            <a href=\"#\"><img oncontextmenu=\"alert('Right click is disabled!');return false\" src=\"");
        if (_jspx_meth_c_url_17((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_11, _jspx_page_context))
          return true;
        out.write("\" width=\"45\" height=\"63\" border=\"0\" \r\n");
        out.write("           onclick=\"window.open('");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/resources/help/");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/Help.htm?decorator=popup&popup=true','help','height=800,width=750,top=0, left=610, scrollbars=yes,resizable=yes').focus();\"/></a>\r\n");
        out.write("           ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_11.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_11.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_11);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_11);
    return false;
  }

  private boolean _jspx_meth_c_url_17(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_11, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_17 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_17.setPageContext(_jspx_page_context);
    _jspx_th_c_url_17.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_11);
    _jspx_th_c_url_17.setValue("/images/help-new.png");
    int _jspx_eval_c_url_17 = _jspx_th_c_url_17.doStartTag();
    if (_jspx_th_c_url_17.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_17);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_17);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_12(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_12 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_12.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_12.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_sec$1auth_authComponent_12.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_12 = _jspx_th_sec$1auth_authComponent_12.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_12 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <!--<a onclick=\"window.open('editScrachCard.html?decorator=popup&popup=true','clipboard','height=600,width=475,top=0, left=610, scrollbars=no,resizable=yes').focus();\">\r\n");
        out.write("          -->\r\n");
        out.write("          <a href=\"javascript:;\" onclick=\"displayMoodal()\" >\r\n");
        out.write("          <img src=\"");
        if (_jspx_meth_c_url_18((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_12, _jspx_page_context))
          return true;
        out.write("\" width=\"70\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\"/></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_12.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_12.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_12);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_12);
    return false;
  }

  private boolean _jspx_meth_c_url_18(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_18 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_18.setPageContext(_jspx_page_context);
    _jspx_th_c_url_18.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_12);
    _jspx_th_c_url_18.setValue("/images/clipboard-trans-new.png");
    int _jspx_eval_c_url_18 = _jspx_th_c_url_18.doStartTag();
    if (_jspx_th_c_url_18.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_18);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_18);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_13(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_13 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_13.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_13.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_13.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_13 = _jspx_th_sec$1auth_authComponent_13.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_13 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <a href=\"toDos.html?fromUser=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.remoteUser}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\"><img src=\"");
        if (_jspx_meth_c_url_19((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_13, _jspx_page_context))
          return true;
        out.write("\" width=\"94\" height=\"63\" border=\"0\"/></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_13.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_13.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_13);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_13);
    return false;
  }

  private boolean _jspx_meth_c_url_19(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_13, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_19 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_19.setPageContext(_jspx_page_context);
    _jspx_th_c_url_19.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_13);
    _jspx_th_c_url_19.setValue("/images/activity-trans.png");
    int _jspx_eval_c_url_19 = _jspx_th_c_url_19.doStartTag();
    if (_jspx_th_c_url_19.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_19);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_19);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_14(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_14 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_14.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_14.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_14.setComponentId("module.section.header.topActivityMgtForAgent");
    int _jspx_eval_sec$1auth_authComponent_14 = _jspx_th_sec$1auth_authComponent_14.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_14 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("         \t<a href=\"toDos.html?fromUser=");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.remoteUser}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("\"><img src=\"");
        if (_jspx_meth_c_url_20((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_14, _jspx_page_context))
          return true;
        out.write("\" width=\"94\" height=\"63\" border=\"0\"/></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_14.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_14.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_14);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_14);
    return false;
  }

  private boolean _jspx_meth_c_url_20(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_14, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_20 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_20.setPageContext(_jspx_page_context);
    _jspx_th_c_url_20.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_14);
    _jspx_th_c_url_20.setValue("/images/activity-trans.png");
    int _jspx_eval_c_url_20 = _jspx_th_c_url_20.doStartTag();
    if (_jspx_th_c_url_20.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_20);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_20);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_15(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_15 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_15.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_15.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_15.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_15 = _jspx_th_sec$1auth_authComponent_15.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_15 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_13((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_15, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_14((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_15, _jspx_page_context))
          return true;
        out.write(" \r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_15.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_15.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_15);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_15);
    return false;
  }

  private boolean _jspx_meth_c_if_13(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_15, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_13 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_13.setPageContext(_jspx_page_context);
    _jspx_th_c_if_13.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_15);
    _jspx_th_c_if_13.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${fileID == ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_13 = _jspx_th_c_if_13.doStartTag();
    if (_jspx_eval_c_if_13 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t\t<a href=\"#\"><img src=\"");
        if (_jspx_meth_c_url_21((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_13, _jspx_page_context))
          return true;
        out.write("\" width=\"85\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\" onclick=\"message(); return false;\"/></a>\r\n");
        out.write("\t\t\t");
        int evalDoAfterBody = _jspx_th_c_if_13.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_13.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_13);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_13);
    return false;
  }

  private boolean _jspx_meth_c_url_21(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_13, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_21 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_21.setPageContext(_jspx_page_context);
    _jspx_th_c_url_21.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_13);
    _jspx_th_c_url_21.setValue("/images/file-cabinet-trans.png");
    int _jspx_eval_c_url_21 = _jspx_th_c_url_21.doStartTag();
    if (_jspx_th_c_url_21.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_21);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_21);
    return false;
  }

  private boolean _jspx_meth_c_if_14(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_15, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_14 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_14.setPageContext(_jspx_page_context);
    _jspx_th_c_if_14.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_15);
    _jspx_th_c_if_14.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${fileID != ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_14 = _jspx_th_c_if_14.doStartTag();
    if (_jspx_eval_c_if_14 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\t\t          \r\n");
        out.write("\t          <a href=\"#\"><img src=\"");
        if (_jspx_meth_c_url_22((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_14, _jspx_page_context))
          return true;
        out.write("\" width=\"85\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\" onclick=\"message(); return false;\"/></a>\r\n");
        out.write("\t      ");
        int evalDoAfterBody = _jspx_th_c_if_14.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_14.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_14);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_14);
    return false;
  }

  private boolean _jspx_meth_c_url_22(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_14, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_22 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_22.setPageContext(_jspx_page_context);
    _jspx_th_c_url_22.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_14);
    _jspx_th_c_url_22.setValue("/images/file-cabinet-trans.png");
    int _jspx_eval_c_url_22 = _jspx_th_c_url_22.doStartTag();
    if (_jspx_th_c_url_22.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_22);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_22);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_16(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_16 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_16.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_16.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_16.setComponentId("module.section.header.driver");
    int _jspx_eval_sec$1auth_authComponent_16 = _jspx_th_sec$1auth_authComponent_16.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_16 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("             ");
        if (_jspx_meth_c_if_15((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_16, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_16((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_16, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("           ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_16.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_16.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_16);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_16);
    return false;
  }

  private boolean _jspx_meth_c_if_15(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_16, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_15 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_15.setPageContext(_jspx_page_context);
    _jspx_th_c_if_15.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_16);
    _jspx_th_c_if_15.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${fileID == ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_15 = _jspx_th_c_if_15.doStartTag();
    if (_jspx_eval_c_if_15 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("\t\t\t<a href=\"#\"><img src=\"");
        if (_jspx_meth_c_url_23((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_15, _jspx_page_context))
          return true;
        out.write("\" width=\"85\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\" onclick=\"message(); return false;\"/></a>\r\n");
        out.write("\t\t\t");
        int evalDoAfterBody = _jspx_th_c_if_15.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_15.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_15);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_15);
    return false;
  }

  private boolean _jspx_meth_c_url_23(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_15, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_23 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_23.setPageContext(_jspx_page_context);
    _jspx_th_c_url_23.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_15);
    _jspx_th_c_url_23.setValue("/images/file-cabinet-trans.png");
    int _jspx_eval_c_url_23 = _jspx_th_c_url_23.doStartTag();
    if (_jspx_th_c_url_23.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_23);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_23);
    return false;
  }

  private boolean _jspx_meth_c_if_16(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_16, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_16 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_16.setPageContext(_jspx_page_context);
    _jspx_th_c_if_16.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_16);
    _jspx_th_c_if_16.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${fileID != ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_16 = _jspx_th_c_if_16.doStartTag();
    if (_jspx_eval_c_if_16 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\t\t          \r\n");
        out.write("\t          <a onclick=\"message(); return false;\" ><img src=\"");
        if (_jspx_meth_c_url_24((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_16, _jspx_page_context))
          return true;
        out.write("\" width=\"85\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\" /></a>\r\n");
        out.write("\t      ");
        int evalDoAfterBody = _jspx_th_c_if_16.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_16.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_16);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_16);
    return false;
  }

  private boolean _jspx_meth_c_url_24(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_16, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_24 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_24.setPageContext(_jspx_page_context);
    _jspx_th_c_url_24.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_16);
    _jspx_th_c_url_24.setValue("/images/file-cabinet-trans.png");
    int _jspx_eval_c_url_24 = _jspx_th_c_url_24.doStartTag();
    if (_jspx_th_c_url_24.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_24);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_24);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_17(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_17 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_17.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_17.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_17.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_17 = _jspx_th_sec$1auth_authComponent_17.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_17 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("         \t");
        if (_jspx_meth_c_if_17((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_17, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("         \t");
        if (_jspx_meth_c_if_18((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_17, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_17.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_17.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_17);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_17);
    return false;
  }

  private boolean _jspx_meth_c_if_17(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_17, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_17 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_17.setPageContext(_jspx_page_context);
    _jspx_th_c_if_17.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_17);
    _jspx_th_c_if_17.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteID == ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_17 = _jspx_th_c_if_17.doStartTag();
    if (_jspx_eval_c_if_17 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("<img src=\"");
        if (_jspx_meth_c_url_25((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_17, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" />");
        int evalDoAfterBody = _jspx_th_c_if_17.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_17.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_17);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_17);
    return false;
  }

  private boolean _jspx_meth_c_url_25(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_17, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_25 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_25.setPageContext(_jspx_page_context);
    _jspx_th_c_url_25.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_17);
    _jspx_th_c_url_25.setValue("/images/notes-trans-new.png");
    int _jspx_eval_c_url_25 = _jspx_th_c_url_25.doStartTag();
    if (_jspx_th_c_url_25.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_25);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_25);
    return false;
  }

  private boolean _jspx_meth_c_if_18(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_17, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_18 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_18.setPageContext(_jspx_page_context);
    _jspx_th_c_if_18.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_17);
    _jspx_th_c_if_18.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteID != ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_18 = _jspx_th_c_if_18.doStartTag();
    if (_jspx_eval_c_if_18 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("<a onclick=\"openNotes();\"><img src=\"");
        if (_jspx_meth_c_url_26((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_18, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" /></a>");
        int evalDoAfterBody = _jspx_th_c_if_18.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_18.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_18);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_18);
    return false;
  }

  private boolean _jspx_meth_c_url_26(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_18, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_26 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_26.setPageContext(_jspx_page_context);
    _jspx_th_c_url_26.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_18);
    _jspx_th_c_url_26.setValue("/images/notes-trans-new.png");
    int _jspx_eval_c_url_26 = _jspx_th_c_url_26.doStartTag();
    if (_jspx_th_c_url_26.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_26);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_26);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_18(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_18 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_18.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_18.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_18.setComponentId("module.section.header.topImagesExternal");
    int _jspx_eval_sec$1auth_authComponent_18 = _jspx_th_sec$1auth_authComponent_18.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_18 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("         \t");
        if (_jspx_meth_c_if_19((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_18, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("         \t");
        if (_jspx_meth_c_if_20((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_18, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_18.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_18.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_18);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_18);
    return false;
  }

  private boolean _jspx_meth_c_if_19(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_18, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_19 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_19.setPageContext(_jspx_page_context);
    _jspx_th_c_if_19.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_18);
    _jspx_th_c_if_19.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteID == ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_19 = _jspx_th_c_if_19.doStartTag();
    if (_jspx_eval_c_if_19 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("<img src=\"");
        if (_jspx_meth_c_url_27((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_19, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" />");
        int evalDoAfterBody = _jspx_th_c_if_19.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_19.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_19);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_19);
    return false;
  }

  private boolean _jspx_meth_c_url_27(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_19, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_27 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_27.setPageContext(_jspx_page_context);
    _jspx_th_c_url_27.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_19);
    _jspx_th_c_url_27.setValue("/images/notes-trans-new.png");
    int _jspx_eval_c_url_27 = _jspx_th_c_url_27.doStartTag();
    if (_jspx_th_c_url_27.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_27);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_27);
    return false;
  }

  private boolean _jspx_meth_c_if_20(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_18, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_20 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_20.setPageContext(_jspx_page_context);
    _jspx_th_c_if_20.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_18);
    _jspx_th_c_if_20.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${noteID != ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_20 = _jspx_th_c_if_20.doStartTag();
    if (_jspx_eval_c_if_20 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("<a onclick=\"openRelatedNotes();\"><img src=\"");
        if (_jspx_meth_c_url_28((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_20, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" /></a>");
        int evalDoAfterBody = _jspx_th_c_if_20.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_20.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_20);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_20);
    return false;
  }

  private boolean _jspx_meth_c_url_28(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_20, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_28 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_28.setPageContext(_jspx_page_context);
    _jspx_th_c_url_28.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_20);
    _jspx_th_c_url_28.setValue("/images/notes-trans-new.png");
    int _jspx_eval_c_url_28 = _jspx_th_c_url_28.doStartTag();
    if (_jspx_th_c_url_28.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_28);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_28);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_19(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_19 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_19.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_19.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_19.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_19 = _jspx_th_sec$1auth_authComponent_19.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_19 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_21((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_19, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        if (_jspx_meth_c_if_22((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_19, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_19.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_19.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_19);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_19);
    return false;
  }

  private boolean _jspx_meth_c_if_21(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_19, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_21 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_21.setPageContext(_jspx_page_context);
    _jspx_th_c_if_21.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_19);
    _jspx_th_c_if_21.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfTasks!=''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_21 = _jspx_th_c_if_21.doStartTag();
    if (_jspx_eval_c_if_21 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <a onclick=\"openTasks();\"><img src=\"");
        if (_jspx_meth_c_url_29((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_21, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" /></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_c_if_21.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_21.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_21);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_21);
    return false;
  }

  private boolean _jspx_meth_c_url_29(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_21, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_29 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_29.setPageContext(_jspx_page_context);
    _jspx_th_c_url_29.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_21);
    _jspx_th_c_url_29.setValue("/images/task-trans-new.png");
    int _jspx_eval_c_url_29 = _jspx_th_c_url_29.doStartTag();
    if (_jspx_th_c_url_29.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_29);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_29);
    return false;
  }

  private boolean _jspx_meth_c_if_22(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_19, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_22 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_22.setPageContext(_jspx_page_context);
    _jspx_th_c_if_22.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_19);
    _jspx_th_c_if_22.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${idOfTasks== ''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_22 = _jspx_th_c_if_22.doStartTag();
    if (_jspx_eval_c_if_22 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <img src=\"");
        if (_jspx_meth_c_url_30((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_22, _jspx_page_context))
          return true;
        out.write("\" width=\"55\" height=\"63\" border=\"0\" />\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_c_if_22.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_22.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_22);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_22);
    return false;
  }

  private boolean _jspx_meth_c_url_30(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_22, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_30 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_30.setPageContext(_jspx_page_context);
    _jspx_th_c_url_30.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_22);
    _jspx_th_c_url_30.setValue("/images/task-trans-new.png");
    int _jspx_eval_c_url_30 = _jspx_th_c_url_30.doStartTag();
    if (_jspx_th_c_url_30.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_30);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_30);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_20(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_20 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_20.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_20.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_20.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_20 = _jspx_th_sec$1auth_authComponent_20.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_20 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("         \r\n");
        out.write("          <a onclick=\"window.open('surveysList.html?decorator=popup&popup=true&from=survey','surveysList','height=600,width=875,top=20, left=60, scrollbars=yes,resizable=yes').focus();\" />\r\n");
        out.write("          <img src=\"");
        if (_jspx_meth_c_url_31((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_20, _jspx_page_context))
          return true;
        out.write("\" width=\"70\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\"/></a>\r\n");
        out.write("          \r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_20.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_20.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_20);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_20);
    return false;
  }

  private boolean _jspx_meth_c_url_31(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_20, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_31 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_31.setPageContext(_jspx_page_context);
    _jspx_th_c_url_31.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_20);
    _jspx_th_c_url_31.setValue("/images/survery-new.png");
    int _jspx_eval_c_url_31 = _jspx_th_c_url_31.doStartTag();
    if (_jspx_th_c_url_31.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_31);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_31);
    return false;
  }

  private boolean _jspx_meth_configByCorp_fieldVisibility_1(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  configByCorp:fieldVisibility
    com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag _jspx_th_configByCorp_fieldVisibility_1 = (com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag) _jspx_tagPool_configByCorp_fieldVisibility_componentId.get(com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag.class);
    _jspx_th_configByCorp_fieldVisibility_1.setPageContext(_jspx_page_context);
    _jspx_th_configByCorp_fieldVisibility_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_configByCorp_fieldVisibility_1.setComponentId("component.tab.header.operations");
    int _jspx_eval_configByCorp_fieldVisibility_1 = _jspx_th_configByCorp_fieldVisibility_1.doStartTag();
    if (_jspx_eval_configByCorp_fieldVisibility_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          \t<td width=\"8%\">\r\n");
        out.write("          \t\t");
        if (_jspx_meth_sec$1auth_authComponent_21((javax.servlet.jsp.tagext.JspTag) _jspx_th_configByCorp_fieldVisibility_1, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("        \t</td>       \r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_configByCorp_fieldVisibility_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_configByCorp_fieldVisibility_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_1);
      return true;
    }
    _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_1);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_21(javax.servlet.jsp.tagext.JspTag _jspx_th_configByCorp_fieldVisibility_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_21 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_21.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_21.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_configByCorp_fieldVisibility_1);
    _jspx_th_sec$1auth_authComponent_21.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_21 = _jspx_th_sec$1auth_authComponent_21.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_21 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("          \r\n");
        out.write("\t          \t\t<a onclick=\"defaultOpsCalendar();\" />\r\n");
        out.write("\t          \t\t\t<img src=\"");
        if (_jspx_meth_c_url_32((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_21, _jspx_page_context))
          return true;
        out.write("\" width=\"82\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false;\"/>\r\n");
        out.write("\t          \t\t</a>\r\n");
        out.write("\t         \t");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_21.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_21.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_21);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_21);
    return false;
  }

  private boolean _jspx_meth_c_url_32(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_21, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_32 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_32.setPageContext(_jspx_page_context);
    _jspx_th_c_url_32.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_21);
    _jspx_th_c_url_32.setValue("/images/ops-calendar.png");
    int _jspx_eval_c_url_32 = _jspx_th_c_url_32.doStartTag();
    if (_jspx_th_c_url_32.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_32);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_32);
    return false;
  }

  private boolean _jspx_meth_c_url_33(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_33 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_33.setPageContext(_jspx_page_context);
    _jspx_th_c_url_33.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_c_url_33.setValue("/images/myaccount-new.png");
    int _jspx_eval_c_url_33 = _jspx_th_c_url_33.doStartTag();
    if (_jspx_th_c_url_33.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_33);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_33);
    return false;
  }

  private boolean _jspx_meth_c_if_24(javax.servlet.jsp.tagext.JspTag _jspx_th_configByCorp_userGuideVisibility_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_24 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_24.setPageContext(_jspx_page_context);
    _jspx_th_c_if_24.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_configByCorp_userGuideVisibility_1);
    _jspx_th_c_if_24.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType == 'USER'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_24 = _jspx_th_c_if_24.doStartTag();
    if (_jspx_eval_c_if_24 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("            <a onclick=\"window.open('salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=ALL/ General','guideList','height=600,width=995,top=20, left=60, scrollbars=yes,resizable=yes').focus();\" />\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_c_if_24.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_24.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_24);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_24);
    return false;
  }

  private boolean _jspx_meth_c_if_25(javax.servlet.jsp.tagext.JspTag _jspx_th_configByCorp_userGuideVisibility_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_25 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_25.setPageContext(_jspx_page_context);
    _jspx_th_c_if_25.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_configByCorp_userGuideVisibility_1);
    _jspx_th_c_if_25.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType != 'USER'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_25 = _jspx_th_c_if_25.doStartTag();
    if (_jspx_eval_c_if_25 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("           <a onclick=\"window.open('salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=Portals','guideList','height=600,width=995,top=20, left=60, scrollbars=yes,resizable=yes').focus();\" />\r\n");
        out.write("     \t");
        int evalDoAfterBody = _jspx_th_c_if_25.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_25.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_25);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_25);
    return false;
  }

  private boolean _jspx_meth_c_url_34(javax.servlet.jsp.tagext.JspTag _jspx_th_configByCorp_userGuideVisibility_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_34 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_34.setPageContext(_jspx_page_context);
    _jspx_th_c_url_34.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_configByCorp_userGuideVisibility_1);
    _jspx_th_c_url_34.setValue("/images/guide-trans-new.png");
    int _jspx_eval_c_url_34 = _jspx_th_c_url_34.doStartTag();
    if (_jspx_th_c_url_34.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_34);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_34);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_22(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_22 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_22.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_22.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_22.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_22 = _jspx_th_sec$1auth_authComponent_22.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_22 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <a href=\"#\">\r\n");
        out.write("         <img oncontextmenu=\"alert('Right click is disabled!');return false\" src=\"");
        if (_jspx_meth_c_url_35((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_22, _jspx_page_context))
          return true;
        out.write("\" width=\"45\" height=\"63\" border=\"0\" \r\n");
        out.write("          onclick=\"window.open('");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/resources/help/");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/Help.htm?decorator=popup&popup=true','help','height=800,width=750,top=0, left=610, scrollbars=no,resizable=yes').focus();\"/></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_22.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_22.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_22);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_22);
    return false;
  }

  private boolean _jspx_meth_c_url_35(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_22, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_35 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_35.setPageContext(_jspx_page_context);
    _jspx_th_c_url_35.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_22);
    _jspx_th_c_url_35.setValue("/images/help-new.png");
    int _jspx_eval_c_url_35 = _jspx_th_c_url_35.doStartTag();
    if (_jspx_th_c_url_35.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_35);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_35);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_23(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_23 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_23.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_23.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_23.setComponentId("module.section.header.topImagesExternal");
    int _jspx_eval_sec$1auth_authComponent_23 = _jspx_th_sec$1auth_authComponent_23.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_23 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <a href=\"#\">\r\n");
        out.write("         <img oncontextmenu=\"alert('Right click is disabled!');return false\" src=\"");
        if (_jspx_meth_c_url_36((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_23, _jspx_page_context))
          return true;
        out.write("\" width=\"45\" height=\"63\" border=\"0\" \r\n");
        out.write("          onclick=\"window.open('");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/resources/help/");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/Help.htm?decorator=popup&popup=true','help','height=800,width=750,top=0, left=610, scrollbars=no,resizable=yes').focus();\"/></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_23.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_23.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_23);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_23);
    return false;
  }

  private boolean _jspx_meth_c_url_36(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_23, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_36 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_36.setPageContext(_jspx_page_context);
    _jspx_th_c_url_36.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_23);
    _jspx_th_c_url_36.setValue("/images/help-new.png");
    int _jspx_eval_c_url_36 = _jspx_th_c_url_36.doStartTag();
    if (_jspx_th_c_url_36.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_36);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_36);
    return false;
  }

  private boolean _jspx_meth_sec$1auth_authComponent_24(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_12, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sec-auth:authComponent
    com.trilasoft.app.webapp.tags.ControlAccessByRoleTag _jspx_th_sec$1auth_authComponent_24 = (com.trilasoft.app.webapp.tags.ControlAccessByRoleTag) _jspx_tagPool_sec$1auth_authComponent_componentId.get(com.trilasoft.app.webapp.tags.ControlAccessByRoleTag.class);
    _jspx_th_sec$1auth_authComponent_24.setPageContext(_jspx_page_context);
    _jspx_th_sec$1auth_authComponent_24.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_12);
    _jspx_th_sec$1auth_authComponent_24.setComponentId("module.section.header.topImages");
    int _jspx_eval_sec$1auth_authComponent_24 = _jspx_th_sec$1auth_authComponent_24.doStartTag();
    if (_jspx_eval_sec$1auth_authComponent_24 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <!--<a onclick=\"window.open('editScrachCard.html?decorator=popup&popup=true','clipboard','height=600,width=475,top=0, left=610, scrollbars=no,resizable=yes').focus();\">\r\n");
        out.write("          --><a href=\"javascript:;\" onclick=\"displayMoodal()\" >\r\n");
        out.write("          <img src=\"");
        if (_jspx_meth_c_url_37((javax.servlet.jsp.tagext.JspTag) _jspx_th_sec$1auth_authComponent_24, _jspx_page_context))
          return true;
        out.write("\" width=\"70\" height=\"63\" border=\"0\" oncontextmenu=\"alert('Right click is disabled!');return false\"/></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_sec$1auth_authComponent_24.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_sec$1auth_authComponent_24.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_24);
      return true;
    }
    _jspx_tagPool_sec$1auth_authComponent_componentId.reuse(_jspx_th_sec$1auth_authComponent_24);
    return false;
  }

  private boolean _jspx_meth_c_url_37(javax.servlet.jsp.tagext.JspTag _jspx_th_sec$1auth_authComponent_24, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_37 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_37.setPageContext(_jspx_page_context);
    _jspx_th_c_url_37.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec$1auth_authComponent_24);
    _jspx_th_c_url_37.setValue("/images/clipboard-trans-new.png");
    int _jspx_eval_c_url_37 = _jspx_th_c_url_37.doStartTag();
    if (_jspx_th_c_url_37.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_37);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_37);
    return false;
  }

  private boolean _jspx_meth_c_if_26(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_26 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_26.setPageContext(_jspx_page_context);
    _jspx_th_c_if_26.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_1);
    _jspx_th_c_if_26.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType!='ACCOUNT'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_26 = _jspx_th_c_if_26.doStartTag();
    if (_jspx_eval_c_if_26 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <a onclick=\"logOut();\"><img src=\"");
        if (_jspx_meth_c_url_38((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_26, _jspx_page_context))
          return true;
        out.write("\" width=\"57\" height=\"63\" border=\"0\" /></a>\r\n");
        out.write("          ");
        int evalDoAfterBody = _jspx_th_c_if_26.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_26.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_26);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_26);
    return false;
  }

  private boolean _jspx_meth_c_url_38(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_26, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_38 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_38.setPageContext(_jspx_page_context);
    _jspx_th_c_url_38.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_26);
    _jspx_th_c_url_38.setValue("/images/logoff-trans-new.png");
    int _jspx_eval_c_url_38 = _jspx_th_c_url_38.doStartTag();
    if (_jspx_th_c_url_38.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_38);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_38);
    return false;
  }

  private boolean _jspx_meth_c_if_27(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_27 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_27.setPageContext(_jspx_page_context);
    _jspx_th_c_if_27.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_1);
    _jspx_th_c_if_27.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${userType=='ACCOUNT'}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_27 = _jspx_th_c_if_27.doStartTag();
    if (_jspx_eval_c_if_27 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("          <a onclick=\"accountUserlogOut();\"><img src=\"");
        if (_jspx_meth_c_url_39((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_27, _jspx_page_context))
          return true;
        out.write("\" width=\"57\" height=\"63\" border=\"0\" /></a>\r\n");
        out.write("           ");
        int evalDoAfterBody = _jspx_th_c_if_27.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_27.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_27);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_27);
    return false;
  }

  private boolean _jspx_meth_c_url_39(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_27, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_39 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_39.setPageContext(_jspx_page_context);
    _jspx_th_c_url_39.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_27);
    _jspx_th_c_url_39.setValue("/images/logoff-trans-new.png");
    int _jspx_eval_c_url_39 = _jspx_th_c_url_39.doStartTag();
    if (_jspx_th_c_url_39.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_39);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_39);
    return false;
  }

  private boolean _jspx_meth_configByCorp_fieldVisibility_2(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_28, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  configByCorp:fieldVisibility
    com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag _jspx_th_configByCorp_fieldVisibility_2 = (com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag) _jspx_tagPool_configByCorp_fieldVisibility_componentId.get(com.trilasoft.app.webapp.tags.ControlVisibilityByCorpTag.class);
    _jspx_th_configByCorp_fieldVisibility_2.setPageContext(_jspx_page_context);
    _jspx_th_configByCorp_fieldVisibility_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_28);
    _jspx_th_configByCorp_fieldVisibility_2.setComponentId("component.standard.claimTab");
    int _jspx_eval_configByCorp_fieldVisibility_2 = _jspx_th_configByCorp_fieldVisibility_2.doStartTag();
    if (_jspx_eval_configByCorp_fieldVisibility_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("<li>job#<b>cl</b> -> Claims page</li>\r\n");
        out.write("\t");
        int evalDoAfterBody = _jspx_th_configByCorp_fieldVisibility_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_configByCorp_fieldVisibility_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_2);
      return true;
    }
    _jspx_tagPool_configByCorp_fieldVisibility_componentId.reuse(_jspx_th_configByCorp_fieldVisibility_2);
    return false;
  }

  private boolean _jspx_meth_c_remove_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:remove
    org.apache.taglibs.standard.tag.common.core.RemoveTag _jspx_th_c_remove_0 = (org.apache.taglibs.standard.tag.common.core.RemoveTag) _jspx_tagPool_c_remove_var_scope_nobody.get(org.apache.taglibs.standard.tag.common.core.RemoveTag.class);
    _jspx_th_c_remove_0.setPageContext(_jspx_page_context);
    _jspx_th_c_remove_0.setParent(null);
    _jspx_th_c_remove_0.setVar("idOfTasks");
    _jspx_th_c_remove_0.setScope("session");
    int _jspx_eval_c_remove_0 = _jspx_th_c_remove_0.doStartTag();
    if (_jspx_th_c_remove_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_remove_var_scope_nobody.reuse(_jspx_th_c_remove_0);
      return true;
    }
    _jspx_tagPool_c_remove_var_scope_nobody.reuse(_jspx_th_c_remove_0);
    return false;
  }

  private boolean _jspx_meth_c_remove_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:remove
    org.apache.taglibs.standard.tag.common.core.RemoveTag _jspx_th_c_remove_1 = (org.apache.taglibs.standard.tag.common.core.RemoveTag) _jspx_tagPool_c_remove_var_scope_nobody.get(org.apache.taglibs.standard.tag.common.core.RemoveTag.class);
    _jspx_th_c_remove_1.setPageContext(_jspx_page_context);
    _jspx_th_c_remove_1.setParent(null);
    _jspx_th_c_remove_1.setVar("tableName");
    _jspx_th_c_remove_1.setScope("session");
    int _jspx_eval_c_remove_1 = _jspx_th_c_remove_1.doStartTag();
    if (_jspx_th_c_remove_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_remove_var_scope_nobody.reuse(_jspx_th_c_remove_1);
      return true;
    }
    _jspx_tagPool_c_remove_var_scope_nobody.reuse(_jspx_th_c_remove_1);
    return false;
  }
}
