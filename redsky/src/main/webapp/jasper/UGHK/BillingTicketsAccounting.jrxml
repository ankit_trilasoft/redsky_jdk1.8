<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="BillingTicketsAccounting" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="782" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="5fe99cc8-754a-400a-a2c3-75c72fa08d95">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="begindate" class="java.util.Date">
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="enddate" class="java.util.Date">
		<defaultValueExpression><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="Corporate ID" class="java.lang.String" isForPrompting="false"/>
	<queryString>
		<![CDATA[SELECT
    'BillingTicketsAccounting.jrxml',
     serviceorder.`corpID` AS serviceorder_corpID,
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     serviceorder.`status` AS serviceorder_status,
     serviceorder.`routing` AS serviceorder_routing,
     serviceorder.`packingMode` AS serviceorder_packingMode,
     serviceorder.`mode` AS serviceorder_mode,
     serviceorder.`lastName` AS serviceorder_lastName,
     serviceorder.`job` AS serviceorder_job,
     serviceorder.`firstName` AS serviceorder_firstName,
     serviceorder.`email2` AS serviceorder_email2,
     serviceorder.`email` AS serviceorder_email,
     serviceorder.`registrationNumber` AS serviceorder_registrationNumber,
     workticket.`date1` AS workticket_date1,
     workticket.`date2` AS workticket_date2,
     workticket.`instructions` AS workticket_instructions,
     workticket.`service` AS workticket_service,
     workticket.`ticket` AS workticket_ticket,
     workticket.`targetActual` AS workticket_targetActual,
     workticket.`warehouse` AS workticket_warehouse,
     workticket.`zip` AS workticket_zip,
     workticket.`coordinator` AS workticket_coordinator,
     serviceorder.`coordinator` AS serviceorder_coordinator,
     CONCAT(serviceorder.`lastName`," , ",serviceorder.`firstName`)AS Shipper,
     billing.`shipNumber` AS billing_shipNumber,
     billing.`billToCode` AS billing_billToCode,
     billing.`billtoname` AS billing_billtoname,
     workticket.`jobType` AS workticket_jobType,
     billing.`id` AS billing_id,
     serviceorder.`id` AS serviceorder_id,
     workticket.`serviceOrderId` AS workticket_serviceOrderId
FROM
     `billing` billing INNER JOIN `serviceorder` serviceorder ON billing.`id` = serviceorder.`id` AND billing.`corpID` = $P{Corporate ID}
     LEFT OUTER JOIN `workticket` workticket ON serviceorder.`id` = workticket.`serviceOrderId` AND workticket.`corpID` = $P{Corporate ID}
WHERE
 serviceorder.`corpID` = $P{Corporate ID}
 AND workticket.`date1` >= $P{begindate}
 AND workticket.`date2` <= $P{enddate}
 AND workticket.`jobType` IN ("STO","DOM","UVL","MVL","LOC","INF","LOF","STF","MLL","ULL")
 AND billing.`billToCode` NOT IN ("500270","500135","500130")
 AND workticket.`service`<> "WW"
 AND workticket.`companydivision` not like 'D%']]>
	</queryString>
	<field name="BillingTicketsAccounting.jrxml" class="java.lang.String"/>
	<field name="serviceorder_corpID" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_status" class="java.lang.String"/>
	<field name="serviceorder_routing" class="java.lang.String"/>
	<field name="serviceorder_packingMode" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="serviceorder_email2" class="java.lang.String"/>
	<field name="serviceorder_email" class="java.lang.String"/>
	<field name="serviceorder_registrationNumber" class="java.lang.String"/>
	<field name="workticket_date1" class="java.sql.Timestamp"/>
	<field name="workticket_date2" class="java.sql.Timestamp"/>
	<field name="workticket_instructions" class="java.lang.String"/>
	<field name="workticket_service" class="java.lang.String"/>
	<field name="workticket_ticket" class="java.lang.Long"/>
	<field name="workticket_targetActual" class="java.lang.String"/>
	<field name="workticket_warehouse" class="java.lang.String"/>
	<field name="workticket_zip" class="java.lang.String"/>
	<field name="workticket_coordinator" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="billing_shipNumber" class="java.lang.String"/>
	<field name="billing_billToCode" class="java.lang.String"/>
	<field name="billing_billtoname" class="java.lang.String"/>
	<field name="workticket_jobType" class="java.lang.String"/>
	<field name="billing_id" class="java.lang.Long"/>
	<field name="serviceorder_id" class="java.lang.Long"/>
	<field name="workticket_serviceOrderId" class="java.lang.Long"/>
	<sortField name="billing_billToCode"/>
	<sortField name="workticket_date1"/>
	<sortField name="serviceorder_lastName"/>
	<group name="billto">
		<groupExpression><![CDATA[$F{billing_billToCode}]]></groupExpression>
		<groupHeader>
			<band height="50" splitType="Stretch">
				<textField isBlankWhenNull="true">
					<reportElement key="textField" x="69" y="10" width="70" height="18" uuid="6abcf151-f9f6-4eab-9e91-b3cb2a1edaf5"/>
					<textElement>
						<font size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{billing_billToCode}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement key="textField" x="150" y="10" width="629" height="18" uuid="8f2506d8-3132-4bef-bcc2-57edb7c54975"/>
					<textElement>
						<font size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{billing_billtoname}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-21" x="-1" y="10" width="55" height="17" uuid="e506fb4c-3e1c-4671-a7c7-9fa6ea4e3c01"/>
					<textElement>
						<font size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Bill To :]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="13" splitType="Stretch">
				<break>
					<reportElement key="element-1" x="0" y="9" width="782" height="1" uuid="c12ab42c-2586-4233-82fa-9a7e68efa88b"/>
				</break>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="80" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="185" y="0" width="412" height="23" forecolor="#663300" uuid="ca008c16-b497-4849-acc6-ef390c6be1c2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font size="18" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Workticket by Ticket Service Date]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement key="textField-2" x="411" y="23" width="107" height="20" forecolor="#660000" uuid="14206093-4ed8-4561-b34b-f6c2a013e925"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true" isItalic="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{enddate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement key="textField-3" x="268" y="25" width="98" height="18" forecolor="#660000" uuid="ee6af1ad-8284-403d-8e2c-ecb6809a6a88"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true" isItalic="true" pdfFontName="Helvetica-BoldOblique"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{begindate}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-19" x="365" y="23" width="36" height="21" forecolor="#330000" uuid="cc3769cd-20d2-47e3-960c-7aea8a65b057"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[  to]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-23" x="711" y="61" width="71" height="18" uuid="fc97f3f6-d242-49f3-8670-8949a089bc1c"/>
				<textElement>
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[REG#]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-24" x="642" y="61" width="50" height="18" uuid="12d4a907-ea80-4a43-a7d0-828558d4e0b6"/>
				<textElement>
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Tkt#]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-25" x="531" y="61" width="50" height="18" uuid="53c2dcdc-5975-4149-8142-2dbce0b4c199"/>
				<textElement>
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Shipnum]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-26" x="496" y="61" width="31" height="18" uuid="14f92c38-ec34-4eec-8a6f-2f22d33830be"/>
				<textElement>
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[WH]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-27" x="365" y="61" width="85" height="18" uuid="0666bbfa-e141-4d47-b6ba-cb870e673fe9"/>
				<textElement>
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Instructions]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-28" x="322" y="61" width="34" height="18" uuid="0d17a6ab-8fe9-408e-a6b2-2fe96d79d3f3"/>
				<textElement textAlignment="Right">
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Svc]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-29" x="287" y="61" width="33" height="18" uuid="e751b3e3-4d6f-4d63-b09b-ffc037029612"/>
				<textElement>
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Mode]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-30" x="245" y="61" width="34" height="18" uuid="ad29ca1d-8d7a-446f-906e-14180d5a82b5"/>
				<textElement>
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Type]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-31" x="118" y="61" width="72" height="18" uuid="c338ffcf-0c40-4e0e-b47d-d165c77d9a2e"/>
				<textElement>
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-32" x="8" y="61" width="106" height="18" uuid="10617c6c-b3c8-4dc6-a5fd-28c0475d0460"/>
				<textElement textAlignment="Center">
					<font isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Ticket Date]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="21" splitType="Stretch">
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="0" width="57" height="18" uuid="21988461-6324-41dc-9b0b-89a22e1754b3"/>
				<textFieldExpression><![CDATA[$F{workticket_date1}]]></textFieldExpression>
			</textField>
			<textField pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement key="textField" x="72" y="0" width="57" height="18" uuid="dc8b9f82-5d4d-4cb1-8179-7f5a20f4f3cb"/>
				<textFieldExpression><![CDATA[$F{workticket_date2}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-22" x="58" y="0" width="10" height="16" uuid="411d53d2-bb3d-4a1d-8d19-f9ffc57b171d"/>
				<text><![CDATA[ - ]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="255" y="0" width="29" height="18" uuid="f1516ef2-c486-441e-b5b6-0718d7600d1f"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="290" y="0" width="33" height="18" uuid="bd153d07-20ba-41e0-8239-f2cfb937e08a"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_mode}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="329" y="0" width="27" height="18" uuid="14bf6a29-8be8-45b4-966d-f0ecb28bf74f"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_service}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="360" y="0" width="130" height="18" uuid="d8d8eff4-cde5-4d78-a80b-dd6f6261b712"/>
				<textElement>
					<font isItalic="true" pdfFontName="Helvetica-Oblique"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{workticket_instructions}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="711" y="0" width="68" height="18" uuid="0810a6be-7618-4384-828f-d3b2d92b8e37"/>
				<textElement>
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_registrationNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="642" y="0" width="64" height="18" uuid="8f4139cb-990c-420f-a34d-4f9f08542ef9"/>
				<textFieldExpression><![CDATA[$F{workticket_ticket}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="532" y="0" width="102" height="18" uuid="380bda35-febe-42e2-892d-4508bc678a05"/>
				<textFieldExpression><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="497" y="0" width="29" height="18" uuid="7e694e15-f3b9-4a62-b694-ccefa07bf6db"/>
				<textFieldExpression><![CDATA[$F{workticket_warehouse}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="133" y="0" width="117" height="18" uuid="94203837-b58d-432c-b622-2296e8f257a9"/>
				<textElement>
					<font size="10" isItalic="true" pdfFontName="Helvetica-Oblique"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="27" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="574" y="8" width="170" height="19" forecolor="#000000" backcolor="#FFFFFF" uuid="04623b27-12e3-4bc2-ba38-49c8ffaebf9e"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="746" y="8" width="36" height="19" forecolor="#000000" backcolor="#FFFFFF" uuid="40c7d602-4ec0-4959-b9f3-ceae00d4aba8"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy" isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="8" width="209" height="19" forecolor="#000000" backcolor="#FFFFFF" uuid="cb766c04-d5d9-42b5-b83c-217eebeddbec"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-1" x="0" y="6" width="782" height="1" forecolor="#00FFCC" uuid="90bb4ce9-7719-4b69-bff9-06ced2738e9e"/>
				<graphicElement>
					<pen lineWidth="2.0" lineStyle="Solid"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement key="staticText-20" x="278" y="8" width="227" height="17" uuid="328a9a15-2eea-4d48-aaf7-dc16c41af47c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="Arial"/>
				</textElement>
				<text><![CDATA[BillingTicketsAccounting]]></text>
			</staticText>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
