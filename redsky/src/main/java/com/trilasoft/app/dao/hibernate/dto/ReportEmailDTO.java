package com.trilasoft.app.dao.hibernate.dto;

public class ReportEmailDTO
{
	private Object id;
    private Object email;
  	private Object firstName;  	
  	private Object lastName;
  	private Object userName;
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public Object getEmail() {
		return email;
	}
	public void setEmail(Object email) {
		this.email = email;
	}
	public Object getFirstName() {
		return firstName;
	}
	public void setFirstName(Object firstName) {
		this.firstName = firstName;
	}
	public Object getLastName() {
		return lastName;
	}
	public void setLastName(Object lastName) {
		this.lastName = lastName;
	}
	public Object getUserName() {
		return userName;
	}
	public void setUserName(Object userName) {
		this.userName = userName;
	}
  	
}