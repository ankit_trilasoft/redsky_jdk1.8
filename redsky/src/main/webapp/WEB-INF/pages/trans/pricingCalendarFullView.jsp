<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<html>
<head>
<title></title>
<meta name="heading" content="Pricing Calendar Full View" />
</head>
<s:form id="pricingCalendarFullView" name="pricingCalendarFullView" action="">
<table>
<tr><td>2 Men rate</td><td>${men2}</td></tr>
<tr><td>3 Men rate</td><td>${men3}</td></tr>
<tr><td>4 Men rate</td><td>${men4}</td></tr>
<tr><td>Extra Men rate</td><td>${extraMen}</td></tr>
</table>

</s:form>