<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %> 

<head> 

</head> 
<script language="javascript" type="text/javascript">
<%@ include file="/scripts/progressbar.js"%>
</script>
<s:form id="publishContractProgressBar" name="publishContractProgressBar" action="" method="post" validate="true">   
 <div id="Layer1" align="left" style="width:100%; margin: -5px; padding: 0px; background-color:#678EB6; background-image: url('images/bg_storage.png'); size:10px; background-repeat: no-repeat; ">
<table class='searchLabel' cellspacing="2" cellpadding="4" border="0" align="center">

<tbody>
<div id="progressBarDiv">
	<c:if test="${not empty presentCount}" >
      <tr>
      <td align="left" style="padding-left: 30px;" class="listwhitetext"><h1><b>${presentCount}/${totalCount} of ${agentCorpIdName} has been processed</b></h1></td>
    </tr>
      
      <tr>
      <td align="left" style="padding-left: 100px;">	
	<script type="text/javascript">
		var myProgBar = new progressBar(
			1,         //border thickness
			'#000000', //border colour
			'#f8f8ff', //background colour
			'#0000cd', //bar colour
			340,       //width of bar (excluding border)
			20,        //height of bar (excluding border)
			1          //direction of progress: 1 = right, 2 = down, 3 = left, 4 = up
		);
	</script>
	<script type="text/javascript">
		var presentCount = '${presentCount}';
		var totalCount = '${totalCount}';
		var finalPercentage = presentCount/totalCount;
		myProgBar.setBar(finalPercentage);       //set the progress bar to indicate a progress of 50%
		myProgBar.setCol('#023A73'); //change the colour of the progress bar
	</script>
	
      </td>
     </tr>
     </c:if> 
</div>
<TR>
     	<td align="right" style="padding-right: 10px; padding-bottom: 5px;">
     		<a href="javascript:window.close()">Close</a>
     	</td>
     </TR>
</tbody></table>
</div>      
</s:form>

<script type="text/javascript">
try{
	setTimeout("location.reload();",10000);
}catch(e){}
</script>