<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Operational Management Control</title> 
<meta name="heading" content="Operational Management Control"/>
<script type="text/javascript">
	
</script>
<style type="text/css">
.text-area {
border:1px solid #219DD1;
}
</style>
<script language="JavaScript">
	
	function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==36); 
	} 
	
	function validate(){
		<c:if test="${hubWarehouseLimit=='Tkt'}">
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimit'].value==''){
			alert('Please enter the default daily operational limit.');
			return false;
		}
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstWt'].value==''){
			alert('Please enter the default Weight Limit.');
			return false;
		}
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstVol'].value==''){
			alert('Please enter the default Volume Limit.');
			return false;
		}
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimitPC'].value==''){
			alert('Please enter the warning threshold for daily limit.');
			return false;
		}
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimitPC'].value > 100){
			document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimitPC'].value = '';
			alert('Warning threshold % should be within 100.');
			return false;
		}
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.minServiceDayHub'].value==''){
			alert('Please enter the minimum service days.');
			return false;
		}
		</c:if>
		<c:if test="${hubWarehouseLimit=='Crw'}">
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyCrewCapacityLimit'].value==''){
			alert('Please enter the Crew capacity limit.');
			return false;
		}
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyCrewThreshHoldLimit'].value==''){
			alert('Please enter the warning threshold limit for Crew.');
			return false;
		}
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyCrewThreshHoldLimit'].value > 100){
			document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyCrewThreshHoldLimit'].value = '';
			alert('Warning threshold % should be within 100.');
			return false;
		}
		if(document.forms['dailyOpsForm'].elements['operationsHubLimits.minServiceDayHub'].value==''){
			alert('Please enter the minimum service days.');
			return false;
		}
		</c:if>
	}

	var count=0;
	var shiftCount=0;
	function onlyDoubleValue(evt,targetElement){
	  var keyCode = evt.keyCode || evt.which;
	  targetVal=targetElement.value;
	  if(keyCode==16){
	  shiftCount++;
	  return true;
	  }
	  if(targetVal.indexOf('.')>-1){
	   
	  }
	  else{
	  count=0;
	  }
	  if(((keyCode==110)||((keyCode==190)))  && (count==0) && (targetVal!='')){
	   count++;
	   return true;
	  }
	  else if((keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  (keyCode==35) || (keyCode==36)){
	    return true;
	  }
	  else if(((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) && shiftCount ==0){
	  return true;
	  }
	  else{
	   return false;
	  }
	}

	function shiftValueUpdate(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	if(keyCode==16){
	shiftCount=0;
	}
	}
	function onlyNumberValue(evt){
		  var keyCode = evt.keyCode || evt.which;
		  if(keyCode==16){
		  shiftCount++;
		  return true;
		  }	  
		  if((keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  (keyCode==35) || (keyCode==36)){
		    return true;
		  }
		  else if(((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105)) && shiftCount ==0){
		  return true;
		  }
		  else{
		   return false;
		  }
		}

	function dotNotAllowed(target,fieldName){
		var dotValue=target.value;
		if(dotValue=='.' && dotValue.length==1 ){
			alert("Please enter valid Weight or Volume.");
			document.forms['dailyOpsForm'].elements[fieldName].value="";
			document.forms['dailyOpsForm'].elements[fieldName].value.focus();
			return false;
		    }
	     }
	function getValueForUnassignedWH(){
		var wareHouse=document.forms['dailyOpsForm'].elements['operationsHubLimits.hubID'].value;
		if(wareHouse=='0'){
		var url="getValueForUnassignedWHAjax.html?ajax=1&decorator=simple&popup=true&wareHouse="+encodeURI(wareHouse);
		http55555.open("GET", url, true);
	    http55555.onreadystatechange = handleHttpResponse55555;
	    http55555.send(null);
		}else{
			   document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimit'].value='';
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimitPC'].value='';
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.minServiceDayHub'].value='';
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstWt'].value='';
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstVol'].value='';

		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimit'].readOnly =false;
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimitPC'].readOnly =false;
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.minServiceDayHub'].readOnly =false;
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstWt'].readOnly =false;
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstVol'].readOnly =false;
		}
	}
	function handleHttpResponse55555(){
	    if (http55555.readyState == 4){        
	       var selected = http55555.responseText
	       selected = selected.trim();
	       selected = selected.replace("[",'');
	       selected = selected.replace("]",'');
	       if(selected!=""){
	       var sel = selected.split("~"); 
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimit'].value=sel[0];
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimitPC'].value=sel[1];
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.minServiceDayHub'].value=sel[2];
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstWt'].value=sel[3];
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstVol'].value=sel[4];

		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimit'].readOnly =true;
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimitPC'].readOnly =true;
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.minServiceDayHub'].readOnly =true;
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstWt'].readOnly =true;
		       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstVol'].readOnly =true;
	       }
	    }
	}
	var http55555 = getHTTPObject55555();
	function getHTTPObject55555()
	{
	var xmlhttp;
	if(window.XMLHttpRequest)
	{
	    xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)
	{
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	    if (!xmlhttp)
	    {
	        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	    }
	}
	return xmlhttp;
	}
</script>
</head>
	
<s:form cssClass="form_magn" id="dailyOpsForm" name="dailyOpsForm" action="saveHubLimit.html" method="post" validate="true">
<s:hidden name="operationsHubLimits.id"/>
<s:hidden name="dailyOpHubLimitValue" value="%{operationsHubLimits.dailyOpHubLimit}"/>
<s:hidden name="dailyMaxEstWtValue" value="%{operationsHubLimits.dailyMaxEstWt}"/>
<s:hidden name="dailyMaxEstVolValue" value="%{operationsHubLimits.dailyMaxEstVol}"/>
<s:hidden name="dailyDefLimitValue" value="%{operationsHubLimits.dailyOpHubLimitPC}"/>
<s:hidden name="dailyMinServValue" value="%{operationsHubLimits.minServiceDayHub}"/>
<s:hidden name="crewCapacityValue" value="%{operationsHubLimits.dailyCrewCapacityLimit}"/>
<s:hidden name="crewThreshHoldValue" value="%{operationsHubLimits.dailyCrewThreshHoldLimit}"/>
<div id="Layer1" style="width:100%"> 
<div id="newmnav" class="nav_tabs"><!-- sandeep -->
		  <ul>		  	
		  		<li><a href="hubList.html"><span>Hub&nbsp;/&nbsp;WH List</span></a></li>
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Operational&nbsp;/&nbsp;WH Management<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  		<c:if test="${not empty operationsHubLimits.id}">
		  		<li><a href="resourceControlList.html?hub=${operationsHubLimits.hubID}"><span>Resource Limit Control</span></a></li>
		  			<li><a href="dailyControlList.html?hub=${operationsHubLimits.hubID}"><span>Daily Limit Control</span></a></li>
		  		</c:if>		  			  		
		  </ul>
</div>

<div class="spn">&nbsp;</div>
<div style="padding-bottom:0px;"></div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
	  <tbody>
		  <tr>
		  	<td align="left" class="listwhitetext">
			  	<table class="detailTabLabel" border="0" cellpadding="2" >
					  <tbody>  	
					  	<tr>
					  		<td align="left" height="3px"></td>
					  	</tr>
					  	<tr>
					  	<c:if test="${operationChk=='H'}">
					  	<c:if test="${empty operationsHubLimits.id}">
					  		<td align="left" width="15px"></td>
					  		<td align="left">Hub<font color="red" size="2">*</font>
					  		<s:select cssClass="list-menu" name="operationsHubLimits.hubID" list="%{hub}" cssStyle="width:100px"/></td>
						</c:if>
						</c:if>
						<c:if test="${operationChk=='W'}">
						<c:if test="${empty operationsHubLimits.id}">
					  		<td align="left" width="15px"></td>
					  		<td align="left">WareHouse<font color="red" size="2">*</font>
					  		<s:select cssClass="list-menu" name="operationsHubLimits.hubID" list="%{house}" headerKey=""  headerValue="" onclick="getValueForUnassignedWH();" cssStyle="width:200px"/></td>
						</c:if>
						</c:if>
						
						<c:if test="${not empty operationsHubLimits.id}">
							<td align="left" width="15px"></td>
					  		<td align="left">Hub&nbsp;/&nbsp;WH Selected : <strong> 
					  		<c:forEach var="entry" items="${distinctHubDescription}">
							<c:if test="${operationsHubLimits.hubID==entry.key}">
							<c:out value="${entry.value}" />
							</c:if>
							</c:forEach>
					  		</strong></td>
					  		<s:hidden name="operationsHubLimits.hubID"/>
						</c:if>
						<c:if test="${hubWarehouseLimit=='Tkt'}">						
						    <td align="left" width="15px">#&nbsp;of&nbsp;Tickets</td>
						    <td align="left" width="15px">Max&nbsp;Est&nbsp;Wt<font color="red" size="2">*</font></td>
						    <td align="left" width="15px">Max&nbsp;Est&nbsp;vol<font color="red" size="2">*</font></td>
						 </c:if>
						 <c:if test="${hubWarehouseLimit=='Crw'}">
						 <td align="left" width="15px">#&nbsp;of&nbsp;Members</td>
						 </c:if>
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px"></td>
					  		<c:if test="${hubWarehouseLimit=='Tkt'}">
					  		<td align="left" >Default Daily Operational Limit For (${typeService})<font color="red" size="2">*</font></td>
					  		<td align="left" width="90px">
					  		<s:textfield name="operationsHubLimits.dailyOpHubLimit"  maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  		<td align="left" width="90px">
					  		<s:textfield name="operationsHubLimits.dailyMaxEstWt"  maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" onkeydown="return onlyDoubleValue(event,this);" onkeyup="shiftValueUpdate(event)" onchange="return dotNotAllowed(this,'operationsHubLimits.dailyMaxEstWt');"/></td>
					  		<td align="left" >
					  		<s:textfield name="operationsHubLimits.dailyMaxEstVol"  maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" onkeydown="return onlyDoubleValue(event,this);" onkeyup="shiftValueUpdate(event)" onchange="return dotNotAllowed(this,'operationsHubLimits.dailyMaxEstVol');"/></td>
					  		
					  		<td align="left"></td>
					  		<s:hidden name="operationsHubLimits.dailyCrewCapacityLimit"/>
					  		<s:hidden name="operationsHubLimits.dailyCrewThreshHoldLimit"/>
					  		</c:if>
					  		<c:if test="${hubWarehouseLimit=='Crw'}">	
					  		<td align="left" >Default Daily Operational Limit For (${typeService}) <font color="red" size="2">*</font></td>
					  		<td align="left" width="90px">
					  		<s:textfield name="operationsHubLimits.dailyCrewCapacityLimit"  maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  		<s:hidden name="operationsHubLimits.dailyOpHubLimit"/>
					  		<s:hidden name="operationsHubLimits.dailyMaxEstWt"/>
					  		<s:hidden name="operationsHubLimits.dailyMaxEstVol"/>
					  		<s:hidden name="operationsHubLimits.dailyOpHubLimitPC"/>
					  		</c:if>
					  		<!--  
					  		<td align="left"><input type="button" class="cssbutton1" style="width: 100px" name="Daily Control" value="Daily Control" onclick="dailyControl();"/></td>
					  		-->
					  	</tr>
					  	<tr>
					  		<td align="left" width="15px"></td>
					  		<td align="left">Enter Warning Threshold For Daily Limit (e.g. 85%)<font color="red" size="2">*</font></td>
					  		<c:if test="${hubWarehouseLimit=='Tkt'}">
					  		<td align="left" colspan="4"><s:textfield name="operationsHubLimits.dailyOpHubLimitPC" maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  		</c:if>
					  		<c:if test="${hubWarehouseLimit=='Crw'}">
					  		<td align="left" colspan="4"><s:textfield name="operationsHubLimits.dailyCrewThreshHoldLimit" maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  		</c:if>
						</tr>
						
					  	<tr>
					  		<td align="left" width="15px"></td>
					  		<td align="left">Enter Minimum Service Days For Ticket Entry<font color="red" size="2">*</font></td>
					  		<td align="left" colspan="4"><s:textfield name="operationsHubLimits.minServiceDayHub"  maxlength="10" size="10" cssClass="input-text" cssStyle="text-align:right" onkeydown="return onlyNumberValue(event);" onkeyup="shiftValueUpdate(event)"/></td>
					  	</tr>
					  	
					  	</tbody>
				</table>
			 </td>
		    </tr>
		      	<tr>
					  		<td align="left" height="13px"></td>
					  	</tr>		  	
		 </tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	
				<table border="0" width="750px;">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${operationsHubLimits.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="operationsHubLimits.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${operationsHubLimits.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty operationsHubLimits.id}">
								<s:hidden name="operationsHubLimits.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{operationsHubLimits.createdBy}"/></td>
							</c:if>
							<c:if test="${empty operationsHubLimits.id}">
								<s:hidden name="operationsHubLimits.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${operationsHubLimits.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="operationsHubLimits.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${operationsHubLimits.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty operationsHubLimits.id}">
								<s:hidden name="operationsHubLimits.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{operationsHubLimits.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty operationsHubLimits.id}">
								<s:hidden name="operationsHubLimits.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

	<table class="detailTabLabel" border="0">
		<tbody> 	
			<tr>
				<td align="left" height="3px"></td>
			</tr>	  	
			<tr>
				<td align="left"><s:submit cssClass="cssbutton1" type="button" key="button.save" onclick="return validate();"/></td>
		       	<td align="right"><s:reset cssClass="cssbutton1" type="button" key="Reset"/></td>
			</tr>		  	
		</tbody>
	</table>
</div>

</s:form>
<script type="text/javascript"> 
var f = document.getElementById('dailyOpsForm'); 
f.setAttribute("autocomplete", "off");
 </script>
 <script type="text/javascript">
 try{
	 var wareHouse=document.forms['dailyOpsForm'].elements['operationsHubLimits.hubID'].value;
		if(wareHouse=='0'){
		document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimit'].readOnly =true;
       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyOpHubLimitPC'].readOnly =true;
       document.forms['dailyOpsForm'].elements['operationsHubLimits.minServiceDayHub'].readOnly =true;
       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstWt'].readOnly =true;
       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyMaxEstVol'].readOnly =true;
       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyCrewCapacityLimit'].readOnly =true;
       document.forms['dailyOpsForm'].elements['operationsHubLimits.dailyCrewThreshHoldLimit'].readOnly =true;
		}
 } catch(e){}
</script>