<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>	
	<title>Network DataFields List</title>
</head>
<script language="javascript" type="text/javascript">
function goToCustomerDetail(targetValue){
        document.forms['networkDataFieldsList'].action = 'nlist.html';
        document.forms['networkDataFieldsList'].submit();
}
function confirmSubmit(targetElement){
	 var agree=confirm("Are you sure you want to remove this record?");
	 var did = targetElement;
	 var tempPageNumber= <%= request.getParameter("d-148826-p") %> ;
	 var mName = document.forms['networkDataFieldsList'].elements['mName'].value;
	 var fieldName = document.forms['networkDataFieldsList'].elements['fieldName'].value;
	 if(tempPageNumber=='' || tempPageNumber=='0' || tempPageNumber=='null'){
	  tempPageNumber='1';
	  }
	 if (agree){
	  location.href="networkDataFieldDelete.html?id="+did+"&paginationNum="+tempPageNumber+"&mName="+mName+"&fieldName="+fieldName;
	 }else{
	  return false;
	 }
}	
function clear_fields(){
	document.forms['networkDataFieldsList'].elements['mName'].value = "";
	document.forms['networkDataFieldsList'].elements['fieldName'].value = "";
	document.forms['networkDataFieldsList'].elements['types'].value = "";
	document.forms['networkDataFieldsList'].elements['tranType'].value = "";
	}
	
function goToSearch(){
		return selectSearchField();
        document.forms['networkDataFieldsList'].action = 'searchNetworkDataFieldsList.html';
        document.forms['networkDataFieldsList'].submit();
}

function selectSearchField(){	
		var modelName=document.forms['networkDataFieldsList'].elements['mName'].value; 
		var fieldName=document.forms['networkDataFieldsList'].elements['fieldName'].value;
		var types=document.forms['networkDataFieldsList'].elements['types'].value;
		var tranType=document.forms['networkDataFieldsList'].elements['tranType'].value;
		modelName=modelName.trim();
		fieldName=fieldName.trim();
		types=types.trim();
		tranType=tranType.trim();
		document.forms['networkDataFieldsList'].elements['mName'].value=modelName; 
		document.forms['networkDataFieldsList'].elements['fieldName'].value=fieldName;
		document.forms['networkDataFieldsList'].elements['types'].value=types;
		document.forms['networkDataFieldsList'].elements['tranType'].value=tranType;
	    if(modelName=='' && fieldName=='' && types=='' && tranType==''){
			alert('Please select any one of the search criteria!');	
			return false;	
		}		
}
</script>
<s:form name="networkDataFieldsList" id="networkDataFieldsList" action="searchNetworkDataFieldsList">
<s:set name="nlist" value="nlist" scope="request"/>  
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:70px; height:25px" onclick="location.href='<c:url value="/networkDataFieldEdit.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set> 

<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:58px;" align="top" method="search" key="button.search" onclick="return goToSearch();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:58px;" onclick="clear_fields();"/> 
</c:set>
<div id="layer1" style="width:100%; ">
<div id="otabs" style="">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>		 
		</div>
		<div class="spnb">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" style="width:99%;margin-top:2px;" border="0" cellpadding="0" cellspacing="0">
<thead>
<tr>
<th>Model Name</th>
<th>Field Name</th>
<th>Type</th>
<th>TransactionType</th>
<th>&nbsp;</th>
</tr></thead>	
		<tbody>
		<tr>			
			<td width="">
			    <s:select  name="mName" list="%{combo1Map}" cssClass="list-menu" cssStyle="width:120px"/>
			</td>
			<td width="">
			    <s:textfield name="fieldName" size="35" required="true" cssClass="text medium" />
			</td>
			<td><s:select name="types" list="%{type}" headerKey=" " headerValue=" " cssClass="list-menu" cssStyle="width:120px"/></td>
			<td><s:select name="tranType" list="%{trType}" headerKey=" " headerValue=" " cssClass="list-menu" cssStyle="width:120px"/></td>
			<td><c:out value="${searchbuttons}" escapeXml="false" /></td>
    </tr>
   
		</tbody>
	</table>
	<div style="!margin-top:7px;"></div>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> </div>
<c:out value="${searchresults}" escapeXml="false" /> 

    <display:table name="nlist" class="table" requestURI="" id="nlist" export="${empty param.popup}" defaultsort="1" pagesize="10" style="width:99%; margin-left:5px;margin-top: 2px;!margin-top:-5px;" 
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' > 
	<c:choose>
		<c:when test="${sessionCorpID == 'TSFT'}">
			<display:column property="id" href="networkDataFieldEdit.html" sortable="true" title="ID" paramId="id" paramProperty="id" />
		</c:when>
		<c:otherwise>
			<display:column property="id" sortable="true" title="ID"/>
		</c:otherwise>
	</c:choose>	  
   <display:column property="modelName" sortable="true" title="Model Name"/>
   <display:column property="fieldName" sortable="true" title="Field Name"/>
   <display:column property="toModelName" sortable="true" title="To Model Name"/>
   <display:column property="toFieldName" sortable="true" title="To Field Name"/>
   <display:column property="type" sortable="true" title="Type"/>
   <display:column property="transactionType" sortable="true" title="Transaction Type"/>
    <display:column  title="useSysDefault"  style="text-align: center;width:2%;" >
	    <c:if test="${nlist.useSysDefault=='true'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
		</c:if>
		<c:if test="${nlist.useSysDefault=='false'}">
			<img style="cursor: default;" src="${pageContext.request.contextPath}/images/cancel001.gif"/>
		</c:if>
		
	</display:column>
	
	<%-- <display:column title="Remove" style="width: 15px;">
	<c:if test="${sessionCorpID == 'TSFT'}">
		<a><img align="middle" title="" onclick="confirmSubmit('${nlist.id}');" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/></a>
	</c:if>
	</display:column>  --%>
	
   <display:setProperty name="paging.banner.item_name" value="networkDataFields"/>   
    <display:setProperty name="paging.banner.items_name" value="nlist"/>   
    <display:setProperty name="export.excel.filename" value="networkDataFields List.xls"/>   
    <display:setProperty name="export.csv.filename" value="networkDataFields List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="networkDataFields List.pdf"/>  
    </display:table>

	<c:if test="${sessionCorpID == 'TSFT'}">
		<c:out value="${buttons}" escapeXml="false" />
	</c:if>
     
    </s:form>
   