<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title><fmt:message key="notesForm.title"/></title>   
    <meta name="heading" content="<fmt:message key='notesForm.heading'/>"/> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/redskyditor/tiny_mce.js"></script> 
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <style type="text/css">

	</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<style type="text/css">
#overlay {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->
        <script language="javascript" type="text/javascript">
        <c:set var="OIFlag" value="true" />
        <sec-auth:authComponent componentId="module.accountingPortalTab.serviceorder.operationResourceTab">
        <c:set var="OIFlag" value="false" />
        </sec-auth:authComponent>
        </script> 

<script language="javascript" type="text/javascript">
<c:if test="${userType!='USER'}">
	window.onload = function() { 
	<c:if test="${not empty notes.id && pageContext.request.remoteUser!=notes.createdBy}">
		var elementsLen=document.forms['notesForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['notesForm'].elements[i].type=='text'){
					document.forms['notesForm'].elements[i].readOnly =true;
					document.forms['notesForm'].elements[i].className = 'input-textUpper';
				}else{
					document.forms['notesForm'].elements[i].disabled=true;
				}
		}		
	</c:if>
	
	<c:if test="${empty notes.id || pageContext.request.remoteUser==notes.createdBy}">	
		var elementsLen=document.forms['notesForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['notesForm'].elements[i].type=='text'){
				document.forms['notesForm'].elements[i].readOnly =true;
				document.forms['notesForm'].elements[i].className = 'input-textUpper';						
			}else{
				document.forms['notesForm'].elements[i].disabled=true;
			}						
		}
		if(document.forms['notesForm'].elements['saveButton']){
				document.forms['notesForm'].elements['saveButton'].disabled=false;
		}
		if(document.forms['notesForm'].elements['subject']){
				document.forms['notesForm'].elements['subject'].readOnly=false;
				document.forms['notesForm'].elements['subject'].className = 'input-text';
		}
		if(document.forms['notesForm'].elements['noteTextArea']){
				document.forms['notesForm'].elements['noteTextArea'].disabled=false;
				//document.forms['notesForm'].elements['noteTextArea'].className = 'listwhitetext';
		}
		if(document.forms['notesForm'].elements['addNewNote'])
		{
			document.forms['notesForm'].elements['addNewNote'].disabled=false;
		}
		<c:if test="${userType =='ACCOUNT'}">
		<c:if test="${notes.corpID =='UTSI'}">
     
		if(document.forms['notesForm'].elements['remindBtn']){
			document.forms['notesForm'].elements['remindBtn'].disabled=false;
	    }
		</c:if>
		</c:if>
	</c:if>
}
</c:if>

function enableAllField(){
	showOrHideAutoNote(1);
	var elementsLen=document.forms['notesForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
		if(document.forms['notesForm'].elements[i].type=='text'){
				document.forms['notesForm'].elements[i].readOnly =false;
				document.forms['notesForm'].elements[i].className = 'input-text';
			}else{
				document.forms['notesForm'].elements[i].disabled=false;
			}
	}
	<c:if test="${userType=='ACCOUNT'}">	
	if(document.forms['notesForm'].elements['saveButton']){
		document.forms['notesForm'].elements['LinktoFileCabinet'].disabled=true;
		document.forms['notesForm'].elements['addNewNote'].disabled=true;
		document.forms['notesForm'].elements['emailBtn'].disabled=true;
		document.forms['notesForm'].elements['saveButton'].disabled=true;
		<c:if test="${notes.corpID =='UTSI'}">
		document.forms['notesForm'].elements['remindBtn'].disabled=true;
		</c:if>
     }	
	</c:if>	
}
function showOrHideAutoNote(value) {
	    if (value==0) {
        if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   }
   else if (value==1) {
       if (document.layers)
          document.layers["overlay"].visibility='show';
       else
          document.getElementById("overlay").style.visibility='visible';
   }
}
</script>

    <script type="text/javascript">
			// Use it to attach the editor to all textareas with full featured setup
			//WYSIWYG.attach('all', full);
			
			// Use it to attach the editor directly to a defined textarea
			<configByCorp:fieldVisibility componentId="component.field.partnerUser.showEditor">
			 tinyMCE.init({
			
			 mode : "textareas",
			 theme : "advanced",
			 relative_urls : false,
			 remove_script_host : false,
			 plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
			 
			 theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,cleanup,code,|,insertdate,inserttime|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
				 theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : true,
					// Example content CSS (should be your site CSS)
					content_css : "css/content.css",

					// Drop lists for link/image/media/template dialogs
					template_external_list_url : "lists/template_list.js",
					external_link_list_url : "lists/link_list.js",
					external_image_list_url : "lists/image_list.js",
					media_external_list_url : "lists/media_list.js"
			 }) 
			 </configByCorp:fieldVisibility>
		</script> 


	<script language="javascript" type="text/javascript">
		function onFormLoad() {
			if(document.forms['notesForm'].elements['notes.forwardDate'].value == "null" ){
				document.forms['notesForm'].elements['notes.forwardDate'].value="";
			}
			if( document.forms['notesForm'].elements['notes.forwardToUser'].value == "" ) {
				document.forms['notesForm'].elements['forwardBtn'].disabled = true ;
				//document.forms['notesForm'].elements['emailBtn'].disabled = true ;
			}
			var f = document.getElementById('notesForm'); 
			f.setAttribute("autocomplete", "off"); 
		}		
	</script>
	
	<script language="javascript" type="text/javascript">
		//var cal = new CalendarPopup('mydiv1'); 
		//cal.showNavigationDropdowns();		
	</script>
	
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="javascript" type="text/javascript">
		function setTimeMask() {
		oTimeMask1 = new Mask("##:##", "remindTime");
		oTimeMask1.attach(document.forms['notesForm'].remindTime);
		
		}
</script>
		
	<script language="JavaScript">
		function onlyNumsAllowed(evt)
		{
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
		}
		function onlyFloatNumsAllowed(evt)
		{
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
		}
		
		function onlyCharsAllowed(evt)
		{
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222)  ; 
		}
		
		function onlyTimeFormatAllowed(evt)
		{
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
		}
	</script>
	
	<script type="text/javascript">
	function validate_email(field)
	{
	with (field)
	{
	apos=value.indexOf("@")
	dotpos=value.lastIndexOf(".")
	if (apos<1||dotpos-apos<2) 
	  {alert("Not a valid e-mail address!");return false}
	else {return true}
	}
	}
	</script>	
	
	<script type="text/javascript"> 
		function forwardToMyMessage1(){ 

		}
	</script>
	
	<script type="text/javascript"> 
		function forwardToMyMessage(){ 
			document.forms['notesForm'].elements['myMessage.fromUser'].value=document.forms['notesForm'].elements['notes.updatedBy'].value;		
			document.forms['notesForm'].elements['myMessage.subject'].value=document.forms['notesForm'].elements['notes.subject'].value;		
			document.forms['notesForm'].elements['myMessage.message'].value=document.forms['notesForm'].elements['notes.note'].value;		
			document.forms['notesForm'].elements['myMessage.toUser'].value=document.forms['notesForm'].elements['notes.forwardToUser'].value; 
			document.forms['notesForm'].elements['myMessage.sentOn'].value=document.forms['notesForm'].elements['notes.updatedOn'].value;
						document.forms['notesForm'].elements['myMessage.noteId'].value=document.forms['notesForm'].elements['notes.id'].value;
			
		}
	</script>

	<script type="text/javascript"> 
		function emailForm(){ 
		var chk = checkMandatryField();
		if(chk==true){
		var daReferrer = document.referrer; 
		var email = ""; 
		var errorMsg = "here here here is the error error error error"; 
		var subject = document.forms['notesForm'].elements['notes.subject'].value; 
		var id = document.forms['notesForm'].elements['notes.notesId'].value;
		var firstName = "";
		var lastName = "";
		if(document.forms['notesForm'].elements['notes.noteType'].value == 'Customer File'){
			firstName = "${notesFirstName}";
			lastName = "${notesLastName}";
		}else{
			
			firstName = "${notesFirstName}";
			lastName = "${notesLastName}";
		}
		
		subject = id+"  "+firstName+" "+lastName+" "+subject;
		var name = document.forms['notesForm'].elements['notes.createdBy'].value;
		var signatureNotes = document.forms['notesForm'].elements['signatureNote'].value;
		var formNotes = document.forms['notesForm'].elements['notes.note'].value;
		var StrippedString = formNotes.replace(/(<([^>]+)>)/ig,"");
		var cleantext=StrippedString.replace(/(;&nbsp)/ig,"");
		cleantext = cleantext.replace(/(&nbsp;)/ig,"");
		var body_message = cleantext+'\n\n\n'+ signatureNotes; 
		var mailto_link = 'mailto:'+encodeURIComponent(email)+'?subject='+encodeURIComponent(subject)+'&body='+encodeURIComponent(body_message);		
		win = window.open(mailto_link,'emailWindow'); 
		document.forms['notesForm'].action ='${empty param.popup?"saveNotes.html":"saveNotes.html?decorator=popup&popup=true"}';
		document.forms['notesForm'].submit();
		if (win && win.open &&!win.closed) win.close(); 
		}
		
	} 
	</script> 
<script type="text/javascript">
	function enableForwardBtn() {
		if( document.forms['notesForm'].elements['notes.forwardToUser'].value == "" ) {
				document.forms['notesForm'].elements['forwardBtn'].disabled = true ;
				//document.forms['notesForm'].elements['emailBtn'].disabled = true ;
			}else {
				document.forms['notesForm'].elements['forwardBtn'].disabled = false ;
				//document.forms['notesForm'].elements['emailBtn'].disabled = false ;
			}
			document.forms['notesForm'].elements['remindMe'].value = "";
	}
	function checkDateAndTime(){
		  if( document.forms['notesForm'].elements['notes.forwardDate'].value == "" || document.forms['notesForm'].elements['notes.remindTime'].value == "") {
		  	alert("Either Follow up date OR Follow up time field is empty! please fill both to proceed for reminder");
		  }
	}

</script>
<SCRIPT LANGUAGE="JavaScript">
function IsValidTime() {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['notesForm'].elements['notes.remindTime'].value;

var matchArray = timeStr.match(timePat);
if (document.forms['notesForm'].elements['notes.forwardDate'].value == '' || document.forms['notesForm'].elements['notes.remindTime'].value == '' || document.forms['notesForm'].elements['notes.remindTime'].value == '00:00' || document.forms['notesForm'].elements['notes.followUpFor'].value == '' || document.forms['notesForm'].elements['notes.remindInterval'].value == '') {
	if(document.forms['notesForm'].elements['notes.forwardDate'].value == '' && (document.forms['notesForm'].elements['notes.remindTime'].value == '' || document.forms['notesForm'].elements['notes.remindTime'].value=='00:00')){
		alert ("Please select follow up date and time.");
		document.forms['notesForm'].elements['notes.forwardDate'].focus();
		return false;
	}else {
		if(document.forms['notesForm'].elements['notes.forwardDate'].value == ''){
			alert ("Please select follow up date");
			document.forms['notesForm'].elements['notes.forwardDate'].focus();
			return false;
		}
	
		if(document.forms['notesForm'].elements['notes.followUpFor'].value == ''){
			alert ("Please select follow up For ");
			document.forms['notesForm'].elements['notes.followUpFor'].focus();
			return false;
		}
		if(document.forms['notesForm'].elements['notes.remindInterval'].value == ''){
			alert ("Please select reminder interval");
			document.forms['notesForm'].elements['notes.remindInterval'].focus();
			return false;
		}
	}
}
if (matchArray == null) {
alert("Time is not in a valid format.");
document.forms['notesForm'].elements['notes.remindTime'].value = '';
document.forms['notesForm'].elements['notes.remindTime'].focus();
//document.forms['notesForm'].elements['remindBtn'].disabled = true ;
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("Follow up  time must be between 0 and 23(Hrs)");
document.forms['notesForm'].elements['notes.remindTime'].value = '';
document.forms['notesForm'].elements['notes.remindTime'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("Follow up  time must be between 0 and 59(Mins)");
document.forms['notesForm'].elements['notes.remindTime'].value = '';
document.forms['notesForm'].elements['notes.remindTime'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['notesForm'].elements['notes.remindTime'].value = '';
document.forms['notesForm'].elements['notes.remindTime'].focus();
return false;
}
return checkDate();
document.forms['notesForm'].submit();
return false;
}

function setRemindValue(){
   document.forms['notesForm'].elements['remindMe'].value = "remind";
}
function setbtnType(targetElement){
   document.forms['notesForm'].elements['btntype'].value = targetElement.value;
   var elementsLen=document.forms['notesForm'].elements.length;
			for(i=0;i<=elementsLen-1;i++)
			{
				
						document.forms['notesForm'].elements[i].disabled=false;
			}

	<c:if test="${userType=='AGENT' || userType=='ACCOUNT'}">		
			document.forms['notesForm'].elements['notes.noteType'].disabled=false;
			document.forms['notesForm'].elements['notes.noteSubType'].disabled=false;
	</c:if>
}
</script>

<SCRIPT LANGUAGE="JavaScript">
	function completeTimeString() {
		stime1 = document.forms['notesForm'].elements['notes.remindTime'].value;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['notesForm'].elements['notes.remindTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['notesForm'].elements['notes.remindTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['notesForm'].elements['notes.remindTime'].value = stime1 + "00";
			}
		}
		
	}
/* removed this function call from the textarea of notes entring textarea id =noteTextArea 
function  checkLength(){
	var txt = document.forms['notesForm'].elements['notes.note'].value;
	if(txt.length >2000){
		document.forms['notesForm'].elements['notes.note'].value = txt.substring(0,2000);
		return false;
	}else{
		if((2000 - txt.length) >= 0){
		document.forms['notesForm'].elements['charLeft'].value=2000 - txt.length;
		}
		return true;
	}
}

*/
function chkuvl(){
	//New var
	//alert("my flds....");
	var sub= document.forms['notesForm'].elements['notes.subject'].value;
	var noteDetails= document.forms['notesForm'].elements['notes.note'].value;
	var uvlstatus= document.forms['notesForm'].elements['notes.uvlSentStatus'];
	var categ= document.forms['notesForm'].elements['notes.category'].value;
	//alert(uvlstatus.checked);
	//alert(categ);

	if(uvlstatus.checked){
		if(categ==null || categ==''){
			alert('Kindly select the category required.');
			return false;
			
		}
		if(sub==null || sub==''){
			alert('Please fill the note subject.');
			return false;
			
		}
		if(noteDetails==null || noteDetails==''){
			alert('Please fill the note detail.');
			return false;
			
		}

		//return false;
	}
	 return true
	
}
function checkMandatryField() {
	<c:if test="${param.notesList == 'YES'}">
		document.forms['notesForm'].elements['notes.networkAgent'].checked=true;
	</c:if>
	//alert("in mand...")
	if(!chkuvl()){
		return false;
	}

if(document.forms['notesForm'].elements['notes.noteType'].value=='')
{
	alert('Type is a Required Field.');
	return false;
}
var status = document.forms['notesForm'].elements['notes.noteStatus'].value;
var compDate= document.forms['notesForm'].elements['notes.complitionDate'].value;
var type = document.forms['notesForm'].elements['notes.noteType'].value;
if(status=='CMP' && compDate==''&& type=='Issue Resolution'){
	alert('Please fill the completion date.');
	return false;
}
 return true
}
function  checkLengthOnSave(clickType){
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
if(clickType == 'none'){
if ('${autoSavePrompt}' == 'No'){
	var id1 = document.forms['notesForm'].elements['customerFile.id'].value;
	var sid = document.forms['notesForm'].elements['serviceOrder.id'].value;
	var idw = document.forms['notesForm'].elements['workTicket.id'].value;
	var jobNumber = document.forms['notesForm'].elements['serviceOrder.shipNumber'].value;
	var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
	if(id1 != ''){
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				noSaveAction = 'customerServiceOrders.html?id='+id1;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
				noSaveAction = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.surveys'){
				noSaveAction = 'surveysList.html?id1='+id1;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.docs'){
				openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
				}
			if(sid != ''){
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.serviceorderdtl'){
				noSaveAction = 'editServiceOrderUpdate.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.billing'){
				noSaveAction = 'editBilling.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				noSaveAction = 'accountLineList.html?sid='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
				noSaveAction = 'pricingList.html?sid='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
				<c:if test="${forwardingTabVal!='Y'}">
	  				noSaveAction = 'containers.html?id='+sid;
	  			</c:if>
		  		<c:if test="${forwardingTabVal=='Y'}">
		  			noSaveAction = 'containersAjaxList.html?id='+sid;
		  		</c:if>
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.domestic'){
				noSaveAction = 'editMiscellaneous.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.status'){
             <c:if test="${serviceOrder.job =='RLO'}">
            	noSaveAction = 'editDspDetails.html?id='+sid; 
			</c:if>
            <c:if test="${serviceOrder.job !='RLO'}">
            	noSaveAction =  'editTrackingStatus.html?id='+sid; 
			</c:if>
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.ticket'){
				noSaveAction = 'customerWorkTickets.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.claims'){
				noSaveAction= 'claims.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.socustomerfile'){
				noSaveAction = 'editCustomerFile.html?id='+id1;
				}
			}
			if(idw != ''){
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.workticket'){
				noSaveAction = 'editWorkTicketUpdate.html?id='+idw;
				}
			}
		}
		processAutoSave(document.forms['notesForm'], '${empty param.popup?"saveNotes!saveOnTabChange.html":"saveNotes!saveOnTabChange.html?decorator=popup&popup=true"}', noSaveAction);
	
}


else{
	var id1 = document.forms['notesForm'].elements['customerFile.id'].value;
	var sid = document.forms['notesForm'].elements['serviceOrder.id'].value;
	var idw = document.forms['notesForm'].elements['workTicket.id'].value;
	var jobNumber = document.forms['notesForm'].elements['serviceOrder.shipNumber'].value;
	if (document.forms['notesForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Do you want to save the note and continue? press OK to continue with save OR press CANCEL");
		if(agree){
			document.forms['notesForm'].action = 'saveNotes!saveOnTabChange.html';
			document.forms['notesForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'customerServiceOrders.html?id='+id1;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
				location.href = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.surveys'){
				location.href = 'surveysList.html?id1='+id1;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.docs'){
				openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
				}
			if(sid != ''){
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.serviceorderdtl'){
				location.href = 'editServiceOrderUpdate.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				location.href = 'accountLineList.html?sid='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
				location.href = 'pricingList.html?sid='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
				<c:if test="${forwardingTabVal!='Y'}">
			 		location.href = 'containers.html?id='+sid;
  				</c:if>
	  			<c:if test="${forwardingTabVal=='Y'}">
	  				location.href = 'containersAjaxList.html?id='+sid;
	  			</c:if>
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.domestic'){
				location.href = 'editMiscellaneous.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.status'){
	            <c:if test="${serviceOrder.job =='RLO'}">
	            location.href = 'editDspDetails.html?id='+sid; 
			</c:if>
            <c:if test="${serviceOrder.job !='RLO'}">
            location.href =  'editTrackingStatus.html?id='+sid; 
			</c:if>
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.ticket'){
				location.href = 'customerWorkTickets.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.claims'){
				location.href = 'claims.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.socustomerfile'){
				location.href = 'editCustomerFile.html?id='+id1;
				}
			}
			if(idw != ''){
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.workticket'){
				location.href = 'editWorkTicketUpdate.html?id='+idw;
				}
			}
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'customerServiceOrders.html?id='+id1;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
				location.href = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.surveys'){
				location.href = 'surveysList.html?id1='+id1;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.docs'){
				openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
				}
			if(sid != ''){
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.serviceorderdtl'){
				location.href = 'editServiceOrderUpdate.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				location.href = 'accountLineList.html?sid='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
				location.href = 'pricingList.html?sid='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
				<c:if test="${forwardingTabVal!='Y'}">
		 			location.href = 'containers.html?id='+sid;
				</c:if>
  				<c:if test="${forwardingTabVal=='Y'}">
  					location.href = 'containersAjaxList.html?id='+sid;
  				</c:if>
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.domestic'){
				location.href = 'editMiscellaneous.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.status'){
	            <c:if test="${serviceOrder.job =='RLO'}">
	            location.href = 'editDspDetails.html?id='+sid; 
			</c:if>
            <c:if test="${serviceOrder.job !='RLO'}">
            location.href =  'editTrackingStatus.html?id='+sid; 
			</c:if>
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.ticket'){
				location.href = 'customerWorkTickets.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.claims'){
				location.href = 'claims.html?id='+sid;
				}
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.socustomerfile'){
				location.href = 'editCustomerFile.html?id='+id1;
				}
			}
			if(idw != ''){
			if(document.forms['notesForm'].elements['gotoPageString'].value == 'gototab.workticket'){
				location.href = 'editWorkTicketUpdate.html?id='+idw;
				}
			}
	}
	}
}
}
		return true;
	
}
function changeStatus(){
	document.forms['notesForm'].elements['formStatus'].value = '1';
}
function getSubType(){
target=document.forms['notesForm'].elements['notes.noteType'];
if(target.value==''){
    	document.forms['notesForm'].elements['notes.noteSubType'].options[0].text = '';
		document.forms['notesForm'].elements['notes.noteSubType'].options[0].value = '';
	}
    if(target.value != ''){
     	var url="getSubTypeList.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
     	http2.open("GET", url, true);
     	http2.onreadystatechange = handleHttpResponse;
     	http2.send(null);
   	}
}

function handleHttpResponse(){
            if (http2.readyState == 4) { 
              	var results = http2.responseText
             	results = results.trim();
              	var res = results.split("#");
              	if(res.length == 2)  {
	                var targetElement = document.forms['notesForm'].elements['notes.noteSubType'];
					targetElement.length = (res.length-1);
 					for(i=1;i<res.length;i++){ 	
 						document.forms['notesForm'].elements['notes.noteSubType'].options[0].text= res[i];
						document.forms['notesForm'].elements['notes.noteSubType'].options[0].value = res[i];
					}
				} else {
					var targetElement = document.forms['notesForm'].elements['notes.noteSubType'];
					targetElement.length = res.length;
 					for(i=0;i<res.length;i++){
 						document.forms['notesForm'].elements['notes.noteSubType'].options[i].text = res[i];
						document.forms['notesForm'].elements['notes.noteSubType'].options[i].value = res[i];
					}
				}
			}
}
function getNoteStatusDetails(){
	target=document.forms['notesForm'].elements['notes.noteType'];	
	    if(target.value != '' && target.value=='Exception Service' ){
	     	var url="getNoteStatusList.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
	     	http22.open("GET", url, true);
	     	http22.onreadystatechange = handleHttpResponse22;
	     	http22.send(null);
	   	}else{
	   		var url="getNoteStatusList.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI('');
	     	http22.open("GET", url, true);
	     	http22.onreadystatechange = handleHttpResponse22;
	     	http22.send(null);
	   	}
	}

	function handleHttpResponse22(){
	            if (http22.readyState == 4) { 
	              	var results = http22.responseText
	             	results = results.trim();
	              	var res = results.split("#");
	              	if(res.length == 2)  {
		                var targetElement = document.forms['notesForm'].elements['notes.noteStatus'];
						targetElement.length = (res.length-1);
	 					for(i=1;i<res.length;i++){ 	
	 						document.forms['notesForm'].elements['notes.noteStatus'].options[0].text= res[i];
							document.forms['notesForm'].elements['notes.noteStatus'].options[0].value = res[i];
						}
					} else {
						var targetElement = document.forms['notesForm'].elements['notes.noteStatus'];
						targetElement.length = res.length;
	 					for(i=0;i<res.length;i++){
	 						document.forms['notesForm'].elements['notes.noteStatus'].options[i].text = res[i];
							document.forms['notesForm'].elements['notes.noteStatus'].options[i].value = res[i];
						}
					}
	              	document.forms['notesForm'].elements['notes.noteStatus'].value='${notes.noteStatus}';
				}
	}
	function getSubCategoryDetails(){
		var notesFor='${noteFor}';	
		target=document.forms['notesForm'].elements['notes.noteType'];	
		    if(target.value != '' && target.value=='Exception Service' ){
			    if(notesFor=='ServiceOrder' || notesFor=='agentQuotesSO'){
			    	var spNumber='${serviceOrder.shipNumber}';
			    	//alert(spNumber)
			    	var url="getNoteSubCategorylinkedTo.html?ajax=1&decorator=simple&popup=true&noteFor="+encodeURI(target.value)+"&spNumber="+encodeURI(spNumber);
			    	http23.open("GET", url, true);
			     	http23.onreadystatechange = handleHttpResponse23;
			     	http23.send(null);
			    }else{
			    	var url="getNoteSubCategory.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
			     	http23.open("GET", url, true);
			     	http23.onreadystatechange = handleHttpResponse23;
			     	http23.send(null);
			    }		     
		   	}else{
		   		var url="getNoteSubCategory.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
		     	http23.open("GET", url, true);
		     	http23.onreadystatechange = handleHttpResponse23;
		     	http23.send(null);
		   	}
		}
		function handleHttpResponse23(){
		            if (http23.readyState == 4) { 
		              	var results = http23.responseText
		             	results = results.trim();
		              	var res = results.split("#");
		              	//alert(res)
		              	var targetElement = document.forms['notesForm'].elements['notes.issueType'];
		              	 targetElement.length= res.length;
		           	   for(i=0; i<res.length; i++){
		 	     		if(res[i] == ''){
		 	     		}else{
		 	     		document.forms['notesForm'].elements['notes.issueType'].options[i].text =res[i].split('~')[1];
		 	     		document.forms['notesForm'].elements['notes.issueType'].options[i].value=res[i].split('~')[0];		 	     	
		 		         }
		 	     		document.forms['notesForm'].elements['notes.issueType'].value='${notes.issueType}';
		                 }
		              
					}
		}
		function getSubCategorySoDetails(){
			var notesFor='${noteFor}';
			var spNumberCorpId="";		
			target=document.forms['notesForm'].elements['notes.noteType'];	
			    if(target.value != '' && target.value=='Exception Service' ){
			    	var spNumber=document.forms['notesForm'].elements['notes.linkedTo'].value;			    
			    	if(spNumber!=''){
				    	//alert(spNumber)
				    	spNumberCorpId=spNumber.substring(0,4); 
				    	//alert(spNumberCorpId)			    
			    		var url="getNoteSubCategorylinkedTo.html?ajax=1&decorator=simple&popup=true&noteFor="+encodeURI(target.value)+"&spNumberCorpId="+spNumberCorpId+"&spNumber="+encodeURI(spNumber);
				     	http24.open("GET", url, true);
				     	http24.onreadystatechange = handleHttpResponse24;
				     	http24.send(null);
			    	}else if(spNumber=='' && (notesFor=='ServiceOrder' || notesFor=='agentQuotesSO')){	
			    		spNumber='${serviceOrder.shipNumber}';
			    		spNumberCorpId='${serviceOrder.corpID}';		    		
			    		var url="getNoteSubCategorylinkedTo.html?ajax=1&decorator=simple&popup=true&noteFor="+encodeURI(target.value)+"&spNumberCorpId="+spNumberCorpId+"&spNumber="+encodeURI(spNumber);
				     	http24.open("GET", url, true);
				     	http24.onreadystatechange = handleHttpResponse24;
				     	http24.send(null);
			    	}else{				    	
			    		var url="getNoteSubCategory.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
				     	http24.open("GET", url, true);
				     	http24.onreadystatechange = handleHttpResponse24;
				     	http24.send(null);
			    	}			     	
			   	}else{				 
			   		var url="getNoteSubCategory.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
			     	http24.open("GET", url, true);
			     	http24.onreadystatechange = handleHttpResponse24;
			     	http24.send(null);
			   	}
			}
			function handleHttpResponse24(){
			            if (http24.readyState == 4) { 
			              	var results = http24.responseText
			             	results = results.trim();
			              	var res = results.split("#");
			              	//alert(res)
			              	var targetElement = document.forms['notesForm'].elements['notes.issueType'];
			              	 targetElement.length= res.length;
			           	   for(i=0; i<res.length; i++){
			 	     		if(res[i] == ''){
			 	     		}else{
			 	     		document.forms['notesForm'].elements['notes.issueType'].options[i].text =res[i].split('~')[1];
			 	     		document.forms['notesForm'].elements['notes.issueType'].options[i].value=res[i].split('~')[0];		 	     	
			 		         }
			 	     		document.forms['notesForm'].elements['notes.issueType'].value='${notes.issueType}';
			                 }
			              
						}
			}
			function getGradingDetails(){
				var notesFor='${noteFor}';
				var CFbillToCode='';
				var spNumberCorpId="";
				target=document.forms['notesForm'].elements['notes.noteType'];	
				    if(target.value != '' && target.value=='Exception Service' ){
				    	var spNumber=document.forms['notesForm'].elements['notes.linkedTo'].value;
				    	if(spNumber!=''){
				    	  	//alert(spNumber)				    
					    	spNumberCorpId=spNumber.substring(0,4); 
					    	//alert(spNumberCorpId)					    	
				    		var url="getNoteGradinglinkedTo.html?ajax=1&decorator=simple&popup=true&noteFor="+encodeURI(target.value)+"&spNumberCorpId="+spNumberCorpId+"&spNumber="+encodeURI(spNumber);
					     	http25.open("GET", url, true);
					     	http25.onreadystatechange = handleHttpResponse25;
					     	http25.send(null);
				    	}else{
					    	if(notesFor=='CustomerFile' || notesFor=='agentQuotes'){
					    		CFbillToCode='${customerFile.billToCode}';
					    	var sequenceNumber='${customerFile.sequenceNumber}';
					    		spNumberCorpId=sequenceNumber.substring(0,4); 
					    	}else if(notesFor=='ServiceOrder' || notesFor=='agentQuotesSO'){
					    		CFbillToCode='${serviceOrder.billToCode}';
					    	    var sequenceNumber='${serviceOrder.sequenceNumber}';
					    		spNumberCorpId=sequenceNumber.substring(0,4); 
					    	}				    	
					    	//alert(CFbillToCode)
				    		var url="getNoteGrading.html?ajax=1&decorator=simple&popup=true&spNumberCorpId="+spNumberCorpId+"&spNumber="+encodeURI(CFbillToCode)+"&noteFor=" + encodeURI(target.value);
					     	http25.open("GET", url, true);
					     	http25.onreadystatechange = handleHttpResponse25;
					     	http25.send(null);
				    	}			     	
				   	}else{
				   		var url="getNoteGrading.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
				     	http25.open("GET", url, true);
				     	http25.onreadystatechange = handleHttpResponse25;
				     	http25.send(null);
				   	}
				}
				function handleHttpResponse25(){
				            if (http25.readyState == 4) { 
				              	var results = http25.responseText
				             	results = results.trim();
				              	var res = results.split("#");
				              	//alert(res)
				              	var targetElement = document.forms['notesForm'].elements['notes.grading'];
				              	 targetElement.length= res.length;
				           	   for(i=0; i<res.length; i++){
				 	     		if(res[i] == ''){
				 	     		}else{
				 	     		document.forms['notesForm'].elements['notes.grading'].options[i].text =res[i].split('~')[1];
				 	     		document.forms['notesForm'].elements['notes.grading'].options[i].value=res[i].split('~')[0];		 	     	
				 		         }
				 	     		document.forms['notesForm'].elements['notes.grading'].value='${notes.grading}';
				                 }
				              
							}
				}
				var http425 = getHTTPObject();
				function findNewAgentPortal(){
				var spNumber=document.forms['notesForm'].elements['notes.linkedTo'].value;
				var spNumberCorpId="";
					if(spNumber!=''){
					 spNumberCorpId=spNumber.substring(0,4);
					  var url = "findNewAgentPortal.html?ajax=1&decorator=simple&popup=true&spNumberCorpId="+spNumberCorpId+"&spNumber=" + encodeURI(spNumber)+"&cid=";
					  http425.open("GET", url, true);
					  http425.onreadystatechange = handleHttpResponseAgents12;
					  http425.send(null);
					}else{
					spNumberCorpId='${customerFile.id}';
					var url = "findNewAgentPortal.html?ajax=1&decorator=simple&popup=true&spNumberCorpId="+spNumberCorpId+"&spNumber=" + encodeURI(spNumber)+"&cid=${customerFile.id}";
					http425.open("GET", url, true);
					http425.onreadystatechange = handleHttpResponseAgents12;
					http425.send(null);
					}
				}

				function handleHttpResponseAgents12(){
				            if (http425.readyState == 4) { 
				              	var results = http425.responseText
				              	res=results.split("~");
				              	//alert(res)
				              	tarElem=document.getElementById("supplierId");
				              	tarElem.length = res.length;
				           		for(i=0;i<res.length;i++)
									{
									if(res[i] == ''){
									document.forms['notesForm'].elements['notes.supplier'].options[i].text = '';
									document.forms['notesForm'].elements['notes.supplier'].options[i].value = '';
									}else{
									codeName = res[i].split("#");
									document.forms['notesForm'].elements['notes.supplier'].options[i].text = codeName[1].trim();;
									document.forms['notesForm'].elements['notes.supplier'].options[i].value = codeName[0].trim(); 
									
									 } 
									 }
									 document.getElementById("supplierId").value='${notes.supplier}';
				             	
							}
				}
function goPrevIsRal() {
	//progressBarAutoSave('1');
	showOrHideAutoNote(1);
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.customerNumber'].value;
	var url="editPrevNoteIsRal.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherIsRal; 
     http5.send(null); 
   }
   
 function goNextIsRal() {
	//progressBarAutoSave('1');
	showOrHideAutoNote(1);
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.customerNumber'].value;
	var url="editNextNoteIsRal.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherIsRal; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherIsRal(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText;
               results = results.trim();
               var id1=document.forms['notesForm'].elements['id1'].value;
             //  var noteFor1=document.forms['notesForm'].elements['noteFor'].value;
               var noteFor1=document.getElementById('noteFor').value
               location.href = 'editNewNote.html?from=list&id1='+id1+'&id='+results+'&notesId=${notesId}&noteFor='+noteFor1+'&isRal=true';
             }
       }
 
function goPrevIsRalServiceOrder() {
	//progressBarAutoSave('1');
		showOrHideAutoNote(1);
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.customerNumber'].value;
	var url="editPrevNoteIsRal.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherIsRalServiceOrder; 
     http5.send(null); 
   }
   
 function goNextIsRalServiceOrder() {
	 showOrHideAutoNote(1);
	//progressBarAutoSave('1');
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.customerNumber'].value;
	var url="editNextNoteIsRal.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherIsRalServiceOrder; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherIsRalServiceOrder(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               var id1=document.forms['notesForm'].elements['id1'].value;
               var noteFor1=document.forms['notesForm'].elements['noteFor'].value;
               location.href = 'editNewNoteForServiceOrder.html?from=list&id1='+id1+'&id='+results+'&notesId=${notesId}&noteFor=${noteFor}&isRal=true';
             }
       }       
       
       
function goPrevIsRalTicket() {
	//progressBarAutoSave('1');
	showOrHideAutoNote(1);
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.customerNumber'].value;
	var url="editPrevNoteIsRal.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherIsRalTicket; 
     http5.send(null); 
   }
   
 function goNextIsRalTicket() {
	//progressBarAutoSave('1');
	showOrHideAutoNote(1);
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.customerNumber'].value;
	var url="editNextNoteIsRal.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherIsRalTicket; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherIsRalTicket(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               var id1=document.forms['notesForm'].elements['id1'].value;
               var noteFor1=document.forms['notesForm'].elements['noteFor'].value;
               location.href = 'editNewNoteForWorkTicket.html?from=list&id1='+id1+'&id='+results+'&notesId=${notesId}&noteFor='+noteFor1+'&isRal=true';
             }
       }
function findCustomerOtherSOIsRal(position) {
 var seqNm=document.forms['notesForm'].elements['notes.customerNumber'].value;
 var soIdNum=document.forms['notesForm'].elements['notes.id'].value;
 var url="notesNextPrevIsRal.html?ajax=1&decorator=simple&popup=true&seqNm=" + encodeURI(seqNm)+"&noteFor=${noteFor}&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	
	  function goToUrlIsRal(id)
	{
	    var id1=document.forms['notesForm'].elements['id1'].value;
	    var noteFor1=document.forms['notesForm'].elements['noteFor'].value;
	    location.href = 'editNewNote.html?from=list&id1='+id1+'&id='+id+'&notesId=${notesId}&noteFor=${noteFor}&isRal=true';
	}
function findCustomerOtherSOIsRalTicket(position) {
 var seqNm=document.forms['notesForm'].elements['notes.customerNumber'].value;
 var soIdNum=document.forms['notesForm'].elements['notes.id'].value;
 var url="notesNextPrevIsRalTicket.html?ajax=1&decorator=simple&popup=true&seqNm=" + encodeURI(seqNm)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	
	  function goToUrlIsRalTicket(id)
	{
	    var id1=document.forms['notesForm'].elements['id1'].value;
	    var noteFor1=document.forms['notesForm'].elements['noteFor'].value;
        location.href = 'editNewNoteForWorkTicket.html?from=list&id1='+id1+'&id='+id+'&notesId=${notesId}&noteFor='+noteFor1+'&isRal=true';
	}
function findCustomerOtherSOIsRalServiceOrder(position) {
 var seqNm=document.forms['notesForm'].elements['notes.customerNumber'].value;
 var soIdNum=document.forms['notesForm'].elements['notes.id'].value;
 var url="notesNextPrevIsRalServiceOrder.html?ajax=1&decorator=simple&popup=true&seqNm=" + encodeURI(seqNm)+"&noteFor=${noteFor}&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	
	  function goToUrlIsRalServiceOrder(id)
	{
	    var id1=document.forms['notesForm'].elements['id1'].value;
	    var noteFor1=document.forms['notesForm'].elements['noteFor'].value;
        location.href = 'editNewNoteForServiceOrder.html?from=list&id1='+id1+'&id='+id+'&notesId=${notesId}&noteFor=${noteFor}&isRal=true';
	}
function goPrev() {
	//progressBarAutoSave('1');
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.notesId'].value;
	var url="editPrevNote.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	//progressBarAutoSave('1');
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.notesId'].value;
	var url="editNextNote.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               var id1=document.forms['notesForm'].elements['id1'].value;
               location.href = 'editNewNote.html?from=list&id1='+id1+'&notesId=${notesId}&noteFor=${noteFor}&id='+results;
             }
       }     
       
function goPrevServiceNotes() {
	//progressBarAutoSave('1');
	showOrHideAutoNote(1);
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.notesId'].value;
	var url="editPrevNote.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseServiceNotes; 
     http5.send(null); 
   }
   
 function goNextServiceNotes() {
	//progressBarAutoSave('1');
	showOrHideAutoNote(1);
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.notesId'].value;
	var url="editNextNote.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseServiceNotes; 
     http5.send(null); 
   }
   
  function handleHttpResponseServiceNotes(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               var id1=document.forms['notesForm'].elements['id1'].value;
               var noteFor1=document.forms['notesForm'].elements['noteFor'].value;
               location.href = 'editNewNoteForServiceOrder.html?id1='+id1+'&from=list&notesId=${notesId}&noteFor=${noteFor}&id='+results;
             }
       }    
 function goPrevTicketNotes() {
	//progressBarAutoSave('1');
	showOrHideAutoNote(1);
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.notesId'].value;
	var url="editPrevNote.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseTicketNotes; 
     http5.send(null); 
   }      
function goNextTicketNotes() {
	//progressBarAutoSave('1');
	showOrHideAutoNote(1);
	var soIdNum =document.forms['notesForm'].elements['notes.id'].value;
	var seqNm =document.forms['notesForm'].elements['notes.notesId'].value;
	var url="editNextNote.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseTicketNotes; 
     http5.send(null); 
   }
   
  function handleHttpResponseTicketNotes(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               var id1=document.forms['notesForm'].elements['id1'].value;
               var noteFor1=document.forms['notesForm'].elements['noteFor'].value;
               location.href = 'editNewNoteForWorkTicket.html?id1='+id1+'&from=list&notesId=${notesId}&noteFor='+noteFor1+'&id='+results;
             }
       }    
        
function findCustomerOtherSO(position) {
 var seqNm=document.forms['notesForm'].elements['notes.notesId'].value;
 var soIdNum=document.forms['notesForm'].elements['notes.id'].value;
 var url="notesNextPrev.html?ajax=1&decorator=simple&popup=true&seqNm=" + encodeURI(seqNm)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	
	  function goToUrl(id)
	{
	    var id1=document.forms['notesForm'].elements['id1'].value;
        location.href = 'editNewNote.html?from=list&id1='+id1+'&id='+id+'&notesId=${notesId}&noteFor=${noteFor}';
	}
function findSeviceOtherSO(position) {
 var seqNm=document.forms['notesForm'].elements['notes.notesId'].value;
 var soIdNum=document.forms['notesForm'].elements['notes.id'].value;
 var url="notesNextPrevSo.html?ajax=1&decorator=simple&popup=true&seqNm=" + encodeURI(seqNm)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	
	  function goToUrlSevice(id)
	{
		var id1=document.forms['notesForm'].elements['id1'].value;
	   	location.href = 'editNewNoteForServiceOrder.html?id1='+id1+'&from=list&id='+id+'&notesId=${notesId}&noteFor=${noteFor}';
	}	 
function findTicketOtherSO(position) {
 var seqNm=document.forms['notesForm'].elements['notes.notesId'].value;
 var soIdNum=document.forms['notesForm'].elements['notes.id'].value;
 var url="notesNextPrevTicket.html?ajax=1&decorator=simple&popup=true&seqNm=" + encodeURI(seqNm)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
	
	  function goToUrlTicket(id)
	{
	    var id1=document.forms['notesForm'].elements['id1'].value;
	    location.href = 'editNewNoteForWorkTicket.html?id1='+id1+'&from=list&id='+id+'&notesId=${notesId}&noteFor=${noteFor}';
	}
var http25= getHTTPObject5();
var http24= getHTTPObject();
var http23=getHTTPObject4();
var http22=getHTTPObject5();
var http2 = getHTTPObject();
var http5 = getHTTPObject5();
function getHTTPObject4()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject5()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function disabledAll(){

		
	var i;
			for(i=0;i<=100;i++){
					document.forms['notesForm'].elements[i].disabled = true;
					
				
			}

	}
/* mail with attachment method starts here  */
  
		/* function emailAttachment(whatform)
	   {
	      
		var daReferrer = document.referrer; 
		var email = ""; 
		var errorMsg = "here  is the error "; 
		var subject = document.forms['notesForm'].elements['notes.subject'].value; 
		var id = document.forms['notesForm'].elements['notes.notesId'].value;
		var firstName = "";
		var lastName = "";
		if(document.forms['notesForm'].elements['notes.noteType'].value == 'Customer File'){
			firstName = '${notesFirstName}';
			lastName = '${notesLastName}';
		}else{
			
			firstName = '${notesFirstName}';
			lastName = '${notesLastName}';
		}
		
		subject = id+"  "+firstName+" "+lastName+" "+subject;
		var name = document.forms['notesForm'].elements['notes.createdBy'].value;
		var signatureNotes = document.forms['notesForm'].elements['signatureNote'].value;
		var formNotes = document.forms['notesForm'].elements['notes.note'].value; 
		var body_message = formNotes +'\n\n\n'+ signatureNotes; 
		var file ="C://sig.txt";
		var mailto_link = 'mailto:'+encodeURI(email)+'?subject='+encodeURI(subject)+'&body='+encodeURI(body_message)+'&attachment='+encodeURI(file);
		alert(mailto_link);
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 

} 
/* mail with attachment method ends here */

 function linkVisible(){
var chkedData = document.forms['notesForm'].elements['notes.noteType'].value;
var linkDiv = document.getElementById("linkto");
var commonDiv = document.getElementById("common");
var common1 = document.getElementById("common1");
var displayDiv = document.getElementById("displayLink");
var noteSubTypeForAll= noteSubTypeForAll = document.getElementById("noteSubTypeForAll");
var noteSubTypeForExceptionService=noteSubTypeForExceptionService = document.getElementById("noteSubTypeForExceptionService");
var IssueTypeId = document.getElementById("IssueTypeId");
var SubCategoryId = document.getElementById("SubCategoryId");
var GradingId=document.getElementById("GradingId");
var ApprovedId=document.getElementById("ApprovedId");
var ExceptionActual=document.getElementById("ExceptionActual");
	if(chkedData == 'Issue Resolution'){
		linkDiv.style.display = 'block';
		//commonDiv.style.display = 'none';
		common1.style.display = 'none';
		displayDiv.style.display = 'block';
		SubCategoryId.style.display = 'none';
		IssueTypeId.style.display = 'block';
		GradingId.style.display = 'block';
		ApprovedId.style.display = 'none';
		ExceptionActual.style.display = 'none';
	}else if(chkedData == 'Exception Service'){
		noteSubTypeForExceptionService.style.display = 'block';
		noteSubTypeForAll.style.display = 'none';
		//linktoException.style.display = 'block';
		linkDiv.style.display = 'block';
		commonDiv.style.display = 'none';
		displayDiv.style.display = 'block';
		SubCategoryId.style.display = 'block';
		IssueTypeId.style.display = 'none';
		GradingId.style.display = 'none';
		ApprovedId.style.display = 'block';
		ExceptionActual.style.display = 'block';
	}else{
		noteSubTypeForExceptionService.style.display = 'none';
		noteSubTypeForAll.style.display = 'block';
		displayDiv.style.display = 'none';
		commonDiv.style.display = 'block';
		common1.style.display = 'block';
		linkDiv.style.display = 'none';
		//linktoException.style.display = 'none';
	}


}
	
function changeNoteImg(){
try{
 var id1=document.forms['notesForm'].elements['id1'].value;
 if(id1==''){
 id1=document.forms['notesForm'].elements['id'].value;
 }
 var openerLocation=encodeURI(window.opener.location); 
      var openerindex=openerLocation.indexOf("searchScheduleResouce"); 
      if(openerindex>0)
      {
      opener.document.getElementById("open"+id1).src="${pageContext.request.contextPath}/images/closed.png";
      }
}
catch(e){}
}
	
</SCRIPT>
<script type="text/javascript">
var http2222 = getHTTPObject();
function findNewAgents(){
var spNumber=document.forms['notesForm'].elements['notes.linkedTo'].value;

	if(spNumber!=''){
	  var url = "findNewAgents.html?ajax=1&decorator=simple&popup=true&spNumber=" + encodeURI(spNumber)+"&cid=";
	  http2222.open("GET", url, true);
	  http2222.onreadystatechange = handleHttpResponseAgents;
	  http2222.send(null);
	}else{
		var spNumber='${serviceOrder.shipNumber}';
	var url = "findNewAgents.html?ajax=1&decorator=simple&popup=true&spNumber=" + encodeURI(spNumber)+"&cid=${customerFile.id}";
	  http2222.open("GET", url, true);
	  http2222.onreadystatechange = handleHttpResponseAgents;
	  http2222.send(null);
	}
}



function handleHttpResponseAgents(){
            if (http2222.readyState == 4) { 
              	var results = http2222.responseText
              	res=results.split("~");
              	tarElem=document.getElementById("supplierId");
              	tarElem.length = res.length;
           		for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['notesForm'].elements['notes.supplier'].options[i].text = '';
					document.forms['notesForm'].elements['notes.supplier'].options[i].value = '';
					}else{
					codeName = res[i].split("#");
					document.forms['notesForm'].elements['notes.supplier'].options[i].text = codeName[1].trim();;
					document.forms['notesForm'].elements['notes.supplier'].options[i].value = codeName[0].trim(); 
					
					 } 
					 }
					 document.getElementById("supplierId").value='${notes.supplier}';
             	
			}
}
function activeStatusCheck(){
	try{
	if((document.forms['notesForm'].elements['bookingAgFlag'].value=='BA') && (document.forms['notesForm'].elements['notes.bookingAgent'].checked==true)) {
		document.forms['notesForm'].elements['notes.bookingAgent'].checked=true;
	}else{
		document.forms['notesForm'].elements['notes.bookingAgent'].checked=false;
	}
	if((document.forms['notesForm'].elements['networkAgFlag'].value=='NA') && (document.forms['notesForm'].elements['notes.networkAgent'].checked==true)){
		document.forms['notesForm'].elements['notes.networkAgent'].checked=true;
	}else{
		document.forms['notesForm'].elements['notes.networkAgent'].checked=false;
	}
	if((document.forms['notesForm'].elements['originAgFlag'].value=='OA') && (document.forms['notesForm'].elements['notes.originAgent'].checked==true)){
		document.forms['notesForm'].elements['notes.originAgent'].checked=true;
	}else{
		document.forms['notesForm'].elements['notes.originAgent'].checked=false;
	}
	if((document.forms['notesForm'].elements['subOriginAgFlag'].value=='SOA') && (document.forms['notesForm'].elements['notes.subOriginAgent'].checked==true)){
		document.forms['notesForm'].elements['notes.subOriginAgent'].checked=true;
	}else{
		document.forms['notesForm'].elements['notes.subOriginAgent'].checked=false;
	}
	if((document.forms['notesForm'].elements['destAgFlag'].value=='DA') && (document.forms['notesForm'].elements['notes.destinationAgent'].checked==true)){
		document.forms['notesForm'].elements['notes.destinationAgent'].checked=true;
	}else{
		document.forms['notesForm'].elements['notes.destinationAgent'].checked=false;
	}
	if((document.forms['notesForm'].elements['subDestAgFlag'].value=='SDA') && (document.forms['notesForm'].elements['notes.subDestinationAgent'].checked==true)){
		document.forms['notesForm'].elements['notes.subDestinationAgent'].checked=true;
	}else{
		document.forms['notesForm'].elements['notes.subDestinationAgent'].checked=false;
	}try{
	   document.forms['notesForm'].elements['isBookingAgent'].disabled=false;
	   document.forms['notesForm'].elements['isNetworkAgent'].disabled=false;
	   document.forms['notesForm'].elements['isOriginAgent'].disabled=false;
	   document.forms['notesForm'].elements['isSubOriginAgent'].disabled=false;
	   document.forms['notesForm'].elements['isDestAgent'].disabled=false;
	   document.forms['notesForm'].elements['isSubDestAgent'].disabled=false;
	   }catch(e){}	
}catch(e){}
}	

function setCheckFlagBA(targetElement,type){
	var status = targetElement.checked;
	if( status == true){
		document.forms['notesForm'].elements['checkFlagBA'].value=type;
	   }
	 else{
		document.forms['notesForm'].elements['checkFlagBA'].value='N'+type;   
      }
}
function setCheckFlagNA(targetElement,type){
	var status = targetElement.checked;
	if( status == true){
		document.forms['notesForm'].elements['checkFlagNA'].value=type;
	   }
	 else{
		document.forms['notesForm'].elements['checkFlagNA'].value='N'+type;   
      }
}
function setCheckFlagOA(targetElement,type){
	var status = targetElement.checked;
	if( status == true){
		document.forms['notesForm'].elements['checkFlagOA'].value=type;
	   }
	 else{
		document.forms['notesForm'].elements['checkFlagOA'].value='N'+type;   
      }
}
function setCheckFlagSOA(targetElement,type){
	var status = targetElement.checked;
	if( status == true){
		document.forms['notesForm'].elements['checkFlagSOA'].value=type;
	   }
	 else{
		document.forms['notesForm'].elements['checkFlagSOA'].value='N'+type;   
      }
}
function setCheckFlagDA(targetElement,type){
	var status = targetElement.checked;
	if( status == true){
		document.forms['notesForm'].elements['checkFlagDA'].value=type;
	   }
	 else{
		document.forms['notesForm'].elements['checkFlagDA'].value='N'+type;   
      }
}
function setCheckFlagSDA(targetElement,type){
	var status = targetElement.checked;
	if( status == true){
		document.forms['notesForm'].elements['checkFlagSDA'].value=type;
	   }
	 else{
		document.forms['notesForm'].elements['checkFlagSDA'].value='N'+type;   
      }
}
function popupOpen(){
	location.href="myAllFiles.html?fileId=${notes.notesId}&id=${notes.id}&decorator=popup&popup=true","height=755,width=500";	
	}
function popupOpen2(){
	location.href="myAllFiles.html?fileId=${serviceOrder.shipNumber}&id=${notes.id}&decorator=popup&popup=true","height=755,width=500";
}
function checkNotesType(){
	var notesFor='${noteFor}';
	var notesType=document.forms['notesForm'].elements['notes.noteType'].value;
	 if(notesFor=='agentQuotes' && notesType!='Issue Resolution' && notesType!='Exception Service'){
	  alert('You can not change noteType')
	  document.forms['notesForm'].elements['notes.noteType'].value='Customer File';
	  linkVisible();
	  getNoteStatusDetails();
	  getSubType();
	  getSubCategoryDetails();
	}else if(notesFor!=notesType && notesFor!='ServiceOrder' && notesFor!='CustomerFile' && notesFor!='agentQuotes' && notesFor!='agentQuotesSO' ){
	  alert('You can not change noteType.')
	  document.forms['notesForm'].elements['notes.noteType'].value='${notes.noteType}';
	  linkVisible();
	  getNoteStatusDetails();
	  getSubType();
	  getSubCategoryDetails();
	}else if(notesFor=='ServiceOrder' && notesType!='Issue Resolution' && notesType!='Exception Service' && notesType!='ServiceOrder' && notesType!='Report'){
	    alert('You can not change noteType.')
		document.forms['notesForm'].elements['notes.noteType'].value='${notes.noteType}';
		linkVisible();
		getNoteStatusDetails();
		getSubType();	
		getSubCategoryDetails();
	}else if(notesFor=='CustomerFile' && notesType!='Exception Service' && notesType!='Issue Resolution' && notesType!='CustomerFile'){
		alert('You can not change noteType.')
		document.forms['notesForm'].elements['notes.noteType'].value='${notes.noteType}';
		linkVisible();
		getNoteStatusDetails();
		getSubType();
		getSubCategoryDetails();
	}else if(notesFor=='agentQuotesSO' && notesType!='Issue Resolution'&& notesType!='Exception Service' ){
		 alert('You can not change noteType.')
		  document.forms['notesForm'].elements['notes.noteType'].value='${notes.noteType}';
		  linkVisible();
		  getNoteStatusDetails();
		  getSubType();	
		  getSubCategoryDetails();
	}
	 
	 typeBasedComponent();
}
function chkCmpDate(){
	var status = document.forms['notesForm'].elements['notes.noteStatus'].value;
	var type = document.forms['notesForm'].elements['notes.noteType'].value;
	var showDiv = document.getElementById("imp");
	if(status == 'CMP' && type=='Issue Resolution'){
		showDiv.style.display = 'block';
	}else{
		showDiv.style.display = 'none';
	}
}

function typeBasedComponent(){
	var note_type = document.forms['notesForm'].elements['notes.noteType'].value;
	var checkComponet = document.forms['notesForm'].elements['issueResolutionType'].value;
	var typeBasedShowCheckBox = document.getElementById("typeBasedShowCheckBox");
	var typeBasedIssueBox = document.getElementById("typeBasedIssueBox");
	var typeBasedDetailsBox = document.getElementById("typeBasedDetailsBox");
	if(checkComponet=='Y' && note_type == 'Issue Resolution'){
		typeBasedShowCheckBox.style.display = 'block';
		typeBasedIssueBox.style.display = 'block';
		typeBasedDetailsBox.style.display='none';
	} else{
		typeBasedShowCheckBox.style.display = 'none';
		typeBasedIssueBox.style.display = 'none';
		typeBasedDetailsBox.style.display='block';
	}
}

function setCausedByVal(){
	var roleCausedByVal = document.forms['notesForm'].elements['notes.roleCausedBy'].value;
	if(roleCausedByVal == 'BRO'){
		document.forms['notesForm'].elements['notes.supplier'].value='${trackingStatus.brokerCode}';
	}else if(roleCausedByVal == 'DA'){
		document.forms['notesForm'].elements['notes.supplier'].value='${trackingStatus.destinationAgentCode}';
	}else if(roleCausedByVal == 'FRO'){
		document.forms['notesForm'].elements['notes.supplier'].value='${trackingStatus.forwarderCode}';
	}else if(roleCausedByVal == 'OA'){
		document.forms['notesForm'].elements['notes.supplier'].value='${trackingStatus.originAgentCode}';
	}else if(roleCausedByVal == 'SDA'){
		document.forms['notesForm'].elements['notes.supplier'].value='${trackingStatus.destinationSubAgentCode}';
	}else if(roleCausedByVal == 'SOA'){
		document.forms['notesForm'].elements['notes.supplier'].value='${trackingStatus.originSubAgentCode}';
	}else if(roleCausedByVal == ''){
		document.forms['notesForm'].elements['notes.supplier'].value=' ';
	}
}

</script>

</head>


<s:form id="notesForm" name="notesForm" action='${empty param.popup?"saveNotes.html":"saveNotes.html?decorator=popup&popup=true"}' method="post" validate="true" onsubmit="return checkMandatryField();" > 
<s:hidden name="formStatus"/>
<s:hidden name="checkFlagBA" />
<s:hidden name="checkFlagNA" />
<s:hidden name="checkFlagOA" />
<s:hidden name="checkFlagSOA" />
<s:hidden name="checkFlagDA" />
<s:hidden name="checkFlagSDA" />
<s:hidden name="checkSO" />
<s:hidden name="flagAcc" value="<%=request.getParameter("flagAcc") %>" />

<s:hidden name="bookingAgFlag" />
<s:hidden name="networkAgFlag" />
<s:hidden name="originAgFlag" />
<s:hidden name="subOriginAgFlag" />
<s:hidden name="destAgFlag" />
<s:hidden name="subDestAgFlag" />
<s:hidden name="notes.billToCode"/>
<s:hidden name="notes.bookingAgentCode"/>
<s:hidden name="noteFor"/>
<%-- <s:textfield name="testt" value="${trackingStatus.originAgent}"></s:textfield> --%>
<c:set var="salesPortalAccess" value="false" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>
<s:hidden name="noteFor" value="<%= request.getParameter("noteFor") %>" />
<c:set var="noteFor" value="<%= request.getParameter("noteFor") %>" />
<c:set var="notesKey" value="<%=request.getParameter("id") %>"/>
<s:hidden name="notesKey" value="<%=request.getParameter("id") %>" />
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<s:hidden  name="from"  id="from" value="<%=request.getParameter("from") %>" />
<s:hidden name="PPID" id="PPID" value="<%=request.getParameter("PPID") %>" />
<c:set var="PPID" value="<%=request.getParameter("PPID") %>"/>
<c:if test="${noteFor=='agentQuotes'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="CF"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id1") %>" />
		<s:hidden name="noteFor" id="noteFor" value="<%=request.getParameter("noteFor") %>" />
		<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
		<s:hidden name="ppType" id ="ppType" value="" />
        <c:set var="ppType" value=""/>
</c:if>
<c:if test="${noteFor=='CustomerFile'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="CF"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id1") %>" />
		<%--<s:hidden name="noteFor" id="noteFor" value="<%=request.getParameter("noteFor") %>" />
		--%><s:hidden name="ppType" id ="ppType" value="" />
        <c:set var="ppType" value=""/>
</c:if>
<c:if test="${noteFor=='ServiceOrder'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="SO"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id1") %>" />
		<s:hidden name="noteFor"  id="noteFor" value="<%=request.getParameter("noteFor") %>" />
		<s:hidden name="ppType" id ="ppType" value="" />
        <c:set var="ppType" value=""/>
</c:if>
<c:if test="${noteFor=='agentQuotesSO'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="SO"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id1") %>" />
		<s:hidden name="noteFor" id="noteFor" value="<%=request.getParameter("noteFor") %>" />
		<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
		<s:hidden name="ppType" id ="ppType" value="" />
        <c:set var="ppType" value=""/>
</c:if>
<c:if test="${noteFor=='Partner'}">
<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id="fileNameFor" value="PO"/>
		<s:hidden name="fileID" id ="fileID" value="<%=request.getParameter("id1") %>" />
		<s:hidden name="noteFor" id="noteFor" value="<%=request.getParameter("noteFor") %>" />
		<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("ppType")%>" />
        <c:set var="ppType" value="<%= request.getParameter("ppType")%>"/>
        <s:hidden name="ppCode"  id= "ppCode" value=""/>
</c:if>
<c:if test="${myFileFor=='CF'}">
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
	<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
	<s:hidden name="fileID" id ="fileID" value="%{customerFile.id}" />
</c:if>
<c:if test="${myFileFor!='CF'}"> 
	<c:if test="${empty serviceOrder.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty serviceOrder.id}">
		<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
		<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
	</c:if>	
</c:if>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
   <s:hidden name="signatureNote" value="${signatureNote}"/>
   <s:hidden name="notes.name" value="${notesFirstName} ${notesLastName}" /> 
<c:if test="${param.popup}"> 
	<c:set var="imageId" value="<%=request.getParameter("imageId") %>" />
	<s:hidden name="imageId" value="<%=request.getParameter("imageId") %>" />
	<c:set var="fieldId" value="<%=request.getParameter("fieldId") %>" />
	<s:hidden name="fieldId" value="<%=request.getParameter("fieldId") %>" />
	<c:set var="subType" value="<%=request.getParameter("subType") %>" />
	<s:hidden name="subType" value="<%=request.getParameter("subType") %>" />
	<%--<s:hidden name="noteFor" value="<%= request.getParameter("noteFor") %>" />
	--%><c:set var="noteFor" value="<%= request.getParameter("noteFor") %>" scope="session" />
</c:if>
	<s:hidden name="notes.followUpId" value="<%=request.getParameter("noteId")%>"/> 
	<c:set var="generatedfrom" value="<%=request.getParameter("generatedfrom") %>"/>
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	
		
	<c:set var="issueResolutionType" value="N"/>
	<configByCorp:fieldVisibility componentId="component.notes.issueResolution.NegativeComment">
		<s:hidden name="issueResolutionType" value="Y" />
	</configByCorp:fieldVisibility>
	
	
<%-- <c:choose>
<c:when test="${not empty param.subType}"> 
	setTimeout('setSubType("${param.subType}")',500);
</c:when>
<c:otherwise>
	setTimeout('setSubType("${notes.noteSubType}")',500);
</c:otherwise>
</c:choose> --%>
	
<c:set var="buttons"> 
	
	<sec-auth:authComponent componentId="module.section.notes.FollowUp">
	<c:if test="${!param.popup}"> 
	<c:choose>
		<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
		<c:if test="${not empty notes.id}">
			<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:90px; font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForWorkTicket.html?id=${workTicket.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
        	</c:if>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty claim.id}">
		<c:if test="${not empty notes.id}">
			<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:90px; font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForClaim.html?id=${claim.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
        	</c:if>
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty creditCard.id}">
			<c:if test="${not empty notes.id}">
			<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:90px; font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForCreditCard.html?id=${creditCard.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
        	</c:if>
		</c:when>
		<c:when test="${not empty serviceOrder.id && empty workTicket.id && empty claim.id}">
		<c:if test="${not empty notes.id}">
			<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:90px; font-size: 15"  id="addNewNote" name="addNewNote"
        	onclick="location.href='<c:url value="/editNewNoteForServiceOrder.html?id=${serviceOrder.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
        	</c:if>
		</c:when>
		<c:when test="${not empty payroll.id }">
		<c:if test="${not empty notes.id}">
			<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:90px; font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForPayroll.html?id=${payroll.id}&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
        	</c:if>
		</c:when>
		<c:when test="${not empty partnerPrivate.id}">
		<c:if test="${not empty notes.id}">
			<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:90px; font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForPartnerPrivate.html?id=${partnerPrivate.id}&notesId=${notesId}&noteFor=${noteFor}&ppType=${ppType}&PPID=${PPID}"/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
        	</c:if>
		</c:when>
		<c:when test="${not empty mss.id }">
		<c:if test="${not empty notes.id}">
			<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:90px; font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForMss.html?id=${mss.id}&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
        	</c:if>
		</c:when>
		<c:otherwise>
			<c:if test="${not empty notes.id}">
			<input type="button" class="cssbutton" id="addNewNote" name="addNewNote" style="margin-right: 0px;height: 25px;width:90px; font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNote.html?id1=${customerFile.id }&notesId=${notesId}&noteFor=${noteFor} "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
        	</c:if>
		</c:otherwise>
	 </c:choose> 
     </c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.section.notes.FollowUp">
	 <input type="button" name="emailBtn" class="cssbutton"  value="Email" size="20" style="width:60px; height:25px" onclick="emailForm();" />
	</sec-auth:authComponent>
	<s:submit cssClass="cssbutton" cssStyle="width:60px; height:25px" id="saveButton" method="save" key="button.save" onclick="enableAllField();activeStatusCheck();changeNoteImg();setbtnType(this);return checkLengthOnSave();return checkMandatryField();" />
	 </c:set>  
<s:hidden name="btntype" />
<s:hidden name="notes.id" value="${notes.id}" />
<c:set var="notes.id" value="${notes.id}"/>
<s:hidden name="maxId" />
<s:hidden name="minId" />
<s:hidden name="countId"/>
<s:hidden name="maxIdIsRal" />
<s:hidden name="minIdIsRal" />
<s:hidden name="countIdIsRal"/>	
<s:text id="notesSystemDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.systemDate" /></s:text>
<s:hidden name="notes.systemDate" value="%{notesSystemDateFormattedValue}" />
<s:hidden name="notes.corpID" />

<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
<c:set var="id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="id1" value="<%=request.getParameter("id1") %>"/>
<c:set var="id1" value="<%=request.getParameter("id1") %>"/>

<s:hidden name="notesId" value="<%=request.getParameter("notesId") %>"/>
<s:set name="notesId" value="<%=request.getParameter("notesId") %>"/>
<s:hidden name="isRal" value="<%=request.getParameter("isRal") %>"/>
<c:set var="isRal" value="<%=request.getParameter("isRal") %>"/>
<s:hidden name="accountNotesFor" value="<%=request.getParameter("accountNotesFor") %>"/>
<c:set var="accountNotesFor" value="<%=request.getParameter("accountNotesFor") %>"/>
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="payroll.id" value="<%=request.getParameter("id1") %>"/>
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="workTicket.id" />
<s:hidden name="claim.id" />
<s:hidden name="creditCard.id" />
<s:hidden name="container.id" />
<s:hidden name="carton.id" />
<s:hidden name="partnerPrivate.id"/>
<s:hidden name="vehicle.id" />
<s:hidden name="accountLine.id" />
<s:hidden name="servicePartner.id" />
<s:hidden name="notes.customerNumber" />
<s:hidden name="notes.reminderStatus" />
<s:hidden name="notes.networkLinkId" />
<s:hidden name="notes.myFileId" />
<s:hidden name="linkFile" value="${notes.myFileId}"  />	
<c:set var="noteFrom" value="${noteFrom}" scope="session"/>
<s:hidden name="mss.id" />
<s:hidden name="dsFamilyDetails.id" />
<s:hidden name="noteForNextPrev"/>
<s:hidden name="notes.toDoRuleId" />
<sec-auth:authComponent componentId="module.script.form.NotesScript">
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:set var="idOfTasks" value="" scope="session"/>
</sec-auth:authComponent>

<s:hidden name="notes.notesKeyId" />
<c:if test="${!param.popup}">

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
	<c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
	<c:redirect url="/customerServiceOrders.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.raterequest' }">
	<c:redirect url="/customerRateOrders.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.surveys' }">
	<c:redirect url="/surveysList.html?id1=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.serviceorderdtl' }">
	<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
	<c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
	<c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.newAccounting' }">
	<c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.forwarding' }">
	<c:if test="${forwardingTabVal!='Y'}">
		<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
	</c:if>
	<c:if test="${forwardingTabVal=='Y'}">
		<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
	<c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
			<c:if test="${serviceOrder.job =='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}"> 
				<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
			</c:if>	
</c:when>
<c:when test="${gotoPageString == 'gototab.ticket' }">
	<c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
	<c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.socustomerfile' }">
	<c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.workticket' }">
	<c:redirect url="/editWorkTicketUpdate.html?id=${workTicket.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<c:choose>
<c:when test="${noteFor=='agentQuotesSO'}">
	<div id="newmnav">
				  <ul>
				     <sec-auth:authComponent componentId="module.tab.notes.quotationFileTab">
		    				<li ><a href="QuotationFileForm.html?id=${customerFile.id}"><span>Quotation File</span></a></li>
		    		</sec-auth:authComponent>
		    		 <sec-auth:authComponent componentId="module.tab.notes.serviceOrdersTab">
		    				<li id="newmnav1" style="background:#FFF "><a href="quotationServiceOrders.html?id=${customerFile.id}" class="current"><span>Quotes<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    		</sec-auth:authComponent>
				    
				  </ul>
				   </div><div style="width: 770px"><div class="spn">&nbsp;</div></div>
				  </c:when>
<c:when test="${noteFor=='agentQuotes'}">
	<div id="newmnav">
				   <ul>
				   <sec-auth:authComponent componentId="module.tab.notes.quotationFileTab">
		    			<li id="newmnav1" style="background:#FFF "><a href="QuotationFileForm.html?id=${customerFile.id}"  class="current"><span>Quotation File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    		</sec-auth:authComponent>
		    		<sec-auth:authComponent componentId="module.tab.notes.serviceOrdersTab">
		    			<li><a href="quotationServiceOrders.html?id=${customerFile.id} "><span>Quotes</span></a></li>
		    		</sec-auth:authComponent>
				    
				  </ul>
				    </div><div style="width: 770px"><div class="spn">&nbsp;</div></div>
				  </c:when>
<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
		<div id="newmnav">
				  <ul>
				  <sec-auth:authComponent componentId="module.tab.notes.ticketTab">
				  <c:if test="${serviceOrder.job !='RLO'}">
		    			<li id="newmnav1" style="background:#FFF "><a class="current" onclick="setReturnString('gototab.workticket');return checkLengthOnSave('none');"><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    			</c:if>
		    	</sec-auth:authComponent>
			  	   </ul>
		</div><div style="width: 790px"><div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
		</div>
	</c:when>
		<c:when test="${noteFor=='Claim'}">	
		<div id="newmnav">
				  <ul>
				  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
				  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
				  <c:if test="${serviceOrder.job !='RLO'}">
		    	   <li id="newmnav1" style="background:#FFF "><a onclick="return checkLengthOnSave('none');" href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
		    	   </c:if>
		    			</c:if>
		    	</sec-auth:authComponent>
			  	   </ul>
		</div><div style="width: 790px"><div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
		</div>
	</c:when>
	
<c:when test="${not empty serviceOrder.id && empty workTicket.id}">
		<div id="newmnav">
		  <ul>
		  <sec-auth:authComponent componentId="module.tab.notes.serviceOrderDetailsTab">
		    	 <li id="newmnav1" style="background:#FFF"><a onclick="setReturnString('gototab.serviceorderdtl');return checkLengthOnSave('none');" class="current"><span>S/O Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.notes.serviceOrderDetailsTab.external">
		    	<li id="newmnav1" style="background:#FFF "><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" class="current"><span>S/O Details<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		 </sec-auth:authComponent>
		     
      <c:if test="${userType =='ACCOUNT'}">
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
          <sec-auth:authComponent componentId="module.accountingPortalTab.serviceorder.operationResourceTab">
     <li><a href="operationResourceFromAcPortal.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
      </sec-auth:authComponent>
      </c:if>
         </c:if> 
		     
		     <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
          </sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.notes.billingTab">
		     <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
			  	<li><a onclick="setReturnString('gototab.billing');return checkLengthOnSave('none');"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.notes.accountingTab">
		<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		    	<li><a onclick="setReturnString('gototab.accounting');return checkLengthOnSave('none');"><span>Accounting</span></a></li>
		</c:if>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		    	<li><a onclick="setReturnString('gototab.newAccounting');return checkLengthOnSave('none');"><span>Accounting</span></a></li>
		</c:if>
		</sec-auth:authComponent>
		
		  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	              </c:if>
	              </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	         <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	          </c:if>	
		<sec-auth:authComponent componentId="module.tab.notes.forwardingTab">
		 <c:if test="${OIFlag == true }">
 	             <c:if test="${serviceOrder.corpID!='CWMS' ||(fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">    
		<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		<c:if test="${userType!='ACCOUNT' && serviceOrder.job !='RLO'}">
		    <c:if test="${forwardingTabVal!='Y'}">
				<li><a onclick="return checkLengthOnSave('none');" href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			</c:if>
			<c:if test="${forwardingTabVal=='Y'}">
				<li><a onclick="return checkLengthOnSave('none');" href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			</c:if>
		</c:if>
		<c:if test="${userType=='ACCOUNT' && serviceOrder.job !='RLO'}">
                <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
        </c:if>
		    	</c:if>
		    	</c:if></c:if>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.notes.domesticTab">
		   <c:if test="${OIFlag == true }">
		    	<c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		    	<c:if test="${serviceOrder.job !='RLO'}">
			  <li><a onclick="setReturnString('gototab.domestic');return checkLengthOnSave('none');"><span>Domestic</span></a></li>
			  </c:if>
			  </c:if>
			  </c:if>
	    </sec-auth:authComponent>
	    <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
         <c:if test="${serviceOrder.job =='INT'}">
         <li><a onclick="setReturnString('gototab.domestic');return checkLengthOnSave('none');"><span>Domestic</span></a></li>
         </c:if>
        </sec-auth:authComponent>
	    <sec-auth:authComponent componentId="module.tab.notes.statusTab">
	     <c:if test="${userType !='ACCOUNT'}">
		    	 <li><a onclick="setReturnString('gototab.status');return checkLengthOnSave('none');" ><span>Status</span></a></li>
		 </c:if>
		 </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.notes.summaryTab">
		  <c:if test="${OIFlag == true }">
		  <li><a href="findSummaryList.html?id=${serviceOrder.id}" ><span>Summary</span></a></li>
		  </c:if>
		 </sec-auth:authComponent>
		 <sec-auth:authComponent componentId="module.tab.notes.ticketTab">
		 <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		 <c:if test="${serviceOrder.job !='RLO'}">
		    	 <li><a onclick="setReturnString('gototab.ticket');return checkLengthOnSave('none');"><span>Ticket</span></a></li>
		    	 </c:if>
		    	 </c:if>
		  </sec-auth:authComponent>
		  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
		  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
		  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		  <c:if test="${serviceOrder.job !='RLO'}">
		   <li><a onclick="return checkLengthOnSave('none');" href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
		   </c:if>
		   </c:if>
		   </sec-auth:authComponent>
		   </configByCorp:fieldVisibility>
		   <sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
		    	 <li><a onclick="return checkLengthOnSave('none');" href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
		    </sec-auth:authComponent>
		    <!--<sec-auth:authComponent componentId="module.tab.notes.reportTab">
		    	 <li><a onclick="openWindow('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&reportModule=customerFile&reportSubModule=customerFile&decorator=popup&popup=true',750,400)"><span>Forms</span></a></li>
		    </sec-auth:authComponent>
          --><sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
          </sec-auth:authComponent>
			</ul>
		</div><div style="width: 790px"><div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
		</div>
	</c:when>
<c:otherwise>
<c:if test="${empty payroll.id && empty partnerPrivate.id }">
		<div id="newmnav">
				  <ul>
				  <sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
		    			<li id="newmnav1" style="background:#FFF "><a onmouseover="completeTimeString();" onclick=" return checkLengthOnSave('none');" class="current" href="editCustomerFile.html?id=${customerFile.id}"  ><span>Customer File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    	  </sec-auth:authComponent>
		    	  <sec-auth:authComponent componentId="module.tab.notes.serviceOrderTab">
		    			<li><a onmouseover="completeTimeString();" onclick="return checkLengthOnSave('none');" href="customerServiceOrders.html?id=${customerFile.id}"><span>Service Order</span></a></li>
		    	  </sec-auth:authComponent>
		    	  <sec-auth:authComponent componentId="module.tab.notes.rateRequestTab">
		    			<li><a onmouseover="completeTimeString();" onclick="setReturnString('gototab.raterequest');return checkLengthOnSave('none');"><span>Rate Request</span></a></li>
		    	  </sec-auth:authComponent>
		    	  <sec-auth:authComponent componentId="module.tab.notes.surveysTab">
		    			<li><a onmouseover="completeTimeString();" onclick="setReturnString('gototab.surveys');return checkLengthOnSave('none');"><span>Surveys</span></a></li>
		    	</sec-auth:authComponent><!--
		    	<sec-auth:authComponent componentId="module.tab.notes.reportTab">
		    		  <li><a onmouseover="completeTimeString();" onclick="setReturnString('gototab.reports');return checkLengthOnSave('none');"><span>Forms</span></a></li>
		        </sec-auth:authComponent>
				  --></ul>
		</div><div style="width: 750px"><div class="spn">&nbsp;</div>
		<div style="padding-bottom:3px;"></div>
		</div>
		</c:if>
</c:otherwise>
</c:choose>

<c:choose>
<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
    <div class="center-content">
		<table class="" cellspacing="1" cellpadding="0" border="0" style="width: 790px">
			<tbody>
			<tr><td align="left" class="listwhitebox">
				<table class="detailTabLabel" border="0" width="100%">
				  <tbody>  	
				  	<tr>
				  	<td align="left" height="5px"></td>
				  	</tr>
				  	<tr>
				  	<td align="right"><fmt:message key="billing.shipper"/></td>
				  	<td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"  size="19"  cssClass="input-textUpper" readonly="true"  onfocus="onFormLoad();setTimeMask();"/>
				  	<td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="14" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.originCountry"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="13" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="3" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.Type"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="3" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.commodity"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="8" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.routing"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="8" readonly="true"/></td>
				  	</tr>
				  	<tr>
				  	<td align="right"><fmt:message key="billing.jobNo"/></td>
				  	<td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="15" readonly="true"/></td>
				  	<s:hidden name="serviceOrder.sequenceNumber" /><s:hidden name="serviceOrder.ship" />
				  	<td align="right"><fmt:message key="billing.registrationNo"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="14" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.destination"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="13" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="3" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.mode"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="3" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.AccName"/></td>
				  	<td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="32" readonly="true"/></td>
				  	</tr>
				  	<tr>
				  	<td align="left" height="5px"></td>
				  	</tr>
		   		  </tbody>
			  </table>
			  </td></tr>
			</tbody>
		 </table> 
		 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	</c:when>
<c:when test="${not empty serviceOrder.id && empty workTicket.id}">
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
   <div class="center-content">
		<table class="" cellspacing="1" cellpadding="0" border="0" style="width: 790px">
			<tbody>
			<tr><td align="left" class="listwhitebox">
				<table class="detailTabLabel" border="0" width="100%">
				  <tbody>  	
				  	<tr>
				  	<td align="left" height="5px"></td>
				  	</tr>
				  	<tr>
				  	<td align="right"><fmt:message key="billing.shipper"/></td>
				  	<td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"  size="19"  cssClass="input-textUpper" readonly="true"  onfocus="onFormLoad();setTimeMask();"/>
				  	<td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="14" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.originCountry"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="13" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="3" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.Type"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="3" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.commodity"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="8" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.routing"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="8" readonly="true"/></td>
				  	</tr>
				  	<tr>
				  	<td align="right"><fmt:message key="billing.jobNo"/></td>
				  	<td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="15" readonly="true"/></td>
				  	<s:hidden name="serviceOrder.sequenceNumber" /><s:hidden name="serviceOrder.ship" />
				  	<td align="right"><fmt:message key="billing.registrationNo"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="14" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.destination"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="13" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="3" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.mode"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="3" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.AccName"/></td>
				  	<td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="32" readonly="true"/></td>
				  	</tr>
				  	<tr>
				  	<td align="left" height="5px"></td>
				  	</tr>
		   		  </tbody>
			  </table>
			  </td></tr>
			</tbody>
		 </table> 
		 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	</c:when>
<c:otherwise>
<div id="content" align="center">
<c:if test="${empty payroll.id && empty partnerPrivate.id }">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
     <div class="center-content">
		<table class="" cellspacing="1" cellpadding="0" border="0">
			<tbody>
			<tr><td align="left" class="listwhitebox">
				<table class="detailTabLabel" border="0">
				  <tbody>  	
				  	<tr>
						<td>Shipper</td>
						<td><s:textfield cssClass="input-textUpper"  name="customerFile.firstName" required="true" size="20" readonly="true" onfocus="onFormLoad();setTimeMask();"/></td>
						<td><s:textfield cssClass="input-textUpper"  name="customerFile.lastName" required="true" size="20" readonly="true"/></td>
						<td>Origin</td>
						<td><s:textfield cssClass="input-textUpper"  name="customerFile.originCityCode" required="true" size="25" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper"  name="customerFile.originCountryCode" required="true" size="3" readonly="true"/></td>
						<td>Type</td>
						<td><s:textfield cssClass="input-textUpper"  name="customerFile.job" required="true" size="3" readonly="true"/></td>
						<td>Destination</td>
						<td><s:textfield cssClass="input-textUpper"  name="customerFile.destinationCityCode" required="true" size="25" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper"  name="customerFile.destinationCountryCode" required="true" size="3" readonly="true"/></td>
					</tr>
				  </tbody>
				 </table>
				</td>
			</tr>
			</tbody>
		</table>
				 </div>
				 
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>
</c:otherwise>
</c:choose>
<c:if test="${isRal}" >	
<div id="newmnav" style="float:left">
				  <ul>
				 <sec-auth:authComponent componentId="module.tab.notes.notesListTab">
				    <li><a href="notess.html?id=${idOfWhom }&notesId=${notesId}&noteFor=${noteFor}"><span>List</span></a></li>
				 </sec-auth:authComponent> 	
				   	<li id="newmnav1" style="background:#FFF "><a  class="current" ><span>Notes<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<c:if test="${noteFor=='CustomerFile'}">
					<li><a href="raleventNotess.html?id=${customerFile.id}&customerNumber=${customerFile.sequenceNumber}&noteFor=${noteFor}&noteFrom=${noteFrom}"><span>Related Notes List</span></a></li>
				    </c:if>
				    <c:if test="${noteFor=='ServiceOrder' && not empty serviceOrder.id}">
				    <li><a href="raleventNotess.html?id=${serviceOrder.id}&customerNumber=${customerFile.sequenceNumber}&noteFor=${noteFor}&noteFrom=${noteFrom}"><span>Related Notes List</span></a></li>
				    </c:if>
				    <c:if test="${noteFor=='ServiceOrder' && empty serviceOrder.id}">
				    <li><a href="raleventNotess.html?id=${idOfWhom}&customerNumber=${customerFile.sequenceNumber}&noteFor=${noteFor}&noteFrom=${noteFrom}"><span>Related Notes List</span></a></li>
				    </c:if>
				   
				  	<li><a onclick="window.open('auditList.html?id=${notes.id}&tableName=notes&decorator=popup&popup=true','audit','height=600,width=825,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
				   
				  </ul>
		</div>
		
		
<c:choose>
<c:when test="${noteForNextPrev=='WorkTicket' && empty payroll.id && salesPortalAccess=='false'}">
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
	    <c:if test="${not empty notes.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${notes.id > minIdIsRal}" >
  		<a><img align="middle" onclick="goPrevIsRalTicket();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${notes.id == minIdIsRal}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
     	<td width="20px" align="left">
  		<c:if test="${notes.id < maxIdIsRal}" >
  		<a><img align="middle" onclick="goNextIsRalTicket();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${notes.id == maxIdIsRal}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countIdIsRal != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSOIsRalTicket(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Related Ticket Notes List" title="Related Ticket Notes List" /></a> 
		</c:if>
		<c:if test="${countIdIsRal == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if>
		</tr></table>
		<div class="spn" >&nbsp;</div>
		</c:when>
		<c:when test="${noteForNextPrev=='CustomerFile' && empty payroll.id && salesPortalAccess=='false'}">
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
	    <c:if test="${not empty notes.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${notes.id > minIdIsRal}" >
  		<a><img align="middle" onclick="goPrevIsRal();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${notes.id == minIdIsRal}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${notes.id < maxIdIsRal}" >
  		<a><img align="middle" onclick="goNextIsRal();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${notes.id == maxIdIsRal}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countIdIsRal != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSOIsRal(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Related Customer Notes List" title="Related Customer Notes List" /></a> 
		</c:if>
		<c:if test="${countIdIsRal == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if>
		</tr></table>
		<div class="spn" >&nbsp;</div>
		</c:when>
		<c:otherwise>
		<c:if test="${salesPortalAccess=='false'}">
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;"><tr>
		
	    <c:if test="${not empty notes.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${notes.id > minIdIsRal}" >
  		<a><img align="middle" onclick="goPrevIsRalServiceOrder();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${notes.id == minIdIsRal}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${notes.id < maxIdIsRal}" >
  		<a><img align="middle" onclick="goNextIsRalServiceOrder();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${notes.id == maxIdIsRal}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countIdIsRal != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSOIsRalServiceOrder(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Related SO Notes List" title="Related SO Notes List" /></a> 
		</c:if>
		<c:if test="${countIdIsRal == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if>
		</tr></table>
		</c:if>
		<div class="spn" >&nbsp;</div>
		
		</c:otherwise>
		</c:choose>
</c:if>

<c:if test="${!isRal}" >	
<div id="newmnav" style="float:left;!margin-bottom:-17px;">
				  <ul><!--
				   <c:if test="${not empty partnerPrivate.id }">
				   <sec-auth:authComponent componentId="module.tab.notes.notesListTab">
				    <li id="newmnav1" style="background:#FFF;margin-bottom:2px; "><a class="current" href="notess.html?id=${idOfWhom}&notesId=${notesId}&noteFor=Partner"><span>List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   </sec-auth:authComponent>
				   </c:if>
				    --><c:if test="${ not empty payroll.id}">
				   <sec-auth:authComponent componentId="module.tab.notes.notesListTab">
				    <li id="newmnav1" style="background:#FFF;margin-bottom:2px; "><a class="current" href="notess.html?id=${idOfWhom}&notesId=${notesId}&noteFor=Crew"><span>List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   </sec-auth:authComponent>
				   </c:if>
				    <c:if test="${empty payroll.id && empty partnerPrivate.id }">
				   <sec-auth:authComponent componentId="module.tab.notes.notesListTab">
				    <li id="newmnav1" style="background:#FFF;margin-bottom:2px; "><a class="current" href="notess.html?id=${idOfWhom}&notesId=${notesId}&noteFor=${noteFor}"><span>List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   </sec-auth:authComponent>
				   </c:if>
				     <c:if test="${empty payroll.id && not empty partnerPrivate.id }">
				   <sec-auth:authComponent componentId="module.tab.notes.notesListTab">
				    <li id="newmnav1" style="background:#FFF;margin-bottom:2px; "><a class="current" href="notess.html?id=${idOfWhom}&notesId=${notesId}&noteFor=${noteFor}&ppType=${ppType}&PPID=${PPID}"><span>List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   </sec-auth:authComponent>
				   </c:if>
				   <c:if test="${empty payroll.id && empty partnerPrivate.id }">
				   	<!--<li><a href="raleventNotess.html?id=${customerFile.id}&customerNumber=${customerFile.sequenceNumber}&noteFor=${noteFor}&noteFrom=${noteFrom}"><span>Related Notes</span></a></li>
		 			-->
		 			<c:if test="${noteFor=='CustomerFile'}">
					<li><a href="raleventNotess.html?id=${customerFile.id}&customerNumber=${customerFile.sequenceNumber}&noteFor=${noteFor}&noteFrom=${noteFrom}"><span>Related Notes</span></a></li>
				    </c:if>
				    <c:if test="${noteFor=='ServiceOrder' && not empty serviceOrder.id }">
				    <li><a href="raleventNotess.html?id=${serviceOrder.id}&customerNumber=${customerFile.sequenceNumber}&noteFor=${noteFor}&noteFrom=${noteFrom}"><span>Related Notes</span></a></li>
				    </c:if>
				    <c:if test="${noteFor=='ServiceOrder' && empty serviceOrder.id }">
				    <li><a href="raleventNotess.html?id=${idOfWhom}&customerNumber=${customerFile.sequenceNumber}&noteFor=${noteFor}&noteFrom=${noteFrom}"><span>Related Notes</span></a></li>
				    </c:if>
		 			</c:if> 
		 			<c:if test="${empty payroll.id && empty partnerPrivate.id }">
		 			<li><a onclick="openWindow('auditList.html?id=${notes.id}&tableName=notes&decorator=popup&popup=true','audit','height=600,width=825,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
				 </c:if> 
				  </ul>
		</div>
	
		
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float:left;"><tr>
		
	<c:if test="${not empty notes.id}">
	<c:if test="${noteForNextPrev=='ServiceOrder'}">
	 	<td width="20px" align="right">
	 	<c:if test="${notes.id > minId}" >
  		<a><img align="middle" onclick="goPrevServiceNotes();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${notes.id == minId}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
     	<td width="20px" align="left">
  		<c:if test="${notes.id < maxId}" >
  		<a><img align="middle" onclick="goNextServiceNotes();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${notes.id == maxId}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countId != 1}" >
		<a><img class="openpopup" onclick="findSeviceOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="SO Notes List" title="SO Notes List" /></a> 
		</c:if>
		<c:if test="${countId == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if>
		<c:if test="${noteForNextPrev=='WorkTicket'}">
	 	<td width="20px" align="right">
	 	<c:if test="${notes.id > minId}" >
  		<a><img align="middle" onclick="goPrevTicketNotes();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${notes.id == minId}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
     	<td width="20px" align="left">
  		<c:if test="${notes.id < maxId}" >
  		<a><img align="middle" onclick="goNextTicketNotes();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${notes.id == maxId}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countId != 1}" >
		<a><img class="openpopup" onclick="findTicketOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Ticket Notes List" title="Ticket Notes List" /></a> 
		</c:if>
		<c:if test="${countId == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if>
		
		<c:if test="${noteForNextPrev=='CustomerFile'}">
		<td width="20px" align="right">
	 	<c:if test="${notes.id > minId}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${notes.id == minId}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${notes.id < maxId}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${notes.id == maxId}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countId != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer Notes List" title="Customer Notes List" /></a> 
		</c:if>
		<c:if test="${countId == 1}" >
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		</c:if>
		</c:if>
		</tr></table>
		<div class="spn">&nbsp;</div>
</c:if>

</c:if>

<div id="Layer1" onkeydown="changeStatus();">

<table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0" height="250" width="747" style="!margin-top:5px;">
	
	<tbody>
	
	<tr class="subcontent-tab"><td ><font style="padding-left:10px ">Notes</font></td></tr>
		<tr>
			<td style="margin:0; ">
				<table cellspacing="0" border="0" cellpadding="1" style="margin:0;padding: 0px;">
				<sec-auth:authComponent componentId="module.section.notes.FollowUp">
				<tr>
				<td style="margin:0;padding:0px;">
				<table  cellspacing="0" cellpadding="5"  border="0" style="margin-bottom:0;">
				
					<tr>
						<c:set var="isSurveyFlag" value="false"/>
						<c:set var="isTicketFlag" value="false"/>
						<c:set var="isInvoiceFlag" value="false"/>
						<c:set var="isPortalFlag" value="false"/>
						<c:set var="isQuoteFlag" value="false"/>
						<c:set var="isAccPortal" value="false"/>
						<c:set var="isPartnerPortal" value="false"/>
						
						<c:if test="${notes.displaySurvey}">
						 	<c:set var="isSurveyFlag" value="true"/>
						</c:if>
						<c:if test="${notes.displayTicket}">
						 	<c:set var="isTicketFlag" value="true"/>
						</c:if>
						<c:if test="${notes.displayInvoice}">
						 	<c:set var="isInvoiceFlag" value="true"/>
						</c:if>
						<c:if test="${notes.displayPortal}">
						 	<c:set var="isPortalFlag" value="true"/>
						</c:if>
						<c:if test="${notes.displayQuote}">
						 	<c:set var="isQuoteFlag" value="true"/>
						</c:if>
						<c:if test="${notes.accPortal}">
						 	<c:set var="isAccPortal" value="true"/>
						</c:if>
						<c:if test="${notes.partnerPortal}">
						 	<c:set var="isPartnerPortal" value="true"/>
						</c:if>

						<c:if test="${notes.originAgent}">
						 	<c:set var="isOriginAgent" value="true"/>
						</c:if>
						<c:if test="${notes.destinationAgent}">
						 	<c:set var="isDestAgent" value="true"/>
						</c:if>
						<c:if test="${notes.bookingAgent}">
						 	<c:set var="isBookingAgent" value="true"/>
						</c:if>
						<c:if test="${notes.subOriginAgent}">
						 	<c:set var="isSubOriginAgent" value="true"/>
						</c:if>
						<c:if test="${notes.subDestinationAgent}">
						 	<c:set var="isSubDestAgent" value="true"/>
						</c:if>
						<c:if test="${notes.networkAgent}">
						 	<c:set var="isNetworkAgent" value="true"/>
						</c:if>


						<td align="right" class="listwhitetext"><fmt:message key="notes.notesId"/></td>
						<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text"  name="notes.notesId" id="editorValue" cssStyle="width:104px" maxlength="15" readonly="true" /></td>
						
						
						<c:if test="${accountNotesFor!='AC'}">
						
						<td align="right" class="listwhitetext" width=""><fmt:message key="notes.noteStatus"/></td>
						<td align="left" class="listwhitetext" colspan="5"> <s:select cssClass="list-menu" name="notes.noteStatus" list="%{notestatus}" cssStyle="width:105px" headerKey="" headerValue="" onchange="chkCmpDate();changeStatus();"/></td>
					</c:if>
					<c:if test="${accountNotesFor=='AC'}">
					<s:hidden name="notes.noteStatus" />	
					</c:if>
						</tr>
						<tr>
												
						</tr>
						<tr>
						<c:if test="${!param.popup}">
							<c:if test="${not empty checkCF }">
								<td align="right" class="listwhitetext" style="width:42px;"><fmt:message key="notes.noteType"/><font color="red" size="2">*</font></td>
								<td align="left" class="listwhitetext" width="75px"> <s:select cssClass="list-menu" id="noteType_empty_checkCF" name="notes.noteType" list="%{notetype}" cssStyle="width:105px;height:20px" onchange="changeStatus();getSubType();getNoteStatusDetails();linkVisible();getSubCategoryDetails();getGradingDetails();checkNotesType();chkCmpDate();"/></td>
								
							</c:if>	
							<c:if test="${empty checkCF }">
								<td align="right" class="listwhitetext" style="width:42px;"><fmt:message key="notes.noteType"/><font color="red" size="2">*</font></td>
								<td align="left" class="listwhitetext" width="75px"> <s:select cssClass="list-menu" id="noteType_not_empty_checkCF" name="notes.noteType" list="%{notetypeNotForCF}" cssStyle="width:105px;height:20px"  onchange="changeStatus();getSubType();checkNotesType();"/></td>
							</c:if>	
							<c:if test="${empty payroll.id}">				
							<td align="right" class="listwhitetext" width="55px" style="!padding:0px">
							<div id="noteSubTypeForAll">
							<fmt:message key="notes.noteSubType"/>
							</div>
							<div id="noteSubTypeForExceptionService" style="display:none">
							<fmt:message key="notes.noteCategory" />
							</div>
							</td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.noteSubType" list="%{notesubtype}" cssStyle="width:105px;"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
							</c:if>
							<c:if test="${not empty payroll.id}">
							<td align="right" class="listwhitetext" width="55px" style="!padding:0px">
						    <div id="noteSubTypeForAll">
							<fmt:message key="notes.noteSubType"/>
							</div>
							<div id="noteSubTypeForExceptionService" style="display:none">
							<fmt:message key="notes.noteCategory" />
							</div>
							</td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.noteSubType" list="%{notesubtype}" cssStyle="width:105px;" onchange="changeStatus();"/></td>
							</c:if>
						</c:if>
						
						<c:if test="${param.popup && generatedfrom == 'activityManagement'}">
						<%--<s:hidden name="noteFor" value="<%= request.getParameter("noteFor") %>" />
						--%><c:set var="noteFor" value="<%= request.getParameter("noteFor") %>" scope="session" />
									<td align="right" class="listwhitetext" style="width:42px; !width:95px"><fmt:message key="notes.noteType"/><font color="red" size="2">*</font></td>
								<td align="left" class="listwhitetext" width="75px"> <s:textfield cssClass="input-text" name="notes.noteType" cssStyle="width:105px;height:15px" readonly="true" /></td>
							
								
							<c:if test="${accountNotesFor=='AC'}">
							<td align="right" class="listwhitetext" width="102px" style="!padding:0px">Type Of Contact</td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.noteSubType" list="%{notesubtypecontact}" cssStyle="width:105px;"  onchange="changeStatus();"/></td>
							</c:if>
							
							<c:if test="${accountNotesFor!='AC'}">
							<c:if test="${empty payroll.id}">
							<td align="right" class="listwhitetext" width="55px" style="!padding:0px">
							<div id="noteSubTypeForAll">
							<fmt:message key="notes.noteSubType"/>
							</div>
							<div id="noteSubTypeForExceptionService" style="display:none">
							<fmt:message key="notes.noteCategory" />
							</div>
							</td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.noteSubType" list="%{notesubtype}" cssStyle="width:105px;"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
							</c:if>
							<c:if test="${not empty payroll.id}">
							<td align="right" class="listwhitetext" width="55px" style="!padding:0px">
							<div id="noteSubTypeForAll">
							<fmt:message key="notes.noteSubType"/>
							</div>
							<div id="noteSubTypeForExceptionService" style="display:none">
							<fmt:message key="notes.noteCategory" />
							</div>
							</td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.noteSubType" list="%{notesubtype}" cssStyle="width:105px;"  onchange="changeStatus();"/></td>
							</c:if>
							</c:if><td></td>
						</c:if>
						
						
						<c:if test="${param.popup && generatedfrom != 'activityManagement'}">
							<td align="right" class="listwhitetext" width="42"><fmt:message key="notes.noteType"/><font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext" width="75px"> <s:textfield cssClass="input-text" name="notes.noteType" cssStyle="width:105px;" readonly="true" /></td>
							
							<c:if test="${accountNotesFor=='AC'}">
							<td align="right" class="listwhitetext" width="102px" style="!padding:0px">Type Of Contact</td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.noteSubType" list="%{notesubtypecontact}" cssStyle="width:91px;"  onchange="changeStatus();"/></td>
							</c:if>
							
							<c:if test="${accountNotesFor!='AC'}">
							<c:if test="${empty payroll.id}">
							<td align="right" class="listwhitetext" width="55px" style="!padding:0px">
							<div id="noteSubTypeForAll">
							<fmt:message key="notes.noteSubType"/>
							</div>
							<div id="noteSubTypeForExceptionService" style="display:none">
							<fmt:message key="notes.noteCategory" />
							</div>
							</td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.noteSubType" list="%{notesubtype}" cssStyle="width:105px;"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
							</c:if>
							<c:if test="${not empty payroll.id}">
							<td align="right" class="listwhitetext" width="55px" style="!padding:0px"><fmt:message key="notes.noteSubType"/></td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.noteSubType" list="%{notesubtype}" cssStyle="width:105px;"  onchange="changeStatus();"/></td>
							</c:if>
							</c:if><td></td>
						</c:if>
						</tr>
						
						
						
						
						
						
						<tr>
						<td colspan="4">
						
						</td>
						
						</tr>
						</table>
				</td>
			  <td style="vertical-align: top; padding-top: 6px;">
			  <fieldset id="linkto" style="!height:55px;margin:0px;padding:0px 0px 2px 0px;">
				<c:if test="${not empty checkCF && notes.noteType!='Partner' }">
				<s:hidden name="checkCF" value="${checkCF}" />
				<c:set var="checkCF" value="${checkCF}" scope="request" />
				<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="2" width="100%" style="margin-bottom: 0px;" >
				
				<tr>
					<td align="right" class="listwhitetext">Linked&nbsp;To</td>
				<td align="left" class="listwhitetext">
				<s:select cssClass="list-menu" name="notes.linkedTo" list="%{linkedToList}" cssStyle="width:100px;"  headerKey="" headerValue="" onchange="changeStatus();getSubCategorySoDetails();getGradingDetails();findNewAgents();"/>
				</td>
					<td align="right" class="listwhitetext" width="" style="!padding:0px">
					<div id="GradingId" >
					Grading
					</div>
					<div id="ApprovedId" >
					Approved&nbsp;By&nbsp;/&nbsp;Denied&nbsp;By
					</div>
					</td>
					<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.grading" list="%{grading}" cssStyle="width:75px;"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
							
						</tr>
						
						<tr>	
						<td align="right" class="listwhitetext" width="60" style="!padding:0px">
						<div id="IssueTypeId" >
						Issue&nbsp;Type
						</div>
						<div id="SubCategoryId" >
						Sub-Category
						</div>						
						</td>
						<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.issueType" list="%{issueType}" cssStyle="width:100px;"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
						<td align="right" class="listwhitetext" width="53" style="!padding:0px">Caused&nbsp;By</td>
						<td align="left" class="listwhitetext" > <s:select id="supplierId" cssClass="list-menu" name="notes.supplier" list="%{supplier}" cssStyle="width:75px;"  onchange="changeStatus();"/></td>
						</tr>
						
						</table>
				</c:if>
				</fieldset>
				</td><!--
				 <td id="linktoException">
				<c:if test="${not empty checkCF && notes.noteType!='Partner' }">
				<s:hidden name="checkCF" value="${checkCF}" />
				<c:set var="checkCF" value="${checkCF}" scope="request" />
				<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="4" width="100%" style="margin-bottom: 0px;" >
				
				<tr>
					<td align="right" class="listwhitetext" style="!padding:0px">Linked To</td>
				<td align="left" class="listwhitetext">
				<s:select cssClass="list-menu" name="notes.linkedTo" list="%{linkedToList}" cssStyle="width:110px;"  headerKey="" headerValue="" onchange="changeStatus(); findNewAgents();"/>
				</td>
					<td align="right" class="listwhitetext" width="" style="!padding:0px">Approved By / Denied By</td>
					<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.grading" list="%{grading}" cssStyle="width:105px;"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
							
						</tr>
						
						<tr>	
						<td align="right" class="listwhitetext" width="60" style="!padding:0px">Sub-Category</td>
						<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.issueType" list="%{issueType}" cssStyle="width:110px;"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
						
						<td align="right" class="listwhitetext" width="53" style="!padding:0px">Caused By</td>
						<td align="left" class="listwhitetext" > <s:select id="supplierId" cssClass="list-menu" name="notes.supplier" list="%{supplier}" cssStyle="width:105px;"  onchange="changeStatus();"/></td>
						</tr>
						
						</table>
				</c:if>
				</td>
				--><c:if test="${accountNotesFor!='AC'}">
				<td id= "common" width="" align="right" valign="top">
				<fieldset id="common1" style="padding:2px;margin:0px;">
				<legend>Display On</legend>
				<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="3" width="100%" style="margin-bottom: 0px;">
				<tr>					
					<td width="8px"style="padding:0px"><s:checkbox key="notes.displaySurvey" value="${isSurveyFlag}" fieldValue="true" onclick="changeStatus();"/></td>
					<td align="left" class="listwhitetext" ><fmt:message key='notes.displaySurvey'/></td>					
					<td width="8px" style="padding:0px"><s:checkbox key="notes.displayPortal" value="${isPortalFlag}" fieldValue="true" onclick="changeStatus();"/></td>
					<td align="left" class="listwhitetext" ><fmt:message key='notes.displayPortal'/></td>
				</tr>
				
				<tr>	
				<td width="8px" style="padding:0px"><s:checkbox key="notes.displayTicket" value="${isTicketFlag}" fieldValue="true" onclick="changeStatus();"/></td>
				<td align="left" class="listwhitetext" ><fmt:message key='notes.displayTicket'/></td>				
				<td width="8px" style="padding:0px"><s:checkbox key="notes.accPortal" value="${isAccPortal}" fieldValue="true"/></td>
				<td align="left" width="80px" class="listwhitetext" >Account&nbsp;Portal&nbsp;&nbsp;</td>				
				<td width="8px" style="padding:0px"><s:checkbox key="notes.partnerPortal" value="${isPartnerPortal}" fieldValue="true"/></td>
				<td align="left" width="80px" class="listwhitetext" >Partner&nbsp;Portal&nbsp;&nbsp;</td>
				</tr>				
				</table>
				</fieldset>	
			<c:if test="${not empty checkSO }">	
				<fieldset id="common2" style="padding:2px;">
				<legend>Network Access</legend>
				<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="3" width="100%" style="margin-bottom: 0px;">							
				<tr>
				<c:if test="${originAgentFlag=='OA'}">				
					<td width="8px" style="padding:0px"><s:checkbox name="notes.originAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
			    <c:if test="${originAgentFlag==''}">
			    	<td width="8px" style="padding:0px"><s:checkbox name="notes.originAgent" value="${isOriginAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagOA(this,'OA');"/></td>
				</c:if>
				<td align="left" class="listwhitetext" >Origin&nbsp;Agent</td>
												
				<c:if test="${destAgentFlag=='DA'}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.destinationAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
				<c:if test="${destAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.destinationAgent" value="${isDestAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagDA(this,'DA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Dest.&nbsp;Agent</td>
													
				<c:if test="${bookingAgentFlag=='BA'}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.bookingAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
				<c:if test="${bookingAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.bookingAgent" value="${isBookingAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagBA(this,'BA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Booking&nbsp;Agent</td>				
				</tr>
				
				<tr>
				<c:if test="${subOriginAgentFlag=='SOA'}">	
					<td width="8px" style="padding:0px"><s:checkbox name="notes.subOriginAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
				<c:if test="${subOriginAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.subOriginAgent" value="${isSubOriginAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagSOA(this,'SOA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Sub&nbsp;Origin&nbsp;Agent</td>
					
				<c:if test="${subDestAgentFlag=='SDA'}">			
					<td width="8px" style="padding:0px"><s:checkbox name="notes.subDestinationAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();" /></td>
				</c:if>
				<c:if test="${subDestAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.subDestinationAgent" value="${isSubDestAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagSDA(this,'SDA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Sub&nbsp;Dest.&nbsp;Agent</td>	
							
				<c:if test="${networkAgentFlag=='NA'}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.networkAgent" value="true" fieldValue="true" disabled="true" onclick="changeStatus();"/></td>
				</c:if>
				<c:if test="${networkAgentFlag==''}">
					<td width="8px" style="padding:0px"><s:checkbox name="notes.networkAgent" value="${isNetworkAgent}" fieldValue="true" onclick="changeStatus();setCheckFlagNA(this,'NA');"/></td>
				</c:if>
				<td align="left" width="80px" class="listwhitetext" >Network&nbsp;Agent</td>
				</tr>
				
				</table>
				</fieldset>	
				</c:if>
				</td>
				</c:if>
				</tr>
				
				</sec-auth:authComponent>
				<c:if test="${accountNotesFor=='AC'}">
						<tr>
						<td colspan="4">
						<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;padding:0px;">
						<tr>
						<td align="right" class="listwhitetext" width="" style="!padding:0px">Activity Type&nbsp;&nbsp;</td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.notesActivity" list="%{notesActivity}" cssStyle="width:95px;" onchange="changeStatus();"/></td>
						<td align="right" class="listwhitetext" style="width:116px; !width:65px; !padding-right:0;">Date&nbsp;Of&nbsp;Contact&nbsp;</td>
						<td width="6"></td>
						<c:if test="${empty notes.dateOfContact}">
							<td align="left" width="68px" style="!margin-left:-10;"><s:textfield cssClass="input-text"  id="dateOfContact" name="notes.dateOfContact" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" ><img id="dateOfContact_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${not empty notes.dateOfContact}">
							<s:text id="notesDateOfContactFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.dateOfContact"/></s:text>
							<td align="left" width="68px"><s:textfield cssClass="input-text"  id="dateOfContact" name="notes.dateOfContact" value="%{notesDateOfContactFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" ><img id="dateOfContact_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							
							
						</tr>
						</table>
						</td>
						</tr>
						</c:if>
				</table>
				<!-- ////////////COMPONENT BASED FOR SUDD(TICKET NO : 11782)/////////////////////////////////////////////// -->
				
				<div id="typeBasedShowCheckBox" style="display:none;">
						<table  style="padding:0  0 0 48px;margin:0px">
						<tr>
							<c:set var="isNegativeCommentFlag" value="false"/>
							<c:set var="isReportToAgentFlag" value="false"/>
							
							<c:if test="${notes.negativeComment}">
								<c:set var="isNegativeCommentFlag" value="true"/>
							</c:if>
							<c:if test="${notes.reportToAgent}">
								<c:set var="isReportToAgentFlag" value="true"/>
							</c:if>
							<td align="right" width="8px"style="padding:0px"><s:checkbox key="notes.negativeComment" value="${isNegativeCommentFlag}"/></td>
							<td align="left" class="listwhitetext" ><fmt:message key='notes.negativeComment'/></td>
							<td align="right" width="50"><s:checkbox key="notes.reportToAgent" value="${isReportToAgentFlag}"/></td>
							<td align="left" class="listwhitetext" style="padding-right:38px"><fmt:message key='notes.reportToAgent'/></td>
							<td align="right" class="listwhitetext"><fmt:message key='notes.roleCausedBy'/></td>
							<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.roleCausedBy" list="%{roleCausedBy}" cssStyle="width:100px;"  headerKey="" headerValue="" onchange="changeStatus();setCausedByVal();"/></td>
						</tr>
						</table>
						</div>
						
				<!-- ////////////////////////////////////////////////////////////////////////////////////// -->
				
				<sec-auth:authComponent componentId="module.section.notes.FollowUp">
				<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="2"  style="margin: 0px;">
				<tr >
				<td colspan="6"  id="displayLink">				
				<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="2"  style="margin-bottom: 0px;">
				<tr>
				<td id="ExceptionActual" >
				<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="2"  style="margin-bottom: 0px;">
				<tr>
				<td align="right" class="listwhitetext" width="76">Actual&nbsp;Value</td>
				<td align="left"><s:textfield cssClass="input-text" cssStyle="width:60px;" maxlength="7" name="notes.actualValue" onkeydown="return onlyFloatNumsAllowed(event)" /></td>
				<td align="right" class="listwhitetext" width="93">Requested&nbsp;Value</td> 
				<td align="left" width="74"><s:textfield cssClass="input-text" cssStyle="width:60px;" maxlength="7" name="notes.requestedValue" onkeydown="return onlyFloatNumsAllowed(event)" /></td>
					<td align="right" class="listwhitetext" style="width:68px;">Request&nbsp;Date</td>
					<c:if test="${empty notes.requestDate}">
							<td align="left" width="63px" style="!margin-left:-10px;"><s:textfield cssClass="input-text"  id="requestDate" name="notes.requestDate" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" width="20" ><img id="requestDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${not empty notes.requestDate}">
							<s:text id="requestDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.requestDate"/></s:text>
							<td align="left" width="63px"><s:textfield cssClass="input-text"  id="requestDate" name="notes.requestDate" value="%{requestDateFormattedValue}" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" width="20"><img id="requestDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<td align="right" class="listwhitetext" style="width:80px;">Submitted&nbsp;Date</td>
					<c:if test="${empty notes.submittedDate}">
							<td align="left" width="63px" style="!margin-left:-10px;"><s:textfield cssClass="input-text"  id="submittedDate" name="notes.submittedDate" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" width="" ><img id="submittedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${not empty notes.submittedDate}">
							<s:text id="submittedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.submittedDate"/></s:text>
							<td align="left" width="63px"><s:textfield cssClass="input-text"  id="submittedDate" name="notes.submittedDate" value="%{submittedDateFormattedValue}" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" width=""><img id="submittedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					</tr>
					</table>
				</td>
				</tr>
				<tr>
					<td colspan="20">			
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="2"  style="margin: 0px;">
					<tr>
					<td align="left" class="listwhitetext" style="!width:83px;">Completion&nbsp;Date<div id="imp" style="float:right;!margin-top:-10px;"><font color="red" size="2">*</font></div></td>
					<c:if test="${empty notes.complitionDate}">
							<td align="left" width="" style="!margin-left:-10px;"><s:textfield cssClass="input-text"  id="complitionDate" name="notes.complitionDate" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" width="20px" ><img id="complitionDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${not empty notes.complitionDate}">
							<s:text id="notesComplitionDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.complitionDate"/></s:text>
							<td align="left" width=""><s:textfield cssClass="input-text"  id="complitionDate" name="notes.complitionDate" value="%{notesComplitionDateFormattedValue}" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" width="20px"><img id="complitionDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>	
				<td align="left" class="listwhitetext" width="20px"></td>
					<td colspan="12">	
					<fieldset style="padding:0 0 15px;height:20px;margin:0px;">
					<table class="detailTabLabel" width="500px">
					<tr>
					<legend style="margin-left:6px">Display On : </legend>				
					<td width="8px"style="padding:0px"><s:checkbox key="notes.displaySurvey" value="${isSurveyFlag}" fieldValue="true" onclick="changeStatus();"/></td>
					<td align="left" class="listwhitetext" ><fmt:message key='notes.displaySurvey'/></td>					
					<td width="8px" style="padding:0px"><s:checkbox key="notes.displayPortal" value="${isPortalFlag}" fieldValue="true" onclick="changeStatus();"/></td>
					<td align="left" class="listwhitetext" width="100"><fmt:message key='notes.displayPortal'/></td>
					<td width="8px" style="padding:0px"><s:checkbox key="notes.displayTicket" value="${isTicketFlag}" fieldValue="true" onclick="changeStatus();"/></td>
					<td align="left" class="listwhitetext" ><fmt:message key='notes.displayTicket'/></td>				
					<td width="8px" style="padding:0px"><s:checkbox key="notes.accPortal" value="${isAccPortal}" fieldValue="true"/></td>
					<td align="left" width="80px" class="listwhitetext" >Account&nbsp;Portal&nbsp;&nbsp;</td>				
					<td width="8px" style="padding:0px"><s:checkbox key="notes.partnerPortal" value="${isPartnerPortal}" fieldValue="true"/></td>
					<td align="left" width="80px" class="listwhitetext" >Partner&nbsp;Portal&nbsp;&nbsp;</td>
					</tr>
					</table>
					</fieldset>
					</td>
					</tr>
					</table>
					</td>
				</tr>				
				</table>
				
						</td>
						</tr>
				</table>
			</sec-auth:authComponent>	
					<table style="margin-bottom:0px;padding-bottom:0px;" border="0">
					<tr>			
					<td colspan="5">
					<c:if test="${memoIntegrationFlag=='Y'}" >
					<table style="margin:0px;padding:0px;" border="0">					
						<tr>
						<td align="right" class="listwhitetext" width="">Category</td>
						<td align="left" class="listwhitetext" colspan="3"> <s:select cssClass="list-menu" name="notes.category" list="%{uvlmemocategory}" cssStyle="width:105px" headerKey="" headerValue="" /></td>
						<td align="right" class="listwhitetext" width="73">Send&nbsp;to&nbsp;UVL</td>
						<td align="left" class="listwhitetext" ><s:checkbox name="notes.uvlSentStatus"></s:checkbox> <%-- <s:select cssClass="list-menu" name="notes.uvlSentStatus" list="%{uvlmemoxfer}" cssStyle="width:105px" headerKey="" headerValue="" /> --%></td>
						</tr>				
					</table>
						</c:if>
						<c:if test="${memoIntegrationFlag!='Y'}" >
						<s:hidden name="notes.category" />
						<s:hidden name="notes.uvlSentStatus"/>
						</c:if>
					</td>
					</tr>
					
						<tr>
						<%-- <td align="right" class="listwhitetext" style="width:50px; !width:55px; " ><fmt:message key="notes.subject"/></td>
						<td align="left" class="listwhitetext"  style="padding-bottom: 2px;"><s:textfield cssClass="list-menu"  cssStyle="height:16px;width:681px;!width:625px;" id="subject" name="notes.subject" maxlength="100" /></td> --%>
						<td align="right" class="listwhitetext"><fmt:message key="notes.subject"/></td>
						<td align="left" class="listwhitetext"><s:textfield cssClass="list-menu"  cssStyle="height:16px;width:648px;!width:625px;" id="subject" name="notes.subject" maxlength="100" /></td>
						</tr>
						
					<tr>
						<td colspan="4">
						<div id="typeBasedDetailsBox" style="display:block;">
						<!-- <table  style="width:100%;padding:0  0 0 35px;margin:0px"> -->
						<table>
					<tr>
						<%-- <td align="right" class="listwhitetext" valign="top"><fmt:message key="notes.note"/></td> --%>
						<td align="right" class="listwhitetext" valign="top" style="width:34px; !width:55px; "><fmt:message key="notes.note"/></td>
						<td align="left" class="listwhitetext" width="20px"><s:textarea name="notes.note" id="noteTextArea" cols="122" rows="10"   cssClass="textarea" readonly="false"/></td>														
					</tr>
					<tr>
			        <%-- <td align="center" class="listwhitetext" colspan="5" valign="bottom" style="">						
								<c:out value="${buttons}" escapeXml="false" />					
					</td> --%>
					</tr>
					</table>
					</div>
					</td>
					</tr>
					
					<tr>
						<td colspan="4">
	<!-- ////////////COMPONENT BASED FOR SUDD(TICKET NO : 11782)/////////////////////////////////////////////// -->
						<div id="typeBasedIssueBox" style="display:none">
						<table  style="width:100%;padding:0  0 0 55px;margin:0px">
						
					<tr>
						<td align="center" class="listwhitetext" valign="top"><b><fmt:message key="notes.issue"/></b></td>
						<td align="center" class="listwhitetext" valign="top"><b><fmt:message key="notes.resolution"/></b></td>
					</tr>
						<tr>	
							<td align="left" class="listwhitetext" width="20px"><s:textarea name="notes.issue" id="issueTextArea" cols="84" rows="7"  cssClass="textarea" readonly="false"/></td>														
							<td align="left" class="listwhitetext" width="20px"><s:textarea name="notes.resolution" id="resolutionTextArea" cols="84" rows="7"  cssClass="textarea" readonly="false"/></td>														
						</tr>
						<tr>
			       
					</tr>
					</table>
					</div>
	<!-- /////////////////////////////////////////////////////////// -->				
					</td>
					</tr>
			 <tr>
			
			        <td align="center" class="listwhitetext" colspan="5" valign="bottom" style="">						
								<c:out value="${buttons}" escapeXml="false" />					
					</td>
			</tr> 
			<tr>
				<td></td>
			</tr>		
			<c:if test="${not empty notes.id}" >
				<tr>
					<td></td>
        	<td>
        	<display:table name="myFileList" class="table" requestURI="" id="myFileList" export="false" defaultsort="2" defaultorder="ascending" pagesize="5" style="width:100%;margin:0px;padding;0px;" >   
					   <display:column property="description" sortable="true" title="Description" />
					   <display:column sortable="true" title="Document" >
					   	<a onclick="javascript:openWindow('ImageServletAction.html?id=${myFileList.id}&decorator=popup&popup=true',900,600);"><c:out value="${myFileList.fileFileName}" escapeXml="false"/></a>
					   </display:column>
					   <display:column property="createdOn" sortable="true" title="Created On" />
					   <display:column property="updatedOn" sortable="true" title="Updated On" />
					  </display:table>
        		</td>
        		<!--<td>
        			<input type="button" class="cssbutton" cssStyle="width:150px; height:25px" id="mailWithAttachment" name="mailWithAttachment" value="Mail With Attachment" onclick="return emailAttachment('IPM.Note.mailWithAttachment'); " />
				</td>
			--></tr>
			
			<tr>
			<td></td>
			<c:if test="${param.popup}"> 
			<c:if test="${not empty checkCF && checkCF!='SO'}">
				<td align="center">
					<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:120px; font-size: 15;"  
        	onclick="popupOpen();"  
        	value="Link to File Cabinet"/> 
        	</td>
        	</c:if>
        	<c:if test="${not empty checkCF  && checkCF=='SO' }">
        	<td align="center">
					<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:120px; font-size: 15;"  
        	onclick="popupOpen();"  
        	value="Link to File Cabinet"/> 
        	</td>
        	</c:if>
        	<c:if test="${empty checkCF }">
        	<td align="center">
					<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:120px; font-size: 15;"  
        	onclick="popupOpen2();"  
        	value="Link to File Cabinet"/> 
        	</td>
        	</c:if>
        		</c:if>	
        		<c:if test="${!param.popup}"> 
        		<c:if test="${not empty checkCF && checkCF!='SO' }">
				<td align="center">
					<input type="button" name="LinktoFileCabinet" class="cssbutton" style="margin-right: 0px;height: 25px;width:120px; font-size: 15;"  
        	onclick="javascript:openWindow('myAllFiles.html?fileId=${notes.notesId}&id=${notes.id}&decorator=popup&popup=true',755,500);"  
        	value="Link to File Cabinet"/> 
        	</td>
        	</c:if>
        	<c:if test="${not empty checkCF  && checkCF=='SO' }">
        	<td align="center">
					<input type="button" name="LinktoFileCabinet"  class="cssbutton" style="margin-right: 0px;height: 25px;width:120px; font-size: 15;"  
        	onclick="javascript:openWindow('myAllFiles.html?fileId=${notes.notesId}&id=${notes.id}&decorator=popup&popup=true',755,500);"  
        	value="Link to File Cabinet"/> 
        	</td>
        	</c:if>	
        	 <c:if test="${empty checkCF }">
        	<td align="center">
				<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:120px; font-size: 15;"  
        	onclick="javascript:openWindow('myAllFiles.html?fileId=${serviceOrder.shipNumber}&id=${notes.id}&decorator=popup&popup=true',755,500);"  
        	value="Link to File Cabinet"/> 
        	</td>
        	</c:if>	
        	</c:if>
			</tr>
					</c:if>	
				</table>		
			</td>
		</tr>
	</tbody>
</table>

<sec-auth:authComponent componentId="module.section.notes.FollowUp">
<div class="mainDetailTable" style="width:745px;">
					<table  class="colored_bg" style="width:100%; !width:100%; margin:0px; padding-bottom:5px;"  border="0">
					<tbody>
					<tr><td align="right"><img style="cursor:default" src="${pageContext.request.contextPath}/images/clock-blue.gif" /></td><td class="listwhitetext" colspan="3"><font size="2"><b>Follow Up</b></font></td></tr>
						<tr>					
							<td align="right" class="listwhitetext" style="width:68px; !width:65px; !padding-right:0;">On</td>
							
							<td align="left" colspan="2">
							<table border="0" class="detailTabLabel" cellpadding="0" cellspacing="0">
							<tr>
							<c:if test="${empty notes.forwardDate}">
							<td align="left" width="63px" style="!margin-left:-10px;"><s:textfield cssClass="input-text"  id="forwardDate" name="notes.forwardDate" size="6" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<c:if test="${userType=='USER'}">
							<td align="left" width="20px" ><img id="forwardDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</c:if>
							<c:if test="${not empty notes.forwardDate}">
							<s:text id="notesForwardDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.forwardDate"/></s:text>
							<td align="left" width="63px"><s:textfield cssClass="input-text"  id="forwardDate" name="notes.forwardDate" value="%{notesForwardDateFormattedValue}" size="6" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/></td>
							<c:if test="${userType=='USER'}">
							<td align="left" width="20px"><img id="forwardDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							</c:if>
							</tr>
							</table>
							</td>
							<td align="left" colspan="2">
							<table border="0" class="detailTabLabel" cellpadding="0" cellspacing="0">
							<tr>
						    <c:choose>
							<c:when test="${userType =='ACCOUNT'}">
				           	<c:if test="${notes.corpID =='UTSI'}">
		                     <td align="right" class="listwhitetext" width="20px">
							<td align="left" class="listwhitetext" width="40px"><s:hidden cssClass="input-text" id="remindTime" name="notes.remindTime"  value ="8:00"  onkeydown="return onlyTimeFormatAllowed(event)" onclick="setTimeMask();" onchange = "completeTimeString(); return IsValidTime();"/></td>
							 </c:if>
						    <c:if test="${notes.corpID !='UTSI'}">
		                    <td align="right" class="listwhitetext" width="20px">@</td>
							<td align="left" class="listwhitetext" width="40px"><s:textfield cssClass="input-text" id="remindTime" name="notes.remindTime" size="2" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onclick="setTimeMask();" onchange = "completeTimeString(); return IsValidTime();"/></td>
							<td width="10px"><img id="calender" style="cursor: default" src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=15 WIDTH=15 "/></td>
						      </c:if>
							</c:when>
							<c:otherwise>
						     <td align="right" class="listwhitetext" width="20px">@</td>
							<td align="left" class="listwhitetext" width="40px"><s:textfield cssClass="input-text" id="remindTime" name="notes.remindTime" size="2" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onclick="setTimeMask();" onchange = "completeTimeString(); return IsValidTime();"/></td>
							<td width="10px"><img id="calender" style="cursor: default" src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=15 WIDTH=15 "/></td>
							</c:otherwise>
							</c:choose>
							<td width="10px"></td>
							<td align="right" class="listwhitetext" width="20px">For</td>
							<td align="left" class="listwhitetext" width="50px" colspan="2"><s:select cssClass="list-menu" name="notes.followUpFor" list="%{all_user}" cssStyle="width:141px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							    <c:choose>
							<c:when test="${userType =='ACCOUNT'}">
				           	<c:if test="${notes.corpID =='UTSI'}">
							<td align="right" class="listwhitetext" width="60px">
							</c:if>
							<c:if test="${notes.corpID !='UTSI'}">
							<td align="right" class="listwhitetext" width="60px">Remind&nbsp;@</td>
							<td align="left" class="listwhitetext" width="100px"><s:select cssClass="list-menu" name="notes.remindInterval" list="%{remindIntervals}" cssStyle="width:95px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							</c:if>
							</c:when>
							<c:otherwise>
							<td align="right" class="listwhitetext" width="60px">Remind&nbsp;@</td>
							<td align="left" class="listwhitetext" width="100px"><s:select cssClass="list-menu" name="notes.remindInterval" list="%{remindIntervals}" cssStyle="width:95px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							</c:otherwise>
							</c:choose>
							<td align="left" class="listwhitetext" style=""><input type="submit" name="remindBtn" class="cssbutton"  value="Set Follow Up" style="width:87px; height:25px" onclick="setbtnType(this);completeTimeString();setRemindValue();return IsValidTime(); return false"/></td>
							<s:hidden name="remindMe" />
							</tr>
							</table>
							</td>
							
						</tr>
							</tbody>
				</table>	
				</div>		
</sec-auth:authComponent>

</div>
<div id="mydiv1" style="position:absolute; margin-top:-10px"></div>
<table width="730px">
					<tbody>
					<tr><td height="20"></td></tr>
						<tr>
							<td align="right" class="listwhitetext" width="75px" ><b><fmt:message key='notes.createdOn'/></b></td>
							<td style="width:125px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${notes.createdOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="notes.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${notes.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>
							<td align="right" class="listwhitetext" width="75px"><b><fmt:message key='notes.createdBy' /></b></td>
							<c:if test="${not empty notes.id}">
								<s:hidden name="notes.createdBy"/>
								<td width="80px" style="font-size:1em"><s:label name="createdBy" value="%{notes.createdBy}"/></td>
							</c:if>
							<c:if test="${empty notes.id}">
								<s:hidden name="notes.createdBy" value="${pageContext.request.remoteUser}"/>
								<td width="80px" style="font-size:1em"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							<td align="right" class="listwhitetext" width="75px" style="font-size:1em"><b><fmt:message key='notes.updatedOn'/></b></td>
							<s:text id="customerFileupdatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.updatedOn" /></s:text>
							<td valign="top" style="font-size:1em"><s:hidden name="notes.updatedOn" value="%{customerFileupdatedOnFormattedValue}" /></td>
							
							<td style="width:130px ;font-size:1em"><fmt:formatDate value="${notes.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
							<td align="right" class="listwhitetext" width="75px"><b><fmt:message key='notes.updatedBy' /></b></td>
							<c:if test="${not empty notes.id}">
								<s:hidden name="notes.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{notes.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty notes.id}">
								<s:hidden name="notes.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
</table>
			
	<s:hidden key="myMessage.fromUser" />
	<s:hidden key="myMessage.toUser"/>
	<s:hidden key="myMessage.subject" />
	<s:hidden key="myMessage.message"  />
	<s:hidden key="myMessage.sentOn" /> 
	<s:hidden name="myMessage.noteId" />
	<c:if test="${param.popup}"> 
	<div id="overlay"  >
<div id="layerLoading">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
       <img src="<c:url value='/images/ajax-loader.gif'/>" />       
       </td>
       </tr>
     </table>
   </td>
  </tr>
</table>  
   </div>   
   </div>
   </c:if>
	<c:if test="${!param.popup}"> 
	<div id="overlay" style="display:none"  >
<div id="layerLoading">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
       <img src="<c:url value='/images/ajax-loader.gif'/>" />       
       </td>
       </tr>
     </table>
   </td>
  </tr>
</table>  
   </div>   
   </div>
   </c:if>
</s:form>

<script type="text/javascript">
showOrHideAutoNote(0);
function checkDate(){  
		var date1 = document.forms['notesForm'].elements['notes.forwardDate'].value; 
		var systenDate = new Date();
		var mySplitResult = date1.split("-");
	   	var day = mySplitResult[0];
	   	var month = mySplitResult[1];
	   	var year = mySplitResult[2];
	 	if(month == 'Jan'){
	       month = "01";
	   	}else if(month == 'Feb'){
	       month = "02";
	   	}else if(month == 'Mar'){
	       month = "03"
	   	}else if(month == 'Apr'){
	       month = "04"
	   	}else if(month == 'May'){
	       month = "05"
	   	}else if(month == 'Jun'){
	       month = "06"
	   	}else if(month == 'Jul'){
	       month = "07"
	   	}else if(month == 'Aug'){
	       month = "08"
	   	}else if(month == 'Sep'){
	       month = "09"
	   	}else if(month == 'Oct'){
	       month = "10"
	   	}else if(month == 'Nov'){
	       month = "11"
	   	}else if(month == 'Dec'){
	       month = "12";
	   	}
	   	var finalDate = month+"-"+day+"-"+year;
	   	date1 = finalDate.split("-");
	  	var enterDate = new Date(date1[0]+"/"+date1[1]+"/20"+date1[2]);
	  	var newSystenDate = new Date(systenDate.getMonth()+1+"/"+systenDate.getDate()+"/"+systenDate.getFullYear());
	  	var daysApart = Math.round((enterDate-newSystenDate)/86400000);
	  	
	  	if(daysApart < 0){
	    	alert("Cannot Enter Past Date - Please Re-Enter");
	    	document.forms['notesForm'].elements['notes.forwardDate'].value='';
	    	return false;
	  	}
	  	
	  	if(date1 != ''){
	  		if(daysApart == 0){
	  			var time = document.forms['notesForm'].elements['notes.remindTime'].value;
				var hour = time.substring(0, time.indexOf(":"))
				var min = time.substring(time.indexOf(":")+1, time.length);
	  			
	  			if (hour < 0  || hour > 23) {
					alert("Follow up  time must be between 0 and 23(Hrs)");
					document.forms['notesForm'].elements['notes.remindTime'].value = '00:00'
					document.forms['notesForm'].elements['notes.remindTime'].focus();
					return false;
				}
				if (min<0 || min > 59) {
					alert ("Follow up  time must be between 0 and 59(Mins)");
					document.forms['notesForm'].elements['notes.remindTime'].value = '00:00'
					document.forms['notesForm'].elements['notes.remindTime'].focus();
					return false;
				}
				
				if(systenDate.getHours() > hour){
					document.forms['notesForm'].elements['notes.remindTime'].value = '00:00'
					alert("Cannot Enter Past Time - Please Re-Enter");
					return false;
				}else if(systenDate.getHours() == hour && systenDate.getMinutes() > min){
					document.forms['notesForm'].elements['notes.remindTime'].value = '00:00'
					alert("Cannot Enter Past Time - Please Re-Enter");
					return false;
				}
				
			}
	  	}
}
try {
linkVisible();
<c:if test="${userType!='AGENT'}">
findNewAgents();
</c:if>
getNoteStatusDetails();
getSubType();
//getSubCategoryDetails();
getSubCategorySoDetails();
getGradingDetails();
chkCmpDate();
<c:if test="${userType=='AGENT'}">
findNewAgentPortal();
</c:if>
}
catch(e){}
</script> 
<script type="text/javascript">
	setCalendarFunctionality();
	try{
		<c:if test="${from == 'View' && noteFor=='Partner'}">
		var elementsLen=document.forms['notesForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['notesForm'].elements[i].type=='text'){
					document.forms['notesForm'].elements[i].readOnly =true;
					document.forms['notesForm'].elements[i].className = 'input-textUpper';
				}else{
					document.forms['notesForm'].elements[i].disabled=true;
				}
		}
		</c:if>
	}catch(e){}
</script>
<script type="text/javascript">
<c:choose>
<c:when test="${not empty param.subType}"> 
	setTimeout('setSubType("${param.subType}")',500);
</c:when>
<c:otherwise>
	setTimeout('setSubType("${notes.noteSubType}")',500);
</c:otherwise>
</c:choose>

function setSubType(subType){
	document.forms['notesForm'].elements['notes.noteSubType'].value = subType;
	<c:if test="${param.NetwokAccess !='' || not empty param.NetwokAccess}">  
		document.forms['notesForm'].elements['${param.NetwokAccess}'].checked=true;
		document.forms['notesForm'].elements['${param.NetwokAccess}'].disabled=false; 
	</c:if>
}


function abc () {
	var note_type = document.forms['notesForm'].elements['notes.noteType'].value;
	var checkComponet = document.forms['notesForm'].elements['issueResolutionType'].value;
	var typeBasedShowCheckBox = document.getElementById("typeBasedShowCheckBox");
	var typeBasedIssueBox = document.getElementById("typeBasedIssueBox");
	var typeBasedDetailsBox = document.getElementById("typeBasedDetailsBox");
	if(checkComponet=='Y' && note_type == 'Issue Resolution'){
		typeBasedShowCheckBox.style.display = 'block';
		typeBasedIssueBox.style.display = 'block';
		typeBasedDetailsBox.style.display='none';
	} else{
		typeBasedShowCheckBox.style.display = 'none';
		typeBasedIssueBox.style.display = 'none';
		typeBasedDetailsBox.style.display='block';
	}
}
abc(); 
</script>

