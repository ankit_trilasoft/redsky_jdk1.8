/**
 * WSFormDesign.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSFormDesign  extends com.keysurvey.api.v68.WSObject  implements java.io.Serializable {
    private com.keysurvey.api.v68.form.design.WSFormAlign footerAlignement;

    private java.lang.String footerText;

    private long formId;

    private boolean progressIndicatorType;

    private boolean showPageNumber;

    private java.lang.String thankYouPage;

    public WSFormDesign() {
    }

    public WSFormDesign(
           long versionId,
           com.keysurvey.api.v68.form.design.WSFormAlign footerAlignement,
           java.lang.String footerText,
           long formId,
           boolean progressIndicatorType,
           boolean showPageNumber,
           java.lang.String thankYouPage) {
        super(
            versionId);
        this.footerAlignement = footerAlignement;
        this.footerText = footerText;
        this.formId = formId;
        this.progressIndicatorType = progressIndicatorType;
        this.showPageNumber = showPageNumber;
        this.thankYouPage = thankYouPage;
    }


    /**
     * Gets the footerAlignement value for this WSFormDesign.
     * 
     * @return footerAlignement
     */
    public com.keysurvey.api.v68.form.design.WSFormAlign getFooterAlignement() {
        return footerAlignement;
    }


    /**
     * Sets the footerAlignement value for this WSFormDesign.
     * 
     * @param footerAlignement
     */
    public void setFooterAlignement(com.keysurvey.api.v68.form.design.WSFormAlign footerAlignement) {
        this.footerAlignement = footerAlignement;
    }


    /**
     * Gets the footerText value for this WSFormDesign.
     * 
     * @return footerText
     */
    public java.lang.String getFooterText() {
        return footerText;
    }


    /**
     * Sets the footerText value for this WSFormDesign.
     * 
     * @param footerText
     */
    public void setFooterText(java.lang.String footerText) {
        this.footerText = footerText;
    }


    /**
     * Gets the formId value for this WSFormDesign.
     * 
     * @return formId
     */
    public long getFormId() {
        return formId;
    }


    /**
     * Sets the formId value for this WSFormDesign.
     * 
     * @param formId
     */
    public void setFormId(long formId) {
        this.formId = formId;
    }


    /**
     * Gets the progressIndicatorType value for this WSFormDesign.
     * 
     * @return progressIndicatorType
     */
    public boolean isProgressIndicatorType() {
        return progressIndicatorType;
    }


    /**
     * Sets the progressIndicatorType value for this WSFormDesign.
     * 
     * @param progressIndicatorType
     */
    public void setProgressIndicatorType(boolean progressIndicatorType) {
        this.progressIndicatorType = progressIndicatorType;
    }


    /**
     * Gets the showPageNumber value for this WSFormDesign.
     * 
     * @return showPageNumber
     */
    public boolean isShowPageNumber() {
        return showPageNumber;
    }


    /**
     * Sets the showPageNumber value for this WSFormDesign.
     * 
     * @param showPageNumber
     */
    public void setShowPageNumber(boolean showPageNumber) {
        this.showPageNumber = showPageNumber;
    }


    /**
     * Gets the thankYouPage value for this WSFormDesign.
     * 
     * @return thankYouPage
     */
    public java.lang.String getThankYouPage() {
        return thankYouPage;
    }


    /**
     * Sets the thankYouPage value for this WSFormDesign.
     * 
     * @param thankYouPage
     */
    public void setThankYouPage(java.lang.String thankYouPage) {
        this.thankYouPage = thankYouPage;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSFormDesign)) return false;
        WSFormDesign other = (WSFormDesign) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.footerAlignement==null && other.getFooterAlignement()==null) || 
             (this.footerAlignement!=null &&
              this.footerAlignement.equals(other.getFooterAlignement()))) &&
            ((this.footerText==null && other.getFooterText()==null) || 
             (this.footerText!=null &&
              this.footerText.equals(other.getFooterText()))) &&
            this.formId == other.getFormId() &&
            this.progressIndicatorType == other.isProgressIndicatorType() &&
            this.showPageNumber == other.isShowPageNumber() &&
            ((this.thankYouPage==null && other.getThankYouPage()==null) || 
             (this.thankYouPage!=null &&
              this.thankYouPage.equals(other.getThankYouPage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getFooterAlignement() != null) {
            _hashCode += getFooterAlignement().hashCode();
        }
        if (getFooterText() != null) {
            _hashCode += getFooterText().hashCode();
        }
        _hashCode += new Long(getFormId()).hashCode();
        _hashCode += (isProgressIndicatorType() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isShowPageNumber() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getThankYouPage() != null) {
            _hashCode += getThankYouPage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSFormDesign.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormDesign"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("footerAlignement");
        elemField.setXmlName(new javax.xml.namespace.QName("", "footerAlignement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormAlign"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("footerText");
        elemField.setXmlName(new javax.xml.namespace.QName("", "footerText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "formId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("progressIndicatorType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "progressIndicatorType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("showPageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "showPageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("thankYouPage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "thankYouPage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
