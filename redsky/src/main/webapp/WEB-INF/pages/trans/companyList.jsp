<%@ include file="/common/taglibs.jsp"%>  

<head>   
    <title><fmt:message key="companyList.title"/></title>   
    <meta name="heading" content="<fmt:message key='companyList.heading'/>"/>  
    
<style>
  span.pagelinks {display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:2px;margin-top:-8px;!margin-top:-17px;padding:2px 0px;text-align:right;width:100%;
}
.table td, .table th, .tableHeaderTable td {padding:0.4em;}
</style> 

<script language="javascript" type="text/javascript">
function clear_fields(){
document.forms['companyListForm'].elements['companyName'].value = '';
document.forms['companyListForm'].elements['countryID'].value = '';
document.forms['companyListForm'].elements['billGroup'].value = '';
document.forms['companyListForm'].elements['status'].value = '';

}
</script>
</head>   
  <!-- By Madhu -->
  <s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <!--  -->  
<c:set var="buttons">  

     <input type="button" class="cssbutton" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editCompany.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	
</c:set>   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>   
<div id="Layer1" style="width:100%;vertical-align:top;margin-top:-30px;">

		<div class="spnblk">&nbsp;</div>
<s:form id="companyListForm" method="post">  
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top:-2px; "><span></span></div>
   <div class="center-content">
<table class="table" style="width:98%"  >
<thead>
<tr>
<th><fmt:message key="company.companyName"/></th>
<th><fmt:message key="company.countryID"/></th>
<th><fmt:message key="company.billGroup"/></th>
<th><fmt:message key="company.status"/></th>
<th></th>

</tr></thead>
<tbody>
<tr>
<td> <s:textfield name="companyName" size="12"  cssClass="input-text"/></td>
<td><s:select  name="countryID" list="%{CountryIdList}" cssClass="list-menu" cssStyle="width:80px" headerKey="" headerValue="" /></td>
<td><s:select  name="billGroup" list="%{rskyBillGroup}" cssClass="list-menu" headerKey="" headerValue="" /></td>
<td><s:select  list="%{statusList}" name="status" cssClass="list-menu" headerKey="" headerValue=""/></td>
<td width="130px" style="border:none;"><c:out value="${searchbuttons}" escapeXml="false"/></td>

</tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="otabs">
	  <ul>
	    <li><a class="current"><span>Company List</span></a></li>
	  </ul>
</div>	
<display:table name="companies" class="table" style="clear:both;" requestURI="" id="companyList" export="true" pagesize="50">
    <display:column property="id" sortable="true" titleKey="company.id" url="/editCompany.html" paramId="id" paramProperty="id"/>
    <display:column property="corpID" sortable="true" titleKey="company.corpID"/>
    <display:column property="parentCorpId" sortable="true" titleKey="company.parentID"/>    
    <display:column property="companyName" sortable="true" titleKey="company.companyName"/>
    <display:column property="licenseType" sortable="true" title="company.licenseType"/>
	<display:column property="goLiveDate" sortable="true" title="GoLiveDate" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="clientBillingDate" sortable="true" title="ClientBillDate"  format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="companyDivisionFlag" sortable="true" style="text-align:center;width: 20px" title="Company DivReq"/>
	<display:column property="multiCurrency" sortable="true" style="text-align:center;width: 20px" title="Multi Currency"/>
	<display:column property="estimateVATFlag" sortable="true" style="text-align:center;width: 20px" title="Est. VAT"/>
	<display:column property="defaultBillingRate" sortable="true" style="text-align:center;" title="Rate&nbsp;1"/>
	<display:column property="rate2" sortable="true" style="text-align:center;" title="Rate&nbsp;2"/>
	<display:column property="minimumBillAmount" sortable="true" style="text-align:right;width: 40px" title="Minimum Amount"/>
	<display:column property="timeZone" sortable="true" title="TimeZone"/>
    <%-- <display:column property="rskyBillGroup" sortable="true" title="Billing Group"/> --%>
 <display:column sortable="true"  title="Billing&nbsp;Group" style="">
 <c:forEach var="entry" items="${rskyBillGroup}">
 <c:if test="${companyList.rskyBillGroup==entry.key}">
 <c:out value="${entry.value}" />
 </c:if>
 </c:forEach>
 </display:column>
  
    <display:setProperty name="export.excel.filename" value="Company List.xls"/>   
    <display:setProperty name="export.csv.filename" value="company List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="company List.pdf"/>   
</display:table>   

  </div>
<c:out value="${buttons}" escapeXml="false" />   
</s:form>  

<script type="text/javascript">   
   
</script>  
