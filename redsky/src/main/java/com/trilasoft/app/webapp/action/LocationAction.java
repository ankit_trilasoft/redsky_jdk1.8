package com.trilasoft.app.webapp.action;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Location;
import com.trilasoft.app.model.RefMasterDTO;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;

import com.trilasoft.app.model.Storage;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.StorageLibraryManager;
import com.trilasoft.app.service.StorageManager;
import com.trilasoft.app.service.LocationManager;
import com.trilasoft.app.service.WorkTicketManager;
import com.trilasoft.app.model.BookStorage;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.StorageLibrary;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.WorkTicket;

import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.math.BigDecimal;
import java.util.*;

public class LocationAction extends BaseAction implements Preparable{

	private LocationManager locationManager;

	private StorageManager storageManager;

	// private BookStorageManager bookStorageManager;

	private NotesManager notesManager;

	private Location location;
	
	private StorageLibrary storageLibrary;

	private StorageLibraryManager storageLibraryManager;
	
	private WorkTicket workTicket;

	private WorkTicketManager workTicketManager;

	private List maxStorage;

	private static List weightunits;
	private List volumeunits;
	private Long autoSequenceNumber;

	private Long id;

	private List locations;

	private BookStorage bookStorage;

	private Long id1;

	private String shipNumber;

	private Storage storage;

	private CustomerFileManager customerFileManager;

	private List<SystemDefault> sysDefaultDetail;

	private String countTicketLocationNotes;

	private RefMasterManager refMasterManager;

	private static Map<String, String> locTYPE;
	
	private static Map<String, String> loctype_isactive;

	private static Map<String, String> capacity;

	private static List<String> occupiedX;

	private Long ticket;

	private static Date createdon;

	private String sessionCorpID;

	private boolean occupied;

	private String occupieType;

	private String occupy = new String("");

	private static Map<String, String> actionType;

	private static Map<String, String> house;

	private String from;

	private String prifix;
	private String code;
	private String storageLibrayType;
	private String whouse;
	private String locationByStorage=new String("");
	private String storageTypeCode;
	private String checkForSameStorage;
	private List storageLocationId;
	private List wtSoDetails;
	private String locationId;
	private Company company;
	private CompanyManager companyManager;
	private String oiJobList;
	static final Logger logger = Logger.getLogger(LocationAction.class);
    public LocationAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
		company=companyManager.findByCorpID(sessionCorpID).get(0);
        if(company!=null && company.getOiJob()!=null){
			oiJobList=company.getOiJob();  
		  }
		
	}
	
	public String list() {
		workTicket = workTicketManager.get(id1);
		locations = locationManager.locationList(sessionCorpID);
		return SUCCESS;
	}

	public String listMain() {
		//System.out.println("\n\n listMain() called \n\n");
		locations = locationManager.locationList(sessionCorpID);
		getComboList(sessionCorpID);
		return SUCCESS;
	}

	public String delete() {
		if (from != null && !from.equalsIgnoreCase("main")) {
			workTicket = workTicketManager.get(id1);
		}
		try {
			locationManager.remove(id);
			saveMessage(getText("location.deleted"));
			locations = locationManager.locationList(sessionCorpID);
		} catch (Exception ex) {
			ex.printStackTrace();
			locations = locationManager.locationList(sessionCorpID);
		}

		return SUCCESS;
	}

	public String getActionList() {
		actionType = new LinkedHashMap<String, String>();
		actionType.put("L", "L" + " : " + "Load");
		actionType.put("A", "A" + " : " + "Access");
		actionType.put("R", "R" + " : " + "Release");
		actionType.put("M", "M" + " : " + "Move");
		return SUCCESS;
	}

	public String edit() {
		//System.out.println("\n\n edit() called \n\n");
		if (from != null && !from.equalsIgnoreCase("main")) {
			workTicket = workTicketManager.get(id1);
			ticket = workTicket.getTicket();
		}

		if (id != null) {
			location = locationManager.get(id);
			createdon = location.getCreatedOn();
			location.setCreatedOn(createdon);

			storage = new Storage();
			storage.setCreatedOn(new Date());
			storage.setUpdatedOn(new Date());
			storage.setCreatedOn(new Date());

			sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					storage.setUnit(systemDefault.getWeightUnit());
					storage.setVolUnit(systemDefault.getVolumeUnit());
				}
			}

			maxStorage = storageManager.findMaximum();
			if (maxStorage.get(0) == null) {
				autoSequenceNumber = Long.parseLong("1");
			} else {
				autoSequenceNumber = Long.parseLong(maxStorage.get(0).toString()) + 1;
			}
			storage.setIdNum(autoSequenceNumber);

		} else {
			location = new Location();
			location.setCreatedOn(new Date());
			location.setUpdatedOn(new Date());

			storage = new Storage();
			storage.setUpdatedOn(new Date());
			storage.setCreatedOn(new Date());
		}
		getActionList();
		getComboList(sessionCorpID);
		if(from !=("main"))
		{
			getNotesForIconChange(); 
		}
		return SUCCESS;
	}

	public String editUnit() {
		
			workTicket = workTicketManager.get(id1);
			ticket = workTicket.getTicket();

		if (id != null) {
			storageLibrary = storageLibraryManager.get(id);		
			storageLocationId=storageManager.findLocationByStorage(storageLibrary.getStorageId(),sessionCorpID);
			if(storageLocationId!=null && !storageLocationId.isEmpty()){
				locationByStorage = storageLocationId.get(0).toString();
			}
			//storageTypeCode=refMasterManager.findCodeByParameter(sessionCorpID,"STORAGE_TYPE",storageLibrary.getStorageType());
			storage = new Storage();
			storage.setCreatedOn(new Date());
			storage.setUpdatedOn(new Date());
			storage.setCreatedOn(new Date());
			storage.setVolume(new BigDecimal(0));
			storage.setMeasQuantity(new BigDecimal(0));
			storage.setPieces(0);
			sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					storage.setUnit(systemDefault.getWeightUnit());
					storage.setVolUnit(systemDefault.getVolumeUnit());
				}
			}

			maxStorage = storageManager.findMaximum();
			if (maxStorage.get(0) == null) {
				autoSequenceNumber = Long.parseLong("1");
			} else {
				autoSequenceNumber = Long.parseLong(maxStorage.get(0).toString()) + 1;
			}
			storage.setIdNum(autoSequenceNumber);

		} else {
			location = new Location();
			location.setCreatedOn(new Date());
			location.setUpdatedOn(new Date());

			storage = new Storage();
			storage.setUpdatedOn(new Date());
			storage.setCreatedOn(new Date());
			
		}
		getActionList();
		getComboList(sessionCorpID);
		if(from !=("main"))
		{
			getNotesForIconChange(); 
		}
		return SUCCESS;
	}
	public String findLocationList(){
		getComboList(sessionCorpID);	
		//code=refMasterManager.findCodeByParameter(sessionCorpID,"LOCTYPE",storageLibrayType);
		locations = locationManager.findLocationList(sessionCorpID,whouse,storageLibrayType);
		return SUCCESS;
	}
	
	
	public String  formShow()
	{
		getComboList(sessionCorpID);
		location = new Location();
		location.setCreatedOn(new Date());
		location.setUpdatedOn(new Date());

		location.setCubicFeet(new BigDecimal(0));
		location.setCubicMeter(new BigDecimal(0));
		location.setUtilizedVolCft(new BigDecimal(0));
		location.setUtilizedVolCbm(new BigDecimal(0));
		
		storage = new Storage();
		storage.setUpdatedOn(new Date());
		storage.setCreatedOn(new Date());
		location.setUpdatedBy(getRequest().getRemoteUser());
		
		//System.out.println("\n\n formShow() called ...\n\n");
		return SUCCESS;
	}
	public String save() throws Exception {
		//System.out.println("\n\n save() called");
		if (from != null && !from.equalsIgnoreCase("main")) {
			workTicket = workTicketManager.get(id1);
		}
		getComboList(sessionCorpID);
		if (cancel != null) {
			return "cancel";
		}
		if (delete != null) {
			return delete();
		}
		boolean isNew = (location.getId() == null);

		String key = (isNew) ? "location.added" : "location.updated";
		saveMessage(getText(key));
		if(location.getOccupied() != null){
			if (location.getOccupied().equalsIgnoreCase("true")) {
				location.setOccupied("X");
			} else {
				location.setOccupied("");
			}
		}else{
			location.setOccupied("");
		}
		

		if (!isNew) {
			location.setCreatedOn(createdon);
		} else {
			if (prifix.length() == 1 && (prifix.equalsIgnoreCase("0") || prifix.equalsIgnoreCase("1") || prifix.equalsIgnoreCase("2") || prifix.equalsIgnoreCase("3")
							|| prifix.equalsIgnoreCase("4") || prifix.equalsIgnoreCase("5") || prifix.equalsIgnoreCase("6") || prifix.equalsIgnoreCase("7")
							|| prifix.equalsIgnoreCase("8") || prifix.equalsIgnoreCase("9"))) {
				prifix = "0".concat(prifix);
			}
			location.setLocationId(prifix.concat(location.getLocationId()));
			location.setCreatedOn(new Date());

		}
		location.setCorpID(sessionCorpID);
		location.setUpdatedOn(new Date());
		location.setUpdatedBy(getRequest().getRemoteUser());
		if(!from.equalsIgnoreCase("main"))
		{
			getNotesForIconChange(); 
		}
		locationManager.save(location);
		return SUCCESS;
	}

	public String search() {
		if(location!=null){
		String locationId = location.getLocationId() != null ? location.getLocationId() : "";
		String warehouse = location.getWarehouse() != null ? location.getWarehouse() : "";
		String type = location.getType() != null ? location.getType() : "";
		String capacity = location.getCapacity() != null ? location.getCapacity() : "";
		//locations = locationManager.findByLocation(locationId, type, occupieType, capacity, sessionCorpID);
		locations = locationManager.findByLocationWithWarehouse(locationId, type, occupieType, capacity, sessionCorpID,warehouse);
		}else{
			locations = locationManager.locationList(sessionCorpID);
		}
		getComboList(sessionCorpID);
		return SUCCESS;
	}
	
	public String searchLocationForStorage() {
		String locationId = location.getLocationId() != null ? location.getLocationId() : "";
		String type = location.getType() != null ? location.getType() : "";
		locations = locationManager.searchLocationForStorage(locationId, type,sessionCorpID,whouse);
		getComboList(sessionCorpID);
		return SUCCESS;
	}
	
	public String workTicketByLocation(){
		wtSoDetails=locationManager.workTicketByLocation(locationId);
		return SUCCESS;
	}
	public String getComboList(String corpID) {
		loctype_isactive = new LinkedHashMap<String, String>();
		List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpID,"'LOCTYPE'");
		for (RefMasterDTO refObj : allParamValue) {
			loctype_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		}
		locTYPE = refMasterManager.findByParameter(corpID, "LOCTYPE");
		capacity = refMasterManager.findByParameter(corpID, "capacity");
		occupiedX = refMasterManager.occupied("OCCUPIED", sessionCorpID);
		house = refMasterManager.findByParameter(corpID, "HOUSE");

		weightunits = new ArrayList();
		weightunits.add("Lbs");
		weightunits.add("Kgs");
		volumeunits = new ArrayList();
	  	volumeunits.add("Cft");
	  	volumeunits.add("Cbm");
		return SUCCESS;
	}

	public String getNotesForIconChange() {		
		List m = notesManager.countTicketLocationNotes(ticket);
		//System.out.println("\n\n\n\nNotes method called------------>"+ticket);

		if (m.isEmpty()) {
			countTicketLocationNotes = "0";
		//	System.out.println("\n 11111111111111111==========>Notes Value::"+countTicketLocationNotes);
		} else {
			countTicketLocationNotes = ((m).get(0)).toString();
		//	System.out.println("\n 22222222222222222====>Notes Value::"+countTicketLocationNotes);
		}
		return SUCCESS;
	}

	public String getCountTicketLocationNotes() {
		return countTicketLocationNotes;
	}

	public void setCountTicketLocationNotes(String countTicketLocationNotes) {
		this.countTicketLocationNotes = countTicketLocationNotes;
	}

	public Long getTicket() {
		return ticket;
	}

	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public void setLocationManager(LocationManager locationManager) {
		this.locationManager = locationManager;
	}

	public void setStorageManager(StorageManager storageManager) {
		this.storageManager = storageManager;
	}

	/*
	 * public void setBookStorageManager(BookStorageManager bookStorageManager) {
	 * this.bookStorageManager = bookStorageManager; }
	 */

	public List getLocations() {
		return locations;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public BookStorage getBookStorage() {
		return bookStorage;
	}

	public void setBookStorage(BookStorage bookStorage) {
		this.bookStorage = bookStorage;
	}

	public Storage getStorage() {
		return storage;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public static Map<String, String> getActionType() {
		return actionType;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public static Date getCreatedon() {
		return createdon;
	}

	public static void setCreatedon(Date createdon) {
		LocationAction.createdon = createdon;
	}

	public boolean isOccupied() {
		return occupied;
	}

	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}

	public String getOccupy() {
		return occupy;
	}

	public void setOccupy(String occupy) {
		this.occupy = occupy;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public static Map<String, String> getCapacity() {
		return capacity;
	}

	public static void setCapacity(Map<String, String> capacity) {
		LocationAction.capacity = capacity;
	}

	public static Map<String, String> getLocTYPE() {
		return locTYPE;
	}

	public static void setLocTYPE(Map<String, String> locTYPE) {
		LocationAction.locTYPE = locTYPE;
	}

	public static List<String> getOccupiedX() {
		return occupiedX;
	}

	public static void setOccupiedX(List<String> occupiedX) {
		LocationAction.occupiedX = occupiedX;
	}

	public static List getWeightunits() {
		return weightunits;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getOccupieType() {
		return occupieType;
	}

	public void setOccupieType(String occupieType) {
		this.occupieType = occupieType;
	}

	public Long getId1() {
		return id1;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public static Map<String, String> getHouse() {
		return house;
	}

	public static void setHouse(Map<String, String> house) {
		LocationAction.house = house;
	}

	public String getPrifix() {
		return prifix;
	}

	public void setPrifix(String prifix) {
		this.prifix = prifix;
	}

	public Long getId() {
		return id;
	}

	public List getVolumeunits() {
		return volumeunits;
	}

	public void setVolumeunits(List volumeunits) {
		this.volumeunits = volumeunits;
	}

	public StorageLibrary getStorageLibrary() {
		return storageLibrary;
	}

	public void setStorageLibrary(StorageLibrary storageLibrary) {
		this.storageLibrary = storageLibrary;
	}

	public void setStorageLibraryManager(StorageLibraryManager storageLibraryManager) {
		this.storageLibraryManager = storageLibraryManager;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStorageLibrayType() {
		return storageLibrayType;
	}

	public void setStorageLibrayType(String storageLibrayType) {
		this.storageLibrayType = storageLibrayType;
	}

	public String getWhouse() {
		return whouse;
	}

	public void setWhouse(String whouse) {
		this.whouse = whouse;
	}

	public String getStorageTypeCode() {
		return storageTypeCode;
	}

	public void setStorageTypeCode(String storageTypeCode) {
		this.storageTypeCode = storageTypeCode;
	}

	public String getLocationByStorage() {
		return locationByStorage;
	}

	public void setLocationByStorage(String locationByStorage) {
		this.locationByStorage = locationByStorage;
	}

	public List getStorageLocationId() {
		return storageLocationId;
	}

	public void setStorageLocationId(List storageLocationId) {
		this.storageLocationId = storageLocationId;
	}

	public String getCheckForSameStorage() {
		return checkForSameStorage;
	}

	public void setCheckForSameStorage(String checkForSameStorage) {
		this.checkForSameStorage = checkForSameStorage;
	}

	public List getWtSoDetails() {
		return wtSoDetails;
	}

	public void setWtSoDetails(List wtSoDetails) {
		this.wtSoDetails = wtSoDetails;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public static Map<String, String> getLoctype_isactive() {
		return loctype_isactive;
	}

	public static void setLoctype_isactive(Map<String, String> loctype_isactive) {
		LocationAction.loctype_isactive = loctype_isactive;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}
}
