//--Created By Bibhash Kumar Pathak---

package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.service.DataCatalogManager;
import com.trilasoft.app.service.RefMasterManager;

public class DataCatalogAction extends BaseAction implements Preparable{
	
	
	private Long id;
	private Long maxId;
	private String sessionCorpID;
	private DataCatalogManager dataCatalogManager; 
	private DataCatalog dataCatalog;
	private List<DataCatalog> dataCatalogList;
	private Set DataCatalogSet;
	private List searchList;	
	private String rulesfield;
	private String savedataCatalog;
	private String tableName;
	private String defineByToDoRule;
	private String auditable;
	private String charge;
	private String fieldName;
	private String description;
	/* Added By Kunal Sharma. Ticket Number: 5953 */
	private String usDomestic;
	/* private String isdateField; */
	private String visible;
	private  Map<String,String>usDomesticList;
	Date currentdate = new Date();
	private RefMasterManager refMasterManager;

	static final Logger logger = Logger.getLogger(DataCatalogAction.class);
	public void prepare() throws Exception {
		usDomesticList = refMasterManager.findByParameter(sessionCorpID, "FieldType");
	}
		
	 public DataCatalogAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}
	 @SkipValidation
	public String catalogList(){
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			dataCatalogList=dataCatalogManager.getList();
		//DataCatalogSet=new HashSet(dataCatalogList);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");		
	return SUCCESS;

	 }
	 
	 @SkipValidation
	 public String delete()	{
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 dataCatalogManager.remove(id); 
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		      return SUCCESS;		    
		}
	 
	public String getRulesfield() {
		return rulesfield;
	}

	public void setRulesfield(String rulesfield) {
		this.rulesfield = rulesfield;
	}
	
	public void setsavedataCatalog(String searchdataCatalog) {   
        this.savedataCatalog = searchdataCatalog;   
    }
	@SkipValidation
	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id != null)
		{
			dataCatalog=dataCatalogManager.get(id);
		}
		else
		{
			dataCatalog=new DataCatalog();
			dataCatalog.setCreatedOn(new Date());
			dataCatalog.setValueDate(new Date());		
			dataCatalog.setUpdatedOn(new Date());
		}
		dataCatalog.setCorpID("TSFT");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
}
	
    
	
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (dataCatalog.getId() == null); 
		
		if(isNew)
	    {
			dataCatalog.setCreatedOn(new Date());
	    }
	    
		rulesfield=dataCatalog.getTableName().toLowerCase() +"."+ dataCatalog.getFieldName();
		dataCatalog.setRulesFiledName(rulesfield);
		dataCatalog.setCorpID("TSFT");
		dataCatalog.setUpdatedOn(new Date());
		dataCatalog.setUpdatedBy(getRequest().getRemoteUser());
		dataCatalog= dataCatalogManager.save(dataCatalog);
		search();
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "DataCatalog has been added successfully" : "DataCatalog has been updated successfully";
		saveMessage(getText(key));
		}
		if (!isNew) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return INPUT;   
        } else {   
        	maxId = Long.parseLong(dataCatalogManager.findMaximumId().get(0).toString());
        	dataCatalog = dataCatalogManager.get(maxId);
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
          return SUCCESS;
        } 
	}

	@SkipValidation
	  public String search()
	    {  
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//boolean auditableFlag = false;
	    /*boolean tableName = (dataCatalog.getTableName() == null);
	    boolean fieldName = (dataCatalog.getFieldName() == null);
	    boolean auditable = (dataCatalog.getAuditable() == null);
	    boolean description = (dataCatalog.getDescription() == null);
	    boolean defineByToDoRule = (dataCatalog.getDefineByToDoRule() == null);
	    boolean isdateField = (dataCatalog.getIsdateField() == null);
	    boolean configerable = (dataCatalog.getConfigerable() == null);
	    System.out.println(dataCatalog.getAuditable());
	    System.out.println(dataCatalog.getDefineByToDoRule());
	    System.out.println(dataCatalog.getIsdateField());
	    System.out.println(dataCatalog.getConfigerable());*/
	    
	      // if(!tableName || !fieldName || !auditable || !description || !defineByToDoRule || !isdateField || !configerable)
	       //{
	    	   dataCatalogList = dataCatalogManager.searchdataCatalog(tableName, fieldName, auditable, description, usDomestic, defineByToDoRule, visible, charge);
	    	   //DataCatalogSet =new HashSet(searchList);
	    //}
	   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    return SUCCESS;     
	    } 
	
	private String gotoPageString;
	private String validateFormNav;
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		validateFormNav = "OK";
    	String s = save();
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return gotoPageString;
    }
	
	
	
	public DataCatalog getDataCatalog() {
		return dataCatalog;
	}

	public void setDataCatalog(DataCatalog dataCatalog) {
		this.dataCatalog = dataCatalog;
	}

	public void setDataCatalogManager(DataCatalogManager dataCatalogManager) {
		this.dataCatalogManager = dataCatalogManager;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public String getDefineByToDoRule() {
		return defineByToDoRule;
	}

	public void setDefineByToDoRule(String defineByToDoRule) {
		this.defineByToDoRule = defineByToDoRule;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<DataCatalog> getDataCatalogList() {
		return dataCatalogList;
	}

	public void setDataCatalogList(List<DataCatalog> dataCatalogList) {
		this.dataCatalogList = dataCatalogList;
	}

	public Set getDataCatalogSet() {
		return DataCatalogSet;
	}

	public void setDataCatalogSet(Set dataCatalogSet) {
		DataCatalogSet = dataCatalogSet;
	}

	public List getSearchList() {
		return searchList;
	}

	public void setSearchList(List searchList) {
		this.searchList = searchList;
	}

	public String getAuditable() {
		return auditable;
	}

	public void setAuditable(String auditable) {
		this.auditable = auditable;
	}
	
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/*public String getIsdateField() {
		return isdateField;
	}

	public void setIsdateField(String isdateField) {
		this.isdateField = isdateField;
	}*/

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public Map<String, String> getUsDomesticList() {
		return usDomesticList;
	}

	public void setUsDomesticList(Map<String, String> usDomesticList) {
		this.usDomesticList = usDomesticList;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getUsDomestic() {
		return usDomestic;
	}

	public void setUsDomestic(String usDomestic) {
		this.usDomestic = usDomestic;
	}

}
