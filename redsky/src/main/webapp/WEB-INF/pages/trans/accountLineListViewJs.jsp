<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/common/taglibs.jsp"%>
<script language="JavaScript" type="text/javascript">
function calculateMGMTFees() { 
	showOrHideAutoSave('1');	
	window.location = "MGMTFeesCMMCalculate.html?btntype=yes&sid="+${serviceOrder.id}; 
	}
function calculateDiscountFees(){
	showOrHideAutoSave('1'); 
	var url="findDiscountFeeVat.html?ajax=1&&decorator=simple&popup=true&sid="+${serviceOrder.id};
	http5555.open("GET", url, true);
	http5555.onreadystatechange = httpResponseCalculateDiscountFees;
	http5555.send(null);
	}   

	function httpResponseCalculateDiscountFees(){
	 if (http5555.readyState == 4){ 
	     var results = http5555.responseText 
	     results = results.trim();
		 results = results.replace('[','');
         results=results.replace(']','');  
         if(results.length>2)
         {
        	 showOrHideAutoSave(0);
             alert("For Account line "+results+ ", Payable VAT code is missing - DMM Fee cannot be generated") 
         } else{
        	 window.location = "discountfeesCalculate.html?btntype=yes&sid="+${serviceOrder.id}; 
        	 //document.forms['serviceForm1'].action = 'discounteesCalculate.html?btntype=yes';
        	 //document.forms['serviceForm1'].submit();
         } 
	                         
	}
	}	
function findAccountLineActiveList(){
	showOrHide(1);
	var accountLineStatus = 'true';
	var accountLineSortValue = document.forms['pricingListForm'].elements['accountLineSortValue'].value;
	new Ajax.Request('findAccountlineActiveListAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}&sid=${serviceOrder.id}&accountLineStatus='+accountLineStatus+'&accountLineSortValue='+accountLineSortValue,
			{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			response = response.trim();
			var accLineDetailsDiv = document.getElementById("accountLineRows");
			accLineDetailsDiv.innerHTML = response;
			jQuery.noConflict();
			(function ($) {
				$(".tablesorter")
					.collapsible("td.collapsible", {
						collapse: true
					})
				.tablesorter({})
				.tablesorterPager({container: $("#pager"), positionFixed: false});
				$('#expandList')
			    .unbind('click')
			    .click( function() {
			        $('.collapsed').removeClass('collapsed').addClass('expanded').css("display","table-cell");
			        $("table tr.expand-child td").css("display","table-cell").show();
			    })
			    
			    $('#collapseList')
			    .unbind('click')
			    .click( function() {
			        $('.expanded').removeClass('expanded').addClass('collapsed');
			        $("table tr.expand-child td").css("display","none").hide();
			    })
			})(jQuery);
			showOrHide(0);
		},
		onFailure: function(){ 
		}
	});
	
}
function findAccountlineAllList(){
	showOrHide(1);
	var accountLineStatus = 'allStatus';
	var accountLineSortValue = document.forms['pricingListForm'].elements['accountLineSortValue'].value;
	new Ajax.Request('findAccountlineAllListAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}&sid=${serviceOrder.id}&accountLineStatus='+accountLineStatus+'&accountLineSortValue='+accountLineSortValue,
			{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			response = response.trim();
			var accLineDetailsDiv = document.getElementById("accountLineRows");
			accLineDetailsDiv.innerHTML = response;
			jQuery.noConflict();
			(function ($) {
				$(".tablesorter")
					.collapsible("td.collapsible", {
						collapse: true
					})
				.tablesorter({})
				.tablesorterPager({container: $("#pager"), positionFixed: false});
				$('#expandList')
			    .unbind('click')
			    .click( function() {
			        $('.collapsed').removeClass('collapsed').addClass('expanded').css("display","table-cell");
			        $("table tr.expand-child td").css("display","table-cell").show();
			    })
			    
			    $('#collapseList')
			    .unbind('click')
			    .click( function() {
			        $('.expanded').removeClass('expanded').addClass('collapsed');
			        $("table tr.expand-child td").css("display","none").hide();
			    })
			})(jQuery);
			showOrHide(0);
		},
		onFailure: function(){ 
		}
	});
}

function showOrHide(value) {
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} 
} 
showOrHide(0);
function checkBillingComplete() {
	if('${billing.contract}'==''){
		alert("There is no pricing contract in billing: Please select.");
		return false;
	}else if('${billing.billComplete}' !=''){ 
		var agree = confirm("The billing has been completed, do you still want to add lines?");
		if(agree){
			findAllPricingLineId('AddLine');
		}else {         }
	}else{
		findAllPricingLineId('AddLine');
	}
}

function addLineInAccountLineListViewAjax(){
	showOrHide(1);
	var accountLineStatus = document.forms['pricingListForm'].elements['accountLineStatus'].value;
	var accountLineSortValue = document.forms['pricingListForm'].elements['accountLineSortValue'].value;
	new Ajax.Request('addLineInAccountLineListViewAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}&accountLineStatus='+accountLineStatus+'&accountLineSortValue='+accountLineSortValue,
			{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			response = response.trim();
			var accLineDetailsDiv = document.getElementById("accountLineRows");
			accLineDetailsDiv.innerHTML = response;
			jQuery.noConflict();
			(function ($) {
				$(".tablesorter")
					.collapsible("td.collapsible", {
						collapse: true
					})
				.tablesorter({})
				.tablesorterPager({container: $("#pager"), positionFixed: false});
				$('#expandList')
			    .unbind('click')
			    .click( function() {
			        $('.collapsed').removeClass('collapsed').addClass('expanded').css("display","table-cell");
			        $("table tr.expand-child td").css("display","table-cell").show();
			    })
			    
			    $('#collapseList')
			    .unbind('click')
			    .click( function() {
			        $('.expanded').removeClass('expanded').addClass('collapsed');
			        $("table tr.expand-child td").css("display","none").hide();
			    })
			})(jQuery);
			showOrHide(0);
		},
		onFailure: function(){ 
		}
	});
}

function addDefaultTemplateAjax(){
	showOrHide(1);
	var accountLineStatus = document.forms['pricingListForm'].elements['accountLineStatus'].value;
	var accountLineSortValue = document.forms['pricingListForm'].elements['accountLineSortValue'].value;
	new Ajax.Request('addDefaultTemplateAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}&accountLineStatus='+accountLineStatus+'&accountLineSortValue='+accountLineSortValue,
			{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			response = response.trim();
			var accLineDetailsDiv = document.getElementById("accountLineRows");
			accLineDetailsDiv.innerHTML = response;
			jQuery.noConflict();
			(function ($) {
				$(".tablesorter")
					.collapsible("td.collapsible", {
						collapse: true
					})
				.tablesorter({})
				.tablesorterPager({container: $("#pager"), positionFixed: false});
				$('#expandList')
			    .unbind('click')
			    .click( function() {
			        $('.collapsed').removeClass('collapsed').addClass('expanded').css("display","table-cell");
			        $("table tr.expand-child td").css("display","table-cell").show();
			    })
			    
			    $('#collapseList')
			    .unbind('click')
			    .click( function() {
			        $('.expanded').removeClass('expanded').addClass('collapsed');
			        $("table tr.expand-child td").css("display","none").hide();
			    })
			})(jQuery);
			showOrHide(0);
		},
		onFailure: function(){ 
		}
	});
}
function addDefaultAccountingTemplateAjax(){
		var customerFileContract='${billing.contract}';
		if(customerFileContract==''){
			alert("Please select Pricing Contract from billing");
			return false;
		}
		if('${billing.billComplete}'!=''){ 
			var agree = confirm("The billing has been completed, do you still want to add lines?");
			if(agree){
				
				showOrHide(1);
				var url="checkDefaultAccountingTemplateAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
				httpTemplate.open("POST", url, true);
				httpTemplate.onreadystatechange = handleHttpResponseForAddTemplate;
				httpTemplate.send(null);
			}else{}
		}else{
			
			showOrHide(1);
			var url="checkDefaultAccountingTemplateAjax.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}";
			httpTemplate.open("POST", url, true);
			httpTemplate.onreadystatechange = handleHttpResponseForAddTemplate;
			httpTemplate.send(null);
		}
}
function handleHttpResponseForAddTemplate(){
	if (httpTemplate.readyState == 4){
		showOrHide(0);
		var results = httpTemplate.responseText;
		results = results.trim();
		if(results == 'NO'){
			alert('No Default Template Found');
			return false;
		}else{
			findAllPricingLineId('AddTemplate');
		}
	}
}

function getId(aid){
	if(aid=='temp'){
		aid=accountLineIdScript;
	}  
	var accIdList = document.getElementById("accId").value;
	if(accIdList!=''){
		var check = accIdList.indexOf(aid);
		if(check > -1){
		}else{
			accIdList = accIdList+','+aid;
		}
	}else{
		accIdList = aid;
	}
	document.getElementById("accId").value = accIdList;
}


var pricingData = {records: []};
function findAllPricingLineId(str){
	var idList = document.getElementById("accId").value;
	
	if(str == 'AddLine' && idList == ''){
		addLineInAccountLineListViewAjax();
	}else if(str == 'AddTemplate' && idList== ''){
		addDefaultTemplateAjax();
	}else{
		var id = idList.split(",");
		var check =saveValidationPricing();
		if(check){
			pricingData = {records: []};
			if(id!=''){
				for (i=0;i<id.length;i++){
					insertPricingData(id[i].trim());
					
					if(pricingData.records.length == id.length){
						autoSavePricingAjax(str);
					}
				}
			}else{
				autoSavePricingAjax(str);
			}
		}
	}
}

function saveValidationPricing(){
	var idList = document.getElementById("accId").value;
	var ChargesMandatory='${checkContractChargesMandatory}';
	var contractType = '${contractType}';
	
		if(idList!=''){			
			var id = idList.split(",");
			for (var i=0;i<id.length;i++){
					var chargeCodeVal = document.forms['pricingListForm'].elements['chargeCode'+id[i].trim()].value;
					var categoryVal = document.forms['pricingListForm'].elements['category'+id[i].trim()].value;
					var companyDivisionVal = document.forms['pricingListForm'].elements['companyDivision'+id[i].trim()].value;
					var vendorCodeVal = document.forms['pricingListForm'].elements['vendorCode'+id[i].trim()].value;
					var lineNo = document.forms['pricingListForm'].elements['accountLineNumber'+id[i].trim()].value;
					var actualRevenueVal = document.forms['pricingListForm'].elements['actualRevenue'+id[i].trim()].value;
					
					if(categoryVal.trim()==''){
						alert("Please select Category for # "+lineNo);
						return false;
					}
					if(companyDivisionVal.trim()==''){
						alert("Please select Division for # "+lineNo);
						return false;
					}
					if(chargeCodeVal.trim()=='' && (ChargesMandatory=='1' || contractType=='true')){
						alert("Please select Charge Code for # "+lineNo);
						return false;
					}
					if((actualRevenueVal!=0 || actualRevenueVal!=0.00) && (chargeCodeVal.trim()==null || chargeCodeVal.trim()=='')){
						alert("You can not remove Charge Code as there is value in the Receivable details section for # "+lineNo);
						return false;
					}
					
					if(document.getElementById("chargeCodeValidationMessage").value!=''){
						var chargeCodeid=id[i].trim();
						if(document.getElementById("chargeCodeValidationMessage").value==chargeCodeid){
							alert("Charge code does not exist according to the pricing contract for # "+lineNo+", Please select another.");
							return false;
						}
					}
					<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
						var revisionExpenseVal = document.forms['pricingListForm'].elements['revisionExpense'+id[i].trim()].value;
						if(((revisionExpenseVal!=0) || (revisionExpenseVal!=0.00) || (revisionExpenseVal!=0.0)) && ((vendorCodeVal == null) || (vendorCodeVal==''))){
							alert("Revision Expense cannot be entered as no Partner has been selected.\nPlease select a partner before entering revision expense for # "+lineNo);
							return false;
						}
					</c:if>
			}
			return true; 
		}else{
			return true;	
		}
	}

var addPriceLine ='';
function autoSavePricingAjax(str){
	var check =saveValidationPricing();
	var idList = document.getElementById("accId").value;
	var accountLineSortValue = document.forms['pricingListForm'].elements['accountLineSortValue'].value;
	if(check){
		if(idList =='' && str == ''){
			alert('Nothing found to save.');
			return false;
		}else{
			showOrHide(1);
			var actualDiscountFlag ="FALSE";
			var accountLineStatus = document.forms['pricingListForm'].elements['accountLineStatus'].value;
			try{
				<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				actualDiscountFlag="TRUE";
				</c:if>
			}catch(e){}
			var revisionDiscountFlag ="FALSE";
			try{
				<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				revisionDiscountFlag="TRUE";
				</c:if>
			}catch(e){}				 			  
					
			//alert(JSON.stringify(pricingData))
			$.ajax({
				type: "POST",
				url: "saveAccountlineAjax.html?ajax=1&decorator=simple&popup=true",
				data: { sid: '${serviceOrder.id}', id: '${serviceOrder.id}',actualDiscountFlag:actualDiscountFlag,revisionDiscountFlag:revisionDiscountFlag,
					addPriceLine:str,accountLineStatus:accountLineStatus,accountLineSortValue:accountLineSortValue,pricingData:JSON.stringify(pricingData)
				},
				success: function (data, textStatus, jqXHR) {
					
				var accLineDetailsDiv = document.getElementById("accountLineRows");
		    	accLineDetailsDiv.innerHTML = data;
		    	jQuery.noConflict();
				(function ($) {
					$(".tablesorter")
						.collapsible("td.collapsible", {
							collapse: true
						})
					.tablesorter({})
					.tablesorterPager({container: $("#pager"), positionFixed: false});
		    		$('#expandList')
				    .unbind('click')
				    .click( function() {
				        $('.collapsed').removeClass('collapsed').addClass('expanded').css("display","table-cell");
				        $("table tr.expand-child td").css("display","table-cell").show();
				    })
				    
				    $('#collapseList')
				    .unbind('click')
				    .click( function() {
				        $('.expanded').removeClass('expanded').addClass('collapsed');
				        $("table tr.expand-child td").css("display","none").hide();
				    })
				})(jQuery);
				showOrHide(0);
				},
				error: function (data, textStatus, jqXHR) {
				}
			});
		}
	}
}

function insertPricingData(id){
	try{
		pricingData.records.push({
			"id": id,
			"category": getFieldValue('category'+id), 
			"chargeCode":getFieldValue('chargeCode'+id), 
			"vendorCode": getFieldValue('vendorCode'+id), 
			"estimateVendorName":getFieldValue('estimateVendorName'+id), 
			"basis": getFieldValue('basis'+id), 
			"estimateQuantity":getFieldValue('estimateQuantity'+id), 
			"estimateRate":getFieldValue('estimateRate'+id), 
			"estimateSellRate":getFieldValue('estimateSellRate'+id), 
			"estimateExpense":getFieldValue('estimateExpense'+id), 
			"estimatePassPercentage":getFieldValue('estimatePassPercentage'+id), 
			"estimateRevenueAmount":getFieldValue('estimateRevenueAmount'+id), 
			"quoteDescription":getFieldValue('quoteDescription'+id), 
			"statusCheck":getFieldValue('statusCheck'+id), 
			"companyDivision":getFieldValue('companyDivision'+id), 
			"division":getFieldValue('division'+id), 
			"vatDecr":getFieldValue('vatDecr'+id), 
			"vatAmt":getFieldValue('vatAmt'+id), 
			"deviation":getFieldValue('deviation'+id), 
			"estimateDeviation":getFieldValue('estimateDeviation'+id), 
			"estimateSellDeviation":getFieldValue('estimateSellDeviation'+id), 
			"estCurrencyNew":getFieldValue('estCurrencyNew'+id), 
			"estSellCurrencyNew":getFieldValue('estSellCurrencyNew'+id), 
			"estValueDateNew":getFieldValue('estValueDateNew'+id), 
			"estSellValueDateNew":getFieldValue('estSellValueDateNew'+id), 
			"estExchangeRateNew":getFieldValue('estExchangeRateNew'+id),
			"estSellExchangeRateNew":getFieldValue('estSellExchangeRateNew'+id), 
			"estLocalRateNew":getFieldValue('estLocalRateNew'+id), 
			"estSellLocalRateNew":getFieldValue('estSellLocalRateNew'+id), 
			"estLocalAmountNew":getFieldValue('estLocalAmountNew'+id), 
			"estSellLocalAmountNew":getFieldValue('estSellLocalAmountNew'+id), 
			"revisionCurrencyNew":getFieldValue('revisionCurrencyNew'+id), 
			"countryNew":getFieldValue('countryNew'+id), 
			"revisionValueDateNew":getFieldValue('revisionValueDateNew'+id), 
			"valueDateNew":getFieldValue('valueDateNew'+id), 
			"revisionExchangeRateNew":getFieldValue('revisionExchangeRateNew'+id), 
			"exchangeRateNew":getFieldValue('exchangeRateNew'+id), 
			"estimatePayableContractCurrencyNew":getFieldValue('estimatePayableContractCurrencyNew'+id),
			"estimateContractCurrencyNew":getFieldValue('estimateContractCurrencyNew'+id),
			"estimatePayableContractValueDateNew":getFieldValue('estimatePayableContractValueDateNew'+id),
			"estimateContractValueDateNew":getFieldValue('estimateContractValueDateNew'+id),
			"estimatePayableContractExchangeRateNew":getFieldValue('estimatePayableContractExchangeRateNew'+id),
			"estimatePayableContractExchangeRateNew":getFieldValue('estimatePayableContractExchangeRateNew'+id),
			"estimateContractExchangeRateNew":getFieldValue('estimateContractExchangeRateNew'+id),
			"estimatePayableContractRateNew":getFieldValue('estimatePayableContractRateNew'+id),
			"estimateContractRateNew":getFieldValue('estimateContractRateNew'+id),
			"estimatePayableContractRateAmmountNew":getFieldValue('estimatePayableContractRateAmmountNew'+id),
			"estimateContractRateAmmountNew":getFieldValue('estimateContractRateAmmountNew'+id),
			"revisionContractCurrencyNew":getFieldValue('revisionContractCurrencyNew'+id),
			"revisionPayableContractCurrencyNew":getFieldValue('revisionPayableContractCurrencyNew'+id),
			"contractCurrencyNew":getFieldValue('contractCurrencyNew'+id),
			"payableContractCurrencyNew":getFieldValue('payableContractCurrencyNew'+id),
			"revisionContractValueDateNew":getFieldValue('revisionContractValueDateNew'+id),
			"revisionPayableContractValueDateNew":getFieldValue('revisionPayableContractValueDateNew'+id),
			"contractValueDateNew":getFieldValue('contractValueDateNew'+id),
			"payableContractValueDateNew":getFieldValue('payableContractValueDateNew'+id),
			"revisionPayableContractExchangeRateNew":getFieldValue('revisionPayableContractExchangeRateNew'+id),
			"contractExchangeRateNew":getFieldValue('contractExchangeRateNew'+id),
			"payableContractExchangeRateNew":getFieldValue('payableContractExchangeRateNew'+id),
			"accountLineNumber":getFieldValue('accountLineNumber'+id),
			"pricePointAgent":getFieldValue('pricePointAgent'+id),
			"pricePointMarket":getFieldValue('pricePointMarket'+id),
			"pricePointTariff":getFieldValue('pricePointTariff'+id),
			"estimateDiscount":getFieldValue('estimateDiscount'+id),
			"actualRevenue":getFieldValue('actualRevenue'+id),
			"recRate":getFieldValue('recRate'+id),
			"recQuantity":getFieldValue('recQuantity'+id),
			"actualExpense":getFieldValue('actualExpense'+id),
			"actualDiscount":getFieldValue('actualDiscount'+id),
			"payableContractCurrency":getFieldValue('payableContractCurrency'+id),
			"payableContractExchangeRate":getFieldValue('payableContractExchangeRate'+id),
			"payableContractRateAmmount":getFieldValue('payableContractRateAmmount'+id),
			"payableContractValueDate":getFieldValue('payableContractValueDate'+id),
			"country":getFieldValue('country'+id),
			"exchangeRate":getFieldValue('exchangeRate'+id),
			"localAmount":getFieldValue('localAmount'+id),
			"valueDate":getFieldValue('valueDate'+id),
			"contractCurrency":getFieldValue('contractCurrency'+id),
			"contractExchangeRate":getFieldValue('contractExchangeRate'+id),
			"contractRate":getFieldValue('contractRate'+id),
			"contractValueDate":getFieldValue('contractValueDate'+id),
			"contractRateAmmount":getFieldValue('contractRateAmmount'+id),
			"recRateCurrency":getFieldValue('recRateCurrency'+id),
			"recRateExchange":getFieldValue('recRateExchange'+id),
			"recCurrencyRate":getFieldValue('recCurrencyRate'+id),
			"actualRevenueForeign":getFieldValue('actualRevenueForeign'+id),
			"racValueDate":getFieldValue('racValueDate'+id),
			"revisionPayableContractCurrency":getFieldValue('revisionPayableContractCurrency'+id),
			"revisionPayableContractValueDate":getFieldValue('revisionPayableContractValueDate'+id),
			"revisionPayableContractExchangeRate":getFieldValue('revisionPayableContractExchangeRate'+id),
			"revisionPayableContractRate":getFieldValue('revisionPayableContractRate'+id),
			"revisionPayableContractRateAmmount":getFieldValue('revisionPayableContractRateAmmount'+id),
			"revisionContractCurrency":getFieldValue('revisionContractCurrency'+id),
			"revisionContractValueDate":getFieldValue('revisionContractValueDate'+id),
			"revisionContractExchangeRate":getFieldValue('revisionContractExchangeRate'+id),
			"revisionContractRate":getFieldValue('revisionContractRate'+id),
			"revisionContractRateAmmount":getFieldValue('revisionContractRateAmmount'+id),
			"revisionCurrency":getFieldValue('revisionCurrency'+id),
			"revisionValueDate":getFieldValue('revisionValueDate'+id),
			"revisionExchangeRate":getFieldValue('revisionExchangeRate'+id),
			"revisionLocalRate":getFieldValue('revisionLocalRate'+id),
			"revisionLocalAmount":getFieldValue('revisionLocalAmount'+id),
			"revisionSellCurrency":getFieldValue('revisionSellCurrency'+id),
			"revisionSellValueDate":getFieldValue('revisionSellValueDate'+id),
			"revisionSellExchangeRate":getFieldValue('revisionSellExchangeRate'+id),
			"revisionSellLocalRate":getFieldValue('revisionSellLocalRate'+id),
			"revisionSellLocalAmount":getFieldValue('revisionSellLocalAmount'+id),
			"revisionQuantity":getFieldValue('revisionQuantity'+id),
			"revisionDiscount":getFieldValue('revisionDiscount'+id),
			"revisionRate":getFieldValue('revisionRate'+id),
			"revisionSellRate":getFieldValue('revisionSellRate'+id),
			"revisionExpense":getFieldValue('revisionExpense'+id),
			"revisionRevenueAmount":getFieldValue('revisionRevenueAmount'+id),
			"revisionPassPercentage":getFieldValue('revisionPassPercentage'+id),
			"revisionVatDescr":getFieldValue('revisionVatDescr'+id),
			"revisionVatPercent":getFieldValue('revisionVatPercent'+id),
			"revisionVatAmt":getFieldValue('revisionVatAmt'+id),
			"recVatDescr":getFieldValue('recVatDescr'+id),
			"recVatPercent":getFieldValue('recVatPercent'+id),
			"recVatAmt":getFieldValue('recVatAmt'+id),
			"payVatDescr":getFieldValue('payVatDescr'+id),
			"payVatPercent":getFieldValue('payVatPercent'+id),
			"payVatAmt":getFieldValue('payVatAmt'+id),
			"payingStatus":getFieldValue('payingStatus'+id),
			"receivedDate":getFieldValue('receivedDate'+id),
			"invoiceNumber":getFieldValue('invoiceNumber'+id),
			"invoiceDate":getFieldValue('invoiceDate'+id),
			"estimateSellQuantity":getFieldValue('estimateSellQuantity'+id),
			"revisionSellQuantity":getFieldValue('revisionSellQuantity'+id),
			"accountLineCostElement":getFieldValue('accountLineCostElement'+id),
			"accountLineScostElementDescription":getFieldValue('accountLineScostElementDescription'+id),
			"estExpVatAmt":getFieldValue('estExpVatAmt'+id),
			"revisionExpVatAmt":getFieldValue('revisionExpVatAmt'+id),
			"recVatGl":getFieldValue('recVatGl'+id),
			"payVatGl":getFieldValue('payVatGl'+id),
			"qstRecVatGl":getFieldValue('qstRecVatGl'+id),
			"qstPayVatGl":getFieldValue('qstPayVatGl'+id),
			"qstRecVatAmt":getFieldValue('qstRecVatAmt'+id),
			"qstPayVatAmt":getFieldValue('qstPayVatAmt'+id),
			"varianceRevenueAmount":getFieldValue('varianceRevenueAmount'+id),
			"varianceExpenseAmount":getFieldValue('varianceExpenseAmount'+id),
			"revisionDeviation":getFieldValue('revisionDeviation'+id),
			"revisionSellDeviation":getFieldValue('revisionSellDeviation'+id),
			"receivableSellDeviation":getFieldValue('receivableSellDeviation'+id),
			"payDeviation":getFieldValue('payDeviation'+id),
			"billToCode":getFieldValue('billToCode'+id),
			"billToName":getFieldValue('billToName'+id),
			"revisionDescription":getFieldValue('revisionDescription'+id),
			"includeLHF":getFieldValue('includeLHF'+id),
			"ignoreForBilling":getFieldValue('ignoreForBilling'+id),
			"description":getFieldValue('description'+id),
			"note":getFieldValue('note'+id),
			"recGl":getFieldValue('recGl'+id),
			"payGl":getFieldValue('payGl'+id)
		});
	}catch(e){}
}

function getAccountLineList(target){
	showOrHide(1);
	var accountLineStatus = document.forms['pricingListForm'].elements['accountLineStatus'].value;
	var fieldName = document.forms['pricingListForm'].elements['fieldName'].value;
	var fieldValue = document.forms['pricingListForm'].elements['fieldValue'].value;
	var accountLineSortValue = document.forms['pricingListForm'].elements['accountLineSortValue'].value;
	var shipNumber = '${serviceOrder.shipNumber}';
	var accountLineOrderByVal = "accountLineNumber";
	var accountLineQuery='';
	if(target!='payNotInvoiced' && target!='recNotInvoiced'){
		accountLineQuery = "from AccountLine where shipNumber ='"+shipNumber+"' and "+fieldName+"='"+target.value.trim()+"'";
	}else if(target=='payNotInvoiced'){
		accountLineQuery = "from AccountLine where shipNumber ='"+shipNumber+"' and (invoiceNumber is null or invoiceNumber='')";
	}else if(target=='recNotInvoiced'){
		accountLineQuery = "from AccountLine where shipNumber ='"+shipNumber+"' and (recInvoiceNumber is null or recInvoiceNumber='')";
	}else{}
	new Ajax.Request('getAccountLineListByFilterAjax.html?ajax=1&decorator=simple&popup=true&accountLineQuery='+accountLineQuery+'&sid=${serviceOrder.id}&accountLineStatus='+accountLineStatus+'&fieldName='+fieldName+'&accountLineOrderByVal='+accountLineOrderByVal+'&accountLineSortValue='+accountLineSortValue,
			{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			response = response.trim();
			var accLineDetailsDiv = document.getElementById("accountLineRows");
			accLineDetailsDiv.innerHTML = response;
			if(target!='payNotInvoiced' && target!='recNotInvoiced'){
				var x=document.getElementById("fieldValue")
				for(var a = 0; a < x.length; a++){
					if(fieldValue == document.forms['pricingListForm'].elements['fieldValue'].options[a].value){
						document.forms['pricingListForm'].elements['fieldValue'].options[a].selected="true";
					}
				}
			}
			jQuery.noConflict();
			(function ($) {
				$(".tablesorter")
				.collapsible("td.collapsible", {
					collapse: true
				})
				.tablesorter({})
				.tablesorterPager({container: $("#pager"), positionFixed: false});
				$('#expandList')
			    .unbind('click')
			    .click( function() {
			        $('.collapsed').removeClass('collapsed').addClass('expanded').css("display","table-cell");
			        $("table tr.expand-child td").css("display","table-cell").show();
			    })
			    
			    $('#collapseList')
			    .unbind('click')
			    .click( function() {
			        $('.expanded').removeClass('expanded').addClass('collapsed');
			        $("table tr.expand-child td").css("display","none").hide();
			    })
			})(jQuery);
			showOrHide(0);
		},
		onFailure: function(){ 
		}
	});
}

function getFieldValueList(target){
	//showOrHide(1);
	if(target.value==''){
		findAccountLineActiveList();
	}else if(target.value!='payNotInvoiced' && target.value!='recNotInvoiced'){
	var accountLineStatus = document.forms['pricingListForm'].elements['accountLineStatus'].value;
	var shipNumber = '${serviceOrder.shipNumber}';
	document.forms['pricingListForm'].elements['fieldValue'].value='';
	var accountLineSortValue = document.forms['pricingListForm'].elements['accountLineSortValue'].value;
		new Ajax.Request('getAccountLineFieldValueListAjax.html?ajax=1&decorator=simple&popup=true&fieldName='+target.value+'&shipNumber='+shipNumber+'&accountLineStatus='+accountLineStatus+'&accountLineSortValue='+accountLineSortValue,
				{
			method:'get',
			onSuccess: function(transport){
				var response = transport.responseText || "";
				response = response.trim();
				res = response.split("@");
	            targetElement = document.getElementById('fieldValue');
				targetElement.length = res.length;
					for(i=0;i<res.length;i++){
						if(res[i] == ''){
							document.getElementById('fieldValue').options[i].text = '';
							document.getElementById('fieldValue').options[i].value = '';
						}else{
							document.getElementById('fieldValue').options[i].text = res[i];
							document.getElementById('fieldValue').options[i].value = res[i];
						}
					}
				//showOrHide(0);
			},
			onFailure: function(){ 
			}
		});
	}else{
		getAccountLineList(target.value);
	}
}

function sortAccountLineList(accountLineOrderByVal){
	var accIdList = document.getElementById("accId").value;
	if(accIdList!=''){
		findAllPricingLineId('');
	}
	showOrHide(1);
	var accountLineStatus = document.forms['pricingListForm'].elements['accountLineStatus'].value;
	var accountLineSortValue = document.forms['pricingListForm'].elements['accountLineSortValue'].value;
	var shipNumber = '${serviceOrder.shipNumber}';
	var accountLineQuery = "from AccountLine where shipNumber ='"+shipNumber+"' ";
	if(accountLineSortValue=='asc'){
		accountLineSortValue = 'desc';
		var accountLineOrderByVal = accountLineOrderByVal +" "+ accountLineSortValue ;
	}else{
		accountLineSortValue = 'asc';
		var accountLineOrderByVal = accountLineOrderByVal +" "+ accountLineSortValue ;
	}
	new Ajax.Request('getAccountLineListByFilterAjax.html?ajax=1&decorator=simple&popup=true&accountLineQuery='+accountLineQuery+'&sid=${serviceOrder.id}&accountLineStatus='+accountLineStatus+'&accountLineOrderByVal='+accountLineOrderByVal+'&accountLineSortValue='+accountLineSortValue,
			{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			response = response.trim();
			var accLineDetailsDiv = document.getElementById("accountLineRows");
			accLineDetailsDiv.innerHTML = response;
			jQuery.noConflict();
			(function ($) {
				$(".tablesorter")
					.collapsible("td.collapsible", {
						collapse: true
					})
				.tablesorter({})
				.tablesorterPager({container: $("#pager"), positionFixed: false});
				$('#expandList')
			    .unbind('click')
			    .click( function() {
			        $('.collapsed').removeClass('collapsed').addClass('expanded').css("display","table-cell");
			        $("table tr.expand-child td").css("display","table-cell").show();
			    })
			    
			    $('#collapseList')
			    .unbind('click')
			    .click( function() {
			        $('.expanded').removeClass('expanded').addClass('collapsed');
			        $("table tr.expand-child td").css("display","none").hide();
			    })
			})(jQuery);
			showOrHide(0);
		},
		onFailure: function(){ 
		}
	});
}

function showOrHide(value) {
	if (value==0) {
		if (document.layers)
			document.layers["overlay"].visibility='hide';
		else
			document.getElementById("overlay").style.visibility='hidden';
	}else if (value==1) {
		if (document.layers)
			document.layers["overlay"].visibility='show';
		else
			document.getElementById("overlay").style.visibility='visible';
	}
}

function saveForm(str){
	showOrHide(1);
	document.getElementById("pricingBtnId").style.display="none";
	document.getElementById("submitBtn").style.display="block";

	<c:if test="${not empty serviceOrder.id}">
	var obj = document.getElementById('accInnerTable');
	if(obj != null && obj != undefined){
		try{

			document.getElementById('pricingFooterValue').value = pricingFooterValue;
			alert("D"+document.getElementById('pricingFooterValue').value)
		}catch(e){

		}
	}
	</c:if> 	  	          
	var id = document.getElementById('submitButtonIdOFFMOVE');
	if(id == null || id == undefined){
		document.getElementById('submitButtonId').click();
	}else{
		document.getElementById('submitButtonIdOFFMOVE').click();
	}
}

function disableRevenue(type){
	if(type==''){
		var aidListIds=document.getElementById("accId").value;
		if(aidListIds!=''){
		var accArr=aidListIds.split(",");
		for(var g=0;g<accArr.length;g++){
			var rev1=document.forms['pricingListForm'].elements['category'+accArr[g]].value;
			if(rev1 == "Internal Cost"){
				document.forms['pricingListForm'].elements['estimateRevenueAmount'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['estimateRevenueAmount'+accArr[g]].value=0;
				document.forms['pricingListForm'].elements['estimateSellRate'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['estimateSellRate'+accArr[g]].value=0;
				document.forms['pricingListForm'].elements['estimatePassPercentage'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['estimatePassPercentage'+accArr[g]].value=0;

				<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				document.forms['pricingListForm'].elements['revisionRevenueAmount'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['revisionRevenueAmount'+accArr[g]].value=0;
				document.forms['pricingListForm'].elements['revisionSellRate'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['revisionSellRate'+accArr[g]].value=0;
				document.forms['pricingListForm'].elements['revisionPassPercentage'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['revisionPassPercentage'+accArr[g]].value=0;
				calculateRevisionBlock(accArr[g],'','','','');
				</c:if>
				<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].value=0;
				document.forms['pricingListForm'].elements['recRate'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['recRate'+accArr[g]].value=0;
				document.forms['pricingListForm'].elements['recQuantity'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['recQuantity'+accArr[g]].value=0;
				<c:if test="${multiCurrency=='Y'}"> 
				document.forms['pricingListForm'].elements['recCurrencyRate'+accArr[g]].readOnly=true;
				document.forms['pricingListForm'].elements['recCurrencyRate'+accArr[g]].value=0;
				</c:if> 
				document.forms['pricingListForm'].elements['payingStatus'+accArr[g]].value='I';
				calculateActualRevenue(accArr[g],'','BAS','','','');
				</c:if>
			}else{
				document.forms['pricingListForm'].elements['estimateRevenueAmount'+accArr[g]].readOnly=false;
				document.forms['pricingListForm'].elements['estimateSellRate'+accArr[g]].readOnly=false;
				document.forms['pricingListForm'].elements['estimatePassPercentage'+accArr[g]].readOnly=false;

				<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				document.forms['pricingListForm'].elements['revisionRevenueAmount'+accArr[g]].readOnly=false;
				document.forms['pricingListForm'].elements['revisionSellRate'+accArr[g]].readOnly=false;
				document.forms['pricingListForm'].elements['revisionPassPercentage'+accArr[g]].readOnly=false;
				</c:if>
				<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].readOnly=false;
				document.forms['pricingListForm'].elements['recRate'+accArr[g]].readOnly=false;
				document.forms['pricingListForm'].elements['recQuantity'+accArr[g]].readOnly=false;
				<c:if test="${multiCurrency=='Y'}"> 
				document.forms['pricingListForm'].elements['recCurrencyRate'+accArr[g]].readOnly=false;
				</c:if> 
				</c:if>
			}
		}
		}
		//totalOfestimate();
	}else{
		var rev1=document.forms['pricingListForm'].elements['category'+type].value;
		if(rev1 == "Internal Cost"){
			document.forms['pricingListForm'].elements['estimateRevenueAmount'+type].readOnly=true;
			document.forms['pricingListForm'].elements['estimateRevenueAmount'+type].value=0;
			document.forms['pricingListForm'].elements['estimateSellRate'+type].readOnly=true;
			document.forms['pricingListForm'].elements['estimateSellRate'+type].value=0;
			document.forms['pricingListForm'].elements['estimatePassPercentage'+type].readOnly=true;
			document.forms['pricingListForm'].elements['estimatePassPercentage'+type].value=0;

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionRevenueAmount'+type].readOnly=true;
			document.forms['pricingListForm'].elements['revisionRevenueAmount'+type].value=0;
			document.forms['pricingListForm'].elements['revisionSellRate'+type].readOnly=true;
			document.forms['pricingListForm'].elements['revisionSellRate'+type].value=0;
			document.forms['pricingListForm'].elements['revisionPassPercentage'+type].readOnly=true;
			document.forms['pricingListForm'].elements['revisionPassPercentage'+type].value=0;
			calculateRevisionBlock(type,'','','','');
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['actualRevenue'+type].readOnly=true;
			document.forms['pricingListForm'].elements['actualRevenue'+type].value=0;
			document.forms['pricingListForm'].elements['recRate'+type].readOnly=true;
			document.forms['pricingListForm'].elements['recRate'+type].value=0;
			document.forms['pricingListForm'].elements['recQuantity'+type].readOnly=true;
			document.forms['pricingListForm'].elements['recQuantity'+type].value=0;
			<c:if test="${multiCurrency=='Y'}"> 
			document.forms['pricingListForm'].elements['recCurrencyRate'+type].readOnly=true;
			document.forms['pricingListForm'].elements['recCurrencyRate'+type].value=0;
			</c:if> 
			document.forms['pricingListForm'].elements['payingStatus'+type].value='I';
			calculateActualRevenue(type,'','BAS','','','');
			</c:if>
		}else{
			document.forms['pricingListForm'].elements['estimateRevenueAmount'+type].readOnly=false;
			document.forms['pricingListForm'].elements['estimateSellRate'+type].readOnly=false;
			document.forms['pricingListForm'].elements['estimatePassPercentage'+type].readOnly=false;

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionRevenueAmount'+type].readOnly=false;
			document.forms['pricingListForm'].elements['revisionSellRate'+type].readOnly=false;
			document.forms['pricingListForm'].elements['revisionPassPercentage'+type].readOnly=false;
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['actualRevenue'+type].readOnly=false;
			document.forms['pricingListForm'].elements['recRate'+type].readOnly=false;
			document.forms['pricingListForm'].elements['recQuantity'+type].readOnly=false;
			<c:if test="${multiCurrency=='Y'}"> 
			document.forms['pricingListForm'].elements['recCurrencyRate'+type].readOnly=false;
			</c:if> 
			</c:if>

		}
		//totalOfestimate();
	}
}
function saveForm(str){
	showOrHide(1);
	document.getElementById("pricingBtnId").style.display="none";
	document.getElementById("submitBtn").style.display="block";

	<c:if test="${not empty serviceOrder.id}">
	var obj = document.getElementById('accInnerTable');
	if(obj != null && obj != undefined){
		try{

			document.getElementById('pricingFooterValue').value = pricingFooterValue;
			alert("D"+document.getElementById('pricingFooterValue').value)
		}catch(e){

		}
	}
	</c:if> 	  	          
	var id = document.getElementById('submitButtonIdOFFMOVE');
	if(id == null || id == undefined){
		document.getElementById('submitButtonId').click();
	}else{
		document.getElementById('submitButtonIdOFFMOVE').click();
	}
}
function ContainerAutoSave(clickType){
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	var check=false;
	if('${serviceOrder.status}'=='CNCL'){
		var check = true;
	}else{
		var check =saveValidationPricing('autoTab');
	}
	if(check){ 	 
		progressBarAutoSave('1');
		var elementsLen=document.forms['pricingListForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++) {

			document.forms['pricingListForm'].elements[i].disabled=false;
		}
		var id1 = '${serviceOrder.id}';
		var jobNumber = '${serviceOrder.shipNumber}';
		var idc = '${customerFile.id}';
		if (document.forms['pricingListForm'].elements['formStatus'].value == '1'){
			if ('${autoSavePrompt}' == 'No'){
				var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
					noSaveAction = 'customerServiceOrders.html?id='+idc;
				}
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.accounting'){
					noSaveAction = 'accountLineList.html?sid='+id1;
				}
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.containers'){
					<c:if test="${forwardingTabVal!='Y'}">
					noSaveAction = 'containers.html?id='+id1;
					</c:if>
					<c:if test="${forwardingTabVal=='Y'}">
					noSaveAction = 'containersAjaxList.html?id='+id1;
					</c:if>
				}
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.billing'){
					noSaveAction = 'editBilling.html?id='+id1;
				}
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.domestic'){
					noSaveAction = 'editMiscellaneous.html?id='+id1;
				}
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.status'){
					var rel='${serviceOrder.job}';
					if(rel=='RLO')
					{ 
						noSaveAction = 'editDspDetails.html?id='+id1; 
						relo="yes";
					}else{
						noSaveAction =  'editTrackingStatus.html?id='+id1;
					} 
				}
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.ticket'){
					noSaveAction = 'customerWorkTickets.html?id='+id1;
				}
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.claims'){
					noSaveAction = 'claims.html?id='+id1;
				}
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
					var cidVal='${customerFile.id}';
					noSaveAction = 'editCustomerFile.html?id='+cidVal;
				}
				if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
					noSaveAction = 'soAdditionalDateDetails.html?sid='+id1;
				}
				processAutoSave(document.forms['pricingListForm'], 'saveServiceOrder!saveOnTabChange.html', noSaveAction);
			}
			else{
				if(!(clickType == 'save')){
					var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='serviceOrderDetail.heading'/>");
					if(agree){
						document.forms['pricingListForm'].action = 'saveServiceOrder!saveOnTabChange.html';
						document.forms['pricingListForm'].submit();
					}else{
						if(id1 != ''){
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
								location.href = 'customerServiceOrders.html?id='+idc;
							}
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.accounting'){
								location.href = 'accountLineList.html?sid='+id1;
							}
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.containers'){
								<c:if test="${forwardingTabVal!='Y'}">
								location.href = 'containers.html?id='+id1;
								</c:if>
								<c:if test="${forwardingTabVal=='Y'}">
								location.href = 'containersAjaxList.html?id='+id1;
								</c:if>
							}
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.billing'){
								location.href = 'editBilling.html?id='+id1;
							}
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.domestic'){
								location.href = 'editMiscellaneous.html?id='+id1;
							}
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.status'){

								var rel='${serviceOrder.job}';
								if(rel=='RLO')
								{ 
									location.href = 'editDspDetails.html?id='+id1; 
									relo="yes";
								}else{
									location.href =  'editTrackingStatus.html?id='+id1;
								} 
							}
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.ticket'){
								location.href = 'customerWorkTickets.html?id='+id1;
							}
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.claims'){
								location.href = 'claims.html?id='+id1;
							}
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
								var cidVal='${customerFile.id}';
								location.href = 'editCustomerFile.html?id='+cidVal;
							}
							if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
								location.href = 'soAdditionalDateDetails.html?sid='+id1;
							}
						} }
				}else{
					if(id1 != ''){
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
							location.href = 'customerServiceOrders.html?id='+idc;
						}
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.accounting'){
							location.href = 'accountLineList.html?sid='+id1;
						}
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.containers'){
							<c:if test="${forwardingTabVal!='Y'}">
							location.href = 'containers.html?id='+id1;
							</c:if>
							<c:if test="${forwardingTabVal=='Y'}">
							location.href = 'containersAjaxList.html?id='+id1;
							</c:if>
						}
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.billing'){
							location.href = 'editBilling.html?id='+id1;
						}
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.domestic'){
							location.href = 'editMiscellaneous.html?id='+id1;
						}
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.status'){
							var rel='${serviceOrder.job}';
							if(rel=='RLO')
							{ 
								location.href = 'editDspDetails.html?id='+id1; 
								relo="yes";
							}else{
								location.href =  'editTrackingStatus.html?id='+id1;
							}                 
						}
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.ticket'){
							location.href = 'customerWorkTickets.html?id='+id1;
						}
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.claims'){
							location.href = 'claims.html?id='+id1;
						}
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
							var cidVal='${customerFile.id}';
							location.href = 'editCustomerFile.html?id='+cidVal;
						}
						if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
							location.href = 'soAdditionalDateDetails.html?sid='+id1;
						}
					} }  } }
		else{	  
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'customerServiceOrders.html?id='+idc;
			}
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				location.href = 'accountLineList.html?sid='+id1;
			}
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.containers'){
				<c:if test="${forwardingTabVal!='Y'}">
				location.href = 'containers.html?id='+id1;
				</c:if>
				<c:if test="${forwardingTabVal=='Y'}">
				location.href = 'containersAjaxList.html?id='+id1;
				</c:if>
			}
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.billing'){
				location.href = 'editBilling.html?id='+id1;
			}
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.domestic'){
				location.href = 'editMiscellaneous.html?id='+id1;
			}
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.status'){               
				var rel='${serviceOrder.job}';
				if(rel=='RLO')
				{ 
					location.href = 'editDspDetails.html?id='+id1; 
					relo="yes";
				}else{
					location.href =  'editTrackingStatus.html?id='+id1;
				} 
			}
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.ticket'){
				location.href = 'customerWorkTickets.html?id='+id1;
			}
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.claims'){
				location.href = 'claims.html?id='+id1;
			}
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
				location.href = 'editCustomerFile.html?id='+cidVal;
			}
			if(document.forms['pricingListForm'].elements['gotoPageString'].value == 'gototab.criticaldate'){
				location.href = 'soAdditionalDateDetails.html?sid='+id1;
			}
		} } 
}

function calculateActualRevenue(aid,targetElement,type,field1,field2,field3) {
	if(field1!='' && field2!='' & field3!=''){
		rateOnActualizationDate(field1,field2,field3);
	}	   
	var chargeCode=document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	var vendorCode=document.forms['pricingListForm'].elements['vendorCode'+aid].value; 
	var recGl=document.forms['pricingListForm'].elements['recGl'+aid].value;
	var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value;   
	var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
	var acref="${accountInterface}";
	acref=acref.trim();
	if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("Vendor Code Missing. Please select");
		document.forms['pricingListForm'].elements[targetElement.name].value='';
	}else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
		alert("Charge code needs to be selected");
		document.forms['pricingListForm'].elements[targetElement.name].value='';
		if((targetElement.name.indexOf("recQuantity")>-1) ||  (targetElement.name.indexOf("recRate")>-1)){
			document.forms['pricingListForm'].elements['recQuantity'+aid].value='0.00';
			document.forms['pricingListForm'].elements['recRate'+aid].value='0.00';
			document.forms['pricingListForm'].elements['actualRevenue'+aid].value='0.00';
		}
	}else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("Pay GL missing. Please select another charge code.");
		document.forms['pricingListForm'].elements[targetElement.name].value='';
	}else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC'){
		alert("Receivable GL missing. Please select another charge code.");
		document.forms['pricingListForm'].elements[targetElement.name].value='';
	}else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
		document.forms['pricingListForm'].elements[targetElement.name].value='';
	}else{		   

		var basis=document.forms['pricingListForm'].elements['basis'+aid].value;
		var baseRateVal=1;
		var roundVal=0.00;
		var recQuantity=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
		var recRate=0.00;
		recRate=document.forms['pricingListForm'].elements['recRate'+aid].value;
		if(recRate=='') {
			recRate = document.forms['pricingListForm'].elements['recRate'+aid].value=0.00;
		}
		var actualRevenue=document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
		var actualExpense=document.forms['pricingListForm'].elements['actualExpense'+aid].value;
		var actualDiscount=0.00;
		try{
			actualDiscount=document.forms['pricingListForm'].elements['actualDiscount'+aid].value;
		}catch(e){}
		if(recQuantity=='' ||recQuantity=='0' ||recQuantity=='0.0' ||recQuantity=='0.00') {
			recQuantity = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
		} 
		if( basis=="cwt" || basis=="%age"){
			baseRateVal=100;
		}else if(basis=="per 1000"){
			baseRateVal=1000;
		} else {
			baseRateVal=1;  	
		}
		actualRevenue=(recQuantity*recRate)/baseRateVal;
		if(actualDiscount!=0.00){
			actualRevenue=(actualRevenue*actualDiscount)/100;
		}
		document.forms['pricingListForm'].elements['actualRevenue'+aid].value=actualRevenue;
		<c:if test="${contractType}">
		var contractCurrency=document.forms['pricingListForm'].elements['contractCurrency'+aid].value;		
		var contractExchangeRate=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value;
		var contractRate=document.forms['pricingListForm'].elements['contractRate'+aid].value;
		var contractRateAmmount=document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value;
		var contractValueDate=document.forms['pricingListForm'].elements['contractValueDate'+aid].value;
		if(contractCurrency!=''){
			var ss=findExchangeRateGlobal(contractCurrency);
			contractExchangeRate=parseFloat(ss);
			document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value=contractExchangeRate;
			document.forms['pricingListForm'].elements['contractValueDate'+aid].value=currentDateGlobal();
			contractRate=recRate*contractExchangeRate;
			document.forms['pricingListForm'].elements['contractRate'+aid].value=contractRate;
			contractRateAmmount=(recQuantity*contractRate)/baseRateVal;
			if(actualDiscount!=0.00){
				contractRateAmmount=(contractRateAmmount*actualDiscount)/100;
			}
			document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value=contractRateAmmount;	 		
		}
		</c:if>
		<c:if test="${multiCurrency=='Y'}">
		var recRateCurrency=document.forms['pricingListForm'].elements['recRateCurrency'+aid].value;		
		var recRateExchange=document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
		var recCurrencyRate=document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value;
		var actualRevenueForeign=document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value;
		var racValueDate=document.forms['pricingListForm'].elements['racValueDate'+aid].value;
		if(recRateCurrency!=''){
			var ss=findExchangeRateGlobal(recRateCurrency);
			recRateExchange=parseFloat(ss);
			document.forms['pricingListForm'].elements['recRateExchange'+aid].value=recRateExchange;
			document.forms['pricingListForm'].elements['racValueDate'+aid].value=currentDateGlobal();
			recCurrencyRate=recRate*recRateExchange;
			document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value=recCurrencyRate;
			actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
			if(actualDiscount!=0.00){
				actualRevenueForeign=(actualRevenueForeign*actualDiscount)/100;
			}
			document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value=actualRevenueForeign;	 		
		}
		</c:if>
		<c:if test="${contractType}">
		var payableContractCurrency=document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value;		
		var payableContractExchangeRate=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value;
		var payableContractRateAmmount=document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value;
		var payableContractValueDate=document.forms['pricingListForm'].elements['payableContractValueDate'+aid].value;
		if(payableContractCurrency!=''){
			var ss=findExchangeRateGlobal(payableContractCurrency);
			payableContractExchangeRate=parseFloat(ss);
			document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value=payableContractExchangeRate;
			document.forms['pricingListForm'].elements['payableContractValueDate'+aid].value=currentDateGlobal();
			payableContractRateAmmount=actualExpense*payableContractExchangeRate;
			document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value=payableContractRateAmmount;	 		
		}
		</c:if>
		var country=document.forms['pricingListForm'].elements['country'+aid].value;		
		var exchangeRate=document.forms['pricingListForm'].elements['exchangeRate'+aid].value;
		var localAmount=document.forms['pricingListForm'].elements['localAmount'+aid].value;
		var valueDate=document.forms['pricingListForm'].elements['valueDate'+aid].value;
		if(country!=''){
			var ss=findExchangeRateGlobal(country);
			exchangeRate=parseFloat(ss);
			document.forms['pricingListForm'].elements['exchangeRate'+aid].value=exchangeRate;
			document.forms['pricingListForm'].elements['valueDate'+aid].value=currentDateGlobal();
			localAmount=actualExpense*exchangeRate;
			document.forms['pricingListForm'].elements['localAmount'+aid].value=localAmount;	 		
		}
		/* var actualTotalRevenue=0.00;
		var actualTotalExpense=0.00;
		var tempActualTotalExpense=0.00;
		var tempActualTotalRevenue=0.00;
		var actualGrossMargin=0.00;
		var actualGrossMarginPercentage=0.00;
		var aidListIds=document.forms['pricingListForm'].elements['aidListIds'].value;
		var accArr=aidListIds.split(",");
		for(var g=0;g<accArr.length;g++){
			tempActualTotalRevenue="0.00";
			if(document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].value!=''){
				tempActualTotalRevenue=document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].value;
			}			 	
			actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
			tempActualTotalExpense="0.00";
			if(document.forms['pricingListForm'].elements['actualExpense'+accArr[g]].value!=''){
				tempActualTotalExpense=document.forms['pricingListForm'].elements['actualExpense'+accArr[g]].value;
			}
			actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
		}
		actualGrossMargin=actualTotalRevenue-actualTotalExpense;
		try{
			if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
				actualGrossMarginPercentage=0.00;
			}else{
				actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
			}
		}catch(e){
			actualGrossMarginPercentage=0.00;
		}
		<c:if test="${not empty serviceOrder.id}">
		document.forms['pricingListForm'].elements['projectedActualRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*100)/100;
		document.forms['pricingListForm'].elements['projectedActualExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*100)/100;		
		</c:if>
		actualTotalRevenue=0.00;
		actualTotalExpense=0.00;
		tempActualTotalExpense=0.00;
		tempActualTotalRevenue=0.00;
		actualGrossMargin=0.00;
		actualGrossMarginPercentage=0.00;
		var tempRecInvoiceNumber='';
		var tempPayingStatus=''; 		 
		for(var g=0;g<accArr.length;g++){
			tempRecInvoiceNumber=document.forms['pricingListForm'].elements['recInvoiceNumber'+accArr[g]].value; 
			if(tempRecInvoiceNumber!=undefined && tempRecInvoiceNumber!=null && tempRecInvoiceNumber.trim()!=''){
				if(document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].value!=""){
					tempActualTotalRevenue=document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].value;
				}
				actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
			}
			tempPayingStatus=document.forms['pricingListForm'].elements['payingStatus'+accArr[g]].value;
			if(tempPayingStatus!=undefined && tempPayingStatus!=null && tempPayingStatus.trim()!='' &&( tempPayingStatus.trim()=='A' || tempPayingStatus.trim()=='I')){
				if(document.forms['pricingListForm'].elements['actualExpense'+accArr[g]].value!=""){
					tempActualTotalExpense=document.forms['pricingListForm'].elements['actualExpense'+accArr[g]].value;
				}
				actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
			} 	
		}
		actualGrossMargin=actualTotalRevenue-actualTotalExpense;
		try{
			if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
				actualGrossMarginPercentage=0.00;
			}else{
				actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
			}
		}catch(e){
			actualGrossMarginPercentage=0.00;
		}		
		<c:if test="${not empty serviceOrder.id}">
		document.forms['pricingListForm'].elements['actualTotalRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*100)/100;
		document.forms['pricingListForm'].elements['actualTotalExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*100)/100;
		document.forms['pricingListForm'].elements['actualGrossMargin'+${serviceOrder.id}].value=Math.round(actualGrossMargin*100)/100;
		document.forms['pricingListForm'].elements['actualGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(actualGrossMarginPercentage*100)/100;
		</c:if> */
		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
		calculateVatSection('REC',aid,type);
		</c:if>
		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
		calculateVatSection('PAY',aid,type);
		</c:if>
		</c:if>
	}	
}

function totalOfestimate(){
	// Code Gross Section
	try{
		var estimatedTotalRevenue=0.00;
		var estimatedTotalExpense=0.00;
		var tempEstimatedTotalExpense=0.00;
		var tempEstimatedTotalRevenue=0.00;
		var estimatedGrossMargin=0.00;
		var estimatedGrossMarginPercentage=0.00;
		var aidListIds=document.forms['pricingListForm'].elements['aidListIds'].value;
		var accArr=aidListIds.split(",");
		for(var g=0;g<accArr.length;g++){
			if(document.forms['pricingListForm'].elements['estimateRevenueAmount'+accArr[g]].value!=""){
				tempEstimatedTotalRevenue=document.forms['pricingListForm'].elements['estimateRevenueAmount'+accArr[g]].value;
			}
			estimatedTotalRevenue=estimatedTotalRevenue+parseFloat(tempEstimatedTotalRevenue);
			if(document.forms['pricingListForm'].elements['estimateExpense'+accArr[g]].value!=""){
				tempEstimatedTotalExpense=document.forms['pricingListForm'].elements['estimateExpense'+accArr[g]].value;
			}
			estimatedTotalExpense=estimatedTotalExpense+parseFloat(tempEstimatedTotalExpense);
		}
		estimatedGrossMargin=estimatedTotalRevenue-estimatedTotalExpense;
		try{
			if((estimatedTotalRevenue==0)||(estimatedTotalRevenue==0.0)||(estimatedTotalRevenue==0.00)){
				estimatedGrossMarginPercentage=0.00;
			}else{
				estimatedGrossMarginPercentage=(estimatedGrossMargin*100)/estimatedTotalRevenue;
			}
		}catch(e){
			estimatedGrossMarginPercentage=0.00;
		}
		<c:if test="${not empty serviceOrder.id}">
		document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=Math.round(estimatedTotalRevenue*100)/100;
		document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=Math.round(estimatedTotalExpense*100)/100;
		document.forms['pricingListForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=Math.round(estimatedGrossMargin*100)/100;
		document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(estimatedGrossMarginPercentage*100)/100;
		</c:if>	
	}catch(e){}
}

function calculateRevisionBlock(aid,type1,field1,field2,field3) {
	if(field1!='' && field2!='' & field3!=''){
		rateOnActualizationDate(field1,field2,field3);
	}	   
	var basis=document.forms['pricingListForm'].elements['basis'+aid].value;
	var baseRateVal=1;
	var roundVal=0.00;
	var revisionSellQuantity=0.00;
	<c:if test="${contractType}">
	revisionSellQuantity=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
	if(revisionSellQuantity=='' ||revisionSellQuantity=='0' ||revisionSellQuantity=='0.0' ||revisionSellQuantity=='0.00') {
		revisionSellQuantity = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
	}			 
	</c:if>
	var revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
	if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		revisionQuantity = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	} 
	if( basis=="cwt" || basis=="%age"){
		baseRateVal=100;
	}else if(basis=="per 1000"){
		baseRateVal=1000;
	} else {
		baseRateVal=1;  	
	}
	var revisionDiscount=0.00;
	try{
		revisionDiscount=document.forms['pricingListForm'].elements['revisionDiscount'+aid].value;
	}catch(e){}

	//Code For revision Est	 
	var revisionRate=0.00;
	revisionRate=document.forms['pricingListForm'].elements['revisionRate'+aid].value;
	if(revisionRate=='') {
		revisionRate = document.forms['pricingListForm'].elements['revisionRate'+aid].value=0.00;
	}
	revisionBuyFX(aid);
	var revisionExpense=0.00;
	revisionExpense=document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
	if(revisionExpense=='') {
		revisionExpense = document.forms['pricingListForm'].elements['revisionExpense'+aid].value=0.00;
	}
	revisionExpense=(revisionRate*revisionQuantity)/baseRateVal;
	document.forms['pricingListForm'].elements['revisionExpense'+aid].value=revisionExpense;

	//Code For revision Sell	 
	var revisionSellRate=0.00;
	revisionSellRate=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
	if(revisionSellRate=='') {
		revisionSellRate = document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=0.00;
	}
	<c:if test="${multiCurrency=='Y'}">
	revisionSellFX(aid);
	</c:if>
	var revisionRevenueAmount=0.00;
	revisionRevenueAmount=document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;
	if(revisionRevenueAmount=='') {
		revisionRevenueAmount = document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=0.00;
	}
	<c:if test="${contractType}">
	revisionRevenueAmount=(revisionSellRate*revisionSellQuantity)/baseRateVal;
	</c:if>
	<c:if test="${!contractType}">
	revisionRevenueAmount=(revisionSellRate*revisionQuantity)/baseRateVal;
	</c:if>

	if(revisionDiscount!=0.00){
		revisionRevenueAmount=(revisionRevenueAmount*revisionDiscount)/100;
	}
	document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=revisionRevenueAmount;	
	//Code For Revision Pass Percentage   
	calculateRevisionPassPercentage('revisionRevenueAmount'+aid,'revisionExpense'+aid,'revisionPassPercentage'+aid);
	// Code Gross Section
	/* var revisedTotalRevenue=0.00;
	var revisedTotalExpense=0.00;
	var tempRevisedTotalExpense=0.00;
	var tempRevisedTotalRevenue=0.00;
	var revisedGrossMargin=0.00;
	var revisedGrossMarginPercentage=0.00;
	var aidListIds=document.forms['pricingListForm'].elements['aidListIds'].value;
	var accArr=aidListIds.split(",");
	for(var g=0;g<accArr.length;g++){
		if(document.forms['pricingListForm'].elements['revisionRevenueAmount'+accArr[g]].value!=""){
			tempRevisedTotalRevenue=document.forms['pricingListForm'].elements['revisionRevenueAmount'+accArr[g]].value;
		}
		revisedTotalRevenue=revisedTotalRevenue+parseFloat(tempRevisedTotalRevenue);
		if(document.forms['pricingListForm'].elements['revisionExpense'+accArr[g]].value!=""){
			tempRevisedTotalExpense=document.forms['pricingListForm'].elements['revisionExpense'+accArr[g]].value;
		}
		revisedTotalExpense=revisedTotalExpense+parseFloat(tempRevisedTotalExpense);
	}
	revisedGrossMargin=revisedTotalRevenue-revisedTotalExpense;
	try{
		if((revisedTotalRevenue==0)||(revisedTotalRevenue==0.0)||(revisedTotalRevenue==0.00)){
			revisedGrossMarginPercentage=0.00;
		}else{
			revisedGrossMarginPercentage=(revisedGrossMargin*100)/revisedTotalRevenue;
		}
	}catch(e){
		revisedGrossMarginPercentage=0.00;
	}
	<c:if test="${not empty serviceOrder.id}">
	document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value=Math.round(revisedTotalRevenue*100)/100;
	document.forms['pricingListForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value=Math.round(revisedTotalExpense*100)/100;
	document.forms['pricingListForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value=Math.round(revisedGrossMargin*100)/100;
	document.forms['pricingListForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(revisedGrossMarginPercentage*100)/100;
	</c:if> */	 
	<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	calculateVatSection('REV',aid,type1); 
	</c:if>
	</c:if>   
}

function findExchangeRateGlobal(currency){
	var rec='1';
	<c:forEach var="entry" items="${currencyExchangeRate}">
	if('${entry.key}'==currency.trim()){
		rec='${entry.value}';
	}
	</c:forEach>
	return rec;
}

function rateOnActualizationDate(field1,field2,field3){
	<c:if test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
	<c:if test="${contractType}">	
	var currency=""; 
	var currentDate=currentDateRateOnActualization();
	try{
		currency=document.forms['pricingListForm'].elements[field1.trim().split('~')[0]].value;
		document.forms['pricingListForm'].elements[field2.trim().split('~')[0]].value=currentDate;
		document.forms['pricingListForm'].elements[field3.trim().split('~')[0]].value=findExchangeRateRateOnActualization(currency);
	}catch(e){}
	try{
		currency=document.forms['pricingListForm'].elements[field1.trim().split('~')[1]].value;
		document.forms['pricingListForm'].elements[field2.trim().split('~')[1]].value=currentDate;
		document.forms['pricingListForm'].elements[field3.trim().split('~')[1]].value=findExchangeRateRateOnActualization(currency);
	}catch(e){}		
	</c:if>
	</c:if>
}

function findExchangeRateRateOnActualization(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}

function currentDateRateOnActualization(){
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	var daym=mydate.getDate()
	if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4); 
	return datam;
}

function checkChargeCodeMandatory(sid,aid,chargeCode){
	var ChargesMandatory='${checkContractChargesMandatory}';	
	var contractType = '${contractType}';
	if((ChargesMandatory=='1' || contractType=='true') && (chargeCode==undefined || chargeCode==null || chargeCode=='' || chargeCode.trim()=='')){
		alert('Charge code needs to be selected');
	}else{
		window.open('editAccountLine.html?sid='+sid+'&id='+aid+'',"width=250,height=70,scrollbars=1");
	}
}

function selectPricingMarketPlace(category,id){
	try{
		var  hidMarketPlace='hidMarketPlace'+id;
		var  hidMarketPlace11='hidMarketPlace11'+id;
		var val = document.getElementById(category).value;
		var pricingFlag='${pricePointFlag}';
		var e2 = document.getElementById(hidMarketPlace);
		var e3 = document.getElementById(hidMarketPlace11);
		if( pricingFlag=='true' && (val=='Origin' || val=='Destin')){
			e2.style.display='inline-block'; 
			e3.style.display='none';
		}else{
			e2.style.display='none';
			e3.style.display='none';
		}
	}catch(e){}
	disableRevenue(id);
}

function chk(chargeCode,category,aid){
	accountLineIdScript=aid;
	var val = document.forms['pricingListForm'].elements[category].value;
	var quotationContract = '${billing.contract}'; 
	var accountCompanyDivision = document.forms['pricingListForm'].elements['companyDivision'+aid].value;
	var originCountry='${serviceOrder.originCountryCode}';
	var destinationCountry='${serviceOrder.destinationCountryCode}';
	var mode;		
	if('${serviceOrder.mode}'!=undefined){
		mode='${serviceOrder.mode}'; }else{mode="";}

	if(document.forms['pricingListForm'].elements['recGl'+aid] == undefined && document.forms['pricingListForm'].elements['payGl'+aid] == undefined){
		recGlVal = 'secondDescription';
		payGlVal = 'thirdDescription'; 
	}else{
		recGlVal = 'recGl'+aid;
		payGlVal = 'payGl'+aid; 
	} 
	var estimateContractCurrency='tenthDescription';
	var estimatePayableContractCurrency='fourthDescription';
	<c:if test="${contractType}">  
	estimateContractCurrency='contractCurrencyNew'+aid;
	estimatePayableContractCurrency='payableContractCurrencyNew'+aid;
	</c:if>
	if(val=='Internal Cost'){
		quotationContract="Internal Cost Management";
		javascript:openWindow('internalCostChargess.html?accountCompanyDivision='+accountCompanyDivision+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode);
		document.forms['pricingListForm'].elements[chargeCode].select();
	}
	else {
		if(quotationContract=='' || quotationContract==' '){
			alert("There is no pricing contract in billing: Please select.");
		}
		else{
			javascript:openWindow('chargess.html?contract='+quotationContract+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode+'&categoryType='+val);
		}
	}
	document.forms['pricingListForm'].elements[chargeCode].focus();
}  	

function winOpenForActgCode(vendorCode,vendorName,category,aid){
	accountLineIdScript=aid;
	var sid='${serviceOrder.id}';
	var val = document.forms['pricingListForm'].elements[category].value;
	var finalVal = "OK";
	var indexCheck = val.indexOf('Origin'); 
	openWindow('findVendorCode.html?sid='+sid+'&defaultListFlag=show&partnerType=PA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+vendorName+'&fld_code='+vendorCode+'&fld_seventhDescription=seventhDescription',screen.width-30,500);
	document.forms['pricingListForm'].elements[vendorCode].select(); 
}

function onlyNumberAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39); 
}

function checkAccountLineNumber(temp) { 
	var accountLineNumber=document.forms['pricingListForm'].elements[temp].value; 
	var accountLineNumber1=document.forms['pricingListForm'].elements[temp].value;
	accountLineNumber=accountLineNumber.trim();
	accountLineNumber1=accountLineNumber1.trim();
	if(accountLineNumber!=""){
		if(accountLineNumber>999)  {
			alert("Manually you can not Enter Line # greater than 999");
			document.forms['pricingListForm'].elements[temp].value=0;
		}
		else if(accountLineNumber1.length<2)  { 
			document.forms['pricingListForm'].elements[temp].value='00'+document.forms['pricingListForm'].elements[temp].value;
		}
		else if(accountLineNumber1.length==2)  { 
			var aa = document.forms['pricingListForm'].elements[temp].value;
			document.forms['pricingListForm'].elements[temp].value='0'+aa;
		}} } 

function findPartnerDetailsPriceing(nameId,CodeId,DivId,partnerTypeWhereClause,id,evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;  
	if(keyCode!=9){
		var nameValue=document.getElementById(nameId).value;
		if(nameValue.length<=1){
			document.getElementById(CodeId).value="";	
		}
		//document.getElementById(CodeId).value="";
		if(nameValue.length>=3){
			$.get("partnerDetailsPricingAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", 
					{partnerNameAutoCopmlete: nameValue,partnerNameId:nameId,paertnerCodeId:CodeId,autocompleteDivId:DivId,autocompleteCondition:partnerTypeWhereClause,pricingAccountLineId:id},
					function(data){
						document.getElementById(DivId).style.display = "block";
						$("#"+DivId).html(data);
						//var partnerDetails= document.getElementById('partnerDetailsAutoCompleteListSize').value;
						//if(partnerDetails=='true'){
						//	document.getElementById(nameId).value="";
						//	document.getElementById(CodeId).value="";
						//}
					});	
		}else{
			document.getElementById(DivId).style.display = "none";	
		}
	}else{
		document.getElementById(DivId).style.display = "none";
	}
}

function changeBasis(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,targetElement){  
	var quantityValue = document.forms['pricingListForm'].elements[quantity].value;
	var basisValue = document.forms['pricingListForm'].elements[basis].value; 
	var oldBasis="old"+basis;
	var oldQuantity="old"+quantity;
	var oldQuantityValue = document.forms['pricingListForm'].elements[oldQuantity].value;
	var oldBasisValue = document.forms['pricingListForm'].elements[oldBasis].value;
	var actualRevenue=document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
	
	if(('${serviceOrder.mode}'=="AIR")|| ('${serviceOrder.mode}'=="Air")){
		if(basisValue=='cwt'){
			document.forms['pricingListForm'].elements[quantity].value='${miscellaneous.estimateGrossWeight}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value='${miscellaneous.estimateGrossWeight}';
			</c:if>
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value='${miscellaneous.actualGrossWeight}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value='${miscellaneous.actualGrossWeight}';
			</c:if>		  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value='${miscellaneous.actualGrossWeight}';
			}
			</c:if>
		}else if(basisValue=='kg'){
			document.forms['pricingListForm'].elements[quantity].value='${miscellaneous.estimateGrossWeightKilo}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value='${miscellaneous.estimateGrossWeightKilo}';
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value='${miscellaneous.actualGrossWeightKilo}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value='${miscellaneous.actualGrossWeightKilo}';
			</c:if>		  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value='${miscellaneous.actualGrossWeightKilo}';
			}
			</c:if>
		}else if(basisValue=='cft'){
			document.forms['pricingListForm'].elements[quantity].value='${miscellaneous.estimateCubicFeet}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value='${miscellaneous.estimateCubicFeet}';
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value='${miscellaneous.actualCubicFeet}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value='${miscellaneous.actualCubicFeet}';
			</c:if>		  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value='${miscellaneous.actualCubicFeet}';
			}
			</c:if>
		}else if(basisValue=='cbm'){
			document.forms['pricingListForm'].elements[quantity].value='${miscellaneous.estimateCubicMtr}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value='${miscellaneous.estimateCubicMtr}';
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value='${miscellaneous.actualCubicMtr}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value='${miscellaneous.actualCubicMtr}';
			</c:if>		  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value='${miscellaneous.actualCubicMtr}';
			}
			</c:if>
		}else if(basisValue=='each'){
			document.forms['pricingListForm'].elements[quantity].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
			</c:if>		  		  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			}
			</c:if>
		}else if(basisValue=='value'){
			document.forms['pricingListForm'].elements[quantity].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
			</c:if>		  	  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			}
			</c:if>
		}else if(basisValue=='flat'){
			document.forms['pricingListForm'].elements[quantity].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
			</c:if>		  		  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			}
			</c:if>
		}
	}else{
		if(basisValue=='cwt'){
			document.forms['pricingListForm'].elements[quantity].value='${miscellaneous.estimatedNetWeight}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value='${miscellaneous.estimatedNetWeight}';
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value='${miscellaneous.actualNetWeight}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value='${miscellaneous.actualNetWeight}';
			</c:if>		  		  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value='${miscellaneous.actualNetWeight}';
			}
			</c:if>
		}else if(basisValue=='kg'){
			document.forms['pricingListForm'].elements[quantity].value='${miscellaneous.estimatedNetWeightKilo}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value='${miscellaneous.estimatedNetWeightKilo}';
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value='${miscellaneous.actualNetWeightKilo}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value='${miscellaneous.actualNetWeightKilo}';
			</c:if>		  		  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value='${miscellaneous.actualNetWeightKilo}';
			}
			</c:if>
		}else if(basisValue=='cft'){
			document.forms['pricingListForm'].elements[quantity].value='${miscellaneous.netEstimateCubicFeet}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value='${miscellaneous.netEstimateCubicFeet}';
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value='${miscellaneous.netActualCubicFeet}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value='${miscellaneous.netActualCubicFeet}';
			</c:if>		  	  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value='${miscellaneous.netActualCubicFeet}';
			}
			</c:if>
		}else if(basisValue=='cbm'){
			document.forms['pricingListForm'].elements[quantity].value='${miscellaneous.netEstimateCubicMtr}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value='${miscellaneous.netEstimateCubicMtr}';
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value='${miscellaneous.netActualCubicMtr}';
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value='${miscellaneous.netActualCubicMtr}';
			</c:if>		  	  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value='${miscellaneous.netActualCubicMtr}';
			}
			</c:if>
		}else if(basisValue=='each'){
			document.forms['pricingListForm'].elements[quantity].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
			</c:if>		  	  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			}
			</c:if>
		}else if(basisValue=='value'){
			document.forms['pricingListForm'].elements[quantity].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
			</c:if>		  		  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			}
			</c:if>
		}else if(basisValue=='flat'){
			document.forms['pricingListForm'].elements[quantity].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
			</c:if>

			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
			<c:if test="${contractType}">
			document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
			</c:if>		  	  
			</c:if>
			<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			if(actualRevenue=='' ||actualRevenue=='0' ||actualRevenue=='0.0' ||actualRevenue=='0.00') {
				document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			}
			</c:if>
		} }
	calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid);
	calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,'','','');
	<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	calculateRevisionBlock(aid,'BAS','','','');
	</c:if>
	<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	//calculateActualRevenue(aid,targetElement,'BAS','','','');
	</c:if>
}
var http88 = getHTTPObject88(); 
var httpTemplate = getHTTPObject88();
var http5513 = getHTTPObject88();
var httpCMMAgentInvoiceCheck =	getHTTPObject77();
var httpCheckZeroforInvoice = getHTTPObject77();
function getHTTPObject77() {
	var xmlhttp;
	if(window.XMLHttpRequest)  {
		xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		if (!xmlhttp) {
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
	}
	return xmlhttp;
}
function getHTTPObject88() {
	var xmlhttp;
	if(window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)  {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		if (!xmlhttp)   {
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		}  }
	return xmlhttp;
}

function findRevisedEstimateQuantitys(aid){
	var charge = document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	var rev = document.forms['pricingListForm'].elements['category'+aid].value;
	var estimateExpense = document.forms['pricingListForm'].elements['estimateExpense'+aid].value;
	var basis = document.forms['pricingListForm'].elements['basis'+aid].value;
	var estimateQuantity = document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
	var estimateRate = document.forms['pricingListForm'].elements['estimateRate'+aid].value;
	var estimateSellRate = document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
	var estimateRevenueAmount = document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
	var estimatePassPercentage = document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value;
	//var displayOnQuote = document.forms['pricingListForm'].elements['displayOnQuote'+aid].value;
	var deviation = document.forms['pricingListForm'].elements['deviation'+aid].value;
	var estimateDeviation = document.forms['pricingListForm'].elements['estimateDeviation'+aid].value;
	var estimateSellDeviation = document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
	
	var billingContract = '${billing.contract}';
	var shipNumber='${serviceOrder.shipNumber}';
	var comptetive = document.forms['pricingListForm'].elements['inComptetive'].value;
	if(charge!=''){
		if(rev == "Internal Cost"){
			if(charge=="VANPAY" ){
				getVanPayCharges("Estimate",aid);
			}else if(charge=="DOMCOMM" ){
				getDomCommCharges("Estimate",aid);
			}else if(charge=="SALESCM" && comptetive=="Y") { 
				var totalExpense = eval(document.forms['pricingListForm'].elements['inEstimatedTotalExpense'].value);
				var totalRevenue =eval(document.forms['pricingListForm'].elements['inEstimatedTotalRevenue'].value); 
				var commisionRate =eval(document.forms['pricingListForm'].elements['salesCommisionRate'].value); 
				var grossMargin = eval(document.forms['pricingListForm'].elements['grossMarginThreshold'].value);
				var commision=((totalRevenue*commisionRate)/100);
				var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
				calGrossMargin=calGrossMargin*100;
				var calGrossMargin=Math.round(calGrossMargin);
				if(calGrossMargin>=grossMargin)  {
					document.forms['pricingListForm'].elements['estimateExpense'+aid].value=Math.round(commision*100)/100; 
					document.forms['pricingListForm'].elements['basis'+aid].value='each';
					document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
					<c:if test="${contractType}">
					document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
					</c:if>			    	
				} else { 
					commision=((.8*totalRevenue)-totalExpense); 
					commision=Math.round(commision*100)/100;
					if(commision<0) { 
						document.forms['pricingListForm'].elements['estimateExpense'+aid].value=0; 
						document.forms['pricingListForm'].elements['basis'+aid].value='each';
						document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
						<c:if test="${contractType}">
						document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
						</c:if>			    					    	
					} else {
						document.forms['pricingListForm'].elements['estimateExpense'+aid].value=Math.round(commision*100)/100;
						document.forms['pricingListForm'].elements['basis'+aid].value='each';
						document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
						<c:if test="${contractType}">
						document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
						</c:if>			    					    	
					} 
				}  
			}else if(charge=="SALESCM" && comptetive!="Y") {
				var agree =confirm("This job is not competitive; do you still want to calculate commission?");
				if(agree) {
					var accountCompanyDivision =  '${serviceOrder.companyDivision}'; 
					var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
					http88.open("GET", url, true);
					http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate,basis,aid);};
					http88.send(null);
				} else {
					return false;
				}
			} else {
				var accountCompanyDivision =  '${serviceOrder.companyDivision}';
				var url="internalCostsExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
				http88.open("GET", url, true);
				http88.onreadystatechange = function(){ handleHttpResponse700(estimateExpense, estimateQuantity, estimateRate, basis,aid);};
				http88.send(null);
			}
		} else{	
			var url="invoiceExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=${serviceOrder.id}";
			http88.open("GET", url, true);
			http88.onreadystatechange = function(){ handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, deviation,estimateDeviation, estimateSellDeviation,aid);};
			http88.send(null);
		}
	}else{
		alert("Please select Charge Code.");
	} 
}

var decimalValueCount = 0 ;
var count = 0;
var negativeCount=0;
function clearDecimal(){
	count = 0;
	decimalValueCount = 0;
}

function numbersonly(myfield, e) {
	var key;
	var keychar;
	var val = myfield.value;
	if(count > 0){	decimalValueCount++;    if(decimalValueCount > 2){ return true; }		}
	if(val.indexOf(".")<0)
	{  count=0;	}
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
	keychar = String.fromCharCode(key);
	if ((key==null) || (key==0) || (key==8) ||     (key==9) || (key==13) || (key==27) )
		return true;
	else if ((("0123456789").indexOf(keychar) > -1)){
		return true;
	}
	else if ((count<1)&&(keychar == ".")){
		count++;
		return true;
	}
	else if ((negativeCount<1)&&(keychar == "-"))
	{
		negativeCount++;
		return true;
	}
	else if ((negativeCount>0)&&(keychar == "-"))
	{
		return false;
	}
	else if ((count>0)&&(keychar == ".")){
		return false;
	}
	else
		return false;
}
function showBillingDatails(id,type,position) {
	var url="showBillingDatailsFields.html?ajax=1&decorator=simple&popup=true&aid=" + encodeURI(id)+"&showBillingType=" + encodeURI(type);
	ajax_showTooltip(url,position);	
	var containerdiv =  document.getElementById("para1").scrollLeft;
	var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;	
	document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';	 
}

function getAccountlineField(aid,basis,quantity,chargeCode,contract,vendorCode){
	accountLineBasis=document.forms['pricingListForm'].elements[basis].value;
	accountLineEstimateQuantity=document.forms['pricingListForm'].elements[quantity].value;
	chargeCode=document.forms['pricingListForm'].elements[chargeCode].value;
	contract=document.forms['pricingListForm'].elements[contract].value;
	vendorCode=document.forms['pricingListForm'].elements[vendorCode].value;
	<c:if test="${contractType}">
	var estimatePayableContractCurrencyTemp=document.forms['pricingListForm'].elements['estimatePayableContractCurrencyNew'+aid].value; 
	var estimatePayableContractValueDateTemp=document.forms['pricingListForm'].elements['estimatePayableContractValueDateNew'+aid].value;
	var estimatePayableContractExchangeRateTemp=document.forms['pricingListForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
	var estimatePayableContractRateTemp=document.forms['pricingListForm'].elements['estimatePayableContractRateNew'+aid].value;
	var estimatePayableContractRateAmmountTemp=document.forms['pricingListForm'].elements['estimatePayableContractRateAmmountNew'+aid].value;
	</c:if>
	var estCurrencyTemp=document.forms['pricingListForm'].elements['estCurrencyNew'+aid].value; 
	var estValueDateTemp=document.forms['pricingListForm'].elements['estValueDateNew'+aid].value;
	var estExchangeRateTemp=document.forms['pricingListForm'].elements['estExchangeRateNew'+aid].value;
	var estLocalRateTemp=document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value;
	var estLocalAmountTemp=document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value;

	var estimateQuantityTemp=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
	var estimateRateTemp=document.forms['pricingListForm'].elements['estimateRate'+aid].value;
	var estimateExpenseTemp=document.forms['pricingListForm'].elements['estimateExpense'+aid].value;
	var basisTemp=document.forms['pricingListForm'].elements['basis'+aid].value;
	<c:if test="${contractType}">
	openWindow('editBuyEstimateCurrency.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estimatePayableContractCurrencyTemp='+estimatePayableContractCurrencyTemp+'&estimatePayableContractValueDateTemp='+estimatePayableContractValueDateTemp+'&estimatePayableContractExchangeRateTemp='+estimatePayableContractExchangeRateTemp+'&estimatePayableContractRateTemp='+estimatePayableContractRateTemp+'&estimatePayableContractRateAmmountTemp='+estimatePayableContractRateAmmountTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	</c:if>
	<c:if test="${!contractType}">
	openWindow('editBuyEstimateCurrency.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	</c:if>
}

function onlyRateAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode; 
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110||(keyCode==189)|| (keyCode==173)); 
}

function onlyNumsAllowedPersent(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
}
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode; 
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function changeEstimatePassPercentage(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt){
	var estimate=0; 
	var estimateround=0;
	var estimateRevenue=0;
	var estimateRevenueRound=0;
	var sellRateValue=0;
	var sellRateround=0;
	var	estimatepasspercentage =0;
	var quantityValue = 0;
	var oldRevenue=0;
	var oldRevenueSum=0;
	var balanceRevenue=0; 
	var buyRateValue=0;  
	oldRevenue=eval(document.forms['pricingListForm'].elements[revenue].value); 
	<c:if test="${not empty serviceOrder.id}">
	oldRevenueSum=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	</c:if> 
	quantityValue = eval(document.forms['pricingListForm'].elements[quantity].value); 
	buyRateValue = eval(document.forms['pricingListForm'].elements[buyRate].value); 
	estimatepasspercentage= document.forms['pricingListForm'].elements[estimatePassPercentage].value;
	estimate = (quantityValue*buyRateValue); 
	if(quantityValue>0 && estimate>0){
		if( document.forms['pricingListForm'].elements[basis].value=="cwt" || document.forms['pricingListForm'].elements[basis].value=="%age"){
			estimate = estimate/100; 	  	
		} 
		if( document.forms['pricingListForm'].elements[basis].value=="per 1000"){
			estimate = estimate/1000; 	  	
		}
		estimateRevenue=((estimate)*estimatepasspercentage/100)
		estimateRevenueRound=Math.round(estimateRevenue*100)/100; 
		document.forms['pricingListForm'].elements[revenue].value=estimateRevenueRound;   
		if( document.forms['pricingListForm'].elements[basis].value=="cwt" || document.forms['pricingListForm'].elements[basis].value=="%age"){
			sellRateValue=(estimateRevenue/quantityValue)*100;
			sellRateround=Math.round(sellRateValue*100)/100;

			document.forms['pricingListForm'].elements[sellRate].value=sellRateround;	  	
		}
		else if( document.forms['pricingListForm'].elements[basis].value=="per 1000"){
			sellRateValue=(estimateRevenue/quantityValue)*1000;
			sellRateround=Math.round(sellRateValue*100)/100; 
			document.forms['pricingListForm'].elements[sellRate].value=sellRateround;	  	
		} else if(document.forms['pricingListForm'].elements[basis].value!="cwt" || document.forms['pricingListForm'].elements[basis].value!="%age"||document.forms['pricingListForm'].elements[basis].value!="per 1000")
		{
			sellRateValue=(estimateRevenue/quantityValue);
			sellRateround=Math.round(sellRateValue*100)/100; 
			document.forms['pricingListForm'].elements[sellRate].value=sellRateround;	  	
		}
		balanceRevenue=estimateRevenueRound-oldRevenue;
		oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		balanceRevenue=Math.round(balanceRevenue*100)/100; 
		oldRevenueSum=oldRevenueSum+balanceRevenue;  
		<c:if test="${not empty serviceOrder.id}">
		var newRevenueSum=""+oldRevenueSum;
		if(newRevenueSum.indexOf(".") == -1){
			newRevenueSum=newRevenueSum+".00";
		} 
		if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
			newRevenueSum=newRevenueSum+"0";
		} 
		document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
		</c:if>
		if(estimateRevenueRound>0){
			//document.forms['pricingListForm'].elements[displayOnQuots].checked = true;
		}
		try{
			<c:if test="${systemDefaultVatCalculationNew=='Y'}">
			calculateVatAmt(estVatPer,revenue,estAmt);
			</c:if>
		}catch(e){}
		//calculateGrossMargin();
	}
	else{
		document.forms['pricingListForm'].elements[estimatePassPercentage].value=0;}} 
function checkFloat1(temp) { 
	negativeCount = 0;
	var check='';  
	var i;
	var dotcheck=0;
	var s = temp.value;
	if(s.indexOf(".") == -1){
		dotcheck=eval(s.length)
	}else{
		dotcheck=eval(s.indexOf(".")-1)
	}
	var fieldName = temp.name;  
	var count = 0;
	var countArth = 0;
	for (i = 0; i < s.length; i++) {   
		var c = s.charAt(i); 
		if(c == '.') {
			count = count+1
		}
		if(c == '-') {
			countArth = countArth+1
		}
		if((i!=0)&&(c=='-')) {
			alert("Invalid data." ); 
			document.forms['pricingListForm'].elements[fieldName].select();
			document.forms['pricingListForm'].elements[fieldName].value='';
			return false;
		} 	
		if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))  {
			check='Invalid'; 
		}
	}
	if(dotcheck>15){
		check='tolong';
	}
	if(check=='Invalid'){ 
		alert("Invalid data." ); 
		document.forms['pricingListForm'].elements[fieldName].select();
		document.forms['pricingListForm'].elements[fieldName].value='';
		return false;
	} else if(check=='tolong'){ 
		alert("Data Too Long." ); 
		document.forms['pricingListForm'].elements[fieldName].select();
		document.forms['pricingListForm'].elements[fieldName].value='';
		return false;
	} else{
		s=Math.round(s*100)/100;
		var value=""+s;
		if(value.indexOf(".") == -1){
			value=value+".00";
		} 
		if((value.indexOf(".")+3 != value.length)){
			value=value+"0";
		}
		document.forms['pricingListForm'].elements[fieldName].value=value;
		return true;
	}
}
function getAccountlineFieldRev(aid,basis,quantity,chargeCode,contract,vendorCode){
	var basis=document.forms['pricingListForm'].elements[basis].value;
	var revisionQuantity=document.forms['pricingListForm'].elements[quantity].value;
	var chargeCode=document.forms['pricingListForm'].elements[chargeCode].value;
	var contract=document.forms['pricingListForm'].elements[contract].value;  
	var vendorCode=document.forms['pricingListForm'].elements[vendorCode].value;  
	var strValue="";
	strValue=basis+"~"+revisionQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
	var revisionCurrency=document.forms['pricingListForm'].elements['revisionCurrency'+aid].value; 
	var revisionValueDate=document.forms['pricingListForm'].elements['revisionValueDate'+aid].value;
	var revisionLocalRate=document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value;
	var revisionExchangeRate=document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;
	var revisionLocalAmount=document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value;
	strValue=strValue+"~"+revisionCurrency+"~"+revisionValueDate+"~"+revisionLocalRate+"~"+revisionExchangeRate+"~"+revisionLocalAmount;
	var revisionExpense=document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
	var revisionDiscount=0.00;
	<c:if test="${chargeDiscountFlag}">
	revisionDiscount=document.forms['pricingListForm'].elements['revisionDiscount'+aid].value;
	</c:if>
	var revisionRate=document.forms['pricingListForm'].elements['revisionRate'+aid].value;
	strValue=strValue+"~"+revisionExpense+"~"+revisionDiscount+"~"+revisionRate;
	<c:if test="${contractType}">
	var revisionPayableContractCurrency=document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value; 
	var revisionPayableContractValueDate=document.forms['pricingListForm'].elements['revisionPayableContractValueDate'+aid].value;
	var revisionPayableContractRate=document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
	var revisionPayableContractExchangeRate=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value;
	var revisionPayableContractRateAmmount=document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value;
	strValue=strValue+"~"+revisionPayableContractCurrency+"~"+revisionPayableContractValueDate+"~"+revisionPayableContractRate+"~"+revisionPayableContractExchangeRate+"~"+revisionPayableContractRateAmmount;
	</c:if>	  
	openWindow('editBuyRevisionCurrency.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);
}
function checkNotesForRevnue(aid,url){
	var chargeCode=document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	if(chargeCode==''){
		alert("Charge code needs to be selected");
	}else{
		openWindow('notess.html?id='+aid+'&notesId='+aid+'&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true',755,500);
	}
}

function invoiceDetailsMethod(aid){
	var chargeCode=document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	var vendorCode=document.forms['pricingListForm'].elements['vendorCode'+aid].value; 
	var recGl=document.forms['pricingListForm'].elements['recGl'+aid].value;
	var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value;   
	var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
	var payingStatus=document.forms['pricingListForm'].elements['payingStatus'+aid].value;
	var receivedDate=document.forms['pricingListForm'].elements['receivedDate'+aid].value; 
	var invoiceNumber=document.forms['pricingListForm'].elements['invoiceNumber'+aid].value;
	var invoiceDate=document.forms['pricingListForm'].elements['invoiceDate'+aid].value;   
	var valueDate=document.forms['pricingListForm'].elements['valueDate'+aid].value;
	var type='PAY';
	var acref="${accountInterface}";
	acref=acref.trim();
	if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("Vendor Code Missing. Please select");
	}else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
		alert("Charge code needs to be selected");
	}	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("Pay GL missing. Please select another charge code.");
	}	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC'){
		alert("Receivable GL missing. Please select another charge code.");
	}else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
	}else{		   
		openWindow('actualInvoiceSectionFile.html?ajax=1&aid='+aid+'&payingStatus='+payingStatus+'&receivedDate='+receivedDate+'&invoiceNumber='+invoiceNumber+'&invoiceDate='+invoiceDate+'&vendorCode='+vendorCode+'&valueDate='+valueDate+'&decorator=popup&popup=true',920,350);
	}
}

function getAccountlineFieldPay(aid,basis,quantity,chargeCode,contract,vendorCode){
	var basis=document.forms['pricingListForm'].elements[basis].value;
	var recQuantity=document.forms['pricingListForm'].elements[quantity].value;
	var chargeCode=document.forms['pricingListForm'].elements[chargeCode].value;
	var contract=document.forms['pricingListForm'].elements[contract].value;  
	var vendorCode=document.forms['pricingListForm'].elements[vendorCode].value; 
	var recGl=document.forms['pricingListForm'].elements['recGl'+aid].value;
	var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value;   
	var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
	var type='PAY';
	var acref="${accountInterface}";
	acref=acref.trim();
	if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("Vendor Code Missing. Please select");
	}else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
		alert("Charge code needs to be selected");
	}	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("Pay GL missing. Please select another charge code.");
	}	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC'){
		alert("Receivable GL missing. Please select another charge code.");
	}else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
	}else{		   
		var strValue="";
		strValue=basis+"~"+recQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
		var country=document.forms['pricingListForm'].elements['country'+aid].value; 
		var valueDate=document.forms['pricingListForm'].elements['valueDate'+aid].value;
		var exchangeRate=document.forms['pricingListForm'].elements['exchangeRate'+aid].value;
		var localAmount=document.forms['pricingListForm'].elements['localAmount'+aid].value;
		strValue=strValue+"~"+country+"~"+valueDate+"~"+exchangeRate+"~"+localAmount;
		var actualExpense=document.forms['pricingListForm'].elements['actualExpense'+aid].value;
		var actualDiscount=0.00;
		<c:if test="${chargeDiscountFlag}">
		actualDiscount=document.forms['pricingListForm'].elements['actualDiscount'+aid].value;
		</c:if>
		strValue=strValue+"~"+actualExpense+"~"+actualDiscount;
		<c:if test="${contractType}">
		var payableContractCurrency=document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value; 
		var payableContractValueDate=document.forms['pricingListForm'].elements['payableContractValueDate'+aid].value;
		var payableContractExchangeRate=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value;
		var payableContractRateAmmount=document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value;
		strValue=strValue+"~"+payableContractCurrency+"~"+payableContractValueDate+"~"+payableContractExchangeRate+"~"+payableContractRateAmmount;
		</c:if>	  
		openWindow('editBuyActualCurrency.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);
	}
}
function discriptionView1(discription){ 
	document.getElementById(discription).style.height="15px"; 
	SetCursorToTextEnd(discription);
}
function SetCursorToTextEnd(textControlID){ 
	var text = document.getElementById(textControlID); 
	text.scrollTop = 1;
}

function discriptionView(discription){ 
	document.getElementById(discription).style.height="50px"; 
}

function checkCompanyDiv(id){
	var companyDivision = validateFieldValue('companyDivision'+id);
	if(companyDivision==null || companyDivision==undefined || companyDivision.trim()==''){
		alert("Company Division is required field");
		companyDivision = validateFieldValue('tempCompanyDivision'+id);
		document.forms['pricingListForm'].elements['companyDivision'+id].value=companyDivision;
	}else if(validateFieldValue('payAccDate'+id)!='' || validateFieldValue('recAccDate'+id)!='') { 	 
		alert('You can not change the CompanyDivision as Sent To Acc has been already filled');
		companyDivision = validateFieldValue('tempCompanyDivision'+id);
		document.forms['pricingListForm'].elements['companyDivision'+id].value=companyDivision;
	}else{
	}
}

function selectAdd(str){
	var id = idList.split(",");
	for (i=0;i<id.length;i++){
		if(str.checked == true){
			document.getElementById('additionalService'+id[i].trim()).checked = true;
		}else{
			document.getElementById('additionalService'+id[i].trim()).checked = false;
		}
	}
}

function checkChargeCode(aid){
	var charge = document.forms['pricingListForm'].elements['chargeCode'+aid].value
	var category = document.forms['pricingListForm'].elements['category'+aid].value
	
	 var chargeCheck=document.getElementById('chargeCodeValidationVal').value;
	 if(chargeCheck=='' || chargeCheck==null){
		 document.getElementById('chargeCodeValidationMessage').value="";
	if(charge.trim()!=""){
		progressBarAutoSave('1');
		var val = document.forms['pricingListForm'].elements['category'+aid].value;
		var jobType='${serviceOrder.job}'; 
		var routing='${serviceOrder.routing}'; 
		var soCompanyDivision =	'${serviceOrder.companyDivision}'; 
		var sid='${serviceOrder.id}'; 
		var accountCompanyDivision = document.forms['pricingListForm'].elements['companyDivision'+aid].value;
		var quotationContract = '${billing.contract}';
		var myRegxp = /^[a-zA-Z0-9-_\'('&'\)']+$/;
		if(charge.match(myRegxp)==false){
			alert( "Please enter a valid charge code." );
			document.forms['pricingListForm'].elements[chargeCode].value='';
			progressBarAutoSave('0');
		}
		else{
			if(val=='Internal Cost'){
				quotationContract="Internal Cost Management";
				var url="checkInternalCostChargeCode.html?ajax=1&decorator=simple&popup=true&accountCompanyDivision="+encodeURIComponent(accountCompanyDivision)+"&chargeCode="+encodeURIComponent(charge)+"&jobType="+encodeURIComponent(jobType)+"&routing="+encodeURIComponent(routing)+"&soId="+sid+"&soCompanyDivision="+encodeURIComponent(soCompanyDivision); 
				http5.open("GET", url, true);
				http5.onreadystatechange =function(){handleHttpResponseChargeCode(aid,'Internal');} ;
				http5.send(null);
			}
			else{
				if(quotationContract=='' || quotationContract==' '){
					alert("There is no pricing contract in billing: Please select.");
					document.forms['pricingListForm'].elements[chargeCode].value = "";
					progressBarAutoSave('0');
				}
				else{
					var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+quotationContract+"&chargeCode="+charge+"&jobType="+jobType+"&routing="+routing+"&soCompanyDivision="+soCompanyDivision; 
					http5.open("GET", url, true);
					http5.onreadystatechange =function(){handleHttpResponseChargeCode(aid,'NoInternal');} ;
					http5.send(null);
				}}}}}}
/* function fillDiscription(aid){ 
	var chargeCode = document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	var quotationContract = '${billing.contract}';
	var jobtype = '${serviceOrder.job}'; 
	var routing = '${serviceOrder.routing}'; 
	var companyDivision = '${serviceOrder.companyDivision}';
	var sid='${serviceOrder.id}'; 
	var url = "findQuotationDiscription.html?ajax=1&contract="+quotationContract+"&chargeCode="+chargeCode+"&decorator=simple&popup=true&jobType="+jobtype+"&routing1="+routing+"&soId="+sid+"&companyDivision="+companyDivision;
	http50.open("GET", url, true);
	http50.onreadystatechange =  function(){fillDiscriptionResponse(aid);};
	http50.send(null);
}

function fillDiscriptionResponse(aid){ 
	if (http50.readyState == 4)
	{
		var result= http50.responseText
		result = result.trim();   
		result = result.replace('[','');
		result=result.replace(']','');
		var res = result.split("#"); 
		var discriptionValue = document.forms['pricingListForm'].elements['quoteDescription'+aid].value; 
		if(result.indexOf('NoDiscription')<0) {

			if(res[5]==''){   
				document.forms['pricingListForm'].elements['quoteDescription'+aid].value = res[0]; 
			}else{
				document.forms['pricingListForm'].elements['quoteDescription'+aid].value = res[5]; 
			}

			document.forms['pricingListForm'].elements['recGl'+aid].value = res[1];     
			document.forms['pricingListForm'].elements['payGl'+aid].value = res[2]; 
			<c:if test="${contractType}">
				document.forms['pricingListForm'].elements['contractCurrency'+aid].value = res[3];
			</c:if>
			fillDefaultInvoiceNumber(aid);
		}
	}
	<c:if test="${contractType}">
	var contCurrency = document.forms['pricingListForm'].elements['contractCurrency'+aid].value;
	var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(contCurrency);
	http77.open("GET", url, true);
	http77.onreadystatechange = function(){handleHttpResponseContractRate(aid);};
	http77.send(null);
	</c:if>
}

function handleHttpResponseContractRate(aid) { 
	if (http77.readyState == 4) {
		var results = http77.responseText
		results = results.trim(); 
		results = results.replace('[','');
		results=results.replace(']',''); 
		if(results.length>1) {
			document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value=results 
		} else {
			document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value=1;
		}
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
			year+=1900
			var day=mydate.getDay()
			var month=mydate.getMonth()+1
			if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
			daym="0"+daym
			var datam = daym+"-"+month+"-"+y.substring(2,4); 
		document.forms['pricingListForm'].elements['contractValueDate'+aid].value=datam;  
	}   } */

function checkPurchaseInvoiceProcessing(target,name,aid) { 
	   var payingStatus=document.forms['pricingListForm'].elements['payingStatus'+aid].value; 
	   var vendorCode=document.forms['pricingListForm'].elements['vendorCode'+aid].value;
	   vendorCode=vendorCode.trim();
	   var estimateVendorName=document.forms['pricingListForm'].elements['estimateVendorName'+aid].value;
	   var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
	   var country=document.forms['pricingListForm'].elements['country'+aid].value;
	   country=country.trim();
	   var invoiceNumber=document.forms['pricingListForm'].elements['invoiceNumber'+aid].value;
	   var invoiceDate='';
	   try{
	   invoiceDate=document.forms['pricingListForm'].elements['invoiceDate'+aid].value;
	   }catch(e){}
	   invoiceDate=invoiceDate.trim();
	   if(invoiceDate!=''){
		   invoiceDate=convertItProperDateFormate(invoiceDate);
	   }
	   invoiceNumber=invoiceNumber.trim();
	  <c:choose>
       <c:when test="${accCorpID=='UTSI'}">
		   	   if(payingStatus=='A') {
				   <c:if test="${not empty accountLine.id}">
				   var idss='${accountLine.id}';
				            var url="getAllAccListAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&invoiceDateNew="+invoiceDate+"&currencyNew="+country+"&idss="+idss;
						    http1986.open("GET", url, true);
						    http1986.onreadystatechange = function(){ handleHttpResponseAccListDataNew(target,name,aid,vendorCode,estimateVendorName,actgCode);};
						    http1986.send(null); 
				   </c:if>
				   <c:if test="${empty accountLine.id}">
				            var url="getAllAccListAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&invoiceDateNew="+invoiceDate+"&currencyNew="+country+"&idss=null";
						    http1986.open("GET", url, true);
						    http1986.onreadystatechange = function(){ handleHttpResponseAccListDataNew(target,name,aid,vendorCode,estimateVendorName,actgCode);};
						    http1986.send(null); 
				   </c:if>		   
					  } else{
			              if(name=='CUR'){
			                  findExchangeRate(aid);
			                  }else if(name=='VEN'){
			                	  <c:if test="${accountInterface!='Y'}">
			                	  checkVendorName(aid);fillCurrencyByChargeCode(aid);
			                	  </c:if>
			                	  <c:if test="${accountInterface=='Y'}">
			                	  vendorCodeForActgCode(aid);fillCurrencyByChargeCode(aid);
			                	  </c:if> 
			                  }   }            
 	  </c:when>
       <c:when test="${accCorpID=='SSCW' || accCorpID=='SUDD'}">
	          if(name=='VEN'){
		          if((invoiceNumber!='')&&(vendorCode!='')){
						   <c:if test="${not empty accountLine.id}">
						   var idss='${accountLine.id}';
						            var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss="+idss;
								    http1983.open("GET", url, true);
								    http1983.onreadystatechange = function(){ handleHttpResponseVendorInvoiceEnhancementNew(target,name,aid,vendorCode,estimateVendorName,actgCode);};
								    http1983.send(null); 
						   </c:if>
						   <c:if test="${empty accountLine.id}">
						            var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss=null";
								    http1983.open("GET", url, true);
								    http1983.onreadystatechange = function(){ handleHttpResponseVendorInvoiceEnhancementNew(target,name,aid,vendorCode,estimateVendorName,actgCode);};
								    http1983.send(null); 
						   </c:if>		   	            
		          }else{
	            	  <c:if test="${accountInterface!='Y'}">
	            	  checkVendorName(aid);fillCurrencyByChargeCode(aid);
	            	  </c:if>
	            	  <c:if test="${accountInterface=='Y'}">
	            	  vendorCodeForActgCode(aid);fillCurrencyByChargeCode(aid);
	            	  </c:if>
		          }  
	          }else{
	                findExchangeRate(aid);
	          }
		  </c:when>
 	  <c:otherwise>
       if(name=='CUR'){
           findExchangeRate(aid);
           }else if(name=='VEN'){
         	  <c:if test="${accountInterface!='Y'}">
         	  checkVendorName(aid);fillCurrencyByChargeCode(aid);
         	  </c:if>
         	  <c:if test="${accountInterface=='Y'}">
         	  vendorCodeForActgCode(aid);fillCurrencyByChargeCode(aid);
         	  </c:if>
           }    	  
       </c:otherwise>
   </c:choose>	  
}

function handleHttpResponseAccListDataNew(target,name,aid,vendorCode,estimateVendorName,actgCode){
    if (http1986.readyState == 4){
        var results = http1986.responseText
        results = results.trim();
        if(results!=''){
            if(name=='CUR'){
	    		 //alert(results);
	  	         document.forms['pricingListForm'].elements['country'+aid].value = document.forms['pricingListForm'].elements['countryNew'+aid].value; 
            }else if(name=='VEN'){
	    		 //alert(results);
	  	       document.forms['pricingListForm'].elements['vendorCode'+aid].value = document.forms['pricingListForm'].elements['vendorCodeOld'+aid].value;
	  	       document.forms['pricingListForm'].elements['estimateVendorName'+aid].value= document.forms['pricingListForm'].elements['estimateVendorNameOld'+aid].value;
	  	       document.forms['pricingListForm'].elements['actgCode'+aid].value= document.forms['pricingListForm'].elements['actgCodeOld'+aid].value;
            }else if(name=='INV'){
		    		 //alert(results);
		    		 var tt= document.forms['pricingListForm'].elements['invoiceDate'+aid].value;
		    		 var payStatus = document.forms['pricingListForm'].elements['payingStatus'+aid].value;
		    		tt=revConvertItProperDateFormate(tt);
         	     document.forms['pricingListForm'].elements['invoiceDate'+aid].value =tt; 
         	  document.forms['pricingListForm'].elements['payingStatus'+aid].value =payStatus;
            } 
        }else{
        if(name=='CUR'){
        	findExchangeRate(aid);
        }else if(name=='VEN'){
      	  <c:if test="${accountInterface!='Y'}">
      	  checkVendorName(aid);fillCurrencyByChargeCode(aid);
      	  </c:if>
      	  <c:if test="${accountInterface=='Y'}">
      	  vendorCodeForActgCode(aid);fillCurrencyByChargeCode(aid);
      	  </c:if> 
        }  }  } 
}

function handleHttpResponseVendorInvoiceEnhancementNew(target,name,aid,vendorCode,estimateVendorName,actgCode){
    if (http1983.readyState == 4){
      var results = http1983.responseText
      results = results.trim();
      if(results!=''){
      	var catVal = document.forms['pricingListForm'].elements['category'+aid].value;
      	if(catVal=='Internal Cost'){
           	  <c:if test="${accountInterface!='Y'}">
            	  checkVendorName(aid);fillCurrencyByChargeCode(aid);
            	  </c:if>
            	  <c:if test="${accountInterface=='Y'}">
            	  vendorCodeForActgCode(aid);fillCurrencyByChargeCode(aid);
            	  </c:if>	                                  	
      	}else{
             alert(results);
           /*   // if(agree) {
	             	 // <c:if test="${accountInterface!='Y'}">
	              	  //checkVendorName(aid);fillCurrencyByChargeCode(aid);
	              	  //</c:if>
	              	  //<c:if test="${accountInterface=='Y'}">
	              	  //vendorCodeForActgCode(aid);fillCurrencyByChargeCode(aid);
	              	  //</c:if>	                  
             // } else{  */
            	document.forms['pricingListForm'].elements['vendorCode'+aid].value = document.forms['pricingListForm'].elements['vendorCodeOld'+aid].value;
   	  	       	document.forms['pricingListForm'].elements['estimateVendorName'+aid].value= document.forms['pricingListForm'].elements['estimateVendorNameOld'+aid].value;
   	  	       	document.forms['pricingListForm'].elements['actgCode'+aid].value= document.forms['pricingListForm'].elements['actgCodeOld'+aid].value;
              }
      }else{
    	  <c:if test="${accountInterface!='Y'}">
    	  checkVendorName(aid);fillCurrencyByChargeCode(aid);
    	  </c:if>
    	  <c:if test="${accountInterface=='Y'}">
    	  vendorCodeForActgCode(aid);fillCurrencyByChargeCode(aid);
    	  </c:if>
      } } 
}

function findExchangeRate(aid){
    var country =document.forms['pricingListForm'].elements['country'+aid].value; 
    var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(country);
    http77.open("GET", url, true);
    http77.onreadystatechange = function(){ handleHttpResponse88(aid);};
    http77.send(null);
}
function handleHttpResponse88(aid) { 
         if (http77.readyState == 4) {
            var results = http77.responseText
            results = results.trim(); 
            results = results.replace('[','');
            results=results.replace(']','');  
            if(results.length>1) {
              document.forms['pricingListForm'].elements['exchangeRate'+aid].value=results
              var mydate=new Date();
	          var daym;
	          var year=mydate.getFullYear()
	          var y=""+year;
	          if (year < 1000)
	          year+=1900
	          var day=mydate.getDay()
	          var month=mydate.getMonth()+1
	          if(month == 1)month="Jan";
	          if(month == 2)month="Feb";
	          if(month == 3)month="Mar";
			  if(month == 4)month="Apr";
			  if(month == 5)month="May";
			  if(month == 6)month="Jun";
			  if(month == 7)month="Jul";
			  if(month == 8)month="Aug";
			  if(month == 9)month="Sep";
			  if(month == 10)month="Oct";
			  if(month == 11)month="Nov";
			  if(month == 12)month="Dec";
			  var daym=mydate.getDate()
			  if (daym<10)
			  daym="0"+daym
			  var datam = daym+"-"+month+"-"+y.substring(2,4); 
			  document.forms['pricingListForm'].elements['valueDate'+aid].value=datam; 
            } else {
            document.forms['pricingListForm'].elements['exchangeRate'+aid].value=1; 
            }
            var contractRate=0;
            <c:if test="${contractType}">
            contractRate = document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value;
            </c:if>
            if(contractRate==0){
            	convertedAmount('none');
            }else{
            	convertLocalAmount('none');
                }   }  
 } 

function convertItProperDateFormate(target){ 
	var calArr=target.split("-");
	var year=calArr[2];
	year="20"+year;
	var month=calArr[1];
	  	   if(month == 'Jan') { month = "01"; }  
	  else if(month == 'Feb') { month = "02"; } 
	  else if(month == 'Mar') { month = "03"; } 
	  else if(month == 'Apr') { month = "04"; } 
	  else if(month == 'May') { month = "05"; } 
	  else if(month == 'Jun') { month = "06"; }  
	  else if(month == 'Jul') { month = "07"; } 
	  else if(month == 'Aug') { month = "08"; }  
	  else if(month == 'Sep') { month = "09"; }  
	  else if(month == 'Oct') { month = "10"; }  
	  else if(month == 'Nov') { month = "11"; }  
	  else if(month == 'Dec') { month = "12"; }	
	var day=calArr[0];
	var date=year+"-"+month+"-"+day;
	return date;
}

function checkAccountInterface(aid){
	<c:if test="${accountInterface!='Y'}">
	checkVendorName(aid);
	</c:if>
	<c:if test="${accountInterface=='Y'}">
	vendorCodeForActgCode(aid);
	</c:if>
}

function noQuotes(evt) {   
	  var keyCode= evt.which ? evt.which : evt.keyCode;
	   if(keyCode==222 || keyCode==13){
	   return false;
	   }
	   else { return true; } 
} 

function closeMypricingDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";	
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
}

function calculateExpense(basis,quantity,buyRate,expense,revenue,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid){
	var expenseValue=0;
	var expenseRound=0;
	var oldExpense=0;
	var oldExpenseSum=0;
	var balanceExpense=0;
	oldExpense=eval(document.forms['pricingListForm'].elements[expense].value);
	<c:if test="${not empty serviceOrder.id}">
	//oldExpenseSum=eval(document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
	</c:if>
	var basisValue = document.forms['pricingListForm'].elements[basis].value;
	checkFloatNew('pricingListForm',quantity,'Nothing');    
	var quantityValue = document.forms['pricingListForm'].elements[quantity].value;
	checkFloatNew('pricingListForm',buyRate,'Nothing');    
	var buyRateValue = document.forms['pricingListForm'].elements[buyRate].value;
	if(basisValue=="cwt" || basisValue== "%age"){
		expenseValue=(quantityValue*buyRateValue)/100 ;
	}
	else if(basisValue=="per 1000"){
		expenseValue=(quantityValue*buyRateValue)/1000 ;
	}else{
		expenseValue=(quantityValue*buyRateValue);
	}
	expenseRound=Math.round(expenseValue*100)/100;
	//balanceExpense=expenseRound-oldExpense;
	//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
	//balanceExpense=Math.round(balanceExpense*100)/100; 
	//oldExpenseSum=oldExpenseSum+balanceExpense;
	//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
	var expenseRoundNew=""+expenseRound;
	if(expenseRoundNew.indexOf(".") == -1){
		expenseRoundNew=expenseRoundNew+".00";
	} 
	if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
		expenseRoundNew=expenseRoundNew+"0";
	}
	document.forms['pricingListForm'].elements[expense].value=expenseRoundNew;
	try{
		var buyRate1=document.forms['pricingListForm'].elements['estimateRate'+aid].value;
		var estCurrency1=document.forms['pricingListForm'].elements['estCurrencyNew'+aid].value;
		if(estCurrency1.trim()!=""){
			var Q1=0;
			var Q2=0;
			Q1=buyRate1;
			Q2=document.forms['pricingListForm'].elements['estExchangeRateNew'+aid].value;
			var E1=Q1*Q2;
			E1=Math.round(E1*100)/100;
			document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value=E1;
			var estLocalAmt1=0;
			if(basisValue=="cwt" || basisValue== "%age"){
				estLocalAmt1=(quantityValue*E1)/100 ;
			}
			else if(basisValue=="per 1000"){
				estLocalAmt1=(quantityValue*E1)/1000 ;
			}else{
				estLocalAmt1=(quantityValue*E1);
			}	 
			estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
			document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value=estLocalAmt1;     
		}
		<c:if test="${contractType}">
		try{
			var estimatePayableContractExchangeRate=document.forms['pricingListForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
			var estLocalRate=document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value;
			var estExchangeRate=document.forms['pricingListForm'].elements['estExchangeRateNew'+aid].value
			var recCurrencyRate=(estimatePayableContractExchangeRate*estLocalRate)/estExchangeRate;
			var roundValue=Math.round(recCurrencyRate*100)/100;
			document.forms['pricingListForm'].elements['estimatePayableContractRateNew'+aid].value=roundValue ; 
			var Q11=0;
			var Q21=0;
			Q11=document.forms['pricingListForm'].elements[quantity].value;
			Q21=document.forms['pricingListForm'].elements['estimatePayableContractRateNew'+aid].value;
			var E11=Q11*Q21;
			var estLocalAmt11=0;
			if(basisValue=="cwt" || basisValue== "%age"){
				estLocalAmt11=E11/100 ;
			}
			else if(basisValue=="per 1000"){
				estLocalAmt11=E11/1000 ;
			}else{
				estLocalAmt11=E11;
			}	 
			estLocalAmt11=Math.round(estLocalAmt11*100)/100;    
			document.forms['pricingListForm'].elements['estimatePayableContractRateAmmountNew'+aid].value=estLocalAmt11;
		}catch(e){}     
		</c:if>
	}catch(e){}
	/* <c:if test="${not empty serviceOrder.id}">
	var newExpenseSum=""+oldExpenseSum;
	if(newExpenseSum.indexOf(".") == -1){
		newExpenseSum=newExpenseSum+".00";
	} 
	if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
		newExpenseSum=newExpenseSum+"0";
	} 
	document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
	</c:if>
	if(expenseRoundNew>0){
		//document.forms['pricingListForm'].elements[displayOnQuots].checked = true;
	} */
	try{
		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
		calculateVatAmt(estVatPer,revenue,estAmt);
		</c:if>
	}catch(e){}
	calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
}

function calculateestimatePassPercentage(revenue,expense,estimatePassPercentage){
	var revenueValue=0;
	var expenseValue=0;
	var estimatePassPercentageValue=0; 
	checkFloatNew('pricingListForm',estimatePassPercentage,'Nothing'); 
	revenueValue=eval(document.forms['pricingListForm'].elements[revenue].value);
	expenseValue=eval(document.forms['pricingListForm'].elements[expense].value); 
	try{
		estimatePassPercentageValue=(revenueValue/expenseValue)*100;
	}catch(e){
		estimatePassPercentageValue=0.00;
	} 
	estimatePassPercentageValue=Math.round(estimatePassPercentageValue);
	if(document.forms['pricingListForm'].elements[expense].value == '' || document.forms['pricingListForm'].elements[expense].value == 0||document.forms['pricingListForm'].elements[expense].value == 0.00){
		document.forms['pricingListForm'].elements[estimatePassPercentage].value='';
	}else{ 
		document.forms['pricingListForm'].elements[estimatePassPercentage].value = estimatePassPercentageValue;
	} 
	//calculateGrossMargin();
}

function calculateGrossMargin(){
	<c:if test="${not empty serviceOrder.id}">
	var revenueValue=0;
	var expenseValue=0;
	var grossMarginValue=0; 
	var grossMarginPercentageValue=0; 
	revenueValue=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	expenseValue=eval(document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
	grossMarginValue=revenueValue-expenseValue;
	grossMarginValue=Math.round(grossMarginValue*100)/100; 
	document.forms['pricingListForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
	if(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
		document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
	}else{
		grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
		grossMarginPercentageValue=Math.round(grossMarginPercentageValue*100)/100; 
		document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
	}
	</c:if>    
}

function revisionBuyFX(aid){
	var basis=document.forms['pricingListForm'].elements['basis'+aid].value;
	var baseRateVal=1;
	var roundVal=0.00;
	var revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
	if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		revisionQuantity = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	} 
	if( basis=="cwt" || basis=="%age"){
		baseRateVal=100;
	}else if(basis=="per 1000"){
		baseRateVal=1000;
	} else {
		baseRateVal=1;  	
	}
	var revisionDiscount=0.00;
	try{
		revisionDiscount=document.forms['pricingListForm'].elements['revisionDiscount'+aid].value;
	}catch(e){}
	var revisionRate=0.00;
	revisionRate=document.forms['pricingListForm'].elements['revisionRate'+aid].value;
	if(revisionRate=='') {
		revisionRate = document.forms['pricingListForm'].elements['revisionRate'+aid].value=0.00;
	}
	<c:if test="${contractType}">
	var revisionPayableContractCurrency=document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value;		
	var revisionPayableContractExchangeRate=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value;
	var revisionPayableContractRate=document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
	var revisionPayableContractRateAmmount=document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value;
	var revisionPayableContractValueDate=document.forms['pricingListForm'].elements['revisionPayableContractValueDate'+aid].value;
	if(revisionPayableContractCurrency!=''){
		var ss=findExchangeRateGlobal(revisionPayableContractCurrency);
		revisionPayableContractExchangeRate=parseFloat(ss);
		document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value=revisionPayableContractExchangeRate;
		document.forms['pricingListForm'].elements['revisionPayableContractValueDate'+aid].value=currentDateGlobal();
		revisionPayableContractRate=revisionRate*revisionPayableContractExchangeRate;
		document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value=revisionPayableContractRate;
		revisionPayableContractRateAmmount=(revisionQuantity*revisionPayableContractRate)/baseRateVal;
		document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value=revisionPayableContractRateAmmount;	 		
	}
	</c:if>

	var revisionCurrency=document.forms['pricingListForm'].elements['revisionCurrency'+aid].value;		
	var revisionExchangeRate=document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;
	var revisionLocalRate=document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value;
	var revisionLocalAmount=document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value;
	var revisionValueDate=document.forms['pricingListForm'].elements['revisionValueDate'+aid].value;
	if(revisionCurrency!=''){
		var ss=findExchangeRateGlobal(revisionCurrency);
		revisionExchangeRate=parseFloat(ss);
		document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value=revisionExchangeRate;
		document.forms['pricingListForm'].elements['revisionValueDate'+aid].value=currentDateGlobal();
		revisionLocalRate=revisionRate*revisionExchangeRate;
		document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=revisionLocalRate;
		revisionLocalAmount=(revisionQuantity*revisionLocalRate)/baseRateVal;
		document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=revisionLocalAmount;	 		
	}
}

function validateFieldValue(fieldId){

	var result1 =" ";
	try {
		var result =document.forms['pricingListForm'].elements[fieldId];

		if(result == null || result ==  undefined){
			return result1;
		}else{
			return result.value;
		}
	}catch(e) {
		return result1;
	}
}
function getFieldValue(fieldId){
	var result1 ="";
	var result ="";
	try {
		result = document.forms['pricingListForm'].elements[fieldId];
		if(result == null || result ==  undefined){
			result = document.getElementById(fieldId);
		}
	}catch(e) {}
	try {
		if(result == null || result ==  undefined){
			return result1;
		}else{
			return result.value;
		}
	}catch(e) {
		return result1;
	}
}					 
var httpvandor = getHTTPObject(); 
function vendorCodeForActgCode(aid){
	var vendorId = document.forms['pricingListForm'].elements['vendorCode'+aid].value;
	if(vendorId == ''){
		document.forms['pricingListForm'].elements['estimateVendorName'+aid].value="";
	}
	if(vendorId != ''){
		var myRegxp = /([a-zA-Z0-9_-]+)$/;
		if(myRegxp.test(vendorId)==false){
			alert("Vendor code not valid" );
			document.forms['pricingListForm'].elements['estimateVendorName'+aid].value="";
			document.forms['pricingListForm'].elements['vendorCode'+aid].value="";
		}else{
			var companyDivision=document.forms['pricingListForm'].elements['companyDivision'+aid].value;
			var url=""
				<c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
			var actgCodeFlag='';
			try{
				actgCodeFlag=document.forms['pricingListForm'].elements['actgCodeFlag'+aid].value;
			}catch(e){} 
			url="vendorNameForActgCodeLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision)+"&actgCodeFlag="+encodeURI(actgCodeFlag);	     
			</c:if>
			<c:if test="${companyDivisionAcctgCodeUnique=='N' || companyDivisionAcctgCodeUnique==''}"> 
			url="vendorNameForUniqueActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision);
			</c:if>
			httpvandor.open("GET", url, true);
			httpvandor.onreadystatechange = function(){handleHttpResponse22219(aid);} ;
			httpvandor.send(null);
		} }	    
}
function handleHttpResponse22219(aid){
	if (httpvandor.readyState == 4){
		var results = httpvandor.responseText
		results = results.trim(); 

		var res = results.split("#");  
		if(res.length >= 2){ 
			if(res[3]=='Approved'){
				if(res[2] != "null" && res[2] !=''){
					document.forms['pricingListForm'].elements['estimateVendorName'+aid].value = res[1];
					document.forms['pricingListForm'].elements['actgCode'+aid].value = res[2];
				    <c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
				    try{
				    	var aid=vendorCode.replace('vendorCode',''); aid=aid.trim();
				    	if(res[8] != "null" && res[8].trim() !=''){
							document.forms['pricingListForm'].elements['companyDivision'+aid].value=res[8].trim();
							document.forms['pricingListForm'].elements['companyDivision'+aid].disabled=true;
							document.forms['pricingListForm'].elements['vendorCodeOwneroperator'+aid].value='Y'; 
				    	}else{
				    		document.forms['pricingListForm'].elements['companyDivision'+aid].disabled=false;
				    		document.forms['pricingListForm'].elements['vendorCodeOwneroperator'+aid].value=''; 
				    	}
				    	}catch(e){} 
				     </c:if>
			    }  else { 
					document.forms['pricingListForm'].elements['estimateVendorName'+aid].value=res[1]; 
					document.forms['pricingListForm'].elements['actgCode'+aid].value="";
					document.forms['pricingListForm'].elements['vendorCode'+aid].select(); 
				}
			}else{
				alert("Partner code is not approved" ); 
				document.forms['pricingListForm'].elements['estimateVendorName'+aid].value="";
				document.forms['pricingListForm'].elements['vendorCode'+aid].value="";
				document.forms['pricingListForm'].elements['actgCode'+aid].value="";
				document.forms['pricingListForm'].elements['vendorCode'+aid].select(); 
			}     
		}else{
			alert("Partner code not valid" ); 
			document.forms['pricingListForm'].elements['estimateVendorName'+aid].value="";
			document.forms['pricingListForm'].elements['vendorCode'+aid].value="";
			document.forms['pricingListForm'].elements['actgCode'+aid].value="";
			document.forms['pricingListForm'].elements['vendorCode'+aid].select(); 
		}  } } 

function checkFloatNew(formName,fieldName,message)
{   var i;
var s = document.forms[formName].elements[fieldName].value;
var count = 0;
if(s.trim()=='.') {
	document.forms[formName].elements[fieldName].value='0.00';
}else{
	for (i = 0; i < s.length; i++)
	{   
		var c = s.charAt(i); 
		if(c == '.') {
			count = count+1
		}
		if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) && (c != '-') ) 
		{
			document.forms[formName].elements[fieldName].value='0.00';
		} }    } }

function selectiveInvoiceCheck(rowId,targetElement){
	progressBarAutoSave('1');
	var selectiveInvoice=true;
	if(targetElement.checked==false)
	{
		selectiveInvoice=false;
	}
	var sid = '${serviceOrder.id}';
	var url="updateSelectiveInvoice.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&id="+rowId+"&selectiveInvoice="+selectiveInvoice;
	httpCMMAgentInvoiceCheck.open("GET", url, true);
	httpCMMAgentInvoiceCheck.onreadystatechange = selectiveInvoiceCheckResponse;
	httpCMMAgentInvoiceCheck.send(null);
}
function selectiveInvoiceCheckResponse(){
	if (httpCMMAgentInvoiceCheck.readyState == 4)
	{
		var results = httpCMMAgentInvoiceCheck.responseText  
	}
	progressBarAutoSave('0');
}

function currentDateGlobal(){
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	var daym=mydate.getDate()
	if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4); 
	return datam;
}

function calculateRevisionPassPercentage(revenue,expense,estimatePassPercentage){
	var revenueValue=0;
	var expenseValue=0;
	var estimatePassPercentageValue=0; 
	checkFloatNew('pricingListForm',estimatePassPercentage,'Nothing'); 
	revenueValue=eval(document.forms['pricingListForm'].elements[revenue].value);
	expenseValue=eval(document.forms['pricingListForm'].elements[expense].value); 
	try{
		estimatePassPercentageValue=(revenueValue/expenseValue)*100;
	}catch(e){
		estimatePassPercentageValue=0.00;
	} 
	estimatePassPercentageValue=Math.round(estimatePassPercentageValue);
	if(document.forms['pricingListForm'].elements[expense].value == '' || document.forms['pricingListForm'].elements[expense].value == 0||document.forms['pricingListForm'].elements[expense].value == 0.00){
		document.forms['pricingListForm'].elements[estimatePassPercentage].value='';
	}else{ 
		document.forms['pricingListForm'].elements[estimatePassPercentage].value = estimatePassPercentageValue;
	} 
}

function changeVatAmt(estVatDec,estVatPer,revenue,estAmt,estSellAmt) {
	var aid=estVatDec.replace('vatDecr','');
	aid=aid.trim();
	calculateVatSection('EST',aid,'EST');
}  
function calculateVatAmt(estVatPer,revenue,estAmt){
	var aid=estVatPer.replace('vatPers' ,'');
	aid=aid.trim();
	calculateVatAmtSection('EST',aid);		 
} 
function calculateVatSectionAll(type,aid,type1){
	var vatExclude=document.forms['pricingListForm'].elements['VATExclude'+aid].value;
	if(vatExclude==null || vatExclude==false || vatExclude=='false'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = false;
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = false;
		var chargeCode=document.forms['pricingListForm'].elements['chargeCode'+aid].value;
		var vendorCode=document.forms['pricingListForm'].elements['vendorCode'+aid].value; 
		var recGl=document.forms['pricingListForm'].elements['recGl'+aid].value;
		var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value;   
		var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
		var acref="${accountInterface}";
		acref=acref.trim();
		if(vendorCode.trim()=='' && type1!='BAS' && type1=='PAY'){
			alert("Vendor Code Missing. Please select");
		}else if(chargeCode.trim()=='' && type1!='BAS' && type1!='FX' && (type1=='REC' || type1=='PAY')){
			alert("Charge code needs to be selected");
		}	else if(payGl.trim()=='' && type1!='BAS' && type1=='PAY'){
			alert("Pay GL missing. Please select another charge code.");
		}	else if(recGl.trim()=='' && type1!='BAS' && type1=='REC'){
			alert("Receivable GL missing. Please select another charge code.");
		}else if(actgCode.trim()=='' && type1!='BAS' && acref =='Y' && type1=='PAY'){
			alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
		}else if(type=='EXPENSE'){
			<c:forEach var="entry" items="${payVatPercentList}">
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
				calculateVatAmtSectionAll(type,aid);
			}
			</c:forEach>
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value==''){
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
				calculateVatAmtSectionAll(type,aid);
			} 
			<configByCorp:fieldVisibility componentId="accountLine.payVatGl">
			var relo1='';
			<c:forEach var="entry1" items="${payVatPayGLMap}" > 
			if(relo1 ==''){
				if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
					document.forms['pricingListForm'].elements['payVatGl'+aid].value='${entry1.value}'; 
					relo1 ="yes";
				}   }
			</c:forEach>
			relo1='';
			<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
			if(relo1 ==''){
				if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
					document.forms['pricingListForm'].elements['qstPayVatGl'+aid].value='${entry1.value}'; 
					relo1 ="yes";
				}   }
			</c:forEach>		 	     
			</configByCorp:fieldVisibility>


		}else if(type=='REVENUE'){
			<c:forEach var="entry" items="${estVatPersentList}">
			if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
				calculateVatAmtSectionAll(type,aid);
			}
			</c:forEach>
			if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value==''){
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
				calculateVatAmtSectionAll(type,aid);
			}
			<configByCorp:fieldVisibility componentId="accountLine.recVatGL"> 
			var relo1=""  
				<c:forEach var="entry1" items="${euvatRecGLMap}">
			if(relo1==""){ 

				if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry1.key}') {
					document.forms['pricingListForm'].elements['recVatGl'+aid].value='${entry1.value}';  
					relo1="yes"; 
				}  }
			</c:forEach>
			relo1='';
			<c:forEach var="entry1" items="${qstEuvatRecGLMap}" > 
			if(relo1 ==''){
				if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry1.key}'){
					document.forms['pricingListForm'].elements['qstRecVatGl'+aid].value='${entry1.value}'; 
					relo1 ="yes";
				}   }
			</c:forEach>        
			</configByCorp:fieldVisibility> 

		}else {
		}
	}else{
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value="";
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";			  
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = true;
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = true;
		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatAmtTemp'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatAmtTemp'+aid].value=0;

		</c:if>
		document.forms['pricingListForm'].elements['vatAmt'+aid].value=0;			
		document.forms['pricingListForm'].elements['estExpVatAmt'+aid].value=0;			
		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['revisionVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['revisionExpVatAmt'+aid].value=0;			
		</c:if>
	}
}
function calculateVatAmtSectionAll(type,aid){
	var vatExclude=document.forms['pricingListForm'].elements['VATExclude'+aid].value;
	if(vatExclude==null || vatExclude==false || vatExclude=='false'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = false;
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = false;

		if(type=='EXPENSE'){
			try{	 
				var payVatAmt=0.00;
				var actualExpense=0.00;
				if(document.forms['pricingListForm'].elements['payVatPercent'+aid].value!=''){
					var payVatPercent=document.forms['pricingListForm'].elements['payVatPercent'+aid].value;
					actualExpense = document.forms['pricingListForm'].elements['actualExpense'+aid].value;
					<c:if test="${contractType}"> 
					actualExpense=  document.forms['pricingListForm'].elements['localAmount'+aid].value;
					</c:if>

					var firstPersent="";
					var secondPersent=""; 
					var relo="" 
						<c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
					if(relo==""){ 
						if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry.key}') {
							var arr='${entry.value}';
							if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
								firstPersent=arr.split(':')[0];
								secondPersent=arr.split(':')[1];
							}
							relo="yes"; 
						}  }
					</c:forEach> 
					if(firstPersent!='' && secondPersent!=''){
						var p1=parseFloat(firstPersent);
						var p2=parseFloat(secondPersent);
						var temp=0.00;
						payVatAmt=(actualExpense*p1)/100;
						temp=payVatAmt;
						payVatAmt=Math.round(payVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['payVatAmt'+aid].value=payVatAmt;
						payVatAmt=(actualExpense*p2)/100;
						temp=temp+payVatAmt;
						payVatAmt=Math.round(payVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['qstPayVatAmt'+aid].value=payVatAmt;
						document.forms['pricingListForm'].elements['payVatAmtTemp'+aid].value=temp;
					}else{
						payVatAmt=(actualExpense*payVatPercent)/100;
						payVatAmt=Math.round(payVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['payVatAmt'+aid].value=payVatAmt;
						document.forms['pricingListForm'].elements['payVatAmtTemp'+aid].value=payVatAmt;
						document.forms['pricingListForm'].elements['qstPayVatAmt'+aid].value=0.00;						    
					}
				}
			}catch(e){}  

			try{
				var estExpVatAmt=0;
				if(document.forms['pricingListForm'].elements['payVatPercent'+aid].value!=''){
					var payVatPercent=eval(document.forms['pricingListForm'].elements['payVatPercent'+aid].value);
					var estimateExpense= eval(document.forms['pricingListForm'].elements['estimateExpense'+aid].value);
					<c:if test="${contractType}">
					estimateExpense= eval(document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value); 
					</c:if>
					estExpVatAmt=(estimateExpense*payVatPercent)/100;
					estExpVatAmt=Math.round(estExpVatAmt*100)/100; 
					document.forms['pricingListForm'].elements['estExpVatAmt'+aid].value=estExpVatAmt;
				}
			}catch(e){} 

			try{
				var revisionExpVatAmt=0;
				if(document.forms['pricingListForm'].elements['payVatPercent'+aid].value!=''){
					var payVatPercent=eval(document.forms['pricingListForm'].elements['payVatPercent'+aid].value);
					var revisionExpense= eval(document.forms['pricingListForm'].elements['revisionExpense'+aid].value);
					<c:if test="${contractType}">
					revisionExpense= eval(document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value); 
					</c:if>
					revisionExpVatAmt=(revisionExpense*payVatPercent)/100;
					revisionExpVatAmt=Math.round(revisionExpVatAmt*100)/100; 
					document.forms['pricingListForm'].elements['revisionExpVatAmt'+aid].value=revisionExpVatAmt;
				}
			}catch(e){} 

		}else if(type=='REVENUE'){
			try{
				var recVatAmt=0;
				if(document.forms['pricingListForm'].elements['recVatPercent'+aid].value!=''){
					var recVatPercent=eval(document.forms['pricingListForm'].elements['recVatPercent'+aid].value);
					var actualRevenue= eval(document.forms['pricingListForm'].elements['actualRevenue'+aid].value);
					<c:if test="${contractType}"> 
					actualRevenue=  document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value;
					</c:if>
					var firstPersent="";
					var secondPersent=""; 
					var relo="" 
						<c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
					if(relo==""){ 
						if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
							var arr='${entry.value}';
							if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
								firstPersent=arr.split(':')[0];
								secondPersent=arr.split(':')[1];
							}
							relo="yes"; 
						}  }
					</c:forEach> 
					if(firstPersent!='' && secondPersent!=''){
						var p1=parseFloat(firstPersent);
						var p2=parseFloat(secondPersent);
						var temp=0.00;
						recVatAmt=(actualRevenue*p1)/100;
						temp=recVatAmt;
						recVatAmt=Math.round(recVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['recVatAmt'+aid].value=recVatAmt;
						recVatAmt=(actualRevenue*p2)/100;
						temp=temp+recVatAmt;
						recVatAmt=Math.round(recVatAmt*100)/100;
						document.forms['pricingListForm'].elements['qstRecVatAmt'+aid].value=recVatAmt;
						document.forms['pricingListForm'].elements['recVatAmtTemp'+aid].value=temp;
					}else{
						recVatAmt=(actualRevenue*recVatPercent)/100;
						recVatAmt=Math.round(recVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['recVatAmt'+aid].value=recVatAmt;
						document.forms['pricingListForm'].elements['recVatAmtTemp'+aid].value=recVatAmt;
						document.forms['pricingListForm'].elements['qstRecVatAmt'+aid].value=0.00;
					}
				}
			}catch(e){}  
			try{
				var estVatAmt=0;
				if(document.forms['pricingListForm'].elements['recVatPercent'+aid].value!=''){
					var estVatPercent=eval(document.forms['pricingListForm'].elements['recVatPercent'+aid].value);
					var estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value);
					<c:if test="${contractType}">
					estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value); 
					</c:if>
					estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
					estVatAmt=Math.round(estVatAmt*100)/100; 
					document.forms['pricingListForm'].elements['vatAmt'+aid].value=estVatAmt;
				}
			}catch(e){} 


			try{
				var revisionVatAmt=0;
				if(document.forms['pricingListForm'].elements['recVatPercent'+aid].value!=''){
					var revisionVatPercent=eval(document.forms['pricingListForm'].elements['recVatPercent'+aid].value);
					var revisionRevenueAmount= eval(document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value);
					<c:if test="${contractType}">
					revisionRevenueAmount= eval(document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value); 
					</c:if>
					revisionVatAmt=(revisionRevenueAmount*revisionVatPercent)/100;
					revisionVatAmt=Math.round(revisionVatAmt*100)/100; 
					document.forms['pricingListForm'].elements['revisionVatAmt'+aid].value=revisionVatAmt;
				}
			}catch(e){} 



		}else{
		}
	}else{
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value="";
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";				  
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = true;
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = true;					  
		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatAmtTemp'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatAmtTemp'+aid].value=0;

		</c:if>
		document.forms['pricingListForm'].elements['vatAmt'+aid].value=0;			
		document.forms['pricingListForm'].elements['estExpVatAmt'+aid].value=0;			
		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['revisionVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['revisionExpVatAmt'+aid].value=0;			
		</c:if>
	}
}

function calculateVatSection(type,aid,type1){
	var vatExclude=document.forms['pricingListForm'].elements['VATExclude'+aid].value;
	if(vatExclude==null || vatExclude==false || vatExclude=='false'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = false;
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = false;

		var chargeCode=document.forms['pricingListForm'].elements['chargeCode'+aid].value;
		var vendorCode=document.forms['pricingListForm'].elements['vendorCode'+aid].value; 
		var recGl=document.forms['pricingListForm'].elements['recGl'+aid].value;
		var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value;   
		var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
		var acref="${accountInterface}";
		acref=acref.trim();
		if(vendorCode.trim()=='' && type1!='BAS' && type1=='PAY'){
			alert("Vendor Code Missing. Please select");
		}else if(chargeCode.trim()=='' && type1!='BAS' && type1!='FX' && (type1=='REC' || type1=='PAY')){
			alert("Charge code needs to be selected");
		}	else if(payGl.trim()=='' && type1!='BAS' && type1=='PAY'){
			alert("Pay GL missing. Please select another charge code.");
		}	else if(recGl.trim()=='' && type1!='BAS' && type1=='REC'){
			alert("Receivable GL missing. Please select another charge code.");
		}else if(actgCode.trim()=='' && type1!='BAS' && acref =='Y' && type1=='PAY'){
			alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
		}else if(type=='EST'){
			<c:forEach var="entry" items="${estVatPersentList}">
			if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
				calculateVatAmtSection(type,aid);
			}
			</c:forEach>
			if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value==''){
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
				calculateVatAmtSection(type,aid);
			}
			<c:forEach var="entry" items="${payVatPercentList}">
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
				calculateVatAmtSection(type,aid);
			}
			</c:forEach>
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value==''){
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
				calculateVatAmtSection(type,aid);
			}   

		}else if(type=='REV'){
			<c:forEach var="entry" items="${estVatPersentList}">
			if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
				calculateVatAmtSection(type,aid);
			}
			</c:forEach>
			if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value==''){
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
				calculateVatAmtSection(type,aid);
			}
			<c:forEach var="entry" items="${payVatPercentList}">
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
				calculateVatAmtSection(type,aid);
			}
			</c:forEach>
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value==''){
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
				calculateVatAmtSection(type,aid);
			}   

		}else if(type=='REC'){
			<c:forEach var="entry" items="${estVatPersentList}">
			if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
				calculateVatAmtSection(type,aid);
			}
			</c:forEach>
			if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value==''){
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
				calculateVatAmtSection(type,aid);
			} 

			<configByCorp:fieldVisibility componentId="accountLine.recVatGL"> 
			var relo1=""  
				<c:forEach var="entry1" items="${euvatRecGLMap}">
			if(relo1==""){ 

				if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry1.key}') {
					document.forms['pricingListForm'].elements['recVatGl'+aid].value='${entry1.value}';  
					relo1="yes"; 
				}  }
			</c:forEach>
			relo1='';
			<c:forEach var="entry1" items="${qstEuvatRecGLMap}" > 
			if(relo1 ==''){
				if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry1.key}'){
					document.forms['pricingListForm'].elements['qstRecVatGl'+aid].value='${entry1.value}'; 
					relo1 ="yes";
				}   }
			</c:forEach>        
			</configByCorp:fieldVisibility> 

		}else if(type=='PAY'){
			<c:forEach var="entry" items="${payVatPercentList}">
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry.key}') { 
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value='${entry.value}'; 
				calculateVatAmtSection(type,aid);
			}
			</c:forEach>
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value==''){
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
				calculateVatAmtSection(type,aid);
			} 

			<configByCorp:fieldVisibility componentId="accountLine.payVatGl">
			var relo1='';
			<c:forEach var="entry1" items="${payVatPayGLMap}" > 
			if(relo1 ==''){
				if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
					document.forms['pricingListForm'].elements['payVatGl'+aid].value='${entry1.value}'; 
					relo1 ="yes";
				}   }
			</c:forEach>
			relo1='';
			<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
			if(relo1 ==''){
				if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
					document.forms['pricingListForm'].elements['qstPayVatGl'+aid].value='${entry1.value}'; 
					relo1 ="yes";
				}   }
			</c:forEach>		 	     
			</configByCorp:fieldVisibility>

		}else{
		}
	}else{
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value="";
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";			  
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = true;
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = true;
		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatAmtTemp'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatAmtTemp'+aid].value=0;

		</c:if>
		document.forms['pricingListForm'].elements['vatAmt'+aid].value=0;			
		document.forms['pricingListForm'].elements['estExpVatAmt'+aid].value=0;			
		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['revisionVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['revisionExpVatAmt'+aid].value=0;			
		</c:if>
	}
}
function calculateVatAmtSection(type,aid){
	var vatExclude=document.forms['pricingListForm'].elements['VATExclude'+aid].value;
	if(vatExclude==null || vatExclude==false || vatExclude=='false'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = false;
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = false;

		if(type=='EST'){
			try{
				var estVatAmt=0;
				if(document.forms['pricingListForm'].elements['recVatPercent'+aid].value!=''){
					var estVatPercent=eval(document.forms['pricingListForm'].elements['recVatPercent'+aid].value);
					var estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value);
					<c:if test="${contractType}">
					estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value); 
					</c:if>
					estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
					estVatAmt=Math.round(estVatAmt*100)/100; 
					document.forms['pricingListForm'].elements['vatAmt'+aid].value=estVatAmt;
				}
			}catch(e){} 
			try{
				var estExpVatAmt=0;
				if(document.forms['pricingListForm'].elements['payVatPercent'+aid].value!=''){
					var payVatPercent=eval(document.forms['pricingListForm'].elements['payVatPercent'+aid].value);
					var estimateExpense= eval(document.forms['pricingListForm'].elements['estimateExpense'+aid].value);
					<c:if test="${contractType}">
					estimateExpense= eval(document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value); 
					</c:if>
					estExpVatAmt=(estimateExpense*payVatPercent)/100;
					estExpVatAmt=Math.round(estExpVatAmt*100)/100; 
					document.forms['pricingListForm'].elements['estExpVatAmt'+aid].value=estExpVatAmt;
				}
			}catch(e){} 

		}else if(type=='REV'){ 
			try{
				var revisionVatAmt=0;
				if(document.forms['pricingListForm'].elements['recVatPercent'+aid].value!=''){
					var revisionVatPercent=eval(document.forms['pricingListForm'].elements['recVatPercent'+aid].value);
					var revisionRevenueAmount= eval(document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value);
					<c:if test="${contractType}">
					revisionRevenueAmount= eval(document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value); 
					</c:if>
					revisionVatAmt=(revisionRevenueAmount*revisionVatPercent)/100;
					revisionVatAmt=Math.round(revisionVatAmt*100)/100; 
					document.forms['pricingListForm'].elements['revisionVatAmt'+aid].value=revisionVatAmt;
				}
			}catch(e){} 
			try{
				var revisionExpVatAmt=0;
				if(document.forms['pricingListForm'].elements['payVatPercent'+aid].value!=''){
					var payVatPercent=eval(document.forms['pricingListForm'].elements['payVatPercent'+aid].value);
					var revisionExpense= eval(document.forms['pricingListForm'].elements['revisionExpense'+aid].value);
					<c:if test="${contractType}">
					revisionExpense= eval(document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value); 
					</c:if>
					revisionExpVatAmt=(revisionExpense*payVatPercent)/100;
					revisionExpVatAmt=Math.round(revisionExpVatAmt*100)/100; 
					document.forms['pricingListForm'].elements['revisionExpVatAmt'+aid].value=revisionExpVatAmt;
				}
			}catch(e){} 
		}else if(type=='REC'){				  
			try{
				var recVatAmt=0;
				if(document.forms['pricingListForm'].elements['recVatPercent'+aid].value!=''){
					var recVatPercent=eval(document.forms['pricingListForm'].elements['recVatPercent'+aid].value);
					var actualRevenue= eval(document.forms['pricingListForm'].elements['actualRevenue'+aid].value);
					<c:if test="${contractType}"> 
					actualRevenue=  document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value;
					</c:if>
					var firstPersent="";
					var secondPersent=""; 
					var relo="" 
						<c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
					if(relo==""){ 
						if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
							var arr='${entry.value}';
							if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
								firstPersent=arr.split(':')[0];
								secondPersent=arr.split(':')[1];
							}
							relo="yes"; 
						}  }
					</c:forEach> 
					if(firstPersent!='' && secondPersent!=''){
						var p1=parseFloat(firstPersent);
						var p2=parseFloat(secondPersent);
						var temp=0.00;
						recVatAmt=(actualRevenue*p1)/100;
						temp=recVatAmt;
						recVatAmt=Math.round(recVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['recVatAmt'+aid].value=recVatAmt;
						recVatAmt=(actualRevenue*p2)/100;
						temp=temp+recVatAmt;
						recVatAmt=Math.round(recVatAmt*100)/100;
						document.forms['pricingListForm'].elements['qstRecVatAmt'+aid].value=recVatAmt;
						document.forms['pricingListForm'].elements['recVatAmtTemp'+aid].value=temp;
					}else{
						recVatAmt=(actualRevenue*recVatPercent)/100;
						recVatAmt=Math.round(recVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['recVatAmt'+aid].value=recVatAmt;
						document.forms['pricingListForm'].elements['recVatAmtTemp'+aid].value=recVatAmt;
						document.forms['pricingListForm'].elements['qstRecVatAmt'+aid].value=0.00;
					}
				}
			}catch(e){}    
		}else if(type=='PAY'){
			try{	 
				var payVatAmt=0.00;
				var actualExpense=0.00;
				if(document.forms['pricingListForm'].elements['payVatPercent'+aid].value!=''){
					var payVatPercent=document.forms['pricingListForm'].elements['payVatPercent'+aid].value;
					actualExpense = document.forms['pricingListForm'].elements['actualExpense'+aid].value;
					<c:if test="${contractType}"> 
					actualExpense=  document.forms['pricingListForm'].elements['localAmount'+aid].value;
					</c:if>

					var firstPersent="";
					var secondPersent=""; 
					var relo="" 
						<c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
					if(relo==""){ 
						if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry.key}') {
							var arr='${entry.value}';
							if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
								firstPersent=arr.split(':')[0];
								secondPersent=arr.split(':')[1];
							}
							relo="yes"; 
						}  }
					</c:forEach> 
					if(firstPersent!='' && secondPersent!=''){
						var p1=parseFloat(firstPersent);
						var p2=parseFloat(secondPersent);
						var temp=0.00;
						payVatAmt=(actualExpense*p1)/100;
						temp=payVatAmt;
						payVatAmt=Math.round(payVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['payVatAmt'+aid].value=payVatAmt;
						payVatAmt=(actualExpense*p2)/100;
						temp=temp+payVatAmt;
						payVatAmt=Math.round(payVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['qstPayVatAmt'+aid].value=payVatAmt;
						document.forms['pricingListForm'].elements['payVatAmtTemp'+aid].value=temp;
					}else{
						payVatAmt=(actualExpense*payVatPercent)/100;
						payVatAmt=Math.round(payVatAmt*100)/100; 
						document.forms['pricingListForm'].elements['payVatAmt'+aid].value=payVatAmt;
						document.forms['pricingListForm'].elements['payVatAmtTemp'+aid].value=payVatAmt;
						document.forms['pricingListForm'].elements['qstPayVatAmt'+aid].value=0.00;						    
					}
				}
			}catch(e){}  

		}else{
		}
	}else{
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value="";
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";					  				  
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = true;
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = true;
		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatAmtTemp'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatAmtTemp'+aid].value=0;

		</c:if>
		document.forms['pricingListForm'].elements['vatAmt'+aid].value=0;			
		document.forms['pricingListForm'].elements['estExpVatAmt'+aid].value=0;			
		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['revisionVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['revisionExpVatAmt'+aid].value=0;			
		</c:if>

	}

}

function isSoExtFlag(){
	document.forms['pricingListForm'].elements['isSOExtract'].value="yes";
}

function calculateRevenueNew(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid){
	var revenueValue=0;
	var revenueRound=0;
	var oldRevenue=0;
	var oldRevenueSum=0;
	var balanceRevenue=0;
	oldRevenue=eval(document.forms['pricingListForm'].elements[revenue].value);
	<c:if test="${not empty serviceOrder.id}">
	//oldRevenueSum=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	</c:if>
	var basisValue = document.forms['pricingListForm'].elements[basis].value;
	checkFloatNew('pricingListForm',quantity,'Nothing');
	var quantityValue = document.forms['pricingListForm'].elements[quantity].value;
	checkFloatNew('pricingListForm',sellRate,'Nothing');
	var sellRateValue = document.forms['pricingListForm'].elements[sellRate].value;
	if(basisValue=="cwt" || basisValue== "%age"){
		revenueValue=(quantityValue*sellRateValue)/100 ;
	}
	else if(basisValue=="per 1000"){
		revenueValue=(quantityValue*sellRateValue)/1000 ;
	}else{
		revenueValue=(quantityValue*sellRateValue);
	} 
	revenueRound=Math.round(revenueValue*100)/100;
	//balanceRevenue=revenueRound-oldRevenue;
	//oldRevenueSum=Math.round(oldRevenueSum*100)/100;
	//balanceRevenue=Math.round(balanceRevenue*100)/100; 
	//oldRevenueSum=oldRevenueSum+balanceRevenue; 
	//oldRevenueSum=Math.round(oldRevenueSum*100)/100;
	var revenueRoundNew=""+revenueRound;
	if(revenueRoundNew.indexOf(".") == -1){
		revenueRoundNew=revenueRoundNew+".00";
	} 
	if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
		revenueRoundNew=revenueRoundNew+"0";
	}
	document.forms['pricingListForm'].elements[revenue].value=revenueRoundNew;
	try{
		var buyRate1=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
		var estCurrency1=document.forms['pricingListForm'].elements['estSellCurrencyNew'+aid].value;
		if(estCurrency1.trim()!=""){
			var Q1=0;
			var Q2=0;
			Q1=buyRate1;
			Q2=document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value;
			var E1=Q1*Q2;
			E1=Math.round(E1*100)/100;
			document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value=E1;
			var estLocalAmt1=0;
			if(basisValue=="cwt" || basisValue== "%age"){
				estLocalAmt1=(quantityValue*E1)/100 ;
			}
			else if(basisValue=="per 1000"){
				estLocalAmt1=(quantityValue*E1)/1000 ;
			}else{
				estLocalAmt1=(quantityValue*E1);
			}	 
			estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
			document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
		}
		<c:if test="${contractType}">
		try{
			var estimateContractExchangeRate=0;
			estimateContractExchangeRate=document.forms['pricingListForm'].elements['estimateContractExchangeRateNew'+aid].value; 
			var QbuyRate1=0;
			QbuyRate1=buyRate1;
			document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value=QbuyRate1*estimateContractExchangeRate
			var Q11=0;
			var Q21=0; 
			Q11=document.forms['pricingListForm'].elements[quantity].value;
			Q21=document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value;
			var E11=Q11*Q21;
			var estLocalAmt11=0;
			if(basisValue=="cwt" || basisValue== "%age"){
				estLocalAmt11=E11/100 ;
			}
			else if(basisValue=="per 1000"){
				estLocalAmt11=E11/1000 ;
			}else{
				estLocalAmt11=E11;
			}	 
			estLocalAmt11=Math.round(estLocalAmt11*100)/100;    
			document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value=estLocalAmt11;
		}catch(e){}     
		</c:if>
	}catch(e){}
	/* <c:if test="${not empty serviceOrder.id}">
	var newRevenueSum=""+oldRevenueSum;
	if(newRevenueSum.indexOf(".") == -1){
		newRevenueSum=newRevenueSum+".00";
	} 
	if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
		newRevenueSum=newRevenueSum+"0";
	} 
	//document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
	</c:if>
	if(revenueRoundNew>0){
		//document.forms['pricingListForm'].elements[displayOnQuots].checked = true;
	} */
	try{
		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
		calculateVatAmt(estVatPer,revenue,estAmt);
		</c:if>
	}catch(e){} 	  
	//calculateGrossMargin();
}
function copyPartnerPricingDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId,pricingAccountLineId){
	lastName=lastName.replace("~","'");
	document.getElementById(partnerNameId).value=lastName;
	document.getElementById(paertnerCodeId).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";
	checkPurchaseInvoiceProcessing(this,'VEN',pricingAccountLineId);
	driverCommission(pricingAccountLineId);
	getId(pricingAccountLineId);
	var contract = '${billing.contract}';
	var category=document.forms['pricingListForm'].elements['category'+pricingAccountLineId].value;
	var chargeCode = document.forms['pricingListForm'].elements['chargeCode'+pricingAccountLineId].value;
	enablePreviousFunction(chargeCode,category,pricingAccountLineId,contract);
} 

var http50 = getHTTPObject50();
function getHTTPObject50() {
	var xmlhttp;
	if(window.XMLHttpRequest)  {
		xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)  {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		if (!xmlhttp)  {
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
	}
	return xmlhttp;
}
var elemtId = '';
function triggerCalendarId(elementId){
	return elemtId = elementId;
 }

function checkAccrualReadyValidationOnDate(){
	if(elemtId ==  'accrualReadyDate-trigger'){
		elemtId='';
		checkAccrualReadyDateValidation();
	}
}
function checkAccrualReadyDateValidation(){ 
	
	var revisedGrossMargin = '${serviceOrder.revisedGrossMarginPercentage}';
	var minMargin = '${minMargin}';
	var revisedGrossMarginPercent = parseFloat(revisedGrossMargin);
	var minMarginPercent = parseFloat(minMargin);
	if(revisedGrossMarginPercent < minMarginPercent){
		alert("You can not change the Accrual Ready Date as Accrual Margin is less than the set account minimum margin");
		document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
		return false;
	}  
	<c:if test="${serviceOrder.mode=='Air'}">
	<c:set var="modeWeight" value="${miscellaneous.actualGrossWeight}" />
	<c:set var="modeVolumeDesc" value="${miscellaneous.actualCubicFeet}" />
	</c:if>
	<c:if test="${serviceOrder.mode!='Air' && serviceOrder.mode!=''}">
	<c:set var="modeWeight" value="${miscellaneous.actualNetWeight}" />
	<c:set var="modeVolumeDesc" value="${miscellaneous.netActualCubicFeet}" />
	</c:if>
	<c:choose>
	<c:when test="${empty trackingStatus.originAgentCode}">
	alert('You can not change the Accrual Ready Date as Origin Agent code is blank in status page.');
	document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
	return false;
    </c:when>
    <c:when test="${empty trackingStatus.destinationAgentCode}">
	alert('You can not change the Accrual Ready Date as Destination Agent code is blank in status page.');
	document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
	return false;
    </c:when> 
    <c:when test="${accrualReadyContractFlag}">
	alert("You can not change the Accrual Ready Date as Service contract is blank in routing.");
	document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
	return false;
    </c:when>
	<c:when test="${empty serviceOrder.billToCode && empty customerFile.accountCode && empty modeWeight && empty modeVolumeDesc && empty billing.insuranceHas}">
	alert("You can not change the Accrual Ready Date as billToCode, accountCode, Weight, Volume and insuranceHas value not filled." )
	document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
	</c:when>
	<c:when test="${empty serviceOrder.billToCode}">
	alert("You can not change the Accrual Ready Date as billToCode not filled in billing detail." )
	document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
	</c:when>
	<c:when test="${empty customerFile.accountCode}">
	alert("You can not change the Accrual Ready Date as accountCode not filled in customerfile detail." )
	document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
	</c:when>
	<c:when test="${empty modeWeight}">
	alert("You can not change the Accrual Ready Date as mode or weight not filled in serviceorder detail." )
	document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
	</c:when>
	<c:when test="${empty modeVolumeDesc}">
	alert("You can not change the Accrual Ready Date as mode or volume not filled in serviceorder detail." )
	document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
	</c:when>
	<c:when test="${empty billing.insuranceHas}">
	alert("You can not change the Accrual Ready Date as insuranceHas not filled in billing detail." )
	document.forms['pricingListForm'].elements['serviceOrder.accrualReadyDate'].value ='${serviceOrder.accrualReadyDate}'
	</c:when>
	<c:otherwise>
	fillField('${serviceOrder.id}','${serviceOrder.corpID}');
	</c:otherwise>
	</c:choose>
}

function checkAccrualReadyValidation(){ 
	
	var revisedGrossMargin = '${serviceOrder.revisedGrossMarginPercentage}';
	var minMargin = '${minMargin}';
	var revisedGrossMarginPercent = parseFloat(revisedGrossMargin);
	var minMarginPercent = parseFloat(minMargin);
	if(revisedGrossMarginPercent < minMarginPercent){
		alert("You can not change the Accrual Ready Date as Accrual Margin is less than the set account minimum margin");
		return false;
	}  
	<c:if test="${serviceOrder.mode=='Air'}">
	<c:set var="modeWeight" value="${miscellaneous.actualGrossWeight}" />
	<c:set var="modeVolumeDesc" value="${miscellaneous.actualCubicFeet}" />
	</c:if>
	<c:if test="${serviceOrder.mode!='Air' && serviceOrder.mode!=''}">
	<c:set var="modeWeight" value="${miscellaneous.actualNetWeight}" />
	<c:set var="modeVolumeDesc" value="${miscellaneous.netActualCubicFeet}" />
	</c:if>
	<c:choose>
	<c:when test="${empty trackingStatus.originAgentCode}">
	alert('You can not change the Accrual Ready Date as Origin Agent code is blank in status page.');
	return false;
    </c:when>
    <c:when test="${empty trackingStatus.destinationAgentCode}">
	alert('You can not change the Accrual Ready Date as Destination Agent code is blank in status page.');
	return false;
    </c:when> 
    <c:when test="${accrualReadyContractFlag}">
	alert("You can not change the Accrual Ready Date as Service contract is blank in routing.");
	return false;
    </c:when>
	<c:when test="${empty serviceOrder.billToCode && empty customerFile.accountCode && empty modeWeight && empty modeVolumeDesc && empty billing.insuranceHas}">
	alert("You can not change the Accrual Ready Date as billToCode, accountCode, Weight, Volume and insuranceHas value not filled." )
	</c:when>
	<c:when test="${empty serviceOrder.billToCode}">
	alert("You can not change the Accrual Ready Date as billToCode not filled in billing detail." )
	</c:when>
	<c:when test="${empty customerFile.accountCode}">
	alert("You can not change the Accrual Ready Date as accountCode not filled in customerfile detail." )
	</c:when>
	<c:when test="${empty modeWeight}">
	alert("You can not change the Accrual Ready Date as mode or weight not filled in serviceorder detail." )
	</c:when>
	<c:when test="${empty modeVolumeDesc}">
	alert("You can not change the Accrual Ready Date as mode or volume not filled in serviceorder detail." )
	</c:when>
	<c:when test="${empty billing.insuranceHas}">
	alert("You can not change the Accrual Ready Date as insuranceHas not filled in billing detail." )
	</c:when>
	<c:otherwise>
	fillField('${serviceOrder.id}','${serviceOrder.corpID}');
	</c:otherwise>
	</c:choose>
}

function accrualDateDel(evt,targetElement) 
{
	var oldvalue=targetElement.value;
		var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==46){
  		targetElement.value = '';
  }else{
  if(keyCode==8){
  	targetElement.value = '';
  }
  }
  if(targetElement.value == ''){
	  var agree =confirm("Do you want to delete the Accrual Ready Date.");
	     if(agree) {
	    	 var accrualDate='';
	    	 var id='${serviceOrder.id}';
	    	 var sessionCorpID='${serviceOrder.corpID}'; 
	    	 accrualDate=accrualDate.trim();
	    	 $.get("autoFillAccrualFields.html?ajax=1&accrualDateDel=y&decorator=simple&popup=true", 
	    				{sid:id,sessionCorpID:sessionCorpID,accrualDate:accrualDate,roleSupervisor:''},
	    				function(data){
	    					result = data.trim();
	    					if(data.length<500){
	    					document.getElementById("accrualReadyUpdatedBy").value = data.split("~")[0].trim();
	    					//document.getElementById("accrualReadyDate").value = data.split("~")[1].trim();
	    					}
	    				});
	     }else{
	    	 targetElement.value = oldvalue; 
	     }
  }
  	return false;
  
}
function fillField(id,sessionCorpID){
	<c:set var="rolesAssign" value="${roles}" />
	<c:if test="${fn:contains(rolesAssign, 'ROLE_SUPERVISOR')}">
	<c:set var="rolesAssignFlag" value="Y" />
	</c:if>
var rolassinflag='${appUserName==soCoordinator}';
var accrualDate='';
 accrualDate=document.getElementById("accrualReadyDate").value;
 accrualDate=accrualDate.trim();
 <c:choose>
 <c:when test="${rolesAssignFlag!='Y'}">
	if(rolassinflag=='true' && accrualDate!=''){
		alert("You can not change the Accrual Ready Date,It has already filled.");
		return false;
	}else{
	$.get("autoFillAccrualFields.html?ajax=1&decorator=simple&popup=true", 
			{sid:id,sessionCorpID:sessionCorpID,accrualDate:accrualDate,roleSupervisor:''},
			function(data){
				result = data.trim();
				if(data.length<500){
				document.getElementById("accrualReadyUpdatedBy").value = data.split("~")[0].trim();
				document.getElementById("accrualReadyDate").value = data.split("~")[1].trim();
				}
			});
	}
	</c:when>
	<c:otherwise>
	$.get("autoFillAccrualFields.html?ajax=1&decorator=simple&popup=true", 
			{sid:id,sessionCorpID:sessionCorpID,accrualDate:accrualDate,roleSupervisor:'Y'},
			function(data){
				result = data.trim();
				if(data.length<500){
				document.getElementById("accrualReadyUpdatedBy").value = data.split("~")[0].trim();
				document.getElementById("accrualReadyDate").value = data.split("~")[1].trim();
				}
			});
	</c:otherwise>
 </c:choose>
}	

function getHTTPObjectInternalCostVendorCode() {
	var xmlhttp;
	if(window.XMLHttpRequest)  {
		xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		if (!xmlhttp) {
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		} }
	return xmlhttp;
}									
var httpInternalCost = getHTTPObjectInternalCostVendorCode(); 
var http2 = getHTTPObjectInternalCostVendorCode(); 
var http5 = getHTTPObjectInternalCostVendorCode();
var populateCostElementDatahttp=getHTTPObjectInternalCostVendorCode(); 
//var httpcheckChargesDataAjax = getHTTPObjectInternalCostVendorCode(); 
var httpChargeDiscount= getHTTPObjectInternalCostVendorCode(); 
function updateContractChangeCharge(id,accContract){
	var chargeCodeValidationVal=document.getElementById('chargeCodeValidationVal').value;
	if(chargeCodeValidationVal=='' || chargeCodeValidationVal==null){
		var chargeCode = document.forms['pricingListForm'].elements['chargeCode'+id].value;
		var billingContract = '${billing.contract}';
		var chargeCodetemp= document.forms['pricingListForm'].elements['accChargeCodeTemp'+id].value;
		var sid='${serviceOrder.id}';
		var accountlineContract=accContract;
		var shipNumber='${serviceOrder.shipNumber}';
		if(sid!='' && sid!=null && chargeCode!=''){
			if((chargeCode.trim()!=chargeCodetemp.trim()) && (billingContract.trim()!=accountlineContract.trim())){
				//showOrHide(1);
				url = "updateContractChangeChargeAjax.html?ajax=1&id="+id+"&sid="+sid+"&billingContract="+billingContract+"&shipNumber="+shipNumber;
				http2.open("GET", url, true); 
				http2.onreadystatechange = handleHttpResponse233;
				http2.send(null);	 
			}
		}}
}

function handleHttpResponse233(){
	if (http2.readyState == 4){
		var results = http2.responseText
		results = results.trim();
		showOrHide(0);
	}
}
function findInternalCostVendorCode(aid){
	var vendorCode='vendorCode'+aid;
	var estimateVendorName='estimateVendorName'+aid;
	var actgCode='actgCode'+aid;
	var rev = document.forms['pricingListForm'].elements['category'+aid].value;
	var companyDivision = document.forms['pricingListForm'].elements['companyDivision'+aid].value; 
	if(rev == "Internal Cost"){
		document.forms['pricingListForm'].elements[vendorCode].value=""; 
		document.forms['pricingListForm'].elements[estimateVendorName].value="";  
		var url="findInternalCostVendorCode.html?ajax=1&decorator=simple&popup=true&companyDivision="+encodeURI(companyDivision);
		httpInternalCost.open("GET", url, true);
		httpInternalCost.onreadystatechange = function(){ httpInternalCostVendorCode(aid);};
		httpInternalCost.send(null);
	} }
function httpInternalCostVendorCode(aid) { 
	if (httpInternalCost.readyState == 4)  {
		var result= httpInternalCost.responseText
		result = result.trim();           
		if(result !=""){
			document.forms['pricingListForm'].elements['vendorCode'+aid].value= result;      
			checkAccountInterface(aid);
		}  } }   

function enablePreviousFunction(chargeCode,category,id,idCondition){
	<configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
	if(document.getElementById("chargeCodeValidationMessage").value!='NothingData'){
		document.getElementById('chargeCodeValidationVal').value='';
		document.getElementById('ChargeNameDivId'+id).style.display = "none";
		checkChargeCode(id);
		//fillDiscription(id);
		updateContractChangeCharge(id,idCondition);
		findInternalCostVendorCode(id);
	}
	</configByCorp:fieldVisibility>
}									
function checkVendorName(aid){
	var vendorId = document.forms['pricingListForm'].elements['vendorCode'+aid].value;
	if(vendorId == ''){
		document.forms['pricingListForm'].elements['estimateVendorName'+aid].value="";
	}
	if(vendorId != ''){
		var myRegxp = /([a-zA-Z0-9_-]+)$/;
		if(myRegxp.test(vendorId)==false){
			alert("Vendor code not valid" );
			document.forms['pricingListForm'].elements['estimateVendorName'+aid].value="";
			document.forms['pricingListForm'].elements['vendorCode'+aid].value="";
		}else{
			var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
			http5.open("GET", url, true);
			http5.onreadystatechange =function(){handleHttpResponsevendorCode(aid);} ;
			http5.send(null);
		} }}
function handleHttpResponsevendorCode(aid){
	if (http5.readyState == 4){
		var results = http5.responseText
		results = results.trim();
		var res = results.split("#"); 
		if(res.size() >= 2){ 
			if(res[2] == 'Approved'){
				document.forms['pricingListForm'].elements['estimateVendorName'+aid].value = res[1];
				document.forms['pricingListForm'].elements['vendorCode'+aid].select();
			}else{
				alert("Vendor code is not approved" ); 
				document.forms['pricingListForm'].elements['estimateVendorName'+aid].value="";
				document.forms['pricingListForm'].elements['vendorCode'+aid].value="";
				document.forms['pricingListForm'].elements['vendorCode'+aid].select();
			}
		}else{
			alert("Vendor code not valid" );
			document.forms['pricingListForm'].elements['estimateVendorName'+aid].value="";
			document.forms['pricingListForm'].elements['vendorCode'+aid].value="";
			document.forms['pricingListForm'].elements['vendorCode'+aid].select();
		}}}	
function handleHttpResponseChargeCode(aid,temp) {
	if (http5.readyState == 4) {
		var results = http5.responseText
		results = results.trim(); 
		var res = results.split("#"); 
		/* if(results.length>4)  {
			<c:if test="${costElementFlag}">
			try{
				var ctype= document.forms['pricingListForm'].elements['category'+aid].value;
				ctype=ctype.trim();
				if(ctype==''){
					document.forms['pricingListForm'].elements['category'+aid].value = res[12];
				}
			}catch(e){}
			</c:if>
			populateCostElementData(aid);
			<c:if test="${contractType}">  
			document.forms['pricingListForm'].elements['contractCurrencyNew'+aid].value=res[9];
			document.forms['pricingListForm'].elements['payableContractCurrencyNew'+aid].value=res[10];
			accountLineIdScript=aid;
			checkChargesDataFromSO(); 
			</c:if> 
			<c:if test="${chargeDiscountFlag}">
			checkChargeDiscount(aid);
			</c:if>
		}else{
			document.getElementById("ChargeNameDivId"+aid).style.display = "none";
			document.forms['pricingListForm'].elements['chargeCode'+aid].focus();
			alert("Charge code does not exist according the pricing contract in Pricing Detail, Please select another" );
			document.forms['pricingListForm'].elements['chargeCode'+aid].value = "";  
		} */
		
		 
        var description1 = "";
        var note = "";  
        var wordingTemp="";
        var quoteDescription="";
         if(results.length>6){
        	if(results.indexOf("Login")==-1){
        		<c:if test="${multiCurrency=='Y'}">
	        		var estSellCurrency = document.forms['pricingListForm'].elements['estSellCurrencyNew'+aid].value;
	        		var estSellValueDate = document.forms['pricingListForm'].elements['estSellValueDateNew'+aid].value;
	        		var estSellExchangeRate = document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value;
	        		var revisionSellCurrency = document.forms['pricingListForm'].elements['revisionSellCurrency'+aid].value;
	        		var revisionSellValueDate = document.forms['pricingListForm'].elements['revisionSellValueDate'+aid].value;
	        		var revisionSellExchangeRate = document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;
	        		var recRateCurrency = document.forms['pricingListForm'].elements['recRateCurrency'+aid].value;
	        		var racValueDate = document.forms['pricingListForm'].elements['racValueDate'+aid].value;
	        		var recRateExchange = document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
	        		rateOnActualizationDate(estSellCurrency,estSellValueDate,estSellExchangeRate);
	        		rateOnActualizationDate(revisionSellCurrency,revisionSellValueDate,revisionSellExchangeRate);
	        		rateOnActualizationDate(recRateCurrency,racValueDate,recRateExchange);
        		</c:if>	
           <c:if test="${accountInterface=='Y'}">
           <c:if test="${costElementFlag}">
           	document.forms['pricingListForm'].elements['chargeCostElement'].value=res[11]; 
            var ctype= document.forms['pricingListForm'].elements['category'+aid].value;
       		ctype=ctype.trim();
       		if(ctype==''){
       			document.forms['pricingListForm'].elements['category'+aid].value = res[12];
       		}
       		document.forms['pricingListForm'].elements['recGl'+aid].value = res[1];
            document.forms['pricingListForm'].elements['payGl'+aid].value = res[2];
             
            populateCostElementData(aid);
			</c:if>
			<c:if test="${!costElementFlag}">
				document.forms['pricingListForm'].elements['recGl'+aid].value = res[1];
           		document.forms['pricingListForm'].elements['payGl'+aid].value = res[2];
            </c:if>
	       <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
	            if(res[5]=='Y'){
	            	document.forms['pricingListForm'].elements['includeLHF'+aid].value=true;
	            	document.forms['pricingListForm'].elements['fifthDescription'].value=true;
	            }else{
	             	document.forms['pricingListForm'].elements['includeLHF'+aid].value=false;
	             	document.forms['pricingListForm'].elements['fifthDescription'].value=false;
	            }
	      </configByCorp:fieldVisibility>
            </c:if>
            if(res[6]=='Y'){
                document.forms['pricingListForm'].elements['ignoreForBilling'+aid].value=true;
                document.forms['pricingListForm'].elements['sixthDescription'].value=true;
            }else{
                 document.forms['pricingListForm'].elements['ignoreForBilling'+aid].value=false;
                 document.forms['pricingListForm'].elements['sixthDescription'].value=false;
             }
            <configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
            	quoteDescription = document.forms['pricingListForm'].elements['quoteDescription'+aid].value;
            	quoteDescription=quoteDescription.trim();
            	document.forms['pricingListForm'].elements['buyDependSell'+aid].value=res[7];
            <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	            description1 = document.forms['pricingListForm'].elements['description'+aid].value;
	            note = document.forms['pricingListForm'].elements['note'+aid].value;
	            note=note.trim();
	            description1=description1.trim();
            if(description1==''){
                if(wordingTemp==''){
        			document.forms['pricingListForm'].elements['description'+aid].value=res[4];
                }else{
                	document.forms['pricingListForm'].elements['description'+aid].value=wordingTemp;
                }  
            }
            if(note==''){
	            if(wordingTemp==''){
	            	document.forms['pricingListForm'].elements['note'+aid].value=res[4];
	            }else{
	            	document.forms['pricingListForm'].elements['note'+aid].value=wordingTemp;
	            } 
	        }
            </c:if>
            if(quoteDescription==''){
                if(wordingTemp==''){
                	document.forms['pricingListForm'].elements['quoteDescription'+aid].value=res[4];
                }else{
                	document.forms['pricingListForm'].elements['quoteDescription'+aid].value=wordingTemp;
                }  
            }
          <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
            revisionDescription = document.forms['pricingListForm'].elements['revisionDescription'+aid].value;
            if(revisionDescription==''){
                if(wordingTemp==''){
                	document.forms['pricingListForm'].elements['revisionDescription'+aid].value=res[4];
                }else{
                	document.forms['pricingListForm'].elements['revisionDescription'+aid].value=wordingTemp;
                }  
            }
           </c:if>
            </configByCorp:fieldVisibility>
            <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
            <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	            if(wordingTemp==''){
	            	 document.forms['pricingListForm'].elements['description'+aid].value=res[4];
	            }else{
	            	 document.forms['pricingListForm'].elements['description'+aid].value=wordingTemp;
	            }
	            if(wordingTemp==''){
	            	document.forms['pricingListForm'].elements['note'+aid].value=res[4];
	            }else{
	            	document.forms['pricingListForm'].elements['note'+aid].value=wordingTemp;
	            }
            </c:if>
            if(wordingTemp==''){
            	 document.forms['pricingListForm'].elements['quoteDescription'+aid].value=res[4];
            }else{
            	 document.forms['pricingListForm'].elements['quoteDescription'+aid].value=wordingTemp;
            }
            <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	            if(wordingTemp==''){
	           	 	document.forms['pricingListForm'].elements['revisionDescription'+aid].value=res[4];
	           	}else{
	           	 	document.forms['pricingListForm'].elements['revisionDescription'+aid].value=wordingTemp;
	           	}
            </c:if>
            </configByCorp:fieldVisibility>
        	<c:if test="${systemDefaultVatCalculation=='true'}">
        	var createdBy = document.forms['pricingListForm'].elements['createdBy'+aid].value;
        	var networkSynchedId = document.forms['pricingListForm'].elements['networkSynchedId'+aid].value;
        	var chargeCode = document.forms['pricingListForm'].elements['chargeCode'+aid].value;
        	var accNetworkGroup = '${trackingStatus.accNetworkGroup}';
        	var soNetworkGroup = '${trackingStatus.soNetworkGroup}';
        	<c:if test="${(((createdBy != 'Networking'  && ( !(((fn:indexOf(createdBy,'Stg Bill')>=0 ) &&  not empty networkSynchedId) && !(accNetworkGroup)) )) && (chargeCode != 'MGMTFEE'))|| !(soNetworkGroup))}">
        	if(res[8]=='Y'){
	    		var vatRec=0.00;
	    		vatRec=document.forms['pricingListForm'].elements['recVatAmt'+aid].value;
	    		vatRec=parseFloat(vatRec);
	    		var vatPay=0.00;
	    		vatPay=document.forms['pricingListForm'].elements['payVatAmt'+aid].value;
	    		vatPay=parseFloat(vatPay);
	    		if((vatRec>0)||(vatPay>0)){
	    			var agree = confirm("The new charge code entered is VAT Exempt. Click OK to continue or Click Cancel.");
	    			 if(agree) {
	                 	document.forms['pricingListForm'].elements['VATExclude'+aid].value=true;
	    				document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=true;
	    				document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=true; 
	    				document.forms['pricingListForm'].elements['recVatPercent'+aid].disabled=true;
	    				document.forms['pricingListForm'].elements['recVatAmt'+aid].disabled=true;
	    				document.forms['pricingListForm'].elements['payVatPercent'+aid].disabled=true;
	    				document.forms['pricingListForm'].elements['payVatAmt'+aid].disabled=true;
			    		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";
				        document.forms['pricingListForm'].elements['payVatDescr'+aid].value=""; 
				        document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
				        document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
				        document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
				        document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;	
	    			   }else { 
	    				   document.forms['pricingListForm'].elements['chargeCode'+aid].value=document.forms['pricingListForm'].elements['accChargeCodeTemp'+aid].value;
	    			   }
	    		}else{
			        	document.forms['pricingListForm'].elements['VATExclude'+aid].value=true;
						document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=true;
						document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=true; 
						document.forms['pricingListForm'].elements['recVatPercent'+aid].disabled=true;
						document.forms['pricingListForm'].elements['recVatAmt'+aid].disabled=true;
						document.forms['pricingListForm'].elements['payVatPercent'+aid].disabled=true;
						document.forms['pricingListForm'].elements['payVatAmt'+aid].disabled=true;
			    		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";
				        document.forms['pricingListForm'].elements['payVatDescr'+aid].value=""; 
				        document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
				        document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
				        document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
				        document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
	    		}
        	}else{
        	    document.forms['pricingListForm'].elements['VATExclude'+aid].value=false; 
        		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=false; 
				if(temp=="NoInternal"){
					document.forms['pricingListForm'].elements['recVatPercent'+aid].disabled=false;
					document.forms['pricingListForm'].elements['recVatAmt'+aid].disabled=false;
					document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=false;
				}
				document.forms['pricingListForm'].elements['payVatPercent'+aid].disabled=false;
				document.forms['pricingListForm'].elements['payVatAmt'+aid].disabled=false;
        	}
        	</c:if>
        	</c:if>
        	<c:if test="${contractType}">  
        	if(document.forms['pricingListForm'].elements['recInvoiceNumber'+aid].value == ''){ 
            	document.forms['pricingListForm'].elements['contractCurrencyNew'+aid].value=res[9]; 
        	}
        	if(document.forms['pricingListForm'].elements['payingStatus'+aid].value!='A'){
        		document.forms['pricingListForm'].elements['payableContractCurrencyNew'+aid].value=res[10]; 
        	}
        	//checkChargesData();
        	checkChargesDataFromSO(aid);
        	</c:if> 
       		findInternalCostVendorCode(aid);
       		}else{
    			alert("Session has been ended, Login Again");
    			window.location=document.location.href;  
       		} 
        	 fillDefaultInvoiceNumber(aid);
      }else{
            if(temp=="NoInternal"){
            	document.getElementById("ChargeNameDivId"+aid).style.display = "none";
            	document.forms['pricingListForm'].elements['chargeCode'+aid].focus();
            	var lineNo = document.forms['pricingListForm'].elements['accountLineNumber'+aid].value;
            	alert("Charge code does not exist according to the billing contract for # "+lineNo+", Please select another" );
            	document.getElementById("ChargeNameDivId"+aid).style.display = "none";
            }else{
            	document.forms['pricingListForm'].elements['chargeCode'+aid].focus();
            	var lineNo = document.forms['pricingListForm'].elements['accountLineNumber'+aid].value;
            	alert("Charge code does not exist according to the Company Division for # "+lineNo+", Please select another" );
            }
	            document.forms['pricingListForm'].elements['chargeCode'+aid].value = "";
	            document.forms['pricingListForm'].elements['recGl'+aid].value = "";
	            document.forms['pricingListForm'].elements['payGl'+aid].value = "";
	            document.forms['pricingListForm'].elements['VATExclude'+aid].value=document.forms['pricingListForm'].elements['oldVATExclude'+aid].value;
	            <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
	            document.forms['pricingListForm'].elements['includeLHF'+aid].value=false;
	            </configByCorp:fieldVisibility>
	            document.forms['pricingListForm'].elements['ignoreForBilling'+aid].value=false;
	            document.forms['pricingListForm'].elements['buyDependSell'+aid].value="";
		}
	}
	progressBarAutoSave('0');
}
var httpcheckChargesDataAjax = getHTTPObjectcheckChargesDataAjax(); 
function getHTTPObjectcheckChargesDataAjax() {
	var xmlhttp;
	if(window.XMLHttpRequest)  {
		xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		if (!xmlhttp) {
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		} }
	return xmlhttp;
}
function checkChargesDataAjax1(aid){ 
	if(aid=='temp'){
		aid=accountLineIdScript;
	}  
	var chargeCode= document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	var billingContract = '${billing.contract}';
    var sid='${serviceOrder.id}';
    var url="checkChargesDataAjax.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(billingContract)+"&chargeCode="+encodeURIComponent(chargeCode)+"&soId="+sid; 
    httpcheckChargesDataAjax.open("GET", url, true);
    httpcheckChargesDataAjax.onreadystatechange =function(){handleHttpResponsecheckChargesDataAjax(aid);} ;
    httpcheckChargesDataAjax.send(null);     
    }
function handleHttpResponsecheckChargesDataAjax(aid){ 
	if (httpcheckChargesDataAjax.readyState == 4){
	var results = httpcheckChargesDataAjax.responseText 
     results = results.trim();  
     results = results.replace('[','');
     results=results.replace(']','');  
     if(results==''){
    	 <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
    		var contractdescriptionValue=document.forms['pricingListForm'].elements['firstDescription'].value;
    		document.forms['pricingListForm'].elements['note'+aid].value=contractdescriptionValue;
    		document.forms['pricingListForm'].elements['description'+aid].value=contractdescriptionValue;
    		document.forms['pricingListForm'].elements['quoteDescription'+aid].value=contractdescriptionValue;
    		document.forms['pricingListForm'].elements['revisionDescription'+aid].value=contractdescriptionValue;
    		</configByCorp:fieldVisibility>
    		<configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
    		var  note = document.forms['pricingListForm'].elements['note'+aid].value;
    		note= note.trim();
    		var description= document.forms['pricingListForm'].elements['description'+aid].value;
    		description= description.trim(); 
    		var quoteDescription= document.forms['pricingListForm'].elements['quoteDescription'+aid].value;
    		quoteDescription= quoteDescription.trim(); 
    		var revisionDescription= document.forms['pricingListForm'].elements['revisionDescription'+aid].value;
    		revisionDescription= revisionDescription.trim(); 
    		var contractdescriptionValue=document.forms['pricingListForm'].elements['firstDescription'].value;
    		if(note=='') {  
    			document.forms['pricingListForm'].elements['note'+aid].value=contractdescriptionValue;
    		} 
    		if(description=='') {  
    			document.forms['pricingListForm'].elements['description'+aid].value=contractdescriptionValue;
    			} 
    		if(quoteDescription=='') {  
    			document.forms['pricingListForm'].elements['quoteDescription'+aid].value=contractdescriptionValue;
    			} 
    		if(revisionDescription=='') {  
    			document.forms['pricingListForm'].elements['revisionDescription'+aid].value=contractdescriptionValue;
    			} 
    		</configByCorp:fieldVisibility> 
     } else{
    	 <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
 		var contractdescriptionValue=results;
 		document.forms['pricingListForm'].elements['note'+aid].value=contractdescriptionValue;
 		document.forms['pricingListForm'].elements['revisionDescription'+aid].value=contractdescriptionValue;
 		document.forms['pricingListForm'].elements['description'+aid].value=contractdescriptionValue;
 		document.forms['pricingListForm'].elements['quoteDescription'+aid].value=contractdescriptionValue;
 		</configByCorp:fieldVisibility>
 		<configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
 		var  note = document.forms['pricingListForm'].elements['note'+aid].value;
		note= note.trim();
		var description= document.forms['pricingListForm'].elements['description'+aid].value;
		description= description.trim(); 
		var quoteDescription= document.forms['pricingListForm'].elements['quoteDescription'+aid].value;
		quoteDescription= quoteDescription.trim(); 
		var revisionDescription= document.forms['pricingListForm'].elements['revisionDescription'+aid].value;
		revisionDescription= revisionDescription.trim(); 
 		var contractdescriptionValue=results;
 		if(note=='') {  
 			document.forms['pricingListForm'].elements['note'+aid].value=contractdescriptionValue;
 		} 
 		if(description=='') {  
 			document.forms['pricingListForm'].elements['description'+aid].value=contractdescriptionValue;
 			} 
 		if(quoteDescription=='') {  
 			document.forms['pricingListForm'].elements['quoteDescription'+aid].value=contractdescriptionValue;
 			} 
 		if(revisionDescription=='') {  
			document.forms['pricingListForm'].elements['revisionDescription'+aid].value=contractdescriptionValue;
			} 
 		</configByCorp:fieldVisibility> 
     }  } }


function checkChargesDataFromSO(aid) {
	if(aid=='temp'){
		aid=accountLineIdScript;
	} 
	<c:if test="${contractType}"> 
	var contractCurrency=document.forms['pricingListForm'].elements['contractCurrencyNew'+aid].value;		  		
	if(contractCurrency.trim()!="")
	{ 
		document.forms['pricingListForm'].elements['estimateContractCurrencyNew'+aid].value=contractCurrency.trim();
		document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value=contractCurrency.trim();
		document.forms['pricingListForm'].elements['contractCurrency'+aid].value=contractCurrency.trim();
		var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
		if('${entry.key}'==contractCurrency.trim()){
			rec='${entry.value}';
		}
		</c:forEach>
		document.forms['pricingListForm'].elements['estimateContractExchangeRateNew'+aid].value=rec;
		document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value=rec;
		document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value=rec;
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
			year+=1900
			var day=mydate.getDay()
			var month=mydate.getMonth()+1
			if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
			daym="0"+daym
			var datam = daym+"-"+month+"-"+y.substring(2,4);
		document.forms['pricingListForm'].elements['estimateContractValueDateNew'+aid].value=datam;
		document.forms['pricingListForm'].elements['revisionContractValueDate'+aid].value=datam;
		document.forms['pricingListForm'].elements['contractValueDate'+aid].value=datam;
	}
	var payableContractCurrency=document.forms['pricingListForm'].elements['payableContractCurrencyNew'+aid].value;
	if(payableContractCurrency.trim()!="")
	{ 
		document.forms['pricingListForm'].elements['estimatePayableContractCurrencyNew'+aid].value=payableContractCurrency.trim();
		document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value=payableContractCurrency.trim();
		document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value=payableContractCurrency.trim();
		var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
		if('${entry.key}'==payableContractCurrency.trim()){
			rec='${entry.value}';
		}
		</c:forEach>
		document.forms['pricingListForm'].elements['estimatePayableContractExchangeRateNew'+aid].value=rec;
		document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value=rec;
		document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value=rec;

		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
			year+=1900
			var day=mydate.getDay()
			var month=mydate.getMonth()+1
			if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
			daym="0"+daym
			var datam = daym+"-"+month+"-"+y.substring(2,4);
		document.forms['pricingListForm'].elements['estimatePayableContractValueDateNew'+aid].value=datam;
		document.forms['pricingListForm'].elements['revisionPayableContractValueDate'+aid].value=datam;
		document.forms['pricingListForm'].elements['payableContractValueDate'+aid].value=datam;
	}
	calculateEstimateByChargeCodeBlock(aid,'BAS');
	<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	calculateRevisionByChargeCodeBlock(aid,'BAS');
	</c:if>
	<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">		  
	calculateActualByChargeCodeBlock(aid,'','BAS');
	</c:if>  		  		
	</c:if>		
}

function populateCostElementData(aid){
	if(aid==''){
		aid=accountLineIdScript;
	}
	<c:if test="${costElementFlag}">
	var chargeCode= document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	var billingContract = '${billing.contract}';
	var sid='${serviceOrder.id}';
	var url="populateCostElementDataAjax.html?ajax=1&decorator=simple&popup=true&contract="+encodeURIComponent(billingContract)+"&chargeCode="+encodeURIComponent(chargeCode)+"&soId="+sid;
	populateCostElementDatahttp.open("GET", url, true);
	populateCostElementDatahttp.onreadystatechange = function(){handleHttpResponsePopulateCostElementData(aid);} ;
	populateCostElementDatahttp.send(null);
	</c:if>
}
function handleHttpResponsePopulateCostElementData(aid) { 
	if (populateCostElementDatahttp.readyState == 4) {
		var results = populateCostElementDatahttp.responseText
		results = results.trim();
		if(results.length>1){
			document.forms['pricingListForm'].elements['accountLineCostElement'+aid].value=results.split("~")[0];
			document.forms['pricingListForm'].elements['accountLineScostElementDescription'+aid].value=results.split("~")[1];
		}

	}
}
function checkChargeDiscount(aid){
	var quotationContract = '${billing.contract}';
	var chargeCode = document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	if(chargeCode!=''){
		var url="checkChargeDiscount.html?ajax=1&decorator=simple&popup=true&contract="+quotationContract+"&chargeCode="+chargeCode; 
		httpChargeDiscount.open("GET", url, true);
		httpChargeDiscount.onreadystatechange =function(){handleHttpResponseChargeDiscount(aid);} ;
		httpChargeDiscount.send(null);
	}
}
function handleHttpResponseChargeDiscount(aid){
	if (httpChargeDiscount.readyState == 4) {
		var results = httpChargeDiscount.responseText
		results = results.trim(); 
		if(results.length>3){
			results = results.replace("{",'');
			results = results.replace("}",'');  
			var discountLength=results.split(",")         
			targetElement = document.forms['pricingListForm'].elements['estimateDiscount'+aid];
			targetElement.length = discountLength.length;
			for(i=0;i<discountLength.length;i++)
			{
				document.forms['pricingListForm'].elements['estimateDiscount'+aid].options[i].value=discountLength[i].trim().split("=")[0];
				document.forms['pricingListForm'].elements['estimateDiscount'+aid].options[i].text=discountLength[i].trim().split("=")[1];
			}
			document.forms['pricingListForm'].elements['estimateDiscount'+aid].value=document.forms['pricingListForm'].elements['oldEstimateDiscount'+aid].value;

			var flagDiscountTemp='false';
			try{
				<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				flagDiscountTemp='true';
				</c:if>
			}catch(e){}
			if(flagDiscountTemp=='true'){		       
				targetElement = document.forms['pricingListForm'].elements['revisionDiscount'+aid];
				targetElement.length = discountLength.length;
				for(i=0;i<discountLength.length;i++)
				{
					document.forms['pricingListForm'].elements['revisionDiscount'+aid].options[i].value=discountLength[i].trim().split("=")[0];
					document.forms['pricingListForm'].elements['revisionDiscount'+aid].options[i].text=discountLength[i].trim().split("=")[1];
				}
				document.forms['pricingListForm'].elements['revisionDiscount'+aid].value=document.forms['pricingListForm'].elements['oldRevisionDiscount'+aid].value;
			}

			flagDiscountTemp='false';
			try{
				<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
				flagDiscountTemp='true';
				</c:if>
			}catch(e){}
			if(flagDiscountTemp=='true'){		       
				targetElement = document.forms['pricingListForm'].elements['actualDiscount'+aid];
				targetElement.length = discountLength.length;
				for(i=0;i<discountLength.length;i++)
				{
					document.forms['pricingListForm'].elements['actualDiscount'+aid].options[i].value=discountLength[i].trim().split("=")[0];
					document.forms['pricingListForm'].elements['actualDiscount'+aid].options[i].text=discountLength[i].trim().split("=")[1];
				}
				document.forms['pricingListForm'].elements['actualDiscount'+aid].value=document.forms['pricingListForm'].elements['oldActualDiscount'+aid].value;
			} 
		}
	}
}
function calculateEstimateByChargeCodeBlock(aid,type1){
	//Code For estimate Est	 
	estimateBuyByChargeCodeFX(aid);
	//Code For estimate Sell	 
	<c:if test="${multiCurrency=='Y'}">
	estimateSellByChargeCodeFX(aid);
	</c:if>
	//Code For Estimate Pass Percentage   
	try{
		calculateestimatePassPercentage('estimateRevenueAmount'+aid,'estimateExpense'+aid,'estimatePassPercentage'+aid);
	}catch(e){}
	// Code Gross Section
	/* var estimatedTotalRevenue=0.00;
	var estimatedTotalExpense=0.00;
	var tempEstimatedTotalExpense=0.00;
	var tempEstimatedTotalRevenue=0.00;
	var estimatedGrossMargin=0.00;
	var estimatedGrossMarginPercentage=0.00;
	var aidListIds=document.forms['pricingListForm'].elements['aidListIds'].value;
	var accArr=aidListIds.split(",");
	for(var g=0;g<accArr.length;g++){
		if(document.forms['pricingListForm'].elements['estimateRevenueAmount'+accArr[g]].value!=""){
			tempEstimatedTotalRevenue=document.forms['pricingListForm'].elements['estimateRevenueAmount'+accArr[g]].value;
		}
		estimatedTotalRevenue=estimatedTotalRevenue+parseFloat(tempEstimatedTotalRevenue);
		if(document.forms['pricingListForm'].elements['estimateExpense'+accArr[g]].value!=""){
			tempEstimatedTotalExpense=document.forms['pricingListForm'].elements['estimateExpense'+accArr[g]].value;
		}
		estimatedTotalExpense=estimatedTotalExpense+parseFloat(tempEstimatedTotalExpense);
	}
	estimatedGrossMargin=estimatedTotalRevenue-estimatedTotalExpense;
	try{
		if((estimatedTotalRevenue==0)||(estimatedTotalRevenue==0.0)||(estimatedTotalRevenue==0.00)){
			estimatedGrossMarginPercentage=0.00;
		}else{
			estimatedGrossMarginPercentage=(estimatedGrossMargin*100)/estimatedTotalRevenue;
		}
	}catch(e){
		estimatedGrossMarginPercentage=0.00;
	}
	<c:if test="${not empty serviceOrder.id}">
	document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=Math.round(estimatedTotalRevenue*100)/100;
	document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=Math.round(estimatedTotalExpense*100)/100;
	document.forms['pricingListForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=Math.round(estimatedGrossMargin*100)/100;
	document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(estimatedGrossMarginPercentage*100)/100;
	</c:if> */	 
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	calculateVatSection('EST',aid,type1); 
	</c:if>
}
function estimateBuyByChargeCodeFX(aid){
	var roundVal=0.00;
	var estimatePayableContractCurrency=document.forms['pricingListForm'].elements['estimatePayableContractCurrencyNew'+aid].value;		
	var estimatePayableContractExchangeRate=document.forms['pricingListForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
	var estimatePayableContractRate=document.forms['pricingListForm'].elements['estimatePayableContractRateNew'+aid].value;
	var estimatePayableContractRateAmmount=document.forms['pricingListForm'].elements['estimatePayableContractRateAmmountNew'+aid].value;
	var estimateCurrency=document.forms['pricingListForm'].elements['estCurrencyNew'+aid].value;
	var estimateExchangeRate=document.forms['pricingListForm'].elements['estExchangeRateNew'+aid].value;
	var estimateLocalRate=document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value;
	var estimateLocalAmount=document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value;

	if(estimatePayableContractCurrency!=''){
		roundVal=estimatePayableContractRate/estimatePayableContractExchangeRate;
		document.forms['pricingListForm'].elements['estimateRate'+aid].value=roundVal;
		if(estimateCurrency!=''){
			document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value=roundVal*estimateExchangeRate;
		}
		roundVal=estimatePayableContractRateAmmount/estimatePayableContractExchangeRate;
		document.forms['pricingListForm'].elements['estimateExpense'+aid].value=roundVal;
		if(estimateCurrency!=''){
			document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value=roundVal*estimateExchangeRate;
		}
	}
}
function estimateSellByChargeCodeFX(aid){
	var roundVal=0.00;
	var estimateContractCurrency=document.forms['pricingListForm'].elements['estimateContractCurrencyNew'+aid].value;		
	var estimateContractExchangeRate=document.forms['pricingListForm'].elements['estimateContractExchangeRateNew'+aid].value;
	var estimateContractRate=document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value;
	var estimateContractRateAmmount=document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value;
	var estimateSellCurrency=document.forms['pricingListForm'].elements['estSellCurrencyNew'+aid].value;		
	var estimateSellExchangeRate=document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value;
	var estimateSellLocalRate=document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value;
	var estimateSellLocalAmount=document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value;

	if(estimateContractCurrency!=''){
		roundVal=estimateContractRate/estimateContractExchangeRate;
		document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=roundVal;
		if(estimateSellCurrency!=''){
			document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value=roundVal*estimateSellExchangeRate;
		}
		roundVal=estimateContractRateAmmount/estimateContractExchangeRate;
		document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundVal;
		if(estimateSellCurrency!=''){
			document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value=roundVal*estimateSellExchangeRate;
		}
	}
}

function calculateRevisionByChargeCodeBlock(aid,type1){
	//Code For revision Est	 
	revisionBuyByChargeCodeFX(aid);
	//Code For revision Sell	 
	<c:if test="${multiCurrency=='Y'}">
	revisionSellByChargeCodeFX(aid);
	</c:if>
	//Code For Revision Pass Percentage   
	calculateRevisionPassPercentage('revisionRevenueAmount'+aid,'revisionExpense'+aid,'revisionPassPercentage'+aid);
	// Code Gross Section
	/* var revisedTotalRevenue=0.00;
	var revisedTotalExpense=0.00;
	var tempRevisedTotalExpense=0.00;
	var tempRevisedTotalRevenue=0.00;
	var revisedGrossMargin=0.00;
	var revisedGrossMarginPercentage=0.00;
	var aidListIds=document.forms['pricingListForm'].elements['aidListIds'].value;
	var accArr=aidListIds.split(",");
	for(var g=0;g<accArr.length;g++){
		if(document.forms['pricingListForm'].elements['revisionRevenueAmount'+accArr[g]].value!=""){
			tempRevisedTotalRevenue=document.forms['pricingListForm'].elements['revisionRevenueAmount'+accArr[g]].value;
		}
		revisedTotalRevenue=revisedTotalRevenue+parseFloat(tempRevisedTotalRevenue);
		if(document.forms['pricingListForm'].elements['revisionExpense'+accArr[g]].value!=""){
			tempRevisedTotalExpense=document.forms['pricingListForm'].elements['revisionExpense'+accArr[g]].value;
		}
		revisedTotalExpense=revisedTotalExpense+parseFloat(tempRevisedTotalExpense);
	}
	revisedGrossMargin=revisedTotalRevenue-revisedTotalExpense;
	try{
		if((revisedTotalRevenue==0)||(revisedTotalRevenue==0.0)||(revisedTotalRevenue==0.00)){
			revisedGrossMarginPercentage=0.00;
		}else{
			revisedGrossMarginPercentage=(revisedGrossMargin*100)/revisedTotalRevenue;
		}
	}catch(e){
		revisedGrossMarginPercentage=0.00;
	}
	<c:if test="${not empty serviceOrder.id}">
	document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value=Math.round(revisedTotalRevenue*100)/100;
	document.forms['pricingListForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value=Math.round(revisedTotalExpense*100)/100;
	document.forms['pricingListForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value=Math.round(revisedGrossMargin*100)/100;
	document.forms['pricingListForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(revisedGrossMarginPercentage*100)/100;
	</c:if> */	 
	<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	calculateVatSection('REV',aid,type1); 
	</c:if>
	</c:if>   
}
function revisionBuyByChargeCodeFX(aid){
	var roundVal=0.00;
	var revisionPayableContractCurrency=document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value;		
	var revisionPayableContractExchangeRate=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value;
	var revisionPayableContractRate=document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
	var revisionPayableContractRateAmmount=document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value;
	var revisionCurrency=document.forms['pricingListForm'].elements['revisionCurrency'+aid].value;		
	var revisionExchangeRate=document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;
	var revisionLocalRate=document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value;
	var revisionLocalAmount=document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value;

	if(revisionPayableContractCurrency!=''){
		roundVal=revisionPayableContractRate/revisionPayableContractExchangeRate;
		document.forms['pricingListForm'].elements['revisionRate'+aid].value=roundVal;
		if(revisionCurrency!=''){
			document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=roundVal*revisionExchangeRate;
		}
		roundVal=revisionPayableContractRateAmmount/revisionPayableContractExchangeRate;
		document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundVal;
		if(revisionCurrency!=''){
			document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=roundVal*revisionExchangeRate;
		}
	}
}
function revisionSellByChargeCodeFX(aid){
	var roundVal=0.00;
	var revisionContractCurrency=document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value;		
	var revisionContractExchangeRate=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value;
	var revisionContractRate=document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
	var revisionContractRateAmmount=document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value;
	var revisionSellCurrency=document.forms['pricingListForm'].elements['revisionSellCurrency'+aid].value;		
	var revisionSellExchangeRate=document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;
	var revisionSellLocalRate=document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value;
	var revisionSellLocalAmount=document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value;

	if(revisionContractCurrency!=''){
		roundVal=revisionContractRate/revisionContractExchangeRate;
		document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=roundVal;
		if(revisionSellCurrency!=''){
			document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=roundVal*revisionSellExchangeRate;
		}
		roundVal=revisionContractRateAmmount/revisionContractExchangeRate;
		document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundVal;
		if(revisionSellCurrency!=''){
			document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=roundVal*revisionSellExchangeRate;
		}
	}

}

function calculateActualByChargeCodeBlock(aid,type1){
	//Code For estimate Est	 
	actualBuyByChargeCodeFX(aid);

	//Code For estimate Sell	 
	<c:if test="${multiCurrency=='Y'}">
	actualSellByChargeCodeFX(aid);
	</c:if>
	/* var actualTotalRevenue=0.00;
	var actualTotalExpense=0.00;
	var tempActualTotalExpense=0.00;
	var tempActualTotalRevenue=0.00;
	var actualGrossMargin=0.00;
	var actualGrossMarginPercentage=0.00;
	var aidListIds=document.forms['pricingListForm'].elements['aidListIds'].value;
	var accArr=aidListIds.split(",");
	for(var g=0;g<accArr.length;g++){
		tempActualTotalRevenue="0.00";
		if(document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].value!=''){
			tempActualTotalRevenue=document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].value;
		}			 	
		actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
		tempActualTotalExpense="0.00";
		if(document.forms['pricingListForm'].elements['actualExpense'+accArr[g]].value!=''){
			tempActualTotalExpense=document.forms['pricingListForm'].elements['actualExpense'+accArr[g]].value;
		}
		actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
	}
	actualGrossMargin=actualTotalRevenue-actualTotalExpense;
	try{
		if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
			actualGrossMarginPercentage=0.00;
		}else{
			actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
		}
	}catch(e){
		actualGrossMarginPercentage=0.00;
	}
	<c:if test="${not empty serviceOrder.id}">
	document.forms['pricingListForm'].elements['projectedActualRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*100)/100;
	document.forms['pricingListForm'].elements['projectedActualExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*100)/100;		
	</c:if> */
	actualTotalRevenue=0.00;
	actualTotalExpense=0.00;
	tempActualTotalExpense=0.00;
	tempActualTotalRevenue=0.00;
	actualGrossMargin=0.00;
	actualGrossMarginPercentage=0.00;
	var tempRecInvoiceNumber='';
	var tempPayingStatus=''; 		 
	for(var g=0;g<accArr.length;g++){
		tempRecInvoiceNumber=document.forms['pricingListForm'].elements['recInvoiceNumber'+accArr[g]].value; 
		if(tempRecInvoiceNumber!=undefined && tempRecInvoiceNumber!=null && tempRecInvoiceNumber.trim()!=''){
			if(document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].value!=""){
				tempActualTotalRevenue=document.forms['pricingListForm'].elements['actualRevenue'+accArr[g]].value;
			}
			actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
		}
		tempPayingStatus=document.forms['pricingListForm'].elements['payingStatus'+accArr[g]].value;
		if(tempPayingStatus!=undefined && tempPayingStatus!=null && tempPayingStatus.trim()!='' &&( tempPayingStatus.trim()=='A' || tempPayingStatus.trim()=='I')){
			if(document.forms['pricingListForm'].elements['actualExpense'+accArr[g]].value!=""){
				tempActualTotalExpense=document.forms['pricingListForm'].elements['actualExpense'+accArr[g]].value;
			}
			actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
		} 	
	}
	actualGrossMargin=actualTotalRevenue-actualTotalExpense;
	try{
		if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
			actualGrossMarginPercentage=0.00;
		}else{
			actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
		}
	}catch(e){
		actualGrossMarginPercentage=0.00;
	}		
	<c:if test="${not empty serviceOrder.id}">
	document.forms['pricingListForm'].elements['actualTotalRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*100)/100;
	document.forms['pricingListForm'].elements['actualTotalExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*100)/100;
	document.forms['pricingListForm'].elements['actualGrossMargin'+${serviceOrder.id}].value=Math.round(actualGrossMargin*100)/100;
	document.forms['pricingListForm'].elements['actualGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(actualGrossMarginPercentage*100)/100;
	</c:if>
	<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	calculateVatSection('REC',aid,type1);
	</c:if>
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	calculateVatSection('PAY',aid,type1);
	</c:if>
	</c:if>
}
function actualBuyByChargeCodeFX(aid){
	var roundVal=0.00;
	var payableContractCurrency=document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value;		
	var payableContractExchangeRate=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value;
	var payableContractRateAmmount=document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value;
	var exchangeRate=document.forms['pricingListForm'].elements['exchangeRate'+aid].value;						
	var localAmount=document.forms['pricingListForm'].elements['localAmount'+aid].value;
	if(payableContractCurrency!=''){
		roundVal=(payableContractRateAmmount/payableContractExchangeRate);
		document.forms['pricingListForm'].elements['actualExpense'+aid].value=roundVal
		if(exchangeRate!=''){
			roundVal=roundVal*exchangeRate;
			document.forms['pricingListForm'].elements['localAmount'+aid].value=roundVal;
		}
	}
}
function actualSellByChargeCodeFX(aid){
	var roundVal=0.00;
	var contractCurrency=document.forms['pricingListForm'].elements['contractCurrency'+aid].value;		
	var contractExchangeRate=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value;
	var contractRate=document.forms['pricingListForm'].elements['contractRate'+aid].value;
	var contractRateAmmount=document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value;
	var recRateCurrency=document.forms['pricingListForm'].elements['recRateCurrency'+aid].value;		
	var recRateExchange=document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
	var recCurrencyRate=document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value;
	var actualRevenueForeign=document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value;

	if(contractCurrency!=''){
		roundVal=contractRate/contractExchangeRate;
		document.forms['pricingListForm'].elements['recRate'+aid].value=roundVal;
		if(recRateCurrency!=''){
			document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value=roundVal*recRateExchange;
		}
		roundVal=contractRateAmmount/contractExchangeRate;
		document.forms['pricingListForm'].elements['actualRevenue'+aid].value=roundVal;
		if(recRateCurrency!=''){
			document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value=roundVal*recRateExchange;
		}}}
function vatDescrEnable(){
	var aidListIds = document.getElementById("accId").value;
	if(aidListIds!=''){
		var accArr=aidListIds.split(",");
		for(var g=0;g<accArr.length;g++){
			var vatExclude=document.forms['pricingListForm'].elements['VATExclude'+accArr[g]].value;
			try{
				if(vatExclude=='true' || vatExclude==true){
					document.forms['pricingListForm'].elements['payVatDescr'+accArr[g]].value="";
					document.forms['pricingListForm'].elements['recVatDescr'+accArr[g]].value="";							  							 
					document.forms['pricingListForm'].elements['payVatDescr'+accArr[g]].disabled=true;
					document.forms['pricingListForm'].elements['recVatDescr'+accArr[g]].disabled=true;
					<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					document.forms['pricingListForm'].elements['recVatAmt'+accArr[g]].value=0;
					document.forms['pricingListForm'].elements['recVatPercent'+accArr[g]].value=0;
					document.forms['pricingListForm'].elements['payVatAmt'+accArr[g]].value=0;
					document.forms['pricingListForm'].elements['payVatPercent'+accArr[g]].value=0;
					document.forms['pricingListForm'].elements['recVatAmtTemp'+accArr[g]].value=0;
					document.forms['pricingListForm'].elements['payVatAmtTemp'+accArr[g]].value=0;
	
					</c:if>
					document.forms['pricingListForm'].elements['vatAmt'+accArr[g]].value=0;			
					document.forms['pricingListForm'].elements['estExpVatAmt'+accArr[g]].value=0;			
					<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					document.forms['pricingListForm'].elements['revisionVatAmt'+accArr[g]].value=0;
					document.forms['pricingListForm'].elements['revisionExpVatAmt'+accArr[g]].value=0;			
					</c:if>
				}else{
					document.forms['pricingListForm'].elements['payVatDescr'+accArr[g]].disabled=false;
					document.forms['pricingListForm'].elements['recVatDescr'+accArr[g]].disabled=false;
				}
			}catch(e){}
		}
	}
}

function copyChargeDetails(chargecode,chargecodeId,autocompleteDivId,id,idCondition){
	document.getElementById(chargecodeId).value=chargecode;
	document.getElementById(autocompleteDivId).style.display = "none";
	document.getElementById('chargeCodeValidationVal').value='';
	//fillDiscription(id);
	checkChargeCode(id);
	updateContractChangeCharge(id,idCondition);
	findInternalCostVendorCode(id);
}
function closeMyChargeDiv(autocompleteDivId,chargecode,chargecodeId,id,idCondition){
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(chargecodeId).value==''){
		document.getElementById("quoteDescription"+id).value="";	
	}
	document.getElementById('chargeCodeValidationVal').value='';
	//fillDiscription(id);
	checkChargeCode(id);
	updateContractChangeCharge(id,idCondition);
}
function closeMyChargeDiv2(chargecodeId,id){
	if(document.getElementById(chargecodeId).value==''){
		document.getElementById("quoteDescription"+id).value="";	
	}
	document.getElementById('chargeCodeValidationVal').value='';
	document.forms['pricingListForm'].elements['chargeCodeValidationMessage'].value='NothingData';

}

function fillCurrencyByChargeCode(aid) {
	if(aid=='temp'){
		aid=accountLineIdScript;
		checkAccountInterface('vendorCode'+aid,'estimateVendorName'+aid,'actgCode'+aid);
	}
	fillPayVatByVendorCode(aid);			  		  	
	<c:if test="${contractType}"> 
	var vendorCode = document.forms['pricingListForm'].elements['vendorCode'+aid].value;  
	var url="fillCurrencyByChargeCode.html?ajax=1&decorator=simple&popup=true&vendorCode="+vendorCode;  
	http5513.open("GET", url, true); 
	http5513.onreadystatechange = function(){ handleHttpResponse4113(aid);};
	http5513.send(null); 
	</c:if> 
} 
function handleHttpResponse4113(aid) { 
	if (http5513.readyState == 4)  {
		var results = http5513.responseText
		results = results.trim(); 
		if(results!=""){
			var res = results.split("~");
			if(res[0].trim()!=''){

				document.forms['pricingListForm'].elements['estCurrencyNew'+aid].value=res[0];                   
				document.forms['pricingListForm'].elements['estExchangeRateNew'+aid].value=res[1];
				<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">          
				document.forms['pricingListForm'].elements['revisionCurrency'+aid].value=res[0];
				document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value=res[1];
				</c:if>
				<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}"> 
				document.forms['pricingListForm'].elements['country'+aid].value=res[0]; 
				document.forms['pricingListForm'].elements['exchangeRate'+aid].value=res[1];
				</c:if>
				var mydate=new Date();
				var daym;
				var year=mydate.getFullYear()
				var y=""+year;
				if (year < 1000)
					year+=1900
					var day=mydate.getDay()
					var month=mydate.getMonth()+1
					if(month == 1)month="Jan";
				if(month == 2)month="Feb";
				if(month == 3)month="Mar";
				if(month == 4)month="Apr";
				if(month == 5)month="May";
				if(month == 6)month="Jun";
				if(month == 7)month="Jul";
				if(month == 8)month="Aug";
				if(month == 9)month="Sep";
				if(month == 10)month="Oct";
				if(month == 11)month="Nov";
				if(month == 12)month="Dec";
				var daym=mydate.getDate()
				if (daym<10)
					daym="0"+daym
					var datam = daym+"-"+month+"-"+y.substring(2,4);
				document.forms['pricingListForm'].elements['estValueDateNew'+aid].value=datam;
				<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">         
				document.forms['pricingListForm'].elements['revisionValueDate'+aid].value=datam;
				</c:if>
				<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}"> 
				document.forms['pricingListForm'].elements['valueDate'+aid].value=datam;
				</c:if>
			}
			<c:if test="${contractType}">

			try{
				var buyRate1=document.forms['pricingListForm'].elements['estimateRate'+aid].value;
				var estCurrency1=document.forms['pricingListForm'].elements['estCurrencyNew'+aid].value;
				var basisValue=document.forms['pricingListForm'].elements['basis'+aid].value;
				var quantityValue1=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
				if(quantityValue1=='' ||quantityValue1=='0' ||quantityValue1=='0.0' ||quantityValue1=='0.00') {   
					document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
				}
				try{
					var estimateSellQuantity=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
					if(estimateSellQuantity=='' ||estimateSellQuantity=='0' ||estimateSellQuantity=='0.0' ||estimateSellQuantity=='0.00') {   
						document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
					}
				}catch(e){}	  		      
				var quantityValue=0;
				quantityValue=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
				if(estCurrency1.trim()!=""){

					try{
						var val1=0.00;
						val1=document.forms['pricingListForm'].elements['estExchangeRateNew'+aid].value;
						var val2=0.00;
						val2=document.forms['pricingListForm'].elements['estimatePayableContractRateNew'+aid].value;
						var val3=0.00;
						val3=document.forms['pricingListForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
						if((val2!='')&&(val2!='0.00')&&(val2!='0.0')&&(val2!='0')){
							var recCurrencyRate1=(val1*val2)/val3;
							var roundValue=Math.round(recCurrencyRate1*100)/100;
							document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value=roundValue;
						}else{
							var Q1=0;
							var Q2=0;
							Q1=buyRate1;
							Q2=document.forms['pricingListForm'].elements['estExchangeRateNew'+aid].value;
							var E1=Q1*Q2;
							E1=Math.round(E1*100)/100;
							document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value=E1;
						}			  			          			        
					}catch(e){ 
					}
					var E1=document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value;
					var estLocalAmt1=0;
					if(basisValue=="cwt" || basisValue== "%age"){
						estLocalAmt1=(quantityValue*E1)/100 ;
					}
					else if(basisValue=="per 1000"){
						estLocalAmt1=(quantityValue*E1)/1000 ;
					}else{
						estLocalAmt1=(quantityValue*E1);
					}	 
					estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
					document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value=estLocalAmt1;     
				}
			}catch(e){}

			</c:if>
		}
	} 

}
function fillPayVatByVendorCode(aid) {
	fillDefaultInvoiceNumber(aid);
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	var vatDesc=document.forms['pricingListForm'].elements['payVatDescr'+aid].value;
	if(document.forms['pricingListForm'].elements['payPostDate'+aid].value=='' && document.forms['pricingListForm'].elements['payAccDate'+aid].value=='') {
		if(document.forms['pricingListForm'].elements['VATExclude'+aid].value!='true'){
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.originAgentCode}'){
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.originAgentVatCode}';
			}
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.destinationAgentCode}'){
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.destinationAgentVatCode}';
			}
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.originSubAgentCode}'){
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.originSubAgentVatCode}';
			} 
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.destinationSubAgentCode}'){
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.destinationSubAgentVatCode}';
			}
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.forwarderCode}'){
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.forwarderVatCode}';
			}
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.brokerCode}'){
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.brokerVatCode}';
			}
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.networkPartnerCode}'){
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.networkPartnerVatCode}';
			}
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${serviceOrder.bookingAgentCode}'){
				try{
					document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.bookingAgentVatCode}';
				}catch(e){}
			}
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${billing.vendorCode}'){
				try{
					document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.vendorCodeVatCode}';
				}catch(e){}
			}
			if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${billing.vendorCode1}'){
				try{
					document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.vendorCodeVatCode1}';
				}catch(e){}
			}
			<c:if test="${empty accountLine.id}">	
			var syspayable='${systemDefault.payableVat}';
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value =='' && syspayable!=''){
				try{
					document.forms['pricingListForm'].elements['payVatDescr'+aid].value=syspayable;
				}catch(e){}
			}
			</c:if>
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value ==''){
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value=document.forms['pricingListForm'].elements['payVatDescr'+aid].value;
			}	
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value ==''){
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value=vatDesc;
			}						        
			getPayPercent(aid);
		} 
	}
	</c:if>
}
function getPayPercent(aid){ 
	<c:if test="${systemDefaultVatCalculation=='true'}">
	var payAccDate='';
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	payAccDate=document.forms['pricingListForm'].elements['payAccDate'+aid].value;
	</c:if>
	if (payAccDate!='') { 
		alert("You can not change the VAT as Sent To Acc has been already filled");
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value=document.forms['pricingListForm'].elements['payVatDescr'+aid].value;
	} else{
		var relo='';
		<c:forEach var="entry" items="${payVatPercentList}" > 
		if(relo==''){
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry.key}'){
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value='${entry.value}';
				calculateVatSectionAll('EXPENSE',aid,'BAS');
				relo="yes";
			}   }
		</c:forEach>
		<configByCorp:fieldVisibility componentId="accountLine.payVatGl">
		var relo1='';
		<c:forEach var="entry1" items="${payVatPayGLMap}" > 
		if(relo1 ==''){
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
				document.forms['pricingListForm'].elements['payVatGl'+aid].value='${entry1.value}'; 
				relo1 ="yes";
			}   }
		</c:forEach>
		relo1='';
		<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
		if(relo1 ==''){
			if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
				document.forms['pricingListForm'].elements['qstPayVatGl'+aid].value='${entry1.value}'; 
				relo1 ="yes";
			}   }
		</c:forEach>		 	     
		</configByCorp:fieldVisibility>

		if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value==''){
			document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
			calculateVatSectionAll('EXPENSE',aid,'BAS');
		}  }
	</c:if>
}	

function calculateRevenue(basis,quantity,sellRate,revenue,expense,estimatePassPercentage,displayOnQuots,estVatPer,estAmt,aid,field1,field2,field3) {
	if(field1!='' && field2!='' & field3!=''){
		rateOnActualizationDate(field1,field2,field3);
	}	   
	var revenueValue=0;
	var revenueRound=0;
	var oldRevenue=0;
	var oldRevenueSum=0;
	var balanceRevenue=0;
	try{
		<c:if test="${contractType}">
		var estimateSellQuantity=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
		if(estimateSellQuantity=='' ||estimateSellQuantity=='0' ||estimateSellQuantity=='0.0' ||estimateSellQuantity=='0.00') {
			document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
		}			 
		</c:if>
	}catch(e){}
	oldRevenue=eval(document.forms['pricingListForm'].elements[revenue].value);
	<c:if test="${not empty serviceOrder.id}">
	//oldRevenueSum=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	</c:if>
	var basisValue = document.forms['pricingListForm'].elements[basis].value;
	checkFloatNew('pricingListForm',quantity,'Nothing');
	var quantityValue = document.forms['pricingListForm'].elements[quantity].value;
	<c:if test="${contractType}"> 
	checkFloatNew('pricingListForm','estimateSellQuantity'+aid,'Nothing');
	quantityValue = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
	</c:if>
	checkFloatNew('pricingListForm',sellRate,'Nothing');
	var sellRateValue = document.forms['pricingListForm'].elements[sellRate].value;
	if(basisValue=="cwt" || basisValue== "%age"){
		revenueValue=(quantityValue*sellRateValue)/100 ;
	}
	else if(basisValue=="per 1000"){
		revenueValue=(quantityValue*sellRateValue)/1000 ;
	}else{
		revenueValue=(quantityValue*sellRateValue);
	} 
	var estimateDiscount=0.00;
	try{
		estimateDiscount=document.forms['pricingListForm'].elements['estimateDiscount'+aid].value;
	}catch(e){}
	if(estimateDiscount!=0.00){
		revenueValue=(revenueValue*estimateDiscount)/100;
	}	    
	revenueRound=Math.round(revenueValue*100)/100;
	//balanceRevenue=revenueRound-oldRevenue;
	//oldRevenueSum=Math.round(oldRevenueSum*100)/100;
	//balanceRevenue=Math.round(balanceRevenue*100)/100; 
	//oldRevenueSum=oldRevenueSum+balanceRevenue; 
	//oldRevenueSum=Math.round(oldRevenueSum*100)/100;
	var revenueRoundNew=""+revenueRound;
	if(revenueRoundNew.indexOf(".") == -1){
		revenueRoundNew=revenueRoundNew+".00";
	} 
	if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
		revenueRoundNew=revenueRoundNew+"0";
	}

	document.forms['pricingListForm'].elements[revenue].value=revenueRoundNew;
	try{
		var buyRate1=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
		var estCurrency1=document.forms['pricingListForm'].elements['estSellCurrencyNew'+aid].value;
		if(estCurrency1.trim()!=""){
			var Q1=0;
			var Q2=0;
			Q1=buyRate1;
			Q2=document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value;
			var E1=Q1*Q2;
			E1=Math.round(E1*100)/100;
			document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value=E1;
			var estLocalAmt1=0;
			if(basisValue=="cwt" || basisValue== "%age"){
				estLocalAmt1=(quantityValue*E1)/100 ;
			}
			else if(basisValue=="per 1000"){
				estLocalAmt1=(quantityValue*E1)/1000 ;
			}else{
				estLocalAmt1=(quantityValue*E1);
			}	 
			estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
			document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
		}
		<c:if test="${contractType}"> 
		try{
			var estimatePayableContractExchangeRate=document.forms['pricingListForm'].elements['estimateContractExchangeRateNew'+aid].value;
			var estLocalRate=document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value;
			var estExchangeRate=document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value
			var recCurrencyRate=(estimatePayableContractExchangeRate*estLocalRate)/estExchangeRate;
			var roundValue=Math.round(recCurrencyRate*100)/100;
			document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value=roundValue ;
			var Q11=0;
			var Q21=0;
			Q11=document.forms['pricingListForm'].elements[quantity].value;
			<c:if test="${contractType}"> 
			Q11 = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
			</c:if>
			Q21=document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value;
			var E11=Q11*Q21;
			var estLocalAmt11=0;
			if(basisValue=="cwt" || basisValue== "%age"){
				estLocalAmt11=E11/100 ;
			}
			else if(basisValue=="per 1000"){
				estLocalAmt11=E11/1000 ;
			}else{
				estLocalAmt11=E11;
			}	 
			estLocalAmt11=Math.round(estLocalAmt11*100)/100;    
			document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value=estLocalAmt11;
		}catch(e){}     
		</c:if>
	}catch(e){}
	/* <c:if test="${not empty serviceOrder.id}">
	var newRevenueSum=""+oldRevenueSum;
	if(newRevenueSum.indexOf(".") == -1){
		newRevenueSum=newRevenueSum+".00";
	} 
	if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
		newRevenueSum=newRevenueSum+"0";
	} 
	//document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
	</c:if>
	if(revenueRoundNew>0){
		//document.forms['pricingListForm'].elements[displayOnQuots].checked = true;
	} */
	try{
		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
		calculateVatAmt(estVatPer,revenue,estAmt);
		</c:if>
	}catch(e){}
	calculateestimatePassPercentage(revenue,expense,estimatePassPercentage);
}
function handleHttpResponse701(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage,deviation,estimateDeviation, estimateSellDeviation,aid) { 
	if (http88.readyState == 4)  {
		var results = http88.responseText;
		results = results.trim(); 
		<c:if test="${!contractType}">
		var res = results.split("#");
		var multquantity=res[14];
		if(res[17] == undefined){             
			document.forms['pricingListForm'].elements['estimateRate'+aid].value ="0.00";
		}else{                  
			document.forms['pricingListForm'].elements['estimateRate'+aid].value = Math.round(res[17]*100)/100 ;
		}
		if(res[3]=="AskUser1"){ 
			var reply = prompt("Please Enter Quantity", "");
			if(reply) {
				if((reply>0 || reply<9)|| reply=='.')  {
					document.forms['pricingListForm'].elements['estimateQuantity'+aid].value = Math.round(reply*100)/100;
					<c:if test="${contractType}">
					document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value= Math.round(reply*100)/100;
					</c:if>			    			            	   			  	            	
				}else {
					alert("Please Enter legitimate Quantity");
				} 
			}else{
				document.forms['pricingListForm'].elements['estimateQuantity'+aid].value = document.forms['pricingListForm'].elements[estimateQuantity].value;
				<c:if test="${contractType}">
				document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
				</c:if>			    			            	   			  	            			  	             	
			}
		}else{             
			if(res[1] == undefined){
				document.forms['pricingListForm'].elements['estimateQuantity'+aid].value ="0.00";
				<c:if test="${contractType}">
				document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value="0.00";
				</c:if>			    			            	   			  	            	

			}else{
				document.forms['pricingListForm'].elements['estimateQuantity'+aid].value = Math.round(res[1]*100)/100;
				<c:if test="${contractType}">
				document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value= Math.round(res[1]*100)/100;
				</c:if>			    			            	   			  	            	

			}  
		}
		if(res[5]=="AskUser"){ 
			var reply = prompt("Please Enter the value of Rate", "");
			if(reply)  {
				if((reply>0 || reply<9)|| reply=='.') {
					document.forms['pricingListForm'].elements['estimateSellRate'+aid].value = Math.round(reply*100)/100;
				} else {
					alert("Please Enter legitimate Rate");
				} 
			}else{
				document.forms['pricingListForm'].elements['estimateRate'+aid].value = document.forms['pricingListForm'].elements[estimateRate].value;
			} 
		}else{
			if(res[4] == undefined){
				document.forms['pricingListForm'].elements['estimateSellRate'+aid].value ="0.00";
			}else{
				document.forms['pricingListForm'].elements['estimateSellRate'+aid].value = Math.round(res[4]*100)/100 ;
			}  
		}
		if(res[5]=="BuildFormula"){
			if(res[6] == undefined){
				document.forms['pricingListForm'].elements['estimateSellRate'+aid].value ="0.00";
			}else{
				document.forms['pricingListForm'].elements['estimateSellRate'+aid].value = Math.round(res[6]*100)/100 ;
			}  
		}
		var Q1 = document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
		<c:if test="${contractType}">
		Q1=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
		</c:if>

		var Q2 = document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
		if(res[5]=="BuildFormula" && res[6] != undefined ){
			Q2 =res[6]
		}
		var oldRevenue = eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value);
		var oldExpense=eval(document.forms['pricingListForm'].elements['estimateExpense'+aid].value);
		var E1=Q1*Q2;
		var E2="";
		E2=Math.round(E1*100)/100;
		document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value = E2;
		var type = res[8];
		var typeConversion=res[10];   
		if(type == 'Division')   {
			if(typeConversion==0) 	 {
				E1=0*1;
				document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value = E1;
			} else {	
				E1=(Q1*Q2)/typeConversion;
				E2=Math.round(E1*100)/100;
				document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value = E2;
			} }
		if(type == ' ' || type == '') {
			E1=(Q1*Q2);
			E2=Math.round(E1*100)/100;
			document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value = E2;
		}
		if(type == 'Multiply')  {
			E1=(Q1*Q2*typeConversion);
			E2=Math.round(E1*100)/100;
			document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value = E2;
		}

		var Q11 = document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
		var Q22 = document.forms['pricingListForm'].elements['estimateRate'+aid].value;
		var E11=Q11*Q22;
		var E22="";
		E22=Math.round(E11*100)/100;
		document.forms['pricingListForm'].elements['estimateExpense'+aid].value = E22;
		if(type == 'Division')  {
			if(typeConversion==0)  {
				E11=0*1;
				document.forms['pricingListForm'].elements['estimateExpense'+aid].value = E11;
			} else  {	
				E11=(Q11*Q22)/typeConversion;
				E22=Math.round(E11*100)/100;
				document.forms['pricingListForm'].elements['estimateExpense'+aid].value = E22;
			}  }
		if(type == ' ' || type == '')  {
			E11=(Q11*Q22);
			E22=Math.round(E11*100)/100;
			document.forms['pricingListForm'].elements['estimateExpense'+aid].value = E22;
		}
		if(type == 'Multiply')  {
			E11=(Q11*Q22*typeConversion);
			E22=Math.round(E11*100)/100;
			document.forms['pricingListForm'].elements['estimateExpense'+aid].value = E22;
		} 

		document.forms['pricingListForm'].elements['basis'+aid].value=res[9];
		var chargedeviation=res[15];
		if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
			var buyDependSellAccount=res[11];
			var estimateRevenueAmt=0;
			var finalEstimateRevenueAmount=0; 
			var finalEstimateExpenceAmount=0;
			var estimateSellRates=0; 
			var sellDeviation=0;
			var estimatepasspercentages=0;
			var estimatepasspercentageRound=0;
			var buyDeviation=0;
			var oldEstDeviation =  document.forms['pricingListForm'].elements['estimateDeviation'+aid].value;
			var oldEstSellDeviation = document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
			sellDeviation=res[12];
			buyDeviation=res[13]; 
			document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value=sellDeviation;
			document.forms['pricingListForm'].elements['estimateDeviation'+aid].value=buyDeviation;

			document.forms['pricingListForm'].elements['deviation'+aid].value=chargedeviation; 
			document.forms['pricingListForm'].elements['buyDependSell'+aid].value=res[11];

			estimateRevenueAmt=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value; 
			if(sellDeviation==0 && sellDeviation==undefined){
				sellDeviation = document.forms['pricingListForm'].elements[oldEstSellDeviation].value;
			}

			finalEstimateRevenueAmount=Math.round((estimateRevenueAmt*(sellDeviation/100))*100)/100; 
			document.forms['pricingListForm'].elements[estimateRevenueAmount].value=finalEstimateRevenueAmount;
			if(buyDeviation==0 && buyDeviation==undefined){
				buyDeviation = document.forms['pricingListForm'].elements[oldEstDeviation].value;
			}

			if(buyDependSellAccount=="Y"){
				finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation/100))*100)/100;
			}else{
				finalEstimateExpenceAmount= Math.round((estimateRevenueAmt*(buyDeviation/100))*100)/100;
			}
			document.forms['pricingListForm'].elements[estimateExpense].value=finalEstimateExpenceAmount;
			estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
			estimatepasspercentageRound=Math.round(estimatepasspercentages);
			if(document.forms['pricingListForm'].elements['estimateExpense'+aid].value == '' || document.forms['pricingListForm'].elements['estimateExpense'+aid].value == 0){
				document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value='';
			}else{
				document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value=estimatepasspercentageRound;
			}
			// calculate estimate rate 
			var estimateQuantity1 =0;
			var estimateDeviation1=100;
			estimateQuantity1=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value; 
			var basis1 =document.forms['pricingListForm'].elements['basis'+aid].value; 
			var estimateExpense1 =eval(document.forms['pricingListForm'].elements['estimateExpense'+aid].value);
			//alert(estimateExpense1) 
			var estimateRate1=0;
			var estimateRateRound1=0; 
			if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
				estimateQuantity1 =document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
			} 
			if( basis1=="cwt" || basis1=="%age"){
				estimateRate1=(estimateExpense1/estimateQuantity1)*100;
				estimateRateRound1=Math.round(estimateRate1*100)/100;
				document.forms['pricingListForm'].elements['estimateRate'+aid].value=estimateRateRound1;	  	
			}  else if(basis1=="per 1000"){
				estimateRate1=(estimateExpense1/estimateQuantity1)*1000;
				estimateRateRound1=Math.round(estimateRate1*100)/100;
				document.forms['pricingListForm'].elements['estimateRate'+aid].value=estimateRateRound1;		  	
			} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
				estimateRate1=(estimateExpense1/estimateQuantity1); 
				estimateRateRound1=Math.round(estimateRate1*100)/100; 
				document.forms['pricingListForm'].elements['estimateRate'+aid].value=estimateRateRound1;		  	
			}

			estimateDeviation1=document.forms['pricingListForm'].elements['estimateDeviation'+aid].value 
			if(estimateDeviation1!='' && estimateDeviation1>0){
				estimateRate1=estimateRateRound1*100/estimateDeviation1;
				estimateRateRound1=Math.round(estimateRate1*100)/100;
				document.forms['pricingListForm'].elements['estimateRate'+aid].value=estimateRateRound1;		
			}

		}else {
			document.forms['pricingListForm'].elements['estimateDeviation'+aid].value=0.00;
			document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value=0.00;
			var bayRate=0;
			bayRate = res[17];
			if(bayRate!='' && bayRate!=0 && bayRate!=undefined){
				var estimateQuantityPay=0;
				estimateQuantityPay= document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
				Q1=estimateQuantityPay;
				Q2=bayRate; 
				if(type == 'Division')   {
					if(typeConversion==0) 	 {
						E1=0*1;
						document.forms['pricingListForm'].elements['estimateExpense'+aid].value = E1;
					} else {	
						E1=(Q1*Q2)/typeConversion;
						E2=Math.round(E1*100)/100;
						document.forms['pricingListForm'].elements['estimateExpense'+aid].value = E2;
					} }
				if(type == ' ' || type == '') {
					E1=(Q1*Q2);
					E2=Math.round(E1*100)/100;
					document.forms['pricingListForm'].elements['estimateExpense'+aid].value = E2;
				}
				if(type == 'Multiply')  {
					E1=(Q1*Q2*typeConversion);
					E2=Math.round(E1*100)/100;
					document.forms['pricingListForm'].elements['estimateExpense'+aid].value = E2;
				}
				document.forms['pricingListForm'].elements['estimateRate'+aid].value = Q2;
				var estimatepasspercentages=0;
				var estimatepasspercentageRound=0;
				var finalEstimateRevenueAmount=0; 
				var finalEstimateExpenceAmount=0;
				finalEstimateRevenueAmount=  document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
				finalEstimateExpenceAmount=document.forms['pricingListForm'].elements['estimateExpense'+aid].value;
				estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
				estimatepasspercentageRound=Math.round(estimatepasspercentages);
				if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
					document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value='';
				}else{
					document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value=estimatepasspercentageRound;
				} 
			}else{	           	   
				var quantity =0;
				var rate =0;
				var estimatepasspercentages=0;
				var estimatepasspercentageRound=0;
				var finalEstimateRevenueAmount=0; 
				var finalEstimateExpenceAmount=0;
				quantity= document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;	               
				rate=document.forms['pricingListForm'].elements['estimateRate'+aid].value;	               
				finalEstimateExpenceAmount = (quantity*rate); 
				if( document.forms['pricingListForm'].elements['basis'+aid].value=="cwt" || document.forms['pricingListForm'].elements['basis'+aid].value=="%age")  {
					finalEstimateExpenceAmount = finalEstimateExpenceAmount/100; 
				}
				if( document.forms['pricingListForm'].elements['basis'+aid].value=="per 1000")  {
					finalEstimateExpenceAmount = finalEstimateExpenceAmount/1000; 
				}
				finalEstimateExpenceAmount= Math.round((finalEstimateExpenceAmount)*100)/100;
				finalEstimateRevenueAmount=  document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
				estimatepasspercentages = (finalEstimateRevenueAmount/finalEstimateExpenceAmount)*100;
				estimatepasspercentageRound=Math.round(estimatepasspercentages);

				if(finalEstimateExpenceAmount == '' || finalEstimateExpenceAmount == 0.00 || finalEstimateExpenceAmount == 0){
					document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value='';
				}else{
					document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value=estimatepasspercentageRound;
				}
			}

		}

		//#7639 - Issue with Compute functionality in Account line and Pricing section start

		//#7639 - Issue with Compute functionality in Account line and Pricing section End			            
		try{
			var basisValue = document.forms['pricingListForm'].elements['basis'+aid].value;
			checkFloatNew('pricingListForm',estimateQuantity,'Nothing');
			var quantityValue = document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;

			var buyRate1=document.forms['pricingListForm'].elements['estimateRate'+aid].value;
			var estCurrency1=document.forms['pricingListForm'].elements['estCurrencyNew'+aid].value;
			if(estCurrency1.trim()!=""){
				var Q1=0;
				var Q2=0;
				var estimateExpenseQ1=0;
				Q1=buyRate1;
				Q2=document.forms['pricingListForm'].elements['estExchangeRateNew'+aid].value;
				var E1=Q1*Q2;
				E1=Math.round(E1*100)/100;
				document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value=E1;
				var estLocalAmt1=0;
				estimateExpenseQ1=document.forms['pricingListForm'].elements['estimateExpense'+aid].value	
				estLocalAmt1=estimateExpenseQ1*Q2; 
				estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
				document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value=estLocalAmt1;     
			}

			buyRate1=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
			estCurrency1=document.forms['pricingListForm'].elements['estSellCurrencyNew'+aid].value;
			if(estCurrency1.trim()!=""){
				var Q1=0;
				var Q2=0;
				var estimateRevenueAmountQ1=0;
				Q1=buyRate1;
				Q2=document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value;
				var E1=Q1*Q2;
				E1=Math.round(E1*100)/100;
				document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value=E1;
				var estLocalAmt1=0;
				estimateRevenueAmountQ1=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value
				estLocalAmt1=estimateRevenueAmountQ1*Q2;

				estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
				document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value=estLocalAmt1;     
			}	
			var type01 = res[8];
			var typeConversion01=res[10]; 					
			//#7298 - Copy esimates not calculating preset calculations in charges start
			//#7298 - Copy esimates not calculating preset calculations in charges End				
		}catch(e){}

		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
		try{
			var estVatP='vatPers'+aid;
			var revenueP='estimateRevenueAmount'+aid;
			var estVatA='vatAmt'+aid;

			calculateVatAmt(estVatP,revenueP,estVatA);
		}catch(e){}
		</c:if>            
		//--------------------Start--estimateRevenueAmount-------------------------//
		var revenueValue=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
		var revenueRound=0;
		var oldRevenueSum=0;
		var balanceRevenue=0;
		<c:if test="${not empty serviceOrder.id}">
		oldRevenueSum=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
		</c:if> 	  	          
		revenueRound=Math.round(revenueValue*100)/100;
		balanceRevenue=revenueRound-oldRevenue;
		oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		balanceRevenue=Math.round(balanceRevenue*100)/100; 
		oldRevenueSum=oldRevenueSum+balanceRevenue; 
		oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		var revenueRoundNew=""+revenueRound;
		if(revenueRoundNew.indexOf(".") == -1){
			revenueRoundNew=revenueRoundNew+".00";
		} 
		if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
			revenueRoundNew=revenueRoundNew+"0";
		}
		document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=revenueRoundNew;
		<c:if test="${not empty serviceOrder.id}">
		var newRevenueSum=""+oldRevenueSum;
		if(newRevenueSum.indexOf(".") == -1){
			newRevenueSum=newRevenueSum+".00";
		} 
		if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
			newRevenueSum=newRevenueSum+"0";
		} 
		document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
		</c:if>
		if(revenueRoundNew>0){
			/* document.forms['pricingListForm'].elements[displayOnQuote].checked = true; */
		}
		//--------------------End--estimateRevenueAmount-------------------------//

		//-------------------Start---estimateExpense-------------------------//                
		var expenseValue=document.forms['pricingListForm'].elements['estimateExpense'+aid].value;
		var expenseRound=0;
		var oldExpenseSum=0;
		var balanceExpense=0;
		<c:if test="${not empty serviceOrder.id}">
		//oldExpenseSum=eval(document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
		</c:if>
		expenseRound=Math.round(expenseValue*100)/100;
		//balanceExpense=expenseRound-oldExpense;
		//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		//balanceExpense=Math.round(balanceExpense*100)/100; 
		//oldExpenseSum=oldExpenseSum+balanceExpense;
		//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		var expenseRoundNew=""+expenseRound;
		if(expenseRoundNew.indexOf(".") == -1){
			expenseRoundNew=expenseRoundNew+".00";
		} 
		if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
			expenseRoundNew=expenseRoundNew+"0";
		}
		document.forms['pricingListForm'].elements['estimateExpense'+aid].value=expenseRoundNew;
		/* <c:if test="${not empty serviceOrder.id}">
		var newExpenseSum=""+oldExpenseSum;
		if(newExpenseSum.indexOf(".") == -1){
			newExpenseSum=newExpenseSum+".00";
		} 
		if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
			newExpenseSum=newExpenseSum+"0";
		} 
		document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
		</c:if>
		if(expenseRoundNew>0){
			document.forms['pricingListForm'].elements[displayOnQuote].checked = true;
		} */
		//-------------------End---estimateExpense-------------------------//
		//calculateGrossMargin();
		/* <c:if test="${not empty serviceOrder.id}">
		var revenueValue=0;
		var expenseValue=0;
		var grossMarginValue=0; 
		var grossMarginPercentageValue=0; 
		revenueValue=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
		expenseValue=eval(document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
		grossMarginValue=revenueValue-expenseValue;
		grossMarginValue=Math.round(grossMarginValue*100)/100; 
		document.forms['pricingListForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
		if(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
			document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
		}else{
			grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
			grossMarginPercentageValue=Math.round(grossMarginPercentageValue*100)/100; 
			document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
		}
		</c:if> */
		</c:if>      
		<c:if test="${contractType}">
		var res = results.split("#"); 
		<c:choose>
		<c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
		if(document.forms['pricingListForm'].elements['estimateContractCurrencyNew'+aid].value!=res[18]){
			document.forms['pricingListForm'].elements['estimateContractCurrencyNew'+aid].value=res[18]; 
			rateOnActualizationDate('estSellCurrencyNew'+aid+'~estimateContractCurrencyNew'+aid,'estSellValueDateNew'+aid+'~estimateContractValueDateNew'+aid,'estSellExchangeRateNew'+aid+'~estimateContractExchangeRateNew'+aid);
		}
		</c:when>
		<c:otherwise>

		document.forms['pricingListForm'].elements['estimateContractCurrencyNew'+aid].value=res[18];
		document.forms['pricingListForm'].elements['estimateContractValueDateNew'+aid].value=currentDateRateOnActualization();
		document.forms['pricingListForm'].elements['estimateContractExchangeRateNew'+aid].value=findExchangeRateRateOnActualization(res[18]);
		</c:otherwise>
		</c:choose>
		document.forms['pricingListForm'].elements['estimatePayableContractCurrencyNew'+aid].value=res[19];
		document.forms['pricingListForm'].elements['estimatePayableContractValueDateNew'+aid].value=currentDateRateOnActualization();
		document.forms['pricingListForm'].elements['estimatePayableContractExchangeRateNew'+aid].value=findExchangeRateRateOnActualization(res[19]);
		var multquantity=res[14]
		if(res[3]=="AskUser1"){ 
			var reply = prompt("Please Enter Quantity", "");
			if(reply) {
				if((reply>0 || reply<9)|| reply=='.')  {
					document.forms['pricingListForm'].elements['estimateQuantity'+aid].value = Math.round(reply*100)/100;
					<c:if test="${contractType}">
					document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value= Math.round(reply*100)/100;
					</c:if>	
				} else {
					alert("Please Enter legitimate Quantity");
				} }
			else{
				document.forms['pricingListForm'].elements['estimateQuantity'+aid].value =document.forms['pricingListForm'].elements['estimateQuantity'+aid].value ;
				<c:if test="${contractType}">
				document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value= document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
				</c:if>	
			}
		}else{             
			if(res[1] == undefined){
				document.forms['pricingListForm'].elements['estimateQuantity'+aid].value ="";
				<c:if test="${contractType}">
				document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value="";
				</c:if>
			}else{
				document.forms['pricingListForm'].elements['estimateQuantity'+aid].value = Math.round(res[1]*100)/100;
				<c:if test="${contractType}">
				document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value= Math.round(res[1]*100)/100;
				</c:if>
			}  }
		if(res[5]=="AskUser"){ 
			var reply = prompt("Please Enter the value of Rate", "");
			if(reply)  {
				if((reply>0 || reply<9)|| reply=='.') {
					document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value = Math.round(reply*100)/100;
				} else {
					alert("Please Enter legitimate Rate");
				} }
			else{
				document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value = document.forms['pricingListForm'].elements[estimateRate].value;
			} 
		}else{
			if(res[4] == undefined){
				document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value ="";
			}else{
				document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value =Math.round(res[4]*100)/100 ;
			}  }
		if(res[5]=="BuildFormula")   {
			if(res[6] == undefined){
				document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value ="";
			}else{
				document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value =Math.round(res[6]*100)/100 ;
			}  }
		var Q1 = document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
		var Q2 = document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value;
		if(res[5]=="BuildFormula" && res[6] != undefined){
			Q2 = res[6];
		}
		var oldRevenue = eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value);
		var oldExpense=eval(document.forms['pricingListForm'].elements['estimateExpense'+aid].value);		           
		var E1=Q1*Q2;
		var E2="";
		E2=Math.round(E1*100)/100;
		document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
		var type = res[8];
		var typeConversion=res[10];   
		if(type == 'Division')   {
			if(typeConversion==0) 	 {
				E1=0*1;
				document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value = E1;
			} else {	
				E1=(Q1*Q2)/typeConversion;
				E2=Math.round(E1*100)/100;
				document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
			} }
		if(type == ' ' || type == '' ) {
			E1=(Q1*Q2);
			E2=Math.round(E1*100)/100;
			document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
		}
		if(type == 'Multiply')  {
			E1=(Q1*Q2*typeConversion);
			E2=Math.round(E1*100)/100;
			document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value = E2;
		} 
		document.forms['pricingListForm'].elements['basis'+aid].value=res[9];
		var chargedeviation=res[15];
		document.forms['pricingListForm'].elements['deviation'+aid].value=chargedeviation;
		if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){ 
			document.forms['pricingListForm'].elements['buyDependSellNew'+aid].value=res[11];
			var buyDependSellAccount=res[11];
			var estimateRevenueAmount1=0;
			var finalEstimateRevenueAmount=0; 
			var finalEstimateExpenceAmount=0;
			var finalEstimateContractRateAmmount=0;
			var estimateContractRateAmmount1=0;
			var estimatePayableContractRateAmmount1=0;
			var estimateSellRate1=0; 
			var sellDeviation1=0;
			var  estimatepasspercentage=0
			var estimatepasspercentageRound=0;
			var buyDeviation1=0;
			sellDeviation1=res[12]
			buyDeviation1=res[13] 
			var estimateContractExchangeRate1=document.forms['pricingListForm'].elements['estimateContractExchangeRateNew'+aid].value
			estimateContractRateAmmount1 =document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value
			estimateRevenueAmount1=estimateContractRateAmmount1/estimateContractExchangeRate1;
			estimateRevenueAmount1=Math.round(estimateRevenueAmount1*100)/100;
			estimateRevenueAmount1=document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value;  
			finalEstimateRevenueAmount=Math.round((estimateRevenueAmount1*(sellDeviation1/100))*100)/100;
			finalEstimateContractRateAmmount=Math.round((estimateContractRateAmmount1*(sellDeviation1/100))*100)/100;
			document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value=finalEstimateContractRateAmmount; 
			if(buyDependSellAccount=="Y"){
				finalEstimateExpenceAmount= Math.round((finalEstimateRevenueAmount*(buyDeviation1/100))*100)/100;
			}else{
				finalEstimateExpenceAmount= Math.round((estimateRevenueAmount1*(buyDeviation1/100))*100)/100;
			}
			document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value=sellDeviation1;
			document.forms['pricingListForm'].elements['oldEstimateSellDeviationNew'+aid].value=sellDeviation1;
			document.forms['pricingListForm'].elements['estimateDeviation'+aid].value=buyDeviation1;
			document.forms['pricingListForm'].elements['oldEstimateDeviationNew'+aid].value=buyDeviation1;
			var estimatePayableContractExchangeRate=document.forms['pricingListForm'].elements['estimatePayableContractExchangeRateNew'+aid].value
			estimatePayableContractRateAmmount1=finalEstimateExpenceAmount*estimatePayableContractExchangeRate;
			estimatePayableContractRateAmmount1=Math.round(estimatePayableContractRateAmmount1*100)/100;
			document.forms['pricingListForm'].elements['estimatePayableContractRateAmmountNew'+aid].value=estimatePayableContractRateAmmount1;
			calEstimatePayableContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage,deviation,estimateDeviation, estimateSellDeviation,aid);
		}else {
			document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value=0;
			document.forms['pricingListForm'].elements['oldEstimateSellDeviationNew'+aid].value=0;
			document.forms['pricingListForm'].elements['estimateDeviation'+aid].value=0;
			document.forms['pricingListForm'].elements['oldEstimateDeviationNew'+aid].value=0;
			var bayRate=0
			bayRate = res[17] ;
			if(bayRate!=0){
				var estimateQuantityPay=0;
				estimateQuantityPay= document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
				Q1=estimateQuantityPay;
				Q2=bayRate; 
				if(type == 'Division')   {
					if(typeConversion==0) 	 {
						E1=0*1;
						document.forms['pricingListForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E1;
					} else {	
						E1=(Q1*Q2)/typeConversion;
						E2=Math.round(E1*100)/100;
						document.forms['pricingListForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E2;
					} }
				if(type == ' ' || type == '') {
					E1=(Q1*Q2);
					E2=Math.round(E1*100)/100;
					document.forms['pricingListForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E2;
				}
				if(type == 'Multiply')  {
					E1=(Q1*Q2*typeConversion);
					E2=Math.round(E1*100)/100;
					document.forms['pricingListForm'].elements['estimatePayableContractRateAmmountNew'+aid].value = E2;
				}
				document.forms['pricingListForm'].elements['estimatePayableContractRateNew'+aid].value =Math.round(Q2*100)/100; 
			} }
		//calculateEstimateSellRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid); 
		//calculateEstimateRateByContractRateNew(estimateExpense, estimateQuantity, estimateRate,basis,estimateSellRate,estimateRevenueAmount,estimatePassPercentage, displayOnQuote,deviation,estimateDeviation, estimateSellDeviation,aid);

		var estimatePayableContractRatecal= document.forms['pricingListForm'].elements['estimatePayableContractRateNew'+aid].value
		var estimatePayableContractRateAmmountcal= document.forms['pricingListForm'].elements['estimatePayableContractRateAmmountNew'+aid].value
		var  estimatePayableContractCurrencycal=document.forms['pricingListForm'].elements['estimatePayableContractCurrencyNew'+aid].value;			        
		if(estimatePayableContractCurrencycal.trim()!=""){
			var estimatePayableContractExchangeRatecal=document.forms['pricingListForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
			var estimateRatecal=estimatePayableContractRatecal/estimatePayableContractExchangeRatecal;
			estimateRatecal=Math.round(estimateRatecal*100)/100;
			var estimateExpensecal=estimatePayableContractRateAmmountcal/estimatePayableContractExchangeRatecal;
			estimateExpensecal=Math.round(estimateExpensecal*100)/100;
			document.forms['pricingListForm'].elements['estimateRate'+aid].value=estimateRatecal; 
			document.forms['pricingListForm'].elements['estimateExpense'+aid].value=estimateExpensecal; 
		} 
		var estimateRatecal= document.forms['pricingListForm'].elements['estimateRate'+aid].value;
		var estimateExpensecal= document.forms['pricingListForm'].elements['estimateExpense'+aid].value;
		var estCurrencycal=document.forms['pricingListForm'].elements['estCurrencyNew'+aid].value;
		if(estCurrencycal.trim()!=""){
			var estExchangeRatecal = document.forms['pricingListForm'].elements['estExchangeRateNew'+aid].value;
			var estLocalRatecal=estimateRatecal*estExchangeRatecal;
			estLocalRatecal=Math.round(estLocalRatecal*100)/100;
			var estLocalAmountcal=estimateExpensecal*estExchangeRatecal;
			estLocalAmountcal=Math.round(estLocalAmountcal*100)/100;
			document.forms['pricingListForm'].elements['estLocalRateNew'+aid].value=estLocalRatecal;
			document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value=estLocalAmountcal;
		}  
		var estimateContractRatecal= document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value
		var estimateContractRateAmmountcal= document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value
		var  estimateContractCurrencycal=document.forms['pricingListForm'].elements['estimateContractCurrencyNew'+aid].value;
		if(estimateContractCurrencycal.trim()!=""){
			var estimateContractExchangeRatecal=document.forms['pricingListForm'].elements['estimateContractExchangeRateNew'+aid].value;
			var estimateSellRatecal=estimateContractRatecal/estimateContractExchangeRatecal;
			estimateSellRatecal=Math.round(estimateSellRatecal*100)/100;
			var estimateRevenueAmountcal=estimateContractRateAmmountcal/estimateContractExchangeRatecal;
			estimateRevenueAmountcal=Math.round(estimateRevenueAmountcal*100)/100;
			document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=estimateSellRatecal;
			document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=estimateRevenueAmountcal;
		} 

		var estimateSellRatecal= document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
		var estimateRevenueAmountcal= document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
		var estSellCurrencycal=document.forms['pricingListForm'].elements['estSellCurrencyNew'+aid].value;
		if(estSellCurrencycal.trim()!=""){
			var estSellExchangeRatecal = document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value;
			var estSellLocalRatecal=estimateSellRatecal*estSellExchangeRatecal;
			estSellLocalRatecal=Math.round(estSellLocalRatecal*100)/100;
			var estSellLocalAmountcal=estimateRevenueAmountcal*estSellExchangeRatecal;
			estSellLocalAmountcal=Math.round(estSellLocalAmountcal*100)/100;
			document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value=estSellLocalRatecal;
			document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value=estSellLocalAmountcal;
		}

		var estimateRevenueAmountpasspercentages= document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value
		var estimateExpensepasspercentages= document.forms['pricingListForm'].elements['estimateExpense'+aid].value
		var estimatepasspercentagesTest = (estimateRevenueAmountpasspercentages/estimateExpensepasspercentages)*100;
		var estimatepasspercentageRoundTest=Math.round(estimatepasspercentagesTest);
		if(document.forms['pricingListForm'].elements['estimateExpense'+aid].value == '' || document.forms['pricingListForm'].elements['estimateExpense'+aid].value == 0){
			document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value='';
		}else{
			document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value=estimatepasspercentageRoundTest;
		}

		var type01 = res[8];
		var typeConversion01=res[10];  
		//#7298 - Copy esimates not calculating preset calculations in charges start

		//#7298 - Copy esimates not calculating preset calculations in charges End 
		//--------------------Start--estimateRevenueAmount-------------------------//
		var revenueValue=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
		var revenueRound=0;
		var oldRevenueSum=0;
		var balanceRevenue=0;
		<c:if test="${not empty serviceOrder.id}">
		oldRevenueSum=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
		</c:if> 	  	          
		revenueRound=Math.round(revenueValue*100)/100;
		balanceRevenue=revenueRound-oldRevenue;
		oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		balanceRevenue=Math.round(balanceRevenue*100)/100; 
		oldRevenueSum=oldRevenueSum+balanceRevenue; 
		oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		var revenueRoundNew=""+revenueRound;
		if(revenueRoundNew.indexOf(".") == -1){
			revenueRoundNew=revenueRoundNew+".00";
		} 
		if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
			revenueRoundNew=revenueRoundNew+"0";
		}
		document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=revenueRoundNew;

		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
		try{
			var estVatP='vatPers'+aid;
			var revenueP='estimateRevenueAmount'+aid;
			var estVatA='vatAmt'+aid;
			calculateVatAmt(estVatP,revenueP,estVatA);
		}catch(e){}
		</c:if>  
		<c:if test="${not empty serviceOrder.id}">
		var newRevenueSum=""+oldRevenueSum;
		if(newRevenueSum.indexOf(".") == -1){
			newRevenueSum=newRevenueSum+".00";
		} 
		if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
			newRevenueSum=newRevenueSum+"0";
		} 
		document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
		</c:if>
		if(revenueRoundNew>0){
			/* document.forms['pricingListForm'].elements[displayOnQuote].checked = true; */
		}
		//--------------------End--estimateRevenueAmount-------------------------//

		//-------------------Start---estimateExpense-------------------------//                
		var expenseValue=document.forms['pricingListForm'].elements['estimateExpense'+aid].value;
		var expenseRound=0;
		var oldExpenseSum=0;
		var balanceExpense=0;
		<c:if test="${not empty serviceOrder.id}">
		//oldExpenseSum=eval(document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
		</c:if>
		expenseRound=Math.round(expenseValue*100)/100;
		//balanceExpense=expenseRound-oldExpense;
		//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		//balanceExpense=Math.round(balanceExpense*100)/100; 
		//oldExpenseSum=oldExpenseSum+balanceExpense;
		//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		var expenseRoundNew=""+expenseRound;
		if(expenseRoundNew.indexOf(".") == -1){
			expenseRoundNew=expenseRoundNew+".00";
		} 
		if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
			expenseRoundNew=expenseRoundNew+"0";
		}
		document.forms['pricingListForm'].elements['estimateExpense'+aid].value=expenseRoundNew;
		/* <c:if test="${not empty serviceOrder.id}">
		var newExpenseSum=""+oldExpenseSum;
		if(newExpenseSum.indexOf(".") == -1){
			newExpenseSum=newExpenseSum+".00";
		} 
		if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
			newExpenseSum=newExpenseSum+"0";
		} 
		document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
		</c:if>
		if(expenseRoundNew>0){
			document.forms['pricingListForm'].elements[displayOnQuote].checked = true; 
		} */
		//-------------------End---estimateExpense-------------------------//
		//calculateGrossMargin();
		/* <c:if test="${not empty serviceOrder.id}">
		var revenueValue=0;
		var expenseValue=0;
		var grossMarginValue=0; 
		var grossMarginPercentageValue=0; 
		revenueValue=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
		expenseValue=eval(document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
		grossMarginValue=revenueValue-expenseValue;
		grossMarginValue=Math.round(grossMarginValue*100)/100; 
		document.forms['pricingListForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
		if(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
			document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
		}else{
			grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
			grossMarginPercentageValue=Math.round(grossMarginPercentageValue*100)/100; 
			document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
		}
		</c:if> */ 

		</c:if> 
	}  
}



function autoCompleterAjaxCallChargeCode(id,divid,idCondition){
	<configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
	document.getElementById('chargeCodeValidationVal').value="AutoPricingChargeCode";
	document.getElementById('chargeCodeValidationMessage').value=id;
	var ChargeName = document.getElementById('chargeCode'+id).value;
	var contract ="${billing.contract}";
	var idval='chargeCode'+id;
	contract=contract.trim();
	var category =  document.getElementById('category'+id).value;
	var accountCompanyDivision = document.forms['pricingListForm'].elements['companyDivision'+id].value;
	var originCountry='${serviceOrder.originCountryCode}';
	var destinationCountry='${serviceOrder.destinationCountryCode}';
	var mode;
	var costElementFlag='${costElementFlag}';
	var pageCondition="";
	if(category=='Internal Cost'){
		pageCondition='AccountInternalCategory';
	}
	if(category!='Internal Cost'){
		pageCondition='AccountNonInternalCategory';
	}
	if('${serviceOrder.mode}'!=undefined){
		mode='${serviceOrder.mode}'; 
	}else{
		mode="";
	}

	if(contract!=""){
		if(ChargeName.length>=3){
			$.get("ChargeCodeAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
				searchName:ChargeName,
				contract: contract,
				chargeCodeId:idval,
				autocompleteDivId: divid,
				id:id,
				idCondition:idCondition,
				searchcompanyDivision:accountCompanyDivision,
				originCountry:originCountry,
				destinationCountry:destinationCountry,
				serviceMode:mode,
				categoryCode:category,
				pageCondition:pageCondition,
				costElementFlag:costElementFlag
			}, function(idval) {
				document.getElementById(divid).style.display = "block";
				$("#" + divid).html(idval)
			})
		} else {
			document.getElementById(divid).style.display = "none"
		}
	}
	else{
		alert("There is No Billing Contract: Please select.");
		document.getElementById("contract"+id).focus();
	}
	</configByCorp:fieldVisibility>
}

function revisionSellFX(aid){
	var basis=document.forms['pricingListForm'].elements['basis'+aid].value;
	var baseRateVal=1;
	var roundVal=0.00;
	var revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
	if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		revisionQuantity = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	}
	var revisionSellQuantity=0.00;
	<c:if test="${contractType}">
	revisionSellQuantity=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
	if(revisionSellQuantity=='' ||revisionSellQuantity=='0' ||revisionSellQuantity=='0.0' ||revisionSellQuantity=='0.00') {
		revisionSellQuantity = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
	}		 
	</c:if> 
	if( basis=="cwt" || basis=="%age"){
		baseRateVal=100;
	}else if(basis=="per 1000"){
		baseRateVal=1000;
	} else {
		baseRateVal=1;  	
	}
	var revisionDiscount=0.00;
	try{
		revisionDiscount=document.forms['pricingListForm'].elements['revisionDiscount'+aid].value;
	}catch(e){}
	var revisionSellRate=0.00;
	revisionSellRate=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
	if(revisionSellRate=='') {
		revisionSellRate = document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=0.00;
	} 
	<c:if test="${contractType}">
	var revisionContractCurrency=document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value;		
	var revisionContractExchangeRate=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value;
	var revisionContractRate=document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
	var revisionContractRateAmmount=document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value;
	var revisionContractValueDate=document.forms['pricingListForm'].elements['revisionContractValueDate'+aid].value;
	if(revisionContractCurrency!=''){
		var ss=findExchangeRateGlobal(revisionContractCurrency);
		revisionContractExchangeRate=parseFloat(ss);
		document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value=revisionContractExchangeRate;
		document.forms['pricingListForm'].elements['revisionContractValueDate'+aid].value=currentDateGlobal();
		revisionContractRate=revisionSellRate*revisionContractExchangeRate;
		document.forms['pricingListForm'].elements['revisionContractRate'+aid].value=revisionContractRate;
		<c:if test="${contractType}">
		revisionContractRateAmmount=(revisionSellQuantity*revisionContractRate)/baseRateVal;
		</c:if>
		<c:if test="${!contractType}">
		revisionContractRateAmmount=(revisionQuantity*revisionContractRate)/baseRateVal;
		</c:if>
		if(revisionDiscount!=0.00){
			revisionContractRateAmmount=(revisionContractRateAmmount*revisionDiscount)/100;
		}
		document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value=revisionContractRateAmmount;	 		
	}
	</c:if>
	var revisionSellCurrency=document.forms['pricingListForm'].elements['revisionSellCurrency'+aid].value;		
	var revisionSellExchangeRate=document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;
	var revisionSellLocalRate=document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value;
	var revisionSellLocalAmount=document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value;
	var revisionSellValueDate=document.forms['pricingListForm'].elements['revisionSellValueDate'+aid].value;
	if(revisionSellCurrency!=''){
		var ss=findExchangeRateGlobal(revisionSellCurrency);
		revisionSellExchangeRate=parseFloat(ss);
		document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value=revisionSellExchangeRate;
		document.forms['pricingListForm'].elements['revisionSellValueDate'+aid].value=currentDateGlobal();
		revisionSellLocalRate=revisionSellRate*revisionSellExchangeRate;
		document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=revisionSellLocalRate;
		<c:if test="${contractType}">
		revisionSellLocalAmount=(revisionSellQuantity*revisionSellLocalRate)/baseRateVal;
		</c:if>
		<c:if test="${!contractType}">
		revisionSellLocalAmount=(revisionQuantity*revisionSellLocalRate)/baseRateVal;
		</c:if>
		if(revisionDiscount!=0.00){
			revisionSellLocalAmount=(revisionSellLocalAmount*revisionDiscount)/100;
		}	 		
		document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=revisionSellLocalAmount;	 		
	}
}	

function calculateEstimateRate(){
	var accountlineId=document.forms['pricingListForm'].elements['accId'].value
	var accEstimateQuantity=document.forms['pricingListForm'].elements['accEstimateQuantity'].value
	var accEstimateRate=document.forms['pricingListForm'].elements['accEstimateRate'].value 
	var accEstimateExpense=document.forms['pricingListForm'].elements['accEstimateExpense'].value 
	var expenseValue=0;
	var expenseRound=0;
	var oldExpense=0;
	var oldExpenseSum=0;
	var balanceExpense=0;
	oldExpense=eval(document.forms['pricingListForm'].elements['estimateExpense'+accountlineId].value);
	<c:if test="${not empty serviceOrder.id}">
	//oldExpenseSum=eval(document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value);
	</c:if> 
	expenseValue=accEstimateExpense;
	expenseRound=Math.round(expenseValue*100)/100;
	//balanceExpense=expenseRound-oldExpense;
	//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
	//balanceExpense=Math.round(balanceExpense*100)/100; 
	//oldExpenseSum=oldExpenseSum+balanceExpense;
	//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
	var expenseRoundNew=""+expenseRound;
	if(expenseRoundNew.indexOf(".") == -1){
		expenseRoundNew=expenseRoundNew+".00";
	} 
	if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
		expenseRoundNew=expenseRoundNew+"0";
	} 
	document.forms['pricingListForm'].elements['estimateExpense'+accountlineId].value=expenseRoundNew;
	document.forms['pricingListForm'].elements['estimateRate'+accountlineId].value=accEstimateRate;
	estimateQuantity=document.forms['pricingListForm'].elements['estimateQuantity'+accountlineId].value;
	if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {   
		document.forms['pricingListForm'].elements['estimateQuantity'+accountlineId].value=accEstimateQuantity;
	}
	/* <c:if test="${not empty serviceOrder.id}">
	var newExpenseSum=""+oldExpenseSum;
	if(newExpenseSum.indexOf(".") == -1){
		newExpenseSum=newExpenseSum+".00";
	} 
	if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
		newExpenseSum=newExpenseSum+"0";
	} 
	document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
	</c:if>
	if(expenseRoundNew>0){
		document.forms['pricingListForm'].elements['displayOnQuote'+accountlineId].checked = true;
	} */
	try{
		<c:if test="${systemDefaultVatCalculationNew=='Y'}">
		calculateVatAmtSection('EST',accountlineId);
		</c:if>
	}catch(e){}

	var revenue='estimateRevenueAmount'+accountlineId;
	var expense='estimateExpense'+accountlineId;
	var estimatePassPercentage='estimatePassPercentage'+accountlineId;
	calculateestimatePassPercentage(revenue,expense,estimatePassPercentage); 
} 	
function setFieldValue(id,result){
	document.forms['pricingListForm'].elements[id].value = result;
	//document.getElementById(id).value = result;
}
function calculateVatAmtEstExpTemp(vatPer,vatAmt,aid){
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	var vatExclude=document.forms['pricingListForm'].elements['VATExclude'+aid].value;
	if(vatExclude==null || vatExclude==false || vatExclude=='false'){		 
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = false;

		var estVatAmt=0;
		if(document.forms['pricingListForm'].elements[vatPer]!=undefined && document.forms['pricingListForm'].elements[vatPer].value!=''){
			var estVatPercent=eval(document.forms['pricingListForm'].elements[vatPer].value);
			var estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['accEstimateExpense'].value);
			<c:if test="${contractType}">
			estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['estLocalAmountNew'+aid].value);
			</c:if>
			estVatAmt=(estimateRevenueAmount*estVatPercent)/100;

			estVatAmt=Math.round(estVatAmt*100)/100; 
			document.forms['pricingListForm'].elements[vatAmt].value=estVatAmt;
		}

	}else{
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value="";	  	  		  
		document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled = true;	
		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['payVatAmtTemp'+aid].value=0;

		</c:if>
		document.forms['pricingListForm'].elements['vatAmt'+aid].value=0;			
		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['revisionVatAmt'+aid].value=0;
		</c:if>
	}
	</c:if>
}
function changeStatus(){
	document.forms['pricingListForm'].elements['formStatus'].value='1';
}

function getAccountlineFieldNew(aid,basis,quantity,chargeCode,contract,vendorCode){
	accountLineBasis=document.forms['pricingListForm'].elements[basis].value;
	accountLineEstimateQuantity=document.forms['pricingListForm'].elements[quantity].value;
	<c:if test="${contractType}">
	accountLineEstimateQuantity=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
	</c:if>	  
	chargeCode=document.forms['pricingListForm'].elements[chargeCode].value;
	contract=document.forms['pricingListForm'].elements[contract].value;  
	vendorCode=document.forms['pricingListForm'].elements[vendorCode].value;  
	<c:if test="${contractType}">
	var estimateContractCurrencyTemp=document.forms['pricingListForm'].elements['estimateContractCurrencyNew'+aid].value; 
	var estimateContractValueDateTemp=document.forms['pricingListForm'].elements['estimateContractValueDateNew'+aid].value;
	var estimateContractExchangeRateTemp=document.forms['pricingListForm'].elements['estimateContractExchangeRateNew'+aid].value;
	var estimateContractRateTemp=document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value;
	var estimateContractRateAmmountTemp=document.forms['pricingListForm'].elements['estimateContractRateAmmountNew'+aid].value;
	</c:if>	  
	var estSellCurrencyTemp=document.forms['pricingListForm'].elements['estSellCurrencyNew'+aid].value; 
	var estSellValueDateTemp=document.forms['pricingListForm'].elements['estSellValueDateNew'+aid].value;
	var estSellExchangeRateTemp=document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value;
	var estSellLocalRateTemp=document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value;
	var estSellLocalAmountTemp=document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value;

	var estimateQuantityTemp=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
	var estimateSellRateTemp=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
	var estimateRevenueAmountTemp=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
	var basisTemp=document.forms['pricingListForm'].elements['basis'+aid].value;
	var estimateSellQuantity=1;
	<c:if test="${contractType}">
	estimateSellQuantity=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
	</c:if>

	<c:if test="${contractType}">
	openWindow('editSellEstimateCurrency.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estimateContractCurrencyTemp='+estimateContractCurrencyTemp+'&estimateContractValueDateTemp='+estimateContractValueDateTemp+'&estimateContractExchangeRateTemp='+estimateContractExchangeRateTemp+'&estimateContractRateTemp='+estimateContractRateTemp+'&estimateContractRateAmmountTemp='+estimateContractRateAmmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&estimateSellQuantity='+estimateSellQuantity+'&decorator=popup&popup=true',920,350);
	// openWindow('getAccountlineFieldNew.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estimateContractCurrencyTemp='+estimateContractCurrencyTemp+'&estimateContractValueDateTemp='+estimateContractValueDateTemp+'&estimateContractExchangeRateTemp='+estimateContractExchangeRateTemp+'&estimateContractRateTemp='+estimateContractRateTemp+'&estimateContractRateAmmountTemp='+estimateContractRateAmmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&estimateSellQuantity='+estimateSellQuantity+'&decorator=popup&popup=true',920,350);
	</c:if>
	<c:if test="${!contractType}">
	openWindow('editSellEstimateCurrency.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&estimateSellQuantity='+estimateSellQuantity+'&decorator=popup&popup=true',920,350);
	// openWindow('getAccountlineFieldNew.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&estimateSellQuantity='+estimateSellQuantity+'&decorator=popup&popup=true',920,350);
	</c:if>
}		

function calculateAccountingACToBillingCurrency(accLineList,currency,extRate,valueDate){
	if(accLineList.trim()!=""){
		var str=accLineList.split(",");
		for(i=0;i<str.length;i++){
			var oldRevenue=0;
			var oldRevenueSum=0;
			var balanceRevenue=0;
			var aid=str[i];
			oldRevenue=eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value); 
			document.forms['pricingListForm'].elements['estSellCurrencyNew'+aid].value=currency;
			document.forms['pricingListForm'].elements['estSellValueDateNew'+aid].value=valueDate;
			document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value=extRate;
			<c:if test="${contractType}">
			var extSellRateBelow=0;
			var extEstimateContractRateEXAbove=0;
			var extEstimateContractRateAbove=0;
			var estSellLocalRate1=0;
			var roundValue=0;
			var estimateQuantity1=0;
			extSellRateBelow=document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value;
			extEstimateContractRateEXAbove=document.forms['pricingListForm'].elements['estimateContractExchangeRateNew'+aid].value;
			extEstimateContractRateAbove=document.forms['pricingListForm'].elements['estimateContractRateNew'+aid].value;
			try{
				if((extEstimateContractRateAbove!='')&&(extEstimateContractRateAbove!='0.00')&&(extEstimateContractRateAbove!='0')&&(extEstimateContractRateAbove!='0.0')){
					estSellLocalRate1=(extSellRateBelow*extEstimateContractRateAbove)/extEstimateContractRateEXAbove; 
					roundValue=Math.round(estSellLocalRate1*100)/100;
					document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value=roundValue;
				}else{
					var recRateExcha1=document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value
					var recRat1=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value
					var recCurrencyR1=recRat1*recRateExcha1;
					var roundValue4=Math.round(recCurrencyR1*100)/100;
					document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value=roundValue4 ; 
				}
			}catch(e){
			}

			try{
				estSellLocalRate1=document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value;
				estimateQuantity1=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
				<c:if test="${contractType}">
				estimateQuantity1=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
				</c:if>
				var basis1 =document.forms['pricingListForm'].elements['basis'+aid].value; 
				var estimateSellLocalAmountRound=0;

				if(estimateQuantity1=='' ||estimateQuantity1=='0' ||estimateQuantity1=='0.0' ||estimateQuantity1=='0.00') {
					estimateQuantity1 =document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
					<c:if test="${contractType}">
					estimateQuantity1=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
					</c:if>
				} 
				if(basis1=="cwt" || basis1=="%age"){
					estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/100;
					estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*100)/100;
					document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;	  	
				}  else if(basis1=="per 1000"){
					estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1)/1000;
					estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*100)/100;
					document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
				} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
					estimateSellLocalAmount1=(estSellLocalRate1*estimateQuantity1); 
					estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount1*100)/100; 
					document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value=estimateSellLocalAmountRound;		  	
				}
				var estimateDeviation1="0";
				try{
					estimateDeviation1=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
				}catch(e){}
				if(estimateDeviation1!='' && estimateDeviation1>0){
					estimateSellLocalAmountRound=estimateSellLocalAmountRound*estimateDeviation1/100;
					estimateSellLocalAmountRound=Math.round(estimateSellLocalAmountRound*100)/100;
					document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value = estimateSellLocalAmountRound;	 
				} 
			}catch(e){
				document.forms['pricingListForm'].elements['estSellLocalAmountNew'+aid].value = '0.00';
			}
			</c:if>
			<c:if test="${!contractType}">
			var estSellLocalRate1=0;
			extSellRateBelow=document.forms['pricingListForm'].elements['estSellExchangeRateNew'+aid].value;
			estSellLocalRate1=document.forms['pricingListForm'].elements['estSellLocalRateNew'+aid].value;
			var estimateQuantity1=0;
			var roundValue=0; 
			try{
				if(extSellRateBelow==0) {
					document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=0*1;
				} else{
					var amount1=estSellLocalRate1/extSellRateBelow; 
					roundValue=Math.round(amount1*100)/100;
					document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=roundValue ; 
				}}catch(e){
					document.forms['pricingListForm'].elements['estimateSellRate'+aid].value='0.00' ;
				}
				var estSelRate=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
				estimateQuantity1=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value;
				var basis1 =document.forms['pricingListForm'].elements['basis'+aid].value;
				try{
					var amount1=estSelRate*estimateQuantity1;
					if(basis1=="cwt" || basis1=="%age"){
						amount1=(amount1)/100;
					}  else if(basis1=="per 1000"){
						amount1=(amount1)/1000;
					} else if(basis1!="cwt" && basis1!="%age" && basis1!="per 1000") {
						amount1=amount1;
					}
					roundValue=Math.round(amount1*100)/100;
					document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue ; 
				}catch(e){
					document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value='0.00' ;
				}
				try{
					var estimateRevenue=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
					var estimate=document.forms['pricingListForm'].elements['estimateExpense'+aid].value;
					if(estimate==null || estimate== undefined || estimate.trim()=='' || parseInt(estimate.trim())==0){
						document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value='0';
					}else{
						var estimatepasspercentage1 = (estimateRevenue/estimate)*100;
						roundValue=Math.round(estimatepasspercentage1);
						document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value=roundValue;
					}
				}catch(e){
					document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value='0';
				}
				</c:if>


				/* <c:if test="${not empty serviceOrder.id}">
				oldRevenueSum=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue${serviceOrder.id}'].value);


				var revenueValue=0;
				revenueValue=eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value); 
				var revenueRound=0;
				revenueRound=Math.round(revenueValue*100)/100;
				balanceRevenue=revenueRound-oldRevenue;
				oldRevenueSum=Math.round(oldRevenueSum*100)/100;
				balanceRevenue=Math.round(balanceRevenue*100)/100; 
				oldRevenueSum=oldRevenueSum+balanceRevenue; 
				oldRevenueSum=Math.round(oldRevenueSum*100)/100;

				var newRevenueSum=""+oldRevenueSum; 
				if(newRevenueSum.indexOf(".") == -1){
					newRevenueSum=newRevenueSum+".00";
				} 
				if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
					newRevenueSum=newRevenueSum+"0";
				}  
				document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
				var revenueValuesum=0;
				var expenseValuesum=0;
				var grossMarginValuesum=0; 
				var grossMarginPercentageValuesum=0; 
				revenueValuesum=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
				expenseValuesum=eval(document.forms['pricingListForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value); 
				grossMarginValuesum=revenueValuesum-expenseValuesum;
				grossMarginValuesum=Math.round(grossMarginValuesum*100)/100; 
				document.forms['pricingListForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=grossMarginValuesum; 
				if(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0 || document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value == 0.00){
					document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value='';
				}else{
					grossMarginPercentageValuesum=(grossMarginValuesum*100)/revenueValuesum; 
					grossMarginPercentageValuesum=Math.round(grossMarginPercentageValuesum*100)/100; 
					document.forms['pricingListForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValuesum;
				}
				</c:if> */  
				try{
					<c:if test="${systemDefaultVatCalculationNew=='Y'}">
					<c:if test="${!contractType}">
					calculateVatAmt('vatPers'+aid,'estimateRevenueAmount'+aid,'vatAmt'+aid);
					</c:if>
					<c:if test="${contractType}">
					calculateVatAmt('vatPers'+aid,'estSellLocalAmountNew'+aid,'vatAmt'+aid);
					</c:if>
					</c:if>
				}catch(e){}
		}
	}
}	

function calculateEstimateSellRate(){

	var accountlineId=document.forms['pricingListForm'].elements['accId'].value;
	var accEstimateQuantity=document.forms['pricingListForm'].elements['accEstimateQuantity'].value;
	<c:if test="${contractType}">
	accEstimateQuantity=document.forms['pricingListForm'].elements['estimateSellQuantity'+accountlineId].value;
	</c:if>
	var accEstimateSellRate=document.forms['pricingListForm'].elements['accEstimateSellRate'].value ;
	var accEstimateRevenueAmount=document.forms['pricingListForm'].elements['accEstimateRevenueAmount'].value ;
	var expenseValue=0;
	var expenseRound=0;
	var oldExpense=0;
	var oldExpenseSum=0;
	var balanceExpense=0;
	oldExpense=eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+accountlineId].value);
	<c:if test="${not empty serviceOrder.id}">
	//oldExpenseSum=eval(document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value);
	</c:if> 
	expenseValue=accEstimateRevenueAmount;
	expenseRound=Math.round(expenseValue*100)/100;
	//balanceExpense=expenseRound-oldExpense;
	//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
	//balanceExpense=Math.round(balanceExpense*100)/100; 
	//oldExpenseSum=oldExpenseSum+balanceExpense;
	//oldExpenseSum=Math.round(oldExpenseSum*100)/100;
	var expenseRoundNew=""+expenseRound;
	if(expenseRoundNew.indexOf(".") == -1){
		expenseRoundNew=expenseRoundNew+".00";
	} 
	if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
		expenseRoundNew=expenseRoundNew+"0";
	} 
	document.forms['pricingListForm'].elements['estimateRevenueAmount'+accountlineId].value=expenseRoundNew;
	document.forms['pricingListForm'].elements['estimateSellRate'+accountlineId].value=accEstimateSellRate;
	estimateQuantity=document.forms['pricingListForm'].elements['estimateQuantity'+accountlineId].value;
	<c:if test="${contractType}">
	estimateSellQuantity=document.forms['pricingListForm'].elements['estimateSellQuantity'+accountlineId].value;
	</c:if>
	if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {   
		document.forms['pricingListForm'].elements['estimateQuantity'+accountlineId].value=accEstimateQuantity;
	}
	<c:if test="${contractType}">
	if(estimateSellQuantity=='' ||estimateSellQuantity=='0' ||estimateSellQuantity=='0.0' ||estimateSellQuantity=='0.00') {   
		document.forms['pricingListForm'].elements['estimateSellQuantity'+accountlineId].value=accEstimateQuantity;
	}	     
	</c:if>
	/* <c:if test="${not empty serviceOrder.id}">
	var newExpenseSum=""+oldExpenseSum;
	if(newExpenseSum.indexOf(".") == -1){
		newExpenseSum=newExpenseSum+".00";
	} 
	if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
		newExpenseSum=newExpenseSum+"0";
	} 
	document.forms['pricingListForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=newExpenseSum;
	</c:if>
	if(expenseRoundNew>0){
		document.forms['pricingListForm'].elements['displayOnQuote'+accountlineId].checked = true;
	} */
	var revenue='estimateRevenueAmount'+accountlineId;
	var expense='estimateExpense'+accountlineId;
	var estimatePassPercentage='estimatePassPercentage'+accountlineId;
	calculateestimatePassPercentage(revenue,expense,estimatePassPercentage); 

}

function calculateVatAmtEstTemp(vatPer,vatAmt,aid){
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
	var vatExclude=document.forms['pricingListForm'].elements['VATExclude'+aid].value;
	if(vatExclude==null || vatExclude==false || vatExclude=='false'){		 
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = false;

		var estVatAmt=0;
		if(document.forms['pricingListForm'].elements[vatPer]!=undefined && document.forms['pricingListForm'].elements[vatPer].value!=''){
			var estVatPercent=eval(document.forms['pricingListForm'].elements[vatPer].value);
			var estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['accEstimateRevenueAmount'].value);
			<c:if test="${contractType}">
			estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['accEstSellLocalAmount'].value);
			</c:if>
			estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
			estVatAmt=Math.round(estVatAmt*100)/100; 
			document.forms['pricingListForm'].elements[vatAmt].value=estVatAmt;
		}

	}else{		  
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";	  
		document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled = true;		  
		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatAmtTemp'+aid].value=0;


		</c:if>
		document.forms['pricingListForm'].elements['vatAmt'+aid].value=0;			
		<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		document.forms['pricingListForm'].elements['revisionVatAmt'+aid].value=0;
		</c:if>
	}
	</c:if>
}	
function getAccountlineFieldRevNew(aid,basis,quantity,chargeCode,contract,vendorCode){
	var basis=document.forms['pricingListForm'].elements[basis].value;
	var revisionQuantity=document.forms['pricingListForm'].elements[quantity].value;
	<c:if test="${contractType}">
	revisionQuantity=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
	</c:if>
	var chargeCode=document.forms['pricingListForm'].elements[chargeCode].value;
	var contract=document.forms['pricingListForm'].elements[contract].value;  
	var vendorCode=document.forms['pricingListForm'].elements[vendorCode].value;  
	var strValue="";
	strValue=basis+"~"+revisionQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
	var revisionSellCurrency=document.forms['pricingListForm'].elements['revisionSellCurrency'+aid].value; 
	var revisionSellValueDate=document.forms['pricingListForm'].elements['revisionSellValueDate'+aid].value;
	var revisionSellLocalRate=document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value;
	var revisionSellExchangeRate=document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;
	var revisionSellLocalAmount=document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value;
	strValue=strValue+"~"+revisionSellCurrency+"~"+revisionSellValueDate+"~"+revisionSellLocalRate+"~"+revisionSellExchangeRate+"~"+revisionSellLocalAmount;
	var revisionRevenueAmount=document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;
	var revisionDiscount=0.00;
	<c:if test="${chargeDiscountFlag}">
	revisionDiscount=document.forms['pricingListForm'].elements['revisionDiscount'+aid].value;
	</c:if>
	var revisionSellRate=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
	strValue=strValue+"~"+revisionRevenueAmount+"~"+revisionDiscount+"~"+revisionSellRate;
	<c:if test="${contractType}">
	var revisionContractCurrency=document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value; 
	var revisionContractValueDate=document.forms['pricingListForm'].elements['revisionContractValueDate'+aid].value;
	var revisionContractRate=document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
	var revisionContractExchangeRate=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value;
	var revisionContractRateAmmount=document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value;
	strValue=strValue+"~"+revisionContractCurrency+"~"+revisionContractValueDate+"~"+revisionContractRate+"~"+revisionContractExchangeRate+"~"+revisionContractRateAmmount;
	</c:if>	  
	//openWindow('getAccountlineFieldRevNew.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);	
	openWindow('editSellRevisionCurrency.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);
}	

function getAccountlineFieldRec(aid,basis,quantity,chargeCode,contract,vendorCode){
	var basis=document.forms['pricingListForm'].elements[basis].value;
	var recQuantity=document.forms['pricingListForm'].elements[quantity].value;
	var chargeCode=document.forms['pricingListForm'].elements[chargeCode].value;
	var contract=document.forms['pricingListForm'].elements[contract].value;  
	var vendorCode=document.forms['pricingListForm'].elements[vendorCode].value; 
	var recGl=document.forms['pricingListForm'].elements['recGl'+aid].value;
	var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value; 
	var type='REC';  
	var acref="${accountInterface}";
	acref=acref.trim();
	var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
	if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("Vendor Code Missing. Please select");
	}else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
		alert("Charge code needs to be selected");
	}	else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("Pay GL missing. Please select another charge code.");
	}	else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC'){
		alert("Receivable GL missing. Please select another charge code.");
	}else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
		alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
	}else{		   
		var strValue="";
		strValue=basis+"~"+recQuantity+"~"+chargeCode+"~"+contract+"~"+vendorCode;
		var recRateCurrency=document.forms['pricingListForm'].elements['recRateCurrency'+aid].value; 
		var racValueDate=document.forms['pricingListForm'].elements['racValueDate'+aid].value;
		var recCurrencyRate=document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value;
		var recRateExchange=document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
		var actualRevenueForeign=document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value;
		strValue=strValue+"~"+recRateCurrency+"~"+racValueDate+"~"+recCurrencyRate+"~"+recRateExchange+"~"+actualRevenueForeign;
		var actualRevenue=document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
		var actualDiscount=0.00;
		<c:if test="${chargeDiscountFlag}">
		actualDiscount=document.forms['pricingListForm'].elements['actualDiscount'+aid].value;
		</c:if>
		var recRate=document.forms['pricingListForm'].elements['recRate'+aid].value;
		strValue=strValue+"~"+actualRevenue+"~"+actualDiscount+"~"+recRate;
		<c:if test="${contractType}">
		var contractCurrency=document.forms['pricingListForm'].elements['contractCurrency'+aid].value; 
		var contractValueDate=document.forms['pricingListForm'].elements['contractValueDate'+aid].value;
		var contractRate=document.forms['pricingListForm'].elements['contractRate'+aid].value;
		var contractExchangeRate=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value;
		var contractRateAmmount=document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value;
		strValue=strValue+"~"+contractCurrency+"~"+contractValueDate+"~"+contractRate+"~"+contractExchangeRate+"~"+contractRateAmmount;
		</c:if>	  
		//openWindow('getAccountlineFieldRec.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);
		openWindow('editSellActualCurrency.html?ajax=1&aid='+aid+'&strValue='+strValue+'&decorator=popup&popup=true',920,350);
	}
}
function appendFieldValue(id,result){
	var temp=document.forms['pricingListForm'].elements[id].value;
	if(temp!=''){
		if(temp.indexOf(result.split("~")[0])>-1){
			temp=removeFromList(temp,result.split("~")[0]);
			if(temp!=''){
				temp=temp+","+result;
			}else{
				temp=result;
			}
		}else{
			temp=temp+","+result;
		}
	}else{
		temp=result;
	}
	document.forms['pricingListForm'].elements[id].value = temp;
}	
function marketPlaceCode(vendorCode,category,aid,vendorName,estimateRate,contractType){					  
	var val = document.getElementById(category).value;
	var mode='${serviceOrder.mode}';
	var packingMode='${serviceOrder.packingMode}';
	var jobType='${serviceOrder.job}';
	var weight="";
	var volume="";
	var storageUnit='${weightType}';
	//var storageUnit='lbscft';
	if(storageUnit=='lbscft'){
		//weight='${miscellaneous.estimatedNetWeight}';
		//volume='${miscellaneous.netEstimateCubicFeet}';
		weight='${miscellaneous.estimateGrossWeight}';
		volume='${miscellaneous.estimateCubicFeet}';
	}else if(storageUnit=='kgscbm'){
		//weight='${miscellaneous.estimatedNetWeightKilo}';
		//volume='${miscellaneous.netEstimateCubicMtr}';
		weight='${miscellaneous.estimateGrossWeightKilo}';
		volume='${miscellaneous.estimateCubicMtr}';
	}
	var soid='<%=request.getParameter("id") %>';
	var countryCode="";
	var packService="";
	if(val=='Origin'){
		countryCode='${serviceOrder.originCountry}'
	}else if(val=='Destin'){
		countryCode='${serviceOrder.destinationCountry}'
	}
	var pricePointexpired='${pricePointexpired}';		 
	if(pricePointexpired=='true' || pricePointexpired==true){	
		if(jobType=='STO'){
			if(weight!='' && volume!=''){
				window.open('getMarketArea.html?packingMode='+packingMode+'&jobType='+jobType+'&vendorCodeId='+vendorCode+'&vendorNameId='+vendorName+'&estimateRateid='+estimateRate+'&marketCountryCode='+countryCode+'&packService='+val+'&pricingMode='+mode+'&pricingWeight='+weight+'&pricingVolume='+volume+'&pricingStorageUnit='+storageUnit+'&contractType='+contractType+'&aid='+aid+'&soid='+soid+'&decorator=popup&popup=true','myWindow','width=950,height=600,left=100');
			}else{
				alert("Please Enter the Following Data - Estimated Weight and Volume..."); 
			}
		}else{
			if(mode!='' && weight!='' && volume!='' ){
				if(mode=='Air'){	
					openWindow('getMarketArea.html?packingMode='+packingMode+'&jobType='+jobType+'&vendorCodeId='+vendorCode+'&vendorNameId='+vendorName+'&estimateRateid='+estimateRate+'&marketCountryCode='+countryCode+'&packService='+val+'&pricingMode='+mode+'&pricingWeight='+weight+'&pricingVolume='+volume+'&pricingStorageUnit='+storageUnit+'&contractType='+contractType+'&soid='+soid+'&aid='+aid+'&decorator=popup&popup=true','myWindow','width=950,height=600,left=100'); 
				}else if(mode=='Sea'){
					if(packingMode=='RORO' || packingMode=='GRP' || packingMode=='BKBLK' || packingMode=='LCL' || packingMode=='LVAN' || packingMode=='T/W' || packingMode=='LOOSE' || packingMode=='LVCO' || packingMode=='LVCNT' || packingMode=='LVCNTR'){
						packService="LCL";
					}
					if(packService!=''){
						openWindow('getMarketArea.html?packingMode='+packingMode+'&jobType='+jobType+'&vendorCodeId='+vendorCode+'&vendorNameId='+vendorName+'&estimateRateid='+estimateRate+'&marketCountryCode='+countryCode+'&packService='+val+'&pricingMode='+mode+'&pricingWeight='+weight+'&pricingVolume='+volume+'&pricingStorageUnit='+storageUnit+'&soid='+soid+'&contractType='+contractType+'&aid='+aid+'&decorator=popup&popup=true','myWindow','width=950,height=600,left=100');	
					}else{
						alert("The Selected Pack Mode is not Supported by Price Point!");
					}				  
				}else{
					alert("The Selected Mode is not supported by Price Point!");
				}	
			}else{
				alert("Please Enter the Following Data - Mode, Estimated Weight, Estimated Volume and Packing Mode ...");
			}
		}
	}else{
		alert("Your PricePoint subscription has expired. \n Please send email to support@redskymobility.com for renewal of the subscription.");  
	}		  
}		

function goPrev() {
	progressBarAutoSave('1');
	var soIdNum ='${serviceOrder.id}';
	var seqNm ='${serviceOrder.sequenceNumber}';
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	http5.open("GET", url, true); 
	http5.onreadystatechange = handleHttpResponseOtherShip; 
	http5.send(null); 
} 
function goNext() {
	progressBarAutoSave('1');
	var soIdNum ='${serviceOrder.id}';
	var seqNm ='${serviceOrder.sequenceNumber}';
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	http5.open("GET", url, true); 
	http5.onreadystatechange = handleHttpResponseOtherShip; 
	http5.send(null); 
} 
function handleHttpResponseOtherShip(){
	if (http5.readyState == 4) {
		var results = http5.responseText
		results = results.trim();
		location.href = 'pricingList.html?sid='+results;
	}   }     
function findCustomerOtherSO(position) {
	var sid='${customerFile.id}';
	var soIdNum='${serviceOrder.id}';
	var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
	ajax_showTooltip(url,position);	
} 
function goToUrl(id) {
	location.href = 'pricingList.html?sid='+id;
}


/*     # 9770 - Generating invoice and posting payables from the Pricing screen              Start                */

var httpVatExclude=getHTTPObject77(); 
var http999 = getHTTPObject77(); 			
var http666= getHTTPObject77(); 
var http8 = getHTTPObject77(); 
var http7 = getHTTPObject77(); 
var http3 = getHTTPObject77();
var http4 = getHTTPObject77();
var http3 = getHTTPObject77();
var http6 = getHTTPObject77();
var http33 = getHTTPObject77();
var httpCMMAgent = getHTTPObject77(); 	

function  findBookingAgent(temp) {
	var msg='' ;
	var validationForInv= '${oAdAWeightVolumeValidationForInv}';
	var subOaDaValidationForInv = '${subOADAValidationForInv}';
	var agentFlag='${ownBookingAgentFlag}';
	if(subOaDaValidationForInv == true || validationForInv == true){
		if(validationForInv == true){
			if('${trackingStatus.originAgentCode}'== null || '${trackingStatus.originAgentCode}'== ''){
				if(msg==''){
					msg = "Kindly fill Origin Agent" ;
				}else{
					msg=msg+", Origin Agent";
				}
			}
			if('${trackingStatus.destinationAgentCode}'== null || '${trackingStatus.destinationAgentCode}'== ''){
				if(msg==''){
					msg = "Kindly fill Destination Agent" ;
				}else{
					msg=msg+", Destination Agent";
				}
			}
			if('${miscellaneous.actualNetWeight}'== null || '${miscellaneous.actualNetWeight}'== ''){
				if(msg==''){
					msg = "Kindly fill Actual Net Weight";
				}else{
					msg=msg+", Actual Net Weight";
				}
			}
			if('${miscellaneous.netActualCubicFeet}'== null || '${miscellaneous.netActualCubicFeet}'== ''){
				if(msg==''){
					msg = "Kindly fill Net Actual CubicFeet";
				}else{
					msg=msg+", Net Actual CubicFeet";
				}
			}}
		if(subOaDaValidationForInv == true){
			if(agentFlag == true){
				if('${trackingStatus.originSubAgentCode}'== null || '${trackingStatus.originSubAgentCode}'== ''){
					if(msg==''){
						msg = "Kindly fill Sub Origin Agent" ;
					}else{
						msg=msg+", Sub Origin Agent";
					}
				}
				if('${trackingStatus.destinationSubAgentCode}'== null || '${trackingStatus.destinationSubAgentCode}'== ''){
					if(msg==''){
						msg = "Kindly fill Sub Destination Agent" ;
					}else{
						msg=msg+", Sub Destination Agent";
					}
				}
			}} 

		//if(subOaDaValidationForInv == true || validationForInv == true){
		if('${trackingStatus.originAgentCode}'== '' || '${trackingStatus.destinationAgentCode}'== '' || '${miscellaneous.actualNetWeight}'== '' || '${miscellaneous.netActualCubicFeet}'== '' || ((subOaDaValidationForInv == true) &&(agentFlag == true) && ('${trackingStatus.originSubAgentCode}'== '' || '${trackingStatus.destinationSubAgentCode}'== ''))){
			if('${trackingStatus.originAgentCode}'== '' || '${trackingStatus.destinationAgentCode}'== '' || '${miscellaneous.actualNetWeight}'== '' || '${miscellaneous.netActualCubicFeet}'== '')
			{
				var agree = confirm(msg+" details. To add those now click 'OK' otherwise click 'Cancel'.");
			}else{
				var agree = confirm(msg+" details. To add those now click 'OK' otherwise click 'Cancel' to continue invoicing.");
			}
			if(agree){		      			
				var url = "editTrackingStatus.html?id=${serviceOrder.id}";
				location.href = url;
				return false;
			}else{
				if('${trackingStatus.originAgentCode}'== '' || '${trackingStatus.destinationAgentCode}'== '' || '${miscellaneous.actualNetWeight}'== '' || '${miscellaneous.netActualCubicFeet}'== '')
				{
					return false;
				}
			}}
	} 
	if(temp=='postingDate'){
		<c:if test="${accountInterface=='Y'}">
		<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
		      	    <c:set var="closedCompanyDivision" value="Y" />
		          </configByCorp:fieldVisibility>
		            <c:if test="${closedCompanyDivision=='Y'}">
		              findAccountCompanyDivisionForPayPost();
		</c:if> 
	    <c:if test="${closedCompanyDivision!='Y'}">	
		var serviceOrderId = '${serviceOrder.id}';
		var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		http8.open("GET", url, true);
		http8.onreadystatechange = handleHttpResponse1488;
		http8.send(null); 
		</c:if> 
		</c:if>
		<c:if test="${accountInterface=='N'||accountInterface==''}">
		findPayableStatus();
		</c:if>
	}
	if(temp=='invoiceGen'){
		var CWMSCommission=true; 
		<configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
		CWMSCommission=false;
		<c:if test="${commissionJobName == 'Yes'}">
		<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
		<c:if test="${empty compDivForBookingAgentList}"> 
		findAccountCompanyDivision();
		</c:if>
		<c:if test="${not empty compDivForBookingAgentList}">
		if('${fn:length(serviceOrder.salesMan)}' == 0){
			alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
			return false;
		}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' == 0)&&('${fn:length(billing.personBilling)}' == 0))){                        			           	
			alert('Billing Person & Forwarder is not assigned to this order.');
			return false;		           		
		}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' != 0)&&('${fn:length(billing.personBilling)}' == 0))){                        			           	
			alert('Billing Person is not assigned to this order.');
			return false;		           		
		}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' == 0)&&('${fn:length(billing.personBilling)}' != 0))){                        			           	
			alert('Forwarder is not assigned to this order.');
			return false;		           		
		}else{
			findAccountCompanyDivision(); 
		}
		</c:if>
		</c:if>
		<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
		if('${fn:length(serviceOrder.salesMan)}' == 0){
			alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
			return false;
		}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' == 0)&&('${fn:length(billing.personBilling)}' == 0))){                        			           	
			alert('Billing Person & Forwarder is not assigned to this order.');
			return false;		           		
		}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' != 0)&&('${fn:length(billing.personBilling)}' == 0))){                        			           	
			alert('Billing Person is not assigned to this order.');
			return false;		           		
		}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' == 0)&&('${fn:length(billing.personBilling)}' != 0))){                        			           	
			alert('Forwarder is not assigned to this order.');
			return false;		           		
		}else{
			findAccountCompanyDivision(); 
		}
		</c:if>
		</c:if>
		<c:if test="${commissionJobName != 'Yes'}">
		findAccountCompanyDivision(); 
		</c:if>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.button.commission.automation">
		CWMSCommission=false;
		<c:if test="${commissionJobName == 'Yes'}">
		<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
		<c:if test="${empty compDivForBookingAgentList}"> 
		findAccountCompanyDivision();
		</c:if>
		<c:if test="${not empty compDivForBookingAgentList}">
		if('${fn:length(serviceOrder.salesMan)}' == 0){
			alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
			return false;
		}else{
			findAccountCompanyDivision(); 
		}
		</c:if>
		</c:if>
		<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
		if('${fn:length(serviceOrder.salesMan)}' == 0){
			alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
			return false;
		}else{
			findAccountCompanyDivision(); 
		}
		</c:if>
		</c:if>
		<c:if test="${commissionJobName != 'Yes'}">
		findAccountCompanyDivision(); 
		</c:if>
		</configByCorp:fieldVisibility>
		if(CWMSCommission){ 
			findAccountCompanyDivision(); 
		}
	}
}
 
       function  findAccountCompanyDivisionForPayPost() {
      	 progressBarAutoSave('1');
         var serviceOrderId = '${serviceOrder.id}';
         var url="findAccountCompanyDivision.html?ajax=1&companyDivisionPayPostFlag=Y&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
         http88.open("GET", url, true);
         http88.onreadystatechange = handleHttpResponseAccountCompanyDivisionForPayPost;
         http88.send(null);
       }
     function handleHttpResponseAccountCompanyDivisionForPayPost(){
      if (http88.readyState == 4)
           {
              var results = http88.responseText
              results = results.trim();
              results = results.replace('[','');
              results=results.replace(']','');   
              
              if(results.length>2)
              { 
              	var checkCloseCompanyDivision=false;
              	var closeDivision="";
                var res = results.split("@"); 
                for (i=0;i<res.length;i++){ 
              	  if(!checkCloseCompanyDivision){
              	  if(res[i] !='' && (('${closeCompanyDivision}').indexOf(res[i])>=0)){ 
              		  checkCloseCompanyDivision=true;
              		  closeDivision=res[i];
              	  }
                }
                }
                if(checkCloseCompanyDivision){
              	  alert("Payable cannot be posted to accounting as the company division "+closeDivision+" has been closed ");
              	  progressBarAutoSave('0');
                }else{
                   var serviceOrderId = '${serviceOrder.id}';
		           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		           http8.open("GET", url, true);
		           http8.onreadystatechange = handleHttpResponse1488;
		           http8.send(null); 
                }
                } else{
                	var serviceOrderId = '${serviceOrder.id}';
		           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		           http8.open("GET", url, true);
		           http8.onreadystatechange = handleHttpResponse1488;
		           http8.send(null); 
                }
     }
     }
function handleHttpResponse1488() {
	var bookingagentCode = '${serviceOrder.bookingAgentCode}';
	if (http8.readyState == 4)
	{
		var results = http8.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']','');  
		if(results=='0')
		{
			alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.")
		} else if(results!='0') {
			findPayableStatus();
		}
	}
}			 

try{
	  <configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
	  	<c:if test="${commissionJobName == 'Yes'}">
	    	<c:if test="${emptyList!=true}" >
	    	  document.forms['serviceForm1'].elements['buttonType'].value = getCookie('buttonType');
	  				if(document.forms['serviceForm1'].elements['buttonType'].value== "invoice"){
						//alert("Invoice Generated, click 'Commission' button to compute relevant commission.");
		 				calCommission(true);
		  				document.forms['serviceForm1'].elements['buttonType'].value="";
	 				 }
	  		  document.cookie = 'buttonType' + "=; expires=" +  new Date().toGMTString();
	  		</c:if>
	  	</c:if>
	  </configByCorp:fieldVisibility>
	  <configByCorp:fieldVisibility componentId="component.button.commission.automation">
		<c:if test="${commissionJobName == 'Yes'}">
	  	<c:if test="${emptyList!=true}" >
	  	  document.forms['serviceForm1'].elements['buttonType'].value = getCookie('buttonType');
					if(document.forms['serviceForm1'].elements['buttonType'].value== "invoice"){
						//alert("Invoice Generated, click 'Commission' button to compute relevant commission.");
		 				var obj = document.getElementById('autoCommission');
		 				autoCommissionCal(obj);
		  				document.forms['serviceForm1'].elements['buttonType'].value="";
					 }
			  document.cookie = 'buttonType' + "=; expires=" +  new Date().toGMTString();
			</c:if>
		</c:if>
	</configByCorp:fieldVisibility>
	}catch(e){
	}

function  findPayableStatus()
{ 
	var serviceOrderId = '${serviceOrder.id}';
	var url="payableStatusList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
	http7.open("GET", url, true);
	http7.onreadystatechange = handleHttpResponse148;
	http7.send(null); 

}
function handleHttpResponse148()
{
	if (http7.readyState == 4)
	{
		var results = http7.responseText
		results = results.trim();
		//alert(results);
		var res = results.split("#");  
		if(res[0]=='No')
		{ 
			if(res[1]=='0')
			{
				findPayPostDate();
			}
			else if(res[2]=='0')
			{
				findPayPostDate();
			}
			else
			{
				alert("The status is not approved ");
			}
		}
		else if(results=='Yes')
		{ 
			findPayPostDate();
		}
	}
}		        
function findPayPostDate()  {
	var serviceOrderId = '${serviceOrder.id}';
	var url="payPostDateList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId);
	http2.open("GET", url, true);
	http2.onreadystatechange = handleHttpResponse115;
	http2.send(null); 
}
function handleHttpResponse115() { 
	if (http2.readyState == 4) {
		var results = http2.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']','');  
		if(results.length>5)
		{ 
			findPayableInvoice(); 
		} else {
			alert("No Payable lines found for Posting"); 
			 progressBarAutoSave('0');
		}
	}
}
function findPayableInvoice() { 
	var serviceOrderId = '${serviceOrder.id}';
	var url="payableInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
	http3.open("GET", url, true);
	http3.onreadystatechange = handleHttpResponse126;
	http3.send(null); 
}

function handleHttpResponse126() { 
	if (http3.readyState == 4)
	{
		var results = http3.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']','');  
		if(results.length>5)
		{ 
			alert("There are payables with missing vendor invoice#");
		} else {
			findPayableVendor(); 
		}
	}
} 	
function findPayableVendor() { 
	var serviceOrderId = '${serviceOrder.id}';
	var url="payableVendorList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
	http4.open("GET", url, true);
	http4.onreadystatechange = handleHttpResponse136;
	http4.send(null); 
}

function handleHttpResponse136() { 
	if (http4.readyState == 4)
	{
		var results = http4.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']','');   
		if(results.length>2)
		{ 
			alert("The vendor code is missing");
		} else {
			findPayableCharge();
		}
	}
}
function findPayableCharge() 
{ 
	var serviceOrderId = '${serviceOrder.id}';
	var url="payableChargeList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
	http5.open("GET", url, true);
	http5.onreadystatechange = handleHttpResponse146;
	http5.send(null);

}  

function handleHttpResponse146()
{

	if (http5.readyState == 4)
	{
		var results = http5.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']','');  
		//alert(results)
		if(results.length>2)
		{ 
			alert("The Charge code is missing");
		}
		else
		{ 
			findPayableGl();  
		}
	}
} 	

function findPayableGl() 
{ 
	var serviceOrderId = '${serviceOrder.id}';
	var url="payableGLCodeList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
	http6.open("GET", url, true);
	http6.onreadystatechange = handleHttpResponse147;
	http6.send(null);

}   
function handleHttpResponse147()
{

	if (http6.readyState == 4)
	{
		var results = http6.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']','');   
		if(results.length>2)
		{ 
			alert("The GL code is missing in the 'Payable Detail' of 'Accounting Transfer Information' section");
		}
		else
		{ 
			var shipNumberForTickets='${serviceOrder.shipNumber}';
			var sid='${serviceOrder.id}';
			document.forms['pricingListForm'].elements['buttonType'].value = "invoice";
			if('${billing.billComplete}'!=''){
				var agree = confirm("The billing has been completed, do you want to still post accountlines?");
				if(agree)
				{
					openWindow('findPayPostDateForPricing.html?sid='+sid+'&decorator=popup&popup=true',900,300); 
				}else { 
				}
			}else{
				openWindow('findPayPostDateForPricing.html?sid='+sid+'&decorator=popup&popup=true',900,300); 
			}

		}
	}
}	

function  findAccountCompanyDivision() {
	progressBarAutoSave('1');
	var serviceOrderId = '${serviceOrder.id}';
	var url="findAccountCompanyDivision.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId)+"&billingCMMContractType=${billingCMMContractType}"; 
	http88.open("GET", url, true);
	http88.onreadystatechange = handleHttpResponseAccountCompanyDivision;
	http88.send(null);
}
function handleHttpResponseAccountCompanyDivision(){
	if (http88.readyState == 4)
	{
		var results = http88.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']','');   
		if(results.length>2)
		{ 
			var checkCloseCompanyDivision=false;
			var res = results.split("@"); 
        	<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
        	  var closeDivision="";
              
              for (i=0;i<res.length;i++){ 
        	  if(!checkCloseCompanyDivision){
        	  if(res[i] !='' && (('${closeCompanyDivision}').indexOf(res[i])>=0)){ 
        		  checkCloseCompanyDivision=true;
        		  closeDivision=res[i];
        	  }
              }
              }
            </configByCorp:fieldVisibility>
          if(checkCloseCompanyDivision){
        	  alert("Cannot generate invoice to accounting as the company division "+closeDivision+" has been closed ");
        	  progressBarAutoSave('0');
          }else{
			var res = results.split("@");  
			if(res.length == 2)
			{
				document.forms['pricingListForm'].elements['companyDivisionForTicket'].value=res[1];    
				<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
				var serviceOrderId = '${serviceOrder.id}';
				var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
				http8.open("GET", url, true);
				http8.onreadystatechange = handleHttpResponse1489;
				http8.send(null); 
				</c:if>
				<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
				var serviceOrderId = '${serviceOrder.id}';
				var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
				http8.open("GET", url, true);
				http8.onreadystatechange = handleHttpResponse1499;
				http8.send(null); 
				</c:if>
			}
			else{
				<c:if test="${singleCompanyDivisionInvoicing}">
				alert("Cannot generate invoice as multiple company divisions found for invoicing, please fix this.");
				progressBarAutoSave('0');
				</c:if>
				<c:if test="${!singleCompanyDivisionInvoicing}">	  
				var agree = confirm("The new invoice has different company divisions, do you want to still proceed and put these in the same invoice #?");
				if(agree)
				{
					<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
					var serviceOrderId = '${serviceOrder.id}';
					var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
					http8.open("GET", url, true);
					http8.onreadystatechange = handleHttpResponse1489;
					http8.send(null); 
					</c:if>
					<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
					var serviceOrderId = '${serviceOrder.id}';
					var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
					http8.open("GET", url, true);
					http8.onreadystatechange = handleHttpResponse1499;
					http8.send(null); 
					</c:if>
				}else { 
					progressBarAutoSave('0');
				}
				</c:if>
			}
          }
		} else{
			alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
			progressBarAutoSave('0');
		}
		
	}
}	 

function handleHttpResponse1489() {
	var bookingagentCode = '${serviceOrder.bookingAgentCode}';
	if (http8.readyState == 4)
	{
		var results = http8.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']',''); 
		if(results=='0')
		{
			alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.");
			progressBarAutoSave('0');
		}
		else if(results!='0')
		{
			<c:if test="${multiCurrency!='Y'}"> 
			findGenerateInvoice();
			</c:if> 
			<c:if test="${multiCurrency=='Y'}"> 
			findbillingCurrency();
			</c:if>
		}
	}
}
function handleHttpResponse1499() {
	var bookingagentCode = '${serviceOrder.bookingAgentCode}';
	if (http8.readyState == 4)
	{
		var results = http8.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']',''); 
		if(results=='0')
		{
			alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.");
			progressBarAutoSave('0');
		}
		else if(results!='0')
		{
			<c:if test="${multiCurrency!='Y'}"> 
			findDistAmtGenerateInvoice();
			</c:if> 
			<c:if test="${multiCurrency=='Y'}"> 
			findbillingCurrency();
			</c:if> 
		}
	}
}

function findbillingCurrency(){
	var sid = '${serviceOrder.id}';
	var url="findbillingCurrency.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
	http33.open("GET", url, true);
	http33.onreadystatechange = handleHttpResponseCurrency;
	http33.send(null);
}

function handleHttpResponseCurrency(){ 
	if (http33.readyState == 4)
	{
		var results = http33.responseText 
		results = results.trim();  
		if(results.length>2)
		{ 
			var res = results.split("@");
			if(res.length == 2)
			{ 
				<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
				findGenerateInvoice();
				</c:if>
				<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
				findDistAmtGenerateInvoice(); 
				</c:if> 
			} else {
				<c:choose>
				<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
				<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
				findGenerateInvoice();
				</c:if>
				<c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
				findDistAmtGenerateInvoice(); 
				</c:if> 
				</c:when>
				<c:otherwise>
				alert("Invoice cannot be generated as the Billing currency of all the account line(s) do not match.");
				progressBarAutoSave('0');
				</c:otherwise>
				</c:choose>
			}  
		} else  { 
			alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.');
			progressBarAutoSave('0');
		}
	}
}

function findGenerateInvoice(){
	var billToCode = '${serviceOrder.id}';
	var url="actualInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(billToCode);
	http3.open("GET", url, true);
	http3.onreadystatechange = handleHttpResponse141;
	http3.send(null);
}

function handleHttpResponse141() { 
	if (http3.readyState == 4)
	{
		var results = http3.responseText 
		results = results.trim();  

		if(results.length>2){
			<c:choose>
			<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
			findCMMAgentBilltoCode();
			</c:when>
			<c:otherwise>    
			var res = results.split("@"); 
			if(res.length == 2){  
				document.forms['pricingListForm'].elements['billToCodeForTicket'].value=res[1];
				findBookingAgentNameGen(res[1]); 
			} else {
				alert("Cannot generate invoice as Multiple Bill Tos found for invoicing, please fix this.");
				progressBarAutoSave('0');
			}
			</c:otherwise>
			</c:choose> 
		} else  { 
			alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
			progressBarAutoSave('0'); 
		}
	}
}

function findDistAmtGenerateInvoice(){ 
	var sid = '${serviceOrder.id}';
	var url="distAmtInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
	http3.open("GET", url, true);
	http3.onreadystatechange = handleHttpResponse1114;
	http3.send(null);
}

function handleHttpResponse1114()
{ 
	if (http3.readyState == 4)
	{
		var results = http3.responseText 
		results = results.trim();   
		if(results.length>2) {
			<c:choose>
			<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
			findCMMAgentBilltoCode();
			</c:when>
			<c:otherwise>  
			var res = results.split("@");
			if(res.length == 2)  {  
				document.forms['pricingListForm'].elements['billToCodeForTicket'].value=res[1]; 
				findDistAmtBookAgent(res[1]); 
			}  else  {
				alert("Cannot generate invoice as Multiple Bill Tos found for invoicing, please fix this.");
				progressBarAutoSave('0');
			}
			</c:otherwise>
			</c:choose>  
		}  else  { 
			alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.');
			progressBarAutoSave('0');
		}
	}
}		        			            
function findCMMAgentBilltoCode(){
	var sid = '${serviceOrder.id}';
	var url="findCMMAgentBilltoCode.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
	httpCMMAgent.open("GET", url, true);
	httpCMMAgent.onreadystatechange = CMMAgentBilltoCodeResponse;
	httpCMMAgent.send(null);
}      
function CMMAgentBilltoCodeResponse(){
	if (httpCMMAgent.readyState == 4)
	{
		var results = httpCMMAgent.responseText 
		results = results.trim(); 
		var res = results.split("#"); 
		if(res[0]!='0'){
			if(res[0] == '1'){
				if(res[1] == '1'){
					document.forms['pricingListForm'].elements['billToCodeForTicket'].value=res[2];
					findBookingAgentNameGen(res[2]); 
				}else{
					alert('Cannot generate invoice as Multiple Currency Found.');
					progressBarAutoSave('0');
				}	
			}else{
				alert('Cannot generate invoice as Multiple Bill to Found.');
				progressBarAutoSave('0');
			} 
		}else  { 
			alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.');
			progressBarAutoSave('0');
		}
	}
} 

function findBookingAgentNameGen(target){ 
	var billToCode=target; 
	var sid = '${serviceOrder.id}';
	var url="accountList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(target);
	http2.open("GET", url, true);
	http2.onreadystatechange = handleHttpResponse414;
	http2.send(null);
}
function handleHttpResponse414() { 
	if (http2.readyState == 4){
		var results = http2.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']','');     
		var res = results.split("@");  
		if(res[1].trim()==''){ 
			var shipNumberForTickets='${serviceOrder.shipNumber}';
			var sid='${serviceOrder.id}';
			if(res[4].trim()!=''){
				alert("Cannot Invoice, as Bill to Code is missing in line # "+res[4]); 
			}else{
				alert("Cannot Invoice, as Bill to Code is missing");
			} 	
			progressBarAutoSave('0');
		}
		else if(res[2].trim()==''){
			if(res[4].trim()!=''){
				alert("Cannot Invoice, as Charge Code is missing in line # "+res[4]); 
			}else{
				alert("Cannot Invoice, as Charge Code is missing");
			}
			progressBarAutoSave('0');
		}
		<c:if test="${accountInterface=='Y'}">
		else if(res[3].trim()==''){
			if(res[4].trim()!=''){
				alert("Cannot Invoice, as GL Code of Receivable Detail is missing in line # "+res[4]); 
			}else{
				alert("Cannot Invoice, as GL Code is missing in the 'Receivable Detail' of 'Accounting Transfer Information' section");
			}
			progressBarAutoSave('0');
		}
		</c:if>																													
		else{ 
			//findBillToAuthority();
			getBilltoCodeforGenerateInvoice();
		}
	}
}

function findDistAmtBookAgent(target){  
	var billToCode=target; 
	var sid = '${serviceOrder.id}';
	var url="distAmtAccountList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(target);
	http2.open("GET", url, true);
	http2.onreadystatechange =function(){ handleHttpResponse444412(target);} ;
	http2.send(null);
}
function handleHttpResponse444412(target)
{

	if (http2.readyState == 4)
	{
		var results = http2.responseText
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']','');  
		var res = results.split("@");  
		if(res[1].trim()=='')  { 
			var shipNumberForTickets='${serviceOrder.shipNumber}';
			var sid='${serviceOrder.id}';
			if(res[6].trim()!=''){
				alert("Cannot Invoice, as Bill to Code is missing in line # "+res[6]);  
			}else{
				alert("Cannot Invoice, as Bill to Code is missing"); 	
			}	
			progressBarAutoSave('0');
		} else if(res[2].trim()=='')
		{
			if(res[6].trim()!=''){
				alert("Cannot Invoice, as Charge Code is missing in line # "+res[6]);  
			}else{
				alert("Cannot Invoice, as Charge Code is missing");
			}
			progressBarAutoSave('0');
		}
		<c:if test="${accountInterface=='Y'}">
		else if(res[3].trim()=='')
		{
			if(res[6].trim()!=''){
				alert("Cannot Invoice, as GL Code of Receivable Detail is missing in line # "+res[6]);  
			}else{
				alert("Cannot Invoice, as GL Code is missing in the 'Receivable Detail' of 'Accounting Transfer Information' section");
			}
			progressBarAutoSave('0');
		} 
		</c:if>
		else if(res[4].trim()!=res[5].trim())
		{
			alert("Actual revenue total ("+ res[4]+") and distribution amount total ("+res[5]+") are not same. So, cannot generate invoice.");
			progressBarAutoSave('0');
		}
		else
		{
			//findBillToAuthority();
			checkZeroAmountForInvoice(target);
		}
	}
}
function findBillToAuthority(){ 
	var  jobtypeForTicket='${serviceOrder.job}';
	var  billToCodeForTicket=document.forms['pricingListForm'].elements['billToCodeForTicket'].value
	var shipNumberForTickets='${serviceOrder.shipNumber}';
	var sid='${serviceOrder.id}';
	var url="findBillToAuthority.html?ajax=1&decorator=simple&popup=true&billToCodeForTicket="+billToCodeForTicket+"&sid=" + encodeURI(sid);
	http666.open("GET", url, true);
	http666.onreadystatechange = handleHttpResponseBillToAuthority;
	http666.send(null);
}

function handleHttpResponseBillToAuthority() { 
	if (http666.readyState == 4)
	{
		var results = http666.responseText 
		results = results.trim(); 
		results = results.replace('[','');
		results=results.replace(']','');  

		if(results==0) { 
			document.forms['pricingListForm'].elements['buttonType'].value = "invoice";
			if('${billing.billComplete}'!=''){
				alert("Billing has been marked as complete for this service order.");
				progressBarAutoSave('0');
			}else{
				var  jobtypeForTicket='${serviceOrder.job}';
				if(jobtypeForTicket=='STO'||jobtypeForTicket=='TPS'||jobtypeForTicket=='STL'){
					findBillToAuthState();
				}else{
					validateChargeVatExclude(); 
				}
			}
		} else if(results!=0){
			var  billToCodeForTicket=document.forms['pricingListForm'].elements['billToCodeForTicket'].value
			var  billToAuthority='${billing.billToAuthority}';
			if(billToAuthority==''){
				alert("The following account "+billToCodeForTicket+" does not have authorization #, So Invoicing cannot be done.");
				progressBarAutoSave('0');
			} else if(billToAuthority!=''){ 
				if('${billing.billComplete}'!=''){
					alert("Billing has been marked as complete for this service order.");
					progressBarAutoSave('0');
				}else{
					var  jobtypeForTicket='${serviceOrder.job}';
					if(jobtypeForTicket=='STO'||jobtypeForTicket=='TPS'||jobtypeForTicket=='STL'){
						findBillToAuthState();
					}else{
						validateChargeVatExclude(); 
					}
				}
			}
		}
	}
}
function findBillToAuthState(){ 
	var  jobtypeForTicket='${serviceOrder.job}';
	var  billToCodeForTicket=document.forms['pricingListForm'].elements['billToCodeForTicket'].value
	var shipNumberForTickets='${serviceOrder.shipNumber}';
	var sid='${serviceOrder.id}';
	var url="findBillToAuthState.html?ajax=1&decorator=simple&popup=true&billToCodeForTicket="+billToCodeForTicket+"&sid=" + encodeURI(sid);
	http999.open("GET", url, true);
	http999.onreadystatechange = handleHttpResponseBillToAuthState;
	http999.send(null);
}

function handleHttpResponseBillToAuthState() { 
	if (http999.readyState == 4)
	{
		var results = http999.responseText 
		results = results.trim(); 
		results = results.replace('[','');
		results=results.replace(']','');   
		if(results!='STATE'&& results!='state') { 
			validateChargeVatExclude(); 
		} else if(results=='STATE'|| results=='state'){
			alert("WARNING: Confirm that access was not billed on export authorization.")
			validateChargeVatExclude(); 
		}
	}
}
function validateChargeVatExclude(){

	<c:if test="${systemDefaultVatCalculation == 'true'}">
	var serviceOrderId = '${serviceOrder.id}';
	var url="validateChargeVatExclude.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
	httpVatExclude.open("GET", url, true);
	httpVatExclude.onreadystatechange = handleHttpResponseVatExclude;
	httpVatExclude.send(null);
	</c:if>
	<c:if test="${systemDefaultVatCalculation != 'true'}">
	var  jobtypeForTicket='${serviceOrder.job}';
	var  billToCodeForTicket=document.forms['pricingListForm'].elements['billToCodeForTicket'].value
	var  companyDivisionForTicket=document.forms['pricingListForm'].elements['companyDivisionForTicket'].value
	var shipNumberForTickets='${serviceOrder.shipNumber}';
	var sid='${serviceOrder.id}';
	document.forms['pricingListForm'].elements['buttonType'].value = "invoice"; 
	openWindow('activeWorkTicketsForPricing.html?ajax=1&buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&companyDivisionForTicket='+companyDivisionForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,500);
	</c:if>
}
function  handleHttpResponseVatExclude(){
	if (httpVatExclude.readyState == 4)
	{

		var results = httpVatExclude.responseText 
		results = results.trim();
		results = results.replace('[','');
		results=results.replace(']',''); 
		if(results!='')
		{
			alert('Please enter Receivable VAT for the Account line No. '+ results);
			progressBarAutoSave('0');
		}else{
			var  jobtypeForTicket='${serviceOrder.job}';
			var  billToCodeForTicket=document.forms['pricingListForm'].elements['billToCodeForTicket'].value
			var  companyDivisionForTicket=document.forms['pricingListForm'].elements['companyDivisionForTicket'].value
			var shipNumberForTickets='${serviceOrder.shipNumber}';
			var sid='${serviceOrder.id}';
			document.forms['pricingListForm'].elements['buttonType'].value = "invoice"; 
			openWindow('activeWorkTicketsForPricing.html?ajax=1&buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&companyDivisionForTicket='+companyDivisionForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,500);
		}
	}
}


function findUserPermission1(name,AccDivision,position) { 
	var accountLinecompanyDivision=AccDivision;
	var url="reportBySubModuleAccountLineList1.html?ajax=1&decorator=simple&popup=true&recInvNumb=" + encodeURI(name)+"&id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode =${billing.billToCode}&accountLinecompanyDivision="+accountLinecompanyDivision+"&reportModule=serviceOrder&reportSubModule=Accounting";
	ajax_showTooltip(url,position);	
}
function openReport(id,claimNumber,invNum,jobNumber,bookNumber,reportName,docsxfer,jobType){
	window.open('viewFormParam.html?id='+id+'&invoiceNumber='+invNum+'&claimNumber='+claimNumber+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&bookNumber='+bookNumber+'&noteID=${noteID}&custID=${custID}&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
}
function validatefields(id,invNum,jobNumber,reportName,docsxfer,jobType,val){
	var url='viewReportWithParam.html?formFrom=list&id='+id+'&invoiceNumber='+invNum+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&fileType='+val+'&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'';
	location.href=url;	  
}
function winClose(id,invNum,claimNumber,cid,jobNumber,regNumber,bookNumber,noteID,custID,emailOut,reportModule,reportSubModule,formReportFlag,formNameVal){
	var url = "viewFormParamEmailSetup.html?decorator=popup&popup=true&id="+id+"&invoiceNumber="+invNum+"&jobNumber="+jobNumber+"&regNumber="+regNumber+"&docsxfer="+emailOut+"&reportModule="+reportModule+"&reportSubModule="+reportSubModule+"&formReportFlag="+formReportFlag+"&formNameVal="+formNameVal+"&claimNumber="+claimNumber+"&cid="+cid+"&bookNumber="+bookNumber+"&noteID="+noteID+"&custID="+custID;
	window.open(url,'accountProfileForm','height=650,width=750,top=1,left=200, scrollbars=yes,resizable=yes').focus();
}

function invoicePreview(name,position){
	//var url='viewReceipt.html?jasperName=InvoicePreview.jrxml&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Service Order Number='+encodeURI(name)+'&reportParameter_Corporate ID=${sessionCorpID}&fileType=PDF';
	//document.forms['pricingListForm'].action =url;
	//document.forms['pricingListForm'].submit();
	window.location = 'viewReceipt.html?jasperName=InvoicePreview.jrxml&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Service Order Number='+encodeURI(name)+'&reportParameter_Corporate ID=${sessionCorpID}&fileType=PDF'; 
	return true;
}													     			        
function findReverseInvoice() { 
	var sid='${serviceOrder.id}'; 
	openWindow('reverseInvoiceList.html?buttonType=invoice&sid='+sid+'&decorator=popup&popup=true',680,300);
}	
function openSendtoDates(){ 
	var sid='${serviceOrder.id}'; 
	openWindow('resetSendtoDates.html?sid='+sid+'&decorator=popup&popup=true','','300','250');
}		
function getAdvDtls(position){
	var url="showAdvances.html?ajax=1&sequenceNum=${serviceOrder.shipNumber}&driver=ACCOUNT&decorator=simple&popup=true";
	ajax_SoTooltip(url,position);	
}
function selectAll(str){
	var id = idList.split(",");
	for (i=0;i<id.length;i++){
		if(str.checked == true){
			document.getElementById('displayOnQuote'+id[i].trim()).checked = true;
		}else{
			document.getElementById('displayOnQuote'+id[i].trim()).checked = false;
		}
	}
}
function findVendorInvoicesForUnposting() { 
	var shipNumber=document.forms['pricingListForm'].elements['serviceOrder.shipNumber'].value;
	openWindow('findVendorInvoicesForUnposting.html?shipNumber='+shipNumber+'&decorator=popup&popup=true',680,300);
}
function fillDefaultInvoiceNumber(aid){
	var aVendorCode=document.forms['pricingListForm'].elements['vendorCode'+aid].value;
	var aChargeCode=document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	var fillDefaultInvoiceNumberFlag=false;
	if(aVendorCode!=null && aVendorCode!=undefined && aVendorCode!='' && aChargeCode!=null && aChargeCode!=undefined && aChargeCode!=''){
		fillDefaultInvoiceNumberFlag=true;
	}
	<configByCorp:fieldVisibility componentId="component.field.Alternative.SetRegistrationNumber">
	var invoiceNumber=document.forms['pricingListForm'].elements['invoiceNumber'+aid].value;
	var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
	var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value;
	var soRegistrationNumber='${serviceOrder.registrationNumber}';
	payGl=payGl.trim();	invoiceNumber=invoiceNumber.trim();	actgCode=actgCode.trim();
	if(fillDefaultInvoiceNumberFlag && (invoiceNumber=='')&&(actgCode!='')&&(payGl!='')){
	if(soRegistrationNumber!=''){
		document.forms['pricingListForm'].elements['invoiceNumber'+aid].value ='${serviceOrder.registrationNumber}';
	}else{	
		document.forms['pricingListForm'].elements['invoiceNumber'+aid].value ='${serviceOrder.shipNumber}';
	}
	document.forms['pricingListForm'].elements['payingStatus'+aid].value ='A'; 
	document.forms['pricingListForm'].elements['receivedDate'+aid].value=currentDateGlobal();
	document.forms['pricingListForm'].elements['invoiceDate'+aid].value=currentDateGlobal(); 
	}
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.PayableInvoiceNumber.SetServiceOrderId">
	var vendorCodeOOCheck=document.forms['pricingListForm'].elements['vendorCodeOwneroperator'+aid].value;
	if(fillDefaultInvoiceNumberFlag && vendorCodeOOCheck!=undefined && vendorCodeOOCheck!=null && vendorCodeOOCheck!='' && vendorCodeOOCheck=='Y'){
		var invoiceNumber=document.forms['pricingListForm'].elements['invoiceNumber'+aid].value;
		var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
		var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value;
		var soRegistrationNumber='${serviceOrder.registrationNumber}';
		payGl=payGl.trim();	invoiceNumber=invoiceNumber.trim();	actgCode=actgCode.trim();
			if((invoiceNumber=='')&&(actgCode!='')&&(payGl!='')){ 
				document.forms['pricingListForm'].elements['invoiceNumber'+aid].value = "${serviceOrder.id}"+document.forms['pricingListForm'].elements['vendorCode'+aid].value; 
				document.forms['pricingListForm'].elements['payingStatus'+aid].value ='A'; 
				document.forms['pricingListForm'].elements['receivedDate'+aid].value=currentDateGlobal();
				document.forms['pricingListForm'].elements['invoiceDate'+aid].value=currentDateGlobal(); 
			}
	}
	</configByCorp:fieldVisibility>
}


function checkZeroAmountForInvoice(targetBilltoCode)
{
	<c:set var="checkfromGenerateInvoiceValidation" value="N"/>
 	<configByCorp:fieldVisibility componentId="component.field.generateInvoice.ZeroAmountValidation">
 		<c:set var="checkfromGenerateInvoiceValidation" value="Y"/>
 	</configByCorp:fieldVisibility>
	<c:choose>
	  <c:when test="${checkfromGenerateInvoiceValidation=='Y'}">
	  	var billToCode=targetBilltoCode; 
		var sid = '${serviceOrder.id}';
  		var url="checkZeroforInvoice.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(billToCode);
   		httpCheckZeroforInvoice.open("GET", url, true);
   		httpCheckZeroforInvoice.onreadystatechange = handleHttpResponseforCheckZeroInvoice;
   		httpCheckZeroforInvoice.send(null);
	  </c:when>
		<c:otherwise>
			findBillToAuthority();
   		</c:otherwise>
  	</c:choose>
	
		
   
}  

function handleHttpResponseforCheckZeroInvoice()
{
 		if(httpCheckZeroforInvoice.readyState == 4)
		{
               var results = httpCheckZeroforInvoice.responseText
               results = results.trim(); 
                if(parseFloat(results)!=0)
               {
                	findBillToAuthority();
               }
               else
               {
            	   alert("You cannot generate Invoice as Invoice amount is 0");
              		progressBarAutoSave('0');
                  	return false;
            	   
               }
         }
}

function getBilltoCodeforGenerateInvoice(){ 
	var sid = '${serviceOrder.id}';
     var url="distAmtInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponseGetBilltoCode;
     http3.send(null);
}

function handleHttpResponseGetBilltoCode()
        { 
         if (http3.readyState == 4)
             {
                var results = http3.responseText 
                results = results.trim();   	
                if(results.length>2) {
                	
                  var res = results.split("@");
                  if(res.length == 2)  {  
                    
                    checkZeroAmountForInvoice(res[1]);
                  }  else  {
                	  
                  }
                   
 				}  else  { 
                   
                 }
             }
        }   

function showCheckBoxesInACList(id,position) {
	var url="showCheckBoxesInACListAjax.html?ajax=1&decorator=simple&popup=true&aid=" + encodeURI(id);
	ajax_showTooltip(url,position);
	var containerdiv =  document.getElementById("para1").scrollLeft;
	var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;
	document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';
	//setTimeout(scrollRight,800);
}
function scrollRight(){
	window.scrollTo(1400,0);
}

function updateCheckBoxValue(fieldName,tableName,aid,targetElement){
	 var fieldValue=false;
	 if(targetElement.checked){
		 fieldValue=true;  
  	  }
	 if(targetElement.checked==false){
		 fieldValue=false; 
	 }
	 updateCheckBoxValueInAcList(fieldName,tableName,aid,fieldValue);
}

function updateCheckBoxValueInAcList(fieldName,tableName,aid,fieldValue){
	var updateQuery ='';	
	var userName = '${user.username}';
	var sid = '${serviceOrder.id}';
	if(fieldName=='activateAccPortal' || fieldName=='status'){
		var soNetworkGroup = '${trackingStatus.accNetworkGroup}';
		if(soNetworkGroup==true || soNetworkGroup=='true'){
			updateQuery = "update "+tableName+" set "+fieldName+"="+fieldValue+",updatedBy='"+userName+"', updatedOn=now() where (id='"+aid+"' or networkSynchedId= '"+aid+"') and serviceOrderId='"+sid+"'"
		}else{
			updateQuery = "update "+tableName+" set "+fieldName+"="+fieldValue+",updatedBy='"+userName+"', updatedOn=now() where id="+aid;
		}
	}else{
		updateQuery = "update "+tableName+" set "+fieldName+"="+fieldValue+",updatedBy='"+userName+"', updatedOn=now() where id="+aid;
	}
	//alert(updateQuery)
	new Ajax.Request('/redsky/updateCheckBoxValueInAcListAjax.html?ajax=1&updateQuery='+updateQuery+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			    var response = transport.responseText || "no response text";
				    if(fieldName=='status'){
				    	var statusVal = document.forms['pricingListForm'].elements['accountLineStatus'].value;
				    	statusVal =statusVal.trim();
				    	if(statusVal=='true'){
				    		findAccountLineActiveList();
				    	}else if(statusVal=='allStatus'){
				    		findAccountlineAllList();
				    	}else{}
				    	ajax_hideTooltip();
				    }
			    },
			    onFailure: function(){ 
				    }
			  });
}

function inactiveStatusCheck(rowId,targetElement){ 
	 <c:choose> 
		 <c:when test="${((trackingStatus.accNetworkGroup) && (billingDMMContractType) && (trackingStatus.soNetworkGroup) )}"> 
	     var url="checkNetworkAgentInvoice.html?ajax=1&decorator=simple&popup=true&networkAgentId=" + encodeURI(rowId);
	     httpNetworkAgent.open("GET", url, true);
	     httpNetworkAgent.onreadystatechange = function(){ checkNetworkAgentInvoiceResponse(rowId,targetElement);}; 
	     httpNetworkAgent.send(null);
	     </c:when>
	     <c:otherwise> 
		   if(targetElement.checked){
			   updateCheckBoxValueInAcList('status','accountline',rowId,true);
		     }
		   if(targetElement.checked==false){
			   updateCheckBoxValueInAcList('status','accountline',rowId,false);
		   }
	     </c:otherwise>
    </c:choose>
   } 
function checkNetworkAgentInvoiceResponse(rowId,targetElement){
	 if (httpNetworkAgent.readyState == 4){
      var results = httpNetworkAgent.responseText
      results = results.trim(); 
      if(results=='true'){
   	   alert("This account line cannot be deactivated as either it has been transferred to Accounting and/or Invoiced in UTSI Instance "); 
   	   document.getElementById(rowId).checked=true;
      }else{
	   	   if(targetElement.checked){
	   			updateCheckBoxValueInAcList('status','accountline',rowId,true);  
	   	   }
	   		if(targetElement.checked==false){
	   			updateCheckBoxValueInAcList('status','accountline',rowId,false);
		   }
      }
   }
} 
var httpNetworkAgent= getHTTPObject();

function findWorkTicketList(){
  var shipNumber = '${serviceOrder.shipNumber}';
  var sid ='${serviceOrder.id}';
  openWindow('activeWorkTicketList.html?sid='+sid+'&shipNumber='+shipNumber+'&decorator=popup&popup=true',900,300);
}

function sendEmail(){
	var billingEmail = document.getElementById('billingEmail').value;
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	if(reg.test(billingEmail) == false){
	alert('Invalid Email Address');
	}else{
	var subject = 'S/O# ${serviceOrder.shipNumber}';
	subject = subject+"  "+"${serviceOrder.firstName} ";
	subject = subject+"${serviceOrder.lastName}"; 
	var mailto_link = 'mailto:'+encodeURI(billingEmail)+'?subject='+encodeURI(subject);		
	win = window.open(mailto_link,'emailWindow'); 
	if (win && win.open &&!win.closed) win.close(); 
	}
}

function getContact(){
	var partnerCode = document.getElementById('billToCode').value;	
	openWindow('searchBillingEmail.html?billToCode='+partnerCode+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billing.billingEmail&fld_code=billing.billingEmail');
}

function findVendorInvoiceTotal(position) {
	  var sid = '${serviceOrder.id}'; 
	  var url="findVendorInvoiceTotal.html?ajax=1&decorator=simple&popup=true&buttonType=invoice&sid=" + encodeURI(sid);
	  ajax_showTooltip(url,position);	
}

function findInvoiceTotal(position) {
	  var sid = '${serviceOrder.id}';
	  var divisionFlag="NO"; 
	  try{
		  divisionFlag=document.forms['pricingListForm'].elements['divisionFlag'].value;
	  }catch(e){} 
	  var url="findInvoiceTotal.html?ajax=1&decorator=simple&popup=true&buttonType=invoice&sid=" + encodeURI(sid)+"&divisionFlag="+divisionFlag;
	  ajax_showTooltip(url,position);	
}

function findCopyInvoice() { 
	 if(document.getElementById('billComplete').value!=''){
        alert("Billing has been marked as complete so cannot copy.")
    }else{
	  var sid = '${serviceOrder.id}'; 
	  openWindow('copyInvoiceList.html?buttonType=invoice&sid='+sid+'&decorator=popup&popup=true',680,300);
     }
}

function calCommission(isPayableRequiredFlag){
	
	<c:if test="${vanLineAccountView =='category'}">
	 document.forms['pricingListForm'].elements['vanLineAccountView'].value = "${vanLineAccountView}";
	</c:if>
	if(isPayableRequiredFlag){
		window.location = 'calculateCommisssionLines.html?btntype=yes&isPayableRequired=YES&sid='+${serviceOrder.id}; 
		//document.forms['pricingListForm'].action = 'calculateCommisssion.html?btntype=yes&isPayableRequired=YES';
	}else{
		window.location = 'calculateCommisssionLines.html?btntype=yes&sid='+${serviceOrder.id}; 
		//document.forms['pricingListForm'].action = 'calculateCommisssion.html?btntype=yes';
	}
	
	//document.forms['pricingListForm'].submit();
}
function autoCommissionCal(obj){
	obj.disable= true;
	window.location = 'autoCommisssionCalculateLines.html?btntype=yes&sid='+${serviceOrder.id}; 
	//document.forms['serviceForm1'].action = 'autoCommisssionCalculate.html?btntype=yes';
	//document.forms['serviceForm1'].submit();
}

<c:if test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD" || serviceOrder.status == "CLSD"}'>
	var elementsLen=document.forms['pricingListForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
			if(document.forms['pricingListForm'].elements[i].type=='text') {
				document.forms['pricingListForm'].elements[i].disabled=true;
				document.forms['pricingListForm'].elements[i].className = 'input-textUpper pr-f11';
			} else { 
				document.forms['pricingListForm'].elements[i].disabled=true;
			}
		}
	var totalImages = document.images.length;
	for (var i=0;i<totalImages;i++){
			if(document.images[i].src.indexOf('calender.png')>0){
				var el = document.getElementById(document.images[i].id);  
					document.images[i].src = 'images/navarrow.gif'; 
					if((el.getAttribute("id")).indexOf('trigger')>0){
						var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
						el.setAttribute("id",newId);
					}
			}
	}
	var div = document.getElementById("allButton");
	var nodelist = div.getElementsByTagName("input");
	var i;
	for (i = 0; i < nodelist.length; i++) {
	    nodelist[i].className = "pricelistbtn-dis";
	}
	document.getElementById("TicketList").disabled =false;
	document.getElementById("pageFirst").disabled =false;
	document.getElementById("pagedisplay").disabled =false;
	document.getElementById("pagedisplay").readOnly =false;
	document.getElementById("pageNext").disabled =false;
	document.getElementById("pageLast").disabled =false;
	document.getElementById("pagesize").disabled =false;
	document.getElementById("fieldName").disabled =false;
	document.getElementById("fieldValue").disabled =false;
</c:if>

	var elementsLen=document.forms['pricingListForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++){
			if(document.forms['pricingListForm'].elements[i].type === "hidden" && document.forms['pricingListForm'].elements[i].name === "id"){
				var aid = document.forms['pricingListForm'].elements[i].value;
				var accIdList = document.getElementById("accountLineId").value;
					if(accIdList!=''){
						var check = accIdList.indexOf(aid);
						if(check > -1){
						}else{
							accIdList = accIdList+','+aid;
						}
					}else{
						accIdList = aid;
					}
					document.getElementById("accountLineId").value = accIdList;
				
					var soNetworkGroup = ${trackingStatus.soNetworkGroup};
					var accNetworkGroup = ${trackingStatus.accNetworkGroup};
					var billingDMMContractType = ${billingDMMContractType};
					var billingCMMContractType = ${billingCMMContractType};
					var chargeCode = document.getElementById('chargeCode'+aid).value;
					var createdBy = document.getElementById('createdBy'+aid).value;
					var networkSynchedId = document.forms['pricingListForm'].elements['networkSynchedId'+aid].value;
					
					if(billingDMMContractType && (chargeCode =='DMMFEE' || chargeCode =='DMMFXFEE')){
						trap(aid);
					}else if(soNetworkGroup  && (billingCMMContractType || billingDMMContractType) && (createdBy =='Networking' || ((createdBy.indexOf("Stg Bill")>=0 && networkSynchedId !=null )  &&  !(accNetworkGroup)))){
						trap(aid);
					}else if(soNetworkGroup  && billingCMMContractType && chargeCode=='MGMTFEE'){
						trap(aid);
					}else if(soNetworkGroup  && billingDMMContractType && (chargeCode =='DMMFEE' || chargeCode =='DMMFXFEE')){
						trap(aid);
					}else if(chargeCode =='MGMTFEE'){
						trap(aid);
					}else if(billingDMMContractType && (chargeCode =='DMMFEE' || chargeCode =='DMMFXFEE')){
						trap(aid);
					}else if(soNetworkGroup  && (billingCMMContractType || billingDMMContractType) && (createdBy=='Networking')){
						trap(aid);
					}else{}
					
				}
		}
	function trap(aid){
			if(document.images) {
			    var totalImages = document.images.length;
			    	for (var i=0;i<totalImages;i++) {
							if(document.images[i].src.indexOf('calender.png')>0) { 
								var el = document.getElementById(document.images[i].id);  
								document.images[i].src = 'images/navarrow.gif'; 
								if((el.getAttribute("id")).indexOf('trigger')>0){ 
									el.removeAttribute('id');
								}
							}
							if((document.images[i].src.indexOf('open-popup.gif')>0) && (document.images[i].id.indexOf('vendorCodeOpenpopup'+aid)>=0)){  
								var el = document.getElementById(document.images[i].id); 
								try{
									el.onclick = false;
								}catch(e){}
							    	document.images[i].src = 'images/navarrow.gif';
							}
							if((document.images[i].src.indexOf('open-popup.gif')>0) && (document.images[i].id.indexOf('chargeCodeOpenpopup'+aid)>=0)){  
								var el = document.getElementById(document.images[i].id); 
								try{
									el.onclick = false;
								}catch(e){}
							    	document.images[i].src = 'images/navarrow.gif';
							}
							if(document.images[i].src.indexOf('notes_empty1.jpg')>0) {
								var el = document.getElementById(document.images[i].id);
								try{
									el.onclick = false;
								}catch(e){}
						        	document.images[i].src = 'images/navarrow.gif';
							}
							if(document.images[i].src.indexOf('notes_open1.jpg')>0) {
								var el = document.getElementById(document.images[i].id);
								try{
									el.onclick =false;
								}catch(e){}
									document.images[i].src = 'images/navarrow.gif';
							} 				
							if(document.images[i].src.indexOf('images/nav')>0) {
								var el = document.getElementById(document.images[i].id);
								try{
									el.onclick = false;
								}catch(e){}
									document.images[i].src = 'images/navarrow.gif';
							}
							if(document.images[i].src.indexOf('images/image')>0) {
								var el = document.getElementById(document.images[i].id);
								try{
									el.onclick = false;
								}catch(e){}
									document.images[i].src = 'images/navarrow.gif';
							}
							if((document.images[i].src.indexOf('calc_new1.png')>0) && (document.images[i].id.indexOf('compute'+aid)>=0)){  
								var el = document.getElementById(document.images[i].id); 
								try{
									el.onclick = false;
								}catch(e){}
							    	document.images[i].src = 'images/navarrow.gif';
							}
						} 
				 	}
					var elementsLen=document.forms['pricingListForm'].elements.length;
						for(i=0;i<=elementsLen-1;i++) {
							if(document.forms['pricingListForm'].elements[i].type=='text' && document.forms['pricingListForm'].elements[i].name.indexOf(aid)>=0) {
								document.forms['pricingListForm'].elements[i].disabled =true;
								document.forms['pricingListForm'].elements[i].className = 'input-textUpper pr-f11';
								document.forms['pricingListForm'].elements[i].onkeydown=false;						
							}else{
							 	if(document.forms['pricingListForm'].elements[i].name.indexOf(aid)>=0){
									document.forms['pricingListForm'].elements[i].disabled=true;
							 	}
							} 	
					}
					
					var chargeCode = document.getElementById('chargeCode'+aid).value;
					var createdBy = document.getElementById('createdBy'+aid).value;
					var networkSynchedId = document.forms['pricingListForm'].elements['networkSynchedId'+aid].value;
					var soNetworkGroup = ${trackingStatus.soNetworkGroup};
					var billingDMMContractType = ${billingDMMContractType};
					
			  		if (soNetworkGroup  && billingDMMContractType && chargeCode != 'DMMFEE' && chargeCode != 'DMMFXFEE'  && (createdBy == 'Networking' || ((createdBy.indexOf('Stg Bill')> -1) && (networkSynchedId!=null || networkSynchedId!='') ))){  
			  		
			  			if(document.forms['pricingListForm'].elements['recAccDate'+aid].value=='' ){
						  		document.forms['pricingListForm'].elements['pricingSave'].disabled=false;
						  		document.forms['pricingListForm'].elements['pricingSave'].className ='pricelistbtn';
						  		document.forms['pricingListForm'].elements['Reset'].disabled=false;
						   
						  var vatExcludeFlag = document.forms['pricingListForm'].elements['VATExclude'+aid].value;  
						  if(vatExcludeFlag != 'true') {
						  	document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=false;
						  }
						  var accNonEditFlag= ${accNonEditFlag};
						  if(!(document.forms['pricingListForm'].elements['recInvoiceNumber'+aid].value!='' && accNonEditFlag)){
						  		document.getElementById('accountLineBillToCodeOpenpopup'+aid).src='images/open-popup.gif'; 
						  		document.getElementById('accountLineBillToCodeOpenpopup'+aid).onclick = function(){
						  	 		openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billToName'+aid+'&fld_code=billToCode'+aid+'');
							    }
						  }
						  document.forms['pricingListForm'].elements['description'+aid].disabled =false;
						  document.forms['pricingListForm'].elements['description'+aid].className ='input-text pr-f11';
						  document.forms['pricingListForm'].elements['quoteDescription'+aid].disabled =false;
						  document.forms['pricingListForm'].elements['quoteDescription'+aid].className ='input-text pr-f11';
						  document.forms['pricingListForm'].elements['companyDivision'+aid].disabled=false; 
					 	}
			  		} 
					if((!(soNetworkGroup))  && billingDMMContractType && (chargeCode == 'DMMFEE' || chargeCode == 'DMMFXFEE' )){
						document.forms['pricingListForm'].elements['pricingSave'].disabled=false;
				  		document.forms['pricingListForm'].elements['pricingSave'].className ='pricelistbtn';
					  	document.forms['pricingListForm'].elements['Reset'].disabled=false;
						document.forms['pricingListForm'].elements['invoiceNumber'+aid].readOnly =false; 
					  	document.forms['pricingListForm'].elements['invoiceNumber'+aid].className ='input-text pr-f11';
					}
					
					document.getElementById("TicketList").disabled =false;
					document.getElementById("pageFirst").disabled =false;
					document.getElementById("pagedisplay").disabled =false;
					document.getElementById("pagedisplay").readOnly =false;
					document.getElementById("pageNext").disabled =false;
					document.getElementById("pageLast").disabled =false;
					document.getElementById("pagesize").disabled =false;
					document.getElementById("fieldName").disabled =false;
					document.getElementById("fieldValue").disabled =false;
	}
	<c:if test="${systemDefaultVatCalculationNew=='Y'}">
		var accountLineId = document.getElementById("accountLineId").value;
		var accId=accountLineId.split(",");
			for(var i=0;i<accId.length;i++){
				var vatExclude=document.forms['pricingListForm'].elements['VATExclude'+accId[i]].value;
				if(vatExclude==true || vatExclude=='true'){
					document.forms['pricingListForm'].elements['payVatDescr'+accId[i]].disabled = true;
					document.forms['pricingListForm'].elements['payVatPercent'+accId[i]].disabled = true;
					document.forms['pricingListForm'].elements['recVatDescr'+accId[i]].disabled = true;
					document.forms['pricingListForm'].elements['recVatPercent'+accId[i]].disabled = true;
				}
			}
	</c:if>
	
	function populateVarienceAmount(aid){
		<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
		var baseCurrency = '${baseCurrency}';
		var country=document.forms['pricingListForm'].elements['country'+aid].value;
		if(country==baseCurrency){
			var revisionExpense = document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
			var actualExpense = document.forms['pricingListForm'].elements['actualExpense'+aid].value;
			if(revisionExpense!=actualExpense){
				var varianceExpenseAmt = actualExpense-revisionExpense;
				document.forms['pricingListForm'].elements['varianceExpenseAmount'+aid].value = Math.round(varianceExpenseAmt*100)/100;
				document.forms['pricingListForm'].elements['payingStatus'+aid].value = 'VH';
			}
		}else{
			document.forms['pricingListForm'].elements['payingStatus'+aid].value = 'FH';
		}
		populateAccrualHold(aid);
		</configByCorp:fieldVisibility>
	}
	
	function populateAccrualHold(aid){
		var revisionExpense = document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
		var accrueChecked = '${serviceOrder.accrueChecked}';
		if((revisionExpense=='0' || revisionExpense=='0.00') && (accrueChecked!=null && accrueChecked!='')){
			document.forms['pricingListForm'].elements['payingStatus'+aid].value = 'VH';
		}else if((revisionExpense=='0' || revisionExpense=='0.00') && (accrueChecked==null || accrueChecked=='')){
			document.forms['pricingListForm'].elements['payingStatus'+aid].value = 'AH';
		}
	}
		
	function resetFormData(){
		document.forms['pricingListForm'].reset();
	}
	
	(function ($) {
		$('#expandList')
	    .unbind('click')
	    .click( function() {
	        $('.collapsed').removeClass('collapsed').addClass('expanded').css("display","table-cell");
	        $("table tr.expand-child td").css("display","table-cell").show();
	    })
	    
	    $('#collapseList')
	    .unbind('click')
	    .click( function() {
	        $('.expanded').removeClass('expanded').addClass('collapsed');
	        $("table tr.expand-child td").css("display","none").hide();
	    })
    })(jQuery);
    
	
/*		End New JS ************************************************************************************************  */

</script>
