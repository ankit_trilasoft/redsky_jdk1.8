<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Rating Feedback List</title>
    <meta name="heading" content="Rating Feedback List"/> 
<style type="text/css">
/* collapse */

span.pagelinks {
display:block;
font-size:0.85em;
margin-top:-11px;
padding:2px 0px;
text-align:right;
width:99%;
}
</style> 

</head>
 
<s:form id="feedbackForm" action="" method="post" >  
<s:set name="ratingFeedback" value="ratingFeedback" scope="request"/>  
<display:table name="ratingFeedback" class="table" requestURI="" id="ratingFeedbackList" defaultsort="2" export="false" pagesize="10" style="width:100%">   
	<display:column property="year" sortable="true" title="Year" style="width:40px"/>
	<display:column property="shipNumber" sortable="true" title="S/O Number" style="width:40px"/>
	<display:column property="oaeValuation" sortable="true" title="Rating" style="width:25px"/>
    <display:column property="feedback" sortable="true" title="Feedback"/>
	
    <display:setProperty name="paging.banner.item_name" value="ratingFeedback"/>   
    <display:setProperty name="paging.banner.items_name" value="ratingFeedback"/>   
  
    <display:setProperty name="export.excel.filename" value="Feedback List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Feedback List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Feedback List.pdf"/>   
</display:table>  
<input type="button"" name="addBtn" class="cssbutton" style="width:85px; height:25px; margin-left:15px;" onclick="window.close();" align="top" value="Close"/>   
</s:form>
<script type="text/javascript">   
    Form.focusFirstElement($("feedbackForm")); 
</script>  
