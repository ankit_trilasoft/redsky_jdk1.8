package com.trilasoft.app.dao.hibernate.dto;

import java.util.Date;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ScheduledSurveyDTO {
	
	private Long id;
	private String userName;
	private Date surveyDate;
	private String fromHrs;
	private String toHrs;
	private String state;
	private String city;
	private String activity;
	private String billTo;
	private String billToCode;
	private String sequenceNumber;
	private String jobType;
	private String surveyAddress;
	private String eventType;
	private String contlFlag;
	private String surveyType;
	private String corpId;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("userName",
				userName).append("surveyDate", surveyDate).append("fromHrs",
				fromHrs).append("toHrs", toHrs).append("state", state).append(
				"city", city).append("activity", activity).append("billTo",
				billTo).append("billToCode", billToCode).append(
				"sequenceNumber", sequenceNumber).append("jobType", jobType)
				.append("surveyAddress", surveyAddress).append("eventType",
						eventType).append("contlFlag", contlFlag).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ScheduledSurveyDTO))
			return false;
		ScheduledSurveyDTO castOther = (ScheduledSurveyDTO) other;
		return new EqualsBuilder().append(id, castOther.id).append(userName,
				castOther.userName).append(surveyDate, castOther.surveyDate)
				.append(fromHrs, castOther.fromHrs).append(toHrs,
						castOther.toHrs).append(state, castOther.state).append(
						city, castOther.city).append(activity,
						castOther.activity).append(billTo, castOther.billTo)
				.append(billToCode, castOther.billToCode).append(
						sequenceNumber, castOther.sequenceNumber).append(
						jobType, castOther.jobType).append(surveyAddress,
						castOther.surveyAddress).append(eventType,
						castOther.eventType).append(contlFlag,
						castOther.contlFlag).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(userName).append(
				surveyDate).append(fromHrs).append(toHrs).append(state).append(
				city).append(activity).append(billTo).append(billToCode)
				.append(sequenceNumber).append(jobType).append(surveyAddress)
				.append(eventType).append(contlFlag).toHashCode();
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getBillTo() {
		return billTo;
	}
	public void setBillTo(String billTo) {
		this.billTo = billTo;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getFromHrs() {
		return fromHrs;
	}
	public void setFromHrs(String fromHrs) {
		this.fromHrs = fromHrs;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getSurveyDate() {
		return surveyDate;
	}
	public void setSurveyDate(Date surveyDate) {
		this.surveyDate = surveyDate;
	}
	public String getToHrs() {
		return toHrs;
	}
	public void setToHrs(String toHrs) {
		this.toHrs = toHrs;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getSurveyAddress() {
		return surveyAddress;
	}
	public void setSurveyAddress(String surveyAddress) {
		this.surveyAddress = surveyAddress;
	}
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	public String getContlFlag() {
		return contlFlag;
	}
	public void setContlFlag(String contlFlag) {
		this.contlFlag = contlFlag;
	}
	/**
	 * @return the surveyType
	 */
	public String getSurveyType() {
		return surveyType;
	}
	/**
	 * @param surveyType the surveyType to set
	 */
	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}
	/**
	 * @return the corpId
	 */
	public String getCorpId() {
		return corpId;
	}
	/**
	 * @param corpId the corpId to set
	 */
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	
	

}
