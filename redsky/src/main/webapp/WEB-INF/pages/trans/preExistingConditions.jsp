 <%@ include file="/common/taglibs.jsp"%> 
<div id="otabs" style="margin-top:-30px;">
<ul>
<li><a class="current"><span>  Location and Room details</span></a></li>
</ul>
</div>
<div class="spn"></div>
 <%-- 
 <display:table name="preConditionList"  id ="preCondition" class="table" requestURI=""  pagesize="10" style="width:98%;margin-left:10px;" >
 	<display:column title="Ticket #" style="width:100px;" group="1" property="ticket" />
	<display:column title="Location " style="width:100px;" property="location" />
	<display:column title="Address Line" property="address"></display:column>
	<display:column title="City , State" style="width:150px;">${preCondition.city}${preCondition.state} </display:column>
	<display:column title="Country" style="width:100px;"  property="country"></display:column>
	<display:column title="Zip" style="width:80px;" property="zip"></display:column>
	<display:column title="Photos"  style="width:12px;" >
	 	<c:if test="${preCondition.photos!=null && preCondition.photos!=''}" >
			<img  src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="imageList('${preCondition.photos}',this,'locationId')"/>
		</c:if>
	</display:column>
</display:table> 
--%>

 <display:table name="preConditionList"  id ="preCondition" class="table" requestURI=""  style="width:98%;margin-left:10px;" >
 	<display:column title="Ticket #" style="width:65px;" group="1" property="ticket" />
	<display:column title="Location " style="width:80px;"  group="2" property="location" />
	<display:column title="Address" group="3" >
		<c:if test="${preCondition.address!=null && preCondition.address!=''}" >
			<c:out value="${preCondition.address}" />
		</c:if>
		
		<c:if test="${preCondition.photos!=null && preCondition.photos!=''}" >
			<img title="Location Image"  src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="20" height="20" style="vertical-align: middle;" onclick="imageList('${preCondition.photos}',this,'locationId')"/>
		</c:if>
	</display:column>
	<%-- <display:column title="City , State" style="width:150px;">${preCondition.city}${preCondition.state} </display:column>
	<display:column title="Country" style="width:100px;"  property="country"></display:column>
	<display:column title="Zip" style="width:80px;" property="zip"></display:column>  --%>
	<display:column title="Rooms" >
		${preCondition.rooms }
		<c:if test="${preCondition.dPhotos!=null && preCondition.dPhotos!=''}" >
				<img  title="Rooms Image" src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="20" height="20" style="vertical-align: middle;" onclick="imageList('${preCondition.dPhotos}',this,'roomId')"/>
			</c:if>
	</display:column>
		<display:column title="Floors" property="floors"></display:column>
		<display:column title="Damaged" style="text-align: center ; width:10px;">
				<c:if test="${preCondition.hasDamage!=null && preCondition.hasDamage=='Y'}" >
					<img src="${pageContext.request.contextPath}/images/tick01.gif" />
				</c:if>
				<c:if test="${preCondition.hasDamage==null || preCondition.hasDamage=='N'}" >
					<img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
				</c:if>
		</display:column>
		<display:column title="Comments" property="comments"></display:column>
</display:table> 