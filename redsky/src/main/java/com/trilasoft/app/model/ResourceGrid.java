package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name = "resourcegrid")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class ResourceGrid extends BaseObject{
	
	private Long id;
	
	private String category;
	
	private String resource;
	
	private BigDecimal resourceLimit = new BigDecimal(0.00);
	
	private String hubID;
	
	private Date workDate;
	
	private String corpID;

	private String updatedBy;

	private String createdBy;

	private Date createdOn;

	private Date updatedOn;
	private Long parentId;

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ResourceGrid))
			return false;
		ResourceGrid castOther = (ResourceGrid) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(category, castOther.category)
				.append(resource, castOther.resource)
				.append(resourceLimit, castOther.resourceLimit)
				.append(hubID, castOther.hubID)
				.append(workDate, castOther.workDate)
				.append(corpID, castOther.corpID)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(parentId, castOther.parentId).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(category)
				.append(resource).append(resourceLimit).append(hubID)
				.append(workDate).append(corpID).append(updatedBy)
				.append(createdBy).append(createdOn).append(updatedOn)
				.append(parentId).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("category", category).append("resource", resource)
				.append("resourceLimit", resourceLimit).append("hubID", hubID)
				.append("workDate", workDate).append("corpID", corpID)
				.append("updatedBy", updatedBy).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("parentId", parentId).toString();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}
	
	public String getHubID() {
		return hubID;
	}

	public void setHubID(String hubID) {
		this.hubID = hubID;
	}

	public Date getWorkDate() {
		return workDate;
	}

	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public BigDecimal getResourceLimit() {
		return resourceLimit;
	}

	public void setResourceLimit(BigDecimal resourceLimit) {
		this.resourceLimit = resourceLimit;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	
}
