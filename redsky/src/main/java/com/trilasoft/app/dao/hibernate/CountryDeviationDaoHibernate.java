package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CountryDeviationDao;
import com.trilasoft.app.model.CountryDeviation;

public class CountryDeviationDaoHibernate extends GenericDaoHibernate<CountryDeviation, Long> implements CountryDeviationDao {

	public CountryDeviationDaoHibernate() {
		super(CountryDeviation.class);
	}
	public List findDeviation(String charge, String deviationType){
		if(deviationType.equalsIgnoreCase("sell")){
		return getHibernateTemplate().find("from CountryDeviation where charge='"+charge+"' and purchase='N' ");
		}else{
		return getHibernateTemplate().find("from CountryDeviation where charge='"+charge+"' and purchase='Y' ");	
		}
	}
	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from CountryDeviation");
	}
	public void delSelectedDevitaion(String delId){
		getHibernateTemplate().bulkUpdate("delete from CountryDeviation where id in("+delId+")");
	}
	public List findAllDeviation(String chargeId, String corpId,	String contract){
		String Query ="select id from countrydeviation where contractName='"+contract+"' and corpid='"+corpId+"' and charge='"+chargeId+"'";
		List check = this.getSession().createSQLQuery("select id from countrydeviation where contractName='"+contract+"' and corpid='"+corpId+"' and charge='"+chargeId+"'").list(); 
		return check;
	}
}
