package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.IntegrationLog;

public interface IntegrationLogManager extends GenericManager<IntegrationLog, Long>{
	
	public List<IntegrationLog>findIntegrationLogs(String fileName, Date processedOn, String recordID, String message);
	
	public List findServiceOrder(String shipNum, String corpID);
	
	public List getAllWithServiceOrderId(String corpId);
	
	public List findIntegrationCenterLogsWithServiceOrderId(String fileName, Date date, String recordID, String message, String transId,String intType, String corpId);
	
	public List findIntegrationLogsWithServiceOrderId(String fileName, Date date, String recordID, String message, String transId,String intType, String corpId);
	public List findMemoUpload(String fileName, String recordID, String message, String date, String corpId);
	
	public List getWeeklyXML(String corpId);
}
