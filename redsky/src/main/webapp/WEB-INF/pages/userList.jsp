<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="userList.title"/></title>
    <meta name="heading" content="<fmt:message key='userList.heading'/>"/>
    
  <script language="javascript" type="text/javascript">
  function showFailureToolTip(usr,obj){
	 // alert(usr);
	  ajax_showTooltip('findtoolTipFailure.html?ajax=1&decorator=simple&popup=true&tableFieldName='+usr,obj);
  }
  
  function clear_fields(){
	var i;
		for(i=0;i<=5;i++){
			document.forms['searchForm'].elements[i].value = "";
		}
		document.forms['searchForm'].elements['activeUser'].checked=false;
	}
	function findUserRole(setName,position) { 
  var url="findUserRole.html?decorator=simple&popup=true&buttonType=invoice&userName=" + encodeURI(setName);
  ajax_showTooltip(url,position);	
  }


function setActive(target){
	if(target.checked){
		document.forms['searchForm'].elements['enableUser'].value='Y'
	}
	else{
		document.forms['searchForm'].elements['enableUser'].value='N'
	}
}
function tickBox(){
 var val=document.forms['searchForm'].elements['enableUser'].value;
 if(val=='Y'){
	 document.forms['searchForm'].elements['activeUser'].checked=true;
	}
}
</script>
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:2px;margin-top:-16px;!margin-top:-17px;padding:2px 0px;text-align:right;width:100%;}
</style>
</head>
<body>

<c:set var="buttons">
     <c:if test="${adminAccess}">
    <input type="button" class="cssbuttonA"
        onclick="location.href='<c:url value="/editUser.html?method=Add&from=list"/>'"
        value="<fmt:message key="button.add"/>"/>
    </c:if>
    <!--<input type="button" class="cssbutton1" onclick="location.href='<c:url value="/mainMenu.html"/>'"
        value="<fmt:message key="button.done"/>"/>
--></c:set>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px;" method="userListBySearch" key="button.search"/> 
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>
<s:form cssClass="form_magn" id="searchForm" action="searchUser" method="post" validate="true">
<s:hidden name="enableUser" value="${enableUser }"/>
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top:-2px; "><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>
<th><fmt:message key="user.userType"/></th>
<th><fmt:message key="user.username"/></th>
<th><fmt:message key="user.firstName"/></th>
<th><fmt:message key="user.lastName"/></th>
<th><fmt:message key="user.email"/></th>
<th>Supervisor</th>
</tr></thead>	
		<tbody>
		<tr>
		<!--<td width="20" align="left"><s:textfield name="userType" required="true" cssClass="input-text" size="12"/></td> -->
			<td align="left">
			<s:select name="userType" list="%{usertype}"  cssStyle="width:140px" cssClass="list-menu" headerKey="" headerValue="" onclick="changeStatus()"/>
			</td>
			
			<td align="left">
			    <s:textfield name="username" required="true" cssClass="input-text" size="20" cssStyle="width:140px"/>
			</td>
			<td align="left">
			    <s:textfield name="firstName" required="true" cssClass="input-text" size="20" cssStyle="width:140px"/>
			</td>
			<td align="left">
			    <s:textfield name="lastName" required="true" cssClass="input-text" size="20" cssStyle="width:140px"/>
			</td>
			<td align="left">
			    <s:textfield name="email" required="true" cssClass="input-text" size="30" cssStyle="width:140px"/>
			</td>
			<td align="left">
			<s:select list="%{supervisorList}" name="supervisor" cssClass="list-menu" cssStyle="width:140px" headerKey="" headerValue=""/>			    
			</td>
			</tr>
			<tr>
			<td colspan="5" align="right"></td>
			<td align="left" style="border-left:hidden;text-align:left;" colspan="1" class="listwhitetext">
			Enabled<s:checkbox key="activeUser"  onclick="setActive(this);" name="activeUser"/>
			<c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>
   <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
   <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 
   <c:set var="displayDateTimeFormat" value="dd-MMM-yyyy HH:mm" scope="session"/>
<div id="otabs">
  <ul>
    <li><a class="current"><span>User List</span></a></li>
  </ul>
</div>
<jsp:useBean id="now" class="java.util.Date" scope="request"/> 
<fmt:parseNumber
    value="${ now.time /(1000 * 60 * 60 * 24)}"
    integerOnly="true" var="nowDays" scope="request"/>
<div class="spnblk">&nbsp;</div>
<display:table name="users" requestURI="" defaultsort="2" id="users" class="table" pagesize="10" style="!margin-top:-2px;">
<c:if test="${not empty users}">
<display:column property="userType" escapeXml="true" sortable="true" titleKey="user.userType" style="width: 8%; cursor: default;"/>
    <display:column  sortable="true" title="User Name" style="width: 25%" sortProperty="username">
   	<a href="editUser.html?from=list&id=${users.id}"><c:out value="${users.username}"></c:out></a>
    </display:column>    
    <display:column property="alias" escapeXml="true" sortable="true" title="Alias" style="width: 15%"/>
    <display:column property="fullName" escapeXml="true" sortable="true" titleKey="activeUsers.fullName" style="width: 30%"/>

      <%-- <display:column  sortable="true" sortProperty="login_time" style="width:30%;"  title="Last Login&nbsp;(Status)" >
    <fmt:formatDate value="${users.login_time}" pattern="${displayDateTimeFormat}"/>
     <c:if  test="${users.login_time!='' && users.login_status!=null }"><c:out value="${users.login_status}"/></c:if>
     </display:column> --%>
    <c:if test="${not empty users.pwdexpiryDate}">
      <fmt:parseNumber
    value="${ users.pwdexpiryDate.time /(1000 * 60 * 60 * 24)}"
    integerOnly="true" var="otherDays" scope="page"/>
   <c:set value="${otherDays-nowDays}" var="dateDiff"/> 
   </c:if>
    <c:if test="${empty users.pwdexpiryDate}">
     <c:set value="70" var="dateDiff"/> 
    </c:if>
     <c:choose>
      <c:when test="${dateDiff ne null && dateDiff lt -1}">
      <display:column property="pwdexpiryDate"  style="background:#F59696;width:80px;" title="Password Expiry&nbsp;Date"  sortable="true" sortProperty="pwdexpiryDate" format="{0,date,dd-MMM-yyyy}"/>
     </c:when>
    <c:when test="${dateDiff ne null && dateDiff ge -1 && dateDiff lt 29}">
     <display:column property="pwdexpiryDate"  style="width:100px;background:#E4D970;" title="Password Expiry&nbsp;Date"  sortable="true" sortProperty="pwdexpiryDate" format="{0,date,dd-MMM-yyyy}"/>
     </c:when>
     <c:otherwise>
     <display:column property="pwdexpiryDate"  style="width:50px;" title="Password Expiry&nbsp;Date"  sortable="true" sortProperty="pwdexpiryDate" format="{0,date,dd-MMM-yyyy}"/>
     </c:otherwise>
     </c:choose>
     <%-- <display:column  escapeXml="true" sortable="true" title="Failed Attempts" style="width: 15%">
     <a href="#" onmouseover="showFailureToolTip('${users.username}',this)">
         	<c:out value="${users.loginFailureReason}" />
        </a> 
    </display:column> --%>
    <%-- <display:column title="Failed Attempts" style="width: 10%" >
   	<a href="#" onmouseover="showFailureToolTip('${users.username}',this)"><c:set var="string44" value="" /><c:set var="string4" value="${fn:split(users.loginFailureReason, ',')}" /><c:forEach var="tmp" items="${string4}"><c:set var="string44" value="${tmp}" /></c:forEach><c:out value="${string44}"></c:out></a>
    </display:column>  --%>
    <display:column property="userTitle" escapeXml="true" sortable="true" title="Title" style="width: 15%"/>
    <display:column property="email" sortable="true" titleKey="user.email" style="width: 25%" autolink="true" media="html"/>
    <display:column property="email" titleKey="user.email" media="csv xml excel pdf"/>
    <display:column sortProperty="enabled" sortable="true" titleKey="user.enabled" style="width: 16%; padding-left: 15px" media="html">
        <input type="checkbox" disabled="disabled" <c:if test="${users.enabled}">checked="checked"</c:if>/>
    </display:column>
    <display:column property="enabled" titleKey="user.enabled" media="csv xml excel pdf"/>
    <display:column title="Photograph" >
    <c:if test="${users.location == null || users.location == ''}">
    <img id="userImage" src="${pageContext.request.contextPath}/images/no-image.png" style="padding-left:22px" alt="" width="40" height="25" />
    </c:if>
    <c:if test="${!(users.location == null || users.location == '')}">
	<img src="UserImage?location=${users.location}" style="padding-left:22px" alt="" width="40" height="25" />
    </c:if>
    </display:column>
     <display:column title="Roles" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserRole('${users.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	</c:if>
	<c:if test="${empty users}">
	<display:column property="userType" escapeXml="true" sortable="true" titleKey="user.userType" style="width: 8%; cursor: default;"/>
    <display:column  sortable="true" title="User Name" style="width: 35%" sortProperty="username">
   	<a href="editUser.html?from=list&id=${users.id}"><c:out value="${users.username}"></c:out></a>
    </display:column>    
    <display:column property="alias" escapeXml="true" sortable="true" title="Alias" style="width: 15%"/>
    <display:column property="fullName" escapeXml="true" sortable="true" titleKey="activeUsers.fullName" style="width: 30%"/>

     <display:column  sortable="true" sortProperty="login_time"   title="Last Login&nbsp;(Status)">
    <fmt:formatDate value="${users.login_time}" pattern="${displayDateTimeFormat}"/>
     <c:if  test="${users.login_time!='' && users.login_status!=null }"><c:out value="${users.login_status}"/></c:if>
    
     </display:column>
     <c:choose>
     <c:when test=""></c:when>
     <c:otherwise>
     <display:column property="pwdexpiryDate"  style="width:50px;" title="Password Expiry&nbsp;Date"  sortable="true" sortProperty="pwdexpiryDate" format="{0,date,dd-MMM-yyyy}"/>
     </c:otherwise>
     </c:choose>
    <display:column   title="Failed Attempts" style="width: 10%" >
   	<a href="#" onmouseover="showFailureToolTip('${users.username}',this)"><c:set var="string44" value="" /><c:set var="string4" value="${fn:split(users.loginFailureReason, ',')}" /><c:forEach var="tmp" items="${string4}"><c:set var="string44" value="${tmp}" /></c:forEach><c:out value="${string44}"></c:out></a>
    </display:column> 
    <display:column property="userTitle" escapeXml="true" sortable="true" title="Title" style="width: 15%"/>
    <display:column property="email" sortable="true" titleKey="user.email" style="width: 25%" autolink="true" media="html"/>
    <display:column property="email" titleKey="user.email" media="csv xml excel pdf"/>
    <display:column sortProperty="enabled" sortable="true" titleKey="user.enabled" style="width: 16%; padding-left: 15px" media="html">
        <input type="checkbox" disabled="disabled" <c:if test="${users.enabled}">checked="checked"</c:if>/>
    </display:column>
    <display:column property="enabled" titleKey="user.enabled" media="csv xml excel pdf"/>
    <display:column title="Photograph" >
    <c:if test="${users.location == null || users.location == ''}">
    <img id="userImage" src="${pageContext.request.contextPath}/images/no-image.png" style="padding-left:22px" alt="" width="40" height="25" />
    </c:if>
    <c:if test="${!(users.location == null || users.location == '')}">
	<img src="UserImage?location=${users.location}" style="padding-left:22px" alt="" width="40" height="25" />
    </c:if>
    </display:column>
     <display:column title="Roles" style="width: 15px;">
		<a><img align="middle" title="User List" onclick="findUserRole('${users.username}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
	</display:column>
	</c:if>
    <display:setProperty name="paging.banner.item_name" value="user"/>
    <display:setProperty name="paging.banner.items_name" value="users"/>

    <display:setProperty name="export.excel.filename" value="User List.xls"/>
    <display:setProperty name="export.csv.filename" value="User List.csv"/>
    <display:setProperty name="export.pdf.filename" value="User List.pdf"/>
</display:table>


<script type="text/javascript">
    highlightTableRows("users");
</script>
<c:out value="${buttons}" escapeXml="false" />
</s:form>
<script language="javascript">
try{tickBox();}
catch(e){}
</script>
</body>
