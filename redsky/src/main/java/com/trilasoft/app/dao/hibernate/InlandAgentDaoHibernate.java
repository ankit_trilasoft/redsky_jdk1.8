package com.trilasoft.app.dao.hibernate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.InlandAgentDao;
import com.trilasoft.app.model.InlandAgent;
import com.trilasoft.app.model.ServiceOrder;

public class InlandAgentDaoHibernate extends GenericDaoHibernate<InlandAgent, Long> implements InlandAgentDao {

	public InlandAgentDaoHibernate() {
		super(InlandAgent.class);
		// TODO Auto-generated constructor stub
	}
	
	public List getByShipNumber(String shipNumber) {
		return getHibernateTemplate().find("from InlandAgent where shipNumber=? order by idNumber",shipNumber);
	}
	public List<InlandAgent> findByInLandAgentList(String shipNumber) {
		return getHibernateTemplate().find("from InlandAgent where shipNumber=? order by idNumber", shipNumber);
	}
	public List findMaximumChild(String shipNm) {
		return getHibernateTemplate().find("select max(id) from InlandAgent where   shipNumber=?", shipNm);
	}

	public List findMinimumChild(String shipNm) {
		return getHibernateTemplate().find("select min(id) from InlandAgent where   shipNumber=?", shipNm);
	}
	public List findCountChild(String shipNm) {
		return getHibernateTemplate().find("select count(*) from InlandAgent where  shipNumber=?", shipNm);
	}
	public List findMaximumIdNumber(String shipNum) {
    	List list=  this.getSession().createSQLQuery("select max(idNumber) from inlandagent where shipNumber='"+shipNum+"' ").list();
    	return list;
    }

    public List findMaxId() {
		return getHibernateTemplate().find("select max(id) from InlandAgent");
	}
    public void updateInlandAgentDetailsFromList(Long id,String fieldName,String fieldValue,String sessionCorpID,String shipNumber, String userName,String fieldType){
    		String query="";
    		String actualDate="";
    		try{
    			if (fieldValue!=null && fieldType.equals("D")) {
    				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
		              Date du = new Date();
		              du = df.parse(fieldValue);
		              df = new SimpleDateFormat("yyyy-MM-dd");
		              actualDate = df.format(du);
    	 			fieldValue=actualDate;
    	 			}
    			
    		if(fieldName!=null && !fieldName.equalsIgnoreCase("") && !fieldType.equals("B")){
    				query="update inlandagent set "+fieldName+"='"+fieldValue+"', updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' and shipNumber='"+shipNumber+"' and corpId='"+sessionCorpID+"' ";
    		}
    		if(fieldName!=null && !fieldName.equalsIgnoreCase("") && fieldType.equals("B")){
				query="update inlandagent set "+fieldName+"="+fieldValue+", updatedOn=now(),updatedby='"+userName+"' where id ='"+id+"' and shipNumber='"+shipNumber+"' and corpId='"+sessionCorpID+"' ";
    		}
    		this.getSession().createSQLQuery(query).executeUpdate();
    		}catch(Exception e){
    			e.printStackTrace();
    		}
    }
    public List getContainerNumber(String shipNumber) {
		return getHibernateTemplate().find("select containerNumber from Container where status = true and  shipNumber=? and containerNumber is not null",shipNumber);
	}

}
