package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.ForwardingMarkUpControl;

public interface ForwardingMarkUpManager extends GenericManager<ForwardingMarkUpControl, Long>{
	 public List checkById(Long id);
}
