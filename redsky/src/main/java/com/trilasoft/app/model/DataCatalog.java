
//Created by Bibhash Kumar Pathak
package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


@Entity
@Table(name="datacatalog")

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class DataCatalog extends BaseObject{
	private Long id;  
	private String corpID;
    public String tableName;   
    public String fieldName;
    public String defineByToDoRule;
    public String isdateField;
    public String description;
    public String rulesFiledName;
    public String auditable;
    public String visible;
    public String charge;
    
    private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Date valueDate;
	private String usDomestic;
	private String referenceTable;
	private String parameterLinkingCode;	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("tableName", tableName).append("fieldName",
				fieldName).append("defineByToDoRule", defineByToDoRule).append(
				"isdateField", isdateField).append("description", description)
				.append("rulesFiledName", rulesFiledName).append("auditable",
						auditable).append("visible", visible).append("charge",
						charge).append("createdBy", createdBy).append(
						"updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("valueDate", valueDate).append("usDomestic",usDomestic).append("referenceTable",referenceTable).append("parameterLinkingCode",parameterLinkingCode)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DataCatalog))
			return false;
		DataCatalog castOther = (DataCatalog) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(tableName, castOther.tableName)
				.append(fieldName, castOther.fieldName).append(
						defineByToDoRule, castOther.defineByToDoRule).append(
						isdateField, castOther.isdateField).append(description,
						castOther.description).append(rulesFiledName,
						castOther.rulesFiledName).append(auditable,
						castOther.auditable).append(visible, castOther.visible)
				.append(charge, castOther.charge).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(valueDate,
						castOther.valueDate).append(usDomestic,castOther.usDomestic).append(referenceTable,castOther.referenceTable).append(parameterLinkingCode,castOther.parameterLinkingCode).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(tableName).append(fieldName).append(defineByToDoRule)
				.append(isdateField).append(description).append(rulesFiledName)
				.append(auditable).append(visible).append(charge).append(
						createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).append(valueDate).append(usDomestic).append(referenceTable).append(parameterLinkingCode).toHashCode();
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column( length=25 )
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	
	@Column( length=50 )
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column( length=50 )
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column( length=25 )
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column( length=25)
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	@Column( length=5000)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column( length=100)
	public String getRulesFiledName() {
		return rulesFiledName;
	}
	public void setRulesFiledName(String rulesFiledName) {
		this.rulesFiledName = rulesFiledName;
	}
	@Column( length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length=5)
	public String getAuditable() {
		return auditable;
	}
	public void setAuditable(String auditable) {
		this.auditable = auditable;
	}
	
	@Column( length=5)
	public String getDefineByToDoRule() {
		return defineByToDoRule;
	}
	public void setDefineByToDoRule(String defineByToDoRule) {
		this.defineByToDoRule = defineByToDoRule;
	}
	@Column( length=5)
	public String getIsdateField() {
		return isdateField;
	}
	public void setIsdateField(String isdateField) {
		this.isdateField = isdateField;
	}
	@Column( length=25)
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	@Column( length=5)
	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	@Column( length=40)
	public String getReferenceTable() {
		return referenceTable;
	}
	public void setReferenceTable(String referenceTable) {
		this.referenceTable = referenceTable;
	}
	@Column( length=40)
	public String getParameterLinkingCode() {
		return parameterLinkingCode;
	}
	public void setParameterLinkingCode(String parameterLinkingCode) {
		this.parameterLinkingCode = parameterLinkingCode;
	}
	@Column( length=50)
	public String getUsDomestic() {
		return usDomestic;
	}
	public void setUsDomestic(String usDomestic) {
		this.usDomestic = usDomestic;
	}
	
	}
	
	


