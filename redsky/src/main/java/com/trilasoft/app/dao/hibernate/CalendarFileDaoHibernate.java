package com.trilasoft.app.dao.hibernate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CalendarFileDao;
import com.trilasoft.app.model.CalendarFile;

public class CalendarFileDaoHibernate extends GenericDaoHibernate<CalendarFile, Long> implements CalendarFileDao {

	public CalendarFileDaoHibernate() {   
        super(CalendarFile.class);   
    } 
	
	public List findByUserName(String corpID,String user){
    	return getHibernateTemplate().find("from CalendarFile where userName like '"+user+"%' OR userName like '"+corpID+"%' ");
    }
	
	public List getUserFormSysDefault(String usrName){
		return getHibernateTemplate().find("select CONCAT(workStartTime,'#',workEndTime) from User where username = ?", usrName);
	}
	private StringBuilder newSurveyFromBuilder;
	private StringBuilder newSurveyToBuilder;
	private String newSurveyFrom;
	private String newSurveyTo;
	public List searchMyEvents(String sessionCorpID,String consult,String surveyFrom,String surveyTo){
    	List list1 = new ArrayList();
		if(!surveyFrom.equalsIgnoreCase("")){
		    try {
		    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		    	newSurveyFromBuilder = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(surveyFrom) ));
		        newSurveyFrom = newSurveyFromBuilder.toString();
		        //System.out.println(newSurveyFromBuilder.toString());
			} catch (ParseException e) {
				
				e.printStackTrace();
			}   
			}else{
				newSurveyFrom = "";
			}
		if(!surveyTo.equalsIgnoreCase("")){
		    try {
		    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		    	newSurveyToBuilder = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(surveyTo) ));
		        newSurveyTo = newSurveyToBuilder.toString();
		        //System.out.println(newSurveyToBuilder.toString());
			} catch (ParseException e) {
				
				e.printStackTrace();
			}   
			}else{
				newSurveyTo = "";
			}
		if(!newSurveyFrom.equalsIgnoreCase("") && !newSurveyTo.equalsIgnoreCase("")){
			list1 = getHibernateTemplate().find("From CalendarFile Where (userName like '"+consult.replaceAll("'", "''")+"%' or userName like '"+sessionCorpID+"') AND (surveyDate BETWEEN '"+newSurveyFrom+"' and '"+newSurveyTo+"')");
		}else{
			if(newSurveyFrom.equalsIgnoreCase("") && !newSurveyTo.equalsIgnoreCase("")){
				list1 = getHibernateTemplate().find("From CalendarFile Where (userName like '"+consult.replaceAll("'", "''")+"%' or userName like '"+sessionCorpID+"') AND (surveyDate <= '"+newSurveyTo+"')");
			}
			if(!newSurveyFrom.equalsIgnoreCase("") && newSurveyTo.equalsIgnoreCase("")){
				list1 = getHibernateTemplate().find("From CalendarFile Where (userName like '"+consult.replaceAll("'", "''")+"%' or userName like '"+sessionCorpID+"') AND (surveyDate >= '"+newSurveyFrom+"')");
			}
			if(newSurveyFrom.equalsIgnoreCase("") && newSurveyTo.equalsIgnoreCase("")){
				list1 = getHibernateTemplate().find("From CalendarFile Where (userName like '"+consult.replaceAll("'", "''")+"%' or userName like '"+sessionCorpID+"')");
			}
		}
		return list1;
    }
	public List getDriverListDataSummary(String sessionCorpID){
		return getHibernateTemplate().find("from Partner where isOwnerOp is true and partnerCode is not null and partnerCode!='' and status='Approved' and noDispatch is false ");
	}
	public String driverDetailCode(String myDriverCode){
		String dName="";
		List dList= getHibernateTemplate().find("select CONCAT(firstName,'~',lastName) from PartnerPublic where isOwnerOp is true and partnerCode = ?", myDriverCode);
		if((dList!=null) && (!dList.isEmpty()))
		{
			dName=dList.get(0).toString();
		}
		return dName;
	}
}
