package com.trilasoft.app.dao.hibernate;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.RefJobDocumentTypeDao;
import com.trilasoft.app.model.RefJobDocumentType;


public class RefJobDocumentTypeDaoHibernate extends GenericDaoHibernate<RefJobDocumentType, Long> implements RefJobDocumentTypeDao {   

    public RefJobDocumentTypeDaoHibernate() {   
        super(RefJobDocumentType.class);   
    } 
    public List findRefJobDocumentList(String corpID){
    	return getHibernateTemplate().find("from RefJobDocumentType where corpID=?",corpID);
    }
    public List searchRefJobDocument(String jobType,String corpID){
    	return getHibernateTemplate().find("from RefJobDocumentType where corpID='"+corpID+"' and jobType = '"+jobType+"'");
    }
    public String getAllDoument(String jobType,String corpID)
    {
    	
    	List docList=getHibernateTemplate().find("select refJobDocs from RefJobDocumentType where corpID='"+corpID+"' and jobType = '"+jobType+"'");
    	if(!docList.isEmpty())
    	{
    	 return docList.get(0).toString();
    	}
    	else
    	{
    		return "";
    	}
    }
}