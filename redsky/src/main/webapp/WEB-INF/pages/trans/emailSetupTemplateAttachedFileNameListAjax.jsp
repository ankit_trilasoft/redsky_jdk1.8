<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<s:form id="editEmailTemplatePopupAjaxForm" name="editEmailTemplatePopupAjaxForm">
<s:hidden name="attachedFileNameListPopupEditVal" id="attachedFileNameListPopupEditVal" value="${attachedFileNameListEdit}"/>
<table class="table" style="width:100%; margin:0px;">
<thead>
	<tr>
		<!-- <th width="">#</th> -->
		<th style="min-width: 150px;">Attached File Name</th>
		<th width="50px" class="centeralign">File</th>
		<th width="50px" class="centeralign">Remove</th>
	</tr>
</thead>
<tbody>
<c:if test="${not empty attachedFileNameListEdit}">
	<c:set var = "replacedAttachedFileList" value = "${attachedFileNameListEdit}"/>
	<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,'[', '')}" />
	<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,']', '')}" />
	<c:forTokens items="${replacedAttachedFileList}" delims="^" var="attachedFileItem" varStatus="rowCounter">
		<tr>
		<%-- <td>${rowCounter.count}</td> --%>
			<td><s:textfield cssClass="input-textUpper"	name="fileName" value="${attachedFileItem}" readonly="true" cssStyle="width:90%;background:none;border:none;" />
			</td>
			<td class="txt-center">
				<a><img align="middle" onclick="downloadAttachedFileEdit('${attachedFileItem}','${emailSetupTemplate.id}');"
						style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/download16.png" /></a>
			</td>
			<td class="txt-center">
				<div ALIGN="center">
					<a><img align="middle"
						onclick="deleteAttachedFileName('${attachedFileItem}','${attachedFileNameListEdit}');"
						style="margin: 0px 0px 0px 0px;"
						src="${pageContext.request.contextPath}/images/remove16.png" />
					</a>
				</div>
			</td>	
		</tr>
	</c:forTokens>
</c:if>

</tbody>
</table>
</s:form>