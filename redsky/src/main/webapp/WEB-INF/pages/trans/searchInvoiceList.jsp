<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>
<title>Update Authorization</title>
   
 <script language="javascript" type="text/javascript"> 
  function clear_fields(){ 
			    document.forms['searchForm'].elements['invoiceNum'].value = "";
			   
		        document.forms['searchForm'].elements['authortionNum'].value = "";
		        
}


  function selectSearchField(){ 
		var recInvoiceNumber=document.forms['searchForm'].elements['invoiceNum'].value; 
		var authNumber= document.forms['searchForm'].elements['authortionNum'].value;
		
		if(recInvoiceNumber=='')
		{
			alert('Please enter the Invoice Number!');	
			return false;	
		}
		
		
		
}

function goToSearch(){
	return selectSearchField();
  document.forms['searchForm'].action = 'searchInvoiceList.html';
  document.forms['searchForm'].submit();
}


function findTotalOfInvoice1(shipNumber,invoiceNumber,position)
{ 
var url="findTotalOfInvoice.html?ajax=1&decorator=simple&popup=true&shipNumber=" +shipNumber+ "&recInvoiceNumber="+invoiceNumber; 
  ajax_showTooltip(url,position);	
//openWindow('findTotalOfInvoice.html?shipNumber='+ShipNumber+'&recInvoiceNumber='+InvoiceNumber+'&decorator=popup&popup=true',900,300);
}
</script>     
</head>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-10px;
!margin-top:-18px;
padding:2px;
text-align:right;
width:100%;
!width:100%;
}

div.error, span.error, li.error, div.message {
width:450px;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>   
<s:form id="searchForm" name="searchForm" action="searchInvoiceList" method="post" validate="true" >
<s:hidden name="invoiceLineId" id="invoiceLineId" value=""/>
<div id="layer4" style="width:100%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		

<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
		<table class="table" style="width:100%"  >
		<thead>
		<tr>
		<th><fmt:message key="accountLine.recInvoiceNumber" /> <font color="red" size="2" id="astr">*</font></th>
		<th>Authorization Number <font color="red" size="2" id="astr">*</font></th>
		<th></th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="invoiceNum" required="true" cssClass="input-text" size="30" />
					</td>
					<td width="" align="left">
					    <s:textfield name="authortionNum" required="true" cssClass="input-text" size="30"/>
					</td>
					<td>
					</td>
					</tr>
					<tr>
				<td width="" align="left">
					   <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" method="searchInvoiceList" key="button.search" onclick="return goToSearch();" />   
    
               </td>
					<td width="" align="center">
					<input type="button" class="cssbutton" value="Update" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="updateBulkCompute()"/> 
                    </td>
					<td>
					<input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/>  
					</td>
					</tr>
				</table>
		         
		  
		  
		
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
			
		
<div id="Layer1" style="width:100%" >
	<div id="otabs" style="margin-top:-15px;">
		  <ul>
		    <li><a class="current"><span>Find Our Invoice List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>


 			
	

<display:table name="findInvoiceList" class="table" requestURI="" id="findInvoiceList" style="width:100%; margin-top: 1px;!margin-top: -1px;" defaultsort="1"  >

      <display:column group="1"   title="<input type='checkbox' id='selectall'  style='margin-left:11px; vertical-align:middle;' name='selectall' onclick='selectAll(this);'/>Select&nbsp;All" media="html">
     
   
	<c:if test="${findInvoiceList.recInvoiceNumber != '' && findInvoiceList.recInvoiceNumber != null}">
	 <input type="checkbox" id=${findInvoiceList.recInvoiceNumber} value=${findInvoiceList.recInvoiceNumber} name="inv" class="inv"  onclick="setValues('${findInvoiceList.recInvoiceNumber}',this)"/> 
   
   </c:if>
     </display:column>  
    
     
     <display:column group="1" sortable="true" titleKey="accountLine.recInvoiceNumber" style="width:10%" sortProperty="recInvoiceNumber" paramProperty="recInvoiceNumber" >
     <a onclick="findTotalOfInvoice1('${findInvoiceList.shipNumber}','${findInvoiceList.recInvoiceNumber}',this)"/>
		 
			<c:out value="${findInvoiceList.recInvoiceNumber}" /></a>
	
    
     </display:column>
  <%--  <display:column property="shipNumber" sortable="true" titleKey="accountLine.shipNumber"  style="width:10%" url="/accountLineList.html?sid=${findInvoiceList.sid}&id=${findInvoiceList.id}"/>--%>
    <c:choose>
<c:when test="${newAccountline=='Y'}">
<display:column sortable="true"  title="S/O#" style="width:10%" sortProperty="shipNumber" property="shipNumber">
	   <a href="pricingList.html?sid=${findInvoiceList.sid}&id=${findInvoiceList.id}"><c:out value="${findInvoiceList.shipNumber}" /></a>
     </display:column>
</c:when>
<c:otherwise>
     <display:column sortable="true"  title="S/O#" style="width:10%" sortProperty="shipNumber">
	   <a href="accountLineList.html?sid=${findInvoiceList.sid}&id=${findInvoiceList.id}"><c:out value="${findInvoiceList.shipNumber}" /></a>
     </display:column>
 </c:otherwise>
 </c:choose>    
      <%--
      <display:column property="recInvoiceNumber" sortable="true" titleKey="accountLine.recInvoiceNumber" href="accountLineList.html?sid=${findInvoiceList.sid}"    
       paramId="id" paramProperty="id" style="width:25px"/>   
     --%>
     <display:column  property="lastName" sortable="true"  sortProperty="lastName" style="width:10%" title="Shipper"/>
     <display:column  property="receivedInvoiceDate"  sortable="true" sortProperty="receivedInvoiceDate" titleKey="accountLine.receivedInvoiceDate"  format="{0,date,dd-MMM-yyyy}" style="width:10%"/> 
   	 <display:column  property="billToCode"   sortable="true" sortProperty="billToCode" title="Bill To Code"  style="width:7%"/>
	 <display:column  property="billToName"  sortable="true" sortProperty="billToName" title="Bill To Name"  style="width:10%"/>
	 <display:column property="companyDivision" sortable="true" title="S/O Com.&nbsp;Div." sortProperty="companyDivision" style="width:5%"/>
	 <display:column property="accCompanyDivision" sortable="true" title="Acc. Com.&nbsp;Div." sortProperty="companyDivision" style="width:5%"/>
	 <%--<display:column property="oldBillToCode"   sortable="true" title="Old Code"  style="width:7%"/> --%>
	 <display:column property="totalActualRevenue" headerClass="containeralign" style="text-align: right;width:7%;" sortable="true" sortProperty="totalActualRevenue" titleKey="accountLine.actualRevenue"/>
	 <display:column property="description" sortable="true" sortProperty="description" titleKey="accountLine.description"  style="width:30%"/>
	 <display:column property="paymentStatus" sortable="true" sortProperty="paymentStatus" title="Payment&nbsp;Status"  style="width:5%"/>
	 	<display:column property="authorization" sortable="true" sortProperty="authorization" title="Authorization"  style="width:5%">
	 	
	 	</display:column>
	 
	 
		
</display:table>
<tr>
		<td align="left" class="listwhitetext" style="padding-left:10px;">
				
		
		          <table class="pickList11" style="margin:0px">
		          		
		         
		         
			         
		  
</div>
</div>
</s:form>

<script type="text/javascript"> 
var httpCMMAgent = getHTTPObject7();  
function getHTTPObject7()
{
 var xmlhttp;
 if(window.XMLHttpRequest)
 {
     xmlhttp = new XMLHttpRequest();
 }
 else if (window.ActiveXObject)
 {
     xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
     if (!xmlhttp)
     {
         xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
     }
 }
 return xmlhttp;
}

function setValues(rowId,targetElement){
	   var userCheckStatus = document.getElementById("invoiceLineId").value
	  if(targetElement.checked){
		  if(userCheckStatus == '' ){
			  document.getElementById("invoiceLineId").value = rowId;
			}else{
				if(userCheckStatus.indexOf(rowId)>=0){
				}else{
					userCheckStatus = userCheckStatus + "~" +rowId;
				}
				document.getElementById("invoiceLineId").value = userCheckStatus.replace('~ ~',"~");
			}
	   }else{
			if(userCheckStatus == ''){
				document.getElementById("invoiceLineId").value = rowId;
		     }else{
		    	 var check = userCheckStatus.indexOf(rowId);
		 		if(check > -1){
		 			var values = userCheckStatus.split("~");
		 		   for(var i = 0 ; i < values.length ; i++) {
		 		      if(values[i]== rowId) {
		 		    	 values.splice(i, 1);
		 		    	userCheckStatus = values.join("~");
		 		    	document.getElementById("invoiceLineId").value = userCheckStatus;
		 		      }
		 		   }
		 		}else{
		 			userCheckStatus = userCheckStatus + "~" +rowId;
		 			document.getElementById("invoiceLineId").value = userCheckStatus.replace('~ ~',"~");
		 		}
		     }
	   }
	}

 function updateBulkCompute(){
	  var authorizationNum =document.forms['searchForm'].elements['authortionNum'].value ;
	  var invoiceLineId=document.getElementById("invoiceLineId").value;
	 
	  if(authorizationNum==""){
		  alert("Please enter authorization Number.");
	  }else if(invoiceLineId.trim()==""){
		  alert("Please select at least Invoice Number");
	  }
	 else
		 {
    
	 var agree = confirm("Press OK to proceed, or press Cancel.");
          if(agree){
          	  var url="updateAuthorization.html?ajax=1&decorator=simple&popup=true&authortionNum="+authorizationNum+"&invoiceLineIdNew="+invoiceLineId;
        	
        	    httpCMMAgent.open("GET", url, true);
        	    httpCMMAgent.onreadystatechange = selectiveInvoiceCheckResponse;
        	    httpCMMAgent.send(null); 
 }
           else {
           return false; 
           } 
           
    }}
 function selectiveInvoiceCheckResponse(){
		  if (httpCMMAgent.readyState == 4)
	   { 
			   var results = httpCMMAgent.responseText 
			   results = results.trim();  
			   var recInvoiceNumber=document.forms['searchForm'].elements['invoiceNum'].value; 
			   location.href = 'searchInvoiceList.html?invoiceNum='+recInvoiceNumber;
}  }
 function selectAll(target){
		var inputs = document.getElementsByName("inv");
		var userCheckStatus = document.getElementById("invoiceLineId").value;
		
		for(var i = 0; i < inputs.length; i++) {
			inputs[i].checked = target.checked; 
			if(inputs[i].checked==true)
			{
			if(userCheckStatus == ''){
		    	 userCheckStatus=document.getElementById("invoiceLineId").value = inputs[i].value;
		    }else{
		     	 userCheckStatus=	document.getElementById("invoiceLineId").value = userCheckStatus + '~' + inputs[i].value;  
			}
		}
			else
				{
				 userCheckStatus=document.getElementById("invoiceLineId").value = "";
				}
				}
 }

</script>  
		  	