package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DynamicPricingDao;
import com.trilasoft.app.model.DynamicPricing;

public class DynamicPricingDaoHibernate extends GenericDaoHibernate<DynamicPricing, Long> implements DynamicPricingDao{
	public DynamicPricingDaoHibernate(){
		super(DynamicPricing.class);
	}
	public DynamicPricing getDynamicPricingByType(String type,String sessionCorpID){
		 List dynamicPricing = getHibernateTemplate().find("from DynamicPricing where dynamicPricingType=?", type);
		 DynamicPricing dynamicPricingTemp=null;
		 if(dynamicPricing!=null && !dynamicPricing.isEmpty()){
			 dynamicPricingTemp=(DynamicPricing)dynamicPricing.get(0);
		 }
		 return dynamicPricingTemp;
	}
	public List getDynamicPricingCalenderByType(String type,String sessionCorpID){
		 return getHibernateTemplate().find("from DynamicPricingCalender where dynamicPricingType=? order by currentDate", type);
	}
}
 