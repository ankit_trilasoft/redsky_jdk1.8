package com.trilasoft.app.service;
import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.DsTemporaryAccommodation;;

public interface DsTemporaryAccommodationManager extends GenericManager<DsTemporaryAccommodation, Long>{
	List getListById(Long id);
}

