<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="agentBaseDetails.title"/></title>   
    <meta name="heading" content="<fmt:message key='agentBaseDetails.heading'/>"/>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>


</head> 
<style>
.mainDetailTable {
	width:650px;
}
</style>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="AG" />
<c:set var="ppType" value="AG"/>

<s:form id="agentBaseForm" action="saveBase.html" method="post" validate="true">   
<s:hidden name="partner.id" />
<s:hidden name="agentBase.id" />
<s:hidden name="partnerId" value="${partner.id}"/>
<s:hidden name="agentBase.corpID" />
<s:hidden name="agentBase.partnerCode" />
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:25px"  
        onclick="location.href='<c:url value="/editPartnerRatesForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>

	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.partnerlist' }">
	<c:redirect url="/partnerPublics.html?partnerType=AG"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountdetail' }">
	<c:redirect url="/editPartnerPublic.html?id=${partner.id}&partnerType=AG"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.scacLOIlist' }">
	<c:redirect url="/baseSCACList.html?id=${agentBase.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<div id="newmnav">
				  <ul>
				    <!--
				    <li><a href="editPartnerAddForm.html?id=${partner.id}&partnerType=AG" ><span>Partner Detail</span></a></li>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Rate Matrix<img src="images/navarrow.gif" align="absmiddle" /></span></span></a></li>
					<li><a href="searchPartner.html?partnerType=AG"><span>Partner List</span></a></li>
					<li><a href="baseSCACList.html?id=${agentBase.id}"><span>SCAC/LOI</span></a></li>
					-->
					<li><a onclick="setReturnString('gototab.accountdetail');return checkdate('none');"><span>Agent Detail</span></a></li>
					<li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=AG&id=${partner.id}"><span>Agent Profile</span></a></li>
			  		<c:if test="${sessionCorpID!='TSFT' }">
			  		<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=AG&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
			  		</c:if>
			  		<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=AG"><span>Acct Ref</span></a></li>
					<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=AG"><span>Vanline Ref</span></a></li>
					<li><a href="baseList.html?id=${partner.id}&partnerType=AG"><span>Base List</span></a></li>
					<c:if test="${not empty agentBase.id}"> 
						<li><a href="baseSCACList.html?id=${agentBase.id}"><span>SCAC/LOI</span></a></li>
					</c:if>
					<li><a onclick="setReturnString('gototab.partnerlist');return checkdate('none');"><span>Partner List</span></a></li>
						<%--<c:if test='${partner.latitude == ""  || partner.longitude == "" || partner.latitude == null  || partner.longitude == null}'>
							<li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
						</c:if>
						<c:if test='${partner.latitude != "" && partner.longitude != "" && partner.latitude != null && partner.longitude != null}'>
							<li><a href="partnerRateGrids.html?partnerId=${partner.id}"><span>Rate Matrix</span></a></li>
						</c:if> 
						--%><li><a href="partnerReloSvcs.html?id=${partner.id}&partnerType=AG"><span>Services</span></a></li>
					<c:if test="${partner.partnerPortalActive == true}">
							<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=AG"><span>Portal Users & contacts</span></a></li>
							<configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
							<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=AG&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
					        </configByCorp:fieldVisibility>
					</c:if>
					<li id="newmnav1" style="background:#FFF "><a class="current"><span>Base Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<c:if test="${not empty agentBase.id}">
					<li><a onclick="window.open('auditList.html?id=${agentBase.id}&tableName=agentbase&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li> 
					</c:if>
				  </ul>
		</div>
		<div style="width:100%">
		<div class="spn">&nbsp;</div>
		
		</div>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<table>
					<tbody>
						<tr>
							<td align="left" class="listwhite" colspan="6"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
					  		<td align="right" class="listwhitetext">Partner Code</td>
							<td><s:textfield cssClass="input-textUpper" key="partner.partnerCode" required="true" readonly="true"/></td>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext" width="70px">Partner Name</td>
							<td><s:textfield key="partner.lastName" cssClass="input-textUpper" required="true" size="35" readonly="true"/></td>
						</tr>
						<tr>
							<td align="left" class="listwhite" colspan="6"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='agentBase.baseCode'/></td>
							<td><s:select name="agentBase.baseCode" list="%{military}" cssStyle="width:95px" cssClass="list-menu" onchange="findAgentBaseName()"/></td>
							<!-- 
								<s:textfield cssClass="input-text" name="agentBase.baseCode" size="10" maxlength="10" onchange="findAgentBaseName()"/>
							 	<td><img class="openpopup" width="17" height="20" onclick="changeStatus();openAgentBasePopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							-->
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='agentBase.description'/></td>
							<td><s:textfield readonly="true" cssClass="input-text" name="agentBase.description"  size="50" maxlength="100"/></td>
						</tr>	
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>	
  
<table>
		<tbody>
					<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.createdOn'/></b></td>
							<td valign="top"></td>
							<td style="width:120px">
							<fmt:formatDate var="agentBaseCreatedOnFormattedValue" value="${agentBase.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="agentBase.createdOn" value="${agentBaseCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${agentBase.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.createdBy' /></b></td>
							<c:if test="${not empty agentBase.id}">
								<s:hidden name="agentBase.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{agentBase.createdBy}"/></td>
							</c:if>
							<c:if test="${empty agentBase.id}">
								<s:hidden name="agentBase.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.updatedOn'/></b></td>
							<fmt:formatDate var="agentBaseupdatedOnFormattedValue" value="${agentBase.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="agentBase.updatedOn" value="${agentBaseupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${agentBase.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='agentBase.updatedBy' /></b></td>
							<c:if test="${not empty agentBase.id}">
								<s:hidden name="agentBase.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{agentBase.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty agentBase.id}">
								<s:hidden name="agentBase.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
		</tbody>
</table>               
        <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:60px; height:25px"/>   
        <!--  
        <c:if test="${not empty agentBase.id}"> 
        	<input type="button" class="cssbutton" onclick="openAgentBaseSCAC();" value="Add SCAC/LOI" style="width:100px; height:25px"/> 
        </c:if>
        -->
       
  
  <table><tr><td height="80px"></td></tr></table>
</s:form>

<script language="JavaScript">
function onlyFloatNumsAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39)||(keyCode==110) || (keyCode==109) || ( keyCode==190); 
}
</script>
<script>
function checkdate(clickType){
	if ('${autoSavePrompt}' == 'No'){
			var noSaveAction = '<c:out value="${agentBase.id}"/>';
      		var id = document.forms['agentBaseForm'].elements['agentBase.id'].value;
			var id1 = document.forms['agentBaseForm'].elements['partner.id'].value;
    		
            if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				noSaveAction = 'editPartnerPublic.html?id='+id1+'&partnerType=AG';
			}
			if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					noSaveAction = 'searchPartnerView.html';
				}else{
					noSaveAction = 'partnerPublics.html?partnerType=AG';
				}
			}
			if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.scacLOIlist'){
				noSaveAction = 'baseSCACList.html?id='+id;
			}
			processAutoSave(document.forms['agentBaseForm'], 'saveBase!saveOnTabChange.html', noSaveAction);
	}else{
			var id = document.forms['agentBaseForm'].elements['agentBase.id'].value;
			var id1 = document.forms['agentBaseForm'].elements['partner.id'].value;
    		
            if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AG';
			}
			if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.partnerlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
					location.href = 'searchPartnerView.html';
				}else{
					location.href = 'partnerPublics.html?partnerType=AG';
				}
			}
			if(document.forms['agentBaseForm'].elements['gotoPageString'].value == 'gototab.scacLOIlist'){
				location.href = 'baseSCACList.html?id='+id;
			}
	}	
}

function changeStatus(){
	document.forms['agentBaseForm'].elements['formStatus'].value = '1';
}

function openAgentBasePopWindow(){
	javascript:openWindow('baseAgentPopup.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=agentBase.description&fld_code=agentBase.baseCode');
}

function findAgentBaseName(){
	var baseCode = document.forms['agentBaseForm'].elements['agentBase.baseCode'].value;
    var url="baseAgentName.html?ajax=1&decorator=simple&popup=true&baseCode=" + encodeURI(baseCode);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse4;
    http2.send(null);
}

function handleHttpResponse4(){
		if (http2.readyState == 4){
         var results = http2.responseText
         results = results.trim();
         var res = results.split("#"); 
             if(res.size() >= 2){
 					document.forms['agentBaseForm'].elements['agentBase.description'].value = res[1];
 				}else{
                     alert("Invalid Base Code, please select another");
                    
					 document.forms['agentBaseForm'].elements['agentBase.description'].value="";
					 document.forms['agentBaseForm'].elements['agentBase.baseCode'].value="";
					 document.forms['agentBaseForm'].elements['agentBase.baseCode'].select();
               }
   		}
}

var http2 = getHTTPObject();

function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}


function openAgentBaseSCAC(){
	javascript:openWindow('baseSCACList.html?id=${agentBase.id}&decorator=popup&popup=true',755,500);
}
</script>


<script type="text/javascript">   
    <c:if test="${hitflag == 1}" >
    	<c:redirect url="/baseList.html?id=${partner.id}" ></c:redirect>
	</c:if>  
</script>