/**
 * 
 */
package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.InventoryLocation;
import com.trilasoft.app.model.WorkTicketInventoryData;
;

/**
 * @author GVerma
 *
 */
public interface InventoryLocationDao extends GenericDao<InventoryLocation, Long> {
	public  List getAllLocation(String corpId,String shipNumber);
	public List checkInventoryLocation(String corpID, String locType,String ticket);
}
