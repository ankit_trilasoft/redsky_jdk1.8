package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.appfuse.model.Role;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ReportsDao;
import com.trilasoft.app.model.PrintDailyPackage;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.service.ReportsManager;
  
public class ReportsManagerImpl extends GenericManagerImpl<Reports, Long> implements ReportsManager {   
	ReportsDao reportsDao;   
  
    public ReportsManagerImpl(ReportsDao reportsDao) {   
        super(reportsDao);   
        this.reportsDao = reportsDao;   
    }
    
  
    public List findByModule(String module, String corpID) {   
        return reportsDao.findByModule(module, corpID);   
    }

	public List findByCriteria(String module, String subModule, String menu, String reportName, String description, String corpID, String type){
    	return reportsDao.findByCriteria(module, subModule, menu, reportName, description, corpID, type);
    }
    
    public JasperPrint generateReport(Long reportId, Map parameters) {
		return reportsDao.generateReport(reportId,parameters);
		
	}
	public String getDefaultStationary(String jobNumber, String invoiceNumber,String sessionCorpID){
		return reportsDao.getDefaultStationary(jobNumber,invoiceNumber,sessionCorpID);
	}
    public JasperPrint generateReport(Long reportId) {
		return reportsDao.generateReport(reportId);
		
	}

	public JasperReport getReportTemplate(Long reportId) {
		return reportsDao.getReportTemplate(reportId);
	}
	
	public List findByCorpID(String corpID){
		return reportsDao.findByCorpID(corpID);
	}
	
	public List findByEnabled(String module , String menu, String subModule, String description, String sessionCorpID,String userName, Set<Role> roles,Boolean SalesPortalAccess){
		return reportsDao.findByEnabled( module ,  menu,  subModule,  description,  sessionCorpID,userName, roles,SalesPortalAccess);
	}
	
	public List findByDocsxfer(){
		return reportsDao.findByDocsxfer();
	}
	
	public List findByFormFlag(String userName ,String module , String menu, String subModule, String description, String sessionCorpID){
		return reportsDao.findByFormFlag(userName,module ,  menu,  subModule,  description,  sessionCorpID);
	}
	
	public List findBySubModule(String module, String subModule,String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes) {   
	     return reportsDao.findBySubModule(module, subModule, corpID, enabled, companyDivision, jobType, billToCode, modes);   
	}
	
	public List findBySubModule1(String module, String subModule, String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes,String recInvNumb,String jobNumber){   
	     return reportsDao.findBySubModule1(module, subModule, corpID, enabled, companyDivision, jobType, billToCode, modes,recInvNumb,jobNumber);
	}
	
	public List findByModuleCriteria(String subModule, String module, String menu, String description, String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes,String routingfilter){
		return reportsDao.findByModuleCriteria(subModule, module, menu, description, corpID, enabled, companyDivision, jobType, billToCode, modes,routingfilter);
	}
	
	public List findReportByUserCriteria(String module, String menu, String subModule, String description, String corpID){
    	return reportsDao.findReportByUserCriteria(module, menu, subModule, description, corpID);
	}
	
	public List findFormByUserCriteria(String module, String menu, String subModule, String description, String corpID){
    	return reportsDao.findFormByUserCriteria(module, menu, subModule, description, corpID);
	}
	
	public List findByRelatedModuleCriteria(String subModule, String module, String menu, String description, String corpID, String enabled){
		return reportsDao.findByRelatedModuleCriteria(subModule, module, menu, description, corpID, enabled);
	}


	/*public List findByModuleReports(String corpID) {
		return reportsDao.findByModuleReports(corpID);
	}*/
	
	public Map<String,String> findByModuleReports(String corpID) {
		return reportsDao.findByModuleReports(corpID);
	}


	public List findByModuleCoordinator(String corpID) {
		return reportsDao.findByModuleCoordinator(corpID);
	}
	
	public List findByModuleEstimator(String sessionCorpID) {
		return reportsDao.findByModuleEstimator(sessionCorpID);
	}


	public List getReportsValidationFlag(String sessionCorpID) {
		return reportsDao.getReportsValidationFlag(sessionCorpID);
	}


	public Map<String, String> findByPayableStatus() {
		return reportsDao.findByPayableStatus();
	}
	
	public Map<String, String> findByPayingStatus() {
		return reportsDao.findByPayingStatus();
	}


	public Map<String, String> findByOmni() {
		return reportsDao.findByOmni();
	}

	public List findByMode() {
		return reportsDao.findByMode();
	}
	
	public Map<String,String> findByCommodity(String sessionCorpID) {
		return reportsDao.findByCommodity(sessionCorpID);
	}


	public List findCrewName(String sessionCorpID) {
		return reportsDao.findCrewName(sessionCorpID);
	}
	
	public List findUserRoles(String corpID){
		return reportsDao.findUserRoles(corpID);
	}

	public Map<String, String> findByRouting(String sessionCorpID) {
		return reportsDao.findByRouting(sessionCorpID);
	}

	public Map<String, String> findByBank(String sessionCorpID){
		return reportsDao.findByBank(sessionCorpID);
		
	}

	public void saveReport(Reports reports) {
		reportsDao.saveReport(reports)	;
	}


	public Map<String, String> findByCurrency() {
		return reportsDao.findByCurrency();
	}

	public Map<String, String> findByJob(String sessionCorpID) {
		return reportsDao.findByJob(sessionCorpID);
	}


	public List findByCompanyCode(String sessionCorpID) {
		return reportsDao.findByCompanyCode(sessionCorpID);
	}
	
	public String generateReportExtract(Long reportId, Map parameters) { 
		return reportsDao.generateReportExtract(reportId, parameters);
	}
	public List findContract(String sessionCorpID){
		return reportsDao.findContract(sessionCorpID);
	}
	public Map<String, String> findByBaseScore(String sessionCorpID){
		return reportsDao.findByBaseScore(sessionCorpID);
	}
	public List findBySubModules(String module, String subModule,String corpID, String enabled){
		return reportsDao.findBySubModules(module, subModule, corpID, enabled);
	}
	
	public List findBySubModules1(String module, String subModule,String companyDivision,String jobType,String billToCode, String modes, String corpID, String enabled,Long id,String jobNumber){
		return reportsDao.findBySubModules1(module, subModule, companyDivision, jobType, billToCode, modes, corpID, enabled,id,jobNumber);
	}
	public Map<String, String> findOwner(String corpId, String parameter) {
	 return reportsDao.findOwner(corpId, parameter);
	}
	public Map<String, String> findJobOwner(String corpId, String parameter){
		return reportsDao.findJobOwner(corpId, parameter);
	}
	public List checkBillCodes(String billCode, String corpID){
	 return reportsDao.checkBillCodes(billCode, corpID);
	}
	
	public List checkModes(String modes, String corpID){
	 return reportsDao.checkModes(modes, corpID);
	}
	
	public void updateTableStatus(String modelName,String fieldName,String value,Long sid){
		reportsDao.updateTableStatus(modelName, fieldName, value, sid);
	}
	
	public List getServiceOrderGraph(String sessionCorpID, String jrxmlName){
		return reportsDao.getServiceOrderGraph(sessionCorpID, jrxmlName);
	}
	public void updateTableStatusWithCondition(String modelName,String fieldName,String value,String condition,Long sid){
		reportsDao.updateTableStatusWithCondition(modelName, fieldName, value,condition, sid);
	}
	public void updateTableStatusWithCondition1(String modelName,String fieldName,String value,String condition,String sid){
		reportsDao.updateTableStatusWithCondition1(modelName, fieldName, value,condition, sid);
	}


	public Reports getReportTemplateForWT(String jrxmlName, String module,String corpId) {
		return reportsDao.getReportTemplateForWT(jrxmlName,module, corpId);
	}


	public List<PrintDailyPackage> getByCorpId(String corpId) {
		return reportsDao.getByCorpId(corpId);
	}
	public JasperPrint generateReport(JasperReport jasperReport, Map parameters){
		return reportsDao.generateReport(jasperReport, parameters);
	}


	public List getdescription(String corpId,String module) {
		return reportsDao.getdescription(corpId,module);
	}
	public String getReportCommentByJrxmlName(String jrxmlName,String corpId){
		return reportsDao.getReportCommentByJrxmlName(jrxmlName,corpId);
	}
	public List getReportCommentByModule(String module,String corpId){
		return reportsDao.getReportCommentByModule(module,corpId);
	}
	public List getReportId(String corpId){
		return reportsDao.getReportId(corpId);
		
	}
	public Map<String,String> findByCommodities(String sessionCorpID){
		return reportsDao.findByCommodities(sessionCorpID);
	}	
	public String getBillToCodeByFormName(String jrxmlName,String corpId){
		return reportsDao.getBillToCodeByFormName(jrxmlName,corpId);
	}
	public List findByRedskyCustomer(){
		return reportsDao.findByRedskyCustomer();
	}
	public List findByRedskyCustomerCorp(String RedSkyCustomer){
		return reportsDao.findByRedskyCustomerCorp(RedSkyCustomer);
	}
	public List findCrewType(String sessionCorpID) {
		return reportsDao.findCrewType(sessionCorpID);
	}
	public List findQuotationFormList(String corpID, String companyDivision,String jobType,String billToCode,String modes,String jobNumber){
		return reportsDao.findQuotationFormList(corpID,companyDivision,jobType,billToCode,modes,jobNumber);
	}
	public String billToDefaultStationary(String sessionCorpID,	String invoiceNumber){
		return reportsDao.billToDefaultStationary(sessionCorpID, invoiceNumber);
	}
	public Map<String, String> findRelocationServices(String sessionCorpID) {
		return reportsDao.findRelocationServices(sessionCorpID);
	}
	public List findRelatedSO(String sessionCorpID, String jobNumber, String custID){
		return reportsDao.findRelatedSO(sessionCorpID, jobNumber, custID);
	}
	public List getVanLineCodeCompDiv(String sessionCorpID){
		return reportsDao.getVanLineCodeCompDiv(sessionCorpID);
	}
	public List findContainer(String sessionCorpID, String shipNumber){
		return reportsDao.findContainer(sessionCorpID, shipNumber);	
	}
	public JasperReport getReportTemplate(Long id, String preferredLanguage){
		return reportsDao.getReportTemplate(id, preferredLanguage);
	}
	
	public Map<String,String> findByCompanyDivisions(String sessionCorpID){
		return reportsDao.findByCompanyDivisions(sessionCorpID);
	}
	public List getIdfromMyfileforServiceOrder(String JobNumber,String sessionCorpID,String invoiceNumber,String reportName){
		return reportsDao.getIdfromMyfileforServiceOrder(JobNumber,sessionCorpID,invoiceNumber,reportName);
	}
	
	public String getAgentRoleList(Long reportId){
		return reportsDao.getAgentRoleList(reportId);
	}
	
	public List findAgentGroup(String sessionCorpID){
		return reportsDao.findAgentGroup(sessionCorpID);
	}
	
	public List findBySubModuleFromAccountline(String corpID, String enabled, String companyDivision, String jobType, String billToCode, String modes,String shipNumber){   
	     return reportsDao.findBySubModuleFromAccountline(corpID, enabled, companyDivision, jobType, billToCode, modes,shipNumber);
	}
	public String getDefaultStationaryFromServiceorder(String jobNumber,String sessionCorpID){
		return reportsDao.getDefaultStationaryFromServiceorder(jobNumber,sessionCorpID);
	}
	
	public List findFormsForAutocomplete(String moduleName,String jrxmlName,String corpId){
		return reportsDao.findFormsForAutocomplete(moduleName,jrxmlName,corpId);
	}
	public List findReportId(String jrxmlName,String corpId){
		return reportsDao.findReportId(jrxmlName,corpId);
	}

}