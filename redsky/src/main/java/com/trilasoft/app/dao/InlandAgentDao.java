package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.InlandAgent;

public interface InlandAgentDao extends GenericDao<InlandAgent,Long> {

	public List getByShipNumber(String shipNumber);
	public List<InlandAgent> findByInLandAgentList(String shipNumber);
	public List findMaximumChild(String shipNm);
	public List findMinimumChild(String shipNm);
	public List findCountChild(String shipNm);
	public List findMaximumIdNumber(String shipNum);
    public List findMaxId();
    public void updateInlandAgentDetailsFromList(Long id,String fieldName,String fieldValue,String sessionCorpID,String shipNumber, String userName,String fieldType);
    public List getContainerNumber(String shipNumber);
}
