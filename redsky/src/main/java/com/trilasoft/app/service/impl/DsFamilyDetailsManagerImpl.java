package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DsFamilyDetailsDao;
import com.trilasoft.app.model.DsFamilyDetails;
import com.trilasoft.app.service.DsFamilyDetailsManager;

public class DsFamilyDetailsManagerImpl extends GenericManagerImpl<DsFamilyDetails, Long> implements DsFamilyDetailsManager {
	DsFamilyDetailsDao dsFamilyDetailsDao;
	public DsFamilyDetailsManagerImpl(DsFamilyDetailsDao dsFamilyDetailsDao) {
		super(dsFamilyDetailsDao); 
        this.dsFamilyDetailsDao = dsFamilyDetailsDao; 
	}
	public List getDsFamilyDetailsList(Long id, String sessionCorpID) {
		return	dsFamilyDetailsDao.getDsFamilyDetailsList(id, sessionCorpID);
	} 
	
	public void setCFileFamilySize(Long id, String sessionCorpID){
		dsFamilyDetailsDao.setCFileFamilySize(id, sessionCorpID);
	}
	public Long findRemoteDsFamilyDetails(Long networkId, Long id) {
		return dsFamilyDetailsDao.findRemoteDsFamilyDetails(networkId, id);
	}
	public void deleteNetworkDsFamilyDetails(Long customerFileId, Long networkId) {
		dsFamilyDetailsDao.deleteNetworkDsFamilyDetails(customerFileId, networkId);
		
	}
	public int updateFamilyDetailsNetworkId(Long id) {
		return dsFamilyDetailsDao.updateFamilyDetailsNetworkId(id);
	}
	public String getDsFamilyDetailsBlankFieldId(String ikeaflag,Long cid, String sessionCorpID){
		return	dsFamilyDetailsDao.getDsFamilyDetailsBlankFieldId(ikeaflag,cid, sessionCorpID);
	}
	public Long getDsFamilyIdByRelationship(Long cid,String sessionCorpID,String relationshipVal){
		return	dsFamilyDetailsDao.getDsFamilyIdByRelationship(cid, sessionCorpID,relationshipVal);
	}
	public List findRelationshipList(Long cid,String sessionCorpID){
		return	dsFamilyDetailsDao.findRelationshipList(cid, sessionCorpID);
	}
	public List findSpouseDetails(Long id, String sessionCorpID) {
		return	dsFamilyDetailsDao.findSpouseDetails(id, sessionCorpID);
	}
	public int findRelationshipList1(Long cid, String sessionCorpID,Long id) {
		// TODO Auto-generated method stub
		return	dsFamilyDetailsDao.findRelationshipList1(cid, sessionCorpID, id);
	}
	public int findRelationshipList2(Long cid, String sessionCorpID) {
		// TODO Auto-generated method stub
		return	dsFamilyDetailsDao.findRelationshipList2(cid, sessionCorpID);
	}
}
