package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ContractAccountDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.ContractAccount;

public class ContractAccountDaoHibernate extends GenericDaoHibernate<ContractAccount,Long> implements ContractAccountDao{
	private HibernateUtil hibernateUtil;
	public ContractAccountDaoHibernate(){
		super(ContractAccount.class);
	}
 
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}

	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from ContractAccount");
	}

	public List findContractAccountList(String contract, String corpId) {
		return getHibernateTemplate().find("from ContractAccount where contract='"+contract.replaceAll("'", "''")+"' and corpID='"+corpId+"' and (accountType!='Agent'or accountType is null or accountType='Account')");
	}

	public List isExisted(String accountCode,String contract, String corpId) {
		return getHibernateTemplate().find("from ContractAccount where accountCode='"+accountCode+"' and corpID='"+corpId+"' and contract='"+contract+"'");
	}
	public List findContractAccountByContract(String sessionCorpID){
		String query= "select c.id, x.contract, c.begin, c.ending, count(*) from contractaccount x, contract c where c.corpid = '"+sessionCorpID+"' and x.corpid = '"+sessionCorpID+"' and x.contract = c.contract and (x.accountType!='Agent'or x.accountType is null or x.accountType='Account')  group by x.contract having count(*) > 0 order by x.contract, c.begin ";
		List list= this.getSession().createSQLQuery(query).list();	
		//System.out.println("\n\nQUERY::"+query);
		List contractAccount= new ArrayList();
		ContractAccountDTO contractAccountDTO;
		Iterator it=list.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			
			contractAccountDTO=new ContractAccountDTO();
			contractAccountDTO.setId(obj[0]);
			contractAccountDTO.setContract(obj[1]);
			contractAccountDTO.setBegin(obj[2]);
			contractAccountDTO.setEnding(obj[3]);
			contractAccountDTO.setCount(obj[4]);
			contractAccount.add(contractAccountDTO);
		}
		return contractAccount;
	}
	
	
	public class ContractAccountDTO
	{
		
		private Object id;
		private Object contract;
		private Object corpID;
		private Object begin;
		private Object ending;
		private Object count;
		public Object getBegin() {
			return begin;
		}
		public void setBegin(Object begin) {
			this.begin = begin;
		}
		public Object getContract() {
			return contract;
		}
		public void setContract(Object contract) {
			this.contract = contract;
		}
		public Object getCorpID() {
			return corpID;
		}
		public void setCorpID(Object corpID) {
			this.corpID = corpID;
		}
		public Object getCount() {
			return count;
		}
		public void setCount(Object count) {
			this.count = count;
		}
		public Object getEnding() {
			return ending;
		}
		public void setEnding(Object ending) {
			this.ending = ending;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
	}


	public List validateAccountcode(String accountCode, String sessionCorpID) {
		// TODO Auto-generated method stub
		return getHibernateTemplate().find("select lastName from Partner where partnerCode='"+accountCode+"' ");
	}
	public List findContractAccountRespectToContract(String contract,String sessionCorpID){
		List check = this.getSession().createSQLQuery("select id from contractaccount where contract = '"+contract+"' and corpID='"+sessionCorpID+"'").list(); 
		return check;
	}
	public List findChildCode(String parentCode ,String sessionCorpID){
		List check = this.getSession().createSQLQuery("select concat(if(lastName is null,' ',lastName),'~',if(partnerCode is null,'',partnerCode)) from partnerpublic where agentParent= '"+parentCode+"' and isAccount=true and corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"' ,'TSFT') and status='Approved'").list(); 
		return check;
	}
	public int countPartnerCode(String childCode,String partnerCode,String contract,String sessionCorpID ){
		String temp  = this.getSession().createSQLQuery("select count(*) from contractaccount where accountCode='"+childCode+"'  and contract = '"+contract+"' and  corpID='"+sessionCorpID+"' ").list().get(0).toString();	
	    int countValue=Integer.parseInt(temp);
		return countValue;
	}
	public List findChildAccountCode(String parentCode,String contract,String sessionCorpID){
		List check = this.getSession().createSQLQuery("select concat(if(accountcode is null,' ',accountcode),'~',id) from contractaccount where parentAccountCode= '"+parentCode+"' and contract = '"+contract+"' and corpID='"+sessionCorpID+"'").list(); 
		return check;
	}
	public List checkPublicParentCode(String ChildCode,String partnerCode,String sessionCorpID ){
		List check = this.getSession().createSQLQuery("select * from partnerpublic where agentParent= '"+partnerCode+"' and partnercode='"+ChildCode+"' and isAccount=true and corpID in ('"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"' ,'TSFT') and status='Approved' ").list(); 
		return check;
		
	}
	public void findDeleteContractAccountOfOtherCorpId(String contractName, String agentCorpId){
		 try {
				List contractAccountList = this.getSession().createSQLQuery("select id from contractaccount where contract = '"+contractName+"' and corpID='"+agentCorpId+"'").list(); 
				if(contractAccountList.size()>0){
					String contractAccountId = contractAccountList.toString().replace("[", "").replace("]", "");
					String query = "delete from contractaccount where id in("+contractAccountId+") and corpid='"+agentCorpId+"'";
					this.getSession().createSQLQuery(query).executeUpdate();
				}
			 } catch (Exception e) {
					e.printStackTrace();
			}
	}

	
}
