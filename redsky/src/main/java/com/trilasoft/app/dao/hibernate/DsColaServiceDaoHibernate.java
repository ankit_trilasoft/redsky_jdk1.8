package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsColaServiceDao;
import com.trilasoft.app.model.DsColaService;


public class DsColaServiceDaoHibernate extends GenericDaoHibernate<DsColaService, Long> implements DsColaServiceDao {

	public DsColaServiceDaoHibernate() {
		super(DsColaService.class);
		
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsColaService where id = "+id+" ");
	}

}
