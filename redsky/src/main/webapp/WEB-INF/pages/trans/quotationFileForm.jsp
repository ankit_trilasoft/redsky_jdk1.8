<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>
<head>
<title><fmt:message key="quotationFileDetail.title" /></title>
<meta name="heading" content="<fmt:message key='quotationFileDetail.heading'/>" />

<style type="text/css">
h2 {background-color: #FBBFFF}
.upper-case{text-transform:capitalize;}
.ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;  
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height  */
  * html .ui-autocomplete {
    height: 100px;
  }		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />	
<script type="text/javascript"> 
//# 10219 start
<c:if test="${checkAccessQuotation== true}">
{
<c:redirect url="/customerFiles.html">
</c:redirect>
}
</c:if>
// #10219 end 


function titleCase(element){ 
	var txt=element.value; var spl=txt.split(" "); 
	var upstring=""; for(var i=0;i<spl.length;i++){ 
	try{ 
	upstring+=spl[i].charAt(0).toUpperCase(); 
	}catch(err){} 
	upstring+=spl[i].substring(1, spl[i].length); 
	upstring+=" ";   
	} 
	element.value=upstring.substring(0,upstring.length-1); 	 
}
</script> 

<script type="text/javascript">
function openadd()
{
animatedcollapse.ontoggle=function($, divobj, state){
	
	if (divobj.id=="address"){ //if "peter" DIV is being toggled
				  if (state=="block"){ //if div is expanded
		   animatedcollapse.show("additional");
		}
	 else{
	      animatedcollapse.hide("additional");
		 }
					  
	
	}
}
}
</script>
<script type="text/JavaScript">

function callPostalCode(){
	window.open('http://www.canadapost.ca/cpo/mc/personal/postalcode/fpc.jsf', '_blank');
}
</script>
</head>

<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<s:hidden name="fileID" id ="fileID" value="%{customerFile.id}" />
<c:set var="fileID" value="%{customerFile.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" value="agentQuotes" />
<s:form id="customerFileForm" name="customerFileForm" action="saveQuotationFile" method="post" validate="true" onsubmit ="return submit_form();">
<s:hidden name="defaultcontract" value="${defaultcontract}"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="customerFile.surveyEmailLanguage" />
<s:hidden name="customerFile.cportalEmailLanguage" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="validateFormNav" />
<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
<s:hidden name="quotesToValidate"/>
<s:hidden name="checkConditionForOriginDestin"/>
 <s:hidden name="stdAddDestinState" />
<s:hidden name="stdAddOriginState" />
<s:hidden name="emailSetupValue" />
<s:hidden name="count" />
 <configByCorp:fieldVisibility componentId="component.field.ServiceOrder.checkCompanyBAA">
	<s:hidden name="checkCompanyBA" value="YES"/>
	</configByCorp:fieldVisibility>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
	<c:when test="${gotoPageString == 'gototab.quotaion' }">
	   <c:redirect url="/quotations.html"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.serviceorder' }">
	   <c:redirect url="/quotationServiceOrders.html?id=${customerFile.id}&forQuotation=QC"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.survey' }">
	   <c:redirect url="/surveyDetails.html?cid=${customerFile.id}&Quote=y"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.accountpolicy1' }">
	<c:redirect url="/showAccountPolicyQuotation.html?id=${customerFile.id}&code=${customerFile.billToCode}&jobNumber=${customerFile.sequenceNumber}" />
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose> 
</c:if>
<s:hidden name="reset" id="reset" value=""/>
<s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
<s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
		<s:hidden name="customerFile.id" value="%{customerFile.id}" />
		<s:hidden name="customerFile.isNetworkRecord" />
		<s:hidden name="custJobType" value="<%=request.getParameter("custJobType") %>" />
		<s:hidden name="contractBillCode" value="<%=request.getParameter("contractBillCode") %>" />
		<s:hidden name="custCreatedOn" value="<%=request.getParameter("custCreatedOn") %>" />
		<s:hidden name="custStatus"/>
		<s:hidden name="customerFile.corpID" />
		<s:hidden name="emailSurveyFlag" />
		<s:hidden name="id" value="<%=request.getParameter("id") %>" />
		<s:hidden id="countNotes" name="countNotes" value="<%=request.getParameter("countNotes") %>"/>
		<s:hidden id="countOriginNotes" name="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>"/>
		<s:hidden id="countDestinationNotes" name="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>"/>
		<s:hidden id="countSurveyNotes" name="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>"/>
		<s:hidden id="countVipNotes" name="countVipNotes" value="<%=request.getParameter("countVipNotes") %>"/>
		<s:hidden id="countBillingNotes" name="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<s:hidden id="countSpouseNotes" name="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<s:hidden id="countContactNotes" name="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<s:hidden id="countCportalNotes" name="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<s:hidden id="countEntitlementNotes" name="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<c:set var="countNotes" value="<%=request.getParameter("countNotes") %>" />
		<c:set var="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>" />
		<c:set var="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>" />
		<c:set var="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>" />
		<c:set var="countVipNotes" value="<%=request.getParameter("countVipNotes") %>" />
		<c:set var="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<c:set var="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<c:set var="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<c:set var="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<c:set var="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<s:hidden name="dCountry" />
		<s:hidden name="oCountry" />
		<s:hidden name="customerFile.salesStatus" />
		<s:hidden name="jobNumber" value="%{customerFile.sequenceNumber}"/>
	    <c:set var="jobNumber" value="<%=request.getParameter("sequenceNumber") %>" />
		<s:hidden name="SNumber" value="%{customerFile.sequenceNumber}"/>
		<c:set var="SNumber" value="${customerFile.sequenceNumber}" />
	<%
    String value=(String)pageContext.getAttribute("SNumber") ;
    Cookie cookie = new Cookie("TempnotesId",value);
    cookie.setMaxAge(3600);
    response.addCookie(cookie);
    %>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <c:set var="from" value="<%=request.getParameter("from") %>"/>
	<c:set var="field" value="<%=request.getParameter("field") %>"/>
	<s:hidden name="field" value="<%=request.getParameter("field") %>" />
	<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
	<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
<div id="Layer1"  onkeydown="changeStatus();" style="width:100%;">	
	<c:if test="${not empty customerFile.id}">		
		<div id="newmnav">
		  <ul>
		  	<li id="newmnav1" style="background:#FFF "><a onclick="setReturnString('gototab.quotaion'); return saveAuto('none');" class="current"><span>Quotation File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	<li><a onclick="setReturnString('gototab.serviceorder'); return IsValidTime('saveAuto');"><span>Quotes</span></a></li>
		    <%-- <c:if test="${usertype!='ACCOUNT' && usertype!='AGENT'}">
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    </c:if> --%>
		    <li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&companyDivision=${customerFile.companyDivision}&jobType=${customerFile.job}&billToCode=${customerFile.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=quotation&reportSubModule=quotation&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
            <li><a onclick="window.open('auditList.html?id=${customerFile.id}&tableName=customerfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
             <c:if test="${voxmeIntergartionFlag=='true'}">             
		  <li><a onclick="setReturnString('gototab.survey');return saveAuto('none');"><span>Survey Details</span></a></li>              
             </c:if>
             
		    	    	
		    <li><a onclick="setReturnString('gototab.accountpolicy1');return saveAuto('none');"><span>Account Policy</span></a></li>
		    
          </ul> 
		</div><div class="spn spnSF">&nbsp;</div>
		
	</c:if>	
	
	<c:if test="${empty customerFile.id}">
		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a onclick="setReturnString('gototab.quotaion');return saveAuto('none');" class="current"><span>Quotation File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a><span>Quotes</span></a></li>
		    
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li>
		    <li><a><span>Account Policy</span></a></li>
		  </ul>
		</div><div class="spn spnSF">&nbsp;</div>
	</c:if>
	

<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
		<tbody>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;" border="0">
							<tr>
								<td class="headtab_left">
							</td>
								<td NOWRAP class="headtab_center">&nbsp;Quotation Details
							</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_center">&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>	
				<!--<div class="subcontent-tab" style="height:18px;">Quotation Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
				
				--><table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr><td style="width:50px; height:5px;" colspan="11"></td></tr>
						
						<tr>
							<td align="right" style="width:105px">&nbsp;</td>
							
							<td align="left" class="listwhitetext" valign="bottom" colspan="">Quotation File #</td>
							<td width="10px"></td>	
							<td align="left"></td>
							<td align="right" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.quotationStatus'/></td>
							<td align="right" style="width:10px"></td>							
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.statusDate'/></td>
							<td align="right" style="width:10px"></td>
							<configByCorp:fieldVisibility componentId="component.field.qfStatusReason.showVOER">	
							<td align="left" class="listwhitetext" id="statusReasonId1" style="display:none;">Status&nbsp;Reason<font color="red" size="2">*</font></td>
						    </configByCorp:fieldVisibility>
							<td class="listwhitetext" align="left" valign="bottom"   width="">
							<configByCorp:fieldVisibility componentId="component.field.Alternative.showQuoteToGo">
							<c:if test="${quotesToValidate=='QTG' && customerFile.estimateNumber!='' && customerFile.estimateNumber != null }">
							<c:if test="${not empty customerFile.id}">Quotes To-Go Tracking #</c:if>
							</c:if>
							</configByCorp:fieldVisibility></td>
						</tr>
						<tr>
							<td align="right" style="width:105px">&nbsp;</td>
							<td align="left" class="listwhitetext" valign="top" colspan=""> 
								<s:textfield cssClass="input-textUpper" key="customerFile.sequenceNumber" required="true" cssStyle="width:339px" maxlength="15" readonly="true" onfocus = "myDate();" tabindex="1" /> </td>
							<td width="10px"></td>
							<td align="left" class="listwhitetext" valign="top" style="width:4px">
								<c:if test="${not empty customerFile.controlFlag}">
									<s:hidden  key="customerFile.controlFlag" />
								</c:if>
								<c:if test="${not empty customerFile.familysize}">
									<s:hidden  key="customerFile.familysize" />
								</c:if>
								<c:if test="${empty customerFile.controlFlag}">
									<s:hidden  key="customerFile.controlFlag" value="Q" /> 
								</c:if>	
							</td>	
							<td align="right" style="width:4px"></td>
							<c:if test="${empty customerFile.id}">
							<td align="left" class="listwhitetext" valign="top"><s:select cssClass="list-menu"   name="customerFile.quotationStatus" list="{'New'}" cssStyle="width:120px" onchange="changeStatus();autoPopulate_customerFile_statusDate(this);return lastName();" tabindex="2" /></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">	
							<td align="left" class="listwhitetext" valign="top"><s:select cssClass="list-menu"   name="customerFile.quotationStatus" list="%{orderStatus1}" cssStyle="width:120px" onchange="showStatusReason();changeStatus();autoPopulate_customerFile_statusDate(this);return lastName();" tabindex="2" /></td>
							</c:if>
							
							<td style="width:19px"></td>
							<c:if test="${not empty customerFile.statusDate}">
								<s:text id="customerFileStatusDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.statusDate" /></s:text>
								<td valign="top"><s:textfield cssClass="input-textUpper" name="customerFile.statusDate" value="%{customerFileStatusDateFormattedValue}" cssStyle="width:65px" maxlength="11" tabindex="3"/></td>
							</c:if>
							<c:if test="${empty customerFile.statusDate}">
								<td valign="top"><s:textfield cssClass="input-textUpper"  name="customerFile.statusDate" cssStyle="width:65px" tabindex="3"/></td>
							</c:if>
							<td style="width:10px"></td>
							<configByCorp:fieldVisibility componentId="component.field.qfStatusReason.showVOER">							
							<td align="left" class="listwhitetext" id="statusReasonId" valign="top" style="display:none;"><s:select cssClass="list-menu"   name="customerFile.qfStatusReason" list="%{qfReason}" headerKey="" headerValue="" cssStyle="width:130px"  tabindex="4" /></td>
							</configByCorp:fieldVisibility>
							<td align="left" class="listwhitetext" valign="top" width="30" >
									<configByCorp:fieldVisibility componentId="component.field.Alternative.showQuoteToGo">
									<c:if test="${quotesToValidate=='QTG' && customerFile.estimateNumber!='' && customerFile.estimateNumber != null }">
										<c:if test="${not empty customerFile.id}">
											<s:textfield cssClass="input-text"  name="customerFile.estimateNumber"  size="20" readonly="true" maxlength="20" tabindex="3" />
										</c:if>
									</c:if>
									</configByCorp:fieldVisibility>
									</td>
									 	<c:if test="${not empty customerFile.id}">
						   	<c:if test="${jobTypes !='UVL' && jobTypes !='MVL' && jobTypes !='ULL' && jobTypes !='MLL' && jobTypes !='DOM' && jobTypes !='LOC' && customerFile.originCountryCode != customerFile.destinationCountryCode}">
								<td align="right"><a><img src="images/viewcustom2.jpg" onclick="findCustomInfo(this);" alt="Customs" title="Customs" /></a></td>
						    </c:if>
						    </c:if>
									
						         
						</tr>
					</tbody>
				</table>
				
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr><td style="width:50px; height:5px;"></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td align="right" style="width:105px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.prefix'/></td>
							<td align="right" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.firstName' /></td>
							<td align="right" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.middleInitial'/></td>
							<td align="right" style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.lastName'/></td>
							<td align="right" style="width:10px"></td>
							
							<td align="left" style="width:28px;!width:35px;" class="listwhitetext" >
								<configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber">
									<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.socialSecurityNumber',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.socialSecurityNumber'/></a>
								</configByCorp:fieldVisibility>
							</td>
							
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.vip',this);return false" onmouseout="ajax_hideTooltip()">&nbsp;&nbsp;<fmt:message key='serviceOrder.vip'/>&nbsp;&nbsp;</a></td>
							<td align="right" valign="middle" class="listwhitetext" rowspan="2">
								<c:set var="isVipFlag" value="false"/>
								<c:if test="${customerFile.vip}">
									<c:set var="isVipFlag" value="true"/>
								</c:if>
								<div style="position:absolute;top:65px;margin-left:20px;">
									<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
								</div>
							</td>
						</tr>
						<tr>
							<td style="width:105px">&nbsp;</td>
							<td align="left" class="listwhitetext" valign="top" > <s:select cssClass="list-menu" name="customerFile.prefix" cssStyle="width:50px" list="%{preffix}" headerKey="" headerValue="" onchange="changeStatus();" onfocus = "myDate();" tabindex="4"/></td>
							<td style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text upper-case" name="customerFile.firstName" onblur="titleCase(this)" onkeypress="" onchange="noQuote(this);" required="true"
							cssStyle="width:252px" maxlength="80" onkeydown="return onlyAlphaNumericAllowed(event)" onfocus = "myDate();" tabindex="5" /> </td>
							<td style="width:10px"></td>
							<td align="left" class="listwhitetext" valign="top"> <s:textfield cssClass="input-text" key="customerFile.middleInitial"
							required="true" cssStyle="width:13px" maxlength="1" onkeydown="return onlyCharsAllowed(event)" tabindex="6" /> </td>
							<td style="width:23px"></td>
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text upper-case" key="customerFile.lastName" onblur="titleCase(this)" onkeypress="" onchange="noQuote(this);" required="true"
							cssStyle="width:206px" maxlength="80"  onfocus = "myDate();" tabindex="7" /> </td>
							<td style="width:10px"></td>
							<td align="left" width="25" >
								<configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber">
					        		<s:textfield cssClass="input-text" key="customerFile.socialSecurityNumber" cssStyle="width:54px" maxlength="9" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')" tabindex="7"/>
				        		</configByCorp:fieldVisibility> 
				        	</td>
							<td align="left" width="30px" valign="">&nbsp;&nbsp;<s:checkbox id="customerFile.vip" key="customerFile.vip" value="${isVipFlag}" fieldValue="true" onclick="changeStatus(),setVipImage()" tabindex="8"/></td>
							<c:if test="${empty customerFile.id}">
							<td align="right" style="width:185px;"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countVipNotes == '0' || countVipNotes == '' || countVipNotes == null}">
								<td align="right" style="width:185px;"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" style="width:185px;"><img id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td style="width:105px"></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.originCountry',this);return false" onmouseout="ajax_hideTooltip()">Origin</a></td>
							<td style="width:5px"></td>
							<td valign="bottom"></td>
							<td style="width:15px"></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.destinationCountry',this);return false" onmouseout="ajax_hideTooltip()">Destination</a></td>
							<td style="width:5px"></td>
							<td valign="bottom"></td>
							<td style="width:15px"></td>
							<td align="left" class="listwhitetext">
							<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.customerEmployer',this);return false" onmouseout="ajax_hideTooltip()">
							<fmt:message key='customerFile.customerEmployer'/>
							</a>
							</td>
						</tr>
						<tr>
							<td style="width:105px">&nbsp;</td>
							<td align="left" ><s:textfield cssClass="input-textUpper" name="customerFile.originCityCode" cssStyle="width:117px" maxlength="30" readonly="true"  tabindex="9" /></td>
							<td align="right" style="width:5px"></td>
							<td align="left" ><s:textfield cssClass="input-textUpper"  name="customerFile.originCountryCode" cssStyle="width:35px" maxlength="30" readonly="true" tabindex="10"/></td>
							<td align="right" style="width:15px"></td>
							<td align="left" ><s:textfield cssClass="input-textUpper" name="customerFile.destinationCityCode" cssStyle="width:115px" maxlength="30" readonly="true" tabindex="11"/></td>
							<td align="right" style="width:5px"></td>
							<td align="left" ><s:textfield cssClass="input-textUpper" name="customerFile.destinationCountryCode" cssStyle="width:35px" maxlength="30" readonly="true" tabindex="12"/></td>
							<td align="right" style="width:23px"></td>
							<td align="left">							
							<s:textfield cssClass="input-textUpper" key="customerFile.customerEmployer" cssStyle="width:275px" readonly="true" tabindex="13"/>		
							 </td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="1">
					<tbody>
						<tr>
							<td style="width:105px"></td>
							<td align="left" class="listwhitetext" >Code<font color="red" size="2">*</font></td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:100px">Name</td>							
							<td style="width:5px"></td>						
							<c:if  test="${compDivFlag == 'Yes'}">
							<td align="left" class="listwhitetext" >Company&nbsp;Division<font color="red" size="2">*</font></td>
							<td style="width:5px"></td>
							</c:if>
							<td align="left" class="listwhitetext" style="width:65px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.bookingDate',this);return false" onmouseout="ajax_hideTooltip()">Booking Date</a></td>
							<td align="left"></td>
							
							<td style="width:5px"></td>
							<td align="right" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.initialContactDate',this);return false" onmouseout="ajax_hideTooltip()">Initial Contact</a></td>
							
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:105px">Booking Agent:</td>
							<td><s:textfield cssClass="input-text" id="quotationBookingAgentCodeId" name="customerFile.bookingAgentCode" cssStyle="width:65px" maxlength="8" onchange="valid(this,'special');findBookingAgentName();findCompanyDivisionByBookAg();" tabindex="14"   /></td>
							<td><img id="customerfile.bookingAgentPopUp" class="openpopup" width="17" height="20" onclick="openBookingAgentPopWindow();document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left"><s:textfield cssClass="input-text" id="quotationBookingAgentNameId" name="customerFile.bookingAgentName" cssStyle="width:247px;" maxlength="250"   onkeyup="findPartnerDetails('quotationBookingAgentNameId','quotationBookingAgentCodeId','quotationBookingAgentNameDivId',' and ((isAccount=true or isAgent=true) or (isVendor=true and typeOfVendor=\\'Agent Broker\\'))','',event);" onchange="findPartnerDetailsByName('quotationBookingAgentCodeId','quotationBookingAgentNameId');"   tabindex="15"/>
							<div id="quotationBookingAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
							<c:if  test="${compDivFlag == 'Yes'}">
							<td style="width:19px"></td>							
							<td align="left"><s:select cssClass="list-menu" id="companyDivision" name="customerFile.companyDivision" list="%{companyDivis}"  headerKey="" headerValue="" cssStyle="width:100px" onchange="getJobList();getContract();changeStatus();getNewCoordAndSale('change')" tabindex="16" /></td>
							</c:if>
							<c:if  test="${compDivFlag != 'Yes'}">
							<s:hidden name="customerFile.companyDivision"/>
							</c:if>					
							<td style="width:5px"></td>							 
							<c:if test="${not empty customerFile.bookingDate}">
								<s:text id="customerFileBookingFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.bookingDate"/></s:text>
								<td><s:textfield id="bookingDate" cssClass="input-text" name="customerFile.bookingDate" value="%{customerFileBookingFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"  /></td>
								<td><img id="bookingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.bookingDate}">
								<td><s:textfield id="bookingDate" cssClass="input-text" name="customerFile.bookingDate" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
								<td><img id="bookingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:5px"></td>
							<c:if test="${not empty customerFile.initialContactDate}">
							<s:text id="customerFileInitialFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.initialContactDate"/></s:text>
							<td><s:textfield id="initialContactDate"  cssClass="input-text" name="customerFile.initialContactDate" value="%{customerFileInitialFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td><td><img id="initialContactDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.initialContactDate}">
							<td><s:textfield id="initialContactDate" cssClass="input-text" name="customerFile.initialContactDate" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"  /></td><td><img id="initialContactDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr><td style="width:50px; height:5px;"></td></tr>	
						<tr>
							<td style="width:105px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.job'/><font color="red" size="2">*</font></td>
							<td style="width:15px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.coordinator'/></td>
							<td style="width:15px"></td>
							<td align="left"></td>
						</tr>
						<tr>
							<td style="width:105px">&nbsp;</td>
							<td align="left"><s:select id="jobQuote" cssClass="list-menu" name="customerFile.job"  list="%{job}" cssStyle="width:345px" headerKey="" headerValue="" onchange="setCountry();setStatas();getContract(); changeStatus();getNewCoordAndSale('change');" tabindex="19" /></td>
							<td style="width:22px"></td>
							<td align="left"><s:select cssClass="list-menu" name="customerFile.coordinator" list="%{coordinatorList}" id="coordinator" cssStyle="width:179px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="20" /></td>
							<td style="width:22px"></td>
							<td align="left"><s:hidden  name="customerFile.salesMan"   /></td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0">
					<tbody>
						<tr><td style="width:50px; height:5px;"></td></tr>						
						<tr>
							<td style="width:105px"></td>
							<td align="left" class="listwhitetext">
							<a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.comptetive',this);return false" onmouseout="ajax_hideTooltip()">
							<fmt:message key='customerFile.comptetive'/>
							</a>
							</td>
							<td style="width:15px"></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.sourceCode',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.sourceCode'/></a></td>
							<td style="width:15px"></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.moveDate',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.moveDate'/></a></td>
						</tr>
						<tr>
							<td style="width:105px"></td>
							<td align="left"><s:select cssClass="list-menu" cssStyle="width:60px" name="customerFile.comptetive" list="{'Y','N'}" onchange="changeStatus();" tabindex="21" /></td>
							<td style="width:15px"></td>
							<td><s:select cssClass="list-menu" name="customerFile.source" list="%{lead}" cssStyle="width:269px" headerKey="" headerValue="" onchange="changeStatus();"  tabindex="22" /></td>
							<td style="width:22px"></td>
							<c:if test="${not empty customerFile.moveDate}">
							<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
							<td align="left" width="30px"><img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.moveDate}">
							<td><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"  /></td>
							<td width="30px">&nbsp;<img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<!-- <td colspan="4"></td> -->							
							<c:if test="${empty customerFile.id}">
										<td align="right" width="390px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countNotes == '0' || countNotes == '' || countNotes == null}">
									<td align="right" class="listwhitetext" width="390px"><img id="countNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
									<td align="right" class="listwhitetext" width="390px"><img id="countNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Customer&imageId=countNotesImage&fieldId=countNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tbody>
						<tr><td style="width:50px; height:5px;"></td></tr>					
							<tr><td align="center" colspan="15" class="vertlinedata"></td></tr>											
					</tbody>
				</table>
				
				
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr><td style="width:100px; height:5px;" colspan="10"></td></tr>
						<tr>
							<td style="width:105px">&nbsp;</td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.estimator'/></td>
							<td style="width:10px"></td>
							<td align="left" class="listwhitetext" colspan="4" ><fmt:message key='customerFile.survey'/></td>
							<td style="width:3px" colspan="8"></td>
							<td></td>
							<td align="left" class="listwhitetext">Add to Cal.</td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:105px">Pre Move Survey:&nbsp;</td>
							<td width="130"><s:select cssClass="list-menu" name="customerFile.estimator" list="%{sale}" id="estimator" cssStyle="width:175px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="24" /></td>
							<td style="width:10px"></td>							
							<td colspan="13">
							<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
							<tr>
							<c:if test="${not empty customerFile.survey}">
							<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
							<td><s:textfield cssClass="input-text" id="survey" name="customerFile.survey" value="%{customerFileSurveyFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" tabindex="25"/></td><td>&nbsp;<img id="survey_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.survey}">
							<td><s:textfield cssClass="input-text" id="survey" name="customerFile.survey" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="survey_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td style="width:16px"></td>
							<td class="listwhitetext"><b>Time</b>
							<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td style="width:3px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
							<td style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.surveyTime" id="surveyTime" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onchange = "completeTimeString();" tabindex="26" /> </td>
							<td style="width:10px"></td>
							<td style="width:0px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime2'/></td>
							<td style="width:3px"></td>
							<td width="90px"><s:textfield cssClass="input-text" name="customerFile.surveyTime2" id="surveyTime2" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onchange = "completeTimeString();" tabindex="26" /> <img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" > </td>
							
							</tr>
							</table>
							</td>
							<td class="listwhitetext" width="210" align="left">
							<img src="${pageContext.request.contextPath}/images/event_calendar.png"  align="top" onclick="calendarICSFunction();" style="padding-left:10px;"></td>	
							<td>
							</td>
						<c:if test="${empty customerFile.id}">
							<td align="right" style="width:125px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
								<c:choose>
									<c:when test="${countSurveyNotes == '0' || countSurveyNotes == '' || countSurveyNotes == null}">
									<td align="right" style="width:125px"><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
									</c:when>
									<c:otherwise>
									<td align="right" style="width:125px"><img id="countSurveyNotesImage"src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
									</c:otherwise>
								</c:choose> 
							</c:if>
							</tr>
							<tr><td height="5"></td></tr>
							<configByCorp:fieldVisibility componentId="component.field.prefSurveyDate.showCorpId">
						<tr>
						<td>
						</td>
						<td colspan="20">
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
						<tr>
						<td style="width:131px">&nbsp;</td>
						<td style="width:10px"></td>
							<%--  <c:if test="${customerFile.job !='RLO'}"> --%>
							<td align="left" class="listwhitetext" colspan="13"><fmt:message key='customerFile.prefSurveyDate1'/></td>							 
							<td align="left" class="listwhitetext" colspan="3"><fmt:message key='customerFile.prefSurveyDate2'/></td>
							<td align="left"></td>
							<%-- </c:if> --%>
				        </tr>
						<tr>
						<td style="width:100px">&nbsp;</td>
						<td style="width:10px"></td>
							 <%-- <c:if test="${customerFile.job !='RLO'}"> --%>
						<c:if test="${not empty customerFile.prefSurveyDate1}">
							<s:text id="customerFilePrefSurveyFormattedValue1" name="${FormDateValue}"><s:param name="value" value="customerFile.prefSurveyDate1"/></s:text>
							<td><s:textfield cssClass="input-text" id="prefSurveyDate1" name="customerFile.prefSurveyDate1" value="%{customerFilePrefSurveyFormattedValue1}" cssStyle="width:60px" maxlength="11" onkeydown="return onlyDel(event,this)" tabindex="25"/></td><td>&nbsp;<img id="prefSurveyDate1_trigger" style="vertical-align:bottom;padding-right:9px;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.prefSurveyDate1}">
							<td><s:textfield cssClass="input-text" id="prefSurveyDate1" name="customerFile.prefSurveyDate1" cssStyle="width:60px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="prefSurveyDate1_trigger" style="vertical-align:bottom;padding-right:9px;" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>							
							
							
							<td style="width:10px"></td>
							<td class="listwhitetext" ><b>Time</b>
							<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td style="width:5px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
							<td style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.prefSurveyTime1" id="prefSurveyTime1" size="3" maxlength="5"  onkeydown="return onlyTimeFormatAllowed(event)" onchange = "completeTimeString();"   /> </td>
							
							<td style="width:10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.surveyTime2'/></td>
							<td style="width:3px"></td>
							<td width="90"><s:textfield cssClass="input-text" name="customerFile.prefSurveyTime2" id="prefSurveyTime2" size="3" maxlength="5"   onkeydown="return onlyTimeFormatAllowed(event)" onchange = "completeTimeString();"  /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
							<td align="left"></td>
							
							
							<c:if test="${not empty customerFile.prefSurveyDate2}">
							<s:text id="customerFilePrefSurveyFormattedValue2" name="${FormDateValue}"><s:param name="value" value="customerFile.prefSurveyDate2"/></s:text>
							<td><s:textfield cssClass="input-text" id="prefSurveyDate2" name="customerFile.prefSurveyDate2" value="%{customerFilePrefSurveyFormattedValue2}" cssStyle="width:60px" maxlength="11" onkeydown="return onlyDel(event,this)" tabindex="25"/></td><td>&nbsp;<img id="prefSurveyDate2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.prefSurveyDate2}">
							<td><s:textfield cssClass="input-text" id="prefSurveyDate2" name="customerFile.prefSurveyDate2" cssStyle="width:60px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="prefSurveyDate2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							 <td style="width:10px"></td>
							<td class="listwhitetext" ><b>Time</b>
							<img src="${pageContext.request.contextPath}/images/clock.png" HEIGHT=12 WIDTH=12 align="top" style="cursor:default;" ></td>
							<td style="width:5px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='customerFile.surveyTime'/></td>
							<td style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.prefSurveyTime3" id="prefSurveyTime3" size="3" maxlength="5"  onkeydown="return onlyTimeFormatAllowed(event)" onchange = "changeStatus();completeTimeString();" /> </td>
							<td style="width:10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key='customerFile.surveyTime2'/></td>
							<td style="width:3px"></td>
							<td><s:textfield cssClass="input-text" name="customerFile.prefSurveyTime4" id="prefSurveyTime4" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onchange = "changeStatus();completeTimeString();" /><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >  </td>
							<td align="left"></td> 
						<%-- </c:if> --%>
						</tr>
						</table>
						</td>
						</tr>						
						</configByCorp:fieldVisibility>
						
						<tr><td style="width:100px; height:5px;" colspan="10"></td></tr>						
						
						<tr>
							<td align="right" class="listwhitetext" style="padding-top:15px">Origin Agent:&nbsp;</td>
							<td align="left" style="padding-top:5px;" colspan="15" >
							<table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0" width="">
							<tr>
							<td align="left" class="listwhitetext">Code</td>
							<td></td>
							<td align="left" class="listwhitetext">Name</td>
							</tr>
							<tr>
							<td><s:textfield cssClass="input-text" key=""  name="customerFile.originAgentCode" id="originAgentCode" cssStyle="width:69px;" maxlength="10" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'hidTSOriginAgent');changeStatus();" onblur="findOriginAgentName();" />
							</td>
							<td align="left" width="24px"><img id="openpopup.img" style="vertical-align:top;" class="openpopup" width="15" height="20" onclick="javascript:winOpenOrigin();document.forms['customerFileForm'].elements['customerFile.originAgentCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left"><s:textfield cssClass="input-text" name="customerFile.originAgentName"  id="originAgentName" cssStyle="width:240px;" maxlength="250" onkeyup="findPartnerDetails('originAgentName','originAgentCode','quotationoriginAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'hidTSOriginAgent');findPartnerDetailsByName('originAgentCode','originAgentName');"  />
							<div id="quotationoriginAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
							<td align="left" class="listwhitetext" width="21px"></td>
							<td>
								 <c:if test="${voxmeIntergartionFlag=='true'}">
								 	<c:if test="${quotesToValidate=='Voxme' }">
								 		<input type="button" style="width:102px;font-size:11px;" value="Send To Voxme" class="cssbuttonB" onclick="sendMailFromVoxme()" />
								 	</c:if>
								 </c:if>
							</td>
							<script type="text/javascript">
							try{
							showPartnerAlert('onload','${customerFile.originAgentCode}','hidTSOriginAgent');
							}catch(e){}
							</script>
							<%-- <td align="left" class="listwhitetext" width="15px"></td><td align="left" class="listwhitetext">Email Status <img class="openpopup" id="openpopup.img1"  onmouseover="emailSetupValue1();" onmouseout="emailSetupValue2();" src="<c:url value='/images/email_small.gif'/>"  />
							</td> --%>
								<c:if test="${count > 0}">
						<td align="left" class="listwhitetext" width="15px"></td><td align="left" class="listwhitetext">Email Status <img class="openpopup" id="openpopup.img1"  src="<c:url value='/images/email_small.gif'/>"  />
								</td>
							<td>
							<c:choose> 
			             <c:when test="${emailSetupValue=='SaveForEmail'}">
					     <img  src="<c:url value='/images/ques-small.gif'/>" align="middle" title="Ready For Sending"/>
			             </c:when>
			             <c:when test="${fn1:indexOf(emailSetupValue,'SaveForEmail')>-1}">
					     <img  src="<c:url value='/images/cancel001.gif'/>" align="middle" title="${fn:replace(emailSetupValue,'SaveForEmail','')}"/>
			             </c:when> 							
			             <c:otherwise>
				           <img  src="<c:url value='/images/tick01.gif'/>" align="middle" title="${emailSetupValue}"/>
			              </c:otherwise>			
		                </c:choose>	
							</td>
							</c:if>
							</tr>
							</table>
							</td>							
							</tr>							
							<tr><td style="width:100px; height:5px;" colspan="10"></td></tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="1">
					<tbody>
						<tr>
							<td style="width:95px">&nbsp;</td>
							<td align="left" class="listwhitetext" colspan="2"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.priceSubmissionToAccDate',this);return false" onmouseout="ajax_hideTooltip()">To&nbsp;Account</a></td>
							
							<td align="left"></td>
							<td style="width:1px"></td>
							<td align="left" class="listwhitetext" colspan="2"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.priceSubmissionToTranfDate',this);return false" onmouseout="ajax_hideTooltip()">To&nbsp;Transferee</a></td>
							
							<td style="width:1px"></td>
							<td align="left" class="listwhitetext" colspan="2"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.priceSubmissionToBookerDate',this);return false" onmouseout="ajax_hideTooltip()">To&nbsp;Booker</a></td>
							
							<td style="width:5px"></td>
							<td align="left" class="listwhitetext" colspan="4"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.quoteAcceptenceDate',this);return false" onmouseout="ajax_hideTooltip()">Quote&nbsp;Acceptance</a></td>
								
							</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:105px">Price Submission:&nbsp;</td>
							<c:if test="${not empty customerFile.priceSubmissionToAccDate}">
							<s:text id="customerFileSubmissionToAccFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToAccDate"/></s:text>
							<td><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="customerFile.priceSubmissionToAccDate" value="%{customerFileSubmissionToAccFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td><td><img id="priceSubmissionToAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToAccDate}">
							<td><s:textfield id="priceSubmissionToAccDate" cssClass="input-text" name="customerFile.priceSubmissionToAccDate" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td><td><img id="priceSubmissionToAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="left"></td>
							<td align="left" style="width:1px"></td>
							<c:if test="${not empty customerFile.priceSubmissionToTranfDate}">
							<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToTranfDate"/></s:text>
							<td><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="customerFile.priceSubmissionToTranfDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td><td>&nbsp;<img id="priceSubmissionToTranfDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToTranfDate}">
							<td><s:textfield id="priceSubmissionToTranfDate" cssClass="input-text" name="customerFile.priceSubmissionToTranfDate" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td><td><img id="priceSubmissionToTranfDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							
							<td style="width:5px"></td>
							<c:if test="${not empty customerFile.priceSubmissionToBookerDate}">
							<s:text id="customerFilepriceSubmissionToBookerFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToBookerDate"/></s:text>
							<td><s:textfield id="priceSubmissionToBookerDate" cssClass="input-text" name="customerFile.priceSubmissionToBookerDate" value="%{customerFilepriceSubmissionToBookerFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
							<td><img id="priceSubmissionToBookerDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToBookerDate}">
							<td><s:textfield id="priceSubmissionToBookerDate" cssClass="input-text" name="customerFile.priceSubmissionToBookerDate" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
							<td><img id="priceSubmissionToBookerDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td style="width:5px"></td>
							<c:if test="${not empty customerFile.quoteAcceptenceDate}">
							<s:text id="customerFileAcceptenceFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.quoteAcceptenceDate"/></s:text>
							<td><s:textfield id="quoteAcceptenceDate" cssClass="input-text" name="customerFile.quoteAcceptenceDate" value="%{customerFileAcceptenceFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
							<td><img id="quoteAcceptenceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.quoteAcceptenceDate}">
							<td><s:textfield id="quoteAcceptenceDate" cssClass="input-text" name="customerFile.quoteAcceptenceDate" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
							<td><img id="quoteAcceptenceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="right" class="listwhitetext" style="width:10px"></td>
							<c:if test="${not empty customerFile.id}">
								<td><input type="button" value="Schedule Survey" class="cssbuttonB" style="width:102px;font-size:11px;" onclick="scheduleSurvey();" tabindex="30"/></td>
							</c:if>
								<c:if test="${empty customerFile.id}">
								<td  align="left" ><input type="button" disabled value="Schedule Survey" style="width:102px;font-size:11px;" class="cssbuttonB" onclick="scheduleSurvey();" tabindex="30"/></td>
							</c:if>
							<td style="width:5px"></td>
							<td align="left" class="listwhitetext" style="width:5px">English</td>
							<c:if test="${customerFile.surveyEmailLanguage==null || customerFile.surveyEmailLanguage==''}">
							<c:set var="ischecked" value="true"/>
							<c:set var="ischecked13" value="false"/>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.customerFileGerman">
							<c:set var="ischecked" value="false"/>
							<c:set var="ischecked12" value="true"/>
							</configByCorp:fieldVisibility>
							</c:if>
							<c:if test="${customerFile.surveyEmailLanguage!=null && customerFile.surveyEmailLanguage!=''}">
								<c:set var="ischecked13" value="false"/>
								<c:set var="ischecked" value="false"/>
								<c:set var="ischecked12" value="false"/>
							<c:set var="splittedString" value="${fn:split(customerFile.surveyEmailLanguage, ',')}" />
								<c:forEach var="lang" items="${splittedString}">
									<c:if test="${fn:indexOf(lang,'ENGLISH')>=0}" >
										<c:set var="ischecked" value="true"/>
									</c:if>
									<c:if test="${fn:indexOf(lang,'GERMAN')>=0}" >
										<c:set var="ischecked12" value="true"/>
									</c:if>
									<c:if test="${fn:indexOf(lang,'DUTCH')>=0}" >
										<c:set var="ischecked13" value="true"/>
									</c:if>
								</c:forEach>
							</c:if>							
															
							<td align="left"><configByCorp:fieldVisibility componentId="component.field.Alternative.showForSelectedCorpId"><s:checkbox name="checkEnglish" value="${ischecked}" fieldValue="true" onclick="changeStatus()" tabindex="31"/></configByCorp:fieldVisibility></td>
							<td align="left"><configByCorp:fieldVisibility componentId="component.field.Alternative.showForBourOnly"><s:checkbox name="checkBourEnglish" value="${ischecked}" fieldValue="true" onclick="changeStatus()" tabindex="31"/></configByCorp:fieldVisibility></td>
							<td align="left" class="listwhitetext" style="width:2px"></td>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.customerFileDutch">
							<td align="left" class="listwhitetext" style="width:10px">Dutch</td>
							</configByCorp:fieldVisibility>
							<td align="left"><configByCorp:fieldVisibility componentId="component.field.Alternative.customerFileDutch"><s:checkbox name="checkDutch" value="${ischecked13}" fieldValue="true" onclick="changeStatus()" tabindex="32"/></configByCorp:fieldVisibility></td>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.customerFileGerman">
							<td align="left" class="listwhitetext" style="width:10px"></td>
							<td align="left" class="listwhitetext" style="width:10px">German</td>
							<td align="left">
							<s:checkbox name="checkGerman" value="${ischecked12}" fieldValue="true" onclick="changeStatus()" />
							</td>
							</configByCorp:fieldVisibility>		
							
							<c:if test="${not empty customerFile.id }">
								<td><input type="button" name="Button1" value="Send Survey Email" style="width:112px;font-size:11px;" class="cssbuttonB" onclick="sendSurveyEmail();" tabindex="33" /></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<td><input type="button" name="Button1" disabled value="Send Survey Email" style="width:112px;font-size:11px;" class="cssbuttonB" onclick="sendSurveyEmail();" tabindex="33"/></td>
							</c:if>
							<configByCorp:fieldVisibility componentId="component.field.Alternative.showQuoteToGo">
							<c:if test="${quotesToValidate=='QTG' && (customerFile.estimateNumber=='' || customerFile.estimateNumber == null)}">
							<s:hidden name="customerFile.quotesToGoFlag"/>
							<s:hidden  name="customerFile.estimateNumber" />
							<c:if test="${not empty customerFile.id}">
								<td class="listwhitetext">
								<img src="${pageContext.request.contextPath}/images/quotes-to-go.png" onclick="quotesToGoMethod();"/>
								</td>
							</c:if>
							</c:if>
							</configByCorp:fieldVisibility>
							<c:if test="${quotesToValidate!='QTG'}">
							<s:hidden name="customerFile.quotesToGoFlag" />
							<s:hidden  name="customerFile.estimateNumber" />
							</c:if>
					<c:set var="ischeckedQtgFlag" value="false"/>
					<c:if test="${customerFile.quotesToGoFlag}">
						<c:set var="ischeckedQtgFlag" value="true"/>
					</c:if>
					<configByCorp:fieldVisibility componentId="component.field.Alternative.showQuoteToGo">
					<c:if test="${quotesToValidate=='QTG' && customerFile.estimateNumber!='' && customerFile.estimateNumber != null}">
					<c:if test="${not empty customerFile.id}">
					<td align="right" class="listwhitetext" style="width:10px"><s:hidden name="customerFile.quotesToGoFlag"/></td>							
					<td align="left" class="listwhitetext" style="">Sent&nbsp;to&nbsp;Quotes-To-Go</td>
					</c:if></c:if></configByCorp:fieldVisibility></tr>
					</tbody>
				</table>				
				</td>
				</tr>
				
				
									<tr>
										<td height="10" width="100%" align="left" >
										<!--<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('address')" style="cursor: pointer"><a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span>&nbsp;&nbsp;Address Details</a></div>
										--><div  onClick="javascript:animatedcollapse.toggle('address'); openadd()" style="margin: 0px">
										<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
										<tr>
										<td class="headtab_left"></td>
										<td NOWRAP class="headtab_center">&nbsp;Address Details	</td>
										<td width="28" valign="top" class="headtab_bg"></td>
										<td class="headtab_bg_center">&nbsp;</td>
										<td class="headtab_right"></td>
										</tr>
										</table>
										</div>
										<div class="dspcont" id="address" style="width: 100%">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
											<tbody>
											<tr><td class="subcontenttabChild" colspan="7"><font color="black"><b>&nbsp;Origin</b></font></td></tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>															
															<tr>
																<td style="width:105px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/></td>
																<td align="left"></td>
																<td style="width:35px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCompany'/></td>
															</tr>
															<tr>
																<td style="width:105px"></td>
																<td align="left"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress1" cssStyle="width:230px" maxlength="100" tabindex="34" /></td>
																<td width="22"><img class="openpopup" width="17" height="20" onclick="openStandardAddressesPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																<td align="left"></td>
																<td align="left"><s:textfield cssClass="input-text" name="customerFile.originCompany" cssStyle="width:282px" maxlength="70" onblur="copyCompanyToDestination(this)" tabindex="35" /></td>
															</tr>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td style="width:105px"></td>
																<td align="left"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress2" cssStyle="width:230px" maxlength="100" tabindex="36" /></td>
																<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address2</font></td>
																<td style="width:70px"></td>
																<td align="left"></td>
															</tr>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td style="width:105px"></td>
																<td ><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress3" cssStyle="width:230px" maxlength="100" tabindex="37" /></td>
																<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address3</font></td>
																<td style="width:70px"></td>
																<td align="left"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
														
															<tr>
																<td style="width:105px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
																<td align="left"></td>
																<td width="57px"></td>
																<td align="left" id="originStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originState' /></td>
												                <td align="left" id="originStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originState' /><font color="red" size="2">*</font></td>
																<td align="left" style="width:5px"></td>
																
															</tr>
															<tr>
																<td style="width:105px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.originCountry" list="%{ocountry}" id="ocountry" cssStyle="width:235px" onblur="getOriginCountryCode(this);" onchange="ostateMandatory();changeStatus();getOriginCountryCode(this);autoPopulate_customerFile_originCountry(this);getState(this);enableStateListOrigin();"  headerKey="" headerValue="" tabindex="38"/></td><td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation();"/></td>																
																<td width="105px"></td>
																<td><s:select cssClass="list-menu" id="originState" name="customerFile.originState" list="%{ostates}" cssStyle="width:286px"  onchange="changeStatus();autoPopulate_customerFile_originCityCode(this); " headerKey="" headerValue="" tabindex="39"/></td>
																<td style="width:5px"></td>
																
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="0">
														<tbody>
															<tr><td style="width:100px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td style="width:105px"></td>
															<td align="left" class="listwhitetext" onmouseover ="gettooltip('customerFile.originCity');" onmouseout="hideddrivetip();"><fmt:message key='customerFile.originCity'/><font color="red" size="2">*</font></td>
																<td style="width:5px"></td>
																<td style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td>																
																</tr>
															<tr>
															<td style="width:105px"></td>
															<td><s:textfield cssClass="input-text upper-case" name="customerFile.originCity" id="originCity" cssStyle="width:137px;" maxlength="30"  onblur="titleCase(this);autoPopulate_customerFile_originCityCode(this,'special');" onkeyup="autoCompleterAjaxCallOriCity();" onkeypress="" tabindex="40"/></td>
																<td align="left" class="listwhitetext" style="width:10px"><div id="zipCodeList" style="vertical-align:middle;"><a><img class="openpopup" id="navigation8" onclick="findCityStateNotPrimary('OZ', this)" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></td>
																<td style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originZip" cssStyle="width:54px;" maxlength="10" onchange="findCityStateOfZipCode(this,'OZ');" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="valid(this,'special');findCityStateOfZipCode(this,'OZ');"  tabindex="41"/></div></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
																<td>
                                                            <li><a id="myUniqueLinkId" onclick="callPostalCode();" onmouseover="" style="cursor: pointer;">&nbsp;&nbsp;&nbsp;&nbsp;<u><strong><font size="2"><spam >Look-Up Canadian Postal Code</spam></font></strong></u></a>
                                                            </li></td>
                                                            </configByCorp:fieldVisibility>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td style="width:105px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originDayPhone'/></td>
																<td align="left"></td>
																<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.originDayPhoneExt'/>&nbsp;</td>
																
																<td style="width:20px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originHomePhone'/><configByCorp:fieldVisibility componentId="component.field.customerFile.phoneNumberMandatory"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
																<td style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originMobile'/><configByCorp:fieldVisibility componentId="component.field.customerFile.phoneNumberMandatory"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
																<td style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originFax'/></td>
															</tr>
															<tr>	
																<td style="width:105px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhone" cssStyle="width:163px;" maxlength="20"  tabindex="42" /></td>
																<td style="width:8px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhoneExt" cssStyle="width:54px;" maxlength="3"  tabindex="43" /></td>
																<td style="width:36px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originHomePhone" cssStyle="width:111px;" maxlength="20"  tabindex="44" /></td>
																<td style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originMobile" cssStyle="width:111px;" maxlength="25"  tabindex="45"/></td>
																<td style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originFax" cssStyle="width:120px;" maxlength="20"  tabindex="46"/></td>
																<td width="10px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td style="width:104px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email'/></td>
																<td style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email2'/></td>
																<td style="width:10px"></td>
																<td align="left"></td>
																<td style="width:10px"></td>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td style="width:10px"></td>
															</tr>
															<tr>
																<td style="width:100px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email" cssStyle="width:231px;" maxlength="65" tabindex="47" onchange="EmailPortDate();" /></td>
																
																<td align="left" class="listwhitetext" style="width:35px">&nbsp;<img class="openpopup" onclick="sendEmail(document.forms['customerFileForm'].elements['customerFile.email'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email}"/></td>																
																<td ><s:textfield cssClass="input-text" name="customerFile.email2" cssStyle="width:215px;" maxlength="65" tabindex="48" onchange="autoPopulateSecondryEmail();" /></td>																																
																
																<td align="center" class="listwhitetext" style="width:22px"><img class="openpopup" onclick="sendEmail(document.forms['customerFileForm'].elements['customerFile.email2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email2}"/></td>																																																
																<td ></td>
																<td style="width:10px"></td>																
																<td ><s:textfield cssClass="input-text" name="customerFile.originPreferredContactTime" cssStyle="width:120px;" maxlength="30" tabindex="48" /></td>
																<td align="left" class="listwhitetext" style="width:14px"></td>
																<c:if test="${empty customerFile.id}">
																<td style="width:200px" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countOriginNotes == '0' || countOriginNotes == '' || countOriginNotes == null}">
																		<td style="width:200px" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="width:200px" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
														</tbody>
													</table>
													</td>
													
												</tr>
												<tr><td style="width:100px; height:5px;" colspan="7"></td></tr>
												<tr>
													<td align="center" colspan="10" class="vertlinedata"></td>
												</tr>
												<tr><td class="subcontenttabChild" colspan="7"><font color="black"><b>&nbsp;Destination</b></font></td></tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext" >
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td style="width:105px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationAddress1'/></td>
																<td align="left"></td>
																<td style="width:70px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCompany'/></td>
															</tr>
															<tr>
																<td style="width:105px"></td>
																<td align="left"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress1" cssStyle="width:230px;" maxlength="100" tabindex="49" /></td>
																<td width="22"><img class="openpopup" width="17" height="20" onclick="openStandardAddressesDestinationPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																<td align="left"></td>
																<td align="left"><s:textfield cssClass="input-text" name="customerFile.destinationCompany" cssStyle="width:282px;" maxlength="70" tabindex="50" /></td>
															</tr>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td style="width:105px"></td>
																<td align="left"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress2" cssStyle="width:230px;" maxlength="100" tabindex="51" /></td>
																<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address2</font></td>
																<td align="left"></td>
																<td style="width:57px"></td>
																<td align="left"></td>
															</tr>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td style="width:105px"></td>
																<td ><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress3" cssStyle="width:230px;" maxlength="100" tabindex="52" /></td>
																<td align="left" class="listwhitetext"><font color="gray">&nbsp;&nbsp;Address3</font></td>
																<td style="width:57px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															
															<tr>
																<td style="width:105px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCountry'/><font color="red" size="2">*</font></td>
																<td align="left"></td>
																<td width="57px"></td>
																<td align="left" id="destinationStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.destinationState' /></td>
												                <td align="left" id="destinationStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.destinationState' /><font color="red" size="2">*</font></td>
																<td style="width:5px"></td>
																
															<tr>
																<td style="width:105px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.destinationCountry" list="%{dcountry}" id="dcountry" cssStyle="width:235px" onblur="getDestinationCountryCode(this);" onchange="state();dstateMandatory();changeStatus();getDestinationCountryCode(this);autoPopulate_customerFile_destinationCountry(this);getDestinationState(this);enableStateListDestin();"  headerKey="" headerValue="" tabindex="53"/></td><td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openDestinationLocation();"/></td>																
																<td width="105px"></td>
																<td><s:select cssClass="list-menu" id="destinationState" name="customerFile.destinationState" list="%{dstates}" cssStyle="width:286px" value="%{customerFile.destinationState}"  onchange="changeStatus();autoPopulate_customerFile_destinationCityCode(this);" headerKey="" headerValue="" tabindex="54"/></td>
																<td style="width:5px"></td>
															
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:100px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td style="width:105px"></td>
													        <td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/><font color="red" size="2">*</font></td>
														    <td style="width:5px"></td>
														    <td style="width:10px"></td>
														    <td align="left"  class="listwhitetext"><fmt:message key='customerFile.destinationZip' /></td>
															</tr>
													       <tr>
													        <td style="width:105px"></td>
													        <td><s:textfield cssClass="input-text upper-case" name="customerFile.destinationCity" id="destinationCity" cssStyle="width:137px;"  maxlength="30"  onblur="titleCase(this);autoPopulate_customerFile_destinationCityCode(this,'special');" onkeyup="autoCompleterAjaxCallDestCity();" onkeypress="" tabindex="55"/></td>
															<td align="left"><div id="zipDestCodeList" style="vertical-align:middle;"><a><img id="navigation9" class="openpopup" onclick="findCityStateNotPrimary('DZ', this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></div></td>
															<td style="width:10px"></td>
															<td><s:textfield cssClass="input-text" name="customerFile.destinationZip" cssStyle="width:54px;"  maxlength="10" onchange="findCityStateOfZipCode(this,'DZ');" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="valid(this,'special');findCityStateOfZipCode(this,'DZ');"  tabindex="56"/></td>
															<td style="width:10px"></td>
															<configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
																<td>
                                                            <li><a id="myUniqueLinkId" onclick="callPostalCode();" onmouseover="" style="cursor: pointer;" >&nbsp;&nbsp;&nbsp;&nbsp;<u><strong><font size="2"><spam >Look-Up Canadian Postal Code</spam></font></strong></u></a>
                                                            </li></td>
                                                            </configByCorp:fieldVisibility>
													
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td style="width:106px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationDayPhone'/></td>
																<td width="8px"></td>
																<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.destinationDayPhoneExt'/>&nbsp;</td>
																<td style="width:20px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationHomePhone'/></td>
																<td style="width:10px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationMobile'/></td>
																<td style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationFax'/></td>
																
															</tr>
															<tr>	
																<td style="width:106px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhone" cssStyle="width:163px;"  maxlength="20"  tabindex="57" /></td>
																<td width="8px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhoneExt" cssStyle="width:54px;"  maxlength="3" tabindex="58" /></td>
																<td style="width:36px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationHomePhone" cssStyle="width:111px;"  maxlength="20"  tabindex="59" /></td>
																<td style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationMobile" cssStyle="width:111px;" maxlength="25"  tabindex="60" /></td>
																<td style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationFax" cssStyle="width:120px;" maxlength="20"  tabindex="61" /></td>
																
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td style="width:105px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail'/></td>
																<td style="width:10px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail2'/></td>
																<td style="width:10px"></td>
																<td align="left"></td>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td style="width:10px"></td>
															</tr>
															<tr>
																<td style="width:100px"></td>
																<td align="left"><s:textfield cssClass="input-text" name="customerFile.destinationEmail" cssStyle="width:231px;" maxlength="65" tabindex="62" /></td>
																<td align="left" class="listwhitetext" style="width:35px">&nbsp;<img class="openpopup" onclick="sendEmail(document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.destinationEmail}"/></td>
																
																<td ><s:textfield cssClass="input-text" name="customerFile.destinationEmail2" cssStyle="width:215px;" maxlength="65" tabindex="63" /></td>
																<td align="center" class="listwhitetext" style="width:22px"><img class="openpopup" onclick="sendEmail(document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.destinationEmail2}"/></td>
																<td style="width:10px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destPreferredContactTime" cssStyle="width:120px;" maxlength="30" tabindex="48" /></td>
																<td ></td>
																<td align="left" class="listwhitetext" style="width:14px"></td>
																<c:if test="${empty customerFile.id}">
																<td style="width:200px" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countDestinationNotes == '0' || countDestinationNotes == '' || countDestinationNotes == null}">
																		<td style="width:200px" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="width:200px" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
															<tr><td style="width:100px; height:5px;" colspan="7"></td></tr>
														</tbody>
													</table>
													</td>
												</tr>
											</tbody>
										</table>
										</div>
										</td>
									</tr>
																		<tr>
	<td height="10" width="100%" align="left" style="margin:0px">
	<div onClick="javascript:animatedcollapse.toggle('additional')" style="margin:0px">
     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left"></td>
<td NOWRAP class="headtab_center">&nbsp;Additional&nbsp;Addresses</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;</td>
<td class="headtab_right"></td>
</tr>
</table>
     </div>
		<div id="additional" class="switchgroup1">
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr><td align="left" colspan="10">
<div style=" width:100%; overflow-x:auto; overflow-y:auto;">
<s:set name="adAddressesDetailsList" value="adAddressesDetailsList" scope="request"/>
<display:table name="adAddressesDetailsList" class="table" requestURI="" id="adAddressesDetailsList"  defaultsort="1" pagesize="10" style="margin:0px 0px 10px 0px; ">
    <display:column title="Description">
    <a href="javascript:openWindow('editAdAddressesDetails.html?id=${adAddressesDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',800,400)">
    <c:out value="${adAddressesDetailsList.description}"></c:out></a>
    </display:column>
    <display:column property="addressType"  title="Address Type" />
    <display:column title="Address" maxLength="15">${adAddressesDetailsList.address1}&nbsp;${adAddressesDetailsList.address2}&nbsp;${adAddressesDetailsList.address3}</display:column>     
    <display:column property="country" title="Country" />
    <display:column property="state" title="State"  /> 
    <display:column property="city"  title="City"  /> 
	<display:column property="phone" title="Phone" />	
	</display:table>
	<c:if test="${not empty customerFile.id}">
	<input type="button" class="cssbutton1" onclick="window.open('editAdAddressesDetails.html?decorator=popup&popup=true&customerFileId=${customerFile.id}','customerFileForm','height=500,width=725,top=0, scrollbars=yes,resizable=yes').focus();" 
	        value="Add Addresses" style="width:110px;" tabindex=""/> 
     </c:if>
     <c:if test="${empty customerFile.id}">
     <input type="button" class="cssbutton1" onclick="massageAddress();"
	     value="Add Addresses" style="width:110px;" tabindex="83"/> 
     </c:if>
</div>
</td></tr> 
</tbody>
</table></div></td></tr>
   <configByCorp:fieldVisibility componentId="component.field.Alternative.spouseForVoerman">									
									<tr>
					<td height="10" width="100%" align="left" style="margin: 0px">
  						<div  onClick="javascript:animatedcollapse.toggle('alternative')" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center">&nbsp;Alternative&nbsp;/&nbsp;Spouse&nbsp;Contact	</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;</td>
					<td class="headtab_right"></td>
					</tr>
					</table>
						</div>
									<%-- <c:if test="${(customerFile.billToCode == '500270') || (empty customerFile.id)}"> --%>
									<!--<tr>
										<td height="10" align="left" class="listwhitetext">
										<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('alternative')">
										<div id="bobcontent2-title" class="handcursor" style="width:100%"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/>&nbsp;&nbsp;Alternative&nbsp;/&nbsp;Spouse&nbsp;Contact</div></div>
										--><div id="alternative" class="switchgroup1">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
												<tbody>
												<tr><td class="subcontenttabChild"><font color="black"><b>&nbsp;At Origin</b></font></td></tr>
													</tbody>
													</table>
														<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0">
														<tbody>												
															<tr><td style="width:50px; height:5px;"></td></tr>														
															<tr>
																<td style="width:93px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactName'/></td>
																<td style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.organization'/></td>
															</tr>
															<tr>
																<td align="left" valign="top" style="width:93px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" id="contactName" name="customerFile.contactName"  cssStyle="width:267px;" maxlength="95" onkeydown="return onlyCharsAllowed(event, this, 'special')" onblur="valid(this,'special')" tabindex="50"/></td>
																<td style="width:23px"></td>
																<td colspan="3"><s:select cssClass="list-menu" name="customerFile.organization" cssStyle="width:219px;" list="%{organiza}" onchange="changeStatus();" tabindex="51"/></td>
															</tr>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" valign="top" style="width:93px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactEmail'/></td>
																<td style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactPhone'/></td>
																<td style="width:15px"></td>
																<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.originDayPhoneExt'/></td>
																<td style="width:15px"></td>
															</tr>
															<tr>
																<td align="left" valign="top" style="width:93px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.contactEmail"  cssStyle="width:267px;" maxlength="65" tabindex="52"/></td>
																<td style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.contactPhone"  cssStyle="width:130px;" maxlength="20"  tabindex="53"/></td>
																<td style="width:15px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.contactNameExt" cssStyle="width:54px;" maxlength="4" onchange="onlyNumeric(this);" tabindex="54"/></td>
																<td style="width:15px"></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>															
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>															
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
												<tbody>
												<tr><tr><td align="center" colspan="10" class="vertlinedata"></td></tr>
												</tr>
												<tr><td class="subcontenttabChild"><font color="black"><b>&nbsp;At Destination</b></font></td></tr>
												<tr><td class="listwhitetext"></td></tr>
													</tbody>
													</table>
													<table width="100%" border="0" cellpadding="2" cellspacing="0" class="detailTabLabel">
			<tr>
			  <td align="left">
<table border="0" cellpadding="2" cellspacing="0" class="detailTabLabel">
														<tbody>
														<tr>
														<td width="93px">&nbsp;</td>
														  <td width="16" align="left" class="listwhitetext"><fmt:message key='customerFile.contactName'/></td>
														<td style="width:15px"></td>
														  <td width="21" align="left" class="listwhitetext"><fmt:message key='customerFile.organization'/></td>
														  </tr>
															<tr>
																<td width="93px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationContactName"  cssStyle="width:267px;" maxlength="95" onkeydown="return onlyCharsAllowed(event, this, 'special')" onblur="valid(this,'special')" tabindex="56"/></td>
																<td align="left" style="width:23px;"></td>
																<td colspan="3"><s:select cssClass="list-menu" name="customerFile.destinationOrganization" list="%{organiza}" cssStyle="width:219px;" onchange="changeStatus();" tabindex="57"/></td>
															</tr>
															<tr>
																<td width="93px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactEmail'/></td>
																<td align="left"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactPhone'/></td>
																<td width="10" style="width:15px"></td>
															  <td width="25" align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.destinationDayPhoneExt'/></td>
																<td style="width:15px"></td>
															</tr>
															<tr>
																<td width="93px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationContactEmail"  cssStyle="width:267px;" maxlength="65" tabindex="58"/></td>
																<td align="left"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationContactPhone"  cssStyle="width:130px;" maxlength="20"  tabindex="59"/></td>
																<td align="left"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationContactNameExt" cssStyle="width:54px;" maxlength="4" onchange="onlyNumeric(this);" tabindex="60"/></td>
																<td align="left"></td>
																<td width="7" align="left" class="listwhitetext" style="width:175px"></td>
																<c:if test="${empty customerFile.id}">
																<td width="150" align="right" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countContactNotes == '0' || countContactNotes == '' || countContactNotes == null}">
																	<td width="150" align="right" valign="bottom"><img id="countContactNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td width="150" align="right"  valign="bottom"><img id="countContactNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>
															</tr>
														</tbody>
														</table></td></tr></table>	
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>																														
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>														
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
												<tbody>
												<tr><tr><td align="center" colspan="10" class="vertlinedata"></td></tr>
												</tr>
												<tr><td class="subcontenttabChild"><font color="black"><b>&nbsp;Spouse Contact</b></font></td></tr>
												</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
															<c:set var="privilegeFlag" value="false"/>
															<c:if test="${customerFile.privileges}">
												 				<c:set var="privilegeFlag" value="true"/>
															</c:if>
															<td style="width:50px; height:5px;"></td></tr>															
															<tr>
																<td align="left" valign="top" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerPrefix'/></td>
																<td align="left" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerFirstName'/></td>
																<td style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerLastName'/></td>
															</tr>
															<tr>
																<td align="left" valign="top" style="width:100px">&nbsp;</td>
																<td align="left"> <s:select cssClass="list-menu" name="customerFile.custPartnerPrefix" cssStyle="width:47px" list="%{preffix}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
																<td style="width:15px"></td>
																<td align="left"> <s:textfield cssClass="input-text" name="customerFile.custPartnerFirstName" required="true"
																cssStyle="width:205px" maxlength="80" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="63"/> </td>
																<td style="width:31px"></td>
																<td align="left"> <s:textfield cssClass="input-text" key="customerFile.custPartnerLastName" required="true"
																cssStyle="width:219px;" maxlength="80" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="64"/> </td>
																<td align="left">&nbsp;&nbsp;<s:checkbox key="customerFile.privileges" value="${privilegeFlag}" fieldValue="true" onclick="changeStatus()" tabindex="65"/></td>
																<td align="left" class="listwhitetext" style="width:65px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.privileges',this);return false" >Joint&nbsp;Account</a></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" valign="top" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerEmail'/></td>
																<td style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerContact'/></td>
																<td style="width:15px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerMobile'/></td>
															</tr>
															<tr>
																<td align="left" valign="top" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.custPartnerEmail"  cssStyle="width:267px;" maxlength="65" tabindex="66"/></td>
																<td style="width:31px"></td>
																<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerContact" cssStyle="width:100px;" maxlength="20"  tabindex="67"/> </td>
																<td style="width:15px"></td>
																<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerMobile" cssStyle="width:100px;" maxlength="25"  tabindex="68"/> </td>
																<c:if test="${empty customerFile.id}">
																<td align="right" style="width:345px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countSpouseNotes == '0' || countSpouseNotes == '' || countSpouseNotes == null}">
																	<td align="right" style="width:345px" valign="bottom"><img id="countSpouseNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td align="right" style="width:345px" valign="bottom"><img id="countSpouseNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>
															</tr>
															<tr><td style="width:50px; height:10px;" colspan="7"></td></tr>
														</tbody>
													</table>
										</div>
										</td>
									</tr>
									<%-- </c:if> --%>	
	</configByCorp:fieldVisibility>
									<tr>
										<td height="10" align="left" class="listwhitetext" colspan="2">
										<!--<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('billing')" style="cursor: pointer"><a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;&nbsp;Billing Details</a></div>
										-->
										<div  onClick="javascript:animatedcollapse.toggle('billing')" style="margin: 0px">
      									<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
										<tr>
										<td class="headtab_left"></td>
										<td NOWRAP class="headtab_center">&nbsp;Billing Details</td>
										<td width="28" valign="top" class="headtab_bg"></td>
											<td class="headtab_bg_center">&nbsp;</td>
											<td class="headtab_right"></td>
												</tr>
											</table>
											</div>
										<div class="dspcont" id="billing">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:50px; height:5px;"></td></tr>															
															<tr>
																<td style="width:105px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.billToCode'/><font color="red" size="2">*</font></td>
																<td style="width:15px"></td>
																<td align="left" class="listwhitetext">Bill To Name</td>
																<td style="width:10px"></td>
																<td style="width:5px"></td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.billToAuthorization',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.billToAuthorization'/></a></td>
																<td style="width:15px"></td>
																<td align="left" class="listwhitetext" ><div style="margin:0px;float:left;"><fmt:message key='customerFile.contract'/></div>
																<c:if test="${checkContractChargesMandatory=='1'}"><div id="contractMandatory"  style="margin: 0px;"><font color="red" size="2">*</font></div></c:if>
																</td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:105px">Billing:&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.billToCode" id="quotationBillToCodeId" cssStyle="width:65px;"  maxlength="8" onblur="findBillToName();" onchange="getContract();checkFindPricingBillingPaybaleBILLName();changeStatus();findAssignmentFromParentAgent();fillAccountCodeByBillToCode();fillBillToAuthority();billToCodeFieldValueCopy();" onkeyup="valid(this,'special')" tabindex="64" /></td>																
																<td><img class="openpopup" width="17" height="20" onclick="openPopWindow(this);document.forms['customerFileForm'].elements['customerFile.billToName'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																<td align="left"><s:textfield cssClass="input-text" id="quotationBillToNameId" name="customerFile.billToName" cssStyle="width:220px;"  maxlength="250"  onkeyup="findPartnerDetails('quotationBillToNameId','quotationBillToCodeId','quotationBillToNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isCarrier=true )',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);" onchange="findPartnerDetailsByName('quotationBillToCodeId','quotationBillToNameId');"  tabindex="65" />
																<div id="quotationBillToNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
																<img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details" onclick="viewPartnerDetailsForBillToCode(this);" src="<c:url value='/images/address2.png'/>" />
																</td>
																<td align="left" width="25">
                                                        <div id="hidBillTo" style="vertical-align:middle;"><a><img class="openpopup" id="noteCustBToNavigations" 
                                                           onclick="getPartnerAlert(document.forms['customerFileForm'].elements['customerFile.billToCode'].value,'noteCustBToNavigations');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
																</td>
																
																<script type="text/javascript">
																	setTimeout("showPartnerAlert('onload','${customerFile.billToCode}','hidBillTo')", 1000);
																</script>
																<td style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.billToAuthorization"  cssStyle="width:125px;" maxlength="50" onblur="valid(this,'special')" tabindex="66" /></td>
																<td style="width:15px"></td>	
																<td><s:select id="contract" cssClass="list-menu" name="customerFile.contract" list="%{contracts}" cssStyle="width:160px"  headerKey="" headerValue="" onchange="changeStatus();checkPriceContract()" tabindex="67"/></td>
																<td style="width:5px">&nbsp;</td>	
																<td><input type="button" class="cssbutton1" onclick="openPopWindow(this)"  name="P" value="Create Private Bill To Code" style="width:160px;" /> </td>
															    <td align="left" class="listwhitetext">&nbsp;&nbsp;<s:checkbox name="destinationcopyaddress"  fieldValue="true" /></td>
															    <td align="left" class="listwhitetext" style="width:15px">Destination</td>
															     
																</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<c:set var="noChargeFlag" value="false"/>
																<c:if test="${customerFile.noCharge}">
									 								<c:set var="noChargeFlag" value="true"/>
																</c:if>
																<td style="width:105px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.billPayMethod'/></td>
																	<td style="width:15px">&nbsp;</td>
																	<td></td>
																	<td></td>
																<td  colspan="2" align="left" class="listwhitetext">&nbsp;Account&nbsp;Reference&nbsp;#</td>
															</tr>
															<tr>
																<td style="width:105px">&nbsp;</td>
																<td colspan="3"><s:select id="billPayMethod" cssClass="list-menu" name="customerFile.billPayMethod" list="%{paytype}" cssStyle="width:311px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="68" /></td>
																<td align="right" class="listwhitetext" style="width:36px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.billToReference"  cssStyle="width:298px" maxlength="20" onblur="valid(this,'special')" tabindex="69" /></td>
															</tr>
															
															<tr>
																<td align="right" class="listwhitetext" style="width:105px;padding-top:12px">Account:&nbsp;</td>
																<td colspan="5">
																<table class="detailTabLabel" cellspacing="0" cellpadding="0">
																<tbody>
																<tr>
																<td style="padding-top:12px;"><s:textfield cssClass="input-text" id="quotationAccountCodeId" name="customerFile.accountCode"  cssStyle="width:65px" maxlength="8" onchange="changeStatus();findAccToName()" onkeyup="valid(this,'special')" tabindex="70" /></td>
																<td style="padding-top:12px"><img class="openpopup" width="17" height="20" onclick="openAccountPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																<td align="left" style="padding-top:12px"><s:textfield cssClass="input-text" id="quotationAccountNameId" name="customerFile.accountName"  onkeyup="findPartnerDetails('quotationAccountNameId','quotationAccountCodeId','quotationAccountNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true )',document.forms['customerFileForm'].elements['customerFile.companyDivision'].value,event);" onchange="findPartnerDetailsByName('quotationAccountCodeId','quotationAccountNameId');" cssStyle="width:220px"  maxlength="250" tabindex="71" />
																<div id="quotationAccountNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
																</td>
																<td align="left" style="padding-top:12px">
																	<div id="hidAcc" style="vertical-align:middle;"><a><img class="openpopup" id="noteCustAccNavigations" onclick="getPartnerAlert(document.forms['customerFileForm'].elements['customerFile.accountCode'].value,'noteCustAccNavigations');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
																</td>
																<script type="text/javascript">
																	setTimeout("showPartnerAlert('onload','${customerFile.accountCode}','hidAcc')", 2000);
																</script>
																
																</tr>
																</tbody>
																</table>
																</td>															
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
																<tr><td align="center" colspan="10" class="vertlinedata"></td></tr>															
															<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>															
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="1">
														<tbody>															
															<tr>
																<td style="width:105px">&nbsp;</td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.personPricing',this);return false" onmouseout="ajax_hideTooltip()">Pricing</a></td>
																<td style="width:7px"></td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.personBilling',this);return false" onmouseout="ajax_hideTooltip()">Billing</a></td>
																<td style="width:10px"></td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.personPayable',this);return false" onmouseout="ajax_hideTooltip()">Payable</a></td>
																<td style="width:5px"></td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.auditor',this);return false" onmouseout="ajax_hideTooltip()">Auditor</a></td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:105px">Responsible Person:&nbsp;</td>
																
																<td><s:select name="customerFile.personPricing" cssClass="list-menu" list="%{pricingList}" id="personPricing" cssStyle="width:150px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="72" /></td>

																<td style="width:7px"></td>																
																<td><s:select name="customerFile.personBilling" cssClass="list-menu" list="%{billingList}" id="personBilling" cssStyle="width:147px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="73" /></td>
												
												                <td style="width:31px"></td>		
																<td><s:select cssClass="list-menu" name="customerFile.personPayable" list="%{payableList}" id="personPayable" cssStyle="width:148px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="74" /></td>

																<td style="width:5px"></td>
																<td><s:select cssClass="list-menu" name="customerFile.auditor" list="%{auditorList}" id="auditor" cssStyle="width:148px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="74" /></td>

																<td style="width:10px"></td>
															</tr>
															<tr><td style="width:50px; height:5px;"></td></tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td align="right" width="105px"></td>
															<td></td>
															<td width="10px"></td>
															<td align="left" width="">Approved&nbsp;By&nbsp;</td>
															<td width="10px"></td>
															<td width="10px"></td>
															<td align="left" width="">Approved&nbsp;On&nbsp;</td>
															<td></td>
															</tr>
															<tr>
																<td align="right" width="96px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.noCharge',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='customerFile.noCharge'/></td>
																<td align="left" width="153px"><s:checkbox key="customerFile.noCharge" value="${noChargeFlag}" fieldValue="true" onclick="changeStatus()" tabindex="75" /></td>
																<td width="10px"></td>
																<td align="left" colspan="2">
																<s:select cssClass="list-menu" name="customerFile.approvedBy" list="%{execList}"  id="approvedBy" cssStyle="width:148px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="76" />
																</td>
																<td width="35px"></td>
																<c:if test="${not empty customerFile.approvedOn}">
							<s:text id="customerFileApprovedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.approvedOn"/></s:text>
							<td><s:textfield cssClass="input-text" id="approvedOn" name="customerFile.approvedOn" value="%{customerFileApprovedOnFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
							<td align="left" width="30px"><img id="approvedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.approvedOn}">
							<td><s:textfield cssClass="input-text" id="approvedOn" name="customerFile.approvedOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
							<td width="30px">&nbsp;<img id="approvedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
														    	
														    	
																<c:if test="${empty customerFile.id}">
																<td align="right" style="width:425px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countBillingNotes == '0' || countBillingNotes == '' || countBillingNotes == null}">
																	<td align="right" style="width:425px"><img id="countBillingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td align="right" style="width:425px"><img id="countBillingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Billing&imageId=countBillingNotesImage&fieldId=countBillingNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>
															</tr>
															<tr><td style="width:50px; height:5px;"></td></tr>
														</tbody>
													</table>		
										</div>
										</td>
									</tr>
							
									
 						<tr>
							<td height="10" align="left" class="listwhitetext">							
							<!--<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('entitlement')" style="cursor: pointer"><a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;&nbsp;Entitlement</a></div>
							--><div  onClick="javascript:animatedcollapse.toggle('entitlement')" style="margin: 0px">
      						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
							<tr>
							<td class="headtab_left"></td>
							<td NOWRAP class="headtab_center">&nbsp;Entitlement&nbsp;/Service Authorized</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
							</tr>
							</table>
							</div>
							
							<div class="dspcont" id="entitlement">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
											<tbody>
												<tr><td style="width:90px; height:5px;"></td></tr>												
												<tr>
													<td align="right" valign="top" class="listwhitetext" style="width:110px"><span style="padding-right:55px;">Comments:&nbsp;</span><br>(max. 254 characters)&nbsp;</td>
													<td width="740px"><s:textarea name="customerFile.entitled" cssStyle="width:535px;height:90px;"  cssClass="textarea" tabindex="77"  onkeydown="limitText()" onkeyup="limitText()"/>
														<s:textfield name="countdown" value="254" readonly="true" cssStyle="width:21px;color:#444444;font-size:12px;" cssClass="balanceBox" />characters&nbsp;left.
													</td>
													
													<td style="width:5px;"></td>
													<td valign="bottom"><!-- input type="button" class="cssbuttonB" value="Add Entitlement" --></td>
													
													<c:if test="${empty customerFile.id}">
													<td align="right" valign="bottom" style="width:115px;" ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
													</c:if>
													<c:if test="${not empty customerFile.id}">
													<c:choose>
														<c:when test="${countEntitlementNotes == '0' || countEntitlementNotes == '' || countEntitlementNotes == null}">
														<td align="right" style="width:115px;" valign="bottom"><img id="countEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:when>
														<c:otherwise>
														<td align="right" style="width:115px;" valign="bottom"><img id="countEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:otherwise>
													</c:choose> 
													</c:if>
													</tr>
													<tr><td height="5">&nbsp;</td></tr>
													<tr><td class="listwhitetext" align="right" >Type&nbsp;Of&nbsp;Contract&nbsp;</td>
													<td class="listwhitetext" colspan="3"><s:select cssClass="list-menu" id="assignmentType" cssStyle="width:150px;"  name="customerFile.assignmentType" list="%{assignmentTypes}" headerKey=" " headerValue=" " onclick="changeStatus();" tabindex="78" /></td></tr>
													<tr><td style="width:50px; height:10px;"></td></tr>
												<tr></tr>
												<tr><td style="width:100px; height:5px;"></td></tr>
											
												<configByCorp:fieldVisibility componentId="component.UGSG.HRDetails_collapseTab">
	
												<tr>
													<td height="10" width="100%" colspan="10" align="left" style="margin: 0px">
														<div   style="margin: 0px">
															 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
															 <tr>
															 <td class="headtab_left"></td>
															 <td NOWRAP  class="headtab_center" style="cursor:default; ">&nbsp;&nbsp;HR Details </td>
															 <td width="28" valign="top" class="headtab_bg"></td>
															 <td class="headtab_bg_center">&nbsp; </td>
															 <td class="headtab_right"> </td>
															 </tr>
															 </table>
												 		</div>
												  		<div id="hr" class="switchgroup1">
														     <table border="0" class="detailTabLabel">
															 <tbody>
															 <tr>
															 <td colspan="5" >
																 <table width="100%" >
																 <tr> 
																  <td align="center"  class="listwhitetext" width="50%"><b><u>Local Country</u></b></td> 
																 </tr>
																 </table>
															 </td>
															 </tr>
															 <tr>
																<td width="10px"></td>
																<td align="right" class="listwhitetext" width="200px" >Name Of Local responsible HR Person</td>
																<td><s:textfield cssClass="input-text" name="customerFile.localHr" cssStyle="width:289px;" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)" tabindex="82" /></td>
																</tr>
																<tr>
																<td width="10px"></td>
																<td align="right" class="listwhitetext">Phone</td>   
																<td><s:textfield cssClass="input-text" name="customerFile.localHrPhone"  cssStyle="width:289px;" maxlength="20"  tabindex="82"/></td>
															    <td width="10px"></td>
															    
															    </tr>
															    <tr>
																<td width="10px"></td>
																<td align="right" class="listwhitetext">Email Address</td>
																<td ><s:textfield cssClass="input-text" name="customerFile.localHrEmail" cssStyle="width:289px;" maxlength="65" tabindex="82"  onkeydown=""/></td>
																<td width="10px"></td>
															 </tr>
															 <tr>
														        <td align="right"></td>
														     </tr>
															 </tbody>
															 </table> 
												 		</div>													
													</td>
												</tr>
												</configByCorp:fieldVisibility>
											</tbody>
										</table>
							</td>
						</tr>
						<configByCorp:fieldVisibility componentId="customerFile.contactName">						
						<tr>
							<td align="left" class="listwhitetext">
							<div id="accountContactHid">
							<!--<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('acc_contact')" style="cursor: pointer"><a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle"/></span>&nbsp;&nbsp;Account Contact</a></div>
							--><div  onClick="javascript:animatedcollapse.toggle('acc_contact')" style="margin: 0px">
      						<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
							<tr>
							<td class="headtab_left">
							</td>
							<td NOWRAP class="headtab_center">&nbsp;Account Contact
							</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_center">&nbsp;
							</td>
							<td class="headtab_right">
							</td>
							</tr>
						</table>
							</div>
							<div class="dspcont" id="acc_contact" onmouseover="checkBillToCode();" style="width: 100%">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
											<tbody>
												<tr><td style="width:100px; height:5px;"></td></tr>
												<tr></tr>												
												<tr><td><b>&nbsp;At Origin</b></td></tr>
												<tr>
													<td align="left" valign="top" style="width:108px">&nbsp;</td>
													<td align="left" class="listwhitetext" width="242px"><fmt:message key='customerFile.contactName'/></td>
													<td style="width:15px"></td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.organization'/></td>
												</tr>
												<tr>
													<td align="right" valign="top" style="width:108px">&nbsp;</td>
													<td><s:textfield cssClass="input-text" id="contactName" name="customerFile.contactName"  size="33" maxlength="35"/></td>
													<td style="width:15px"></td>
													<td><s:select cssClass="list-menu" name="customerFile.organization" list="%{organiza}" onchange="changeStatus();"/></td>
												</tr>
												<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
												<tr>
													<td align="left" valign="top" style="width:108px">&nbsp;</td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactEmail'/></td>
													<td style="width:15px"></td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactPhone'/></td>
													<td style="width:15px"></td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactFax'/></td>
												</tr>
												<tr>
													<td align="right" valign="top" style="width:108px">&nbsp;</td>
													<td><s:textfield cssClass="input-text" name="customerFile.contactEmail"  size="33" maxlength="65" /></td>
													<td style="width:15px"></td>
													<td style="width:130px"><s:textfield cssClass="input-text" name="customerFile.contactPhone"  size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
													<td style="width:15px"></td>
													<td><s:textfield cssClass="input-text" name="customerFile.contactFax" size="15" maxlength="20" onkeydown="return onlyNumsAllowed(event)" /></td>
												</tr>
											</tbody>
										</table>
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
											<tbody>
												<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>											
													<tr><td align="center" colspan="10" class="vertlinedata"></td></tr>											
												<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>											
											</tbody>
										</table>
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
											<tbody>
												<tr><b>&nbsp;At Destination</b>
													<td align="left" valign="top" style="width:108px">&nbsp;</td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactName'/></td>
													<td style="width:15px"></td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.organization'/></td>
												</tr>
												<tr>
													<td align="right" valign="top" style="">&nbsp;</td>
													<td><s:textfield cssClass="input-text" name="customerFile.destinationContactName"  size="33" maxlength="35"/></td>
													<td style="width:15px"></td>
													<td><s:select cssClass="list-menu" name="customerFile.destinationOrganization" list="%{organiza}"/></td>
												</tr>
												<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
												<tr>
													<td align="left" valign="top">&nbsp;</td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactEmail'/></td>
													<td style="width:15px"></td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactPhone'/></td>
													<td style="width:15px"></td>
													<td align="left" class="listwhitetext"><fmt:message key='customerFile.contactFax'/></td>
												</tr>
												<tr>
													<td align="right" valign="top" style="">&nbsp;</td>
													<td><s:textfield cssClass="input-text" name="customerFile.destinationContactEmail"  size="33" maxlength="65" /></td>
													<td style="width:15px"></td>
													<td><s:textfield cssClass="input-text" name="customerFile.destinationContactPhone"  size="15" maxlength="20" onkeydown="return onlyPhoneNumsAllowed(event)"/></td>
													<td style="width:15px"></td>
													<td><s:textfield cssClass="input-text" name="customerFile.destinationContactFax" size="15" maxlength="20" onkeydown="return onlyNumsAllowed(event)" /></td>
													<td style="width:10px"></td>
													<c:if test="${empty customerFile.id}">
													<td align="right" style="width:260px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
													</c:if>
													<c:if test="${not empty customerFile.id}">
													<c:choose>
														<c:when test="${countContactNotes == '0' || countContactNotes == '' || countContactNotes == null}">
														<td align="right" style="width:260px" valign="bottom"><img id="countContactNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:when>
														<c:otherwise>
														<td align="right" style="width:260px" valign="bottom"><img id="countContactNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=agentQuotes&subType=AccountContact&imageId=countContactNotesImage&fieldId=countContactNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:otherwise>
													</c:choose> 
													</c:if>
												</tr>
												<tr><td style="width:100px; height:5px;"></td></tr>
											</tbody>
										</table>
							</div>
							</div>
							</td>
						</tr>						
						</configByCorp:fieldVisibility>
						 <!--  Modifications are also available in this file (subrat) -->
						 <configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
								<tr>						
						<td height="10" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('cportal')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left"></td>
					<td NOWRAP class="headtab_center">&nbsp;Customer Portal Detail</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;</td>
					<td class="headtab_right"></td>
					</tr>
					</table>
					  </div>
							<div id="cportal" class="switchgroup1">							
						<table cellpadding="1" cellspacing="0" class="detailTabLabel">
<tr>													<c:set var="isActiveFlag" value="false"/>
													<c:if test="${customerFile.portalIdActive}">
														<c:set var="isActiveFlag" value="true"/>
													</c:if>
													<td width="104" align="right" class="listwhitetext" >Customer&nbsp;Portal:</td>
    <td width="16" ><s:checkbox key="customerFile.portalIdActive" value="${isActiveFlag}" fieldValue="true" onclick="changeStatus(),checkPortalBoxId()" tabindex="84"/></td>
													<td width="116"  align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.portalIdActive',this);return false" >
    <fmt:message key='customerFile.portalIdActive'/></a></td>
													<td width="40" align="right"  class="listwhitetext" >Email:&nbsp;</td>
    <td width="50" ><s:textfield cssClass="input-textUpper" name="customerFileemail"  value="%{customerFile.email}" readonly = "true" cssStyle="width:232px;" maxlength="65" tabindex="84"/></td>
	
	<c:set var="isSecondaryEmailFlag" value="false"/>
													<c:if test="${customerFile.secondaryEmailFlag}">
														<c:set var="isSecondaryEmailFlag" value="true"/>
													</c:if>
													<td width="119" align="right"  class="listwhitetext" >Secondary Email:</td>
    <td width="16" ><s:checkbox key="customerFile.secondaryEmailFlag" value="${isSecondaryEmailFlag}" fieldValue="true" onclick="changeStatus(),checkPortalBoxSecondaryId()" tabindex="85"/></td>
	<td width="119" align="right"  class="listwhitetext" >Email Sent Date:</td>
    <td width="16" ><s:textfield cssClass="input-textUpper" cssStyle="width:65px;" name="customerFile.emailSentDate" readonly="true" /></td>
												   
  </tr></table>
  
 <table  border="0" cellpadding="1" cellspacing="0" class="detailTabLabel">																																									
												<tr>
												  <td width="107" align="right" class="listwhitetext"><fmt:message key='customerFile.customerPortalId'/></td>
													
												  <td align="left" width="177"><s:textfield cssClass="input-textUpper" name="customerFile.customerPortalId" required="true" cssStyle="width:90px;" maxlength="80" readonly="true" tabindex="85"/></td>
																								
													<c:if test="${not empty customerFile.id}">
													<td ><input type="button" value="Reset password and email to customer" style="width:236px;" class="cssbuttonB" onclick="generatePortalId();" tabindex="86"/></td>
													</c:if>
													<c:if test="${empty customerFile.id}">
													<td  align="left" ><input type="button" disabled value="Reset password and email to customer" style="width:240px;" class="cssbuttonB" onclick="generatePortalId();" tabindex="86"/></td>
													</c:if>
														<c:if test="${customerFile.cportalEmailLanguage==null || customerFile.cportalEmailLanguage==''}">
																				<c:set var="ischecked19" value="true"/>
																				<c:set var="ischecked18" value="false"/>
														</c:if>
														<c:if test="${customerFile.cportalEmailLanguage!=null && customerFile.cportalEmailLanguage!=''}">
																				<c:set var="ischecked19" value="false"/>
																				<c:set var="ischecked18" value="false"/>
														<c:set var="splittedString1" value="${fn:split(customerFile.cportalEmailLanguage, ',')}" />
															<c:forEach var="lang" items="${splittedString1}">
																<c:if test="${fn:indexOf(lang,'ENGLISH')>=0}" >
																	<c:set var="ischecked18" value="true"/>
																</c:if>
																<c:if test="${fn:indexOf(lang,'GERMAN')>=0}" >
																	<c:set var="ischecked19" value="true"/>
																</c:if>
															</c:forEach>
														</c:if>																			
													<configByCorp:fieldVisibility componentId="component.field.Alternative.customerFileGerman">
													<td style="width:10px"></td>
													<td align="left" class="listwhitetext" style="width:10px">English</td>
													<td align="left">
													<s:checkbox name="checkEnglishForCustomerSave" value="${ischecked18}" fieldValue="true" onclick="changeStatus()" />
													</td>							
													<td style="width:10px"></td>
													<td align="left" class="listwhitetext" style="width:10px">German</td>
													<td align="left">
													<s:checkbox name="checkGermanForCustomerSave" value="${ischecked19}" fieldValue="true" onclick="changeStatus()" />
													</td>
													</configByCorp:fieldVisibility>		
													<c:if test="${empty customerFile.id}">
													<td  align="right"  width="442" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
													</c:if>
													<c:if test="${not empty customerFile.id}">
													<c:choose>
														<c:when test="${countCportalNotes == '0' || countCportalNotes == '' || countCportalNotes == null}">
														<td  align="right"  width="442" valign="bottom"><img id="countCportalNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:when>
														<c:otherwise>
														<td  align="right"  width="442" valign="bottom"><img id="countCportalNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:otherwise>
													</c:choose> 
													</c:if>
												</tr>
										</table>										
						</div>
						</td>
						</tr>
						</configByCorp:fieldVisibility>
						
		</tbody>
	</table>
	</div>
<div class="bottom-header" style="!margin-top:35px;"><span></span></div>
</div>
</div> 
</div>
				<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>							
							<td valign="top"></td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${customerFile.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${customerFile.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{customerFile.createdBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${customerFile.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${customerFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{customerFile.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
	
<s:submit cssClass="cssbuttonA"  key="button.save" onmouseover="completeTimeString();" onclick="return IsValidTime('save');checkStatusReason(); checkDate();" tabindex="79"/>
		<c:if test="${not empty customerFile.id}">
		    <input type="button" class="cssbutton1" onclick="location.href='<c:url   value="/QuotationFileForm.html"/>'" value="<fmt:message  key="button.add"/>" tabindex="80" /> 
	    </c:if>  
<!-- 	<input type="submit" name="saveBtn" value="Save" style="width:87px; height:30px" />  -->
<s:hidden name="submitCnt" value="<%=request.getParameter("submitCnt") %>"/>
<s:hidden name="submitType" value="<%=request.getParameter("submitType") %>"/>
<c:set var="submitType" value="<%=request.getParameter("submitType") %>"/>
	<c:choose>
		<c:when test="${submitType == 'job'}">
			<s:reset type="button" cssClass="cssbutton1" key="Reset" />
		</c:when>
		<c:otherwise>
		<c:if test="${not empty customerFile.id}">			
			<s:reset type="button" cssClass="cssbutton1" value="Reset" onclick="findResetCompanyDivisionByBookAg();changeReset();newFunctionForCountryState(); setVipImageReset();setAssignmentReset();" tabindex="81" />
		</c:if>
		<c:if test="${empty customerFile.id}">
			<s:reset type="button" cssClass="cssbutton1" value="Reset" onclick="findResetCompanyDivisionByBookAg();changeReset();newFunctionForCountryState(); setVipImageReset();setAssignmentReset();setStateReset();" tabindex="81" />
			</c:if>
		</c:otherwise>	
	 </c:choose> 
		<c:if test="${not empty customerFile.id}">
		<c:if test="${customerFile.controlFlag != 'C'}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/addAndCopyQuotationFile.html?cid=${customerFile.id}"/>'" 
	        value="Add With Copy" style="width:110px; height:25px" tabindex="82"/> 
	        <configByCorp:fieldVisibility componentId="component.field.CustomerFile.MOL">
			        <input type="button" class="cssbutton1" 
			        onclick="sendToMOL('Starline')" 
			        value="Send Quotation Number to Starline MOL" style="width:250px; height:25px" tabindex="83"/>
			        <input type="button" class="cssbutton1" 
			        onclick="sendToMOL('Highland')" 
			        value="Send Quotation Number to Highland MOL" style="width:250px; height:25px" tabindex="84"/>
	        </configByCorp:fieldVisibility>
	    </c:if>  
		</c:if>	
		 
							
<s:hidden name="customerFile.statusNumber" />	
<s:hidden name="customerFile.nextCheck" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="eigthDescription" />
<s:hidden name="ninthDescription" />
<s:hidden name="tenthDescription"/>
<s:hidden name="sourceCodeTemp" />
<s:hidden name="customerFile.status" />

<s:text id="customerFileSystemDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.systemDate" /></s:text>
<td valign="top"><s:hidden name="customerFile.systemDate" value="%{customerFileSystemDateFormattedValue}" /></td>
							
<s:hidden name="sendEmail" />
<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="agentQuotes" scope="session"/>
<c:set var="idOfTasks" value="${customerFile.id}" scope="session"/>
<c:set var="tableName" value="customerfile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<input type="hidden" name="encryptPass" value="true" />

<s:hidden key="user.id"/>
<s:hidden key="user.version"/>
<c:if test="${hitFlag == 1}" >
	 <c:redirect url="/QuotationFileForm.html?id=${customerFile.id}"  />
</c:if>
</s:form>

<%-- Script shifted from top on 14-Sep-2012 --%>
<%@ include file="/common/QuotationFileFormJavaScript.js"%>
<%@ include file="/common/QuatationFileFormSecJavaScript.js"%>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
