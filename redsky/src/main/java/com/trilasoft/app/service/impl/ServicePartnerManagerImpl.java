/**
 * @Class Name  ServicePartner ManagerImpl
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ServicePartnerDao;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.service.ServicePartnerManager;

public class ServicePartnerManagerImpl extends GenericManagerImpl<ServicePartner, Long> implements ServicePartnerManager {
	ServicePartnerDao servicePartnerDao;

	public ServicePartnerManagerImpl(ServicePartnerDao servicePartnerDao) {
		super(servicePartnerDao);
		this.servicePartnerDao = servicePartnerDao;
	}

	public List findMaxId() {
		return servicePartnerDao.findMaxId();
	}

	public List<ServicePartner> findByLastName(String lastName) {
		return servicePartnerDao.findByLastName(lastName);
	}

	public List checkById(Long id) {
		return servicePartnerDao.checkById(id);
	}

	public List<ServicePartner> portName(String portCode,String sessionCorpID) {
		return servicePartnerDao.portName(portCode,sessionCorpID);
	}

	public List<ServicePartner> findByBroker(String shipNumber) {
		return servicePartnerDao.findByBroker(shipNumber);
	}

	public List<ServicePartner> findByCarriers(String shipNumber) {
		return servicePartnerDao.findByCarriers(shipNumber);
	}

	public List<ServicePartner> findByAgent(String shipNumber) {
		return servicePartnerDao.findByAgent(shipNumber);
	}

	public List<ServicePartner> findByPartner(String shipNumber) {
		return servicePartnerDao.findByPartner(shipNumber);
	}

	public List<ServicePartner> findForParter(String corpId, String shipNumber) {
		return servicePartnerDao.findForPartner(corpId, shipNumber);
	}

	public List<ServicePartner> findByCarrierFromAccountLine(String shipNumber) {
		return servicePartnerDao.findByCarrierFromAccountLine(shipNumber);
	}

	public List checkByShipNumber(String ship) {
		return servicePartnerDao.checkByShipNumber(ship);
	}

	public int updateTrack(String shipNumber, String billLading,String hbillLading,String userName) {
		return servicePartnerDao.updateTrack(shipNumber, billLading, hbillLading,userName);
	}

	public List findCarrierFromAccLine(String carr, String vendorCode, String shipNumber) {
		return servicePartnerDao.findCarrierFromAccLine(carr, vendorCode, shipNumber);
	}

	public List<ServicePartner> findByCarrier(String shipNumber) {
		return servicePartnerDao.findByCarrier(shipNumber);
	}

	public List findTranshipped(String shipNumber) {
		return servicePartnerDao.findTranshipped(shipNumber);
	}

	public int updateDeleteStatus(Long ids) {
		return servicePartnerDao.updateDeleteStatus(ids);
	}

	public int updateServiceOrder(String carrierDeparture, String carrierArrival, String shipNumber,String userName) {
		return servicePartnerDao.updateServiceOrder(carrierDeparture, carrierArrival, shipNumber, userName);
	}

	public int updateServiceOrderDate(Long id, String corpId ,String userName) {
		return servicePartnerDao.updateServiceOrderDate(id, corpId , userName);
	}

	public int updateServiceOrderActualDate(Long id, String corpId, Long customerFileId ,String userName) {
		return servicePartnerDao.updateServiceOrderActualDate(id, corpId,customerFileId ,userName);
	}

	public List findByCarrierCode(String carrierCode) {
		return servicePartnerDao.findByCarrierCode(carrierCode);
	}

	public List getContainerNumber(String shipNumber) {
		return servicePartnerDao.getContainerNumber(shipNumber);
	}

	public List goNextSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return servicePartnerDao.goNextSOChild(sidNum, sessionCorpID, soIdNum);
	}

	public List goPrevSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return servicePartnerDao.goPrevSOChild(sidNum, sessionCorpID, soIdNum);
	}

	public List goSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return servicePartnerDao.goSOChild(sidNum, sessionCorpID, soIdNum);
	}

	public List findCountChild(String shipNm) {
		return servicePartnerDao.findCountChild(shipNm);
	}

	public List findMaximumChild(String shipNm) {
		return servicePartnerDao.findMaximumChild(shipNm);
	}

	public List findMinimumChild(String shipNm) {
		return servicePartnerDao.findMinimumChild(shipNm);
	}
	public List findTranshipChild(String shipNm){
		return servicePartnerDao.findTranshipChild(shipNm);
	}
	public List findClone(String shipNumber) {
		return servicePartnerDao.findClone(shipNumber);
	}
    public int copyAtd(Date atd, String shipNumber){
	  return servicePartnerDao.copyAtd(atd, shipNumber);
}
	
	public int copyAta(Date ata, String shipNumber){
		return servicePartnerDao.copyAta(ata, shipNumber);
	}

	public List routingList(Long serviceOrderId) {
		return servicePartnerDao.routingList(serviceOrderId);
	}

	public Long findRemoteServicePartner(String carrierNumber,Long serviceOrderId) {
		return servicePartnerDao.findRemoteServicePartner(carrierNumber, serviceOrderId);
	}

	public void deleteNetworkServicePartner(Long serviceOrderId,String carrierNumber, String routingStatus) {
		servicePartnerDao.deleteNetworkServicePartner(serviceOrderId,  carrierNumber,  routingStatus);		
	}
	public List routingListOtherCorpId(Long sid){
		return servicePartnerDao.routingListOtherCorpId(sid);
	}
	public List getMaxAtArrivalDate(Long soId, String sessionCorpID){
		return servicePartnerDao.getMaxAtArrivalDate(soId,sessionCorpID);
	}
	public List findServicePartnerBySid(Long sofid, String sessionCorpID){
		return servicePartnerDao.findServicePartnerBySid(sofid, sessionCorpID);	
	}
	public List findRoutingLines(Long grpId, String sessionCorpID){
		return servicePartnerDao.findRoutingLines(grpId, sessionCorpID);
	}
	public List routingListByGrpId(Long servicePartnerId){
		return servicePartnerDao.routingListByGrpId(servicePartnerId);
	}
	public List routingListByIdAndgrpId(Long grpId){
		return servicePartnerDao.routingListByIdAndgrpId(grpId);
	}
	public List routingWithNoLine(String sessionCorpId,Long grpId,Long masterServicePartnerId){
		return servicePartnerDao.routingWithNoLine(sessionCorpId, grpId,masterServicePartnerId);
	}
	public void updateRouting(String oldIdList){
		servicePartnerDao.updateRouting(oldIdList);
	}
	public List findRemoteRoutingList(String shipNumber){
		return servicePartnerDao.findRemoteRoutingList(shipNumber);
	}
	public List routingListByGrpId(Long grpId, Long contId){
		return servicePartnerDao.routingListByGrpId(grpId, contId);
	}

	public void deleteNetworkServicePartnerByParentId(Long parentId, String shipNumber) {
		servicePartnerDao.deleteNetworkServicePartnerByParentId(parentId, shipNumber);
	}
	public List getCarrierListvalue(String partnerCode,String partnerlastName,String partneraliasName,String partnerbillingCountryCode,String partnerbillingState,String corpID){
		return servicePartnerDao.getCarrierListvalue(partnerCode,partnerlastName,partneraliasName,partnerbillingCountryCode,partnerbillingState,corpID);
	}

	public List<ServicePartner> findByCarriersID(Long id) {
		 
		return servicePartnerDao.findByCarriersID(id);
	}
	public List getCarrierNameForAgent(String partnerCode,String corpID){
		return servicePartnerDao.getCarrierNameForAgent(partnerCode,corpID);		
	}
	
	public void updateServicePartnerDetails(Long id,String fieldName,String fieldValue,String fieldName1,String fieldValue1,String tableName, String sessionCorpID, String userName){
		servicePartnerDao.updateServicePartnerDetails(id,fieldName,fieldValue,fieldName1,fieldValue1,tableName,sessionCorpID,userName);
	}

	public void updateStatus(Long id, String routingStatus, String updateRecords) {
		servicePartnerDao.updateStatus(id, routingStatus,updateRecords);
	}
	public List getCarrierName(String partnerCode,String corpID) {
		return servicePartnerDao.getCarrierName( partnerCode, corpID);
	}
	//13674 - Create dashboard
	public List findTransptDetailList(String shipNumber,String corpID) {
		return servicePartnerDao.findTransptDetailList(shipNumber, corpID);
	}
	
}   

