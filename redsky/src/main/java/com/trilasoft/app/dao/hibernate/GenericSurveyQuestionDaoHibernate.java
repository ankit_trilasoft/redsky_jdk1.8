package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.GenericSurveyQuestionDao;
import com.trilasoft.app.model.GenericSurveyQuestion;

public class GenericSurveyQuestionDaoHibernate extends GenericDaoHibernate<GenericSurveyQuestion, Long>implements GenericSurveyQuestionDao{
	public GenericSurveyQuestionDaoHibernate() {
		super(GenericSurveyQuestion.class);
	}
	
	public List findRecords(String sessionCorpID) {
		String query="from GenericSurveyQuestion where corpId='"+sessionCorpID+"'";
		List list = getHibernateTemplate().find(query);
		return list;	
	}
	
	public String checkForBA(Long sid,String sessionCorpID) {
		String bookingAgentFound="";
		String query="select s.bookingagentcode from serviceorder s, companydivision c where s.id="+sid+" and s.corpid='"+sessionCorpID+"' and s.bookingagentcode=c.bookingagentcode and s.corpid=c.corpid";
		List list = this.getSession().createSQLQuery(query).list();
		if(list!=null && !list.isEmpty() && list.get(0)!=null){
			bookingAgentFound = list.get(0).toString();
		}
		return bookingAgentFound;	
	}
	
	public String checkForOA(Long sid,String sessionCorpID) {
		String originAgentFound="";
		String query="select t.originagentcode from trackingstatus t, companydivision c where t.id="+sid+" and t.corpid='"+sessionCorpID+"' and t.originagentcode=c.bookingagentcode and t.corpid=c.corpid";
		List list = this.getSession().createSQLQuery(query).list();
		if(list!=null && !list.isEmpty() && list.get(0)!=null){
			originAgentFound = list.get(0).toString();
		}
		return originAgentFound;	
	}
	
	public String checkForDA(Long sid,String sessionCorpID) {
		String destinationAgentFound="";
		String query="select t.destinationagentcode from trackingstatus t, companydivision c where t.id="+sid+" and t.corpid='"+sessionCorpID+"' and t.destinationagentcode=c.bookingagentcode and t.corpid=c.corpid";
		List list = this.getSession().createSQLQuery(query).list();
		if(list!=null && !list.isEmpty() && list.get(0)!=null){
			destinationAgentFound = list.get(0).toString();
		}
		return destinationAgentFound;	
	}
	
	public Map<List, Map> getSurveyUserList(String joType,String routingType,String sessionCorpID,Long sid,Long cid,boolean isBookingAgent,boolean isOriginAgent,boolean isdestinationAgent,String language){
		Map<List, Map> finalMap= new LinkedHashMap<List, Map>();
		String query="";
		List surveyList = new ArrayList();
		if(sid!=null){
			query="select id,question,answerType,category from genericsurveyquestion where (jobType is null or jobType ='' or jobType like '%"+joType+"%') and (routing is null or routing ='' or routing like '%"+routingType+"%') "
					+ " and corpId='"+sessionCorpID+"' and bookingAgent="+isBookingAgent+" and originAgent="+isOriginAgent+" and destinationAgent="+isdestinationAgent+" and language='"+language+"' and status is true order by category";
				surveyList = this.getSession().createSQLQuery(query).list();
			if(surveyList==null || surveyList.isEmpty()){
				query="select id,question,answerType,category from genericsurveyquestion where (jobType is null or jobType ='' or jobType like '%"+joType+"%') and (routing is null or routing ='' or routing like '%"+routingType+"%') "
						+ " and corpId='"+sessionCorpID+"' and bookingAgent="+isBookingAgent+" and originAgent="+isOriginAgent+" and destinationAgent="+isdestinationAgent+" and language='en' and status is true order by category";
				surveyList = this.getSession().createSQLQuery(query).list();
			}
		}else{
			query="select id,question,answerType,category from genericsurveyquestion where (jobType is null or jobType ='' or jobType like '%"+joType+"%') and corpId='"+sessionCorpID+"' "
					+ " and bookingAgent="+isBookingAgent+" and originAgent="+isOriginAgent+" and destinationAgent="+isdestinationAgent+" and language='"+language+"' and status is true order by category";
				surveyList = this.getSession().createSQLQuery(query).list();
			if(surveyList==null || surveyList.isEmpty()){
				query="select id,question,answerType,category from genericsurveyquestion where (jobType is null or jobType ='' or jobType like '%"+joType+"%') and corpId='"+sessionCorpID+"' "
						+ " and bookingAgent="+isBookingAgent+" and originAgent="+isOriginAgent+" and destinationAgent="+isdestinationAgent+" and language='en' and status is true order by category";
				surveyList = this.getSession().createSQLQuery(query).list();
			}
		}
		
		SurveyListDTO dto = null;
		Iterator it = surveyList.iterator();
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new SurveyListDTO();
			dto.setSurveyQuestionId(row[0]);
			dto.setSurveyQuestionQuestion(row[1]);
			dto.setSurveyQuestionAnswerType(row[2]);
			String description="";
			if(row[3]!=null && !row[3].equals("")){
				List refMasterDescList = getSession().createSQLQuery("select description from refmaster where code='"+row[3]+"' and parameter='GENERICSURVEYCATEGORY' and corpId='"+sessionCorpID+"'").list();
				if(refMasterDescList!=null && !refMasterDescList.isEmpty() && refMasterDescList.get(0)!=null){
					description = refMasterDescList.get(0).toString();
				}
				dto.setSurveyQuestionCategoryType(description);
			}else{
				dto.setSurveyQuestionCategoryType(row[3]);
			}
			
			Map<String, String> surveyAnswerMap= new LinkedHashMap<String, String>();
			String query1= "select id,answer from genericsurveyanswer where surveyQuestionId ='"+row[0]+"' and corpId='"+sessionCorpID+"' and status is true";
			List answerList = this.getSession().createSQLQuery(query1).list();
			
			Iterator itr = answerList.iterator();
			while(itr.hasNext()){
				Object []row1= (Object [])itr.next();
				surveyAnswerMap.put(row1[0].toString(), row1[1].toString());
			}
			String query2="";
			if(sid!=null){
				query2= "select questionid,answerid from surveyanswerbyuser where questionid ='"+row[0]+"' and serviceorderid="+sid+"";
			}else{
				query2= "select questionid,answerid from surveyanswerbyuser where questionid ='"+row[0]+"' and customerFileId="+cid+"";
			}
			List surveyByUsrList = this.getSession().createSQLQuery(query2).list();
			Iterator itr1 = surveyByUsrList.iterator();
			while(itr1.hasNext()){
				Object []row2= (Object [])itr1.next();
				dto.setSurveyByUserQuestionId(row2[0]);
				dto.setSurveyByUserAnswerId(row2[1]);
			}
			List surveyQuestionList= new ArrayList();
			surveyQuestionList.add(dto);
			finalMap.put(surveyQuestionList, surveyAnswerMap);
		}
		
		return finalMap;
	}
	
	public class SurveyListDTO {
		
		private Object surveyQuestionId;
		private Object surveyQuestionQuestion;
		private Object surveyQuestionAnswerType;
		private Object surveyByUserQuestionId;
		private Object surveyByUserAnswerId;
		private Object surveyQuestionCategoryType;
		
		public Object getSurveyQuestionAnswerType() {
			return surveyQuestionAnswerType;
		}
		public void setSurveyQuestionAnswerType(Object surveyQuestionAnswerType) {
			this.surveyQuestionAnswerType = surveyQuestionAnswerType;
		}
		
		public Object getSurveyQuestionId() {
			return surveyQuestionId;
		}
		public void setSurveyQuestionId(Object surveyQuestionId) {
			this.surveyQuestionId = surveyQuestionId;
		}
		public Object getSurveyQuestionQuestion() {
			return surveyQuestionQuestion;
		}
		public void setSurveyQuestionQuestion(Object surveyQuestionQuestion) {
			this.surveyQuestionQuestion = surveyQuestionQuestion;
		}
		public Object getSurveyByUserQuestionId() {
			return surveyByUserQuestionId;
		}
		public void setSurveyByUserQuestionId(Object surveyByUserQuestionId) {
			this.surveyByUserQuestionId = surveyByUserQuestionId;
		}
		public Object getSurveyByUserAnswerId() {
			return surveyByUserAnswerId;
		}
		public void setSurveyByUserAnswerId(Object surveyByUserAnswerId) {
			this.surveyByUserAnswerId = surveyByUserAnswerId;
		}
		public Object getSurveyQuestionCategoryType() {
			return surveyQuestionCategoryType;
		}
		public void setSurveyQuestionCategoryType(Object surveyQuestionCategoryType) {
			this.surveyQuestionCategoryType = surveyQuestionCategoryType;
		}
		
	}
}
