ALTER TABLE `redsky`.`company` ADD COLUMN `cmmdmmAgent` BIT(1) DEFAULT b'0' AFTER `postingDateFlexibility`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `networkCoordinator` VARCHAR(82) AFTER `digitalSignature`;

ALTER TABLE `redsky`.`charges` CHANGE COLUMN `countryFlexiblity` `countryFlexibility` bit(1) DEFAULT b'0';

update charges set countryFlexibility=true where corpid='UTSI' and countryFlexibility is false;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `qfStatusReason` VARCHAR(30) DEFAULT NULL AFTER `emailSentDate`;

Insert into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby,language) values ('VOER', 'DUP', 'Duplicate', 30, 'QFSTATUSREASON',now(),'subrat',now(),'subrat','en'),('VOER', 'OCI', 'Other Corp-ID', 30,'QFSTATUSREASON',now(),'subrat',now(),'subrat','en'),('VOER', 'Other', 'Other', 30, 'QFSTATUSREASON',now(),'subrat',now(),'subrat','en');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('VOER','QFSTATUSREASON',30,true,now(),'subrat',now(),'subrat',1,1,'');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type) values('Billing','insuranceOption' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),('Billing','insuranceOption' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`notes` MODIFY COLUMN `subject` VARCHAR(100) DEFAULT NULL;

CREATE TABLE `redsky`.`print_daily_package` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `corpId` VARCHAR(10) DEFAULT NULL,
  `updatedOn` DATETIME DEFAULT NULL,
  `updatedBy` VARCHAR(45) DEFAULT NULL,
  `createdOn` DATETIME DEFAULT NULL,
  `createdBy` VARCHAR(45) DEFAULT NULL,
  `formName` VARCHAR(200) DEFAULT NULL,
  `formDesc` VARCHAR(500) DEFAULT NULL,
  `jrxmlName` VARCHAR(200) DEFAULT NULL,
  `parameters` VARCHAR(300) DEFAULT NULL,
  `wtServiceType` VARCHAR(100) DEFAULT NULL,
  `mode` VARCHAR(45) DEFAULT NULL,
  `noOfCopies` INTEGER DEFAULT NULL,
  `onePerSOperDay` VARCHAR(45) DEFAULT NULL,
  `military` VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`print_daily_package` MODIFY COLUMN `id` BIGINT(20) NOT NULL DEFAULT NULL AUTO_INCREMENT;

INSERT INTO print_daily_package(corpID,updatedon,updatedby,createdon,createdby,formName,formDesc,jrxmlName,parameters,wtServiceType,mode,noOfCopies,onePerSOperDay,military) VALUES('SSCW',now(),'',now(),'','Driver�s Receipt','Driver Receipt (Secor) by Workticket Date Range and WH','DriverReceiptDateWH.jrxml','WH,beginDt,endDt','All','',2,'Yes','');

ALTER TABLE `redsky`.`customerservicesurvey` MODIFY COLUMN `id` BIGINT(20) UNSIGNED NOT NULL DEFAULT NULL AUTO_INCREMENT;

ALTER TABLE `redsky`.`customerservicesurvey` MODIFY COLUMN `sOID` BIGINT(20) DEFAULT 0;

ALTER TABLE `redsky`.`customerservicesurvey` MODIFY COLUMN `questionID` BIGINT(20) DEFAULT 0, MODIFY COLUMN `answerID` BIGINT(20) DEFAULT 0;

Insert into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby,flex1,flex2,language) values
('SSCW', 'Consultation on moving needs', 'Consultation on moving needs', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Sales Consultant','Surveyresults','en'),
('SSCW', 'Explanation of packing and unpacking services', 'Explanation of packing and unpacking services', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Sales Consultant','Surveyresults','en'),
('SSCW', 'Explanation of valuation coverage', 'Explanation of valuation coverage', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Sales Consultant','Surveyresults','en'),
('SSCW', 'Product knowledge of sales people', 'Product knowledge of sales people', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Sales Consultant','Surveyresults','en'),
('SSCW', 'Keeping promises', 'Keeping promises', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Sales Consultant','Surveyresults','en'),
('SSCW', 'Attitude of sales person', 'Attitude of sales person', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Sales Consultant','Surveyresults','en'),
('SSCW', 'Confirmation of move details prior to arrival of crews', 'Confirmation of move details prior to arrival of crews', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Sales Consultant','Surveyresults','en'),
('SSCW', 'Overall rating of sales person', 'Overall rating of sales person', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Sales Consultant','Surveyresults','en');
 
Insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,flex1,flex2,language) values
('SSCW', 'Overall communication', 'Overall communication', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Move Coordinator','Surveyresults','en'),
('SSCW', 'Explanation of valuation coverage', 'Explanation of valuation coverage', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Move Coordinator','Surveyresults','en'),
('SSCW', 'Explanation of delivery, unpacking services and charges', 'Explanation of delivery, unpacking services and charges', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Move Coordinator','Surveyresults','en'),
('SSCW', 'Attitude of coordinator', 'Attitude of coordinator', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Move Coordinator','Surveyresults','en'),
('SSCW', 'Overall rating of coordinator', 'Overall rating of coordinator', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Move Coordinator','Surveyresults','en');
 
Insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,flex1,flex2,language) values
('SSCW', 'On-time Arrival?', 'On-time Arrival?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Packing Crew','Surveyresults','en'),
('SSCW', 'Crew in Uniform?', 'Crew in Uniform?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Packing Crew','Surveyresults','en'),
('SSCW', 'Handling your goods with care?', 'Handling your goods with care?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Packing Crew','Surveyresults','en'),
('SSCW', 'Labeling cartons with your name and contents?', 'Labeling cartons with your name and contents?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Packing Crew','Surveyresults','en'),
('SSCW', 'Accurately noting pre-existing conditions of goods?', 'Accurately noting pre-existing conditions of goods?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Packing Crew','Surveyresults','en'),
('SSCW', 'Attitude of packing crew', 'Attitude of packing crew', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Packing Crew','YESNO','en'),
('SSCW', 'Overall rating of packing crew', 'Overall rating of packing crew', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Packing Crew','YESNO','en');
 
Insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,flex1,flex2,language) values
('SSCW', 'On-time Arrival?', 'On-time Arrival?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Van Operator and Loading Crew','YESNO','en'),
('SSCW', 'Crew in Uniform?', 'Crew in Uniform?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Van Operator and Loading Crew','YESNO','en'),
('SSCW', 'Accurately noting pre-existing conditions of goods?', 'Accurately noting pre-existing conditions of goods?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Van Operator and Loading Crew','YESNO','en'),
('SSCW', 'Was your residence protected ?', 'Was your residence protected with floor runners and doorframe protectors by the crew?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Van Operator and Loading Crew','YESNO','en'),
('SSCW', 'Was your furniture wrapped ?', 'Was your furniture wrapped in protective covers in your home before being moved from the residence?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Van Operator and Loading Crew','YESNO','en'),
('SSCW', 'Was your upholstered furniture?', 'Was your upholstered furniture (with the exception of leather) protected with ClearGuard plastic wrap?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Van Operator and Loading Crew','YESNO','en'),
('SSCW', 'Attitude of loading crew', 'Attitude of loading crew', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Van Operator and Loading Crew','Surveyresults','en'),
('SSCW', 'Overall rating of loading crew', 'Overall rating of loading crew', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Van Operator and Loading Crew','Surveyresults','en');
 
Insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,flex1,flex2,language) values
('SSCW', 'On-time Arrival?', 'On-time Arrival?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Delivery Crew','YESNO','en'),
('SSCW', 'Crew in Uniform?', 'Crew in Uniform?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Delivery Crew','YESNO','en'),
('SSCW', 'Was your residence protected with floor runners and doorframe protectors by the crew?', 'Was your residence protected with floor runners and doorframe protectors by the crew?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Delivery Crew','YESNO','en'),
('SSCW', 'Was your furniture padded when brought into home?', 'Was your furniture padded when brought into home?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Delivery Crew','YESNO','en'),
('SSCW', 'Was a check-off of items performed at time of delivery?', 'Was a check-off of items performed at time of delivery?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Delivery Crew','YESNO','en'),
('SSCW', 'Attitude of delivery crew', 'Attitude of delivery crew', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Delivery Crew','Surveyresults','en'),
('SSCW', 'Overall rating of delivery crew', 'Overall rating of delivery crew', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Delivery Crew','Surveyresults','en');
 
Insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,flex1,flex2,language) values
('SSCW', 'On-time Arrival?', 'On-time Arrival?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Unpacking Crew','YESNO','en'),
('SSCW', 'Crew in Uniform?', 'Crew in Uniform?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Unpacking Crew','YESNO','en'),
('SSCW', 'Were you satisfied with the way your goods were unpacked?', 'Were you satisfied with the way your goods were unpacked?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Unpacking Crew','YESNO','en'),
('SSCW', 'Was the debris removed at the time of unpacking?', 'Was the debris removed at the time of unpacking?', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Unpacking Crew','YESNO','en'),
('SSCW', 'Attitude of unpacking crew', 'Attitude of unpacking crew', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Unpacking Crew','Surveyresults','en'),
('SSCW', 'Overall rating of unpacking crew', 'Overall rating of unpacking crew', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Unpacking Crew','Surveyresults','en');
 
Insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,flex1,flex2,language) values
('SSCW', 'Overall Move Evaluation', '', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Overall Move Evaluation','Surveyresults','en');
 
Insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,flex1,flex2,language) values
('SSCW', 'Overall Recommendation', '', 35, 'CustomerServiceSurveyquestions',now(),'surya',now(),'surya','Overall Recommendation','Recommendation','en');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','CustomerServiceSurveyquestions',35,true,now(),'surya',now(),'surya',1,1,'');
 
Insert into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby,language) values
('SSCW', ' ', ' ',5,'Surveyresults',now(),'surya',now(),'surya','en'),
('SSCW', 'Excellent', 'Excellent',35,'Surveyresults',now(),'surya',now(),'surya','en'),
('SSCW', 'Good', 'Good', 35, 'Surveyresults',now(),'surya',now(),'surya','en'),
('SSCW', 'Fair', 'Fair', 35, 'Surveyresults',now(),'surya',now(),'surya','en'),
('SSCW', 'Poor', 'Poor', 35, 'Surveyresults',now(),'surya',now(),'surya','en');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','Surveyresults',35,true,now(),'surya',now(),'surya',1,1,'');
 
Insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,language) values
('SSCW', ' ', ' ',5,'Recommendation',now(),'surya',now(),'surya','en'),
('SSCW', 'Highly recommend', 'Highly recommend',35,'Recommendation',now(),'surya',now(),'surya','en'),
('SSCW', 'Would recommend', 'Would recommend', 35, 'Recommendation',now(),'surya',now(),'surya','en'),
('SSCW', 'Might recommend', 'Might recommend', 35, 'Recommendation',now(),'surya',now(),'surya','en'),
('SSCW', 'Would not recommend', 'Would not recommend', 35, 'Recommendation',now(),'surya',now(),'surya','en');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('SSCW','Recommendation',35,true,now(),'surya',now(),'surya',1,1,'');

ALTER TABLE `redsky`.`crew` ADD COLUMN `partnerCode` VARCHAR(8) AFTER `paidSickLeave`,ADD COLUMN `partnerName` VARCHAR(225) AFTER `partnerCode`;

update refmaster set flex2='Surveyresults' where flex2='Survey results' and flex1='Packing Crew' and parameter='CustomerServiceSurveyquestions' and corpid='SSCW' and language='en';

ALTER TABLE `redsky`.`customerservicesurvey` CHANGE COLUMN `sOID` `soID` BIGINT(20) DEFAULT 0;
 
ALTER TABLE `redsky`.`customerservicesurvey` RENAME TO `redsky`.`surveyresult`;

ALTER TABLE `redsky`.`vanline` ADD COLUMN `driverName` VARCHAR(50) AFTER `accCreatedOn`,ADD COLUMN `statementCategory` VARCHAR(10) AFTER `driverName`;
 
ALTER TABLE `redsky`.`miscellaneous` ADD COLUMN `assignDate` DATETIME AFTER `totalActualRevenue`;

