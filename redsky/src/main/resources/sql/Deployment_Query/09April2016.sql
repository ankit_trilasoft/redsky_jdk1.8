// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

CREATE TABLE `losspicture` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT,
     `corpID` varchar(45) NOT NULL,
     `createdBy` varchar(82) DEFAULT NULL,
     `createdOn` datetime DEFAULT NULL,
     `updatedBy` varchar(82) DEFAULT NULL,
     `updatedOn` datetime DEFAULT NULL,
     `photoId` varchar(500) DEFAULT NULL,
     `photoUri` varchar(500) DEFAULT NULL,
     `fileName` varchar(500) DEFAULT NULL,
     `lossId` bigint(20) DEFAULT NULL,
     `claimId` bigint(20) DEFAULT NULL,
     PRIMARY KEY (`id`)
   );
   
   ALTER TABLE `redsky`.`standardaddresses` ADD COLUMN `residenceType` VARCHAR(25) NULL  AFTER `mobPhone` ,
ADD COLUMN `residenceMgmtName` VARCHAR(200) NULL  AFTER `residenceType` ,
ADD COLUMN `residenceDeposit1` VARCHAR(25) NULL  AFTER `residenceMgmtName` ,
ADD COLUMN `residencePurpose1` VARCHAR(100) NULL  AFTER `residenceDeposit1` ,
ADD COLUMN `residenceDeposit2` VARCHAR(25) NULL  AFTER `residencePurpose1` ,
ADD COLUMN `residencePurpose2` VARCHAR(100) NULL  AFTER `residenceDeposit2` ,
ADD COLUMN `residencePayType` VARCHAR(25) NULL  AFTER `residencePurpose2` ,
ADD COLUMN `residenceNotes` VARCHAR(500) NULL  AFTER `residencePayType` ;

INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('IMSB','RESIDENCETYPE',50,true,now(),'IMSB_SETUP',now(),'IMSB_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('IMSB', 'Hotel', 'Hotel', 50, 'RESIDENCETYPE','en',now(),'IMSB_SETUP',now(),'IMSB_SETUP'),
('IMSB', 'Condominium', 'Condominium', 50, 'RESIDENCETYPE','en',now(),'IMSB_SETUP',now(),'IMSB_SETUP'),
('IMSB', 'Terrace', 'Terrace', 50, 'RESIDENCETYPE','en',now(),'IMSB_SETUP',now(),'IMSB_SETUP'),
('IMSB', 'Bungalow', 'Bungalow', 50, 'RESIDENCETYPE','en',now(),'IMSB_SETUP',now(),'IMSB_SETUP'),
('IMSB', 'Apartment', 'Apartment', 50, 'RESIDENCETYPE','en',now(),'IMSB_SETUP',now(),'IMSB_SETUP');


INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('IMSB','RESIDENCEDEPOSITTYPE',50,true,now(),'IMSB_SETUP',now(),'IMSB_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('IMSB', 'Refundable', 'Refundable', 50, 'RESIDENCEDEPOSITTYPE','en',now(),'IMSB_SETUP',now(),'IMSB_SETUP'),
('IMSB', 'Non-Refundable', 'Non-Refundable', 50, 'RESIDENCEDEPOSITTYPE','en',now(),'IMSB_SETUP',now(),'IMSB_SETUP');

insert into role(name,description,corpid,category) values('ROLE_UNPOST_VENDOR','Unposting accountline','SUDD','USER');
insert into role(name,description,corpid,category) values('ROLE_UNPOST_VENDOR','Unposting accountline','SSCW','USER');

ALTER TABLE `redsky`.`accountline` ADD COLUMN `accrueRevenueVariance` DATETIME NULL  AFTER `varianceExpenseAmount` ;

ALTER TABLE `redsky`.`billing` ADD COLUMN `contractReceivedDate` DATETIME NULL  AFTER `networkBillToName` ;

insert into role(name,description,corpid,category) values('ROLE_INTERNAL_BILLING','Internal Billing Role','SSCW','USER');
ALTER TABLE `redsky`.`billing` ADD COLUMN `internalBillingPerson` VARCHAR(82) NULL DEFAULT ''  AFTER `contractReceivedDate`;
ALTER TABLE `redsky`.`customerfile` ADD COLUMN `internalBillingPerson` VARCHAR(82) NULL  AFTER `isUpdater` ; 

ALTER TABLE `redsky`.`standardaddresses` ADD COLUMN `depositAmount1` DECIMAL(19,2) NULL  AFTER `residenceNotes` ,
ADD COLUMN `depositAmount2` DECIMAL(19,2) NULL  AFTER `depositAmount1` ;

ALTER TABLE `redsky`.`workticket` ADD COLUMN `originAddressBook` VARCHAR(30) NULL  AFTER `ticketAssignedStatus` ,
ADD COLUMN `destinationAddressBook` VARCHAR(30) NULL  AFTER `originAddressBook` ;

ALTER TABLE `redsky`.`servicepartner` ADD COLUMN `vendorName` VARCHAR(255) AFTER `status`,
 ADD COLUMN `grade` VARCHAR(255) AFTER `vendorName`,
 ADD COLUMN `containerType` VARCHAR(255) AFTER `grade`,
 ADD COLUMN `freeDatewithContainer` DATETIME AFTER `containerType`,
 ADD COLUMN `chasisProvide` BIT(1) AFTER `freeDatewithContainer`,
 ADD COLUMN `containerDays` INTEGER AFTER `chasisProvide`,
 ADD COLUMN `containerFreeDays` INTEGER AFTER `containerDays`,
 ADD COLUMN `containerFreeDaysSubTotal` DOUBLE AFTER `containerFreeDays`,
 ADD COLUMN `containerFreeDaysTotal` DOUBLE AFTER `containerFreeDaysSubTotal`,
 ADD COLUMN `chasisDays` INTEGER AFTER `containerFreeDaysTotal`,
 ADD COLUMN `chasisfreeDays` INTEGER AFTER `chasisDays`,
 ADD COLUMN `chasisfreeDaysSubTotal` DOUBLE AFTER `chasisfreeDays`,
 ADD COLUMN `chasisfreeDaysTotal` DOUBLE AFTER `chasisfreeDaysSubTotal`,
 ADD COLUMN `otherChargesDesc` VARCHAR(255) AFTER `chasisfreeDaysTotal`,
 ADD COLUMN `otherChargesAmount` DOUBLE AFTER `otherChargesDesc`,
 ADD COLUMN `inLandTotalAmount` DOUBLE AFTER `otherChargesAmount`,
 ADD COLUMN `sentTOFPickUp` DATETIME AFTER `inLandTotalAmount`,
 ADD COLUMN `freeDateAtPort` DATETIME AFTER `sentTOFPickUp`,
 ADD COLUMN `outOfPort` DATETIME AFTER `freeDateAtPort`,
 ADD COLUMN `requestPickDate` DATETIME AFTER `outOfPort`,
 ADD COLUMN `actualPickDate` DATETIME AFTER `requestPickDate`,
 ADD COLUMN `requestDropDate` DATETIME AFTER `actualPickDate`,
 ADD COLUMN `actualDropDate` DATETIME AFTER `requestDropDate`,
 ADD COLUMN `sentTruckingOrderforReturn` DATETIME AFTER `actualDropDate`,
 ADD COLUMN `returntoPortActual` DATETIME AFTER `sentTruckingOrderforReturn`,
 ADD COLUMN `liveLoadDate` DATETIME AFTER `returntoPortActual`,
 ADD COLUMN `liveUnLoadDate` DATETIME AFTER `liveLoadDate`;
 
 ALTER TABLE `redsky`.`refjobtype` ADD COLUMN `internalBillingPerson` VARCHAR(82) NULL  AFTER `accAccountPortal` ;
ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `internalBillingPerson` VARCHAR(82) NULL  AFTER `rddDays` ;

ALTER TABLE `redsky`.`trackingstatus`
 DROP COLUMN `vendorId`,
 DROP COLUMN `vendorName` ,
 DROP COLUMN `grade` ,
 DROP COLUMN `containerType` ,
 DROP COLUMN `freeDatewithContainer` ,
 DROP COLUMN `chasisProvide` ,
 DROP COLUMN `containerDays` ,
 DROP COLUMN `containerFreeDays` ,
 DROP COLUMN `containerFreeDaysSubTotal` ,
 DROP COLUMN `containerFreeDaysTotal` ,
 DROP COLUMN `chasisDays` ,
 DROP COLUMN `chasisfreeDays` ,
 DROP COLUMN `chasisfreeDaysSubTotal` ,
 DROP COLUMN `chasisfreeDaysTotal` ,
 DROP COLUMN `otherChargesDesc` ,
 DROP COLUMN `otherChargesAmount` ,
 DROP COLUMN `inLandTotalAmount` ,
 DROP COLUMN `sentTOFPickUp` ,
 DROP COLUMN `freeDateAtPort` ,
 DROP COLUMN `outOfPort` ,
 DROP COLUMN `requestPickDate` ,
 DROP COLUMN `actualPickDate` ,
 DROP COLUMN `requestDropDate` ,
 DROP COLUMN `actualDropDate` ,
 DROP COLUMN `sentTruckingOrderforReturn` ,
 DROP COLUMN `returntoPortActual` ,
 DROP COLUMN `liveLoad` ,
 DROP COLUMN `liveUnLoad`;
 
 ALTER TABLE `redsky`.`servicepartner`
 DROP COLUMN `vendorName` ,
 DROP COLUMN `grade` ,
 DROP COLUMN `containerType` ,
 DROP COLUMN `freeDatewithContainer` ,
 DROP COLUMN `chasisProvide` ,
 DROP COLUMN `containerDays` ,
 DROP COLUMN `containerFreeDays` ,
 DROP COLUMN `containerFreeDaysSubTotal` ,
 DROP COLUMN `containerFreeDaysTotal` ,
 DROP COLUMN `chasisDays` ,
 DROP COLUMN `chasisfreeDays` ,
 DROP COLUMN `chasisfreeDaysSubTotal` ,
 DROP COLUMN `chasisfreeDaysTotal` ,
 DROP COLUMN `otherChargesDesc` ,
 DROP COLUMN `otherChargesAmount` ,
 DROP COLUMN `inLandTotalAmount` ,
 DROP COLUMN `sentTOFPickUp` ,
 DROP COLUMN `freeDateAtPort` ,
 DROP COLUMN `outOfPort` ,
 DROP COLUMN `requestPickDate` ,
 DROP COLUMN `actualPickDate` ,
 DROP COLUMN `requestDropDate` ,
 DROP COLUMN `actualDropDate` ,
 DROP COLUMN `sentTruckingOrderforReturn` ,
 DROP COLUMN `returntoPortActual` ,
 DROP COLUMN `liveLoadDate` ,
 DROP COLUMN `liveUnLoadDate`;
 
 CREATE TABLE `redsky`.`inlandagent` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  `sequenceNumber` VARCHAR(15),
  `shipNumber` VARCHAR(15),
  `serviceOrderId` BIGINT(20),
  `ship` VARCHAR(2),
  `vendorName` VARCHAR(255),
  `vendorId` VARCHAR(8),
  `grade` VARCHAR(255),
  `containerType` VARCHAR(255),
  `freeDatewithContainer` DATETIME,
  `chasisProvide` BIT(1),
  `otherChargesDesc` VARCHAR(255),
  `sentTOFPickUp` DATETIME,
  `freeDateAtPort` DATETIME,
  `outOfPort` DATETIME,
  `requestPickDate` DATETIME,
  `actualPickDate` DATETIME,
  `requestDropDate` DATETIME,
  `actualDropDate` DATETIME,
  `sentTruckingOrderforReturn` DATETIME,
  `returntoPortActual` DATETIME,
  `liveLoadDate` DATETIME,
  `liveUnLoadDate` DATETIME,
  `otherChargesAmount` DOUBLE,
  `inLandTotalAmount` DOUBLE,
  `chasisfreeDaysSubTotal` DOUBLE,
  `chasisfreeDaysTotal` DOUBLE,
  `containerDays` INTEGER,
  `containerFreeDays` INTEGER,
  `containerFreeDaysSubTotal` DOUBLE,
  `containerFreeDaysTotal` DOUBLE,
  `chasisDays` INTEGER,
  `chasisfreeDays` INTEGER,
  PRIMARY KEY(`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`standardaddresses` ADD COLUMN `email` VARCHAR(60) NULL  AFTER `depositAmount2` ,
ADD COLUMN `contactPersonEmail` VARCHAR(60) NULL  AFTER `email` ,
ADD COLUMN `contactPerson` VARCHAR(80) NULL  AFTER `contactPersonEmail` ,
ADD COLUMN `contactPersonPhone` VARCHAR(20) NULL  AFTER `contactPerson` ;
   
 Insert into refmaster
    (corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
    ('VOER', 'FRL', 'Furniture Rental', 5, 'SERVICE','en','Relo','dspDetails.FRL_serviceEndDate',now(),'dkumar',now(),'dkumar'),
    ('VOER', 'APU', 'Airport Pick Up', 5, 'SERVICE','en','Relo','dspDetails.APU_serviceEndDate',now(),'dkumar',now(),'dkumar');

    Insert into refmaster
    (corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
    ('VOER', 'DsFurnitureRental', 'DsFurnitureRental', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
    ('VOER', 'DsAirportPickUp', 'DsAirportPickUp', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar');

    Insert into refmaster
    (corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
    ('UTSI', 'FRL', 'Furniture Rental', 5, 'SERVICE','en','Relo','dspDetails.FRL_serviceEndDate',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'APU', 'Airport Pick Up', 5, 'SERVICE','en','Relo','dspDetails.APU_serviceEndDate',now(),'dkumar',now(),'dkumar');

    Insert into refmaster
    (corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
    ('UTSI', 'DsFurnitureRental', 'DsFurnitureRental', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar'),
    ('UTSI', 'DsAirportPickUp', 'DsAirportPickUp', 20, 'NOTESUBTYPE','en','Service Order',now(),'dkumar',now(),'dkumar');


Insert into dspnetworkfield(corpid,createdon,createdby,updatedon,updatedby,tableName,type,fieldName)values
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_arrivalDate'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_carColor'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_carMake'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_carModel'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_displyOtherVendorCode'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_driverName'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_driverPhoneNumber'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_emailSent'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_flightDetails'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_paymentResponsibility'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_serviceEndDate'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_serviceStartDate'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorCode'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorCodeEXSO'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorContact'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorEmail'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','APU','APU_vendorName'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_comment'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_deposit'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_depositCurrency'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_displyOtherVendorCode'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_emailSent'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_month'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_paymentResponsibility'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalCurrency'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalEndDate'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalRate'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_rentalStartDate'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_serviceEndDate'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_serviceStartDate'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_terminationNotice'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorCode'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorCodeEXSO'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorContact'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorEmail'),
('VOER', now(),'dkumar', now(),'dkumar','dspdetails','FRL','FRL_vendorName');  


ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `incExcDocTypeLanguage` VARCHAR(5) NULL  AFTER `accrualReadyDate` ;
ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `incExcDocServiceType` TEXT NULL  AFTER `incExcDocTypeLanguage` ;

ALTER TABLE `redsky`.`taskdetails` ADD COLUMN `copyId` BIGINT(20); 

ALTER TABLE `redsky`.`timesheet` CHANGE COLUMN `adjustedBy` `adjustedBy` VARCHAR(82) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL  ;