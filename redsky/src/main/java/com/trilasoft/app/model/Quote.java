package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table ( name="quote")
public class Quote  extends BaseObject {
	
	private CustomerFile customerFile;
	private Long quoteid;
	private Long id;
	private Long quoteNumber;
	private String sequenceNumber;
	private String corpID;
	private String description;
	private String billToCode;
	private String comment1;
	private String comment2;
	private String perItem;
	private String Item;
	private String divideOrMultiply;
	private String billToName;
	private String providedBy;
	private String providedAs;
	private String type;
	private BigDecimal rate;
	private BigDecimal per;
	private BigDecimal amount;
	private BigDecimal discount;
	private int quantity;
	private Date billed;
	private Date sentWB;

//	 ---------------------------------------------- //

	private String sortGroup;
	private BigDecimal shortAul;
	private int invoiceNumber;
	private String comment;
	private BigDecimal estimate;
	private int ticket;
	private Date posted;
	private Date sentAccount;
	private String sentFile;
	private Date estimateDate;
	private BigDecimal entitledAmount;
	private Date estimateSent;
	private String keyer;
	private Date storageBegin;
	private Date storageEnd;
	private String code;
	private int days;
	private BigDecimal intTicket;
	private String createdBy;
	private Date createdOn;
	private Date updatedOn;
	private String updatedBy;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("quoteid", quoteid).append("id", id).append(
						"quoteNumber", quoteNumber).append("sequenceNumber",
						sequenceNumber).append("corpID", corpID).append(
						"description", description).append("billToCode",
						billToCode).append("comment1", comment1).append(
						"comment2", comment2).append("perItem", perItem)
				.append("Item", Item).append("divideOrMultiply",
						divideOrMultiply).append("billToName", billToName)
				.append("providedBy", providedBy).append("providedAs",
						providedAs).append("type", type).append("rate", rate)
				.append("per", per).append("amount", amount).append("discount",
						discount).append("quantity", quantity).append("billed",
						billed).append("sentWB", sentWB).append("sortGroup",
						sortGroup).append("shortAul", shortAul).append(
						"invoiceNumber", invoiceNumber).append("comment",
						comment).append("estimate", estimate).append("ticket",
						ticket).append("posted", posted).append("sentAccount",
						sentAccount).append("sentFile", sentFile).append(
						"estimateDate", estimateDate).append("entitledAmount",
						entitledAmount).append("estimateSent", estimateSent)
				.append("keyer", keyer).append("storageBegin", storageBegin)
				.append("storageEnd", storageEnd).append("code", code).append(
						"days", days).append("intTicket", intTicket).append(
						"createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Quote))
			return false;
		Quote castOther = (Quote) other;
		return new EqualsBuilder().append(quoteid, castOther.quoteid).append(id, castOther.id)
				.append(quoteNumber, castOther.quoteNumber).append(
						sequenceNumber, castOther.sequenceNumber).append(
						corpID, castOther.corpID).append(description,
						castOther.description).append(billToCode,
						castOther.billToCode).append(comment1,
						castOther.comment1)
				.append(comment2, castOther.comment2).append(perItem,
						castOther.perItem).append(Item, castOther.Item).append(
						divideOrMultiply, castOther.divideOrMultiply).append(
						billToName, castOther.billToName).append(providedBy,
						castOther.providedBy).append(providedAs,
						castOther.providedAs).append(type, castOther.type)
				.append(rate, castOther.rate).append(per, castOther.per)
				.append(amount, castOther.amount).append(discount,
						castOther.discount)
				.append(quantity, castOther.quantity).append(billed,
						castOther.billed).append(sentWB, castOther.sentWB)
				.append(sortGroup, castOther.sortGroup).append(shortAul,
						castOther.shortAul).append(invoiceNumber,
						castOther.invoiceNumber).append(comment,
						castOther.comment).append(estimate, castOther.estimate)
				.append(ticket, castOther.ticket).append(posted,
						castOther.posted).append(sentAccount,
						castOther.sentAccount).append(sentFile,
						castOther.sentFile).append(estimateDate,
						castOther.estimateDate).append(entitledAmount,
						castOther.entitledAmount).append(estimateSent,
						castOther.estimateSent).append(keyer, castOther.keyer)
				.append(storageBegin, castOther.storageBegin).append(
						storageEnd, castOther.storageEnd).append(code,
						castOther.code).append(days, castOther.days).append(
						intTicket, castOther.intTicket).append(createdBy,
						castOther.createdBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(updatedBy,
						castOther.updatedBy).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(quoteid)
				.append(id).append(quoteNumber).append(sequenceNumber).append(
						corpID).append(description).append(billToCode).append(
						comment1).append(comment2).append(perItem).append(Item)
				.append(divideOrMultiply).append(billToName).append(providedBy)
				.append(providedAs).append(type).append(rate).append(per)
				.append(amount).append(discount).append(quantity)
				.append(billed).append(sentWB).append(sortGroup).append(
						shortAul).append(invoiceNumber).append(comment).append(
						estimate).append(ticket).append(posted).append(
						sentAccount).append(sentFile).append(estimateDate)
				.append(entitledAmount).append(estimateSent).append(keyer)
				.append(storageBegin).append(storageEnd).append(code).append(
						days).append(intTicket).append(createdBy).append(
						createdOn).append(updatedOn).append(updatedBy)
				.toHashCode();
	}

	/*		@OneToMany(cascade = CascadeType.ALL)
	    @JoinColumn(name="sequenceNumber",
	    referencedColumnName="sequenceNumber")
	    public CustomerFile getCustomerFile() {
	        return customerFile;
	    }
	    
		 public void setCustomerFile(CustomerFile customerFile) {
				this.customerFile = customerFile;
		}  
*/
	@ManyToOne
	@JoinColumn(name="sequenceNumber", nullable=false, updatable=false,
	referencedColumnName="sequenceNumber")
	public CustomerFile getCustomerFile() {
	        return customerFile;
	    }
	    
		 public void setCustomerFile(CustomerFile customerFile) {
				this.customerFile = customerFile;
		}  

		
/*	@Column
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}*/
	@Column(nullable=true, precision=15, scale=5)
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	@Column
	public Date getBilled() {
		return billed;
	}
	public void setBilled(Date billed) {
		this.billed = billed;
	}
	@Column( length=8 )
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	@Column( length=40 )
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	@Column( length=6 )
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column( length=45 )
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Column( length=80 )
	public String getComment1() {
		return comment1;
	}
	public void setComment1(String comment1) {
		this.comment1 = comment1;
	}
	@Column( length=80 )
	public String getComment2() {
		return comment2;
	}
	public void setComment2(String comment2) {
		this.comment2 = comment2;
	}
	@Column( length=15 )
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	@Column( length=65 )
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(nullable=true, precision=15, scale=5)
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	@Column( length=1 )
	public String getDivideOrMultiply() {
		return divideOrMultiply;
	}
	public void setDivideOrMultiply(String divideOrMultiply) {
		this.divideOrMultiply = divideOrMultiply;
	}
	@Column(nullable=true, precision=15, scale=5)
	public BigDecimal getEntitledAmount() {
		return entitledAmount;
	}
	public void setEntitledAmount(BigDecimal entitledAmount) {
		this.entitledAmount = entitledAmount;
	}
	@Column(nullable=true, precision=15, scale=5)
	public BigDecimal getEstimate() {
		return estimate;
	}
	public void setEstimate(BigDecimal estimate) {
		this.estimate = estimate;
	}
	@Column
	public Date getEstimateDate() {
		return estimateDate;
	}
	public void setEstimateDate(Date estimateDate) {
		this.estimateDate = estimateDate;
	}
	@Column
	public Date getEstimateSent() {
		return estimateSent;
	}
	public void setEstimateSent(Date estimateSent) {
		this.estimateSent = estimateSent;
	}
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO) 

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(nullable=true, precision=15, scale=5)
	public BigDecimal getIntTicket() {
		return intTicket;
	}
	public void setIntTicket(BigDecimal intTicket) {
		this.intTicket = intTicket;
	}
	@Column
	public int getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(int invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	@Column( length=45 )
	public String getItem() {
		return Item;
	}
	public void setItem(String item) {
		Item = item;
	}
	@Column( length=15 )
	public String getKeyer() {
		return keyer;
	}
	public void setKeyer(String keyer) {
		this.keyer = keyer;
	}
	@Column(nullable=true, precision=15, scale=5)
	public BigDecimal getPer() {
		return per;
	}
	public void setPer(BigDecimal per) {
		this.per = per;
	}
	@Column( length=15 )
	public String getPerItem() {
		return perItem;
	}
	public void setPerItem(String perItem) {
		this.perItem = perItem;
	}
	@Column
	public Date getPosted() {
		return posted;
	}
	public void setPosted(Date posted) {
		this.posted = posted;
	}
	@Column( length=1 )
	public String getProvidedAs() {
		return providedAs;
	}
	public void setProvidedAs(String providedAs) {
		this.providedAs = providedAs;
	}
	@Column( length=8 )
	public String getProvidedBy() {
		return providedBy;
	}
	public void setProvidedBy(String providedBy) {
		this.providedBy = providedBy;
	}
	@Column
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Column
	public Long getQuoteNumber() {
		return quoteNumber;
	}
	public void setQuoteNumber(Long quoteNumber) {
		this.quoteNumber = quoteNumber;
	}
	@Column(nullable=true, precision=15, scale=5)
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	@Column
	public Date getSentAccount() {
		return sentAccount;
	}
	public void setSentAccount(Date sentAccount) {
		this.sentAccount = sentAccount;
	}
	@Column( length=50 )
	public String getSentFile() {
		return sentFile;
	}
	public void setSentFile(String sentFile) {
		this.sentFile = sentFile;
	}
	@Column
	public Date getSentWB() {
		return sentWB;
	}
	public void setSentWB(Date sentWB) {
		this.sentWB = sentWB;
	}

	@Column(nullable=true, precision=15, scale=5)
	public BigDecimal getShortAul() {
		return shortAul;
	}
	public void setShortAul(BigDecimal shortAul) {
		this.shortAul = shortAul;
	}
	@Column( length=2 )
	public String getSortGroup() {
		return sortGroup;
	}
	public void setSortGroup(String sortGroup) {
		this.sortGroup = sortGroup;
	}
	@Column
	public Date getStorageBegin() {
		return storageBegin;
	}
	public void setStorageBegin(Date storageBegin) {
		this.storageBegin = storageBegin;
	}
	@Column
	public Date getStorageEnd() {
		return storageEnd;
	}
	public void setStorageEnd(Date storageEnd) {
		this.storageEnd = storageEnd;
	}
	@Column
	public int getTicket() {
		return ticket;
	}
	public void setTicket(int ticket) {
		this.ticket = ticket;
	}
	@Column( length=7 )
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column
	public Long getQuoteid() {
		return quoteid;
	}

	public void setQuoteid(Long quoteid) {
		this.quoteid = quoteid;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
