<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="StorageLocations"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="612"
		 pageHeight="792"
		 columnWidth="576"
		 columnSpacing="0"
		 leftMargin="18"
		 rightMargin="18"
		 topMargin="18"
		 bottomMargin="18"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.zoom" value="1.0" />
	<property name="ireport.x" value="0" />
	<property name="ireport.y" value="0" />
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Warehouse" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT	"StorageLocations-R-PDF-PUTT.jrxml",
	DATE_FORMAT(now(),'%d-%b-%y') AS currentdate,
	if(serviceorder.companyDivision is null || serviceorder.companyDivision='','',serviceorder.companyDivision) AS serviceorder_companyDivision,
	serviceorder.`shipNumber` AS serviceorder_shipNumber,
	trim(serviceorder.registrationNumber) AS serviceorder_registrationNumber,
	storage.locationId AS storage_locationId,
	trim(storage.description) AS storage_description,
	location.type AS location_type,
	location.occupied AS location_occupied,
	if((serviceorder.`firstName` is null || trim(serviceorder.`firstName`)=''),trim(serviceorder.`lastName`),concat(trim(serviceorder.`firstName`)," ",trim(serviceorder.`lastName`))) AS shipper
FROM	`serviceorder` serviceorder
	LEFT OUTER JOIN `storage` storage ON serviceorder.`shipNumber` = storage.`shipNumber` AND storage.`corpID` = $P{Corporate ID}
	INNER JOIN `billing` billing ON serviceorder.`id` = billing.`id` AND billing.`corpID` = $P{Corporate ID}
	LEFT OUTER JOIN `location` location ON storage.`locationId` = location.`locationId` AND location.`corpID` = $P{Corporate ID}
WHERE
	serviceorder.`corpID` = $P{Corporate ID}
	AND serviceorder.`status` NOT IN('CNCL','CLSD')
	AND billing.`storageOut` IS NULL
	AND storage.`releaseDate` IS NULL
	AND (if(LEFT(storage.`locationId`,2) IN('12','14'),LEFT(storage.`locationId`,2),RIGHT(LEFT(storage.`locationId`,2),1)) LIKE $P{Warehouse})
GROUP BY storage.`locationId`
ORDER BY serviceorder.companyDivision,shipper,storage.`locationId`]]></queryString>

	<field name="StorageLocations-R-PDF-PUTT.jrxml" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_registrationNumber" class="java.lang.String"/>
	<field name="storage_locationId" class="java.lang.String"/>
	<field name="storage_description" class="java.lang.String"/>
	<field name="location_type" class="java.lang.String"/>
	<field name="location_occupied" class="java.lang.String"/>
	<field name="shipper" class="java.lang.String"/>


		<group  name="CompDiv" isStartNewPage="true" >
			<groupExpression><![CDATA[]]></groupExpression>
			<groupHeader>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="102"
						y="0"
						width="331"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="102"
						height="18"
						key="staticText-29"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="shipper" isReprintHeaderOnEachPage="true" >
			<groupExpression><![CDATA[$F{shipper}]]></groupExpression>
			<groupHeader>
			<band height="25"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="8"
						width="576"
						height="16"
						forecolor="#800080"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{shipper}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="33"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="576"
						height="18"
						forecolor="#0033CC"
						key="staticText-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Storage Locations By Row # And Pallets]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="17"
						width="331"
						height="16"
						forecolor="#CC0033"
						key="staticText-21"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[For Warehouse #]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="336"
						y="17"
						width="240"
						height="16"
						forecolor="#CC0033"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$P{Warehouse}.equals("%") || $P{Warehouse}.equals("%%") ? "All" : $P{Warehouse}]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="25"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="7"
						width="76"
						height="18"
						key="staticText-22"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Location]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="76"
						y="7"
						width="90"
						height="18"
						key="staticText-23"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Ship No.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="166"
						y="7"
						width="87"
						height="18"
						key="staticText-24"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Reg#]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="252"
						y="7"
						width="151"
						height="18"
						key="staticText-26"/>
					<box></box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Description]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="403"
						y="7"
						width="38"
						height="18"
						key="staticText-27"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Type]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="441"
						y="7"
						width="59"
						height="18"
						key="staticText-28"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Occupied]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="76"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{storage_locationId}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="76"
						y="0"
						width="90"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="166"
						y="0"
						width="87"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_registrationNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="252"
						y="0"
						width="151"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{storage_description}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="403"
						y="0"
						width="23"
						height="18"
						forecolor="#CC0033"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{location_type}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="441"
						y="0"
						width="59"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{location_occupied}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="23"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="368"
						y="6"
						width="170"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="540"
						y="6"
						width="36"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="6"
						width="116"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{currentdate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="198"
						y="6"
						width="180"
						height="17"
						key="staticText-20"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="8"/>
					</textElement>
				<text><![CDATA[StorageLocations.jrxml]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="-1"
						y="2"
						width="574"
						height="1"
						key="line-2"/>
					<graphicElement stretchType="NoStretch" fill="Solid" />
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
