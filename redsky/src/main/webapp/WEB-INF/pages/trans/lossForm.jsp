<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title><fmt:message key="lossDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='lossDetail.heading'/>"/>
    <style type="text/css">h2 {background-color: #FBBFFF}</style>
    <style><%@ include file="/common/calenderStyle.css"%></style>
	<style>.table thead th{height:18px;}</style>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">

animatedcollapse.addDiv('paymentStatus', 'fade=0,persist=1,hide=1')

animatedcollapse.init()
		
	
		<sec-auth:authComponent componentId="module.script.form.agentScript">
		$.noConflict();
		jQuery( document ).ready(function( $ ) {				
			$("#lossForm input.textUpper").attr("readonly", "true");
			$("#lossForm input.input-text").attr("disabled", "disabled");
			$("#lossForm select").attr("disabled", "disabled");
			$("#lossForm input:checkbox").attr("disabled", "disabled");
			$("#lossForm input:image").attr("disabled", "disabled");
			$("#datePurchased_trigger").attr("disabled", "disabled");
			$("#lossForm input:submit").hide();
			$("#lossForm input:button").hide();
    		$('#lossForm').find('img[src="${pageContext.request.contextPath}/images/calender.png"]').attr('src','images/navarrow.gif');
    		$('#lossForm').find('img[src="${pageContext.request.contextPath}/images/notes_empty1.jpg"]').attr('src','images/navarrow.gif');
    		$('#lossForm').find('img[src="${pageContext.request.contextPath}/images/notes_open1.jpg"]').attr('src','images/navarrow.gif');
    		$('#lossForm').find('img[src="/redsky/images/open-popup.gif"]').attr('src','images/navarrow.gif');
    		$('#lossForm').find('img[src="/images/navdisable_03.png"]').attr('src','images/navarrow.gif');
    		$('#lossForm').find('img[src="/images/navdisable_04.png"]').attr('src','images/navarrow.gif');
    		$('#lossForm').find('img[src="/images/navdisable_05.png"]').attr('src','images/navarrow.gif');
		});
		</sec-auth:authComponent>
</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->



<script language="JavaScript">
<sec-auth:authComponent componentId="module.script.claim.form.corpAccountScript">

	window.onload = function() { 

		trap();
		var elementsLen=document.forms['lossForm'].elements.length;
		for(i=0;i<=elementsLen-1;i++)
			{
				if(document.forms['lossForm'].elements[i].type=='text')
					{
						document.forms['lossForm'].elements[i].readOnly =true;
						document.forms['lossForm'].elements[i].className = 'input-textUpper';
						
					}
					else
					{
						document.forms['lossForm'].elements[i].disabled=true;
					}
					
						
			}
			
	}
</sec-auth:authComponent>
 function trap(){
	 if(document.images) {
	      var totalImages = document.images.length;
	      	for (var i=0;i<totalImages;i++) {
	    		$('#lossForm').find('img[src="${pageContext.request.contextPath}/images/calender.png"]').attr('src','images/navarrow.gif');
	    		$('#lossForm').find('img[src="${pageContext.request.contextPath}/images/notes_empty1.jpg"]').attr('src','images/navarrow.gif');
	    		$('#lossForm').find('img[src="${pageContext.request.contextPath}/images/notes_open1.jpg"]').attr('src','images/navarrow.gif');
	    		$('#lossForm').find('img[src="/redsky/images/open-popup.gif"]').attr('src','images/navarrow.gif');
	    		$('#lossForm').find('img[src="images/navarrows_03.png"]').attr('src','images/navarrow.gif');
	    		$('#lossForm').find('img[src="images/navarrows_04.png"]').attr('src','images/navarrow.gif');
	    		$('#lossForm').find('img[src="${pageContext.request.contextPath}/images/navarrows_05.png"]').attr('src','images/navarrow.gif');		    		
	    		$('#lossForm').find('img[src="images/navdisable_03.png"]').attr('src','images/navarrow.gif');
	    		$('#lossForm').find('img[src="images/navdisable_04.png"]').attr('src','images/navarrow.gif');
	    		$('#lossForm').find('img[src="images/navdisable_05.png"]').attr('src','images/navarrow.gif');
			} } 
		 
}

function test(){
	return false;
}
function right(e) {

	//var msg = "Sorry, you don't have permission.";
	if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
	}
	
	if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
	}

	else return true;
}
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	
function confirmSubmit(targetElement){
	var agree=confirm('Are you sure you want to remove this Claim Loss Ticket?');
	var id = targetElement;
	if (agree){
		location.href='deleteTicketToLoss.html?id='+id+'&lossNumber=${loss.id}';
		//setTimeout("xx()",5000)
	}
}
function xx()
{
		document.forms['lossForm'].elements['btnType'].value = 'submit';
		document.forms['lossForm'].submit();
}
function doNotSubmit(){
	alert("Cannot delete this Claim Loss Ticket as this Loss is not created Yet");
}
	
</script>
<script LANGUAGE="JavaScript">
function checkVendorName(){
    var vendorId =  document.forms['lossForm'].elements['loss.whoWorkCDE'].value;
    if(vendorId == ''){
    	 document.forms['lossForm'].elements['loss.whoWorkName'].value ='';
    }
    if(vendorId != ''){
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    } 
}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2(){
 			if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                
                var res = results.split("#"); 
           		if(res.size() >= 2){ 
	           		if(res[2] == 'Approved'){
	           			 document.forms['lossForm'].elements['loss.whoWorkName'].value = res[1];
	           		}else{
	           			alert("Vendor code is not approved" ); 
					    document.forms['lossForm'].elements['loss.whoWorkName'].value=""
						document.forms['lossForm'].elements['loss.whoWorkCDE'].value="";
			   			document.forms['lossForm'].elements['loss.whoWorkCDE'].select();
	           		}			
           }else{
                alert("Vendor code not valid");    
			    document.forms['lossForm'].elements['loss.whoWorkName'].value=""
				document.forms['lossForm'].elements['loss.whoWorkCDE'].value="";
			   	document.forms['lossForm'].elements['loss.whoWorkCDE'].select();
		   }
           
      }
}

function updatechequeNumberCustmer(){
    var chequeNumberCustmer=document.forms['lossForm'].elements['loss.chequeNumberCustmer'].value;
	var claimNumberCopy=document.forms['lossForm'].elements['claimNumberCopy'].value;
	var shipNumber = document.forms['lossForm'].elements['shipnum'].value;
	if(chequeNumberCustmer !=''){
	var agree = confirm("Do you want to Copy Shipper Cheque #? Press OK for YES or Cancel for NO.");
			if(agree)
			{
			var url="lossChequeNumberCustmer.html?ajax=1&decorator=simple&popup=true&chequeNumberCustmer=" + encodeURI(chequeNumberCustmer) +"&claimNumberCopy="+ encodeURI(claimNumberCopy) +"&shipnum="+ encodeURI(shipNumber);
		     http22.open("GET", url, true);
             http22.onreadystatechange = handleHttpResponse111;
             http22.send(null);
			}
			}
			else
			{
			alert("Please enter Cheque#.");
			}
			}
		
function handleHttpResponse111(){
      if (http22.readyState == 4){
                 var result= http22.responseText
                 result=result.trim();
                 if(result>0){
                 alert('Cheque # has been copied successfully.')
                 }
                 else
                 {
                 alert('No record found to copy Cheque#.')
                 }
      }
}	

function updatechequeNumber3Party(){
    var chequeNumber3Party=document.forms['lossForm'].elements['loss.chequeNumber3Party'].value;
	var claimNumberCopy=document.forms['lossForm'].elements['claimNumberCopy'].value;
	var shipNumber = document.forms['lossForm'].elements['shipnum'].value;
	if(chequeNumber3Party!='')
	{
	var agree = confirm("Do you want to Copy 3rd Party Cheque #? Press OK for YES or Cancel for NO.");
			if(agree)
			{
			var url="lossChequeNumber3Party.html?ajax=1&decorator=simple&popup=true&chequeNumber3Party=" + encodeURI(chequeNumber3Party) +"&claimNumberCopy="+ encodeURI(claimNumberCopy) +"&shipnum="+ encodeURI(shipNumber);
		     http33.open("GET", url, true);
             http33.onreadystatechange = handleHttpResponse333;
             http33.send(null);
			}
			}
			else{
			alert("Please enter Cheque#.");
			}
			
		}
function handleHttpResponse333(){
      if (http33.readyState == 4){
                 var result= http33.responseText
                 result=result.trim();
                 if(result>0){
                 alert('Cheque # has been copied successfully.')
                 }
                 else
                 {
                 alert('No record found to copy Cheque#.')
                 }
      }
}	


var http22 = getHTTPObject22();
function getHTTPObject22()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 
 var http33 = getHTTPObject33();
function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
</script>

<script>
function applyTicketToAllLosses(){
	var requestedACLT = document.forms['lossForm'].elements['requestedACLT'].value;
	document.forms['lossForm'].elements['claimNum'].value = document.forms['lossForm'].elements['claim.claimNumber'].value;
	document.forms['lossForm'].elements['lossNum'].value = document.forms['lossForm'].elements['loss.idNumber1'].value;
	document.forms['lossForm'].elements['shipNum'].value = document.forms['lossForm'].elements['loss.shipNumber'].value;
	document.forms['lossForm'].elements['seqNum'].value = document.forms['lossForm'].elements['loss.sequenceNumber'].value;
	document.forms['lossForm'].action = 'applyTicketToAllLosses.html';
	document.forms['lossForm'].elements['btnType'].value = "submit";
	document.forms['lossForm'].submit();
	}

</script>
<script language="javascript" type="text/javascript">  
function workTicketsList(targetElement){
	//alert(targetElement.checked);
	var requestedWT = document.forms['lossForm'].elements['requestedACLT'].value;
	if(targetElement.checked){
		if(document.forms['lossForm'].elements['requestedACLT'].value == '' ){
			document.forms['lossForm'].elements['requestedACLT'].value = targetElement.value;
		}else{
			if(requestedWT.indexOf(targetElement.value)>=0){}
			else{
				requestedWT = requestedWT + ',' +targetElement.value;
			}
			document.forms['lossForm'].elements['requestedACLT'].value = requestedWT.replace(',,',",");
		}
	}else{
		if(requestedWT.indexOf(targetElement.value)>=0){
			requestedWT = requestedWT.replace(targetElement.value,"");
		}
		document.forms['lossForm'].elements['requestedACLT'].value = requestedWT.replace(',,',",");
	}
	
	if(document.forms['lossForm'].elements['requestedACLT'].value == ',' ){
			document.forms['lossForm'].elements['requestedACLT'].value = '';
	}
	if((document.forms['lossForm'].elements['requestedACLT'].value).indexOf(",") == 0){
			document.forms['lossForm'].elements['requestedACLT'].value = (document.forms['lossForm'].elements['requestedACLT'].value).substring(1);
	}
	if(document.forms['lossForm'].elements['requestedACLT'].value.lastIndexOf(",") == document.forms['lossForm'].elements['requestedACLT'].value.length -1){
			document.forms['lossForm'].elements['requestedACLT'].value = (document.forms['lossForm'].elements['requestedACLT'].value).substring(0,document.forms['lossForm'].elements['requestedACLT'].value.length-1)
	}
	
	if(document.forms['lossForm'].elements['requestedACLT'].value == ''){
		document.forms['lossForm'].elements['assignTicketsToAllLoss'].disabled = true;
	}else{
		document.forms['lossForm'].elements['assignTicketsToAllLoss'].disabled = false;
	}
	
}


function savingRecord(){
	document.forms['lossForm'].elements['claimNum'].value = document.forms['lossForm'].elements['claim.claimNumber'].value;
	document.forms['lossForm'].elements['lossNum'].value = document.forms['lossForm'].elements['loss.idNumber1'].value;
	document.forms['lossForm'].elements['shipNum'].value = document.forms['lossForm'].elements['loss.shipNumber'].value;
	document.forms['lossForm'].elements['seqNum'].value = document.forms['lossForm'].elements['loss.sequenceNumber'].value;
	

	document.forms['lossForm'].elements['btnType'].value = "save";
}
 </script> 




<script type="text/javascript">
function autoSaveFunc(clickType){
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
progressBarAutoSave('1');
	if ('${autoSavePrompt}' == 'No'){
	
	var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
      		var id1 = document.forms['lossForm'].elements['serviceOrder.id'].value;
      		var idc = document.forms['lossForm'].elements['claim.id'].value;
      		
           if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.serviceorder')
           {
             noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             noSaveAction = 'editBilling.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              noSaveAction = 'accountLineList.html?sid='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.newAccounting')
          {
             noSaveAction = 'pricingList.html?sid='+id1;
          }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.forwarding')
           {
        	  <c:if test="${forwardingTabVal!='Y'}">
	  			noSaveAction = 'containers.html?id='+id1;
	  		</c:if>
		  	<c:if test="${forwardingTabVal=='Y'}">
		  		noSaveAction = 'containersAjaxList.html?id='+id1;
		  </c:if>
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.domestic')
           {
             noSaveAction = 'editMiscellaneous.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.status')
           {
              <c:if test="${serviceOrder.job =='RLO'}">
              noSaveAction = 'editDspDetails.html?id='+id1; 
  			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}">
           noSaveAction =  'editTrackingStatus.html?id='+id1; 
  			</c:if>
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.ticket')
           {
             noSaveAction = 'customerWorkTickets.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.claims')
           {
             noSaveAction = 'claims.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
        	  var cidVal='${customerFile.id}';
             noSaveAction ='editCustomerFile.html?id='+cidVal;
           }
           
           if(document.forms['lossForm'].elements['gotoPageString'].value == 'gototab.claiminfo'){
               noSaveAction = 'editClaim.html?id='+idc;
               }
               
               if(document.forms['lossForm'].elements['gotoPageString'].value == 'gototab.claimitemsc'){
               noSaveAction = 'losss.html?id='+idc;
               }
             
          processAutoSave(document.forms['lossForm'], noSaveAction, noSaveAction);
	
	
	}
	else{
	
   if(!(clickType == 'save'))
   {
     var id1 =document.forms['lossForm'].elements['serviceOrder.id'].value;
     var idc = document.forms['lossForm'].elements['claim.id'].value;
     if (document.forms['lossForm'].elements['formStatus'].value == '1')
     {
       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='lossDetail.heading'/>");
       if(agree)
        {
           document.forms['lossForm'].action ='saveLoss!saveOnTabChange.html';
           document.forms['lossForm'].submit();
        }
       else
        {
         if(id1 != '')
          {
           if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.serviceorder')
           {
             location.href = 'editServiceOrderUpdate.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.billing')
           {
             location.href = 'editBilling.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.accounting')
           {
              location.href = 'accountLineList.html?sid='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.newAccounting')
          {
             location.href = 'pricingList.html?sid='+id1;
          }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.forwarding')
           {
             <c:if test="${forwardingTabVal!='Y'}">
             	location.href = 'containers.html?id='+id1;
	  		</c:if>
		  	<c:if test="${forwardingTabVal=='Y'}">
		  		location.href = 'containersAjaxList.html?id='+id1;
		  </c:if>
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.domestic')
           {
             location.href = 'editMiscellaneous.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.status')
           {
              <c:if test="${serviceOrder.job =='RLO'}">
              location.href = 'editDspDetails.html?id='+id1; 
  			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}">
           location.href =  'editTrackingStatus.html?id='+id1; 
  			</c:if>
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.ticket')
           {
             location.href = 'customerWorkTickets.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.claims')
           {
             location.href = 'claims.html?id='+id1;
           }
          if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.customerfile')
           {
        	  var cidVal='${customerFile.id}';
             location.href ='editCustomerFile.html?id='+cidVal;
           }
           
           if(document.forms['lossForm'].elements['gotoPageString'].value == 'gototab.claiminfo'){
               location.href = 'editClaim.html?id='+idc;
               }
               
               if(document.forms['lossForm'].elements['gotoPageString'].value == 'gototab.claimitemsc'){
               location.href = 'losss.html?id='+idc;
               }
         }
       }
    }
   else
   {
     if(id1 != '')
      {
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.serviceorder')
        {
          location.href = 'editServiceOrderUpdate.html?id='+id1;
        }
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.billing')
        {
          location.href = 'editBilling.html?id='+id1;
        }
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.accounting')
        {
          location.href = 'accountLineList.html?sid='+id1;
        }
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.newAccounting')
       {
         location.href = 'pricingList.html?sid='+id1;
       }
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.forwarding')
        {
          <c:if test="${forwardingTabVal!='Y'}">
       		location.href = 'containers.html?id='+id1;
		</c:if>
	  	<c:if test="${forwardingTabVal=='Y'}">
	  		location.href = 'containersAjaxList.html?id='+id1;
	  	</c:if>
        }
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.domestic')
        {
           location.href = 'editMiscellaneous.html?id='+id1;
        }
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.status')
        {
           <c:if test="${serviceOrder.job =='RLO'}">
           location.href = 'editDspDetails.html?id='+id1; 
			</c:if>
        <c:if test="${serviceOrder.job !='RLO'}">
        location.href =  'editTrackingStatus.html?id='+id1; 
			</c:if>
        }
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.ticket')
        {
           location.href = 'customerWorkTickets.html?id='+id1;
        }
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.claims')
        {
           location.href = 'claims.html?id='+id1;
        }
       if(document.forms['lossForm'].elements['gotoPageString'].value =='gototab.customerfile')
        {
    	   var cidVal='${customerFile.id}';
          location.href ='editCustomerFile.html?id='+cidVal;
        }
        
           if(document.forms['lossForm'].elements['gotoPageString'].value == 'gototab.claiminfo'){
               location.href = 'editClaim.html?id='+idc;
               }
               
               if(document.forms['lossForm'].elements['gotoPageString'].value == 'gototab.claimitemsc'){
               location.href = 'losss.html?id='+idc;
               }
      }
   }
  }
  }
 }

function changeStatus()
{
   document.forms['lossForm'].elements['formStatus'].value = '1';
}


function assignCLTByAction() {
	document.forms['lossForm'].elements['claimNum'].value = document.forms['lossForm'].elements['claim.claimNumber'].value;
	document.forms['lossForm'].elements['lossNum'].value = document.forms['lossForm'].elements['loss.idNumber1'].value;
	document.forms['lossForm'].elements['shipNum'].value = document.forms['lossForm'].elements['loss.shipNumber'].value;
	document.forms['lossForm'].elements['seqNum'].value = document.forms['lossForm'].elements['loss.sequenceNumber'].value;
	var lossAct= document.forms['lossForm'].elements['loss.lossAction'].value;
	if(lossAct == 'C' || lossAct == 'D'){
	  alert("Can not assign ticket as action is Cancel or Denied");
	  return false;
	  } else {
	  javascript:openWindow('claimWorkTickets.html?id=${serviceOrder.id}&claimNum=${claim.claimNumber}&lossNum=${loss.idNumber1}&sequenceNumber=${serviceOrder.sequenceNumber}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=claimLossTicket.ticket');
	  }
}

function pertaininglossList(){
	var itmClmsItem =document.forms['lossForm'].elements['loss.itmClmsItemSeqNbr'].value;
	 javascript:openWindow('pertaininglossList.html?itmClmsItem='+encodeURI(itmClmsItem)+'&claimNum=${claim.claimNumber}&decorator=popup&popup=true');	
}



function removeCLTByAction() {
	document.forms['lossForm'].elements['claimNum'].value = document.forms['lossForm'].elements['claim.claimNumber'].value;
	document.forms['lossForm'].elements['lossNum'].value = document.forms['lossForm'].elements['loss.idNumber1'].value;
	document.forms['lossForm'].elements['shipNum'].value = document.forms['lossForm'].elements['loss.shipNumber'].value;
	document.forms['lossForm'].elements['seqNum'].value = document.forms['lossForm'].elements['loss.sequenceNumber'].value;
	var lossNum = document.forms['lossForm'].elements['loss.idNumber1'].value;
	var lossAct= document.forms['lossForm'].elements['loss.lossAction'].value;
	
	if (lossAct == 'C' || lossAct == 'D'){
    var url="removeCLTByAction.html?ajax=1&decorator=simple&popup=true&lossNum="+encodeURI(lossNum);
    http2.open("GET", url, true);
   ////////// http2.onreadystatechange = function(){handleHttpResponse4444(lossAct);};
   
   http2.onreadystatechange = handleHttpResponse4444;
    http2.send(null);
  }
}
	
function handleHttpResponse4444(lossAct)  {
		    if (http2.readyState == 4) {
		       var results = http2.responseText
                results = results.trim();
                var res = results.split("@");
                ///document.forms['lossForm'].action = save;
               //// document.forms['lossForm'].elements['btnType'].value = "submit";
				///document.forms['lossForm'].submit();
               }	
        }

function chkSelect()
	{
		if (checkFloat('lossForm','loss.requestedAmount','Invalid data in Amount Requested') == false)
           {
              document.forms['lossForm'].elements['loss.requestedAmount'].focus();
              return false
           }
           if (checkFloat('lossForm','loss.paidCompensationCustomer','Invalid data in Amount Paid Shipper') == false)
           {
              document.forms['lossForm'].elements['loss.paidCompensationCustomer'].focus();
              return false
           }
           if (checkFloat('lossForm','loss.paidCompensation3Party','Invalid data in Amount Paid 3rd Party') == false)
           {
              document.forms['lossForm'].elements['loss.paidCompensation3Party'].focus();
              return false
           }
           if (checkFloat('lossForm','loss.chargeBackAmount','Invalid data in Charge Back Amount') == false)
           {
              document.forms['lossForm'].elements['loss.chargeBackAmount'].focus();
              return false
           }
           return checkLength();
	}
function notExists(){
	alert("The Claim info has not been saved yet");
}


function goPrevChild() {
	progressBarAutoSave('1');
	var lossId =document.forms['lossForm'].elements['loss.id'].value;
	var claimNum =document.forms['lossForm'].elements['claim.claimNumber'].value;
	var url="editPrevLoss.html?ajax=1&decorator=simple&popup=true&id="+encodeURI(lossId)+"&claimNum="+encodeURI(claimNum);
	http2.open("GET", url, true); 
    http2.onreadystatechange = handleHttpResponseOtherLossChild; 
    http2.send(null); 
}
   
function goNextChild() {
	progressBarAutoSave('1');
	var lossId =document.forms['lossForm'].elements['loss.id'].value;
	var claimNum =document.forms['lossForm'].elements['claim.claimNumber'].value;
	var url="editNextLoss.html?ajax=1&decorator=simple&popup=true&id="+encodeURI(lossId)+"&claimNum="+encodeURI(claimNum);
	http2.open("GET", url, true); 
    http2.onreadystatechange = handleHttpResponseOtherLossChild; 
    http2.send(null); 
}
   
function handleHttpResponseOtherLossChild(){
    if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       location.href = 'editLoss.html?id='+results;
     }
}  
   
function findCustomerOtherSOChild(position) {
 	var lossId =document.forms['lossForm'].elements['loss.id'].value;
 	var claimNum =document.forms['lossForm'].elements['claim.claimNumber'].value;
 	var url="lossOther.html?ajax=1&decorator=simple&popup=true&id="+encodeURI(lossId)+"&claimNum="+encodeURI(claimNum);
 	ajax_showTooltip(url,position);	
}   

function goToUrlChild(id){
	location.href = "editLoss.html?id="+id;
}
function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}

function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
	lastName=lastName.replace("~","'");
	document.getElementById(partnerNameId).value=lastName;
	document.getElementById(paertnerCodeId).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";	
	if(paertnerCodeId=='lossWhoWorkCDEId'){
		checkVendorName();
	}
}

function checkLength(){
	var txt = document.forms['lossForm'].elements['loss.lossComment'].value;
	if(txt.length >498){
		alert('You can not enter more than 500 characters in Damage');
		document.forms['lossForm'].elements['loss.lossComment'].value = txt.substring(0,498);
		return false;
	}
}

function checkNotesLength(){
	var txt = document.forms['lossForm'].elements['loss.notesLoss'].value;
	if(txt.length >498){
		alert('You can not enter more than 500 characters in Additional Notes');
		document.forms['lossForm'].elements['loss.notesLoss'].value = txt.substring(0,498);
		return false;
	}
}


</script> 		


</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="lossForm" action="saveLoss" onsubmit="return submit_form()" method="post" validate="true">   
<s:hidden name="loss.id" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="loss.corpID" />
<s:hidden name="loss.shipNumber" />
<s:hidden name="loss.idNumber" />
<s:hidden name="loss.sequenceNumber" /> 
<s:hidden name="loss.integrationId" />
<s:hidden name="soId" value="%{serviceOrder.id}"/> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 
<c:set var="shipnum" value="${serviceOrder.shipNumber}"/>
<s:hidden name="shipnum" value="${shipnum}"/>
<c:set var="claimNumberCopy" value="${claim.claimNumber}"/>
<s:hidden name="claimNumberCopy" value="${claimNumberCopy}"/>
<s:hidden name="requestedCLT" />
<s:hidden name="requestedACLT" value="<%=request.getParameter("claimLossTicketList.ticket") %>" />
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="claimNum" />
<s:hidden name="lossNum" />
<s:hidden name="shipNum" />
<s:hidden name="seqNum" />
<s:hidden name="serviceOrder.id"/>
<s:hidden name="claim.id"/>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="idOfWhom" value="${claim.id}" scope="session"/>
<c:set var="noteID" value="${claim.claimNumber}" scope="session"/>
<c:set var="noteFor" value="Claim" scope="session"/>
<c:if test="${empty claim.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty claim.id}">
	<c:if test="${userType!='AGENT'}">
	<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
	<c:if test="${userType=='AGENT'}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
</c:if>

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
   <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
   <c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
   <c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.newAccounting' }">
   <c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.forwarding' }">
   <c:if test="${forwardingTabVal!='Y'}">
   		<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
   </c:if>
   <c:if test="${forwardingTabVal=='Y'}">
   		<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
   <c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
  			<c:if test="${serviceOrder.job =='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}"> 
				<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
			</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.ticket' }">
   <c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
   <c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.reports' }">
   <c:redirect url="/subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode =${billing.billToCode}&reportModule=serviceOrder&reportSubModule=Claims"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
   <c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claiminfo' }">
   <c:redirect url="/editClaim.html?id=${claim.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claimitemsc' }">
   <c:redirect url="/losss.html?id=${claim.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>

<s:hidden id="countLossNotes" name="countLossNotes" value="<%=request.getParameter("countLossNotes") %>"/>
<s:hidden id="countLossTicketNotes" name="countLossTicketNotes" value="<%=request.getParameter("countLossTicketNotes") %>"/>

<c:set var="countLossNotes" value="<%=request.getParameter("countLossNotes") %>" />
<c:set var="countLossTicketNotes" value="<%=request.getParameter("countLossTicketNotes") %>" />


<div id="Layer3" style="width:100%;">
<div id="newmnav">
     <ul>
     	<sec-auth:authComponent componentId="module.tab.claims.serviceorderTab">
             <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.serviceorder');return autoSaveFunc('none');"><span>S/O Details</span></a></li>
        </sec-auth:authComponent>
          <sec-auth:authComponent componentId="module.tab.claims.billingTab"> 
            <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
             	<li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.billing');return autoSaveFunc('none');"><span>Billing</span></a></li>
             </sec-auth:authComponent>
          </sec-auth:authComponent>
          <sec-auth:authComponent componentId="module.tab.claims.accountingTab">
             <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			   </c:when> --%>
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			   <c:otherwise> 
		         <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.accounting');return autoSaveFunc('none');"><span>Accounting</span></a></li>
		       </c:otherwise>
		     </c:choose> 
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		  <c:choose> 
			   <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			   <c:otherwise> 
		         <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.newAccounting');return autoSaveFunc('none');"><span>Accounting</span></a></li>
		       </c:otherwise>
		     </c:choose> 
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
           	  </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
     	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
		  	<sec-auth:authComponent componentId="module.tab.claims.forwardingTab">
		  	<c:if test="${userType!='ACCOUNT' && serviceOrder.job !='RLO'}">
             <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.forwarding');return autoSaveFunc('none');"><span>Forwarding</span></a></li>
            </c:if>
            <c:if test="${userType=='ACCOUNT' && serviceOrder.job !='RLO'}">
              <li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
             </c:if>
            </sec-auth:authComponent>
	        <sec-auth:authComponent componentId="module.tab.claims.summaryTab">
			  	<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
			</sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.tab.claims.domesticTab">  
             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
             <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.domestic');return autoSaveFunc('none');"><span>Domestic</span></a></li>
             </c:if>
            </sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.domestic');return autoSaveFunc('none');"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.tab.claims.statusTab">
             <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.status');return autoSaveFunc('none');"><span>Status</span></a></li>
            </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.claims.ticketTab">  
             <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.ticket');return autoSaveFunc('none');"><span>Ticket</span></a></li>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Claims<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
             </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
             <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.customerfile');return autoSaveFunc('none');"><span>Customer File</span></a></li>
            </sec-auth:authComponent>
            <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
           	</sec-auth:authComponent>
           	<configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 <li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 </configByCorp:fieldVisibility> 
   </ul>
       </div>
     <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>  
       <div class="spn">&nbsp;</div>
       

</div>

<s:hidden name="minLoss" />
<s:hidden name="maxLoss" />
<s:hidden name="countLoss" />

<div id="Layer1"  onkeydown="changeStatus();" style="width:100%;"> 
<table style="width:100%;"><tr><td>
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%> 
</td></tr></table>
<div id="Layer5" style="width:100%;">
<div id="newmnav" style="float:left;">
			<ul>
             <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.claiminfo');return autoSaveFunc('none');"><span>Claim Detail</span></a></li>
              <li><a onmouseover="return chkSelect();" onclick="setReturnString('gototab.claimitemsc');return autoSaveFunc('none');"><span>Claim Items</span></a></li>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Claim Item Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
               <!-- <li><a onclick="openWindow('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&reportModule=serviceOrder&reportSubModule=Loss&decorator=popup&popup=true',750,400)"><span>Forms</span></a></li> -->
               <sec-auth:authComponent componentId="module.tab.claims.reportTab">
               <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Loss&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
            
             <li><a onclick="window.open('auditList.html?id=${loss.id}&tableName=loss&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
             </sec-auth:authComponent>
         </ul></div>
    	<table cellpadding="0" cellspacing="0" height="22px" style="margin:0px; padding:0px;margin-bottom:-10px;">
	    	<tr>
				<c:if test="${not empty loss.id}">
				 	<td width="20px" align="right">
					 	<c:if test="${loss.id> minLoss}" >
				  			<a><img align="middle" onclick="goPrevChild();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
				  		</c:if>
				  		<c:if test="${loss.id == minLoss}" >
				  			<a><img align="middle" src="images/navdisable_03.png"/></a>
				  		</c:if>
			  		</td>
			  		<td width="20px" align="left">
				  		<c:if test="${loss.id < maxLoss}" >
				  			<a><img align="middle" onclick="goNextChild();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
						</c:if>
						<c:if test="${loss.id == maxLoss}" >
				  			<a><img align="middle" src="images/navdisable_04.png"/></a>
				  		</c:if>
					</td>
					<td width="20px" align="left">
						<c:if test="${countLoss != 1}" >
							<a><img class="openpopup" onclick="findCustomerOtherSOChild(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="SO AccountLine List" title="Customer SO List" /></a> 
						</c:if>
						<c:if test="${countLoss == 1}" >
				  			<a><img align="middle" src="images/navdisable_05.png"/></a>
				  		</c:if>
					</td>
				</c:if>
			</tr>
		</table>
    <div class="spn">&nbsp;</div>
       
</div>
<!--
  <ul>
  	<li><a href="editClaim.html?id=${claim.id}"><span>Claim Detail</span></a></li>
   	<li><a href="losss.html?id=${claim.id}"><span>Claim Items</span></a></li>
    <li id="newmnav1" style="background:#FFF"><a class="current""><span>Claim Item Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
</ul></div><div class="spn" width="1000px">&nbsp;</div><br>
--><!--<div>
  <ul id="newmnav">
   <li><a href="editClaim.html?id=${claim.id}"><span>Claim Detail</span></a></li>
   <li><a href="losss.html?id=${claim.id}"><span>Claim Items</span></a></li>
   <li id="newmnav1" style="background:#FFF"><a class="current""><span>Claim Item Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  
-->


<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top"><span></span></div>
<div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;">
<tbody>
<tr>
<td>
<table cellspacing="0" cellpadding="3" border="0" style="width:100%;margin-bottom:5px;" >
<tr><td colspan="15">

<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Claim Item Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext" width="105px"><fmt:message key="claim.claimNumber"/></td>
<td align="left" width=""><s:textfield cssClass="input-textUpper" name="claim.claimNumber" cssStyle="width:175px;" maxlength="10" readonly="true"  tabindex="1" /></td>
<td colspan="4" align="left" width="">
<table border="0" class="detailTabLabel" cellspacing="0" cellpadding="0" width="">
<tr>
<td width="107"></td>
<td align="right" class="listwhitetext" width=""><fmt:message key="loss.idNumber1"/>&nbsp;&nbsp;</td>
<td align="left"><s:textfield cssClass="input-textUpper" name="loss.idNumber1" cssStyle="width:175px;" maxlength="10" readonly="true"  tabindex="1" /></td>
<configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td align="right" class="listwhitetext" width="128">Claim&nbsp;Item&nbsp;Sequence&nbsp;#&nbsp;</td>
<td align="left"><s:textfield cssClass="input-textUpper" name="loss.itmClmsItemSeqNbr" cssStyle="width:175px;" maxlength="10" readonly="true"  tabindex="1" /></td>
</configByCorp:fieldVisibility>
</tr>
</table>
</td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="loss.itemDescription"/></td>
<td align="left" ><s:textfield cssClass="input-text" name="loss.itemDescription" cssStyle="width:175px;" maxlength="500" required="true" tabindex="1" /></td>
<td colspan="4" align="left">
<table border="0" class="detailTabLabel" cellspacing="0" cellpadding="0">
<tr><td width="26"></td><configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td align="right" class="listwhitetext" width="">Alternate&nbsp;Item&nbsp;Description&nbsp;&nbsp;</td>
<td align="left"><s:textfield cssClass="input-text" name="loss.itmAltItemDesc" cssStyle="width:175px;" maxlength="15" required="true" tabindex="2" /></td></configByCorp:fieldVisibility>
<td width="72">
<td align="right" class="listwhitetext" width=""><fmt:message key="loss.inventory"/>&nbsp;&nbsp;</td>
<td align="left"><s:textfield cssClass="input-text" name="loss.inventory" cssStyle="width:175px;" maxlength="15" required="true" tabindex="2" /></td>
</tr>

</table>
</td>
</tr>
 

<tr>
<td colspan="4" align="left">
<table border="0" class="detailTabLabel" cellspacing="0">
<tr>
<td align="right" width="105px" class="listwhitetext" style="padding-right:5px;"><fmt:message key="loss.lossComment1"/> </td>
<td align="left"><s:textarea cssClass="textarea"  name="loss.lossComment"  rows="3" cols="98" onkeydown="return checkLength();" readonly="false" tabindex="3" /></td>

<c:set var="isCartonDamagedFlag" value="false"/>
													<c:if test="${loss.cartonDamaged}">
														<c:set var="isCartonDamagedFlag" value="true"/>
													</c:if>
	<td align="right" class="listwhitetext" style="padding-left:10px;">Carton Damaged</td>
    <td><s:checkbox key="loss.cartonDamaged" value="${isCartonDamagedFlag}" fieldValue="true"  cssStyle="margin:0px;" tabindex="3" onclick="changeStatus();" /></td>
</tr>
</table></td>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="loss.lossType1"/></td>
<td align="left"><s:select cssClass="list-menu"  name="loss.lossType" list="%{clm_loss}" cssStyle="width:180px;" headerKey="" headerValue="  " onchange="changeStatus();" tabindex="4" /></td>

<td align="right" class="listwhitetext">Date Purchased</td>
<c:if test="${not empty loss.datePurchased}">
<s:text id="claimDatePurchasedFormattedValue" name="${FormDateValue}"><s:param name="value" value="loss.datePurchased"/></s:text>
<td><s:textfield cssClass="input-text" id="datePurchased" name="loss.datePurchased" value="%{claimDatePurchasedFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="datePurchased_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty loss.datePurchased}">
<td><s:textfield cssClass="input-text" id="datePurchased" name="loss.datePurchased" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="datePurchased_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>

<tr>
<td align="right" class="listwhitetext">Original Cost</td>
<td align="left"><s:textfield cssClass="input-text" name="loss.originalCost" cssStyle="width:175px;text-align:right" maxlength="16" required="true" onchange="onlyFloat(this);"  tabindex="5" /></td>
<td align="right" class="listwhitetext"><fmt:message key="loss.originalCostCurrency"/></td>
<td align="left"><s:select cssClass="list-menu"  name="loss.originalCostCurrency" list="%{currency}" cssStyle="width:180px;" headerKey="" headerValue=" " tabindex="5"/></td>

<tr>

<td align="right" class="listwhitetext"><fmt:message key="loss.requestedAmount"/></td>
<td align="left" width="190"><s:textfield cssClass="input-text" name="loss.requestedAmount" cssStyle="width:175px;text-align:right" maxlength="16" required="true"   tabindex="6" /></td>
<td align="right" class="listwhitetext" width="150"><fmt:message key="loss.requestedAmountCurrency"/></td>
<td align="left"><s:select cssClass="list-menu"  name="loss.requestedAmountCurrency" list="%{currency}" cssStyle="width:180px" headerKey="" headerValue=" " tabindex="6"/></td>

</tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="loss.warehouse"/></td>
<td align="left"><s:select cssClass="list-menu"  name="loss.warehouse" list="%{house}" cssStyle="width:180px" headerKey="" headerValue="  " onchange="changeStatus();" tabindex="7" /></td>

<td align="right" class="listwhitetext"><fmt:message key="loss.damageByAction1"/></td>
<td align="left"><s:select cssClass="list-menu"  name="loss.damageByAction" list="%{damaged}" cssStyle="width:180px" headerKey="" headerValue="  " onchange="changeStatus();" tabindex="8" /></td>
<sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
<c:if test="${empty loss.id}">
<td   align="right" ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
  
<c:if test="${not empty loss.id}">
<c:choose>
<c:when test="${countLossNotes == '0' || countLossNotes == '' || countLossNotes == null}">
<td align="right" ><img id="countLossNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=ClaimForm&imageId=countLossNotesImage&fieldId=countLossNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=ClaimForm&decorator=popup&popup=true',755,500);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" ><img id="countLossNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=ClaimForm&imageId=countLossNotesImage&fieldId=countLossNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=ClaimForm&decorator=popup&popup=true',755,500);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</sec-auth:authComponent>
</tr>
<tr>
<td align="right" class="listwhitetext">Self Blame</td>
<td align="left"><s:select cssClass="list-menu"  name="loss.selfBlame" list="%{selfBlameValue}" cssStyle="width:100px" headerKey="" headerValue="  " onchange="changeStatus();" tabindex="7" /></td>
</tr>
     <configByCorp:fieldVisibility componentId="component.tab.claim.claimIntegrationAppsLossPicture">
       <tr>
       <td></td>
       <td colspan="10"> <s:set name="lossPictures" value="lossPictures" scope="request"/>
		<c:forEach var="entry" items="${lossPictures}">		 
		<div style="width:550px;">
		<div style="float:left;padding:10px;border:1px solid #dfdfdf;background:#efefef;margin-right:5px;"> <img src='${entry.photoUri}' onclick="javascript:openWindow('${entry.photoUri}',755,500);" height="50"></div>
		</div>
		</c:forEach></td>
		</tr></configByCorp:fieldVisibility>
		
	<configByCorp:fieldVisibility componentId="component.claim.loss.lossImages"> 
		<tr>
          <td></td>
            <td align="left" colspan="3">
             <c:if test="${not empty dataFromLossPicture}">
             <display:table name="dataFromLossPicture" id="dataFromLossPicture" class="table"  requestURI="" style="width:56%;margin-bottom:0px;">
             <display:column  title="Document Name" >
              <a style="cursor:pointer;" onclick="javascript:openWindow('lossDocumentImageServletAction.html?id=${dataFromLossPicture.id}&sessionCorpID=${dataFromLossPicture.corpID}&decorator=popup&popup=true',900,600);">
              <c:out value="${dataFromLossPicture.fileName}" escapeXml="false"
               /></a>
             </display:column>             
             </display:table>
             </c:if>
             </td>
             </tr>
</configByCorp:fieldVisibility>
</tbody>
</table>
</td>
</tr>
<tr>
<td height="10" width="100%" align="left" colspan="5">			
<div  onClick="javascript:animatedcollapse.toggle('paymentStatus')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Payment Status
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
<div id="paymentStatus" class="switchgroup1">
<sec-auth:authComponent componentId="module.claimForm.section.claimStatus.edit">
	<%
	String sectionClaimStatus1="true";
	int permission1  = (Integer)request.getAttribute("module.claimForm.section.claimStatus.edit" + "Permission");
 	if (permission1 > 2 ){
 		sectionClaimStatus1 = "false";
 		System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionClaimStatus1);
 	}
 	System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionClaimStatus1);
  %>
<table class="detailTabLabel" border="0" width="100%">
<tbody> 		  
<tr><td align="left" height="5px"></td></tr>
<tr>
<td>

<table style="margin:0px;">
<tr>
<c:if test="${not empty loss.id}">
<td align="right" class="listwhitetext"><fmt:message key="loss.lossAction"/></td>
<td align="left" width=""><s:select cssClass="list-menu"  name="loss.lossAction" list="%{clm_actn}" disabled="<%=sectionClaimStatus1%>" cssStyle="width:180px" headerKey="" headerValue="  " onchange="removeCLTByAction(); changeStatus();" tabindex="9" /></td> 
</c:if>
<c:if test="${empty loss.id}">
<td align="right" class="listwhitetext"><fmt:message key="loss.lossAction"/></td>
<td align="left" width=""><s:select cssClass="list-menu"  name="loss.lossAction" list="%{clm_actn}" disabled="<%=sectionClaimStatus1%>" cssStyle="width:180px" headerKey="" headerValue="  " onchange="changeStatus();" tabindex="9" /></td> 
</c:if>
<configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td align="right" class="listwhitetext">Denial Reason</td>
<td align="left"><s:select cssClass="list-menu"  name="loss.itmDnlRsnCode" list="%{denialReason}" cssStyle="width:180px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="9" /></td> 
<td align="right" width="114" class="listwhitetext">Exception&nbsp;Code</td>
<td align="left" width=""><s:select cssClass="list-menu" name="loss.itrRespExcpCode" list="%{respExpCode}" cssStyle="width:180px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="9" /></td> 
</configByCorp:fieldVisibility>
</tr>

<tr>
<td align="right" class="listwhitetext" ><fmt:message key="loss.paidCompensationCustomer1"/></td>
<td align="left"><s:textfield cssClass="input-text" name="loss.paidCompensationCustomer" readonly="<%=sectionClaimStatus1%>" maxlength="16" required="true"  cssStyle="text-align:right;width:175px;" tabindex="10" /></td>
<td align="right" width="" class="listwhitetext"><fmt:message key="loss.paidCompensationCustomerCurrency"/></td>
<td align="left"><s:select cssClass="list-menu"  name="loss.paidCompensationCustomerCurrency" list="%{currency}" cssStyle="width:180px" headerKey="" headerValue=" " tabindex="10" /></td>
<configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td align="right" class="listwhitetext">Assessed&nbsp;Liability</td>
<td align="left" width=""><s:textfield cssClass="input-text" name="loss.itrAssessdLiabilityAmt"  cssStyle="width:175px;text-align:right;" maxlength="8" onchange="onlyFloat(this);" tabindex="9" /></td> 
</configByCorp:fieldVisibility>
</tr>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="loss.chequeNumberCustmer"/></td>
<td align="left"><s:textfield cssClass="input-text" name="loss.chequeNumberCustmer" readonly="<%=sectionClaimStatus1%>"  cssStyle="width:175px" maxlength="10" required="true" tabindex="11" /></td>
<td></td><td></td><configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td align="right" class="listwhitetext">Total&nbsp;Liability</td>
<td align="left" width=""><s:textfield cssClass="input-text" cssStyle="text-align:right;width:175px;" name="loss.itrTotalLiabilityAmt" size="14" maxlength="8" onchange="onlyFloat(this)" tabindex="9" /></td> 
</configByCorp:fieldVisibility>

</tr>

<tr>
<td align="right" class="listwhitetext" width=""><fmt:message key="loss.paidCompensation3Party1"/></td>
<td align="left" ><s:textfield cssClass="input-text" name="loss.paidCompensation3Party" readonly="<%=sectionClaimStatus1%>"  cssStyle="width:175px;text-align:right;" maxlength="16" required="true"  tabindex="12" /></td>
<td align="right" class="listwhitetext" width="165"><fmt:message key="loss.paidCompensation3PartyCurrency"/></td>
<td align="left"><s:select cssClass="list-menu"  name="loss.paidCompensation3PartyCurrency" list="%{currency}" cssStyle="width:180px" headerKey="" headerValue=" " tabindex="13"/></td>
<!--<configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td align="right" colspan="2" ><input type="button" class="cssbutton" onclick="pertaininglossList();"
	        value="Other Responsible Agent"   style="width:200px; height:25px" /></td>
</configByCorp:fieldVisibility>
--></tr>


<tr>
<td align="right" class="listwhitetext"><fmt:message key="loss.chequeNumber3Party"/></td>
<td align="left"><s:textfield cssClass="input-text" name="loss.chequeNumber3Party" readonly="<%=sectionClaimStatus1%>"  cssStyle="width:175px" maxlength="10" required="true" tabindex="13" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="loss.whoWorkCDE"/></td>
<td align="left" colspan="5"><s:textfield cssClass="input-textUpper" name="loss.whoWorkCDE" id="lossWhoWorkCDEId" readonly="<%=sectionClaimStatus1%>"  cssStyle="width:175px" maxlength="8" required="true" onchange = "checkVendorName();"  tabindex="14" />
<% if (sectionClaimStatus1.equalsIgnoreCase("false")){%>
<img class="openpopup" width="17" height="20" align="top" onclick="javascript:openWindow('venderPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=loss.whoWorkName&fld_code=loss.whoWorkCDE');" src="<c:url value='/images/open-popup.gif'/>" />
<%} %>
<s:textfield cssClass="input-text" name="loss.whoWorkName" id="lossWhoWorkNameId" cssStyle="width:324px;" maxlength="30"  tabindex="15" onkeyup="findPartnerDetails('lossWhoWorkNameId','lossWhoWorkCDEId','lossWhoWorkNameDivId',' and (isVendor=true or isAccount=true or isAgent=true or isPrivateParty=true  or isCarrier=true )','',event);"  required="false"  readonly="false"/>
<div id="lossWhoWorkNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
</td>
</tr>

<%-- <tr>
<td align="right" width="" class="listwhitetext">Agent Liable</td>
<td align="left"><s:select cssClass="list-menu"  name="loss.agentLiable" list="{'Origin','Destination','Both'}" cssStyle="width:180px" headerKey="" headerValue=" "/></td>
</tr> --%>

<tr>
<td align="right" class="listwhitetext"><fmt:message key="loss.chargeBackTo"/></td>
<td align="left"><s:textfield cssClass="input-text" name="loss.chargeBackTo" readonly="<%=sectionClaimStatus1%>" cssStyle="width:175px" maxlength="16" required="true" tabindex="16" /></td>
<td align="right" class="listwhitetext"><fmt:message key="loss.chargeBackAmount"/></td>
<td align="left"><s:textfield cssClass="input-text" name="loss.chargeBackAmount" readonly="<%=sectionClaimStatus1%>" cssStyle="width:175px;text-align:right;" maxlength="16" required="true"  tabindex="17" /></td>
<configByCorp:fieldVisibility componentId="component.claim.unigroupclaim.edit"> 
<td align="right" class="listwhitetext">Payment&nbsp;ID</td>
<td align="left"><s:textfield cssClass="input-text" name="loss.itrPmntId" cssStyle="width:175px;text-align:right;" maxlength="16" required="true"  tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>

<tr>
<td></td>
<td></td>
<td align="right" width="" class="listwhitetext"><fmt:message key="loss.chargeBackCurrency"/></td>
<td align="left"><s:select cssClass="list-menu"  name="loss.chargeBackCurrency" list="%{currency}" cssStyle="width:180px" headerKey="" headerValue=" " tabindex="18"/></td>

</tr>
<%-- <tr>
<td align="right" class="listwhitetext"><b>Destin</b> <fmt:message key="loss.chargeBackTo"/></td>
<td align="left"><s:textfield cssClass="input-text" name="loss.chargeBack" readonly="<%=sectionClaimStatus1%>" cssStyle="width:175px" maxlength="16" required="true" tabindex="19" /></td>
<td align="right" class="listwhitetext"><fmt:message key="loss.chargeBackAmount"/></td>
<td align="left"><s:textfield cssClass="input-text" name="loss.chargeBackAmountDest" readonly="<%=sectionClaimStatus1%>" cssStyle="width:175px;text-align:right;" maxlength="16" required="true"  tabindex="20" /></td>
</tr>
<tr>
<td></td>
<td></td>
<td align="right" width="" class="listwhitetext"><fmt:message key="loss.chargeBackCurrency"/></td>
<td align="left"><s:select cssClass="list-menu"  name="loss.chargeBackCurrencyDest" list="%{currency}" cssStyle="width:180px" headerKey="" headerValue=" " tabindex="21"/></td>
</tr>
 --%>
<tr>
<% if (sectionClaimStatus1.equalsIgnoreCase("false")){%>
<td align="right" class="listwhitetext"><fmt:message key="loss.notesLoss"/></td>
<td align="left" colspan="4" ><s:textarea cssClass="textarea"  name="loss.notesLoss"  rows="3" cols="98" onkeydown="return checkNotesLength();" readonly="<%=sectionClaimStatus1%>" tabindex="22" /></td>

<sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
<c:if test="${empty loss.id}">
<td  style="position:absolute;right:45px;" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
  
<c:if test="${not empty loss.id}">
<c:choose>
<c:when test="${countLossNotes == '0' || countLossNotes == '' || countLossNotes == null}">
<td style="position:absolute;right:45px;" align="right"><img id="countLossNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=Loss&imageId=countLossNotesImage&fieldId=countLossNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=Loss&decorator=popup&popup=true',755,500);" ></a></td>
</c:when>
<c:otherwise>
<td style="position:absolute;right:45px;" align="right"><img id="countLossNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=Loss&imageId=countLossNotesImage&fieldId=countLossNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=Loss&decorator=popup&popup=true',755,500);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</sec-auth:authComponent>
<%} %>
</tr>
</table>

</td>
</tr>

<tr><td class="subcontenttabChild" colspan="10"><font color="black"><b>&nbsp;Ticket</b></font></td></tr>
<tr>
	<td colspan="15" >
		
		<table class="detailTabLabel" border="0"  width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>	

<tr>
	<td width="12%" align="left" style="padding-top:8px;">
	<% if (sectionClaimStatus1.equalsIgnoreCase("false")){%>
	 		<input type="button" class="cssbutton" onclick="assignCLTByAction();"
	        value="<fmt:message key="button.pullTicketsFromList"/>"   style="width:150px; height:25px" />
	<%} %>
	</td>
	<td align="left" colspan="3" style="margin:0px;">
		<s:set name="claimLossTickets" value="claimLossTickets" scope="request"/>
		<display:table name="claimLossTickets" class="table" requestURI="" id="claimLossTicketList"  defaultsort="2" pagesize="10" style="">
			<display:column title=" "><input type="checkbox" id="checkboxId" name="DD22" value="${claimLossTicketList.ticket}" onclick="workTicketsList(this);"/></display:column>
			<%--<display:column titleKey="claimLossTicket.ticket" style="width:300px"><c:out value="${claimLossTicket.ticket}" /><a> onclick="javascript:openWindow('editClaimLossTicket.html?ticket=${claimLossTicket.ticket}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=claimLossTicket.ticket');"> </a></display:column>--%>
			<c:if test="${not empty loss.id}">
				<display:column titleKey="claimLossTicket.ticket" style="width:200px">
				<% if (sectionClaimStatus1.equalsIgnoreCase("false")){%>
				<a onclick="javascript:openWindow('editClaimLossTicket.html?id=${claimLossTicketList.id}&lossNumber=${loss.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=claimLossTicket.ticket',300,200);" src="<c:url value='/images/open-popup.gif'/>" > 
				<%} %>
				<c:out value="${claimLossTicketList.ticket}" /></a></display:column>
			</c:if>
			<c:if test="${empty loss.id}">
				<display:column titleKey="claimLossTicket.ticket" style="width:200px"><c:out value="${claimLossTicketList.ticket}"/></display:column>
			</c:if>
			<%--   <display:column property="ticket" sortable="true" titleKey="claimLossTicket.ticket" href="editClaimLossTicket.html" paramId="id" paramProperty="id" style="width:300px"/>--%>
			<display:column property="chargeBack" sortable="true" titleKey="claimLossTicket.chargeBack" style="width:10px"/>
			<display:column property="percent" sortable="true" titleKey="claimLossTicket.percent" style="width:25px"/>
			<display:column titleKey="claimLossTicket.claimNumber" style="width:35px"><c:out value="${claim.claimNumber}" /></display:column>
			<display:column titleKey="claimLossTicket.lossNumber" style="width:35px"><c:out value="${loss.idNumber1}" /></display:column>
			<% if (sectionClaimStatus1.equalsIgnoreCase("false")){%>
			<c:if test="${not empty loss.id}">
				<display:column title="Remove"><a><img align="middle" onclick="return confirmSubmit(${claimLossTicketList.id});" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/></a></display:column>
		   	</c:if>
		   	<c:if test="${empty loss.id}">
		   		<display:column title="Remove"><a><img align="middle" onclick="return doNotSubmit();" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/></a></display:column>
		   	</c:if>
		   	<%} %>
		</display:table>
	</td>
	<c:if test="${not empty loss.id}">
		<td width="100px" colspan="3" align="left" style="padding-top:8px;">
		<% if (sectionClaimStatus1.equalsIgnoreCase("false")){%>
		<input type="button" class="cssbutton" name="assignTicketsToAllLoss" style="width:170px; height:25px" value="Apply Tickets to All Losses" onclick="applyTicketToAllLosses()">
		<%} %>
		</td>
	</c:if>
	<c:if test="${empty loss.id}">
		<td width="100px" colspan="3" align="left" style="padding-top:8px;">
		<% if (sectionClaimStatus1.equalsIgnoreCase("false")){%>
		<input type="button"  class="cssbutton" name="assignTicketsToAllLoss" style="width:170px; height:25px" value="Apply Tickets to All Losses" onclick="alert('Cannot assigened this Claim Loss Ticket as this Loss is not created Yet');">
		<%} %>
		</td>
	</c:if>	
</tr>
<tr><td colspan="10" >
<table class="detailTabLabel" border="0" width="100%">
	<tbody>
		<tr>
			<s:hidden name="claimLossTicket.sequenceNumber" value="%{loss.sequenceNumber}" />  
			<s:hidden name="claimLossTicket.shipNumber" value="%{loss.shipNumber}"/>
			<s:hidden name="claimLossTicket.claimNumber" value="%{claim.claimNumber}"/>
			<s:hidden name="claimLossTicket.lossNumber" value="%{loss.idNumber1}"/>
			<td></td>
			<td></td>
			<sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
			<c:if test="${empty loss.id}">
				<td  width="750" align="right" colspan="8"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
			</c:if>
		
			
			<c:if test="${not empty loss.id}">
				<c:choose>
					<c:when test="${countLossTicketNotes == '0' || countLossTicketNotes == '' || countLossTicketNotes == null}">
						<td width="750" align="right" colspan="8"><img id="countLossTicketNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=LossTicket&imageId=countLossTicketNotesImage&fieldId=countLossTicketNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=LossTicket&decorator=popup&popup=true',755,500);" ></a></td>
					</c:when>
					<c:otherwise>
						<td width="750" align="right" colspan="8"><img id="countLossTicketNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=LossTicket&imageId=countLossTicketNotesImage&fieldId=countLossTicketNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${claim.id }&notesId=${serviceOrder.shipNumber }&noteFor=Claim&subType=LossTicket&decorator=popup&popup=true',755,500);" ></a></td>
					</c:otherwise>
				</c:choose> 
			</c:if>	
			</sec-auth:authComponent>
		</tr> 
	
	<tr><td align="left" height="5px"></td></tr>
	</tbody>
</table>
</td>
</tr>
</tbody>

</table>
</td>
</tr>
</tbody>
</table>
</sec-auth:authComponent>
</div>
</td>
</tr></tbody>

</table>
</div>
<div class="bottom-header" style="margin-top:40px;"><span></span></div>
</div>
</div>
<table border="0">
										
						
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='loss.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:130px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${loss.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="loss.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${loss.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='loss.createdBy' /></b></td>
							<c:if test="${not empty loss.id}">
								<s:hidden name="loss.createdBy"/>
								<td style="width:100px"><s:label name="createdBy" value="%{loss.createdBy}"/></td>
							</c:if>
							<c:if test="${empty loss.id}">
								<s:hidden name="loss.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='loss.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${loss.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="loss.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:130px"><fmt:formatDate value="${loss.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='loss.updatedBy' /></b></td>
							<c:if test="${not empty loss.id}">
								<s:hidden name="loss.updatedBy"/>
								<td style="width:100px"><s:label name="updatedBy" value="%{loss.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty loss.id}">
								<s:hidden name="loss.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>

			</tbody>
		</table>
	</td>
</tr>
<c:out value="${button1}" escapeXml="false" />  

<c:set var="button1">
       <s:submit cssClass="cssbutton" id="saveButton" method="save" key="button.save" cssStyle="width:60px; height:25px" onmouseover="return chkSelect();" onclick="return chkSelect();savingRecord()"/>
       <c:if test="${not empty loss.id}">  
     		<input type="button" id="addButton" class="cssbutton" onclick="location.href='<c:url value="/editNewLoss.html?id=${claim.id}"/>'" value="<fmt:message key="button.add"/>" style="width:60px; height:25px" />  
	 	</c:if>
         <%-- <a href="editNewLoss.html?id=${claim.id}"><s:submit cssClass="button" method="add" key="button.add" cssStyle="width:87px; height:30px"/>   --%>
        <input type="button" id="resetButton" class="cssbutton" onclick="this.form.reset()" value="Reset" style="width:60px; height:25px" />   
</c:set>
<c:out value="${button1}" escapeXml="false" /> 
<sec-auth:authComponent componentId="module.tab.claims.customerFileTab">
		
<c:if test="${not empty loss.id}">  
     		<input type="button" id="copyShipperButton" class="cssbutton" onclick="updatechequeNumberCustmer();" value="Copy Shipper Cheque #" />  
</c:if>    
<c:if test="${not empty loss.id}">  
     		<input type="button" id="copy3rdPartyButton" class="cssbutton" onclick="updatechequeNumber3Party();" value="Copy 3rd Party Cheque #" />  
	 	</c:if>
</sec-auth:authComponent>
<s:hidden name="btnType"/>
</tbody>
</table>
</div>
</s:form>

<c:if test="${btnType == 'save' || btnType == 'submit'}" >
	<c:redirect url="/editLoss.html?id=${loss.id}"/>
</c:if>
<script type="text/javascript">   
	//document.forms['lossForm'].elements['assignTicketsToAllLoss'].disabled = true;  
</script>  		

<script type="text/javascript">
	RANGE_CAL_1 = new Calendar({
        inputField: "datePurchased",
        dateFormat: "%d-%b-%y",
        trigger: "datePurchased_trigger",
        bottomBar: true,
        animation:true,
        onSelect: function() {                             
            this.hide();
   		 }
	});
</script>
<script type="text/javascript">
window.onload = function(){	
	<sec-auth:authComponent componentId="module.script.form.claimScript">
		var elementsLength= document.getElementsByTagName('input');
		for(i = 0; i < elementsLength.length; i++){
			if(elementsLength[i].type=='button'){
				elementsLength[i].style.display = 'none';
			}
		}
	</sec-auth:authComponent>	
}

try{
	if(document.forms['lossForm'].elements['requestedACLT'].value == ''){
		document.forms['lossForm'].elements['assignTicketsToAllLoss'].disabled = true;
	}else{
		document.forms['lossForm'].elements['assignTicketsToAllLoss'].disabled = false;
	}
	}catch(e){}
</script>

<script type="text/javascript">
<configByCorp:fieldVisibility componentId="component.field.loss.selfBlameShowVOER">
if(document.forms['lossForm'].elements['loss.selfBlame'].value==null || document.forms['lossForm'].elements['loss.selfBlame'].value==''){	
		document.forms['lossForm'].elements['loss.selfBlame'].value='No';
}
</configByCorp:fieldVisibility>
</script>
