package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
@Entity
@Table(name="taskdetails")
public class TaskDetail extends BaseObject {

	private Long id;
	private String corpId;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String details;
	private String status;
	private Date date;
	private String time;
	private String comment;
	private String transfer;
	private Long ticket;
	private Long workTicketId;
	private String workorder;
	private String shipNumber;
	private String category;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId",corpId)
				.append("createdBy",createdBy)
				.append("createdOn",createdOn)
				.append("updatedBy",updatedBy)
				.append("updatedOn",updatedOn)
				.append("details",details)
				.append("status",status)
				.append("date",date)
				.append("time",time)
				.append("comment",comment)
				.append("transfer",transfer)
				.append("ticket",ticket)
				.append("workTicketId",workTicketId).append(workorder).append(shipNumber).append(category)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if(!(other instanceof TaskDetail))
		return false;
		TaskDetail castOther = (TaskDetail) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(details, castOther.details)
				.append(status, castOther.status)
				.append(date, castOther.date)
				.append(time, castOther.time)
				.append(comment, castOther.comment)
				.append(transfer, transfer)
				.append(ticket, castOther.ticket)
				.append(workTicketId, castOther.workTicketId)
				.append(workorder,castOther.workorder).append(shipNumber, castOther.shipNumber).append(category,castOther.category)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id)
				.append(corpId)
				.append(createdBy)
				.append(createdOn)
				.append(updatedBy)
				.append(updatedOn)
				.append(details)
				.append(status)
				.append(date)
				.append(time)
				.append(comment)
				.append(transfer)
				.append(ticket)
				.append(workTicketId)
				.append(workorder).append(shipNumber).append(category)
				.hashCode();
		
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getTransfer() {
		return transfer;
	}

	public void setTransfer(String transfer) {
		this.transfer = transfer;
	}

	public Long getTicket() {
		return ticket;
	}

	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}

	public Long getWorkTicketId() {
		return workTicketId;
	}

	public void setWorkTicketId(Long workTicketId) {
		this.workTicketId = workTicketId;
	}
	
	@Column
	public String getWorkorder() {
		return workorder;
	}

	public void setWorkorder(String workorder) {
		this.workorder = workorder;
	}

	@Column
	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	
	@Column
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
}