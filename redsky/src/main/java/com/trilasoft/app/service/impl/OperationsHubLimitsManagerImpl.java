package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.OperationsHubLimitsDao;
import com.trilasoft.app.dao.hibernate.OperationsHubLimitsDaoHibernate.HubLimitDTO;
import com.trilasoft.app.model.OperationsHubLimits;
import com.trilasoft.app.service.OperationsHubLimitsManager;

public class OperationsHubLimitsManagerImpl extends GenericManagerImpl<OperationsHubLimits, Long> implements OperationsHubLimitsManager {
	OperationsHubLimitsDao operationsHubLimitsDao;

	public OperationsHubLimitsManagerImpl(OperationsHubLimitsDao operationsHubLimitsDao) {
		super(operationsHubLimitsDao);
		this.operationsHubLimitsDao = operationsHubLimitsDao;
	}

	public List isExisted(String hub, String sessionCorpID) {
		return operationsHubLimitsDao.isExisted(hub, sessionCorpID);
	}

	public List<OperationsHubLimits> getHub(String hubID) {
		return operationsHubLimitsDao.getHub(hubID);
	}
	
	public List getAllRecord(String sessionCorpID){
		return operationsHubLimitsDao.getAllRecord(sessionCorpID);
	}	
	
	public List findListByHub(String sessionCorpID){
		return operationsHubLimitsDao.findListByHub(sessionCorpID);
	}	
	
	
	public void updateDailyOpHubLimitParent(Integer tempHubLimit,Integer tempHubLimitOld,Integer tempHubLimitNew, String sessionCorpID,String hubID){
		operationsHubLimitsDao.updateDailyOpHubLimitParent(tempHubLimit,tempHubLimitOld,tempHubLimitNew,sessionCorpID,hubID);
	}
	
	public void updateDailyMaxEstWtParent(BigDecimal tempdailyMaxEstWt,BigDecimal dailyMaxEstWtOld,BigDecimal dailyMaxEstWtNew, String sessionCorpID,String hubID){
		operationsHubLimitsDao.updateDailyMaxEstWtParent(tempdailyMaxEstWt, dailyMaxEstWtOld,dailyMaxEstWtNew,sessionCorpID,hubID);
	}
	
	public void updateDailyMaxEstVolParent(BigDecimal tempdailyMaxEstVol,BigDecimal dailyMaxEstVolOld,BigDecimal dailyMaxEstVolNew, String sessionCorpID,String hubID){
		operationsHubLimitsDao.updateDailyMaxEstVolParent(tempdailyMaxEstVol, dailyMaxEstVolOld,dailyMaxEstVolNew,sessionCorpID,hubID);
	}
	public List findUnassignedParentValue(String sessionCorpID,String wareHouse){
		return operationsHubLimitsDao.findUnassignedParentValue(sessionCorpID,wareHouse);
	}
	public void updateDailyDefLimit(String sessionCorpID,String dailyOpHubLimitPC){
		operationsHubLimitsDao.updateDailyDefLimit(sessionCorpID,dailyOpHubLimitPC);
	}
	public void updateMinServDay(String sessionCorpID,String minServiceDayHub){
		operationsHubLimitsDao.updateMinServDay(sessionCorpID,minServiceDayHub);
	}
	public void updateCrewLimitParent(Integer tempCrewLimit,Integer tempCrewValueOld,Integer tempCrewValueNew, String sessionCorpID,String hubID){
		operationsHubLimitsDao.updateCrewLimitParent(tempCrewLimit,tempCrewValueOld,tempCrewValueNew,sessionCorpID,hubID);
	}
	public void updateCrewDailyLimit(String sessionCorpID,String dailyCrewLimit){
		operationsHubLimitsDao.updateCrewDailyLimit(sessionCorpID,dailyCrewLimit);
	}	
	public void updateCrewThreshHoldLimit(String sessionCorpID,String dailyCrewThreshHoldLimit){
		operationsHubLimitsDao.updateCrewThreshHoldLimit(sessionCorpID,dailyCrewThreshHoldLimit);
	}
	
}
