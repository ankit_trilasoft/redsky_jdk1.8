<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<head>   
    <title>Email Setup Template List</title>   
    <meta name="heading" content="Email Setup Template List"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
 <style>
 span.pagelinks {
 display:block;font-size:0.90em;margin-bottom:3px;!margin-bottom:2px;margin-top:-8px;padding:2px 0px; text-align:right;width:99%;}
#overlay11 {filter:alpha(opacity=70);-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);}
form{margin-top:-40px;!margin-top:-5px;}
span.pagelinks {!margin-top:-15px;}

</style>
</head>
<script language="javascript" type="text/javascript">
	function clear_fields(){
		document.forms['emailSetupTemplateList'].elements['moduleName'].value = "";
		document.forms['emailSetupTemplateList'].elements['languageName'].value = "";
		document.forms['emailSetupTemplateList'].elements['descriptionName'].value = "";
	}

	function goToSearch(){
		var descriptionName = document.forms['emailSetupTemplateList'].elements['descriptionName'].value;
		 if(descriptionName.includes(" :")){
				alert("Usage of space between text and : is not allowed. Please remove the space and continue");
		  return false;
	 }
		 document.forms['emailSetupTemplateList'].action = 'searchEmailSetupTemplate.html';
		 document.forms['emailSetupTemplateList'].submit();	
	}

</script>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:52px;" align="top" method="" key="button.search" onclick="return goToSearch();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px;" onclick="clear_fields();"/> 
</c:set>
<s:form id="emailSetupTemplateList" action="" method="post" > 

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" style="margin-bottom:0px;">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th width="300px;">Module</th>
			<th width="300px;">Description</th>
			<th width="300px;">Language</th>
			<th width="300px;"></th>	
		</tr>
	</thead>	
	<tbody>
		<tr>			
		<td width="170">
			<configByCorp:customDropDown listType="map" list="${moduleList}" fieldValue="${moduleName}"  attribute="class=list-menu name=moduleName style=width:200px headerKey='' headerValue='' "/>
		</td>
		<td width="">
		    <s:textfield name="descriptionName" maxlength="100" cssStyle="width:215px;" cssClass="input-text" />
		</td>	
		<td align="left">
			<configByCorp:customDropDown listType="map" list="${languageList}" fieldValue="${languageName}" attribute="class=list-menu name=languageName  style=width:200px headerKey='' headerValue='' "/>
		</td>
		
		<td width="120">
			<c:out value="${searchbuttons}" escapeXml="false" /></td>
		</tr>
	</tbody>
</table>
<c:out value="${searchresults}" escapeXml="false" />  
<div style="!margin-top:7px;"></div>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="layer1" style="width:100%"> 
	<div class="spnblk">&nbsp;</div>
		<div id="newmnav" style="margin-bottom:0px;!margin-bottom:-13px;">
		  	<ul>
		    	<li id="newmnav1"><a class="current"><span>E-mail List</span></a></li>		  	
		    	<li><a onclick="location.href='<c:url value="/editEmailSetupTemplate.html"/>'"/><span>Create</span></a></li>
		  	</ul>
		</div>
<div class="spnblk">&nbsp;</div>

<display:table name="emailSetupTemplateList" class="table" requestURI="" id="emailSetupTemplateList" pagesize="10" style="width:100%;margin-top:1px; " >
	
	<display:column property="orderNumber" sortable="true" title="Order" headerClass="centeralign" style="width:5px;text-align:center"></display:column>
	<display:column property="module" sortable="true" title="Module" style="width:50px;"></display:column>
	<display:column title="Description" sortable="true" style="width:250px;" sortProperty="saveAs">
	<a href="editEmailSetupTemplate.html?id=${emailSetupTemplateList.id}"> ${emailSetupTemplateList.saveAs}</a>
	</display:column>
	<display:column title="Language" sortable="true" style="width:50px;" sortProperty="language">
		<c:if test="${emailSetupTemplateList.language == 'en'}"> 
			<c:out value="English" />
		</c:if>
		<c:if test="${emailSetupTemplateList.language == 'nl'}"> 
			<c:out value="Dutch" />
		</c:if>
		<c:if test="${emailSetupTemplateList.language == 'fr'}"> 
			<c:out value="French" />
		</c:if>
		<c:if test="${emailSetupTemplateList.language == 'de'}"> 
			<c:out value="German" />
		</c:if>
	</display:column>
	<display:column property="createdBy" sortable="true" title="Created By" style="width:50px;"></display:column>
	<display:column property="createdOn" sortable="true" title="Created On" style="width:60px;" format="{0,date,dd-MMM-yyyy}"></display:column>
	<display:column property="updatedBy" sortable="true" title="Updated By" style="width:50px;"></display:column>
	<display:column property="updatedOn" sortable="true" title="Updated On" style="width:50px;" format="{0,date,dd-MMM-yyyy}"/>
	<display:column title="Enable" headerClass="centeralign" sortable="true" style="text-align:center;width:10px;">
		<c:if test="${emailSetupTemplateList.enable == true}">
	    	<input type="checkbox" id="checkboxId" value="${emailSetupTemplateList.id}" onclick="emailTemplateActiveInactive('${emailSetupTemplateList.id}',this);" checked/>
	    </c:if>
		<c:if test="${emailSetupTemplateList.enable == false || emailSetupTemplateList.enable == null}">
			<input type="checkbox" id="checkboxId" value="${emailSetupTemplateList.id}" onclick="emailTemplateActiveInactive('${emailSetupTemplateList.id}',this);"/>
		</c:if>
	</display:column>
</display:table>
<div id="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
				<td align="center">
				<table cellspacing="0" cellpadding="3" align="center">
				<tr>
				<td height="200px"></td>
				</tr>
				<tr>
		       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
		           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait...</font>
		       </td>
		       </tr>
		       <tr>
			      	<td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
			           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
			       </td>
		       </tr>
	       	   </table>
		       </td>
	       </tr>
       </table>
	</div>
</div>
</s:form>
<script type="text/javascript">
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   }else if (value==1) {
      if (document.layers)
          document.layers["overlay11"].visibility='show';
       else
          document.getElementById("overlay11").style.visibility='visible';
   }
}
function emailTemplateActiveInactive(id,targetElement){
	showOrHide(1);
	var emailTemplateChecked=true;
	if(targetElement.checked==false){
		emailTemplateChecked=false;
	}
	new Ajax.Request('/redsky/activeInactiveEmailTemplateAjax.html?ajax=1&id='+id+'&emailTemplateActive='+emailTemplateChecked+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			    	var response = transport.responseText || "no response text";
			    	showOrHide(0);
			    },
			    onFailure: function(){ 
				}
			  });
}
showOrHide(0);
</script>