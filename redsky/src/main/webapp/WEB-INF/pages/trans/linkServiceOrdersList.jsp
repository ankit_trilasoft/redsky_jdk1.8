<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head> 

    <title>Merge <c:out value="${soObj.shipNumber }" /> To :</title>   
    <meta name="heading" content="merge"/> 
    <style type="text/css">
	 #overlayForLinkSo {
	filter:alpha(opacity=70);
	-moz-opacity:0.7;
	-khtml-opacity: 0.7;
	opacity: 0.7;
	position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
	background:url(images/over-load.png);
	}
    </style> 
 <script language="javascript">
 function getHTTPObject() {
	  var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	      
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
 }
 var httpMergeSO = getHTTPObject();
 var httpCheckCF = getHTTPObject();
 function showSO(target,val){
	 var soDiv = document.getElementById("soList");
	 var seqNumber = target.value;
	 var bookingAgentcode = document.forms['mergeForm'].elements['CFBookingAgentCode'].value;
	 var agentType = document.forms['mergeForm'].elements['agentType'].value;
	 var soUGWWID = document.forms['mergeForm'].elements['soId'].value;
	 if(seqNumber!=''){
		 soDiv.style.display = 'none';
		 	document.getElementById("loader").style.display = "block";
		 	var url="linkingSO.html?ajax=1&decorator=simple&popup=true&seqNumber="+seqNumber+"&bookingAgentcode="+bookingAgentcode+"&agentType="+agentType+"&soUGWWID="+soUGWWID+"&filter="+val;
		 	httpMergeSO.open("GET", url, true);
		 	httpMergeSO.onreadystatechange =  handleHttpResponseMergeSoList;
			httpMergeSO.send(null);	
	 	}else{
		 	soDiv.style.display = 'none';
	 	}
}
 function handleHttpResponseMergeSoList(){
	if (httpMergeSO.readyState == 4){
	    var results= httpMergeSO.responseText; 
	    var soDiv = document.getElementById("soList");      
	    soDiv.style.display = 'block';
	    document.getElementById("loader").style.display = "none";
	    soDiv.innerHTML = results;
	    
	    }
}

/*  function mergeSO(){
		var soUGWWID = document.forms['mergeForm'].elements['soId'].value;
		var mergingSOID = document.forms['mergeForm'].elements['mergedSoId'].value;
		var agentType = document.forms['mergeForm'].elements['agentType'].value;
		var childAgentType = document.forms['mergeForm'].elements['childAgentType'].value;
		var childAgentCode = document.forms['mergeForm'].elements['childAgentCode'].value;
		location.href ="linkWithSO.html?soUGWWID="+soUGWWID+"&mergingSOID="+mergingSOID+"&decorator=popup&popup=true&agentType="+agentType+"&childAgentType="+childAgentType+"&childAgentCode="+childAgentCode;
	} */
	function mergeFields(){
		document.forms['mergeForm'].elements['closeWindowFlag'].value='1';
		var soUGWWID = document.forms['mergeForm'].elements['soId'].value;
		var mergingSOID = document.forms['mergeForm'].elements['mergedSoId'].value;
		var agentType = document.forms['mergeForm'].elements['agentType'].value;
		var childAgentType = document.forms['mergeForm'].elements['childAgentType'].value;
		var childAgentCode = document.forms['mergeForm'].elements['childAgentCode'].value;
		location.href ="mergingField.html?soUGWWID="+soUGWWID+"&mergingSOID="+mergingSOID+"&decorator=popup&popup=true&agentType="+agentType+"&childAgentType="+childAgentType+"&childAgentCode="+childAgentCode;
	}
	
 function setLinkSOIdvalue(target){
	 document.forms['mergeForm'].elements['mergedSoId'].value = target;
	 document.forms['mergeForm'].elements['Merge'].disabled = false;
 }
 
 function closeMyWindow(){
	var msg = '${successMessage}';
	if(msg!=''){
		alert(msg);
	}
	var count='${hitFlag}';
	if(count=="Y"){
		//window.opener.location.href = window.opener.location.href;
		window.opener.location.href = "toDos.html?fromUser=${userFirstName}";
		//parent.window.opener.document.location.reload();
		document.forms['mergeForm'].elements['closeWindowFlag'].value='1';
		window.close();
	}
 }
 function createSO(){
	var soUGWWID = document.forms['mergeForm'].elements['soId'].value;
	var mergingSeqNum = document.forms['mergeForm'].elements['cfSeqNumber'].value;
	//if(mergingSeqNum==''){
	//	mergingSeqNum = document.forms['mergeForm'].elements['cfSeqNumberCatname'].value;
	//}
	var agentType = document.forms['mergeForm'].elements['agentType'].value;
	var childAgentType = document.forms['mergeForm'].elements['childAgentType'].value;
	var childAgentCode = document.forms['mergeForm'].elements['childAgentCode'].value;
	var cordName =  document.forms['mergeForm'].elements['coordName'].value;
	document.forms['mergeForm'].elements['coordSelected'].value =cordName;
	var agentCompDiv = document.forms['mergeForm'].elements['agentCompDiv'].value 
	
	if(mergingSeqNum!=''){
		document.getElementById("overlayForLinkSo").style.display = "block";
		document.forms['mergeForm'].elements['closeWindowFlag'].value='1';
		location.href ="createNewLinkSO.html?soUGWWID="+soUGWWID+"&mergingSeqNum="+mergingSeqNum+"&decorator=popup&popup=true&agentType="+agentType+"&childAgentType="+childAgentType+"&childAgentCode="+childAgentCode+"&coordSelected="+cordName+"&agentCompDiv"+agentCompDiv;
 	}else{
 	 	alert("Please select the Customer File.");
 	}
 }
 function chkStatusFlag(target){
	 var seqNumber = target.value;
	 if(seqNumber!=''){
			var url="chkCFFlag.html?ajax=1&decorator=simple&popup=true&seqNumber="+seqNumber;
			httpCheckCF.open("GET", url, true);
			httpCheckCF.onreadystatechange =  handleHttpResponseCFFlag;
			httpCheckCF.send(null);	
	 } 
 }
 function handleHttpResponseCFFlag(){
		if (httpCheckCF.readyState == 4){
		    var result= httpCheckCF.responseText; 
		    result = result.trim();
		    if(result=='Q'){
		    	document.forms['mergeForm'].elements['CreateSO'].disabled = true;
		    }
		}
	}
 function createNewCfSo(){
 		var agree= confirm("Are you sure you want to Accept ${soObj.shipNumber }");
		if(agree){
			document.getElementById("overlayForLinkSo").style.display = "block";
			var soId = document.forms['mergeForm'].elements['soId'].value;
			var agentType = document.forms['mergeForm'].elements['agentType'].value;
			var childAgentType = document.forms['mergeForm'].elements['childAgentType'].value;
			var childAgentCode = document.forms['mergeForm'].elements['childAgentCode'].value;
			var cordName =  document.forms['mergeForm'].elements['coordName'].value;
			document.forms['mergeForm'].elements['coordSelected'].value =cordName;
			document.forms['mergeForm'].elements['closeWindowFlag'].value='1';
			document.forms['mergeForm'].action="linkNetworkFileCF.html?soId="+soId+"&agentType="+agentType+"&childAgentType="+childAgentType+"&childAgentCode="+childAgentCode+"&coordSelected="+cordName;
			document.forms['mergeForm'].submit();
		}
}
 window.onbeforeunload = function (evt) {
		if(document.forms['mergeForm'].elements['closeWindowFlag'].value!='1'){
		  var message = 'Are you sure you want to leave?';
		  if (typeof evt == 'undefined') {
			 
		    evt = window.event;
		  }
		  if (evt) {
			  
			  window.opener.closeBoxDetails();
		  }
		  
		  //return message;
		}
	}
//function changeList(target){
//	if(target.checked){
//		document.getElementById('cfSeqNumber').value = '';
//		document.getElementById('cfSeqNumberCatname').value = '';
	//	document.getElementById('cfSeqNumber').style.display = 'none'
	//	document.getElementById('cfSeqNumberCatname').style.display = 'block'
	//}else{
//		document.getElementById('cfSeqNumber').value = '';
//		document.getElementById('cfSeqNumberCatname').value = '';
//		document.getElementById('cfSeqNumber').style.display = 'block'
//		document.getElementById('cfSeqNumberCatname').style.display = 'none'
//	}
//}
</script>
</head>
<form name="mergeForm" id="mergeForm" >
<s:hidden name="soId" value="<%=request.getParameter("soId") %>" />
<c:set var="soId" value="<%=request.getParameter("soId") %>" />
<s:hidden name="agentType" value="<%=request.getParameter("agentType") %>" />
<c:set var="agentType" value="<%=request.getParameter("agentType") %>" />
<s:hidden name="childAgentType" value="<%=request.getParameter("childAgentType") %>" />
<c:set var="childAgentType" value="<%=request.getParameter("childAgentType") %>" />
<s:hidden name="childAgentCode" value="<%=request.getParameter("childAgentCode") %>" />
<c:set var="childAgentCode" value="<%=request.getParameter("childAgentCode") %>" />
<s:hidden name="agentCompDiv" value="<%=request.getParameter("agentCompDiv") %>" />
<c:set var="agentCompDiv" value="<%=request.getParameter("agentCompDiv") %>" />
<c:set var="hitFlag" value="${hitFlag}" />
<s:hidden name="coordSelected"  />
<s:hidden name="mergedSoId" />
<s:hidden name="closeWindowFlag" value=""></s:hidden>
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
   
   
<table style="width:100%;">
<tr>
<td class="listwhitebox">Agent Code </td>
<td class="listwhitebox" style="text-align: left;" >Customer File Number,Name</td>
<td class="listwhitebox" style="text-align: left;" >Responsible Move Manager</td>
</tr>
<tr>
<td><s:textfield name="CFBookingAgentCode" value="<%=request.getParameter("bookingAgentcode") %>" size="10"  cssClass ="input-textUpper" readonly="true" /> </td>
<td style="text-align: left;">
	<s:select cssClass="list-menu" id="cfSeqNumber"  name="cfSeqNumber" list="%{sequenceNumberMap}" headerKey="" headerValue="" cssStyle="width:150px" onchange="showSO(this,'default');chkStatusFlag(this);" />
	<!--<s:select cssClass="list-menu" id = "cfSeqNumberCatname" name="cfSeqNumberCatname" list="%{sequenceNumberMapByName}" headerKey="" headerValue="" cssStyle="width:150px ; display : none" onchange="showSO(this,'name');chkStatusFlag(this);" />-->
</td>
<td>
	<s:select cssClass="list-menu" id="coordName1"  value="%{tempUserForPage }" name="coordName" list="%{coord}" headerKey="" headerValue="" cssStyle="width:150px"  />
</td>
<!--<td class="listwhitebox" style="text-align: left;" valign="top"><input name="chngeList" type="checkbox" onclick="changeList(this);" />Search by Name Only</td>-->
</tr>
<tr>
<td class="listwhitebox" colspan="5">Agent Name</td>
</tr>
<tr>
<td colspan="5" style="text-align: left;"> <s:textfield name="CFBookingAgentName" value="<%=request.getParameter("bookingAgentName") %>" cssStyle="width:88%" cssClass ="input-textUpper" readonly="true" /></td>
</tr>
</table>

</div>
<div class="bottom-header"><span></span></div>
</div>


<div style="text-align:center; display:none" id="loader"><img src="/images/ajax-loader.gif" /></div>
<div id="soList">

</div>
<table>
	<tr>
		<td><input type="button" name="CreateCFSO" value="Create new CF & SO" id="CreateCFSO" onclick ="createNewCfSo()" class="cssbutton"  /></td>
	</tr>
</table>
<div id="overlayForLinkSo" style="display:none">
<div id="layerLoading">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>

       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
       <img src="<c:url value='/images/animation.png'/>" />       
       </td>
       </tr>
       <tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
     </table>
   </td>
  </tr>
</table>  
   </div>   
   </div>
	
</form>
<script>
try{
	document.getElementById("soList").style.display = 'none';
	closeMyWindow();
	
}catch(e){}
</script>