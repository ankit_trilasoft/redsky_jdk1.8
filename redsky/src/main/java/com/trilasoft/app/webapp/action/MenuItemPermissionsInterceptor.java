package com.trilasoft.app.webapp.action;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.navigator.menu.MenuRepository;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;
import org.hibernate.Filter;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.service.MenuItemManager;

public class MenuItemPermissionsInterceptor  implements Interceptor {

	private static Map permissionMap = AppInitServlet.pageActionUrls;
	private String permissionAjaxUrl = (String)AppInitServlet.redskyConfigMap.get("BY_PASS_AJAX_URL");
	private String permissionByPassUrl=(String)AppInitServlet.redskyConfigMap.get("BY_PASS_URL");
	private static final long serialVersionUID = 5067790608840427509L;
    private String[] authorizedRoles;
	private MenuItemManager menuItemManager ;
	private User user;
	private RoleManager roleManager;
	public ResourceBundle resourceBundle;	
	static final Logger logger = Logger.getLogger(MenuItemPermissionsInterceptor.class);
	public MenuItemManager getMenuItemManager() {
		return menuItemManager;
	}

	public void setMenuItemManager(MenuItemManager menuItemManager) {
		this.menuItemManager = menuItemManager;
	}

	public void destroy() {
		
	}

	public void init() {
		
	}
	

	public String intercept(ActionInvocation invocation) throws Exception {
 		  try{
			  if(ActionContext.getContext().getLocale()==null || !ActionContext.getContext().getLocale().toString().equalsIgnoreCase("en")){
				  Locale locale = new Locale("en");
				  ActionContext.getContext().setLocale(locale);
			  }
		  }catch(Exception e){e.printStackTrace();}
		  HttpServletRequest request = ServletActionContext.getRequest();
		  Date currentdate = new Date();
	      HttpSession session = request.getSession();
		  net.sf.navigator.menu.MenuRepository menuRepository = (MenuRepository) session.getAttribute("repository");
		  boolean authorized = false;
		  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		  Filter agentFilter=null;
		  String sessionCorpID = null;
		  Boolean isSecurityEnabled=false;
    	  String url = (request.getRequestURI()).substring(request.getRequestURI().lastIndexOf("/"), request.getRequestURI().length());
    	  
    	  if(url.indexOf("!")>-1){
    		  url=url.replaceAll(url.substring(url.indexOf("!"), url.indexOf(".")), "");
    	  } 
    	  if (auth != null) { 
			   
    		  	try{
				      if(!SecurityContextHolder.getContext().getAuthentication().getName().equalsIgnoreCase("anonymous")){
				         User user = (User) auth.getPrincipal();
					     sessionCorpID = user.getCorpID();
					     this.user=user;
					     isSecurityEnabled=menuItemManager.isSecurityEnabled(sessionCorpID);
					 }else{
					     if(permissionByPassUrl.indexOf(url.replaceAll(url.substring(url.indexOf("."), url.length()), "").trim())>-1){
						     return invocation.invoke();
						 }else{
							logger.warn("\nUnAuthorized USER AUDIT: User Name: "+user.getUsername()+"Start\n\n\n\n\n\n\n\n\n");
					    	logger.warn("\nUnAuthorized USER Roles: "+user.getRoles());
					    	logger.warn("\nUrl: "+url);
					    	logger.warn("\nTime: "+currentdate+"\n\n\n\n\n\n\n\n\n");
					    	logger.warn("\nUnAuthorized USER AUDIT: User Name: "+user.getUsername()+"End");
					    	return "NotAuthorized";
						 }
						 }
				  }catch(Exception e){ 
					  e.printStackTrace();
					  if(user!=null){
					  logger.error("Error determining permissions: user "+user.getUsername()+" roles: "+user.getRoles()+" URL "+url);
					  }else{
						  logger.error("Error determining url: "+url);
					  }
					  }
		  }
		     if(isSecurityEnabled && (user==null || !user.getRoles().contains(roleManager.getRole("ROLE_TRILA")))){   
				    	  
		    	 		  System.out.println("The Menu repository is "+menuRepository);
				    	  boolean isAuthorized = false;
				    	  Set<Role> userRoleList=user.getRoles();
				    	  
				    	  /**
				    	   * Synchronized block for the code that determines URL 
				    	   * permissions for a particular role.
				    	   **/
				    	  synchronized(userRoleList){
				    		  for(Role role:userRoleList){
					    		  List permittedUrls = (List) permissionMap.get(sessionCorpID+"-"+role.getName());
					    		  if(permittedUrls!=null && permittedUrls.contains(url)){
					    			  isAuthorized = true;
					    		  }
					    	  }  
				    	  }
				    	  
				    	  
				    	  try{
				    	  
				    		  if(permissionAjaxUrl.indexOf(url)>-1){
				    		  isAuthorized = true;
				    	  
				    		  }}catch(Exception e){ e.printStackTrace();}
				    	  
				    	  	  System.out.println("Authorizing RS URL: "+url+" for Roles: "+userRoleList);
				    	  	  
				    	  if(!isAuthorized){
				    		logger.warn("\nUnAuthorized USER AUDIT: User Name: "+user.getUsername()+"Start\n\n\n\n\n\n\n\n\n");
				    		logger.warn("\nUnAuthorized USER Roles: "+user.getRoles());
				    		logger.warn("\nUrl: "+url);
				    		logger.warn("\nTime: "+currentdate+"\n\n\n\n\n\n\n\n\n");
				    		logger.warn("\nUnAuthorized USER AUDIT: User Name: "+user.getUsername()+"End");
				        	return "NotAuthorized";
				    	  }else{
				        	return invocation.invoke();
				    	  }
		     }else{
		      return invocation.invoke();
		     }
		 }
	/**
     * Set the roles that this interceptor should treat as authorized.
     * @param authorizedRoles array of role names
     */
    public final void setAuthorizedRoles(String[] authorizedRoles) {
        this.authorizedRoles = authorizedRoles;
    }

    /**
     * Handle a request that is not authorized according to this interceptor.
     * Default implementation sends HTTP status code 403 ("forbidden").
     * <p>This method can be overridden to write a custom message, forward or
     * redirect to some error page or login page, or throw a ServletException.
     * @param request current HTTP request
     * @param response current HTTP response
     * @param handler chosen handler to execute, for type and/or instance evaluation
     * @throws javax.servlet.ServletException if there is an internal error
     * @throws java.io.IOException in case of an I/O error when writing the response
     */
    protected void handleNotAuthorized(HttpServletRequest request,
                                       HttpServletResponse response)
    throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
    }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setRoleManager(RoleManager roleManager) {
		this.roleManager = roleManager;
	}


}
