<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
 <%@page autoFlush="true" %>
<head>   
    <title>Distribution Revenue</title>   
    <meta name="heading" content="Distribution Revenue"/>   
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />  
   
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:2px;margin-top:-18px;!margin-top:-17px;
padding:2px 0px;text-align:right;width:100%;!width:98%;
}

div.error, span.error, li.error, div.message {
width:450px; margin-top:0px; 
}
form {
margin-top:-40px;!margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}
.table td, .table th, .tableHeaderTable td {   
    padding: 0.4em;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:55px;" align="top" method="" key="button.search" onclick=""/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px;" onclick="clear_fields();"/> 
</c:set>
<s:form id="distributionRevenueListForm" action="searchServiceOrder" method="post" name="distributionRevenueListForm"> 
<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
<s:hidden name="accID" />
<s:hidden name="venderDetails" />
<s:hidden name="fileNovenderDetails" />
<s:hidden name="venderDetailsList" />
<s:hidden name="fileNo" />
<%-- <s:hidden name="removeFileNo" />
<s:hidden name="removeAccID" /> --%>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="otabs"><!-- sandeep -->
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:11px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th><fmt:message key="serviceOrder.shipNumber"/></th>
			<th><fmt:message key="serviceOrder.registrationNumber"/></th>
			<th><fmt:message key="serviceOrder.lastName"/></th>
			<th><fmt:message key="serviceOrder.firstName"/></th>
			<th></th>
		</tr>
	</thead>	
	<tbody>
		<tr>			
			<td>
			    <s:textfield name="serviceOrder.shipNumber" size="20" required="true" cssClass="input-text"/>
			</td>
			<td>
			    <s:textfield name="serviceOrder.registrationNumber" size="20" required="true" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="serviceOrder.lastName" size="25" required="true" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="serviceOrder.firstName" size="25" required="true" cssClass="input-text"/>
			</td>
			<td style="vertical-align:bottom;text-align:left;padding:5px 10px 5px 10px;" colspan="2" ><!-- sandeep -->
			<c:out value="${searchbuttons}" escapeXml="false" /></td>
		</tr>
		
	</tbody>
</table>
<c:out value="${searchresults}" escapeXml="false" />  
<div style="!margin-top:7px;"></div>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div class="spnblk">&nbsp;</div> 
<s:set name="SOListExt" value="serviceOrders" scope="request"/>
<display:table name="SOListExt" class="table"  requestURI="" id="soList" pagesize="10">
<display:column title="" style="width:10px"> 
	<img id="${soList_rowNum }" onclick ="openXMLArea('${soList.shipNumber }','C',this,this.id)" src="${pageContext.request.contextPath}/images/plus-small.png"  />
</display:column>
<display:column property="shipNumber"  title="Order #"/>
<c:choose>
<c:when test="${newAccountline=='Y'}">
<display:column title="Registration #">
<a href="javascript:;" onclick="window.open('pricingList.html?sid=${soList.id}')" ><c:out value="${soList.registrationNumber}" /></a>
</display:column>
</c:when>
<c:otherwise>
<display:column title="Registration #">
<a href="javascript:;" onclick="window.open('accountLineList.html?sid=${soList.id}')" ><c:out value="${soList.registrationNumber}" /></a>
</display:column>
</c:otherwise>
</c:choose>
<display:column property="lastName"  title="Last Name"/>
<display:column property="firstName"  title="First Name"/>
</display:table>
<c:if test="${not empty soList  }">
	<input type="button" name="processAcc" value ="Process Accounting" onclick="return processAccountLines()"  class = "cssbutton1" style="width: 145px;"/>
</c:if>
<c:if test="${ empty soList  }">
	<input type="button" name="processAcc" value ="Process Accounting" disabled="disabled" class = "cssbutton1" style="width: 145px;"/>
</c:if>
</s:form>
<script type="text/javascript"> 

//  script for tree .....
var flagValue = 0;
var val = 0;
var lastId ='';

var flagValue1 = 0;
var val1 = 0;
var lastId1 ='';

function openXMLArea(shipNum,controlFlag,position,id){
	
	if(flagValue==0){
		document.getElementById("overlay").style.visibility = "visible";
		var table=document.getElementById("soList");
		var rownum = document.getElementById(id);
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		var rowId = "xml-"+val;
		row.setAttribute("id",rowId);
		new Ajax.Request('processedXML.html?shipNum='+shipNum+'&decorator=simple&popup=true',
				  {
				    method:'get',
					onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      var container = $(shipNum);
				      container.update(response);
				      container.show();
				      document.getElementById("overlay").style.visibility = "hidden";
				    },
				    onFailure: function(){ 
					    //alert('Something went wrong...')
					     }
				  });
		
		     row.innerHTML = "<td colspan=\"5\"><div id="+shipNum+" style=\"margin-top:40px;margin-left:20px;clear:both;\"></div></td>";
		     flagValue = 1;
		     return 0;
	}
	if(flagValue==1){
		//document.getElementById("overlay").style.visibility = "visible";
		var rowId = "xml-"+val;
		document.getElementById(rowId).parentNode.removeChild(document.getElementById(rowId));
		flagValue = 0;
		var table=document.getElementById("soList");
		var rownum = document.getElementById(id);
		if(lastId!=rownum.id){
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		rowId = "xml-"+val;
		row.setAttribute("id",rowId);
		
		new Ajax.Request('processedXML.html?shipNum='+shipNum+'&decorator=simple&popup=true',
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      var container = $(shipNum);
				      container.update(response);
				      container.show();
				      document.getElementById("overlay").style.visibility = "hidden";
				    },
				    onFailure: function(){ //alert('Something went wrong...')
				    	}
				  });
		
		     row.innerHTML = "<td colspan=\"5\"><div id="+shipNum+" style=\"margin-top:40px;margin-left:20px;clear:both;\"></div></td>";
		     flagValue = 1;
		     flagValue1 = 0;
		     val1 = 0;
		     lastId1 ='';
		}
	     return 1;
	}
}
	



function openAccountLineArea(fileName,controlFlag,position,id,shipNum){
		var table=document.getElementById("processedXMLList");
		var rownum = document.getElementById(id);
		lastId1 = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val1 = newrow+1;
		var row=table.insertRow(val1);
		var rowId = "acc-"+val1;
		row.setAttribute("id",rowId);
		 new Ajax.Request('downloadAccList.html?shipNum='+shipNum+'&fileName='+fileName+'&decorator=simple&popup=true',
				  {
				      method:'get',
				      onSuccess: function(transport1){
				     var response1 = transport1.responseText || "no response text";
				      var container1 = $(fileName);
				      container1.update(response1);
				      container1.show();
				    },
				    onFailure: function(){ 
					 //  alert('Something went wrong...')
					     }
				  });
			  row.innerHTML = "<td colspan=\"7\"><div id="+fileName+" style=\"margin-top:60px;margin-left:20px;clear:both;\"></div></td>";
			  var minusId = position.id.replace("plus","sub");
			  var minus = document.getElementById(minusId);
			  
			  minus.style.display = "block";	  
			  position.style.display = "none";
}
function closeAccountLineArea(object){
	var myrow = object.parentNode.parentNode;
	var next = myrow.nextElementSibling; 
	var newrow = myrow.parentNode.rowIndex;
	next.parentNode.removeChild(next);
	 // document.getElementById(rowId).parentNode.removeChild(document.getElementById(rowId));
	 var plusId = object.id.replace("sub","plus");
	 var plus = document.getElementById(plusId);
	 plus.style.display = "block"; 
	 object.style.display = "none";
}

// script for processing lines 
function processAccountLines(){
	var accVal = document.forms['distributionRevenueListForm'].elements['accID'].value;
	var fileVal = document.forms['distributionRevenueListForm'].elements['fileNo'].value;
	var finalVender=document.forms['distributionRevenueListForm'].elements['venderDetails'].value;
	var fileNoVenderDetails=document.forms['distributionRevenueListForm'].elements['fileNovenderDetails'].value;
	var venderDetailsList=document.forms['distributionRevenueListForm'].elements['venderDetailsList'].value;
	//var removeFileVal = document.forms['distributionRevenueListForm'].elements['removeFileNo'].value;
	//var removeAccVal = document.forms['distributionRevenueListForm'].elements['removeAccID'].value;
	if(accVal==',')
		accVal='';
	if(fileVal == ',')
		fileVal = '';
	if(finalVender==',')
		finalVender='';
	if(fileNoVenderDetails==',')
		fileNoVenderDetails=''
	if(venderDetailsList==',')
		venderDetailsList=''
	/* if(removeFileVal==',')
		removeFileVal='';
	if(removeAccVal == ',')
		removeAccVal = ''; */
	//alert(finalVender)
	//alert("ACCID:"+accVal+"\n FileNAme:"+fileVal+"\n remove accid :"+removeAccVal+"\n remove File id:"+removeFileVal)
	// var url = "updateAccLineDowanload.html?accID="+accVal+"&fileName="+fileVal+"&removeFileVal="+removeFileVal+"&removeAccVal="+removeAccVal ;
	 var url = "updateAccLineDowanload.html?accID="+accVal+"&accVenderDetails="+finalVender+"&venderDetailsList="+venderDetailsList+"&fileNoVenderDetails="+fileNoVenderDetails+"&fileName="+fileVal;
	document.forms['distributionRevenueListForm'].action = url;
	document.forms['distributionRevenueListForm'].submit(); 
}

		function venderList(accId,obj){	
			var venderDetailsList=document.forms['distributionRevenueListForm'].elements['venderDetailsList'].value;
			var accIdVender=accId+"~"+obj.value;
			//alert(accIdVender)
			if(venderDetailsList==''){
				document.forms['distributionRevenueListForm'].elements['venderDetailsList'].value=accIdVender;
			}else{
				document.forms['distributionRevenueListForm'].elements['venderDetailsList'].value=venderDetailsList+","+ accIdVender;
			}			
			
		}
// script for file number logic
function updateFileNo(obj){
	var fileVal = document.forms['distributionRevenueListForm'].elements['fileNo'].value;
	var fileNovenderDetails=document.forms['distributionRevenueListForm'].elements['fileNovenderDetails'].value;
	//var removeFileVal = document.forms['distributionRevenueListForm'].elements['removeFileNo'].value;
	  var accIdDetails=document.getElementById('accId'+obj.value).value;
	  accIdDetails=accIdDetails.replace("[","").replace("]","")	;	
	if(obj.checked){
		if(fileVal==''){
			document.forms['distributionRevenueListForm'].elements['fileNo'].value= obj.value;
		}else{
			document.forms['distributionRevenueListForm'].elements['fileNo'].value = fileVal+","+obj.value;
		}
		 var detailsData="";
		  var res = accIdDetails.split(",");		 
		   for(i=0; i<res.length; i++){
			   var venderCodeDetails='';
			   var id='venderCode'+res[i].trim();
			   try{			
			   venderCodeDetails=document.getElementById(id).value;
			   }catch(e){}
			   if(venderCodeDetails!=null && venderCodeDetails!=''){
			    	 var accIdVender=res[i].trim()+"~"+venderCodeDetails;
				   if(detailsData==''){
				    	detailsData=accIdVender
				    }else{
				    	detailsData=detailsData+","+accIdVender;				    	
				    }			    
			    }		    
			  
		  }
			 // alert(detailsData)
			if(fileNovenderDetails=='' && detailsData!='' ){
				 document.forms['distributionRevenueListForm'].elements['fileNovenderDetails'].value=detailsData;
			}else if(detailsData!=''){
				document.forms['distributionRevenueListForm'].elements['fileNovenderDetails'].value=fileNovenderDetails+","+detailsData;
			}  
		/* removeFileVal = removeFileVal.replace(obj.value,'');
		removeFileVal = removeFileVal.replace(',,',',');
		document.forms['distributionRevenueListForm'].elements['removeFileNo'].value= removeFileVal; */
	}else{
		/* if(removeFileVal==''){
			document.forms['distributionRevenueListForm'].elements['removeFileNo'].value= obj.value;
		}else{
			document.forms['distributionRevenueListForm'].elements['removeFileNo'].value = removeFileVal+","+obj.value;
		} */
		
		fileVal = fileVal.replace(obj.value,'');
		fileVal = fileVal.replace(',,',',');
		document.forms['distributionRevenueListForm'].elements['fileNo'].value= fileVal;
	}
}

// script for acc lines id logic
function updateAccId(accObj){
	var fileVal = document.forms['distributionRevenueListForm'].elements['accID'].value;
	var venderOldValue=document.forms['distributionRevenueListForm'].elements['venderDetails'].value;
	var accId=accObj.value;
	//alert(accId);
	var venderCodeDetails=document.getElementById('venderCode'+accId).value;
	if(venderCodeDetails==''){
		venderCodeDetails='A';
	}
	var accIdVender=accId+"~"+venderCodeDetails;
	//alert(accIdVender)	
	if(accObj.checked){
		if(fileVal==''){
			document.forms['distributionRevenueListForm'].elements['accID'].value= accObj.value;
			document.forms['distributionRevenueListForm'].elements['venderDetails'].value=accIdVender;
		}else{
			document.forms['distributionRevenueListForm'].elements['accID'].value = fileVal+","+accObj.value;
			document.forms['distributionRevenueListForm'].elements['venderDetails'].value=venderOldValue+","+accIdVender;
		}
	}else{		
		fileVal = fileVal.replace(accObj.value,'');
		fileVal = fileVal.replace(',,',',');
		 var len=fileVal.length-1;
	    if(len==fileVal.lastIndexOf(",")){
	    	fileVal=fileVal.substring(0,len);
	        }
	    if(fileVal.indexOf(",")==0){
	    	fileVal=fileVal.substring(1,fileVal.length);
	        }        
		document.forms['distributionRevenueListForm'].elements['accID'].value= fileVal;
	}
}
// function for clear value
function clear_fields(){
	document.forms['distributionRevenueListForm'].elements['serviceOrder.shipNumber'].value= "";
	document.forms['distributionRevenueListForm'].elements['serviceOrder.registrationNumber'].value="";
	document.forms['distributionRevenueListForm'].elements['serviceOrder.lastName'].value= ""
	document.forms['distributionRevenueListForm'].elements['serviceOrder.firstName'].value= "";
}
try{
	document.forms['distributionRevenueListForm'].elements['accID'].value= '';
	document.forms['distributionRevenueListForm'].elements['fileNo'].value='';
	document.forms['distributionRevenueListForm'].elements['venderDetails'].value='';
	document.forms['distributionRevenueListForm'].elements['fileNovenderDetails'].value='';
	document.forms['distributionRevenueListForm'].elements['venderDetailsList'].value='';
	
	//document.forms['distributionRevenueListForm'].elements['removeAccID'].value='';
	//document.forms['distributionRevenueListForm'].elements['removeFileID'].value='';
}catch(e){}
</script>

  