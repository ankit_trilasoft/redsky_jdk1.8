/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "TrackingStatus" object in Redsky that allows for TrackingStatus  of Shipment.
 * @Class Name	TrackingStatusAction
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */
package com.trilasoft.app.webapp.action;
import org.apache.log4j.Logger; 
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet; 
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse; 
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext; 
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.TrackingStatusDaoHibernate.DTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.AdAddressesDetails;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.ConsigneeInstruction;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsFamilyDetails;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.InventoryPath;
import com.trilasoft.app.model.Loss;
import com.trilasoft.app.model.NetworkControl;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.RemovalRelocationService;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.Storage;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.UgwwActionTracker;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AccessInfoManager;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.AdAddressesDetailsManager;
import com.trilasoft.app.service.AddressManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CartonManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.ConsigneeInstructionManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.service.CustomManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DataSecuritySetManager;
import com.trilasoft.app.service.DsFamilyDetailsManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.InventoryDataManager;
import com.trilasoft.app.service.InventoryPathManager;
import com.trilasoft.app.service.ItemDataManager;
import com.trilasoft.app.service.LossManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.NetworkControlManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RemovalRelocationServiceManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.StorageManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.UgwwActionTrackerManager;
import com.trilasoft.app.service.VehicleManager;
import com.trilasoft.app.service.WorkTicketManager; 
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text; 
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.trilasoft.app.model.Company;
import com.trilasoft.app.service.CompanyManager;
public class TrackingStatusAction extends BaseAction implements Preparable{
	private  String accountInterface; 
	private  Map<String, String> yesno;
    private  Map<String, String> tan;
    private  Map<String, String> omni;
    private  Map<String, String> demurrag;
    private  Map<String, String> nontemp;
    private  Map<String, String> clearat;
	private  Map<String, String> partnerType;
	private  Map<String, String> SITOUTTA;
	private  Map<String, String> ENTRYFORM;
	private Map<String, String> EQUIP;
	private Map<String, String> military;
	private Map<String, String> sitOriginReason;
	private Map<String, String> sitDestinationReason;
	private Map<String, String> flagCarrierList;
	private  Map<String,String>euVatPercentList;
	private  Map<String,String>payVatPercentList;
	private  Map<String,String>UTSIeuVatPercentList;
	private  Map<String,String>UTSIpayVatList;
    private  Map<String,String>UTSIpayVatPercentList;
    private Map<String, String> reasonList;
	private MyFileManager myFileManager;
	private ContainerManager containerManager;
	private CartonManager cartonManager;
	private VehicleManager vehicleManager;
	private ConsigneeInstructionManager consigneeInstructionManager;
	private AdAddressesDetailsManager adAddressesDetailsManager;
	private AdAddressesDetails adAddressesDetails;
	private DsFamilyDetailsManager dsFamilyDetailsManager;
	private DsFamilyDetails dsFamilyDetails;
	private PartnerManager partnerManager;
	private  ExchangeRateManager exchangeRateManager;
	private UserManager userManager;
	private UgwwActionTrackerManager ugwwActionTrackerManager;
	private User user;
	private Boolean costElementFlag;
	private List accountLineList;
	private CustomerFileAction customerFileAction;
    private List originAgentListTrakingStatus;
    private List destinationAgentListTrakingStatus;
    private List originAgentCodeListTrakingStatus;
    private List destinationAgentCodeListTrakingStatus;
    private List agentAddress;
    private List trackingStatuss;  
    private List bookerList;
    private List originAgentList;
    private List destinationAgentList;
    private List originAgentCodeList;
    private List destinationAgentCodeList;
    private List refMasters;
	private List workTickets;
	private List servicePartnerlist;
	private List maxLineNumber;
	private List findSalesStatus;
	private List customerSO;
	private List vanLineCodeList;
	private List companyCodeList;
	private List bookingAgentList;
	private List weightunits;
    private List volumeunits;
    private List servicePartnerss;
    private List statusNumberList=new ArrayList();
    private List jobstatusNumberList=new ArrayList();
    private List statusNumberCountList=new ArrayList();
    private List userContactList = new ArrayList();
	private Set storages;
	private Set servicePartners;
	private Set accountLines;
	private String netWorkControlValues;
    private Long id;
    private Long maxTrac;
	private Long maxMisc;
	private long tempID;
	private Long autoLineNumber;
	private Date etDepart;
	private Date beginDate;
	private Date endDate;
	private String countStorage;
	private String sessionCorpID;
	private String corpIDform;
	private String usertype;
	private String booker;
	private String originAgent;
	private String destinationAgent;
	private String dateStatus;
	private String countSurveyNotes;
	private String countOriginDetailNotes;
    private String countDestinationDetailNotes;
    private String countWeightDetailNotes;
    private String countOriginPackoutNotes;
    private String countSitOriginNotes;
    private String countOriginForwardingNotes;
    private String countGSTForwardingNotes;
    private String countDestinationImportNotes;
    private String countDestinationStatusNotes;
	private String countSitDestinationNotes;
	private String countInterStateNotes;
	private String countServiceOrderNotes;
	private String countNetworkAgentNotes;
	private String countBookingAgentNotes;
	private String countOriginAgentNotes;
	private String countDestinAgentNotes;
	private String countSubOriginAgentNotes;
	private String countSubDestinAgentNotes;
	private String countBrokerAgentNotes;
	private String countForwarderAgentNotes;
	private String shipNumber;
	private String weightType;
	private String accountLineNumber;
	private String agentStatusOrigin = ""; 
	private String agentStatusDestination = "";
	private String billingAddress1;   
	private String billingAddress2;
	private String billingAddress3;
	private String billingAddress4;
	private String billingCity;
	private String billingState;
	private String billingZip;
	private String billingCountry;
	private String billingFax;
	private String billingPhone;
	private String billingTelex;
	private String billingEmail;
	private String partnerCode;
	private String shipNumberA;
	private String gotoPageString;
	private String shipSize;
	private String shipNm;
	private String minShip;
	private String countShip;
	private String agentType;
	private String agentCodeName;
	private String validateFormNav;
	private String companyCode;
	private String bookingAgentCode;
	private String componentId;
	private Boolean accessAllowed;
	private String baseCurrencyCompanyDivision="";	
	private String baseCurrency;
	/* Added By Kunal for ticket number: 6838 */
	private String userName;
	private String userEmailId;
	private String systemDefaultVatCalculation;
	private WorkTicket workTicket;
	private CustomerFile customerFile;
	private AccountLine accountLine;
	private AccountLine accountLine1;
	private Storage storage;
    private TrackingStatus trackingStatus;   
    private TrackingStatus trackingStatusToRecod;
    private Miscellaneous miscellaneous;
    private Billing billing;
    private Billing billingRecods;
    private MiscellaneousManager miscellaneousManager;
    private  IntegrationLogInfoManager  integrationLogInfoManager;
    private AddressManager addressManager;
    private RefMasterManager refMasterManager;
    private TrackingStatusManager trackingStatusManager;
    private ServiceOrderManager serviceOrderManager;
    private ServiceOrder serviceOrder;
    private ServiceOrder serviceOrderToRecods;
    private ServicePartnerManager servicePartnerManager;
    private ToDoRuleManager toDoRuleManager;
    private AccountLineManager 	accountLineManager;
    private ServicePartner servicePartner;
	private StorageManager storageManager;
	private WorkTicketManager  workTicketManager;
	private CustomerFileManager customerFileManager;
	private NotesManager notesManager;
	private BillingManager billingManager;
	private CompanyDivisionManager companyDivisionManager;
	private Company company; 
	private RemovalRelocationService removalRelocationService;
	private RemovalRelocationServiceManager removalRelocationServiceManager;
	private CompanyManager companyManager;
	private ClaimManager claimManager;
	private LossManager lossManager;
	private Boolean isNetworkBookingAgent=false;
	private Boolean isNetworkDestinationAgent=false;
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    StringBuilder systemDate = new StringBuilder(format.format(new Date()));
    private Boolean isNetwork;
    private Boolean isNetworkOriginAgent;
	private Boolean isNetworkOriginSubAgent;
	private Boolean isNetworkDestinationSubAgent;
	private Boolean isNetworkOrginAgentSection;
	private Boolean isNetworkDestinationAgentSection;
	private Boolean isNetworkNetworkPartnerCode;
	private Boolean isNetworkNetworkPartnerCodeSection;
	private Boolean isNetworkBookingAgentCode;
	private Boolean isNetworkBookingAgentCodeSection;
	private boolean billingContractType;
	private String invoicedLinkedAccountLines;
	private String contractTypeTS;
	private String linkedshipnumberTS;
	private String isRedSky;
    private List customs;
    private CustomManager customManager;
    private String fileNumber;
    private String hitFlag;
    Date currentdate = new Date();
    DecimalFormat decimalFormat = new DecimalFormat("#.####");
    private boolean networkAgent;
    private String disableChk;
    private Boolean agentSearchValidation;
    private DataSecuritySetManager dataSecuritySetManager;
    private String agentRole;
    private String originAgentCode;
	private String destinationAgentCode;
	private String originSubAgentCode;
	private String destinationSubAgentCode;
	private int autoIdNumber1;
	private String notesSubType;
	private String checkAgent;
	private String bookingAgentFlag="";
	private String networkAgentFlag="";
	private String originAgentFlag="";
	private String subOriginAgentFlag="";
	private String destAgentFlag="";
	private String subDestAgentFlag="";
	private Boolean usaFlag;
	private String billToCodeAcc;
	private List allBookingAgentList;
	private List userDetailList;
	private SystemDefault systemDefault;
	private Boolean defaultVendorAccLine = false;
	private String checkOption;
	private String hasPortalUser;
	private Map<String,LinkedHashMap<String, String>> parameterMap = new HashMap<String, LinkedHashMap<String,String>>();
	private Map<String,String> localParameter;
    static final Logger logger = Logger.getLogger(TrackingStatusAction.class);	
    private String voxmeIntergartionFlag;
    private String partCode;
    private String contactName;
    private String conEmail;
    private String isRedSkyDestinAg;
    private String isRedSkySubOrigAg ;
    private String isRedSkySubDestinAg;
    private String isAgentPortal;
    private String isDestinAgentPortal;
    private String isSubOrigAgentPortal;
    private String isSubDestinAgentPortal;
    private boolean accountLineAccountPortalFlag=false; 
    private AccessInfoManager accessInfoManager;
    private String accountLineBillingParam;
    private ErrorLogManager errorLogManager;
    private SystemDefaultManager systemDefaultManager;
    private InventoryPathManager inventoryPathManager;
    private InventoryDataManager inventoryDataManager;   
    private String checkOldCf = "N";
    private String isDecaExtract;
    //private String isSOExtract;
    private String updaterIntegration;
    private String networkVendorCode;
    private ServiceOrder agentServiceOrderCombo;
	private String corpId;
	private Boolean surveyTab = new Boolean(false);
	private String shipNo;
	private List soList;
	private int serviceOrdersSize;
    private Long cid;
    private TrackingStatus copyAgentDetail;
    private Long soGetId;
    private Long soSetId;
    private String type;
    private String oADAAgentCheck;
    private String seqNm;
    private Long soId;
    private String systemDefaultReciprocityJobType;
	private Map<String,String> comDivCodePerCorpIdMap;
	private Map<String,String> containerMap;
	private  List containerNumberList;
    private String coordinatorEvalValidation="";
    private List preferredList;
    private List handOutList;
    private ItemDataManager itemDataManager;
    private List CMMDMMAgentList=new ArrayList();
    private Set linkedshipNumber=new HashSet();
    public  Map<String, String> jobtype=new HashMap<String, String>();
    private String jobTypes;
    private String systemDefaultstorage;
    private boolean checkFieldVisibility=false;
    private String crewArrivalTime;
	private String oiJobList;
	private List<SystemDefault> sysDefaultDetail;
	private String completeDateFlag;
	private String invoiceCount;
	private boolean checkAccessQuotation=false;
	private String moveType;
	private String msgClicked;
	private String linkUpType;
	public String buttonType;
	private Date packA;
	private Date loadA;
	private Date deliveryA;
	private Date actualPackBegin;
	private Date actualLoadBegin;
	private Date actualDeliveryBegin;
	private String divisionFlag;
	private String autoPopulateActualDeliveryDateFlag;
	private EmailSetupManager emailSetupManager;
	private String allIdOfMyFile;
	private String myFileFieldName;
	private String moveTypePartner;
	private NetworkControlManager networkControlManager;
	public String dataNetwork;
	private List maxShip;
	private String ship;
	private Long autoShip;
	private Long prevShip;
	private String shipPrev;
	private String shipnumber;
	private ServiceOrder serviceOrderPrev;
	private List maxSequenceNumber;
	private Long autoSequenceNumber;
	private String seqNum;
	private List checkSequenceNumberList;
	private String linkUp;
	private Long trackingStatusId;
	private String serviceType;
	private List pricingBillingPaybaleList;
	private String networkPartnerCode;
	private String respectiveNetworkAgent;
	private String parentShipNumber;
	List venderDetails=new ArrayList();
	private String originalCorpID;
	private String vatBillingGroup;
	private String creditTerms;
	private String paymentMethod;
	private String extReference;
	private String agentParentName;
	private String agentParentCode;
	private String vatNumber;
	private boolean flagForPartnerAddress = false;
	private Map<String, String> valuesFromParameters;
	private Map<String, String> relocationServices;
	private Map<String, String> locationTypes;
	private Map<String,String> excessWeightBillingList;
	private Map<String, String> service;
	private Map<String, String> notestatus;
	private Map<String, String> notetype;
	private Map<String, String> notesubtype;
	private Notes notes;
	private String seqNumber;
	private String shpNumber;
	private String linkUpButtonType;
	private List documentListCF;
	private String fieldValue;
	private List documentListSO;
	private String documentIdListCF;
	private String documentIdListSO;
	public String cfCoordinatorname;
	public String soCoordinatorname;
	public String returnAjaxStringValue;
	private String fieldName;
	private String soBillToCode;
	private String defaultContactInfo;
	private String originAgentInfo;
	private String supplementGBL;
	private String  usGovJobs;
	private String dashBoardHideJobsList;
	public TrackingStatusAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        this.usertype=user.getUserType();
	}
	public void prepare() throws Exception {
		try
		{
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){  
		getComboList(sessionCorpID);
		 try {
			 if((usertype.trim().equalsIgnoreCase("AGENT"))){ 
			 id = new Long(getRequest().getParameter("id").toString());
			 if(id!=null){
				 agentServiceOrderCombo = serviceOrderManager.getForOtherCorpid(id);
				 corpId=agentServiceOrderCombo.getCorpID();
				 TrackingStatus agentTS = trackingStatusManager.getForOtherCorpid(id);
				 boolean chkCompanySurveyFlag = companyManager.getCompanySurveyFlag(agentServiceOrderCombo.getCorpID()); 
				 if(agentServiceOrderCombo.getIsNetworkRecord() && (agentTS.getSoNetworkGroup() || chkCompanySurveyFlag)){
						surveyTab= true;
				}
				}
			 }
		 } catch (Exception e) {
			e.printStackTrace();
		 }
		getRequest().setAttribute("soLastName","");
		//accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();	
		company=companyManager.findByCorpID(sessionCorpID).get(0);

         if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }
         if (company != null && company.getDashBoardHideJobs() != null) {
				dashBoardHideJobsList = company.getDashBoardHideJobs();	 
			}
		 if(company!=null){
			 if(company.getUTSI()!=null){
				 networkAgent=company.getUTSI();
				}else{
					networkAgent=false;
				}
			 if(company.getVoxmeIntegration()!=null){
					voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
				}
			 defaultVendorAccLine=false;
			 if(company.getFlagDefaultVendorAccountLines()!=null){
				 defaultVendorAccLine=company.getFlagDefaultVendorAccountLines();
				}
			}
		 if(networkAgent){
			 CMMDMMAgentList=companyDivisionManager.findCMMDMMAgentList(); 
		 }
		 sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		 if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault1 : sysDefaultDetail) {
					systemDefault=systemDefault1;
					costElementFlag=false;
					if(systemDefault.getCostElement()){
					costElementFlag=systemDefault.getCostElement();
					}
					systemDefaultVatCalculation="false";
					if(systemDefault.getVatCalculation()){
						systemDefaultVatCalculation="true";	
					}
					if(systemDefault.getAccountLineAccountPortalFlag() !=null ){
						accountLineAccountPortalFlag = 	systemDefault.getAccountLineAccountPortalFlag();
					}
					if(systemDefault.getBaseCurrency() !=null ){
						baseCurrency=systemDefault.getBaseCurrency();
						}
					if(systemDefault.getReciprocityjobtype() !=null ){
						systemDefaultReciprocityJobType=systemDefault.getReciprocityjobtype();
						}
					if(systemDefault.getStorage() !=null ){
						systemDefaultstorage =systemDefault.getStorage();
						}
				      if(systemDefault.getAgentSearchValidation() !=null && systemDefault.getAgentSearchValidation()==true){
			            	agentSearchValidation=true;
				      }
				      if(systemDefault.getUsGovJobs() !=null ){
				    	  usGovJobs=systemDefault.getUsGovJobs();
				      }
				      if(systemDefault.getAccountingInterface()!=null){
							accountInterface =systemDefault.getAccountingInterface();
						}
					
				}
		 }
		 payVatPercentList = refMasterManager.findVatPercentList(sessionCorpID, "PAYVATDESC");
		 allBookingAgentList = trackingStatusManager.getAllBookingAgentList(sessionCorpID);		 
		 comDivCodePerCorpIdMap = companyDivisionManager.getComDivCodePerCorpIdList();
		}
	} catch (Exception e) {
		e.printStackTrace();
	 }
	}
	public String getAllInvoicedLinkedAccountLines(){

		try {
			invoicedLinkedAccountLines=trackingStatusManager.checkAllInvoicedLinkedAccountLines(shipNumber,linkedshipnumberTS,contractTypeTS);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		 return SUCCESS;
	
	}
	public String findChkPartnerRUC(){
		try {
			isRedSky=trackingStatusManager.checkIsRedSkyUser(partCode);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		 return SUCCESS;
	}
	public String chkPartnerAgentRUC(){
		try
		{
		if(trackingStatusManager.getAgentsExistInRedSky(partCode,sessionCorpID,contactName,conEmail)){
			isRedSky="AG";
		}else{
			isRedSky="";	
		}}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		 return SUCCESS;
	}
	
	
//	 Method for editing trackingStatus record calling from main menu >>  serviceOrder list & navigation from other tabs of serviceorder.
	public String edit() {
		try
		{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if((usertype.trim().equalsIgnoreCase("AGENT"))|| (usertype.trim().equalsIgnoreCase("PARTNER")) || (usertype.trim().equalsIgnoreCase("ACCOUNT"))){
		getComboList(corpIDform);
		}
		if (id != null){  
		serviceOrder=serviceOrderManager.get(id);
		if(serviceOrder != null && serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().trim().equals("")))){
			userDetailList=userManager.findUserDetail(serviceOrder.getCoordinator(),sessionCorpID);
		}
		Map<String,String> serviceMap = refMasterManager.findByProperty(sessionCorpID, "SERVICE",serviceOrder.getServiceType());
		if(serviceMap != null && serviceMap.size() > 0){
			String name = serviceMap.get(serviceOrder.getServiceType());
			if(name != null && !"".equals(name)){
				completeDateFlag = "YES";
			}else{
				completeDateFlag = "";
			}
		}
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		customerFile = serviceOrder.getCustomerFile();
		serviceOrdersSize=customerFile.getServiceOrders().size();
		moveType = customerFile.getMoveType();
		checkAccessQuotation=company.getAccessQuotationFromCustomerFile();
		if ((billingManager.checkById(id)).isEmpty())
		{
			billing = new Billing();
			if(serviceOrder !=null && serviceOrder.getJob()!=null && systemDefaultstorage!=null && systemDefaultstorage.contains(serviceOrder.getJob())){
				try{
					String permKey = sessionCorpID +"-"+"component.field.Alternative.historicalContracts";
					checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
						if(checkFieldVisibility){
							billing.setHistoricalContracts(true);	
						}
					}catch(Exception e){
						e.printStackTrace();
					}	
			}
			billing.setShipNumber(serviceOrder.getShipNumber());
    		billing.setSequenceNumber(serviceOrder.getSequenceNumber());
    		billing.setCreatedOn(serviceOrder.getCreatedOn());
    	   	billing.setUpdatedOn(serviceOrder.getUpdatedOn());
    	   	billing.setCreatedBy(serviceOrder.getCreatedBy());
    	   	billing.setUpdatedBy(serviceOrder.getUpdatedBy());
    	 	billing.setCorpID(sessionCorpID);
    	 	billing.setBillingInsurancePayableCurrency(baseCurrency);
        	billing.setBillToCode(serviceOrder.getCustomerFile().getBillToCode());
        	billing.setBillToName(serviceOrder.getCustomerFile().getBillToName()); 
        	billing.setNetworkBillToCode(serviceOrder.getCustomerFile().getAccountCode());
        	billing.setNetworkBillToName(serviceOrder.getCustomerFile().getAccountName()); 
        	int i=customerFile.getServiceOrders().size();
        	if(i==1){
        	billing.setBillToAuthority(serviceOrder.getCustomerFile().getBillToAuthorization()); 
        	}else if(i>1) {
        	int j=	serviceOrderManager.getDoNotCopyAuthorizationSO(serviceOrder.getBillToCode(),sessionCorpID);
        	if(j==0){
        		billing.setBillToAuthority(serviceOrder.getCustomerFile().getBillToAuthorization()); 	
        	}
        	}
        	billing.setBillToReference(serviceOrder.getCustomerFile().getBillToReference());
        	billing.setBillingId(serviceOrder.getCustomerFile().getBillToCode());
        	billing.setBillName(serviceOrder.getCustomerFile().getBillToName());
		  	try{
        	List contractDiscount=billingManager.priceContract(serviceOrder.getCustomerFile().getContract());
    		String[] str1 = contractDiscount.get(0).toString().split("#");
    		String billingInstructionCodeWithDesc=str1[6];
    		billing.setBillingInstructionCodeWithDesc(billingInstructionCodeWithDesc);
    		String[] str2 = str1[6].split(":");
    		String billingInstruction =str2[1];
    		billing.setBillingInstruction(billingInstruction);
        	}catch (Exception e) {
        		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
        	billing.setBillTo1Point(serviceOrder.getCustomerFile().getBillPayMethod());
        	billing.setContract(serviceOrder.getCustomerFile().getContract());
        	billing.setPersonBilling(serviceOrder.getCustomerFile().getPersonBilling());
        	billing.setPersonPayable(serviceOrder.getCustomerFile().getPersonPayable());
        	billing.setPersonPricing(serviceOrder.getCustomerFile().getPersonPricing());
        	billing.setAuditor(serviceOrder.getCustomerFile().getAuditor());
        	billing.setBillTo2Code("");
        	billing.setPrivatePartyBillingCode("");
        	billing.setNoCharge(false);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) 
			{
					for (SystemDefault systemDefault : sysDefaultDetail) 
					{
						billing.setCurrency(systemDefault.getBaseCurrency());
					}
			}
        	billing.setServiceOrder(serviceOrder);
        	billing.setId(serviceOrder.getId());
			billing.setUpdatedOn(new Date());
			billing.setBillToAuthority(serviceOrder.getCustomerFile().getBillToAuthorization());
			billing.setBillToReference(serviceOrder.getCustomerFile().getBillToReference());
			billingManager.save(billing); 
		} else {
			billing = billingManager.get(id);
		}
		billingContractType=trackingStatusManager.getContractType(sessionCorpID ,billing.getContract());
		if((miscellaneousManager.checkById(id)).isEmpty())
		{				
			miscellaneous = new Miscellaneous();
			miscellaneous.setId(serviceOrder.getId());
			miscellaneous.setShipNumber(serviceOrder.getShipNumber());
			miscellaneous.setSequenceNumber(serviceOrder.getSequenceNumber());
			miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
			miscellaneous.setUpdatedOn(serviceOrder.getUpdatedOn());
			miscellaneous.setCreatedBy(serviceOrder.getCreatedBy());
			miscellaneous.setUpdatedBy(serviceOrder.getUpdatedBy());
			miscellaneous.setCorpID(sessionCorpID);
			miscellaneous.setEstimateTareWeight(new BigDecimal(0));
			miscellaneous.setActualTareWeight(new BigDecimal(0));
			miscellaneous.setRwghTare(new BigDecimal(0));
			miscellaneous.setChargeableTareWeight(new BigDecimal(0));
			miscellaneous.setEntitleTareWeight(new BigDecimal(0));
			
			miscellaneous.setEstimateTareWeightKilo(new BigDecimal(0));
			miscellaneous.setActualTareWeightKilo(new BigDecimal(0));
			miscellaneous.setRwghTareKilo(new BigDecimal(0));
			miscellaneous.setChargeableTareWeightKilo(new BigDecimal(0));
			miscellaneous.setEntitleTareWeightKilo(new BigDecimal(0));
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					miscellaneous.setUnit1(systemDefault.getWeightUnit());
					miscellaneous.setUnit2(systemDefault.getVolumeUnit());
					serviceOrder.setDistanceInKmMiles(systemDefault.getDefaultDistanceCalc());
				}
			}
		}else{
			miscellaneous = miscellaneousManager.get(id);
		}		
	    if ((sysDefaultDetail != null && !sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
	            if(systemDefault.getUnitVariable() !=null && systemDefault.getUnitVariable().equalsIgnoreCase("Yes"))
	            	disableChk ="Y";
	            else
	            	disableChk ="N";
			}
		}		
		if ((trackingStatusManager.checkById(id)).isEmpty()){
			trackingStatus = new TrackingStatus();
			trackingStatus.setId(serviceOrder.getId());
            trackingStatus.setCorpID(serviceOrder.getCorpID());
            trackingStatus.setSitOriginDays(0);
            trackingStatus.setSitDestinationDays(0);
            trackingStatus.setFromWH(0);
            trackingStatus.setDemurrageCostPerDay2(0);
            trackingStatus.setUsdaCost(0);
            trackingStatus.setInspectorCost(0);
            trackingStatus.setXrayCost(0);
            trackingStatus.setPerdiumCostPerDay(0);
            trackingStatus.setDemurrageCostPerDay1(0);
            trackingStatus.setSequenceNumber(serviceOrder.getSequenceNumber());
            trackingStatus.setShipNumber(serviceOrder.getShipNumber());
            trackingStatus.setStatus(serviceOrder.getCustomerFile().getStatus());
        	trackingStatus.setCreatedOn(serviceOrder.getCreatedOn());
        	trackingStatus.setUpdatedOn(serviceOrder.getUpdatedOn());
        	trackingStatus.setCreatedBy(serviceOrder.getCreatedBy());
        	trackingStatus.setUpdatedBy(serviceOrder.getUpdatedBy());
        	trackingStatus.setStatusDate(serviceOrder.getCustomerFile().getStatusDate());
        	trackingStatus.setSurvey(serviceOrder.getCustomerFile().getSurvey());
        	trackingStatus.setInitialContact(serviceOrder.getCustomerFile().getInitialContactDate());
        	trackingStatus.setServiceOrder(serviceOrder);
            trackingStatus.setMiscellaneous(miscellaneous);
            trackingStatus.setSurveyDate(serviceOrder.getCustomerFile().getSurvey());
            trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
            trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
            trackingStatus.setCutOffTimeTo("00:00");
            trackingStatus.setDocCutOffTimeTo("00:00");

            trackingStatusManager.save(trackingStatus);
			miscellaneous = trackingStatus.getMiscellaneous();
		}else {
			trackingStatus = trackingStatusManager.get(id);
			if(trackingStatus.getOriginAgentCode()!=null && trackingStatus.getOriginAgentContact()!=null && trackingStatus.getOriginAgentEmail()!=null){
				isRedSky=trackingStatusManager.checkIsRedSkyUser(trackingStatus.getOriginAgentCode());
				if(trackingStatusManager.getAgentsExistInRedSky(trackingStatus.getOriginAgentCode(),sessionCorpID,trackingStatus.getOriginAgentContact(),trackingStatus.getOriginAgentEmail())){
				 isAgentPortal="AG" ;
				}
			}
			if(trackingStatus.getDestinationAgentCode()!=null && trackingStatus.getDestinationAgentContact()!=null && trackingStatus.getDestinationAgentEmail()!=null){
				isRedSkyDestinAg=trackingStatusManager.checkIsRedSkyUser(trackingStatus.getDestinationAgentCode());			
				if(trackingStatusManager.getAgentsExistInRedSky(trackingStatus.getDestinationAgentCode(),sessionCorpID,trackingStatus.getDestinationAgentContact(),trackingStatus.getDestinationAgentEmail())){
					 isDestinAgentPortal="AG" ;
					}
			}
			if(trackingStatus.getOriginSubAgentCode()!=null && trackingStatus.getSubOriginAgentContact()!=null && trackingStatus.getSubOriginAgentEmail()!=null){
				isRedSkySubOrigAg=trackingStatusManager.checkIsRedSkyUser(trackingStatus.getOriginSubAgentCode());			
				if(trackingStatusManager.getAgentsExistInRedSky(trackingStatus.getOriginSubAgentCode(),sessionCorpID,trackingStatus.getSubOriginAgentContact(),trackingStatus.getSubOriginAgentEmail())){
					 isSubOrigAgentPortal="AG" ;
					}
			}
			if(trackingStatus.getDestinationSubAgentCode()!=null && trackingStatus.getSubDestinationAgentAgentContact()!=null && trackingStatus.getSubDestinationAgentEmail()!=null){
				isRedSkySubDestinAg=trackingStatusManager.checkIsRedSkyUser(trackingStatus.getDestinationSubAgentCode());			
				if(trackingStatusManager.getAgentsExistInRedSky(trackingStatus.getDestinationSubAgentCode(),sessionCorpID,trackingStatus.getSubDestinationAgentAgentContact(),trackingStatus.getSubDestinationAgentEmail())){
					 isSubDestinAgentPortal="AG" ;
					}
			}
			miscellaneous = trackingStatus.getMiscellaneous();
		if(miscellaneous!=null){
		if(miscellaneous.getUnit1().equalsIgnoreCase("Lbs")){
			weightType="lbscft";
		}
		else{
			weightType="kgscbm";
		}
		}
		customs = customManager.findBySO(serviceOrder.getId(),sessionCorpID);
		servicePartnerss = new ArrayList(servicePartnerManager.findByCarriersID(serviceOrder.getId()));
		storages = new LinkedHashSet(storageManager.findStorageByShipNumber(serviceOrder.getShipNumber()));
		handOutList=itemDataManager.getAllHandOutList(serviceOrder.getShipNumber());
		bookerList = trackingStatusManager.findByBooker(serviceOrder.getId());
			if(bookerList.isEmpty()){
			trackingStatus.setBooker("");
			}else{
			booker = bookerList.get(0).toString();
			trackingStatus.setBooker(booker);
			}
			if(trackingStatus.getCutOffTimeTo()==null){
				trackingStatus.setCutOffTimeTo("00:00");
			}
			if(trackingStatus.getSurveyTimeFrom()==null){
				trackingStatus.setSurveyTimeFrom("00:00");
			}
			if(trackingStatus.getSurveyTimeTo()==null){
				trackingStatus.setSurveyTimeTo("00:00");
			}
			if(trackingStatus.getDocCutOffTimeTo()==null){
				trackingStatus.setDocCutOffTimeTo("00:00");
			}
			workTickets=serviceOrderManager.getWorkTicketList(serviceOrder.getId());
			if(!(trackingStatus.getBeginLoad() == null)){
			Date BeginLoad = trackingStatus.getBeginLoad();
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(BeginLoad);
			cal2.add(Calendar.DAY_OF_YEAR, 1);
			Date packRoomA = cal2.getTime();
			if(!(trackingStatus.getPackRoomA()== null) && (!(trackingStatus.getPackRoomA()==packRoomA))){
				trackingStatus.setPackRoomA(trackingStatus.getPackRoomA());
			}else{
				trackingStatus.setPackRoomA(packRoomA);
				}
			}
		}
		if (serviceOrder.getMode() !=null && (serviceOrder.getMode().equalsIgnoreCase("Air") ||serviceOrder.getMode().equalsIgnoreCase("Sea"))){
	       	   usaFlag=serviceOrderManager.getUsaFlagCarrier(serviceOrder.getBillToCode(),sessionCorpID);
	       	   if(usaFlag && (trackingStatus.getFlagCarrier()==null || trackingStatus.getFlagCarrier().equals(""))){
	       		   trackingStatus.setFlagCarrier("USA");
	       	   }
	          }
		List invoiceCounttemp=serviceOrderManager.countByGeneratedInvoice(serviceOrder.getShipNumber(),sessionCorpID);
		if(invoiceCounttemp!=null && invoiceCounttemp.get(0)!=null)
    		invoiceCount =	invoiceCounttemp.get(0).toString();
			try{
				containerNumberList=cartonManager.getContainerNumber(serviceOrder.getShipNumber());
				if (containerNumberList == null) {
					containerNumberList = new ArrayList(); 
				}
				}
				catch(NullPointerException np)
				{ 
				  np.printStackTrace();
				  logger.error("Exception Occour: "+ np.getStackTrace()[0]);
				  String logMessage =  "Exception:"+ np.toString()+" at #"+np.getStackTrace()[0].toString();
      	    	  String logMethod =   np.getStackTrace()[0].getMethodName().toString();
      	    	 
				}
				catch(Exception exc){
					  exc.printStackTrace();
					  logger.error("Exception Occour: "+exc.getStackTrace()[0]);
					  String logMessage =  "Exception:"+ exc.toString()+" at #"+exc.getStackTrace()[0].toString();
	      	    	  String logMethod =   exc.getStackTrace()[0].getMethodName().toString();
				}
	}
		if(defaultVendorAccLine == true){
			Map<String,String> oADADetailMap  = trackingStatusManager.findByAgent(serviceOrder.getShipNumber(),sessionCorpID);
			if(oADADetailMap != null && oADADetailMap.size() > 0){
				oADAAgentCheck=(oADADetailMap.get("Origin")==null?"F":oADADetailMap.get("Origin").split("~")[0])+"~"+(oADADetailMap.get("Destin")==null?"F":oADADetailMap.get("Destin").split("~")[0]);
				}
			    agentStatusOrigin = "editable";
			    agentStatusDestination = "editable";
			}	
		
		shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		getStorageForIconChange();
		getNotesForIconChange();
		try{
		 isNetwork=false;
		if(serviceOrder.getIsNetworkRecord()==null){
			isNetwork=false;
		}
		else if(serviceOrder.getIsNetworkRecord()){
			isNetwork=true;
		}
		
		isNetworkBookingAgent=getIsNetworkBookingAgent(sessionCorpID, serviceOrder.getBookingAgentCode());
		isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getDestinationAgentCode() );
		isNetworkOriginAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getOriginAgentCode() );
		isNetworkNetworkPartnerCode=getIsNetworkAgent(sessionCorpID,trackingStatus.getNetworkPartnerCode() );
		isNetworkBookingAgentCode=getIsNetworkAgent(sessionCorpID,serviceOrder.getBookingAgentCode() );
		
		if(isNetwork){
			isNetworkOrginAgentSection=false;
			isNetworkDestinationAgentSection=false;
			isNetworkNetworkPartnerCodeSection=false;
			isNetworkBookingAgentCodeSection=false;
			if(trackingStatus.getOriginAgentExSO()!=null && !trackingStatus.getOriginAgentExSO().equals("") && serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("")){
				List list = serviceOrderManager.findBookingAgentShipNumber(trackingStatus.getOriginAgentExSO());
			    Iterator itr = list.iterator();
			    while(itr.hasNext()){
			    	ServiceOrder newSO = (ServiceOrder)itr.next();
					if (newSO.getBookingAgentShipNumber() != null && !newSO.getBookingAgentShipNumber().equals("") && !newSO.getBookingAgentShipNumber().equals(trackingStatus.getShipNumber())) {
						isNetworkOrginAgentSection=true;
					}
				}
			}
			if((trackingStatus.getDestinationAgentExSO()==null ||  trackingStatus.getDestinationAgentExSO().equals("")) || (serviceOrder.getBookingAgentShipNumber()!=null && (!serviceOrder.getBookingAgentShipNumber().equals("")))){
				List list = serviceOrderManager.findBookingAgentShipNumber(trackingStatus.getDestinationAgentExSO());
			    Iterator itr = list.iterator();
			    while(itr.hasNext()){
			    	ServiceOrder newSO = (ServiceOrder)itr.next();
					if (newSO.getBookingAgentShipNumber() != null && !newSO.getBookingAgentShipNumber().equals("") && !newSO.getBookingAgentShipNumber().equals(trackingStatus.getShipNumber())) {
						isNetworkDestinationAgentSection=true;
					}
				}							
			}
			if((serviceOrder.getBookingAgentShipNumber()!=null && (!serviceOrder.getBookingAgentShipNumber().equals("")))){
				isNetworkNetworkPartnerCodeSection=checkNetworkSection(serviceOrder.getBookingAgentShipNumber(),"NetworkPartnerCode");
			}
			
		}
		 if(trackingStatus.getCrewArrivalDate()!=null){
			 crewArrivalTime=trackingStatus.getCrewArrivalDate().toString().substring(11, 16);
	        }else{
	        	crewArrivalTime="00:00";
	        }
		}catch(Exception ex){
			logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
		}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		
		}catch(Exception e){
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		} 
		
		try {
/*			if(serviceOrder != null && serviceOrder.getId() != null){
				Map<String,String> serviceMap = refMasterManager.findByProperty(sessionCorpID, "SERVICE",serviceOrder.getServiceType());
				if(serviceMap != null && serviceMap.size() > 0){
					String name = serviceMap.get(serviceOrder.getServiceType());
					if(name != null && !"".equals(name)){
						name = name.replace(".",",");
						String[] st = name.split(",");
						Date date = serviceOrderManager.findByProperty(id, st[0],st[1],serviceOrder.getShipNumber());
						if(date!=null)trackingStatus.setServiceCompleteDate(date);
					}
				}
			}*/
			if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatus.getSoNetworkGroup()!=null && trackingStatus.getSoNetworkGroup()){
				if((serviceOrder.getBookingAgentShipNumber()!=null) && (!serviceOrder.getBookingAgentShipNumber().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
					linkedshipNumber.add(serviceOrder.getBookingAgentShipNumber());
				}
				if((trackingStatus.getBookingAgentExSO()!=null) && (!trackingStatus.getBookingAgentExSO().trim().equalsIgnoreCase("")) && (serviceOrder.getBookingAgentShipNumber()!=null) && (!serviceOrder.getBookingAgentShipNumber().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
					linkedshipNumber.add(trackingStatus.getBookingAgentExSO());
				}
				if((trackingStatus.getNetworkAgentExSO()!=null) && (!trackingStatus.getNetworkAgentExSO().trim().equalsIgnoreCase("")) && (serviceOrder.getBookingAgentShipNumber()!=null) && (!serviceOrder.getBookingAgentShipNumber().trim().equalsIgnoreCase("")) && (!serviceOrder.getShipNumber().trim().equalsIgnoreCase(serviceOrder.getBookingAgentShipNumber().trim()))){
					linkedshipNumber.add(trackingStatus.getNetworkAgentExSO());
				}	
				if(linkedshipNumber!=null && !linkedshipNumber.isEmpty() && linkedshipNumber.size()>0 && trackingStatus.getServiceCompleteDate()!=null){
					trackingStatusManager.updateLinkedShipNumberforCompletedDate(linkedshipNumber,trackingStatus.getServiceCompleteDate(),sessionCorpID,getRequest().getRemoteUser());
				}
			}
			
		} catch (Exception e) {
			 e.printStackTrace();
		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
    	 return "errorlog";
		}
		
		return SUCCESS;
	}
//	  End of Method.
  
	private Boolean checkNetworkSection(String bookingAgentShipNumber, String section) {
		try
		{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return trackingStatusManager.checkNetworkSection(bookingAgentShipNumber, section);
		}
		catch(Exception e)
		{
			e.printStackTrace();
            return false;
		}
	}
	private Boolean getIsNetworkAgent(String sessionCorpID,String destinationAgentCode) {
		try
		{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Boolean isNetworkAgent=trackingStatusManager.getIsNetworkAgent(sessionCorpID,destinationAgentCode);
	    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return isNetworkAgent;
		}
		catch(Exception e)
		{
			
			e.printStackTrace(); 
			return false;
		}
	}
	private Boolean getIsNetworkBookingAgent(String sessionCorpID,	String bookingAgentCode) {
		try
		{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Boolean isNetworkAgent=trackingStatusManager.getIsNetworkBookingAgent(sessionCorpID,bookingAgentCode);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return isNetworkAgent;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	@SkipValidation 
    public String customerOtherStatus(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			customerFile = customerFileManager.get(id);
			customerSO = new ArrayList(customerFile.getServiceOrders());
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
    	return SUCCESS;   
    }
	
	 @SkipValidation 
	public String checkNetworkPartnerTypeMethod(){
		 try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			 networkPartnerCode = toDoRuleManager.getNetworkAgentType(networkPartnerCode,sessionCorpID);  
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
		 return SUCCESS;   
	 }
	 
	
	 @SkipValidation 
	 public String chkAgentRoleTypeMethod(){
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 try {
			agentRole =  trackingStatusManager.getAgentTypeRoleForm(sessionCorpID,id,bookingAgentCode,networkPartnerCode,originAgentCode,destinationAgentCode,originSubAgentCode,destinationSubAgentCode);
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS;   
	  
	 }
	 @SkipValidation 
	 public String checkAgentAlreadyLinkUp(){
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 try {
			ServiceOrder so=serviceOrderManager.get(id);
			TrackingStatus ts=trackingStatusManager.get(id);
			String tempShip="";
			String tempCode="";
			agentRole="";
			if(linkUpType.equalsIgnoreCase("linkup_bookingAgent")){
				 tempShip=ts.getBookingAgentExSO();
				 tempCode=so.getBookingAgentCode();								
			}else if(linkUpType.equalsIgnoreCase("linkup_Network")){
				 tempShip=ts.getNetworkAgentExSO();
				 tempCode=ts.getNetworkPartnerCode();				
			}else if(linkUpType.equalsIgnoreCase("linkup_originAgentExSO")){
				 tempShip=ts.getOriginAgentExSO();
				 tempCode=ts.getOriginAgentCode();								
			}else if(linkUpType.equalsIgnoreCase("linkup_destinationAgentExSO")){
				 tempShip=ts.getDestinationAgentExSO();
				 tempCode=ts.getDestinationAgentCode();								
			}else if(linkUpType.equalsIgnoreCase("linkup_originSubAgentExSO")){
				 tempShip=ts.getOriginSubAgentExSO();
				 tempCode=ts.getOriginSubAgentCode();								
			}else if(linkUpType.equalsIgnoreCase("linkup_destinationSubAgentExSO")){
				 tempShip=ts.getDestinationSubAgentExSO();
				 tempCode=ts.getDestinationSubAgentCode();								
			}else{
			}
			if(tempShip!=null && !tempShip.equalsIgnoreCase("")){
				agentRole=tempShip+"~"+tempCode;
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS;   
	  
	 }	 
	 
	
//	 Method used to get etdDate from routing.
	    @SkipValidation 
		public String etdDate (){
	    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			try {
				serviceOrder=serviceOrderManager.get(id);
				SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder beginDates = new StringBuilder(formats.format(etDepart));
				servicePartnerlist=trackingStatusManager.findETD(serviceOrder.getShipNumber(), beginDates.toString());
			} catch (Exception e) {
				 e.printStackTrace();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 return "errorlog";
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
//		  End of Method.
//		 Method used to save the trackingStatus calling from trackingStatus form save button.
		
		public String save() throws Exception {
			try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				serviceOrder=serviceOrderManager.get(id);				
				
				trackingStatus.setServiceOrder(serviceOrder);
				try{					
				if(trackingStatus.getId() != null){
					TrackingStatus temp=trackingStatusManager.get(trackingStatus.getId());
					if(temp.getBookingAgentExSO()!=null && !temp.getBookingAgentExSO().equalsIgnoreCase("") && (trackingStatus.getBookingAgentExSO()==null || trackingStatus.getBookingAgentExSO().equalsIgnoreCase(""))){
						trackingStatus.setBookingAgentExSO(temp.getBookingAgentExSO());
					}
					if(temp.getNetworkAgentExSO()!=null && !temp.getNetworkAgentExSO().equalsIgnoreCase("") && (trackingStatus.getNetworkAgentExSO()==null || trackingStatus.getNetworkAgentExSO().equalsIgnoreCase(""))){
						trackingStatus.setNetworkAgentExSO(temp.getNetworkAgentExSO());
					}
					if(temp.getOriginAgentExSO()!=null && !temp.getOriginAgentExSO().equalsIgnoreCase("") && (trackingStatus.getOriginAgentExSO()==null || trackingStatus.getOriginAgentExSO().equalsIgnoreCase(""))){
						trackingStatus.setOriginAgentExSO(temp.getOriginAgentExSO());
					}
					if(temp.getDestinationAgentExSO()!=null && !temp.getDestinationAgentExSO().equalsIgnoreCase("") && (trackingStatus.getDestinationAgentExSO()==null || trackingStatus.getDestinationAgentExSO().equalsIgnoreCase(""))){
						trackingStatus.setDestinationAgentExSO(temp.getDestinationAgentExSO());
					}
					if(temp.getOriginSubAgentExSO()!=null && !temp.getOriginSubAgentExSO().equalsIgnoreCase("") && (trackingStatus.getOriginSubAgentExSO()==null || trackingStatus.getOriginSubAgentExSO().equalsIgnoreCase(""))){
						trackingStatus.setOriginSubAgentExSO(temp.getOriginSubAgentExSO());
					}
					if(temp.getDestinationSubAgentExSO()!=null && !temp.getDestinationSubAgentExSO().equalsIgnoreCase("") && (trackingStatus.getDestinationSubAgentExSO()==null || trackingStatus.getDestinationSubAgentExSO().equalsIgnoreCase(""))){
						trackingStatus.setDestinationSubAgentExSO(temp.getDestinationSubAgentExSO());
					}
				}
				}catch(Exception e){e.printStackTrace();}
				billing = billingManager.get(id);
				if(buttonType==null){
					buttonType="";
				}
				if(serviceOrder != null && serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().trim().equals("")))){
					userDetailList=userManager.findUserDetail(serviceOrder.getCoordinator(),sessionCorpID);
				}
				try{
					 isNetwork=false;
					if(serviceOrder.getIsNetworkRecord()==null){
						isNetwork=false;
					}
					else if(serviceOrder.getIsNetworkRecord()){
						isNetwork=true;
					}
					
					isNetworkBookingAgent=getIsNetworkBookingAgent(sessionCorpID, serviceOrder.getBookingAgentCode());
					isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getDestinationAgentCode() );
					isNetworkOriginAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getOriginAgentCode() );
					isNetworkOriginSubAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getOriginSubAgentCode() );
					isNetworkDestinationSubAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getDestinationSubAgentCode() );
					isNetworkNetworkPartnerCode=getIsNetworkAgent(sessionCorpID,trackingStatus.getNetworkPartnerCode() );
					isNetworkBookingAgentCode=getIsNetworkAgent(sessionCorpID,serviceOrder.getBookingAgentCode() ); 
					if(isNetwork){
						isNetworkOrginAgentSection=false;
						isNetworkDestinationAgentSection=false;
						isNetworkNetworkPartnerCodeSection=false;
						isNetworkBookingAgentCodeSection=false;
						if(trackingStatus.getOriginAgentExSO()!=null && !trackingStatus.getOriginAgentExSO().equals("") && serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("")){
							List list = serviceOrderManager.findBookingAgentShipNumber(trackingStatus.getOriginAgentExSO());
						    Iterator itr = list.iterator();
						    while(itr.hasNext()){
						    	ServiceOrder newSO = (ServiceOrder)itr.next();
								if (newSO.getBookingAgentShipNumber() != null && !newSO.getBookingAgentShipNumber().equals("") && !newSO.getBookingAgentShipNumber().equals(trackingStatus.getShipNumber())) {
									isNetworkOrginAgentSection=true;
								}
							}
						}
						if((trackingStatus.getDestinationAgentExSO()==null ||  trackingStatus.getDestinationAgentExSO().equals("")) || (serviceOrder.getBookingAgentShipNumber()!=null && (!serviceOrder.getBookingAgentShipNumber().equals("")))){
							List list = serviceOrderManager.findBookingAgentShipNumber(trackingStatus.getDestinationAgentExSO());
						    Iterator itr = list.iterator();
						    while(itr.hasNext()){
						    	ServiceOrder newSO = (ServiceOrder)itr.next();
								if (newSO.getBookingAgentShipNumber() != null && !newSO.getBookingAgentShipNumber().equals("") && !newSO.getBookingAgentShipNumber().equals(trackingStatus.getShipNumber())) {
									isNetworkDestinationAgentSection=true;
								}
							}							
						}
						if((serviceOrder.getBookingAgentShipNumber()!=null && (!serviceOrder.getBookingAgentShipNumber().equals("")))){
							isNetworkNetworkPartnerCodeSection=checkNetworkSection(serviceOrder.getBookingAgentShipNumber(),"NetworkPartnerCode");
						}
					}
					}catch(Exception e){
						e.printStackTrace();
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
	        			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	      	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	      	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	      	    	  return "errorlog";
					}
				
				if((!(usertype.equalsIgnoreCase("PARTNER")))&&(!(usertype.equalsIgnoreCase("AGENT")))){
				if((trackingStatus.getBrokerCode()!=null)&&(!(trackingStatus.getBrokerCode().equals("")))){
					  if(brokerCodeValid(accountInterface).equals("input")){
						String key = "brokerCode is not valid";
						errorMessage(getText(key));
						validateFormNav = "NOTOK";
						return SUCCESS;
					  }
				   }
				if((trackingStatus.getOriginSubAgentCode()!=null)&&(!(trackingStatus.getOriginSubAgentCode().equals("")))){
				  if(originSubAgentCodeValid(accountInterface).equals("input")){
						String key = "originSubAgentCode is not valid";
						errorMessage(getText(key));
						validateFormNav = "NOTOK";
						return SUCCESS;
					  }
				   }
				
   if((trackingStatus.getOriginAgentCode()!=null)&&(!(trackingStatus.getOriginAgentCode().equals("")))){
				   
				 if(originAgentCodeValid(accountInterface).equals("input")){
					String key = "originAgentCode is not valid";
					errorMessage(getText(key));
					validateFormNav = "NOTOK";
					return SUCCESS;
				  }
				}
   if((trackingStatus.getDestinationAgentCode()!=null)&&(!(trackingStatus.getDestinationAgentCode().equals("")))){		
				if(destinationAgentCodeValid(accountInterface).equals("input")){
					String key = "destinationAgentCode is not valid ";
					errorMessage(getText(key));
					validateFormNav = "NOTOK";
					return SUCCESS;
				  }
				}
      if((trackingStatus.getDestinationSubAgentCode()!=null)&&(!(trackingStatus.getDestinationSubAgentCode().equals("")))){					
				  if(destinationSubAgentCodeValid(accountInterface).equals("input")){
						String key = "destinationSubAgentCode is not valid";
						validateFormNav = "NOTOK";
						errorMessage(getText(key));
						return SUCCESS;
					  }
				   }
      if((trackingStatus.getForwarderCode()!=null)&&(!(trackingStatus.getForwarderCode().equals("")))){					
					  if(forwardCodeValid(accountInterface).equals("input")){
							String key = "forwardCode is not valid";
							errorMessage(getText(key));
							validateFormNav = "NOTOK";
							return SUCCESS;
						  }
					   }
      
      if((trackingStatus.getOriginGivenCode()!=null)&&(!(trackingStatus.getOriginGivenCode().equals("")))){
		   
			 if(originGivenValidate(accountInterface).equals("input")){
				String key = "originGivenCode is not valid";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
			  }
			}

      if((trackingStatus.getOriginReceivedCode()!=null)&&(!(trackingStatus.getOriginReceivedCode().equals("")))){
		   
			 if(originReceivedValidate(accountInterface).equals("input")){
				String key = "OriginReceivedCode is not valid";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
			  }
			}

      if((trackingStatus.getDestinationGivenCode()!=null)&&(!(trackingStatus.getDestinationGivenCode().equals("")))){
		   
			 if(destinationGivenValidate(accountInterface).equals("input")){
				String key = "DestinationGivenCode is not valid";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
			  }
			}
      if((trackingStatus.getDestinationReceivedCode()!=null)&&(!(trackingStatus.getDestinationReceivedCode().equals("")))){
		   
			 if(destinationReceivedValidate(accountInterface).equals("input")){
				String key = "DestinationReceivedCode is not valid";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
			  }
			}
      
      
				}
				
				if(trackingStatus.getBeginLoad()!=null && trackingStatus.getBeginPacking() !=null){
				    String sysdatetemp="";
					SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(trackingStatus.getBeginLoad())); 
					sysdatetemp = newDateBuilder.toString();
					Date tempDate=new Date();
					try {
						tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
					} catch (ParseException e) {
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
					String sysdatetempBeginPacking="";
					SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(trackingStatus.getBeginPacking()));  
					sysdatetempBeginPacking = newDatePackBuilder.toString();
					Date tempDateBeginPacking=new Date();
					try {
						tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
					} catch (ParseException e) {
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
					if(tempDateBeginPacking.after(tempDate)){
						String key = "Target Loading Date  should be greater or equal to Target Packing Date";
						errorMessage(getText(key));
						validateFormNav = "NOTOK";
						return SUCCESS;
				   }
      }
      if(loadA!=null && packA!=null){
				    String sysdatetemp="";
					SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(loadA));
					sysdatetemp = newDateBuilder.toString();
					Date tempDate=new Date();
					try {
						tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
					} catch (ParseException e) {
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);

					}
					String sysdatetempPackA="";
					SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(packA));
					sysdatetempPackA = newDatePackBuilder.toString();
					Date tempDatePackA=new Date();
					try {
						tempDatePackA = dateformatnewDateBuilder.parse(sysdatetempPackA);
					} catch (ParseException e) {
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
					if(tempDatePackA.after(tempDate))
					{
						String key = "Actual Loading Date  should be greater or equal to Actual Packing Date";
						errorMessage(getText(key));
						validateFormNav = "NOTOK";
						return SUCCESS;
				   }
      }
      
      if(trackingStatus.getTargetPackEnding()!=null && trackingStatus.getBeginPacking() !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(trackingStatus.getTargetPackEnding())); 
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(trackingStatus.getBeginPacking()));   
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDateBeginPacking.after(tempDate)){
				String key = "Target Packing End Date should be greater or equal to Target Packing Begin Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      
      if(trackingStatus.getBeginLoad()!=null && trackingStatus.getTargetLoadEnding() !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(trackingStatus.getTargetLoadEnding())); 
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(trackingStatus.getBeginLoad()));   
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDateBeginPacking.after(tempDate)){
				String key = "Target Loading Ending Date should be greater or equal to Target Loading Begin Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      
      
      if(trackingStatus.getTargetPackEnding()!=null && trackingStatus.getTargetLoadEnding() !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(trackingStatus.getTargetPackEnding())); 
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(trackingStatus.getTargetLoadEnding()));   
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDate.after(tempDateBeginPacking)){
				String key = "Target Load Ending Date should be greater or equal to Target Pack Ending Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      if(trackingStatus.getBeginPacking()!=null && trackingStatus.getTargetLoadEnding() !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(trackingStatus.getBeginPacking())); 
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(trackingStatus.getTargetLoadEnding()));  
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDate.after(tempDateBeginPacking)){
				String key = "Target Loading Ending Date should be greater Target Packing Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      
      if(actualLoadBegin!=null && actualPackBegin !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(actualLoadBegin)); 
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(actualPackBegin));   
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDateBeginPacking.after(tempDate)){
				String key = "Actual Loading Date should be greater than Actual Packing Begin Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      if(actualLoadBegin!=null && packA !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(packA)); 
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(actualLoadBegin));   
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDate.after(tempDateBeginPacking)){
				String key = "Actual Loading Date should be greater or equal to Actual Packing Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      if(actualPackBegin!=null && packA !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(packA)); 
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(actualPackBegin));   
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDateBeginPacking.after(tempDate)){
				String key = "Actual Packing Date should be greater or equal to Actual Packing Begin Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      if(actualLoadBegin!=null && loadA !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(loadA)); 
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(actualLoadBegin));  
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDateBeginPacking.after(tempDate)){
				String key = "Actual Loading Date should be greater or equal to Actual Loading Begin Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      
      if(trackingStatus.getDeliveryShipper()!=null && trackingStatus.getTargetdeliveryShipper() !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(trackingStatus.getDeliveryShipper()));
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(trackingStatus.getTargetdeliveryShipper()));  
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDate.after(tempDateBeginPacking)){
				String key = "Target Delivery End Date should be greater or equal to Target Delivery Begin Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      
      if(deliveryA!=null && actualDeliveryBegin !=null){
		    String sysdatetemp="";
			SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(actualDeliveryBegin)); 
			sysdatetemp = newDateBuilder.toString();
			Date tempDate=new Date();
			try {
				tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			String sysdatetempBeginPacking="";
			SimpleDateFormat dateformatnewPackDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder newDatePackBuilder = new StringBuilder(dateformatnewPackDateBuilder.format(deliveryA));   
			sysdatetempBeginPacking = newDatePackBuilder.toString();
			Date tempDateBeginPacking=new Date();
			try {
				tempDateBeginPacking = dateformatnewDateBuilder.parse(sysdatetempBeginPacking);
			} catch (ParseException e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
			if(tempDate.after(tempDateBeginPacking)){
				String key = "Actual Delivery End Date should be greater or equal to Actual Delivery Begin Date";
				errorMessage(getText(key));
				validateFormNav = "NOTOK";
				return SUCCESS;
		   }
}
      SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		 DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		 if(trackingStatus.getCrewArrivalDate()!= null){
			 StringBuilder nowYYYYMMDD2 = new StringBuilder( dateformatYYYYMMDD2.format(trackingStatus.getCrewArrivalDate())+" "+crewArrivalTime);
			 String packAD = nowYYYYMMDD2.toString();
		     Date d2 = df2.parse(packAD);
	         Date date = new Date();
			 String currentDate = dateformatYYYYMMDD2.format(date);
				 try {
					Date currentTimeZoneWiseCurrentDate =  dateformatYYYYMMDD2.parse(currentDate);
				} catch (Exception e) {
					e.printStackTrace();
				}
					
					trackingStatus.setCrewArrivalDate(df2.parse(df2.format(d2)));
				}else{
					trackingStatus.setCrewArrivalDate(null);
		 }
		
      
    
      
				Boolean defaultVendorAccLine = false;
				defaultVendorAccLine = trackingStatusManager.getdefaultVendorAccLine(sessionCorpID);
				Long starttime1 = System.currentTimeMillis();
				miscellaneous.setShipNumber(serviceOrder.getShipNumber());
				miscellaneous.setId(serviceOrder.getId());
				miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
				miscellaneous.setUpdatedOn(new Date());
				miscellaneous.setUpdatedBy(getRequest().getRemoteUser());
				miscellaneousManager.save(miscellaneous);
				trackingStatus.setMiscellaneous(miscellaneous);
				if(trackingStatus.getPackA()!=null || trackingStatus.getLoadA()!=null || trackingStatus.getDeliveryA()!=null){ 
					if(billing.getBillToCode()==null || billing.getBillToCode().equalsIgnoreCase("")){
						String key = "Please select the Bill To in Billing screen.";
						validateFormNav = "NOTOK";
						errorMessage(getText(key));
				        return SUCCESS;
					}
					if(billing.getContract()==null || billing.getContract().equalsIgnoreCase("")){
						String key = "Please Select the Pricing Contract from the Billing Tab.";
						validateFormNav = "NOTOK";
						errorMessage(getText(key));
				        return SUCCESS;
					}
					if(serviceOrder.getBookingAgentCode()==null || serviceOrder.getBookingAgentCode().equalsIgnoreCase("")){
						String key = "Please select the Booking Agent in S/O Details.";
						validateFormNav = "NOTOK";
						errorMessage(getText(key));
				        return SUCCESS;
					}
					if(serviceOrder.getCoordinator()==null || serviceOrder.getCoordinator().equalsIgnoreCase("") ){
						String key = "Please select the Coordinator in S/O Details.";
						validateFormNav = "NOTOK";
						errorMessage(getText(key));
				        return SUCCESS;
					}
					if(serviceOrder.getCompanyDivision()==null || serviceOrder.getCompanyDivision().equalsIgnoreCase("") ){
						String key = "Please select the Company Division Type in S/O Details.";
						validateFormNav = "NOTOK";
						errorMessage(getText(key));
				        return SUCCESS;
					}
				}
				try{
				 isNetwork=false;
				if(serviceOrder.getIsNetworkRecord()==null){
					isNetwork=false;
				}
				else if(serviceOrder.getIsNetworkRecord()){
					isNetwork=true;
				}
				if(isNetwork){
					isNetworkOrginAgentSection=false;
					isNetworkDestinationAgentSection=false;
					isNetworkNetworkPartnerCodeSection=false;
					isNetworkBookingAgentCodeSection=false;
					if(trackingStatus.getOriginAgentExSO()!=null && !trackingStatus.getOriginAgentExSO().equals("") && serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("")){
						List list = serviceOrderManager.findBookingAgentShipNumber(trackingStatus.getOriginAgentExSO());
					    Iterator itr = list.iterator();
					    while(itr.hasNext()){
					    	ServiceOrder newSO = (ServiceOrder)itr.next();
							if (newSO.getBookingAgentShipNumber() != null && !newSO.getBookingAgentShipNumber().equals("") && !newSO.getBookingAgentShipNumber().equals(trackingStatus.getShipNumber())) {
								isNetworkOrginAgentSection=true;
							}
						}
					}
					if((trackingStatus.getDestinationAgentExSO()==null ||  trackingStatus.getDestinationAgentExSO().equals("")) || (serviceOrder.getBookingAgentShipNumber()!=null && (!serviceOrder.getBookingAgentShipNumber().equals("")))){
						List list = serviceOrderManager.findBookingAgentShipNumber(trackingStatus.getDestinationAgentExSO());
					    Iterator itr = list.iterator();
					    while(itr.hasNext()){
					    	ServiceOrder newSO = (ServiceOrder)itr.next();
							if (newSO.getBookingAgentShipNumber() != null && !newSO.getBookingAgentShipNumber().equals("") && !newSO.getBookingAgentShipNumber().equals(trackingStatus.getShipNumber())) {
								isNetworkDestinationAgentSection=true;
							}
						}							
					}
					if((serviceOrder.getBookingAgentShipNumber()!=null && (!serviceOrder.getBookingAgentShipNumber().equals("")))){
						isNetworkNetworkPartnerCodeSection=checkNetworkSection(serviceOrder.getBookingAgentShipNumber(),"NetworkPartnerCode");
					}
				
						
				}
				}catch(Exception e){
					e.printStackTrace();
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
        			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
      	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
      	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
      	    	  return "errorlog";
				}
				boolean isNew = (trackingStatus.getId() == null);
				if(isNew){
					trackingStatus.setCreatedOn(new Date());
					if(serviceOrder.getCorpID().toString().equalsIgnoreCase("UGWW")){
						trackingStatus.setUgwIntId(serviceOrder.getId().toString());
					}
				}
				
				try {
					if(serviceOrder != null && serviceOrder.getId() != null){
						Map<String,String> serviceMap = refMasterManager.findByProperty(sessionCorpID, "SERVICE",serviceOrder.getServiceType());
						if(serviceMap != null && serviceMap.size() > 0){
							String name = serviceMap.get(serviceOrder.getServiceType());
							if(name != null && !"".equals(name)){
								name = name.replace(".",",");
								String[] st = name.split(",");
								Date date = serviceOrderManager.findByProperty(id, st[0],st[1],serviceOrder.getShipNumber());
								if(date!=null)trackingStatus.setServiceCompleteDate(date);
							}
						}
					}
					
				} catch (Exception e) {
					 e.printStackTrace();
					 }
				
				trackingStatus.setUpdatedOn(new Date());
				trackingStatus.setUpdatedBy(getRequest().getRemoteUser());
				String errorMsg="";
				String errorMessage="";
				Long tarckId=0L;
				tarckId=trackingStatus.getId();
				TrackingStatus tempoo=trackingStatusManager.get(tarckId);
				if((company.getAllowFutureActualDateEntry()!=null) && (!company.getAllowFutureActualDateEntry())){
				    Date cal = new Date();
				    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
				    formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
				    String ss =formatter.format(cal);
					try {
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
						Date currentTimeZoneWise =  dateformatYYYYMMDD.parse(ss);
						String key1 ="System cannot accept actualizing future dates, please edit following fields:";
						if(trackingStatus.getPackDone()!=null){
							if(trackingStatus.getPackDone().after(currentTimeZoneWise)){
								trackingStatus.setPackDone(tempoo.getPackDone());
								String key = "Actual Shipment Ready Date.";
								errorMsg+=key+"<br>";
							}
						}if(trackingStatus.getManagerOkOn()!=null){
							if(trackingStatus.getManagerOkOn().after(currentTimeZoneWise)){
								trackingStatus.setManagerOkOn(tempoo.getManagerOkOn());
								String key = "Actual Manager's Initials Date.";
								errorMsg+=key+"<br>";
							}
						}if((trackingStatus.getSitOriginA()!=null) && trackingStatus.getSitOriginTA()!=null && trackingStatus.getSitOriginTA().equals("A")){
							if(trackingStatus.getSitOriginA().after(currentTimeZoneWise)){
								trackingStatus.setSitOriginA(tempoo.getSitOriginA());
								String key = "SIT @ Origin - Actual Date.";
								errorMsg+=key+"<br>";
							}
						}if((trackingStatus.getSitOriginLetter()!=null) && trackingStatus.getSitOriginLetterTA()!=null && trackingStatus.getSitOriginLetterTA().equals("A")){
							if(trackingStatus.getSitOriginLetter().after(currentTimeZoneWise)){
								String key = "SIT @ Origin - Actual Send Letter Date.";
								errorMsg+=key+"<br>";
							}
						}if((trackingStatus.getPickUp2PierWH()!=null)&& trackingStatus.getPickUp2PierWHTA()!=null && trackingStatus.getPickUp2PierWHTA().equals("A")){
							if(trackingStatus.getPickUp2PierWH().after(currentTimeZoneWise)){
								trackingStatus.setPickUp2PierWH(tempoo.getPickUp2PierWH());
								String key = "Actual Pick Up Container Date.";
								errorMsg+=key+"<br>";
							}
						}if((trackingStatus.getCutOffDate()!=null)&& trackingStatus.getCutOffDateTA()!=null && trackingStatus.getCutOffDateTA().equals("A")){
							if(trackingStatus.getCutOffDate().after(currentTimeZoneWise)){
								trackingStatus.setCutOffDate(tempoo.getCutOffDate());
								String key = "Actual Container Cut Off Date.";
								errorMsg+=key+"<br>";
							}
						}if((trackingStatus.getReturnContainerDateA()!=null)&& trackingStatus.getReturnContainer()!=null && trackingStatus.getReturnContainer().equals("A")){
							if(trackingStatus.getReturnContainerDateA().after(currentTimeZoneWise)){
								trackingStatus.setReturnContainerDateA(tempoo.getReturnContainerDateA());
								String key = "Actual Return Container/Deliver to Pier Date.";
								errorMsg+=key+"<br>";
							}
						}if((trackingStatus.getClearCustom()!=null)&& (trackingStatus.getClearCustomTA()!=null) && (trackingStatus.getClearCustomTA().equals("A"))){
							if(trackingStatus.getClearCustom().after(currentTimeZoneWise)){
								trackingStatus.setClearCustom(tempoo.getClearCustom());
								String key = "Actual Clear Customs Date.";
								errorMsg+=key+"<br>";
							}
						}if((trackingStatus.getPickUp2PierWHDestination()!=null)  && (trackingStatus.getPickUp2PierWHTADestination()!=null) && (trackingStatus.getPickUp2PierWHTADestination().equals("A")) ){
							if(trackingStatus.getPickUp2PierWHDestination().after(currentTimeZoneWise)){
								trackingStatus.setPickUp2PierWHDestination(tempoo.getPickUp2PierWHDestination());
								String key = "Actual Pick Up Container Date.";
								errorMsg+=key+"<br>";
							}
						}if((trackingStatus.getReturnContainerDateADestination()!=null) && (trackingStatus.getReturnContainerDestination()!=null) && (trackingStatus.getReturnContainerDestination().equals("A")) ){
							if(trackingStatus.getReturnContainerDateADestination().after(currentTimeZoneWise)){
								trackingStatus.setReturnContainerDateADestination(tempoo.getReturnContainerDateADestination());
								String key = "Actual Return Container/Deliver to Pier Date.";
								errorMsg+=key+"<br>";
							}
						}if((trackingStatus.getSitDestinationA()!=null) && (trackingStatus.getSitDestinationTA()!=null) && (trackingStatus.getSitDestinationTA().equals("A"))){
						if(trackingStatus.getSitDestinationA().after(currentTimeZoneWise)){
							trackingStatus.setSitDestinationA(tempoo.getSitDestinationA());
							String key = "SIT @ Destination � Actual Date.";
							errorMsg+=key+"<br>";
						}
						}if((trackingStatus.getSitDestinationLetter()!=null) && (trackingStatus.getSitDestinationLetterTA()!=null) && (trackingStatus.getSitDestinationLetterTA().equals("A"))){
							if(trackingStatus.getSitDestinationLetter().after(currentTimeZoneWise)){
								trackingStatus.setSitDestinationLetter(tempoo.getSitDestinationLetter());
								String key = "SIT @ Destination � Actual Send Letter Date.";
								errorMsg+=key+"<br>";
							}
						}
					 } catch (ParseException e) {
						 logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		        			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		      	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		      	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		      	    	 e.printStackTrace();
		      	    	  return "errorlog";
						}
					}
				if(packA!=null && trackingStatus.getPackA()!=null && (packA.compareTo(trackingStatus.getPackA())==0)){				
					}else{
						trackingStatus.setPackA(packA);
					}
				if(loadA!=null && trackingStatus.getLoadA()!=null && (loadA.compareTo(trackingStatus.getLoadA())==0)){				
					}else{
						trackingStatus.setLoadA(loadA);
					}
				if(deliveryA!=null && trackingStatus.getDeliveryA()!=null && (deliveryA.compareTo(trackingStatus.getDeliveryA())==0)){				
					}else{
						trackingStatus.setDeliveryA(deliveryA);
					}
				if(actualPackBegin!=null && trackingStatus.getActualPackBegin()!=null && (actualPackBegin.compareTo(trackingStatus.getActualPackBegin())==0)){				
				}else{
					trackingStatus.setActualPackBegin(actualPackBegin);
				}
			if(actualLoadBegin!=null && trackingStatus.getActualLoadBegin()!=null && (actualLoadBegin.compareTo(trackingStatus.getActualLoadBegin())==0)){				
				}else{
					trackingStatus.setActualLoadBegin(actualLoadBegin);
				}
			if(actualDeliveryBegin!=null && trackingStatus.getActualDeliveryBegin()!=null && (actualDeliveryBegin.compareTo(trackingStatus.getActualDeliveryBegin())==0)){				
				}else{
					trackingStatus.setActualDeliveryBegin(actualDeliveryBegin);
				}
				
				if((company.getAllowFutureActualDateEntry()!=null) && (!company.getAllowFutureActualDateEntry())){
					Date cal = new Date();
					 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
				        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
				        String ss =formatter.format(cal);
				    try {
				    	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
						Date currentTimeZoneWise =  dateformatYYYYMMDD.parse(ss);
				if(packA!=null && trackingStatus.getPackA()!=null){
					if((trackingStatus.getPackA()).after(currentTimeZoneWise)){
						trackingStatus.setPackA(tempoo.getPackA());
						String key = "Actual Packing Date.";
						errorMsg+=key+"<br>";
					}
				}if(loadA!=null && trackingStatus.getLoadA()!=null){
					if((trackingStatus.getLoadA()).after(currentTimeZoneWise)){
						trackingStatus.setLoadA(tempoo.getLoadA());
						String key = "Actual Loading Date.";
						errorMsg+=key+"<br>";
					}
				}if(deliveryA!=null && trackingStatus.getDeliveryA()!=null){
					if((trackingStatus.getDeliveryA()).after(currentTimeZoneWise)){
						trackingStatus.setDeliveryA(tempoo.getDeliveryA());
						String key = "Actual Delivery Date.";
						errorMsg+=key+"<br>";
						}
					}
				String permKey = sessionCorpID +"-"+"component.trackingstatus.nofuturedaydates";
				checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
					if(checkFieldVisibility){
				if(trackingStatus.getPackDayConfirmCall()!=null)
				{
					if((trackingStatus.getPackDayConfirmCall()).after(currentTimeZoneWise)){
						trackingStatus.setPackDayConfirmCall(tempoo.getPackDayConfirmCall());
						String key = "Packing Day Call.";
						errorMessage+=key+"<br>";
						}
					
				}
				
				if(trackingStatus.getLoadFollowUpCall()!=null)
				{if((trackingStatus.getLoadFollowUpCall()).after(currentTimeZoneWise)){
						trackingStatus.setLoadFollowUpCall(tempoo.getLoadFollowUpCall());
						String key = "Load Day Call.";
						errorMessage+=key+"<br>";
					}
				}
				if(trackingStatus.getDeliveryFollowCall()!=null)
				{
				if((trackingStatus.getDeliveryFollowCall()).after(currentTimeZoneWise)){
						trackingStatus.setDeliveryFollowCall(tempoo.getDeliveryFollowCall());
						String key = "Delivery Day Call.";
						errorMessage+=key+"<br>";
						
						}
					}}
				
				}catch (ParseException e) {
					e.printStackTrace();
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
        			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
      	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
      	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
      	    	  return "errorlog";
				}
}
				if(!errorMsg.equalsIgnoreCase("")){
					String key1 ="System cannot accept actualizing future dates, Please edit following fields:";
						validateFormNav = "NOTOK";
						errorMessage(getText(key1+"<BR>"+errorMsg));
				        return SUCCESS;
					
				}
				if(!errorMessage.equalsIgnoreCase("")){
					String key2 ="System cannot accept  future dates, Please edit following fields:";
						validateFormNav = "NOTOK";
						errorMessage(getText(key2+"<BR>"+errorMessage));
				        return SUCCESS;
					
				}
				customs = customManager.findBySO(serviceOrder.getId(),sessionCorpID);
				servicePartnerss = new ArrayList(servicePartnerManager.findByCarriers(serviceOrder.getShipNumber()));	
				storages = new HashSet(storageManager.findStorageByShipNumber(serviceOrder.getShipNumber()));
				handOutList=itemDataManager.getAllHandOutList(serviceOrder.getShipNumber());
				trackingStatus.setId(serviceOrder.getId());
				List accLineListOrigin = new ArrayList();
				List accLineListDestination = new ArrayList();
				boolean contractTypeForCurrency=false;
				if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
				String	contractTypeValue=	accountLineManager.checkContractType(billing.getContract(),sessionCorpID);
					if((!contractTypeValue.trim().equals("")) && (!(contractTypeValue.trim().equalsIgnoreCase("NAA"))) ){
						contractTypeForCurrency=true;	
					}
					}
				if(defaultVendorAccLine == true && usertype!=null && usertype.equalsIgnoreCase("USER")){
				accLineListOrigin = accountLineManager.getAgentOriginStatus(id);
				accLineListDestination = accountLineManager.getAgentDestStatus(id);
				originAgentCodeListTrakingStatus = trackingStatusManager.findByOriginAgentCodeInTrackingStauts(id);
				destinationAgentCodeListTrakingStatus = trackingStatusManager.findByDestinationAgentCodeInTrackingStauts(id);
				if(originAgentCodeListTrakingStatus.isEmpty() && accLineListOrigin.isEmpty()){
					if(trackingStatus.getOriginAgentCode()!=null && !trackingStatus.getOriginAgentCode().equalsIgnoreCase("") && trackingStatus.getOriginAgentCode().trim().length()>0){
						if(!trackingStatus.getOriginAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode())){
							baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
							baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
							accountLine = new AccountLine();
							boolean activateAccPortal =true;
							try{
							if(accountLineAccountPortalFlag){	
							activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
							accountLine.setActivateAccPortal(activateAccPortal);
							}
							}catch(Exception e){
								e.printStackTrace();
							}
							accountLine.setServiceOrderId(serviceOrder.getId());
							accountLine.setCorpID(corpIDform);
							accountLine.setStatus(true);
							accountLine.setServiceOrder(serviceOrder);
							accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
							accountLine.setShipNumber(serviceOrder.getShipNumber());
							accountLine.setCategory("Origin");
							accountLine.setVendorCode(trackingStatus.getOriginAgentCode()); 
							accountLine.setEstimateVendorName(trackingStatus.getOriginAgent());
							if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
								if(trackingStatus.getOriginAgentCode()!=null && (!(trackingStatus.getOriginAgentCode().toString().equals(""))) ){
									if(billing.getOriginAgentVatCode()!=null && (!(billing.getOriginAgentVatCode().toString().equals("")))){
										accountLine.setPayVatDescr(billing.getOriginAgentVatCode());
						  				String payVatPercent="0";
						  				if(payVatPercentList.containsKey(billing.getOriginAgentVatCode())){ 
						  					payVatPercent=payVatPercentList.get(billing.getOriginAgentVatCode());
						  				}
										accountLine.setPayVatPercent(payVatPercent);	
									}
									
								}	
							}
							accountLine.setActgCode(trackingStatus.getActgCode()); 
							accountLine.setCreatedOn(new Date());
							accountLine.setCreatedBy(trackingStatus.getUpdatedBy());
							accountLine.setUpdatedOn(new Date());
							accountLine.setUpdatedBy(trackingStatus.getUpdatedBy());
							accountLine.setBasis("");
							if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
								String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
								try{  
									String roles[] = agentRoleValue.split("~");
								  if(roles[3].toString().equals("hauler")){
									   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
										   accountLine.setDivision("01");
									   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
										   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
											   accountLine.setDivision("01");
										   }else{ 
											   String divisionTemp="";
													if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
													divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
													}
													if(!divisionTemp.equalsIgnoreCase("")){
														accountLine.setDivision(divisionTemp);
													}else{
														accountLine.setDivision(null);
													}
										     
										   }
									   }else{
										   String divisionTemp="";
												if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
												divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
												}
												if(!divisionTemp.equalsIgnoreCase("")){
													accountLine.setDivision(divisionTemp);
												}else{
													accountLine.setDivision(null);
												}
									   }
								  }else{
									  String divisionTemp="";
											if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
											divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
											}
											if(!divisionTemp.equalsIgnoreCase("")){
												accountLine.setDivision(divisionTemp);
											}else{
												accountLine.setDivision(null);
											}
								  }
								}catch (Exception e1) {
									e1.printStackTrace();
								}
							}
							maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber());
							if ( maxLineNumber.get(0) == null ) {          
					        	accountLineNumber = "001";
					        }else {
				             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
				                if((autoLineNumber.toString()).length() == 2) {
				                	accountLineNumber = "0"+(autoLineNumber.toString());
				                }
				                else if((autoLineNumber.toString()).length() == 1) {
				                	accountLineNumber = "00"+(autoLineNumber.toString());
				                } else {
				                	accountLineNumber=autoLineNumber.toString();
				                }
				            }
					        accountLine.setAccountLineNumber(accountLineNumber);
					        accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
					        accountLine.setContract(billing.getContract());
					        accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
					        accountLine.setNetworkBillToName(billing.getNetworkBillToName());
					        accountLine.setBillToCode(billing.getBillToCode());
					        if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
					    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
					    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
						    		String recVatPercent="0";
						    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
							    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
							    		}
				    				accountLine.setRecVatPercent(recVatPercent);
						    		}	
					        }
				    		accountLine.setBillToName(billing.getBillToName());
				    		
							String billingCurrency="";
							if(contractTypeForCurrency){
							  billingCurrency=serviceOrderManager.getBillingCurrencyValue(billing.getBillToCode(),sessionCorpID);
							} 
				    		if(!billingCurrency.equalsIgnoreCase("") && contractTypeForCurrency)
				    		{
				    			accountLine.setRecRateCurrency(billingCurrency);
				    			accountLine.setEstSellCurrency(billingCurrency);
				    			accountLine.setRevisionSellCurrency(billingCurrency);
				    			accountLine.setRacValueDate(new Date());
				    			accountLine.setEstSellValueDate(new Date());
				    			accountLine.setRevisionSellValueDate(new Date());

				    			List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,billingCurrency);
				    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
				    				try{
				    				accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
				    				accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    				accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    				}catch(Exception e){
				    					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    				accountLine.setRecRateExchange(new BigDecimal(1.0000));
				    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				    				}
				    			}else{
				    				accountLine.setRecRateExchange(new BigDecimal(1.0000));
				    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				    			}
				    		}else{
							if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
								accountLine.setRecRateCurrency(baseCurrency);
				    			accountLine.setEstSellCurrency(baseCurrency);
				    			accountLine.setRevisionSellCurrency(baseCurrency);
				    			accountLine.setRacValueDate(new Date());
				    			accountLine.setEstSellValueDate(new Date());
				    			accountLine.setRevisionSellValueDate(new Date());
								accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
								accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
						 
							}else{
								accountLine.setRecRateCurrency(baseCurrencyCompanyDivision);
				    			accountLine.setEstSellCurrency(baseCurrencyCompanyDivision);
				    			accountLine.setRevisionSellCurrency(baseCurrencyCompanyDivision);
				    			accountLine.setRacValueDate(new Date());
				    			accountLine.setEstSellValueDate(new Date());
				    			accountLine.setRevisionSellValueDate(new Date());
								List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
				    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
				    				try{
				    				accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    				accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    				accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
				    				}catch(Exception e){
				    					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    					accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
					    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
					    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				    				}
				    			}else{
				    				accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
				    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				    			}
							}
				    		}
				    		String vendorCurrency="";
				    		if(contractTypeForCurrency){
				    		 vendorCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getVendorCode(),sessionCorpID);
				    		}
				    		if(!vendorCurrency.equalsIgnoreCase("") && contractTypeForCurrency)
				    		{
				    			accountLine.setEstCurrency(vendorCurrency);
				    			accountLine.setRevisionCurrency(vendorCurrency);
				    			accountLine.setCountry(vendorCurrency);
				    			accountLine.setEstValueDate(new Date());
				    			accountLine.setRevisionValueDate(new Date());
				    			accountLine.setValueDate(new Date());
				    			List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,vendorCurrency);
				    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
				    				try{
				    				accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    				accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    				accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
				    				}catch(Exception e){
				    					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    					accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
					    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
					    				accountLine.setExchangeRate(new Double(1.0));	
				    				}
				    			}else{
				    				accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
				    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setExchangeRate(new Double(1.0));
				    			}
				    		}else{
				    		if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
				    		{
				    		accountLine.setEstCurrency(baseCurrency);
							accountLine.setRevisionCurrency(baseCurrency);	
							accountLine.setCountry(baseCurrency);
							accountLine.setEstValueDate(new Date());
							accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
							accountLine.setRevisionValueDate(new Date());
							accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
							accountLine.setValueDate(new Date());
							accountLine.setExchangeRate(new Double(1.0));
				    		}
				    		else
				    		{
				    		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
							accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
							accountLine.setCountry(baseCurrencyCompanyDivision);
							accountLine.setEstValueDate(new Date());
							accountLine.setRevisionValueDate(new Date());
							accountLine.setValueDate(new Date());
							List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
							if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								try{
								accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
								}catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
				    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setExchangeRate(new Double(1.0));	
								}
							}else{
								accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
								accountLine.setExchangeRate(new Double(1.0));
							}
				    		}
				    		}
					        accountLineManager.save(accountLine);
						}	
					}
				}
				
				if(destinationAgentCodeListTrakingStatus.isEmpty() && accLineListDestination.isEmpty()){
					if(trackingStatus.getDestinationAgentCode()!=null && !trackingStatus.getDestinationAgentCode().equalsIgnoreCase("") && trackingStatus.getDestinationAgentCode().trim().length()>0){
						if(!trackingStatus.getDestinationAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode())){
							baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
							baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
							accountLine1 = new AccountLine();
							boolean activateAccPortal =true;
							try{
							if(accountLineAccountPortalFlag){	
							activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
							accountLine1.setActivateAccPortal(activateAccPortal);
							}
							}catch(Exception e){
								e.printStackTrace();
							}
							accountLine1.setServiceOrderId(serviceOrder.getId());
							accountLine1.setCorpID(corpIDform);
							accountLine1.setStatus(true);
							accountLine1.setServiceOrder(serviceOrder);
							accountLine1.setSequenceNumber(serviceOrder.getSequenceNumber());
							accountLine1.setShipNumber(serviceOrder.getShipNumber());
							accountLine1.setCategory("Destin");
							accountLine1.setVendorCode(trackingStatus.getDestinationAgentCode());
							accountLine1.setEstimateVendorName(trackingStatus.getDestinationAgent());
							accountLine1.setActgCode(trackingStatus.getActgCodeForDis()); 
							if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
								if(trackingStatus.getDestinationAgentCode()!=null && (!(trackingStatus.getDestinationAgentCode().toString().equals(""))) ){
									if(billing.getDestinationAgentVatCode()!=null && (!(billing.getDestinationAgentVatCode().toString().equals("")))){
										accountLine1.setPayVatDescr(billing.getDestinationAgentVatCode());
						  				String payVatPercent="0";
						  				if(payVatPercentList.containsKey(billing.getDestinationAgentVatCode())){ 
						  					payVatPercent=payVatPercentList.get(billing.getDestinationAgentVatCode());
						  				}
						  				accountLine1.setPayVatPercent(payVatPercent);	
									}
									
								}	
							}
							accountLine1.setCreatedOn(new Date());
							accountLine1.setCreatedBy(trackingStatus.getUpdatedBy());
							accountLine1.setUpdatedOn(new Date());
							accountLine1.setUpdatedBy(trackingStatus.getUpdatedBy());
							accountLine1.setBasis("");
							if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
								String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
								try{  
									String roles[] = agentRoleValue.split("~");
								  if(roles[3].toString().equals("hauler")){
									   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
										   accountLine1.setDivision("01");
									   }else if(accountLine1.getChargeCode()!=null && !accountLine1.getChargeCode().equalsIgnoreCase("")) {
										   if(systemDefault.getDefaultDivisionCharges().contains(accountLine1.getChargeCode())){
											   accountLine1.setDivision("01");
										   }else{ 
											   String divisionTemp="";
													if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
													divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
													}
													if(!divisionTemp.equalsIgnoreCase("")){
														accountLine1.setDivision(divisionTemp);
													}else{
														accountLine1.setDivision(null);
													}
										     
										   }
									   }else{
										   String divisionTemp="";
												if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
												divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
												}
												if(!divisionTemp.equalsIgnoreCase("")){
													accountLine1.setDivision(divisionTemp);
												}else{
													accountLine1.setDivision(null);
												}
									   }
								  }else{
									  String divisionTemp="";
											if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
											divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
											}
											if(!divisionTemp.equalsIgnoreCase("")){
												accountLine1.setDivision(divisionTemp);
											}else{
												accountLine1.setDivision(null);
											}
								  }
								}catch (Exception e1) {
									e1.printStackTrace();
								}
							}
							maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber());
							if ( maxLineNumber.get(0) == null ) {          
					        	accountLineNumber = "001";
					        }else {
				             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
				                if((autoLineNumber.toString()).length() == 2) {
				                	accountLineNumber = "0"+(autoLineNumber.toString());
				                }else if((autoLineNumber.toString()).length() == 1) {
				                	accountLineNumber = "00"+(autoLineNumber.toString());
				                } else {
				                	accountLineNumber=autoLineNumber.toString();
				                }
				            	}
					        accountLine1.setAccountLineNumber(accountLineNumber);
					        accountLine1.setCompanyDivision(serviceOrder.getCompanyDivision());
					        accountLine1.setContract(billing.getContract());
					        accountLine1.setNetworkBillToCode(billing.getNetworkBillToCode());
					        accountLine1.setNetworkBillToName(billing.getNetworkBillToName());
					        accountLine1.setBillToCode(billing.getBillToCode());
					        if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
					        	accountLine1.setRecVatDescr(billing.getPrimaryVatCode());
					    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
						    		String recVatPercent="0";
						    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
							    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
							    		}
						    		accountLine1.setRecVatPercent(recVatPercent);
						    		}	
					        }
					        accountLine1.setBillToName(billing.getBillToName());
					        String billingCurrency="";
					        if(contractTypeForCurrency){
					        	billingCurrency=serviceOrderManager.getBillingCurrencyValue(billing.getBillToCode(),sessionCorpID);
					        }
				    		if(!billingCurrency.equalsIgnoreCase("") && contractTypeForCurrency)
				    		{
				    			accountLine1.setRecRateCurrency(billingCurrency);
				    			accountLine1.setEstSellCurrency(billingCurrency);
				    			accountLine1.setRevisionSellCurrency(billingCurrency);
				    			accountLine1.setRacValueDate(new Date());
				    			accountLine1.setEstSellValueDate(new Date());
				    			accountLine1.setRevisionSellValueDate(new Date());

				    			List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,billingCurrency);
				    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
				    				try{
				    					accountLine1.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
				    					accountLine1.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    					accountLine1.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    				}catch(Exception e){
				    					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    					accountLine1.setRecRateExchange(new BigDecimal(1.0000));
				    					accountLine1.setEstSellExchangeRate(new BigDecimal(1.0000));
				    					accountLine1.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				    				}
				    			}else{
				    				accountLine1.setRecRateExchange(new BigDecimal(1.0000));
				    				accountLine1.setEstSellExchangeRate(new BigDecimal(1.0000));
				    				accountLine1.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				    			}
				    		}else{
							if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
								accountLine1.setRecRateCurrency(baseCurrency);
								accountLine1.setEstSellCurrency(baseCurrency);
								accountLine1.setRevisionSellCurrency(baseCurrency);
								accountLine1.setRacValueDate(new Date());
								accountLine1.setEstSellValueDate(new Date());
								accountLine1.setRevisionSellValueDate(new Date());
							    accountLine1.setRecRateExchange(new BigDecimal(1.0000)); 
				    			accountLine1.setEstSellExchangeRate(new BigDecimal(1.0000));
				    		    accountLine1.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				    			 
							}else{
								accountLine1.setRecRateCurrency(baseCurrencyCompanyDivision);
								accountLine1.setEstSellCurrency(baseCurrencyCompanyDivision);
								accountLine1.setRevisionSellCurrency(baseCurrencyCompanyDivision);
								accountLine1.setRacValueDate(new Date());
								accountLine1.setEstSellValueDate(new Date());
								accountLine1.setRevisionSellValueDate(new Date());
								List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
				    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
				    				try{
				    					accountLine1.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    					accountLine1.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    					accountLine1.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
				    				}catch(Exception e){
				    					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    					accountLine1.setRecRateExchange(new BigDecimal(1.0000)); 
				    					accountLine1.setEstSellExchangeRate(new BigDecimal(1.0000));
				    					accountLine1.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				    				}
				    			}else{
				    				accountLine1.setRecRateExchange(new BigDecimal(1.0000)); 
				    				accountLine1.setEstSellExchangeRate(new BigDecimal(1.0000));
				    				accountLine1.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				    			}
							}
				    		}
				    		String vendorCurrency="";
				    		if(contractTypeForCurrency){
				    			vendorCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine1.getVendorCode(),sessionCorpID);
				    		}
				    		if(!vendorCurrency.equalsIgnoreCase("") && contractTypeForCurrency)
				    		{
				    			accountLine1.setEstCurrency(vendorCurrency);
				    			accountLine1.setRevisionCurrency(vendorCurrency);
				    			accountLine1.setCountry(vendorCurrency);
				    			accountLine1.setEstValueDate(new Date());
				    			accountLine1.setRevisionValueDate(new Date());
				    			accountLine1.setValueDate(new Date());
				    			List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,vendorCurrency);
				    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
				    				try{
				    					accountLine1.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    					accountLine1.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				    					accountLine1.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
				    				}catch(Exception e){
				    					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    					accountLine1.setEstExchangeRate(new BigDecimal(1.0000)); 
				    					accountLine1.setRevisionExchangeRate(new BigDecimal(1.0000));
				    					accountLine1.setExchangeRate(new Double(1.0));	
				    				}
				    			}else{
				    				accountLine1.setEstExchangeRate(new BigDecimal(1.0000)); 
				    				accountLine1.setRevisionExchangeRate(new BigDecimal(1.0000));
				    				accountLine1.setExchangeRate(new Double(1.0));
				    			}
				    		}else{
				    		if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
				    		{
				    			accountLine1.setEstCurrency(baseCurrency);
				    			accountLine1.setRevisionCurrency(baseCurrency);	
				    			accountLine1.setCountry(baseCurrency);
				    			accountLine1.setEstValueDate(new Date());
				    			accountLine1.setEstExchangeRate(new BigDecimal(1.0000)); 
				    			accountLine1.setRevisionValueDate(new Date());
				    			accountLine1.setRevisionExchangeRate(new BigDecimal(1.0000)); 
				    			accountLine1.setValueDate(new Date());
				    			accountLine1.setExchangeRate(new Double(1.0));
				    		}
				    		else
				    		{
				    			accountLine1.setEstCurrency(baseCurrencyCompanyDivision);
				    			accountLine1.setRevisionCurrency(baseCurrencyCompanyDivision);
				    			accountLine1.setCountry(baseCurrencyCompanyDivision);
				    			accountLine1.setEstValueDate(new Date());
				    			accountLine1.setRevisionValueDate(new Date());
				    			accountLine1.setValueDate(new Date());
							List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
							if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								try{
									accountLine1.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
									accountLine1.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
									accountLine1.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
								}catch(Exception e){
									accountLine1.setEstExchangeRate(new BigDecimal(1.0000)); 
									accountLine1.setRevisionExchangeRate(new BigDecimal(1.0000));
									accountLine1.setExchangeRate(new Double(1.0));
									e.printStackTrace();
								}
							}else{
								accountLine1.setEstExchangeRate(new BigDecimal(1.0000)); 
								accountLine1.setRevisionExchangeRate(new BigDecimal(1.0000));
								accountLine1.setExchangeRate(new Double(1.0));
							}
				    		}
				    		}
					        accountLineManager.save(accountLine1);
						}	
					}
				}
						
				List accLineListSubOrigin = accountLineManager.getAgentOriginStatus(id);
				if(accLineListSubOrigin.isEmpty() && trackingStatus.getOriginSubAgentCode() != null && !trackingStatus.getOriginSubAgentCode().equalsIgnoreCase("") && trackingStatus.getOriginSubAgentCode().trim().length()>0){
					if(!trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode())){
						baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
						baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
						accountLine = new AccountLine();
						boolean activateAccPortal =true;
						try{
						if(accountLineAccountPortalFlag){	
						activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
						accountLine.setActivateAccPortal(activateAccPortal);
						}
						}catch(Exception e){
							e.printStackTrace();
						}
						accountLine.setServiceOrderId(serviceOrder.getId());
						accountLine.setCorpID(corpIDform);
						accountLine.setStatus(true);
						accountLine.setServiceOrder(serviceOrder);
						accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
						accountLine.setShipNumber(serviceOrder.getShipNumber());
						accountLine.setCategory("Origin");
						accountLine.setVendorCode(trackingStatus.getOriginSubAgentCode());
						accountLine.setEstimateVendorName(trackingStatus.getOriginSubAgent());
						accountLine.setActgCode(trackingStatus.getActgCode()); 
						if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
							if(trackingStatus.getOriginSubAgentCode()!=null && (!(trackingStatus.getOriginSubAgentCode().toString().equals(""))) ){
								if(billing.getOriginSubAgentVatCode()!=null && (!(billing.getOriginSubAgentVatCode().toString().equals("")))){
									accountLine.setPayVatDescr(billing.getOriginSubAgentVatCode());
					  				String payVatPercent="0";
					  				if(payVatPercentList.containsKey(billing.getOriginSubAgentVatCode())){ 
					  					payVatPercent=payVatPercentList.get(billing.getOriginSubAgentVatCode());
					  				}
					  				accountLine.setPayVatPercent(payVatPercent);	
								}
								
							}	
						}
						accountLine.setCreatedOn(new Date());
						accountLine.setCreatedBy(trackingStatus.getUpdatedBy());
						accountLine.setUpdatedOn(new Date());
						accountLine.setUpdatedBy(trackingStatus.getUpdatedBy());
						accountLine.setBasis("");
						if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
							String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
							try{  
								String roles[] = agentRoleValue.split("~");
							  if(roles[3].toString().equals("hauler")){
								   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
									   accountLine.setDivision("01");
								   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
									   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
										   accountLine.setDivision("01");
									   }else{ 
										   String divisionTemp="";
												if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
												divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
												}
												if(!divisionTemp.equalsIgnoreCase("")){
													accountLine.setDivision(divisionTemp);
												}else{
													accountLine.setDivision(null);
												}
									     
									   }
								   }else{
									   String divisionTemp="";
											if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
											divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
											}
											if(!divisionTemp.equalsIgnoreCase("")){
												accountLine.setDivision(divisionTemp);
											}else{
												accountLine.setDivision(null);
											}
								   }
							  }else{
								  String divisionTemp="";
										if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
										}
										if(!divisionTemp.equalsIgnoreCase("")){
											accountLine.setDivision(divisionTemp);
										}else{
											accountLine.setDivision(null);
										}
							  }
							}catch (Exception e1) {
								e1.printStackTrace();
							}
						}
						maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber());
						if ( maxLineNumber.get(0) == null ) {          
				        	accountLineNumber = "001";
				        }else {
				         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
				            if((autoLineNumber.toString()).length() == 2) {
				            	accountLineNumber = "0"+(autoLineNumber.toString());
				            }
				            else if((autoLineNumber.toString()).length() == 1) {
				            	accountLineNumber = "00"+(autoLineNumber.toString());
				            } else {
				            	accountLineNumber=autoLineNumber.toString();
				            }
				        }
				        accountLine.setAccountLineNumber(accountLineNumber);
				        accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
				        accountLine.setContract(billing.getContract());
				        accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
				        accountLine.setNetworkBillToName(billing.getNetworkBillToName());
				        accountLine.setBillToCode(billing.getBillToCode());
				        if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
				    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					    		String recVatPercent="0";
					    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
						    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
						    		}
								accountLine.setRecVatPercent(recVatPercent);
					    		}
				    		}
						accountLine.setBillToName(billing.getBillToName());
						String billingCurrency="";
						if(contractTypeForCurrency){
						 billingCurrency=serviceOrderManager.getBillingCurrencyValue(billing.getBillToCode(),sessionCorpID);
					    }
						if(!billingCurrency.equalsIgnoreCase("") && contractTypeForCurrency)
						{
							accountLine.setRecRateCurrency(billingCurrency);
							accountLine.setEstSellCurrency(billingCurrency);
							accountLine.setRevisionSellCurrency(billingCurrency);
							accountLine.setRacValueDate(new Date());
							accountLine.setEstSellValueDate(new Date());
							accountLine.setRevisionSellValueDate(new Date());

							List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,billingCurrency);
							if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								try{
								accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								}catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								accountLine.setRecRateExchange(new BigDecimal(1.0000));
								accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
								}
							}else{
								accountLine.setRecRateExchange(new BigDecimal(1.0000));
								accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
							}
						}else{
						if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
							accountLine.setRecRateCurrency(baseCurrency);
							accountLine.setEstSellCurrency(baseCurrency);
							accountLine.setRevisionSellCurrency(baseCurrency);
							accountLine.setRacValueDate(new Date());
							accountLine.setEstSellValueDate(new Date());
							accountLine.setRevisionSellValueDate(new Date());
							accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
							accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
							accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
					 
						}else{
							accountLine.setRecRateCurrency(baseCurrencyCompanyDivision);
							accountLine.setEstSellCurrency(baseCurrencyCompanyDivision);
							accountLine.setRevisionSellCurrency(baseCurrencyCompanyDivision);
							accountLine.setRacValueDate(new Date());
							accountLine.setEstSellValueDate(new Date());
							accountLine.setRevisionSellValueDate(new Date());
							List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
							if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								try{
								accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
								}catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
				    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
								}
							}else{
								accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
								accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
							}
						}
						}
						String vendorCurrency="";
						if(contractTypeForCurrency){
						vendorCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getVendorCode(),sessionCorpID);
						}
						if(!vendorCurrency.equalsIgnoreCase("") && contractTypeForCurrency)
						{
							accountLine.setEstCurrency(vendorCurrency);
							accountLine.setRevisionCurrency(vendorCurrency);
							accountLine.setCountry(vendorCurrency);
							accountLine.setEstValueDate(new Date());
							accountLine.setRevisionValueDate(new Date());
							accountLine.setValueDate(new Date());
							List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,vendorCurrency);
							if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								try{
								accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
								}catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
				    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setExchangeRate(new Double(1.0));	
								}
							}else{
								accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
								accountLine.setExchangeRate(new Double(1.0));
							}
						}else{
						if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
						{
						accountLine.setEstCurrency(baseCurrency);
						accountLine.setRevisionCurrency(baseCurrency);	
						accountLine.setCountry(baseCurrency);
						accountLine.setEstValueDate(new Date());
						accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setRevisionValueDate(new Date());
						accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setValueDate(new Date());
						accountLine.setExchangeRate(new Double(1.0));
						}
						else
						{
						accountLine.setEstCurrency(baseCurrencyCompanyDivision);
						accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
						accountLine.setCountry(baseCurrencyCompanyDivision);
						accountLine.setEstValueDate(new Date());
						accountLine.setRevisionValueDate(new Date());
						accountLine.setValueDate(new Date());
						List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
						if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
							try{
							accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
							}catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
								accountLine.setExchangeRate(new Double(1.0));	
							}
						}else{
							accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
							accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
							accountLine.setExchangeRate(new Double(1.0));
						}
						}
						}
				        accountLineManager.save(accountLine);
					}	
				}
							
				List accLineListSubDestination = accountLineManager.getAgentDestStatus(id);
				if(accLineListSubDestination.isEmpty() && trackingStatus.getDestinationSubAgentCode() != null && !trackingStatus.getDestinationSubAgentCode().equalsIgnoreCase("") && trackingStatus.getDestinationSubAgentCode().trim().length()>0){
					if(!trackingStatus.getDestinationSubAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode())){
						baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
						baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrder.getCompanyDivision(),sessionCorpID);
						accountLine = new AccountLine();
						boolean activateAccPortal =true;
						try{
						if(accountLineAccountPortalFlag){	
						activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
						accountLine.setActivateAccPortal(activateAccPortal);
						}
						}catch(Exception e){
							e.printStackTrace();
						}
						accountLine.setServiceOrderId(serviceOrder.getId());
						accountLine.setCorpID(corpIDform);
						accountLine.setStatus(true);
						accountLine.setServiceOrder(serviceOrder);
						accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
						accountLine.setShipNumber(serviceOrder.getShipNumber());
						accountLine.setCategory("Destin");
						accountLine.setVendorCode(trackingStatus.getDestinationSubAgentCode());
						accountLine.setEstimateVendorName(trackingStatus.getDestinationSubAgent());
						accountLine.setActgCode(trackingStatus.getActgCode()); 
						if(systemDefaultVatCalculation.trim().equalsIgnoreCase("true")){
							if(trackingStatus.getDestinationSubAgentCode()!=null && (!(trackingStatus.getDestinationSubAgentCode().toString().equals(""))) ){
								if(billing.getDestinationSubAgentVatCode()!=null && (!(billing.getDestinationSubAgentVatCode().toString().equals("")))){
									accountLine.setPayVatDescr(billing.getDestinationSubAgentVatCode());
					  				String payVatPercent="0";
					  				if(payVatPercentList.containsKey(billing.getDestinationSubAgentVatCode())){ 
					  					payVatPercent=payVatPercentList.get(billing.getDestinationSubAgentVatCode());
					  				}
					  				accountLine.setPayVatPercent(payVatPercent);	
								}
								
							}	
						}
						accountLine.setCreatedOn(new Date());
						accountLine.setCreatedBy(trackingStatus.getUpdatedBy());
						accountLine.setUpdatedOn(new Date());
						accountLine.setUpdatedBy(trackingStatus.getUpdatedBy());
						accountLine.setBasis("");
						if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
							String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
							try{  
								String roles[] = agentRoleValue.split("~");
							  if(roles[3].toString().equals("hauler")){
								   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
									   accountLine.setDivision("01");
								   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
									   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
										   accountLine.setDivision("01");
									   }else{ 
										   String divisionTemp="";
												if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
												divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
												}
												if(!divisionTemp.equalsIgnoreCase("")){
													accountLine.setDivision(divisionTemp);
												}else{
													accountLine.setDivision(null);
												}
									     
									   }
								   }else{
									   String divisionTemp="";
											if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
											divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
											}
											if(!divisionTemp.equalsIgnoreCase("")){
												accountLine.setDivision(divisionTemp);
											}else{
												accountLine.setDivision(null);
											}
								   }
							  }else{
								  String divisionTemp="";
										if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
										}
										if(!divisionTemp.equalsIgnoreCase("")){
											accountLine.setDivision(divisionTemp);
										}else{
											accountLine.setDivision(null);
										}
							  }
							}catch (Exception e1) {
								e1.printStackTrace();
							}
						}
						maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber());
						if ( maxLineNumber.get(0) == null ) {          
				        	accountLineNumber = "001";
				        }else {
				         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
				            if((autoLineNumber.toString()).length() == 2) {
				            	accountLineNumber = "0"+(autoLineNumber.toString());
				            }
				            else if((autoLineNumber.toString()).length() == 1) {
				            	accountLineNumber = "00"+(autoLineNumber.toString());
				            } else {
				            	accountLineNumber=autoLineNumber.toString();
				            }
				        }
				        accountLine.setAccountLineNumber(accountLineNumber);
				        accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
				        accountLine.setContract(billing.getContract());
				        accountLine.setNetworkBillToCode(billing.getNetworkBillToCode());
				        accountLine.setNetworkBillToName(billing.getNetworkBillToName());
				        accountLine.setBillToCode(billing.getBillToCode());
				        if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().equals("")))){
				    		accountLine.setRecVatDescr(billing.getPrimaryVatCode());
				    		if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().equals("")))){
					    		String recVatPercent="0";
					    		if(euVatPercentList.containsKey(billing.getPrimaryVatCode())){
						    		recVatPercent=euVatPercentList.get(billing.getPrimaryVatCode());
						    		}
								accountLine.setRecVatPercent(recVatPercent);
					    		}
				    		}
						accountLine.setBillToName(billing.getBillToName());
						String billingCurrency="";
						if(contractTypeForCurrency){
						 billingCurrency=serviceOrderManager.getBillingCurrencyValue(billing.getBillToCode(),sessionCorpID);
						}
						if(!billingCurrency.equalsIgnoreCase("") && contractTypeForCurrency)
						{
							accountLine.setRecRateCurrency(billingCurrency);
							accountLine.setEstSellCurrency(billingCurrency);
							accountLine.setRevisionSellCurrency(billingCurrency);
							accountLine.setRacValueDate(new Date());
							accountLine.setEstSellValueDate(new Date());
							accountLine.setRevisionSellValueDate(new Date());

							List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,billingCurrency);
							if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								try{
								accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								}catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								accountLine.setRecRateExchange(new BigDecimal(1.0000));
								accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
								}
							}else{
								accountLine.setRecRateExchange(new BigDecimal(1.0000));
								accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
							}
						}else{
						if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals("")){
							accountLine.setRecRateCurrency(baseCurrency);
							accountLine.setEstSellCurrency(baseCurrency);
							accountLine.setRevisionSellCurrency(baseCurrency);
							accountLine.setRacValueDate(new Date());
							accountLine.setEstSellValueDate(new Date());
							accountLine.setRevisionSellValueDate(new Date());
							accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
							accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
							accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
					 
						}else{
							accountLine.setRecRateCurrency(baseCurrencyCompanyDivision);
							accountLine.setEstSellCurrency(baseCurrencyCompanyDivision);
							accountLine.setRevisionSellCurrency(baseCurrencyCompanyDivision);
							accountLine.setRacValueDate(new Date());
							accountLine.setEstSellValueDate(new Date());
							accountLine.setRevisionSellValueDate(new Date());
							List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
							if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								try{
								accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
								}catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
				    				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
								}
							}else{
								accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
								accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
								accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
							}
						}
						}
						String vendorCurrency="";
						if(contractTypeForCurrency){
						vendorCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getVendorCode(),sessionCorpID);
						}
						if(!vendorCurrency.equalsIgnoreCase("") && contractTypeForCurrency)
						{
							accountLine.setEstCurrency(vendorCurrency);
							accountLine.setRevisionCurrency(vendorCurrency);
							accountLine.setCountry(vendorCurrency);
							accountLine.setEstValueDate(new Date());
							accountLine.setRevisionValueDate(new Date());
							accountLine.setValueDate(new Date());
							List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,vendorCurrency);
							if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								try{
								accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
								}catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
				    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
				    				accountLine.setExchangeRate(new Double(1.0));	
								}
							}else{
								accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
								accountLine.setExchangeRate(new Double(1.0));
							}
						}else{
						if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
						{
						accountLine.setEstCurrency(baseCurrency);
						accountLine.setRevisionCurrency(baseCurrency);	
						accountLine.setCountry(baseCurrency);
						accountLine.setEstValueDate(new Date());
						accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setRevisionValueDate(new Date());
						accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
						accountLine.setValueDate(new Date());
						accountLine.setExchangeRate(new Double(1.0));
						}
						else
						{
						accountLine.setEstCurrency(baseCurrencyCompanyDivision);
						accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
						accountLine.setCountry(baseCurrencyCompanyDivision);
						accountLine.setEstValueDate(new Date());
						accountLine.setRevisionValueDate(new Date());
						accountLine.setValueDate(new Date());
						List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
						if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
							try{
							accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
							accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
							}catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
								accountLine.setExchangeRate(new Double(1.0));	
							}
						}else{
							accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
							accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
							accountLine.setExchangeRate(new Double(1.0));
						}
						}
						}
				        accountLineManager.save(accountLine);
					}					
				}
				}
				List bookingAgentCode = billingManager.findBookingAgent(corpIDform);
				if((!(serviceOrder.getJob().toString().equals("LOG"))) && (!(serviceOrder.getJob().toString().equals("OFF")))&& (!(serviceOrder.getJob().toString().equals("FFG")))&& (!(serviceOrder.getJob().toString().equals("MLL")))&& (!(serviceOrder.getJob().toString().equals("ULL"))) ){
				 if(serviceOrder.getBookingAgentCode()!= null){
					if((!(bookingAgentCode.contains(serviceOrder.getBookingAgentCode())) && serviceOrder.getRouting()!=null && (serviceOrder.getRouting().equals("IMP") || serviceOrder.getRouting().equals("NRI") || serviceOrder.getRouting().equals("DIN")))){
						miscellaneousManager.updateBilling(trackingStatus.getDeliveryA(), id , getRequest().getRemoteUser());
						/*if(serviceOrder.getTargetRevenueRecognition() ==null) {
							serviceOrder.setTargetRevenueRecognition(trackingStatus.getDeliveryShipper());	
						}*/
					}else{
						miscellaneousManager.updateBilling(trackingStatus.getLoadA(), id , getRequest().getRemoteUser());
						/*if(serviceOrder.getTargetRevenueRecognition() ==null) {
							serviceOrder.setTargetRevenueRecognition(trackingStatus.getBeginLoad());	
						}*/
					}
				}
}
				if((!(serviceOrder.getJob().equals("STO")))&& (!(serviceOrder.getJob().equals("TPS")))&& (!(serviceOrder.getJob().equals("STF")))&& (!(serviceOrder.getJob().equals("STL"))))
				   {  
					   if((serviceOrder.getStatusNumber().intValue()<10) &&(trackingStatus.getPackA()!=null || trackingStatus.getLoadA()!=null)){
						   trackingStatus.getPackA();
						 }
					   if((serviceOrder.getStatusNumber().intValue()<20)&& trackingStatus.getSitOriginOn()!=null){
					   }
					   if((serviceOrder.getStatusNumber().intValue()<50)&& trackingStatus.getSitDestinationIn()!=null){
					   }
					   if((serviceOrder.getStatusNumber().intValue()<60)&& trackingStatus.getDeliveryA()!=null){
					   }
				   } 
				   serviceOrder.setBrokerCode(trackingStatus.getBrokerCode());
				   serviceOrder.setOriginAgentCode(trackingStatus.getOriginAgentCode());
				   serviceOrder.setOriginSubAgentCode(trackingStatus.getOriginSubAgentCode());
				   serviceOrder.setDestinationAgentCode(trackingStatus.getDestinationAgentCode());
				   serviceOrder.setDestinationSubAgentCode(trackingStatus.getDestinationSubAgentCode());
				   serviceOrder.setForwarderCode(trackingStatus.getForwarderCode());
				   customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				   if(trackingStatus.getDeliveryA()!=null || serviceOrder.getETA()!= null || trackingStatus.getDeliveryShipper()!= null){
					   trackingStatusManager.updateSalesStatus(customerFile.getSequenceNumber(),getRequest().getRemoteUser());
					   customerFile.setSalesStatus("Booked");
				   }
				   if(customerFile.getStatus()!=null){
				   	if((customerFile.getStatus().toString().equals("DWNLD")||customerFile.getStatus().toString().equals("DWND"))&& ((!(serviceOrder.getStatus().toString().equals("DWNLD")))||(serviceOrder.getStatusNumber().intValue()>=10 ))){
				   		customerFile.setStatus("NEW");
						customerFile.setStatusNumber(1);
						customerFile.setStatusDate(new Date());
						customerFile.setUpdatedBy(getRequest().getRemoteUser());
						customerFile.setUpdatedOn(new Date());
				   		customerFileManager.save(customerFile); 
				   	}
				   } 
				   if(!isNew){
				     statusNumberCountList=serviceOrderManager.getStatusNumberCount(serviceOrder.getSequenceNumber(),serviceOrder.getId(),corpIDform);
				   } 
				   if(isNew){
				       miscellaneous.setStatus("New");
				   	statusNumberCountList=serviceOrderManager.getStatusNumberCount(serviceOrder.getSequenceNumber(),corpIDform);	
				   }
				   if((serviceOrder.getStatusNumber().intValue()>=10 && serviceOrder.getStatusNumber().intValue()<=60)||(Integer.parseInt(statusNumberCountList.get(0).toString())>0)){
				   	customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				   	if(customerFile.getStatusNumber()<20){
				   		customerFile.setUpdatedBy(getRequest().getRemoteUser());
						customerFile.setUpdatedOn(new Date());
				   		customerFileManager.save(customerFile); 
				   	}
				   }
				   if(serviceOrder.getStatusNumber().intValue()==60 || serviceOrder.getStatusNumber().intValue()==70 ){
				   	boolean statusCheck=false;
				   	TreeSet statusNumberSet=new TreeSet();
				   	if(!isNew){
				   		statusNumberList=serviceOrderManager.getStatusNumberList(serviceOrder.getSequenceNumber(),corpIDform,serviceOrder.getId());	
				       }
				   	if(!statusNumberList.isEmpty()){
				   		Iterator it=statusNumberList.iterator();
				   	while(it.hasNext()){
				   	Integer number=(Integer)it.next();
				   	if((!(number.toString().equals("60"))) && (!(number.toString().equals("70")))){
				   		statusCheck=false;
				   		break;
				   	}
				   	else {
				   		statusNumberSet.add(number);
				   		statusCheck=true;
				   	}
				   	}
				   	if(statusCheck){
				   		if(serviceOrder.getStatusNumber().intValue()==60){
				   		if(	statusNumberSet.contains(70)){
				   			customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				           	if(customerFile.getStatusNumber()<40){
				           		customerFile.setUpdatedBy(getRequest().getRemoteUser());
				       			customerFile.setUpdatedOn(new Date());
				           		customerFileManager.save(customerFile); 
				           	} 
				   		}
				   		} 
				   		if(serviceOrder.getStatusNumber().intValue()==70){
				   			customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				           	if(customerFile.getStatusNumber()<40){
				           		customerFile.setUpdatedBy(getRequest().getRemoteUser());
				       			customerFile.setUpdatedOn(new Date());
				           		customerFileManager.save(customerFile); 
				   		}
				   	}
				   	} 
				   	} else {
				   		if(serviceOrder.getStatusNumber().intValue()==70){
				   			customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				           	if(customerFile.getStatusNumber()<40){
				           		customerFile.setUpdatedBy(getRequest().getRemoteUser());
				       			customerFile.setUpdatedOn(new Date());
				           		customerFileManager.save(customerFile); 
				   		}
				   	
				   	}
				   }
				   }
				   if(serviceOrder.getStatusNumber().intValue()==100 ){
				   	if(!isNew){
				   		statusNumberList=serviceOrderManager.getStatusNumberList(serviceOrder.getSequenceNumber(),corpIDform,serviceOrder.getId());	
				       }
				   	boolean statusCheck=false;
				   	TreeSet statusNumberSet=new TreeSet();
				   	if(!statusNumberList.isEmpty()){
				   		Iterator it=statusNumberList.iterator();
				       	while(it.hasNext()){
				       	Integer number=(Integer)it.next();
				       	if(!(number.toString().equals("100"))){
				       		statusCheck=false;
				       		break;
				       	}
				       	else {
				       		statusNumberSet.add(number);
				       		statusCheck=true;
				       	}
				       	}
				       	if(statusCheck){
				       		customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				       		if(customerFile.getStatusNumber()<200){
				           		customerFile.setStatus("HOLD");
				           		customerFile.setStatusNumber(200);
				           		customerFile.setStatusDate(new Date());
				           		customerFile.setUpdatedBy(getRequest().getRemoteUser());
				       			customerFile.setUpdatedOn(new Date());
				           		customerFileManager.save(customerFile); 
				   		}
				       		
				       	}
				   	}else {
				   		customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				       	if(customerFile.getStatusNumber()<200){
				       		customerFile.setStatus("HOLD");
				       		customerFile.setStatusNumber(200);
				       		customerFile.setStatusDate(new Date());
				       		customerFile.setUpdatedBy(getRequest().getRemoteUser());
				   			customerFile.setUpdatedOn(new Date());
				       		customerFileManager.save(customerFile); 
					}
				   		
				   	}
				   }
				   if((serviceOrder.getJob().equals("STO")|| serviceOrder.getJob().equals("TPS") || serviceOrder.getJob().equals("STF")|| serviceOrder.getJob().equals("STL"))){
				   	
				   	jobstatusNumberList=serviceOrderManager.getJobstatusNumberList(serviceOrder.getSequenceNumber(),corpIDform,serviceOrder.getId());
				   	 if(jobstatusNumberList.isEmpty()){
				   		if(!(serviceOrder.getStatus().trim().equals("CLSD"))){
				   			customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				           	if(customerFile.getStatusNumber()<50){
				           		customerFile.setStatus("STORG");
				           		customerFile.setStatusNumber(50);
				           		customerFile.setStatusDate(new Date());
				           		customerFile.setUpdatedBy(getRequest().getRemoteUser());
				       			customerFile.setUpdatedOn(new Date());
				           		customerFileManager.save(customerFile); 
				   			
				   		}
				   	}
				   	
				   	}
				   	if(!(jobstatusNumberList.isEmpty())){
				   		boolean statusCheck=false; 
				       		Iterator it=jobstatusNumberList.iterator();
				           	while(it.hasNext()){
				           	String condition=(String)it.next();
				           	String[] conditionArray=condition.split("#");
				           	String job=conditionArray[0];
				           	String stausNumber=conditionArray[1];
				           	String staus=conditionArray[2];
				           	if(((!(job.equals("STO")))&& (!(job.equals("TPS"))) && (!(job.equals("STF")))&& (!(job.equals("STL"))))&&(!(staus.equals("CLSD")))) {
				           	 statusCheck=false;
				           		break;
				           	}
				           	else { 
				           		statusCheck=true;
				           	} 
				   	      }
				   if(statusCheck){
				   	customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				   	if(customerFile.getStatusNumber()<50){
				   		customerFile.setUpdatedBy(getRequest().getRemoteUser());
						customerFile.setUpdatedOn(new Date());
				   		customerFileManager.save(customerFile); 
					
				}
				   } 
				   	}
				   }
				   if(((!(serviceOrder.getJob().equals("STO")))&& (!(serviceOrder.getJob().equals("TPS"))) && (!( serviceOrder.getJob().equals("STF")))&& (!(serviceOrder.getJob().equals("STL")))) &&(serviceOrder.getStatus().trim().equals("CLSD")) ){
				   	jobstatusNumberList=serviceOrderManager.getJobstatusNumberList(serviceOrder.getSequenceNumber(),corpIDform,serviceOrder.getId());	
				   	if(!(jobstatusNumberList.isEmpty())){
				   		boolean statusCheck=false; 
				       		Iterator it=jobstatusNumberList.iterator();
				           	while(it.hasNext()){
				           	String condition=(String)it.next();
				           	String[] conditionArray=condition.split("#");
				           	String job=conditionArray[0];
				           	String stausNumber=conditionArray[1];
				           	String staus=conditionArray[2];
				           	if(((!(job.equals("STO")))&& (!(job.equals("TPS"))) && (!(job.equals("STF")))&& (!(job.equals("STL"))))&&(!(staus.equals("CLSD")))) {
				           	 statusCheck=false;
				           		break;
				           	}
				           	else { 
				           		statusCheck=true;
				           	} 
				   	      }
				   if(statusCheck){
				   	customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
				   	if(customerFile.getStatusNumber()<50){
				   		customerFile.setUpdatedBy(getRequest().getRemoteUser());
						customerFile.setUpdatedOn(new Date());
				   		customerFileManager.save(customerFile); 
					
				}
				   } 
				   	}
				   }
				   
				   if(trackingStatus.getFlagCarrier()!=null){
					   serviceOrder.setServiceOrderFlagCarrier(trackingStatus.getFlagCarrier());
				   }
				   serviceOrder.setSupplementGBL(supplementGBL);
				   serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
				   serviceOrder.setUpdatedOn(new Date());
				   if(serviceOrder.getCorpID().toString().equalsIgnoreCase("STVF") && serviceOrder.getTransitDays()!=null && !serviceOrder.getTransitDays().equals("") && trackingStatus.getBeginLoad()!=null){
					   try {
						Date dt = new Date();
						   Calendar c = Calendar.getInstance(); 
						   c.setTime(trackingStatus.getBeginLoad()); 
						   c.add(Calendar.DATE, Integer.parseInt(serviceOrder.getTransitDays()));
						   dt = c.getTime();
						   serviceOrder.setGrpDeliveryDate(dt);
					} catch (Exception e) {
						e.printStackTrace();
					}
				   }
				   if(updaterIntegration!=null && updaterIntegration.equals("yes")){
						serviceOrder.setIsUpdater(false);
						}
				   serviceOrderManager.save(serviceOrder); 
				boolean contractType=trackingStatusManager.getContractType(sessionCorpID ,billing.getContract());
				try{
				if((buttonType.equalsIgnoreCase("LinkUpNetworkGroup"))&& (contractType)){
					trackingStatus.setSoNetworkGroup(true);
					trackingStatus.setAgentNetworkGroup(false);
					if(networkAgent){
						trackingStatus.setAccNetworkGroup(false);	
						trackingStatus.setUtsiNetworkGroup(true);
					}else{
					trackingStatus.setAccNetworkGroup(true);
					trackingStatus.setUtsiNetworkGroup(false);
					}
				}
				if((buttonType.equalsIgnoreCase("LinkUpBookingAgent"))&& (contractType)&& networkAgent){
					trackingStatus.setSoNetworkGroup(true);
					trackingStatus.setAgentNetworkGroup(false);
					trackingStatus.setAccNetworkGroup(false);	
				    trackingStatus.setUtsiNetworkGroup(true);
					 
				}
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					
				}
				
				try{
					if((autoPopulateActualDeliveryDateFlag!=null)&&(!autoPopulateActualDeliveryDateFlag.equalsIgnoreCase(""))&&(autoPopulateActualDeliveryDateFlag.equalsIgnoreCase("YES"))){
						serviceOrder.setUpdatedBy(getRequest().getRemoteUser());
				        serviceOrder.setUpdatedOn(new Date());
						serviceOrder=serviceOrderManager.save(serviceOrder);
					}	        	  
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				}
				 try{
					    if((serviceOrder.getGrpPref()!=null)&&(!serviceOrder.getGrpPref().equals(""))&&(serviceOrder.getGrpPref()==true)){
				        	if(serviceOrder.getGrpStatus()!=null){
				        		if((serviceOrder.getGrpStatus().indexOf("Finalized")>-1)){
				        			List<ServiceOrder> listByGrpId29=serviceOrderManager.findServiceOrderByGrpId(serviceOrder.getId().toString(),sessionCorpID);		
				                   	if(listByGrpId29!=null && !(listByGrpId29.isEmpty())){
				                   		for (ServiceOrder serviceOrder29 : listByGrpId29) {
				                   			TrackingStatus ts = trackingStatusManager.get(serviceOrder29.getId());
				                   			if (trackingStatus.getPackA() != null || trackingStatus.getLoadA() != null ) {
				                   				ts.setPackA(trackingStatus.getPackA());
				                       			ts.setLoadA(trackingStatus.getLoadA());
											}
				                   			serviceOrder29.setStatusDate(serviceOrder.getStatusDate());
				                   			ts.setSitOriginOn(trackingStatus.getSitOriginOn());
				                   			ts.setSitDestinationIn(trackingStatus.getSitDestinationIn()); 
				                   			ts.setBillLading(trackingStatus.getBillLading());
				                   			ts.setOriginBL(trackingStatus.getOriginBL());
				                   			trackingStatusManager.save(ts);
				                   			serviceOrderManager.save(serviceOrder29);
				                   		}}}}}
				 }catch(Exception e){
					 logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				 }
				 try{
					List<String> exSoList=new ArrayList();
					exSoList.add(trackingStatus.getDestinationAgentExSO());
					exSoList.add(trackingStatus.getOriginAgentExSO());
					exSoList.add(trackingStatus.getOriginSubAgentExSO());
					exSoList.add(trackingStatus.getDestinationSubAgentExSO());	
					exSoList.add(trackingStatus.getNetworkAgentExSO());	
					exSoList.add(trackingStatus.getBookingAgentExSO());	
					
					List<String> list = new ArrayList<String>(new HashSet<String>(exSoList));
					List<String> misMatchList = new ArrayList();
					for (String ship : list) {
						if(list.contains(ship) && list.contains("remove"+ship)){
							misMatchList.add(ship);
							misMatchList.add("remove"+ship);
						}
					}
					list.removeAll(misMatchList);
					checkRemoveLinks(list,trackingStatus);
					}catch(Exception ex){
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
				try{	
				if(trackingStatus.getDestinationAgentExSO()!=null && trackingStatus.getDestinationAgentExSO().toString().contains("remove"))
					trackingStatus.setDestinationAgentExSO("");	
				if(trackingStatus.getOriginAgentExSO()!=null && trackingStatus.getOriginAgentExSO().toString().contains("remove"))
					trackingStatus.setOriginAgentExSO("");
				if(trackingStatus.getOriginSubAgentExSO()!=null && trackingStatus.getOriginSubAgentExSO().toString().contains("remove"))
					trackingStatus.setOriginSubAgentExSO("");
				if(trackingStatus.getDestinationSubAgentExSO()!=null && trackingStatus.getDestinationSubAgentExSO().toString().contains("remove"))
					trackingStatus.setDestinationSubAgentExSO("");
				if(trackingStatus.getNetworkAgentExSO()!=null && trackingStatus.getNetworkAgentExSO().toString().contains("remove")){
					trackingStatus.setNetworkAgentExSO("");
				    trackingStatus.setSoNetworkGroup(false);
				    trackingStatus.setAccNetworkGroup(false);
				    trackingStatus.setUtsiNetworkGroup(false);
				    trackingStatus.setAgentNetworkGroup(false);
				}
}catch(Exception ex){
				logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
////// 14936/////

////////////////////Isha 14936///////////////////////////////////
try
{
	if(sessionCorpID.equalsIgnoreCase("JKMS"))
   {
  if((billing.getBillToCode().equalsIgnoreCase("T167293")|| billing.getBillToCode().equalsIgnoreCase("T185238")))
		  
		 
   {
  	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yyyy");
	  TrackingStatus tsNew=trackingStatusManager.get(trackingStatus.getId());
	  String coordinatorName="";
		String userMail="";
		String userSignature="";
		String coorinatorMail = "";
		String coorinatorSignature = "";
		String coorinatorLastName ="";
		 String msgText1 ="";
		 String userName="";
		 String subject = "";
		 String userFirstName="";
		 String userLasttName="";
		 String userTitle="";
		 String userPhoneNumber="";
		 coordinatorName = serviceOrder.getCoordinator();
			List<User> userCoorinator=userManager.findUserDetails(coordinatorName);
			if (!(userCoorinator == null || userCoorinator.isEmpty())) {
				for (User userCoorinator1 : userCoorinator) {
					coorinatorMail = "movemanagement@jkmoving.com";
					coorinatorSignature=userCoorinator1.getSignature();
					coorinatorLastName =userCoorinator1.getLastName();
					 userMail = userCoorinator1.getEmail();
					 userPhoneNumber=userCoorinator1.getPhoneNumber();
					 userSignature=userCoorinator1.getSignature();
					 userFirstName = userCoorinator1.getFirstName();
					 userLasttName = userCoorinator1.getLastName();
					 userTitle= userCoorinator1.getUserTitle();
					 userName = userFirstName +" "+userLasttName;
				}
			
			}
			
		 if((trackingStatus.getBeginPacking()!=null  && tsNew.getBeginPacking()!=null && trackingStatus.getBeginPacking().compareTo(tsNew.getBeginPacking())!=0)||(tsNew.getBeginPacking()==null && trackingStatus.getBeginPacking()!=null)){
	
	subject = "Packing date scheduled "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"/File: "+billing.getBillToReference();
	 StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(trackingStatus.getBeginPacking()));
	String dateValue = nowYYYYMMDD1.toString();
	msgText1 = "Attn: CapRelo Move Management Team "+"\n\n"+"The Packing date has been scheduled for "+dateValue+"\n\n Transferee: "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"\n\n File: #"+billing.getBillToReference()+"\n\n JK Ref: "+serviceOrder.getShipNumber()+"\n\n Origin City/State/Country: "+serviceOrder.getOriginCity()+"/"+serviceOrder.getOriginState()+"/"+serviceOrder.getOriginCountry()+"\n\n Destination City/State/Country: "+serviceOrder.getDestinationCity()+"/"+serviceOrder.getDestinationState()+"/"+serviceOrder.getDestinationCountry()+"\n\n Mode: "+serviceOrder.getMode()+"\n\n\n\n Best Regards, "+"\n\n "+userName+"\n\n "+userTitle +"\n\n JK Moving Services | GSA Department\n\n"+userPhoneNumber+"\n<a href='http://www.jkmoving.com'>http://www.jkmoving.com</a>\n\n";
	String JkmsLogo = "/usr/local/redskydoc/logo/JKMS_logo.png";
	String location="";
		if(JkmsLogo!=null && !JkmsLogo.equalsIgnoreCase("")){
			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"500px\" height=\"100px\"</br>";
			location = JkmsLogo+"#image1";
		}
	emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"autoemails@jkmoving.com","", location, msgText1,subject, sessionCorpID, "ServiceOrder",serviceOrder.getShipNumber(), "");

		}
		 else
		 {
		 }
		 
	if((trackingStatus.getBeginLoad()!=null  && tsNew.getBeginLoad()!=null && trackingStatus.getBeginLoad().compareTo(tsNew.getBeginLoad())!=0)||(tsNew.getBeginLoad()==null && trackingStatus.getBeginLoad()!=null)){
		subject = "Loading date has been scheduled "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"/File: "+billing.getBillToReference();
		 StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(trackingStatus.getBeginLoad()));
	 		String dateValue = nowYYYYMMDD1.toString();
		msgText1 = "Attn: CapRelo Move Management Team "+"\n\n"+"The loading date is scheduled for "+dateValue+"\n\n Transferee: "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"\n\n File: #"+billing.getBillToReference()+"\n\n JK Ref: "+serviceOrder.getShipNumber()+"\n\n Origin City/State/Country: "+serviceOrder.getOriginCity()+"/"+serviceOrder.getOriginState()+"/"+serviceOrder.getOriginCountry()+"\n\n Destination City/State/Country: "+serviceOrder.getDestinationCity()+"/"+serviceOrder.getDestinationState()+"/"+serviceOrder.getDestinationCountry()+"\n\n Mode: "+serviceOrder.getMode()+"\n\n\n\n Best Regards, "+"\n\n "+userName+"\n\n "+userTitle +"\n\n JK Moving Services | GSA Department\n\n"+userPhoneNumber+"\n<a href='http://www.jkmoving.com'>http://www.jkmoving.com</a>\n\n";
		String JkmsLogo = "/usr/local/redskydoc/logo/JKMS_logo.png";
		String location="";
 		if(JkmsLogo!=null && !JkmsLogo.equalsIgnoreCase("")){
 			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"500px\" height=\"100px\"</br>";
 			location = JkmsLogo+"#image1";
 		}
		emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"autoemails@jkmoving.com","", location, msgText1,subject, sessionCorpID, "ServiceOrder", serviceOrder.getShipNumber(), "");
	  }
	  else
	  {
	  }
	 if((loadA!=null  && tsNew.getLoadA()!=null && loadA.compareTo(tsNew.getLoadA())!=0)||(tsNew.getLoadA()==null && loadA!=null)){
		
			subject = "Actual load date -"+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"/File: "+billing.getBillToReference();
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(loadA));
		 	String dateValue = nowYYYYMMDD1.toString();
			msgText1 = "Attn: CapRelo Move Management Team "+"\n\n"+"The actual loading was done on  "+dateValue+"\n\n Transferee: "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"\n\n File: #"+billing.getBillToReference()+"\n\n JK Ref: "+serviceOrder.getShipNumber()+"\n\n Origin City/State/Country: "+serviceOrder.getOriginCity()+"/"+serviceOrder.getOriginState()+"/"+serviceOrder.getOriginCountry()+"\n\n Destination City/State/Country: "+serviceOrder.getDestinationCity()+"/"+serviceOrder.getDestinationState()+"/"+serviceOrder.getDestinationCountry()+"\n\n Mode: "+serviceOrder.getMode()+"\n\n\n\n Best Regards, "+"\n\n "+userName+"\n\n "+userTitle +"\n\n JK Moving Services | GSA Department\n\n"+userPhoneNumber+"\n<a href='http://www.jkmoving.com'>http://www.jkmoving.com</a>\n\n";
			String JkmsLogo = "/usr/local/redskydoc/logo/JKMS_logo.png";
			String location="";
	   		if(JkmsLogo!=null && !JkmsLogo.equalsIgnoreCase("")){
	   			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"500px\" height=\"100px\"</br>";
	   			location = JkmsLogo+"#image1";
	   		}
			emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"autoemails@jkmoving.com","", location, msgText1,subject, sessionCorpID, "ServiceOrder",serviceOrder.getShipNumber(), "");
		
	  }
	  else
	  {
	  }
	 if((deliveryA!=null  && tsNew.getDeliveryA()!=null && deliveryA.compareTo(tsNew.getDeliveryA())!=0)||(tsNew.getDeliveryA()==null && deliveryA!=null)){
		 subject = " Actual Delivery Date -"+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"/File: "+billing.getBillToReference();
		 StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(deliveryA));
  		String dateValue = nowYYYYMMDD1.toString();
 			msgText1 = "Attn: CapRelo Move Management Team "+"\n\n"+"Delivery and Unpacking was done on "+dateValue+"\n\n Transferee: "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"\n\n File: #"+billing.getBillToReference()+"\n\n JK Ref: "+serviceOrder.getShipNumber()+"\n\n Origin City/State/Country: "+serviceOrder.getOriginCity()+"/"+serviceOrder.getOriginState()+"/"+serviceOrder.getOriginCountry()+"\n\n Destination City/State/Country: "+serviceOrder.getDestinationCity()+"/"+serviceOrder.getDestinationState()+"/"+serviceOrder.getDestinationCountry()+"\n\n Mode: "+serviceOrder.getMode()+"\n\n\n\n Best Regards, "+"\n\n "+userName+"\n\n "+userTitle +"\n\n JK Moving Services | GSA Department\n\n"+userPhoneNumber+"\n<a href='http://www.jkmoving.com'>http://www.jkmoving.com</a>\n\n";
 			String JkmsLogo = "/usr/local/redskydoc/logo/JKMS_logo.png";
 			String location="";
 	   		if(JkmsLogo!=null && !JkmsLogo.equalsIgnoreCase("")){
 	   			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"500px\" height=\"100px\"</br>";
 	   			location = JkmsLogo+"#image1";
 	   		}
 			emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"autoemails@jkmoving.com","", location, msgText1,subject, sessionCorpID, "ServiceOrder", serviceOrder.getShipNumber(), "");
 			
   	  
	  }
	  else
	  {
				
	  }
	 if((trackingStatus.getSitOriginOn()!=null  && tsNew.getSitOriginOn()!=null && trackingStatus.getSitOriginOn().compareTo(tsNew.getSitOriginOn())!=0)||(tsNew.getSitOriginOn()==null && trackingStatus.getSitOriginOn()!=null)){
			
			subject = "Shipment into SIT Origin -"+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"/File: "+billing.getBillToReference();
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(trackingStatus.getSitOriginOn()));
  		String dateValue = nowYYYYMMDD1.toString();
 			msgText1 = "Attn: CapRelo Move Management Team "+"\n\n"+"Shipment was placed into SIT origin on  "+dateValue+"\n\n Transferee: "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"\n\n File: #"+billing.getBillToReference()+"\n\n JK Ref: "+serviceOrder.getShipNumber()+"\n\n Origin City/State/Country: "+serviceOrder.getOriginCity()+"/"+serviceOrder.getOriginState()+"/"+serviceOrder.getOriginCountry()+"\n\n Destination City/State/Country: "+serviceOrder.getDestinationCity()+"/"+serviceOrder.getDestinationState()+"/"+serviceOrder.getDestinationCountry()+"\n\n Mode: "+serviceOrder.getMode()+"\n\n\n\n Best Regards, "+"\n\n "+userName+"\n\n "+userTitle +"\n\n JK Moving Services | GSA Department\n\n"+userPhoneNumber+"\n<a href='http://www.jkmoving.com'>http://www.jkmoving.com</a>\n\n";
 			String JkmsLogo = "/usr/local/redskydoc/logo/JKMS_logo.png";
 			String location="";
 	   		if(JkmsLogo!=null && !JkmsLogo.equalsIgnoreCase("")){
 	   			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"500px\" height=\"100px\"</br>";
 	   			location = JkmsLogo+"#image1";
 	   		}
 			emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"autoemails@jkmoving.com","", location, msgText1,subject, sessionCorpID, "ServiceOrder",serviceOrder.getShipNumber(), "");
 		
 		
 	  }
 	  else
 	  {
 	  }
	 if((trackingStatus.getSitDestinationIn()!=null  && tsNew.getSitDestinationIn()!=null && trackingStatus.getSitDestinationIn().compareTo(tsNew.getSitDestinationIn())!=0)||(tsNew.getSitDestinationIn()==null && trackingStatus.getSitDestinationIn()!=null)){
			subject = "Shipment into SIT Destnation -"+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"/File: "+billing.getBillToReference();
  		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(trackingStatus.getSitDestinationIn()));
  		String dateValue = nowYYYYMMDD1.toString();
  		
 			msgText1 = "Attn: CapRelo Move Management Team "+"\n\n"+"Shipment was placed into SIT destination on  "+dateValue+"\n\n Transferee: "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"\n\n File: #"+billing.getBillToReference()+"\n\n JK Ref: "+serviceOrder.getShipNumber()+"\n\n Origin City/State/Country: "+serviceOrder.getOriginCity()+"/"+serviceOrder.getOriginState()+"/"+serviceOrder.getOriginCountry()+"\n\n Destination City/State/Country: "+serviceOrder.getDestinationCity()+"/"+serviceOrder.getDestinationState()+"/"+serviceOrder.getDestinationCountry()+"\n\n Mode: "+serviceOrder.getMode()+"\n\n\n\n Best Regards, "+"\n\n "+userName+"\n\n "+userTitle +"\n\n JK Moving Services | GSA Department\n\n"+userPhoneNumber+"\n<a href='http://www.jkmoving.com'>http://www.jkmoving.com</a>\n\n";
 			String JkmsLogo = "/usr/local/redskydoc/logo/JKMS_logo.png";
 			String location="";
 	   		if(JkmsLogo!=null && !JkmsLogo.equalsIgnoreCase("")){
 	   			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"500px\" height=\"100px\"</br>";
 	   			location = JkmsLogo+"#image1";
 	   		}
 			emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"autoemails@jkmoving.com","", location, msgText1,subject, sessionCorpID, "ServiceOrder", serviceOrder.getShipNumber(), "");
 		
 		
   	  
 	  }
 	  else
 	  {
 	  }
	 if((trackingStatus.getSitOriginA()!=null  && tsNew.getSitOriginA()!=null && trackingStatus.getSitOriginA().compareTo(tsNew.getSitOriginA())!=0)||(tsNew.getSitOriginA()==null && trackingStatus.getSitOriginA()!=null)){
			
			subject = "Shipment scheduled out of SIT Origin -"+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"/File: "+billing.getBillToReference();
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(trackingStatus.getSitOriginA()));
	  		String dateValue = nowYYYYMMDD1.toString();
			msgText1 = "Attn: CapRelo Move Management Team "+"\n\n"+"Shipment scheduled out of SIT Origin on  "+dateValue+"\n\n Transferee: "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"\n\n File: #"+billing.getBillToReference()+"\n\n JK Ref: "+serviceOrder.getShipNumber()+"\n\n Origin City/State/Country: "+serviceOrder.getOriginCity()+"/"+serviceOrder.getOriginState()+"/"+serviceOrder.getOriginCountry()+"\n\n Destination City/State/Country: "+serviceOrder.getDestinationCity()+"/"+serviceOrder.getDestinationState()+"/"+serviceOrder.getDestinationCountry()+"\n\n Mode: "+serviceOrder.getMode()+"\n\n\n\n Best Regards, "+"\n\n "+userName+"\n\n "+userTitle +"\n\n JK Moving Services | GSA Department\n\n"+userPhoneNumber+"\n<a href='http://www.jkmoving.com'>http://www.jkmoving.com</a>\n\n";
 			String JkmsLogo = "/usr/local/redskydoc/logo/JKMS_logo.png";
 			String location="";
 	   		if(JkmsLogo!=null && !JkmsLogo.equalsIgnoreCase("")){
 	   			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"500px\" height=\"100px\"</br>";
 	   			location = JkmsLogo+"#image1";
 	   		}
 			emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"autoemails@jkmoving.com","", location, msgText1,subject, sessionCorpID, "ServiceOrder", serviceOrder.getShipNumber(), "");
 		 
 	  }
 	  else
 	  {
 				
 	  }
	 if((trackingStatus.getSitDestinationA()!=null  && tsNew.getSitDestinationA()!=null && trackingStatus.getSitDestinationA().compareTo(tsNew.getSitDestinationA())!=0)||(tsNew.getSitDestinationA()==null && trackingStatus.getSitDestinationA()!=null)){
			
 			subject = "Shipment scheduled out of SIT destination -"+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"/File: "+billing.getBillToReference();
 			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(trackingStatus.getSitDestinationA()));
	  		String dateValue = nowYYYYMMDD1.toString();
 			msgText1 = "Attn: CapRelo Move Management Team "+"\n\n"+"Shipment is scheduled out of SIT destination on  "+dateValue+"\n\n Transferee: "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"\n\n File: #"+billing.getBillToReference()+"\n\n JK Ref: "+serviceOrder.getShipNumber()+"\n\n Origin City/State/Country: "+serviceOrder.getOriginCity()+"/"+serviceOrder.getOriginState()+"/"+serviceOrder.getOriginCountry()+"\n\n Destination City/State/Country: "+serviceOrder.getDestinationCity()+"/"+serviceOrder.getDestinationState()+"/"+serviceOrder.getDestinationCountry()+"\n\n Mode: "+serviceOrder.getMode()+"\n\n\n\n Best Regards, "+"\n\n "+userName+"\n\n "+userTitle +"\n\n JK Moving Services | GSA Department\n\n"+userPhoneNumber+"\n<a href='http://www.jkmoving.com'>http://www.jkmoving.com</a>\n\n";
 			String JkmsLogo = "/usr/local/redskydoc/logo/JKMS_logo.png";
 			String location="";
 	   		if(JkmsLogo!=null && !JkmsLogo.equalsIgnoreCase("")){
 	   			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"500px\" height=\"100px\"</br>";
 	   			location = JkmsLogo+"#image1";
 	   		}
 			emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"autoemails@jkmoving.com","", location, msgText1,subject, sessionCorpID, "ServiceOrder", serviceOrder.getShipNumber(), "");
	 }
 	  else
 	  {
 	  }
	 if((trackingStatus.getDeliveryShipper()!=null  && tsNew.getDeliveryShipper()!=null && trackingStatus.getDeliveryShipper().compareTo(tsNew.getDeliveryShipper())!=0)||(tsNew.getDeliveryShipper()==null && trackingStatus.getDeliveryShipper()!=null)){
			subject = "Delivery and unpacking date has been scheduled -"+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"/File: "+billing.getBillToReference();
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(trackingStatus.getDeliveryShipper()));
	  		String dateValue = nowYYYYMMDD1.toString();
			msgText1 = "Attn: CapRelo Move Management Team "+"\n\n"+"Delivery and unpacking has been scheduled for  "+dateValue+"\n\n Transferee: "+serviceOrder.getFirstName()+" "+serviceOrder.getLastName()+"\n\n File: #"+billing.getBillToReference()+"\n\n JK Ref: "+serviceOrder.getShipNumber()+"\n\n Origin City/State/Country: "+serviceOrder.getOriginCity()+"/"+serviceOrder.getOriginState()+"/"+serviceOrder.getOriginCountry()+"\n\n Destination City/State/Country: "+serviceOrder.getDestinationCity()+"/"+serviceOrder.getDestinationState()+"/"+serviceOrder.getDestinationCountry()+"\n\n Mode: "+serviceOrder.getMode()+"\n\n\n\n Best Regards, "+"\n\n "+userName+"\n\n "+userTitle +"\n\n JK Moving Services | GSA Department\n\n"+userPhoneNumber+"\n<a href='http://www.jkmoving.com'>http://www.jkmoving.com</a>\n\n";
 			String JkmsLogo = "/usr/local/redskydoc/logo/JKMS_logo.png";
 			String location="";
 	   		if(JkmsLogo!=null && !JkmsLogo.equalsIgnoreCase("")){
 	   			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"500px\" height=\"100px\"</br>";
 	   			location = JkmsLogo+"#image1";
 	   		}
 			emailSetupManager.globalEmailSetupProcess(userMail,coorinatorMail,"autoemails@jkmoving.com","", location, msgText1,subject, sessionCorpID, "ServiceOrder", serviceOrder.getShipNumber(), "");
 		
   	  
 	  }
 	  else
 	  {
 				
 	  }

}}}
catch(Exception e){
logger.error("Exception Occour: "+ e.getStackTrace()[0]);
String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
String logMethod =   e.getStackTrace()[0].getMethodName().toString();
errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
e.printStackTrace();
return "errorlog";
}


               if(usGovJobs.indexOf(serviceOrder.getJob())>=0){
            	   trackingStatus.setMailClaim(trackingStatus.getDeliveryA());
                }
				trackingStatus=trackingStatusManager.save(trackingStatus); 
				hitFlag="1";
				
				 try {
						if(serviceOrder != null && serviceOrder.getId() != null){
							Map<String,String> serviceMap = refMasterManager.findByProperty(sessionCorpID, "SERVICE",serviceOrder.getServiceType());
							if(serviceMap != null && serviceMap.size() > 0){
								String name = serviceMap.get(serviceOrder.getServiceType());
								if(name != null && !"".equals(name)){
									name = name.replace(".",",");
									String[] st = name.split(",");
									Date date = serviceOrderManager.findByProperty(id, st[0],st[1],serviceOrder.getShipNumber());
								}
							}
						}
					} catch (Exception e1) {
						logger.error("Exception Occour: "+ e1.getStackTrace()[0]);
					}
				
				try{
				isNetworkBookingAgent=getIsNetworkBookingAgent(sessionCorpID, serviceOrder.getBookingAgentCode());
				isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getDestinationAgentCode() );
				isNetworkOriginAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getOriginAgentCode() );
				isNetworkOriginSubAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getOriginSubAgentCode() );
				isNetworkDestinationSubAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getDestinationSubAgentCode() );
				isNetworkNetworkPartnerCode=getIsNetworkAgent(sessionCorpID,trackingStatus.getNetworkPartnerCode() ); 
				isNetworkBookingAgentCode=getIsNetworkAgent(sessionCorpID,serviceOrder.getBookingAgentCode() );
				try{
				containerNumberList=cartonManager.getContainerNumber(serviceOrder.getShipNumber());
				if (containerNumberList == null) {
					containerNumberList = new ArrayList(); 
				}
				}
				catch(NullPointerException np)
				{
				  logger.error("Exception Occour: "+ np.getStackTrace()[0]);
				  String logMessage =  "Exception:"+ np.toString()+" at #"+np.getStackTrace()[0].toString();
      	    	  String logMethod =   np.getStackTrace()[0].getMethodName().toString();
      	    	  np.printStackTrace();
				}
				catch(Exception exc){
					logger.error("Exception Occour: "+exc.getStackTrace()[0]);
					  String logMessage =  "Exception:"+ exc.toString()+" at #"+exc.getStackTrace()[0].toString();
	      	    	  String logMethod =   exc.getStackTrace()[0].getMethodName().toString();
	      	    	  exc.printStackTrace();
				}
				Boolean isNetworkedCustomerFile=getIsNetworkedCustomerFile(customerFile.getId());
				
				List linkedShipNumberList=new ArrayList();
				if(buttonType.equalsIgnoreCase("LinkUpDest")){
				if(isNetworkDestinationAgent){
					if(isNetworkedCustomerFile){
						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),trackingStatus.getDestinationAgentCode() );
						createExternaleEntriesDestination(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile);	
					}else{
						createExternaleEntriesDestination(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile);
					}
						
				}
				if(trackingStatus.getDestinationAgentExSO()!=null && !trackingStatus.getDestinationAgentExSO().equalsIgnoreCase("")){
					linkedShipNumberList.add(trackingStatus.getDestinationAgentExSO());
				}
				}
				if(buttonType.equalsIgnoreCase("LinkUpOrigin")){
				if(isNetworkOriginAgent){
					if(isNetworkedCustomerFile){
						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),trackingStatus.getOriginAgentCode() );
						createExternaleEntriesOrigin(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile);
					}else{
						createExternaleEntriesOrigin(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile);	
					}
					
				}
				if(trackingStatus.getOriginAgentExSO()!=null && !trackingStatus.getOriginAgentExSO().equalsIgnoreCase("")){
					linkedShipNumberList.add(trackingStatus.getOriginAgentExSO());
				}
				}
				if(buttonType.equalsIgnoreCase("LinkUpOrigSub")){
				if(isNetworkOriginSubAgent){
					if(isNetworkedCustomerFile){
						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),trackingStatus.getOriginSubAgentCode() );
						createExternaleEntriesOriginSub(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile);
					}else{
						createExternaleEntriesOriginSub(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile);	
					}
					
				}
				if(trackingStatus.getOriginSubAgentExSO()!=null && !trackingStatus.getOriginSubAgentExSO().equalsIgnoreCase("")){
					linkedShipNumberList.add(trackingStatus.getOriginSubAgentExSO());
				}
				}
				if(buttonType.equalsIgnoreCase("LinkUpDestSub")){
				if(isNetworkDestinationSubAgent){
					if(isNetworkedCustomerFile){
						String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),trackingStatus.getDestinationSubAgentCode() );
						createExternaleEntriesDestinationSub(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile);
					}else{
						createExternaleEntriesDestinationSub(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile);
					}
				  }
				if(trackingStatus.getDestinationSubAgentExSO()!=null && !trackingStatus.getDestinationSubAgentExSO().equalsIgnoreCase("") ){
					linkedShipNumberList.add(trackingStatus.getDestinationSubAgentExSO());
				}
				}
				if((buttonType.equalsIgnoreCase("LinkUpNetworkGroup"))&& (contractType)){
					if(isNetworkNetworkPartnerCode){
						if(isNetworkedCustomerFile){
							String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),trackingStatus.getNetworkPartnerCode() );
							createExternaleEntriesNetworkPartnerCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile);
						}else{
							createExternaleEntriesNetworkPartnerCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile);	
						}
						
					}
					if(trackingStatus.getNetworkAgentExSO()!=null && !trackingStatus.getNetworkAgentExSO().equalsIgnoreCase("")){
						linkedShipNumberList.add(trackingStatus.getNetworkAgentExSO());
					}
					}
				
				if((buttonType.equalsIgnoreCase("LinkUpBookingAgent"))&& (contractType) && networkAgent ){

					if(isNetworkBookingAgentCode){
						if(isNetworkedCustomerFile){
							String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),serviceOrder.getBookingAgentCode() );
							createExternaleEntriesBookingAgentCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile);
						}else{
							createExternaleEntriesBookingAgentCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile);	
						}
					}
					if(trackingStatus.getBookingAgentExSO()!=null && !trackingStatus.getBookingAgentExSO().equalsIgnoreCase("")){
						linkedShipNumberList.add(trackingStatus.getBookingAgentExSO());
					}
				}
				
				if((buttonType.equalsIgnoreCase("LinkUpUGWWNetworkGroup"))){
					if(isNetworkNetworkPartnerCode){
						if(isNetworkedCustomerFile){
							String linkedCustomerFileNumber=getLinkedCustomerFileNumber(customerFile.getSequenceNumber(),trackingStatus.getNetworkPartnerCode() );
							createExternaleEntriesUGWWNetworkPartnerCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,linkedCustomerFileNumber,isNetworkedCustomerFile);
						}else{
							createExternaleEntriesUGWWNetworkPartnerCode(serviceOrder,trackingStatus,customerFile, billing, miscellaneous,"",isNetworkedCustomerFile);	
						}
						
					}
					if(trackingStatus.getNetworkAgentExSO()!=null && !trackingStatus.getNetworkAgentExSO().equalsIgnoreCase("")){
						linkedShipNumberList.add(trackingStatus.getNetworkAgentExSO());
					}
					}
				
				
				
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
				try{
				if(serviceOrder.getIsNetworkRecord()){
					List linkedShipNumber=findLinkerShipNumber(serviceOrder);
					List<Object> miscellaneousRecords=findMiscellaneousRecords(linkedShipNumber,serviceOrder);
					synchornizeMiscellaneous(miscellaneousRecords,miscellaneous,trackingStatus);
					List<Object> trackingStatusRecords=findTrackingStatusRecords(linkedShipNumber,serviceOrder);
					synchornizeTrackingStatus(trackingStatusRecords,trackingStatus,serviceOrder);
					List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
					synchornizeServiceOrder(serviceOrderRecords,serviceOrder,trackingStatus,customerFile);
					 
				}
				Long StartTime = System.currentTimeMillis();
				if(trackingStatus.getOriginAgentCode()!=null && !trackingStatus.getOriginAgentCode().equals("")) {
				List companyDetailsList=companyDivisionManager.findCompanyCodeByBookingAgent(trackingStatus.getOriginAgentCode(), sessionCorpID);
				if(serviceOrder.getRegistrationNumber()!=null && !serviceOrder.getRegistrationNumber().equals("") && serviceOrder.getJob().toString().trim().equalsIgnoreCase("INT") && serviceOrder.getCorpID().toString().trim().equalsIgnoreCase("HOLL") 
						&& companyDetailsList!=null && !companyDetailsList.isEmpty() && companyDetailsList.get(0)!=null){
	    			customerFileAction.createIntegrationLogInfo(serviceOrder.getShipNumber(),"CREATE","TSO");
	            }
				}
				if(trackingStatus.getDestinationAgentCode()!=null && !trackingStatus.getDestinationAgentCode().equals("")) {
					List companyDetailsList=companyDivisionManager.findCompanyCodeByBookingAgent(trackingStatus.getDestinationAgentCode(), sessionCorpID);	
				if(serviceOrder.getRegistrationNumber()!=null && !serviceOrder.getRegistrationNumber().equals("") && serviceOrder.getJob().toString().trim().equalsIgnoreCase("INT") && serviceOrder.getCorpID().toString().trim().equalsIgnoreCase("HOLL") 
						&& companyDetailsList!=null && !companyDetailsList.isEmpty() && companyDetailsList.get(0)!=null){
	    			customerFileAction.createIntegrationLogInfo(serviceOrder.getShipNumber(),"CREATE","TSD");
	            }
				}
				Long timeTaken =  System.currentTimeMillis() - StartTime;
     
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
				try{
					if(serviceOrder.getMode() !=null && serviceOrder.getMode().equalsIgnoreCase("Air") && trackingStatus.getPackDone()!=null){
						boolean checkEmailAlertSent=trackingStatusManager.checkEmailAlertSent(trackingStatus.getForwarderCode(), sessionCorpID);
						if(checkEmailAlertSent==true){
							String emailAdd = trackingStatus.getForwarderEmail();
						
							String from = "info@sscw.com";
							
							String subject="";
							String tempRecipient="";
							String tempRecipientArr[]=emailAdd.split(",");
							for(String str1:tempRecipientArr){
								if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
									if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
									tempRecipient += str1;
								}
							}
							emailAdd=tempRecipient;
							subject="Shipment Ready Date entered for  '" + serviceOrder.getShipNumber() + "'";
							String messageText1 = "Shipment # '" + serviceOrder.getShipNumber() + "' will be ready by --->  "+trackingStatus.getPackDone();
							
							 emailSetupManager.globalEmailSetupProcess(from, emailAdd, "", "", "", messageText1, subject, sessionCorpID,"",serviceOrder.getShipNumber(),"");
						}
					}
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
        			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
      	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
      	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
      	    	 e.printStackTrace();
      	    	  return "errorlog";
				}

				
			findSalesStatus = trackingStatusManager.findSalesStatus(trackingStatus.getSequenceNumber());
				if(!(findSalesStatus.isEmpty()) && (findSalesStatus!=null)){
					trackingStatusManager.updateSalesStatus(trackingStatus.getSequenceNumber(),getRequest().getRemoteUser());
				}			
				trackingStatusManager.updateTrack(id,trackingStatus.getBillLading(),trackingStatus.getHbillLading());
				if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
					if((coordinatorEvalValidation.equals("Y")) && (trackingStatus.getDeliveryA()!=null) && (serviceOrder.getOaCoordinatorEval().equals("")) && (serviceOrder.getDaCoordinatorEval().equals(""))){
					String key = (isNew) ? "Service order has been added Successfully." + "<br><br>" + "Please fill Coord Eval.OA and Coord Eval.DA in Customer Feedback of S/O Details" : "Service order has been updated Successfully" +"<br><br>" + "Please fill Coord Eval.OA and Coord Eval.DA in Customer Feedback of S/O Details";
					saveMessage(getText(key));
					}
					else if((coordinatorEvalValidation.equals("Y")) && (trackingStatus.getDeliveryA()!=null) && (serviceOrder.getOaCoordinatorEval().equals(""))){
						String key = (isNew) ? "Service order has been added Successfully." + "<br><br>" + "Please fill Coord Eval.OA in Customer Feedback of S/O Details" : "Service order has been updated Successfully" +"<br><br>" + "Please fill Coord Eval.OA in Customer Feedback of S/O Details";
						saveMessage(getText(key));
						}
					else if((coordinatorEvalValidation.equals("Y")) && (trackingStatus.getDeliveryA()!=null) && (serviceOrder.getDaCoordinatorEval().equals(""))){
						String key = (isNew) ? "Service order has been added Successfully." + "<br><br>" + "Please fill Coord Eval.DA in Customer Feedback of S/O Details" : "Service order has been updated Successfully" +"<br><br>" + "Please fill Coord Eval.DA in Customer Feedback of S/O Details";
						saveMessage(getText(key));
						}
					else{
						String key = (isNew) ? " Service order has been added Successfully " : "Service order has been updated Successfully";
						saveMessage(getText(key));
						
					}
				}
				if(defaultVendorAccLine == true){
					if(accLineListOrigin.isEmpty()){
						agentStatusOrigin = "editable";
					}else{
						agentStatusOrigin = "editable";
					}
					if(accLineListDestination.isEmpty()){
						agentStatusDestination = "editable";
					}else{
						agentStatusDestination = "editable";
					}
				}
				
				shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				getNotesForIconChange();
				
				if(sessionCorpID.equalsIgnoreCase("STVF") || sessionCorpID.equalsIgnoreCase("RSKY")){/*
					Date d = new Date();
					Date d1 = new Date();
					d1.setTime(d.getTime() - 5 * 24 * 60 * 60 * 1000);
					if(((customerFile.getBookingAgentCode()!=null && !customerFile.getBookingAgentCode().equalsIgnoreCase("")) 
							&& (customerFile.getBookingAgentCode().equalsIgnoreCase("T10509")) && serviceOrder.getJob().equals("GST")) 
							&& ((customerFile.getMoveType()!=null && !customerFile.getMoveType().equalsIgnoreCase("")) 
									&& (trackingStatus.getPackA()==null || (trackingStatus.getPackA().after(d1) || trackingStatus.getPackA().compareTo(d1)==0)) 
									&& (customerFile.getMoveType().equalsIgnoreCase("BookedMove")) && (customerFile.getWelcomeEmailOn()==null))){
						if((customerFile.getEmail()!=null && !customerFile.getEmail().equalsIgnoreCase("")) || (serviceOrder.getEmail()!=null && !serviceOrder.getEmail().equalsIgnoreCase(""))){
							if((trackingStatus.getOriginAgent()!=null && !trackingStatus.getOriginAgent().equalsIgnoreCase("")) && ((serviceOrder.getServiceType()!=null && !serviceOrder.getServiceType().equalsIgnoreCase("")) && (serviceOrder.getServiceType().equalsIgnoreCase("D/OW") || serviceOrder.getServiceType().equalsIgnoreCase("D/D")))){
								welcomeMailWithAttachementForOriginAgent();
							}
							if((trackingStatus.getDestinationAgent()!=null && !trackingStatus.getDestinationAgent().equalsIgnoreCase("")) && ((serviceOrder.getServiceType()!=null && !serviceOrder.getServiceType().equalsIgnoreCase("")) && (serviceOrder.getServiceType().equalsIgnoreCase("W/D")))){
								welcomeMailWithAttachementForDestinationAgent();
							}
						}
					}
				*/}
				if (!isNew) {   
				maxTrac = Long.parseLong(trackingStatusManager.findMaximumId().get(0).toString());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				miscellaneous = trackingStatus.getMiscellaneous();
					return INPUT;   
				}else {   
				maxTrac = Long.parseLong(trackingStatusManager.findMaximumId().get(0).toString());
				trackingStatus = trackingStatusManager.get(maxTrac);
				maxMisc = Long.parseLong(miscellaneousManager.findMaximumId().get(0).toString());
				miscellaneous = miscellaneousManager.get(maxMisc); 
				}
				if(sessionCorpID.equalsIgnoreCase("SSCW")){
					try
					{
					}catch(Exception ex)
					{
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
					
				}
				
				shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
				countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
			} catch (Exception e) {
				 e.printStackTrace();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 return "errorlog";
			}
	       
	       
	        
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
		}
		
		@SkipValidation
	   	public String welcomeMailWithAttachementForOriginAgent() throws AddressException, MessagingException {
	    	try {
	       	String subject = "";
	   	    String msgText1 ="";
	   	    
	   	    msgText1 = "<br>Hello "+((customerFile.getPrefix()==null || customerFile.getPrefix().equalsIgnoreCase(""))?"":customerFile.getPrefix()+" ")+((customerFile.getFirstName()==null || customerFile.getFirstName().equalsIgnoreCase(""))?"":customerFile.getFirstName()+" ")+((customerFile.getLastName()==null || customerFile.getLastName().equalsIgnoreCase(""))?"":customerFile.getLastName())+",<br>";
	       	msgText1=msgText1 +"<br>We are excited to advise you that Stevens Worldwide (SVLM) has been selected to provide services for your upcoming relocation.";
	       	msgText1=msgText1 +"<br>I will be your main point of contact during the packing process.";
	       	msgText1=msgText1 +"<br>My contact information is listed below, please feel free to reach out to me at any time.";
	       	msgText1=msgText1 +"<br><br>I have arranged for you to be contacted by my local agent, "+trackingStatus.getOriginAgent()+" to schedule your survey and confirm your pack date.";
	       	msgText1=msgText1 +"<br>Authorizations/Entitlements have yet to be provided by State and you will be advised as soon as we have them confirmed.";
	       	msgText1=msgText1 +"<br><br>We have attached some information that we hope you will find useful:";
	   		msgText1=msgText1 +"<br>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;International Moving Guide";
	   		msgText1=msgText1 +"<br>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GSA Rights and Responsibilities";
	   		msgText1=msgText1 +"<br><br>Please be sure to put valuables, stocks, bonds, and all cash in a safe place prior to your pack date, as these items are not authorized to be placed in your shipment.";
	   		msgText1=msgText1 +"<br>Some countries have specific guidelines on the importation of alcohol within household good shipments and you may find more information related to this.";
	   		msgText1=msgText1 +"<br><br>Please let me know if you have any questions along the way. I look forward to working with you.";
	   		
			User usr;
			if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equalsIgnoreCase("")){
				usr = (User)userManager.getUserByUsername(customerFile.getCoordinator());
			}else{
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				usr = (User) auth.getPrincipal();
			}
			msgText1=msgText1 +"<br><br>Best regards,";
	   		if(customerFile.getCoordinatorName()!=null){
	   			msgText1 = msgText1 +"<br>"+customerFile.getCoordinatorName();
	   		}
	   		msgText1=msgText1 +"<br>Ph No:-"+usr.getPhoneNumber();
	   		msgText1=msgText1 +"<br>Email:-"+usr.getEmail();
	   		
	   		String StevensLogo = "/usr/local/redskydoc/userData/images/stevens-vanline-WelcomeMail.jpg";
	   		if(StevensLogo!=null && !StevensLogo.equalsIgnoreCase("")){
	   			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"200px\" height=\"100px\"</br>";
	   		}
	   		String pdf1 = "/usr/local/redskydoc/userData/images/GSA_Guide_LR.pdf";
	   		String pdf2 = "/usr/local/redskydoc/userData/images/Relocating_Employees_Rights_and_Responsibilities_for_Household_Goods_Services_FINAL.pdf";
	   		String location="";
	   		if(StevensLogo!=null && !StevensLogo.equalsIgnoreCase("")){
	   			location = StevensLogo+"#image1"+"~"+pdf1+"~"+pdf2;
	   		}else{
	   			location = pdf1+"~"+pdf2;
	   		}
	   		
	   		String from;
   			if ((usr.getEmail() != null) && (!usr.getEmail().equalsIgnoreCase(""))) { 
   				from = usr.getEmail();
   			}else{
   				from = "support@redskymobility.com";	
   			}
   			subject = "Your Upcoming Relocation to "+customerFile.getDestinationCity()+","+customerFile.getDestinationState()+","+customerFile.getDestinationCountry()+"";
	   	    msgText1 = "<html><table><font face='Tahoma' size='2'>"+msgText1+"</font></table></html>";
	   	    String emailTo="";
	   	     if(customerFile.getEmail()!=null && ! customerFile.getEmail().equalsIgnoreCase("") 
	   	    		 && customerFile.getEmail2()!=null && ! customerFile.getEmail2().equalsIgnoreCase("") ){
	   	    	 emailTo=customerFile.getEmail()+","+customerFile.getEmail2(); 
	   	     }else if(serviceOrder.getEmail()!=null && ! serviceOrder.getEmail().equalsIgnoreCase("") 
	   	    		 && serviceOrder.getEmail2()!=null && ! serviceOrder.getEmail2().equalsIgnoreCase("") ){
	    		 emailTo=serviceOrder.getEmail()+","+customerFile.getEmail2(); 
	    	 }else if(customerFile.getEmail()!=null && ! customerFile.getEmail().equalsIgnoreCase("")){
	   	    	  emailTo=customerFile.getEmail(); 
	   	     }else if(serviceOrder.getEmail()!=null && ! serviceOrder.getEmail().equalsIgnoreCase("")){
	   	    	 emailTo=serviceOrder.getEmail();
	   	     }
	   		
	   		customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());
	   		customerFile.setUpdatedBy(getRequest().getRemoteUser());
	   		customerFile.setUpdatedOn(new Date());
	   		customerFile.setWelcomeEmailOn(new Date());
	   		customerFileManager.save(customerFile);
	   		
	   		emailSetupManager.globalEmailSetupProcess(from, emailTo, usr.getEmail(), "", location, msgText1, subject, sessionCorpID,"CfBookedMove",customerFile.getSequenceNumber(),"");
	    	} catch (Exception e) {
				e.printStackTrace();
			}
	   		return "SUCCESS";
	       }
		
		@SkipValidation
	   	public String welcomeMailWithAttachementForDestinationAgent() throws AddressException, MessagingException {
	    	try {
	       	String subject = "";
	   	    String msgText1 ="";
	   	    
	   	    msgText1 = "<br>Hello "+((customerFile.getPrefix()==null || customerFile.getPrefix().equalsIgnoreCase(""))?"":customerFile.getPrefix()+" ")+((customerFile.getFirstName()==null || customerFile.getFirstName().equalsIgnoreCase(""))?"":customerFile.getFirstName()+" ")+((customerFile.getLastName()==null || customerFile.getLastName().equalsIgnoreCase(""))?"":customerFile.getLastName())+",<br>";
	       	msgText1=msgText1 +"<br>We are excited to advise that Stevens International has been selected to provide destination services for your shipment in "+customerFile.getDestinationCity()+"/"+customerFile.getDestinationState()+".";
	       	msgText1=msgText1 +"<br>Once your shipment is ready for delivery, my destination agent "+trackingStatus.getDestinationAgent()+" will contact you to schedule the delivery of your shipment.";
	       	msgText1=msgText1 +"<br>Please provide your delivery address and a preferred delivery date.";
	       	msgText1=msgText1 +"<br><br>We have attached some information that we hope you will find useful:";
	   		msgText1=msgText1 +"<br>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;International Moving Guide";
	   		msgText1=msgText1 +"<br>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GSA Rights and Responsibilities";
	   		msgText1=msgText1 +"<br><br>Please let me know if you have any questions or concerns prior or during the delivery of your shipment.";
	   		
			User usr;
			if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equalsIgnoreCase("")){
				usr = (User)userManager.getUserByUsername(customerFile.getCoordinator());
			}else{
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				usr = (User) auth.getPrincipal();
			}
			msgText1=msgText1 +"<br><br>Best regards,";
	   		if(customerFile.getCoordinatorName()!=null){
	   			msgText1 = msgText1 +"<br>"+customerFile.getCoordinatorName();
	   		}
	   		msgText1=msgText1 +"<br>Ph No:-"+usr.getPhoneNumber();
	   		msgText1=msgText1 +"<br>Email:-"+usr.getEmail();
	   		
	   		String StevensLogo = "/usr/local/redskydoc/userData/images/stevens-vanline-WelcomeMail.jpg";
	   		if(StevensLogo!=null && !StevensLogo.equalsIgnoreCase("")){
	   			msgText1 = msgText1 +"<br><img src=\"cid:image1\" width=\"200px\" height=\"100px\"</br>";
	   		}
	   		String pdf1 = "/usr/local/redskydoc/userData/images/GSA_Guide_LR.pdf";
	   		String pdf2 = "/usr/local/redskydoc/userData/images/Relocating_Employees_Rights_and_Responsibilities_for_Household_Goods_Services_FINAL.pdf";
	   		String location="";
	   		if(StevensLogo!=null && !StevensLogo.equalsIgnoreCase("")){
	   			location = StevensLogo+"#image1"+"~"+pdf1+"~"+pdf2;
	   		}else{
	   			location = pdf1+"~"+pdf2;
	   		}
	   		
	   		String from;
   			if ((usr.getEmail() != null) && (!usr.getEmail().equalsIgnoreCase(""))) { 
   				from = usr.getEmail();
   			}else{
   				from = "support@redskymobility.com";	
   			}
   			subject = "Your Upcoming Relocation to "+customerFile.getDestinationCity()+","+customerFile.getDestinationState()+","+customerFile.getDestinationCountry()+"";
	   	    msgText1 = "<html><table><font face='Tahoma' size='2'>"+msgText1+"</font></table></html>";
	   	    String emailTo="";
	   	 if(customerFile.getEmail()!=null && ! customerFile.getEmail().equalsIgnoreCase("") 
   	    		 && customerFile.getEmail2()!=null && ! customerFile.getEmail2().equalsIgnoreCase("") ){
		 emailTo=customerFile.getEmail()+","+customerFile.getEmail2(); 
	      }
   	     else if(serviceOrder.getEmail()!=null && ! serviceOrder.getEmail().equalsIgnoreCase("") 
   	    		 && serviceOrder.getEmail2()!=null && ! serviceOrder.getEmail2().equalsIgnoreCase("") ){
    		 emailTo=serviceOrder.getEmail()+","+customerFile.getEmail2(); 
    	      }
   	     else if(customerFile.getEmail()!=null && ! customerFile.getEmail().equalsIgnoreCase("")){
   	    	  emailTo=customerFile.getEmail(); 
   	     }else if(serviceOrder.getEmail()!=null && ! serviceOrder.getEmail().equalsIgnoreCase("")){
   	    	 emailTo=serviceOrder.getEmail();
   	     }
	   		
	   		customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());
	   		customerFile.setUpdatedBy(getRequest().getRemoteUser());
	   		customerFile.setUpdatedOn(new Date());
	   		customerFile.setWelcomeEmailOn(new Date());
	   		customerFileManager.save(customerFile);
	   		
	   		emailSetupManager.globalEmailSetupProcess(from, emailTo, usr.getEmail(), "", location, msgText1, subject, sessionCorpID,"CfBookedMove",customerFile.getSequenceNumber(),"");
	    	} catch (Exception e) {
				e.printStackTrace();
			}
	   		return "SUCCESS";
	       }
		
        private String getLinkedCustomerFileNumber(String sequenceNumber,String childAgentCode) {
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
       		String linkedCustomerFileNumber="";
        	try{
			List<TrackingStatus> trackingStatusList=trackingStatusManager.getNetworkedTrackingStatus(sequenceNumber);
			for(TrackingStatus trackingStatus:trackingStatusList){
				ServiceOrder serviceOrder=serviceOrderManager.getForOtherCorpid(trackingStatus.getId());
				if(trackingStatus.getDestinationAgentCode() != null && trackingStatus.getDestinationAgentCode().equalsIgnoreCase(childAgentCode) && trackingStatus.getDestinationAgentExSO()!=null && !trackingStatus.getDestinationAgentExSO().equals("")){
					linkedCustomerFileNumber= trackingStatus.getDestinationAgentExSO();
				}
				if(trackingStatus.getDestinationSubAgentCode()!=null && trackingStatus.getDestinationSubAgentCode().equalsIgnoreCase(childAgentCode) && trackingStatus.getDestinationSubAgentExSO()!=null && !trackingStatus.getDestinationSubAgentExSO().equals("")){
					linkedCustomerFileNumber= trackingStatus.getDestinationSubAgentExSO();
				}
				if(trackingStatus.getOriginAgentCode()!=null && trackingStatus.getOriginAgentCode().equalsIgnoreCase(childAgentCode) && trackingStatus.getOriginAgentExSO()!=null && !trackingStatus.getOriginAgentExSO().equals("")){
					linkedCustomerFileNumber= trackingStatus.getOriginAgentExSO();
				}
				if(trackingStatus.getOriginSubAgentCode()!=null && trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(childAgentCode) && trackingStatus.getOriginSubAgentExSO()!=null && !trackingStatus.getOriginSubAgentExSO().equals("")){
					linkedCustomerFileNumber= trackingStatus.getOriginSubAgentExSO();
				}
				if(trackingStatus.getNetworkPartnerCode()!=null && trackingStatus.getNetworkPartnerCode().equalsIgnoreCase(childAgentCode) && trackingStatus.getNetworkAgentExSO()!=null && !trackingStatus.getNetworkAgentExSO().equals("")){
					linkedCustomerFileNumber= trackingStatus.getNetworkAgentExSO();
				}
				if(serviceOrder.getBookingAgentCode()!=null && serviceOrder.getBookingAgentCode().equalsIgnoreCase(childAgentCode) && trackingStatus.getBookingAgentExSO()!=null && !trackingStatus.getBookingAgentExSO().equals("")){
					linkedCustomerFileNumber= trackingStatus.getBookingAgentExSO();
				}
			}
        	}catch(Exception e){
        		 e.printStackTrace();
        		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
        		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
        	}
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return linkedCustomerFileNumber;
		}
		private Boolean getIsNetworkedCustomerFile(Long customerFileId) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			try
			{
			return customerFileManager.isNetworkedRecord(customerFileId);
			}
			catch(Exception e)
			{
				e.printStackTrace(); 
				return false; 
			}
		}
		public void synchornizeMiscellaneous(List<Object> miscellaneousRecords, Miscellaneous miscellaneous,TrackingStatus trackingStatus) {
			try {
				String fieldType="";
				String uniqueFieldType=""; 
				Iterator  it=miscellaneousRecords.iterator();
				while(it.hasNext()){
					Miscellaneous miscellaneousToRecods=(Miscellaneous)it.next();
					if(!(miscellaneous.getShipNumber().equals(miscellaneousToRecods.getShipNumber()))){
					TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(miscellaneousToRecods.getId());
					if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
						uniqueFieldType=trackingStatusToRecods.getContractType();
					if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
						fieldType="CMM/DMM";
					}
					List fieldToSync=customerFileManager.findFieldToSync("Miscellaneous",fieldType,uniqueFieldType);
					fieldType="";
				    uniqueFieldType=""; 
					Iterator it1=fieldToSync.iterator();
					while(it1.hasNext()){
						String field=it1.next().toString();
						String[] splitField=field.split("~");
						String fieldFrom=splitField[0];
						String fieldTo=splitField[1];
						try{
							PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
							try{
							beanUtilsBean.setProperty(miscellaneousToRecods,fieldTo.trim(),beanUtilsBean.getProperty(miscellaneous, fieldFrom.trim()));
							}catch(Exception ex){
								logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
							}
							
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
						
					}
					try{
					 if(!(miscellaneous.getShipNumber().equals(miscellaneousToRecods.getShipNumber()))){
					miscellaneousToRecods.setUpdatedBy(miscellaneous.getCorpID()+":"+getRequest().getRemoteUser());
					miscellaneousToRecods.setUpdatedOn(new Date());
					 }}catch( Exception e ){
						 logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					 }
					miscellaneousManager.save(miscellaneousToRecods);
				}
				}
			} catch (Exception e) {
				 e.printStackTrace();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
		}
		public List<Object> findMiscellaneousRecords(List linkedShipNumber,ServiceOrder serviceOrder2) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List<Object> recordList= new ArrayList();
        	try {
				Iterator it =linkedShipNumber.iterator();
				while(it.hasNext()){
					String shipNumber= it.next().toString();
					Miscellaneous miscellaneousRemote=miscellaneousManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
					recordList.add(miscellaneousRemote);
				}
			} catch (Exception e) {
				 e.printStackTrace();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return recordList;
		}
		private void checkRemoveLinks(List<String> exSoList, TrackingStatus trackingStatus) {
			try{
			for (String exSo : exSoList) {
	        		if(exSo!=null && exSo.contains("remove")){
	        			trackingStatusManager.removeExternalLink(0L,exSo,getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());	
	        		}
        		}}catch(Exception e){
        			 e.printStackTrace();
        			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
        			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
      	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
      	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
        		}
        	}

		// End of Method.
        
		@SkipValidation
	    public void synchornizeServiceOrder(List<Object> records, ServiceOrder serviceOrder,TrackingStatus trackingStatus,CustomerFile customerFile) {
			try {
				String fieldType="";
				String uniqueFieldType="";
				 
				Iterator  it=records.iterator();
				while(it.hasNext()){
					ServiceOrder serviceOrderToRecods=(ServiceOrder)it.next();
					String soJobType = serviceOrderToRecods.getJob();
					if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
					TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
					if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
						uniqueFieldType=trackingStatusToRecods.getContractType();
					if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
						fieldType="CMM/DMM";
					}
					List fieldToSync=customerFileManager.findFieldToSync("ServiceOrder",fieldType,uniqueFieldType);
					fieldType="";
				    uniqueFieldType=""; 
					Iterator it1=fieldToSync.iterator();
					while(it1.hasNext()){
						String field=it1.next().toString();
						String[] splitField=field.split("~");
						String fieldFrom=splitField[0];
						String fieldTo=splitField[1];
						try{
							PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
							try{
							beanUtilsBean.setProperty(serviceOrderToRecods,fieldTo.trim(),beanUtilsBean.getProperty(serviceOrder, fieldFrom.trim()));
							}catch(Exception ex){
								logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
							}
							
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
						
					}
					try{
				    if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
				    if(serviceOrder.getJob()!=null && (!(serviceOrder.getJob().toString().equals(""))) && (serviceOrder.getJob().toString().equalsIgnoreCase("RLO")) ) {
					    	serviceOrderToRecods.setServiceType(serviceOrder.getServiceType());	
					}
					serviceOrderToRecods.setUpdatedBy(serviceOrder.getCorpID()+":"+getRequest().getRemoteUser());
					serviceOrderToRecods.setUpdatedOn(new Date());
				    }}catch( Exception e){
				    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    }
				    try{
				    	if(!networkAgent){
				    	if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
				    		if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
				    			serviceOrderToRecods.setCoordinator(serviceOrder.getCoordinator());
						}
				    	}
				    	}
				    	if(networkAgent){ 
					    	if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
					    		if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
					    			serviceOrderToRecods.setCoordinator(serviceOrder.getCoordinator());
				    		}
					    	}
					    	
				    		
				    	}	
				    }catch( Exception e){
				    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    }
				    if(serviceOrderToRecods.getCorpID().toString().equalsIgnoreCase("UGWW"))
				    	serviceOrderToRecods.setJob(soJobType);
				    
					 serviceOrderManager.save(serviceOrderToRecods);
			   		 try{
			   			 CustomerFile customerFileToRecods=customerFileManager.getForOtherCorpid(serviceOrderToRecods.getCustomerFileId());
			   			if(customerFile.getContractType()!=null && (!(customerFile.getContractType().toString().trim().equals(""))) && (customerFile.getContractType().toString().trim().equalsIgnoreCase("DMM") || customerFile.getContractType().toString().trim().equalsIgnoreCase("CMM") ) && customerFileToRecods.getContractType()!=null &&(!(customerFileToRecods.getContractType().toString().trim().equals(""))) && (customerFileToRecods.getContractType().toString().trim().equalsIgnoreCase("DMM") || customerFileToRecods.getContractType().toString().trim().equalsIgnoreCase("CMM"))){
			   				customerFileManager.updateLinkedCustomerFileStatus(customerFile.getStatus(),customerFile.getStatusNumber(),customerFile.getStatusDate(),serviceOrderToRecods.getCustomerFileId(),serviceOrder.getCorpID()+":"+getRequest().getRemoteUser());	
			   			}
			   		 }catch( Exception e){
			    	    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			    	    }

				}
				}
			} catch (Exception e) {
				 e.printStackTrace();
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}	
	    	}
		
        public void synchornizeTrackingStatus(List<Object> records, TrackingStatus trackingStatus,ServiceOrder serviceOrder) {
        	try {
				String fieldType="";
				String uniqueFieldType="";

				Iterator  it=records.iterator();
				while(it.hasNext()){
					TrackingStatus trackingStatusToRecods=(TrackingStatus)it.next();
					String forwarderCode="";
			   		String brokerCode="";
			   	    if(trackingStatusToRecods.getForwarderCode()!=null && (!(trackingStatusToRecods.getForwarderCode().equalsIgnoreCase("")))){
			   	    	forwarderCode=trackingStatusToRecods.getForwarderCode();
			   	    }
			   	    if(trackingStatusToRecods.getBrokerCode()!=null && (!(trackingStatusToRecods.getBrokerCode().equalsIgnoreCase("")))){
			   	    	brokerCode =trackingStatusToRecods.getBrokerCode();
			   	    }
					if(!(trackingStatus.getShipNumber().equals(trackingStatusToRecods.getShipNumber()))){
					if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
						uniqueFieldType=trackingStatusToRecods.getContractType();
					if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
						fieldType="CMM/DMM";
					}
					List fieldToSync=customerFileManager.findFieldToSync("TrackingStatus",fieldType,uniqueFieldType);
					fieldType="";
				    uniqueFieldType=""; 
					Iterator it1=fieldToSync.iterator();
					while(it1.hasNext()){
						String field=it1.next().toString();
						String[] splitField=field.split("~");
						String fieldFrom=splitField[0].trim();
						String fieldTo=splitField[1].trim();
						PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
						try{
							beanUtilsBean.setProperty(trackingStatusToRecods,fieldTo,beanUtilsBean.getProperty(trackingStatus, fieldFrom));
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
					}
					try{
				       	 if(!(trackingStatus.getShipNumber().equals(trackingStatusToRecods.getShipNumber()))){
						  trackingStatusToRecods.setUpdatedBy(trackingStatus.getCorpID()+":"+getRequest().getRemoteUser());
						  trackingStatusToRecods.setUpdatedOn(new Date());
				       	 }
				    }catch(Exception e){
				    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    }
					try{
						 if(trackingStatusToRecods.getForwarderCode()!=null && (!(trackingStatusToRecods.getForwarderCode().equalsIgnoreCase("")))){
							boolean checkVendorCode= billingManager.findVendorCode(trackingStatusToRecods.getCorpID(),trackingStatusToRecods.getForwarderCode());
							if(checkVendorCode){
								
							}else{
								trackingStatusToRecods.setForwarderCode("");	
							}
						 }else{
						 if(forwarderCode!=null && (!(forwarderCode.equals("")))){
							 trackingStatusToRecods.setForwarderCode(forwarderCode); 
						 }
						 }
						 }catch(Exception e){
							e.printStackTrace(); 
						 }
					 try{
						 if(trackingStatusToRecods.getBrokerCode()!=null && (!(trackingStatusToRecods.getBrokerCode().equalsIgnoreCase("")))){
							boolean checkVendorCode= billingManager.findVendorCode(trackingStatusToRecods.getCorpID(),trackingStatusToRecods.getBrokerCode());
							if(checkVendorCode){
								
							}else{
								trackingStatusToRecods.setBrokerCode("");	
							}
						 }else{
						 if(brokerCode!=null && (!(brokerCode.equals("")))){
							 trackingStatusToRecods.setBrokerCode(brokerCode); 
						 }
						 }
						 }catch(Exception e){
							e.printStackTrace(); 
						 }
				    trackingStatusToRecods=trackingStatusManager.save(trackingStatusToRecods);
				    ServiceOrder serviceOrderRecords1=serviceOrderManager.getForOtherCorpid(trackingStatusToRecods.getId());
				}
				}
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
        }
        
        @SkipValidation
        public String findAutomaticLinkUpMethod(){
        	try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, partnerCode);
				networkPartnerCode=companyManager.findAutomaticLinkup(externalCorpID);
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	    	return "errorlog";
			} 
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return SUCCESS;
        }
        
        @SkipValidation
        public String findMoveTypePartner(){
        	try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				networkPartnerCode=trackingStatusManager.findMoveTypePartnerValue(sessionCorpID, moveTypePartner);
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	    	return "errorlog";
			} 
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return SUCCESS;
        }
        @SkipValidation
        public List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
        	List<Object> recordList= new ArrayList();
        	try
        	{
        	Iterator it =linkedShipNumber.iterator();
        	while(it.hasNext()){
        		String shipNumber= it.next().toString();
        		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
        		recordList.add(serviceOrderRemote);
        	}}
        	catch(Exception e)
        	{
        		e.printStackTrace();
        	}
        	return recordList;
        	}
        public List<Object> findTrackingStatusRecords(List linkedShipNumber,	ServiceOrder serviceOrder) {
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        	List<Object> recordList= new ArrayList();
        	try
        	{
        	Iterator it =linkedShipNumber.iterator();
        	while(it.hasNext()){
        		String shipNumber= it.next().toString();
        		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
        		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
        		recordList.add(trackingStatusRemote);
        	}}
        	catch(Exception e)
        	{
        		e.printStackTrace();
        	}
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return recordList;
        	}
        public List findLinkerShipNumber(ServiceOrder serviceOrder) {
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        	List linkedShipNumberList= new ArrayList();
        	try
        	{
        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
        		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
        	}else{
        		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
        	}}
        	catch(Exception e)
        	{
        		e.printStackTrace();
        	}
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return linkedShipNumberList;
        }
        
        private void createExternaleEntriesBookingAgentCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile,Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile){
        	try {
				Long customerFileId=0L;
				if(trackingStatus.getBookingAgentExSO().equals("")){
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, serviceOrder.getBookingAgentCode());
					customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"bookingAgentCode", linkedCustomerFileNumber, isNetworkedCustomerFile);
				}else{
					checkRemoveTokenForBookingAgentCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"bookingAgentCode", linkedCustomerFileNumber, isNetworkedCustomerFile);
				}
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			} 
        }
        
        private void createExternaleEntriesNetworkPartnerCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile,Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
        	Long customerFileId=0L;
			if(trackingStatus.getNetworkAgentExSO().equals("")){
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode());
				customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"networkPartnerCode", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}else{
				checkRemoveTokenForNetworkPartnerCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"networkPartnerCode", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
        
        private void createExternaleEntriesUGWWNetworkPartnerCode(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile,Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
        	Long customerFileId=0L;
			if(trackingStatus.getNetworkAgentExSO().equals("")){
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode());
				customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"UGWWnetworkPartnerCode", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}else{
				checkRemoveTokenForUGWWNetworkPartnerCode(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"UGWWnetworkPartnerCode", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
		
		private void createExternaleEntriesDestinationSub(ServiceOrder serviceOrder, TrackingStatus trackingStatus,CustomerFile customerFile, Billing billing,Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
			Long customerFileId=0L;
			if(trackingStatus.getDestinationSubAgentExSO().equals("")){
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getDestinationSubAgentCode());
				customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"destinationSub", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}else{
				checkRemoveTokenForDestinationSub(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"destinationSub", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		private void createExternaleEntriesOriginSub(ServiceOrder serviceOrder, TrackingStatus trackingStatus,CustomerFile customerFile, Billing billing,Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
			Long customerFileId=0L;
			if(trackingStatus.getOriginSubAgentExSO().equals("")){
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getOriginSubAgentCode());
				customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"originSub", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}else{
				checkRemoveTokenForOriginSub(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"originSub", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		private void createExternaleEntriesOrigin(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile,Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
			Long customerFileId=0L;
			if(trackingStatus.getOriginAgentExSO().equals("")){
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getOriginAgentCode());
				customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"origin", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}else{
				checkRemoveTokenForOrigin(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"origin", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
		}
		private void createExternaleEntriesDestination(ServiceOrder serviceOrder,TrackingStatus trackingStatus, CustomerFile customerFile, Billing billing, Miscellaneous miscellaneous, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			
			try
			{Long customerFileId=0L;
			if(trackingStatus.getDestinationAgentExSO().equals("")){
				String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getDestinationAgentCode());
				customerFileId=createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"destination", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}else{
				checkRemoveTokenForDestination(customerFileId,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"destination", linkedCustomerFileNumber, isNetworkedCustomerFile);
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
		}
		private void checkRemoveTokenForDestination(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
			if(trackingStatus.getDestinationAgentExSO().contains("remove")){
				Boolean isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getDestinationAgentCode() ); 
				if(isNetworkDestinationAgent){
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getDestinationAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getDestinationAgentCode());
					createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"destination", linkedCustomerFileNumber, isNetworkedCustomerFile);
				}else{
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getDestinationAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					
				}
				
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		private void checkRemoveTokenForOrigin(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
			if(trackingStatus.getOriginAgentExSO().contains("remove")){
				Boolean isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getOriginAgentCode() ); 
				if(isNetworkDestinationAgent){
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getOriginAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getOriginAgentCode());
					createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"origin", linkedCustomerFileNumber, isNetworkedCustomerFile);
				}else{
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getOriginAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					
				}
				
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		private void checkRemoveTokenForDestinationSub(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
			if(trackingStatus.getDestinationSubAgentExSO().contains("remove")){
				Boolean isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getDestinationSubAgentCode() ); 
				if(isNetworkDestinationAgent){
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getDestinationSubAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getDestinationSubAgentCode());
					createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"destinationSub", linkedCustomerFileNumber, isNetworkedCustomerFile);
				}else{
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getDestinationSubAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					
				}
				
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			}
		private void checkRemoveTokenForOriginSub(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
			if(trackingStatus.getOriginSubAgentExSO().contains("remove")){
				Boolean isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getOriginSubAgentCode() ); 
				if(isNetworkDestinationAgent){
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getOriginSubAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getOriginSubAgentCode());
					createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"originSub", linkedCustomerFileNumber, isNetworkedCustomerFile);
				}else{
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getOriginSubAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					
				}
				
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		private void checkRemoveTokenForNetworkPartnerCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
			if(trackingStatus.getNetworkAgentExSO().contains("remove")){
				Boolean isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getNetworkPartnerCode() ); 
				if(isNetworkDestinationAgent){
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getNetworkAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode());
					createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"networkPartnerCode", linkedCustomerFileNumber, isNetworkedCustomerFile);
				}else{
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getNetworkAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					
				}
				
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		private void checkRemoveTokenForUGWWNetworkPartnerCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			try
			{
			if(trackingStatus.getNetworkAgentExSO().contains("remove")){
				Boolean isNetworkDestinationAgent=getIsNetworkAgent(sessionCorpID,trackingStatus.getNetworkPartnerCode() ); 
				if(isNetworkDestinationAgent){
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getNetworkAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode());
					createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"UGWWnetworkPartnerCode", linkedCustomerFileNumber, isNetworkedCustomerFile);
				}else{
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getNetworkAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					
				}
				
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		
		private void checkRemoveTokenForBookingAgentCode(Long customerFileId,CustomerFile customerFile,ServiceOrder serviceOrder, TrackingStatus trackingStatus,Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			
			try
			{if(trackingStatus.getBookingAgentExSO().contains("remove")){
				Boolean isNetworkBookingAgentCode=getIsNetworkAgent(sessionCorpID,serviceOrder.getBookingAgentCode() ); 
				if(isNetworkBookingAgentCode){
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getBookingAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, serviceOrder.getBookingAgentCode());
					createCustomerFile(externalCorpID,customerFile, serviceOrder,trackingStatus,billing,miscellaneous,"bookingAgentCode", linkedCustomerFileNumber, isNetworkedCustomerFile);
				}else{
					trackingStatusManager.removeExternalLink(customerFileId,trackingStatus.getBookingAgentExSO(),getRequest().getRemoteUser(),trackingStatus.getId(),trackingStatus.getShipNumber(),sessionCorpID,trackingStatus.getSoNetworkGroup());
					
				}
				
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		@SkipValidation
		public void networkControlEntry(ServiceOrder ugwwServiceOrder, ServiceOrder serviceOrder , String agentRole){
			if(!serviceOrder.getCorpID().equals("UGWW")){
			try{
				String agentCode = "";
				TrackingStatus tracObj = trackingStatusManager.getForOtherCorpid(serviceOrder.getId());
				if(agentRole.equalsIgnoreCase("BA"))
					agentCode = serviceOrder.getBookingAgentCode();
				else if(agentRole.equalsIgnoreCase("DA"))
					agentCode= tracObj.getDestinationAgentCode();
				else if(agentRole.equalsIgnoreCase("OA"))
					agentCode= tracObj.getOriginAgentCode();
				else if(agentRole.equalsIgnoreCase("NWA"))
					agentCode= tracObj.getNetworkPartnerCode();
				else if(agentRole.equalsIgnoreCase("SDA"))
					agentCode= tracObj.getDestinationSubAgentCode();
				else if(agentRole.equalsIgnoreCase("SOA"))
					agentCode= tracObj.getOriginSubAgentCode();	
				else
					agentCode= "";
				
				Long StartTime = System.currentTimeMillis();
				NetworkControl networkControl = new NetworkControl();
				networkControl.setSourceCorpId(ugwwServiceOrder.getCorpID());
				networkControl.setSoNumber(serviceOrder.getShipNumber());
				networkControl.setSourceSoNum(ugwwServiceOrder.getShipNumber());
				networkControl.setSourceInfo("");
				networkControl.setSourceStatus("NEW");
				networkControl.setCorpId(serviceOrder.getCorpID());
				networkControl.setStatus("");
				networkControl.setAction("Create Link");
				networkControl.setActionStatus("Done");
				networkControl.setUpdatedOn(new Date());
				networkControl.setCreatedOn(new Date());
				networkControl.setUpdatedBy(getRequest().getRemoteUser());
				networkControl.setCreatedBy(getRequest().getRemoteUser());
				networkControl.setChildAgentCode(agentCode);
				networkControl.setChildAgentType(agentRole);
				networkControl.setCompanyDiv(serviceOrder.getCompanyDivision());
				networkControl = networkControlManager.save(networkControl);
				Long timeTaken =  System.currentTimeMillis() - StartTime;
			}catch (Exception e) {
				 e.printStackTrace();
				
    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
			}
			}
		private Long createCustomerFile(String externalCorpID,CustomerFile customerFile, ServiceOrder serviceOrder, TrackingStatus trackingStatus, Billing billing, Miscellaneous miscellaneous, String forSection, String linkedCustomerFileNumber, Boolean isNetworkedCustomerFile) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			Long customerFileId=0L;
			try
			{
			 boolean billingCMMContractType=false;
			 boolean billingDMMContractType=false;
			 String fieldType="";
			 String uniqueFieldType="";
			 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			     billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			 } 
			 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			     billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			 }
			
			 if(forSection.equalsIgnoreCase("networkPartnerCode") ||forSection.equalsIgnoreCase("bookingAgentCode")){
				 fieldType="CMM/DMM";
				 if(billingCMMContractType){
					 uniqueFieldType="CMM"; 
				 }
				 if(billingDMMContractType){
					 uniqueFieldType="DMM"; 
				 }
			 }
			 try{
				 if((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent ){
					 fieldType="CMM/DMM"; 
					 if(billingCMMContractType){
						 uniqueFieldType="CMM"; 
					 }
					 if(billingDMMContractType){
						 uniqueFieldType="DMM"; 
					 } 
				 }
				 }catch(Exception e){
					 
				 }
			String networkCoordinator=customerFileManager.getNetworkCoordinator(externalCorpID);
			if(linkedCustomerFileNumber.equalsIgnoreCase("") || linkedCustomerFileNumber.contains("remove")){
			Long remoteCustomerFileId = 0L;
            List linkedSequnceNumberList= new ArrayList();
			if(customerFile.getBookingAgentSequenceNumber()==null || customerFile.getBookingAgentSequenceNumber().equals("") ){
			  linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getSequenceNumber(), "Primary");
		   }else{
			  linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getBookingAgentSequenceNumber(), "Secondary");
		   }  
			Iterator it =linkedSequnceNumberList.iterator();
			while(it.hasNext()){
				String sequenceNumber= it.next().toString();
				if(remoteCustomerFileId.intValue() ==0){
				remoteCustomerFileId=customerFileManager.findNewRemoteCustomerFile(sequenceNumber,externalCorpID);  
				}
			}
			
			if(remoteCustomerFileId.intValue() > 0)	{
				CustomerFile customerFileNew1=customerFileManager.getForOtherCorpid(remoteCustomerFileId);
				customerFileId=customerFileNew1.getId();
				createServiceOrder(customerFileNew1, serviceOrder,trackingStatus,billing,miscellaneous,forSection,customerFile);
			}else{
			List fieldToSync=customerFileManager.findFieldToSyncCreate("CustomerFile",fieldType,uniqueFieldType);
			
			CustomerFile customerFileNew= new CustomerFile();
			try{
				Iterator it1=fieldToSync.iterator();
				while(it1.hasNext()){
					String field=it1.next().toString(); 
    				String[] splitField=field.split("~");
    				String fieldFrom=splitField[0];
    				String fieldTo=splitField[1];
					PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					beanUtilsBean.setProperty(customerFileNew,fieldTo.trim(),beanUtilsBean.getProperty(customerFile, fieldFrom.trim()));
					}catch(Exception ex){
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
				}
			}catch(Exception ex){
				logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
			}
			
			customerFileNew.setId(null);
			if(!(forSection.equalsIgnoreCase("networkPartnerCode"))){
			if(customerFileNew.getComptetive()==null || customerFileNew.getComptetive().trim().equals("")){
				customerFileNew.setComptetive("N");
			}
			}
			try{
				if(customerFileNew.getMoveType()==null || (customerFileNew.getMoveType().toString().trim().equals(""))){
				if(customerFileNew.getControlFlag()==null ||(customerFileNew.getControlFlag().toString().trim().equals("")) || (customerFileNew.getControlFlag().trim().equalsIgnoreCase("Q")  )){
					customerFileNew.setMoveType("Quote");
				}else{
					customerFileNew.setMoveType("BookedMove");
				}
				}
				}catch(Exception e){
					
				}
			customerFileNew.setCorpID(externalCorpID);
			maxSequenceNumber = customerFileManager.findMaximumSequenceNumber(externalCorpID);
			if (maxSequenceNumber.get(0) == null) {
				customerFileNew.setSequenceNumber(externalCorpID + "1000001");
			} else {
				autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(externalCorpID, "")) + 1;
				seqNum = externalCorpID + autoSequenceNumber.toString();
				customerFileNew.setSequenceNumber(seqNum);
			}
			customerFileNew.setIsNetworkRecord(true);
			customerFileNew.setBookingAgentSequenceNumber(customerFile.getSequenceNumber());
			customerFileNew.setCreatedBy("Networking");
			customerFileNew.setUpdatedBy("Networking");
			if(customerFileNew.getMoveType() == null || customerFileNew.getMoveType().toString().trim().equals("")){
				customerFileNew.setMoveType("BookedMove");
			}
			customerFileNew.setCreatedOn(new Date());
			customerFileNew.setUpdatedOn(new Date());
			customerFileNew.setSalesStatus("Pending");
			customerFileNew.setIsNetworkGroup(false);
			customerFileNew.setContractType("");
			if(forSection.equalsIgnoreCase("destination")){
				customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getDestinationAgentCode(), sessionCorpID,externalCorpID ));
			}
			if(forSection.equalsIgnoreCase("destinationSub")){
				customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getDestinationSubAgentCode(), sessionCorpID,externalCorpID ));
			}
			if(forSection.equalsIgnoreCase("origin")){
				customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getOriginAgentCode(), sessionCorpID,externalCorpID ));
			}
			if(forSection.equalsIgnoreCase("originSub")){
				customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getOriginSubAgentCode(), sessionCorpID,externalCorpID ));
			}
			if(forSection.equalsIgnoreCase("networkPartnerCode")){
				customerFileNew.setIsNetworkGroup(true);
				if(billingCMMContractType){
				String[] ParentBillTo=trackingStatusManager.findParentBillTo(customerFile.getBillToCode(), sessionCorpID);
				customerFileNew.setAccountCode(ParentBillTo[0]);
				customerFileNew.setAccountName(ParentBillTo[1]);
			
					customerFileNew.setContractType("CMM"); 
		        }
		        if(billingDMMContractType){
		        	String contractAccountDetail="";
		        	if(customerFileNew.getContract() !=null && (!(customerFileNew.getContract().trim().equals("")))){
		        	 contractAccountDetail=customerFileManager.getContractAccountDetail(externalCorpID,customerFileNew.getContract());
		        	}
		        	if(contractAccountDetail!=null && (!(contractAccountDetail.trim().equals("")))){
		        		String[] str1 = contractAccountDetail.split("~");
		        		customerFileNew.setAccountCode(str1[0]);
						customerFileNew.setAccountName(str1[1]);
		        	}else{
		        	String[] ParentBillTo=trackingStatusManager.findParentBillTo(customerFile.getAccountCode(), sessionCorpID);
					customerFileNew.setAccountCode(ParentBillTo[0]);
					customerFileNew.setAccountName(ParentBillTo[1]);
		        	}
		        	customerFileNew.setContractType("DMM");
		        	customerFileNew.setBillToCode(customerFile.getAccountCode());
					customerFileNew.setBillToName(customerFile.getAccountName());
		        	
		        }
				customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getNetworkPartnerCode(), sessionCorpID,externalCorpID ));
			}
			if(forSection.equalsIgnoreCase("bookingAgentCode") || ((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent )){
				if(billingDMMContractType){
				customerFileNew.setBillToCode(trackingStatus.getNetworkPartnerCode());
				customerFileNew.setBillToName(trackingStatus.getNetworkPartnerName());
				customerFileNew.setAccountCode(customerFile.getBillToCode());
				customerFileNew.setAccountName(customerFile.getBillToName()); 
				}
				if(billingCMMContractType){
					String[] ParentBillTo=trackingStatusManager.findParentBillTo(customerFile.getBillToCode(), sessionCorpID);
					customerFileNew.setAccountCode(ParentBillTo[0]);
					customerFileNew.setAccountName(ParentBillTo[1]); 
			        }
				customerFileNew.setIsNetworkGroup(true);
				if(billingCMMContractType){
					customerFileNew.setContractType("CMM"); 
		        }
		        if(billingDMMContractType){
		        	customerFileNew.setContractType("DMM"); 
		        }
				customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(serviceOrder.getBookingAgentCode(), sessionCorpID,externalCorpID ));
			}
			if(forSection.equalsIgnoreCase("UGWWnetworkPartnerCode")){
				customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatus.getNetworkPartnerCode(), sessionCorpID,externalCorpID ));
			}
			if((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent ){
				
			}else{
			if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
			customerFileNew.setBillToCode(customerFileNew.getBookingAgentCode());
			customerFileNew.setBillToName(customerFileNew.getBookingAgentName());
			String companyDivisionNetworkCoordinator=customerFileManager.getCompanyDivisionNetworkCoordinator(customerFileNew.getCompanyDivision(),externalCorpID);
			if(companyDivisionNetworkCoordinator != null && (!(companyDivisionNetworkCoordinator.toString().trim().equals("")))){
				customerFileNew.setCoordinator(companyDivisionNetworkCoordinator);	
			}else{
			customerFileNew.setCoordinator(networkCoordinator);
			}
			customerFileNew.setBillPayMethod("CA");
			} 
			}
			if(!networkAgent){
			if(((forSection.equalsIgnoreCase("bookingAgentCode"))) || ((forSection.equalsIgnoreCase("networkPartnerCode")))){
				if(customerFile.getCoordinator()!=null && (!(customerFile.getCoordinator().toString().equals("")))){
					customerFileNew.setCoordinator(customerFile.getCoordinator());
			}
			}
			}
			if(networkAgent){
				if(((forSection.equalsIgnoreCase("bookingAgentCode"))) || ((forSection.equalsIgnoreCase("networkPartnerCode"))) || ((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent )){
					if(customerFile.getCoordinator()!=null && (!(customerFile.getCoordinator().toString().equals("")))){
						customerFileNew.setCoordinator(customerFile.getCoordinator());
				}
				}	
			}
			customerFileNew.setSystemDate(new Date());
			customerFileNew.setStatus("NEW");
			customerFileNew.setStatusDate(new Date());
			customerFileNew.setContract("");
			if(forSection.equalsIgnoreCase("networkPartnerCode")||forSection.equalsIgnoreCase("bookingAgentCode") || ((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent )){
				customerFileNew.setContract(customerFile.getContract());	
			}
			
			if(customerFileNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
				customerFileNew.setUgwIntId(customerFileNew.getSequenceNumber());
			}
			if(forSection.equalsIgnoreCase("networkPartnerCode")||forSection.equalsIgnoreCase("bookingAgentCode")){
				customerFileNew.setStatus(customerFile.getStatus());
				customerFileNew.setStatusDate(customerFile.getStatusDate());
				customerFileNew.setStatusNumber(customerFile.getStatusNumber());
			}
			
			if(customerFileNew.getBillToCode()!=null && (!(customerFileNew.getBillToCode().equalsIgnoreCase("")))){
				 pricingBillingPaybaleList=accountLineManager.pricingBillingPaybaleName(customerFileNew.getBillToCode(),customerFileNew.getCorpID(),customerFileNew.getJob());
				 if((pricingBillingPaybaleList!=null) && (!(pricingBillingPaybaleList.isEmpty()))){
						String[] str1 = pricingBillingPaybaleList.get(0).toString().split("#");
						if(!(str1[0].equalsIgnoreCase("1"))){
							customerFileNew.setPersonPricing(str1[0]);
							}
						if(!(str1[1].equalsIgnoreCase("1"))){
							customerFileNew.setPersonBilling(str1[1]);
							}
						if(!(str1[2].equalsIgnoreCase("1"))){
							customerFileNew.setPersonPayable(str1[2]);
							}
						if(!(str1[3].equalsIgnoreCase("1"))){
							customerFileNew.setAuditor(str1[3]);
							}
				}
			 }			
			
			customerFileNew=customerFileManager.save(customerFileNew);			
			
			CustomerFile customerFileTemp=customerFileManager.getForOtherCorpid(customerFileNew.getId());
			if("linkUPNotes".equals(linkUp)){
				if((documentIdListCF!=null)&&(!documentIdListCF.equalsIgnoreCase(""))){
			    		String docListArray[]=documentIdListCF.split(",");
			    			for(int i=0;i<docListArray.length;i++){
					    		String idVal=docListArray[i];
					    		MyFile myfile = myFileManager.get(Long.parseLong(idVal)); 
					    		String fileIdval = myfile.getFileId();
					    		Integer fieldLength = fileIdval.length();
					    		if(fieldLength.equals(11)){ 
						    			MyFile myFileTemp = new MyFile();
						    			myFileTemp.setCorpID(externalCorpID);
						    			myFileTemp.setFileId(customerFileTemp.getSequenceNumber());
						    			myFileTemp.setCustomerNumber(customerFileTemp.getSequenceNumber());
						    			myFileTemp.setActive(myfile.getActive());
						    			myFileTemp.setDescription(myfile.getDescription());
						    			myFileTemp.setDocumentCategory(myfile.getDocumentCategory());
						    			myFileTemp.setFileFileName(myfile.getFileFileName());
						    			myFileTemp.setFileContentType(myfile.getFileContentType());
						    			myFileTemp.setFileType(myfile.getFileType());
						    			myFileTemp.setFileSize(myfile.getFileSize());
						    			myFileTemp.setIsSecure(myfile.getIsSecure());
						    			myFileTemp.setLocation(myfile.getLocation());
						    			myFileTemp.setMapFolder(myfile.getMapFolder());
						    			myFileTemp.setCoverPageStripped(myfile.getCoverPageStripped());
						    			myFileTemp.setIsAccportal(myfile.getIsAccportal());
						    			myFileTemp.setIsCportal(myfile.getIsCportal());
						    			myFileTemp.setIsPartnerPortal(myfile.getIsPartnerPortal());
						    			myFileTemp.setIsServiceProvider(myfile.getIsServiceProvider());
						    			myFileTemp.setInvoiceAttachment(myfile.getInvoiceAttachment());
						    			myFileTemp.setIsBookingAgent(myfile.getIsBookingAgent());
						    			myFileTemp.setIsNetworkAgent(myfile.getIsNetworkAgent());
						    			myFileTemp.setIsOriginAgent(myfile.getIsOriginAgent());
						    			myFileTemp.setIsDestAgent(myfile.getIsDestAgent());
						    			myFileTemp.setIsSubOriginAgent(myfile.getIsSubOriginAgent());
						    			myFileTemp.setIsSubDestAgent(myfile.getIsSubDestAgent());
						    			myFileTemp.setNetworkLinkId(myfile.getNetworkLinkId());
						    			myFileTemp.setCreatedOn(new Date());
						    			myFileTemp.setUpdatedOn(new Date());
						    			myFileTemp.setCreatedBy("Networking"+":"+getRequest().getRemoteUser());
						    			myFileTemp.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
						    			myFileManager.save(myFileTemp);
						     		 }
					    		}
			    			}
						}			
			
			customerFileId=customerFileNew.getId();
			if(fieldType!=null && !fieldType.equals("") && fieldType.equals("CMM/DMM") || company.getSurveyLinking() ){
				createAccessInfo(customerFileNew,"CF",customerFile);
			}
			
			createServiceOrder(customerFileNew, serviceOrder,trackingStatus,billing,miscellaneous,forSection, customerFile);
			createFamilyDetail(customerFile, customerFileNew);
			createAdAddressesDetails(customerFile, customerFileNew);
			createRemovalRelocationServiceDetails(customerFile, customerFileNew);
			}
		}
			if(!linkedCustomerFileNumber.equalsIgnoreCase("") && !linkedCustomerFileNumber.contains("remove")){
				ServiceOrder linkedServiOrder=serviceOrderManager.getForOtherCorpid(Long.parseLong(serviceOrderManager.findRemoteServiceOrder(linkedCustomerFileNumber).toString()));
				CustomerFile linkedCustomerFile=customerFileManager.getForOtherCorpid(Long.parseLong(customerFileManager.findRemoteCustomerFile(linkedServiOrder.getSequenceNumber()).toString()));
				if(linkedCustomerFile.getStatus()!=null && linkedCustomerFile.getStatus().equalsIgnoreCase("CNCL")){
					linkedCustomerFile.setStatus("REOPEN");
					linkedCustomerFile.setStatusNumber(1);
					linkedCustomerFile.setStatusDate(new Date());
					linkedCustomerFile=customerFileManager.save(linkedCustomerFile);
				}
				if(customerFile.getStatus()!=null && customerFile.getStatus().equalsIgnoreCase("CNCL")){
					customerFile.setStatus("REOPEN");
					customerFile.setStatusNumber(1);
					customerFile.setStatusDate(new Date());
					customerFile=customerFileManager.save(customerFile);
				}
				createServiceOrder(linkedCustomerFile, serviceOrder,trackingStatus,billing,miscellaneous,forSection,customerFile);
				customerFileId=linkedCustomerFile.getId();
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return customerFileId;
		}
		private void createInventoryData(ServiceOrder serviceOrderNew,ServiceOrder serviceOrderOld){
			long invId = 0L;
			String crtBy="";
			try
			{
			Date craetedOn = new Date();
			CustomerFile customerFileNew = serviceOrderNew.getCustomerFile();
			CustomerFile customerFileOld = serviceOrderOld.getCustomerFile();
			List invDataList =  inventoryDataManager.getListByType("", customerFileOld.getId());
			Iterator<InventoryData> it=invDataList.iterator();
			while(it.hasNext()){
				InventoryData inventoryDataOld=it.next();
				
					inventoryDataOld.setNetworkSynchedId(inventoryDataOld.getId());
					inventoryDataOld = inventoryDataManager.save(inventoryDataOld);
					InventoryData inventoryDataNew = inventoryDataManager.getSyncInventoryData(customerFileNew.getId(),customerFileNew.getCorpID(),inventoryDataOld.getNetworkSynchedId()); 
					 if(inventoryDataNew.getId()== null){
						  inventoryDataNew= new InventoryData();
					 }else{
						 invId = inventoryDataNew.getId();
						 crtBy = inventoryDataNew.getCreatedBy();
						 craetedOn = inventoryDataNew.getCreatedOn();
						 checkOldCf = "linkedInv";
					 }
				try{
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(inventoryDataNew, inventoryDataOld);
					
					 inventoryDataNew.setCorpID(customerFileNew.getCorpID());
					 inventoryDataNew.setCustomerFileID(customerFileNew.getId());
					 if(checkOldCf!=null && checkOldCf.equals("linkedInv")){
						 inventoryDataNew.setCreatedBy(crtBy);
					     inventoryDataNew.setCreatedOn(craetedOn); 
					     inventoryDataNew.setId(invId);
					 }else{
						 inventoryDataNew.setCreatedBy("Networking");
					     inventoryDataNew.setCreatedOn(new Date()); 
					     inventoryDataNew.setId(null);
					 }
					 
					 inventoryDataNew.setCorpID(serviceOrderNew.getCorpID());
					 if(inventoryDataOld.getServiceOrderID()!=null && inventoryDataOld.getServiceOrderID().equals(serviceOrderOld.getId())){
						 inventoryDataNew.setServiceOrderID(serviceOrderNew.getId());
					 }else{
						 inventoryDataNew.setServiceOrderID(null); 
					 }
					 inventoryDataNew.setUpdatedBy("Networking");
					 inventoryDataNew.setUpdatedOn(new Date());
					 if(inventoryDataOld.getWeight()!=null){
					 String unitW = systemDefaultManager.getCorpIdUnit(inventoryDataNew.getCorpID(),"weightUnit");
					 if(!unitW.equals("") && !unitW.equals(systemDefault.getWeightUnit())){
						 try {
							if(unitW.equals("Kgs")){
								 double temp = (Float.parseFloat(inventoryDataOld.getWeight())) * 0.4536;
								 temp =  temp*10000;
								 int tempIntValue = (int) temp;
								 temp = tempIntValue;
								 temp = temp/10000;
									inventoryDataNew.setWeight(""+temp);
									String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
									inventoryDataNew.setTotalWeight(total);
							 }else{
								 double temp = (Float.parseFloat(inventoryDataOld.getWeight())) * 2.2046;
								 temp =  temp*10000;
								 int tempIntValue = (int) temp;
								 temp = tempIntValue;
								 temp = temp/10000;
									inventoryDataNew.setWeight(""+temp);
									String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
									inventoryDataNew.setTotalWeight(total);
							 }
						} catch (Exception e) {
							e.printStackTrace();
						}
					 }
					 }else{
						 inventoryDataNew.setWeight("0");
						 inventoryDataNew.setTotalWeight("0");
					 }
					 if(inventoryDataOld.getCft()!=null){
					 String unitVol = systemDefaultManager.getCorpIdUnit(inventoryDataNew.getCorpID(),"volumeUnit");
					 if(!unitVol.equals("") &&!unitVol.equals(systemDefault.getVolumeUnit())){
						 try {
							if(!unitVol.equalsIgnoreCase("Cft")){
									double temp = (Float.parseFloat(inventoryDataOld.getCft())) * 0.0283;
									 temp =  temp*10000;
									 int tempIntValue = (int) temp;
									 temp = tempIntValue;
									 temp = temp/10000;
									inventoryDataNew.setCft(""+temp);
									String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
									inventoryDataNew.setTotal(total);
								}else{
									double temp = (Float.parseFloat(inventoryDataOld.getCft())) * 35.3147;
									 temp =  temp*10000;
									 int tempIntValue = (int) temp;
									 temp = tempIntValue;
									 temp = temp/10000;
									inventoryDataNew.setCft(""+temp);
									String total = ""+temp * Float.parseFloat(inventoryDataNew.getQuantity());
									inventoryDataNew.setTotal(total);
								}
						} catch (Exception e) {
							e.printStackTrace();
						}
					 }
					 }else{
						 inventoryDataNew.setCft("0");
				    		inventoryDataNew.setTotal("0");
					 }
					 inventoryDataNew.setNetworkSynchedId(inventoryDataOld.getId());
					 inventoryDataNew= inventoryDataManager.save(inventoryDataNew);
					 
					 List oldInventoryPath = inventoryPathManager.getListByInventoryId(inventoryDataOld.getId());
						if(oldInventoryPath!=null && !oldInventoryPath.isEmpty()){
							Iterator itrInventoryPathId = oldInventoryPath.iterator();
							while(itrInventoryPathId.hasNext()){
								
								InventoryPath oldInvPath = (InventoryPath) itrInventoryPathId.next();
								InventoryPath newInvPath = new InventoryPath();
								try{
									BeanUtilsBean beanUtilsInvPath = BeanUtilsBean.getInstance();
									beanUtilsInvPath.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
									beanUtilsInvPath.copyProperties(newInvPath, oldInvPath);
									newInvPath.setId(null);
									newInvPath.setCorpID(inventoryDataNew.getCorpID());
									newInvPath.setInventoryDataId(inventoryDataNew.getId());
									newInvPath.setAccessInfoId(null);
									inventoryPathManager.save(newInvPath);
								}catch(Exception ex){
									logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
								}
							}
						}
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
			    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 ex.printStackTrace();
				}
			}}
			catch(Exception e)
			{
				e.printStackTrace();
			}		
		}
		private void createAccessInfo(Object objNew,String model,Object objOld) {
			CustomerFile customerFileNew = new CustomerFile();
			CustomerFile customerFileOld = new CustomerFile();
			ServiceOrder serviceOrderNew = new ServiceOrder();
			ServiceOrder serviceOrderOld = new ServiceOrder();
			List accessInfoList = new ArrayList();
			if(model.equals("CF")){
				customerFileNew = (CustomerFile) objNew ;
				customerFileOld = (CustomerFile) objOld;
				accessInfoList=accessInfoManager.getAccessInfo(customerFileOld.getId());
			}else if(model.equals("SO")){
				serviceOrderNew = (ServiceOrder) objNew ;
				serviceOrderOld = (ServiceOrder) objOld;
				accessInfoList=accessInfoManager.getAccessInfoForSO(serviceOrderOld.getId());
			}
			Iterator<AccessInfo> it=accessInfoList.iterator();
			while(it.hasNext()){
				AccessInfo accessInfoOld=it.next();
				accessInfoOld.setNetworkSynchedId(accessInfoOld.getId());
				accessInfoOld= accessInfoManager.save(accessInfoOld);
				AccessInfo accessInfoNew= new AccessInfo();
				try{
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(accessInfoNew, accessInfoOld);
					
					 accessInfoNew.setId(null);
					 if(model.equals("CF")){
						 accessInfoNew.setCorpID(customerFileNew.getCorpID());
						 accessInfoNew.setCustomerFileId(customerFileNew.getId());
					 }else if(model.equals("SO")){
						 accessInfoNew.setCorpID(serviceOrderNew.getCorpID());
						 accessInfoNew.setServiceOrderId(serviceOrderNew.getId());
					 }
					 accessInfoNew.setNetworkSynchedId(accessInfoOld.getId());
					accessInfoNew= accessInfoManager.save(accessInfoNew);
					List localAccessinfo = inventoryPathManager.getListByLocId(accessInfoOld.getId(), "accessInfoId");
					try {
						Iterator<InventoryPath> itr=localAccessinfo.iterator();
						while(itr.hasNext()){
							InventoryPath invPathOld = itr.next();
							invPathOld.setNetworkSynchedId(invPathOld.getId());
							invPathOld = inventoryPathManager.save(invPathOld);
							InventoryPath invPathNew= new InventoryPath(); 
							BeanUtilsBean beanUtilsBean22 = BeanUtilsBean.getInstance();
							 beanUtilsBean22.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
							 beanUtilsBean22.copyProperties(invPathNew, invPathOld);
							 invPathNew.setId(null);
							 if(model.equals("CF")){
								 invPathNew.setCorpID(customerFileNew.getCorpID());
							 }else if(model.equals("SO")){
								 invPathNew.setCorpID(serviceOrderNew.getCorpID());
							 }
							 invPathNew.setAccessInfoId(accessInfoNew.getId());
							 invPathNew.setNetworkSynchedId(invPathOld.getId());
							 invPathNew = inventoryPathManager.save(invPathNew);
						}
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}catch(Exception ex){
					 ex.printStackTrace();
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
			    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
				}
				
				
			}
		
			
		}


		private void createServiceOrder(CustomerFile customerFileNew, ServiceOrder serviceOrder, TrackingStatus trackingStatus, Billing billing, Miscellaneous miscellaneous,String forSection,CustomerFile customerFile) {
			 boolean billingCMMContractType=false;
			 boolean billingDMMContractType=false;
			 String fieldType="";
			 String uniqueFieldType="";
			 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			     billingCMMContractType = trackingStatusManager.getCMMContractType(sessionCorpID ,billing.getContract());
			 } 
			 if(billing.getContract()!=null && (!(billing.getContract().toString().equals("")))){
			      billingDMMContractType = trackingStatusManager.getDMMContractType(sessionCorpID ,billing.getContract());
			 }
			 
			 if(forSection.equalsIgnoreCase("networkPartnerCode") ||forSection.equalsIgnoreCase("bookingAgentCode")){
				 fieldType="CMM/DMM"; 
				 if(billingCMMContractType){
					 uniqueFieldType="CMM"; 
				 }
				 if(billingDMMContractType){
					 uniqueFieldType="DMM"; 
				 }
			 }
			 try{
			 if((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent ){
				 fieldType="CMM/DMM"; 
				 if(billingCMMContractType){
					 uniqueFieldType="CMM"; 
				 }
				 if(billingDMMContractType){
					 uniqueFieldType="DMM"; 
				 } 
			 }
			 }catch(Exception e){
				 
			 }
			List fieldToSync=customerFileManager.findFieldToSyncCreate("ServiceOrder",fieldType,uniqueFieldType);
			ServiceOrder serviceOrderNew= new ServiceOrder();
					
			try{
				Iterator serviceOrderFields=fieldToSync.iterator();
				while(serviceOrderFields.hasNext()){
					String field=serviceOrderFields.next().toString();
					String[] splitField=field.split("~");
    				String fieldFrom=splitField[0];
    				String fieldTo=splitField[1];
					PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
                   		beanUtilsBean.setProperty(serviceOrderNew,fieldTo.trim(),beanUtilsBean.getProperty(serviceOrder, fieldFrom.trim()));
					}catch(Exception ex){
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
				}
			serviceOrderNew.setId(null);
			try{
				if(serviceOrderNew.getMoveType()==null || (serviceOrderNew.getMoveType().toString().trim().equals(""))){
				if(serviceOrderNew.getControlFlag()==null ||(serviceOrderNew.getControlFlag().toString().trim().equals("")) || (serviceOrderNew.getControlFlag().trim().equalsIgnoreCase("Q")  )){
					serviceOrderNew.setMoveType("Quote");
				}else{
					serviceOrderNew.setMoveType("BookedMove");
				}
				}
				}catch(Exception e){
					
				}
			serviceOrderNew.setCorpID(customerFileNew.getCorpID());
            if((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent ){
				
			}else{
			if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
			serviceOrderNew.setCoordinator(customerFileNew.getCoordinator());
			}
			}
			if(!networkAgent){
			if(((forSection.equalsIgnoreCase("bookingAgentCode"))) || ((forSection.equalsIgnoreCase("networkPartnerCode")))){
				if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
				/*List aliasNameList=userManager.findCoordinatorByUsername(serviceOrder.getCoordinator(),sessionCorpID);
				if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
					serviceOrderNew.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
				}*/
					serviceOrderNew.setCoordinator(serviceOrder.getCoordinator());
			}
			}
			}
			if(networkAgent){
				if(((forSection.equalsIgnoreCase("bookingAgentCode"))) || ((forSection.equalsIgnoreCase("networkPartnerCode"))) || ((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent )){
					if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
						/*List aliasNameList=userManager.findCoordinatorByNetworkCoordinator(serviceOrder.getCoordinator(),customerFileNew.getCorpID());
					if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
						serviceOrderNew.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
					}*/
						serviceOrderNew.setCoordinator(serviceOrder.getCoordinator());
				}
				}
				}
			maxShip = customerFileManager.findMaximumShipExternal(customerFileNew.getSequenceNumber(),customerFileNew.getCorpID());
			if (maxShip.get(0) == null) {
				ship = "01";

			} else {
				autoShip = Long.parseLong((maxShip).get(0).toString()) + 1;
				if ((autoShip.toString()).length() == 1) {
					ship = "0" + (autoShip.toString());
				} else {
					ship = autoShip.toString();
				}
		}
			
			shipnumber = customerFileNew.getSequenceNumber() + ship;
			serviceOrderNew.setShip(ship);
			serviceOrderNew.setShipNumber(shipnumber);
			serviceOrderNew.setSequenceNumber(customerFileNew.getSequenceNumber());
			serviceOrderNew.setCustomerFile(customerFileNew);
			if((forSection.equalsIgnoreCase("bookingAgentCode") && billingDMMContractType) || ((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent )){
			serviceOrderNew.setBillToCode(trackingStatus.getNetworkPartnerCode());
			serviceOrderNew.setBillToName(trackingStatus.getNetworkPartnerName());	
			}else{
			serviceOrderNew.setBillToCode(customerFileNew.getBillToCode());
			serviceOrderNew.setBillToName(customerFileNew.getBillToName());
			}
			serviceOrderNew.setCreatedBy("Networking");
			serviceOrderNew.setUpdatedBy("Networking");
			serviceOrderNew.setCreatedOn(new Date());
			serviceOrderNew.setUpdatedOn(new Date());
			serviceOrderNew.setNetworkSO(true);
			serviceOrderNew.setCustomerFileId(customerFileNew.getId());
			serviceOrderNew.setBookingAgentCode(serviceOrder.getBookingAgentCode());
			serviceOrderNew.setBookingAgentName(serviceOrder.getBookingAgentName());
			serviceOrderNew.setIsNetworkRecord(true);
			serviceOrderNew.setBookingAgentShipNumber(serviceOrder.getShipNumber());
			serviceOrderNew.setCompanyDivision(customerFileNew.getCompanyDivision());
           if((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent ){
				
			}else{
			if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
			if((serviceOrder.getRouting()!=null) && (serviceOrder.getRouting().equalsIgnoreCase("EXP"))){
				serviceOrderNew.setRouting("IMP");
			}
			else if((serviceOrder.getRouting()!=null) && (serviceOrder.getRouting().equalsIgnoreCase("IMP"))){
				serviceOrderNew.setRouting("EXP");
			}else{
			serviceOrderNew.setRouting(serviceOrder.getRouting());
			}
			serviceOrderNew.setStatus("NEW");
			serviceOrderNew.setStatusNumber(1);
			serviceOrderNew.setStatusDate(new Date());
			}
			}
			if(serviceOrderNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
				serviceOrderNew.setUgwIntId(serviceOrderNew.getShipNumber());
			}
			if("linkUPNotes".equals(linkUp)){
				serviceOrderNew.setServiceType(serviceType);
			}
			serviceOrderNew=serviceOrderManager.save(serviceOrderNew);
			
			if("linkUPNotes".equals(linkUp) && !"".equals(notesSubType) && notesSubType != null){
				
				List<Notes> list = notesManager.getNoteByNotesId(trackingStatusId, sessionCorpID,notesSubType);
				for (Notes notes1 : list) {
					Notes notes = new Notes();
					
					BeanUtils.copyProperties(notes,notes1);
					notes.setId(null);
					notes.setCorpID(serviceOrderNew.getCorpID());
					notes.setCustomerNumber(serviceOrderNew.getSequenceNumber());
					notes.setNotesId(serviceOrderNew.getShipNumber());
					notesManager.save(notes);
				}
			}
			
			if("linkUPNotes".equals(linkUp)){
				if((documentIdListSO!=null)&&(!documentIdListSO.equalsIgnoreCase(""))){
			    		String docListArray[]=documentIdListSO.split(",");
			    			for(int i=0;i<docListArray.length;i++){
					    		String idVal=docListArray[i];
					    		MyFile myfile = myFileManager.get(Long.parseLong(idVal));
					    		String fileIdval = myfile.getFileId();
					    		Integer fieldLength = fileIdval.length();
					    		if(fieldLength.equals(13)){
					    			 String networkLinkId = sessionCorpID + (myfile.getId()).toString();
						     		 myFileManager.upDateNetworkLinkId(networkLinkId,myfile.getId());
						    			MyFile myFileTemp = new MyFile();
						    			myFileTemp.setCorpID(customerFileNew.getCorpID());
						    			myFileTemp.setFileId(shipnumber);
						    			myFileTemp.setCustomerNumber(customerFileNew.getSequenceNumber());
						    			myFileTemp.setActive(myfile.getActive());
						    			myFileTemp.setDescription(myfile.getDescription());
						    			myFileTemp.setDocumentCategory(myfile.getDocumentCategory());
						    			myFileTemp.setFileFileName(myfile.getFileFileName());
						    			myFileTemp.setFileContentType(myfile.getFileContentType());
						    			myFileTemp.setFileType(myfile.getFileType());
						    			myFileTemp.setFileSize(myfile.getFileSize());
						    			myFileTemp.setIsSecure(myfile.getIsSecure());
						    			myFileTemp.setLocation(myfile.getLocation());
						    			myFileTemp.setMapFolder(myfile.getMapFolder());
						    			myFileTemp.setCoverPageStripped(myfile.getCoverPageStripped());
						    			myFileTemp.setIsAccportal(myfile.getIsAccportal());
						    			myFileTemp.setIsCportal(myfile.getIsCportal());
						    			myFileTemp.setIsPartnerPortal(myfile.getIsPartnerPortal());
						    			myFileTemp.setIsServiceProvider(myfile.getIsServiceProvider());
						    			myFileTemp.setInvoiceAttachment(myfile.getInvoiceAttachment());
						    			myFileTemp.setIsBookingAgent(myfile.getIsBookingAgent());
						    			myFileTemp.setIsNetworkAgent(myfile.getIsNetworkAgent());
						    			myFileTemp.setIsOriginAgent(myfile.getIsOriginAgent());
						    			myFileTemp.setIsDestAgent(myfile.getIsDestAgent());
						    			myFileTemp.setIsSubOriginAgent(myfile.getIsSubOriginAgent());
						    			myFileTemp.setIsSubDestAgent(myfile.getIsSubDestAgent());
						    			myFileTemp.setNetworkLinkId(networkLinkId);
						    			myFileTemp.setCreatedOn(new Date());
						    			myFileTemp.setUpdatedOn(new Date());
						    			myFileTemp.setCreatedBy("Networking"+":"+getRequest().getRemoteUser());
						    			myFileTemp.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
						    			myFileManager.save(myFileTemp);
					    			
					    		}
			    			}
						}	
				
			if(allIdOfMyFile!=null && !allIdOfMyFile.equalsIgnoreCase("")){
				String myFileIdArray[] = allIdOfMyFile.split(",");
				for(int i=0;i<myFileIdArray.length;i++){
		    		String idVal=myFileIdArray[i];
					MyFile myFile = myFileManager.get(Long.parseLong(idVal));
					if(myFileFieldName!=null && !myFileFieldName.equalsIgnoreCase("") && (myFileFieldName.equalsIgnoreCase("issuboriginagent"))){
						myFile.setIsSubOriginAgent(false);
					}
					if(myFileFieldName!=null && !myFileFieldName.equalsIgnoreCase("") && (myFileFieldName.equalsIgnoreCase("isoriginagent"))){
						myFile.setIsOriginAgent(false);
					}
					if(myFileFieldName!=null && !myFileFieldName.equalsIgnoreCase("") && (myFileFieldName.equalsIgnoreCase("isdestagent"))){
						myFile.setIsDestAgent(false);
					}
					if(myFileFieldName!=null && !myFileFieldName.equalsIgnoreCase("") && (myFileFieldName.equalsIgnoreCase("issubdestagent"))){
						myFile.setIsSubDestAgent(false);
					}
					if(myFileFieldName!=null && !myFileFieldName.equalsIgnoreCase("") && (myFileFieldName.equalsIgnoreCase("isbookingagent"))){
						myFile.setIsBookingAgent(false);
					}
					if(myFileFieldName!=null && !myFileFieldName.equalsIgnoreCase("") && (myFileFieldName.equalsIgnoreCase("isnetworkagent"))){
						myFile.setIsNetworkAgent(false);
					}
				}
				
			}
		}
			/*if(serviceOrderNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
			  		if(!serviceOrderNew.getJob().toString().trim().equalsIgnoreCase("RLO")){
            			Long StartTime = System.currentTimeMillis();
            			customerFileAction.createIntegrationLogInfo(serviceOrderNew.getShipNumber(),"CREATE");
            			insertIntoTracker(serviceOrderNew.getShipNumber(), "Manual");
            			Long timeTaken =  System.currentTimeMillis() - StartTime;
                		logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");
                    }
            	
			}*/
			
			
			 Miscellaneous miscellaneousNew = new Miscellaneous();
			 List miscellaneousFieldToSync=customerFileManager.findFieldToSyncCreate("Miscellaneous",fieldType,uniqueFieldType);
				Iterator miscellaneousFields=miscellaneousFieldToSync.iterator();
				while(miscellaneousFields.hasNext()){
					String field=miscellaneousFields.next().toString();
					String[] splitField=field.split("~");
    				String fieldFrom=splitField[0];
    				String fieldTo=splitField[1];
					PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					beanUtilsBean.setProperty(miscellaneousNew,fieldTo.trim(),beanUtilsBean.getProperty(miscellaneous, fieldFrom.trim()));
					}catch(Exception ex){
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
				}
			 
			 miscellaneousNew.setId(serviceOrderNew.getId());
			 miscellaneousNew.setCorpID(serviceOrderNew.getCorpID());
			 miscellaneousNew.setShipNumber(serviceOrderNew.getShipNumber());
			 miscellaneousNew.setShip(serviceOrderNew.getShip());
			 miscellaneousNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
			 miscellaneousNew.setCreatedBy("Networking");
			 miscellaneousNew.setUpdatedBy("Networking");
			 miscellaneousNew.setCreatedOn(new Date());
			 miscellaneousNew.setUpdatedOn(new Date());
			 try{
			 String  unitData =customerFileManager.getNetworkUnit(serviceOrderNew.getCorpID());
			 String[] splitField=unitData.split("~");
			 String WeightUnit="";
			 String VolumeUnit="";
			 if(splitField[0]!=null && (!(splitField[0].trim().equalsIgnoreCase("No")))){
			 WeightUnit=	 splitField[0].trim();
			 }
			 if(splitField[1]!=null && (!(splitField[1].trim().equalsIgnoreCase("No")))){
			  VolumeUnit=splitField[1].trim();
			 }
			 if(WeightUnit!=null && (!(WeightUnit.equals("")))){
				 miscellaneousNew.setUnit1(WeightUnit)	; 
			 }
			 if(VolumeUnit!=null && (!(VolumeUnit.equals("")))){
				 miscellaneousNew.setUnit2(VolumeUnit)	; 
			 }
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 miscellaneousNew=miscellaneousManager.save(miscellaneousNew);
	         
	         if(miscellaneousNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
				 miscellaneousNew.setUgwIntId(miscellaneousNew.getId().toString());
			 }
	         miscellaneousNew=miscellaneousManager.save(miscellaneousNew);
	         
	         
	         String tempRole = trackingStatusManager.getAgentTypeRole(sessionCorpID,serviceOrder.getId());
	         
	         if(tempRole.equals("OA")){
	        	 trackingStatus.setOriginAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()))
	        		 trackingStatus.setDestinationAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode()))
	        		 trackingStatus.setDestinationSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode()))
	        		 trackingStatus.setOriginSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginAgentCode().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode()))
	        		 trackingStatus.setNetworkAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
	        		 trackingStatus.setBookingAgentExSO(serviceOrder.getShipNumber());
	         }else if(tempRole.equals("DA")){
	        	 trackingStatus.setDestinationAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()))
	        		 trackingStatus.setOriginAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode()))
	        		 trackingStatus.setDestinationSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode()))
	        		 trackingStatus.setOriginSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationAgentCode().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode()))
	        		 trackingStatus.setNetworkAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
	        		 trackingStatus.setBookingAgentExSO(serviceOrder.getShipNumber());
	         }else if(tempRole.equals("NWA")){
	        	 trackingStatus.setNetworkAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getNetworkPartnerCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()))
	        		 trackingStatus.setOriginAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getNetworkPartnerCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode()))
	        		 trackingStatus.setDestinationSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getNetworkPartnerCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode()))
	        		 trackingStatus.setOriginSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getNetworkPartnerCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()))
	        		 trackingStatus.setDestinationAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getNetworkPartnerCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
	        		 trackingStatus.setBookingAgentExSO(serviceOrder.getShipNumber());
	         }else if(tempRole.equals("BA")){
	        	 trackingStatus.setBookingAgentExSO(serviceOrder.getShipNumber());
	        	 if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()))
	        		 trackingStatus.setOriginAgentExSO(serviceOrder.getShipNumber());
	        	 if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode()))
	        		 trackingStatus.setDestinationSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode()))
	        		 trackingStatus.setOriginSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()))
	        		 trackingStatus.setDestinationAgentExSO(serviceOrder.getShipNumber());
	        	 if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode()))
	        		 trackingStatus.setNetworkAgentExSO(serviceOrder.getShipNumber());
	         }else if(tempRole.equals("SDA")){
	        	 trackingStatus.setDestinationSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationSubAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()))
	        		 trackingStatus.setOriginAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationSubAgentCode().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode()))
	        		 trackingStatus.setNetworkAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationSubAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode()))
	        		 trackingStatus.setOriginSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationSubAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()))
	        		 trackingStatus.setDestinationAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getDestinationSubAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
	        		 trackingStatus.setBookingAgentExSO(serviceOrder.getShipNumber());
	         }else if(tempRole.equals("SOA")){
	        	 trackingStatus.setOriginSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()))
	        		 trackingStatus.setOriginAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode()))
	        		 trackingStatus.setNetworkAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode()))
	        		 trackingStatus.setDestinationSubAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()))
	        		 trackingStatus.setDestinationAgentExSO(serviceOrder.getShipNumber());
	        	 if(trackingStatus.getOriginSubAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
	        		 trackingStatus.setBookingAgentExSO(serviceOrder.getShipNumber());
	         }  
				
	         trackingStatus = trackingStatusManager.save(trackingStatus);
	         
	         
	         
	         
	         
			TrackingStatus trackingStatusNew=new TrackingStatus();
			
			List trackingStatusFieldToSync=customerFileManager.findFieldToSyncCreate("TrackingStatus",fieldType,uniqueFieldType);
			Iterator trackingStatusFields=trackingStatusFieldToSync.iterator();
			while(trackingStatusFields.hasNext()){
				String field=trackingStatusFields.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0].trim();
				String fieldTo=splitField[1].trim();
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
               		beanUtilsBean.setProperty(trackingStatusNew,fieldTo,beanUtilsBean.getProperty(trackingStatus, fieldFrom));
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
			
			trackingStatusNew.setId(serviceOrderNew.getId());
			trackingStatusNew.setContractType("");
			trackingStatusNew.setCorpID(serviceOrderNew.getCorpID());
			trackingStatusNew.setServiceOrder(serviceOrderNew);
			trackingStatusNew.setMiscellaneous(miscellaneousNew);
			trackingStatusNew.setShipNumber(serviceOrderNew.getShipNumber());
			trackingStatusNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
			trackingStatusNew.setSurveyTimeFrom(customerFileNew.getSurveyTime());
			trackingStatusNew.setSurveyTimeTo(customerFileNew.getSurveyTime2());
			trackingStatusNew.setSurveyDate(customerFileNew.getSurvey());
			try{
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			
			if(forSection.equalsIgnoreCase("destination")){
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setDestinationAgentExSO(trackingStatusNew.getShipNumber());
				try{
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
			if(forSection.equalsIgnoreCase("destinationSub")){
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setDestinationSubAgentExSO(trackingStatusNew.getShipNumber());
				
				try{
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
			if(forSection.equalsIgnoreCase("origin")){
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setOriginAgentExSO(trackingStatusNew.getShipNumber());
				
				try{
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
			if(forSection.equalsIgnoreCase("originSub")){
			    trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setOriginSubAgentExSO(trackingStatusNew.getShipNumber());
				
				try{
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
			if(forSection.equalsIgnoreCase("NetworkPartnerCode")){
				trackingStatusNew.setContractType(uniqueFieldType);
				trackingStatusNew.setSoNetworkGroup(true);
				trackingStatusNew.setUtsiNetworkGroup(false);
				if(networkAgent){
					trackingStatusNew.setAccNetworkGroup(true);
					trackingStatusNew.setAgentNetworkGroup(true);
				}else{
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				}
				trackingStatusNew.setNetworkAgentExSO(trackingStatusNew.getShipNumber());
				try{
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
			if(forSection.equalsIgnoreCase("bookingAgentCode") || ((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent )){
				trackingStatusNew.setContractType(uniqueFieldType);
				trackingStatusNew.setNetworkPartnerCode(trackingStatus.getNetworkPartnerCode());
				trackingStatusNew.setNetworkPartnerName(trackingStatus.getNetworkPartnerName());
				try{
					}catch(Exception ex){
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
				trackingStatusNew.setSoNetworkGroup(true);
				trackingStatusNew.setUtsiNetworkGroup(false);
				if(networkAgent){
					trackingStatusNew.setAccNetworkGroup(true);
					trackingStatusNew.setAgentNetworkGroup(true);
				}else{
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				}
				trackingStatusNew.setBookingAgentExSO(trackingStatusNew.getShipNumber());
				try{
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
			if(forSection.equalsIgnoreCase("UGWWnetworkPartnerCode")){ 
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setNetworkAgentExSO(trackingStatusNew.getShipNumber());
				try{ 
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
			
			trackingStatusNew.setCreatedBy("Networking");
			trackingStatusNew.setUpdatedBy("Networking");
			trackingStatusNew.setCreatedOn(new Date());
			trackingStatusNew.setUpdatedOn(new Date());
						
			trackingStatusNew=trackingStatusManager.save(trackingStatusNew);
			if(trackingStatusNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
				trackingStatusNew.setUgwIntId(trackingStatusNew.getId().toString());
			 }
			 try{
				 if(trackingStatusNew.getForwarderCode()!=null && (!(trackingStatusNew.getForwarderCode().equalsIgnoreCase("")))){
					boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getForwarderCode());
					if(checkVendorCode){
						
					}else{
						trackingStatusNew.setForwarderCode("");	
					}
				 }
				 }catch(Exception e){
					e.printStackTrace(); 
				 }
			 try{
				 if(trackingStatusNew.getBrokerCode()!=null && (!(trackingStatusNew.getBrokerCode().equalsIgnoreCase("")))){
					boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getBrokerCode());
					if(checkVendorCode){
						
					}else{
						trackingStatusNew.setBrokerCode("");	
					}
				 }
				 }catch(Exception e){
					e.printStackTrace(); 
				 }
			trackingStatusNew=trackingStatusManager.save(trackingStatusNew);
			billToCodeAcc = billing.getBillToCode();

			 try{
			 Billing billingNew = new Billing(); 
			 
			        List billingFieldToSync=new ArrayList();
			        billingFieldToSync=customerFileManager.findBillingFieldToSyncCreate("Billing",fieldType,uniqueFieldType);
			        Iterator billingFields=billingFieldToSync.iterator();
					while(billingFields.hasNext()){
						String field=billingFields.next().toString();
						String[] splitField=field.split("~");
	    				String fieldFrom=splitField[0];
	    				String fieldTo=splitField[1];
						PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
						try{
						beanUtilsBean.setProperty(billingNew,fieldTo.trim(),beanUtilsBean.getProperty(billing, fieldFrom.trim()));
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
					}
					if(billingDMMContractType){
					if(forSection.equalsIgnoreCase("bookingAgentCode") || ((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent )){
						billingNew.setBillToCode(trackingStatus.getNetworkPartnerCode());
						billingNew.setBillToName(trackingStatus.getNetworkPartnerName());
						billingNew.setNetworkBillToCode(customerFileNew.getAccountCode());
						billingNew.setNetworkBillToName(customerFileNew.getAccountName());
					}
					else if(forSection.equalsIgnoreCase("networkPartnerCode")){
						billingNew.setBillToCode(billing.getNetworkBillToCode());
						billingNew.setBillToName(billing.getNetworkBillToName());
						}
					}
					
			 if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode"))) && (!((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent ))){
			 billingNew.setBillToCode(customerFileNew.getBillToCode());
			 billingNew.setBillToName(customerFileNew.getBillToName());
			 billingNew.setContract(customerFileNew.getContract());
			 billingNew.setBillTo1Point(customerFileNew.getBillPayMethod());
			 billingNew.setBillingInstructionCodeWithDesc("CON : As per Contract");
			 billingNew.setBillingInstruction("As per Contract");
			 billingNew.setSpecialInstruction("As per Quote");
			 }else{
				 if(billingDMMContractType){
				 try{	 
					 String billingCurrency="";
					 String agentBaseCurrency="";
					 agentBaseCurrency=accountLineManager.searchBaseCurrency(serviceOrderNew.getCorpID()).get(0).toString();
					 billingCurrency=serviceOrderManager.getBillingCurrencyValue(billingNew.getBillToCode(),serviceOrderNew.getCorpID());
					 if(!billingCurrency.equalsIgnoreCase(""))
					  {
						 billingNew.setBillingCurrency(billingCurrency);
					  }else{
					      billingNew.setBillingCurrency(agentBaseCurrency);
					   }
					 }catch(Exception e){
						 logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					 }
				 } 
			 }
			 
			 billingNew.setId(serviceOrderNew.getId());
			 billingNew.setCorpID(serviceOrderNew.getCorpID());
			 billingNew.setShipNumber(serviceOrderNew.getShipNumber());
			 billingNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
			 billingNew.setServiceOrder(serviceOrderNew);
			 billingNew.setCreatedBy("Networking");
			 billingNew.setUpdatedBy("Networking");
			 billingNew.setSystemDate(new Date());
			 billingNew.setCreatedOn(new Date());
			 billingNew.setUpdatedOn(new Date());
			 try{
			 if(trackingStatus.getSoNetworkGroup() && trackingStatus.getAccNetworkGroup() && billingCMMContractType && (((forSection.equalsIgnoreCase("bookingAgentCode"))||(forSection.equalsIgnoreCase("networkPartnerCode")))))	{
				 if(billing.getStorageVatDescr()!=null && (!(billing.getStorageVatDescr().toString().trim().equals("")))){ 
				 billingNew.setStorageVatDescr(sessionCorpID+"_"+billing.getStorageVatDescr()) ;
				 }
				 if(billing.getInsuranceVatDescr()!=null && (!(billing.getInsuranceVatDescr().toString().trim().equals("")))){
				 billingNew.setInsuranceVatDescr(sessionCorpID+"_"+billing.getInsuranceVatDescr()) ;
				 }
				 if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().trim().equals("")))){
				 billingNew.setPrimaryVatCode(sessionCorpID+"_"+billing.getPrimaryVatCode()) ;
				 }
				 if(billing.getSecondaryVatCode()!=null && (!(billing.getSecondaryVatCode().toString().trim().equals("")))){
				 billingNew.setSecondaryVatCode(sessionCorpID+"_"+billing.getSecondaryVatCode()) ;
				 }
				 if(billing.getPrivatePartyVatCode()!=null && (!(billing.getPrivatePartyVatCode().toString().trim().equals("")))){
				 billingNew.setPrivatePartyVatCode(sessionCorpID+"_"+billing.getPrivatePartyVatCode()) ;
				 }
				 billingNew.setBillComplete(billing.getBillComplete());
			 }
			 if(trackingStatus.getSoNetworkGroup() && (!trackingStatus.getAccNetworkGroup()) && billingCMMContractType && (((forSection.equalsIgnoreCase("bookingAgentCode"))||(forSection.equalsIgnoreCase("networkPartnerCode")))))	{
				 if(billing.getStorageVatDescr()!=null && (!(billing.getStorageVatDescr().toString().trim().equals("")))){ 
				 String storageVatDescr=billing.getStorageVatDescr();
				 if(storageVatDescr.contains(serviceOrderNew.getCorpID()+"_")){
					 storageVatDescr=storageVatDescr.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					}
				 billingNew.setStorageVatDescr(storageVatDescr) ;
				 }
				 if(billing.getInsuranceVatDescr()!=null && (!(billing.getInsuranceVatDescr().toString().trim().equals("")))){
				 String insuranceVatDescr=billing.getInsuranceVatDescr();
				 if(insuranceVatDescr.contains(serviceOrderNew.getCorpID()+"_")){
					 insuranceVatDescr=insuranceVatDescr.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					}
				 billingNew.setInsuranceVatDescr(insuranceVatDescr) ; 
				 }
				 if(billing.getPrimaryVatCode()!=null && (!(billing.getPrimaryVatCode().toString().trim().equals("")))){
				 String primaryVatCode=billing.getPrimaryVatCode();
				 if(primaryVatCode.contains(serviceOrderNew.getCorpID()+"_")){
					 primaryVatCode=primaryVatCode.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					}
				 billingNew.setPrimaryVatCode(primaryVatCode) ; 
				 }
				 if(billing.getSecondaryVatCode()!=null && (!(billing.getSecondaryVatCode().toString().trim().equals("")))){
				 String secondaryVatCode=billing.getSecondaryVatCode();
				 if(secondaryVatCode.contains(serviceOrderNew.getCorpID()+"_")){
					 secondaryVatCode=secondaryVatCode.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					}
				 billingNew.setSecondaryVatCode(secondaryVatCode) ;
				 }
				 if(billing.getPrivatePartyVatCode()!=null && (!(billing.getPrivatePartyVatCode().toString().trim().equals("")))){
				 String privatePartyVatCode=billing.getPrivatePartyVatCode();
				 if(privatePartyVatCode.contains(serviceOrderNew.getCorpID()+"_")){
					 privatePartyVatCode=privatePartyVatCode.replaceAll(serviceOrderNew.getCorpID()+"_", "");	
					} 
				 billingNew.setPrivatePartyVatCode(privatePartyVatCode) ;
				 }
			 }
			 }catch( Exception e){
	    	    	
	    	 }
			 if(billingNew.getBillToCode()!=null && (!(billingNew.getBillToCode().equalsIgnoreCase("")))){
				 pricingBillingPaybaleList=accountLineManager.pricingBillingPaybaleName(billingNew.getBillToCode(),serviceOrderNew.getCorpID(),serviceOrderNew.getJob());
				 if((pricingBillingPaybaleList!=null) && (!(pricingBillingPaybaleList.isEmpty()))){
						String[] str1 = pricingBillingPaybaleList.get(0).toString().split("#");
						if(!(str1[0].equalsIgnoreCase("1"))){
							billingNew.setPersonPricing(str1[0]);
							}
						if(!(str1[1].equalsIgnoreCase("1"))){
							billingNew.setPersonBilling(str1[1]);
							}
						if(!(str1[2].equalsIgnoreCase("1"))){
							billingNew.setPersonPayable(str1[2]);
							}
						if(!(str1[3].equalsIgnoreCase("1"))){
							billingNew.setAuditor(str1[3]);
							}
						if(!(str1[5].equalsIgnoreCase("1"))){
							billingNew.setClaimHandler(str1[5]);
							}
				 	}
			 }
			 try{
			 if(billingNew.getVendorCode()!=null && (!(billingNew.getVendorCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(billingNew.getCorpID(),billingNew.getVendorCode());
				if(checkVendorCode){
					
				}else{
					billingNew.setVendorCode("");	
				}
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
			 if(trackingStatus.getSoNetworkGroup() && (((forSection.equalsIgnoreCase("bookingAgentCode"))||(forSection.equalsIgnoreCase("networkPartnerCode"))))){
			 String externalCorpID = serviceOrderNew.getCorpID();
	    		String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
	    		if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
					DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
					String estExchangeRate="1"; 
					BigDecimal estExchangeRateBig=new BigDecimal("1.0000");
		            List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,billingNew.getCurrency());
					 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
					 {
						 estExchangeRate=estRate.get(0).toString();
					 
					 } 
					estExchangeRateBig=new BigDecimal(estExchangeRate);	
					billingNew.setExchangeRate(estExchangeRateBig);
					 DecimalFormat decimalFormat = new DecimalFormat("#.##");
		            try{
		            	billingNew.setBaseInsuranceValue(new BigDecimal(decimalFormat.format((billing.getInsuranceValueActual().doubleValue())/(estExchangeRateBig.doubleValue()))));	
		            }catch(Exception e){
		            	e.printStackTrace();
		            }
		            try{
		            	billingNew.setBaseInsuranceTotal((new BigDecimal(decimalFormat.format(((((billingNew.getBaseInsuranceValue())).doubleValue())*(((billing.getInsuranceBuyRate())).doubleValue()) )/(100)))).toString());	
		            }catch(Exception e){
		            	e.printStackTrace();
		            }
		             
	    		}
			 }
			 billingNew=billingManager.save(billingNew);
			 
			 if(billingNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
				 billingNew.setUgwIntId(billingNew.getId().toString());
				 }
			 billingNew=billingManager.save(billingNew);
			 
			 
			 }catch(Exception ex){
				 logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
			 }
			 customerFileManager.updateIsNetworkFlag(customerFile.getId(), customerFile.getCorpID(), serviceOrder.getId(),customerFileNew.getContractType(),customerFileNew.getIsNetworkGroup());
			try{
				if(trackingStatus.getAccNetworkGroup())	{
				if(forSection.equalsIgnoreCase("NetworkPartnerCode")){ 
				if(billingDMMContractType){
				if(billToCodeAcc!=null && (!(billToCodeAcc.toString().equals(""))) && trackingStatus.getNetworkPartnerCode()!=null && (!(trackingStatus.getNetworkPartnerCode().equals(""))) && (!(billToCodeAcc.toString().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode())))){	
				 trackingStatusManager.updateDMMAgentBilltocode(sessionCorpID, serviceOrder.getId(),trackingStatus.getNetworkPartnerCode(),trackingStatus.getNetworkPartnerName()); 		 
				
				 

		         	List accountlineList= accountLineManager.getAllAccountLine(serviceOrder.getId()); 
		      		if(accountlineList!=null && (!(accountlineList.isEmpty()))){
		    			 Iterator accountlineIterator=accountlineList.iterator();
		    			  while (accountlineIterator.hasNext()) {
		    				try{  
		    				Long  Accid = (Long) accountlineIterator.next();
		    				accountLine = accountLineManager.get(Accid);
		    				String billtocode=accountLine.getBillToCode(); 
		    				if(accountLine.getRecInvoiceNumber()==null || accountLine.getRecInvoiceNumber().toString().trim().equals("")){ 
		    					if(billtocode!=null &&  billtocode.equalsIgnoreCase(billToCodeAcc)){	
		    					accountLine.setBillToCode(trackingStatus.getNetworkPartnerCode());
		    					accountLine.setBillToName(trackingStatus.getNetworkPartnerName());
		    					} 
		    					 
		    						
		    				if(billtocode!=null && accountLine.getBillToCode()!=null &&  (!(billtocode.equalsIgnoreCase(accountLine.getBillToCode())))){		
		    				String billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLine.getBillToCode(),accountLine.getCorpID());
		    				if(!billingCurrency.equalsIgnoreCase(""))
		    				{
		    					accountLine.setRecRateCurrency(billingCurrency);
		    					accountLine.setEstSellCurrency(billingCurrency);
		    					accountLine.setRevisionSellCurrency(billingCurrency); 
		    					String recExchangeRate="1"; 
		    		            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
		    		            List recRate=exchangeRateManager.findAccExchangeRate(accountLine.getCorpID(),accountLine.getRecRateCurrency());
		    					 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
		    					 {
		    						 recExchangeRate=recRate.get(0).toString();
		    					 }
		    					recExchangeRateBig=new BigDecimal(recExchangeRate);
		    					accountLine.setRecRateExchange(recExchangeRateBig);
		    					accountLine.setRacValueDate(new Date());
		    					if(accountLine.getRecRate()!=null){
		    					accountLine.setRecCurrencyRate(accountLine.getRecRate().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setRecCurrencyRate(new BigDecimal(0.00));	
		    					}
		    					if(accountLine.getActualRevenue()!=null){
		    					accountLine.setActualRevenueForeign(accountLine.getActualRevenue().multiply(recExchangeRateBig)); 
		    					}else{
		    					accountLine.setActualRevenueForeign(new BigDecimal(0.00));	
		    					}
		    					accountLine.setEstSellExchangeRate(recExchangeRateBig);
		    					accountLine.setEstSellValueDate(new Date()); 
		    					if(accountLine.getEstimateSellRate()!=null){
		    					accountLine.setEstSellLocalRate(accountLine.getEstimateSellRate().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setEstSellLocalRate(new BigDecimal(0.00));	
		    					}
		    					if(accountLine.getEstimateRevenueAmount()!=null){
		    					accountLine.setEstSellLocalAmount(accountLine.getEstimateRevenueAmount().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setEstSellLocalAmount(new BigDecimal(0.00));	
		    					}
		    					accountLine.setRevisionSellExchangeRate(recExchangeRateBig);
		    					accountLine.setRevisionSellValueDate(new Date()); 
		    					if(accountLine.getRevisionSellRate()!=null){
		    					accountLine.setRevisionSellLocalRate(accountLine.getRevisionSellRate().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setRevisionSellLocalRate(new BigDecimal(0.00));	
		    					}
		    					if(accountLine.getRevisionRevenueAmount()!=null){
		    					accountLine.setRevisionSellLocalAmount(accountLine.getRevisionRevenueAmount().multiply(recExchangeRateBig));
		    					}else{
		    						accountLine.setRevisionSellLocalAmount(new BigDecimal(0.00));	
		    					}
		    					try{
		    					    String recVatPercent=accountLine.getRecVatPercent();
		    					    BigDecimal recVatPercentBig=new BigDecimal("0.00");
		    					    BigDecimal recVatAmmountBig=new BigDecimal("0.00");
		    					    BigDecimal revisionVatAmmountBig=new BigDecimal("0.00");
		    					    BigDecimal estVatAmmountBig=new BigDecimal("0.00");
		    					    if(recVatPercent!=null && (!(recVatPercent.trim().equals("")))){
		    					    	recVatPercentBig=new BigDecimal(recVatPercent);
		    					    }
		    					    DecimalFormat decimalFormat = new DecimalFormat("#.####");
		    					   recVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLine.getActualRevenueForeign().multiply(recVatPercentBig)).doubleValue())/100)));
		    					   try{
		    						    estVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLine.getEstSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
		    						    revisionVatAmmountBig = (new BigDecimal(decimalFormat.format(((accountLine.getRevisionSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
		    						    }catch(Exception e){
		    						    	
		    						    }
		    					   accountLine.setRecVatAmt(recVatAmmountBig);
		    					   accountLine.setEstVatAmt(estVatAmmountBig); 
		    					   accountLine.setRevisionVatAmt(revisionVatAmmountBig); 
		    					  
		    					    }catch(Exception e){
		    		                    e.printStackTrace();			    	
		    					    }  
		    				}
		    				accountLine= accountLineManager.save(accountLine);
		    			
		    				}
		    				} 
		    				}catch(Exception e){
		    					e.printStackTrace();
		    				}
		    			  }
		    			  }
		 		}
		         
				}
				}
				}
			}catch(Exception e){
				
			}
			String survryLink="No";
			 try{ 
				 if((fieldType!=null && !fieldType.equals("") && fieldType.equals("CMM/DMM") ) || company.getSurveyLinking()){
					 if(company.getSurveyLinking()!=null && company.getSurveyLinking() ){
						 survryLink="Yes";
					 }
					 if(survryLink.equals("No") && fieldType!=null && !fieldType.equals("") && fieldType.equals("CMM/DMM") ){
						 TrackingStatus ts = trackingStatusManager.getForOtherCorpid(serviceOrderNew.getId());
						 if(ts.getSoNetworkGroup()){
							 survryLink="Yes";
						 }
					 }
					 if(survryLink.equalsIgnoreCase("Yes")){
						 createAccessInfo(serviceOrderNew,"SO",serviceOrder);
						 createInventoryData(serviceOrderNew,serviceOrder);
					 }
				 }
				 
				 createContainerRecords(serviceOrder, serviceOrderNew);
				 createPieceCountsRecords(serviceOrder, serviceOrderNew);
				 createVehicleRecords(serviceOrder, serviceOrderNew);
				 createRoutingRecords(serviceOrder, serviceOrderNew);
				 createConsigneeInstruction(serviceOrder, serviceOrderNew);
			    if(trackingStatus.getSoNetworkGroup())	{
				if(forSection.equalsIgnoreCase("NetworkPartnerCode") || forSection.equalsIgnoreCase("bookingAgentCode") || ((serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getDestinationSubAgentCode())|| serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginAgentCode()) || serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatus.getOriginSubAgentCode())) && (billingCMMContractType || billingDMMContractType)&& networkAgent )){ 
					createClaim(serviceOrder, serviceOrderNew);
				}
			
			}
			}catch(Exception ex){
				logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
			}
			try{
				 if(trackingStatus.getAccNetworkGroup())	{
				if(forSection.equalsIgnoreCase("NetworkPartnerCode")){ 
				 if(billingCMMContractType){
					 accountLineList = accountLineManager.getAccountLinesID(serviceOrder.getShipNumber());
				     Iterator it=accountLineList.listIterator();
				     while(it.hasNext()){
						  Long row=(Long)it.next();
						  accountLine = accountLineManager.getForOtherCorpid(row);
						  try{
							  String partnertype="";
	                      	  if(accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))){
	                      		  partnertype=  partnerManager.checkPartnerType(accountLine.getBillToCode());
	                      	  }
							  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (!(partnertype.equals("")))  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals(""))){	  
							  createAccountLine(serviceOrderNew,serviceOrder,accountLine)  ;
							  }
						  }catch(Exception e){
							  logger.error("Exception Occour: "+ e.getStackTrace()[0]);
							}
				     }
					 
				 }
				 if(billingDMMContractType){
					 accountLineList = accountLineManager.getAccountLinesID(serviceOrder.getShipNumber());
				     Iterator it=accountLineList.listIterator();
				     while(it.hasNext()){
						  Long row=(Long)it.next();
						  accountLine = accountLineManager.getForOtherCorpid(row);
						  try{
							  boolean billtotype=false;
	                      	  if(billing.getNetworkBillToCode()!=null && (!(billing.getNetworkBillToCode().toString().equals(""))) && accountLine.getBillToCode()!=null && (!(accountLine.getBillToCode().toString().equals("")))  && accountLine.getNetworkBillToCode()!=null && (!(accountLine.getNetworkBillToCode().toString().equals("")))  ){
	                      		  billtotype =accountLine.getBillToCode().trim().equalsIgnoreCase(trackingStatus.getNetworkPartnerCode().trim());
	                      	  }
							  if((accountLine.getEstimateRevenueAmount().doubleValue()!=0 || accountLine.getRevisionRevenueAmount().doubleValue()!=0 || accountLine.getActualRevenue().doubleValue()!=0) && (billtotype)  && (accountLine.getRecInvoiceNumber() ==null || accountLine.getRecInvoiceNumber().toString().trim().equals("")) ){
							  createDMMAccountLine(serviceOrderNew,serviceOrder,accountLine,billing)  ;
							  }
						  }catch(Exception e){
							  logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								
							}
				     }
					 
				 }
				 updateExternalAccountLine(serviceOrderNew,customerFileNew.getCorpID());
			 }
			}
			}catch(Exception e){
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
		 	 TrackingStatus trackingStatusOLD=new TrackingStatus();
			 BeanUtilsBean beanUtilsBean4 = BeanUtilsBean.getInstance();
			 beanUtilsBean4.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			 beanUtilsBean4.copyProperties(trackingStatusOLD, trackingStatus); 
			if(forSection.equalsIgnoreCase("destination")){

				trackingStatusOLD.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginAgentCode().equalsIgnoreCase(trackingStatusOLD.getDestinationAgentCode()))
					trackingStatusOLD.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getDestinationAgentCode()))
					trackingStatusOLD.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getNetworkPartnerCode().equalsIgnoreCase(trackingStatusOLD.getDestinationAgentCode()))
					trackingStatusOLD.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				
				if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatusOLD.getDestinationAgentCode())){
					trackingStatusOLD.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					if((billingCMMContractType || billingDMMContractType)&& networkAgent){
						trackingStatusOLD.setSoNetworkGroup(true);
						trackingStatusOLD.setAgentNetworkGroup(false);
						trackingStatusOLD.setAccNetworkGroup(false);	
						trackingStatusOLD.setUtsiNetworkGroup(true);
						trackingStatusOLD.setContractType(uniqueFieldType); 
					}
				}
				if(trackingStatusOLD.getDestinationSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getDestinationAgentCode()))
					trackingStatusOLD.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				
				
			
				
				if(trackingStatusOLD.getDestinationSubAgentExSO()!=null && !trackingStatusOLD.getDestinationSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SDA");
					}
				}
				
				if(trackingStatusOLD.getOriginSubAgentExSO()!=null && !trackingStatusOLD.getOriginSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SOA");
					}
				}
				
				if(trackingStatusOLD.getBookingAgentExSO()!=null && !trackingStatusOLD.getBookingAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getBookingAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				
				if(trackingStatusOLD.getOriginAgentExSO()!=null && !trackingStatusOLD.getOriginAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"OA");
					}
				}
				
				if(trackingStatusOLD.getNetworkAgentExSO()!=null && !trackingStatusOLD.getNetworkAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getNetworkAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"NWA");
					}
				}
				if(serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(serviceOrder.getBookingAgentShipNumber());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				
			}
			if(forSection.equalsIgnoreCase("destinationSub")){

				trackingStatusOLD.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginAgentCode().equalsIgnoreCase(trackingStatusOLD.getDestinationSubAgentCode()))
					trackingStatusOLD.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getDestinationSubAgentCode()))
					trackingStatusOLD.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getNetworkPartnerCode().equalsIgnoreCase(trackingStatusOLD.getDestinationSubAgentCode()))
					trackingStatusOLD.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				
				if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatusOLD.getDestinationSubAgentCode())){
					trackingStatusOLD.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				if((billingCMMContractType || billingDMMContractType)&& networkAgent){
					trackingStatusOLD.setSoNetworkGroup(true);
					trackingStatusOLD.setAgentNetworkGroup(false);
					trackingStatusOLD.setAccNetworkGroup(false);	
					trackingStatusOLD.setUtsiNetworkGroup(true);
					trackingStatusOLD.setContractType(uniqueFieldType); 
				}
				}
				
				if(trackingStatusOLD.getDestinationAgentCode().equalsIgnoreCase(trackingStatusOLD.getDestinationSubAgentCode()))
					trackingStatusOLD.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
			
				
				if(trackingStatusOLD.getDestinationAgentExSO()!=null && !trackingStatusOLD.getDestinationAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"DA");
					}
				}
				
				if(trackingStatusOLD.getOriginSubAgentExSO()!=null && !trackingStatusOLD.getOriginSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SOA");
					}
				}
				
				if(trackingStatusOLD.getBookingAgentExSO()!=null && !trackingStatusOLD.getBookingAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getBookingAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				
				if(trackingStatusOLD.getOriginAgentExSO()!=null && !trackingStatusOLD.getOriginAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"OA");
					}
				}
				
				if(trackingStatusOLD.getNetworkAgentExSO()!=null && !trackingStatusOLD.getNetworkAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getNetworkAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"NWA");
					}
				}
				if(serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(serviceOrder.getBookingAgentShipNumber());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
			}
			if(forSection.equalsIgnoreCase("origin")){

				trackingStatusOLD.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getDestinationSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getOriginAgentCode()))
					trackingStatusOLD.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getOriginAgentCode()))
					trackingStatusOLD.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getNetworkPartnerCode().equalsIgnoreCase(trackingStatusOLD.getOriginAgentCode()))
					trackingStatusOLD.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				
				if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatusOLD.getOriginAgentCode()))
				{
					trackingStatusOLD.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				if((billingCMMContractType || billingDMMContractType)&& networkAgent){
					trackingStatusOLD.setSoNetworkGroup(true);
					trackingStatusOLD.setAgentNetworkGroup(false);
					trackingStatusOLD.setAccNetworkGroup(false);	
					trackingStatusOLD.setUtsiNetworkGroup(true);
					trackingStatusOLD.setContractType(uniqueFieldType); 
				}
				}
				
				if(trackingStatusOLD.getDestinationAgentCode().equalsIgnoreCase(trackingStatusOLD.getOriginAgentCode()))
					trackingStatusOLD.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				
			
				
				if(trackingStatusOLD.getDestinationSubAgentExSO()!=null && !trackingStatusOLD.getDestinationSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SDA");
					}
				}
				
				if(trackingStatusOLD.getOriginSubAgentExSO()!=null && !trackingStatusOLD.getOriginSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SOA");
					}
				}
				
				if(trackingStatusOLD.getBookingAgentExSO()!=null && !trackingStatusOLD.getBookingAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getBookingAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				
				if(trackingStatusOLD.getDestinationAgentExSO()!=null && !trackingStatusOLD.getDestinationAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"DA");
					}
				}
				
				if(trackingStatusOLD.getNetworkAgentExSO()!=null && !trackingStatusOLD.getNetworkAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getNetworkAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"NWA");
					}
				}
				if(serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(serviceOrder.getBookingAgentShipNumber());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				
			}
			if(forSection.equalsIgnoreCase("originSub")){

				trackingStatusOLD.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getDestinationSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getOriginSubAgentCode()))
					trackingStatusOLD.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginAgentCode().equalsIgnoreCase(trackingStatusOLD.getOriginSubAgentCode()))
					trackingStatusOLD.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getNetworkPartnerCode().equalsIgnoreCase(trackingStatusOLD.getOriginSubAgentCode()))
					trackingStatusOLD.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				
				if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatusOLD.getOriginSubAgentCode()))
				{
					trackingStatusOLD.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				if((billingCMMContractType || billingDMMContractType)&& networkAgent){
					trackingStatusOLD.setSoNetworkGroup(true);
					trackingStatusOLD.setAgentNetworkGroup(false);
					trackingStatusOLD.setAccNetworkGroup(false);	
					trackingStatusOLD.setUtsiNetworkGroup(true);
					trackingStatusOLD.setContractType(uniqueFieldType);
				}
				}
				
				if(trackingStatusOLD.getDestinationAgentCode().equalsIgnoreCase(trackingStatusOLD.getOriginSubAgentCode()))
					trackingStatusOLD.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
			
				
				if(trackingStatusOLD.getDestinationSubAgentExSO()!=null && !trackingStatusOLD.getDestinationSubAgentExSO().equals("") ){
						List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationSubAgentExSO());
						if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
							ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
							networkControlEntry(serviceOrderNew,tempOrder,"SDA");
						}
				}
				
				if(trackingStatusOLD.getDestinationAgentExSO()!=null && !trackingStatusOLD.getDestinationAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"DA");
					}
				}
				
				if(trackingStatusOLD.getBookingAgentExSO()!=null && !trackingStatusOLD.getBookingAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getBookingAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				
				if(trackingStatusOLD.getOriginAgentExSO()!=null && !trackingStatusOLD.getOriginAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"OA");
					}
				}
				
				if(trackingStatusOLD.getNetworkAgentExSO()!=null && !trackingStatusOLD.getNetworkAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getNetworkAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"NWA");
					}
				}
				if(serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(serviceOrder.getBookingAgentShipNumber());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				
			}
			if(forSection.equalsIgnoreCase("NetworkPartnerCode")){

				trackingStatusOLD.setContractType(uniqueFieldType);
				trackingStatusOLD.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getDestinationSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getDestinationAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				
			
				
				if(trackingStatusOLD.getDestinationSubAgentExSO()!=null && !trackingStatusOLD.getDestinationSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SDA");
					}
				}
				
				if(trackingStatusOLD.getOriginSubAgentExSO()!=null && !trackingStatusOLD.getOriginSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SOA");
					}
				}
				
				if(trackingStatusOLD.getBookingAgentExSO()!=null && !trackingStatusOLD.getBookingAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getBookingAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				if(serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(serviceOrder.getBookingAgentShipNumber());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				
				if(trackingStatusOLD.getOriginAgentExSO()!=null && !trackingStatusOLD.getOriginAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"OA");
					}
				}
				
				if(trackingStatusOLD.getDestinationAgentExSO()!=null && !trackingStatusOLD.getDestinationAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"DA");
					}
				}
				
			} 
			if(forSection.equalsIgnoreCase("bookingAgentCode")){

				trackingStatusOLD.setContractType(uniqueFieldType);
				trackingStatusOLD.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getDestinationSubAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
					trackingStatusOLD.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
					trackingStatusOLD.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginSubAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
					trackingStatusOLD.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getNetworkPartnerCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
					trackingStatusOLD.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getDestinationAgentCode().equalsIgnoreCase(serviceOrder.getBookingAgentCode()))
					trackingStatusOLD.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
			
				
				if(trackingStatusOLD.getDestinationSubAgentExSO()!=null && !trackingStatusOLD.getDestinationSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SDA");
					}
				}
				
				if(trackingStatusOLD.getOriginSubAgentExSO()!=null && !trackingStatusOLD.getOriginSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SOA");
					}
				}
				
				if(trackingStatusOLD.getDestinationAgentExSO()!=null && !trackingStatusOLD.getDestinationAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"DA");
					}
				}
				
				if(trackingStatusOLD.getOriginAgentExSO()!=null && !trackingStatusOLD.getOriginAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"OA");
					}
				}
				
				if(trackingStatusOLD.getNetworkAgentExSO()!=null && !trackingStatusOLD.getNetworkAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getNetworkAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"NWA");
					}
				}
				if(serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(serviceOrder.getBookingAgentShipNumber());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
			}
			if(forSection.equalsIgnoreCase("UGWWnetworkPartnerCode")){

				trackingStatusOLD.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getDestinationSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getOriginSubAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				
				if(serviceOrder.getBookingAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				
				if(trackingStatusOLD.getDestinationAgentCode().equalsIgnoreCase(trackingStatusOLD.getNetworkPartnerCode()))
					trackingStatusOLD.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				
			
				
				if(trackingStatusOLD.getDestinationSubAgentExSO()!=null && !trackingStatusOLD.getDestinationSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SDA");
					}
				}
				
				if(trackingStatusOLD.getOriginSubAgentExSO()!=null && !trackingStatusOLD.getOriginSubAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginSubAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"SOA");
					}
				}
				
				if(trackingStatusOLD.getBookingAgentExSO()!=null && !trackingStatusOLD.getBookingAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getBookingAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
				
				if(trackingStatusOLD.getOriginAgentExSO()!=null && !trackingStatusOLD.getOriginAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getOriginAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"OA");
					}
				}
				
				if(trackingStatusOLD.getDestinationAgentExSO()!=null && !trackingStatusOLD.getDestinationAgentExSO().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(trackingStatusOLD.getDestinationAgentExSO());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"DA");
					}
				}
				if(serviceOrder.getBookingAgentShipNumber()!=null && !serviceOrder.getBookingAgentShipNumber().equals("") ){
					List tempList = serviceOrderManager.findIdByShipNumber(serviceOrder.getBookingAgentShipNumber());
					if(tempList!=null && !tempList.isEmpty() && tempList.get(0)!=null && !tempList.get(0).toString().equals("")){
						ServiceOrder tempOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(tempList.get(0).toString()));
						networkControlEntry(serviceOrderNew,tempOrder,"BA");
					}
				}
			}
			
			 trackingStatusOLD=trackingStatusManager.save(trackingStatusOLD);
 
			 trackingStatusNew.setNetworkAgentExSO(trackingStatusOLD.getNetworkAgentExSO());
			 trackingStatusNew.setBookingAgentExSO(trackingStatusOLD.getBookingAgentExSO());
			 trackingStatusNew.setOriginAgentExSO(trackingStatusOLD.getOriginAgentExSO());
			 trackingStatusNew.setDestinationAgentExSO(trackingStatusOLD.getDestinationAgentExSO());
			 trackingStatusNew.setOriginSubAgentExSO(trackingStatusOLD.getOriginSubAgentExSO());
			 trackingStatusNew.setDestinationSubAgentExSO(trackingStatusOLD.getDestinationSubAgentExSO()); 
			 if(trackingStatusOLD.getBookingAgentExSO()!=null && (!(trackingStatusOLD.getBookingAgentExSO().equals(""))) && (trackingStatusOLD.getBookingAgentExSO().equals(serviceOrderNew.getShipNumber())) && (billingCMMContractType || billingDMMContractType)&& networkAgent){
                    trackingStatusNew.setContractType(uniqueFieldType);
					trackingStatusNew.setNetworkPartnerCode(trackingStatusOLD.getNetworkPartnerCode());
					trackingStatusNew.setNetworkPartnerName(trackingStatusOLD.getNetworkPartnerName());
					try{
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
					trackingStatusNew.setSoNetworkGroup(true);
					trackingStatusNew.setUtsiNetworkGroup(false);
					if(networkAgent){
						trackingStatusNew.setAccNetworkGroup(true);
						trackingStatusNew.setAgentNetworkGroup(true);
					}else{
					trackingStatusNew.setAccNetworkGroup(false);
					trackingStatusNew.setAgentNetworkGroup(false);
					}
					trackingStatusNew.setBookingAgentExSO(trackingStatusNew.getShipNumber());
					customerFileManager.updateIsNetworkFlag(customerFile.getId(), customerFile.getCorpID(), serviceOrder.getId(),uniqueFieldType,true);
			 }
			 trackingStatusNew=trackingStatusManager.save(trackingStatusNew);
			}catch(Exception ex){
				logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
			}
			
		}
		
		public void  insertIntoTracker(String shipNumber,String action){
			try
			{
			UgwwActionTracker ugwwActionTracker = new UgwwActionTracker();
			
				ugwwActionTracker.setSonumber(shipNumber);
				ugwwActionTracker.setActiontime(new Date());
				ugwwActionTracker.setCorpid(sessionCorpID);
				ugwwActionTracker.setUsername(getRequest().getRemoteUser());
				ugwwActionTracker.setAction(action);
				ugwwActionTracker.setCreatedBy(getRequest().getRemoteUser());
				ugwwActionTracker.setCreatedOn(new Date());
				ugwwActionTracker.setUpdatedOn(new Date());
				ugwwActionTracker.setUpdatedBy(getRequest().getRemoteUser());
				ugwwActionTrackerManager.save(ugwwActionTracker);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
				
			}
		
		@SkipValidation
	    private void createAccountLine(ServiceOrder serviceOrderToRecods, ServiceOrder  serviceOrder,AccountLine accountLine) {
			 
	    		  
	    		try{
	    			decimalFormat= new DecimalFormat("#.####");
	    		trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
	    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
	        	if(trackingStatusToRecod.getSoNetworkGroup()){
	        		trackingStatus=trackingStatusManager.getForOtherCorpid(serviceOrder.getId()); 
	        		String externalCorpID = trackingStatusToRecod.getCorpID();
	        		String externalBaseCurrency=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
	        		UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
	        		UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
	        		List fieldToSync=customerFileManager.findAccFieldToSyncCreate("AccountLine","CMM");
	        		AccountLine accountLineNew= new AccountLine();
	        		try{
	    				Iterator it1=fieldToSync.iterator();
	    				while(it1.hasNext()){
	    				String field=it1.next().toString();
	    				String[] splitField=field.split("~");
	    				String fieldFrom=splitField[0];
	    				String fieldTo=splitField[1];
	    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
						try{
							if(fieldFrom!=null && fieldTo!=null && fieldFrom.trim().equalsIgnoreCase("recRateExchange") && fieldTo.trim().equalsIgnoreCase("exchangeRate"))	{
								BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();
								if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
								payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
								accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
								} 
							}else{
						       beanUtilsBean.setProperty(accountLineNew,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
							}
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
	    				
	    				}
	        		}catch(Exception e){
	        			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
	        		}
	        		/*maxLineNumber = serviceOrderManager.findExternalMaximumLineNumber(serviceOrderToRecods.getShipNumber(),externalCorpID); 
		            if ( maxLineNumber.get(0) == null ) {          
		             	accountLineNumber = "001";
		             }else {
		             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
		                 if((autoLineNumber.toString()).length() == 2) {
		                 	accountLineNumber = "0"+(autoLineNumber.toString());
		                 }
		                 else if((autoLineNumber.toString()).length() == 1) {
		                 	accountLineNumber = "00"+(autoLineNumber.toString());
		                 } 
		                 else {
		                 	accountLineNumber=autoLineNumber.toString();
		                 }
		             }*/
		            accountLineNew.setNetworkSynchedId(accountLine.getId());
		            accountLineNew.setAccountLineNumber(accountLine.getAccountLineNumber()); 
		            accountLineNew.setCompanyDivision(serviceOrderToRecods.getCompanyDivision());
		            accountLineNew.setVendorCode(serviceOrderToRecods.getBookingAgentCode());
		            accountLineNew.setEstimateVendorName(serviceOrderToRecods.getBookingAgentName());
		             String actCode="";
		             String  companyDivisionAcctgCodeUnique1="";
		    		 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(externalCorpID);
		    		 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
		    			 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
		    		 }
		            if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
		    			actCode=partnerManager.getAccountCrossReference(serviceOrderToRecods.getBookingAgentCode(),serviceOrderToRecods.getCompanyDivision(),externalCorpID);
		    		}else{
		    			actCode=partnerManager.getAccountCrossReferenceUnique(serviceOrderToRecods.getBookingAgentCode(),externalCorpID);
		    		}
		            accountLineNew.setActgCode(actCode);
		            if(accountLineNew.getEstimateRevenueAmount() !=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0){
		            accountLineNew.setEstimatePassPercentage(100);
		            }
		            if(accountLineNew.getRevisionRevenueAmount() !=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0){
		            accountLineNew.setRevisionPassPercentage(100);
		            }
		            Boolean extCostElement =  accountLineManager.getExternalCostElement(externalCorpID);
		            /*if(!extCostElement){
		            List  agentGLList=accountLineManager.getGLList(accountLineNew.getChargeCode(),billingRecods.getContract(),externalCorpID);
					if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
						try{
						String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
						String recGl=GLarrayData[0];
						String payGl=GLarrayData[1];
						if(!(recGl.equalsIgnoreCase("NO"))){
							accountLineNew.setRecGl(recGl);
						}
						if(!(payGl.equalsIgnoreCase("NO"))){
							accountLineNew.setPayGl(payGl);
						}
						String VATExclude=GLarrayData[2];
						if(VATExclude.equalsIgnoreCase("Y")){
							accountLineNew.setVATExclude(true);
						}else{
							accountLineNew.setVATExclude(false);
						}
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
					}
	        	}else{*/
	        		String chargeStr="";
					List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),accountLineNew.getChargeCode(),externalCorpID,serviceOrderToRecods.getJob(),serviceOrderToRecods.getRouting(),serviceOrderToRecods.getCompanyDivision());
					if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
						  chargeStr= glList.get(0).toString();
					  }
					   if(!chargeStr.equalsIgnoreCase("")){
						  String [] chrageDetailArr = chargeStr.split("#");
						  accountLineNew.setRecGl(chrageDetailArr[1]);
						  accountLineNew.setPayGl(chrageDetailArr[2]);
						}else{
							accountLineNew.setRecGl("");
							accountLineNew.setPayGl("");
					  }
		            accountLineNew.setId(null);
		            accountLineNew.setCorpID(externalCorpID);
		            accountLineNew.setCreatedBy("Networking");
		            accountLineNew.setUpdatedBy("Networking");
		            accountLineNew.setCreatedOn(new Date());
		            accountLineNew.setUpdatedOn(new Date()); 
		            accountLineNew.setRacValueDate(new Date());
					accountLineNew.setContractValueDate(new Date());
				    accountLineNew.setValueDate(new Date());
				   	BigDecimal payExchangeRateBig=accountLine.getRecRateExchange();
					if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
						payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
					accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
					}
					try{
					}catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		    		}
					if(externalBaseCurrency !=null && baseCurrency!=null && (!(baseCurrency.trim().equalsIgnoreCase(externalBaseCurrency.trim())))){
						DecimalFormat decimalFormatExchangeRate = new DecimalFormat("#.####");
						String estExchangeRate="1"; 
			            BigDecimal estExchangeRateBig=new BigDecimal("1.0000");
			            List estRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstSellCurrency());
						 if((estRate!=null)&&(!estRate.isEmpty())&& estRate.get(0)!=null && (!(estRate.get(0).toString().equals(""))))
						 {
							 estExchangeRate=estRate.get(0).toString();
						 
						 } 
						estExchangeRateBig=new BigDecimal(estExchangeRate);	
						accountLineNew.setEstSellExchangeRate(estExchangeRateBig); 
						accountLineNew.setEstExchangeRate(estExchangeRateBig);
						if(accountLine.getEstSellLocalRate()!=null && accountLine.getEstSellLocalRate().doubleValue()!=0){
						accountLineNew.setEstimateSellRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
						accountLineNew.setEstimateRate(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalRate().doubleValue())/(estExchangeRateBig.doubleValue()))));
						}
						if(accountLine.getEstSellLocalAmount()!=null && accountLine.getEstSellLocalAmount().doubleValue()!=0){
						accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
						accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue())/(estExchangeRateBig.doubleValue()))));
						} 
						BigDecimal estcontractExchangeRateBig=new BigDecimal("1");
						try{ 	
							estcontractExchangeRateBig=((new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getEstimateContractRate().doubleValue())/(accountLineNew.getEstimateSellRate().doubleValue()))))); 
						}catch(Exception es){
							
						} 
						accountLineNew.setEstimateContractExchangeRate(estcontractExchangeRateBig);
						accountLineNew.setEstimatePayableContractExchangeRate(estcontractExchangeRateBig);
						
						
						String revisionExchangeRate="1"; 
			            BigDecimal revisionExchangeRateBig=new BigDecimal("1.0000");
			            List revisionRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionSellCurrency());
						 if((revisionRate!=null)&&(!revisionRate.isEmpty())&& revisionRate.get(0)!=null && (!(revisionRate.get(0).toString().equals(""))))
						 {
							 revisionExchangeRate=revisionRate.get(0).toString();
						 
						 } 
						 revisionExchangeRateBig=new BigDecimal(revisionExchangeRate);	
						accountLineNew.setRevisionSellExchangeRate(revisionExchangeRateBig);
						accountLineNew.setRevisionExchangeRate(revisionExchangeRateBig);
						if(accountLine.getRevisionSellLocalRate()!=null && accountLine.getRevisionSellLocalRate().doubleValue()!=0){
						accountLineNew.setRevisionSellRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						accountLineNew.setRevisionRate(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalRate().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						}
						if(accountLine.getRevisionSellLocalAmount()!=null && accountLine.getRevisionSellLocalAmount().doubleValue()!=0){
						accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue())/(revisionExchangeRateBig.doubleValue()))));
						}
						
						
						
						BigDecimal revisioncontractExchangeRateBig=new BigDecimal("1");	
						try{ 
							revisioncontractExchangeRateBig=((new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getRevisionContractRate().doubleValue())/(accountLineNew.getRevisionSellRate().doubleValue())))));
							
						}catch(Exception es){
							
						}
						accountLineNew.setRevisionContractExchangeRate(revisioncontractExchangeRateBig);
						accountLineNew.setRevisionPayableContractExchangeRate(revisioncontractExchangeRateBig);
						 
						
						
						
						String recExchangeRate="1"; 
			            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
			            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
						 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
						 {
							 recExchangeRate=recRate.get(0).toString();
						 
						 } 
						recExchangeRateBig=new BigDecimal(recExchangeRate);	
						accountLineNew.setRecRateExchange(recExchangeRateBig);
						payExchangeRateBig=accountLineNew.getRecRateExchange();
						if(payExchangeRateBig!=null && (!(payExchangeRateBig.toString().trim().equals("")))){
							payExchangeRateBig=new BigDecimal(decimalFormat.format(payExchangeRateBig));
							accountLineNew.setExchangeRate(Double.parseDouble(payExchangeRateBig.toString()));
						}
						if(accountLine.getRecCurrencyRate()!=null && accountLine.getRecCurrencyRate().doubleValue()!=0){
						accountLineNew.setRecRate((new BigDecimal(decimalFormatExchangeRate.format((accountLine.getRecCurrencyRate().doubleValue())/(recExchangeRateBig.doubleValue())))));
						}
						if(accountLine.getActualRevenueForeign()!=null && accountLine.getActualRevenueForeign().doubleValue()!=0){
						accountLineNew.setActualRevenue((new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue())))));
						try{
							accountLineNew.setActualExpense((new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue())/(recExchangeRateBig.doubleValue())))));
							}catch(Exception e){
				    			
						}
						
						
			    		}
						BigDecimal contractExchangeRateBig=new BigDecimal("1");	
						try{
							
							contractExchangeRateBig=(new BigDecimal(decimalFormatExchangeRate.format((accountLineNew.getContractRate().doubleValue())/(accountLineNew.getRecRate().doubleValue()))));
							
						}catch(Exception es){
							
						}
						accountLineNew.setContractExchangeRate(contractExchangeRateBig);
						accountLineNew.setPayableContractExchangeRate(contractExchangeRateBig); 
					}
					if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){ 
					if(UTSIeuVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){	
					accountLineNew.setRecVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
					}else{
						accountLineNew.setRecVatDescr("");
						accountLineNew.setRecVatPercent("0");
						accountLineNew.setRecVatAmt(new BigDecimal(0));
						accountLineNew.setEstVatAmt(new BigDecimal(0));
						accountLineNew.setRevisionVatAmt(new BigDecimal(0));
						}
					if(UTSIpayVatPercentList.containsKey(sessionCorpID+"_"+accountLine.getRecVatDescr())){ 
					accountLineNew.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
					}else{
						accountLineNew.setPayVatDescr("");
						accountLineNew.setPayVatPercent("0");
						accountLineNew.setPayVatAmt(new BigDecimal(0));
						accountLineNew.setEstExpVatAmt(new BigDecimal(0)); 
						accountLineNew.setRevisionExpVatAmt(new BigDecimal(0));
						}
					}
		            accountLineNew.setServiceOrderId(serviceOrderToRecods.getId());
		            accountLineNew.setSequenceNumber(serviceOrderToRecods.getSequenceNumber());
		            accountLineNew.setShipNumber(serviceOrderToRecods.getShipNumber());
		            accountLineNew.setServiceOrder(serviceOrderToRecods);
		            accountLineNew=accountLineManager.save(accountLineNew);
		             
	        	}
	    		}catch(Exception e){
	    			 e.printStackTrace();
	    			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
	    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    		}
	    	}
		
		
		@SkipValidation
	    private void createDMMAccountLine(ServiceOrder serviceOrderToRecods, ServiceOrder  serviceOrder,AccountLine accountLine,Billing billing){


			 
    		try{
    			decimalFormat= new DecimalFormat("#.####");
    		trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
    		 billingRecods=billingManager.getForOtherCorpid(serviceOrderToRecods.getId()); 
        	if(trackingStatusToRecod.getSoNetworkGroup()){
        		trackingStatus=trackingStatusManager.getForOtherCorpid(serviceOrder.getId()); 
        		String externalCorpID=trackingStatusManager.getExternalCorpID(sessionCorpID, trackingStatus.getNetworkPartnerCode()); 
        		UTSIpayVatList=	refMasterManager.findByParameterUTSIPayVat (sessionCorpID, "PAYVATDESC",externalCorpID);
        		UTSIpayVatPercentList = refMasterManager.findVatPercentList(externalCorpID, "PAYVATDESC");
                UTSIeuVatPercentList=refMasterManager.findVatPercentList(externalCorpID, "EUVAT");
        		List fieldToSync=customerFileManager.findAccFieldToSyncCreate("AccountLine","DMM");
        		AccountLine accountLineNew= new AccountLine();
        		try{
    				Iterator it1=fieldToSync.iterator();
    				while(it1.hasNext()){
    				String field=it1.next().toString();
    				String[] splitField=field.split("~");
    				String fieldFrom=splitField[0];
    				String fieldTo=splitField[1];
    				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					beanUtilsBean.setProperty(accountLineNew,fieldTo.trim(),beanUtilsBean.getProperty(accountLine, fieldFrom.trim()));
					}catch(Exception ex){
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
    				
    				}
        		}catch(Exception e){
        			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
        		}
        		try{
        			if(accountLine.getRecVatDescr()!=null && (!(accountLine.getRecVatDescr().toString().trim().equals("")))){ 
    			    	
    			    	String companyDivisionUTSI=accountLineManager.getUTSICompanyDivision(externalCorpID,accountLine.getBillToCode()) ;
    			    	accountLineNew.setPayVatDescr(sessionCorpID+"_"+accountLine.getRecVatDescr());
    			    	String payVatDescr="";
    			    	if(UTSIpayVatList.containsKey(accountLineNew.getPayVatDescr()+"~"+companyDivisionUTSI)){
    			    		payVatDescr=UTSIpayVatList.get(accountLineNew.getPayVatDescr()+"~"+companyDivisionUTSI);
    			    		accountLineNew.setPayVatDescr(payVatDescr);
    			    		String payVatPercent="0";
    			    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
    	    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
    			    		}
    	    				accountLineNew.setPayVatPercent(payVatPercent);
    			    	} else{
    			    		if(UTSIpayVatList.containsKey(accountLineNew.getPayVatDescr()+"~NO")){
    				    		payVatDescr=UTSIpayVatList.get(accountLineNew.getPayVatDescr()+"~NO");
    				    		accountLineNew.setPayVatDescr(payVatDescr);
    				    		String payVatPercent="0";
    				    		if(UTSIpayVatPercentList.containsKey(payVatDescr)){ 
    		    				payVatPercent=UTSIpayVatPercentList.get(payVatDescr);
    				    		}
    		    				accountLineNew.setPayVatPercent(payVatPercent);
    				    	}else{
    				    		accountLineNew.setPayVatDescr("");
    				    		accountLineNew.setPayVatPercent("0");
    				    	}
    			    	}
    			    	}
        		}catch(Exception e){
        			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
        		}
        		 accountLineNew.setPayableContractCurrency(accountLine.getRecRateCurrency());
			    accountLineNew.setCountry(accountLine.getRecRateCurrency());
			    accountLineNew.setEstimatePayableContractCurrency(accountLine.getEstSellCurrency());
			    accountLineNew.setEstCurrency(accountLine.getEstSellCurrency());
			    accountLineNew.setRevisionPayableContractCurrency(accountLine.getRevisionSellCurrency());
			    accountLineNew.setRevisionCurrency(accountLine.getRevisionSellCurrency());
			    String billingCurrency=serviceOrderManager.getBillingCurrencyValue(accountLineNew.getBillToCode(),externalCorpID);
	    		if(!billingCurrency.equalsIgnoreCase(""))
	    		{
	    			accountLineNew.setRecRateCurrency(billingCurrency);
	    			accountLineNew.setEstSellCurrency(billingCurrency);
	    			accountLineNew.setRevisionSellCurrency(billingCurrency);
	    		}else{ 
	    			String baseCurrencyExternalCorpID="";
	    			String baseCurrencyCompanyDivisionExternalCorpID=""; 
	    			baseCurrencyExternalCorpID=accountLineManager.searchBaseCurrency(externalCorpID).get(0).toString();
	    			baseCurrencyCompanyDivisionExternalCorpID=	 companyDivisionManager.searchBaseCurrencyCompanyDivision(serviceOrderToRecods.getCompanyDivision(),externalCorpID);
	    			if(baseCurrencyCompanyDivisionExternalCorpID==null ||baseCurrencyCompanyDivisionExternalCorpID.equals(""))
	    			{
	    				accountLineNew.setRecRateCurrency(baseCurrencyExternalCorpID);
	    				accountLineNew.setEstSellCurrency(baseCurrencyExternalCorpID);
	    				accountLineNew.setRevisionSellCurrency(baseCurrencyExternalCorpID);
	    			}else{
	    				accountLineNew.setRecRateCurrency(baseCurrencyCompanyDivisionExternalCorpID);
	    				accountLineNew.setEstSellCurrency(baseCurrencyCompanyDivisionExternalCorpID);
	    				accountLineNew.setRevisionSellCurrency(baseCurrencyCompanyDivisionExternalCorpID);
	    			}
	    		}
			    accountLineNew.setNetworkSynchedId(accountLine.getId());
	          try{
		            if(accountLineNew.getBillToCode()!=null && (!(accountLineNew.getBillToCode().toString().equals(""))) && billingRecods.getBillToCode()!=null && (!(billingRecods.getBillToCode().toString().equals(""))) && accountLineNew.getBillToCode().toString().equals(billingRecods.getBillToCode().toString()) )	{
		            	if(!accountLineNew.getVATExclude()){
		            	accountLineNew.setRecVatDescr(billingRecods.getPrimaryVatCode());
			    		if(billingRecods.getPrimaryVatCode()!=null && (!(billingRecods.getPrimaryVatCode().toString().equals("")))){
				    		String recVatPercent="0";
				    		if(UTSIeuVatPercentList.containsKey(billingRecods.getPrimaryVatCode())){
					    		recVatPercent=UTSIeuVatPercentList.get(billingRecods.getPrimaryVatCode());
					    		}
				    		accountLineNew.setRecVatPercent(recVatPercent);
				    		}
		            	}	
		            }
		            }catch(Exception e){
		            	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		            } 
	            accountLineNew.setAccountLineNumber(accountLine.getAccountLineNumber()); 
	            accountLineNew.setCompanyDivision(serviceOrderToRecods.getCompanyDivision());
	            List bookingAgentCodeList=companyDivisionManager.getBookingAgentCode(accountLine.getCompanyDivision(),sessionCorpID);
	            if(bookingAgentCodeList!=null && (!(bookingAgentCodeList.isEmpty())) && bookingAgentCodeList.get(0)!=null && (!(bookingAgentCodeList.get(0).toString().trim().equals(""))))
	            {
	            	accountLineNew.setVendorCode(bookingAgentCodeList.get(0).toString());
	            	List bookingAgentNamelist=	serviceOrderManager.findByBooking(bookingAgentCodeList.get(0).toString(), sessionCorpID);
	                
	            	if(bookingAgentNamelist!=null && (!(bookingAgentNamelist.isEmpty())) && bookingAgentNamelist.get(0)!=null && (!(bookingAgentNamelist.get(0).toString().trim().equals("")))){
		            accountLineNew.setEstimateVendorName(bookingAgentNamelist.get(0).toString());
	                }
	            	
	            }
	             String actCode="";
	             String  companyDivisionAcctgCodeUnique1="";
	    		 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(externalCorpID);
	    		 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
	    			 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
	    		 }
	            if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
	    			actCode=partnerManager.getAccountCrossReference(accountLineNew.getVendorCode(),serviceOrderToRecods.getCompanyDivision(),externalCorpID);
	    		}else{
	    			actCode=partnerManager.getAccountCrossReferenceUnique(accountLineNew.getVendorCode(),externalCorpID);
	    		}
	            accountLineNew.setActgCode(actCode);
	            Boolean extCostElement =  accountLineManager.getExternalCostElement(externalCorpID);
	            if(!extCostElement){
	            List  agentGLList=accountLineManager.getGLList(accountLineNew.getChargeCode(),billingRecods.getContract(),externalCorpID);
				if(agentGLList!=null && (!(agentGLList.isEmpty())) && agentGLList.get(0)!=null){
					try{
					String[] GLarrayData=agentGLList.get(0).toString().split("#"); 
					String recGl=GLarrayData[0];
					String payGl=GLarrayData[1];
					if(!(recGl.equalsIgnoreCase("NO"))){
						accountLineNew.setRecGl(recGl);
					}
					if(!(payGl.equalsIgnoreCase("NO"))){
						accountLineNew.setPayGl(payGl);
					}
					String VATExclude=GLarrayData[2];
					if(VATExclude.equalsIgnoreCase("Y")){
						accountLineNew.setVATExclude(true);
					}else{
						accountLineNew.setVATExclude(false);
					}
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				}
				}
        	}else{
        		String chargeStr="";
				List glList = accountLineManager.findChargeDetailFromSO(billingRecods.getContract(),accountLineNew.getChargeCode(),externalCorpID,serviceOrderToRecods.getJob(),serviceOrderToRecods.getRouting(),serviceOrderToRecods.getCompanyDivision());
				if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
					  chargeStr= glList.get(0).toString();
				  }
				   if(!chargeStr.equalsIgnoreCase("")){
					  String [] chrageDetailArr = chargeStr.split("#");
					  accountLineNew.setRecGl(chrageDetailArr[1]);
					  accountLineNew.setPayGl(chrageDetailArr[2]);
					}else{
						accountLineNew.setRecGl("");
						accountLineNew.setPayGl("");
				  }
        	}
	            accountLineNew.setId(null);
	            accountLineNew.setCorpID(externalCorpID);
	            accountLineNew.setCreatedBy("Networking");
	            accountLineNew.setUpdatedBy("Networking");
	            accountLineNew.setCreatedOn(new Date());
	            accountLineNew.setUpdatedOn(new Date()); 
	           
	            
	            String estimatePayableContractExchangeRate="1"; 
	            BigDecimal estimatePayableContractExchangeRateBig=new BigDecimal("1.0000");
	            List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstimatePayableContractCurrency ());
				 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals(""))))
				 {
					 estimatePayableContractExchangeRate=estimatePayableContractRate.get(0).toString();
				 
				 }
				 estimatePayableContractExchangeRateBig=new BigDecimal(estimatePayableContractExchangeRate);	
				 accountLineNew.setEstimatePayableContractExchangeRate(estimatePayableContractExchangeRateBig);
				 accountLineNew.setEstimatePayableContractValueDate(new Date());
				 accountLineNew.setEstExchangeRate(estimatePayableContractExchangeRateBig);
				 accountLineNew.setEstValueDate(new Date());
				 if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
					  accountLineNew.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
				 }else{
				 accountLineNew.setEstimatePayableContractRateAmmount(accountLine.getEstSellLocalAmount());
				 }
				 accountLineNew.setEstimatePayableContractRate(accountLine.getEstSellLocalRate());
				 if(accountLine.getEstimateSellDeviation()!=null && accountLine.getEstSellLocalAmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLine.getEstSellLocalAmount().doubleValue()>0){
					 accountLineNew.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getEstSellLocalAmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
				 }else{
				 accountLineNew.setEstLocalAmount(accountLine.getEstSellLocalAmount());
				 }
				 accountLineNew.setEstLocalRate(accountLine.getEstSellLocalRate());
				 accountLineNew.setEstimateRate(new BigDecimal(decimalFormat.format(accountLine.getEstSellLocalRate().doubleValue()/accountLineNew.getEstimatePayableContractExchangeRate().doubleValue())));
				 accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format(accountLineNew.getEstimatePayableContractRateAmmount().doubleValue()/accountLineNew.getEstimatePayableContractExchangeRate().doubleValue())));
				
				 
				 
				 String estimateContractExchangeRate="1"; 
	            BigDecimal estimateContractExchangeRateBig=new BigDecimal("1.0000");
	            List estimateContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstimateContractCurrency ());
				 if((estimateContractRate!=null)&&(!estimateContractRate.isEmpty())&& estimateContractRate.get(0)!=null && (!(estimateContractRate.get(0).toString().equals(""))))
				 {
					 estimateContractExchangeRate=estimateContractRate.get(0).toString();
				 
				 } 
				 estimateContractExchangeRateBig=new BigDecimal(estimateContractExchangeRate);	
				 accountLineNew.setEstimateContractExchangeRate(estimateContractExchangeRateBig);
				 accountLineNew.setEstimateContractValueDate(new Date()); 
				 accountLineNew.setEstimateSellRate(new BigDecimal(decimalFormat.format(accountLineNew.getEstimateContractRate().doubleValue()/accountLineNew.getEstimateContractExchangeRate().doubleValue())));
				 if(accountLine.getEstimateSellDeviation()!=null && accountLineNew.getEstimateContractRateAmmount()!=null &&  accountLine.getEstimateSellDeviation().doubleValue()>0 && accountLineNew.getEstimateContractRateAmmount().doubleValue()>0){
					  accountLineNew.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateContractRateAmmount().doubleValue()*100)/accountLine.getEstimateSellDeviation().doubleValue())));	 
					 }else{
						 
					 }
				 accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format(accountLineNew.getEstimateContractRateAmmount().doubleValue()/ accountLineNew.getEstimateContractExchangeRate().doubleValue())));
				 
				 
				String estSellExchangeRate="1"; 
		        BigDecimal estSellExchangeRateBig=new BigDecimal("1.0000");
	            List estSellRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getEstSellCurrency());
				 if((estSellRate!=null)&&(!estSellRate.isEmpty())&& estSellRate.get(0)!=null && (!(estSellRate.get(0).toString().equals(""))))
				 {
					 estSellExchangeRate=estSellRate.get(0).toString();
				 }
				 estSellExchangeRateBig=new BigDecimal(estSellExchangeRate);
				accountLineNew.setEstSellExchangeRate(estSellExchangeRateBig);
				accountLineNew.setEstSellValueDate(new Date());
				
				accountLineNew.setEstSellLocalRate(accountLineNew.getEstimateSellRate().multiply(estSellExchangeRateBig));
				accountLineNew.setEstSellLocalAmount(accountLineNew.getEstimateRevenueAmount().multiply(estSellExchangeRateBig));
				 
				 
				
				
				String revisionPayableContractExchangeRate="1"; 
	            BigDecimal revisionPayableContractExchangeRateBig=new BigDecimal("1.0000");
	            List revisionPayableContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionPayableContractCurrency ());
				 if((revisionPayableContractRate!=null)&&(!revisionPayableContractRate.isEmpty())&& revisionPayableContractRate.get(0)!=null && (!(revisionPayableContractRate.get(0).toString().equals(""))))
				 {
					 revisionPayableContractExchangeRate=revisionPayableContractRate.get(0).toString();
				 
				 }
				 revisionPayableContractExchangeRateBig=new BigDecimal(revisionPayableContractExchangeRate);	
				 accountLineNew.setRevisionPayableContractExchangeRate(revisionPayableContractExchangeRateBig);
				 accountLineNew.setRevisionPayableContractValueDate(new Date());
				 accountLineNew.setRevisionExchangeRate(revisionPayableContractExchangeRateBig);
				 accountLineNew.setRevisionValueDate(new Date());
				 if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
					  accountLineNew.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
				 }else{
				 accountLineNew.setRevisionPayableContractRateAmmount(accountLine.getRevisionSellLocalAmount());
				 }
				 accountLineNew.setRevisionPayableContractRate(accountLine.getRevisionSellLocalRate());
				 if(accountLine.getRevisionSellDeviation()!=null && accountLine.getRevisionSellLocalAmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLine.getRevisionSellLocalAmount().doubleValue()>0){
					 accountLineNew.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getRevisionSellLocalAmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue()))	 );	 
				 }else{
				 accountLineNew.setRevisionLocalAmount(accountLine.getRevisionSellLocalAmount());
				 }
				 accountLineNew.setRevisionLocalRate(accountLine.getRevisionSellLocalRate());
				 accountLineNew.setRevisionRate(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionPayableContractRate().doubleValue()/ accountLineNew.getRevisionPayableContractExchangeRate().doubleValue())));
				 accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionPayableContractRateAmmount().doubleValue()/ accountLineNew.getRevisionPayableContractExchangeRate().doubleValue())));
				
				String revisionContractExchangeRate="1"; 
	            BigDecimal revisionContractExchangeRateBig=new BigDecimal("1.0000");
	            List revisionContractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionContractCurrency ());
				 if((revisionContractRate!=null)&&(!revisionContractRate.isEmpty())&& revisionContractRate.get(0)!=null && (!(revisionContractRate.get(0).toString().equals(""))))
				 {
					 revisionContractExchangeRate=revisionContractRate.get(0).toString();
				 
				 } 
				 revisionContractExchangeRateBig=new BigDecimal(revisionContractExchangeRate);	
				 accountLineNew.setRevisionContractExchangeRate(revisionContractExchangeRateBig);
				 accountLineNew.setRevisionContractValueDate(new Date());
				 accountLineNew.setRevisionSellRate(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionContractRate().doubleValue()/  accountLineNew.getRevisionContractExchangeRate().doubleValue())));
				 if(accountLine.getRevisionSellDeviation()!=null && accountLineNew.getRevisionContractRateAmmount()!=null &&  accountLine.getRevisionSellDeviation().doubleValue()>0 && accountLineNew.getRevisionContractRateAmmount().doubleValue()>0){
					  accountLineNew.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionContractRateAmmount().doubleValue()*100)/accountLine.getRevisionSellDeviation().doubleValue())));	 
					 }else{
						 
					 }
				 accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format(accountLineNew.getRevisionContractRateAmmount().doubleValue()/  accountLineNew.getRevisionContractExchangeRate().doubleValue())));
				 
				 
				String revisionSellExchangeRate="1"; 
		        BigDecimal revisionSellExchangeRateBig=new BigDecimal("1.0000");
	            List revisionSellRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRevisionSellCurrency());
				 if((revisionSellRate!=null)&&(!revisionSellRate.isEmpty())&& revisionSellRate.get(0)!=null && (!(revisionSellRate.get(0).toString().equals(""))))
				 {
					 revisionSellExchangeRate=estSellRate.get(0).toString();
				 }
				 revisionSellExchangeRateBig=new BigDecimal(revisionSellExchangeRate);
				 accountLineNew.setRevisionSellExchangeRate(revisionSellExchangeRateBig);
				 accountLineNew.setRevisionSellValueDate(new Date());
				 
				 accountLineNew.setRevisionSellLocalRate(accountLineNew.getRevisionSellRate().multiply(revisionSellExchangeRateBig));
				 accountLineNew.setRevisionSellLocalAmount(accountLineNew.getRevisionRevenueAmount().multiply(revisionSellExchangeRateBig));
				
				 
				  
				String contractPayableExchangeRate="1"; 
		        BigDecimal contractPayableExchangeRateBig=new BigDecimal("1.0000");
		        List contractPayableRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getPayableContractCurrency ());
				if((contractPayableRate!=null)&&(!contractPayableRate.isEmpty())&& contractPayableRate.get(0)!=null && (!(contractPayableRate.get(0).toString().equals(""))))
					 {
					contractPayableExchangeRate=contractPayableRate.get(0).toString();
					 
					 } 
				contractPayableExchangeRateBig=new BigDecimal(contractPayableExchangeRate);
				accountLineNew.setPayableContractExchangeRate(contractPayableExchangeRateBig);
				accountLineNew.setPayableContractValueDate(new Date()); 
				accountLineNew.setValueDate(new Date());
			    double payExchangeRate=0; 
				payExchangeRate=Double.parseDouble(contractPayableExchangeRate);
				accountLineNew.setExchangeRate(payExchangeRate);
				if(accountLine.getReceivableSellDeviation()!=null && accountLine.getActualRevenueForeign()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLine.getActualRevenueForeign().doubleValue()>0){
					accountLineNew.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));
					accountLineNew.setLocalAmount(new BigDecimal(decimalFormat.format((accountLine.getActualRevenueForeign().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	
				}else{
				accountLineNew.setPayableContractRateAmmount(accountLine.getActualRevenueForeign());
				accountLineNew.setLocalAmount(accountLine.getActualRevenueForeign());
				}
				try{
					accountLineNew.setActualExpense(new BigDecimal(decimalFormat.format(accountLineNew.getPayableContractRateAmmount().doubleValue()/accountLineNew.getPayableContractExchangeRate().doubleValue())));
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		    	}
				 
				 
			    String contractExchangeRate="1"; 
	            BigDecimal contractExchangeRateBig=new BigDecimal("1.0000");
	            List contractRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getContractCurrency ());
				 if((contractRate!=null)&&(!contractRate.isEmpty())&& contractRate.get(0)!=null && (!(contractRate.get(0).toString().equals(""))))
				 {
					 contractExchangeRate=contractRate.get(0).toString();
				 
				 } 
				contractExchangeRateBig=new BigDecimal(contractExchangeRate);	
				accountLineNew.setContractExchangeRate(contractExchangeRateBig);
				accountLineNew.setContractValueDate(new Date()); 
				accountLineNew.setRecRate(new BigDecimal(decimalFormat.format(accountLineNew.getContractRate().doubleValue()/accountLineNew.getContractExchangeRate().doubleValue())));
				 if(accountLine.getReceivableSellDeviation()!=null && accountLineNew.getContractRateAmmount()!=null &&  accountLine.getReceivableSellDeviation().doubleValue()>0 && accountLineNew.getContractRateAmmount().doubleValue()>0){
					  accountLineNew.setContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getContractRateAmmount().doubleValue()*100)/accountLine.getReceivableSellDeviation().doubleValue())));	 
					 }else{
						 
					 }
				accountLineNew.setActualRevenue(new BigDecimal(decimalFormat.format(accountLineNew.getContractRateAmmount().doubleValue()/accountLineNew.getContractExchangeRate().doubleValue())));
				accountLineNew.setRacValueDate(new Date());
				String recExchangeRate="1"; 
	            BigDecimal recExchangeRateBig=new BigDecimal("1.0000");
	            List recRate=exchangeRateManager.findAccExchangeRate(externalCorpID,accountLineNew.getRecRateCurrency());
				 if((recRate!=null)&&(!recRate.isEmpty())&& recRate.get(0)!=null && (!(recRate.get(0).toString().equals(""))))
				 {
					 recExchangeRate=recRate.get(0).toString();
				 }
				recExchangeRateBig=new BigDecimal(recExchangeRate);
				accountLineNew.setRecRateExchange(recExchangeRateBig);
				accountLineNew.setRacValueDate(new Date());
				accountLineNew.setRecCurrencyRate(accountLineNew.getRecRate().multiply(recExchangeRateBig));
				accountLineNew.setActualRevenueForeign(accountLineNew.getActualRevenue().multiply(recExchangeRateBig));
				try{
					if(accountLineNew.getEstimateSellDeviation()!=null && accountLineNew.getEstimateSellDeviation().doubleValue()>0 ){
						if(accountLineNew.getEstimateRevenueAmount()!=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0)
							accountLineNew.setEstimateRevenueAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateRevenueAmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));
						if(accountLineNew.getEstSellLocalAmount()!=null && accountLineNew.getEstSellLocalAmount().doubleValue()>0)
							accountLineNew.setEstSellLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstSellLocalAmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));
						if(accountLineNew.getEstimateContractRateAmmount()!=null && accountLineNew.getEstimateContractRateAmmount().doubleValue()>0)
							accountLineNew.setEstimateContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateContractRateAmmount().doubleValue())*((accountLineNew.getEstimateSellDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getEstimateDeviation()!=null && accountLineNew.getEstimateDeviation().doubleValue()>0 ){
						if(accountLineNew.getEstimateExpense()!=null && accountLineNew.getEstimateExpense().doubleValue()>0)
							accountLineNew.setEstimateExpense(new BigDecimal(decimalFormat.format((accountLineNew.getEstimateExpense().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));
						if(accountLineNew.getEstLocalAmount()!=null && accountLineNew.getEstLocalAmount().doubleValue()>0)
							accountLineNew.setEstLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstLocalAmount().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));
						if(accountLineNew.getEstimatePayableContractRateAmmount()!=null && accountLineNew.getEstimatePayableContractRateAmmount().doubleValue()>0)
							accountLineNew.setEstimatePayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getEstimatePayableContractRateAmmount().doubleValue())*((accountLineNew.getEstimateDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getRevisionSellDeviation()!=null && accountLineNew.getRevisionSellDeviation().doubleValue()>0 ){
						if(accountLineNew.getRevisionRevenueAmount()!=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0)
							accountLineNew.setRevisionRevenueAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionRevenueAmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionSellLocalAmount()!=null && accountLineNew.getRevisionSellLocalAmount().doubleValue()>0)
							accountLineNew.setRevisionSellLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionSellLocalAmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionContractRateAmmount()!=null && accountLineNew.getRevisionContractRateAmmount().doubleValue()>0)
							accountLineNew.setRevisionContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionContractRateAmmount().doubleValue())*((accountLineNew.getRevisionSellDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getRevisionDeviation()!=null && accountLineNew.getRevisionDeviation().doubleValue()>0 ){
						if(accountLineNew.getRevisionExpense()!=null && accountLineNew.getRevisionExpense().doubleValue()>0)
							accountLineNew.setRevisionExpense(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionExpense().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionLocalAmount()!=null && accountLineNew.getRevisionLocalAmount().doubleValue()>0)
							accountLineNew.setRevisionLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionLocalAmount().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));
						if(accountLineNew.getRevisionPayableContractRateAmmount()!=null && accountLineNew.getRevisionPayableContractRateAmmount().doubleValue()>0)
							accountLineNew.setRevisionPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getRevisionPayableContractRateAmmount().doubleValue())*((accountLineNew.getRevisionDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getReceivableSellDeviation()!=null && accountLineNew.getReceivableSellDeviation().doubleValue()>0 ){
						if(accountLineNew.getActualRevenue()!=null && accountLineNew.getActualRevenue().doubleValue()>0)
							accountLineNew.setActualRevenue(new BigDecimal(decimalFormat.format((accountLineNew.getActualRevenue().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));
						if(accountLineNew.getActualRevenueForeign()!=null && accountLineNew.getActualRevenueForeign().doubleValue()>0)
							accountLineNew.setActualRevenueForeign(new BigDecimal(decimalFormat.format((accountLineNew.getActualRevenueForeign().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));
						if(accountLineNew.getContractRateAmmount()!=null && accountLineNew.getContractRateAmmount().doubleValue()>0)
							accountLineNew.setContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getContractRateAmmount().doubleValue())*((accountLineNew.getReceivableSellDeviation().doubleValue())/100))));	
					}
					if(accountLineNew.getPayDeviation()!=null && accountLineNew.getPayDeviation().doubleValue()>0 ){
						if(accountLineNew.getActualExpense()!=null && accountLineNew.getActualExpense().doubleValue()>0)
							accountLineNew.setActualExpense(new BigDecimal(decimalFormat.format((accountLineNew.getActualExpense().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));
						if(accountLineNew.getLocalAmount()!=null && accountLineNew.getLocalAmount().doubleValue()>0)
							accountLineNew.setLocalAmount(new BigDecimal(decimalFormat.format((accountLineNew.getLocalAmount().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));
						if(accountLineNew.getPayableContractRateAmmount()!=null && accountLineNew.getPayableContractRateAmmount().doubleValue()>0)
							accountLineNew.setPayableContractRateAmmount(new BigDecimal(decimalFormat.format((accountLineNew.getPayableContractRateAmmount().doubleValue())*((accountLineNew.getPayDeviation().doubleValue())/100))));	
					} 
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);	
	    		}	
				
				 
				
				  try{
			    		if(accountLineNew.getEstimateRevenueAmount() !=null && accountLineNew.getEstimateRevenueAmount().doubleValue()>0 && accountLineNew.getEstimateExpense()!=null && accountLineNew.getEstimateExpense().doubleValue()>0){
			    			Double estimatePassPercentageValue=(accountLineNew.getEstimateRevenueAmount().doubleValue()/accountLineNew.getEstimateExpense().doubleValue())*100;
			    			Integer estimatePassPercentage=estimatePassPercentageValue.intValue();
			    			accountLineNew.setEstimatePassPercentage(estimatePassPercentage);
				            }
				            if(accountLineNew.getRevisionRevenueAmount() !=null && accountLineNew.getRevisionRevenueAmount().doubleValue()>0 && accountLineNew.getRevisionExpense()!=null &&  accountLineNew.getRevisionExpense().doubleValue()>0){
				            	Double revisionPassPercentageValue=(accountLineNew.getRevisionRevenueAmount().doubleValue()/accountLineNew.getRevisionExpense().doubleValue())*100;
				            	Integer revisionPassPercentage=revisionPassPercentageValue.intValue();
				            	accountLineNew.setRevisionPassPercentage(revisionPassPercentage);
				            }
			    		}catch(Exception e){
			    			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			    		}	
				 
			    try{
			    	
			    String payVatPercent=accountLineNew.getPayVatPercent();
			    BigDecimal payVatPercentBig=new BigDecimal("0.00");
			    BigDecimal payVatAmmountBig=new BigDecimal("0.00");
			    BigDecimal estPayVatAmmountBig=new BigDecimal("0.00");
				BigDecimal revisionpayVatAmmountBig=new BigDecimal("0.00");
			    if(payVatPercent!=null && (!(payVatPercent.trim().equals("")))){
			    	payVatPercentBig=new BigDecimal(payVatPercent);
			    }
			    payVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getLocalAmount().multiply(payVatPercentBig).doubleValue())/100))));
			    try{
				    estPayVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getEstLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
				    revisionpayVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getRevisionLocalAmount().multiply(payVatPercentBig)).doubleValue())/100)));
				    
				  }catch(Exception e){
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    }
			    accountLineNew.setPayVatAmt(payVatAmmountBig);
			    accountLineNew.setEstExpVatAmt(estPayVatAmmountBig);
			    accountLineNew.setRevisionExpVatAmt(revisionpayVatAmmountBig);
			  
			    }catch(Exception e){
			    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			    }
	            accountLineNew.setServiceOrderId(serviceOrderToRecods.getId());
	            accountLineNew.setSequenceNumber(serviceOrderToRecods.getSequenceNumber());
	            accountLineNew.setShipNumber(serviceOrderToRecods.getShipNumber());
	            accountLineNew.setServiceOrder(serviceOrderToRecods);
	            try{
	            	String recVatPercent="0";
	            	recVatPercent=accountLineNew.getRecVatPercent();
				    BigDecimal recVatPercentBig=new BigDecimal("0.00");
				    BigDecimal recVatAmmountBig=new BigDecimal("0.00");
				    BigDecimal revisionVatAmmountBig=new BigDecimal("0.00");
				    BigDecimal estVatAmmountBig=new BigDecimal("0.00");
				    if(recVatPercent!=null && (!(recVatPercent.trim().equals("")))){
				    	recVatPercentBig=new BigDecimal(recVatPercent);
				    }
				    DecimalFormat decimalFormat = new DecimalFormat("#.####");
				    recVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getActualRevenueForeign().multiply(recVatPercentBig)).doubleValue())/100)));
				    try{
					    estVatAmmountBig= (new BigDecimal(decimalFormat.format(((accountLineNew.getEstSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
					    revisionVatAmmountBig = (new BigDecimal(decimalFormat.format(((accountLineNew.getRevisionSellLocalAmount().multiply(recVatPercentBig)).doubleValue())/100)));
					    }catch(Exception e){
					    	
					    }
				    accountLineNew.setRecVatAmt(recVatAmmountBig);
				    accountLineNew.setEstVatAmt(estVatAmmountBig); 
				    accountLineNew.setRevisionVatAmt(revisionVatAmmountBig);
				    }catch(Exception e){
				    	logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				    } 
	            accountLineNew=accountLineManager.save(accountLineNew);
	            
        	}
    		}catch(Exception e){
    			 e.printStackTrace();
    			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
    			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
    	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
    	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
    		}
    	
	}
		
		@SkipValidation
	    public void updateExternalAccountLine( ServiceOrder externalServiceOrder,String externalCorpID){ 
	    	try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				BigDecimal cuDivisormar=new BigDecimal(100);
				 
				externalServiceOrder.setEstimatedTotalRevenue(new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setDistributedTotalAmount(new BigDecimal((accountLineManager.getExternaldistributedAmountSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setProjectedDistributedTotalAmount(new BigDecimal((accountLineManager.getExternalProjectedDistributedTotalAmountSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setProjectedActualExpense(new BigDecimal((accountLineManager.getExternalProjectedActualExpenseSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setProjectedActualRevenue(new BigDecimal((accountLineManager.getExternalProjectedActualRevenueSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				BigDecimal divisormarEst=new BigDecimal(((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())); 
				if(externalServiceOrder.getEstimatedTotalRevenue()==null || externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00")) ||externalServiceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.0000"))){
					externalServiceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
				} 
				else 
				{
					externalServiceOrder.setEstimatedGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEst,2));
				} 
				externalServiceOrder.setEstimatedGrossMargin((new BigDecimal((accountLineManager.getExternalEstimateRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
				externalServiceOrder.setRevisedGrossMargin((new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
				externalServiceOrder.setRevisedTotalRevenue(new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				BigDecimal divisormarEstPer=new BigDecimal(((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				if(externalServiceOrder.getRevisedTotalRevenue() == null || externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00")) ||externalServiceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.0000"))){
					externalServiceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
				}
				else{
					externalServiceOrder.setRevisedGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalRevisionRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormarEstPer,2));
				} 
				externalServiceOrder.setRevisedTotalExpense(new BigDecimal((accountLineManager.getExternalRevisionExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setEstimatedTotalExpense(new BigDecimal((accountLineManager.getExternalEstimateExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setEntitledTotalAmount(new BigDecimal((accountLineManager.getExternalEntitleSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setActualExpense(new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setActualRevenue(new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				externalServiceOrder.setActualGrossMargin((new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()))));
				BigDecimal divisormar=new BigDecimal(((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()));
				if(externalServiceOrder.getActualRevenue()== null || externalServiceOrder.getActualRevenue().equals(new BigDecimal("0"))||externalServiceOrder.getActualRevenue().equals(new BigDecimal("0.00"))||externalServiceOrder.getActualRevenue().equals(new BigDecimal("0.0000"))){
					externalServiceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
				}
				else{
					externalServiceOrder.setActualGrossMarginPercentage((((new BigDecimal((accountLineManager.getExternalActualRevSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString()).subtract (new BigDecimal((accountLineManager.getExternalActualExpSum(externalServiceOrder.getShipNumber(),externalCorpID)).get(0).toString())))).multiply(cuDivisormar)).divide(divisormar,2));
				}
				/*# 7784 - Always show actual gross margin in accountline overview Start*/
				try{
					externalServiceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"EXP")));
				if(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV").doubleValue()!=0){
					externalServiceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((externalServiceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(externalServiceOrder.getShipNumber(),"REV")).doubleValue())*100));
				}else{
					externalServiceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
				}
				}catch(Exception e){}
				/*# 7784 - Always show actual gross margin in accountline overview End*/
				
				serviceOrderManager.updateFromAccountLine(externalServiceOrder); 
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}  
	    } 
		
		public void createConsigneeInstruction(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
			try {
				List consigneeInstructionRecordList=consigneeInstructionManager.consigneeInstructionList(serviceOrder.getId());
				if(!consigneeInstructionRecordList.isEmpty()){
					ConsigneeInstruction consigneeInstructionFrom=(ConsigneeInstruction)consigneeInstructionRecordList.get(0);
					ConsigneeInstruction consigneeInstructionNew= new ConsigneeInstruction();
					try{
						 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
						 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						 beanUtilsBean2.copyProperties(consigneeInstructionNew, consigneeInstructionFrom);
						 
						 consigneeInstructionNew.setId(serviceOrderNew.getId());
						 consigneeInstructionNew.setCorpID(serviceOrderNew.getCorpID());
						 consigneeInstructionNew.setShipNumber(serviceOrderNew.getShipNumber());
						 consigneeInstructionNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
						 consigneeInstructionNew.setShip(serviceOrderNew.getShip());
						 consigneeInstructionNew.setServiceOrderId(serviceOrderNew.getId());
						 consigneeInstructionNew.setCreatedBy("Networking");
						 consigneeInstructionNew.setUpdatedBy("Networking");
						 consigneeInstructionNew.setCreatedOn(new Date());
						 consigneeInstructionNew.setUpdatedOn(new Date());
						 consigneeInstructionNew = consigneeInstructionManager.save(consigneeInstructionNew); 
						 if(consigneeInstructionNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
							 consigneeInstructionNew.setUgwIntId(consigneeInstructionNew.getId().toString());
							 }
						 consigneeInstructionNew= consigneeInstructionManager.save(consigneeInstructionNew);
							 
					}catch(Exception e){
						 e.printStackTrace();
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
				    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
				    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
					}
					
				}
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
			
		}
		
		public void createFamilyDetail(CustomerFile customerFile,CustomerFile customerFileNew) {
			List dsFamilyDetailsList=dsFamilyDetailsManager.getDsFamilyDetailsList(customerFile.getId(),sessionCorpID);
			if(!dsFamilyDetailsList.isEmpty()){ 
				Iterator<DsFamilyDetails> it=dsFamilyDetailsList.iterator();
				while(it.hasNext()){
					DsFamilyDetails dsFamilyDetails = it.next(); 
				try{

					DsFamilyDetails dsFamilyDetailsNew = new DsFamilyDetails();
					BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(dsFamilyDetailsNew, dsFamilyDetails);
    				
    				dsFamilyDetailsNew.setCreatedBy("Networking");
    				dsFamilyDetailsNew.setUpdatedBy("Networking");
    				dsFamilyDetailsNew.setCreatedOn(new Date());
    				dsFamilyDetailsNew.setUpdatedOn(new Date());
    				dsFamilyDetailsNew.setCustomerFileId(customerFileNew.getId());
    				dsFamilyDetailsNew.setCorpID(customerFileNew.getCorpID());
    				dsFamilyDetailsNew.setNetworkId(dsFamilyDetails.getId());
    				dsFamilyDetailsNew.setId(null);
    				dsFamilyDetailsNew = dsFamilyDetailsManager.save(dsFamilyDetailsNew);
    				
    			int i=dsFamilyDetailsManager.updateFamilyDetailsNetworkId(dsFamilyDetails.getId());
				
					 
				}catch(Exception e){
					 e.printStackTrace();
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
				}
				}
			}
			
		}
		
		
		public void createAdAddressesDetails(CustomerFile customerFile,CustomerFile customerFileNew) {
			try {
				List adAddressesDetailsList=adAddressesDetailsManager.getAdAddressesDetailsList(customerFile.getId(),sessionCorpID);
				if(!adAddressesDetailsList.isEmpty()){
					Iterator<AdAddressesDetails> it=adAddressesDetailsList.iterator();
					while(it.hasNext()){ 
					AdAddressesDetails adAddressesDetails= it.next();
					try{
						AdAddressesDetails adAddressesDetailsNew = new AdAddressesDetails();
						BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
						beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						beanUtilsBean.copyProperties(adAddressesDetailsNew, adAddressesDetails);
						
						adAddressesDetailsNew.setCreatedBy("Networking");
						adAddressesDetailsNew.setUpdatedBy("Networking");
						adAddressesDetailsNew.setCreatedOn(new Date());
						adAddressesDetailsNew.setUpdatedOn(new Date());
						adAddressesDetailsNew.setCustomerFileId(customerFileNew.getId());
						adAddressesDetailsNew.setCorpID(customerFileNew.getCorpID());
						adAddressesDetailsNew.setNetworkId(adAddressesDetails.getId());
						adAddressesDetailsNew.setId(null);
						adAddressesDetailsNew = adAddressesDetailsManager.save(adAddressesDetailsNew);
						
						int i=adAddressesDetailsManager.updateAdAddressesDetailsNetworkId(adAddressesDetails.getId());
					}catch(Exception ex){					
						logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					}
					}
				}
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
			
		}
		
		
		public void createRemovalRelocationServiceDetails(CustomerFile customerFile,CustomerFile customerFileNew) {
			try{
			String removalReloCheck=customerFileManager.findRemovalReloCheck(customerFile.getId().toString());
			 if(removalReloCheck.equalsIgnoreCase("true")){
			 removalRelocationService=removalRelocationServiceManager.get(customerFile.getId()); 
			 RemovalRelocationService removalRelocationServiceNew = new RemovalRelocationService();
					BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(removalRelocationServiceNew, removalRelocationService);
    				
    				removalRelocationServiceNew.setCreatedBy("Networking");
    				removalRelocationServiceNew.setUpdatedBy("Networking");
    				removalRelocationServiceNew.setCreatedOn(new Date());
    				removalRelocationServiceNew.setUpdatedOn(new Date());
    				removalRelocationServiceNew.setCustomerFileId(customerFileNew.getId());
    				removalRelocationServiceNew.setId(customerFileNew.getId()) ;
    				removalRelocationServiceNew.setCorpId(customerFileNew.getCorpID());
    				 
    				removalRelocationServiceNew = removalRelocationServiceManager.save(removalRelocationServiceNew); 
    				 
				
				
				}
			}catch(Exception e){
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			
		}
		}
		public void createRoutingRecords(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
			try {
				List routingRecordList=servicePartnerManager.routingList(serviceOrder.getId());
				Iterator<ServicePartner> it=routingRecordList.iterator();
				while(it.hasNext()){
					ServicePartner servicePartnerFrom= it.next();
					ServicePartner servicePartnerNew= new ServicePartner();
					try{
						 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
						 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						 beanUtilsBean2.copyProperties(servicePartnerNew, servicePartnerFrom);
						 
						 servicePartnerNew.setId(null);
						 servicePartnerNew.setCorpID(serviceOrderNew.getCorpID());
						 servicePartnerNew.setCarrierCode("T10000");
						 servicePartnerNew.setShipNumber(serviceOrderNew.getShipNumber());
						 servicePartnerNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
						 servicePartnerNew.setServiceOrder(serviceOrderNew);
						 servicePartnerNew.setServiceOrderId(serviceOrderNew.getId());
						 servicePartnerNew.setCreatedBy("Networking");
						 servicePartnerNew.setUpdatedBy("Networking");
						 servicePartnerNew.setCreatedOn(new Date());
						 servicePartnerNew.setUpdatedOn(new Date()); 
						 servicePartnerNew=servicePartnerManager.save(servicePartnerNew); 
						 
						 if(servicePartnerNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
							 servicePartnerNew.setUgwIntId(servicePartnerNew.getId().toString());
							 }
						 servicePartnerNew= servicePartnerManager.save(servicePartnerNew);
					}catch(Exception e){
						 e.printStackTrace();
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
				    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
				    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
					}
				}
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			}
		}
		public void createVehicleRecords(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
			List vehicleRecordList=vehicleManager.vehicleList(serviceOrder.getId());
			Iterator<Vehicle> it=vehicleRecordList.iterator();
			while(it.hasNext()){
				Vehicle vehicleFrom=it.next();
				Vehicle vehicleNew= new Vehicle();
				try{
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(vehicleNew, vehicleFrom);
					 vehicleNew.setId(null);
					 vehicleNew.setCorpID(serviceOrderNew.getCorpID());
					 vehicleNew.setShipNumber(serviceOrderNew.getShipNumber());
					 vehicleNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
					 vehicleNew.setServiceOrder(serviceOrderNew);
					 vehicleNew.setServiceOrderId(serviceOrderNew.getId());
					 vehicleNew.setCreatedBy("Networking");
					 vehicleNew.setUpdatedBy("Networking");
					 vehicleNew.setCreatedOn(new Date());
					 vehicleNew.setUpdatedOn(new Date()); 
					 vehicleNew= vehicleManager.save(vehicleNew);
					 
					 if(vehicleNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 vehicleNew.setUgwIntId(vehicleNew.getId().toString());
						 }
					 vehicleNew= vehicleManager.save(vehicleNew);
					 
				}catch(Exception e){
					 e.printStackTrace();
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
				}
			}
			
		}
		public void createPieceCountsRecords(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
			List cartonRecordList=cartonManager.cartonList(serviceOrder.getId());
			Iterator<Carton> it=cartonRecordList.iterator();
			while(it.hasNext()){
				Carton cartonFrom=it.next();
				Carton cartonNew= new Carton();
				try{
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(cartonNew, cartonFrom);
					 cartonNew.setId(null);
					 cartonNew.setCorpID(serviceOrderNew.getCorpID());
					 cartonNew.setShipNumber(serviceOrderNew.getShipNumber());
					 cartonNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
					 cartonNew.setServiceOrder(serviceOrderNew);
					 cartonNew.setServiceOrderId(serviceOrderNew.getId());
					 cartonNew.setCreatedBy("Networking");
					 cartonNew.setUpdatedBy("Networking");
					 cartonNew.setCreatedOn(new Date());
					 cartonNew.setUpdatedOn(new Date()); 
					 cartonNew=cartonManager.save(cartonNew);
					 
					 if(cartonNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 cartonNew.setUgwIntId(cartonNew.getId().toString());
						 }
					 cartonNew= cartonManager.save(cartonNew);
					 
					  
				}catch(Exception e){
					 e.printStackTrace();
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
				}
			}
		}
		public void createContainerRecords(ServiceOrder serviceOrder, ServiceOrder serviceOrderNew) {
			List containerRecordList=containerManager.containerList(serviceOrder.getId());
			Iterator<Container> it=containerRecordList.iterator();
			while(it.hasNext()){
				Container containerFrom=it.next();
				Container containerNew= new Container();
				try{
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(containerNew, containerFrom);
					
					 containerNew.setId(null);
					 containerNew.setCorpID(serviceOrderNew.getCorpID());
					 containerNew.setShipNumber(serviceOrderNew.getShipNumber());
					 containerNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
					 containerNew.setServiceOrder(serviceOrderNew);
					 containerNew.setServiceOrderId(serviceOrderNew.getId());
					 containerNew.setCreatedBy("Networking");
					 containerNew.setUpdatedBy("Networking");
					 containerNew.setCreatedOn(new Date());
					 containerNew.setUpdatedOn(new Date());
					 containerNew= containerManager.save(containerNew);
					 
					 if(containerNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 containerNew.setUgwIntId(containerNew.getId().toString());
						 }
					 containerNew= containerManager.save(containerNew);
					 
				}catch(Exception ex){
					ex.printStackTrace();
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
					String logMessage =  "Exception:"+ ex.toString()+" at #"+ex.getStackTrace()[0].toString();
			    	  String logMethod =   ex.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , ex.getStackTrace()[0].getFileName().toString().replace(".java", ""));
				}
			}
		}
		
		public void createClaim(ServiceOrder serviceOrder, ServiceOrder serviceOrderNew){
			trackingStatusToRecod=trackingStatusManager.getForOtherCorpid(serviceOrderNew.getId());
			if(trackingStatusToRecod.getSoNetworkGroup()){
				List <Claim> claimRecordList = claimManager.claimList(serviceOrder.getId()); 
				for(Claim claimFrom : claimRecordList ){
					
					Claim claimNew= new Claim();
					try{
						Long newClaimNumber = claimManager.findMaximumForOtherCorpid(serviceOrderNew.getCorpID());
						 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
						 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
						 beanUtilsBean2.copyProperties(claimNew, claimFrom);
						
						 claimNew.setId(null);
						 claimNew.setCorpID(serviceOrderNew.getCorpID());
						 claimNew.setShipNumber(serviceOrderNew.getShipNumber());
						 claimNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
						 claimNew.setServiceOrder(serviceOrderNew);
						 claimNew.setServiceOrderId(serviceOrderNew.getId());
						 claimNew.setCreatedBy("Networking");
						 claimNew.setUpdatedBy("Networking");
						 claimNew.setCreatedOn(new Date());
						 claimNew.setUpdatedOn(new Date());
						 claimNew.setClaimNumber(newClaimNumber);
						 try{
						 List  l1 = claimManager.findPartnerCodeOtherCorpID(claimFrom.getInsurerCode(),sessionCorpID);
							if(l1!=null && !l1.isEmpty() && l1.get(0)!=null){
								Boolean agentChk   = (Boolean) l1.get(0);
									if(!agentChk){
										claimNew.setInsurerCode("");
										claimNew.setInsurer("");
									}
							}else{
								claimNew.setInsurerCode("");
								claimNew.setInsurer("");
							}
						 }catch(Exception e){
							 logger.error("Exception Occour: "+ e.getStackTrace()[0]);
							 claimNew.setInsurerCode("");
							 claimNew.setInsurer("");
						 }
						 claimNew= claimManager.save(claimNew);
						
						 
					}catch(Exception e){
						 e.printStackTrace();
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
						String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
				    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
				    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
					}
				try{ 
					List <Loss> lossRecordList = lossManager.lossList(claimFrom.getId()); 
					for(Loss lossFrom : lossRecordList ){
						try{
						Loss lossNew= new Loss();
						List  maxIdNumber1 = lossManager.findMaximumIdNumber1(serviceOrderNew.getCorpID());
						if (maxIdNumber1== null ||  maxIdNumber1.get(0) == null ) {          
						String	idNumber1 = "01";
						}else {
							autoIdNumber1 = Integer.parseInt(maxIdNumber1.get(0).toString()) + 1;
						}
						BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
						beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					    beanUtilsBean2.copyProperties(lossNew, lossFrom);
					    lossNew.setIdNumber1(autoIdNumber1);
					    
					    lossNew.setCorpID(serviceOrderNew.getCorpID());
					    lossNew.setId(null);
						lossNew.setShipNumber(serviceOrderNew.getShipNumber());
						lossNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
						lossNew.setCreatedBy("Networking");
						lossNew.setWarehouse("");
						lossNew.setUpdatedBy("Networking");
						lossNew.setCreatedOn(new Date());
					    lossNew.setUpdatedOn(new Date()); 
					    lossNew.setClaimId(claimNew.getId().toString());
					    lossNew.setClaimNumber(claimNew.getClaimNumber());
					    lossNew.setClaim(claimNew);
					    lossNew= lossManager.save(lossNew);	
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
					}	
				}catch(Exception e){
					 e.printStackTrace();
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
				}
				}
	       	}
		}
		
		
		
		@SkipValidation
		public String networkPartner(){
			try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				Boolean test=trackingStatusManager.getIsNetworkAgent(sessionCorpID,respectiveNetworkAgent);
				if(test){
					Set<String> s = new HashSet<String>();
					if (parentShipNumber != null && (!"".equals(parentShipNumber.trim()))) {
						List networkPartnerAgentCorpId = trackingStatusManager.getIsNetworkAgentCorpId(sessionCorpID,respectiveNetworkAgent);
					
							List list = serviceOrderManager.findBookingAgentShipNumber(parentShipNumber);
						    Iterator itr = list.iterator();
						    while(itr.hasNext()){
						    	ServiceOrder newSO = (ServiceOrder)itr.next();
						    	s.add(newSO.getCorpID());
								if (newSO.getBookingAgentShipNumber() != null && !newSO.getBookingAgentShipNumber().equals("")) {
									parentShipNumber=newSO.getBookingAgentShipNumber();
								}
								
							}
						
						if (networkPartnerAgentCorpId != null && networkPartnerAgentCorpId.size()>0) {
							if(s.contains(networkPartnerAgentCorpId.get(0).toString())){
								networkPartnerCode="PARENT";
							}else{
								networkPartnerCode="true";
							}
						}else{
							networkPartnerCode="true";
						}
					}else{
						networkPartnerCode="true";
					}
				}else{
					networkPartnerCode="false";
				}
			} catch (Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 return "errorlog";
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
		public String originSubAgentCodeValid(String accountInterface)throws Exception{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String pType= "" ;  
			if(accountInterface.equalsIgnoreCase("Y")){
			      pType= "";
			}			
			String originSubAgent=	trackingStatus.getOriginSubAgentCode();
			originSubAgent=originSubAgent.trim();
			if(trackingStatusManager.findByoriginSubAgentCode(originSubAgent).isEmpty()){
				return INPUT;
			}
			else{
				List a = accountLineManager.validateDiscription(trackingStatus.getOriginSubAgentCode(),sessionCorpID,pType);
				if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
				String listString = a.get(0).toString(); 
				String[] listString1 = listString.split("#");
				
				trackingStatus.setOriginSubAgent(listString1[0]);
				try{
				if(listString1.length>2){
					if(trackingStatus.getSubOriginAgentEmail()==null || trackingStatus.getSubOriginAgentEmail().trim().equals("")){
						trackingStatus.setSubOriginAgentEmail(listString1[2]);
				}
					
				}
					
				}
				catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 e.printStackTrace();
				}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			   }
			else {
				return INPUT;	
			     } 
		}
	}		
		
		public String brokerCodeValid(String accountInterface)throws Exception{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String pType= "" ;  
			if(accountInterface.equalsIgnoreCase("Y")){
				pType="";
			}else{
				pType= "VN";
			} 
			String broker=	trackingStatus.getBrokerCode();
			broker=broker.trim();
			if(trackingStatusManager.findByoriginSubAgentCode(broker).isEmpty()){
				return INPUT;
			}
		    else{
			List a = accountLineManager.validateDiscription(trackingStatus.getBrokerCode(),sessionCorpID,pType);
			if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
			String listString = a.get(0).toString(); 
			String[] listString1 = listString.split("#");
			
			trackingStatus.setBrokerName(listString1[0]);
			try{
			if(listString1.length>2){
				if(trackingStatus.getBrokerEmail()==null || trackingStatus.getBrokerEmail().trim().equals("")){
				trackingStatus.setBrokerEmail(listString1[2]);
				}
			}
			}catch(Exception e){
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		  }	
			else {
				return INPUT;	
			     }	
		    }
		}		
				
		public String originAgentCodeValid(String accountInterface)throws Exception{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				String pType= "" ;  
			if(accountInterface.equalsIgnoreCase("Y")){
				pType= "";		
			} else {
				pType= "AG";				
			}
			String agentCode=trackingStatus.getOriginAgentCode();
			agentCode=agentCode.trim();
			if(trackingStatusManager.findByoriginSubAgentCode(agentCode).isEmpty()){
				return INPUT;
			}
			else{ 
				List a = accountLineManager.validateDiscription(trackingStatus.getOriginAgentCode(),sessionCorpID,pType);
				if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
				String listString = a.get(0).toString(); 
				String[] listString1 = listString.split("#");
				
				trackingStatus.setOriginAgent(listString1[0]);
				try{
				if(listString1.length>2){
					if(trackingStatus.getOriginAgentEmail()==null || trackingStatus.getOriginAgentEmail().trim().equals("")){
						trackingStatus.setOriginAgentEmail(listString1[2]);
						}
			       }
					
								}catch(Exception e){
									logger.error("Exception Occour: "+ e.getStackTrace()[0]);
									}
								
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
				}
				else {
					return INPUT;	
				     }	
			}						
		}
		
		
		public String destinationAgentCodeValid(String accountInterface)throws Exception{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String pType= "" ;  
			if(accountInterface.equalsIgnoreCase("Y")){
				pType= "" ;	
			}else {
				pType= "AG" ;
			}
			
			String destAgentCode=trackingStatus.getDestinationAgentCode();
			destAgentCode=destAgentCode.trim();
			
			if(trackingStatusManager.findByoriginSubAgentCode(destAgentCode).isEmpty()){
				return INPUT;
			}
			else{
				List a = accountLineManager.validateDiscription(trackingStatus.getDestinationAgentCode(),sessionCorpID,pType);
				if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
				String listString = a.get(0).toString(); 
				String[] listString1 = listString.split("#");
				
				trackingStatus.setDestinationAgent(listString1[0]);
				try{
				if(listString1.length>2){
					if(trackingStatus.getDestinationAgentEmail()==null || trackingStatus.getDestinationAgentEmail().trim().equals("")){
						trackingStatus.setDestinationAgentEmail(listString1[2]);
						}
					
				}
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			     }		
				else {
					return INPUT;	
				     }	
			}       
		}
		@SkipValidation	
		public String checkNetworkCodeCheckCompanydivision(){
			try
			{
		venderDetails=trackingStatusManager.getvenderCodeDetais(networkVendorCode);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		return SUCCESS;
		}
		
		public String destinationSubAgentCodeValid(String accountInterface)throws Exception{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			 
		    String pType= "" ;  
			if(accountInterface.equalsIgnoreCase("Y")){
				pType= "";
			}
			
			String subAgentCode=trackingStatus.getDestinationSubAgentCode();
			subAgentCode=subAgentCode.trim();
			if(trackingStatusManager.findByoriginSubAgentCode(subAgentCode).isEmpty()){
				return INPUT;
			}
			else{
				List a = accountLineManager.validateDiscription(trackingStatus.getDestinationSubAgentCode(),sessionCorpID,pType);
				if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
				String listString = a.get(0).toString(); 
				String[] listString1 = listString.split("#");
				
				trackingStatus.setDestinationSubAgent(listString1[0]);
				try{
				if(listString1.length>2){
					if(trackingStatus.getSubDestinationAgentEmail()==null || trackingStatus.getSubDestinationAgentEmail().trim().equals("")){
						trackingStatus.setSubDestinationAgentEmail(listString1[2]);
						}
					
				}
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
				}
				else {
				return INPUT;	
			     }		
			}	
	}
			
		
		public String forwardCodeValid(String accountInterface)throws Exception{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 
		    String pType= "" ;  
			if(accountInterface.equalsIgnoreCase("Y")){
				pType= "";
			}else{
				pType= "VN" ;				
			}
			
			String subAgentCode=trackingStatus.getForwarderCode();
			subAgentCode=subAgentCode.trim();
			if(trackingStatusManager.findByoriginSubAgentCode(subAgentCode).isEmpty()){
				return INPUT;
			}
			else{
				List a = accountLineManager.validateDiscription(trackingStatus.getForwarderCode(),sessionCorpID,pType);
				if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
				String listString = a.get(0).toString(); 
				String[] listString1 = listString.split("#");
				
				trackingStatus.setForwarder(listString1[0]);
				try{
				if(listString1.length>2){
					if(trackingStatus.getForwarderEmail()==null || trackingStatus.getForwarderEmail().trim().equals("")){
						trackingStatus.setForwarderEmail(listString1[2]);
						}
					
				}
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				}
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
				} else {
				return INPUT;	
				}
			}			
		}
		
		
				public String originGivenValidate(String accountInterface)throws Exception{
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
						String pType= "" ;  
					if(accountInterface.equalsIgnoreCase("Y")){
						pType= "";		
					} else {
						pType= "AG";				
					}
					String agentCode=trackingStatus.getOriginGivenCode();
					agentCode=agentCode.trim();
					if(trackingStatusManager.findByoriginSubAgentCode(agentCode).isEmpty()){
						return INPUT;
					}
					else{ 
						List a = accountLineManager.validateDiscription(trackingStatus.getOriginGivenCode(),sessionCorpID,pType);
						if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
						String listString = a.get(0).toString(); 
						String[] listString1 = listString.split("#");
						
						trackingStatus.setOriginGivenName(listString1[0]);
						
						logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
						return SUCCESS;
						}
						else {
							return INPUT;	
						     }	
					}						
				}
				
				
				public String originReceivedValidate(String accountInterface)throws Exception{
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
						String pType= "" ;  
					if(accountInterface.equalsIgnoreCase("Y")){
						pType= "";		
					} else {
						pType= "AG";				
					}
					String agentCode=trackingStatus.getOriginReceivedCode();
					agentCode=agentCode.trim();
					if(trackingStatusManager.findByoriginSubAgentCode(agentCode).isEmpty()){
						return INPUT;
					}
					else{ 
						List a = accountLineManager.validateDiscription(trackingStatus.getOriginReceivedCode(),sessionCorpID,pType);
						if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
						String listString = a.get(0).toString(); 
						String[] listString1 = listString.split("#");
						
						trackingStatus.setOriginReceivedName(listString1[0]);
						
						logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
						return SUCCESS;
						}
						else {
							return INPUT;	
						     }	
					}						
				}				
				
				
				public String destinationGivenValidate(String accountInterface)throws Exception{
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
						String pType= "" ;  
					if(accountInterface.equalsIgnoreCase("Y")){
						pType= "";		
					} else {
						pType= "AG";				
					}
					String agentCode=trackingStatus.getDestinationGivenCode();
					agentCode=agentCode.trim();
					if(trackingStatusManager.findByoriginSubAgentCode(agentCode).isEmpty()){
						return INPUT;
					}
					else{ 
						List a = accountLineManager.validateDiscription(trackingStatus.getDestinationGivenCode(),sessionCorpID,pType);
						if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
						String listString = a.get(0).toString(); 
						String[] listString1 = listString.split("#");
						
						trackingStatus.setDestinationGivenName(listString1[0]);
						
						logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
						return SUCCESS;
						}
						else {
							return INPUT;	
						     }	
					}						
				}				
				
				
				public String destinationReceivedValidate(String accountInterface)throws Exception{
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
						String pType= "" ;  
					if(accountInterface.equalsIgnoreCase("Y")){
						pType= "";		
					} else {
						pType= "AG";				
					}
					String agentCode=trackingStatus.getDestinationReceivedCode();
					agentCode=agentCode.trim();
					if(trackingStatusManager.findByoriginSubAgentCode(agentCode).isEmpty()){
						return INPUT;
					}
					else{ 
						List a = accountLineManager.validateDiscription(trackingStatus.getDestinationReceivedCode(),sessionCorpID,pType);
						if(a!=null && (!(a.isEmpty())) && a.get(0)!=null){
						String listString = a.get(0).toString(); 
						String[] listString1 = listString.split("#");
						
						trackingStatus.setDestinationReceivedName(listString1[0]);
						
						logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
						return SUCCESS;
						}
						else {
							return INPUT;	
						     }	
					}						
				}				
							
				
		public String saveOnTabChange() throws Exception {
			try
			{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 	if (componentId != null && !componentId.equals("")){
				ServletContext context = getRequest().getSession().getServletContext();
				XmlWebApplicationContext ctx = (XmlWebApplicationContext) context.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
				RoleBasedComponentPermissionUtil roleBasedComponentPermissionUtil = (RoleBasedComponentPermissionUtil) ctx.getBean("componentConfigByRoleUtil");		
				accessAllowed = roleBasedComponentPermissionUtil.getComponentAccessAttrbute(componentId + "." + RoleBasedComponentPermissionUtil.EDIT_ACCESS);
				if(accessAllowed)
				{
					validateFormNav = "OK";
		            String s = save(); 
		            return gotoPageString;
				}
				else
				{
					validateFormNav = "OK";
					return gotoPageString;
				}
			} }
			catch(Exception e)
			{
				e.printStackTrace();
			}
		 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return gotoPageString;
		}
		
	@SkipValidation
	public String bookerAddressSearch(){	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		agentAddress=addressManager.searchBookerAddress(shipNumberA);
			
			try{
			if(!agentAddress.isEmpty())
			{
				ListIterator listIterator = agentAddress.listIterator();				
				while(listIterator.hasNext()) {
					Object []row= (Object [])listIterator.next();
					if(row[0]==null){
						billingAddress1="";
					}else {
						billingAddress1=(String)row[0].toString();
					}
					if(row[1]==null) {
						billingAddress2="";
					} else {
						billingAddress2=(String)row[1].toString();
					}
					
					if(row[2]==null) {
						billingAddress3="";
					}else {
						billingAddress3=(String)row[2].toString();
					}
			    	if(row[3]==null) {
						billingAddress4="";
					}else {
						billingAddress4=(String)row[3].toString();
					}
					if(row[4]==null) {
						billingCity="";
					}else {
						billingCity=(String)row[4].toString();
					}
					if(row[5]==null) {
						billingState="";
					}else {
						billingState=(String)row[5].toString();
					}
					if(row[6]==null) {
						billingZip="";
					}else {
						billingZip=(String)row[6].toString();
					}
					if(row[7]==null) {
						billingCountry="";
					}else {
						billingCountry=(String)row[7].toString();
					}
					if(row[8]==null) {
						billingFax="";
					}else {
						billingFax=(String)row[8].toString();
					}
					if(row[9]==null) {
						billingPhone="";
					}else {
						billingPhone=(String)row[9].toString();
					}
					if(row[10]==null) {
						billingTelex="";
					}else {
						billingTelex=(String)row[10].toString();
					}
					if(row[11]==null) {
						billingEmail="";
					}else {
						billingEmail=(String)row[11].toString();
					}
		       }
			} else{
			}
			}
			catch(Exception e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
		    	 return "errorlog";
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    return SUCCESS;
		}

		@SkipValidation
		public String addressSearch(){	
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			if(originalCorpID==null || originalCorpID.equalsIgnoreCase("")){originalCorpID=sessionCorpID;}
		agentAddress=addressManager.search(partnerCode,originalCorpID,accountLineBillingParam);
			try{
			if(!agentAddress.isEmpty()) {
				ListIterator listIterator = agentAddress.listIterator();				
				while(listIterator.hasNext()) {
					Object []row= (Object [])listIterator.next();
					if(row[0]==null) {
						billingAddress1="";
					}else {
						billingAddress1=(String)row[0].toString();
					}
					if(row[1]==null) {
						billingAddress2="";
					}else {
						billingAddress2=(String)row[1].toString();
					}
					if(row[2]==null) {
						billingAddress3="";
					}else {
						billingAddress3=(String)row[2].toString();
					}
					if(row[3]==null) {
						billingAddress4="";
					}else {
						billingAddress4=(String)row[3].toString();
					}
					if(row[4]==null) {
						billingCity="";
					}else {
						billingCity=(String)row[4].toString();
					}
					if(row[5]==null) {
						billingState="";
					}else {
						billingState=(String)row[5].toString();
					}
					if(row[6]==null) {
						billingZip="";
					}else {
						billingZip=(String)row[6].toString();
					}
					if(row[7]==null) {
						billingCountry="";
					}else {
						billingCountry=(String)row[7].toString();
					}
					if(row[8]==null) {
						billingFax="";
					}else {
						billingFax=(String)row[8].toString();
					}
					if(row[9]==null) {
						billingPhone="";
					}else {
						billingPhone=(String)row[9].toString();
					}
					if(row[10]==null) {
						billingTelex="";
					}else {
						billingTelex=(String)row[10].toString();
					}
					if(row[11]==null) {
						billingEmail="";
					}else {
						billingEmail=(String)row[11].toString();
					}
		       }
			}else{
			}
			}
			catch(Exception e) {
				 e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 return "errorlog";
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    return SUCCESS;
	}
		
		@SkipValidation
		public String viewPartnerDetailsForBillTo(){	
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			agentAddress=addressManager.partnerAddressDetails(partnerCode,sessionCorpID);
			valuesFromParameters = refMasterManager.getCodeWithDecription(sessionCorpID);
				try{
				if(!agentAddress.isEmpty())
				{
					ListIterator listIterator = agentAddress.listIterator();
					while(listIterator.hasNext()) {
						Object []row= (Object [])listIterator.next();
					
						if(row[0]==null){
							billingAddress1="";
						}else {
							billingAddress1=(String)row[0].toString();
						}
						if(row[1]==null) {
							billingAddress2="";
						} else {
							billingAddress2=(String)row[1].toString();
						}
						
						
						if(row[2]==null) {
							billingCity="";
						}else {
							billingCity=(String)row[2].toString();
						}
						if(row[3]==null) {
							billingState="";
						}else {
							billingState=(String)row[3].toString();
						}
						if(row[4]==null) {
							billingZip="";
						}else {
							billingZip=(String)row[4].toString();
						}
						if(row[5]==null) {
							billingCountry="";
						}else {
							billingCountry=(String)row[5].toString();
						}
						if(row[6]==null) {
							billingFax="";
						}else {
							billingFax=(String)row[6].toString();
						}
						if(row[7]==null) {
							billingPhone="";
						}else {
							billingPhone=(String)row[7].toString();
						}
						
						if(row[8]==null) {
							billingEmail="";
						}else {
							billingEmail=(String)row[8].toString();
						}
						
						if(row[9]==null) {
							vatBillingGroup="";
						}else {
							vatBillingGroup=valuesFromParameters.get((String)row[9].toString());
						}
						
						
						if(row[10]==null) {
							creditTerms="";
						}else {
							
							creditTerms = valuesFromParameters.get((String)row[10].toString());
						}
						
						if(row[11]==null) {
							paymentMethod="";
						}else {
							paymentMethod=valuesFromParameters.get((String)row[11].toString());
						}
						if(row[12]==null) {
							extReference="";
						}else {
							extReference=(String)row[12].toString();
							
						}
						if(row[13]==null) {
							agentParentName="";
						}else {
							agentParentName=(String)row[13].toString();
						}
						if(row[14]==null) {
							agentParentCode="";
						}else {
							agentParentCode=(String)row[14].toString();
						}
						if(row[15]==null) {
							vatNumber="";
						}else {
							vatNumber=(String)row[15].toString();
						}
						flagForPartnerAddress = true;
			       }
				} else{
				}
				}
				catch(Exception e) {
					 e.printStackTrace();
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 return "errorlog";
				}
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			    return SUCCESS;
			}
		
		
		
	@SkipValidation 
    public String getStorageForIconChange() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			    	List noteStorage = trackingStatusManager.countForStorage(trackingStatus.getShipNumber());
			if( noteStorage.isEmpty() ){
				countStorage = "0";
			}else {
				countStorage = ((noteStorage).get(0)).toString() ;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;   
    }
	
	@SkipValidation 
	public String getNotesForIconChange() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			List noteOrigin = notesManager.countForOriginDetailNotes(serviceOrder.getShipNumber());
			List noteDestination = notesManager.countForDestinationDetailNotes(serviceOrder.getShipNumber());
			List noteWeight = notesManager.countForWeightDetailNotes(serviceOrder.getShipNumber());
			List noteSurvey = notesManager.countForSurveyNotes(serviceOrder.getSequenceNumber());
			List noteOriginPackout = notesManager.countOriginPackoutNotes(serviceOrder.getShipNumber());
			List noteSitOrigin = notesManager.countSitOriginNotes(serviceOrder.getShipNumber());
			List noteOriginForwarding = notesManager.countOriginForwardingNotes(serviceOrder.getShipNumber());
			List noteGSTForwarding = notesManager.countGSTForwardingNotes(serviceOrder.getShipNumber());
			List noteDestinationStatus = notesManager.countDestinationStatusNotes(serviceOrder.getShipNumber());
			List noteSitDestination = notesManager.countSitDestinationNotes(serviceOrder.getShipNumber());
			List noteDestinationImport = notesManager.countDestinationImportNotes(serviceOrder.getShipNumber());
			List noteInterState = notesManager.countInterStateNotes(serviceOrder.getShipNumber());
			List noteServiceOrder = notesManager.countServiceOrderNotes(serviceOrder.getShipNumber());
			
			List networkNotes = notesManager.countNotesByShipNumAndSubType(serviceOrder.getShipNumber(),"NetworkAgentNotes");
			List originNotes = notesManager.countNotesByShipNumAndSubType(serviceOrder.getShipNumber(),"OriginAgentNotes");
			List destinNotes = notesManager.countNotesByShipNumAndSubType(serviceOrder.getShipNumber(),"DestinAgentNotes");
			List bookingAgentNotes = notesManager.countNotesByShipNumAndSubType(serviceOrder.getShipNumber(),"BookingAgentNotes");
			List subOriginNotes = notesManager.countNotesByShipNumAndSubType(serviceOrder.getShipNumber(),"SubOriginAgentNotes");
			List subDestinNotes = notesManager.countNotesByShipNumAndSubType(serviceOrder.getShipNumber(),"SubDestinAgentNotes");
			List brokerAgentNotes= notesManager.countNotesByShipNumAndSubType(serviceOrder.getShipNumber(),"BrokerAgent");
			List forwarderAgentNotes= notesManager.countNotesByShipNumAndSubType(serviceOrder.getShipNumber(),"ForwarderAgent"); 
			if( networkNotes == null || networkNotes.isEmpty() || networkNotes.get(0) == null || "".equals(networkNotes.get(0).toString().trim()) ){
				countNetworkAgentNotes = "0";
			}else {
				countNetworkAgentNotes = ((networkNotes).get(0)).toString() ;
			}
			
			if( originNotes == null || originNotes.isEmpty() || originNotes.get(0) == null || "".equals(originNotes.get(0).toString().trim()) ){
				countOriginAgentNotes = "0";
			}else {
				countOriginAgentNotes = ((originNotes).get(0)).toString() ;
			}
			if( destinNotes == null || destinNotes.isEmpty() || destinNotes.get(0) == null || "".equals(destinNotes.get(0).toString().trim()) ){
				countDestinAgentNotes = "0";
			}else {
				countDestinAgentNotes = ((destinNotes).get(0)).toString() ;
			}
			if( subOriginNotes == null || subOriginNotes.isEmpty() || subOriginNotes.get(0) == null || "".equals(subOriginNotes.get(0).toString().trim()) ){
				countSubOriginAgentNotes = "0";
			}else {
				countSubOriginAgentNotes = ((subOriginNotes).get(0)).toString() ;
			}
			if( subDestinNotes == null || subDestinNotes.isEmpty() || subDestinNotes.get(0) == null || "".equals(subDestinNotes.get(0).toString().trim()) ){
				countSubDestinAgentNotes = "0";
			}else {
				countSubDestinAgentNotes = ((subDestinNotes).get(0)).toString() ;
			}
			if( bookingAgentNotes == null || bookingAgentNotes.isEmpty() || bookingAgentNotes.get(0) == null || "".equals(bookingAgentNotes.get(0).toString().trim()) ){
				countBookingAgentNotes = "0";
			}else {
				countBookingAgentNotes = ((bookingAgentNotes).get(0)).toString() ;
			}
			if( brokerAgentNotes == null || brokerAgentNotes.isEmpty() || brokerAgentNotes.get(0) == null || "".equals(brokerAgentNotes.get(0).toString().trim()) ){
				countBrokerAgentNotes = "0";
			}else {
				countBrokerAgentNotes = ((brokerAgentNotes).get(0)).toString() ;
			}
			if( forwarderAgentNotes == null || forwarderAgentNotes.isEmpty() || forwarderAgentNotes.get(0) == null || "".equals(forwarderAgentNotes.get(0).toString().trim()) ){
				countForwarderAgentNotes = "0";
			}else {
				countForwarderAgentNotes = ((forwarderAgentNotes).get(0)).toString() ;
			}
			
			if( noteOrigin.isEmpty() ){
				countOriginDetailNotes = "0";
			}else {
				countOriginDetailNotes = ((noteOrigin).get(0)).toString() ;
			}
			if(noteDestination.isEmpty() ){
				countDestinationDetailNotes = "0";
			}else {
				countDestinationDetailNotes = ((noteDestination).get(0)).toString() ;
			}
			if( noteWeight.isEmpty() ){
				countWeightDetailNotes = "0";
			}else {
				countWeightDetailNotes = ((noteWeight).get(0)).toString() ;
			}
			if( noteSurvey.isEmpty() ){
				countSurveyNotes = "0";
			}else {
				countSurveyNotes = ((noteSurvey).get(0)).toString() ;
			}
			if( noteOriginPackout.isEmpty() ){
				countOriginPackoutNotes = "0";
			}else {
				countOriginPackoutNotes = ((noteOriginPackout).get(0)).toString() ;
			}
			if( noteSitOrigin.isEmpty() ){
				countSitOriginNotes = "0";
			}else {
				countSitOriginNotes = ((noteSitOrigin).get(0)).toString() ;
			}
			if( noteOriginForwarding.isEmpty() ){
				countOriginForwardingNotes = "0";
			}else {
				countOriginForwardingNotes = ((noteOriginForwarding).get(0)).toString() ;
			}
			if( noteGSTForwarding.isEmpty() ){
				countGSTForwardingNotes = "0";
			}else {
				countGSTForwardingNotes = ((noteGSTForwarding).get(0)).toString() ;
			}
			if( noteDestinationStatus.isEmpty() ){
				countDestinationStatusNotes = "0";
			}else {
				countDestinationStatusNotes = ((noteDestinationStatus).get(0)).toString() ;
			}
			if( noteSitDestination.isEmpty() ){
				countSitDestinationNotes = "0";
			}else {
				countSitDestinationNotes = ((noteSitDestination).get(0)).toString() ;
			}
			if( noteDestinationImport.isEmpty() ){
				countDestinationImportNotes = "0";
			}else {
				countDestinationImportNotes = ((noteDestinationImport).get(0)).toString() ;
			}
			if( noteInterState.isEmpty() ){
				countInterStateNotes = "0";
			}else {
				countInterStateNotes = ((noteInterState).get(0)).toString() ;
			}
			if( noteServiceOrder.isEmpty() ){
				countServiceOrderNotes = "0";
			}else {
				countServiceOrderNotes = ((noteServiceOrder).get(0)).toString() ;
			}
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;   
	}
	public String getComboList(String corpId){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 try {
	    	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Getting parameter values from combolist");
	    	 String parameters="'RELOCATIONSERVICES','YESNO','TAN','OMNI','DEMURRAG','NONTEMP','CLEARAT','partnerType','SITOUTTA','ENTRYFORM','EQUIP','MILITARY','SITORIGINREASON','SITDESTINATIONREASON','FLAGCARRIER','RDDREASON','LOCATIONTYPE','EXCESSWEIGHTBILLINGTYPE'"  ;
	    	 LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
	    	 relocationServices = new LinkedHashMap<String, String>();
	    	 military = new TreeMap<String, String>();
	    	 locationTypes=new LinkedHashMap<String,String>();
	    	 excessWeightBillingList=new LinkedHashMap<String,String>();
	    	 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpId,parameters);
	    	 for (RefMasterDTO refObj : allParamValue) {
	    		 if(refObj.getParameter().trim().equals("RELOCATIONSERVICES")){
	    		 		relocationServices.put(refObj.getCode().trim(), refObj.getFlex1().trim());
	    		 } else if(refObj.getParameter().trim().equals("LOCATIONTYPE")){
	    		 		locationTypes.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(), refObj.getDescription().trim());
	    	  	 } else if (refObj.getParameter().trim().equals("MILITARY")){
	    		 		military.put(refObj.getCode().trim() ,refObj.getCode().trim() ); 
	    		 	}
	    	  	else if(refObj.getParameter().trim().equals("EXCESSWEIGHTBILLINGTYPE")){ 
    		 		excessWeightBillingList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
	    		 		 }
	    	  	 else {
	    		 		
	    		 		if(parameterMap.containsKey(refObj.getParameter().trim())){
	    		 			tempParemeterMap = parameterMap.get(refObj.getParameter().trim());
	    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
	    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
	    		 		}else{
	    		 			tempParemeterMap = new LinkedHashMap<String, String>();
	    		 			tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
	    		 			parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
	    		 		}
	    		 	}
	    	 }
	    yesno = findByParameterLocal("YESNO");
	    tan=findByParameterLocal("TAN");
		omni=findByParameterLocal("OMNI");
		demurrag=findByParameterLocal("DEMURRAG");
		nontemp=findByParameterLocal("NONTEMP");
		clearat=findByParameterLocal("CLEARAT");
		partnerType=findByParameterLocal("partnerType");
		SITOUTTA=findByParameterLocal("SITOUTTA");
		ENTRYFORM=findByParameterLocal("ENTRYFORM");
		EQUIP=findByParameterLocal("EQUIP");
		sitOriginReason=findByParameterLocal("SITORIGINREASON");
		sitDestinationReason=findByParameterLocal("SITDESTINATIONREASON");
		flagCarrierList=findByParameterLocal("FLAGCARRIER");
		euVatPercentList = refMasterManager.findVatPercentList(corpId, "EUVAT");
		reasonList=findByParameterLocal("RDDREASON");
		containerMap=new HashMap();
		containerMap.put("", "");
		weightunits = new ArrayList();
			weightunits.add("Lbs");
			weightunits.add("Kgs");
			volumeunits = new ArrayList();
			volumeunits.add("Cft");
			volumeunits.add("Cbm");	
			jobtype = refMasterManager.findByParameter(corpId, "JOB");
	}catch (Exception e) {
		 e.printStackTrace();
				logger.error(" ERROR in ServiceOrderAction getComboList ");
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 return "errorlog";
	}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	
		return SUCCESS;
		}
	
	@SkipValidation
	public Map<String,String> findByParameterLocal(String parameter){ 
		localParameter = new HashMap<String, String>();
		try
		{
		if(parameterMap.containsKey(parameter)){
			localParameter = parameterMap.get(parameter);
		}}
		catch(Exception e)
		{
			
		}
		return localParameter;
	}
	
	@SkipValidation
	public String findBookingAgentCode()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			bookingAgentList = trackingStatusManager.findBookingAgentCode(companyCode, sessionCorpID);
		} catch (Exception e) {
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String omniReportForm()
	{	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			companyCodeList = companyDivisionManager.findCompanyCodeList(sessionCorpID);
			bookingAgentList = trackingStatusManager.findBookingAgentCode(companyCode, sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public void omniReport()
	{	 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		StringBuffer query= new StringBuffer();	
		StringBuffer fileName= new StringBuffer();	
		StringBuffer jobBuffer = new StringBuffer("");
		String header=new String();
		String eDate="";		
		if(endDate!=null){
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			eDate = new String( dateformatYYYYMMDD.format( endDate ) );	      
	    }
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		
		
		List omniReportExtractList=trackingStatusManager.omniReportExtract(bookingAgentCode,eDate,companyCode.replace(',', ' ').trim(),sessionCorpID,jobtypeMultiple);
		try		{
			
			if(!omniReportExtractList.isEmpty())
			{	
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OMNITonnage");
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header="Shipnumber"+"\t"+" Shipper's Name"+"\t"+"Origin Services Perfomed by"+"\t"+" OA OMNI #"+"\t"+" Destination Services performed By"+"\t"+" DA OMNI #"+"\t"+" Net Wt Lbs"+"\t"+" No of Shipments"+"\t"+" Mode "+"\n";
			    outputStream.write(header.getBytes());
			    
			Iterator it=omniReportExtractList.iterator();		
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();
					
				if(row[0]!=null)				
				{
					outputStream.write((row[0].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[1]!=null)				
				{
					outputStream.write((row[1].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[2]!=null)				
				{
					outputStream.write((row[2].toString()+"\t").getBytes()) ;
				}				
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[3]!=null)				
				{
							outputStream.write((row[3].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				if(row[4]!=null)				
				{
					outputStream.write((row[4].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				if(row[5]!=null)				
				{
					outputStream.write((row[5].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[6]!=null)				
				{
					String netWtLbs= row[6].toString();
					String netWtLbsResult="'"+netWtLbs;
					outputStream.write((netWtLbsResult+"\t").getBytes()) ;
					
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[7]!=null)				
				{
					String shipmnetNo= row[7].toString();
					String shipmnetNoResult="'"+shipmnetNo;
					outputStream.write((shipmnetNoResult+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				if(row[8]!=null)
				{							
					outputStream.write((row[8].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
								
				}
				outputStream.write("\n".getBytes()) ;
						
		       	}			
		        }				
			else
			{
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OMNITonnage");
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
			}
		}
		catch(Exception e)
		{ e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 } 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	
	
	@SkipValidation
	public void omniReportByOmni()
	{	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			StringBuffer query= new StringBuffer();	
		StringBuffer fileName= new StringBuffer();
		StringBuffer jobBuffer= new StringBuffer();
		String header=new String();
		String eDate="";		
		if(endDate!=null){
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			eDate = new String( dateformatYYYYMMDD.format( endDate ) );	      
	    }
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		
		List omniReportExtractList=trackingStatusManager.omniReportExtractByOmni(bookingAgentCode,eDate,companyCode.replace(',', ' ').trim(),sessionCorpID,jobtypeMultiple);
		try		{
			
			if(!omniReportExtractList.isEmpty())
			{	
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OMNITonnage");
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header="Shipnumber"+"\t"+" Shipper's Name"+"\t"+"Origin Services Perfomed by"+"\t"+" OA OMNI #"+"\t"+" Destination Services performed By"+"\t"+" DA OMNI #"+"\t"+" Net Wt Lbs"+"\t"+" No of Shipments"+"\t"+" Mode "+"\n";
			    outputStream.write(header.getBytes());
			    
			Iterator it=omniReportExtractList.iterator();		
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();
					
				if(row[0]!=null)				
				{
					outputStream.write((row[0].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[1]!=null)				
				{
					outputStream.write((row[1].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[2]!=null)				
				{
					outputStream.write((row[2].toString()+"\t").getBytes()) ;
				}				
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[3]!=null)				
				{
							outputStream.write((row[3].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				if(row[4]!=null)				
				{
					outputStream.write((row[4].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				if(row[5]!=null)				
				{
					outputStream.write((row[5].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[6]!=null)				
				{
					String netWtLbs= row[6].toString();
					String netWtLbsResult="'"+netWtLbs;
					outputStream.write((netWtLbsResult+"\t").getBytes()) ;
					
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[7]!=null)				
				{
					String shipmnetNo= row[7].toString();
					String shipmnetNoResult="'"+shipmnetNo;
					outputStream.write((shipmnetNoResult+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				if(row[8]!=null)
				{							
					outputStream.write((row[8].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
								
				}
				outputStream.write("\n".getBytes()) ;
						
		       	}			
		        }				
			else
			{
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OMNITonnage");
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
			}
		}
		catch(Exception e)
		{    e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	
	
	
	@SkipValidation
	public void omniReportUpdate()
	{	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			StringBuffer query= new StringBuffer();	
		StringBuffer fileName= new StringBuffer();	
		StringBuffer jobBuffer= new StringBuffer("");	
		String header=new String();
		String eDate="";		
		if(endDate!=null){
			SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
			eDate = new String( dateformatYYYYMMDD.format( endDate ) );	      
	    }
		if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
		} else {
			if (jobTypes.indexOf(",") == 0) {
				jobTypes = jobTypes.substring(1);
			}
			if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
				jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
			}
			String[] arrayJobType = jobTypes.split(",");
			int arrayLength = arrayJobType.length;
			for (int i = 0; i < arrayLength; i++) {
				jobBuffer.append("'");
				jobBuffer.append(arrayJobType[i].trim());
				jobBuffer.append("'");
				jobBuffer.append(",");
			}
		}
		String jobtypeMultiple = new String(jobBuffer);
		if (!jobtypeMultiple.equals("")) {
			jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
		} else if (jobtypeMultiple.equals("")) {
			jobtypeMultiple = new String("''");
		}
		List omniReportExtractList=trackingStatusManager.omniReportExtract(bookingAgentCode,eDate,companyCode.replace(',', ' ').trim(),sessionCorpID,jobtypeMultiple);
		try		{
			
			if(!omniReportExtractList.isEmpty())
			{	
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OMNITonnage");
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header="Shipnumber"+"\t"+" Shipper's Name"+"\t"+"Origin Services Perfomed by"+"\t"+" OA OMNI #"+"\t"+" Destination Services performed By"+"\t"+" DA OMNI #"+"\t"+" Net Wt Lbs"+"\t"+" No of Shipments"+"\t"+" Mode "+"\n";
			    outputStream.write(header.getBytes());
			    
			Iterator it=omniReportExtractList.iterator();		
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();
					
				if(row[0]!=null)				
				{
					outputStream.write((row[0].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[1]!=null)				
				{
					outputStream.write((row[1].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[2]!=null)				
				{
					outputStream.write((row[2].toString()+"\t").getBytes()) ;
				}				
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[3]!=null)				
				{
							outputStream.write((row[3].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				if(row[4]!=null)				
				{
					outputStream.write((row[4].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				if(row[5]!=null)				
				{
					outputStream.write((row[5].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[6]!=null)				
				{
					String netWtLbs= row[6].toString();
					String netWtLbsResult="'"+netWtLbs;
					outputStream.write((netWtLbsResult+"\t").getBytes()) ;
					
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				
				if(row[7]!=null)				
				{
					String shipmnetNo= row[7].toString();
					String shipmnetNoResult="'"+shipmnetNo;
					outputStream.write((shipmnetNoResult+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
				}
				
				if(row[8]!=null)
				{							
					outputStream.write((row[8].toString()+"\t").getBytes()) ;
				}
				else
				{
					outputStream.write("\t".getBytes());
								
				}
				outputStream.write("\n".getBytes()) ;
				if(row[0]!=null)
				{	
				trackingStatusManager.getOmniReport(row[0].toString(), sessionCorpID);
				}		
		       	}	
			
		        }				
			else
			{
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OMNITonnage");
				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".xls");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
			}
		}
		catch(Exception e)
		{ e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		} 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	@SkipValidation
	public String findVanlineCodeList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			vanLineCodeList = trackingStatusManager.findVanLineCodeList(partnerCode, sessionCorpID);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		return SUCCESS;
	}
	
	@SkipValidation
	public String findVanlineAgent(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			List vanLineAgent = trackingStatusManager.findVanLineAgent(partnerCode, sessionCorpID);
			if(!vanLineAgent.isEmpty()){
				agentCodeName = vanLineAgent.get(0).toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public void downloadXMLFile() throws Exception
{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			List list = trackingStatusManager.downloadXMLFile(sessionCorpID, fileNumber);
			Iterator its = list.iterator();
			Element root = doc.createElement("OrderTransaction");
			doc.appendChild(root);
			Element child = doc.createElement("TransactionData");
			root.appendChild(child);
			Element child1 = doc.createElement("FromSystem");
			child.appendChild(child1);
			Text text = doc.createTextNode("RedSky");
			child1.appendChild(text);
			Element child2 = doc.createElement("GeneratedDate");
			child.appendChild(child2);
			SimpleDateFormat postformats = new SimpleDateFormat("yyyyMMdd");
			StringBuilder postDates = new StringBuilder(postformats.format(new Date()));
			Text text2 = doc.createTextNode(postDates.toString());
			child2.appendChild(text2);
			Element child3 = doc.createElement("GeneratedTime");
			child.appendChild(child3);
			Calendar calendar = new GregorianCalendar();
			int hour = calendar.get(10);
			int minute = calendar.get(12);
			int second = calendar.get(13);
			String str = (new StringBuilder()).append(hour).append(":").append(minute).append(":").append(second).toString();
			Text text3 = doc.createTextNode(str);
			child3.appendChild(text3);
			Element child4 = doc.createElement("OrderHead");
			root.appendChild(child4);
			
			while(its.hasNext())
			{
				DTO ksdData = (DTO)its.next();
			    Element child5 = doc.createElement("OrderNumber");
			    child4.appendChild(child5);
			    Text text5 = doc.createTextNode(ksdData.getShipNumber().toString());
			    child5.appendChild(text5);
			    Element child6 = doc.createElement("WaybillNo");
			    child4.appendChild(child6);
			    Text text6 = doc.createTextNode("");
			    child6.appendChild(text6);
			    Element child7 = doc.createElement("ShipmentNumber");
			    child4.appendChild(child7);
			    Text text7= doc.createTextNode("");
			    child7.appendChild(text7);
			    Element child8 = doc.createElement("RouteNumber");
			    child4.appendChild(child8);
			    Text text8= doc.createTextNode("");
			    child8.appendChild(text8);
			    Element child9 = doc.createElement("TripNumber");
			    child4.appendChild(child9);
			    Text text9= doc.createTextNode("");
			    child9.appendChild(text9);
			    Element child10 = doc.createElement("ContainAdr");
			    child4.appendChild(child10);
			    Text text10= doc.createTextNode("");
			    child10.appendChild(text10);
			    Element child11 = doc.createElement("ContainDgr");
			    child4.appendChild(child11);
			    Text text11= doc.createTextNode("");
			    child11.appendChild(text11);
			    Element child12 = doc.createElement("ContainImo");
			    child4.appendChild(child12);
			    Text text12= doc.createTextNode("");
			    child12.appendChild(text12);
			    Element child13 = doc.createElement("ColdSensitive");
			    child4.appendChild(child13);
			    Text text13= doc.createTextNode("");
			    child13.appendChild(text13);
			    Element child14 = doc.createElement("FileLocation");
			    child4.appendChild(child14);
			    Text text14= doc.createTextNode("");
			    child14.appendChild(text14);
			    Element child15 = doc.createElement("RegStatus");
			    child4.appendChild(child15);
			    Text text15= doc.createTextNode("");
			    child15.appendChild(text15);
			    Element child16 = doc.createElement("OrderPlacedDate");
			    child4.appendChild(child16);
			    Text text16 = doc.createTextNode(postDates.toString());
			    child16.appendChild(text16);
			    Element child17 = doc.createElement("InvoiceNumber");
			    child4.appendChild(child17);
			    Text text17 = doc.createTextNode(ksdData.getRecInvoiceNumber().toString());
			    child17.appendChild(text17);
			    Element child18 = doc.createElement("InvoiceDate");
			    child4.appendChild(child18);
			    if(ksdData.getReceivedInvoiceDate()!=null &&(!( ksdData.getReceivedInvoiceDate().toString().equals("") ))) 
				{
			    	SimpleDateFormat postformatLevel1 = new SimpleDateFormat("yyyy-MM-dd");
			        Date du = new Date();
			        du = postformatLevel1.parse(ksdData.getReceivedInvoiceDate().toString());  
			    	SimpleDateFormat formats = new SimpleDateFormat("yyyyMMdd");
			        StringBuilder postDates1 = new StringBuilder(formats.format(du));
			        Text text18 = doc.createTextNode(postDates1.toString());
			        child18.appendChild(text18);
			        
				}
				else
				{
					 StringBuilder postDates1 = new StringBuilder();
					 Text text18 = doc.createTextNode(postDates1.toString());
				     child18.appendChild(text18);
				} 
			    Element child19 = doc.createElement("InvoiceCurrencyCode");
			    child4.appendChild(child19);
			    Text text19 = doc.createTextNode(ksdData.getRecRateCurrency().toString());
			    child19.appendChild(text19);
			    Element child20 = doc.createElement("InvoiceAmount");
			    child4.appendChild(child20);
			    Text text20 = doc.createTextNode(ksdData.getActualRevenue().toString());
			    child20.appendChild(text20);
			    Element child21 = doc.createElement("DeliveryDate");
			    child4.appendChild(child21);
			    if(ksdData.getDeliveryA()!=null &&(!( ksdData.getDeliveryA().toString().equals("") ))) 
				{
			    	
			    	
			    	
			    	SimpleDateFormat postformatLevel1 = new SimpleDateFormat("yyyy-MM-dd");
			        Date du = new Date();
			        du = postformatLevel1.parse(ksdData.getDeliveryA().toString());  
			    	SimpleDateFormat formats = new SimpleDateFormat("yyyyMMdd");
			        StringBuilder postDates1 = new StringBuilder(formats.format(du));
			        Text text21 = doc.createTextNode(postDates1.toString());
			        child21.appendChild(text21);
			         
				}
				else
				{
					 StringBuilder postDates1 = new StringBuilder();
					 Text text21 = doc.createTextNode(postDates1.toString());
					 child21.appendChild(text21);
				}
			    Element child22 = doc.createElement("CostCenterMarket");
			    child4.appendChild(child22);
			    Text text22 = doc.createTextNode("");
			    child22.appendChild(text22);
			    Element child23 = doc.createElement("CostCenterDistrict");
			    child4.appendChild(child23);
			    Text text23 = doc.createTextNode("");
			    child23.appendChild(text23);
			    Element child24 = doc.createElement("SellerData");
			    child4.appendChild(child24);
			    Text text24 = doc.createTextNode("");
			    child24.appendChild(text24);
			    Element child25 = doc.createElement("No");
			    child24.appendChild(child25);
			    Text text25 = doc.createTextNode(ksdData.getShipNumber().toString());
			    child25.appendChild(text25);
			    Element child26 = doc.createElement("Name");
			    child24.appendChild(child26);
			    Text text26 = doc.createTextNode(ksdData.getShiperName().toString());
			    child26.appendChild(text26);
			    Element child27 = doc.createElement("AddressLine1");
			    child24.appendChild(child27);
			    Text text27 = doc.createTextNode(ksdData.getAddressLine1().toString());
			    child27.appendChild(text27);
			    Element child28 = doc.createElement("AddressLine2");
			    child24.appendChild(child28);
			    Text text28 = doc.createTextNode(ksdData.getAddressLine2().toString());
			    child28.appendChild(text28);
			    Element child29 = doc.createElement("AddressLine3");
			    child24.appendChild(child29);
			    Text text29 = doc.createTextNode(ksdData.getAddressLine3().toString());
			    child29.appendChild(text29);
			    Element child30 = doc.createElement("AddressLine4");
			    child24.appendChild(child30);
			    Text text30 = doc.createTextNode("");
			    child30.appendChild(text30);
			    Element child31 = doc.createElement("ZIP");
			    child24.appendChild(child31);
			    Text text31 = doc.createTextNode(ksdData.getZip().toString());
			    child31.appendChild(text31);
			    Element child32 = doc.createElement("City");
			    child24.appendChild(child32);
			    Text text32 = doc.createTextNode(ksdData.getCity().toString());
			    child32.appendChild(text32);
			    Element child33 = doc.createElement("Country");
			    child24.appendChild(child33);
			    Text text33 = doc.createTextNode(ksdData.getOriginBucket2().toString());
			    child33.appendChild(text33);
			    Element child34 = doc.createElement("ContactPerson");
			    child24.appendChild(child34);
			    Text text34 = doc.createTextNode(ksdData.getCoordinator().toString());
			    child34.appendChild(text34);
			    Element child35 = doc.createElement("TelephoneNo");
			    child24.appendChild(child35);
			    Text text35 = doc.createTextNode("");
			    child35.appendChild(text35);
			    Element child36 = doc.createElement("FaxNo");
			    child24.appendChild(child36);
			    Text text36 = doc.createTextNode("");
			    child36.appendChild(text36);
			    Element child37= doc.createElement("PickupData");
			    child4.appendChild(child37);
			    Text text37 = doc.createTextNode("");
			    child37.appendChild(text37);
			    Element child38= doc.createElement("No");
			    child37.appendChild(child38);
			    Text text38 = doc.createTextNode("");
			    child38.appendChild(text38);
			    Element child39 = doc.createElement("Name");
			    child37.appendChild(child39);
			    Text text39 = doc.createTextNode("");
			    child39.appendChild(text39);
			    Element child40 = doc.createElement("AddressLine1");
			    child37.appendChild(child40);
			    Text text40 = doc.createTextNode("");
			    child40.appendChild(text40);
			    Element child41 = doc.createElement("AddressLine2");
			    child37.appendChild(child41);
			    Text text41 = doc.createTextNode("");
			    child41.appendChild(text41);
			    Element child42 = doc.createElement("AddressLine3");
			    child37.appendChild(child42);
			    Text text42 = doc.createTextNode("");
			    child42.appendChild(text42);
			    Element child43 = doc.createElement("AddressLine4");
			    child37.appendChild(child43);
			    Text text43 = doc.createTextNode("");
			    child43.appendChild(text43);
			    Element child44 = doc.createElement("ZIP");
			    child37.appendChild(child44);
			    Text text44 = doc.createTextNode("");
			    child44.appendChild(text44);
			    Element child45 = doc.createElement("City");
			    child37.appendChild(child45);
			    Text text45 = doc.createTextNode("");
			    child45.appendChild(text45);
			    Element child46 = doc.createElement("Country");
			    child37.appendChild(child46);
			    Text text46 = doc.createTextNode("");
			    child46.appendChild(text46);
			    Element child47 = doc.createElement("ContactPerson");
			    child37.appendChild(child47);
			    Text text47 = doc.createTextNode("");
			    child47.appendChild(text47);
			    Element child48 = doc.createElement("TelephoneNo");
			    child37.appendChild(child48);
			    Text text48 = doc.createTextNode("");
			    child48.appendChild(text48);
			    Element child49 = doc.createElement("FaxNo");
			    child37.appendChild(child49);
			    Text text49 = doc.createTextNode("");
			    child49.appendChild(text49);
			    Element child50 = doc.createElement("BuyerData");
			    child4.appendChild(child50);
			    Text text50 = doc.createTextNode("");
			    child50.appendChild(text50);
			    Element child51 = doc.createElement("No");
			    child50.appendChild(child51);
			    Text text51 = doc.createTextNode("");
			    child51.appendChild(text51);
			    Element child52 = doc.createElement("Name");
			    child50.appendChild(child52);
			    Text text52 = doc.createTextNode(ksdData.getShiperName().toString());
			    child52.appendChild(text52);
			    Element child53 = doc.createElement("AddressLine1");
			    child50.appendChild(child53);
			    Text text53 = doc.createTextNode(ksdData.getBuyerAddressLine1().toString());
			    child53.appendChild(text53);
			    Element child54 = doc.createElement("AddressLine2");
			    child50.appendChild(child54);
			    Text text54 = doc.createTextNode(ksdData.getBuyerAddressLine2().toString());
			    child54.appendChild(text54);
			    Element child55 = doc.createElement("AddressLine3");
			    child50.appendChild(child55);
			    Text text55 = doc.createTextNode(ksdData.getBuyerAddressLine3().toString());
			    child55.appendChild(text55);
			    Element child56 = doc.createElement("AddressLine4");
			    child50.appendChild(child56);
			    Text text56 = doc.createTextNode("");
			    child56.appendChild(text56);
			    Element child57 = doc.createElement("ZIP");
			    child50.appendChild(child57);
			    Text text57 = doc.createTextNode(ksdData.getBuyerZip().toString());
			    child57.appendChild(text57);
			    Element child58 = doc.createElement("City");
			    child50.appendChild(child58);
			    Text text58 = doc.createTextNode(ksdData.getBuyerCity().toString());
			    child58.appendChild(text58);
			    Element child59 = doc.createElement("Country");
			    child50.appendChild(child59);
			    Text text59 = doc.createTextNode(ksdData.getDestinationBucket2().toString());
			    child59.appendChild(text59);
			    Element child60 = doc.createElement("ContactPerson");
			    child50.appendChild(child60);
			    Text text60 = doc.createTextNode(ksdData.getCoordinator().toString());
			    child60.appendChild(text60);
			    Element child61 = doc.createElement("TelephoneNo");
			    child50.appendChild(child61);
			    Text text61 = doc.createTextNode(ksdData.getWorkPhone().toString());
			    child61.appendChild(text61);
			    Element child62 = doc.createElement("FaxNo");
			    child50.appendChild(child62);
			    Text text62 = doc.createTextNode(ksdData.getFaxNumber().toString());
			    child62.appendChild(text62);
			    Element child63 = doc.createElement("DeliveryData");
			    child4.appendChild(child63);
			    Text text63 = doc.createTextNode("");
			    child63.appendChild(text63);
			    Element child64 = doc.createElement("No");
			    child63.appendChild(child64);
			    Text text64 = doc.createTextNode("");
			    child64.appendChild(text64);
			    Element child65 = doc.createElement("Name");
			    child63.appendChild(child65);
			    Text text65 = doc.createTextNode("");
			    child65.appendChild(text65);
			    Element child66 = doc.createElement("AddressLine1");
			    child63.appendChild(child66);
			    Text text66 = doc.createTextNode("");
			    child66.appendChild(text66);
			    Element child67 = doc.createElement("AddressLine2");
			    child63.appendChild(child67);
			    Text text67 = doc.createTextNode("");
			    child67.appendChild(text67);
			    Element child68 = doc.createElement("AddressLine3");
			    child63.appendChild(child68);
			    Text text68 = doc.createTextNode("");
			    child68.appendChild(text68);
			    Element child69 = doc.createElement("AddressLine4");
			    child63.appendChild(child69);
			    Text text69 = doc.createTextNode("");
			    child69.appendChild(text69);
			    Element child70 = doc.createElement("ZIP");
			    child63.appendChild(child70);
			    Text text70 = doc.createTextNode("");
			    child70.appendChild(text70);
			    Element child71 = doc.createElement("City");
			    child63.appendChild(child71);
			    Text text71 = doc.createTextNode("");
			    child71.appendChild(text71);
			    Element child72 = doc.createElement("Country");
			    child63.appendChild(child72);
			    Text text72 = doc.createTextNode("");
			    child72.appendChild(text72);
			    Element child73 = doc.createElement("ContactPerson");
			    child63.appendChild(child73);
			    Text text73 = doc.createTextNode("");
			    child73.appendChild(text73);
			    Element child74 = doc.createElement("TelephoneNo");
			    child63.appendChild(child74);
			    Text text74 = doc.createTextNode("");
			    child74.appendChild(text74);
			    Element child75 = doc.createElement("FaxNo");
			    child63.appendChild(child75);
			    Text text75 = doc.createTextNode("");
			    child75.appendChild(text75);
			    Element child76 = doc.createElement("HaulierData");
			    child4.appendChild(child76);
			    Text text76 = doc.createTextNode("");
			    child76.appendChild(text76);
			    Element child77 = doc.createElement("No");
			    child76.appendChild(child77);
			    Text text77 = doc.createTextNode("");
			    child77.appendChild(text77);
			    Element child78 = doc.createElement("Name");
			    child76.appendChild(child78);
			    Text text78 = doc.createTextNode("");
			    child65.appendChild(text78);
			    Element child79 = doc.createElement("TransportBorderType");
			    child4.appendChild(child79);
			    Text text79 = doc.createTextNode("");
			    child79.appendChild(text79);
			    Element child80 = doc.createElement("Seller_palletno");
			    child4.appendChild(child80);
			    Text text80 = doc.createTextNode("");
			    child80.appendChild(text80);
			    Element child81 = doc.createElement("Seller_palletno");
			    child4.appendChild(child81);
			    Text text81 = doc.createTextNode("");
			    child81.appendChild(text81);
			    Element child82 = doc.createElement("Consignee_palletno");
			    child4.appendChild(child82);
			    Text text82 = doc.createTextNode(ksdData.getSocialSecurityNumber().toString());
			    child82.appendChild(text82);
			    Element child83 = doc.createElement("TermsOfDelivery");
			    child4.appendChild(child83);
			    Text text83 = doc.createTextNode("");
			    child83.appendChild(text83);
			    Element child84 = doc.createElement("PlaceOfDelivery");
			    child4.appendChild(child84);
			    Text text84= doc.createTextNode("");
			    child84.appendChild(text84);
			    Element child85 = doc.createElement("CountryOfDispatch");
			    child4.appendChild(child85);
			    Text text85= doc.createTextNode("");
			    child85.appendChild(text85);
			    Element child86 = doc.createElement("CountryOfDestination");
			    child4.appendChild(child86);
			    Text text86= doc.createTextNode("");
			    child86.appendChild(text86);
			    Element child87= doc.createElement("TotalGrossWeight");
			    child4.appendChild(child87);
			    Text text87= doc.createTextNode(ksdData.getActualgrossWeight().toString());
			    child87.appendChild(text87);
			    Element child88= doc.createElement("TotalNetWeight");
			    child4.appendChild(child88);
			    Text text88= doc.createTextNode(ksdData.getActualNetWeight().toString());
			    child88.appendChild(text88);
			    Element child89= doc.createElement("TotalNumberOfPackages");
			    child4.appendChild(child89);
			    Text text89= doc.createTextNode(ksdData.getPieces().toString());
			    child89.appendChild(text89);
			    Element child90= doc.createElement("CountryOfOrigin");
			    child4.appendChild(child90);
			    Text text90= doc.createTextNode(ksdData.getOriginBucket2().toString());
			    child90.appendChild(text90);
			    Element child91= doc.createElement("SellerRef");
			    child4.appendChild(child91);
			    Text text91= doc.createTextNode("");
			    child91.appendChild(text91);
			    Element child92= doc.createElement("BuyerRef");
			    child4.appendChild(child92);
			    Text text92= doc.createTextNode("");
			    child92.appendChild(text92);
			    Element child93= doc.createElement("ForwarderInfo");
			    child4.appendChild(child93);
			    Text text93= doc.createTextNode("");
			    child93.appendChild(text93);
			    Element child94= doc.createElement("NationalityTransportBorder");
			    child4.appendChild(child94);
			    Text text94= doc.createTextNode("");
			    child94.appendChild(text94);
			    Element child95= doc.createElement("TransportIDBorder");
			    child4.appendChild(child95);
			    Text text95= doc.createTextNode("");
			    child95.appendChild(text95);
			    Element child96= doc.createElement("NationalityTransportArrival");
			    child4.appendChild(child96);
			    Text text96= doc.createTextNode("");
			    child96.appendChild(text96);
			    Element child97= doc.createElement("TransportIDArrival");
			    child4.appendChild(child97);
			    Text text97= doc.createTextNode("");
			    child97.appendChild(text97);
			    Element child98= doc.createElement("OrderLines");
			    child4.appendChild(child98);
			    Text text98= doc.createTextNode("");
			    child98.appendChild(text98);
			    Element child99= doc.createElement("OrderLine");
			    child98.appendChild(child99);
			    Text text99= doc.createTextNode("");
			    child99.appendChild(text99);
			    Element child100= doc.createElement("OrderNumber");
			    child99.appendChild(child100);
			    Text text100= doc.createTextNode("");
			    child100.appendChild(text100);
			    Element child101= doc.createElement("LineNumber");
			    child99.appendChild(child101);
			    Text text101= doc.createTextNode("");
			    child101.appendChild(text101);
			    Element child102= doc.createElement("ArticleNumber");
			    child99.appendChild(child102);
			    Text text102= doc.createTextNode("");
			    child102.appendChild(text102);
			    Element child103= doc.createElement("ItemNumber");
			    child99.appendChild(child103);
			    Text text103= doc.createTextNode("");
			    child103.appendChild(text103);
			    Element child104= doc.createElement("OriginTerritory");
			    child99.appendChild(child104);
			    Text text104= doc.createTextNode("");
			    child104.appendChild(text104);
			    Element child105= doc.createElement("TarifNumber");
			    child99.appendChild(child105);
			    Text text105= doc.createTextNode("");
			    child105.appendChild(text105);
			    Element child106= doc.createElement("CountryOfOrigin");
			    child99.appendChild(child106);
			    Text text106= doc.createTextNode("");
			    child106.appendChild(text106);
			    Element child107= doc.createElement("CustomsCommodityDescription");
			    child99.appendChild(child107);
			    Text text107= doc.createTextNode("");
			    child107.appendChild(text107);
			    Element child108= doc.createElement("TarifNumber");
			    child99.appendChild(child108);
			    Text text108= doc.createTextNode("");
			    child108.appendChild(text108);
			    Element child109= doc.createElement("CustomsProcedureCode");
			    child99.appendChild(child109);
			    Text text109= doc.createTextNode("");
			    child109.appendChild(text109);
			    Element child110= doc.createElement("GoodsDescription1");
			    child99.appendChild(child110);
			    Text text110= doc.createTextNode("");
			    child110.appendChild(text110);
			    Element child111= doc.createElement("GrossWeight");
			    child99.appendChild(child111);
			    Text text111= doc.createTextNode("");
			    child111.appendChild(text111);
			    Element child112= doc.createElement("NetWeight");
			    child99.appendChild(child112);
			    Text text112= doc.createTextNode("");
			    child112.appendChild(text112);
			    Element child113= doc.createElement("Height");
			    child99.appendChild(child113);
			    Text text113= doc.createTextNode("");
			    child113.appendChild(text113);
			    Element child114= doc.createElement("Length");
			    child99.appendChild(child114);
			    Text text114= doc.createTextNode("");
			    child114.appendChild(text114);
			    Element child115= doc.createElement("Width");
			    child99.appendChild(child115);
			    Text text115= doc.createTextNode("");
			    child115.appendChild(text115);
			    Element child116= doc.createElement("Volume");
			    child99.appendChild(child116);
			    Text text116= doc.createTextNode("");
			    child116.appendChild(text116);
			    Element child117= doc.createElement("VolumeType");
			    child99.appendChild(child117);
			    Text text117= doc.createTextNode("");
			    child117.appendChild(text117);
			    Element child118= doc.createElement("ConfirmedQuantity");
			    child99.appendChild(child118);
			    Text text118= doc.createTextNode("");
			    child118.appendChild(text118);
			    Element child119= doc.createElement("DeliveryDate");
			    child99.appendChild(child119);
			    Text text119= doc.createTextNode("");
			    child119.appendChild(text119);
			    Element child120= doc.createElement("OrderedQuantity");
			    child99.appendChild(child120);
			    Text text120= doc.createTextNode("");
			    child120.appendChild(text120);
			    Element child121= doc.createElement("DeliveredQuantity");
			    child99.appendChild(child121);
			    Text text121= doc.createTextNode("");
			    child121.appendChild(text121);
			    Element child122= doc.createElement("PackageType");
			    child99.appendChild(child122);
			    Text text122= doc.createTextNode("");
			    child122.appendChild(text122);
			    Element child123= doc.createElement("PackageSize");
			    child99.appendChild(child123);
			    Text text123= doc.createTextNode("");
			    child123.appendChild(text123);
			    Element child124= doc.createElement("PackageID");
			    child99.appendChild(child124);
			    Text text124= doc.createTextNode("");
			    child124.appendChild(text124);
			    Element child125= doc.createElement("PackageWeight");
			    child99.appendChild(child125);
			    Text text125= doc.createTextNode("");
			    child125.appendChild(text125);
			    Element child126= doc.createElement("PackageVolume");
			    child99.appendChild(child126);
			    Text text126= doc.createTextNode("");
			    child126.appendChild(text126);
			    Element child127= doc.createElement("Currency");
			    child99.appendChild(child127);
			    Text text127= doc.createTextNode("");
			    child127.appendChild(text127);
			    Element child128= doc.createElement("ItemAmount");
			    child99.appendChild(child128);
			    Text text128= doc.createTextNode("");
			    child128.appendChild(text128);
			    Element child129= doc.createElement("TotalAmount");
			    child99.appendChild(child129);
			    Text text129= doc.createTextNode("");
			    child129.appendChild(text129);
			    Element child130= doc.createElement("NoOfPallets");
			    child99.appendChild(child130);
			    Text text130= doc.createTextNode("");
			    child130.appendChild(text130);
			    Element child131= doc.createElement("Status");
			    child99.appendChild(child131);
			    Text text131= doc.createTextNode("");
			    child131.appendChild(text131);
			    Element child132= doc.createElement("SupplementaryUnits");
			    child99.appendChild(child132);
			    Text text132= doc.createTextNode("");
			    child132.appendChild(text132);
			    Element child133= doc.createElement("ColliType");
			    child99.appendChild(child133);
			    Text text133= doc.createTextNode("");
			    child133.appendChild(text133);
			    Element child134= doc.createElement("Markings");
			    child99.appendChild(child134);
			    Text text134= doc.createTextNode("");
			    child134.appendChild(text134);
			    Element child135= doc.createElement("PackageLines");
			    child4.appendChild(child135);
			    Text text135= doc.createTextNode("");
			    child135.appendChild(text135);
			    Element child136= doc.createElement("Package");
			    child135.appendChild(child136);
			    Text text136= doc.createTextNode("");
			    child136.appendChild(text136);
			    Element child137= doc.createElement("LineNumber");
			    child136.appendChild(child137);
			    Text text137= doc.createTextNode("");
			    child137.appendChild(text137);
			    Element child138= doc.createElement("PackageID");
			    child136.appendChild(child138);
			    Text text138= doc.createTextNode("");
			    child138.appendChild(text138);
			    Element child140= doc.createElement("PackageType");
			    child136.appendChild(child140);
			    Text text140= doc.createTextNode("");
			    child140.appendChild(text140);
			    Element child141= doc.createElement("PackageGrossWeight");
			    child136.appendChild(child141);
			    Text text141= doc.createTextNode("");
			    child141.appendChild(text141);
			    Element child142= doc.createElement("PackageVolume");
			    child136.appendChild(child142);
			    Text text142= doc.createTextNode("");
			    child142.appendChild(text142);
			    Element child143= doc.createElement("Package");
			    child135.appendChild(child143);
			    Text text143= doc.createTextNode("");
			    child143.appendChild(text143);
			    Element child144= doc.createElement("LineNumber");
			    child143.appendChild(child144);
			    Text text144= doc.createTextNode("");
			    child144.appendChild(text144);
			    Element child145= doc.createElement("PackageID");
			    child143.appendChild(child145);
			    Text text145= doc.createTextNode("");
			    child145.appendChild(text145);
			    Element child146= doc.createElement("PackageType");
			    child143.appendChild(child146);
			    Text text146= doc.createTextNode("");
			    child146.appendChild(text146);
			    Element child147= doc.createElement("PackageGrossWeight");
			    child143.appendChild(child147);
			    Text text147= doc.createTextNode("");
			    child147.appendChild(text147);
			    Element child148= doc.createElement("PackageVolume");
			    child143.appendChild(child148);
			    Text text148= doc.createTextNode("");
			    child148.appendChild(text148);
			    
			}

			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer();
			transformer.setOutputProperty("indent", "yes");
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			String xmlString = sw.toString();
			List countryCode=trackingStatusManager.getCountryCode(fileNumber);
			String[] str1=countryCode.get(0).toString().split("~");
			String filePath=null;
			if(str1[0].equals("NOR")){
			filePath = "EXP_"+fileNumber+".xml";
			}
			else if(str1[1].equals("NOR")){
				filePath = "IMP_"+fileNumber+".xml";	
			}
			File file = new File(filePath);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
			bw.write(xmlString);
			bw.flush();
			bw.close();
			HttpServletResponse response = getResponse();
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
			ServletOutputStream out = response.getOutputStream();
			response.setContentType("application/zip");
			String fileName = filePath;
			response.setHeader("Content-Disposition", (new StringBuilder()).append("inline; filename=").append(fileName).toString());
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			out.write(xmlString.getBytes());
   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		}
}
	
	@SkipValidation
	public String userContacts() {
		try {
			userContactList = userManager.getAllUserContactListByPartnerCode(partnerCode, sessionCorpID);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String searchStatusBookingAgent(){
		try {
			userContactList = userManager.getAllUserContactListByPartnerCode(partnerCode, sessionCorpID,userName,userEmailId);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String linkUpNotes() {
		try {
			trackingStatus = trackingStatusManager.get(id);
			serviceOrder = serviceOrderManager.getForOtherCorpid(id);
			service = refMasterManager.findServiceByJobForlinkUpNotes("NotRLO", "TSFT", "SERVICE");
			notestatus = refMasterManager.findByParameter(sessionCorpID,"NOTESTATUS");
			notetype = refMasterManager.findByParameter(sessionCorpID, "NOTETYPE");
			notesubtype = refMasterManager.findByParameter(sessionCorpID,"NOTESUBTYPE");

			notes = new Notes();
			notes.setNoteStatus("NEW");
			notes.setCorpID(sessionCorpID);
			notes.setNoteType("Service Order");
			notes.setCreatedOn(new Date());
			notes.setSystemDate(new Date());
			notes.setUpdatedOn(new Date());
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setDisplayInvoice(false);
			notes.setDisplayQuote(false);
			List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
			if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
				checkAgent = defCheckedShipNumber.get(0).toString();
				if(checkAgent.equalsIgnoreCase("BA")){
					bookingAgentFlag ="BA";
				}else if(checkAgent.equalsIgnoreCase("NA")){
					networkAgentFlag ="NA";
				}else if(checkAgent.equalsIgnoreCase("OA")){
					originAgentFlag ="OA";
				}else if(checkAgent.equalsIgnoreCase("SOA")){
					subOriginAgentFlag ="SOA";
				}else if(checkAgent.equalsIgnoreCase("DA")){
					destAgentFlag ="DA";
				}else if(checkAgent.equalsIgnoreCase("SDA")){
					subDestAgentFlag ="SDA";
				} 
			  }
			linkUp = "";
			if(linkUpButtonType.equalsIgnoreCase("originSubAgentExSO")){
				fieldValue = "issuboriginagent";
			}
			if(linkUpButtonType.equalsIgnoreCase("originAgentExSO")){
				fieldValue = "isoriginagent";
			}
			if(linkUpButtonType.equalsIgnoreCase("destinationAgentExSO")){
				fieldValue = "isdestagent";
			}
			if(linkUpButtonType.equalsIgnoreCase("destinationSubAgentExSO")){
				fieldValue = "issubdestagent";
			}
			if(linkUpButtonType.equalsIgnoreCase("bookingAgentExSO")){
				fieldValue = "isbookingagent";
			}
			if(linkUpButtonType.equalsIgnoreCase("networkAgentExSO")){
				fieldValue = "isnetworkagent";
			}
			documentListCF = trackingStatusManager.findDocumentFromMyFileCF(seqNumber,fieldValue, sessionCorpID);	
			documentListSO = trackingStatusManager.findDocumentFromMyFileSO(seqNumber,shpNumber,fieldValue, sessionCorpID);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	@SkipValidation
	public String saveNoteLinkUp() {
		try {
			trackingStatus = trackingStatusManager.get(id);
			serviceOrder = serviceOrderManager.get(id);
			service = refMasterManager.findServiceByJobForlinkUpNotes("NotRLO", "TSFT", "SERVICE");
			notestatus = refMasterManager.findByParameter(sessionCorpID,"NOTESTATUS");
			notetype = refMasterManager.findByParameter(sessionCorpID, "NOTETYPE");
			notesubtype = refMasterManager.findByParameter(sessionCorpID,"NOTESUBTYPE");

			notes.setNoteGroup("Transaction");
			notes.setName(serviceOrder.getFirstName() + " "+ serviceOrder.getLastName());
			notes.setSystemDate(new Date());
			notes.setRemindTime("00:00");
			notes.setFollowUpFor(getRequest().getRemoteUser().toUpperCase());
			notes.setRemindInterval("15");
			notes = notesManager.save(notes);
			notes.setNetworkLinkId(notes.getCorpID() + notes.getId());
			notes = notesManager.save(notes);
			
			linkUp = "NOTES";
			notesSubType = notes.getNoteSubType();
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		
		return SUCCESS;
	}
	@SkipValidation
    public String addAndCopyAgentDetail(){
		try {
		if (id != null){  
			serviceOrder=serviceOrderManager.get(id);
		}
		soList = trackingStatusManager.getSOListbyCF(cid,id);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
    	return SUCCESS;
    }
	@SkipValidation
    public String copyAgentDetail(){
			try{
			copyAgentDetail=(TrackingStatus)trackingStatusManager.get(soGetId);
			ServiceOrder  serviceOrder=(ServiceOrder)serviceOrderManager.get(soGetId);
			TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(soSetId);
			ServiceOrder  serviceOrderRemote=(ServiceOrder)serviceOrderManager.get(soSetId);
			if(!(serviceOrder.getStatus().trim().equals("CLSD") || serviceOrder.getStatus().trim().equals("HOLD") || serviceOrder.getStatus().trim().equals("DWNLD")|| serviceOrder.getStatus().trim().equals("CNCL"))){
			if(!(serviceOrderRemote.getStatus().trim().equals("CLSD") || serviceOrderRemote.getStatus().trim().equals("HOLD") || serviceOrderRemote.getStatus().trim().equals("DWNLD")|| serviceOrderRemote.getStatus().trim().equals("CNCL"))){
			if(type!=null && !type.equals("") && type.equalsIgnoreCase("origin") ){
				  trackingStatusRemote.setOriginAgentCode(copyAgentDetail.getOriginAgentCode()==null?"":copyAgentDetail.getOriginAgentCode());
				  trackingStatusRemote.setOriginAgent(copyAgentDetail.getOriginAgent()==null?"":copyAgentDetail.getOriginAgent());
				  trackingStatusRemote.setOriginAgentEmail(copyAgentDetail.getOriginAgentEmail()==null?"":copyAgentDetail.getOriginAgentEmail());
				  trackingStatusRemote.setOriginAgentPhoneNumber(copyAgentDetail.getOriginAgentPhoneNumber()==null?"":copyAgentDetail.getOriginAgentPhoneNumber());
				  trackingStatusRemote.setOriginAgentContact(copyAgentDetail.getOriginAgentContact()==null?"":copyAgentDetail.getOriginAgentContact());
			}
			if(type!=null && !type.equals("") && type.equalsIgnoreCase("destination") ){
				trackingStatusRemote.setDestinationAgentCode(copyAgentDetail.getDestinationAgentCode()== null?"":copyAgentDetail.getDestinationAgentCode());
				  trackingStatusRemote.setDestinationAgent(copyAgentDetail.getDestinationAgent()==null?"":copyAgentDetail.getDestinationAgent());
				  trackingStatusRemote.setDestinationAgentEmail(copyAgentDetail.getDestinationAgentEmail()==null?"":copyAgentDetail.getDestinationAgentEmail());
				  trackingStatusRemote.setDestinationAgentPhoneNumber(copyAgentDetail.getDestinationAgentPhoneNumber()==null?"":copyAgentDetail.getDestinationAgentPhoneNumber());
				  trackingStatusRemote.setDestinationAgentContact(copyAgentDetail.getDestinationAgentContact()==null?"":copyAgentDetail.getDestinationAgentContact());
			}
			}
			}
			  trackingStatusRemote.setUpdatedBy(getRequest().getRemoteUser());
			  trackingStatusRemote.setUpdatedOn(new Date());
			  trackingStatusManager.save(trackingStatusRemote);
			hitFlag="1";
			}catch (Exception e) {
				e.printStackTrace();
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 return "errorlog";
			}
		   	return SUCCESS;
    }
	@SkipValidation	
	 public String updateAgentDetails(){
	try{
		Set <Long> str=new HashSet<Long>();
		str.add(soId);
		Map<String,Object> order=serviceOrderManager.getObjectMap(str);
		TrackingStatus trackingStatus = (TrackingStatus)order.get(soId+":TrackingStatus"); 
		ServiceOrder  serviceOrder = (ServiceOrder)order.get(soId+":ServiceOrder");
		if(!(serviceOrder.getStatus().trim().equals("CLSD") || serviceOrder.getStatus().trim().equals("HOLD") || serviceOrder.getStatus().trim().equals("DWNLD")|| serviceOrder.getStatus().trim().equals("CNCL"))){
			List serviceOrderList=customerFileManager.getServiceOrderListRemote(Long.parseLong(seqNm));
			 if(serviceOrderList!=null && (!(serviceOrderList.isEmpty()))&&(serviceOrderList.get(0)!=null)){
				 Iterator iterator=serviceOrderList.iterator();
				  while (iterator.hasNext()) {					  
					  String sid= iterator.next().toString();
					  if(!(sid.equals(soId))){
					  TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(Long.parseLong(sid));
					  ServiceOrder serviceOrderRemote=serviceOrderManager.get(Long.parseLong(sid));
					  if(!(serviceOrderRemote.getStatus().trim().equals("CLSD") || serviceOrderRemote.getStatus().trim().equals("HOLD") || serviceOrderRemote.getStatus().trim().equals("DWNLD")|| serviceOrderRemote.getStatus().trim().equals("CNCL"))){ 
					  String tempNAgExso=trackingStatusRemote.getNetworkAgentExSO()==null?"" : trackingStatusRemote.getNetworkAgentExSO();
					  String tempOAgExso=trackingStatusRemote.getOriginAgentExSO()==null?"" : trackingStatusRemote.getOriginAgentExSO();
					  String tempDAgExso=trackingStatusRemote.getDestinationAgentExSO()==null?"" : trackingStatusRemote.getDestinationAgentExSO();
					  String tempSOAgExso=trackingStatusRemote.getOriginSubAgentExSO()==null?"" : trackingStatusRemote.getOriginSubAgentExSO();
					  String tempSDAgExso=trackingStatusRemote.getDestinationSubAgentExSO()==null?"" : trackingStatusRemote.getDestinationSubAgentExSO();
					  if(tempNAgExso.equals("")){
					  trackingStatusRemote.setNetworkPartnerCode(trackingStatus.getNetworkPartnerCode()== null?"":trackingStatus.getNetworkPartnerCode());
					  trackingStatusRemote.setNetworkPartnerName(trackingStatus.getNetworkPartnerName()==null?"":trackingStatus.getNetworkPartnerName());
					  trackingStatusRemote.setNetworkEmail(trackingStatus.getNetworkEmail()==null?"":trackingStatus.getNetworkEmail());
					  trackingStatusRemote.setNetworkPhoneNumber(trackingStatus.getNetworkPhoneNumber()==null?"":trackingStatus.getNetworkPhoneNumber());
					  trackingStatusRemote.setNetworkContact(trackingStatus.getNetworkContact()==null?"":trackingStatus.getNetworkContact());
					  }
					  if(tempOAgExso.equals("")){
					  trackingStatusRemote.setOriginAgentCode(trackingStatus.getOriginAgentCode()==null?"":trackingStatus.getOriginAgentCode());
					  trackingStatusRemote.setOriginAgent(trackingStatus.getOriginAgent()==null?"":trackingStatus.getOriginAgent());
					  trackingStatusRemote.setOriginAgentEmail(trackingStatus.getOriginAgentEmail()==null?"":trackingStatus.getOriginAgentEmail());
					  trackingStatusRemote.setOriginAgentPhoneNumber(trackingStatus.getOriginAgentPhoneNumber()==null?"":trackingStatus.getOriginAgentPhoneNumber());
					  trackingStatusRemote.setOriginAgentContact(trackingStatus.getOriginAgentContact()==null?"":trackingStatus.getOriginAgentContact());
					  }
					  if(tempDAgExso.equals("")){
					  trackingStatusRemote.setDestinationAgentCode(trackingStatus.getDestinationAgentCode()== null?"":trackingStatus.getDestinationAgentCode());
					  trackingStatusRemote.setDestinationAgent(trackingStatus.getDestinationAgent()==null?"":trackingStatus.getDestinationAgent());
					  trackingStatusRemote.setDestinationAgentEmail(trackingStatus.getDestinationAgentEmail()==null?"":trackingStatus.getDestinationAgentEmail());
					  trackingStatusRemote.setDestinationAgentPhoneNumber(trackingStatus.getDestinationAgentPhoneNumber()==null?"":trackingStatus.getDestinationAgentPhoneNumber());
					  trackingStatusRemote.setDestinationAgentContact(trackingStatus.getDestinationAgentContact()==null?"":trackingStatus.getDestinationAgentContact());
					  }
					  if(tempSOAgExso.equals("")){
					  trackingStatusRemote.setOriginSubAgentCode(trackingStatus.getOriginSubAgentCode()== null?"":trackingStatus.getOriginSubAgentCode());
					  trackingStatusRemote.setOriginSubAgent(trackingStatus.getOriginSubAgent()==null?"":trackingStatus.getOriginSubAgent());
					  trackingStatusRemote.setSubOriginAgentEmail(trackingStatus.getSubOriginAgentEmail()==null?"":trackingStatus.getSubOriginAgentEmail());
					  trackingStatusRemote.setSubOriginAgentPhoneNumber(trackingStatus.getSubOriginAgentPhoneNumber()==null?"":trackingStatus.getSubOriginAgentPhoneNumber());
					  trackingStatusRemote.setSubOriginAgentContact(trackingStatus.getSubOriginAgentContact()==null?"":trackingStatus.getSubOriginAgentContact());
					  }
					  if(tempSDAgExso.equals("")){
					  trackingStatusRemote.setDestinationSubAgentCode(trackingStatus.getDestinationSubAgentCode()== null?"":trackingStatus.getDestinationSubAgentCode());
					  trackingStatusRemote.setDestinationSubAgent(trackingStatus.getDestinationSubAgent()==null?"":trackingStatus.getDestinationSubAgent());
					  trackingStatusRemote.setSubDestinationAgentEmail(trackingStatus.getSubDestinationAgentEmail()==null?"":trackingStatus.getSubDestinationAgentEmail());
					  trackingStatusRemote.setSubDestinationAgentPhoneNumber(trackingStatus.getSubDestinationAgentPhoneNumber()==null?"":trackingStatus.getSubDestinationAgentPhoneNumber());
					  trackingStatusRemote.setSubDestinationAgentAgentContact(trackingStatus.getSubDestinationAgentAgentContact()==null?"":trackingStatus.getSubDestinationAgentAgentContact());
					  }
					  trackingStatusRemote.setBrokerCode(trackingStatus.getBrokerCode()== null?"":trackingStatus.getBrokerCode());
					  trackingStatusRemote.setBrokerName(trackingStatus.getBrokerName()==null?"":trackingStatus.getBrokerName());
					  trackingStatusRemote.setBrokerContact(trackingStatus.getBrokerContact()==null?"":trackingStatus.getBrokerContact());
					  trackingStatusRemote.setBrokerEmail(trackingStatus.getBrokerEmail()==null?"":trackingStatus.getBrokerEmail());
					  trackingStatusRemote.setForwarderCode(trackingStatus.getForwarderCode()== null?"":trackingStatus.getForwarderCode());
					  trackingStatusRemote.setForwarder(trackingStatus.getForwarder()==null?"":trackingStatus.getForwarder());
					  trackingStatusRemote.setForwarderContact(trackingStatus.getForwarderContact()==null?"":trackingStatus.getForwarderContact());
					  trackingStatusRemote.setForwarderEmail(trackingStatus.getForwarderEmail()==null?"":trackingStatus.getForwarderEmail());
					  trackingStatusRemote.setUpdatedBy(getRequest().getRemoteUser());
					  trackingStatusRemote.setUpdatedOn(new Date());
					  trackingStatusManager.save(trackingStatusRemote);
				  }
				  }
				  }
			 }
		}
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return "errorlog";
		}
		 return SUCCESS;
	 }
	
	@SkipValidation
	  public String getEmailFromPortalUser(){   
		  try {
			if(partnerCode!=null){
			    	accountLineList=trackingStatusManager.getEmailFromPortalUser(partnerCode,sessionCorpID);
			}else{
				String message="There is something wrong.Please try Again.";
		    	errorMessage(getText(message));
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return CANCEL;
		}
			return SUCCESS; 
	  }
	
	@SkipValidation
	public String findRddDaysFromPartnerAjax(){
		returnAjaxStringValue = trackingStatusManager.getRddDaysFromPartner(fieldName,soBillToCode,sessionCorpID);
		return SUCCESS;
	}
	
	
	@SkipValidation	
	 public String checkNetworkCoordinatorInSo()
	{
		returnAjaxStringValue=trackingStatusManager.checkNetworkCoordinatorInUser(cfCoordinatorname,soCoordinatorname,sessionCorpID);
		return SUCCESS;
	}
	
	@SkipValidation
	  public String validatePreferredAgentCode(){   
		  try {
			if(partnerCode!=null){
				preferredList=trackingStatusManager.validatePreferredAgentCode(partnerCode,sessionCorpID,billToCodeAcc);
			}else{
			    	String message="The Vendor Name is not exist";
			    	errorMessage(getText(message));
			}
		} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 return CANCEL;
		}
			return SUCCESS; 
	  }
	
	public String getContactInfo()
	{    defaultContactInfo="";
		     if(originAgentInfo!=null){
			 defaultContactInfo= trackingStatusManager.loadInfoByCode(originAgentInfo,sessionCorpID);
		 }
		return SUCCESS;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
		}
	public void setAddressManager(AddressManager addressManager) {
		this.addressManager = addressManager;
	}
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}
	public void setStorageManager(StorageManager storageManager) {
		this.storageManager = storageManager;
	}
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}
	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}
	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	} 
	private void postSave(){
    	toDoRuleManager.executeRules("serviceorder", trackingStatus.getId(), sessionCorpID);
    }
    public String getShipNumberA() {
		return shipNumberA;
	}
	public void setShipNumberA(String shipNumberA) {
		this.shipNumberA = shipNumberA;
	}
	public List getAgentAddress() {
		return agentAddress;
	}
	public void setAgentAddress(List agentAddress) {
		this.agentAddress = agentAddress;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	
	public String getBillingAddress1() {
		return billingAddress1;
	}
	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}
	public String getBillingAddress2() {
		return billingAddress2;
	}
	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}
	public String getBillingAddress3() {
		return billingAddress3;
	}
	public void setBillingAddress3(String billingAddress3) {
		this.billingAddress3 = billingAddress3;
	}
	public String getBillingAddress4() {
		return billingAddress4;
	}
	public void setBillingAddress4(String billingAddress4) {
		this.billingAddress4 = billingAddress4;
	}
	public String getBillingCity() {
		return billingCity;
	}
	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}
	public String getBillingCountry() {
		return billingCountry;
	}
	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}
	public String getBillingEmail() {
		return billingEmail;
	}
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}
	public String getBillingFax() {
		return billingFax;
	}
	public void setBillingFax(String billingFax) {
		this.billingFax = billingFax;
	}
	public String getBillingPhone() {
		return billingPhone;
	}
	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}
	public String getBillingState() {
		return billingState;
	}
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}
	public String getBillingTelex() {
		return billingTelex;
	}
	public void setBillingTelex(String billingTelex) {
		this.billingTelex = billingTelex;
	}
	public String getBillingZip() {
		return billingZip;
	}
	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}  
    public String getBooker() {
		return booker;
	}
	public void setBooker(String booker) {
		this.booker = booker;
	}
	public List getBookerList() {
		return bookerList;
	}
	public void setBookerList(List bookerList) {
		this.bookerList = bookerList;
	}
	public String getCountDestinationStatusNotes() {
		return countDestinationStatusNotes;
	}
	public void setCountDestinationStatusNotes(String countDestinationStatusNotes) {
		this.countDestinationStatusNotes = countDestinationStatusNotes;
	}
	public String getCountSitDestinationNotes() {
		return countSitDestinationNotes;
	}
	public void setCountSitDestinationNotes(String countSitDestinationNotes) {
		this.countSitDestinationNotes = countSitDestinationNotes;
	}
	public String getCountStorage() {
		return countStorage;
	}
	public void setCountStorage(String countStorage) {
		this.countStorage = countStorage;
	}
	public String getDestinationAgent() {
		return destinationAgent;
	}
	public void setDestinationAgent(String destinationAgent) {
		this.destinationAgent = destinationAgent;
	}
	public List getDestinationAgentList() {
		return destinationAgentList;
	}
	public void setDestinationAgentList(List destinationAgentList) {
		this.destinationAgentList = destinationAgentList;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}
	public String getOriginAgent() {
		return originAgent;
	}
	public void setOriginAgent(String originAgent) {
		this.originAgent = originAgent;
	}
	public List getOriginAgentList() {
		return originAgentList;
	}
	public void setOriginAgentList(List originAgentList) {
		this.originAgentList = originAgentList;
	}
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public String getCountSurveyNotes() {
		return countSurveyNotes;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public ServicePartner getServicePartner() {
		return servicePartner;
	}
	public void setServicePartner(ServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}
	public Set getServicePartners() {
		return servicePartners;
	}
	public void setServicePartners(Set servicePartners) {
		this.servicePartners = servicePartners;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public Set getStorages() {
		return storages;
	}
	public void setStorages(Set storages) {
		this.storages = storages;
	}
	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public List getTrackingStatuss() {
		return trackingStatuss;
	}
	public void setTrackingStatuss(List trackingStatuss) {
		this.trackingStatuss = trackingStatuss;
	}
	public WorkTicket getWorkTicket() {
		return workTicket;
	}
	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}
	public List getWorkTickets() {
		return workTickets;
	}
	public void setWorkTickets(List workTickets) {
		this.workTickets = workTickets;
	}
	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}
	public String getGotoPageString() {
	return gotoPageString;
	}
	public void setGotoPageString(String gotoPageString) {
	this.gotoPageString = gotoPageString;
	}
	public List getRefMasters(){
	return refMasters;
	}
	public  List getWeightunits() {
	return weightunits;
	}
	public  List getVolumeunits() {
	return volumeunits;
	}
    public  Map<String, String> getYesno() {
		return yesno;
	}
    public  Map<String, String> getTan() {
		return tan;
	}
    public  Map<String, String> getOmni() {
		return omni;
	}
    public  Map<String, String> getDemurrag() {
		return demurrag;
	}
    public  Map<String, String> getNontemp() {
		return nontemp;
	}
    public  Map<String, String> getClearat() {
		return clearat;
	}
    public  Map<String, String> getPartnerType() {
		return partnerType;
	}
    public  Map<String, String> getSITOUTTA() {
		return SITOUTTA;
	}
    public  Map<String, String> getENTRYFORM() {
		return ENTRYFORM;
	}
    public CustomerFile getCustomerFile() {
		return customerFile;
	}
    public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public String getCountDestinationDetailNotes() {
		return countDestinationDetailNotes;
	}
	public String getCountOriginDetailNotes() {
		return countOriginDetailNotes;
	}
	public String getCountWeightDetailNotes() {
		return countWeightDetailNotes;
	}
	public String getCountOriginPackoutNotes() {
		return countOriginPackoutNotes;
	}
	public void setCountOriginPackoutNotes(String countOriginPackoutNotes) {
		this.countOriginPackoutNotes = countOriginPackoutNotes;
	}
	public void setCountDestinationDetailNotes(String countDestinationDetailNotes) {
		this.countDestinationDetailNotes = countDestinationDetailNotes;
	}
	public void setCountOriginDetailNotes(String countOriginDetailNotes) {
		this.countOriginDetailNotes = countOriginDetailNotes;
	}
	public void setCountSurveyNotes(String countSurveyNotes) {
		this.countSurveyNotes = countSurveyNotes;
	}
	public void setCountWeightDetailNotes(String countWeightDetailNotes) {
		this.countWeightDetailNotes = countWeightDetailNotes;
	}
	public String getCountSitOriginNotes() {
		return countSitOriginNotes;
	}
	public void setCountSitOriginNotes(String countSitOriginNotes) {
		this.countSitOriginNotes = countSitOriginNotes;
	}
	public String getCountOriginForwardingNotes() {
		return countOriginForwardingNotes;
	}
	public void setCountOriginForwardingNotes(String countOriginForwardingNotes) {
		this.countOriginForwardingNotes = countOriginForwardingNotes;
	}
	public String getCountGSTForwardingNotes() {
		return countGSTForwardingNotes;
	}
	public void setCountGSTForwardingNotes(String countGSTForwardingNotes) {
		this.countGSTForwardingNotes = countGSTForwardingNotes;
	}
	public String getCountDestinationImportNotes() {
		return countDestinationImportNotes;
	}
	public void setCountDestinationImportNotes(String countDestinationImportNotes) {
		this.countDestinationImportNotes = countDestinationImportNotes;
	}
	public String getCountInterStateNotes() {
		return countInterStateNotes;
	}
	public void setCountInterStateNotes(String countInterStateNotes) {
		this.countInterStateNotes = countInterStateNotes;
	}
	public AccountLine getAccountLine() {
		return accountLine;
	}
	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}
	public Set getAccountLines() {
		return accountLines;
	}
	public void setAccountLines(Set accountLines) {
		this.accountLines = accountLines;
	}
	public String getDateStatus() {
		return dateStatus;
	}
	public void setDateStatus(String dateStatus) {
		this.dateStatus = dateStatus;
	}
	public Date getEtDepart() {
		return etDepart;
	}
	public void setEtDepart(Date etDepart) {
		this.etDepart = etDepart;
	}
	public List getServicePartnerlist() {
		return servicePartnerlist;
	}
	public void setServicePartnerlist(List servicePartnerlist) {
		this.servicePartnerlist = servicePartnerlist;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public Long getMaxMisc() {
		return maxMisc;
	}
	public void setMaxMisc(Long maxMisc) {
		this.maxMisc = maxMisc;
	}
	public Long getMaxTrac() {
		return maxTrac;
	}
	public void setMaxTrac(Long maxTrac) {
		this.maxTrac = maxTrac;
	}
	public AccountLine getAccountLine1() {
		return accountLine1;
	}
	public void setAccountLine1(AccountLine accountLine1) {
		this.accountLine1 = accountLine1;
	}
	public String getAccountLineNumber() {
		return accountLineNumber;
	}
	public void setAccountLineNumber(String accountLineNumber) {
		this.accountLineNumber = accountLineNumber;
	}
	public List getMaxLineNumber() {
		return maxLineNumber;
	}
	public void setMaxLineNumber(List maxLineNumber) {
		this.maxLineNumber = maxLineNumber;
	}
	public Long getAutoLineNumber() {
		return autoLineNumber;
	}
	public void setAutoLineNumber(Long autoLineNumber) {
		this.autoLineNumber = autoLineNumber;
	}
	public List getDestinationAgentCodeList() {
		return destinationAgentCodeList;
	}
	public void setDestinationAgentCodeList(List destinationAgentCodeList) {
		this.destinationAgentCodeList = destinationAgentCodeList;
	}
	public List getOriginAgentCodeList() {
		return originAgentCodeList;
	}
	public void setOriginAgentCodeList(List originAgentCodeList) {
		this.originAgentCodeList = originAgentCodeList;
	}
	public String getAgentStatusDestination() {
		return agentStatusDestination;
	}
	public void setAgentStatusDestination(String agentStatusDestination) {
		this.agentStatusDestination = agentStatusDestination;
	}
	public String getAgentStatusOrigin() {
		return agentStatusOrigin;
	}
	public void setAgentStatusOrigin(String agentStatusOrigin) {
		this.agentStatusOrigin = agentStatusOrigin;
	}
	public String getCountServiceOrderNotes() {
		return countServiceOrderNotes;
	}
	public void setCountServiceOrderNotes(String countServiceOrderNotes) {
		this.countServiceOrderNotes = countServiceOrderNotes;
	}
	public List getDestinationAgentCodeListTrakingStatus() {
		return destinationAgentCodeListTrakingStatus;
	}
	public void setDestinationAgentCodeListTrakingStatus(List destinationAgentCodeListTrakingStatus) {
		this.destinationAgentCodeListTrakingStatus = destinationAgentCodeListTrakingStatus;
	}
	public List getDestinationAgentListTrakingStatus() {
		return destinationAgentListTrakingStatus;
	}
	public void setDestinationAgentListTrakingStatus(List destinationAgentListTrakingStatus) {
		this.destinationAgentListTrakingStatus = destinationAgentListTrakingStatus;
	}
	public List getOriginAgentCodeListTrakingStatus() {
		return originAgentCodeListTrakingStatus;
	}
	public void setOriginAgentCodeListTrakingStatus(List originAgentCodeListTrakingStatus) {
		this.originAgentCodeListTrakingStatus = originAgentCodeListTrakingStatus;
	}
	public List getOriginAgentListTrakingStatus() {
		return originAgentListTrakingStatus;
	}
	public void setOriginAgentListTrakingStatus(List originAgentListTrakingStatus) {
		this.originAgentListTrakingStatus = originAgentListTrakingStatus;
	}
	public ToDoRuleManager getToDoRuleManager() {
		return toDoRuleManager;
	}
	public  String getAccountInterface() {
		return accountInterface;
	}
	public  void setAccountInterface(String accountInterface) {
		this.accountInterface = accountInterface;
	}

	/**
	 * @return the billing
	 */
	public Billing getBilling() {
		return billing;
	}

	/**
	 * @param billing the billing to set
	 */
	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	/**
	 * @param billingManager the billingManager to set
	 */
	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the eQUIP
	 */
	public Map<String, String> getEQUIP() {
		
		if(miscellaneous !=null && miscellaneous.getId()!=null && miscellaneous.getEquipment()!=null && !miscellaneous.getEquipment().equals(""))
		{
		if(!EQUIP.containsValue(miscellaneous.getEquipment()) && !EQUIP.containsKey(miscellaneous.getEquipment()))
		{EQUIP.put(miscellaneous.getEquipment(), miscellaneous.getEquipment());
		}
		}
		return EQUIP;
	}
	/**
	 * @return the findSalesStatus
	 */
	public List getFindSalesStatus() {
		return findSalesStatus;
	}
	/**
	 * @param findSalesStatus the findSalesStatus to set
	 */
	public void setFindSalesStatus(List findSalesStatus) {
		this.findSalesStatus = findSalesStatus;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public Boolean getAccessAllowed() {
		return accessAllowed;
	}
	public void setAccessAllowed(Boolean accessAllowed) {
		this.accessAllowed = accessAllowed;
	}
	public String getValidateFormNav() {
		return validateFormNav;
	}
	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public String getShipSize() {
		return shipSize;
	}
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}
	public String getShipNm() {
		return shipNm;
	}
	public void setShipNm(String shipNm) {
		this.shipNm = shipNm;
	}
	public long getTempID() {
		return tempID;
	}
	public void setTempID(long tempID) {
		this.tempID = tempID;
	}
	public List getCustomerSO() {
		return customerSO;
	}
	public void setCustomerSO(List customerSO) {
		this.customerSO = customerSO;
	}
	public String getCountShip() {
		return countShip;
	}
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}
	public String getMinShip() {
		return minShip;
	}
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}
	/**
	 * @return the weightType
	 */
	public String getWeightType() {
		return weightType;
	}
	/**
	 * @param weightType the weightType to set
	 */
	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}
	public List getVanLineCodeList() {
		return vanLineCodeList;
	}
	public void setVanLineCodeList(List vanLineCodeList) {
		this.vanLineCodeList = vanLineCodeList;
	}
	public String getAgentType() {
		return agentType;
	}
	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}
	public String getAgentCodeName() {
		return agentCodeName;
	}
	public void setAgentCodeName(String agentCodeName) {
		this.agentCodeName = agentCodeName;
	}
	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}
	public List getCompanyCodeList() {
		return companyCodeList;
	}
	public void setCompanyCodeList(List companyCodeList) {
		this.companyCodeList = companyCodeList;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	/**
	 * @return the servicePartnerss
	 */
	public List getServicePartnerss() {
		return servicePartnerss;
	}
	/**
	 * @param servicePartnerss the servicePartnerss to set
	 */
	public void setServicePartnerss(List servicePartnerss) {
		this.servicePartnerss = servicePartnerss;
	}
	/**
	 * @return the military
	 */
	public Map<String, String> getMilitary() {
		return military;
	}
	/**
	 * @param military the military to set
	 */
	public void setMilitary(Map<String, String> military) {
		this.military = military;
	}
	public List getAccountLineList() {
		return accountLineList;
	}
	public void setAccountLineList(List accountLineList) {
		this.accountLineList = accountLineList;
	}
	public List getStatusNumberCountList() {
		return statusNumberCountList;
	}
	public void setStatusNumberCountList(List statusNumberCountList) {
		this.statusNumberCountList = statusNumberCountList;
	}
	public List getStatusNumberList() {
		return statusNumberList;
	}
	public void setStatusNumberList(List statusNumberList) {
		this.statusNumberList = statusNumberList;
	}
	public List getJobstatusNumberList() {
		return jobstatusNumberList;
	}
	public void setJobstatusNumberList(List jobstatusNumberList) {
		this.jobstatusNumberList = jobstatusNumberList;
	}
	public Map<String, String> getSitDestinationReason() {
		return sitDestinationReason;
	}
	public void setSitDestinationReason(Map<String, String> sitDestinationReason) {
		this.sitDestinationReason = sitDestinationReason;
	}
	public Map<String, String> getSitOriginReason() {
		return sitOriginReason;
	}
	public void setSitOriginReason(Map<String, String> sitOriginReason) {
		this.sitOriginReason = sitOriginReason;
	}
	public Map<String, String> getFlagCarrierList() {
		
		if(trackingStatus !=null && trackingStatus.getId()!=null && trackingStatus.getFlagCarrier()!=null && !trackingStatus.getFlagCarrier().equals(""))
		{
		if(!flagCarrierList.containsValue(trackingStatus.getFlagCarrier()) &&  !flagCarrierList.containsKey(trackingStatus.getFlagCarrier()))
		{flagCarrierList.put(trackingStatus.getFlagCarrier(), trackingStatus.getFlagCarrier());
		}
		}
		return flagCarrierList;
	}
	public void setFlagCarrierList(Map<String, String> flagCarrierList) {
		this.flagCarrierList = flagCarrierList;
	}
	public List getBookingAgentList() {
		return bookingAgentList;
	}
	public void setBookingAgentList(List bookingAgentList) {
		this.bookingAgentList = bookingAgentList;
	}
	public String getBookingAgentCode() {
		return bookingAgentCode;
	}
	public void setBookingAgentCode(String bookingAgentCode) {
		this.bookingAgentCode = bookingAgentCode;
	}
	public Boolean getIsNetworkBookingAgent() {
		return isNetworkBookingAgent;
	}
	public void setIsNetworkBookingAgent(Boolean isNetworkBookingAgent) {
		this.isNetworkBookingAgent = isNetworkBookingAgent;
	}
	public Boolean getIsNetworkDestinationAgent() {
		return isNetworkDestinationAgent;
	}
	public void setIsNetworkDestinationAgent(Boolean isNetworkDestinationAgent) {
		this.isNetworkDestinationAgent = isNetworkDestinationAgent;
	}
	
	public String getRespectiveNetworkAgent() {
		return respectiveNetworkAgent;
	}
	public void setRespectiveNetworkAgent(String respectiveNetworkAgent) {
		this.respectiveNetworkAgent = respectiveNetworkAgent;
	}
	public String getNetworkPartnerCode() {
		return networkPartnerCode;
	}
	public void setNetworkPartnerCode(String networkPartnerCode) {
		this.networkPartnerCode = networkPartnerCode;
	}
	public String getButtonType() {
		return buttonType;
	}
	public void setContainerManager(ContainerManager containerManager) {
		this.containerManager = containerManager;
	}
	public void setCartonManager(CartonManager cartonManager) {
		this.cartonManager = cartonManager;
	}
	public void setVehicleManager(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
	}
	public void setConsigneeInstructionManager(
			ConsigneeInstructionManager consigneeInstructionManager) {
		this.consigneeInstructionManager = consigneeInstructionManager;
	}
	public Boolean getIsNetworkOrginAgentSection() {
		return isNetworkOrginAgentSection;
	}
	public void setIsNetworkOrginAgentSection(Boolean isNetworkOrginAgentSection) {
		this.isNetworkOrginAgentSection = isNetworkOrginAgentSection;
	}
	public Boolean getIsNetworkDestinationAgentSection() {
		return isNetworkDestinationAgentSection;
	}
	public void setIsNetworkDestinationAgentSection(
			Boolean isNetworkDestinationAgentSection) {
		this.isNetworkDestinationAgentSection = isNetworkDestinationAgentSection;
	}
	public List getCustoms() {
		return customs;
	}
	public void setCustoms(List customs) {
		this.customs = customs;
	}
	public void setCustomManager(CustomManager customManager) {
		this.customManager = customManager;
	}
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
		
	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}
	
	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}
	public Boolean getIsNetworkOriginAgent() {
		return isNetworkOriginAgent;
	}
	public void setIsNetworkOriginAgent(Boolean isNetworkOriginAgent) {
		this.isNetworkOriginAgent = isNetworkOriginAgent;
	}
	public Boolean getIsNetwork() {
		return isNetwork;
	}
	public void setIsNetwork(Boolean isNetwork) {
		this.isNetwork = isNetwork;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public Date getPackA() {
		return packA;
	}
	public void setPackA(Date packA) {
		this.packA = packA;
	}
	public Date getDeliveryA() {
		return deliveryA;
	}
	public void setDeliveryA(Date deliveryA) {
		this.deliveryA = deliveryA;
	}
	public Date getLoadA() {
		return loadA;
	}
	public void setLoadA(Date loadA) {
		this.loadA = loadA;
	}
	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getCorpIDform() {
		return corpIDform;
	}
	public void setCorpIDform(String corpIDform) {
		this.corpIDform = corpIDform;
	}
	public Boolean getIsNetworkNetworkPartnerCode() {
		return isNetworkNetworkPartnerCode;
	}
	public void setIsNetworkNetworkPartnerCode(Boolean isNetworkNetworkPartnerCode) {
		this.isNetworkNetworkPartnerCode = isNetworkNetworkPartnerCode;
	}
	public Boolean getIsNetworkNetworkPartnerCodeSection() {
		return isNetworkNetworkPartnerCodeSection;
	}
	public void setIsNetworkNetworkPartnerCodeSection(
			Boolean isNetworkNetworkPartnerCodeSection) {
		this.isNetworkNetworkPartnerCodeSection = isNetworkNetworkPartnerCodeSection;
	}
	public boolean isBillingContractType() {
		return billingContractType;
	}
	public void setBillingContractType(boolean billingContractType) {
		this.billingContractType = billingContractType;
	}
	public ServiceOrder getServiceOrderToRecods() {
		return serviceOrderToRecods;
	}
	public void setServiceOrderToRecods(ServiceOrder serviceOrderToRecods) {
		this.serviceOrderToRecods = serviceOrderToRecods;
	} 
	public Billing getBillingRecods() {
		return billingRecods;
	}
	public void setBillingRecods(Billing billingRecods) {
		this.billingRecods = billingRecods;
	}
	public TrackingStatus getTrackingStatusToRecod() {
		return trackingStatusToRecod;
	}
	public void setTrackingStatusToRecod(TrackingStatus trackingStatusToRecod) {
		this.trackingStatusToRecod = trackingStatusToRecod;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
		this.exchangeRateManager = exchangeRateManager;
	}
	public List getUserContactList() {
		return userContactList;
	}
	public void setUserContactList(List userContactList) {
		this.userContactList = userContactList;
	}
	public UserManager getUserManager() {
		return userManager;
	}
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	public DataSecuritySetManager getDataSecuritySetManager() {
		return dataSecuritySetManager;
	}
	public void setDataSecuritySetManager(
			DataSecuritySetManager dataSecuritySetManager) {
		this.dataSecuritySetManager = dataSecuritySetManager;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public boolean isNetworkAgent() {
		return networkAgent;
	}
	public void setNetworkAgent(boolean networkAgent) {
		this.networkAgent = networkAgent;
	}
	public Boolean getIsNetworkBookingAgentCodeSection() {
		return isNetworkBookingAgentCodeSection;
	}
	public void setIsNetworkBookingAgentCodeSection(
			Boolean isNetworkBookingAgentCodeSection) {
		this.isNetworkBookingAgentCodeSection = isNetworkBookingAgentCodeSection;
	}
	public Boolean getIsNetworkBookingAgentCode() {
		return isNetworkBookingAgentCode;
	}
	public void setIsNetworkBookingAgentCode(Boolean isNetworkBookingAgentCode) {
		this.isNetworkBookingAgentCode = isNetworkBookingAgentCode;
	}
	public void setAdAddressesDetailsManager(
			AdAddressesDetailsManager adAddressesDetailsManager) {
		this.adAddressesDetailsManager = adAddressesDetailsManager;
	}
	public AdAddressesDetails getAdAddressesDetails() {
		return adAddressesDetails;
	}
	public void setAdAddressesDetails(AdAddressesDetails adAddressesDetails) {
		this.adAddressesDetails = adAddressesDetails;
	}
	public void setDsFamilyDetailsManager(
			DsFamilyDetailsManager dsFamilyDetailsManager) {
		this.dsFamilyDetailsManager = dsFamilyDetailsManager;
	}
	public DsFamilyDetails getDsFamilyDetails() {
		return dsFamilyDetails;
	}
	public void setDsFamilyDetails(DsFamilyDetails dsFamilyDetails) {
		this.dsFamilyDetails = dsFamilyDetails;
	}
	public Boolean getCostElementFlag() {
		return costElementFlag;
	}
	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}	
	public String getParentShipNumber() {
		return parentShipNumber;
	}
	public void setParentShipNumber(String parentShipNumber) {
		this.parentShipNumber = parentShipNumber;
	}
	public String getDisableChk() {
		return disableChk;
	}
	public void setDisableChk(String disableChk) {
		this.disableChk = disableChk;
	}
	public void setRemovalRelocationServiceManager(
			RemovalRelocationServiceManager removalRelocationServiceManager) {
		this.removalRelocationServiceManager = removalRelocationServiceManager;
	}
	public RemovalRelocationService getRemovalRelocationService() {
		return removalRelocationService;
	}
	public void setRemovalRelocationService(
			RemovalRelocationService removalRelocationService) {
		this.removalRelocationService = removalRelocationService;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserEmailId() {
		return userEmailId;
	}
	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}
	
	public void setCustomerFileAction(CustomerFileAction customerFileAction) {
		this.customerFileAction = customerFileAction;
	}
	public void setNetworkControlManager(NetworkControlManager networkControlManager) {
		this.networkControlManager = networkControlManager;
	}
	public void setIntegrationLogInfoManager(
			IntegrationLogInfoManager integrationLogInfoManager) {
		this.integrationLogInfoManager = integrationLogInfoManager;
	}
	public String getDataNetwork() {
		return dataNetwork;
	}
	public void setDataNetwork(String dataNetwork) {
		this.dataNetwork = dataNetwork;
	}
	public String getNetWorkControlValues() {
		return netWorkControlValues;
	}
	public void setNetWorkControlValues(String netWorkControlValues) {
		this.netWorkControlValues = netWorkControlValues;
	}
	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}
	public String getDivisionFlag() {
		return divisionFlag;
	}
	public void setDivisionFlag(String divisionFlag) {
		this.divisionFlag = divisionFlag;
	}
	public Map<String, String> getEuVatPercentList() {
		return euVatPercentList;
	}
	public void setEuVatPercentList(Map<String, String> euVatPercentList) {
		this.euVatPercentList = euVatPercentList;
	}
	public Map<String, String> getUTSIpayVatList() {
		return UTSIpayVatList;
	}
	public void setUTSIpayVatList(Map<String, String> uTSIpayVatList) {
		UTSIpayVatList = uTSIpayVatList;
	}
	public Map<String, String> getUTSIpayVatPercentList() {
		return UTSIpayVatPercentList;
	}
	public void setUTSIpayVatPercentList(Map<String, String> uTSIpayVatPercentList) {
		UTSIpayVatPercentList = uTSIpayVatPercentList;
	}
	public String getAutoPopulateActualDeliveryDateFlag() {
		return autoPopulateActualDeliveryDateFlag;
	}
	public void setAutoPopulateActualDeliveryDateFlag(
			String autoPopulateActualDeliveryDateFlag) {
		this.autoPopulateActualDeliveryDateFlag = autoPopulateActualDeliveryDateFlag;
	}
	public Map<String, String> getUTSIeuVatPercentList() {
		return UTSIeuVatPercentList;
	}
	public void setUTSIeuVatPercentList(Map<String, String> uTSIeuVatPercentList) {
		UTSIeuVatPercentList = uTSIeuVatPercentList;
	}
	public String getSystemDefaultVatCalculation() {
		return systemDefaultVatCalculation;
	}
	public void setSystemDefaultVatCalculation(String systemDefaultVatCalculation) {
		this.systemDefaultVatCalculation = systemDefaultVatCalculation;
	}
	public Map<String, String> getPayVatPercentList() {
		return payVatPercentList;
	}
	public void setPayVatPercentList(Map<String, String> payVatPercentList) {
		this.payVatPercentList = payVatPercentList;
	}
	public String getAgentRole() {
		return agentRole;
	}
	public void setAgentRole(String agentRole) {
		this.agentRole = agentRole;
	}
	public String getOriginAgentCode() {
		return originAgentCode;
	}
	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}
	public String getDestinationAgentCode() {
		return destinationAgentCode;
	}
	public void setDestinationAgentCode(String destinationAgentCode) {
		this.destinationAgentCode = destinationAgentCode;
	}
	public String getOriginSubAgentCode() {
		return originSubAgentCode;
	}
	public void setOriginSubAgentCode(String originSubAgentCode) {
		this.originSubAgentCode = originSubAgentCode;
	}
	public String getDestinationSubAgentCode() {
		return destinationSubAgentCode;
	}
	public void setDestinationSubAgentCode(String destinationSubAgentCode) {
		this.destinationSubAgentCode = destinationSubAgentCode;
	}
	public Map<String, String> getService() {
		return service;
	}
	public void setService(Map<String, String> service) {
		this.service = service;
	}
	public Map<String, String> getNotestatus() {
		return notestatus;
	}
	public void setNotestatus(Map<String, String> notestatus) {
		this.notestatus = notestatus;
	}
	public Map<String, String> getNotetype() {
		return notetype;
	}
	public void setNotetype(Map<String, String> notetype) {
		this.notetype = notetype;
	}
	public Map<String, String> getNotesubtype() {
		return notesubtype;
	}
	public void setNotesubtype(Map<String, String> notesubtype) {
		this.notesubtype = notesubtype;
	}
	public Notes getNotes() {
		return notes;
	}
	public void setNotes(Notes notes) {
		this.notes = notes;
	}
	public String getLinkUp() {
		return linkUp;
	}
	public void setLinkUp(String linkUp) {
		this.linkUp = linkUp;
	}
	public Long getTrackingStatusId() {
		return trackingStatusId;
	}
	public void setTrackingStatusId(Long trackingStatusId) {
		this.trackingStatusId = trackingStatusId;
	}
	
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getCountBookingAgentNotes() {
		return countBookingAgentNotes;
	}
	public void setCountBookingAgentNotes(String countBookingAgentNotes) {
		this.countBookingAgentNotes = countBookingAgentNotes;
	}
	
	public String getCountNetworkAgentNotes() {
		return countNetworkAgentNotes;
	}
	public void setCountNetworkAgentNotes(String countNetworkAgentNotes) {
		this.countNetworkAgentNotes = countNetworkAgentNotes;
	}
	public String getCountOriginAgentNotes() {
		return countOriginAgentNotes;
	}
	public void setCountOriginAgentNotes(String countOriginAgentNotes) {
		this.countOriginAgentNotes = countOriginAgentNotes;
	}
	public String getCountDestinAgentNotes() {
		return countDestinAgentNotes;
	}
	public void setCountDestinAgentNotes(String countDestinAgentNotes) {
		this.countDestinAgentNotes = countDestinAgentNotes;
	}
	public String getCountSubOriginAgentNotes() {
		return countSubOriginAgentNotes;
	}
	public void setCountSubOriginAgentNotes(String countSubOriginAgentNotes) {
		this.countSubOriginAgentNotes = countSubOriginAgentNotes;
	}
	public String getCountSubDestinAgentNotes() {
		return countSubDestinAgentNotes;
	}
	public void setCountSubDestinAgentNotes(String countSubDestinAgentNotes) {
		this.countSubDestinAgentNotes = countSubDestinAgentNotes;
	}
	public String getNotesSubType() {
		return notesSubType;
	}
	public void setNotesSubType(String notesSubType) {
		this.notesSubType = notesSubType;
	}
	public String getBookingAgentFlag() {
		return bookingAgentFlag;
	}
	public void setBookingAgentFlag(String bookingAgentFlag) {
		this.bookingAgentFlag = bookingAgentFlag;
	}
	public String getCheckAgent() {
		return checkAgent;
	}
	public void setCheckAgent(String checkAgent) {
		this.checkAgent = checkAgent;
	}
	public String getNetworkAgentFlag() {
		return networkAgentFlag;
	}
	public void setNetworkAgentFlag(String networkAgentFlag) {
		this.networkAgentFlag = networkAgentFlag;
	}
	public String getOriginAgentFlag() {
		return originAgentFlag;
	}
	public void setOriginAgentFlag(String originAgentFlag) {
		this.originAgentFlag = originAgentFlag;
	}
	public String getSubOriginAgentFlag() {
		return subOriginAgentFlag;
	}
	public void setSubOriginAgentFlag(String subOriginAgentFlag) {
		this.subOriginAgentFlag = subOriginAgentFlag;
	}
	public String getDestAgentFlag() {
		return destAgentFlag;
	}
	public void setDestAgentFlag(String destAgentFlag) {
		this.destAgentFlag = destAgentFlag;
	}
	public String getSubDestAgentFlag() {
		return subDestAgentFlag;
	}
	public void setSubDestAgentFlag(String subDestAgentFlag) {
		this.subDestAgentFlag = subDestAgentFlag;
	}
	public String getCompleteDateFlag() {
		return completeDateFlag;
	}
	public void setCompleteDateFlag(String completeDateFlag) {
		this.completeDateFlag = completeDateFlag;
	}
	public String getOriginalCorpID() {
		return originalCorpID;
	}
	public void setOriginalCorpID(String originalCorpID) {
		this.originalCorpID = originalCorpID;
	}
	public String getInvoiceCount() {
		return invoiceCount;
	}
	public void setInvoiceCount(String invoiceCount) {
		this.invoiceCount = invoiceCount;
	}
	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}
	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}
	public void setLossManager(LossManager lossManager) {
		this.lossManager = lossManager;
	}
	public Map<String, String> getReasonList() {
		return reasonList;
	}
	public void setReasonList(Map<String, String> reasonList) {
		this.reasonList = reasonList;
	}
	public Boolean getUsaFlag() {
		return usaFlag;
	}
	public void setUsaFlag(Boolean usaFlag) {
		this.usaFlag = usaFlag;
	}
	public String getBillToCodeAcc() {
		return billToCodeAcc;
	}
	public void setBillToCodeAcc(String billToCodeAcc) {
		this.billToCodeAcc = billToCodeAcc;
	}
	public List getAllBookingAgentList() {
		return allBookingAgentList;
	}
	public void setAllBookingAgentList(List allBookingAgentList) {
		this.allBookingAgentList = allBookingAgentList;
	}
	public List getUserDetailList() {
		return userDetailList;
	}
	public void setUserDetailList(List userDetailList) {
		this.userDetailList = userDetailList;
	}
	public SystemDefault getSystemDefault() {
		return systemDefault;
	}
	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}
	public Boolean getDefaultVendorAccLine() {
		return defaultVendorAccLine;
	}
	public void setDefaultVendorAccLine(Boolean defaultVendorAccLine) {
		this.defaultVendorAccLine = defaultVendorAccLine;
	}
	public Map<String, LinkedHashMap<String, String>> getParameterMap() {
		return parameterMap;
	}
	public void setParameterMap(
			Map<String, LinkedHashMap<String, String>> parameterMap) {
		this.parameterMap = parameterMap;
	}
	public Map<String, String> getLocalParameter() {
		return localParameter;
	}
	public void setLocalParameter(Map<String, String> localParameter) {
		this.localParameter = localParameter;
	}
	public String getIsRedSky() {
		return isRedSky;
	}
	public void setIsRedSky(String isRedSky) {
		this.isRedSky = isRedSky;
	}
	public String getCheckOption() {
		return checkOption;
	}
	public void setCheckOption(String checkOption) {
		this.checkOption = checkOption;
	}
	public String getHasPortalUser() {
		return hasPortalUser;
	}
	public void setHasPortalUser(String hasPortalUser) {
		this.hasPortalUser = hasPortalUser;
	}
	public String getPartCode() {
		return partCode;
	}
	public void setPartCode(String partCode) {
		this.partCode = partCode;
	}
	public String getIsRedSkyDestinAg() {
		return isRedSkyDestinAg;
	}
	public void setIsRedSkyDestinAg(String isRedSkyDestinAg) {
		this.isRedSkyDestinAg = isRedSkyDestinAg;
	}
	public String getIsRedSkySubOrigAg() {
		return isRedSkySubOrigAg;
	}
	public void setIsRedSkySubOrigAg(String isRedSkySubOrigAg) {
		this.isRedSkySubOrigAg = isRedSkySubOrigAg;
	}
	public String getIsRedSkySubDestinAg() {
		return isRedSkySubDestinAg;
	}
	public void setIsRedSkySubDestinAg(String isRedSkySubDestinAg) {
		this.isRedSkySubDestinAg = isRedSkySubDestinAg;
	}
	public String getIsSubDestinAgentPortal() {
		return isSubDestinAgentPortal;
	}
	public void setIsSubDestinAgentPortal(String isSubDestinAgentPortal) {
		this.isSubDestinAgentPortal = isSubDestinAgentPortal;
	}
	public String getIsSubOrigAgentPortal() {
		return isSubOrigAgentPortal;
	}
	public void setIsSubOrigAgentPortal(String isSubOrigAgentPortal) {
		this.isSubOrigAgentPortal = isSubOrigAgentPortal;
	}
	public String getIsDestinAgentPortal() {
		return isDestinAgentPortal;
	}
	public void setIsDestinAgentPortal(String isDestinAgentPortal) {
		this.isDestinAgentPortal = isDestinAgentPortal;
	}
	public String getIsAgentPortal() {
		return isAgentPortal;
	}
	public void setIsAgentPortal(String isAgentPortal) {
		this.isAgentPortal = isAgentPortal;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getConEmail() {
		return conEmail;
	}
	public void setConEmail(String conEmail) {
		this.conEmail = conEmail;
	}
	public boolean isAccountLineAccountPortalFlag() {
		return accountLineAccountPortalFlag;
	}
	public void setAccountLineAccountPortalFlag(boolean accountLineAccountPortalFlag) {
		this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
	}
	public String getAccountLineBillingParam() {
		return accountLineBillingParam;
	}
	public void setAccountLineBillingParam(String accountLineBillingParam) {
		this.accountLineBillingParam = accountLineBillingParam;
	}
	public boolean isCheckAccessQuotation() {
		return checkAccessQuotation;
	}
	public void setCheckAccessQuotation(boolean checkAccessQuotation) {
		this.checkAccessQuotation = checkAccessQuotation;
	}
	public String getMoveType() {
		return moveType;
	}
	public void setMoveType(String moveType) {
		this.moveType = moveType;
	}
	public String getMoveTypePartner() {
		return moveTypePartner;
	}
	public void setMoveTypePartner(String moveTypePartner) {
		this.moveTypePartner = moveTypePartner;
	}
	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}
	public String getIsDecaExtract() {
		return isDecaExtract;
	}
	public void setIsDecaExtract(String isDecaExtract) {
		this.isDecaExtract = isDecaExtract;
	}
	public String getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(String seqNumber) {
		this.seqNumber = seqNumber;
	}
	public String getShpNumber() {
		return shpNumber;
	}
	public void setShpNumber(String shpNumber) {
		this.shpNumber = shpNumber;
	}
	public String getLinkUpButtonType() {
		return linkUpButtonType;
	}
	public void setLinkUpButtonType(String linkUpButtonType) {
		this.linkUpButtonType = linkUpButtonType;
	}

	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}	
	public void setUgwwActionTrackerManager(
			UgwwActionTrackerManager ugwwActionTrackerManager) {
		this.ugwwActionTrackerManager = ugwwActionTrackerManager;
	}

	public void setAccessInfoManager(AccessInfoManager accessInfoManager) {
		this.accessInfoManager = accessInfoManager;
	}

	public List getDocumentListCF() {
		return documentListCF;
	}
	public void setDocumentListCF(List documentListCF) {
		this.documentListCF = documentListCF;
	}
	public List getDocumentListSO() {
		return documentListSO;
	}
	public void setDocumentListSO(List documentListSO) {
		this.documentListSO = documentListSO;
	}
	public String getDocumentIdListCF() {
		return documentIdListCF;
	}
	public void setDocumentIdListCF(String documentIdListCF) {
		this.documentIdListCF = documentIdListCF;
	}
	public String getDocumentIdListSO() {
		return documentIdListSO;
	}
	public void setDocumentIdListSO(String documentIdListSO) {
		this.documentIdListSO = documentIdListSO;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}
	public void setInventoryPathManager(InventoryPathManager inventoryPathManager) {
		this.inventoryPathManager = inventoryPathManager;
	}

	public String getAllIdOfMyFile() {
		return allIdOfMyFile;
	}
	public void setAllIdOfMyFile(String allIdOfMyFile) {
		this.allIdOfMyFile = allIdOfMyFile;
	}
	public String getMyFileFieldName() {
		return myFileFieldName;
	}
	public void setMyFileFieldName(String myFileFieldName) {
		this.myFileFieldName = myFileFieldName;
	}
	public void setInventoryDataManager(InventoryDataManager inventoryDataManager) {
		this.inventoryDataManager = inventoryDataManager;
	}
	
	public String getCheckOldCf() {
		return checkOldCf;
	}
	public void setCheckOldCf(String checkOldCf) {
		this.checkOldCf = checkOldCf;
	}
	public ServiceOrder getAgentServiceOrderCombo() {
		return agentServiceOrderCombo;
	}
	public void setAgentServiceOrderCombo(ServiceOrder agentServiceOrderCombo) {
		this.agentServiceOrderCombo = agentServiceOrderCombo;
	}
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	public Boolean getSurveyTab() {
		return surveyTab;
	}
	public void setSurveyTab(Boolean surveyTab) {
		this.surveyTab = surveyTab;
	}
	public String getNetworkVendorCode() {
		return networkVendorCode;
	}
	public void setNetworkVendorCode(String networkVendorCode) {
		this.networkVendorCode = networkVendorCode;
	}
	public List getVenderDetails() {
		return venderDetails;
	}
	public void setVenderDetails(List venderDetails) {
		this.venderDetails = venderDetails;
	}
	public int getServiceOrdersSize() {
		return serviceOrdersSize;
	}
	public void setServiceOrdersSize(int serviceOrdersSize) {
		this.serviceOrdersSize = serviceOrdersSize;
	}
	public Long getCid() {
		return cid;
	}
	public void setCid(Long cid) {
		this.cid = cid;
	}
	public String getShipNo() {
		return shipNo;
	}
	public void setShipNo(String shipNo) {
		this.shipNo = shipNo;
	}
	public List getSoList() {
		return soList;
	}
	public void setSoList(List soList) {
		this.soList = soList;
	}
	public TrackingStatus getCopyAgentDetail() {
		return copyAgentDetail;
	}
	public void setCopyAgentDetail(TrackingStatus copyAgentDetail) {
		this.copyAgentDetail = copyAgentDetail;
	}
	public Long getSoGetId() {
		return soGetId;
	}
	public void setSoGetId(Long soGetId) {
		this.soGetId = soGetId;
	}
	public Long getSoSetId() {
		return soSetId;
	}
	public void setSoSetId(Long soSetId) {
		this.soSetId = soSetId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List getPricingBillingPaybaleList() {
		return pricingBillingPaybaleList;
	}
	public void setPricingBillingPaybaleList(List pricingBillingPaybaleList) {
		this.pricingBillingPaybaleList = pricingBillingPaybaleList;
	}
	public String getoADAAgentCheck() {
		return oADAAgentCheck;
	}
	public void setoADAAgentCheck(String oADAAgentCheck) {
		this.oADAAgentCheck = oADAAgentCheck;
	}
	public String getCountBrokerAgentNotes() {
		return countBrokerAgentNotes;
	}
	public void setCountBrokerAgentNotes(String countBrokerAgentNotes) {
		this.countBrokerAgentNotes = countBrokerAgentNotes;
	}
	public String getCountForwarderAgentNotes() {
		return countForwarderAgentNotes;
	}
	public void setCountForwarderAgentNotes(String countForwarderAgentNotes) {
		this.countForwarderAgentNotes = countForwarderAgentNotes;
	}
	public String getSeqNm() {
		return seqNm;
	}
	public void setSeqNm(String seqNm) {
		this.seqNm = seqNm;
	}
	public Long getSoId() {
		return soId;
	}
	public void setSoId(Long soId) {
		this.soId = soId;
	}
	public String getUpdaterIntegration() {
		return updaterIntegration;
	}
	public void setUpdaterIntegration(String updaterIntegration) {
		this.updaterIntegration = updaterIntegration;
	}

	public String getSystemDefaultReciprocityJobType() {
		return systemDefaultReciprocityJobType;
	}
	public void setSystemDefaultReciprocityJobType(
			String systemDefaultReciprocityJobType) {
		this.systemDefaultReciprocityJobType = systemDefaultReciprocityJobType;
	}

	public void setCoordinatorEvalValidation(String coordinatorEvalValidation) {
		this.coordinatorEvalValidation = coordinatorEvalValidation;
	}
	public String getLinkUpType() {
		return linkUpType;
	}
	public void setLinkUpType(String linkUpType) {
		this.linkUpType = linkUpType;
	}
	public Map<String, String> getComDivCodePerCorpIdMap() {
		return comDivCodePerCorpIdMap;
	}
	public void setComDivCodePerCorpIdMap(Map<String, String> comDivCodePerCorpIdMap) {
		this.comDivCodePerCorpIdMap = comDivCodePerCorpIdMap;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getSoBillToCode() {
		return soBillToCode;
	}
	public void setSoBillToCode(String soBillToCode) {
		this.soBillToCode = soBillToCode;
	}
	public Map<String, String> getContainerMap() {
		return containerMap;
	}
	public void setContainerMap(Map<String, String> containerMap) {
		this.containerMap = containerMap;
	}
	public Date getActualPackBegin() {
		return actualPackBegin;
	}
	public void setActualPackBegin(Date actualPackBegin) {
		this.actualPackBegin = actualPackBegin;
	}
	public Date getActualLoadBegin() {
		return actualLoadBegin;
	}
	public void setActualLoadBegin(Date actualLoadBegin) {
		this.actualLoadBegin = actualLoadBegin;
	}
	public Date getActualDeliveryBegin() {
		return actualDeliveryBegin;
	}
	public void setActualDeliveryBegin(Date actualDeliveryBegin) {
		this.actualDeliveryBegin = actualDeliveryBegin;
	}
	public List getContainerNumberList() {
		return containerNumberList;
	}
	public void setContainerNumberList(List containerNumberList) {
		if(containerNumberList.isEmpty() && containerNumberList.size()<0){
			containerNumberList=new ArrayList();
		}
		this.containerNumberList = containerNumberList;
	}
	public List getPreferredList() {
		return preferredList;
	}
	public void setPreferredList(List preferredList) {
		this.preferredList = preferredList;
	}
	public List getHandOutList() {
		return handOutList;
	}
	public void setHandOutList(List handOutList) {
		this.handOutList = handOutList;
	}
	public void setItemDataManager(ItemDataManager itemDataManager) {
		this.itemDataManager = itemDataManager;
	}
	public String getVatBillingGroup() {
		return vatBillingGroup;
	}
	public void setVatBillingGroup(String vatBillingGroup) {
		this.vatBillingGroup = vatBillingGroup;
	}
	public String getCreditTerms() {
		return creditTerms;
	}
	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public boolean isFlagForPartnerAddress() {
		return flagForPartnerAddress;
	}
	public void setFlagForPartnerAddress(boolean flagForPartnerAddress) {
		this.flagForPartnerAddress = flagForPartnerAddress;
	}
	public List getCMMDMMAgentList() {
		return CMMDMMAgentList;
	}
	public void setCMMDMMAgentList(List cMMDMMAgentList) {
		CMMDMMAgentList = cMMDMMAgentList;
	}
	public Set getLinkedshipNumber() {
		return linkedshipNumber;
	}
	public void setLinkedshipNumber(Set linkedshipNumber) {
		this.linkedshipNumber = linkedshipNumber;
	}
	public Map<String, String> getJobtype() {
		return jobtype;
	}
	public void setJobtype(Map<String, String> jobtype) {
		this.jobtype = jobtype;
	}
	public String getJobTypes() {
		return jobTypes;
	}
	public void setJobTypes(String jobTypes) {
		this.jobTypes = jobTypes;
	}
	public String getMsgClicked() {
		return msgClicked;
	}
	public void setMsgClicked(String msgClicked) {
		this.msgClicked = msgClicked;
	}
	public Map<String, String> getLocationTypes() {
		return locationTypes;
	}
	public void setLocationTypes(Map<String, String> locationTypes) {
		this.locationTypes = locationTypes;
	}

	public String getOriginAgentInfo() {
		return originAgentInfo;
	}
	public void setOriginAgentInfo(String originAgentInfo) {
		this.originAgentInfo = originAgentInfo;
	}
	public String getDefaultContactInfo() {
		return defaultContactInfo;
	}
	public void setDefaultContactInfo(String defaultContactInfo) {
		this.defaultContactInfo = defaultContactInfo;
	}
	
	public String getContractTypeTS() {
		return contractTypeTS;
	}
	public void setContractTypeTS(String contractTypeTS) {
		this.contractTypeTS = contractTypeTS;
	}
	public String getLinkedshipnumberTS() {
		return linkedshipnumberTS;
	}
	public void setLinkedshipnumberTS(String linkedshipnumberTS) {
		this.linkedshipnumberTS = linkedshipnumberTS;
	}
	public String getInvoicedLinkedAccountLines() {
		return invoicedLinkedAccountLines;
	}
	public void setInvoicedLinkedAccountLines(String invoicedLinkedAccountLines) {
		this.invoicedLinkedAccountLines = invoicedLinkedAccountLines;
	}
	
	public String getCrewArrivalTime() {
		return crewArrivalTime;
	}
	public void setCrewArrivalTime(String crewArrivalTime) {
		this.crewArrivalTime = crewArrivalTime;
	}
	public Map<String, String> getExcessWeightBillingList() {
		return excessWeightBillingList;
	}
	public void setExcessWeightBillingList(
			Map<String, String> excessWeightBillingList) {
		this.excessWeightBillingList = excessWeightBillingList;
	}
	public String getOiJobList() {
		return oiJobList;
	}
	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}
	public String getExtReference() {
		return extReference;
	}
	public void setExtReference(String extReference) {
		this.extReference = extReference;
	}
	public String getAgentParentName() {
		return agentParentName;
	}
	public void setAgentParentName(String agentParentName) {
		this.agentParentName = agentParentName;
	}
	public String getAgentParentCode() {
		return agentParentCode;
	}
	public void setAgentParentCode(String agentParentCode) {
		this.agentParentCode = agentParentCode;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}
	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}
	public String getSoCoordinatorname() {
		return soCoordinatorname;
	}
	public void setSoCoordinatorname(String soCoordinatorname) {
		this.soCoordinatorname = soCoordinatorname;
	}

    /*public String getIsSOExtract() {
		return isSOExtract;
	}
	public void setIsSOExtract(String isSOExtract) {
		this.isSOExtract = isSOExtract;
	}*/

	public Boolean getAgentSearchValidation() {
		return agentSearchValidation;
	}
	public void setAgentSearchValidation(Boolean agentSearchValidation) {
		this.agentSearchValidation = agentSearchValidation;
	}


	public String getSupplementGBL() {
		return supplementGBL;
	}
	public void setSupplementGBL(String supplementGBL) {
		this.supplementGBL = supplementGBL;
	}
	public String getUsGovJobs() {
		return usGovJobs;
	}
	public void setUsGovJobs(String usGovJobs) {
		this.usGovJobs = usGovJobs;
	}
	public String getDashBoardHideJobsList() {
		return dashBoardHideJobsList;
	}
	public void setDashBoardHideJobsList(String dashBoardHideJobsList) {
		this.dashBoardHideJobsList = dashBoardHideJobsList;
	}
} 


