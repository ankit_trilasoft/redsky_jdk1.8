package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ResourceContractChargeDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.OperationsIntelligence;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.model.ResourceContractCharges;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.ItemsJbkEquipManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.OperationsIntelligenceManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class ResourceContractChargeDaoHibernate extends GenericDaoHibernate<ResourceContractCharges, Long> implements ResourceContractChargeDao {
	
	private HibernateUtil hibernateUtil;
	private AccountLineManager accountLineManager;
	private ServiceOrderManager serviceOrderManager;
	private ItemsJbkEquipManager itemsJbkEquipManager;
	private OperationsIntelligenceManager operationsIntelligenceManager;
	private RefMasterManager refMasterManager;
	private SystemDefault systemDefault;
	private  TrackingStatus trackingStatus;
	private  Miscellaneous miscellaneous;
	private MiscellaneousManager miscellaneousManager;
	private TrackingStatusManager trackingStatusManager;
	private AccountLine accountLine;
	
	public ResourceContractChargeDaoHibernate() {
		super(ResourceContractCharges.class);
	}
	protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    } 

	public List findResourceByPartnerId(String corpId, Long parentId) {
		String query = "from ResourceContractCharges where corpID='" + corpId +"' AND parentId='" + parentId +"'";
		List list = getHibernateTemplate().find(query);
		return list;
	}
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}

	public void deleteAll(Long parentId) {
/*		getHibernateTemplate().bulkUpdate("delete from ResourceContractCharges where parentId="+parentId);
		Transaction t = getSession().beginTransaction();*/
		getSession().createSQLQuery("delete from resourcecontractcharges where parentId="+parentId).executeUpdate();
		
	}
	
	public List<ItemsJbkEquip> convertObjectToModel(List tempList){
		List<ItemsJbkEquip> list = new ArrayList<ItemsJbkEquip>();
		Iterator it = tempList.iterator();
		while(it.hasNext()){
//	           Object row= (Object)it.next();
	           Long id = Long.parseLong(it.next().toString());
	           list.add(itemsJbkEquipManager.get(id));
		}
		return list;
	}
	
	public List<OperationsIntelligence> convertObjectToModelForOI(List tempList){
		List<OperationsIntelligence> list = new ArrayList<OperationsIntelligence>();
		Iterator it = tempList.iterator();
		while(it.hasNext()){
//	           Object row= (Object)it.next();
	           Long id = Long.parseLong(it.next().toString());
	           list.add(operationsIntelligenceManager.get(id));
		}
		return list;
	}
	
	public List createPriceCall(ServiceOrder serviceOrder,String tempQuery,String corpId,String contract,String PERDAY,String type,String divisionFlag,String workOrder){
		List chargeList = new  ArrayList();
		List tempList = getSession().createSQLQuery(tempQuery).list();
		List<OperationsIntelligence> list = convertObjectToModelForOI(tempList);
		 Double estimatedbuyrate1 = 0.0;
		 Double estimatedSellRate1 = 0.0;
		 Double revisionSellRate1 = 0.0;
		 Double revisionBuyRate1 = 0.0;
		 String description1 = "" ;
		String tempForType= "true" ;
		List listId=new  ArrayList(); 
		Set accIdSet=new LinkedHashSet();
		String sameAccountLineId=new String("");
		String tempAccountLineId=new String("");
		 for (OperationsIntelligence operationsIntelligence : list) {
			 String query1 = "select r.charge from resourcecontractcharges r,itemsjequip ijq" +
				" where r.corpId = '"+corpId+"' and r.corpId=ijq.corpId and r.parentId=ijq.id" +
				" and ijq.type ='"+operationsIntelligence.getType() +"' and ijq.descript = '"+StringEscapeUtils.escapeSql(operationsIntelligence.getDescription()) +"' and r.contract='"+ contract +"'";
			   List resourceList1 = getSession().createSQLQuery(query1).list();
			   /*if(resourceList1!=null && !resourceList1.isEmpty() && resourceList1.get(0)!=null){
				   System.out.println(resourceList1.get(0).toString());
			   }*/
			/* List resourceList1 = getSession().createSQLQuery(query1).list();*/
			/* if (resourceList1 != null){ */
				/* for (int i = 0; i < resourceList1.size(); i++) {*/
					/*if(resourceList1.get(i) != null){
						 String charge = resourceList1.get(i).toString().trim();*/
						 String charge1 = null;
						 String chargeQuery=null;
						/* if(charge.length() > 0){*/
							 if(type.equalsIgnoreCase("C"))
							 {
							 chargeQuery = "from Charges where corpID='" + corpId +"'"+" AND contract='" + contract +"' and charge='Crew'";
							 charge1 = "Crew";
							 }
							 if(type.equalsIgnoreCase("E"))
							 {
							 chargeQuery = "from Charges where corpID='" + corpId +"'"+" AND contract='" + contract +"' and charge='Equipment'";
							 charge1 = "Equipment";
							 }
							 if(type.equalsIgnoreCase("M"))
							 {
							 chargeQuery = "from Charges where corpID='" + corpId +"'"+" AND contract='" + contract +"' and charge='Material'";
							 charge1 = "Material";
							 }
							 if(type.equalsIgnoreCase("V"))
							 {
							 chargeQuery = "from Charges where corpID='" + corpId +"'"+" AND contract='" + contract +"' and charge='Vehicle'";
							 charge1 = "Vehicle";
							 }
							 if(type.equalsIgnoreCase("T"))
							 {
							 chargeQuery = "from Charges where corpID='" + corpId +"'"+" AND contract='" + contract +"' and charge='Third Party'";
							 charge1 = "Third Party";
							 }
							 
							 if(type.equalsIgnoreCase("Z"))
							 {
						     if(resourceList1!=null && !resourceList1.isEmpty() && resourceList1.get(0)!=null){	 
							 chargeQuery = "from Charges where corpID='" + corpId +"'"+" AND contract='" + contract +"' and charge='"+resourceList1.get(0).toString()+"' ";
							 charge1 = resourceList1.get(0).toString();
						     }else{
						    	 chargeQuery = "from Charges where corpID='" + corpId +"'"+" AND contract='" + contract +"' and charge='Mainfreight'";
								 charge1 = "Mainfreight";
						     }
							 }
							 if(type.equalsIgnoreCase("F"))
							 {
							 chargeQuery = "from Charges where corpID='" + corpId +"'"+" AND contract='" + contract +"' and charge='Fees'";
							 charge1 = "Fees";
							 }
							 List<Charges> chargelist = getHibernateTemplate().find(chargeQuery);
							 if (!chargelist.isEmpty()) {
								 Charges chargeObj = chargelist.get(0);
								 chargeList.add(chargeObj);
								 Double cost = 0.0;
								 String accDay = "";
								 if(operationsIntelligence.getAccountLineId() !=null && (!(operationsIntelligence.getAccountLineId().toString().trim().equals(""))) ){
									 sameAccountLineId =operationsIntelligence.getAccountLineId().toString().trim();  
								 }else{
									 sameAccountLineId= "blank";
								 }
								 if(PERDAY.equals("PERDAY")){

									 int revisionQtyFound=0;
									 String queryForRevisionQtyCheck = "SELECT count(distinct revisionQuantity) FROM operationsintelligence where status is true and shipNumber='" + serviceOrder.getShipNumber() +"' and workorder='"+workOrder+"' and revisionQuantity!=0.00 and ticketstatus is null and accountLineId is null";
									 if(operationsIntelligence.getAccountLineId() !=null ){
										 queryForRevisionQtyCheck = "SELECT count(distinct revisionQuantity) FROM operationsintelligence where status is true and shipNumber='" + serviceOrder.getShipNumber() +"' and workorder='"+workOrder+"' and revisionQuantity!=0.00 and ticketstatus is null and accountLineId = '"+operationsIntelligence.getAccountLineId()+"'"; 
									 }
									 List revisionQtyList = getSession().createSQLQuery(queryForRevisionQtyCheck).list();
									 if((revisionQtyList!=null && !revisionQtyList.isEmpty()) && revisionQtyList.get(0)!=null){
										 revisionQtyFound = Integer.parseInt(revisionQtyList.get(0).toString());
									 }
									 
									 int estHourFound=0;
									 String queryForEstHourCheck = "SELECT count(distinct esthours) FROM operationsintelligence where status is true and shipNumber='" + serviceOrder.getShipNumber() +"' and workorder='"+workOrder+"' and esthours>0 and ticketstatus is null and accountLineId is null";
									 if(operationsIntelligence.getAccountLineId() !=null ){
										 queryForEstHourCheck = "SELECT count(distinct esthours) FROM operationsintelligence where status is true and shipNumber='" + serviceOrder.getShipNumber() +"' and workorder='"+workOrder+"' and esthours>0 and ticketstatus is null and accountLineId = '"+operationsIntelligence.getAccountLineId()+"'"; 
									 }
									 List estHourList = getSession().createSQLQuery(queryForEstHourCheck).list();
									 if((estHourList!=null && !estHourList.isEmpty()) && estHourList.get(0)!=null){
										 estHourFound = Integer.parseInt(estHourList.get(0).toString());
									 }
									 
									 String costQuery1="";
									 costQuery1 = "select quantitiy,esthours ,estimatedexpense,estimatedrevenue,revisionrevenue,revisionexpense,description,type,estimatedsellrate,revisionsellrate,date_format(date,'%c/%d/%y'),revisionQuantity,id,accountLineId  from operationsintelligence where status is true and shipNumber='" + serviceOrder.getShipNumber() +
									 "'and type='"+type+"' and quantitiy>0 and esthours>0 and ticketStatus is null and workorder='"+workOrder+"' and accountLineId is null";
									 if(operationsIntelligence.getAccountLineId() !=null ){
										 costQuery1 = "select quantitiy,esthours ,estimatedexpense,estimatedrevenue,revisionrevenue,revisionexpense,description,type,estimatedsellrate,revisionsellrate,date_format(date,'%c/%d/%y'),revisionQuantity,id,accountLineId  from operationsintelligence where status is true and shipNumber='" + serviceOrder.getShipNumber() +
												 "'and type='"+type+"' and quantitiy>0 and esthours>0 and ticketStatus is null and workorder='"+workOrder+"' and accountLineId = '"+operationsIntelligence.getAccountLineId()+"'"; 
									 }
									 List costList1 = getSession().createSQLQuery(costQuery1).list();
									 Iterator it=costList1.iterator();
									 listId=new  ArrayList(); 
									 accIdSet=new LinkedHashSet();
									while(it.hasNext()){
										Object []row= (Object [])it.next();
										 Double estimatedbuyrate = Double.valueOf(row[2].toString());
										 estimatedbuyrate1 = estimatedbuyrate1+estimatedbuyrate;
										 Double estimatedSellRate = Double.valueOf(row[3].toString());
										 estimatedSellRate1 = estimatedSellRate1 +estimatedSellRate;
										 Double revisionSellRate = Double.valueOf(row[4].toString());
										 revisionSellRate1 = revisionSellRate1+revisionSellRate;
										 listId.add(row[12].toString());
										 if(row[13] !=null && (!(row[13].toString().trim().equals("")))){
										 accIdSet.add(row[13].toString());
										 }
										 Double revisionBuyRate = Double.valueOf(row[5].toString());
										 revisionBuyRate1 = revisionBuyRate1 +revisionBuyRate;
										 Double estSellRate = Double.valueOf(row[8].toString());
										 Double revSellRate = Double.valueOf(row[9].toString());
										 String description = row[6].toString();
										 Double revisionHours = Double.valueOf(row[11].toString());
										 Double estHours = Double.valueOf(row[1].toString());
										if(row[7].toString().equalsIgnoreCase("C")){
											if(description1.equals("")){
												String []expArr = row[0].toString().split("\\.");
					            				if((expArr[1]!=null && !expArr[1].equalsIgnoreCase("")) && expArr[1].equalsIgnoreCase("00")){
					            					if(row[10]!=null && !row[10].equals("")){
					            						if((revisionQtyFound>1) && revisionHours!=0.00){
															description1 = row[10].toString() + " - "+ expArr[0]+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((revisionQtyFound==1) && revisionHours!=0.00 && (!it.hasNext())){
															description1 = row[10].toString() + " - "+ expArr[0]+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((estHourFound>1) && estHours!=0 && revisionHours==0.00){
															description1 = row[10].toString() + " - "+ expArr[0]+" " +description+" "+estHours+" "+"Hrs.";
														}else if((estHourFound==1) && estHours!=0 && revisionHours==0.00 && (!it.hasNext())){
															description1 = row[10].toString() + " - "+ expArr[0]+" " +description+" "+estHours+" "+"Hrs.";
														}else{
															description1 = row[10].toString() + " - "+ expArr[0]+" " +description;
														}
					            						
					            					}else{
					            						if((revisionQtyFound>1) && revisionHours!=0.00){
															description1 = expArr[0]+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((revisionQtyFound==1) && revisionHours!=0.00 && (!it.hasNext())){
															description1 = expArr[0]+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((estHourFound>1) && estHours!=0 && revisionHours==0.00){
															description1 = expArr[0]+" " +description+" "+estHours+" "+"Hrs.";
														}else if((estHourFound==1) && estHours!=0 && revisionHours==0.00 && (!it.hasNext())){
															description1 = expArr[0]+" " +description+" "+estHours+" "+"Hrs.";
														}else{
															description1 = expArr[0]+" " +description;
														}
					            					}
					            				}else{
					            					if(row[10]!=null && !row[10].equals("")){
					            						if((revisionQtyFound>1) && revisionHours!=0.00){
															description1 = row[10].toString() + " - "+ row[0].toString()+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((revisionQtyFound==1) && revisionHours!=0.00 && (!it.hasNext())){
															description1 = row[10].toString() + " - "+ row[0].toString()+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((estHourFound>1) && estHours!=0 && revisionHours==0.00){
															description1 = row[10].toString()+" - " + row[0].toString()+" " +description+" "+estHours+" "+"Hrs.";
														}else if((estHourFound==1) && estHours!=0 && revisionHours==0.00 && (!it.hasNext())){
															description1 = row[10].toString()+" - " + row[0].toString()+" " +description+" "+estHours+" "+"Hrs.";
														}else{
															description1 = row[10].toString() + " - "+ row[0].toString()+" " +description;
														}
					            					}else{
					            						if((revisionQtyFound>1) && revisionHours!=0.00){
															description1 = row[0].toString()+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((revisionQtyFound==1) && revisionHours!=0.00 && (!it.hasNext())){
															description1 = row[0].toString()+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((estHourFound>1) && estHours!=0 && revisionHours==0.00){
															description1 = row[0].toString()+" " +description+" "+estHours+" "+"Hrs.";
														}else if((estHourFound==1) && estHours!=0 && revisionHours==0.00 && (!it.hasNext())){
															description1 = row[0].toString()+" " +description+" "+estHours+" "+"Hrs.";
														}else{
															description1 = row[0].toString()+" " +description;
														}
					            					}
					            				}
											}else{
												if(!description1.contains(description)){
													String []expArr = row[0].toString().split("\\.");
						            				if((expArr[1]!=null && !expArr[1].equalsIgnoreCase("")) && expArr[1].equalsIgnoreCase("00")){
						            					if(revisionQtyFound>1 && revisionHours!=0.00){
															description1 = description1 +", "+expArr[0]+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((revisionQtyFound==1) && revisionHours!=0.00 && (!it.hasNext())){
															description1 = description1 +", "+expArr[0]+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((estHourFound>1) && estHours!=0 && revisionHours==0.00){
															description1 = description1 +", "+ expArr[0]+" " +description+" "+estHours+" "+"Hrs.";
														}else if((estHourFound==1) && estHours!=0 && revisionHours==0.00 && (!it.hasNext())){
															description1 = description1 +", "+ expArr[0]+" " +description+" "+estHours+" "+"Hrs.";
														}else{
															description1 = description1 +", "+expArr[0]+" " +description;
														}
						            				}else{
						            					if(revisionQtyFound>1 && revisionHours!=0.00 ){
															description1 = description1 +", "+row[0].toString()+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((revisionQtyFound==1) && revisionHours!=0.00  && (!it.hasNext())){
															description1 = description1 +", "+row[0].toString()+" " +description+" "+revisionHours+" "+"Hrs.";
														}else if((estHourFound>1) && estHours!=0 && revisionHours==0.00){
															description1 = description1 +", "+ row[0].toString()+" " +description+" "+estHours+" "+"Hrs.";
														}else if((estHourFound==1) && estHours!=0 && revisionHours==0.00 && (!it.hasNext())){
															description1 = description1 +", "+ row[0].toString()+" " +description+" "+estHours+" "+"Hrs.";
														}else{
															description1 = description1 +", "+row[0].toString()+" " +description;
														}
						            				}
												}
											}
										}
										if((row[7].toString().equalsIgnoreCase("M")) || (row[7].toString().equalsIgnoreCase("E")) || (row[7].toString().equalsIgnoreCase("Z")) || (row[7].toString().equalsIgnoreCase("F"))){
											if(description1.equals("")){
												String []expArr = row[0].toString().split("\\.");
					            				if((expArr[1]!=null && !expArr[1].equalsIgnoreCase("")) && expArr[1].equalsIgnoreCase("00")){
					            					if(revSellRate!=0.00){
					            						description1 = expArr[0] + " "+ description +" @ $"+revSellRate+"ea.";
					            					}else{
					            						description1 = expArr[0] + " "+ description +" @ $"+estSellRate+"ea.";
					            					}
					            				}else{
					            					if(revSellRate!=0.00){
					            						description1 = row[0].toString() + " "+ description +" @ $"+revSellRate+"ea.";
					            					}else{
					            						description1 = row[0].toString() + " "+ description +" @ $"+estSellRate+"ea.";
					            					}
					            				}
											}else{
												String []expArr = row[0].toString().split("\\.");
					            				if((expArr[1]!=null && !expArr[1].equalsIgnoreCase("")) && expArr[1].equalsIgnoreCase("00")){
					            					if(revSellRate!=0.00){
					            						description1 = description1 +", "+ expArr[0] + " "+ description +" @ $"+revSellRate+"ea.";
					            					}else{
					            						description1 = description1 +", "+expArr[0] + " "+ description +" @ $"+estSellRate+"ea.";
					            					}
					            				}else{
					            					if(revSellRate!=0.00){
					            						description1 = description1 +", "+row[0].toString() + " "+ description +" @ $"+revSellRate+"ea.";
					            					}else{
					            						description1 = description1 +", "+row[0].toString() + " "+ description +" @ $"+estSellRate+"ea.";
					            					}
					            				}
											}
									    }	
										if(row[7].toString().equalsIgnoreCase("V")){
											if(description1.equals("")){
												if(row[10]!=null && !row[10].equals("")){
													if((revisionQtyFound>1) && revisionHours!=0.00){
														description1 = row[10].toString() + " - "+ description+" "+revisionHours+" "+"Hrs.";
													}else if((revisionQtyFound==1) && revisionHours!=0.00  && (!it.hasNext())){
														description1 = row[10].toString() + " - " +description+" "+revisionHours+" "+"Hrs.";
													}else if((estHourFound>1) && estHours!=0 && revisionHours==0.00){
														description1 = row[10].toString() + " - "+description+" "+estHours+" "+"Hrs.";
													}else if((estHourFound==1) && estHours!=0 && revisionHours==0.00 && (!it.hasNext())){
														description1 = row[10].toString() + " - "+description+" "+estHours+" "+"Hrs.";
													}else{
														description1 = row[10].toString() + " - "+ description;
													}
												}else{
													if((revisionQtyFound>1) && revisionHours!=0.00){
														description1 = description+" "+revisionHours+" "+"Hrs.";
													}else if((revisionQtyFound==1) && revisionHours!=0.00  && (!it.hasNext())){
														description1 = description+" "+revisionHours+" "+"Hrs.";
													}else if((estHourFound>1) && estHours!=0 && revisionHours==0.00){
														description1 = description+" "+estHours+" "+"Hrs.";
													}else if((estHourFound==1) && estHours!=0 && revisionHours==0.00 && (!it.hasNext())){
														description1 = description+" "+estHours+" "+"Hrs.";
													}else{
														description1 = description;
													}
												}
											}else{
													if((revisionQtyFound>1) && revisionHours!=0.00){
														description1 = description1 +", "+ description+" "+revisionHours+" "+"Hrs.";
													}else if((revisionQtyFound==1) && revisionHours!=0.00  && (!it.hasNext())){
														description1 = description1 +", "+description+" "+revisionHours+" "+"Hrs.";
													}else if((estHourFound>1) && estHours!=0 && revisionHours==0.00){
														description1 = description1 +", "+description+" "+estHours+" "+"Hrs.";
													}else if((estHourFound==1) && estHours!=0 && revisionHours==0.00 && (!it.hasNext())){
														description1 = description1 +", "+description+" "+estHours+" "+"Hrs.";
													}else{
														if(row[10]!=null && !row[10].equals("")){
														description1 = description1 +", "+row[10].toString() + " - "+ description;
														}else{
															description1 = description1 +" - "+ description;	
														}
													}
											}
									    }	
									}
									 cost = 1.0;
								 }else{
									 String costQuery1 = "select quantitiy,esthours ,estimatedexpense,estimatedrevenue,revisionrevenue,revisionexpense,description,id,accountLineId from operationsintelligence where status is true and shipNumber='" + serviceOrder.getShipNumber() +
									 "'and description='"+StringEscapeUtils.escapeSql(operationsIntelligence.getDescription())+"' and ticketStatus is null "
									 		+ "and quantitiy>0 and esthours>0 and quantitiy='"+operationsIntelligence.getQuantitiy()+"' and esthours = '"+operationsIntelligence.getEsthours()+"' "
									 				+ " and estimatedexpense='"+operationsIntelligence.getEstimatedexpense()+"' and estimatedrevenue='"+operationsIntelligence.getEstimatedrevenue()+"' and workorder='"+workOrder+"' and accountLineId is null";
									 if(operationsIntelligence.getAccountLineId() !=null ){
										 costQuery1 = "select quantitiy,esthours ,estimatedexpense,estimatedrevenue,revisionrevenue,revisionexpense,description,id,accountLineId from operationsintelligence where status is true and shipNumber='" + serviceOrder.getShipNumber() +
												 "'and description='"+StringEscapeUtils.escapeSql(operationsIntelligence.getDescription())+"' and ticketStatus is null "
												 		+ "and quantitiy>0 and esthours>0 and quantitiy='"+operationsIntelligence.getQuantitiy()+"' and esthours = '"+operationsIntelligence.getEsthours()+"' "
												 				+ " and estimatedexpense='"+operationsIntelligence.getEstimatedexpense()+"' and estimatedrevenue='"+operationsIntelligence.getEstimatedrevenue()+"' and workorder='"+workOrder+"' and accountLineId = '"+operationsIntelligence.getAccountLineId()+"'"; 
									 }
									 
									 List costList1 = getSession().createSQLQuery(costQuery1).list();
									 Iterator it=costList1.iterator();
									 listId=new  ArrayList(); 
									 accIdSet=new LinkedHashSet();
									 while(it.hasNext()){
											Object []row= (Object [])it.next();
											 estimatedbuyrate1 = Double.valueOf(row[2].toString());
											 estimatedSellRate1 = Double.valueOf(row[3].toString());
											 revisionSellRate1 = Double.valueOf(row[4].toString());
											 revisionBuyRate1 = Double.valueOf(row[5].toString());
											 listId.add(row[7].toString());
											 if(row[8] !=null && (!(row[8].toString().trim().equals("")))){
												 accIdSet.add(row[8].toString());
												 }
											 description1 = row[6].toString();
											 cost = 1.0;
										}
								 }
								 if (cost > 0) {
									 if(!type.equalsIgnoreCase("T") && !type.equalsIgnoreCase("Z") && !type.equalsIgnoreCase("F") && sameAccountLineId.trim().equalsIgnoreCase(tempAccountLineId))
										{
											tempForType="false";  
										}
									 if(tempForType.equalsIgnoreCase("true")) 
									 {
									 createAccount(accDay,operationsIntelligence,serviceOrder,chargeObj,corpId,contract,serviceOrder.getShipNumber(),serviceOrder.getId(),charge1,cost,divisionFlag,estimatedbuyrate1,estimatedSellRate1,revisionSellRate1,revisionBuyRate1,description1,listId,accIdSet);
									 }
									 
								 }
							 }
							 tempAccountLineId=sameAccountLineId;
					 }
				
		
		return chargeList;
	}
	
	
	public List createPrice(String corpId, String contract,String shipNumber,Long serviceOrderId,String divisionFlag) {
		String categoryResource = "'C','V'";
		List chargeList =null;
		
		/*String deletequery1="delete from accountline where serviceOrderId="+serviceOrderId+" and corpId='"+corpId+"' and shipNumber='" + shipNumber
		+"' and (categoryResource is not null or categoryResource !='') and (recInvoiceNumber is null or recInvoiceNumber = '') and revisionExpense =0" //and localAmount=0"
		+" and status="+true;*/
		
		/*String deletequery = "delete from accountline where serviceOrderId="+serviceOrderId+"  and shipNumber='"+shipNumber+"'"+
							 " and (categoryResource is not null or categoryResource !='')"+
							 " and (recInvoiceNumber is null or recInvoiceNumber = '')"+
							 " and createdBy like 'System%' and status=true ";
							
							 // " and revisionExpense =0"+
							 //" and actualRevenue=0"+
							// " and revisionRevenueAmount=0"+
							// " and actualRevenue=0"+
							// " and actualExpense=0"+
							// " and distributionAmount=0"+
							 
							// " and status=true";
		getSession().createSQLQuery(deletequery).executeUpdate();*/
		getHibernateTemplate().bulkUpdate("update AccountLine set accountLineNumber='000' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where serviceOrderId="+serviceOrderId+"  and shipNumber='"+shipNumber+"' ");
		
		ServiceOrder serviceOrder = serviceOrderManager.get(serviceOrderId);
		String costQueryForWO = "select distinct workOrder from operationsintelligence where status is true and shipNumber='"+shipNumber+"' and ( quantitiy <> 0 or quantitiy <> null) and ticketStatus is null order by workOrder";
		List costList1 = getSession().createSQLQuery(costQueryForWO).list();
		Iterator it = costList1.iterator();
		while(it.hasNext())
		{
			String workOrder = it.next().toString();
		/* This is for Crew and Vehicle */
		String dayBaseQuery = "select id from operationsintelligence where status is true and  shipNumber='" + shipNumber + "' AND type ='C' and description is not null and quantitiy>0 and workorder='"+workOrder+"' order by accountLineId";
		chargeList = createPriceCall(serviceOrder,dayBaseQuery,corpId,contract,"PERDAY","C",divisionFlag,workOrder);
		
		/* This is for Third Party */
		String tempQueryForTP = "select id from operationsintelligence where status is true and shipNumber='" + shipNumber + "' AND type ='T' and description is not null and quantitiy>0 and workorder='"+workOrder+"' order by accountLineId";
		chargeList = createPriceCall(serviceOrder,tempQueryForTP,corpId,contract,"","T",divisionFlag,workOrder);
		
		/* This is for Equipment */
		
		String tempQueryForEquip = "select id from operationsintelligence where status is true and shipNumber='" + shipNumber + "' AND type ='E'  and description is not null and quantitiy>0 and workorder='"+workOrder+"' order by accountLineId";
		chargeList = createPriceCall(serviceOrder,tempQueryForEquip,corpId,contract,"PERDAY","E",divisionFlag,workOrder);
		
		/* This is for Vechile */
		
		String tempQueryForV = "select id from operationsintelligence where status is true and shipNumber='" + shipNumber + "' AND type ='V'  and description is not null and quantitiy>0 and workorder='"+workOrder+"' order by accountLineId";
		chargeList = createPriceCall(serviceOrder,tempQueryForV,corpId,contract,"PERDAY","V",divisionFlag,workOrder);
		
		/* This is for Material */
		
		String tempQueryForMaterial = "select id from operationsintelligence where status is true and shipNumber='" + shipNumber + "' AND type ='M'  and description is not null and quantitiy>0 and workorder='"+workOrder+"' order by accountLineId";
		chargeList = createPriceCall(serviceOrder,tempQueryForMaterial,corpId,contract,"PERDAY","M",divisionFlag,workOrder);
		
       /* This is for Mainfreight */
		
		String tempQueryForMainfreight = "select id from operationsintelligence where status is true and shipNumber='" + shipNumber + "' AND type ='Z'  and description is not null and quantitiy>0 and workorder='"+workOrder+"' order by accountLineId";
		chargeList = createPriceCall(serviceOrder,tempQueryForMainfreight,corpId,contract,"","Z",divisionFlag,workOrder);
	
	   /* This is for Fees */
	
		String tempQueryForFees = "select id from operationsintelligence where status is true and shipNumber='" + shipNumber + "' AND type ='F'  and description is not null and quantitiy>0 and workorder='"+workOrder+"' order by accountLineId";
		chargeList = createPriceCall(serviceOrder,tempQueryForFees,corpId,contract,"","F",divisionFlag,workOrder);
		}
		
		//		 deleteAccount(list,accountList);
		List idList = getHibernateTemplate().find("select id from AccountLine where serviceOrderId="+serviceOrderId+" and status=true and shipNumber='"+shipNumber+"' and accountLineNumber='000'  ");
		Iterator itAcc=idList.iterator();
		String userName = getRequest().getRemoteUser();
		 while(itAcc.hasNext()){
			  Long id=(Long)itAcc.next(); 
			  AccountLine accountLineOld = accountLineManager.get(id);
			  List maxLineNumber = serviceOrderManager.findMaximumLineNumber(shipNumber); 
			  String accountLineNumber = "";
		        if ( maxLineNumber.get(0) == null ) {          
		         	accountLineNumber = "001";
		         }else { 
		        	 Long autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
		             if((autoLineNumber.toString()).length() == 2) {
		             	accountLineNumber = "0"+(autoLineNumber.toString());
		             }
		             else if((autoLineNumber.toString()).length() == 1) {
		             	accountLineNumber = "00"+(autoLineNumber.toString());
		             } 
		             else {
		             	accountLineNumber=autoLineNumber.toString();
		             }
		         } 
		        accountLineOld.setAccountLineNumber(accountLineNumber);
		        accountLineOld=accountLineManager.save(accountLineOld); 
		 }
		 setServiceOrderExpenseRevenueMargin(corpId,shipNumber,serviceOrderId);
		return chargeList;
	}
	
	public void deleteAccount(List<ItemsJbkEquip> list,List<AccountLine> accountList){
		List list1 = new ArrayList();
		Boolean flag = false;
		Long id = null;
		for (ItemsJbkEquip jbkEquip : list) {
			for (AccountLine accountLine : accountList) {
				 String jEquipCategoryResouce = jbkEquip.getType()+"-"+jbkEquip.getDescript();
				 String accCategory= accountLine.getCategoryResourceName();
				 id = accountLine.getId();
				if(jEquipCategoryResouce.trim().equals(accCategory.trim())){
					flag = true;
					break;
				}
			}
			if(!flag && accountList.size() > 0){
				list1.add(id);
			}
		}
		String li = list1.toString().replace("[", "").replace("]","");
		String del = "delete from accountline where id in ("+li+")";
		getSession().createSQLQuery("").executeUpdate();
	}
	public void updateAccount(ItemsJbkEquip jbkEquip,ServiceOrder serviceOrder,Charges chargeObj,String corpId, String contract,String shipNumber,Long serviceOrderId,String charge){
		
	}
	 
	public void createAccount(String accDay,OperationsIntelligence operationsIntelligence,ServiceOrder serviceOrder,Charges chargeObj,String corpId, String contract,String shipNumber,Long serviceOrderId,String charge,Double cost,String divisionFlag,Double estimatedbuyrate1,Double estimatedSellRate1,Double revisionSellRate1,Double revisionBuyRate1,String description1,List listId,Set accIdSet)
	{
		boolean costElementFlag = getCostElementFlag(corpId);
		
		try{ 
		if(accIdSet != null && (!(accIdSet.isEmpty())) && accIdSet.size()>0 && (!(accIdSet.toString().trim().equals("")))){
			String accId = accIdSet.toString().replace("[","").replace("]", "");
			accountLine=accountLineManager.get(Long.parseLong(accId));
		   if(accountLine != null && (accountLine.getRecInvoiceNumber() ==null || (accountLine.getRecInvoiceNumber().trim().equals("")))){
				 
			 }else{
				 accountLine = new AccountLine();
				}
		}else{
		 accountLine = new AccountLine();
		}
		}catch(Exception e){
			accountLine = new AccountLine();
		}
		 accountLine.setShipNumber(shipNumber);
		 accountLine.setCorpID(corpId);
		 accountLine.setCategory("Revenue");
		 accountLine.setContract(contract);
		 accountLine.setChargeCode(charge);
		 accountLine.setStatus(true);
		 accountLine.setDisplayOnQuote(true);
		 accountLine.setServiceOrderId(serviceOrderId);
		 accountLine.setServiceOrder(serviceOrder);
		 accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
		 accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
		 accountLine.setQuoteDescription(description1);
		 accountLine.setVATExclude(chargeObj.getVATExclude());
		 accountLine.setContractCurrency(chargeObj.getContractCurrency());
		 accountLine.setEstimateContractCurrency(chargeObj.getContractCurrency());
		 accountLine.setRevisionContractCurrency(chargeObj.getContractCurrency());
		 accountLine.setContractValueDate(new Date());
		 accountLine.setEstimateContractValueDate(new Date());
		 accountLine.setRevisionContractValueDate(new Date());
		 
		
		 systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(corpId).get(0);
		 ServiceOrder soObj = serviceOrderManager.get(serviceOrderId);
		 trackingStatus=trackingStatusManager.get(serviceOrderId);
		 miscellaneous = miscellaneousManager.get(serviceOrderId);
		 try{
			///Code for AccountLine division 
			if((divisionFlag!=null)&&(!divisionFlag.equalsIgnoreCase(""))){
			 String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),corpId);
			
		try{  
				String roles[] = agentRoleValue.split("~");
		          if(roles[3].toString().equals("hauler")){
			           if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
			        	   accountLine.setDivision("01");
			           }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
			        	   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
			        		   accountLine.setDivision("01");
			        	   }else{ 
				        	   String divisionTemp="";
								if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
								divisionTemp=refMasterManager.findDivisionInBucket2(corpId,serviceOrder.getJob());
								}
								if(!divisionTemp.equalsIgnoreCase("")){
									accountLine.setDivision(divisionTemp);
								}else{
									accountLine.setDivision(null);
								}
				             
			        	   }
			           }else{
			        	   String divisionTemp="";
							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
							divisionTemp=refMasterManager.findDivisionInBucket2(corpId,serviceOrder.getJob());
							}
							if(!divisionTemp.equalsIgnoreCase("")){
								accountLine.setDivision(divisionTemp);
							}else{
								accountLine.setDivision(null);
							}
			           }
		          }else{
		        	  String divisionTemp="";
						if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
						divisionTemp=refMasterManager.findDivisionInBucket2(corpId,serviceOrder.getJob());
						}
						if(!divisionTemp.equalsIgnoreCase("")){
							accountLine.setDivision(divisionTemp);
						}else{
							accountLine.setDivision(null);
						}
		          }
			}catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
			///Code for AccountLine division
		 }catch(Exception e){}
		 if(!costElementFlag){
			 accountLine.setPayGl(chargeObj.getExpGl());
			 accountLine.setRecGl(chargeObj.getGl());
		 }else{
			 String chargeStr="";
			 //ServiceOrder soObj = serviceOrderManager.get(serviceOrderId);
				List glList = accountLineManager.findChargeDetailFromSO(contract,charge,corpId,soObj.getJob(),soObj.getRouting(),soObj.getCompanyDivision());
				if(glList!=null && !glList.isEmpty() && glList.get(0)!=null && !glList.get(0).toString().equalsIgnoreCase("NoDiscription")){
					  chargeStr= glList.get(0).toString();
				  }
				   if(!chargeStr.equalsIgnoreCase("")){
					  String [] chrageDetailArr = chargeStr.split("#");
					  accountLine.setRecGl(chrageDetailArr[1]);
					  accountLine.setPayGl(chrageDetailArr[2]);
					}else{
					  accountLine.setRecGl("");
					  accountLine.setPayGl("");
				  }
		 }
		 
		 List contractCurrencyExchangeRateList= this.getSession().createSQLQuery("select baseCurrencyRate from exchangerate e where currency = '"+chargeObj.getContractCurrency()+"' and corpID='"+corpId+"' and currencyBaseRate is not null and valueDate =(select max(valueDate) from exchangerate e2 where e.currency = e2.currency and corpID='"+corpId+"' and date_format(valueDate,'%Y-%m-%d') <= date_format(now(),'%Y-%m-%d')) limit 1").list();
			if(contractCurrencyExchangeRateList != null && (!(contractCurrencyExchangeRateList.isEmpty())) && contractCurrencyExchangeRateList.get(0)!= null && (!(contractCurrencyExchangeRateList.get(0).toString().equals("")))){
				try{
				accountLine.setContractExchangeRate(new BigDecimal(contractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setEstimateContractExchangeRate(new BigDecimal(contractCurrencyExchangeRateList.get(0).toString()));
				accountLine.setRevisionContractExchangeRate(new BigDecimal(contractCurrencyExchangeRateList.get(0).toString()));
				}catch(Exception e){
				accountLine.setContractExchangeRate(new BigDecimal(1.0000)); 
				accountLine.setEstimateContractExchangeRate(new BigDecimal(1.0000));
				accountLine.setRevisionContractExchangeRate(new BigDecimal(1.0000));
				}
			}else{
				accountLine.setContractExchangeRate(new BigDecimal(1.0000));
				accountLine.setEstimateContractExchangeRate(new BigDecimal(1.0000));
				accountLine.setRevisionContractExchangeRate(new BigDecimal(1.0000));
			}
		 accountLine.setPayableContractCurrency(chargeObj.getPayableContractCurrency());
		accountLine.setEstimatePayableContractCurrency(chargeObj.getPayableContractCurrency());
		accountLine.setRevisionPayableContractCurrency(chargeObj.getPayableContractCurrency());
		accountLine.setPayableContractValueDate(new Date());
		accountLine.setEstimatePayableContractValueDate(new Date());
		accountLine.setRevisionPayableContractValueDate(new Date());
		List payableContractCurrencyExchangeRateList= this.getSession().createSQLQuery("select baseCurrencyRate from exchangerate e where currency = '"+chargeObj.getPayableContractCurrency()+"' and corpID='"+corpId+"' and currencyBaseRate is not null and valueDate =(select max(valueDate) from exchangerate e2 where e.currency = e2.currency and corpID='"+corpId+"' and date_format(valueDate,'%Y-%m-%d') <= date_format(now(),'%Y-%m-%d')) limit 1").list();
		if(payableContractCurrencyExchangeRateList != null && (!(payableContractCurrencyExchangeRateList.isEmpty())) && payableContractCurrencyExchangeRateList.get(0)!= null && (!(payableContractCurrencyExchangeRateList.get(0).toString().equals("")))){
			try{
			accountLine.setPayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
			accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
			accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(payableContractCurrencyExchangeRateList.get(0).toString()));
			}catch(Exception e){
			accountLine.setPayableContractExchangeRate(new BigDecimal(1.0000)); 
			accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(1.0000));
			accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(1.0000));
			}
		}else{
			accountLine.setPayableContractExchangeRate(new BigDecimal(1.0000));
			accountLine.setEstimatePayableContractExchangeRate(new BigDecimal(1.0000));
			accountLine.setRevisionPayableContractExchangeRate(new BigDecimal(1.0000));
		}
		List list2=this.getSession().createSQLQuery("select baseCurrency from companydivision where companyCode='"+serviceOrder.getCompanyDivision()+"' and corpID='"+corpId+"'").list();
		String baseCurrencyCompanyDivision="";
		if(!list2.isEmpty() && list2.get(0)!=null){
			baseCurrencyCompanyDivision= list2.get(0).toString();
		}
		
		String baseCurrency=accountLineManager.searchBaseCurrency(corpId).get(0).toString();
		String multiCurrency=accountLineManager.findmultiCurrency(corpId).get(0).toString();
		if(multiCurrency.equalsIgnoreCase("Y")){
			if(baseCurrencyCompanyDivision==null ||baseCurrencyCompanyDivision.equals(""))
			{
			accountLine.setRecRateCurrency(baseCurrency);
			accountLine.setRacValueDate(new Date());
			accountLine.setRecRateExchange(new BigDecimal(1.0000));
			accountLine.setEstSellCurrency(baseCurrency);
			accountLine.setEstSellValueDate(new Date());
			accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
			accountLine.setRevisionSellCurrency(baseCurrency);
			accountLine.setRevisionSellValueDate(new Date());
			accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
			}
			else
			{
			accountLine.setRecRateCurrency(baseCurrencyCompanyDivision);
			accountLine.setRacValueDate(new Date());
			accountLine.setEstSellCurrency(baseCurrencyCompanyDivision);
			accountLine.setEstSellValueDate(new Date());
			accountLine.setRevisionSellCurrency(baseCurrencyCompanyDivision);
			accountLine.setRevisionSellValueDate(new Date());
			List accExchangeRateList= this.getSession().createSQLQuery("select baseCurrencyRate from exchangerate e where currency = '"+baseCurrencyCompanyDivision+"' and corpID='"+corpId+"' and currencyBaseRate is not null and valueDate =(select max(valueDate) from exchangerate e2 where e.currency = e2.currency and corpID='"+corpId+"' and date_format(valueDate,'%Y-%m-%d') <= date_format(now(),'%Y-%m-%d')) limit 1").list();		
			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals(""))))
			{
				try{
				accountLine.setRecRateExchange(new BigDecimal(accExchangeRateList.get(0).toString()));
				accountLine.setEstSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				accountLine.setRevisionSellExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
				}catch(Exception e){
				accountLine.setRecRateExchange(new BigDecimal(1.0000)); 
				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
				}
			}else{
				accountLine.setRecRateExchange(new BigDecimal(1.0000));
				accountLine.setEstSellExchangeRate(new BigDecimal(1.0000));
				accountLine.setRevisionSellExchangeRate(new BigDecimal(1.0000));
			}
			}
			
		}
		if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
		{
		accountLine.setEstCurrency(baseCurrency);
		accountLine.setRevisionCurrency(baseCurrency);	
		accountLine.setCountry(baseCurrency);
		accountLine.setEstValueDate(new Date());
		accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
		accountLine.setRevisionValueDate(new Date());
		accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
		accountLine.setValueDate(new Date());
		accountLine.setExchangeRate(new Double(1.0));
		}
		else
		{
		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
		accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
		accountLine.setCountry(baseCurrencyCompanyDivision);
		accountLine.setEstValueDate(new Date());
		accountLine.setRevisionValueDate(new Date());
		accountLine.setValueDate(new Date());
		List accExchangeRateList = this.getSession().createSQLQuery("select baseCurrencyRate from exchangerate e where currency = '"+baseCurrencyCompanyDivision+"' and corpID='"+corpId+"' and currencyBaseRate is not null and valueDate =(select max(valueDate) from exchangerate e2 where e.currency = e2.currency and corpID='"+corpId+"' and date_format(valueDate,'%Y-%m-%d') <= date_format(now(),'%Y-%m-%d')) limit 1").list();		
		if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals(""))))
		{
			try{
			accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
			accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
			accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
			}catch(Exception e){
				accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
				accountLine.setExchangeRate(new Double(1.0));	
			}
		}else{
			accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
			accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
			accountLine.setExchangeRate(new Double(1.0));
		}
		}
		
		 accountLine.setCreatedOn(new Date());
		 accountLine.setCreatedBy("System :"+serviceOrder.getCreatedBy());
		 accountLine.setUpdatedOn(new Date());
		 accountLine.setUpdatedBy("System :"+serviceOrder.getUpdatedBy()); 
		   List maxLineNumber = serviceOrderManager.findMaximumLineNumber(shipNumber); 
		   String accountLineNumber = "";
		 if ( maxLineNumber.get(0) == null ) {          
          	accountLineNumber = "001";
          }else {
          	Long autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
              if((autoLineNumber.toString()).length() == 2) {
              	accountLineNumber = "0"+(autoLineNumber.toString());
              }
              else if((autoLineNumber.toString()).length() == 1) {
              	accountLineNumber = "00"+(autoLineNumber.toString());
              } 
              else {
              	accountLineNumber=autoLineNumber.toString();
              } 
		 }
		 accountLine.setAccountLineNumber(accountLineNumber);
		 accountLine.setEstimateQuantity(new BigDecimal(cost));
		 accountLine.setRevisionQuantity(new BigDecimal(cost));
		 //accountLine.setCategoryResource("YES");
		 accountLine.setCategoryResource(accDay);
		 accountLine.setCategoryResourceName(operationsIntelligence.getType()+"-@-"+operationsIntelligence.getDescription());
		 
		 accountLine.setBasis("flat");
		 accountLine.setEstimateRate(new BigDecimal(estimatedbuyrate1));
		 accountLine.setRevisionRate(new BigDecimal(revisionBuyRate1));
		 
		 String priceType = chargeObj.getPriceType();
		 /*if("FromField".equals(priceType)){
			 String priceSource =chargeObj.getPriceSource();
			 if(priceSource.length() > 1){
				 int index = priceSource.indexOf('.');
				 String tableName = priceSource.substring(0, index);
				 String colName = priceSource.substring(index+1, priceSource.length());
				 
				 String query3 = "select "+colName+" from "+tableName+" where corpId='"+corpId+"' and shipNumber='"+shipNumber+"'";
				 List li = getSession().createSQLQuery(query3).list();
				 if(li.get(0) != null){
					 accountLine.setEstimateSellRate(new BigDecimal(li.get(0).toString()));
				 }
			 }
		 }else if("Preset".equals(priceType)){
			 accountLine.setEstimateSellRate(chargeObj.getQuantity2preset());
		 }else if("RateGrid".equals(priceType)){
			 String rateGrid = "from RateGrid where corpId='"+corpId+"' and charge='"+chargeObj.getId()+"' and contractName='"+contract+"' and quantity1 >= "+cost+" order by quantity1";
			 List<RateGrid> rateGd = getHibernateTemplate().find(rateGrid);
			 if(rateGd.size() > 0 && rateGd.get(0).getRate1() != null && !"".equals(rateGd.get(0).getRate1().trim())){
				 accountLine.setEstimateSellRate(new BigDecimal(rateGd.get(0).getRate1()));
			 }
		 }*/
		 accountLine.setEstimateSellRate(new BigDecimal(estimatedSellRate1));
		 accountLine.setRevisionSellRate(new BigDecimal(revisionSellRate1));
		 String basis = chargeObj.getBasis();
		 BigDecimal expense = new BigDecimal(0.0);
		 BigDecimal revenue = new BigDecimal(0.0);
		 BigDecimal expenseForRev = new BigDecimal(0.0);
		 BigDecimal revenueForRev = new BigDecimal(0.0);
		 
		 BigDecimal mk = new BigDecimal(0);
		 BigDecimal mkForRev = new BigDecimal(0);
		 BigDecimal quantity = new BigDecimal(cost);
		 BigDecimal buyRate = new BigDecimal(accountLine.getEstimateRate().toString());
		 BigDecimal sellRate = new BigDecimal(accountLine.getEstimateSellRate().toString());
		 
		 BigDecimal buyRateForRev = new BigDecimal(accountLine.getRevisionRate().toString());
		 BigDecimal sellRateForRev = new BigDecimal(accountLine.getRevisionSellRate().toString());
		 
		 if("cwt".equals(basis) || "%age".equals(basis)){
			 expense=((quantity.multiply(buyRate)).divide(new BigDecimal(100)));
			 revenue=((quantity.multiply(sellRate)).divide(new BigDecimal(100)));
			 expenseForRev=((quantity.multiply(buyRateForRev)).divide(new BigDecimal(100)));
			 revenueForRev=((quantity.multiply(sellRateForRev)).divide(new BigDecimal(100)));
			 accountLine.setEstimateExpense(expense);
			 accountLine.setEstimateRevenueAmount(revenue);
			 
			 accountLine.setRevisionExpense(expenseForRev);
			 accountLine.setRevisionRevenueAmount(revenueForRev);
			 
			 
		 }else if ("per 1000".equals(basis)) {
			 expense=((quantity.multiply(buyRate)).divide(new BigDecimal(1000)));
			 revenue=((quantity.multiply(sellRate)).divide(new BigDecimal(1000)));
			 expenseForRev=((quantity.multiply(buyRateForRev)).divide(new BigDecimal(1000)));
			 revenueForRev=((quantity.multiply(sellRateForRev)).divide(new BigDecimal(1000)));
			 accountLine.setEstimateExpense(expense);
			 accountLine.setEstimateRevenueAmount(revenue);
			 
			 accountLine.setRevisionExpense(expenseForRev);
			 accountLine.setRevisionRevenueAmount(revenueForRev);
		}else{
			expense=(quantity.multiply(buyRate));
			revenue=(quantity.multiply(sellRate));
			
			 expenseForRev=(quantity.multiply(buyRateForRev));
			 revenueForRev=(quantity.multiply(sellRateForRev));
			
			accountLine.setEstimateExpense(expense);
			accountLine.setEstimateRevenueAmount(revenue);
			
			 accountLine.setRevisionExpense(expenseForRev);
			 accountLine.setRevisionRevenueAmount(revenueForRev);
		}
		try {
				mk=((revenue.divide(expense,2, RoundingMode.HALF_UP)).multiply(new BigDecimal(100)));
				accountLine.setEstimatePassPercentage(Integer.valueOf(Math.round(mk.intValue())));
		}catch(Exception e){
			accountLine.setEstimatePassPercentage(0);
		}
			try{	
				mkForRev=((revenueForRev.divide(expenseForRev,2, RoundingMode.HALF_UP)).multiply(new BigDecimal(100)));
				accountLine.setRevisionPassPercentage(Integer.valueOf(Math.round(mkForRev.intValue())));
		} catch (Exception e) {
			accountLine.setRevisionPassPercentage(0);
		}
		accountLine=accountLineManager.save(accountLine);
		String operationsIntelligenceIds = listId.toString().replace("[","").replace("]", "");
		operationsIntelligenceManager.updateAccountLineId(serviceOrder.getShipNumber(),serviceOrder.getId(),operationsIntelligenceIds,accountLine.getId());
	}
	
	public void setServiceOrderExpenseRevenueMargin(String corpID,String shipNumber,Long serviceOrderId){
		String accQuery = "from AccountLine where corpId='"+corpID+"' and shipNumber='"+shipNumber+"' and serviceOrderId="+serviceOrderId+" and status is true";
		List<AccountLine> list = getHibernateTemplate().find(accQuery);
		if(list.size() > 0){
			BigDecimal totalExpense = new BigDecimal(0);
			BigDecimal totalRev = new BigDecimal(0);
			BigDecimal grossMg = new BigDecimal(0);
			BigDecimal grossMgPercentage = new BigDecimal(0);
			for (AccountLine accountLine : list) {
				totalExpense = totalExpense.add(accountLine.getEstimateExpense());
				totalRev = totalRev.add(accountLine.getEstimateRevenueAmount());
			}
			grossMg = totalRev.subtract(totalExpense);
			try {
			grossMgPercentage = (grossMg.divide(totalRev,2, RoundingMode.HALF_UP)).multiply(new BigDecimal(100));
			}catch (Exception e) {
				grossMgPercentage = new BigDecimal(0);
			}
			String SOquery = "update serviceorder "+
						"set estimatedtotalexpense="+totalExpense+",estimatedtotalrevenue="+totalRev+
						", estimatedGrossMargin ="+grossMg+", estimatedGrossMarginPercentage="+grossMgPercentage+
						" where id="+serviceOrderId;
			getSession().createSQLQuery(SOquery).executeUpdate();
		}
	}

	/*public AccountLineManager getAccountLineManager() {
		return accountLineManager;
	}*/

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	/*public ServiceOrderManager getServiceOrderManager() {
		return serviceOrderManager;
	}*/

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	
	public void setItemsJbkEquipManager(ItemsJbkEquipManager itemsJbkEquipManager) {
		this.itemsJbkEquipManager = itemsJbkEquipManager;
	}

	/*public ItemsJbkEquipManager getItemsJbkEquipManager() {
		return itemsJbkEquipManager;
	}*/
	public Boolean getCostElementFlag(String corpID) {
		List costElementList=getHibernateTemplate().find("select costElement from SystemDefault where corpID = ?", corpID);
			if((costElementList!=null)&&(!costElementList.isEmpty()))
			{
				if(costElementList.get(0).toString().equalsIgnoreCase("true"))
					return true;
				else
					return false; 
			}
			else
			{
			return false;	
			}
		}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setOperationsIntelligenceManager(
			OperationsIntelligenceManager operationsIntelligenceManager) {
		this.operationsIntelligenceManager = operationsIntelligenceManager;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
}
