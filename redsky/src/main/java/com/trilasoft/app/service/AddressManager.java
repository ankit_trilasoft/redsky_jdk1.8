
package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.Partner;

public interface AddressManager extends GenericManager<Partner, Long> {	
	
	public List search(String partnerCode,String originalCorpID,String accountLineBillingParam);
	public List searchBookerAddress(String shipNumberA);
	public List findOriginAddress(String sequenceNumber, String corpID);
	public List partnerAddressDetails(String partnerCode,String corpID);
	
}