package com.trilasoft.app.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Parameter {
	
	private static final String TYPE_DATE = "date";
	private static final String TYPE_STRING = "string";
	private static final String TYPE_NUMBER = "number";
	private String name;
	private String type;
	private String value;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Object getParameter(){
		Object returnObj = null;
		if (type.equals(TYPE_DATE)){
			SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
			try {
				returnObj = sdf.parse(value);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}	
		if (type.equals(TYPE_STRING)){
				returnObj = value;
			}
		if (type.equals(TYPE_NUMBER)){
			returnObj = value;
		}
		return returnObj;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/*
	 * parameterString is of the form
	 * <parameter type>:<parameter name>
	 * For example
	 * date:Date From
	 */
	public String getNameFromString(String parameterString) {
		return parameterString.substring(parameterString.indexOf(":")+1);
	}
	
	/*
	 * parameterString is of the form
	 * <parameter type>:<parameter name>
	 * For example
	 * date:Date From
	 */
	public String getTypeFromString(String parameterString) {
		return parameterString.substring(0, parameterString.indexOf(":"));
	}	

}
