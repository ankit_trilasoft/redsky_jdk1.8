package com.trilasoft.app.dao;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ItemData;


public interface ItemDataDao extends GenericDao<ItemData, Long>{

	public String weightAndVolumeFromRfMaster(String itemDescription);

	public void deleteFromItemData(Long id,int workTicketId);

	public void updateItemDataWeightAndVolume(String totalWeight,String totalVolume, Integer workTicketId,String totalEstimatedPieces,String totalEstimatedVolume,String totalEstimatedWeight);

	public List getAllHandOutList(String shipNumber);
	
	public List getAllHandOutList(String shipNumber, String itemDescription);
	
	public void updateHandoutList(String shipNumber, long handoutItemId,Integer ticket);
	
	public void updateAllHoTicketInHandOutList(String shipNumber, Integer ticket,Boolean releaseAllItemsValue);
	
	public String updateHoTicketByHo(String shipNumber, Long id,Boolean hoValue,Integer ticket);
	
	public Object getItemDataByLine(Long ticket,Integer line, String serviceType);
	
	public Integer getMaxLine(String shipNumber);
	
	public void saveNotes(Long id, Integer workTicketId, String valueOfNotes);
	
	public List getAllHOItemList(Long hoTicket, String shipNumber);
	
	public void updatetotalActQty(Integer workTicketId, String totalActualQty);
	
}
