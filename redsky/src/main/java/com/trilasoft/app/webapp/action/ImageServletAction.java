package com.trilasoft.app.webapp.action;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.service.MyFileManager;
public class ImageServletAction extends BaseAction implements Preparable {
	private String sessionCorpID;
	 private String imageURL;
	 private String contentType;
	 private String fileFileName;
	 private String eMail="";	
	 private MyFileManager myFileManager;
	 private String usertype;
	 private MyFile myFile;
	 public final static int BUFFER = 102400000;
	 static final Logger logger = Logger.getLogger(ImageServletAction.class);
	 Date currentdate = new Date();
	 InputStream fileInputStream;
	 public InputStream getFileInputStream() {
			return fileInputStream;
		}
	public void prepare() throws Exception { 
		
	}
		public ImageServletAction(){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal(); 
			this.sessionCorpID = user.getCorpID(); 
			usertype=user.getUserType();
		}
		private String myFileForVal;
		private String noteForVal;
		private String activeVal;
		private String secureVal;
		private String shipNumber;
		private String myFileDocUrl;
		private String sequenceNumber;
		private Long fileIdVal;
		private String myFileJspName;
		private Long acId;
		private Long sid;
		private boolean checkLHF;
		private String myFileFromVal;
		private String relatedDocsVal;
		private String ppTypeVal;
		private Long ppIdVal;
		@SkipValidation
		public String extractFileImageServlet() throws ServletException,IOException{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			 SortedMap myfileMap = new TreeMap();
			 List imageURLList = new ArrayList();
			 List contentTypeList = new ArrayList();
			 List fileFileNameList = new ArrayList();
			 HttpServletResponse response = getResponse();
			 ServletOutputStream out = response.getOutputStream();
			 String id = getRequest().getParameter("id");
			 String param = getRequest().getParameter("param");
			 String seqNo = getRequest().getParameter("seqNo");
			 String[] arrayid = id.split(",");
			 int arrayLength = arrayid.length;
			 logger.warn("arrayLength: "+arrayLength);
			 for (int i = 0; i < arrayLength; i++) {
				 id = arrayid[i];
				 if((id!=null)&&(!id.toString().equalsIgnoreCase(""))){
					 if(usertype!=null && usertype.equalsIgnoreCase("AGENT")){
						 myFile=myFileManager.getForOtherCorpid(Long.parseLong(id));
					 }else{
						 myFile=myFileManager.get(Long.parseLong(id));
					 }
					 Boolean visible=false;
				    if(usertype.equalsIgnoreCase("ACCOUNT") && (myFile.getIsAccportal() || myFile.getIsServiceProvider())){
				    	visible=true;
				    }
				    if((usertype.equalsIgnoreCase("AGENT") || usertype.equalsIgnoreCase("PARTNER")) && myFile.getIsPartnerPortal()){
				    	visible=true;
				    }
				    if(usertype.equalsIgnoreCase("CUSTOMER") && myFile.getIsCportal()){
				    	visible=true;
				    }
				    if(usertype.equalsIgnoreCase("DRIVER")){
				    	visible=true;
				    }				    
				    if(usertype.equalsIgnoreCase("USER")){
				    	visible=true;
				    }
				    if(visible){
				    imageURL = myFile.getLocation();
				    }
					contentType = myFile.getFileContentType();
					contentType = "application/octet-stream";
					fileFileName = myFile.getFileFileName();
					fileFileName=fileFileName.replaceAll(" ", "");
					myfileMap.put(imageURL, fileFileName);
					imageURLList.add(imageURL);
					contentTypeList.add(contentType);
					fileFileNameList.add(fileFileName);
				 }
			 }
			 logger.warn("arrayLength: "+arrayLength+"imageURL: "+imageURL+"contentType: "+contentType+"fileFileName: "+fileFileName);
			 String resultType="success";
			 try {
					if (param != null && param.equalsIgnoreCase("DWNLD")){
						try {
							 logger.warn("seqNo: "+seqNo);
			                response.setContentType("application/zip");
							response.setHeader("Content-Disposition","inline; filename=RedSkyFile_"+seqNo+".zip");
							BufferedInputStream origin = null;
							ServletOutputStream dest = response.getOutputStream();
							ZipOutputStream outZip = new ZipOutputStream(new BufferedOutputStream(dest));
							byte data[] = new byte[BUFFER];
							logger.warn("seqNo: "+seqNo);
							Iterator it = imageURLList.iterator();
							logger.warn("imageURLList size: "+imageURLList.size());
							while(it.hasNext()){
								  String filesLOC = (String)it.next();
								  if(!(new File(filesLOC).exists()))continue;
								  
								  FileInputStream fi = new FileInputStream(filesLOC);
								  origin = new BufferedInputStream(fi, BUFFER);
						            String fileNAME = "";
						            if(filesLOC.lastIndexOf("\\") != -1){
						          	  fileNAME = filesLOC.substring(filesLOC.lastIndexOf("\\")+1, filesLOC.length());
						            }
						            if(filesLOC.lastIndexOf("/") != -1){
						          	  fileNAME = filesLOC.substring(filesLOC.lastIndexOf("/")+1, filesLOC.length());
						            }
						            ZipEntry entry = new ZipEntry(fileNAME);
						            outZip.putNextEntry(entry);
						            int count;
						            while((count = origin.read(data, 0, BUFFER)) != -1) {
									          outZip.write(data, 0, count);
						            }
						            origin.close();
							}
							outZip.close();							
							logger.warn("imageURLList size: "+imageURLList.size());
						}catch (Exception e) {
							resultType="error";
							if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
								myFileDocUrl = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&shipNumber="+shipNumber+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
								myFileDocUrl = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&sequenceNumber="+sequenceNumber+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
								myFileDocUrl = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&shipNumber="+shipNumber+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
								myFileDocUrl = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&sequenceNumber="+sequenceNumber+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("AC")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("editAccountLine"))){
								myFileDocUrl = "editAccountLine.html?sid="+sid+"&id="+acId+"&checkLHF="+checkLHF+"&myFileForVal="+myFileForVal+"";
							}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("relatedFiles"))){
								myFileDocUrl = "relatedFiles.html?id="+fileIdVal+"&myFileFrom="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("relatedFiles"))){
								myFileDocUrl = "relatedFiles.html?id="+fileIdVal+"&myFileFrom="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
								myFileDocUrl = "basketFiles.html?id="+fileIdVal+"&myFileFrom="+myFileFromVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&relatedDocs="+relatedDocsVal+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
								myFileDocUrl = "basketFiles.html?id="+fileIdVal+"&myFileFrom="+myFileFromVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&relatedDocs="+relatedDocsVal+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("secureFiles"))){
								myFileDocUrl = "secureFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("secureFiles"))){
								myFileDocUrl = "secureFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("PO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
								myFileDocUrl = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("PO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
								myFileDocUrl = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("PO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
								myFileDocUrl = "basketFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&relatedDocs="+relatedDocsVal+"&active="+activeVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("AP")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("accountFiles"))){
								myFileDocUrl = "accountFiles.html?sid="+sid+"&seqNum="+sequenceNumber+"&myFileJspName="+myFileJspName+"";
							}else if((myFileForVal != null && myFileForVal.equals("SD")) ){
								myFileDocUrl = "redskyDashboard.html?sid="+sid;
							} else if((myFileForVal != null && myFileForVal.equals("AgentPayableProcessing")) ){
								myFileDocUrl = "approveRejectList.html?shipNumber="+shipNumber+"&decorator=popup&popup=true";
							}
							e.printStackTrace();
						}
					}else{
						logger.warn("imageURL: "+imageURL);
						try {
							fileInputStream = new FileInputStream(new File(imageURL));
							logger.warn("imageURL: "+imageURL);
							return SUCCESS;
						} catch (Exception e) {
							e.printStackTrace();
							int pos = imageURL.indexOf(myFile.getId().toString());
							if (pos > 0) {
								String exceptionDir = imageURL.substring(0, pos);
								System.out.println("exception directory " + exceptionDir);
								File f = new File(exceptionDir);
								File[] filesList = f.listFiles();
								for (File f1 : filesList) {
									if (f1.isFile() && f1.getName().contains(myFile.getId().toString())) {
										System.out.println("Exception File file name" + f1.getName());
										fileInputStream = new FileInputStream(new File(exceptionDir + f1.getName()));
										logger.warn("imageURL: " + imageURL);
										return SUCCESS;
									}
								}
							}
/*
							
							
							String exceptionDir = imageURL.substring(0,imageURL.indexOf(myFile.getId().toString()));
							System.out.println("exception directory "+exceptionDir);
							File f = new File (exceptionDir);
							File[] filesList = f.listFiles();
					        for(File f1 : filesList){
					            if(f1.isFile() && f1.getName().contains(myFile.getId().toString())){
					                System.out.println("Exception File file name"+f1.getName());
					                fileInputStream = new FileInputStream(new File(exceptionDir+f1.getName()));
					                logger.warn("imageURL: "+imageURL);
									return SUCCESS;
					            }
					        }*/
						
						resultType="error";
						if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
							myFileDocUrl = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&shipNumber="+shipNumber+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
							myFileDocUrl = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&sequenceNumber="+sequenceNumber+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
							myFileDocUrl = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&shipNumber="+shipNumber+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
							myFileDocUrl = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&sequenceNumber="+sequenceNumber+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("AC")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("editAccountLine"))){
							myFileDocUrl = "editAccountLine.html?sid="+sid+"&id="+acId+"&checkLHF="+checkLHF+"&myFileForVal="+myFileForVal+"";
						}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("relatedFiles"))){
							myFileDocUrl = "relatedFiles.html?id="+fileIdVal+"&myFileFrom="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("relatedFiles"))){
							myFileDocUrl = "relatedFiles.html?id="+fileIdVal+"&myFileFrom="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
							myFileDocUrl = "basketFiles.html?id="+fileIdVal+"&myFileFrom="+myFileFromVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&relatedDocs="+relatedDocsVal+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
							myFileDocUrl = "basketFiles.html?id="+fileIdVal+"&myFileFrom="+myFileFromVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&relatedDocs="+relatedDocsVal+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("secureFiles"))){
							myFileDocUrl = "secureFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("secureFiles"))){
							myFileDocUrl = "secureFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("PO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
							myFileDocUrl = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("PO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
							myFileDocUrl = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("PO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
							myFileDocUrl = "basketFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&relatedDocs="+relatedDocsVal+"&active="+activeVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"&myFileJspName="+myFileJspName+"";
						}else if((myFileForVal != null && myFileForVal.equals("AP")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("accountFiles"))){
							myFileDocUrl = "accountFiles.html?sid="+sid+"&seqNum="+sequenceNumber+"&myFileJspName="+myFileJspName+"";
						}
						else if((myFileForVal != null && myFileForVal.equals("SD")) ){
							myFileDocUrl = "redskyDashboard.html?sid="+sid;
						}else if((myFileForVal != null && myFileForVal.equals("AgentPayableProcessing")) ){
							myFileDocUrl = "approveRejectList.html?shipNumber="+shipNumber+"&decorator=popup&popup=true";
						}
						return (resultType.equalsIgnoreCase("error")?ERROR:SUCCESS);
						}
					}
				}catch (Exception e) {
					resultType="error";
					e.printStackTrace();
					if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
						myFileDocUrl = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&shipNumber="+shipNumber+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
						myFileDocUrl = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&sequenceNumber="+sequenceNumber+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
						myFileDocUrl = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&shipNumber="+shipNumber+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
						myFileDocUrl = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&sequenceNumber="+sequenceNumber+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("AC")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("editAccountLine"))){
						myFileDocUrl = "editAccountLine.html?sid="+sid+"&id="+acId+"&checkLHF="+checkLHF+"&myFileForVal="+myFileForVal+"";
					}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("relatedFiles"))){
						myFileDocUrl = "relatedFiles.html?id="+fileIdVal+"&myFileFrom="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("relatedFiles"))){
						myFileDocUrl = "relatedFiles.html?id="+fileIdVal+"&myFileFrom="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
						myFileDocUrl = "basketFiles.html?id="+fileIdVal+"&myFileFrom="+myFileFromVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&relatedDocs="+relatedDocsVal+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
						myFileDocUrl = "basketFiles.html?id="+fileIdVal+"&myFileFrom="+myFileFromVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&relatedDocs="+relatedDocsVal+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("SO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("secureFiles"))){
						myFileDocUrl = "secureFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("CF")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("secureFiles"))){
						myFileDocUrl = "secureFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("PO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
						myFileDocUrl = "myFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("PO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
						myFileDocUrl = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("PO")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
						myFileDocUrl = "basketFiles.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&relatedDocs="+relatedDocsVal+"&active="+activeVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("AP")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("accountFiles"))){
						myFileDocUrl = "accountFiles.html?sid="+sid+"&seqNum="+sequenceNumber+"&myFileJspName="+myFileJspName+"";
					}else if((myFileForVal != null && myFileForVal.equals("SD")) ){
						myFileDocUrl = "redskyDashboard.html?sid="+sid;
					}else if((myFileForVal != null && myFileForVal.equals("AgentPayableProcessing")) ){
						myFileDocUrl = "approveRejectList.html?shipNumber="+shipNumber+"&decorator=popup&popup=true";
					}
				}
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return (resultType.equalsIgnoreCase("error")?ERROR:SUCCESS);
		}
		
		public String getSessionCorpID() {
			return sessionCorpID;
		}
		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}
		public String getImageURL() {
			return imageURL;
		}
		public void setImageURL(String imageURL) {
			this.imageURL = imageURL;
		}
		public String getContentType() {
			return contentType;
		}
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		public String getFileFileName() {
			return fileFileName;
		}
		public void setFileFileName(String fileFileName) {
			this.fileFileName = fileFileName;
		}
		public String geteMail() {
			return eMail;
		}
		public void seteMail(String eMail) {
			this.eMail = eMail;
		}
		public void setMyFileManager(MyFileManager myFileManager) {
			this.myFileManager = myFileManager;
		}
		public MyFile getMyFile() {
			return myFile;
		}
		public void setMyFile(MyFile myFile) {
			this.myFile = myFile;
		}
		public String getUsertype() {
			return usertype;
		}
		public void setUsertype(String usertype) {
			this.usertype = usertype;
		}
		public String getMyFileForVal() {
			return myFileForVal;
		}
		public void setMyFileForVal(String myFileForVal) {
			this.myFileForVal = myFileForVal;
		}
		public String getNoteForVal() {
			return noteForVal;
		}
		public void setNoteForVal(String noteForVal) {
			this.noteForVal = noteForVal;
		}
		public String getActiveVal() {
			return activeVal;
		}
		public void setActiveVal(String activeVal) {
			this.activeVal = activeVal;
		}
		public String getSecureVal() {
			return secureVal;
		}
		public void setSecureVal(String secureVal) {
			this.secureVal = secureVal;
		}
		public String getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(String shipNumber) {
			this.shipNumber = shipNumber;
		}
		public String getMyFileDocUrl() {
			return myFileDocUrl;
		}
		public void setMyFileDocUrl(String myFileDocUrl) {
			this.myFileDocUrl = myFileDocUrl;
		}
		public String getSequenceNumber() {
			return sequenceNumber;
		}
		public void setSequenceNumber(String sequenceNumber) {
			this.sequenceNumber = sequenceNumber;
		}
		public Long getFileIdVal() {
			return fileIdVal;
		}
		public void setFileIdVal(Long fileIdVal) {
			this.fileIdVal = fileIdVal;
		}
		public String getMyFileJspName() {
			return myFileJspName;
		}
		public void setMyFileJspName(String myFileJspName) {
			this.myFileJspName = myFileJspName;
		}
		public Long getAcId() {
			return acId;
		}
		public void setAcId(Long acId) {
			this.acId = acId;
		}
		public Long getSid() {
			return sid;
		}
		public void setSid(Long sid) {
			this.sid = sid;
		}
		public boolean isCheckLHF() {
			return checkLHF;
		}
		public void setCheckLHF(boolean checkLHF) {
			this.checkLHF = checkLHF;
		}
		public String getMyFileFromVal() {
			return myFileFromVal;
		}
		public void setMyFileFromVal(String myFileFromVal) {
			this.myFileFromVal = myFileFromVal;
		}
		public String getRelatedDocsVal() {
			return relatedDocsVal;
		}
		public void setRelatedDocsVal(String relatedDocsVal) {
			this.relatedDocsVal = relatedDocsVal;
		}
		public String getPpTypeVal() {
			return ppTypeVal;
		}
		public void setPpTypeVal(String ppTypeVal) {
			this.ppTypeVal = ppTypeVal;
		}
		public Long getPpIdVal() {
			return ppIdVal;
		}
		public void setPpIdVal(Long ppIdVal) {
			this.ppIdVal = ppIdVal;
		}
}
