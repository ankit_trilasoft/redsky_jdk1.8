<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
  <%@ include file="/common/tooltip.jsp"%>
<head>   
    <title><fmt:message key="menuItemList.title"/></title>   
    <meta name="heading" content="<fmt:message key='menuItemList.heading'/>"/> 

<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style>
<style type="text/css">
#overlay111 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style> 
<script language="javascript" type="text/javascript">  
function requestedMenuItems(targetElement, evt,url,parentName){
	var permisionVal=0;
	 if((evt.checked==true)){permisionVal=2;}else{permisionVal=0;}	 
	
	populateListData();	
	if(parentName.trim()!="")
	{
	var agree = confirm(" Click Ok to update related page permissions\n[Clicking cancel will not update related page permissions\n you can choose individual pages to grant access to]");
	 if(agree) {
		 showOrHide(1);
		 document.forms['form2'].elements['childPageUpdateFlag'].value="YES";
		 var perRecTemp = document.forms['form2'].elements['perRec'].value;

	    var url = "updateSecurities.html";
	    var parameters = "decorator=simple&popup=true&perRec="+perRecTemp+"&menuId="+targetElement+"&menuRight="+permisionVal+"&menuUrl="+url;
	    http2.open("POST", url, true); 
	    http2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	    http2.setRequestHeader("Content-length", parameters .length);
	    http2.setRequestHeader("Connection", "close");
	    http2.onreadystatechange = handleHttpResponse2;
	    http2.send(parameters);
	    	   }else {
		   document.getElementById("loader").style.display = "block";
		   document.forms['form2'].elements['childPageUpdateFlag'].value="NO";
	       document.forms['form2'].action ="assignMenuItem.html?decorator=popup&popup=true&menuUrl="+url;
		   document.forms['form2'].submit();
		   
	   }
	}else{
		document.getElementById("loader").style.display = "block";
		   document.forms['form2'].elements['childPageUpdateFlag'].value="NO";
	       document.forms['form2'].action ="assignMenuItem.html?decorator=popup&popup=true&menuUrl="+url;
		   document.forms['form2'].submit();
		   showOrHide(0);
	}
}
function handleHttpResponse2(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();
     	
     	  showOrHide(0);
	}

}
function clear_fields(){
	document.forms['form2'].elements['parentN'].value = '';
	document.forms['form2'].elements['menuN'].value = '';
	document.forms['form2'].elements['titleName'].value = '';
	document.forms['form2'].elements['urlN'].value = '';
	document.forms['form2'].elements['pageUrl'].value = '';
	
}
function findMenuItemList(){
	document.getElementById("loader").style.display = "block";
    document.forms['form2'].action ="menuItems.html?decorator=popup&popup=true";
	document.forms['form2'].submit();
	
}
function getXMLHttpRequestObject()
{
  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
var http2 = getXMLHttpRequestObject();
    function populateListData(){
    	try{
    		  var role='${perRec}';
    			var permission=0;
    			 var newIds="";
    				<c:forEach items="${menuItems}" var="list1">
    					var sid="${list1.id}";
    					var url="${list1.url}";	
    					if(document.getElementById(sid).checked==true){permission=2;}else{permission=0;}
    						 if(newIds==''){   
    						     newIds=sid+"#"+permission+"#"+role+"#"+url;
    						 }else{
    						     newIds=newIds + ',' + sid+"#"+permission+"#"+role+"#"+url;
    						 }
    				</c:forEach>
    				document.forms['form2'].elements['requestedMIP'].value=newIds;
    		}catch(e){}	
    }
    function showOrHide(value) {
        if (value == 0) {
           	if (document.layers)
               document.layers["overlay111"].visibility='hide';
            else
               document.getElementById("overlay111").style.visibility='hidden';
       	}else if (value == 1) {
       		if (document.layers)
              document.layers["overlay111"].visibility='show';
           	else
              document.getElementById("overlay111").style.visibility='visible';
       	}
    }

 </script>  
 
</head>
<s:form id="form2" action="assignMenuItem.html?decorator=popup&popup=true" method="post">
<div id="overlay111">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
  
   </div>
   </div>
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
    <input type="button" class="cssbutton" value="Search" style="width:55px; height:25px;" onclick="findMenuItemList();"/>
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Parent Name</th>
		<th>Menu Name</th>		
		<th>Title</th>
		<th>URL</th>	
		<th>Page URL</th>	
		</tr>
		</thead>	
		<tbody>
				<tr>
  				    <td align="left">
					    <s:textfield name="parentN" required="true" cssClass="input-text" size="20"/>
					</td>
					<td align="left">
					    <s:textfield name="menuN" required="true" cssClass="input-text" size="20"/>
					</td>					
  				    <td align="left">
					    <s:textfield name="titleName" required="true" cssClass="input-text" size="20"/>
					</td>
					<td align="left">
					    <s:textfield name="urlN" required="true" cssClass="input-text" size="20"/>
					</td>					
					<td align="left">
					    <s:textfield name="pageUrl" required="true" cssClass="input-text" size="20"/>
					</td>					
				</tr>		
				<tr><td></td><td></td><td></td><td></td><td align="right" colspan="0"><c:out value="${searchbuttons}" escapeXml="false"/></td></tr>			
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<s:hidden name="id" value="<%=request.getParameter("id") %>" />
	<s:hidden name="requestedMIP" value=""/>
	<s:hidden name="perRec" value="<%=request.getParameter("perRec") %>" />
<s:hidden name="menuItem.id" value="%{menuItem.id}"/>
<s:hidden name="childPageUpdateFlag" value=""/>
<div id="Layer1">

<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  <ul id="newmnav">
  <li class="current"><a><span>Menu List</span></a></li>
  </ul>
</td>
<td style="padding-top:10px;">
<span style="font-weight: bold; font-size: 13px; padding: 20px 0pt 0pt 20px;">${perRec}</span>
</td>
</tr>
</tbody></table>
<s:set name="menuItems" value="menuItems" scope="request"/>   
<display:table name="menuItems" class="table" requestURI="" id="menuItemList" export="false" defaultsort="2" >
<%--
<display:column title="Select"><s:select name="menuItemPermissionList.permission" list="{'0','1','2'}" onchange="requestedMenuItems('menuItems.html?MIP=${menuItemList.id}&perMen=${menuItemPermissionList.permission}');" /></display:column><s:hidden name="user.id"/>

<display:column title="Select">${mps[menuItemList.id][1]}
</display:column>
<c:if test="${temp1 == temp}" >
<s:select name="MIP" list="{'0','1','2'}" onchange="requestedMenuItems('${menuItemList.id}',this);" value="${mps[mp][1]}"/>
</c:if>
<display:column title="Select">
<s:select name="MIP" list="{'0','1','2'}" onchange="requestedMenuItems('${menuItemList.id}',this);" />
</display:column>
--%>

<display:column title="Assign" sortable="true">
	<c:set var="selectPermission" value="false" />
	<c:forEach var="mp" begin="0" end="${mipCount}" step="1">
	<c:set var="temp" value="${menuItemList.id}" />
	<c:set var="temp1" value="${mps[mp][0]}" />

	<c:if test="${temp1 == temp}" >
	<c:if test="${mps[mp][1] == '2'}" >
		<c:set var="selectPermission" value="true" />
		</c:if>
	<c:if test="${mps[mp][1] != '2'}" >
		<c:set var="selectPermission" value="false" />
		</c:if>
	</c:if>
	<c:if test="${temp1 != temp && selectPermission==false}" >
		<c:set var="selectPermission" value="false" />
	</c:if>
	</c:forEach> 
	<script language="javascript" type="text/javascript">

	</script>
<c:set var="trimTextNew" value="${fn:trim(menuItemList.parentName)}"/>
<s:checkbox id="${menuItemList.id}" name="permision11" value="${selectPermission}" onclick="requestedMenuItems('${menuItemList.id}',this,'${menuItemList.url}','${trimTextNew}')" required="true" />
</display:column>
<%--<display:column title="Select"><s:checkbox name="MIP" fieldValue="${menuItemList.id}" onclick="requestedMenuItems(this);"/></display:column><s:hidden name="user.id"/> --%>
<display:column property="parentName" sortable="true" titleKey="menuItem.parentName" /><s:hidden name="user.id"/>   
<display:column property="name" sortable="true" titleKey="menuItem.name" />
<c:set var="str1" value="${menuItemList.title}"/>
  <c:forEach var="num1" items="${fn:split(str1, '~')}" varStatus="rowCounter">
	  <c:if test="${rowCounter.count == 1}">
	  	<c:set var="titleTemp" value="${num1}"/>
	  </c:if>
	  <c:if test="${rowCounter.count == 2}">
	  	<c:set var="childTemp" value="${num1}"/>
	  </c:if>
	  <c:if test="${rowCounter.count == 3}">
	  	<c:set var="roleTemp" value="${num1}"/>
	  </c:if>	  
  </c:forEach>
<display:column property="description" sortable="true" titleKey="menuItem.description" />
<display:column titleKey="menuItem.title" >
<c:out value="${titleTemp}"/>
</display:column>
<display:column property="url" sortable="true" titleKey="menuItem.url" />
<c:if test="${securityChecked == 'Y'}">
<display:column title="Assign&nbsp;ROLE" style="width:120px">
  <c:set var="checkParent" value="${fn:trim(menuItemList.parentName)}"/>
  	<c:if test="${checkParent != ''}">
		<c:out value="${roleTemp}"/>
	</c:if>
</display:column>
</c:if>
<display:column title="Page&nbsp;Security" >
 <c:set var="roleTemp" value=""/>
<c:set var="trimText" value="${fn:trim(menuItemList.parentName)}"/>
	<c:if test="${trimText != ''}">
	<c:if test="${childTemp == 'Y'}">
   <input type="button" style="color:green" class="cssbutton" onclick="javascript:openWindow('pageSecurities.html?perRec=${perRec}&menuUrl=${menuItemList.url}&menuId=${menuItemList.id}&menuName=${menuItemList.name}&parentN=${menuItemList.parentName}&decorator=popup&popup=true');document.getElementById('loader').style.display = 'block';" value="Page URLs"/>
   </c:if>		
	<c:if test="${childTemp != 'Y'}">
   <input type="button" class="cssbutton" onclick="javascript:openWindow('pageSecurities.html?perRec=${perRec}&menuUrl=${menuItemList.url}&menuId=${menuItemList.id}&menuName=${menuItemList.name}&parentN=${menuItemList.parentName}&decorator=popup&popup=true');document.getElementById('loader').style.display = 'block';" value="Page URLs"/>
   </c:if>		
	</c:if>
</display:column>
<display:column title="Assign Module" >
<c:set var="trimText" value="${fn:trim(menuItemList.parentName)}"/>
	<c:if test="${trimText != ''}">
	<input type="button" class="cssbutton" onclick="javascript:openWindow('moduleItems.html?perRec=${perRec}&decorator=popup&popup=true');" value="Assign Module"  />
	</c:if>
</display:column>
</display:table>   
</div> 

<%-- 

<c:forEach var="mp" begin="0" end="${mipCount}" step="1">
<c:forEach var="mi" begin="0" end="2" step="1">
${mps[mp][mi]}
</c:forEach> 
</c:forEach> 
--%>


<c:out value="${button1}" escapeXml="false" />  


<div id="loader" style="text-align:center; display:none">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>
</s:form>   
<script type="text/javascript">   
    //highlightTableRows("menuItemList");
 </script>
 <script>
setTimeout("showOrHide(0)",2000);
</script>
 <script language="javascript" type="text/javascript">
 try{
	 
populateListData();
 }catch(e){}
</script>
