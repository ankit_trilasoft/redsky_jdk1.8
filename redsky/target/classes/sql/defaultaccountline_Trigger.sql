DELIMITER $$
create trigger redsky.trigger_add_history_defaultaccountline
BEFORE UPDATE on redsky.defaultaccountline
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

       IF (OLD.corpID <> NEW.corpID ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
       select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
   END IF;


      IF (OLD.basis <> NEW.basis ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.basis~') into fieldNameList;
       select CONCAT(oldValueList,OLD.basis,'~') into oldValueList;
       select CONCAT(newValueList,NEW.basis,'~') into newValueList;
   END IF;



      
      IF (OLD.mode <> NEW.mode ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.mode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.mode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.mode,'~') into newValueList;
   END IF;
   
   IF (OLD.originCity <> NEW.originCity ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.originCity~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originCity,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originCity,'~') into newValueList;
   END IF;
   
   IF (OLD.destinationCity <> NEW.destinationCity ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.destinationCity~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationCity,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationCity,'~') into newValueList;
   END IF;



      
      IF (OLD.jobType <> NEW.jobType ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.jobType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.jobType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.jobType,'~') into newValueList;
   END IF;



      
      IF (OLD.serviceType <> NEW.serviceType ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.serviceType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceType,'~') into newValueList;
   END IF;



      
      IF (OLD.chargeBack <> NEW.chargeBack ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.chargeBack~') into fieldNameList;
       select CONCAT(oldValueList,OLD.chargeBack,'~') into oldValueList;
       select CONCAT(newValueList,NEW.chargeBack,'~') into newValueList;
   END IF;



      
      IF (OLD.categories <> NEW.categories ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.categories~') into fieldNameList;
       select CONCAT(oldValueList,OLD.categories,'~') into oldValueList;
       select CONCAT(newValueList,NEW.categories,'~') into newValueList;
   END IF;



      
      IF (OLD.route <> NEW.route ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.route~') into fieldNameList;
       select CONCAT(oldValueList,OLD.route,'~') into oldValueList;
       select CONCAT(newValueList,NEW.route,'~') into newValueList;
   END IF;



      
      IF (OLD.vendorCode <> NEW.vendorCode ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.vendorCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.vendorCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.vendorCode,'~') into newValueList;
   END IF;


      IF (OLD.vendorName <> NEW.vendorName ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.vendorName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.vendorName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.vendorName,'~') into newValueList;
   END IF;



      
      IF (OLD.contract <> NEW.contract ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.contract~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
       select CONCAT(newValueList,NEW.contract,'~') into newValueList;
   END IF;



      
      IF (OLD.chargeCode <> NEW.chargeCode ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.chargeCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.chargeCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.chargeCode,'~') into newValueList;
   END IF;



      
      IF (OLD.recGl <> NEW.recGl ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.recGl~') into fieldNameList;
       select CONCAT(oldValueList,OLD.recGl,'~') into oldValueList;
       select CONCAT(newValueList,NEW.recGl,'~') into newValueList;
   END IF;



      
      IF (OLD.payGl <> NEW.payGl ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.payGl~') into fieldNameList;
       select CONCAT(oldValueList,OLD.payGl,'~') into oldValueList;
       select CONCAT(newValueList,NEW.payGl,'~') into newValueList;
   END IF;

     IF (OLD.uploaddataflag <> NEW.uploaddataflag ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.uploaddataflag~') into fieldNameList;
       select CONCAT(oldValueList,OLD.uploaddataflag,'~') into oldValueList;
       select CONCAT(newValueList,NEW.uploaddataflag,'~') into newValueList;
   END IF;

      
      IF (OLD.billToCode <> NEW.billToCode ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.billToCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.billToCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.billToCode,'~') into newValueList;
   END IF;



      
      IF (OLD.billToName <> NEW.billToName ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.billToName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.billToName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.billToName,'~') into newValueList;
   END IF;



      
      IF (OLD.description <> NEW.description ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.description~') into fieldNameList;
       select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
       select CONCAT(newValueList,NEW.description,'~') into newValueList;
   END IF;



      
      IF (OLD.commodity <> NEW.commodity ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.commodity~') into fieldNameList;
       select CONCAT(oldValueList,OLD.commodity,'~') into oldValueList;
       select CONCAT(newValueList,NEW.commodity,'~') into newValueList;
   END IF;



      
      IF (OLD.packMode <> NEW.packMode ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.packMode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.packMode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.packMode,'~') into newValueList;
   END IF;



      
      IF (OLD.orderNumber <> NEW.orderNumber ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.orderNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.orderNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.orderNumber,'~') into newValueList;
   END IF;



      
      IF (OLD.markUp <> NEW.markUp ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.markUp~') into fieldNameList;
       select CONCAT(oldValueList,OLD.markUp,'~') into oldValueList;
       select CONCAT(newValueList,NEW.markUp,'~') into newValueList;
   END IF;



      
      IF (OLD.companyDivision <> NEW.companyDivision ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.companyDivision~') into fieldNameList;
       select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
       select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
   END IF;




      
      IF (OLD.estVatPercent <> NEW.estVatPercent ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.estVatPercent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estVatPercent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estVatPercent,'~') into newValueList;
   END IF;




    IF ((OLD.amount <> NEW.amount) or (OLD.amount is null and NEW.amount is not null)
      or (OLD.amount is not null and NEW.amount is null)) THEN
        select CONCAT(fieldNameList,'defaultaccountline.amount~') into fieldNameList;
        IF(OLD.amount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.amount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.amount is not null) then
            select CONCAT(oldValueList,OLD.amount,'~') into oldValueList;
        END IF;
        IF(NEW.amount is not null) then
            select CONCAT(newValueList,NEW.amount,'~') into newValueList;
        END IF;
   END IF;    
   

    IF ((OLD.quantity <> NEW.quantity) or (OLD.quantity is null and NEW.quantity is not null)
      or (OLD.quantity is not null and NEW.quantity is null)) THEN
        select CONCAT(fieldNameList,'defaultaccountline.quantity~') into fieldNameList;
        IF(OLD.quantity is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.quantity is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.quantity is not null) then
            select CONCAT(oldValueList,OLD.quantity,'~') into oldValueList;
        END IF;
        IF(NEW.quantity is not null) then
            select CONCAT(newValueList,NEW.quantity,'~') into newValueList;
        END IF;
   END IF;     


    IF ((OLD.rate <> NEW.rate) or (OLD.rate is null and NEW.rate is not null)
      or (OLD.rate is not null and NEW.rate is null)) THEN
        select CONCAT(fieldNameList,'defaultaccountline.rate~') into fieldNameList;
        IF(OLD.rate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.rate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.rate is not null) then
            select CONCAT(oldValueList,OLD.rate,'~') into oldValueList;
        END IF;
        IF(NEW.rate is not null) then
            select CONCAT(newValueList,NEW.rate,'~') into newValueList;
        END IF;
   END IF;     


    IF ((OLD.sellRate <> NEW.sellRate) or (OLD.sellRate is null and NEW.sellRate is not null)
      or (OLD.sellRate is not null and NEW.sellRate is null)) THEN
        select CONCAT(fieldNameList,'defaultaccountline.sellRate~') into fieldNameList;
        IF(OLD.sellRate is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.sellRate is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.sellRate is not null) then
            select CONCAT(oldValueList,OLD.sellRate,'~') into oldValueList;
        END IF;
        IF(NEW.sellRate is not null) then
            select CONCAT(newValueList,NEW.sellRate,'~') into newValueList;
        END IF;
   END IF;     


    IF ((OLD.estimatedRevenue <> NEW.estimatedRevenue) or (OLD.estimatedRevenue is null and NEW.estimatedRevenue is not null)
      or (OLD.estimatedRevenue is not null and NEW.estimatedRevenue is null)) THEN
        select CONCAT(fieldNameList,'defaultaccountline.estimatedRevenue~') into fieldNameList;
        IF(OLD.estimatedRevenue is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estimatedRevenue is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estimatedRevenue is not null) then
            select CONCAT(oldValueList,OLD.estimatedRevenue,'~') into oldValueList;
        END IF;
        IF(NEW.estimatedRevenue is not null) then
            select CONCAT(newValueList,NEW.estimatedRevenue,'~') into newValueList;
        END IF;
   END IF;     




    IF ((OLD.estVatAmt <> NEW.estVatAmt) or (OLD.estVatAmt is null and NEW.estVatAmt is not null)
      or (OLD.estVatAmt is not null and NEW.estVatAmt is null)) THEN
        select CONCAT(fieldNameList,'defaultaccountline.estVatAmt~') into fieldNameList;
        IF(OLD.estVatAmt is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.estVatAmt is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.estVatAmt is not null) then
            select CONCAT(oldValueList,OLD.estVatAmt,'~') into oldValueList;
        END IF;
        IF(NEW.estVatAmt is not null) then
            select CONCAT(newValueList,NEW.estVatAmt,'~') into newValueList;
        END IF;
   END IF;     



 
   
   IF (OLD.estVatDescr <> NEW.estVatDescr ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.estVatDescr~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estVatDescr,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estVatDescr,'~') into newValueList;
   END IF;

   IF (OLD.estExpVatPercent <> NEW.estExpVatPercent ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.estExpVatPercent~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estExpVatPercent,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estExpVatPercent,'~') into newValueList;
   END IF;

   IF (OLD.originCountry <> NEW.originCountry ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.originCountry~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originCountry,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originCountry,'~') into newValueList;
   END IF;

   IF (OLD.destinationCountry <> NEW.destinationCountry ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.destinationCountry~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationCountry,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationCountry,'~') into newValueList;
   END IF;

   IF (OLD.equipment <> NEW.equipment) THEN
       select CONCAT(fieldNameList,'defaultaccountline.equipment~') into fieldNameList;
       select CONCAT(oldValueList,OLD.equipment,'~') into oldValueList;
       select CONCAT(newValueList,NEW.equipment,'~') into newValueList;
   END IF;

   IF (OLD.estExpVatDescr <> NEW.estExpVatDescr ) THEN
       select CONCAT(fieldNameList,'defaultaccountline.estExpVatDescr~') into fieldNameList;
       select CONCAT(oldValueList,OLD.estExpVatDescr,'~') into oldValueList;
       select CONCAT(newValueList,NEW.estExpVatDescr,'~') into newValueList;
   END IF;


   CALL add_tblHistory (OLD.id,"defaultaccountline", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$

