package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.StorageLibrary;

public interface StorageLibraryDao extends GenericDao<StorageLibrary,Long> {

public List storageLibrarySearch(String storageId, String storageType,String storageMode);

public List storageLibraryList(String sessionCorpID);

public List findStorageLibraryList(String sessionCorpID);


public List storageLibraryUnitSearch(String storageId, String storageType,String storageMode);
public StorageLibrary getByStorageId(String storageId,String sessionCorpID);

public List findAllocateStorage(String sessionCorpID);

public List findStoLocRearrangeList(String sessionCorpID);

public List searchStoLocRearrangeList(String sessionCorpID, String locationId, String ticket);

}
