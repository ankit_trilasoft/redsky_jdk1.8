delimiter $$

CREATE DEFINER=`root`@`%` FUNCTION `commission_margin`(id bigint(20),Type char(2)) RETURNS decimal(18,2)
BEGIN
declare Var decimal(18,2);
    declare Newid bigint(20);

declare commissionMargin decimal(18,2);
declare commissionMarginPer decimal(18,2);

set Newid=id ;

if type ='M' then
select
Sum(a.ActualRevenue)-Sum(case when a.ActualExpense<>0 then a.ActualExpense else a.RevisionExpense end )
into commissionMargin
from accountline a inner join  charges c
where a.serviceOrderId=Newid  and c.corpid = a.corpid
and a.status = true and a.contract= c.contract and a.chargeCode= c.charge
and c.commissionable is true and c.commission is not null;

set Var= case when commissionMargin='' or commissionMargin is null then 0 else commissionMargin end  ;
end if;

if type ='mp' then
select round(((Sum(a.ActualRevenue)-Sum(case when a.ActualExpense<>0 then a.ActualExpense else a.RevisionExpense end ))/ case when Sum(a.ActualRevenue) <> 0 and  Sum(a.ActualRevenue)  <> 0.00 and Sum(a.ActualRevenue)  <> 0.0000  then Sum(a.ActualRevenue) else null end )*100,2) into commissionMarginPer
from accountline a inner join  charges c
where a.serviceOrderId=Newid  and c.corpid = a.corpid
and a.status = true and a.contract= c.contract and a.chargeCode= c.charge
and c.commissionable is true and c.commission is not null;

set Var= case when commissionMarginPer='' or commissionMarginPer is null then 0 else commissionMarginPer end ;
end if;


    RETURN var;

end $$
