<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Ops Calendar</title>
<meta name="heading" content="Ops Calendar" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />	
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
<style type="text/css"> 
	.input-textarea{border:1px solid #219DD1;color:#000000;	font-family:arial,verdana;	font-size:12px;	height:45px;text-decoration:none;	}
	.bgblue{background:url(images/blue_band.jpg); height: 30px; width:630px; background-repeat: no-repeat;font-family: Arial, Helvetica, sans-serif;font-size: 12px;
		font-weight: bold;	color: #007a8d; padding-left: 40px; }	
	div#bar{	z-index:999;background-color:#fff;	}
	.table {margin-bottom:6px !important;}
</style>

<script type="text/javascript">

function getSchedule(type){
	var hubID=document.forms['opsCalendarForm'].elements['hubID'].value;
	
	
	if((hubID!=null)&&(hubID.trim()!=''))
	{
		document.forms['opsCalendarForm'].elements['type'].value = type;		
		hubID = hubID.replace(',','');	
		hubID = hubID.trim(); 
		var url="opsCalendarList.html?decorator=popup&popup=true";
		document.forms['opsCalendarForm'].action = url;
		document.forms['opsCalendarForm'].submit();
	}else{
		alert("Kindly select Hub/Warehouse.");
	}
}


<%--function refresh(){
	alert("hi");
	var hubID=document.forms['opsCalendarForm'].elements['hubID'].value;
	var  type='';
	//document.forms['opsCalendarForm'].elements['fromDate'].value="${fDate}";

	//var fromDate=document.forms['opsCalendarForm'].elements['fromDate'].value;

	//var fromDate1="${fromDate}";
	//document.forms['opsCalendarForm'].elements['fromDate'].value=fromDate1;
	if((hubID!=null)&&(hubID.trim()!=''))
	{
var fromDate="${fDate}";
	alert("line1");
	hubID = hubID.replace(',','');	
	alert("line2");
	hubID = hubID.trim(); 
	alert("hjkl");
//	fromDate1="${paraMeter2}";
	location.href = "opsCalendarList.html?hubID="+hubID+"&fromDate="+fromDate+"&type="+type+"&decorator=popup&popup=true";
		//javascript:openWindow("printOpsCalendar.html?hubID="+hubID+"&fromDate="+fromDate+"&decorator=popup&popup=true",1000,600)
	}
	else{
		alert("Kindly select Hub/Warehouse.");
	}

	}--%>


	
	
	
	
	function currentRefresh()
{
		var hubID=document.forms['opsCalendarForm'].elements['hubID'].value; 
		var fromDate1=document.forms['opsCalendarForm'].elements['fromDate'].value; 
		//var fromDate="${paraMeter2}";
		
		hubID = hubID.replace(',','');	
	
		hubID = hubID.trim(); 
		//fromDate1="${paraMeter2}";
		if((hubID!=null)&&(hubID.trim()!=''))
  {
		location.href = "opsCalendarList.html?hubID="+hubID+"&fromDate="+fromDate1+"&type=refresh&decorator=popup&popup=true";
			//javascript:openWindow("printOpsCalendar.html?hubID="+hubID+"&fromDate="+fromDate+"&decorator=popup&popup=true",1000,600)
    }
		else
		{
			alert("Kindly select Hub/Warehouse.");
		}
}
	
function dynamicPricingForm(){
	location.href = 'dynamicPricingCalenderView.html?decorator=popup&popup=true';
}
function crewCalendarForm(){
	location.href = 'crewCalenderView.html?decorator=popup&popup=true';
}

function planningCalendar(){
	var checkForAppendFields = 'No';
	<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
		checkForAppendFields ="Yes";
	</configByCorp:fieldVisibility>
	location.href = 'planningCalendarList.html?decorator=popup&popup=true&checkForAppendFields='+checkForAppendFields;
}
function workPlanning(){
	location.href = 'workPlan.html?decorator=popup&popup=true';
}
function printOpsCalendar(){
 var hubID=document.forms['opsCalendarForm'].elements['hubID'].value;
	var fromDate=document.forms['opsCalendarForm'].elements['fromDate'].value;
	if((hubID!=null)&&(hubID.trim()!=''))
{
	javascript:openWindow("printOpsCalendar.html?hubID="+hubID+"&fromDate="+fromDate+"&decorator=popup&popup=true",1000,600)
}
	else
		{
		alert("Kindly select Hub/Warehouse.");
		}
		}
	
</script>
 
</head>
<%--
<script type="text/javascript">


function makeWarehouseList(tValue){

	
	var target=tValue;
	var key='';
	var value='';
	var lineFile='';
	var tempFile='';
	<c:forEach var="entry" items="${houseHub}">
     key="${entry.key}";
	value="${entry.value}";
	
	if(key.trim()==target.trim()){
		tempFile=value.split(",");
		
		for(var i=0;i<tempFile.length;i++){
			if(lineFile==''){
			
				value=tempFile[i];
				value=value.replace("'","");
				value=value.replace("'","");
				lineFile=value;
			}else{
				
				value=tempFile[i];
				value=value.replace("'","");
				value=value.replace("'","");
				lineFile=lineFile+","+value;				
			}
		}
	}
	</c:forEach>
	
	
	if(lineFile!=''){
	
		var finalList=lineFile.split(",");
		var targetElement = document.forms['opsCalendarForm'].elements['wHouse'];
		targetElement.length = finalList.length+1;
		document.forms['opsCalendarForm'].elements['wHouse'].options[0].text = '';
		document.forms['opsCalendarForm'].elements['wHouse'].options[0].value = '';
		for(i=0;i<finalList.length;i++){
			 key=finalList[i];
			 value=findValueInHouse(finalList[i]);			
			   document.forms['opsCalendarForm'].elements['wHouse'].options[i+1].text=value;
			   document.forms['opsCalendarForm'].elements['wHouse'].options[i+1].value=key;
		}
	}else{
		var targetElement = document.forms['opsCalendarForm'].elements['wHouse'];
		targetElement.length = houseLength()+1;
		document.forms['opsCalendarForm'].elements['wHouse'].options[0].text = '';
		document.forms['opsCalendarForm'].elements['wHouse'].options[0].value = '';
		var j=0;
		<c:forEach var="entry" items="${house}">
				key="${entry.key}";
				value="${entry.value}";
			   document.forms['opsCalendarForm'].elements['wHouse'].options[j+1].text=value;
			   document.forms['opsCalendarForm'].elements['wHouse'].options[j+1].value=key;
			   j++;
		</c:forEach>	
		document.forms['opsCalendarForm'].elements['wHouse'].value='';
	}
	<c:if test="${not empty wHouse}">
	document.forms['opsCalendarForm'].elements['wHouse'].value='${wHouse}';
	</c:if>
		
	
}
function houseLength(){
	var cont=0;
	var key='';
	<c:forEach var="entry" items="${house}">
	key="${entry.key}";
	if(key!=''){
		cont++;
	}	
	</c:forEach>	
	return cont;
}
function findValueInHouse(key1){
	var key='';
	var value='';
	<c:forEach var="entry" items="${house}">
	if(value==''){
		key="${entry.key}";
		if(key==key1){
		value="${entry.value}";	
		}
	}	
	</c:forEach>	
	return value;
}
<script type="text/javascript">
<c:if test="${empty hubID}">

document.forms['opsCalendarForm'].elements['hubID'].value='${defaultHub}';
</c:if>
<c:if test="${not empty hubID}">
document.forms['opsCalendarForm'].elements['hubID'].value='${hubID}';

	//makeWarehouseList('${hubID}');
</c:if>

</script> <%-- 
<script type="text/javascript">


<c:if test="${empty hubID}">

document.forms['opsCalendarForm'].elements['hubID'].value='${defaultHub}';
</c:if>
<c:if test="${empty whouse}">
document.forms['opsCalendarForm'].elements['hubID'].value='${whouse}';
</c:if>
<c:if test="${empty hubID}">

document.forms['opsCalendarForm'].elements['hubID'].value='${hubID}';
</c:if>
</script>--%>

<s:form name="opsCalendarForm" id="opsCalendarForm" action="opsCalendarList.html?decorator=popup&popup=true" method="post" validate="true">
<c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<fmt:formatDate var="fromFormattedValue" value="${fromDate}" pattern="${displayDateTimeFormat}"/>
<s:hidden name="fromDate" value="${fromFormattedValue}"/> 
<div id="bar"></div>
<s:hidden name="type"/>
<div id="layer1" style="width:100%; margin: 0px auto;">
	<div id="newmnav" style="float:left;">
	<ul>
		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Ops Calendar</span></a></li>
		<configByCorp:fieldVisibility componentId="component.field.Resource.DynamicPricingView">
		<li><a onclick="dynamicPricingForm();"><span>Price Calendar</span></a></li>
		</configByCorp:fieldVisibility>
		<c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
		<li><a onclick="crewCalendarForm();"><span>Crew Calendar</span></a></li>
		</c:if>
		<li><a onclick="workPlanning();"><span>Work Planning</span></a></li>
		<configByCorp:fieldVisibility componentId="component.field.Resource.OpsCalendarView">	
		<li><a onclick="planningCalendar();"><span>Planning Calendar</span></a></li>
		</configByCorp:fieldVisibility>	
		
	</ul>
	</div>
		<div style="float:right;position: relative;top: -8px;right: 20px;">
	<table class="" cellspacing="1" cellpadding="2" border="0" style="margin:0px" width="100%">
	<tr>		  			
	<c:if test="${operationChk=='H'}"> 

<td align="right" width="20px" class="listwhitetext">Hub</td>
<td align="left" class="listwhitetext" width="100px"><s:select cssClass="list-menu" name="hubID" list="%{hub}" cssStyle="width:175px;height:21px;" headerKey="" headerValue="" onchange="getSchedule('');"/></td>



</c:if>
<c:if test="${operationChk=='W'}">

<td align="right" width="63px" class="listwhitetext" >Warehouse</td>	
<td width="160px" > 
<s:select cssClass="list-menu" name="hubID" list="%{house}" cssStyle="width:175px;height:21px;" headerKey="" headerValue="" onchange="getSchedule('');"/>	 
</td>
</c:if>
		  		<!-- <td width="90"> <input type="button" class="cssbutton" style="margin-right: 5px;width:85px;" value="Reload" onclick="refreshCalendar();"/></td>
			<td><input type="button" class="cssbutton" style="margin-right:5px;width:65px;" value="Print" onclick="printPlanningCalendar();"/></td>	 -->
			<td width="70"><button type="button" class="btn btn-xs btn-success btn-blue" style="width:auto;padding:3px 8px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.16),0 2px 5px 0 rgba(0,0,0,.12) !important;"  title="Reload" onclick="currentRefresh();">Reload <i class="fa fa-refresh"></i></button></td>
		  <td><button type="button" class="btn btn-xs btn-success" style="width:auto;padding:6px 8px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.16),0 2px 5px 0 rgba(0,0,0,.12) !important;"  title="Print" onclick="printOpsCalendar();"><i class="fa fa-print"></i></button></td>  		

</tr>	 
</table>
	</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="left" >
<%-- <div id="liquid-round" >
<div class="top" style="margin-top: 10px;!margin-top:-4px;"><span></span></div>
<div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="" width="100%">		
	<tr><td height="5px">&nbsp;</td></tr>	
	<tr>		
	
	</tr>
	<tr><td height="20" class="listwhitetext" style="font-size:12px;">
		<c:if test="${from == 'list'}">
			Hub&nbsp;/&nbsp;WH Selected : <strong>
			<c:forEach var="entry" items="${distinctHubDescription}">
				<c:if test="${hubID==entry.key}">
					<c:out value="${entry.value}" />
						</c:if>
					</c:forEach>
			</strong>
		</c:if>
	</td></tr>	 				
</table> 
</div>
<div class="bottom-header" ><span></span></div>
</div> --%>
<c:if test="${from != 'list'}">
	<div id="liquid-round" >
<div class="top" style="margin-top: 10px;"><span></span></div>
<div class="center-content">
<div style="height:130px;"></div>
</div>
<div class="bottom-header" ><span></span></div>
</div>
</c:if>

<c:if test="${from == 'list'}">
	<div id="liquid-round" >
<div class="top" style="margin-top: 10px;"><span></span></div>
<div class="center-content">
		<div style="width:100%;margin:0px auto;text-align:left;padding:5px 0px;font-size:13px;">
			Hub&nbsp;/&nbsp;WH Selected : <strong>
			<c:forEach var="entry" items="${distinctHubDescription}">
				<c:if test="${hubID==entry.key}">
					<c:out value="${entry.value}" />
						</c:if>
					</c:forEach>
			</strong>
		</div>
		<display:table name="workList" class="table" requestURI="" id="workList"  pagesize="14">
		   <c:choose>
		    	<c:when test="${workList.day == 'Sunday' || workList.day == 'Saturday' || workList.holiday == '1' }">
		    		<display:column property="date" title="Date" sortable="true" style="color: #fff; background-color: #959695" format="{0,date,dd-MMM-yyyy}"/>
		   			<display:column property="day" sortable="true"  title="Day" style="width:30px; color: #fff; background-color: #959695"/>
		    		<display:column property="status" sortable="true" title="Status" style="width:20px;color: #fff; background-color: #959695"/>
		    		<c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
		    			<display:column property="availableCrew" sortable="true"  title="Available Crew" style="width:100px; color: #fff; background-color: #959695;text-align:right"/>
		    			<c:if test="${workList.crewUsed > workList.crewHubLimit}">
		    				<display:column property="crewUsed" sortable="true" title="Crew Used" style="width:100px;background:#FF3232;text-align:right"/>
		    			</c:if>
		    			<c:if test="${workList.crewUsed < workList.crewHubLimit}">
		    				<display:column property="crewUsed" sortable="true" title="Crew Used" style="width:100px;color: #fff; background-color: #959695;text-align:right"/>
		    			</c:if>
		    		</c:if>
		    		<display:column property="comment" sortable="true" maxLength="100" title="Comments" style="width:400px;color: #fff; background-color: #959695"/>
		    	</c:when>
		    	<c:otherwise>
		    		<display:column property="date" title="Date" sortable="true" style="width:70px" format="{0,date,dd-MMM-yyyy}"/>
		    		<display:column property="day" sortable="true"  title="Day" style="width:30px"/>
		    		<display:column property="status" sortable="true" title="Status" style="width:20px"/>	
		    		<c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
		    			<display:column property="availableCrew" sortable="true"  title="Available Crew" style="width:100px;text-align:right"/>
		    			<c:if test="${workList.crewUsed > workList.crewHubLimit}">
		    				<display:column property="crewUsed" sortable="true" title="Crew Used" style="width:100px;background:#FF3232;text-align:right"/>
		    			</c:if>
		    			<c:if test="${workList.crewUsed < workList.crewHubLimit}">
		    				<display:column property="crewUsed" sortable="true" title="Crew Used" style="width:100px;text-align:right"/>
		    			</c:if>
		    		</c:if>
		    		<display:column property="comment" sortable="true" maxLength="100" title="Comments" style="width:400px"/>
   		    	</c:otherwise>
		   </c:choose>
		    
		   <display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
		   <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
		   <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/> 
		</display:table>
	<div style="float:left;">
	<table style="margin-bottom:6px !important;" class="" cellpadding="0" cellspacing="0" >
		<tbody>  
			<!-- <tr class="colored_bg" height="30px">
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">
					<input type="button" name="previous" class="cssbutton" style="width:115px; margin-left:5px;" align="top" value="Previous 14 Days" onclick="getSchedule('previous');"/>   
				</td>
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">
					<input type="button" name="Close" class="cssbutton" style="width:105px; margin-left:5px;" align="top" value="Close" onclick="window.close();"/>   
				</td>
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">
					<input type="button" name="next" class="cssbutton" style="width:105px; margin-left:5px;" align="top" value="Next 14 Days" onclick="getSchedule('next');"/>   
				</td>
			</tr> -->
			<tr height="30px">
				<td class="listwhitetext">					   
					<button type="button" name="previous" class="btn btn-xs btn-success" style="width:auto;padding:6px 8px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.16),0 2px 5px 0 rgba(0,0,0,.12) !important;"  title="" onclick="getSchedule('previous');"><i class="fa fa-angle-double-left"></i> Previous 14 Days</button></td>
				</td>				
				<td class="listwhitetext" style="padding-left:20px;">
				<button type="button" name="next" class="btn btn-xs btn-success" style="width:auto;padding:6px 18px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.16),0 2px 5px 0 rgba(0,0,0,.12) !important;"  title="" onclick="getSchedule('next');">Next 14 Days <i class="fa fa-angle-double-right"></i></button></td>					   
				</td>				
			</tr>	
		</tbody>
	</table>
	</div>
	<div style="float:right;">
	<button type="button" name="Close" class="btn btn-xs btn-danger" style="width:auto;padding:6px 18px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.16),0 2px 5px 0 rgba(0,0,0,.12) !important;"  title="" onclick="window.close();">Close</button></td>
	</div>
	<div style="clear:both;"></div>
	</div>
<div class="bottom-header" style="margin-top:40px;"><span></span></div>
</div>
	
</c:if>  
</div>  
</div>
</s:form>