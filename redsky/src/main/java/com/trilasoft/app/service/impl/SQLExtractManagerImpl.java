package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.SQLExtractDao;
import com.trilasoft.app.model.SQLExtract;
import com.trilasoft.app.service.SQLExtractManager;

public class SQLExtractManagerImpl extends GenericManagerImpl<SQLExtract, Long> implements SQLExtractManager {
	SQLExtractDao sqlExtractDao;   
	
	public SQLExtractManagerImpl(SQLExtractDao sqlExtractDao) {   
        super(sqlExtractDao);   
        this.sqlExtractDao = sqlExtractDao;   
    }

	public String testSQLQuery(String sqlQuery) {
		return sqlExtractDao.testSQLQuery(sqlQuery);
	}

	public List getAllList(String sessionCorpID) {
		return sqlExtractDao.getAllList(sessionCorpID);
	}
	public List getAllUserList(String sessionCorpID){
		return sqlExtractDao.getAllUserList(sessionCorpID);
	}
	public List sqlDataExtract(String sqlText) {
		return sqlExtractDao.sqlDataExtract(sqlText);
	}

	public List getAllCorpId() {
		return sqlExtractDao.getAllCorpId();
	}

	public List  getUniquename(String fileName) {
		return sqlExtractDao.getUniquename(fileName);
	}

	public List getSearch(String fileName, String corpID) {
		return sqlExtractDao.getSearch(fileName, corpID);
	}

	public List getSchedulerQueryList() {
		return sqlExtractDao.getSchedulerQueryList();
	}

	public List getSqlExtraxtSearch(String fileName, String description,String sessionCorpID) {
		return sqlExtractDao.getSqlExtraxtSearch(fileName, description,sessionCorpID);
	} 
	public List getAllSqlExtractFiles(){
		return sqlExtractDao.getAllSqlExtractFiles();
	}
	public List getAllListForTSFT() {
		return sqlExtractDao.getAllListForTSFT();
	}
	public List getSqlExtractByID(Long id,String sessionCorpID ) {
		return sqlExtractDao.getSqlExtractByID(id,sessionCorpID);
	}
	public int deleteSqlExtract(Long id){
				return sqlExtractDao.deleteSqlExtract(id);
	}
	
}
