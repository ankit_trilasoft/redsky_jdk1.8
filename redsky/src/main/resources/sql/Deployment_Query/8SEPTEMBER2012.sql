Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby) 
values ('TSFT', 'Defect', 'Defect', 25, 'CATEGORY','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Enhancement', 'Enhancement', 25, 'CATEGORY','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Task', 'Task', 25, 'CATEGORY','en',now(),'kkumar',now(),'kkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','CATEGORY',25,true,now(),'kkumar',now(),'kkumar',1,1,'');
 
ALTER TABLE `redsky`.`newsupdate` ADD COLUMN `status` BIT(1) DEFAULT b'0' AFTER `ticketNo`;

ALTER TABLE `redsky`.`newsupdate` ADD COLUMN `category` VARCHAR(80) DEFAULT NULL AFTER `status`;

ALTER TABLE `redsky`.`company` ADD COLUMN `ftpDir` VARCHAR(255) AFTER `automaticLinkup`;

ALTER TABLE `redsky`.`address` MODIFY COLUMN `state` VARCHAR(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;

ALTER TABLE `redsky`.`standardaddresses` MODIFY COLUMN `state` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

CREATE TABLE `commission` (
   `id` BIGINT(20) NOT NULL,
   `shipNumber` VARCHAR(20),
   `corpId` VARCHAR(15),
   `salesPerson` VARCHAR(8),
   `salesPersonAmount` DECIMAL(19,2) DEFAULT 0.00,
   `salesPersonPercentage` DECIMAL(19,2) DEFAULT 0.00,
   `settledDate` DATETIME DEFAULT NULL,
   `commissionLine1Id` BIGINT(20) UNSIGNED NOT NULL,
   `salesPersonSupplimentAmount1` DECIMAL(19,2) DEFAULT 0.00,
   `salesPersonSupplimentPercentage1` DECIMAL(19,2) DEFAULT 0.00,
   `salesPersonSupplimentAmount2` DECIMAL(19,2) DEFAULT 0.00,
   `salesPersonSupplimentPercentage2` DECIMAL(19,2) DEFAULT 0.00,
   `salesPersonSupplimentAmount3` DECIMAL(19,2) DEFAULT 0.00,
   `salesPersonSupplimentPercentage3` DECIMAL(19,2) DEFAULT 0.00,
   `consultant` VARCHAR(8),
   `consultantAmount` DECIMAL(19,2) DEFAULT 0.00,
   `consultantPercentage` DECIMAL(19,2) DEFAULT 0.00,
   `commissionLine2Id` BIGINT(20) NOT NULL,
   `consultantSupplimentAmount1` DECIMAL(19,2) DEFAULT 0.00,
   `consultantSupplimentPercentage1` DECIMAL(19,2) DEFAULT 0.00,
   `consultantSupplimentAmount2` DECIMAL(19,2) DEFAULT 0.00,
   `consultantSupplimentPercentage2` DECIMAL(19,2) DEFAULT 0.00,
   `consultantSupplimentAmount3` DECIMAL(19,2) DEFAULT 0.00,
   `consultantSupplimentPercentage3` DECIMAL(19,2) DEFAULT 0.00,
   PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

ALTER TABLE `commission` MODIFY COLUMN `commissionLine1Id` VARCHAR(25) DEFAULT NULL,  MODIFY COLUMN `commissionLine2Id` VARCHAR(25) DEFAULT NULL;

ALTER TABLE `commission` ADD COLUMN `createdBy` VARCHAR(45) AFTER `consultantSupplimentPercentage3`,  ADD COLUMN `updatedBy` VARCHAR(45) AFTER `createdBy`,  ADD COLUMN `createdOn` DATETIME AFTER `updatedBy`,  ADD COLUMN `updatedOn` DATETIME AFTER `createdOn`;

ALTER TABLE `commission` ADD COLUMN `commissionType` VARCHAR(5) AFTER `updatedOn`;

DROP TABLE IF EXISTS `redsky`.`emailsetup`;
CREATE TABLE  `redsky`.`emailsetup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `retryCount` int(10) DEFAULT NULL,
  `recipientTo` varchar(200) DEFAULT NULL,
  `recipientCc` varchar(200) DEFAULT NULL,
  `recipientBcc` varchar(200) DEFAULT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `body` varchar(1000) DEFAULT NULL,
  `signature` varchar(200) DEFAULT NULL,
  `attchedFileLocation` varchar(250) DEFAULT NULL,
  `dateSent` datetime DEFAULT NULL,
  `emailStatus` varchar(45) DEFAULT NULL,
  `corpId` varchar(30) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
 
ALTER TABLE `redsky`.`authorizationno` ADD COLUMN `workTicketNumber` BIGINT(20) NOT NULL AFTER `serviceOrderId`;

ALTER TABLE `redsky`.`authorizationno` MODIFY COLUMN `workTicketNumber` BIGINT(20) DEFAULT NULL;

ALTER TABLE `redsky`.`miscellaneous` ADD COLUMN `totalActualRevenue` DECIMAL(19,2) DEFAULT NULL AFTER `ugwIntId`;

ALTER TABLE `redsky`.`commission` MODIFY COLUMN `salesPerson` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,  MODIFY COLUMN `consultant` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL;

ALTER TABLE `redsky`.`commission` 
  DROP COLUMN `salesPersonSupplimentAmount1`,
  DROP COLUMN `salesPersonSupplimentPercentage1`,
  DROP COLUMN `salesPersonSupplimentAmount2`,
  DROP COLUMN `salesPersonSupplimentPercentage2`,
  DROP COLUMN `salesPersonSupplimentAmount3`,
  DROP COLUMN `salesPersonSupplimentPercentage3`,
  DROP COLUMN `consultantSupplimentAmount1`,
  DROP COLUMN `consultantSupplimentPercentage1`,
  DROP COLUMN `consultantSupplimentAmount2`,
  DROP COLUMN `consultantSupplimentPercentage2`,
  DROP COLUMN `consultantSupplimentAmount3`,
  DROP COLUMN `consultantSupplimentPercentage3`;

ALTER TABLE `redsky`.`commission` MODIFY COLUMN `id` BIGINT(20) NOT NULL AUTO_INCREMENT,  ADD COLUMN `actualRevenue` DECIMAL(19,2) AFTER `commissionType`,  ADD COLUMN `actualExpence` DECIMAL(19,2) AFTER `actualRevenue`,  ADD COLUMN `aid` BIGINT(20) NOT NULL AFTER `actualExpence`;


