<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
<head>   
    <title><fmt:message key="defaultAccountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='defaultAccountLineList.heading'/>"/>
    <style type="text/css">
    
	div#main {margin:-5px 0 0;!margin:0px;}
	 span.pagelinks {display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:0px;margin-top:0px;padding:1px 0px;!padding-left:50px;text-align:right;width:100%;!width:94%;}
	.table tbody td a {background:transparent none repeat scroll 0% 50%;cursor:pointer;text-decoration:underline;}
	.table td{padding:0.3em;}
	table-fc tbody th, .table tbody td {white-space:nowrap;}
	.table td, .table th, .tableHeaderTable td {padding: 0.3em;}
 	.pr-f11{	font-family:arial,verdana;	font-size:11px;	}
 	.head-bdr {border-right:medium solid #003366 !important;}
 	
</style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script language="javascript" type="text/javascript">
function clear_fields(){
			document.getElementById('defaultAccountLine.jobType').value = "";
			document.getElementById('defaultAccountLine.route').value = "";
			document.getElementById('defaultAccountLine.mode').value = "";
			document.getElementById('defaultAccountLine.serviceType').value = "";
			document.getElementById('defaultAccountLine.billToCode').value = "";
			document.getElementById('defaultAccountLine.contract').value = "";
			document.getElementById('defaultAccountLine.packMode').value = "";
			document.getElementById('defaultAccountLine.billToCode').value = "";
			<c:if test="${companyDivisionGlobal>1}">
			document.getElementById('defaultAccountLine.companyDivision').value = "";
			</c:if>
			document.getElementById('defaultAccountLine.commodity').value = "";
			document.getElementById('defaultAccountLine.originCountry').value = "";
			document.getElementById('defaultAccountLine.destinationCountry').value = "";
			document.getElementById('defaultAccountLine.equipment').value = "";
			}
			
			


function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Accounting Template?");
	var did = targetElement;
	var a= document.getElementById('defaultAccountLine.contract').value;
	var b= document.getElementById('defaultAccountLine.jobType').value;
	var c= document.getElementById('defaultAccountLine.route').value;
	var d= document.getElementById('defaultAccountLine.mode').value;
	var e= document.getElementById('defaultAccountLine.serviceType').value;
	var f= document.getElementById('defaultAccountLine.packMode').value;
	var g= document.getElementById('defaultAccountLine.commodity').value;
	var h= document.getElementById('defaultAccountLine.originCountry').value;
	var i= document.getElementById('defaultAccountLine.destinationCountry').value;
	var j= document.getElementById('defaultAccountLine.equipment').value;
	var k='';
	<c:if test="${companyDivisionGlobal>1}">
		k= document.getElementById('defaultAccountLine.companyDivision').value;
	</c:if>
	var l= document.getElementById('defaultAccountLine.billToCode').value;
	if (agree){
		location.href='defaultAccountLinedelete.html?id='+did+'&searchcontract='+a+'&searchjobType='+b+'&searchroute='+c+'&searchmode='+d+'&searchserviceType='+e+'&searchpackMode='+f+'&searchcommodity='+g+'&searchoriginCountry='+h+'&searchdestinationCountry='+i+'&searchequipment='+j+'&searchcompanyDivision='+k+'&searchbillToCode='+l;
	}else{
		return false;
	}
}		
			
			
</script>
<script type="text/javascript">
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http3 = getHTTPObject();
    var http5 = getHTTPObject();
    var httpCE = getHTTPObject();


function getUpdateListValue(id){
	
	if(document.getElementById("successMessages")!=null){
	document.getElementById("successMessages").style.display = 'none';
	}

	var d23='';
	var d24='';
	var d28='';
	var d29='';
	var d10='';
	 var d1=	document.getElementById('contract'+id).value;
	 var d2 = 	document.getElementById ('jobType'+id).value;
	 var d3 = 	document.getElementById ('mode'+id).value;
	 var d4 = 	document.getElementById ('billToCode'+id).value;
	 var d5 = 	document.getElementById ('billToName'+id).value;
	 var d6 = 	document.getElementById ('route'+id).value;
	 var d7 = 	document.getElementById ('packMode'+id).value;
	 var d8 = 	document.getElementById ('commodity'+id).value;
	 var d9=  	document.getElementById('serviceType'+id).value;
	 <c:if test="${companyDivisionGlobal>1}"> 
	  d10 = 	document.getElementById ('companyDivision'+id).value;
	 </c:if>
	 var d11 = 	document.getElementById ('equipment'+id).value;
	 var d12 = 	document.getElementById ('originCountry'+id).value;
	 var d13 = 	document.getElementById ('destinationCountry'+id).value;
	 var d14 = 	document.getElementById ('orderNumber'+id).value;
	 var d15 = 	document.getElementById ('categories'+id).value;
	 var d16 = 	document.getElementById ('basis'+id).value;
	 var d17=	document.getElementById('vendorCode'+id).value;
	 var d18 = 	document.getElementById ('vendorName'+id).value;
	 var d19 = 	document.getElementById ('chargeCode'+id).value;
	 var d20 = 	document.getElementById ('recGl'+id).value;
	 var d21 = 	document.getElementById ('payGl'+id).value;
	 var d22 = 	document.getElementById ('quantity'+id).value;
	 <c:if test="${systemDefaultVatCalculation=='true'}">
	  d23 = 	document.getElementById ('estVatDescr'+id).value;
	  d24 = 	document.getElementById ('estVatPercent'+id).value;
	 </c:if>
	 var d25=	document.getElementById('sellRate'+id).value;
	 var d26 = 	document.getElementById ('estimatedRevenue'+id).value;
	 var d27 = 	document.getElementById ('markUp'+id).value;
	 if(d27==''){
		 d27=0;
	 }
	 <c:if test="${systemDefaultVatCalculation=='true'}">
	  d28 = 	document.getElementById ('estExpVatDescr'+id).value;
	  d29 = 	document.getElementById ('estExpVatPercent'+id).value;
	 </c:if>
	 var d30 = 	document.getElementById ('rate'+id).value;
	 var d31 = 	document.getElementById ('amount'+id).value;
	 var d32 = 	document.getElementById ('description'+id).value;
	 var d33 = 	document.getElementById ('estVatAmt'+id).value;
	 var allValue=encodeURIComponent(id)+'@'+encodeURIComponent(d1)+'@'+encodeURIComponent(d2)+'@'+encodeURIComponent(d3)+'@'+encodeURIComponent(d4)+'@'+encodeURIComponent(d5)+'@'+encodeURIComponent(d6)+'@'+encodeURIComponent(d7)+'@'+encodeURIComponent(d8)+'@'+encodeURIComponent(d9)+'@'+encodeURIComponent(d10)+'@'+encodeURIComponent(d11)+'@'+encodeURIComponent(d12);
	 allValue=allValue+'@'+encodeURIComponent(d13)+'@'+encodeURIComponent(d14)+'@'+encodeURIComponent(d15)+'@'+encodeURIComponent(d16)+'@'+encodeURIComponent(d17)+'@'+encodeURIComponent(d18)+'@'+encodeURIComponent(d19)+'@'+encodeURIComponent(d20)+'@'+encodeURIComponent(d21)+'@'+encodeURIComponent(d22)+'@'+encodeURIComponent(d23)+'@'+encodeURIComponent(d24);
	 allValue=allValue+'@'+encodeURIComponent(d25)+'@'+encodeURIComponent(d26)+'@'+encodeURIComponent(d27)+'@'+encodeURIComponent(d28)+'@'+encodeURIComponent(d29)+'@'+encodeURIComponent(d30)+'@'+encodeURIComponent(d31)+'@'+encodeURIComponent(d32)+'@'+encodeURIComponent(d33);
if(document.getElementById('jobType'+id).value==""){
	 alert("Job Type is required field");
	  return false;
}
if(document.getElementById('contract'+id).value==""){
	alert("Contract is required field");
	  return false;
}



else{
	 updateDefaultAccountList(allValue);
	return true; 
	}
}

function showOrHide(value) {
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} } 

var http120 = getHTTPObject();

function updateDefaultAccountList(allValue){
	showOrHide(1);
	var url ='';
	if(allValue!=null){
		  url = "updateDefaultAccountListAjax.html?ajax=1&changerOrderList="+allValue;
		  http120.open("POST", url, true); 
		  http120.onreadystatechange = handleHttpResponse120;
		  http120.send(null);
	}else{
		
	}
}

function handleHttpResponse120(){
	if (http120.readyState == 4){		
            var results = http120.responseText
            showOrHide(0);
	}
}

function saveChanges(){
document.forms['defaultAccountLineForm'].action ='orderedDefaultAccountLines.html';
document.forms['defaultAccountLineForm'].submit(); 
document.forms['defaultAccountLineForm'].submit(); 

}
function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36) ; 
	}
	
function changeStatus(){
	document.forms['defaultAccountLineForm'].elements['formStatus'].value = '1';
}  

function valueChk(id){
	var contract = document.getElementById('contract'+id).value;
	if(contract=='' || contract==' '){
		document.getElementById('chargeCode'+id).value="";
        return false;
	}
	var chargeCode= document.getElementById('chargeCode'+id).value;
	if(chargeCode !=''){
		checkChargeCode(id);
	}	
}


function checkChargeCode(id){
	var chargeCodeValidationVal=document.getElementById('chargeCodeValidationVal').value;
	if(chargeCodeValidationVal=='' || chargeCodeValidationVal==null){
     var category =  document.getElementById('categories'+id).value;
    var chargeCode=  document.getElementById('chargeCode'+id).value;
    var contract = document.getElementById('contract'+id).value;
    var compDivis="";
    <c:if test="${companyDivisionGlobal>1}">
     compDivis=document.getElementById('companyDivision'+id).value;
	</c:if>
    var jobType=document.getElementById('jobType'+id).value;
    var routing=document.getElementById('route'+id).value;
	if(chargeCode!=''){
	if(category=='Internal Cost' && compDivis==''){
	alert("Please enter company division first.");
	document.getElementById('chargeCode'+id).value='';
	progressBarAutoSave('0');
    return false;
	}
	else if(category=='Internal Cost'){
//	 contract="Internal Cost Management";
//	 var url="defaultTemplateChargeCode.html?decorator=simple&popup=true&contract="+contract+"&chargeCode="+chargeCode; 
     var url="checkInternalCostChargeCode.html?ajax=1&decorator=simple&popup=true&accountCompanyDivision="+encodeURIComponent(compDivis)+"&chargeCode="+encodeURIComponent(chargeCode)+"&jobType="+encodeURIComponent(jobType)+"&routing="+encodeURIComponent(routing)+"&soCompanyDivision="+encodeURIComponent(compDivis); 
     http5.open("GET", url, true);
     http5.onreadystatechange = function(){ handleHttpResponse112('Internal',id);};
     http5.send(null);
	}else{ 
	if(contract=='' || contract==' '){
        document.getElementById('recGl'+id).value='';
        document.getElementById('payGl'+id).value='';
		document.getElementById('chargeCode'+id).value='';
		document.getElementById('description'+id).value='';
		document.getElementById('contract'+id).focus();
		alert("There is no pricing contract: Please select.");
		progressBarAutoSave('0');
		return false;
	} else {
	var url="defaultTemplateChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+contract+"&chargeCode="+chargeCode+"&jobType="+encodeURIComponent(jobType)+"&routing="+encodeURIComponent(routing)+"&soCompanyDivision="+encodeURIComponent(compDivis); 
    http5.open("GET", url, true);
    http5.onreadystatechange =function(){ handleHttpResponse112('No Internal',id);};
    http5.send(null);
    }
    }
}else{
	 document.getElementById('chargeCode'+id).value='';
	 document.getElementById('recGl'+id).value='';
     document.getElementById('payGl'+id).value='';
     document.getElementById('description'+id).value='';
}
	progressBarAutoSave('0');
}
}


function handleHttpResponse112(temp,id){
	if (http5.readyState == 4){
            var results = http5.responseText
            
            results = results.trim(); 
            results = results.replace('[','');
            results=results.replace(']',''); 
            var res = results.split("#"); 
            if(results.length>6){
            	document.getElementById('recGl'+id).value=res[1];
            	document.getElementById('payGl'+id).value=res[2];
            	document.getElementById('description'+id).value=res[4];
            //alert("good charge code" );
	        	<c:if test="${costElementFlag}">
	        	try{
	        		var ctype= document.getElementById('categories'+id).value;
	          		 ctype=ctype.trim();
	          		if(ctype==''){
	          			document.getElementById('categories'+id).value = res[12];
	          		}
	        	}catch(e){
		        	}
	        	</c:if>
                
            }else{
                if(temp=='Internal'){
                document.getElementById("ChargeNameDivId"+id).style.display = "none";
                alert("Charge code does not exist according the  contract, Please select another" );
                //document.getElementById('contract'+id).value=a;
                 document.getElementById('chargeCode'+id).value='';
                document.getElementById('recGl'+id).value= '';
            	document.getElementById('payGl'+id).value= '';
            	document.getElementById('description'+id).value= ''; 
                }
                else{
                document.getElementById("ChargeNameDivId"+id).style.display = "none";
                alert("Charge code does not exist according the  contract, Please select another" );
                document.getElementById('chargeCode'+id).value='';
                document.getElementById('recGl'+id).value= '';
            	document.getElementById('payGl'+id).value= '';
            	document.getElementById('description'+id).value= ''; 
            	
            	
                }
			}
			
            getUpdateListValue(id);
			progressBarAutoSave('1');
     }
	
	
}





function getContract(id) {
	var custJobType = document.getElementById('jobType'+id).value;
	var contractBillCode = document.getElementById('billToCode'+id).value;
	var custCreatedOn = document.getElementById('createdOn'+id).value;
	var url="findContracOnJobBasis.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType)+"&contractBillCode="+encodeURI(contractBillCode)+"&custCreatedOn="+encodeURI(custCreatedOn)+"&id="+id;
     http5.open("GET", url, true);
     http5.onreadystatechange = function(){ handleHttpResponse3(id);}; 
     http5.send(null);
	
}

function handleHttpResponse3(id){
		if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
                targetElementVal= document.getElementById('contract'+id).value;
                //alert(targetElementVal)
                targetElement =  document.getElementById('contract'+id);
				targetElement.length = res.length;
				//alert(res.length)
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.getElementById('contract'+id).options[i].text = '';
						document.getElementById('contract'+id).options[i].value = '';
					}else{
						document.getElementById('contract'+id).options[i].text =res[i];
						document.getElementById('contract'+id).options[i].value =res[i];
					}
				}
				if(document.forms['defaultAccountLineForm'].elements['formStatus'].value == '1'){
					document.getElementById('contract'+id).value=targetElementVal;
				}
				else{
					document.getElementById('contract'+id).value=targetElementVal;
				}
         }
}

function getCommodity(id){
    var commodityType = document.getElementById('jobType'+id).value;
    var url;
    if(commodityType=='STO'|| commodityType=='STF'|| commodityType=='STL' || commodityType=='TPS' ){
      url="getCommodity.html?ajax=1&decorator=simple&popup=true&commodityParameter=" + encodeURI('COMMODITS');
     }
     else
     {
       url="getCommodity.html?ajax=1&decorator=simple&popup=true&commodityParameter=" + encodeURI('COMMODIT');
     }
     http2.open("GET", url, true);
     http2.onreadystatechange = function(){ handleHttpResponse100(id);};
     http2.send(null);
}

function handleHttpResponse100(id)
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                res = results.replace("{",'');
                res = res.replace("}",'');
                res = res.split(",");
                
                targetElement =  document.getElementById('commodity'+id).value;
				targetElement.length = res.length;
				var job = document.getElementById('jobType'+id).value;;
 				for(i=0;i<res.length;i++)
					{
					  if(res[i] == ''){
						  document.getElementById('commodity'+id).options[i].text = '';
						  document.getElementById('commodity'+id).options[i].value = '';
					}
					else
					{
					
					   stateVal = res[i].replace("=",'+');
					   stateVal=stateVal.split("+");
					   document.getElementById('commodity'+id).options[i].text=stateVal[1];
					   document.getElementById('commodity'+id).options[i].value=stateVal[0].trim();
					   if(job == "")
					   {
						   document.getElementById('commodity'+id).options[0].selected=true;
						   document.getElementById('commodity'+id).options[0].selected=true;
					   }
					}
             
             }
           
       }
   } 



function disableAmountRevenue(id) {
	
	  var rev = document.getElementById('categories'+id).value; 
	     if(rev == "Internal Cost"){
	    	 
	    	 document.getElementById('sellRate'+id).readOnly=true;
	    	 document.getElementById('sellRate'+id).className="input-textUpper" ;
	    	 document.getElementById('sellRate'+id).value=0;
	    	 document.getElementById('estimatedRevenue'+id).readOnly=true;
	    	 document.getElementById('estimatedRevenue'+id).className="input-textUpper" ;
	    	 document.getElementById('estimatedRevenue'+id).value=0;
	    	 document.getElementById('markUp'+id).readOnly=true;
	    	 document.getElementById('markUp'+id).className="input-textUpper" ;
	    	 document.getElementById('markUp'+id).value=0;
	    
	     
	    }else{
	    	
	    	document.getElementById('sellRate'+id).readOnly=false;
	    	document.getElementById('estimatedRevenue'+id).readOnly=false;
	    	document.getElementById('markUp'+id).readOnly=false;
	    	document.getElementById('sellRate'+id).className="input-text" ;
	    	document.getElementById('estimatedRevenue'+id).className="input-text" ;
	    	document.getElementById('markUp'+id).className="input-text" ;
	     
	    }
	   }



function resetChargeCode(id){
	var category = document.getElementById('categories'+id).value; 
	if(category=='Internal Cost'){
		
		document.getElementById('chargeCode'+id).value='';
        document.getElementById('recGl'+id).value= '';
    	document.getElementById('payGl'+id).value= '';
    	document.getElementById('description'+id).value= ''; 
	       
	       }
	}
	
	
	
	
function expenseOld(id) {
    var estimate=0;
    var estimateSell=0;
    var estimateround=0;
    var estimateSellRound=0;
    var estimateOther=0;
    var passPercentage=0;
    var passPercentageround=0;
    var quantity = document.getElementById('quantity'+id).value;
    var quantityabs=Math.abs(quantity);
    var rate = document.getElementById('rate'+id).value;
    var sellRate = document.getElementById('sellRate'+id).value;
	var estimatePassPercentage = document.getElementById('markUp'+id).value;
    if(estimatePassPercentage==''){
    	estimatePassPercentage=0;
    }
	estimate = (quantity*rate);
	estimateSell=(quantity*sellRate) 
    if( document.getElementById('basis'+id).value=="cwt" || document.getElementById('basis'+id).value=="%age")
    {
   	   estimate = estimate/100;
   	   estimateSell=estimateSell/100; 	  	
    }
    if( document.getElementById('basis'+id).value=="per 1000")
    {
   	   estimate = estimate/1000;
   	   estimateSell=estimateSell/1000; 	  	
    } 
   	    estimateOther = ((estimate)*estimatePassPercentage/100);
   	    estimateround=Math.round(estimate*100)/100;
   	    estimateOtherRev=Math.round(estimateOther*100)/100;
   	    estimateSellRound=Math.round(estimateSell*100)/100;
   	    passPercentage=((estimateSell/estimate)*100);
   	    passPercentageround=Math.round(passPercentage);
   	   
   	 
   	 		document.getElementById('amount'+id).value = estimateround;
   	 		document.getElementById('estimatedRevenue'+id).value = estimateSellRound;
   	 		document.getElementById('markUp'+id).value=passPercentageround;		
	if(document.getElementById('amount'+id).value == '' || document.getElementById('amount'+id).value == 0){
	document.getElementById('markUp'+id).value='';
	   }
}



function getVatPercentEst(id){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){
        	
        	
        if(document.getElementById('estVatDescr'+id).value=='${entry.key}') {
        	document.getElementById('estVatPercent'+id).value='${entry.value}'; 
        calculateVatAmtEst(id);
        relo="yes"; 
       }  }
    </c:forEach>
      if(document.getElementById('estVatDescr'+id).value==''){
    	 document.getElementById('estVatPercent'+id).value=0;
      calculateVatAmtEst(id);
      }  
      </c:if> 
	}
function calculateVatAmtEst(id){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	var estVatAmt=0;
	if(document.getElementById('estVatDescr'+id).value!=''){
	var estVatPercent=eval(document.getElementById('estVatPercent'+id).value);
	var estimateRevenueAmount= eval(document.getElementById('estimatedRevenue'+id).value);
	<c:if test="${contractType}">
	  estimateRevenueAmount= eval(document.getElementById('sellRate'+id).value);
	</c:if>
	estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
	estVatAmt=Math.round(estVatAmt*100)/100; 
	document.getElementById('estVatAmt'+id).value=estVatAmt;
	}
    </c:if>
}


function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
} 


function checkVatForReadonlyFieldsEst(var1,id){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	       var var2= document.getElementById('estVatDescr'+id).value;
	       if(var1=='first' && var2==''){   
	    	   document.getElementById('estVatPercent'+id).readOnly=true;
	    	   document.getElementById('estVatPercent'+id).className='input-textUpper';
	           }else{ 
	        	   document.getElementById('estVatPercent'+id).readOnly=false;
	        	   document.getElementById('estVatPercent'+id).className='input-text'; 
	           }	       
	     </c:if>  
	}
	
	
function getExpVatPercentEst(id){ 
	<c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${payVatPercentList}">
        if(relo==""){ 
        if(document.getElementById('estExpVatDescr'+id).value=='${entry.key}') {
        	document.getElementById('estExpVatPercent'+id).value='${entry.value}'; 
         
        relo="yes"; 
       }  }
    </c:forEach>
      if(document.getElementById('estExpVatDescr'+id).value==''){
    	  document.getElementById('estExpVatPercent'+id).value=0;
       
      }  
      </c:if> 
	}
	
	
	
	
function checkFloatEstVat(temp,id)  { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.name;
	if(temp.value>100){
	alert("You cannot enter more than 100% VAT ");
	document.getElementById(fieldName).select();
	document.getElementById(fieldName).value='0.00';
	document.getElementById("estVatAmt"+id).value='';
	return false;
	}  
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
       	  alert("Invalid data in vat%." );
       		document.getElementById(fieldName).select();
    		document.getElementById(fieldName).value='';
    		document.getElementById("estVatAmt"+id).value='';
       	  return false;
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  } 
    if(check=='Invalid'){ 
    alert("Invalid data in vat%." ); 
    document.getElementById(fieldName).select();
	document.getElementById(fieldName).value='';
	document.getElementById("estVatAmt"+id).value=''; 
    return false;
    }  else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.getElementById(fieldName).value=value;
    calculateVatAmtEst(id)
    return true;
}  }



function mockupForRevenue(id) {
	   var estimate=0;
    var estimateround=0;
    var sellRateround=0;
    var estimatepasspercentageRound=0;
    var	estimatepasspercentage = document.getElementById('markUp'+id).value;
    var sellRate = document.getElementById('sellRate'+id).value;
    var quantity = document.getElementById('quantity'+id).value;
    var quantityabs=Math.abs(quantity);
    var rate = document.getElementById('rate'+id).value;
	   var estimateRevenue = document.getElementById('estimatedRevenue'+id).value; 
	   estimate = (quantity*rate);
	   	if(document.getElementById('basis'+id).value=="cwt" || document.getElementById('basis'+id).value=="%age"){
    		  estimate = estimate/100;
    		  if(quantity!=0){
    		  sellRate=(estimateRevenue/quantity)*100;
    		  sellRateround=Math.round(sellRate);
    		  }else{
    		  sellRateround=0;
    		  }
    		  document.getElementById('sellRate'+id).value=sellRateround;	  	
    	}
    	if( document.getElementById('basis'+id).value=="per 1000"){
    		  estimate = estimate/1000;
    		  if(quantity!=0){
    		  sellRate=(estimateRevenue/quantity)*1000;
    		  sellRateround=Math.round(sellRate);
    		  }
    		  else{
    		  sellRateround=0;
    		  }
    		  document.getElementById('sellRate'+id).value=sellRateround;	  	
    	}
    	else if(document.getElementById('basis'+id).value!="cwt" || document.getElementById('basis'+id).value!="%age"||document.getElementById('basis'+id).value!="per 1000")
	    {
	    if(quantity!=0){
	        sellRate=estimateRevenue/quantity;
    	    sellRateround=Math.round(sellRate);
    	    }
    	   else{
    		  sellRateround=0;
    		  }
	    document.getElementById('sellRate'+id).value=sellRateround;
	    }
	    if(estimate!=0){
	   estimatepasspercentage = (estimateRevenue/estimate)*100;
	   estimatepasspercentageRound=Math.round(estimatepasspercentage);
	   }else{
	   estimatepasspercentageRound=0;
	        }
	   if(document.getElementById('amount'+id).value == '' || document.getElementById('amount'+id).value == 0){
		   document.getElementById('markUp'+id).value='';
	   }else{
		   document.getElementById('markUp'+id).value=estimatepasspercentageRound;
	   }
	  
	 } 
	 
	 
function calculateRevenue(id) {
	    var estimate=0;
	    var sellRateround=0;
    var estimateround=0;
    var estimateRevenue=0;
    var estimateRevenueRound=0;
   var	estimatepasspercentage = document.getElementById('markUp'+id).value;
   var sellRate = document.getElementById('sellRate'+id).value;
   var quantity = document.getElementById('quantity'+id).value;
   var quantityabs=Math.abs(quantity);
   var rate = document.getElementById('rate'+id).value;
	   estimate = (quantity*rate); 
	   	if( document.getElementById('basis'+id).value=="cwt" || document.getElementById('basis'+id).value=="%age"){
   		estimate = estimate/100; 	  	
   	}  
   
   	if( document.getElementById('basis'+id).value=="per 1000"){
   		estimate = estimate/1000; 	  	
   	}
   	estimateRevenue=((estimate)*estimatepasspercentage/100)
   	estimateRevenueRound=Math.round(estimateRevenue*100)/100;
   	document.getElementById('estimatedRevenue'+id).value=estimateRevenueRound;   
     if( document.getElementById('basis'+id).value=="cwt" || document.getElementById('basis'+id).value=="%age"){
   	  if(quantity!=0){
   	  sellRate=(estimateRevenue/quantity)*100;
   	  sellRateround=Math.round(sellRate);
   	  }else{
   	  sellRateround=0;
   	  }
   	document.getElementById('sellRate'+id).value=sellRateround;	  	
   	}
    else if( document.getElementById('basis'+id).value=="per 1000"){
   	  if(quantity!=0){
   	  sellRate=(estimateRevenue/quantity)*1000;
   	  sellRateround=Math.round(sellRate);
   	  }else{
   	  sellRateround=0;
   	   }
   	document.getElementById('sellRate'+id).value=sellRateround;	  	
   	} else if(document.getElementById('basis'+id).value!="cwt" || document.getElementById('basis'+id).value!="%age"||document.getElementById('basis'+id).value!="per 1000")
    {
     if(quantity!=0){
      sellRate=(estimateRevenue/quantity);
   	  sellRateround=Math.round(sellRate);
   	  }else{
   	  sellRateround=0;
   	   }
     document.getElementById('sellRate'+id).value=sellRateround;	  	
    }
}


function checkVendorName(id){
	var vendorId = document.getElementById('vendorCode'+id).value;
	if(vendorId == ''){
		document.getElementById('vendorName'+id).value = '';
	}
    if(vendorId != ''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = function(){ handleHttpResponse2(id);}; 
     http2.send(null);
    } 

}


function handleHttpResponse2(id){
    if (http2.readyState == 4){
           var results = http2.responseText
           results = results.trim();
           var res = results.split("#"); 
      		if(res.size() >= 2){ 
          		if(res[2] == 'Approved'){
          			document.getElementById('vendorName'+id).value = res[1];
          			getUpdateListValue(id);
          		}else{
          			alert("Vendor code is not approved" ); 
          			document.getElementById('vendorName'+id).value="";
          			document.getElementById('vendorCode'+id).value="";
          			document.getElementById('vendorCode'+id).select();
          		}			
          }else{
               alert("Vendor code not valid");    
               	document.getElementById('vendorName'+id).value="";
     			document.getElementById('vendorCode'+id).value="";
     			document.getElementById('vendorCode'+id).select();
		   }
 	}
}


var tempId="";
function chk(id){
	if(document.getElementById('contract'+id).value==''){
		alert("Contract is required field");
		  return false;
	}
	if(document.getElementById('jobType'+id).value==""){
		 alert("Job Type is required field");
		  return false;
	}
	tempId=id;
    var category = document.getElementById('categories'+id).value;
	var contract = document.getElementById('contract'+id).value;
	var companyDiv="";
	<c:if test="${companyDivisionGlobal>1}">
	 companyDiv=document.getElementById('companyDivision'+id).value;
	</c:if>
	if(document.getElementById('recGl'+id) == undefined && document.getElementById('payGl'+id) == undefined){
		recGlVal = 'secondDescription';
		payGlVal = 'thirdDescription';
	}else{
		recGlVal = document.getElementById('recGl'+id);
		payGlVal = document.getElementById('payGl'+id);
	}	
 if(category=='Internal Cost' && companyDiv==''){
    alert("Please enter company division first.");
    return false;
    }
	else if(category=='Internal Cost'){
	//  contract="Internal Cost Management";
	document.getElementById('typeFormPlace').value="1";
	 javascript:openWindow('defaultChargesByComDiv.html?comDiv='+companyDiv+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=description'+id+'&fld_secondDescription=recGl'+id+'&fld_description=description'+id+'&fld_thirdDescription=payGl'+id+'&fld_code=chargeCode'+id);
	 document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].select();
	} else {
	if(contract=='' || contract==' '){
		document.getElementById('chargeCode'+id).value='';
		document.getElementById('recGl'+id).value = '';
		document.getElementById('payGl'+id).value = '';
		document.getElementById('contract'+id).focus();
		alert("There is no pricing contract: Please select.");
		return false;
	}else{
		document.getElementById('typeFormPlace').value="1";
		javascript:openWindow('defaultTemplateChargess.html?contract='+contract+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=recGl'+id+'&fld_description=description'+id+'&fld_thirdDescription=payGl'+id+'&fld_code=chargeCode'+id);
		document.getElementById('chargeCode'+id).select();
	 
	}
	}
}


function updateChargeCodeAfterPopUpClosed(){
	showOrHide(1);
	if(tempId!=null && tempId!=''){
		var regId=tempId;
	}
	var a=document.getElementById('chargeCode'+regId).value;
	getUpdateListValue(regId)
}

var tempVendorCode="";
var tempDescription="";
function checkVendor(id){
	if(document.getElementById('contract'+id).value==''){
		alert("Contract is required field");
		  return false;
	}
	if(document.getElementById('jobType'+id).value==""){
		 alert("Job Type is required field");
		  return false;
	}
	document.getElementById('typeFormPlace').value="1";
	tempVendorCode=id;
	tempDescription="Vendor";
	javascript:openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=vendorName'+id+'&fld_code=vendorCode'+id)
}

function updateVendorDefaultAccountListAfterPopUpClosed()
{
	showOrHide(1);
	if(tempVendorCode!=null && tempVendorCode!=''){
		var regven=tempVendorCode;
	}
	if(tempDescription=='Vendor'){
	var a=document.getElementById('vendorCode'+regven).value;
	var b=document.getElementById('vendorName'+regven).value;
	getUpdateListValue(regven)
	}
	
	if(tempDescription=='BillToCode'){
		var a=document.getElementById('billToCode'+regven).value;
		var b=document.getElementById('billToName'+regven).value;
		getUpdateListValue(regven)
		}
	

}






function findBillToName(id){
    var billToCode = document.getElementById('billToCode'+id).value;
    if(billToCode==''){
    	document.getElementById('billToName'+id).value="";
	}
    if(billToCode!=''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange =function(){ handleHttpResponse33(id);};  
     http2.send(null);
    }
}

function handleHttpResponse33(id){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim(); 
                var res = results.split("#"); 
           		if(res.size() >= 2){ 
	           		if(res[2] == 'Approved'){
	           			if(res[3]== '' || res[3] == 'null'){
	           				document.getElementById('billToName'+id).value = res[1];
 							//document.forms['defaultAccountLineForm'].elements['defaultAccountLine.customerEmployer'].value = res[1];
 							document.getElementById('billToName'+id).select();
 							getContract(id);
 							getUpdateListValue(id);
		           		}else{
		           			//document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value=
						 	document.getElementById('billToName'+id).value=res[1];
						 	document.getElementById('billToCode'+id).select();
						 	getContract(id);
 							getUpdateListValue(id);
		           		}	
	           		}else{
	           			alert("Bill To code is not approved" ); 
	           			document.getElementById('billToCode'+id).value="";
	           			document.getElementById('billToName'+id).value="";
					 	document.getElementById('billToCode'+id).select();
	           		}
	           }else{
	            alert("Bill To code not valid");    
	            document.getElementById('billToCode'+id).value="";
	            document.getElementById('billToName'+id).value="";
				document.getElementById('billToCode'+id).select();
			   }
           }
}


function openPopWindowlist(id){
	if(document.getElementById('contract'+id).value==''){
		alert("Contract is required field");
		  return false;
	}
	if(document.getElementById('jobType'+id).value==""){
		 alert("Job Type is required field");
		  return false;
	}
	document.getElementById('typeFormPlace').value="1";
	tempVendorCode=id;
	tempDescription="BillToCode";
	javascript:openWindow('partnersPopup.html?partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billToName'+id+'&fld_code=billToCode'+id);
	document.getElementById('billToName'+id).focus();
	
} 



function autoCompleterAjaxCallVendorName(id,populatedatafield){
	
		//document.getElementById('vendorCode'+id).value="";
		
	if(populatedatafield =="Vendor"){	
	var vendorName = document.getElementById('vendorName'+id).value;
	if(vendorName.length>=4){
	var data = 'searchNameAutocompleteListAjax.html?ajax=1&searchName='+encodeURI(vendorName)+'&populateDataField='+populatedatafield+'&decorator=simple&popup=true';
	$( "#vendorName"+id ).autocomplete({				 
	      source: data
	    });
}
else{
	$( "#vendorName"+id ).autocomplete({				 
	      source: null
	    });
}
}
	if(populatedatafield =="BillToName"){
	var billtoName = document.getElementById('billToName'+id).value;
	if(billtoName.length>=4){
		//alert(populatedatafield)
	var data = 'searchNameAutocompleteListAjax.html?ajax=1&searchName='+encodeURI(billtoName)+'&populateDataField='+populatedatafield+'&decorator=simple&popup=true';
	$( "#billToName"+id ).autocomplete({				 
	      source: data
	    });
}
else{
	$( "#billToName"+id ).autocomplete({				 
	      source: null
	    });
}
	
	}  
	
	}
	
 function displayVendorName(id){ 
	var value=document.getElementById('vendorName'+id).value;
	var name=value.split("#");
	if(name[1]!=null){
	document.getElementById("vendorName"+id).value=name[0];
	document.getElementById("vendorCode"+id).value=name[1]; 
	document.getElementById("vendorName"+id).focus();
	getUpdateListValue(id);
	}
    if(value==''){
    	document.getElementById('vendorCode'+id).value="";
	}
} 
 
 function displayBillToName(id){ 
		var value=document.getElementById('billToName'+id).value;
		var name=value.split("#");
		if(name[1]!=null){
		document.getElementById("billToName"+id).value=name[0];
		document.getElementById("billToCode"+id).value=name[1]; 
		document.getElementById("billToName"+id).focus();
		getUpdateListValue(id);
		}
		 if(value==''){
		    	document.getElementById('billToCode'+id).value="";
			}
	} 


 function autoCompleterAjaxCallChargeCode(id,divid){
	 <configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
	 	document.getElementById('chargeCodeValidationVal').value='charge';
		var ChargeName = document.getElementById('chargeCode'+id).value;
		var contract = document.getElementById('contract'+id).value;
		var idval='chargeCode'+id;
		contract=contract.trim();
		var category =  document.getElementById('categories'+id).value;
		var pageCondition="";
	 	if(category=='Internal Cost'){
	 		pageCondition='TemplateInternalCategory';
	 	}
	 	if(category!='Internal Cost'){
	 		pageCondition='TemplateNonInternalCategory';
	 	}
		var companyDiv="";
		<c:if test="${companyDivisionGlobal>1}">
		companyDiv=document.getElementById('companyDivision'+id).value;
		</c:if>
		var idCondition='';
		if(contract!=""){
			if(ChargeName.length>=3){
	            $.get("ChargeCodeAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
	            	searchName:ChargeName,
	            	contract: contract,
	            	chargeCodeId:idval,
	            	autocompleteDivId: divid,
	            	id:id,
	            	idCondition:idCondition,
	            	categoryCode:category,
	            	pageCondition:pageCondition,
	            	searchcompanyDivision:companyDiv
	            }, function(idval) {
	                document.getElementById(divid).style.display = "block";
	                $("#" + divid).html(idval)
	            })
	        } else {
	            document.getElementById(divid).style.display = "none"
	        }
		}
		else{
			alert("There is no pricing contract: Please select.");
			document.getElementById("contract"+id).focus();
		}
		</configByCorp:fieldVisibility>	
 }
 
 function copyChargeDetails(chargecode,chargecodeId,autocompleteDivId,id,idCondition){
			document.getElementById(chargecodeId).value=chargecode;
			document.getElementById(autocompleteDivId).style.display = "none";
			document.getElementById('chargeCodeValidationVal').value='';
			document.getElementById('chargeCodeValidationMessage').value='NothingData';
			checkChargeCode(id);
			
		}

 
 function closeMyChargeDiv(autocompleteDivId,chargecode,chargecodeId,id,idCondition){
			document.getElementById(autocompleteDivId).style.display = "none";
			 if(document.getElementById(chargecodeId).value==''){
				document.getElementById("description"+id).value="";	
			}
			 document.getElementById('chargeCodeValidationVal').value='';
			 document.getElementById('chargeCodeValidationMessage').value='NothingData';
			 checkChargeCode(id);
			}
 function closeMyChargeDiv2(chargecodeId,id){
	 if(document.getElementById(chargecodeId).value==''){
		document.getElementById("description"+id).value="";	
	}
	 document.getElementById('chargeCodeValidationVal').value='';
	 document.getElementById('chargeCodeValidationMessage').value='NothingData';
	 
	}

 function enablePreviousFunction(id,checkValue){
	 document.getElementById('ChargeNameDivId'+id).style.display = "none";
	 if(document.getElementById("chargeCodeValidationMessage").value!='NothingData' && document.getElementById("chargeCodeValidationMessage").value!=''){	 
	 	document.getElementById('chargeCodeValidationVal').value='';
		 
		 document.getElementById('chargeCode'+id).focus();
		 checkChargeCode(id);
		 document.getElementById('chargeCodeValidationVal').value='AutoValue';
		 document.getElementById("chargeCodeValidationMessage").value=checkValue;
	 }
}	
		
		
</script>
<script type="text/javascript">	
function getIdAndOrderNumber(thisfield,id){
	var var1=id+'#'+thisfield.value;
	var changerOrderList= document.forms['defaultAccountLineForm'].elements['changerOrderList'].value; 
	if(changerOrderList==''){
	changerOrderList=var1+",";
	}else{
	changerOrderList=var1+","+changerOrderList;
	}
	document.forms['defaultAccountLineForm'].elements['changerOrderList'].value=changerOrderList;
	}
function checkAccountLineNumber(temp) { 
	
       var accountLineNumber=document.getElementById(temp).value; 
       accountLineNumber=accountLineNumber.trim();
       if(accountLineNumber!=""){
	       if(accountLineNumber>99999){
	         alert("Manually you can not Enter Line # greater than 99999");
	         document.getElementById(temp).value=0;
	       }
       }else if(accountLineNumber==""){
    	   document.getElementById(temp).value=0;
       }
     }
 function openPopWindow(){
	 document.getElementById('typeFormPlace').value="0";
	javascript:openWindow('partnersPopup.html?partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=defaultAccountLine.billToName&fld_code=defaultAccountLine.billToCode');
}  

function quotesNotAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==222){
  return false;
  }
  else{
  return true;
  }
}

function submitSearch(){
	var tempPageNumber= <%= request.getParameter("d-7182065-p") %> ;
	if(tempPageNumber=='' || tempPageNumber=='0' || tempPageNumber=='null' || tempPageNumber==null){
		tempPageNumber='1';
		}
		var url = "searchDefaultAccountLines.html?d-7182065-p="+tempPageNumber;
		 document.forms['defaultAccountLineForm'].action = url;
	    document.forms['defaultAccountLineForm'].submit();
		
	}

</script>
</head>


<s:form cssClass="form_magn" id="defaultAccountLineForm" name="defaultAccountLineForm" method="post" validate="true">
<c:set var="ischecked" value="false"/>
	<configByCorp:fieldVisibility componentId="component.tab.defaultAccountLineList.template">
	<c:set var="ischecked" value="true"/>
</configByCorp:fieldVisibility>
<s:hidden name="changerOrderList" value=""/>
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="firstDescription" /> 
<s:hidden name="formStatus" value=""/>
<div id="Layer1"  style="width:100%">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<div id="content" align="center" style="margin-bottom:0px;">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
	<table class="table" cellspacing="0" cellpadding="0" style="width:100%">
		<thead>
			<tr>
			<th><fmt:message key="defaultAccountLine.contract" /></th>
			<th><fmt:message key="defaultAccountLine.jobType" /></th>
			<th><fmt:message key="defaultAccountLine.route" /><font color="#003366" size="2">*</font></th>
			<th><fmt:message key="defaultAccountLine.mode" /><font color="#003366" size="2">*</font></th>
			<th><fmt:message key="defaultAccountLine.serviceType" /><font color="#003366" size="2">*</font></th>
			<th><fmt:message key="defaultAccountLine.packMode"/><font color="#003366" size="2">*</font></th>
			<th><fmt:message key="defaultAccountLine.commodity"/><font color="#003366" size="2">*</font></th> 
			<%-- <th><fmt:message key="defaultAccountLine.categories" /></th>
			<th><fmt:message key="defaultAccountLine.billToCode" /></th>--%>
			</tr>			
		</thead>
		<tbody>
			<tr>
			    <td width=""><s:select id="defaultAccountLine.contract" name="defaultAccountLine.contract" list="%{contractListNew}" cssStyle="width:120px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();"/></td>
				<td width=""><s:select id="defaultAccountLine.jobType" name="defaultAccountLine.jobType" list="%{job}" cssStyle="width:120px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();"/></td>
				<td width=""><s:select id="defaultAccountLine.route" name="defaultAccountLine.route" list="%{routing}" cssStyle="width:120px" cssClass="list-menu"  onchange="changeStatus();"/></td>
				<td width=""><s:select id="defaultAccountLine.mode" name="defaultAccountLine.mode" list="%{mode}" cssStyle="width:87px" cssClass="list-menu"  onchange="changeStatus();"/></td>
				<td width=""><s:select id="defaultAccountLine.serviceType" name="defaultAccountLine.serviceType" list="%{service}" cssStyle="width:120px" cssClass="list-menu"  onchange="changeStatus();"/></td>
				<td width=""><s:select id="defaultAccountLine.packMode" name="defaultAccountLine.packMode" list="%{pkmode}" cssStyle="width:100px" cssClass="list-menu"  onchange="changeStatus();"/></td>
				<c:choose>
					<c:when test="${defaultAccountLine.jobType =='STO' || defaultAccountLine.jobType =='STL' ||defaultAccountLine.jobType =='STF' ||defaultAccountLine.jobType =='TPS'}">
						<td align="left"><s:select cssClass="list-menu" id="defaultAccountLine.commodity" name="defaultAccountLine.commodity" list="%{commodits}" cssStyle="width:130px"/></td>
					 </c:when>
					 <c:otherwise>
						<td align="left"><s:select cssClass="list-menu" id="defaultAccountLine.commodity" name="defaultAccountLine.commodity" list="%{commodit}" cssStyle="width:130px"/></td>		
					 </c:otherwise>
				</c:choose> 
				
				<s:hidden name="defaultAccountLine.categories"/>
				<s:hidden name="defaultAccountLine.billToName"/>
				<%--<td width=""><s:select name="defaultAccountLine.categories" list="%{category}" cssStyle="width:100px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();validService();"/></td>
				<td width=""><s:select name="defaultAccountLine.billToCode" list="%{billToCodeList}" cssStyle="width:120px" cssClass="list-menu" headerKey="" headerValue="" onchange="changeStatus();"/></td>--%>
				</tr>
				<tr>
				<td style="border:none;" class="listwhitetext">
					Origin Country<br>
			<s:select id="defaultAccountLine.originCountry" name="defaultAccountLine.originCountry" list="%{ocountry}" cssClass="list-menu" cssStyle="width:120px;margin-right:18px;margin-top:2px;margin-bottom:3px;" headerKey="" headerValue="" />
			</td>
			<td style="border:none;" class="listwhitetext">
					Destination Country<br>
			<s:select id="defaultAccountLine.destinationCountry" name="defaultAccountLine.destinationCountry" list="%{dcountry}" cssClass="list-menu" cssStyle="width:120px;margin-right:18px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			<td style="border:none;" class="listwhitetext">
					Equipment<br>
			<s:select id="defaultAccountLine.equipment" name="defaultAccountLine.equipment" list="%{EQUIP}" cssClass="list-menu" cssStyle="width:120px;margin-right:18px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			<c:if test="${companyDivisionGlobal>1}">
				<td class="listwhitetext" style="border:0px solid #FFFFFF; text-align:left;">Company Division<font color="#003366" size="2">*</font><br>
				<s:select cssClass="list-menu" id="defaultAccountLine.companyDivision" name="defaultAccountLine.companyDivision" list="%{comDivis}" cssStyle="width:90px" headerKey="" headerValue=""/> </td>
				</c:if>
				<td style="border:0px solid #FFFFFF; text-align:left;" class="listwhitetext" colspan="2"><fmt:message key="defaultAccountLine.billToCode"/><font color="#003366" size="2">*</font><br>
				<s:textfield cssClass="input-text"  id="defaultAccountLine.billToCode" name="defaultAccountLine.billToCode" cssStyle="width:82px" maxlength="20" onkeydown="return quotesNotAllowed(event);" /><img class="openpopup" width="17" height="20" align="top" onclick="openPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
				<c:if test="${companyDivisionGlobal<=1}">
				<td></td>
				</c:if>
				<td width="" style="border-left: hidden;"> 
				<input type="button" class="cssbutton" style="width:55px; height:25px" value="Search" onclick="submitSearch();"/>
			<%--	<s:submit cssClass="cssbutton" method="search" cssStyle="width:60px; " key="button.search"  /> --%>
			    <input type="button" class="cssbutton" value="Clear" style="width:60px;" onclick="clear_fields();"/></td> 
			</tr>			
		</tbody>		
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<div style="clear: both; line-height: 28px;">&nbsp;</div>
<c:set var="button1">
     <input type="button" class="cssbutton" style="width:60px;" onclick="location.href='<c:url value="/editDefaultAccountLine.html"/>'"  
        value="<fmt:message key="button.add"/>"/>
        <c:if test="${ischecked =='false' && defaultAccountLines!='[]'}">
			<input type="button" class="cssbutton" value="Save" style="width:60px;" onclick="saveChanges();"/></td>
			</c:if>  
			  
     <input type="button" class="cssbutton1" onclick="window.open('copyContractFromDefaultAccountLineList.html?decorator=popup&popup=true','defaultAccountLineFormr','height=500,width=750,top=0, scrollbars=yes,resizable=yes').focus();" 
       value="Copy" style="width:60px; height:25px;margin-bottom:5px;" /> 
        <input type="button" class="cssbutton1" onclick="window.open('copyContractFromDefaultAccountLineListAll.html?decorator=popup&popup=true','defaultAccountLineFormr','height=500,width=750,top=0, scrollbars=yes,resizable=yes').focus();" 
       value="Copy All" style="width:60px; height:25px;margin-bottom:5px;" />
       
       <input type="button" class="cssbutton1" onclick="window.open('uploadAccountingTemplate.html?decorator=popup&popup=true','defaultAccountLineFormr','height=500,width=750,top=0, scrollbars=yes,resizable=yes').focus();" 
       value="Upload" style="width:60px; height:25px;margin-bottom:5px;" /></td>
           
</c:set>
<s:hidden name="typeFormPlace" id="typeFormPlace" value=""/>
</s:form>
<s:form id="defaultAccountLineFormr"  name="defaultAccountLineFormr" method="post" action="" validate="true">
<s:hidden name="generateMessage" />
<s:hidden name="chargeCodeValidationMessage" id="chargeCodeValidationMessage"/>
<s:hidden name="chargeCodeValidationVal" id="chargeCodeValidationVal"/>
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="firstDescription" /> 
<c:if test="${defaultAccountLines=='[]'}">
<c:out value="${button1}" escapeXml="false" />
</c:if>
<c:if test="${defaultAccountLines!='[]'}">  
<s:hidden name="defaultAccountLine.id" value="%{defaultAccountLine.id}"/>
<div id="generateMsg" class="message" style="display:none;width:300px;background:none;border:none;">
 				<table style="margin:0px;padding:0px;"><tr>
 				<td>
				<font color="red" style="text-align:center;width:100%;font:12px arial,verdana;"><b>
					<script type="text/javascript"> 
					document.forms['defaultAccountLineFormr'].elements['generateMessage'].value = getCookie('generateMessage');
					if(document.forms['defaultAccountLineFormr'].elements['generateMessage'].value == "AddLineMessage"){
						document.getElementById('generateMsg').style.display = 'block' 
						document.write("Template has been Added successfully.");
						document.forms['defaultAccountLineFormr'].elements['generateMessage'].value="";
					}
					   document.cookie = 'generateMessage' + "=; expires=" +  new Date().toGMTString();
					</script>
					</b></font></td></tr></table>
				</div>
<div style="clear: both; line-height: 28px;">&nbsp;</div>
<div id="otabs" style="margin-top:0px;margin-bottom:0px; " >
		  <ul>
		    <li><a class="current"><span>Default Account Line List</span></a></li>
		  </ul>
		</div><div class="spnblk">&nbsp;</div>
<s:set name="defaultAccountLines" value="defaultAccountLines" scope="request"/>
<c:choose>
<c:when test="${ischecked =='true'}">
<display:table name="defaultAccountLines" class="table" requestURI="" id="defaultAccountLines"  defaultsort="1" style="width:100%;font-size:11px;" pagesize="15">
 
  <display:column  sortable="true"  title="#" paramId="id" paramProperty="id" sortProperty="orderNumber"  href="editDefaultAccountLine.html">
  	<c:out value='${defaultAccountLines_rowNum}'/>
  </display:column> 
   <display:column  sortable="true" title="Job" >
    	<select class="list-menu pr-f11" style="width:100px;" id="jobType${defaultAccountLines.id}" name="jobType${defaultAccountLines.id}"  onchange="getContract(${defaultAccountLines.id});getCommodity(${defaultAccountLines.id}),getUpdateListValue(${defaultAccountLines.id});">
    		<option value=""></option>
    		<c:forEach var="chrms2" items="${job}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms2.key == defaultAccountLines.jobType}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${chrms2.key}' />" <c:out value='${selectedInd}' />>
						<c:out value="${chrms2.value}"></c:out>
    				</option>
			</c:forEach> 
		</select>
    </display:column> 
    <display:column  sortable="true" title="Contract">
    	<select class="list-menu pr-f11" style="width:160px;" id="contract${defaultAccountLines.id}"       onchange="changeStatus();valueChk(${defaultAccountLines.id}),getUpdateListValue(${defaultAccountLines.id});" tabindex="" >
    	<option value=""></option>
   			<c:forEach var="varcont" items="${contractList}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${varcont == defaultAccountLines.contract}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${varcont}' />" <c:out value='${selectedInd}' />>
						<c:out value="${varcont}"></c:out>
    				</option>
			</c:forEach>
		</select>
   </display:column> 
    <display:column  sortable="true" title="Mode" >
    	<select class="list-menu pr-f11" style="width:60px;" value="${defaultAccountLines.mode}" id="mode${defaultAccountLines.id}" name="mode${defaultAccountLines.id}"     tabindex="" onchange="getUpdateListValue(${defaultAccountLines.id});">
    		 <c:forEach var="varmode" items="${mode}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${varmode == defaultAccountLines.mode}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
					<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${varmode}' />" <c:out value='${selectedInd}' />>
					<c:out value="${varmode}"></c:out>
    				</option>
			</c:forEach>
   		</select> 
    </display:column> 
    <display:column  sortable="true" titleKey="defaultAccountLine.billToCode" >
    	<input type="text" class="input-text pr-f11" name="billToCode${defaultAccountLines.id}" id="billToCode${defaultAccountLines.id}" value="${defaultAccountLines.billToCode}" style="width:40px" onchange = "findBillToName(${defaultAccountLines.id});" />
    	<img class="openpopup" width="17" height="20" align="top" onclick="openPopWindowlist(${defaultAccountLines.id});" src="<c:url value='/images/open-popup.gif'/>" />
    </display:column>
    <display:column  sortable="true" titleKey="defaultAccountLine.billToName" >
    	<input type="text" style="width:80px;"  class="input-text pr-f11" name="billToName${defaultAccountLines.id}" id="billToName${defaultAccountLines.id}" value="${defaultAccountLines.billToName}" onkeyup="autoCompleterAjaxCallVendorName(${defaultAccountLines.id},'BillToName')" onchange="displayBillToName(${defaultAccountLines.id});" />
    </display:column> 
    <display:column  sortable="true" titleKey="defaultAccountLine.route"  >
    	<select class="list-menu pr-f11" style="width:60px;" id="route${defaultAccountLines.id}" name="route${defaultAccountLines.id}" tabindex="" onchange="getUpdateListValue(${defaultAccountLines.id});">
    		<c:forEach var="chrms3" items="${routing}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms3.key == defaultAccountLines.route}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${chrms3.key}' />" <c:out value='${selectedInd}' />>
						<c:out value="${chrms3.value}"></c:out>
    				</option>
			</c:forEach> 
		</select>
    </display:column> 
    <display:column  sortable="true" titleKey="defaultAccountLine.packMode" >
    	<select class="list-menu pr-f11" style="width:60px;"  id="packMode${defaultAccountLines.id}" name="packMode${defaultAccountLines.id}"    tabindex="" onchange="getUpdateListValue(${defaultAccountLines.id});">
    		<c:forEach var="chrms4" items="${pkmode}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms4.key == defaultAccountLines.packMode}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${chrms4.key}' />" <c:out value='${selectedInd}' />>
						<c:out value="${chrms4.value}"></c:out>
    				</option>
			</c:forEach> 
		</select> 
    </display:column>
    <c:choose>
		<c:when test="${defaultAccountLines.jobType =='STO' || defaultAccountLines.jobType =='STL' ||defaultAccountLines.jobType =='STF' ||defaultAccountLines.jobType =='TPS'}">
    <display:column  sortable="true" titleKey="defaultAccountLine.commodity" >
     	<select class="list-menu pr-f11" style="width:60px;"  id="commodity${defaultAccountLines.id}" name="commodity${defaultAccountLines.id}"   tabindex="" onchange="getUpdateListValue(${defaultAccountLines.id});">
    		<c:forEach var="chrms6" items="${commodits}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms6.key == defaultAccountLines.commodity}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${chrms6.key}' />" <c:out value='${selectedInd}' />>
						<c:out value="${chrms6.value}"></c:out>
    				</option>
			</c:forEach> 
		</select> 
    </display:column>
    </c:when>
	<c:otherwise>
	<display:column  sortable="true" titleKey="defaultAccountLine.commodity" >
     	<select class="list-menu pr-f11" style="width:60px;"  id="commodity${defaultAccountLines.id}" name="commodity${defaultAccountLines.id}"   tabindex="" onchange="getUpdateListValue(${defaultAccountLines.id});">
    		<c:forEach var="chrms6" items="${commodit}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms6.key == defaultAccountLines.commodity}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${chrms6.key}' />" <c:out value='${selectedInd}' />>
						<c:out value="${chrms6.value}"></c:out>
    				</option>
			</c:forEach> 
		</select> 
    </display:column>
    </c:otherwise>  
    </c:choose>
    <display:column  sortable="true" titleKey="defaultAccountLine.serviceType" >
    	<select class="list-menu pr-f11" id="serviceType${defaultAccountLines.id}" name="serviceType${defaultAccountLines.id}"    style="width:140px;" onchange="getUpdateListValue(${defaultAccountLines.id});">
    		<c:forEach var="chrms5" items="${service}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms5.key == defaultAccountLines.serviceType}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${chrms5.key}' />" <c:out value='${selectedInd}' />>
						<c:out value="${chrms5.value}"></c:out>
    				</option>
			</c:forEach> 
		</select> 
    </display:column> 
    <c:if test="${companyDivisionGlobal>1}">
    <display:column  sortable="true" title="Company Division" >
    	<select class="list-menu pr-f11" id="companyDivision${defaultAccountLines.id}"  onchange="resetChargeCode(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});"    tabindex="">
    		<option value=""></option>
    		<c:forEach var="varcomp" items="${comDivis}" varStatus="loopStatus1">
				<c:choose>
					<c:when test="${varcomp == defaultAccountLines.companyDivision}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${varcomp}' />" <c:out value='${selectedInd}' />>
						<c:out value="${varcomp}"></c:out>
    				</option>
			</c:forEach>
		</select>
  	</display:column>
  	</c:if>
   <display:column  sortable="true" title="Equipment" >
    	<select class="list-menu pr-f11" id="equipment${defaultAccountLines.id}" name="equipment${defaultAccountLines.id}"  onchange="changeStatus();getUpdateListValue(${defaultAccountLines.id});" style="width:60px;" tabindex="">
     		<option value=""></option>
    		<c:forEach var="chrms9" items="${EQUIP}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms9.key == defaultAccountLines.equipment}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
					<option value="<c:out value='${chrms9.key}' />" <c:out value='${selectedInd}' />>
						<c:out value="${chrms9.value}"></c:out>
    				</option>
			</c:forEach> 
		</select> 
	</display:column>
   	<display:column  sortable="true" titleKey="defaultAccountLine.originCountry" >
    	<select class="list-menu pr-f11" style="width:110px;" id="originCountry${defaultAccountLines.id}" name="originCountry${defaultAccountLines.id}"  onchange="changeStatus();getUpdateListValue(${defaultAccountLines.id});"  tabindex="" >
    		<option value=""></option>
    		<c:forEach var="chrms7" items="${ocountry}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms7.key == defaultAccountLines.originCountry}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
				<option value="<c:out value='${chrms7.key}' />" <c:out value='${selectedInd}' />>
					<c:out value="${chrms7.value}"></c:out>
    			</option>
			</c:forEach> 
		</select> 
	</display:column>
    <display:column  sortable="true" titleKey="defaultAccountLine.destinationCountry" headerClass="head-bdr" style="border-right:medium solid #003366;">
    	<select class="list-menu pr-f11" style="width:110px;" id="destinationCountry${defaultAccountLines.id}" name="destinationCountry${defaultAccountLines.id}"  onchange="changeStatus();getUpdateListValue(${defaultAccountLines.id});"  tabindex="">
    		<option value=""></option>
    		<c:forEach var="chrms8" items="${dcountry}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms8.key == defaultAccountLines.destinationCountry}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
				<option value="<c:out value='${chrms8.key}' />" <c:out value='${selectedInd}' />>
					<c:out value="${chrms8.value}"></c:out>
    			</option>
			</c:forEach> 
		</select> 
	</display:column>
	<display:column  sortable="true" title="Order#"   style="width:20px" >
    	<input type="text"  id="orderNumber${defaultAccountLines.id}" name="orderNumber${defaultAccountLines.id}" value="${defaultAccountLines.orderNumber}" class="input-text pr-f11" style="width:30px;text-align:right" maxlength="5" onchange="checkAccountLineNumber('orderNumber${defaultAccountLines.id}'),getUpdateListValue(${defaultAccountLines.id});" onkeydown="return onlyNumsAllowed(event)"  />
    </display:column> 
    <display:column sortable="true" titleKey="defaultAccountLine.categories" >
    	<select class="list-menu pr-f11" id="categories${defaultAccountLines.id}" name="categories${defaultAccountLines.id}"  onchange="disableAmountRevenue(${defaultAccountLines.id});resetChargeCode(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});"  tabindex="">
    	<option value=""></option>
    		<c:forEach var="chrms1" items="${category}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms1.key == defaultAccountLines.categories}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
				<option value="<c:out value='${chrms1.key}' />" <c:out value='${selectedInd}' />>
					<c:out value="${chrms1.value}"></c:out>
    			</option>
			</c:forEach> 
		</select>
    </display:column> 
    <display:column title="Basis"  sortable="true"  > 
    	<select class="list-menu pr-f11" style="width:50px;" id="basis${defaultAccountLines.id}" name="basis${defaultAccountLines.id}"  onchange="expenseOld(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});"  tabindex="">
    		<option value=""></option>
    		<c:forEach var="chrms" items="${basis}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${chrms.key == defaultAccountLines.basis}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
				<option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
					<c:out value="${chrms.value}"></c:out>
    			</option>
			</c:forEach> 
		</select>
    </display:column>
    <display:column  sortable="true" titleKey="defaultAccountLineList.vendorCode" >
    	<input type="text" class="input-text pr-f11" style="width:40px;" name="vendorCode${defaultAccountLines.id}" id="vendorCode${defaultAccountLines.id}" value="${defaultAccountLines.vendorCode}" onchange= "checkVendorName(${defaultAccountLines.id});" cssStyle="width:80px">
     	<img align="top" class="openpopup" width="17" height="20" onclick="checkVendor(${defaultAccountLines.id});" src="<c:url value='/images/open-popup.gif'/>" />
    </display:column>
    <display:column sortable="true" titleKey="defaultAccountLine.vendorName">
    	<input type="text" style="width:80px;" class="input-text pr-f11"  name="vendorName${defaultAccountLines.id}" id="vendorName${defaultAccountLines.id}" value="${defaultAccountLines.vendorName}"  onkeyup="autoCompleterAjaxCallVendorName(${defaultAccountLines.id},'Vendor')" onchange="displayVendorName(${defaultAccountLines.id})"   />
    </display:column>
    <display:column   sortable="true" titleKey="defaultAccountLine.chargeCode" >
    	<input type="Hidden" class="input-text pr-f11" style="text-align:right;width:40px;" name="payGl${defaultAccountLines.id}" id="payGl${defaultAccountLines.id}" value="${defaultAccountLines.payGl}"  onchange="getUpdateListValue(${defaultAccountLines.id});" readonly="true"/>
    	<input type="Hidden" class="input-text pr-f11" style="text-align:right;width:40px;" name="recGl${defaultAccountLines.id}" id="recGl${defaultAccountLines.id}" value="${defaultAccountLines.recGl}"  onchange="getUpdateListValue(${defaultAccountLines.id});" readonly="true"/>
    	<input type="text" class="input-text pr-f11" style="width:55px;" name="chargeCode${defaultAccountLines.id}" id="chargeCode${defaultAccountLines.id}" value="${defaultAccountLines.chargeCode}" onblur="checkChargeCode(${defaultAccountLines.id});" onkeyup="autoCompleterAjaxCallChargeCode(${defaultAccountLines.id},'ChargeNameDivId${defaultAccountLines.id}')" >
    	<img class="openpopup" width="17" height="20" align="top" onclick="chk(${defaultAccountLines.id});"  src="<c:url value='/images/open-popup.gif'/>" />
    	<div id="ChargeNameDivId${defaultAccountLines.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
    </display:column>
    
    <display:column  style="text-align: right"  sortable="true" titleKey="defaultAccountLine.quantity">
    	<input type="text" class="input-text pr-f11" style="width:40px;text-align:right;" name="quantity${defaultAccountLines.id}" id="quantity${defaultAccountLines.id}" value="${defaultAccountLines.quantity}"  onchange="expenseOld(${defaultAccountLines.id});getVatPercentEst(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});" onkeydown="return onlyFloatNumsAllowed(event);" onselect="enablePreviousFunction('${defaultAccountLines.id}','checkValue');" />
    </display:column>
    <c:if test="${systemDefaultVatCalculation=='true'}">
    <display:column  sortable="true" title="Rec&nbsp;VAT Desc " >
    	<select class="list-menu pr-f11" id="estVatDescr${defaultAccountLines.id}" name="estVatDescr${defaultAccountLines.id}"  onchange="getVatPercentEst(${defaultAccountLines.id});changeStatus();checkVatForReadonlyFieldsEst('first',${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});"  tabindex="">
    		<option value=""></option>
    		<c:forEach var="evvar" items="${euVatList}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${evvar.key == defaultAccountLines.estVatDescr}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
				<option value="<c:out value='${evvar.key}' />" <c:out value='${selectedInd}' />>
					<c:out value="${evvar.value}"></c:out>
    			</option>
			</c:forEach> 
		</select> 
	</display:column>
	<display:column  sortable="true" title="VAT&nbsp;%" >
		<input type="text" class="input-text pr-f11" style="text-align:right;width:40px;" id="estVatPercent${defaultAccountLines.id}" name="estVatPercent${defaultAccountLines.id}" value="${defaultAccountLines.estVatPercent}" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"   onchange="checkFloatEstVat(this,${defaultAccountLines.id});getVatPercentEst(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});" maxLength="6" tabindex=""/>
	</display:column> 
	</c:if>
	<display:column  sortable="true" titleKey="defaultAccountLine.sellRate" >
		<input type="text" class="input-text pr-f11" name="sellRate${defaultAccountLines.id}" id="sellRate${defaultAccountLines.id}" value="${defaultAccountLines.sellRate}" style="width: 40px;text-align:right;" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="expenseOld(${defaultAccountLines.id});getVatPercentEst(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});"/>
	</display:column>
	<display:column  sortable="true" titleKey="defaultAccountLineList.estimatedRevenue" >
		<input type="text" class="input-text pr-f11" name="estimatedRevenue${defaultAccountLines.id}" id="estimatedRevenue${defaultAccountLines.id}" value="${defaultAccountLines.estimatedRevenue}" onkeydown="return onlyFloatNumsAllowed(event)" onchange="mockupForRevenue(${defaultAccountLines.id});getVatPercentEst(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});" style="width: 40px;text-align:right;"/>
	</display:column>
	<display:column  sortable="true" titleKey="defaultAccountLine.markUp" >
		<input type="text" class="input-text pr-f11" name="markUp${defaultAccountLines.id}" id="markUp${defaultAccountLines.id}" value="${defaultAccountLines.markUp}" onchange="calculateRevenue(${defaultAccountLines.id});getVatPercentEst(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});" style="width: 40px;text-align:right;"/>
	</display:column>
	<c:if test="${systemDefaultVatCalculation=='true'}">
	<display:column  sortable="true" titleKey="defaultAccountLineList.estExpVatDescr" >
    	<select class="list-menu pr-f11" id="estExpVatDescr${defaultAccountLines.id}" name="estExpVatDescr${defaultAccountLines.id}" value="${defaultAccountLines.estExpVatDescr}"  onchange="getExpVatPercentEst(${defaultAccountLines.id});changeStatus();getUpdateListValue(${defaultAccountLines.id});"  style="width:84px;">
    		<option value=""></option>
    		<c:forEach var="payvar" items="${payVatList}" varStatus="loopStatus">
				<c:choose>
					<c:when test="${payvar.key == defaultAccountLines.estExpVatDescr}">
						<c:set var="selectedInd" value=" selected"></c:set>
					</c:when>
					<c:otherwise>
						<c:set var="selectedInd" value=""></c:set>
					</c:otherwise>
				</c:choose>
				<option value="<c:out value='${payvar.key}' />" <c:out value='${selectedInd}' />>
					<c:out value="${payvar.value}"></c:out>
    			</option>
			</c:forEach> 
		</select> 
	</display:column>
	<display:column  sortable="true" title="VAT&nbsp;%" >
		<input type="text" class="input-text pr-f11" style="text-align:right;width:40px;" id="estExpVatPercent${defaultAccountLines.id}" name="estExpVatPercent${defaultAccountLines.id}" value="${defaultAccountLines.estExpVatPercent}"  maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)" onchange="return checkFloatEstVat(this,${defaultAccountLines.id});changeStatus();getUpdateListValue(${defaultAccountLines.id});" maxLength="6" tabindex=""/>
	</display:column> 
	</c:if>
    <display:column headerClass="containeralign"   sortable="true" title="Buy&nbsp;Rate" >
    	<input type="text" class="input-text pr-f11" name="rate${defaultAccountLines.id}" id="rate${defaultAccountLines.id}" value="${defaultAccountLines.rate}" onchange="expenseOld(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});" onkeydown="return onlyFloatNumsAllowed(event)" style="width:40px;text-align: right;" />
    	<input type="hidden" cssStyle="text-align:right" id="estVatAmt${defaultAccountLines.id}" name="estVatAmt${defaultAccountLines.id}" value="${defaultAccountLines.estVatAmt}"   tabindex=""/>
    	
    </display:column> 
    <display:column headerClass="containeralign" style="text-align: right"  sortable="true" title="Estimate Expense" >
    	<input type="text" class="input-text pr-f11" name="amount${defaultAccountLines.id}" id="amount${defaultAccountLines.id}" value="${defaultAccountLines.amount}" onkeydown="return onlyFloatNumsAllowed(event)" onchange="expenseOld(${defaultAccountLines.id});getUpdateListValue(${defaultAccountLines.id});" style="width:40px;text-align: right;"/>
	</display:column>
    <display:column  sortable="true" titleKey="defaultAccountLine.description" >
    	<input type="text" class="input-text pr-f11" name="description${defaultAccountLines.id}" id="description${defaultAccountLines.id}" value="${defaultAccountLines.description}"  onchange="getUpdateListValue(${defaultAccountLines.id});"/>
     	<fmt:formatDate var="defaultAccountLineCreatedOnFormattedValue" value="${defaultAccountLines.createdOn}" pattern="${displayDateTimeEditFormat}"/>
		<input type="hidden" name="createdOn${defaultAccountLines.id}" id="createdOn${defaultAccountLines.id}" value="${defaultAccountLineCreatedOnFormattedValue}"/>
	</display:column>
	<display:column title="Audit" style="width:25px; text-align: center;">
   	<a><img align="middle" src="images/report-ext.png" style="margin: 0px 0px 0px 0px;" onclick="window.open('auditList.html?id=${defaultAccountLines.id}&tableName=defaultaccountline&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"></a>
   </display:column>
	
    <display:column  style="text-align: center" ><a><img align="middle" onclick="return confirmSubmit(${defaultAccountLines.id});" style="margin: 0px 0px 0px 1px;" src="images/recyle-trans.gif"/></a></display:column>	 
    <display:setProperty name="export.excel.filename" value="Default Account Line List.xls"/>   
	<display:setProperty name="export.csv.filename" value="Default Account Line List.csv"/>   
	<display:setProperty name="export.pdf.filename" value="Default Account Line List.pdf"/>
	
</display:table> 

</c:when>
<c:otherwise>

<display:table name="defaultAccountLines" class="table" requestURI="" id="defaultAccountLines" export="true" defaultsort="1" style="width:100%;font-size:11px;" pagesize="25">   
    <display:column  sortable="true" title="Order#"  sortProperty="orderNumber" style="width:20px" >
    <input type="text"  id="orderNumber${defaultAccountLines.id}" name="orderNumber${defaultAccountLines.id}" value="${defaultAccountLines.orderNumber}" class="input-text" style="width:40px;text-align:right" maxlength="5" onchange="checkAccountLineNumber('orderNumber${defaultAccountLines.id}'),getIdAndOrderNumber(this,${defaultAccountLines.id});" onkeydown="return onlyNumsAllowed(event)"  />
    </display:column>
    <display:column property="contract" sortable="true" titleKey="defaultAccountLine.contract"  href="editDefaultAccountLine.html" paramId="id" paramProperty="id"/>
    <display:column property="jobType" sortable="true" title="Job" />
    <display:column property="route" sortable="true" titleKey="defaultAccountLine.route"  /> 
    <display:column property="mode" sortable="true" titleKey="defaultAccountLine.mode" />
    <display:column property="serviceType" sortable="true" titleKey="defaultAccountLine.serviceType" /> 
    <display:column property="packMode" sortable="true" titleKey="defaultAccountLine.packMode" /> 
    <display:column property="commodity" sortable="true" titleKey="defaultAccountLine.commodity" />
    <display:column property="originCountry" sortable="true" titleKey="defaultAccountLine.originCountry" />
    <display:column property="destinationCountry" sortable="true" titleKey="defaultAccountLine.destinationCountry" /> 
    <display:column property="equipment" sortable="true" title="Equipment" />
    <display:column property="categories" sortable="true" titleKey="defaultAccountLine.categories" /> 
    <display:column property="basis" sortable="true" titleKey="defaultAccountLine.basis" /> 
    <display:column headerClass="containeralign" style="text-align: right" property="quantity" sortable="true" titleKey="defaultAccountLine.quantity" /> 
    <display:column headerClass="containeralign" style="text-align: right" property="rate" sortable="true" title="Buy&nbsp;Rate" /> 
    <display:column headerClass="containeralign" style="text-align: right" property="sellRate" sortable="true" title="Sell&nbsp;Rate" /> 
    <display:column property="chargeCode" sortable="true" titleKey="defaultAccountLine.chargeCode" />
    <display:column property="billToCode" sortable="true" titleKey="defaultAccountLine.billToCode" />  
    <display:column headerClass="containeralign" style="text-align: right" property="amount" sortable="true" title="Estimate Expense" />
    <display:column headerClass="containeralign" style="text-align: right" property="estimatedRevenue" sortable="true" title="Estimate Revenue" />
    <display:column property="vendorCode" sortable="true" titleKey="defaultAccountLine.vendorCode" /> 
    <display:column property="companyDivision" sortable="true" title="Company Division" />
    <display:column  style="text-align: center" ><a><img align="middle" onclick="return confirmSubmit(${defaultAccountLines.id});" style="margin: 0px 0px 0px 1px;" src="images/recyle-trans.gif"/></a></display:column>	 
    <display:setProperty name="export.excel.filename" value="Default Account Line List.xls"/>   
<display:setProperty name="export.csv.filename" value="Default Account Line List.csv"/>   
<display:setProperty name="export.pdf.filename" value="Default Account Line List.pdf"/> 
</display:table> 

</c:otherwise>
</c:choose>
<c:out value="${button1}" escapeXml="false" />
</c:if> 


</s:form>



<script type="text/javascript">   
</script>  
 