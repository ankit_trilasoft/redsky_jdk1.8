package com.trilasoft.app.service;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.ClaimLossTicket;
import com.trilasoft.app.model.Loss;

import java.math.BigDecimal;
import java.util.List; 
import java.util.Set;

public interface ClaimLossTicketManager extends GenericManager<ClaimLossTicket, Long>  {
	public List findClaimLossTickets(String lossNumber, Long claimNum, String corpID);
    public boolean findTicketbyLoss(Long ticket, String seqNum, String lossNum);
    public int applyTicketToAllLoss(Long ticket, String seqNum, String lossNum);
    public List<Claim> findClaimBySeqNum(String sequenceNumber);
    public List<Loss> findLossBySeqNum(String sequenceNumber);
    public boolean findLossTicket(Long ticket, String seqNum, Long claimNum, String lossNum);
    public List findCustLossTickets(Long ticket, String seqNum, boolean isAppliedToAllLoss);
	public List findCustClmLossTickets(String seqNum, String corpID);
	public List findCustClmLossPercent(Long claimNum, Long ticket, String corpID);
	public List findCustClmLossChargeBack(Long claimNum, Long ticket, String corpID);
	public List<Loss> findLossByClaimNum(Long claimNum, String corpID);
	public int applyTicketToAllLossOldAlso(Long ticket, Long claimNum, String lossNum, String corpID);
	public void removeCLTByAction(String lossNum, String corpID);
} 
