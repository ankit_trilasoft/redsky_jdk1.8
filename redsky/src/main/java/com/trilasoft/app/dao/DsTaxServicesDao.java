package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao; 

import com.trilasoft.app.model.DsTaxServices;

public interface DsTaxServicesDao extends GenericDao<DsTaxServices, Long>{
	List getListById(Long id);
}
