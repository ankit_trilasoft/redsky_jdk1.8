package com.trilasoft.app.webapp.action;

import java.util.Date;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.service.TaskCheckListManager;


public class TaskCheckListAction extends BaseAction implements Preparable {
	private TaskCheckListManager taskCheckListManager;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(TaskCheckListAction.class);

	public TaskCheckListAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
	}

	public void prepare() throws Exception {
			
	}

	public TaskCheckListManager getTaskCheckListManager() {
		return taskCheckListManager;
	}

	public void setTaskCheckListManager(TaskCheckListManager taskCheckListManager) {
		this.taskCheckListManager = taskCheckListManager;
	}
	
}
