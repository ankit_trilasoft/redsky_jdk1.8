<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Status&nbsp;Screen</title>  
<style type="text/css">
body { margin:0px; padding:0px;}
</style>
</head>
<body id="emailStatus">	
		<table align="left" style="margin-left:5px;margin:0px;padding:0px;" width="200px" cellspacing="0" cellpadding="0" border="0">
		<tr valign="top"> 	
	<td align="left"><b>Email&nbsp;Status</b></td>
	<td align="right"  style="width:200px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
		</tr>
		<tr><td valign="top" colspan="2">
<s:form id="recipientWithEmailList"  action="recipientWithEmailsStatus" method="post" validate="true" >
<div style="overflow-x:auto;width:600px;" >
<display:table name="recipientWithEmailList" class="table" requestURI="" id="recipientWithEmailList" export="false" defaultsort="7"  defaultorder="descending" pagesize="25" style="width:100%;margin-top:0px;!margin-top:8px; "> 
    <display:column property="dateSent"  title="Sent Date" style="width:40px;"/> 
    <display:column property="recipientTo" title="To"  style="width:80px;"/>
	<display:column property="recipientCc" title="CC" style="width:80px;"/>
	<display:column property="subject" title="Subject"  maxLength="50" style="width:200px;"/>
     <display:column title="Email&nbsp;Status"  style="width:10px;"   paramId="id" paramProperty="id">	
	<c:choose> 
			<c:when test="${recipientWithEmailList.emailStatus=='SaveForEmail'}">
					<img  src="<c:url value='/images/ques-small.gif'/>" align="middle" title="Ready For Sending"/>
			</c:when>
			<c:when test="${fn:indexOf(recipientWithEmailList.emailStatus,'SaveForEmail')>-1}">
					<img  src="<c:url value='/images/cancel001.gif'/>" align="middle" title="${fn:replace(recipientWithEmailList.emailStatus,'SaveForEmail','')}"/>
			</c:when> 							
			<c:otherwise>
				<img  src="<c:url value='/images/tick01.gif'/>" align="middle" title="${recipientWithEmailList.emailStatus}"/>
			</c:otherwise>			
		</c:choose>		
	</display:column> 	
</display:table>
</div>
</s:form>
</td></tr></table>
</body> 