package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ControlExpirationsDao;
import com.trilasoft.app.dao.ControlExpirationsDao;
import com.trilasoft.app.model.ControlExpirations;
import com.trilasoft.app.service.ControlExpirationsManager;

public class ControlExpirationsManagerImpl extends GenericManagerImpl<ControlExpirations, Long> implements ControlExpirationsManager {
	ControlExpirationsDao controlExpirationsDao;
	
	public ControlExpirationsManagerImpl(ControlExpirationsDao controlExpirationsDao) { 
        super(controlExpirationsDao); 
        this.controlExpirationsDao = controlExpirationsDao; 
    }
	public List getDriverValidation(String driverCode,String corpId,String parentId){
		return controlExpirationsDao.getDriverValidation(driverCode, corpId,parentId);
	}
}
