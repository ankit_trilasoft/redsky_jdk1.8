<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn111" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>

<%
    response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
%>
<style>
table-fc tbody th, .table tbody td {white-space:nowrap;}
.table td, .table th, .tableHeaderTable td {padding: 0.3em;}
/* .table thead th, .tableHeaderTable td {background:#bcd2ef url("images/bg_priceheader.png") repeat-x scroll 0 0;} */
.key_acc {  background: transparent url("images/key_acc_price.jpg") no-repeat scroll 0 0; cursor: default; height: 24px;margin-bottom:2px;}
.table tr.even {background-color: #e0f4ff;}
.pr-f11{font-family:arial,verdana;font-size:11px;}
.table tfoot th, tfoot td {background: #bcd9eb url("images/footer-fowrd-blueshade-new.jpg") repeat-x scroll 0 0; color: #434242;height: auto;}
/* .cssbuttonA {background: #edf6ff -moz-linear-gradient(center top , #edf6ff 5%, #bedaf7 100%) repeat scroll 0 0;border: 1px solid #419dfa;border-radius: 5px;
    box-shadow: 0 1px 0 0 #c9e5ff inset; color: #222222; font: bold 12px arial;padding: 0;} */
</style>
<div id="para1" > 
<s:hidden name="aidListIds" value="${aidListIds}"/>
<s:hidden name="billingDMMContractTypePage" value="${billingDMMContractType}" />
<s:hidden name="userRoleExecutive" value="${userRoleExecutive}"/>
<s:hidden id="countReceivableDetailNotes" name="countReceivableDetailNotes" value="<%=request.getParameter("countReceivableDetailNotes") %>"/>
<c:set var="countReceivableDetailNotes" value="<%=request.getParameter("countReceivableDetailNotes") %>" />
<c:choose>
<c:when test="${!discountUserFlag.pricingRevision && !discountUserFlag.pricingActual}">
<c:set var="screenSizeLen" value="100%"/>
</c:when>
<c:otherwise>
<c:set var="screenSizeLen" value="100%"/>
</c:otherwise>
</c:choose>
<table id="accInnerTable" width="${screenSizeLen}" border="0" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
<tr>
<td >
<table border="0" style="width:${screenSizeLen};margin-top:2px;margin:0px;padding:2px 3px 3px 5px;height:25px;background:#BCD2EF url(images/bg_listheader.png) repeat-x scroll 0 0;border:2px solid #74B3DC;" border="0" cellpadding="0" cellspacing="0">
<tr>
 <c:if test="${accountLineList!='[]'}"> 
<c:choose>
<c:when test="${discountUserFlag.pricingRevision && discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
<td width="26%" align="right"></td>
<td width="24%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Estimate</td>
<td width="18%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Revision</td>
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="14%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Revenue</td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="10%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Revenue</td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="10%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Revenue</td>
</c:if>
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="10%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Revenue</td>
</c:if>
<td width="7%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Expense</td>
<td width="28%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
</c:when> 
<c:when test="${discountUserFlag.pricingRevision && discountUserFlag.pricingActual!=true && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
<td width="30%" align="right"></td>
<td width="27%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Estimate</td>
<td width="25%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Revision</td>
<td width="10%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
<td width="25%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
</c:when> 
<c:when test="${discountUserFlag.pricingRevision!=true && discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
<td width="22%" align="right"></td>
<td width="25%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Estimate</td>
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="21%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Revenue</td>
</c:if>
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="21%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Revenue</td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="21%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Revenue</td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="17%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Revenue</td>
</c:if>
<td width="20%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual Expense</td>
<td width="5%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
<td width="25%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
</c:when> 
<c:otherwise>
<td width="42%" align="right"></td>
<td width="25%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Estimate</td>
<td width="20%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
<td width="18%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
<td width="5%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
</c:otherwise> 
</c:choose>    
</c:if>
 <c:if test="${accountLineList=='[]'}"> 
 <c:choose>
<c:when test="${discountUserFlag.pricingRevision && discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="15%" align="right"></td>
</c:if>
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="15%" align="right"></td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="15%" align="right"></td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="12%" align="right"></td>
</c:if>
<td width="30%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Estimate</td>
<td width="20%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Revision</td>
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="12%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Revenue</td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="12%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Revenue</td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="12%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Revenue</td>
</c:if>
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="8%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Revenue</td>
</c:if>
<td width="7%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Expense</td>
<td width="28%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
</c:when> 
<c:when test="${discountUserFlag.pricingRevision && discountUserFlag.pricingActual!=true && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
<td width="30%" align="right"></td>
<td width="27%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Estimate</td>
<td width="25%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Revision</td>
<td width="10%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
<td width="25%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
</c:when> 
<c:when test="${discountUserFlag.pricingRevision!=true && discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
<td width="22%" align="right"></td>
<td width="21%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Estimate</td>
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="22%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Revenue</td>
</c:if>
<c:if test="${multiCurrency=='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="22%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Revenue</td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew=='Y'}">
<td width="22%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Revenue</td>
</c:if>
<c:if test="${multiCurrency!='Y' && systemDefaultVatCalculationNew!='Y'}">
<td width="18%" align="right" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Revenue</td>
</c:if>
<td width="20%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Actual&nbsp;Expense</td>
<td width="5%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
<td width="25%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
</c:when> 
<c:otherwise>
<td width="42%" align="right"></td>
<td width="25%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;">Estimate</td>
<td width="20%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
<td width="18%" align="center" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
<td width="5%" align="left" class="listwhitetext" style="font-size:13px;font-weight:bold;"></td>
</c:otherwise> 
</c:choose>
 </c:if>                     
</tr>
</table>
</td>

</tr>

	<tr>
		<td>
           <display:table name="accountLineList"  class="table" requestURI="" id="accountLineList" style="width:${screenSizeLen};margin-top:1px;margin-left:0px;margin-right:0px;"  defaultsort="2"  >
		                     <display:column title="&nbsp;" style="width:5px;">
		                     <c:if test="${serviceOrder.moveType=='Quote'}">
		                     <c:out value="${accountLineList_rowNum}"/>
		                     </c:if>
		                     <c:if test="${serviceOrder.moveType!='Quote'}">
		                     <a onclick="checkChargeCodeMandatory('${serviceOrder.id}','${accountLineList.id}','${accountLineList.chargeCode}');">
									<img id="${accountLineList_rowNum}" src="images/file_edit.png"  width="15" height="17" align="top" />
							 </a>
		                     </c:if>
		                     </display:column>   
							 <c:if test="${accountLineList!='[]'}"> 
							 <c:choose>
	                         <c:when test="${(((!discountUserFlag.pricingActual && !discountUserFlag.pricingRevision) &&  (accountLineList.revisionExpense!='0.00' || accountLineList.revisionRevenueAmount!='0.00' || accountLineList.actualRevenue!='0.00' || accountLineList.actualExpense!='0.00' ||accountLineList.distributionAmount!='0.00' || accountLineList.chargeCode=='MGMTFEE'|| accountLineList.chargeCode=='DMMFEE'|| accountLineList.chargeCode=='DMMFXFEE')) || ((discountUserFlag.pricingActual || discountUserFlag.pricingRevision) &&  (accountLineList.chargeCode=='MGMTFEE'|| accountLineList.chargeCode=='DMMFEE'|| accountLineList.chargeCode=='DMMFXFEE')) || (!trackingStatus.accNetworkGroup  && trackingStatus.soNetworkGroup && accountLineList.createdBy == 'Networking') || serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD')}">
	                            <display:column sortProperty="accountLineNumber"   titleKey="accountLine.accountLineNumber"  style="width:10px;" > 
								<input type="text" style="text-align:left" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber }" size="1" maxlength="3" class="input-textUpper pr-f11 "  readonly="true" />
								</display:column>
								<display:column   titleKey="accountLine.category" style="width:70px" >  
								<input type="text" style="width:70px" name="category${accountLineList.id}" value="${accountLineList.category }"  maxlength="3" class="input-textUpper pr-f11 "  readonly="true" />
								</display:column>
								<display:column title="Charge &nbsp;Code" style="width:100px; whitespace: nowrap;" >
								<input type="text"  id="chargeCode${accountLineList.id}"  name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" size="10"  maxlength="25" class="input-textUpper pr-f11 " readonly="true"/>
								<input type="hidden"  name="vendorCodeOwneroperator${accountLineList.id}" id="vendorCodeOwneroperator${accountLineList.id}"/>
								<input type="hidden"  name="accChargeCodeTemp${accountLineList.id}" value="${accountLineList.chargeCode}"/>
								<input type="hidden"  name="contractCurrency${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="recGl${accountLineList.id}" value="${accountLineList.recGl}"/>
								<input type="hidden"  name="payGl${accountLineList.id}" value="${accountLineList.payGl}"/>
								<input type="hidden"  name="contractExchangeRate${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
								<input type="hidden"  name="contractValueDate${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="payableContractCurrency${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractExchangeRate${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								<input type="hidden"  name="payableContractValueDate${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="division${accountLineList.id}" value="${accountLineList.division}"/>
								<input type="hidden"  name="payPostDate${accountLineList.id}" value="${accountLineList.payPostDate}"/>
								<input type="hidden"  name="VATExclude${accountLineList.id}" value="${accountLineList.VATExclude}"/>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<input type="hidden"  name="vatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>
								<input type="hidden"  name="estVatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>
								<input type="hidden"  name="estExpVatAmt${accountLineList.id}" value="${accountLineList.estExpVatAmt}"/>		
								</c:if>	
								<c:if test="${systemDefaultVatCalculationNew!='Y' || discountUserFlag.pricingRevision!=true}">
								<input type="hidden"  name="revisionVatAmt${accountLineList.id}" value="${accountLineList.revisionVatAmt}"/>
								<input type="hidden"  name="revisionExpVatAmt${accountLineList.id}" value="${accountLineList.revisionExpVatAmt}"/>								
								</c:if>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<input type="hidden"  name="recVatDescr${accountLineList.id}" value="${accountLineList.recVatDescr}"/>								
								<input type="hidden"  name="recVatPercent${accountLineList.id}" value="${accountLineList.recVatPercent}"/>
								<input type="hidden"  name="payVatDescr${accountLineList.id}" value="${accountLineList.payVatDescr}"/>								
								<input type="hidden"  name="payVatPercent${accountLineList.id}" value="${accountLineList.payVatPercent}"/>
								</c:if>
								<c:if test="${systemDefaultVatCalculationNew!='Y' || discountUserFlag.pricingActual!=true}">
								<input type="hidden"  name="recVatAmt${accountLineList.id}" value="${accountLineList.recVatAmt}"/>								
								<input type="hidden"  name="payVatAmt${accountLineList.id}" value="${accountLineList.payVatAmt}"/>																														
								</c:if>
								<c:if test="${contractType}">
								<input type="hidden" id="estimatePayableContractCurrencyNew${accountLineList.id}" name="estimatePayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimatePayableContractCurrency}"/>
								<input type="hidden" id="estimatePayableContractValueDateNew${accountLineList.id}" name="estimatePayableContractValueDateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractValueDate}"/>
								<input type="hidden" id="estimatePayableContractExchangeRateNew${accountLineList.id}" name="estimatePayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractExchangeRate}"/>
								<input type="hidden" id="estimatePayableContractRateNew${accountLineList.id}" name="estimatePayableContractRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRate}"/>
								<input type="hidden"  name="estimatePayableContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRateAmmount}"/>
								<input type="hidden"  name="estimateContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimateContractCurrency}"/>
								<input type="hidden"  name="estimateContractValueDateNew${accountLineList.id}" value="${accountLineList.estimateContractValueDate}"/>
								<input type="hidden"  name="estimateContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimateContractExchangeRate}"/>
								<input type="hidden"  name="estimateContractRateNew${accountLineList.id}" value="${accountLineList.estimateContractRate}"/>
								<input type="hidden"  name="estimateContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimateContractRateAmmount}"/>
								<input type="hidden"  name="revisionContractCurrency${accountLineList.id}" value="${accountLineList.revisionContractCurrency}"/>
								<input type="hidden"  name="revisionContractValueDate${accountLineList.id}" value="${accountLineList.revisionContractValueDate}"/>
								<input type="hidden"  name="revisionContractExchangeRate${accountLineList.id}" value="${accountLineList.revisionContractExchangeRate}"/>
								<input type="hidden"  name="revisionPayableContractCurrency${accountLineList.id}" value="${accountLineList.revisionPayableContractCurrency}"/>
								<input type="hidden"  name="revisionPayableContractValueDate${accountLineList.id}" value="${accountLineList.revisionPayableContractValueDate}"/>
								<input type="hidden"  name="revisionPayableContractExchangeRate${accountLineList.id}" value="${accountLineList.revisionPayableContractExchangeRate}"/>

								<input type="hidden"  name="contractCurrencyNew${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="contractValueDateNew${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="contractExchangeRateNew${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>

								<input type="hidden"  name="payableContractCurrencyNew${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractValueDateNew${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="payableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								
								</c:if>						
								<input type="hidden"  id="estCurrencyNew${accountLineList.id}" name="estCurrencyNew${accountLineList.id}" value="${accountLineList.estCurrency}"/>
								<input type="hidden"  id="estValueDateNew${accountLineList.id}" name="estValueDateNew${accountLineList.id}" value="${accountLineList.estValueDate}"/>
								<input type="hidden"  id="estExchangeRateNew${accountLineList.id}" name="estExchangeRateNew${accountLineList.id}" value="${accountLineList.estExchangeRate}"/>
								<input type="hidden"  id="estLocalRateNew${accountLineList.id}" name="estLocalRateNew${accountLineList.id}" value="${accountLineList.estLocalRate}"/>
								<input type="hidden"  name="estLocalAmountNew${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
								<input type="hidden"  name="estSellCurrencyNew${accountLineList.id}" value="${accountLineList.estSellCurrency}"/>
								<input type="hidden"  name="estSellValueDateNew${accountLineList.id}" value="${accountLineList.estSellValueDate}"/>
								<input type="hidden"  name="estSellExchangeRateNew${accountLineList.id}" value="${accountLineList.estSellExchangeRate}"/>
								<input type="hidden"  name="estSellLocalRateNew${accountLineList.id}" value="${accountLineList.estSellLocalRate}"/>
								<input type="hidden"  name="estSellLocalAmountNew${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
								<input type="hidden"  name="estSellLocalAmount${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
								<input type="hidden"  name="buyDependSellNew${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
								<input type="hidden"  name="buyDependSell${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
								<input type="hidden"  name="oldEstimateSellDeviationNew${accountLineList.id}" />								
								<input type="hidden"  name="oldEstimateDeviationNew${accountLineList.id}" />
								<input type="hidden" name ="estimateDeviation${accountLineList.id}"  value="${accountLineList.estimateDeviation}" />
								<input type="hidden" name ="estimateSellDeviation${accountLineList.id}"  value="${accountLineList.estimateSellDeviation}" /> 
								<input type="hidden"  name="revisionCurrency${accountLineList.id}" value="${accountLineList.revisionCurrency}"/>
								<input type="hidden"  name="revisionValueDate${accountLineList.id}" value="${accountLineList.revisionValueDate}"/>
								<input type="hidden"  name="revisionExchangeRate${accountLineList.id}" value="${accountLineList.revisionExchangeRate}"/>
								<input type="hidden"  name="estLocalAmount${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
								<input type="hidden"  name="countryNew${accountLineList.id}" value="${accountLineList.country}"/>
								<input type="hidden"  name="valueDateNew${accountLineList.id}" value="${accountLineList.valueDate}"/>
								<input type="hidden"  name="exchangeRateNew${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
								<input type="hidden"  name="accountLineCostElement${accountLineList.id}" value="${accountLineList.accountLineCostElement}"/>
								<input type="hidden"  name="accountLineScostElementDescription${accountLineList.id}" value="${accountLineList.accountLineScostElementDescription}"/>
								<input type="hidden"  name="itemNew${accountLineList.id}" value="${accountLineList.itemNew}"/>
								<input type="hidden"  name="checkNew${accountLineList.id}" value="${accountLineList.checkNew}"/>
							<c:if test="${companyDivisionGlobal<=1}"> 
			                   <s:hidden name="companyDivision${accountLineList.id}" id="companyDivision${accountLineList.id}"  value="${accountLineList.companyDivision}" />
			                    <s:hidden name="tempCompanyDivision${accountLineList.id}" id="tempCompanyDivision${accountLineList.id}"  value="${accountLineList.companyDivision}" />
			                    <s:hidden name="recAccDate${accountLineList.id}" id="recAccDate${accountLineList.id}"  value="${accountLineList.recAccDate}" />
			                    <s:hidden name="payAccDate${accountLineList.id}" id="payAccDate${accountLineList.id}"  value="${accountLineList.payAccDate}" />
									
							</c:if>						
								</display:column>
								<display:column   title="Bill&nbsp;To&nbsp;Code" style="width:100px" > 
								<input type="text" id="billToCode${accountLineList.id}" name="billToCode${accountLineList.id}"  value="${accountLineList.billToCode }" size="6" class="input-textUpper pr-f11 " readonly="true" />
								<c:if test="${accountInterface=='Y'}">
								<c:if test="${!utsiRecAccDateFlag  && !utsiPayAccDateFlag}"> 
								<c:if test="${(!accNonEditFlag && empty accountLineList.recAccDate) || (accNonEditFlag && (accountLineList.recInvoiceNumber=='' || empty accountLineList.recInvoiceNumber))}"> 
								  <c:if test="${receivableSectionDetail==true}"> 
								   <img align="top" id="accountLineBillToCodeOpenpopup"  class="openpopup" width="17" height="20" onclick="javascript:openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billToName${accountLineList.id}&fld_code=billToCode${accountLineList.id}');" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" />
								   </c:if>
								 </c:if>
								<c:if test="${(!accNonEditFlag && not empty accountLineList.recAccDate) || (accNonEditFlag && (accountLineList.recInvoiceNumber!='' && not empty accountLineList.recInvoiceNumber))}"> 
								<c:if test="${receivableSectionDetail==true}"> 
								   <c:if test="${(!accNonEditFlag)}">
								  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as Sent To Acc has been already filled.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if>
									<c:if test="${(accNonEditFlag)}">
								  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as invoice has been generated.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if>
									</c:if>
								</c:if>
								</c:if>
								<c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
								<c:if test="${receivableSectionDetail==true}"> 
								  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as sent to Accounting and/or already Invoiced in UTSI Instance')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if> 
								</c:if>
								</c:if>
								<c:if test="${accountInterface!='Y'}">
								<c:if test="${accountLineList.recInvoiceNumber==''|| accountLineList.recInvoiceNumber==null}"> 
								  <c:if test="${receivableSectionDetail==true}"> 
								   <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="checkBilltoCode();" id="openpopup10.img" src="<c:url value='/images/open-popup.gif'/>" />
								   </c:if>
								 </c:if>
								<c:if test="${accountLineList.recInvoiceNumber!=''&& accountLineList.recInvoiceNumber!=null}"> 
								<c:if test="${receivableSectionDetail==true}"> 
								  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="checkBilltoCode();" id="openpopup11.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if>
								</c:if>
								</c:if>
								</display:column>
								<display:column  title="Bill&nbsp;To&nbsp;Name" style="width:50px" >
								<input type="text" id="billToName${accountLineList.id}" name="billToName${accountLineList.id}" readonly="true" value="${accountLineList.billToName }" size="15" class="input-textUpper pr-f11 "  /> 
								</display:column>
								<display:column   title="Vendor Code" style="width:100px" > 
								<input type="text" id="vendorCode${accountLineList.id}" name="vendorCode${accountLineList.id}"  value="${accountLineList.vendorCode }" size="6" class="input-textUpper pr-f11 " readonly="true" />
								 <input type="hidden" id="pricePointAgent${accountLineList.id}"  name="pricePointAgent${accountLineList.id}" />
				                 <input type="hidden" id="pricePointMarket${accountLineList.id}"  name="pricePointMarket${accountLineList.id}" />
				                 <input type="hidden" id="pricePointTariff${accountLineList.id}"  name="pricePointTariff${accountLineList.id}" />								
				                 <input type="hidden" id="actgCode${accountLineList.id}"  name="actgCode${accountLineList.id}" value="${accountLineList.actgCode}"/>
								</display:column>
								<display:column  title="Vendor &nbsp;Name" style="width:50px" >
								<input type="text" id="estimateVendorName${accountLineList.id}" name="estimateVendorName${accountLineList.id}" readonly="true" onkeyup="findPartnerDetailsPriceing('estimateVendorName${accountLineList.id}','vendorCode${accountLineList.id}','estimateVendorNameDivId${accountLineList.id}',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','${accountLineList.id}',event);" onchange="findPartnerDetailsByName('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}');" value="${accountLineList.estimateVendorName }" size="15" class="input-textUpper pr-f11 "  /> 
								<div id="estimateVendorNameDivId${accountLineList.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
								</display:column>
								<display:column   title="Basis"  style="width:80px"> 
								<input type="text" id="basis${accountLineList.id}" name ="basis${accountLineList.id}"  value="${accountLineList.basis}" style="width:50px" class="input-textUpper pr-f11 " readonly="true"> 
								<input type="hidden"  id="basis${accountLineList.id}" name="oldbasis${accountLineList.id}" value="${accountLineList.basis}"/>
								</display:column>
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Rec VAT&nbsp;Desc"  style="width:50px">
								<input type="hidden" name ="recVatDescr${accountLineList.id}"  value="${accountLineList.recVatDescr}" style="width:70px" class="input-textUpper pr-f11 " readonly="true"> 
								<input type="hidden"  name="recVatPercent${accountLineList.id}" value="${accountLineList.recVatPercent}"/>
								 <c:set var="selectedVat" value=""></c:set>
								<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.recVatDescr}">
	                                  <c:set var="selectedVat" value="${chrms.value}"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                 
	                                 </c:otherwise>
                                  </c:choose>
                                </c:forEach> 
								<input type="text" value="${selectedVat}" style="width:70px" class="input-textUpper pr-f11 " readonly="true">																
								</display:column>
								</c:if>
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT&nbsp;Desc"  style="width:50px">
								<input type="hidden" name ="payVatDescr${accountLineList.id}"  value="${accountLineList.payVatDescr}" style="width:70px" class="input-textUpper pr-f11 " readonly="true"> 
								<input type="hidden"  name="payVatPercent${accountLineList.id}" value="${accountLineList.payVatPercent}"/>
								<c:set var="selectedVat" value=""></c:set>
								<c:forEach var="chrms" items="${payVatList}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.payVatDescr}">
	                                  <c:set var="selectedVat" value="${chrms.value}"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  
	                                 </c:otherwise>
                                  </c:choose>
                                </c:forEach> 
								<input type="text" value="${selectedVat}" style="width:70px" class="input-textUpper pr-f11 " readonly="true">																								
								</display:column>
								</c:if>									
								<display:column title="Quantity"  style="width:18px;border-left:medium solid #003366;" >
								<input type="text" style="text-align:right" name="estimateQuantity${accountLineList.id}"  value="${accountLineList.estimateQuantity }" size="4" class="input-textUpper pr-f11 " readonly="true"/>
								<input type="hidden"  name="oldestimateQuantity${accountLineList.id}" value="${accountLineList.estimateQuantity }"/>
								</display:column> 
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateRate${accountLineList.id}" value="${accountLineList.estimateRate }" size="5" class="input-textUpper pr-f11 " readonly="true"/>
								</display:column>  
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estCurrency!=baseCurrency  }"> 
			                           <img  class="openpopup"  style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>
								<c:if test="${chargeDiscountFlag}">
								<display:column   title="Disc %"  style="width:80px"> 
								<input type="text" name ="estimateDiscount${accountLineList.id}"  value="${accountLineList.estimateDiscount}" style="width:50px" class="input-textUpper pr-f11 " readonly="true"> 
								<input type="hidden"  name="oldEstimateDiscount${accountLineList.id}" value="${accountLineList.estimateDiscount}"/>
								</display:column>
								</c:if>
								<c:if test="${contractType}">
								<display:column title="Rec&nbsp;Quantity"  style="width:18px;" >
								<input type="text" style="text-align:right" name="estimateSellQuantity${accountLineList.id}" id="estimateSellQuantity${accountLineList.id}" value="${accountLineList.estimateSellQuantity}" size="4" class="input-textUpper pr-f11 " readonly="true"/>
								<input type="hidden"  name="oldEstimateSellQuantity${accountLineList.id}" value="${accountLineList.estimateSellQuantity}"/>
								</display:column> 			
								</c:if>																					
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateSellRate${accountLineList.id}" value="${accountLineList.estimateSellRate }" size="5" class="input-textUpper pr-f11 " readonly="true"/>
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estSellCurrency!=baseCurrency}"> 
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>					
								</c:if>									
								<display:column title="Expense"  style="width:20px" >
								<input type="text" style="text-align:right" name="estimateExpense${accountLineList.id}" value="${accountLineList.estimateExpense }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								<input type="text" style="text-align:right" name="estimatePassPercentage${accountLineList.id}" value="${accountLineList.estimatePassPercentage }" maxlength="4"  size="2" class="input-textUpper pr-f11 " readonly="true"/>
								</display:column> 
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column title="Revenue" style="width:20px;border-right:medium solid #d1d1d1;" >
								<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>
								</c:if>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<display:column title="Revenue" style="width:20px;border-right:medium solid #003366;" >
								<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>								
								</c:if>
								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT Amt"  style="width:50px;">
								<input type="text" name ="estExpVatAmt${accountLineList.id}"  value="${accountLineList.estExpVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true"> 
								</display:column>
								
								<display:column   title="Rec VAT Amt"  style="width:50px;border-right:medium solid #003366;">
								 <input type="text" name ="vatAmt${accountLineList.id}"  value="${accountLineList.estVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								<input type="hidden"  name="estVatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>
								</display:column>								
								</c:if>									
								<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								<display:column title="Quantity"  style="width:18px" >
								<input type="text" style="text-align:right" name="revisionQuantity${accountLineList.id}"  value="${accountLineList.revisionQuantity }" size="4" class="input-textUpper pr-f11 " readonly="true"/>
								<img id="rateImage"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedQuantitys('${accountLineList.id}');"/>
								<input type="hidden" name ="revisionDeviation${accountLineList.id}"  value="${accountLineList.revisionDeviation}" />
								<input type="hidden" name ="revisionSellDeviation${accountLineList.id}"  value="${accountLineList.revisionSellDeviation}" />
								<input type="hidden" name ="oldRevisionSellDeviation${accountLineList.id}" />
								<input type="hidden" name ="oldRevisionDeviation${accountLineList.id}" />
								<input type="hidden"  name="oldRevisionQuantity${accountLineList.id}" value="${accountLineList.revisionQuantity }"/>
								
								<c:if test="${contractType}">
								<input type="hidden"  name="revisionPayableContractRate${accountLineList.id}" value="${accountLineList.revisionPayableContractRate}"/>
								<input type="hidden"  name="revisionPayableContractRateAmmount${accountLineList.id}" value="${accountLineList.revisionPayableContractRateAmmount}"/>
								<input type="hidden"  name="revisionContractRate${accountLineList.id}" value="${accountLineList.revisionContractRate}"/>
								<input type="hidden"  name="revisionContractRateAmmount${accountLineList.id}" value="${accountLineList.revisionContractRateAmmount}"/>
								</c:if>
								<input type="hidden"  name="revisionLocalRate${accountLineList.id}" value="${accountLineList.revisionLocalRate}"/>
								<input type="hidden"  name="revisionLocalAmount${accountLineList.id}" value="${accountLineList.revisionLocalAmount}"/>
								<c:if test="${multiCurrency=='Y'}">
								<input type="hidden"  name="revisionSellCurrency${accountLineList.id}" value="${accountLineList.revisionSellCurrency}"/>
								<input type="hidden"  name="revisionSellValueDate${accountLineList.id}" value="${accountLineList.revisionSellValueDate}"/>
								<input type="hidden"  name="revisionSellExchangeRate${accountLineList.id}" value="${accountLineList.revisionSellExchangeRate}"/>
								<input type="hidden"  name="revisionSellLocalRate${accountLineList.id}" value="${accountLineList.revisionSellLocalRate}"/>
								<input type="hidden"  name="revisionSellLocalAmount${accountLineList.id}" value="${accountLineList.revisionSellLocalAmount}"/>
								</c:if>
								</display:column> 
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="revisionRate${accountLineList.id}" value="${accountLineList.revisionRate}" size="5" class="input-textUpper pr-f11 " readonly="true"/>
								</display:column>  
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.revisionCurrency!=baseCurrency}">
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>
								<c:if test="${chargeDiscountFlag}">
								<display:column   title="Disc %"  style="width:80px"> 
								<input type="text" name ="revisionDiscount${accountLineList.id}"  value="${accountLineList.revisionDiscount}" style="width:50px" class="input-textUpper pr-f11 " readonly="true"> 
								<input type="hidden"  name="oldRevisionDiscount${accountLineList.id}" value="${accountLineList.revisionDiscount}"/>
								</display:column>
								</c:if>	
								<c:if test="${contractType}">
								<display:column title="Rec&nbsp;Quantity"  style="width:18px" >
								<input type="text" style="text-align:right" name="revisionSellQuantity${accountLineList.id}"  value="${accountLineList.revisionSellQuantity }" size="4" class="input-textUpper pr-f11 " readonly="true"/>
								<input type="hidden"  name="oldRevisionSellQuantity${accountLineList.id}" value="${accountLineList.revisionSellQuantity }"/>
								</display:column>			
								</c:if>											
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="revisionSellRate${accountLineList.id}" value="${accountLineList.revisionSellRate}" size="5" class="input-textUpper pr-f11 " readonly="true"/>
								<input type="hidden"  name="oldRevisionSellRate${accountLineList.id}" value="${accountLineList.revisionSellRate}"/>
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.revisionSellCurrency!=baseCurrency}">
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>					
								</c:if>
								<display:column title="Variance Expense Amount"  style="width:20px" >
								<input type="text" style="text-align:right" id="varianceExpenseAmount${accountLineList.id}" name="varianceExpenseAmount${accountLineList.id}" value="${accountLineList.varianceExpenseAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>	
								<display:column title="Variance Revenue Amount"  style="width:20px" >
								<input type="text" style="text-align:right" id="varianceRevenueAmount${accountLineList.id}" name="varianceRevenueAmount${accountLineList.id}" value="${accountLineList.varianceRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>																																															

								<display:column title="Expense"  style="width:20px" >
								<input type="text" style="text-align:right" name="revisionExpense${accountLineList.id}" value="${accountLineList.revisionExpense }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								<input type="text" style="text-align:right" name="revisionPassPercentage${accountLineList.id}" value="${accountLineList.revisionPassPercentage }" maxlength="4"  size="2" class="input-textUpper pr-f11 " readonly="true"/>
								</display:column> 
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column title="Revenue" style="width:20px;border-right:medium solid #d1d1d1;" >
								<input type="text" style="text-align:right" name="revisionRevenueAmount${accountLineList.id}" value="${accountLineList.revisionRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>
								</c:if>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}"> 
								<display:column title="Revenue" style="width:20px;border-right:medium solid #003366;" >
								<input type="text" style="text-align:right" name="revisionRevenueAmount${accountLineList.id}" value="${accountLineList.revisionRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>								
								</c:if>
								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT Amt"  style="width:50px;">
								<input type="text" name ="revisionExpVatAmt${accountLineList.id}"  value="${accountLineList.revisionExpVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true"> 
								</display:column>
								<display:column   title="Rec VAT Amt"  style="width:50px;border-right:medium solid #003366;">
								<input type="text" name ="revisionVatAmt${accountLineList.id}"  value="${accountLineList.revisionVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true"> 
								</display:column>								
								</c:if>																
								</c:if>
								<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								<display:column title="Quantity"   headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="recQuantity${accountLineList.id}"  value="${accountLineList.recQuantity}" size="4" class="input-textUpper pr-f11 " readonly="true"/>
								<input type="hidden"  name="tempRecQuantity${accountLineList.id}" value="${accountLineList.recQuantity}"/>
								</display:column> 
								<c:if test="${chargeDiscountFlag}">
								<display:column   title="Disc %"  style="width:80px"> 
								<input type="text" name ="actualDiscount${accountLineList.id}"  value="${accountLineList.actualDiscount}" style="width:50px" class="input-textUpper pr-f11 " readonly="true"> 
								<input type="hidden"  name="oldActualDiscount${accountLineList.id}" value="${accountLineList.actualDiscount}"/>
								</display:column>
								</c:if>
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="recRate${accountLineList.id}" value="${accountLineList.recRate}" size="5" class="input-textUpper pr-f11 " readonly="true"/>
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.recRateCurrency!=baseCurrency}"> 
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>					
								</c:if>
								<display:column title="Revenue" style="width:20px" >
								<img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png"  onclick="getReceivableFieldDetails('${accountLineList.id}');" />
								<input type="text" style="text-align:right" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								<configByCorp:fieldVisibility componentId="component.field.Pricing.GeneratingInvoice">
								<c:if test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
						        	<a><img align="top" title="Forms" onclick="findUserPermission1('${accountLineList.recInvoiceNumber}','${accountLineList.companyDivision}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></a>
						        </c:if>
						        </configByCorp:fieldVisibility>
								</display:column>
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Rec VAT Amt"  style="width:50px;border-right:medium solid #999999;">
								<input type="hidden" name ="recVatAmt${accountLineList.id}"  value="${accountLineList.recVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								<c:set var="recTemp1" value="${accountLineList.recVatAmt}" />
								<c:set var="recTemp2" value="${accountLineList.qstRecVatAmt}" />
								<c:set var="recTemp" value="${recTemp1 + recTemp2}"/>
								<input type="text" name ="recVatAmtTemp${accountLineList.id}"  value="${recTemp}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								 
								</display:column>
								</c:if>
																
								<display:column title="Expense"  headerClass="RemoveBorderRight" style="width:80px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" size="6" class="input-textUpper pr-f11 " readonly="true"/>
								
								<c:if test="${contractType}">
								<input type="hidden"  name="payableContractRateAmmount${accountLineList.id}" value="${accountLineList.payableContractRateAmmount}"/>
								<input type="hidden"  name="contractRate${accountLineList.id}" value="${accountLineList.contractRate}"/>
								<input type="hidden"  name="contractRateAmmount${accountLineList.id}" value="${accountLineList.contractRateAmmount}"/>
								</c:if>
								<c:choose>
								<c:when test="${(serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD') }">
						        <img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png"  onclick="getPayableSectionDetails('${accountLineList.id}');" />
						        </c:when> 
						        <c:otherwise>
								<img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png"  />
								</c:otherwise></c:choose>
								<input type="hidden"  name="country${accountLineList.id}" value="${accountLineList.country}"/>
								<input type="hidden"  name="valueDate${accountLineList.id}" value="${accountLineList.valueDate}"/>
								<input type="hidden"  name="exchangeRate${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
								<input type="hidden"  name="localAmount${accountLineList.id}" value="${accountLineList.localAmount}"/>
								<input type="hidden"  name="recRateCurrency${accountLineList.id}" value="${accountLineList.recRateCurrency}"/>
								<input type="hidden"  name="racValueDate${accountLineList.id}" value="${accountLineList.racValueDate}"/>
								<input type="hidden"  name="recRateExchange${accountLineList.id}" value="${accountLineList.recRateExchange}"/>
								<input type="hidden"  name="recCurrencyRate${accountLineList.id}" value="${accountLineList.recCurrencyRate}"/>
								<input type="hidden"  name="actualRevenueForeign${accountLineList.id}" value="${accountLineList.actualRevenueForeign}"/>
								<input type="hidden"  name="recInvoiceNumber${accountLineList.id}" value="${accountLineList.recInvoiceNumber}"/>
								<input type="hidden"  name="invoiceNumber${accountLineList.id}" value="${accountLineList.invoiceNumber}"/>
								<input type="hidden"  name="invoiceDate${accountLineList.id}" value="${accountLineList.invoiceDate}"/>
								<input type="hidden"  name="receivedDate${accountLineList.id}" value="${accountLineList.receivedDate}"/>
								<input type="hidden"  name="payingStatus${accountLineList.id}" value="${accountLineList.payingStatus}"/>
								
								<input type="hidden"  name="recVatGl${accountLineList.id}" value="${accountLineList.recVatGl}"/>
								<input type="hidden"  name="payVatGl${accountLineList.id}" value="${accountLineList.payVatGl}"/>
								<input type="hidden"  name="qstRecVatGl${accountLineList.id}" value="${accountLineList.qstRecVatGl}"/>
								<input type="hidden"  name="qstPayVatGl${accountLineList.id}" value="${accountLineList.qstPayVatGl}"/>
								<input type="hidden"  name="qstRecVatAmt${accountLineList.id}" value="${accountLineList.qstRecVatAmt}"/>
								<input type="hidden"  name="qstPayVatAmt${accountLineList.id}" value="${accountLineList.qstPayVatAmt}"/>
								<c:if test="${empty accountLineList.payAccDate}">
								<img id="rateImage"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedQuantityLocalAmounts('${accountLineList.id}');"/>
								</c:if>
								<c:choose>								
						         <c:when test="${accountLineList.payingStatus=='A'}">
						         <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						        </c:when>
						        <c:when test="${accountLineList.payingStatus=='N'}">
						        <img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						        </c:when>
						        <c:when test="${accountLineList.payingStatus=='P'}">
						        <img id="target" src="${pageContext.request.contextPath}/images/q1.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						        </c:when>
						        <c:when test="${accountLineList.payingStatus=='S'}">
						         <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						        </c:when>
						         <c:when test="${accountLineList.payingStatus=='I'}">
						         <img id="active" src="${pageContext.request.contextPath}/images/internal-Cost.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						         </c:when>
						        <c:otherwise>
						         </c:otherwise>						         
								</c:choose>							
								</display:column>
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT Amt"  style="width:50px;border-right:medium solid #d1d1d1;">
								<input type="hidden" name ="payVatAmt${accountLineList.id}"  value="${accountLineList.payVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								<c:set var="payTemp1" value="${accountLineList.payVatAmt}" />
								<c:set var="payTemp2" value="${accountLineList.qstPayVatAmt}" />
								<c:set var="payTemp" value="${payTemp1 + payTemp2}"/>
								<input type="text" name ="payVatAmtTemp${accountLineList.id}"  value="${payTemp}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								</display:column>
								</c:if>								
								
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;border-right:medium solid #003366;" >
									<c:choose>
			                        <c:when test="${accountLineList.country!=baseCurrency}"> 
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column>					
								</c:if>																										   
	                            <display:column title="Description" style="width:100px" >
								<textarea  name="quoteDescription${accountLineList.id}"  id="quoteDescription${accountLineList.id}"  style="overflow:auto;" class="input-textUpper pr-f11 " readonly="true"  cols="15" rows="1">${accountLineList.quoteDescription }</textarea>
								</display:column> 
								<c:if test="${companyDivisionGlobal>1}"> 
								<display:column   title="Div" style="width:58px" >  
								<c:choose>
	                            <c:when test="${((!(accountLineList.revisionExpense!='0.00' || accountLineList.revisionRevenueAmount!='0.00' || accountLineList.actualRevenue!='0.00' || accountLineList.actualExpense!='0.00' ||accountLineList.distributionAmount!='0.00' || accountLineList.chargeCode=='MGMTFEE'|| accountLineList.chargeCode=='DMMFEE'|| accountLineList.chargeCode=='DMMFXFEE' ))&& (!trackingStatus.accNetworkGroup  && trackingStatus.soNetworkGroup && billingDMMContractType))}">
	                            <select name ="companyDivision${accountLineList.id}" id ="companyDivision${accountLineList.id}" style="width:58px" onchange="checkCompanyDiv('${accountLineList.id}')" class="list-menu pr-f11"> 
										<c:forEach var="chrms" items="${companyDivis}" >
		                                  <c:choose>
			                                  <c:when test="${chrms == accountLineList.companyDivision}">
			                                  <c:set var="selectedInd" value=" selected"></c:set>
			                                  </c:when>
			                                 <c:otherwise>
			                                  <c:set var="selectedInd" value=""></c:set>
			                                 </c:otherwise>
		                                  </c:choose>
		                                 <option value="<c:out value='${chrms}' />" <c:out value='${selectedInd}' />>
		                                <c:out value="${chrms}"></c:out>
		                                </option>
		                                </c:forEach> 
								</select>
	                            </c:when>
	                            <c:otherwise>
								<input type="text" style="width:58px" name="companyDivision${accountLineList.id}" id="companyDivision${accountLineList.id}" value="${accountLineList.companyDivision}"  maxlength="3" class="input-textUpper pr-f11 "  readonly="true" />
								</c:otherwise>
								</c:choose>
								</display:column>
								</c:if>
								<c:choose>
		  	                     <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
		  	                     <display:column style="width:60px;padding:0px;text-align:center;" 
								title="Display&nbsp;On Quote All<input type='checkbox' disabled='disabled' style='margin-left:20px; vertical-align:middle;' name='selectall' onclick='selectAll(this);'/>" media="html" > 
                                  <c:if test="${accountLineList.displayOnQuote}"> 
                                  <input type="hidden"  name="displayOnQuote${accountLineList.id}" value="${accountLineList.displayOnQuote}" id="displayOnQuote${accountLineList.id}"/>
                                  <input type="checkbox"  name="Quote"  checked="checked"  disabled="disabled" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.displayOnQuote==false}">
                                 <input type="hidden"  name="displayOnQuote${accountLineList.id}" value="${accountLineList.displayOnQuote}" id="displayOnQuote${accountLineList.id}"/>
                                <input type="checkbox"  name="Quote"   disabled="disabled" /> 
                                 </c:if>
                                 </display:column>
		  	                     </c:when>
		  	                     <c:otherwise>
								<display:column style="width:60px;padding:0px;text-align:center;" 
								title="Display&nbsp;On Quote All<input type='checkbox' style='margin-left:20px; vertical-align:middle;' name='selectall' onclick='selectAll(this);'/>" media="html" > 
                                  <c:if test="${accountLineList.displayOnQuote}"> 
                                  <input type="hidden"  name="displayOnQuote${accountLineList.id}" value="${accountLineList.displayOnQuote}" id="displayOnQuote${accountLineList.id}"/>
                                  <input type="checkbox"  name="Quote"  checked="checked"  disabled="disabled" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.displayOnQuote==false}">
                                 <input type="hidden"  name="displayOnQuote${accountLineList.id}" value="${accountLineList.displayOnQuote}" id="displayOnQuote${accountLineList.id}"/>
                                <input type="checkbox"  name="Quote"   disabled="disabled" /> 
                                 </c:if>
                                 </display:column></c:otherwise></c:choose>
                                 <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
                                 <c:choose>
		  	                     <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
		  	                      <display:column title="Roll Up In Invoice <input type='checkbox' disabled='disabled' name='selectall' id='selectall' style='margin-left; vertical-align:middle;' onclick='selectAllRollInvoice(this);'/>All" media="html" >
                                 <c:choose>
                                 <c:when test="${accountLineList.rollUpInInvoice}">
                                 <input type="hidden"  name="rollUpInInvoice${accountLineList.id}" value="${accountLineList.rollUpInInvoice}" id="rollUpInInvoice${accountLineList.id}"/>
                                  <input type="checkbox"  name="rollUpInInvoice"  checked="checked"  disabled="disabled" />                                  
                                 </c:when>
                                 <c:otherwise>
                                 <input type="hidden"  name="rollUpInInvoice${accountLineList.id}" value="${accountLineList.rollUpInInvoice}" id="rollUpInInvoice${accountLineList.id}"/>
                                <input type="checkbox"  name="rollUpInInvoice"   disabled="disabled" />                                  
                                 </c:otherwise>
                                 </c:choose>
                                 </display:column>
		  	                     </c:when>
		  	                     <c:otherwise>
                                 <display:column title="Roll Up In Invoice <input type='checkbox' name='selectall' id='selectall' style='margin-left; vertical-align:middle;' onclick='selectAllRollInvoice(this);'/>All" media="html" >
                                 <c:choose>
                                 <c:when test="${accountLineList.rollUpInInvoice}">
                                 <input type="hidden"  name="rollUpInInvoice${accountLineList.id}" value="${accountLineList.rollUpInInvoice}" id="rollUpInInvoice${accountLineList.id}"/>
                                  <input type="checkbox"  name="rollUpInInvoice"  checked="checked"  disabled="disabled" />                                  
                                 </c:when>
                                 <c:otherwise>
                                 <input type="hidden"  name="rollUpInInvoice${accountLineList.id}" value="${accountLineList.rollUpInInvoice}" id="rollUpInInvoice${accountLineList.id}"/>
                                <input type="checkbox"  name="rollUpInInvoice"   disabled="disabled" />                                  
                                 </c:otherwise>
                                 </c:choose>
                                 </display:column></c:otherwise></c:choose>
                                 </configByCorp:fieldVisibility>
                                 <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">	
								<c:choose>
		  	                    <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
		  	                    <display:column title="Additional Services <input type='checkbox' style='margin-left:22px; vertical-align:middle;' disabled='disabled' name='selectall' onclick='selectAdd(this);'/>All" media="html"   style="width:50px;padding:0px;text-align:center;"> 
                                 <c:if test="${accountLineList.additionalService}"> 
                                  <input type="hidden"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" checked="checked"  value="${accountLineList.additionalService}"  /> 
                                  <input type="checkbox"  name="additionalService"  checked="checked"  disabled="disabled" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.additionalService==false}">
                                 <input type="hidden"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" value="${accountLineList.additionalService}"  /> 
                                 <input type="checkbox"  name="additionalService"    disabled="disabled" /> 
                                 </c:if>
                                 </display:column>
		  	                    </c:when>
		  	                    <c:otherwise>
								<display:column title="Additional Services <input type='checkbox' style='margin-left:22px; vertical-align:middle;' name='selectall' onclick='selectAdd(this);'/>All" media="html"   style="width:50px;padding:0px;text-align:center;"> 
                                 <c:if test="${accountLineList.additionalService}"> 
                                  <input type="hidden"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" checked="checked"  value="${accountLineList.additionalService}"  /> 
                                  <input type="checkbox"  name="additionalService"  checked="checked"  disabled="disabled" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.additionalService==false}">
                                 <input type="hidden"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" value="${accountLineList.additionalService}"  /> 
                                 <input type="checkbox"  name="additionalService"    disabled="disabled" /> 
                                 </c:if>
                                 </display:column></c:otherwise></c:choose>
								</configByCorp:fieldVisibility>
	                            <c:choose>
		  	                    <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
		  	                    <display:column style="width:50px;padding:0px;text-align:center;" title="<font style='padding-left:5px;'>Act &nbsp;&nbsp;All </font> <br><input type='checkbox' disabled='disabled' style='margin-left:6px; vertical-align:middle;' checked='checked' id='selectallActive' name='selectallActive' onclick='selectAllActive(this);'/>"  media="html">
				                 <c:if test="${accountLineList.status}"> 
				                 <input type="hidden"  name="statusCheck${accountLineList.id}" value="${accountLineList.status}"/>
				                 <input type="checkbox"  name="statusCheckHidden"  checked="checked" disabled="disabled" /> 
				                 </c:if>
				               </display:column>
		  	                    </c:when>
		  	                    <c:otherwise>
	                            <display:column style="width:50px;padding:0px;text-align:center;" title="<font style='padding-left:5px;'>Act &nbsp;&nbsp;All </font> <br><input type='checkbox' style='margin-left:6px; vertical-align:middle;' checked='checked' id='selectallActive' name='selectallActive' onclick='selectAllActive(this);'/>"  media="html">
				                 <c:if test="${accountLineList.status}"> 
				                 <input type="hidden"  name="statusCheck${accountLineList.id}" value="${accountLineList.status}"/>
				                 <input type="checkbox"  name="statusCheckHidden"  checked="checked" disabled="disabled" /> 
				                 </c:if>
				               </display:column></c:otherwise></c:choose>
								<display:column title="INV"   style="width:50px;padding:0px;">
								<c:choose> 
						        <c:when test="${(trackingStatus.accNetworkGroup && billingCMMContractType) || serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD' }">
						        <input type="checkbox"   checked="checked" disabled="disabled"   />
						        </c:when> 
						        <c:otherwise>
						         <c:if test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
						         <input type="checkbox"   checked="checked" disabled="disabled"   />
						         </c:if>
						         <c:if test="${accountLineList.recInvoiceNumber == '' || accountLineList.recInvoiceNumber == null}">
						         <c:if test="${accountLineList.selectiveInvoice}">
						           <input type="checkbox"   checked="checked" onclick="selectiveInvoiceCheck(${accountLineList.id},this)" />
						         </c:if>
						         <c:if test="${accountLineList.selectiveInvoice==false}">
								   <input type="checkbox"     onclick="selectiveInvoiceCheck(${accountLineList.id},this)" />  
							     </c:if>
							     </c:if>
							     </c:otherwise></c:choose>
						        </display:column>
				               	 <display:column title="Audit" style="width:25px; text-align: center;">
	    							<a><img align="middle" src="images/report-ext.png" style="margin: 0px 0px 0px 0px;" onclick="window.open('auditList.html?id=${accountLineList.id}&tableName=accountline&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"></a>
								</display:column>                                
	                         </c:when>
	                         <c:otherwise> 
								<display:column sortProperty="accountLineNumber"   titleKey="accountLine.accountLineNumber"  style="width:10px;" > 
								<input type="text" style="text-align:left" name="accountLineNumber${accountLineList.id}" value="${accountLineList.accountLineNumber }" size="1" maxlength="3" class="input-text pr-f11" onkeydown="return onlyNumberAllowed(event)" onchange="checkAccountLineNumber('accountLineNumber${accountLineList.id}');" />
								</display:column>
								<display:column   titleKey="accountLine.category" style="width:70px" > 
								<select name ="category${accountLineList.id}" id="category${accountLineList.id}" style="width:70px" onchange="selectPricingMarketPlace('category${accountLineList.id}','${accountLineList.id}');" class="list-menu pr-f11"> 
								<c:forEach var="chrms" items="${category}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.category}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								 </display:column>
								<display:column title="Charge &nbsp;Code" style="width:100px; whitespace: nowrap;" >
			                     <c:choose>
	                            <c:when test="${empty accountLineList.recAccDate && empty accountLineList.payAccDate}">
									<input type="text" id="chargeCode${accountLineList.id}" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode }" size="10" maxlength="25" class="input-text pr-f11" onkeyup="autoCompleterAjaxCallChargeCode(${accountLineList.id},'ChargeNameDivId${accountLineList.id}','${accountLineList.contract}')" onblur="updateContractChangeCharge('chargeCode${accountLineList.id}','accChargeCodeTemp${accountLineList.id}','${accountLineList.id}','${accountLineList.contract}');fillDiscription('quoteDescription${accountLineList.id}','chargeCode${accountLineList.id}','category${accountLineList.id}','recGl${accountLineList.id}','payGl${accountLineList.id}','contractCurrency${accountLineList.id}','contractExchangeRate${accountLineList.id}','contractValueDate${accountLineList.id}','payableContractCurrency${accountLineList.id}','payableContractExchangeRate${accountLineList.id}','payableContractValueDate${accountLineList.id}','accChargeCodeTemp${accountLineList.id}','${accountLineList.id}')" onchange="checkChargeCode('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');" />								
									<img class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="chk('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');findInternalCostVendorCode('${accountLineList.id}');"  src="<c:url value='/images/open-popup.gif'/>" />
									<div id="ChargeNameDivId${accountLineList.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
	                            </c:when>
	                            <c:otherwise>
	                            	  <input type="text" name="chargeCode${accountLineList.id}"  value="${accountLineList.chargeCode}" size="10"  maxlength="25" class="input-textUpper pr-f11 " readonly="true"/>
	                            </c:otherwise>
                               </c:choose>
                               <input type="hidden"  name="vendorCodeOwneroperator${accountLineList.id}" id="vendorCodeOwneroperator${accountLineList.id}"/>
								<input type="hidden"  name="accChargeCodeTemp${accountLineList.id}" value="${accountLineList.chargeCode}"/>
								<input type="hidden"  name="contractCurrency${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="recGl${accountLineList.id}" value="${accountLineList.recGl}"/>
								<input type="hidden"  name="payGl${accountLineList.id}" value="${accountLineList.payGl}"/>
								<input type="hidden"  name="contractExchangeRate${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>
								<input type="hidden"  name="contractValueDate${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="payableContractCurrency${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractExchangeRate${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								<input type="hidden"  name="payableContractValueDate${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="division${accountLineList.id}" value="${accountLineList.division}"/>
								<input type="hidden"  name="payPostDate${accountLineList.id}" value="${accountLineList.payPostDate}"/>
								<input type="hidden"  name="VATExclude${accountLineList.id}" value="${accountLineList.VATExclude}"/>
								<input type="hidden"  name="oldVATExclude${accountLineList.id}" value="${accountLineList.VATExclude}"/>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<input type="hidden"  name="vatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>
								<input type="hidden"  name="estVatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>	
								<input type="hidden"  name="estExpVatAmt${accountLineList.id}" value="${accountLineList.estExpVatAmt}"/>								
								</c:if>	
								<c:if test="${systemDefaultVatCalculationNew!='Y' || discountUserFlag.pricingRevision!=true}">
								<input type="hidden"  name="revisionVatAmt${accountLineList.id}" value="${accountLineList.revisionVatAmt}"/>
								<input type="hidden"  name="revisionExpVatAmt${accountLineList.id}" value="${accountLineList.revisionExpVatAmt}"/>								
								</c:if>
								<c:if test="${systemDefaultVatCalculationNew!='Y' || discountUserFlag.pricingActual!=true}">
								<input type="hidden"  name="recVatAmt${accountLineList.id}" value="${accountLineList.recVatAmt}"/>								
								<input type="hidden"  name="payVatAmt${accountLineList.id}" value="${accountLineList.payVatAmt}"/>																														
								</c:if>	
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<input type="hidden"  name="recVatDescr${accountLineList.id}" value="${accountLineList.recVatDescr}"/>
								<input type="hidden"  name="recVatPercent${accountLineList.id}" value="${accountLineList.recVatPercent}"/>
								<input type="hidden"  name="payVatDescr${accountLineList.id}" value="${accountLineList.payVatDescr}"/>
								<input type="hidden"  name="payVatPercent${accountLineList.id}" value="${accountLineList.payVatPercent}"/>
								</c:if>
								<input type="hidden"  name="accRecVatDescr${accountLineList.id}" value="${accountLineList.recVatDescr}"/>
								<input type="hidden"  name="accPayVatDescr${accountLineList.id}"  value="${accountLine.payVatDescr}"/>
								<input type="hidden"  name="oldRecVatDescr${accountLineList.id}" value="${accountLineList.recVatDescr}"/>
								<input type="hidden"  name="oldPayVatDescr${accountLineList.id}" value="${accountLineList.payVatDescr}"/>	
								<c:if test="${contractType}">
								<input type="hidden" id="estimatePayableContractCurrencyNew${accountLineList.id}" name="estimatePayableContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimatePayableContractCurrency}"/>
								<input type="hidden" id="estimatePayableContractValueDateNew${accountLineList.id}" name="estimatePayableContractValueDateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractValueDate}"/>
								<input type="hidden" id="estimatePayableContractExchangeRateNew${accountLineList.id}" name="estimatePayableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractExchangeRate}"/>
								<input type="hidden" id="estimatePayableContractRateNew${accountLineList.id}" name="estimatePayableContractRateNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRate}"/>
								<input type="hidden"  name="estimatePayableContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimatePayableContractRateAmmount}"/>
								<input type="hidden"  name="estimateContractCurrencyNew${accountLineList.id}" value="${accountLineList.estimateContractCurrency}"/>
								<input type="hidden"  name="estimateContractValueDateNew${accountLineList.id}" value="${accountLineList.estimateContractValueDate}"/>
								<input type="hidden"  name="estimateContractExchangeRateNew${accountLineList.id}" value="${accountLineList.estimateContractExchangeRate}"/>
								<input type="hidden"  name="estimateContractRateNew${accountLineList.id}" value="${accountLineList.estimateContractRate}"/>
								<input type="hidden"  name="estimateContractRateAmmountNew${accountLineList.id}" value="${accountLineList.estimateContractRateAmmount}"/>
								
								<input type="hidden"  name="revisionContractCurrency${accountLineList.id}" value="${accountLineList.revisionContractCurrency}"/>
								<input type="hidden"  name="revisionContractValueDate${accountLineList.id}" value="${accountLineList.revisionContractValueDate}"/>
								<input type="hidden"  name="revisionContractExchangeRate${accountLineList.id}" value="${accountLineList.revisionContractExchangeRate}"/>
 
								<input type="hidden"  name="revisionPayableContractCurrency${accountLineList.id}" value="${accountLineList.revisionPayableContractCurrency}"/>
								<input type="hidden"  name="revisionPayableContractValueDate${accountLineList.id}" value="${accountLineList.revisionPayableContractValueDate}"/>
								<input type="hidden"  name="revisionPayableContractExchangeRate${accountLineList.id}" value="${accountLineList.revisionPayableContractExchangeRate}"/>
								
								<input type="hidden"  name="contractCurrencyNew${accountLineList.id}" value="${accountLineList.contractCurrency}"/>
								<input type="hidden"  name="contractValueDateNew${accountLineList.id}" value="${accountLineList.contractValueDate}"/>
								<input type="hidden"  name="contractExchangeRateNew${accountLineList.id}" value="${accountLineList.contractExchangeRate}"/>


								<input type="hidden"  name="payableContractCurrencyNew${accountLineList.id}" value="${accountLineList.payableContractCurrency}"/>
								<input type="hidden"  name="payableContractValueDateNew${accountLineList.id}" value="${accountLineList.payableContractValueDate}"/>
								<input type="hidden"  name="payableContractExchangeRateNew${accountLineList.id}" value="${accountLineList.payableContractExchangeRate}"/>
								
								</c:if>						
								<input type="hidden"  id="estCurrencyNew${accountLineList.id}" name="estCurrencyNew${accountLineList.id}" value="${accountLineList.estCurrency}"/>
								<input type="hidden"  id="estValueDateNew${accountLineList.id}" name="estValueDateNew${accountLineList.id}" value="${accountLineList.estValueDate}"/>
								<input type="hidden"  id="estExchangeRateNew${accountLineList.id}" name="estExchangeRateNew${accountLineList.id}" value="${accountLineList.estExchangeRate}"/>
								<input type="hidden"  id="estLocalRateNew${accountLineList.id}" name="estLocalRateNew${accountLineList.id}" value="${accountLineList.estLocalRate}"/>
								<input type="hidden"  name="estLocalAmountNew${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
								<input type="hidden"  name="estSellCurrencyNew${accountLineList.id}" value="${accountLineList.estSellCurrency}"/>
								<input type="hidden"  name="estSellValueDateNew${accountLineList.id}" value="${accountLineList.estSellValueDate}"/>
								<input type="hidden"  name="estSellExchangeRateNew${accountLineList.id}" value="${accountLineList.estSellExchangeRate}"/>
								<input type="hidden"  name="estSellLocalRateNew${accountLineList.id}" value="${accountLineList.estSellLocalRate}"/>
								<input type="hidden"  name="estSellLocalAmountNew${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
								<input type="hidden"  name="estSellLocalAmount${accountLineList.id}" value="${accountLineList.estSellLocalAmount}"/>
								<input type="hidden"  name="buyDependSellNew${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
								<input type="hidden"  name="buyDependSell${accountLineList.id}" value="${accountLineList.buyDependSell}"/>
								<input type="hidden"  name="oldEstimateSellDeviationNew${accountLineList.id}" />								
								<input type="hidden"  name="oldEstimateDeviationNew${accountLineList.id}" />
								<input type="hidden"  name="revisionCurrency${accountLineList.id}" value="${accountLineList.revisionCurrency}"/>
								<input type="hidden"  name="revisionValueDate${accountLineList.id}" value="${accountLineList.revisionValueDate}"/>
								<input type="hidden"  name="revisionExchangeRate${accountLineList.id}" value="${accountLineList.revisionExchangeRate}"/>
								<input type="hidden"  name="estLocalAmount${accountLineList.id}" value="${accountLineList.estLocalAmount}"/>
								<input type="hidden"  name="countryNew${accountLineList.id}" value="${accountLineList.country}"/>
								<input type="hidden"  name="valueDateNew${accountLineList.id}" value="${accountLineList.valueDate}"/>
								<input type="hidden"  name="exchangeRateNew${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
								<input type="hidden"  name="accountLineCostElement${accountLineList.id}" value="${accountLineList.accountLineCostElement}"/>
								<input type="hidden"  name="accountLineScostElementDescription${accountLineList.id}" value="${accountLineList.accountLineScostElementDescription}"/>
								<input type="hidden"  name="itemNew${accountLineList.id}" value="${accountLineList.itemNew}"/>
								<input type="hidden"  name="checkNew${accountLineList.id}" value="${accountLineList.checkNew}"/>
							<c:if test="${companyDivisionGlobal<=1}"> 
                   				<s:hidden name="companyDivision${accountLineList.id}" id="companyDivision${accountLineList.id}"  value="${accountLineList.companyDivision}" />
		                    	<s:hidden name="tempCompanyDivision${accountLineList.id}" id="tempCompanyDivision${accountLineList.id}"  value="${accountLineList.companyDivision}" />
		                    	<s:hidden name="recAccDate${accountLineList.id}" id="recAccDate${accountLineList.id}"  value="${accountLineList.recAccDate}" />
		                    	<s:hidden name="payAccDate${accountLineList.id}" id="payAccDate${accountLineList.id}"  value="${accountLineList.payAccDate}" />
									
							</c:if>						
														
								</display:column>
								<display:column   title="Bill&nbsp;To&nbsp;Code" style="width:100px" > 
								<input type="text" id="billToCode${accountLineList.id}" name="billToCode${accountLineList.id}"  value="${accountLineList.billToCode }" size="6" class="input-textUpper pr-f11 " readonly="true" />
								<c:if test="${accountInterface=='Y'}">
								<c:if test="${!utsiRecAccDateFlag  && !utsiPayAccDateFlag}"> 
								<c:if test="${(!accNonEditFlag && empty accountLineList.recAccDate) || (accNonEditFlag && (accountLineList.recInvoiceNumber=='' || empty accountLineList.recInvoiceNumber))}"> 
								  <c:if test="${receivableSectionDetail==true}"> 
								   <img align="top" id="accountLineBillToCodeOpenpopup"  class="openpopup" width="17" height="20" onclick="javascript:openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=billToName${accountLineList.id}&fld_code=billToCode${accountLineList.id}');" id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" />
								   </c:if>
								 </c:if>
								<c:if test="${(!accNonEditFlag && not empty accountLineList.recAccDate) || (accNonEditFlag && (accountLineList.recInvoiceNumber!='' && not empty accountLineList.recInvoiceNumber))}"> 
								<c:if test="${receivableSectionDetail==true}"> 
								   <c:if test="${(!accNonEditFlag)}">
								  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as Sent To Acc has been already filled.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if>
									<c:if test="${(accNonEditFlag)}">
								  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as invoice has been generated.')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if>
									</c:if>
								</c:if>
								</c:if>
								<c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
								<c:if test="${receivableSectionDetail==true}"> 
								  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Billing Party as sent to Accounting and/or already Invoiced in UTSI Instance')" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if> 
								</c:if>
								</c:if>
								<c:if test="${accountInterface!='Y'}">
								<c:if test="${accountLineList.recInvoiceNumber==''|| accountLineList.recInvoiceNumber==null}"> 
								  <c:if test="${receivableSectionDetail==true}"> 
								   <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="checkBilltoCode();" id="openpopup10.img" src="<c:url value='/images/open-popup.gif'/>" />
								   </c:if>
								 </c:if>
								<c:if test="${accountLineList.recInvoiceNumber!=''&& accountLineList.recInvoiceNumber!=null}"> 
								<c:if test="${receivableSectionDetail==true}"> 
								  <img align="top" id="accountLineBillToCodeOpenpopup" class="openpopup" width="17" height="20" onclick="checkBilltoCode();" id="openpopup11.img" src="<c:url value='/images/open-popup.gif'/>" />  
									</c:if>
								</c:if>
								</c:if>
								</display:column>
								<display:column  title="Bill&nbsp;To&nbsp;Name" style="width:50px" >
								<input type="text" id="billToName${accountLineList.id}" name="billToName${accountLineList.id}" readonly="true" value="${accountLineList.billToName }" size="15" class="input-textUpper pr-f11 "  /> 
								</display:column>
								<display:column   title="Vendor Code" style="width:100px" > 
							    <c:choose>
	                            <c:when test="${empty accountLineList.payAccDate}">
								<input type="text" name="vendorCode${accountLineList.id}" id="vendorCode${accountLineList.id}" value="${accountLineList.vendorCode }" size="6" class="input-text pr-f11" onchange="checkAccountInterface('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}','actgCode${accountLineList.id}');fillCurrencyByChargeCode('${accountLineList.id}');"  onfocus="enablePreviousFunction('chargeCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','${accountLineList.contract}');"/>
								<img  class="openpopup" style="text-align:right;vertical-align:top;" width="17" height="20" onclick="javascript:winOpenForActgCode('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}');" src="<c:url value='/images/open-popup.gif'/>" />
								<img  class="openpopup"  id="hidMarketPlace${accountLineList.id}" style="text-align:right;vertical-align:top;display:none;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}','${contractType}');"   src="<c:url value='/images/market-areaPP1.png'/>" />
								<c:if test="${accountLineList.category=='Destin' || accountLineList.category=='Origin' }">
								<c:if test="${pricePointFlag}">
								<c:if test="${accountLineList.externalIntegrationReference=='PP'}">
								<!--<a class="tooltips"><img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;float:right;vertical-align:top;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}');"   src="<c:url value='/images/market-areaPP1.png'/>" /><span>Pricing Point Ref# :<br/>${accountLineList.reference}</span></a>
							    --><img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;vertical-align:top;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}','${contractType}');"   src="<c:url value='/images/market-areaPP1.png'/>" />
							    </c:if>
							    <c:if test="${accountLineList.externalIntegrationReference!='PP'}">
								<img  class="openpopup"  id="hidMarketPlace11${accountLineList.id}" style="text-align:right;vertical-align:top;" onclick="javascript:marketPlaceCode('vendorCode${accountLineList.id}','category${accountLineList.id}','${accountLineList.id}','estimateVendorName${accountLineList.id}','estimateRate${accountLineList.id}','${contractType}');"   src="<c:url value='/images/market-areaPP1.png'/>" />
							    </c:if>
							    </c:if>
							    </c:if>
	                            </c:when>
	                            <c:otherwise>
	                                  <input type="text" id="vendorCode${accountLineList.id}" name="vendorCode${accountLineList.id}"  value="${accountLineList.vendorCode }" size="6" class="input-textUpper pr-f11 " readonly="true" />
	                            </c:otherwise>
                               </c:choose>
								 <input type="hidden" id="pricePointAgent${accountLineList.id}"  name="pricePointAgent${accountLineList.id}" />
				                 <input type="hidden" id="pricePointMarket${accountLineList.id}"  name="pricePointMarket${accountLineList.id}" />
				                 <input type="hidden" id="pricePointTariff${accountLineList.id}"  name="pricePointTariff${accountLineList.id}" />
				                 <input type="hidden" id="actgCode${accountLineList.id}"  name="actgCode${accountLineList.id}" value="${accountLineList.actgCode}"/>
								</display:column>
								<display:column  title="Vendor &nbsp;Name" style="width:50px" >
								<c:choose>
	                            <c:when test="${empty accountLineList.payAccDate}">
								<input type="text" name="estimateVendorName${accountLineList.id}" id="estimateVendorName${accountLineList.id}" value="${accountLineList.estimateVendorName }"  onkeyup="findPartnerDetailsPriceing('estimateVendorName${accountLineList.id}','vendorCode${accountLineList.id}','estimateVendorNameDivId${accountLineList.id}',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','${accountLineList.id}',event);" onchange="findPartnerDetailsByName('vendorCode${accountLineList.id}','estimateVendorName${accountLineList.id}');" size="15" class="input-text pr-f11" /> 
								<div id="estimateVendorNameDivId${accountLineList.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
								</c:when>
								<c:otherwise>
								<input type="text" name="estimateVendorName${accountLineList.id}" id="estimateVendorName${accountLineList.id}" value="${accountLineList.estimateVendorName }"  readonly="true" size="15" class="input-text pr-f11" />
								</c:otherwise>
								</c:choose>
								</display:column>
								<display:column   title="Basis"  style="width:80px;!width:95px;"> 
								<select id="basis${accountLineList.id}" name ="basis${accountLineList.id}" style="width:50px" class="list-menu pr-f11" onchange="changeBasis('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}',this)"> 
								<c:forEach var="chrms" items="${basis}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.value == accountLineList.basis}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								<input type="hidden"  name="oldbasis${accountLineList.id}" value="${accountLineList.basis}"/>
							    </display:column>
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Rec VAT&nbsp;Desc"  style="width:50px"> 
								<c:if test="${empty accountLineList.recAccDate}">
								<select name ="recVatDescr${accountLineList.id}" style="width:75px" class="list-menu pr-f11" onchange="calculateVatSectionAll('REVENUE','${accountLineList.id}','BAS');">																 								
								<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.recVatDescr}">
	                                  <c:set var="selectedInd" value="selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								</c:if>
								<c:if test="${not empty accountLineList.recAccDate}">
								<input type="hidden" name ="recVatDescr${accountLineList.id}"  value="${accountLineList.recVatDescr}" style="width:70px" class="input-textUpper pr-f11 " readonly="true">
								<c:set var="selectedVat" value=""></c:set>
								<c:forEach var="chrms" items="${estVatList}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.recVatDescr}">
	                                  <c:set var="selectedVat" value="${chrms.value}"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  
	                                 </c:otherwise>
                                  </c:choose>
                                </c:forEach> 
								<input type="text" value="${selectedVat}" style="width:70px" class="input-textUpper pr-f11 " readonly="true">							
								</c:if>
								<input type="hidden"  name="recVatPercent${accountLineList.id}" value="${accountLineList.recVatPercent}"/>
								</display:column>
								</c:if>	
							<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT&nbsp;Desc"  style="width:50px"> 
								<c:if test="${empty accountLineList.payAccDate}">
								<select name ="payVatDescr${accountLineList.id}" style="width:75px" class="list-menu pr-f11"  onchange="calculateVatSectionAll('EXPENSE','${accountLineList.id}','BAS');">																 								
								<c:forEach var="chrms" items="${payVatList}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.payVatDescr}">
	                                  <c:set var="selectedInd" value="selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								</c:if>
								<c:if test="${not empty accountLineList.payAccDate}">
								<input type="hidden" name ="payVatDescr${accountLineList.id}"  value="${accountLineList.payVatDescr}" style="width:70px" class="input-textUpper pr-f11 " readonly="true">
								<c:set var="selectedVat" value=""></c:set>
								<c:forEach var="chrms" items="${payVatList}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.payVatDescr}">
	                                  <c:set var="selectedVat" value="${chrms.value}"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  
	                                 </c:otherwise>
                                  </c:choose>
                                </c:forEach> 
								<input type="text" value="${selectedVat}" style="width:70px" class="input-textUpper pr-f11 " readonly="true">
								</c:if>
								<input type="hidden"  name="payVatPercent${accountLineList.id}" value="${accountLineList.payVatPercent}"/>
								</display:column>
								</c:if>							    
								<display:column title="Quantity"  style="width:18px;border-left:medium solid #003366;" >
								<input type="text" style="text-align:right" id="estimateQuantity${accountLineList.id}"  name="estimateQuantity${accountLineList.id}"  value="${accountLineList.estimateQuantity }" size="4" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}','','','');calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}','estSellLocalAmountNew${accountLineList.id}');"/>
								<img id="rateImage"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedEstimateQuantitys('chargeCode${accountLineList.id}','category${accountLineList.id}','estimateExpense${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','deviation${accountLineList.id}','estimateDeviation${accountLineList.id}','estimateSellDeviation${accountLineList.id}','${accountLineList.id}');"/>			
								<input type="hidden" name ="deviation${accountLineList.id}"  value="${accountLineList.deviation}" />
								<input type="hidden" name ="estimateDeviation${accountLineList.id}"  value="${accountLineList.estimateDeviation}" />
								<input type="hidden" name ="estimateSellDeviation${accountLineList.id}"  value="${accountLineList.estimateSellDeviation}" />
								<input type="hidden"  name="oldestimateQuantity${accountLineList.id}" value="${accountLineList.estimateQuantity }"/>
								</display:column>								
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right"  id="estimateRate${accountLineList.id}"  name="estimateRate${accountLineList.id}" value="${accountLineList.estimateRate }" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateExpense('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');"/>
								</display:column>  
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estCurrency!=baseCurrency}">			                           
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','EST',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineField('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column> 
								<c:if test="${chargeDiscountFlag}">	
								<display:column   title="Disc %"  style="width:80px"> 
								     <c:forEach items="${discountMap}" var="current">   
       									   <c:if test="${current.key==accountLineList.id}"> 
       									   <c:set value="${current.value}" var="disMap"/>
       									   </c:if>
    								  </c:forEach>  
								<select name ="estimateDiscount${accountLineList.id}" id="estimateDiscount${accountLineList.id}" style="width:70px" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}','','','');" class="list-menu pr-f11" > 
								<c:forEach var="chrms" items="${disMap}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.estimateDiscount}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>								
								<input type="hidden"  name="oldEstimateDiscount${accountLineList.id}" value="${accountLineList.estimateDiscount}"/>
								</display:column>	
								</c:if>
								<c:if test="${contractType}">
								<display:column title="Rec&nbsp;Quantity"  style="width:18px;" >
								<input type="text" style="text-align:right" id="estimateSellQuantity${accountLineList.id}"  name="estimateSellQuantity${accountLineList.id}"  value="${accountLineList.estimateSellQuantity }" size="4" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}','','','');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}','estSellLocalAmountNew${accountLineList.id}');"/>
								<input type="hidden"  name="oldEstimateSellQuantity${accountLineList.id}" value="${accountLineList.estimateSellQuantity}"/>
								</display:column>			
								</c:if>					
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="estimateSellRate${accountLineList.id}" value="${accountLineList.estimateSellRate }" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevenue('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}','estSellCurrencyNew${accountLineList.id}~estimateContractCurrencyNew${accountLineList.id}','estSellValueDateNew${accountLineList.id}~estimateContractValueDateNew${accountLineList.id}','estSellExchangeRateNew${accountLineList.id}~estimateContractExchangeRateNew${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}','estSellLocalAmountNew${accountLineList.id}');"/>
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.estSellCurrency!=baseCurrency}">			                           
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','ESTSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldNew('${accountLineList.id}','basis${accountLineList.id}','estimateQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column> 
								</c:if>									
								<display:column title="Expense"  style="width:20px" >
								<input type="text" style="text-align:right" name="estimateExpense${accountLineList.id}" value="${accountLineList.estimateExpense }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								<input type="text" style="text-align:right" name="estimatePassPercentage${accountLineList.id}" value="${accountLineList.estimatePassPercentage }" maxlength="4"  size="2" class="input-text pr-f11" onchange="changeEstimatePassPercentage('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateRate${accountLineList.id}','estimateExpense${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}');calculateRevenueNew('basis${accountLineList.id}','estimateQuantity${accountLineList.id}','estimateSellRate${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','estimateExpense${accountLineList.id}','estimatePassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','vatPers${accountLineList.id}','vatAmt${accountLineList.id}','${accountLineList.id}');changeVatAmt('vatDecr${accountLineList.id}','vatPers${accountLineList.id}','estimateRevenueAmount${accountLineList.id}','vatAmt${accountLineList.id}','estSellLocalAmountNew${accountLineList.id}');"  onkeydown="return onlyNumsAllowedPersent(event)" />
								</display:column> 
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column title="Revenue" style="width:20px;border-right:medium solid #d1d1d1;" >
								<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>
								</c:if>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<display:column title="Revenue" style="width:20px;border-right:medium solid #003366;" >
								<input type="text" style="text-align:right" name="estimateRevenueAmount${accountLineList.id}" value="${accountLineList.estimateRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>								
								</c:if>
								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT Amt"  style="width:50px;">
								<input type="text" name ="estExpVatAmt${accountLineList.id}"  value="${accountLineList.estExpVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true"> 
								</display:column>
								<display:column   title="Rec VAT Amt"  style="width:50px;border-right:medium solid #003366;">
								 <input type="text" name ="vatAmt${accountLineList.id}"  value="${accountLineList.estVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								<input type="hidden"  name="estVatAmt${accountLineList.id}" value="${accountLineList.estVatAmt}"/>
								</display:column>																
								</c:if>								
								<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
											
								<display:column title="Quantity"  style="width:18px" >
								<input type="text" style="text-align:right" id="revisionQuantity${accountLineList.id}"  name="revisionQuantity${accountLineList.id}"  value="${accountLineList.revisionQuantity }" size="4" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevisionBlock('${accountLineList.id}','','','','');"/>
								<img id="rateImage"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedQuantitys('${accountLineList.id}');"/>
								<input type="hidden" name ="revisionDeviation${accountLineList.id}"  value="${accountLineList.revisionDeviation}" />
								<input type="hidden" name ="revisionSellDeviation${accountLineList.id}"  value="${accountLineList.revisionSellDeviation}" />
								<input type="hidden" name ="oldRevisionSellDeviation${accountLineList.id}" />
								<input type="hidden" name ="oldRevisionDeviation${accountLineList.id}" />
								<input type="hidden"  name="oldRevisionQuantity${accountLineList.id}" value="${accountLineList.revisionQuantity }"/>
								<c:if test="${contractType}">
								<input type="hidden"  name="revisionPayableContractRate${accountLineList.id}" value="${accountLineList.revisionPayableContractRate}"/>
								<input type="hidden"  name="revisionPayableContractRateAmmount${accountLineList.id}" value="${accountLineList.revisionPayableContractRateAmmount}"/>
								<input type="hidden"  name="revisionContractRate${accountLineList.id}" value="${accountLineList.revisionContractRate}"/>
								<input type="hidden"  name="revisionContractRateAmmount${accountLineList.id}" value="${accountLineList.revisionContractRateAmmount}"/>
								</c:if>
								<input type="hidden"  name="revisionLocalRate${accountLineList.id}" value="${accountLineList.revisionLocalRate}"/>
								<input type="hidden"  name="revisionLocalAmount${accountLineList.id}" value="${accountLineList.revisionLocalAmount}"/>
								<c:if test="${multiCurrency=='Y'}">
								<input type="hidden"  name="revisionSellCurrency${accountLineList.id}" value="${accountLineList.revisionSellCurrency}"/>
								<input type="hidden"  name="revisionSellValueDate${accountLineList.id}" value="${accountLineList.revisionSellValueDate}"/>
								<input type="hidden"  name="revisionSellExchangeRate${accountLineList.id}" value="${accountLineList.revisionSellExchangeRate}"/>
								<input type="hidden"  name="revisionSellLocalRate${accountLineList.id}" value="${accountLineList.revisionSellLocalRate}"/>
								<input type="hidden"  name="revisionSellLocalAmount${accountLineList.id}" value="${accountLineList.revisionSellLocalAmount}"/>
								</c:if>
								</display:column>								
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right"  id="revisionRate${accountLineList.id}"  name="revisionRate${accountLineList.id}" value="${accountLineList.revisionRate }" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevisionBlock('${accountLineList.id}','','','','');"/>
								</display:column>  
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.revisionCurrency!=baseCurrency}">		                           
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REV',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRev('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column> 
								<c:if test="${chargeDiscountFlag}">	
								<display:column   title="Disc %"  style="width:80px"> 
								     <c:forEach items="${discountMap}" var="current">   
       									   <c:if test="${current.key==accountLineList.id}"> 
       									   <c:set value="${current.value}" var="disMap"/>
       									   </c:if>
    								  </c:forEach>  
								<select name ="revisionDiscount${accountLineList.id}" id="revisionDiscount${accountLineList.id}" style="width:70px" onchange="calculateRevisionBlock('${accountLineList.id}','','','','');" class="list-menu pr-f11" > 
								<c:forEach var="chrms" items="${disMap}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.revisionDiscount}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>								
								<input type="hidden"  name="oldRevisionDiscount${accountLineList.id}" value="${accountLineList.revisionDiscount}"/>
								</display:column>	
								</c:if>	
								<c:if test="${contractType}">
								<display:column title="Rec&nbsp;Quantity"  style="width:18px" >
								<input type="text" style="text-align:right" id="revisionSellQuantity${accountLineList.id}"  name="revisionSellQuantity${accountLineList.id}"  value="${accountLineList.revisionSellQuantity }" size="4" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevisionBlock('${accountLineList.id}','','','','');"/>
								<input type="hidden"  name="oldRevisionSellQuantity${accountLineList.id}" value="${accountLineList.revisionSellQuantity }"/>
								</display:column>			
								</c:if>												
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<input type="text" style="text-align:right" name="revisionSellRate${accountLineList.id}" value="${accountLineList.revisionSellRate }" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateRevisionBlock('${accountLineList.id}','revisionSellCurrency${accountLineList.id}~revisionContractCurrency${accountLineList.id}','revisionSellValueDate${accountLineList.id}~revisionContractValueDate${accountLineList.id}','revisionSellExchangeRate${accountLineList.id}~revisionContractExchangeRate${accountLineList.id}');"/>
								<input type="hidden"  name="oldRevisionSellRate${accountLineList.id}" value="${accountLineList.revisionSellRate}"/>
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									<c:choose>
			                        <c:when test="${accountLineList.revisionSellCurrency!=baseCurrency}">			                           
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REVSELL',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRevNew('${accountLineList.id}','basis${accountLineList.id}','revisionQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
								</display:column> 
								</c:if>	
								<display:column title="Variance Expense Amount"  style="width:20px" >
								<input type="text" style="text-align:right" id="varianceExpenseAmount${accountLineList.id}" name="varianceExpenseAmount${accountLineList.id}" value="${accountLineList.varianceExpenseAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>	
								<display:column title="Variance Revenue Amount"  style="width:20px" >
								<input type="text" style="text-align:right" id="varianceRevenueAmount${accountLineList.id}" name="varianceRevenueAmount${accountLineList.id}" value="${accountLineList.varianceRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>																																															
								<display:column title="Expense"  style="width:20px" >
								<input type="text" style="text-align:right" name="revisionExpense${accountLineList.id}" value="${accountLineList.revisionExpense }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								<input type="text" style="text-align:right" name="revisionPassPercentage${accountLineList.id}" value="${accountLineList.revisionPassPercentage }" maxlength="4"  size="2" class="input-text pr-f11" onchange="changeRevisionPassPercentage('basis${accountLineList.id}','revisionQuantity${accountLineList.id}','revisionRate${accountLineList.id}','revisionExpense${accountLineList.id}','revisionSellRate${accountLineList.id}','revisionRevenueAmount${accountLineList.id}','revisionPassPercentage${accountLineList.id}','displayOnQuote${accountLineList.id}','${accountLineList.id}');"  onkeydown="return onlyNumsAllowedPersent(event)" />
								</display:column> 
								<c:if test="${systemDefaultVatCalculationNew=='Y'}"> 
								<display:column title="Revenue" style="width:20px;border-right:medium solid #d1d1d1;" >
								<input type="text" style="text-align:right" name="revisionRevenueAmount${accountLineList.id}" value="${accountLineList.revisionRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>								
								</c:if>	
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<display:column title="Revenue" style="width:20px;border-right:medium solid #003366;" >
								<input type="text" style="text-align:right" name="revisionRevenueAmount${accountLineList.id}" value="${accountLineList.revisionRevenueAmount }" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</display:column>								
								</c:if>										
								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT Amt"  style="width:50px;">
								<input type="text" name ="revisionExpVatAmt${accountLineList.id}"  value="${accountLineList.revisionExpVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">								 
								</display:column>
								<display:column   title="Rec VAT Amt"  style="width:50px;border-right:medium solid #003366;">
								<input type="text" name ="revisionVatAmt${accountLineList.id}"  value="${accountLineList.revisionVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true"> 
								</display:column>								
								
								</c:if>															
								</c:if>								
								
								<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								<display:column title="Quantity"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<c:if test="${empty accountLineList.recAccDate}">	
								<input type="text" style="text-align:right" name="recQuantity${accountLineList.id}"  value="${accountLineList.recQuantity}" size="4" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'REC','','','');"/>
								</c:if>
								<c:if test="${not empty accountLineList.recAccDate}">	
								<input type="text" style="text-align:right" name="recQuantity${accountLineList.id}"  value="${accountLineList.recQuantity}" size="4" class="input-textUpper pr-f11 " readonly="true"/>
								</c:if>
								<input type="hidden"  name="tempRecQuantity${accountLineList.id}" value="${accountLineList.recQuantity}"/>
								</display:column>
								<c:if test="${chargeDiscountFlag}">	
								<display:column   title="Disc %"  style="width:80px"> 
								<c:if test="${empty accountLineList.recAccDate}">	
 								<c:forEach items="${discountMap}" var="current">   
       									   <c:if test="${current.key==accountLineList.id}"> 
       									   <c:set value="${current.value}" var="disMap"/>
       									   </c:if>
    						    </c:forEach>  
								<select name ="actualDiscount${accountLineList.id}" id="actualDiscount${accountLineList.id}" style="width:70px" onchange="calculateActualRevenue('${accountLineList.id}',this,'REC','','','');" class="list-menu pr-f11" > 
								<c:forEach var="chrms" items="${disMap}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == accountLineList.actualDiscount}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
                                </c:forEach> 
								</select>
								</c:if>
								<c:if test="${not empty accountLineList.recAccDate}">	
								<input type="text" name ="actualDiscount${accountLineList.id}"  value="${accountLineList.actualDiscount}" style="width:50px" class="input-textUpper pr-f11 " readonly="true">
								</c:if>
								<input type="hidden"  name="oldActualDiscount${accountLineList.id}" value="${accountLineList.actualDiscount}"/>
								</display:column>
								</c:if>	
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								<c:if test="${empty accountLineList.recAccDate}">	
								<input type="text" style="text-align:right" name="recRate${accountLineList.id}" value="${accountLineList.recRate}" size="5" class="input-text pr-f11" onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'REC','recRateCurrency${accountLineList.id}~contractCurrency${accountLineList.id}','racValueDate${accountLineList.id}~contractValueDate${accountLineList.id}','recRateExchange${accountLineList.id}~contractExchangeRate${accountLineList.id}');"/>
								<img id="rateImage"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedReceivableQuantitys('${accountLineList.id}');"/>
								</c:if>
								<c:if test="${not empty accountLineList.recAccDate}">	
								<input type="text" style="text-align:right" name="recRate${accountLineList.id}" value="${accountLineList.recRate}" size="5" class="input-textUpper pr-f11 " readonly="true"/>
								</c:if>
								<input type="hidden" name="oldRecRate${accountLineList.id}" value="${accountLineList.checkNew}"/>
								<input type="hidden" name="checkNew${accountLineList.id}" value="${accountLineList.checkNew}"/>
								<input type="hidden" name="basisNew${accountLineList.id}" value="${accountLineList.basisNew}"/>
								<input type="hidden" name="basisNewType${accountLineList.id}" value="${accountLineList.basisNewType}"/>
								<input type="hidden" name="description${accountLineList.id}" value="${accountLineList.description}"/>
								<input type="hidden" name="receivableSellDeviation${accountLineList.id}" value="${accountLineList.receivableSellDeviation}"/>
								<input type="hidden" name="oldReceivableDeviation${accountLineList.id}" value="${accountLineList.receivableSellDeviation}"/> 
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
								<c:if test="${empty accountLineList.recAccDate}">	
									<c:choose>
			                        <c:when test="${accountLineList.recRateCurrency!=baseCurrency}">
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
			                       </c:if>
			                       <c:if test="${not empty accountLineList.recAccDate}">	 
			                        <c:choose>
			                        <c:when test="${accountLineList.recRateCurrency!=baseCurrency}"> 
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','REC',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldRec('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose>
			                        </c:if>
								</display:column>					
								</c:if>
								<display:column title="Revenue" style="width:20px" >
								<img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png"  onclick="getReceivableFieldDetails('${accountLineList.id}');" />
								<c:if test="${empty accountLineList.recAccDate}">	
								<input type="text" style="text-align:right" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</c:if>
								<c:if test="${not empty accountLineList.recAccDate}">	
								<input type="text" style="text-align:right" name="actualRevenue${accountLineList.id}" value="${accountLineList.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
								</c:if>
								<configByCorp:fieldVisibility componentId="component.field.Pricing.GeneratingInvoice">
								<c:if test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
						        <a><img align="top" title="Forms" onclick="findUserPermission1('${accountLineList.recInvoiceNumber}','${accountLineList.companyDivision}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/></a>
						        </c:if>
						        </configByCorp:fieldVisibility>
						        <c:set var="countReceivableDetailNotes" value=""></c:set>
								<c:forEach var="chrms" items="${noteMap}" varStatus="loopStatus">
								<c:choose>
								<c:when test="${chrms.key == accountLineList.id}">
 								<c:set var="countReceivableDetailNotes" value="${chrms.value}"></c:set>
 								<c:if test="${countReceivableDetailNotes == '0' || countReceivableDetailNotes == '' || countReceivableDetailNotes == null}">
 								<c:if test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
 								<img id="countReceivableDetailNotesImage1" src="${pageContext.request.contextPath}/images/notes_empty1-new.png"  style="vertical-align:middle;padding-left:5px;" onclick="checkNotesForRevnue('${accountLineList.id}','notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true');"/><a onclick="checkNotesForRevnue('${accountLineList.id}','notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true');"></a>
 								</c:if>
 								<c:if test="${empty accountLineList.recInvoiceNumber}">
								<img id="countReceivableDetailNotesImage1" src="${pageContext.request.contextPath}/images/notes_empty1-new.png" style="vertical-align:middle;padding-left:5px;" onclick="javascript:checkNotesForRevnue('${accountLineList.id}','notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true');"/><a onclick="javascript:checkNotesForRevnue('${accountLineList.id}','notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true');" ></a>
								</c:if>
 								</c:if>
 								<c:if test="${countReceivableDetailNotes >= '1'}">
 								<c:choose>
 								<c:when test="${empty accountLineList.recInvoiceNumber}">
 								<img id="countReceivableDetailNotesImage1" src="${pageContext.request.contextPath}/images/notes_open1-new.png" style="vertical-align:middle;padding-left:5px;" onclick="javascript:checkNotesForRevnue('${accountLineList.id}','notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true');"/><a onclick="javascript:checkNotesForRevnue('${accountLineList.id}','notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true');"></a>
 								</c:when>
 								<c:otherwise>
 								<img id="countReceivableDetailNotesImage1" src="${pageContext.request.contextPath}/images/notes_open1-new.png" style="vertical-align:middle;padding-left:5px;"  onclick="javascript:checkNotesForRevnue('${accountLineList.id}','notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true');"/><a onclick="javascript:checkNotesForRevnue('${accountLineList.id}','notess.html?id=${accountLineList.id}&notesId=${accountLineList.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage1&fieldId=countReceivableDetailNotes&decorator=popup&popup=true');"></a>
								</c:otherwise>
 								</c:choose>
 								</c:if> 
 								</c:when>
								<c:otherwise>
								</c:otherwise>
								</c:choose>
								</c:forEach> 
								</display:column>
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="VAT Amt"  style="width:50px;border-right:medium solid #999999;">
								<c:if test="${empty accountLineList.recAccDate}">
								<input type="text" name ="recVatAmt${accountLineList.id}"  value="${accountLineList.recVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								<c:set var="recTemp1" value="${accountLineList.recVatAmt}" />
								<c:set var="recTemp2" value="${accountLineList.qstRecVatAmt}" />
								<c:set var="recTemp" value="${recTemp1 + recTemp2}"/>
								<input type="hidden" name ="recVatAmtTemp${accountLineList.id}"  value="${recTemp}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								
								</c:if>
								<c:if test="${not empty accountLineList.recAccDate}">
								<input type="text" name ="recVatAmt${accountLineList.id}"  value="${accountLineList.recVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								<c:set var="recTemp1" value="${accountLineList.recVatAmt}" />
								<c:set var="recTemp2" value="${accountLineList.qstRecVatAmt}" />
								<c:set var="recTemp" value="${recTemp1 + recTemp2}"/>
								<input type="hidden" name ="recVatAmtTemp${accountLineList.id}"  value="${recTemp}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								
								</c:if> 
								</display:column>
								</c:if>			
								<display:column title="Expense"  headerClass="RemoveBorderRight" style="width:80px;!border-right-style:none;" >
								<c:if test="${empty accountLineList.payAccDate}">
								<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" class="input-text pr-f11" size="6"  onkeydown="return onlyRateAllowed(event)" onkeypress="return numbersonly(this,event);" onblur="clearDecimal();return checkFloat1(this)" onchange="calculateActualRevenue('${accountLineList.id}',this,'PAY','','','');"/>
								</c:if>
								<c:if test="${not empty accountLineList.payAccDate}">
								<input type="text" style="text-align:right" name="actualExpense${accountLineList.id}" value="${accountLineList.actualExpense}" size="6" class="input-textUpper pr-f11 " readonly="true"/>
								</c:if>
								<c:if test="${contractType}">
								<input type="hidden"  name="payableContractRateAmmount${accountLineList.id}" value="${accountLineList.payableContractRateAmmount}"/>
								<input type="hidden"  name="contractRate${accountLineList.id}" value="${accountLineList.contractRate}"/>
								<input type="hidden"  name="contractRateAmmount${accountLineList.id}" value="${accountLineList.contractRateAmmount}"/>
								</c:if>
								<img id="rateImage"  class="openpopup" align="top" src="${pageContext.request.contextPath}/images/popup_icon14.png" onclick="getPayableSectionDetails('${accountLineList.id}');"/>
								<input type="hidden"  name="country${accountLineList.id}" value="${accountLineList.country}"/>
								<input type="hidden"  name="valueDate${accountLineList.id}" value="${accountLineList.valueDate}"/>
								<input type="hidden"  name="exchangeRate${accountLineList.id}" value="${accountLineList.exchangeRate}"/>
								<input type="hidden"  name="localAmount${accountLineList.id}" value="${accountLineList.localAmount}"/>
								<input type="hidden"  name="recRateCurrency${accountLineList.id}" value="${accountLineList.recRateCurrency}"/>
								<input type="hidden"  name="racValueDate${accountLineList.id}" value="${accountLineList.racValueDate}"/>
								<input type="hidden"  name="recRateExchange${accountLineList.id}" value="${accountLineList.recRateExchange}"/>
								<input type="hidden"  name="recCurrencyRate${accountLineList.id}" value="${accountLineList.recCurrencyRate}"/>
								<input type="hidden"  name="actualRevenueForeign${accountLineList.id}" value="${accountLineList.actualRevenueForeign}"/>
								<input type="hidden"  name="recInvoiceNumber${accountLineList.id}" value="${accountLineList.recInvoiceNumber}"/>						
								<input type="hidden"  name="invoiceNumber${accountLineList.id}" value="${accountLineList.invoiceNumber}"/>
								<input type="hidden"  name="invoiceDate${accountLineList.id}" value="${accountLineList.invoiceDate}"/>
								<input type="hidden"  name="receivedDate${accountLineList.id}" value="${accountLineList.receivedDate}"/>
								<input type="hidden"  name="payingStatus${accountLineList.id}" value="${accountLineList.payingStatus}"/>
								
								<input type="hidden"  name="recVatGl${accountLineList.id}" value="${accountLineList.recVatGl}"/>
								<input type="hidden"  name="payVatGl${accountLineList.id}" value="${accountLineList.payVatGl}"/>
								<input type="hidden"  name="qstRecVatGl${accountLineList.id}" value="${accountLineList.qstRecVatGl}"/>
								<input type="hidden"  name="qstPayVatGl${accountLineList.id}" value="${accountLineList.qstPayVatGl}"/>
								<input type="hidden"  name="qstRecVatAmt${accountLineList.id}" value="${accountLineList.qstRecVatAmt}"/>
								<input type="hidden"  name="qstPayVatAmt${accountLineList.id}" value="${accountLineList.qstPayVatAmt}"/>
								<input type="hidden"  name="payDeviation${accountLineList.id}" value="${accountLineList.payDeviation}"/>
								<input type="hidden"  name="oldPayDeviation${accountLineList.id}" value="${accountLineList.payDeviation}"/>
								<input type="hidden"  name="createdBy${accountLineList.id}" value="${accountLineList.createdBy}"/>
								<input type="hidden"  name="networkSynchedId${accountLineList.id}" value="${accountLineList.networkSynchedId}"/>
								<c:if test="${empty accountLineList.payAccDate}">
								<img id="rateImage"  class="openpopup" width="16" height="17" align="top" src="${pageContext.request.contextPath}/images/calc_new1.png" onclick="findRevisedQuantityLocalAmounts('${accountLineList.id}');"/>
								</c:if>
								<c:choose>								
						         <c:when test="${accountLineList.payingStatus=='A' || accountLineList.payingStatus=='C'}">
						         <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						        </c:when>
						        <c:when test="${accountLineList.payingStatus=='N'}">
						        <img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						        </c:when>
						        <c:when test="${accountLineList.payingStatus=='P'}">
						        <img id="target" src="${pageContext.request.contextPath}/images/q1.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						        </c:when>
						        <c:when test="${accountLineList.payingStatus=='S'}">
						         <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						        </c:when>
						         <c:when test="${accountLineList.payingStatus=='I'}">
						         <img id="active" src="${pageContext.request.contextPath}/images/internal-Cost.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
						         </c:when>
						        <c:otherwise>
						         </c:otherwise>						         
								</c:choose>							
								
								</display:column>
								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="VAT Amt"  style="width:50px;border-right:medium solid #d1d1d1;">
								<c:if test="${empty accountLineList.payAccDate}">
								<input type="text" name ="payVatAmt${accountLineList.id}"  value="${accountLineList.payVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								<c:set var="payTemp1" value="${accountLineList.payVatAmt}" />
								<c:set var="payTemp2" value="${accountLineList.qstPayVatAmt}" />
								<c:set var="payTemp" value="${payTemp1 + payTemp2}"/>
								<input type="hidden" name ="payVatAmtTemp${accountLineList.id}"  value="${payTemp}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								</c:if>
								<c:if test="${not empty accountLineList.payAccDate}">
								<input type="text" name ="payVatAmt${accountLineList.id}"  value="${accountLineList.payVatAmt}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								<c:set var="payTemp1" value="${accountLineList.payVatAmt}" />
								<c:set var="payTemp2" value="${accountLineList.qstPayVatAmt}" />
								<c:set var="payTemp" value="${payTemp1 + payTemp2}"/>
								<input type="hidden" name ="payVatAmtTemp${accountLineList.id}"  value="${payTemp}" style="width:50px;text-align:right;" class="input-textUpper pr-f11 " readonly="true">
								
								</c:if> 
								</display:column>
								</c:if>															
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;border-right:medium solid #003366;" >
								<c:if test="${empty accountLineList.payAccDate}">
									<c:choose>
			                        <c:when test="${accountLineList.country!=baseCurrency}">
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 
			                        </c:if>
								<c:if test="${not empty accountLineList.payAccDate}">
									<c:choose>
			                        <c:when test="${accountLineList.country!=baseCurrency}">
			                           <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}_COLOR.gif'/>" />
			                        </c:when> 
			                        <c:otherwise>
			                          <img  class="openpopup" style="float:right;" width="" height="" onmouseover="showBillingDatails('${accountLineList.id}','PAY',this);" onmouseout="ajax_hideTooltip();" onclick="javascript:getAccountlineFieldPay('${accountLineList.id}','basis${accountLineList.id}','recQuantity${accountLineList.id}','chargeCode${accountLineList.id}','serviceOrder.contract','vendorCode${accountLineList.id}');" src="<c:url value='/images/${accCorpID}.gif'/>" />
			                        </c:otherwise>
			                        </c:choose> 								
								</c:if>				                        
								</display:column>
								</c:if>			 
	                            <display:column title="Description" style="width:100px" >
								<textarea  name="quoteDescription${accountLineList.id}"  id="quoteDescription${accountLineList.id}"  style="overflow:auto;" class="input-text pr-f11" onselect="discriptionView('quoteDescription${accountLineList.id}')" onblur="discriptionView1('quoteDescription${accountLineList.id}')" value ="${accountLineList.quoteDescription }"  cols="15" rows="1">${accountLineList.quoteDescription }</textarea>
								</display:column> 
								<c:if test="${companyDivisionGlobal>1}"> 
								<display:column   title="Div" style="width:58px" > 
								<c:choose>
								 <c:when test="${singleCompanyDivisionInvoicing && accountLineList.recInvoiceNumber != null && accountLineList.recInvoiceNumber!=''}">
								 <input type="text" style="width:58px" name="companyDivision${accountLineList.id}" id="companyDivision${accountLineList.id}" value="${accountLineList.companyDivision}"  maxlength="3" class="input-textUpper pr-f11 "  readonly="true" />
								 </c:when>
	                            <c:when test="${accountLineList.category!='Internal Cost' && companyDivisionAcctgCodeUnique!='Y'}">
										<select name ="companyDivision${accountLineList.id}" id ="companyDivision${accountLineList.id}" style="width:58px" onchange="checkCompanyDiv('${accountLineList.id}')" class="list-menu pr-f11"> 
										<c:forEach var="chrms" items="${companyDivis}" >
		                                  <c:choose>
			                                  <c:when test="${chrms == accountLineList.companyDivision}">
			                                  <c:set var="selectedInd" value=" selected"></c:set>
			                                  </c:when>
			                                 <c:otherwise>
			                                  <c:set var="selectedInd" value=""></c:set>
			                                 </c:otherwise>
		                                  </c:choose>
		                                 <option value="<c:out value='${chrms}' />" <c:out value='${selectedInd}' />>
		                                <c:out value="${chrms}"></c:out>
		                                </option>
		                                </c:forEach> 
										</select>
										<s:hidden name="tempCompanyDivision${accountLineList.id}" id="tempCompanyDivision${accountLineList.id}"  value="${accountLineList.companyDivision}" />
										<s:hidden name="recAccDate${accountLineList.id}" id="recAccDate${accountLineList.id}"  value="${accountLineList.recAccDate}" />
										<s:hidden name="payAccDate${accountLineList.id}" id="payAccDate${accountLineList.id}"  value="${accountLineList.payAccDate}" />
								</c:when>
								
								<c:otherwise>
										<input type="text" style="width:58px" name="companyDivision${accountLineList.id}" id="companyDivision${accountLineList.id}" value="${accountLineList.companyDivision}"  maxlength="3" class="input-textUpper pr-f11 "  readonly="true" />
										<s:hidden name="recAccDate${accountLineList.id}" id="recAccDate${accountLineList.id}"  value="${accountLineList.recAccDate}" />
										<s:hidden name="payAccDate${accountLineList.id}" id="payAccDate${accountLineList.id}"  value="${accountLineList.payAccDate}" />										
							    </c:otherwise>
								 </c:choose>
								 </display:column>
								 </c:if>
								<display:column style="width:60px;padding:0px;text-align:center;" 
								title="Display&nbsp;On Quote All<input type='checkbox' style='margin-left:20px; vertical-align:middle;' name='selectall' onclick='selectAll(this);'/>" media="html" > 
                                 <c:if test="${accountLineList.displayOnQuote}"> 
                                  <input type="checkbox" style="text-align:center;" name="displayOnQuote${accountLineList.id}" id="displayOnQuote${accountLineList.id}"  checked="checked"  value="${accountLineList.status}" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.displayOnQuote==false}">
                                 <input type="checkbox"  style="text-align:center;" name="displayOnQuote${accountLineList.id}" id="displayOnQuote${accountLineList.id}" value="${accountLineList.status}" /> 
                                 </c:if>
                                 </display:column>
                                 <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
                                 <display:column title="Roll Up In Invoice <input type='checkbox' name='selectall' id='selectall' style='margin-left; vertical-align:middle;' onclick='selectAllRollInvoice(this);'/>All" media="html" >
                                  <c:choose>
                                  <c:when test="${accountLineList.rollUpInInvoice}">
                                  <input type="checkbox" style="text-align:center;" name="rollUpInInvoice${accountLineList.id}" id="rollUpInInvoice${accountLineList.id}"  checked="checked"  value="${accountLineList.status}" />
                                  </c:when>
                                  <c:otherwise>
                                  <input type="checkbox"  style="text-align:center;" name="rollUpInInvoice${accountLineList.id}" id="rollUpInInvoice${accountLineList.id}" value="${accountLineList.status}" />
                                  </c:otherwise>
                                  </c:choose>                                  
                                 </display:column>
                                 </configByCorp:fieldVisibility>
                                   <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">	
								<display:column title="Additional Services <input type='checkbox' style='margin-left:22px; vertical-align:middle;' name='selectall' onclick='selectAdd(this);'/>All" media="html"    style="width:50px;padding:0px;text-align:center;"> 
                                 <c:if test="${accountLineList.additionalService}"> 
                                  <input type="checkbox"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" checked="checked"  value="${accountLineList.status}" /> 
                                 </c:if>
                                 <c:if test="${accountLineList.additionalService==false}">
                                 <input type="checkbox"  name="additionalService${accountLineList.id}" id="additionalService${accountLineList.id}" value="${accountLineList.status}" /> 
                                 </c:if>
                                 </display:column>
								</configByCorp:fieldVisibility>
	                            <display:column style="width:50px;padding:0px;text-align:center;"  title="<font style='padding-left:5px;'>Act &nbsp;&nbsp;All </font><br><input type='checkbox' style='margin-left:6px; vertical-align:middle;' checked='checked' id='selectallActive' name='selectallActive' onclick='selectAllActive(this);'/>"  media="html">
				                 <c:choose> 
                                 <c:when test="${((!(trackingStatus.accNetworkGroup)) && (billingCMMContractType || billingDMMContractType) && (trackingStatus.soNetworkGroup) && ((accountLineList.createdBy == 'Networking') || ( (fn111:indexOf(accountLineList.createdBy,'Stg Bill')>=0 )  &&  not empty accountLineList.networkSynchedId) )) || (trackingStatus.soNetworkGroup  && billingDMMContractType && (accountLineList.chargeCode == 'DMMFEE' || accountLineList.chargeCode == 'DMMFXFEE' || accountLineList.createdBy == 'Networking' )) ||(trackingStatus.soNetworkGroup  && billingCMMContractType && accountLineList.chargeCode == 'MGMTFEE') ||(accountLineList.chargeCode == 'MGMTFEE') || (billingDMMContractType && (accountLineList.chargeCode == 'DMMFEE' || accountLineList.chargeCode == 'DMMFXFEE' ))}">
                                  <c:if test="${accountLineList.status}">
                                  <input type="checkbox"  name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}" checked="checked"  value="${accountLineList.status}" onchange="" disabled="disabled"/> 
                                  <s:hidden name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}"  value="${accountLineList.status}" ></s:hidden>
                                  </c:if>
                                  </c:when>
                                  <c:otherwise>
				                 <c:if test="${accountLineList.status}"> 
				                 <c:if test="${accountInterface!='Y'}">
                                 <c:choose> 
                                 <c:when test="${accountLineList.actualRevenue!='0.00' || accountLineList.actualExpense!='0.00'}">
                                 <input type="checkbox"  name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}" checked="checked"  value="${accountLineList.status}" onchange="" disabled="disabled"/> 
                                  <s:hidden name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}"  value="${accountLineList.status}" ></s:hidden>
	                             </c:when>
	                             <c:otherwise> 
                                 <input type="checkbox"  name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}" checked="checked"  value="${accountLineList.status}" onchange="inactiveStatusCheck(${accountLineList.id},this)"/> 
	                             </c:otherwise> </c:choose>
	                            </c:if>
	                            <c:if test="${accountInterface=='Y'}">
                                <c:choose> 
                                 <c:when test="${ not empty accountLineList.accruePayable || not empty accountLineList.accrueRevenue || not empty accountLineList.recAccDate || not empty accountLineList.payAccDate || not empty accountLineList.receivedInvoiceDate }">
                                  <input type="checkbox"  name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}" checked="checked"  value="${accountLineList.status}" onchange="" disabled="disabled"/> 
                                  <s:hidden name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}"  value="${accountLineList.status}" ></s:hidden>
	                              </c:when>
	                              <c:otherwise>
                                   <input type="checkbox"  name="statusCheck${accountLineList.id}" id="statusCheck${accountLineList.id}" checked="checked"  value="${accountLineList.status}" onchange="inactiveStatusCheck(${accountLineList.id},this)"/> 
	                             </c:otherwise></c:choose>
	                              </c:if> 
				                 </c:if>
				                 </c:otherwise></c:choose>
				               </display:column>
								<display:column title="INV"   style="width:50px;padding:0px;">
								<c:choose> 
						        <c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}">
						        <input type="checkbox"   checked="checked" disabled="disabled"   />
						        </c:when> 
						        <c:otherwise>
						         <c:if test="${accountLineList.recInvoiceNumber != '' && accountLineList.recInvoiceNumber != null}">
						         <input type="checkbox"   checked="checked" disabled="disabled"   />
						         </c:if>
						         <c:if test="${accountLineList.recInvoiceNumber == '' || accountLineList.recInvoiceNumber == null}">
						         <c:if test="${accountLineList.selectiveInvoice}">
						           <input type="checkbox"   checked="checked" onclick="selectiveInvoiceCheck(${accountLineList.id},this)" />
						         </c:if>
						         <c:if test="${accountLineList.selectiveInvoice==false}">
								   <input type="checkbox"     onclick="selectiveInvoiceCheck(${accountLineList.id},this)" />  
							     </c:if>
							     </c:if>
							     </c:otherwise></c:choose>
						        </display:column>
				               	<display:column title="Audit" style="width:25px; text-align: center;">
	    							<a><img align="middle" src="images/report-ext.png" style="margin: 0px 0px 0px 0px;" onclick="window.open('auditList.html?id=${accountLineList.id}&tableName=accountline&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"></a>
								</display:column>		              				               
	                         </c:otherwise>
                             </c:choose>
                             </c:if>
		                     <c:if test="${accountLineList=='[]'}">
		                     <display:column sortProperty="accountLineNumber"   titleKey="accountLine.accountLineNumber"  style="width:10px;" > 
								
								</display:column>
								<display:column   titleKey="accountLine.category" style="width:70px" > 
								
								 </display:column>
								<display:column title="Charge &nbsp;Code" style="width:100px; whitespace: nowrap;" >
			                     </display:column>
			                     <display:column   title="Bill To Code" style="width:100px" > 
							    </display:column>
								<display:column  title="Bill To&nbsp;Name" style="width:50px" >
								</display:column>
								<display:column   title="Vendor Code" style="width:100px" > 
							    </display:column>
								<display:column  title="Vendor &nbsp;Name" style="width:50px" >
								</display:column>
								<display:column   title="Basis"  style="width:80px;!width:95px;"> 
								</display:column>
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Rec VAT&nbsp;Desc"  style="width:50px"> 
								</display:column>
								</c:if>	
							<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT&nbsp;Desc"  style="width:50px"> 
								</display:column>
								</c:if>							    
								<display:column title="Quantity"  style="width:18px;border-left:medium solid #003366;" >
								</display:column>								
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								</display:column>  
							<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;"> </display:column> 
								<c:if test="${chargeDiscountFlag}">	
								<display:column   title="Disc %"  style="width:80px"> 
								     </display:column>	
								</c:if>
								<c:if test="${contractType}">
								<display:column title="Rec&nbsp;Quantity"  style="width:18px;" >
								</display:column>			
								</c:if>					
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									</display:column> 
								</c:if>									
								<display:column title="Expense"  style="width:20px" >
								
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								</display:column> 
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column title="Revenue" style="width:20px;border-right:medium solid #d1d1d1;" >
								</display:column>
								</c:if>
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<display:column title="Revenue" style="width:20px;border-right:medium solid #003366;" >
								</display:column>								
								</c:if>
								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT Amt"  style="width:50px;">
								</display:column>
								<display:column   title="Rec VAT Amt"  style="width:50px;border-right:medium solid #003366;">
								 </display:column>																
								</c:if>								
								<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
											
								<display:column title="Quantity"  style="width:18px" >
								</display:column>								
								<display:column title="Buy Rate" headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								</display:column>  
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									</display:column> 
								<c:if test="${chargeDiscountFlag}">	
								<display:column   title="Disc %"  style="width:80px"> 
								     </display:column>	
								</c:if>	
								<c:if test="${contractType}">
								<display:column title="Rec&nbsp;Quantity"  style="width:18px" >
								</display:column>			
								</c:if>												
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
									</display:column> 
								</c:if>									
								<display:column title="Expense"  style="width:20px" >
								</display:column>
								<display:column title="Mk%"  style="width:10px" >
								</display:column> 
								<c:if test="${systemDefaultVatCalculationNew=='Y'}"> 
								<display:column title="Revenue" style="width:20px;border-right:medium solid #d1d1d1;" >
								</display:column>								
								</c:if>	
								<c:if test="${systemDefaultVatCalculationNew!='Y'}">
								<display:column title="Revenue" style="width:20px;border-right:medium solid #003366;" >
								</display:column>								
								</c:if>										
								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="Pay VAT Amt"  style="width:50px;">
								</display:column>
								<display:column   title="Rec VAT Amt"  style="width:50px;border-right:medium solid #003366;">
								</display:column>
								</c:if>															
								</c:if>								
								
								<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								<display:column title="Quantity"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								</display:column>
								<c:if test="${chargeDiscountFlag}">	
								<display:column   title="Disc %"  style="width:80px"> 
								</display:column>
								</c:if>	
								<display:column title="Sell Rate"  headerClass="RemoveBorderRight" style="width:20px;!border-right-style:none;" >
								</display:column>
								<c:if test="${multiCurrency=='Y'}">
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;" >
								</display:column>					
								</c:if>								
								<display:column title="Revenue" style="width:20px" >
								</display:column>
								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="VAT Amt"  style="width:50px;border-right:medium solid #999999;">
								</display:column>
								</c:if>			
								<display:column title="Expense"  headerClass="RemoveBorderRight" style="width:80px;!border-right-style:none;" >
								</display:column>
								
								<c:if test="${systemDefaultVatCalculationNew=='Y'}">
								<display:column   title="VAT Amt"  style="width:50px;border-right:medium solid #d1d1d1;">
								</display:column>
								</c:if>															
								<display:column title="FX" headerClass="RemoveBorder FXRight" style="width:20px; border-left:medium hidden;!border-left-style:none;border-right:medium solid #003366;" >
								</display:column>
								</c:if>			 
	                            <display:column title="Description" style="width:100px" >
								</display:column> 
								<c:if test="${companyDivisionGlobal>1}"> 
								<display:column   title="Div" style="width:58px" > 
								</display:column>
								 </c:if>
								<display:column style="width:60px;padding:0px;text-align:center;" 
								title="Display&nbsp;On Quote All<input type='checkbox' style='margin-left:20px; vertical-align:middle;' name='selectall' onclick=''/>" media="html" > 
                                 </display:column>
                                 <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
                                 <display:column title="Roll Up In Invoice <input type='checkbox' name='selectall' id='selectall' style='margin-left; vertical-align:middle;' onclick=''/>All" media="html" >
                                  </display:column>
                                 </configByCorp:fieldVisibility>
                                   <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">	
								<display:column title="Additional Services <input type='checkbox' style='margin-left:22px; vertical-align:middle;' name='selectall' onclick=''/>All" media="html"    style="width:50px;padding:0px;text-align:center;"> 
                                 </display:column>
								</configByCorp:fieldVisibility>
	                            <display:column style="width:50px;padding:0px;text-align:center;"  title="<font style='padding-left:5px;'>Act &nbsp;&nbsp;All </font><br><input type='checkbox' style='margin-left:6px; vertical-align:middle;' checked='checked' id='selectallActive' name='selectallActive' onclick=''/>"  media="html">
				                </display:column>
								<display:column title="INV"   style="width:50px;padding:0px;">
								</display:column>
				               <display:column title="Audit" style="width:25px; text-align: center;">
								</display:column>
		                     </c:if> 
                               <display:footer>
                               
                               <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								<tr> 
				 	  	          <td align="right" colspan="11">Projected Actual Totals</td>
		  	                    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      
				 	  	          
			  	                    <td align="left" colspan="3"></td>
		  	                    <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>	
		  	                    <c:if test="${contractType}">
		  	                    <td></td>
		  	                    </c:if>		  	                    			 	  	          
						          <td align="left" width="40px">
						          
						          </td>
						          <c:if test="${multiCurrency=='Y'}">
						          <td align="left"> 
						  	      </td>
						  	      </c:if>
						  	      <td align="left" width="70px">
						  	      
						  	      </td> 
		  	                    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      
						  	     <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
											
								<td></td><td></td><td></td>
								
		  	                    <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>
		  	                    <c:if test="${contractType}">
		  	                    <td></td>
		  	                    </c:if>
								<td></td>			  	                    
								<c:if test="${multiCurrency=='Y'}">
			  	                    <td align="left" ></td>
			  	                    </c:if>	
			  	                    <td></td>
			  	                    <td></td>						          
								   <td align="left" width="40px">
						          
						          </td>
						          <td></td>
						  	      <td align="left" width="70px">
						  	      
						  	      </td>	
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      
								</c:if>	
			  	                    <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			  	                    <td></td>
			  	                    <c:if test="${chargeDiscountFlag}">
			  	                    <td></td>
			  	                    </c:if>
			  	                    <td ></td>
			  	                    <c:if test="${multiCurrency=='Y'}">
			  	                    <td></td>
			  	                    </c:if>
						  	      <td align="left" width="70px">
						  	      
						  	      <input type="text" style="text-align:right" name="projectedActualRevenue${serviceOrder.id}" value="${serviceOrder.projectedActualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	      </td>
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="1" ></td>
		  	                    </c:if>	
						  	      <td>
						          <input type="text" style="text-align:right" name="projectedActualExpense${serviceOrder.id}" value="${serviceOrder.projectedActualExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						          </td>
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="1" ></td>
		  	                    </c:if>							  	      
									<td></td>
			  	                    </c:if>	
			  	                    <c:if test="${companyDivisionGlobal>1}"> 					  	      
						  	      <td></td>
									</c:if>
		  	                    <td></td>
		  	                    <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
		  	                      <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                    <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    <td colspan="2" align="right"></td>
		  	                    <td></td>
		  	                    <td></td>
		  	                    </tr> 
                               </c:if>
                               
                                 <tr> 
				 	  	          <td align="right" colspan="11"><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
				 	  	         <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      
				 	  	          
			  	                    <td align="left" colspan="2"></td>
		  	                    <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>	
		  	                    <c:if test="${contractType}">
		  	                    <td></td>
		  	                    </c:if>		
		  	                   <c:if test="${multiCurrency=='Y'}">
						          <td> 
						  	      </td>    
						  	      </c:if>        			 	  	          
						          <td align="left" width="40px">
						          <input type="text" style="text-align:right" name="estimatedTotalExpense${serviceOrder.id}" value="${serviceOrder.estimatedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						          </td>
						          <td align="left"> 
						  	      </td>
						  	      <td align="left" width="70px">
						  	      <input type="text" style="text-align:right" name="estimatedTotalRevenue${serviceOrder.id}" value="${serviceOrder.estimatedTotalRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	      </td> 
		  	                    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      
						  	     <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
											
								<td></td><td></td><td></td>
								
		  	                    <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>
		  	                    <c:if test="${contractType}">
		  	                    <td></td>
		  	                    </c:if>
								<td></td>			  	                    
								<c:if test="${multiCurrency=='Y'}">
			  	                    <td align="left" ></td>
			  	                    </c:if>
								<td></td><td></td> 
								   <td align="left" width="40px">
						          <input type="text" style="text-align:right" name="revisedTotalExpense${serviceOrder.id}" value="${serviceOrder.revisedTotalExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						          </td>
						          <td></td>
						  	      <td align="left" width="70px">
						  	      <input type="text" style="text-align:right" name="revisedTotalRevenue${serviceOrder.id}" value="${serviceOrder.revisedTotalRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	      </td>	
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      
								</c:if>	
			  	                    <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
			  	                    <td></td>
			  	                    <c:if test="${chargeDiscountFlag}">
			  	                    <td></td>
			  	                    </c:if>
			  	                    <td></td>
			  	                    <c:if test="${multiCurrency=='Y'}">
			  	                    <td></td>
			  	                    </c:if>
						  	      <td align="left" width="70px">
						  	      <input type="text" style="text-align:right" name="actualTotalRevenue${serviceOrder.id}" value="${serviceOrder.actualRevenue}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	      </td>
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="1" ></td>
		  	                    </c:if>	
						  	      <td>
						          <input type="text" style="text-align:right" name="actualTotalExpense${serviceOrder.id}" value="${serviceOrder.actualExpense}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						          </td>
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="1" ></td>
		  	                    </c:if>							  	      
									<td></td>
			  	                    </c:if>						  	      
						  	      
								<c:if test="${companyDivisionGlobal>1}"> 	
		  	                  <td></td>  </c:if><td></td>
		  	                    <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
		  	                      <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                    <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    <td></td>
		  	                    <c:choose>
		  	                    <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
		  	                    <td colspan="2" align="right"><input type="button" class="cssbuttonA" style="width:60px; height:25px; margin-top:-4px;"  name="Inactivate" id="Inactivate"  disabled="disabled" value="Inactivate" onclick="findAllPricingLineId('Inactive');" tabindex=""/></td>
		  	                    </c:when>
		  	                    <c:otherwise>
		  	                    <td colspan="2" align="right"><input type="button" class="cssbuttonA" style="width:60px; height:25px; margin-top:-4px;"  name="Inactivate" id="Inactivate"  value="Inactivate" onclick="findAllPricingLineId('Inactive');" tabindex=""/></td>
		  	                    </c:otherwise></c:choose>
		  	                    <td></td>
		  	                    </tr>
		  	                    <tr>
		  	                     <td align="right" colspan="11"><b><div align="right">Gross&nbsp;Margin</div></b></td>
		  	                     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      
		  	                     
		  	                    <td align="left" colspan="3"></td>
		  	                    <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>
		  	                 <c:if test="${contractType}">
		  	                    <td></td>
		  	                    </c:if>			  	                    
						          <td align="left" width="40px">
						          </td>
						          <c:if test="${multiCurrency=='Y'}">
						  	      <td align="left"> </td>
						  	      </c:if>
						          <td align="left" width="70px" >
						  	      <input type="text" style="text-align:right" name="estimatedGrossMargin${serviceOrder.id}" value="${serviceOrder.estimatedGrossMargin}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	      </td>
						  	       <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      
								<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								<td></td><td></td><td></td>
								
		  	                    <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>
		  	                    <c:if test="${contractType}">
		  	                    <td></td>
		  	                    </c:if>		  	                    	
								<td></td>			  	                    
								<c:if test="${multiCurrency=='Y'}">
			  	                    <td align="left" ></td>
			  	                    </c:if>
			  	                    <td></td><td></td>							          
								   <td></td>
						          <td></td>
						  	      <td align="left" width="70px">
						  	      <input type="text" style="text-align:right" name="revisedGrossMargin${serviceOrder.id}" value="${serviceOrder.revisedGrossMargin}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	      </td>	
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>
								</c:if>
												  	      
						  	    <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  	                    <td></td>
		  	                    <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>
		  	                    <td></td>
		  	                     <c:if test="${multiCurrency=='Y'}">
		  	                    <td></td>
		  	                    </c:if>
		  	                    <td align="left" width="70px" >
						  	      <input type="text" style="text-align:right" name="actualGrossMargin${serviceOrder.id}" value="${serviceOrder.actualGrossMargin}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	    </td>
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="1" ></td>
		  	                    </c:if>	
		  	                    <td></td>
		  				  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="1" ></td>
		  	                    </c:if>	
		  	                    <td></td>
		  	                    </c:if>
 								<c:if test="${companyDivisionGlobal>1}"> 	
		  	                   <td></td> </c:if><td></td>
		  	                    <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
		  	                      <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                   
		  	                    <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                    <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    <td colspan="2" align="right"></td>
		  	                    <td></td>
		  	                    <td></td>
		  	                    </tr>
		  	                    <tr>
		  	                     <td align="right" colspan="11"><b><div align="right">Gross&nbsp;Margin%</div></b></td>
		  	                    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      
		  	                     
		  	                    <td align="left" colspan="3"></td>
		  	                  <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>
		  	                 <c:if test="${contractType}">
		  	                    <td></td>
		  	                    </c:if>			  	                    
						          <td align="left" width="40px">
						          </td>
						          <c:if test="${multiCurrency=='Y'}">
						  	      <td align="left"> </td>
						  	      </c:if>
						          <td align="left" width="40px">
						  	      <input type="text" style="text-align:right" name="estimatedGrossMarginPercentage${serviceOrder.id}" value="${serviceOrder.estimatedGrossMarginPercentage}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	      
						  	      </td>	
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>						  	      	
						  	     <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
								<td></td><td></td><td></td>
								
		  	                    <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>	
		  	                    <c:if test="${contractType}">
		  	                    <td></td>
		  	                    </c:if>
		  	                    
								<td></td>			  	                    
								<c:if test="${multiCurrency=='Y'}">
			  	                    <td align="left" ></td>
			  	                    </c:if>	
			  	                    <td></td><td></td>						          
								   <td></td>
						          <td></td>
						  	      <td align="left" width="70px">
						  	      <input type="text" style="text-align:right" name="revisedGrossMarginPercentage${serviceOrder.id}" value="${serviceOrder.revisedGrossMarginPercentage}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	      </td>	
						  	    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="2" ></td>
		  	                    </c:if>
								</c:if>						  	        	                    
						  	      <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  	                    <td></td>
		  	                    <c:if test="${chargeDiscountFlag}">
		  	                    <td></td>
		  	                    </c:if>
		  	                    <td></td>
		  	                     <c:if test="${multiCurrency=='Y'}">
		  	                    <td></td>
		  	                    </c:if>
						          <td align="left" width="40px">
						  	      <input type="text" style="text-align:right" name="actualGrossMarginPercentage${serviceOrder.id}" value="${serviceOrder.actualGrossMarginPercentage}" readonly="readonly" size="6" class="input-textUpper pr-f11 "/>
						  	      </td>	
						  	    <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="1" ></td>
		  	                    </c:if>		  	                    
		  	                    <td></td>
						  	     <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		  	                    <td align="left" colspan="1" ></td>
		  	                    </c:if>			  	                    
		  	                   <td></td>
		  	                    </c:if> 
								<c:if test="${companyDivisionGlobal>1}"> 	
		  	                    <td></td> </c:if><td></td>
		  	                    <configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
		  	                      <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    
		  	                    <configByCorp:fieldVisibility componentId="component.field.AccountLine.additionalService">
		  	                    <td></td>
		  	                    </configByCorp:fieldVisibility>
		  	                    <td colspan="2"></td>
		  	                    <td></td>
		  	                    <td></td>
		  	                    </tr>		  	                 
                             </display:footer> 
                          </display:table> 
		</td>
	</tr>
</table>
</div>
<table cellpadding="0" cellspacing="0"  class="detailTabLabel" border="0" style="margin:5px;padding:0px;" >
	<tr>
	  	<td>
<c:choose>
  <c:when test="${serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD'}">
  </c:when>
  <c:otherwise>
  <c:choose>
  <c:when test="${(!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup ) }">
  <input type="button" class="cssbuttonA" style="width:65px; height:25px" name="pricingAddLine" value="Add Line"  disabled="true" tabindex=""/> 
  <input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" disabled="true" tabindex=""/>
  <input type="button" class="cssbuttonA" style="width:130px; height:25px" disabled="true"	value="Add Default Template" tabindex=""/>
  </c:when>
<c:otherwise>  
			<input type="button" name="pricingAddLine" value="Add Line" onclick="checkBillingComplete();" class="cssbuttonA" style="width:65px; height:25px" tabindex=""/> 
			
			<c:if test="${not empty accountLineList}">
				<input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="findAllPricingLineId('');" tabindex=""/>
			</c:if>
			<c:if test="${empty accountLineList}">
				<input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="" disabled="disabled" tabindex=""/>
			</c:if>
            <input type="button" class="cssbuttonA" style="width:130px; height:25px" onclick="addDefaultSOPricingLineAjax('${serviceOrder.defaultAccountLineStatus}');" 	value="Add Default Template" tabindex="" />
            <configByCorp:fieldVisibility componentId="component.field.Pricing.GeneratingInvoice">
            <c:if test="${customerFile.moveType == null  || customerFile.moveType!='Quote'}">
            <sec-auth:authComponent componentId="module.accountLine.edit" replacementHtml="">
            			     <c:choose>
			    <c:when test="${!trackingStatus.accNetworkGroup && trackingStatus.soNetworkGroup }">
			    <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="Copy Estimates"  value="Copy Estimates" disabled="disabled"/>
			    </c:when>
			    <c:otherwise>
			    <input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="Copy Estimates"  value="Copy Estimates" onclick="javascript:openWindow('copyEstimates.html?sid=${serviceOrder.id}&decorator=popup&popup=true&pricingType=Y')" />
			    </c:otherwise>
			    </c:choose>
			    
			   <c:if test="${accountInterface=='Y'}">
			    <input type="button" class="cssbuttonA" style="width:160px; height:25px"   value="Apply Payb. Posting Date" onclick="findBookingAgent('postingDate');"/>
				</c:if>
				
				<c:choose> 
				<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
					<c:choose> 
						<c:when test="${billToLength>0}">
							<input type="button" class="cssbuttonA" style="width:120px; height:25px"
							value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
						</c:when>
						<c:otherwise>
							<input type="button" class="cssbuttonA" style="width:120px; height:25px"
							value="Generate Invoice" onclick="findBookingAgent('invoiceGen');" disabled="disabled"/>
						</c:otherwise>
					</c:choose>
					
				</c:when>
				<c:otherwise>
				<input type="button" class="cssbuttonA" style="width:120px; height:25px"
						value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/>
				</c:otherwise>
				</c:choose>	
				</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.accountLine.unPostPayable.Button" replacementHtml="">
		<input type="button" class="cssbuttonA" style="width:110px; height:25px"   value="Unpost Payable" onclick="findVendorInvoicesForUnposting()"/>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.accountLineReverseInvoices.edit" replacementHtml="">
	 <c:choose>
    <c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup }">
     <input type="button" class="cssbuttonA" style="width:115px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();" disabled="disabled"/>
    </c:when>
    <c:otherwise>
	<c:if test="${emptyList!=true}" >
	<input type="button" class="cssbuttonA" style="width:115px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();"/>
   </c:if> 
   <c:if test="${emptyList==true}" >
   <input type="button" class="cssbuttonA" style="width:115px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();" disabled="disabled"/>
   </c:if>
   </c:otherwise></c:choose>  
    </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.accountLineReset.edit" replacementHtml="">
	<c:if test="${accountInterface=='Y'}">
	<c:if test="${emptyList!=true}" >
	<input type="button" class="cssbuttonA" style="width:130px; height:25px"   value="Reset Send to Dates" onclick="openSendtoDates();"/>
    </c:if>
    <c:if test="${emptyList==true}" >
    <input type="button" class="cssbuttonA" style="width:130px; height:25px"   value="Reset Send to Dates" onclick="openSendtoDates();" disabled="disabled"/>
    </c:if>
    </c:if>
    </sec-auth:authComponent> 
				
				<configByCorp:fieldVisibility componentId="component.accountLine.PreviewInvoice.editAll"> 
    			<c:if test="${emptyList!=true}" >                                                                                                   
   				<input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onClick="return invoicePreview('${serviceOrder.shipNumber}',this);"/>
   				 </c:if>
   				<c:if test="${emptyList==true}" >
    			<input type="button" class="cssbuttonA" style="width:110px; height:25px"  name="PreviewInvoice"  value="Preview Invoice" onClick="return invoicePreview('${serviceOrder.shipNumber}',this);" disabled="disabled"/>
   				</c:if>
    			</configByCorp:fieldVisibility>
				</c:if>
				</configByCorp:fieldVisibility>
     <configByCorp:fieldVisibility componentId="component.button.SSCW.showAdvances">
		<input type="button" class="cssbuttonA" style="width:105px; height:25px; margin-top: 5px;"  name="Show Advances"  value="Show Advances" onclick="getAdvDtls(this)" />
	</configByCorp:fieldVisibility>
				
      </c:otherwise>
      </c:choose>
              <c:if test="${serviceOrder.corpID!='CWMS'}">
        <%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
              <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}"> 
           		<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
           		<c:if test="${trackingStatus.soNetworkGroup==null || trackingStatus.soNetworkGroup==false}">
               	 	<input type="button" class="cssbutton1" onclick="createPrice()" value="Create Pricing" style="width:120px; height:25px; margin-left:5px;margin-bottom:5px;" tabindex=""/>
                </c:if>
                	<c:if test="${trackingStatus.soNetworkGroup==true}">
								<input type="button" class="cssbutton1" disabled="true" value="Create Pricing" style="width:120px; height:25px;  margin:5px 0px;" tabindex=""/>
							</c:if>
                </configByCorp:fieldVisibility>
            </c:if>
            </c:if>
            </c:otherwise></c:choose>
            <c:if test="${company.accessQuotationFromCustomerFile==true}">
			<c:if test="${customerFile.moveType=='Quote'}">
            	<a><img align="middle" title="Forms" style="vertical-align:top;" onclick="findQuotationForms(this);" src="${pageContext.request.contextPath}/images/invoiceList.png"/></a>
			</c:if>
			</c:if>
		</td>
	</tr> 	
</table>