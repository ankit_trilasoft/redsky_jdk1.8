<%@ include file="/common/taglibs.jsp"%> 

<head>
	<meta name="heading" content="<fmt:message key='sharingForm.title'/>"/> 
	<title><fmt:message key="sharingForm.title"/></title>
	
<script language="javascript" type="text/javascript">

	function imposeMaxLength(Object, MaxLen)
	{
	return (Object.value.length <= MaxLen);
	}

	function changeStatus(){
	document.forms['sharingForm'].elements['formStatus'].value = '1';
    }
	
	function autoSave(clickType){		
	if(!(clickType == 'save')){
	if ('${autoSavePrompt}' == 'No'){

			var noSaveAction = '<c:out value="${sharing.id}"/>';

			var id1 = document.forms['sharingForm'].elements['sharing.id'].value;

			if(document.forms['sharingForm'].elements['gotoPageString'].value == 'gototab.shareList'){

			noSaveAction = 'sharings.html';

			}
			
		processAutoSave(document.forms['sharingForm'], 'savesharing!saveOnTabChange.html', noSaveAction);

		}else{
	var id1 = document.forms['sharingForm'].elements['sharing.id'].value;
	if (document.forms['sharingForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='sharingForm.title'/>");
		if(agree){
			document.forms['sharingForm'].action = 'savesharing!saveOnTabChange.html';
			document.forms['sharingForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['sharingForm'].elements['gotoPageString'].value == 'gototab.shareList'){
				location.href = 'sharings.html';
			}	
		  }
		}
	}else{
	if(id1 != ''){
		if(document.forms['sharingForm'].elements['gotoPageString'].value == 'gototab.shareList'){
				location.href = 'sharings.html';
				}
		   }
		}
	  }
	}
	}
</script>
	
</head>

<body>

<s:form id="sharingForm" name="sharingForm" action="savesharing" method="post" validate="true">

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:choose>
<c:when test="${gotoPageString == 'gototab.shareList' }">
    <c:redirect url="/sharings.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>       

<div id="Layer1" style="width:80%" onkeydown="changeStatus();">

<div id="newmnav">
		  <ul>
		    <li><a onclick="setReturnString('gototab.shareList');return autoSave();"><span>Revenue Sharing List</span></a></li>
		    <!--<li><a href="sharings.html"><span>Revenue Sharing List</span></a></li>
		    --><li id="newmnav1" style="background:#FFF "><a class="current"><span>Revenue Sharing Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    
		  </ul>
		</div><div class="spn">&nbsp;</div>
	<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" ><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="1" border="0" style="width:600px">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		  	
		  	<tr>
		  	<td width="10px"></td>
		  		<td align="right"><fmt:message key="sharingform.code"/></td>
		  		<td align="left" width="80px"><s:textfield name="sharing.code"  maxlength="25" cssStyle="width:65px"   cssClass="input-text" readonly="false" tabindex="1"/></td>
		  		<td align="right" width="100px"><fmt:message key="payrollform.companyDivision"/></td>
		  		<td align="left" ><s:select cssClass="list-menu"   name="sharing.companyDivision" headerKey=" " headerValue=" " list="%{compDevision}" cssStyle="width:206px" onchange="changeStatus();" tabindex="9"/></td> 
		  		<td width="10px"></td>
		  	</tr>
		  	    
		  		<tr>
		  		<td></td>
		  		<td align="right"><fmt:message key="sharingform.discription"/></td>
		  		<td align="left" colspan="3"><s:textfield name="sharing.description"   maxlength="250" cssStyle="width:464px"  cssClass="input-text" readonly="false" tabindex="2"/></td>
		  		<td width="10px"></td>
		  	</tr> 			  	    
		  	   
		  	    <tr>
		  	   <td></td>
		  		<td align="right"><fmt:message key="sharingform.usuallyUsedFor"/></td>
		  		<td align="left"><s:textfield name="sharing.usuallyUsedFor"  maxlength="25" cssStyle="width:150px"  cssClass="input-text" readonly="false" tabindex="3"/></td>
		  		<td width="10px"></td>
		  	</tr> 
		  	<tr>
		  	<td></td>
		  
				  <td align="right" valign="top" width="130px"><fmt:message key="sharingform.discriptionArea"/></td>
				 
		  			<td align="left" colspan="5"><s:textarea cssClass="textarea" name="sharing.descriptionArea"  onkeypress="return imposeMaxLength(this,1000);" cssStyle="width:464px;height:70px;" readonly="false" tabindex="4"/></td>
		  			<td width="10px"></td>
		  			
		  			</tr>
     		  	
		  </tbody>
		  </table>
		  	 
			 </tbody>
	</table> 
		  </div>
<div class="bottom-header"><span></span></div>
</div>
</div>	
 		  <table class="detailTabLabel" border="0" style="width:750px">
				<tbody>
				
					<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='sharingform.createdOn'/></b></td>
							<td style="">
							<fmt:formatDate var="sharingCreatedOnFormattedValue" value="${sharing.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="sharing.createdOn" value="${sharingCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${sharing.createdOn}" pattern="${displayDateTimeFormat}"/>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='sharingform.createdBy' /></b></td>
							<c:if test="${not empty sharing.id}">
								<s:hidden name="sharing.createdBy"/>
								<td style=""><s:label name="createdBy" value="%{sharing.createdBy}"/></td>
							</c:if>
							<c:if test="${empty sharing.id}">
								<s:hidden name="sharing.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style=""><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='sharing.updatedOn'/></b></td>
							<fmt:formatDate var="sharingupdatedOnFormattedValue" value="${sharing.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="sharing.updatedOn" value="${sharingupdatedOnFormattedValue}"/>
							<td style=""><fmt:formatDate value="${sharing.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='sharing.updatedBy' /></b></td>
							<c:if test="${not empty sharing.id}">
								<s:hidden name="sharing.updatedBy"/>
								<td style=""><s:label name="updatedBy" value="%{sharing.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty sharing.id}">
								<s:hidden name="sharing.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style=""><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table>
		  
		  <table> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save"/>  
        		</td>
       
                
        		<td align="right">
        		<c:if test="${not empty sharing.id}">
        		<input type="button" class="cssbutton1" style="width:50px;"  value="Add" onclick="location.href='<c:url value="/sharingForm.html"/>'" /> 
        		 </c:if>
	           
        		</td>
                
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
       	  	</tr>
       	  	</table>		
</div>
<s:hidden name="sharing.id"/>
</s:form>


		