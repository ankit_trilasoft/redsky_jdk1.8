package com.trilasoft.app.service.impl;


import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsTemporaryAccommodationDao;
import com.trilasoft.app.model.DsTemporaryAccommodation;
import com.trilasoft.app.service.DsTemporaryAccommodationManager;

public class DsTemporaryAccommodationManagerImpl extends GenericManagerImpl<DsTemporaryAccommodation, Long> implements DsTemporaryAccommodationManager {
	
	DsTemporaryAccommodationDao dsTemporaryAccommodationDao;
	
	public DsTemporaryAccommodationManagerImpl(DsTemporaryAccommodationDao dsTemporaryAccommodationDao) {
		super(dsTemporaryAccommodationDao); 
        this.dsTemporaryAccommodationDao = dsTemporaryAccommodationDao; 
	}
	public List getListById(Long id) {
		return dsTemporaryAccommodationDao.getListById(id);
	}
}
