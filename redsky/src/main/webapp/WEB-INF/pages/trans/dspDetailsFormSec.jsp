<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CAR_vendorCode)>-1) && dspDetails.CAR_vendorCode !='' && dspDetails.CAR_vendorCode !=null}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CAR')>-1)}">
	<!-- begin car -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('car')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='CAR'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="car">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.CAR_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('CAR_','${dspDetails.CAR_vendorCodeEXSO}'),chkIsVendorRedSky('CAR_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('CAR_','${dspDetails.CAR_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.CAR_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','CAR_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.CAR_serviceStartDate}">	   
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_serviceStartDate"/></s:text>
			 <td><s:textfield id="CAR_serviceStartDate" cssClass="input-text" name="dspDetails.CAR_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAR_serviceStartDate}">
		<td><s:textfield id="CAR_serviceStartDate" cssClass="input-text" name="dspDetails.CAR_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="CAR_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
				<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSAutomobileAssistanceNotes == '0' || countDSAutomobileAssistanceNotes == '' || countDSAutomobileAssistanceNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSAutomobileAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSAutomobileAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.CAR_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.CAR_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_serviceEndDate"/></s:text>
			 <td><s:textfield id="CAR_serviceEndDate" cssClass="input-text" name="dspDetails.CAR_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAR_serviceEndDate}">
		<td><s:textfield id="CAR_serviceEndDate" cssClass="input-text" name="dspDetails.CAR_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.CAR_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.CAR_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.CAR_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedCAR_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.CAR_displyOtherVendorCode}">
	 <c:set var="ischeckedCAR_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.CAR_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedCAR_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>	
</table>
<table>
	<tr>
	<td align="right"   class="listwhitetext">Notification Date</td>
   	<c:if test="${not empty dspDetails.CAR_notificationDate}">
	 <s:text id="notificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_notificationDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_notificationDate" cssClass="input-text" name="dspDetails.CAR_notificationDate" value="%{notificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_notificationDate}">
	<td width="65px" ><s:textfield id="CAR_notificationDate" cssClass="input-text" name="dspDetails.CAR_notificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if> 
 	<td align="right"   class="listwhitetext">Pick-up Date</td>
   	<c:if test="${not empty dspDetails.CAR_pickUpDate}">
	 <s:text id="pickUpDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_pickUpDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_pickUpDate" cssClass="input-text" name="dspDetails.CAR_pickUpDate" value="%{pickUpDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_pickUpDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_pickUpDate}">
	<td width="65px" ><s:textfield id="CAR_pickUpDate" cssClass="input-text" name="dspDetails.CAR_pickUpDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_pickUpDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if>
	</tr>
		<tr>
	<td align="right"   class="listwhitetext">Registration Date</td>
   	<c:if test="${not empty dspDetails.CAR_registrationDate}">
	 <s:text id="registrationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_registrationDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_registrationDate" cssClass="input-text" name="dspDetails.CAR_registrationDate" value="%{registrationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_registrationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_registrationDate}">
	<td width="65px" ><s:textfield id="CAR_registrationDate" cssClass="input-text" name="dspDetails.CAR_registrationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_registrationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if> 
 	<td align="right"   class="listwhitetext">Delivery Date</td>
   	<c:if test="${not empty dspDetails.CAR_deliveryDate}">
	 <s:text id="deliveryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_deliveryDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_deliveryDate" cssClass="input-text" name="dspDetails.CAR_deliveryDate" value="%{deliveryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_deliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_deliveryDate}">
	<td width="65px" ><s:textfield id="CAR_deliveryDate" cssClass="input-text" name="dspDetails.CAR_deliveryDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="CAR_deliveryDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
 </c:if>
	</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.CAR_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>	
</table>	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end car -->
	</c:if>
	</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CAR_vendorCode)<0) && dspDetails.CAR_vendorCode !='' && dspDetails.CAR_vendorCode !=null && dspDetails.CAR_displyOtherVendorCode==false}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CAR')>-1)}">
<s:hidden name="dspDetails.CAR_vendorCode" />
<s:hidden name="dspDetails.CAR_vendorName" />
	 <c:if test="${not empty dspDetails.CAR_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CAR_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.CAR_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CAR_serviceStartDate}">
		 <s:hidden   name="dspDetails.CAR_serviceStartDate"/> 
	 </c:if>
<s:hidden name="dspDetails.CAR_vendorContact" />
	 <c:if test="${not empty dspDetails.CAR_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CAR_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.CAR_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CAR_serviceEndDate}">
		 <s:hidden   name="dspDetails.CAR_serviceEndDate"/> 
	 </c:if>
<s:hidden name="dspDetails.CAR_vendorEmail" />
<s:hidden name="dspDetails.CAR_paymentResponsibility" />
<s:hidden name="dspDetails.CAR_displyOtherVendorCode" />
<s:hidden name="dspDetails.CAR_vendorCodeEXSO" />
	 <c:if test="${not empty dspDetails.CAR_notificationDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CAR_notificationDate" /></s:text>
			 <s:hidden  name="dspDetails.CAR_notificationDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CAR_notificationDate}">
		 <s:hidden   name="dspDetails.CAR_notificationDate"/> 
	 </c:if>
	 <c:if test="${not empty dspDetails.CAR_pickUpDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CAR_pickUpDate" /></s:text>
			 <s:hidden  name="dspDetails.CAR_pickUpDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CAR_pickUpDate}">
		 <s:hidden   name="dspDetails.CAR_pickUpDate"/> 
	 </c:if>
	 <c:if test="${not empty dspDetails.CAR_registrationDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CAR_registrationDate" /></s:text>
			 <s:hidden  name="dspDetails.CAR_registrationDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CAR_registrationDate}">
		 <s:hidden   name="dspDetails.CAR_registrationDate"/> 
	 </c:if>
	 <c:if test="${not empty dspDetails.CAR_deliveryDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.CAR_deliveryDate" /></s:text>
			 <s:hidden  name="dspDetails.CAR_deliveryDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.CAR_deliveryDate}">
		 <s:hidden   name="dspDetails.CAR_deliveryDate"/> 
	 </c:if>
<s:hidden name="dspDetails.CAR_comment" />
</c:if>
</c:if>
<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.CAR_vendorCode)<0) && dspDetails.CAR_vendorCode !='' && dspDetails.CAR_vendorCode !=null && dspDetails.CAR_displyOtherVendorCode}">
<c:if test="${(fn1:indexOf(serviceOrder.serviceType,'CAR')>-1)}">
	<!-- begin car -->
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('car')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='CAR'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="car">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.CAR_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('CAR_','${dspDetails.CAR_vendorCodeEXSO}'),chkIsVendorRedSky('CAR_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('CAR_','${dspDetails.CAR_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.CAR_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','CAR_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.CAR_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_serviceStartDate"/></s:text>
			 <td><s:textfield id="CAR_serviceStartDate" cssClass="input-textUpper" name="dspDetails.CAR_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAR_serviceStartDate}">
		<td><s:textfield id="CAR_serviceStartDate" cssClass="input-textUpper" name="dspDetails.CAR_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
		<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSAutomobileAssistanceNotes == '0' || countDSAutomobileAssistanceNotes == '' || countDSAutomobileAssistanceNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSAutomobileAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSAutomobileAssistanceNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsAutomobileAssistan&imageId=countDSAutomobileAssistanceNotesImage&fieldId=countDSAutomobileAssistanceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.CAR_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.CAR_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_serviceEndDate"/></s:text>
			 <td><s:textfield id="CAR_serviceEndDate" cssClass="input-textUpper" name="dspDetails.CAR_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.CAR_serviceEndDate}">
		<td><s:textfield id="CAR_serviceEndDate" cssClass="input-textUpper" name="dspDetails.CAR_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.CAR_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.CAR_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
	<c:set var="ischeckedCAR_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.CAR_displyOtherVendorCode}">
	 <c:set var="ischeckedCAR_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
<td class="listwhitetext" width=""><s:checkbox key="dspDetails.CAR_displyOtherVendorCode" onclick="changeStatus();" value="${ischeckedCAR_displyOtherVendorCode}"  disabled="true" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>	
</table>
<table>
	<tr>
	<td align="right"   class="listwhitetext">Notification Date</td>
   	<c:if test="${not empty dspDetails.CAR_notificationDate}">
	 <s:text id="notificationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_notificationDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_notificationDate" cssClass="input-textUpper" name="dspDetails.CAR_notificationDate" value="%{notificationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_notificationDate}">
	<td width="65px" ><s:textfield id="CAR_notificationDate" cssClass="input-textUpper" name="dspDetails.CAR_notificationDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
 </c:if> 
 	<td align="right"   class="listwhitetext">Pick-up Date</td>
   	<c:if test="${not empty dspDetails.CAR_pickUpDate}">
	 <s:text id="pickUpDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_pickUpDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_pickUpDate" cssClass="input-textUpper" name="dspDetails.CAR_pickUpDate" value="%{pickUpDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_pickUpDate}">
	<td width="65px" ><s:textfield id="CAR_pickUpDate" cssClass="input-textUpper" name="dspDetails.CAR_pickUpDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
 </c:if>
	</tr>
		<tr>
	<td align="right"   class="listwhitetext">Registration Date</td>
   	<c:if test="${not empty dspDetails.CAR_registrationDate}">
	 <s:text id="registrationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_registrationDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_registrationDate" cssClass="input-textUpper" name="dspDetails.CAR_registrationDate" value="%{registrationDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_registrationDate}">
	<td width="65px" ><s:textfield id="CAR_registrationDate" cssClass="input-textUpper" name="dspDetails.CAR_registrationDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
 </c:if> 
 	<td align="right"   class="listwhitetext">Delivery Date</td>
   	<c:if test="${not empty dspDetails.CAR_deliveryDate}">
	 <s:text id="deliveryDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.CAR_deliveryDate"/></s:text>
	 <td width="65px" ><s:textfield id="CAR_deliveryDate" cssClass="input-textUpper" name="dspDetails.CAR_deliveryDate" value="%{deliveryDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
	</c:if>
   <c:if test="${empty dspDetails.CAR_deliveryDate}">
	<td width="65px" ><s:textfield id="CAR_deliveryDate" cssClass="input-textUpper" name="dspDetails.CAR_deliveryDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
 </c:if>
	</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.CAR_comment" onkeyup="textLimit(this,4999)" readonly="true" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>	
</table>	
		</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<!-- end car -->
	</c:if>
	</c:if>
	<!-- start col -->
	<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.COL_vendorCode)>-1) && dspDetails.COL_vendorCode !='' && dspDetails.COL_vendorCode !=null }">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'COL')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('col')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center">
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='COL'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="col">
  <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0"  cellspacing="0" cellpadding="0"  style="margin:0px;" >
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.COL_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('COL_','${dspDetails.COL_vendorCodeEXSO}'),chkIsVendorRedSky('COL_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('COL_','${dspDetails.COL_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.COL_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','COL_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.COL_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_serviceStartDate"/></s:text>
			 <td><s:textfield id="COL_serviceStartDate" cssClass="input-text" name="dspDetails.COL_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="COL_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.COL_serviceStartDate}">
		<td><s:textfield id="COL_serviceStartDate" cssClass="input-text" name="dspDetails.COL_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="COL_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSColaServiceNotes == '0' || countDSColaServiceNotes == '' || countDSColaServiceNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSColaServiceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countdspDetailsNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.COL_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
 	    <c:if test="${not empty dspDetails.COL_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_serviceEndDate"/></s:text>
			 <td><s:textfield id="COL_serviceEndDate" cssClass="input-text" name="dspDetails.COL_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="COL_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.COL_serviceEndDate}">
		<td><s:textfield id="COL_serviceEndDate" cssClass="input-text" name="dspDetails.COL_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="COL_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.COL_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.COL_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.COL_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedCOL_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.COL_displyOtherVendorCode}">
	 <c:set var="ischeckedCOL_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.COL_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedCOL_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table align="left" class="detailTabLabel">

<tr> 
<td  align="right" class="listwhitetext" >Estimated&nbsp;Tax&nbsp;Allowance</td>
<td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedTaxAllowance" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
<td align="right" width="300px" class="listwhitetext">Notification Date</td>
	    <c:if test="${not empty dspDetails.COL_notificationDate}">
			 <s:text id="customerFileNotificationToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_notificationDate"/></s:text>
			 <td width="50"><s:textfield id="COL_notificationDate" cssClass="input-text" name="dspDetails.COL_notificationDate" value="%{customerFileNotificationToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="COL_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.COL_notificationDate}">
		<td width="50"><s:textfield id="COL_notificationDate" cssClass="input-text" name="dspDetails.COL_notificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="COL_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr>
<tr>
<td  align="right" class="listwhitetext" >Estimated&nbsp;Housing&nbsp;Allowance</td>
<td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedHousingAllowance" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
</tr>
<tr>
<td  align="right" class="listwhitetext" >Estimated&nbsp;Transportation&nbsp;Allowance</td>
<td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedTransportationAllowance" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
</tr>
<tr>
<td  align="right" class="listwhitetext" >Estimated Allowance&nbsp;${currencySign}</td>
    <td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedAllowanceEuro" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>

<td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.COL_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>
</tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end col -->
	<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.COL_vendorCode)<0) && dspDetails.COL_vendorCode !='' && dspDetails.COL_vendorCode !=null && dspDetails.COL_displyOtherVendorCode==false }">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'COL')>-1}">
<s:hidden name="dspDetails.COL_vendorCode" />
<s:hidden name="dspDetails.COL_vendorName" />
	 <c:if test="${not empty dspDetails.COL_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.COL_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.COL_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.COL_serviceStartDate}">
		 <s:hidden   name="dspDetails.COL_serviceStartDate"/> 
	 </c:if>
<s:hidden name="dspDetails.COL_vendorContact" />
	 <c:if test="${not empty dspDetails.COL_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.COL_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.COL_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.COL_serviceEndDate}">
		 <s:hidden   name="dspDetails.COL_serviceEndDate"/> 
	 </c:if>
	 <s:hidden name="dspDetails.COL_vendorCodeEXSO" />
<s:hidden name="dspDetails.COL_vendorEmail" />
<s:hidden name="dspDetails.COL_paymentResponsibility"/>
<s:hidden name="dspDetails.COL_displyOtherVendorCode" />
<s:hidden name="dspDetails.COL_estimatedAllowanceEuro" />
	 <c:if test="${not empty dspDetails.COL_notificationDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.COL_notificationDate" /></s:text>
			 <s:hidden  name="dspDetails.COL_notificationDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.COL_notificationDate}">
		 <s:hidden   name="dspDetails.COL_notificationDate"/> 
	 </c:if>
<s:hidden name="dspDetails.COL_comment" />
<s:hidden name="dspDetails.COL_estimatedHousingAllowance" />
<s:hidden name="dspDetails.COL_estimatedTransportationAllowance" />
<s:hidden name="dspDetails.COL_estimatedTaxAllowance" />
	</c:if>
	</c:if>			
			<!-- start col -->
	<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.COL_vendorCode)<0) && dspDetails.COL_vendorCode !='' && dspDetails.COL_vendorCode !=null && dspDetails.COL_displyOtherVendorCode }">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'COL')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('col')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center">
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='COL'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="col">
  <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0"  cellspacing="0" cellpadding="0"  style="margin:0px;" >
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.COL_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('COL_','${dspDetails.COL_vendorCodeEXSO}'),chkIsVendorRedSky('COL_'),changeStatus();"  /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('COL_','${dspDetails.COL_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.COL_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','COL_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.COL_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_serviceStartDate"/></s:text>
			 <td><s:textfield id="COL_serviceStartDate" cssClass="input-textUpper" name="dspDetails.COL_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.COL_serviceStartDate}">
		<td><s:textfield id="COL_serviceStartDate" cssClass="input-textUpper" name="dspDetails.COL_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
		<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSColaServiceNotes == '0' || countDSColaServiceNotes == '' || countDSColaServiceNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSColaServiceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countdspDetailsNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCola&imageId=countDSColaServiceNotesImage&fieldId=countDSColaServiceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.COL_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	   
	   <c:if test="${not empty dspDetails.COL_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_serviceEndDate"/></s:text>
			 <td><s:textfield id="COL_serviceEndDate" cssClass="input-textUpper" name="dspDetails.COL_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.COL_serviceEndDate}">
		<td><s:textfield id="COL_serviceEndDate" cssClass="input-textUpper" name="dspDetails.COL_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onchange="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.COL_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.COL_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedCOL_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.COL_displyOtherVendorCode}">
	 <c:set var="ischeckedCOL_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.COL_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedCOL_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table align="left" class="detailTabLabel">

<tr> 
<td  align="right" class="listwhitetext" >Estimated&nbsp;Tax&nbsp;Allowance</td>
<td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedTaxAllowance" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
<td align="right" width="225px" class="listwhitetext">Notification Date</td>
	    <c:if test="${not empty dspDetails.COL_notificationDate}">
			 <s:text id="customerFileNotificationToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.COL_notificationDate"/></s:text>
			 <td width="50"><s:textfield id="COL_notificationDate" cssClass="input-text" name="dspDetails.COL_notificationDate" value="%{customerFileNotificationToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="COL_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.COL_notificationDate}">
		<td width="50"><s:textfield id="COL_notificationDate" cssClass="input-text" name="dspDetails.COL_notificationDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="COL_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if> 
</tr> 
<tr>
<td  align="right" class="listwhitetext" >Estimated&nbsp;Housing&nbsp;Allowance</td>
<td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedHousingAllowance" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
</tr>
<tr>
<td  align="right" class="listwhitetext" >Estimated&nbsp;Transportation&nbsp;Allowance</td>
<td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedTransportationAllowance" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
</tr>
<tr>
<td  align="right" class="listwhitetext" >Estimated Allowance&nbsp;${currencySign}</td>
    <td ><s:textfield  cssClass="input-text" name="dspDetails.COL_estimatedAllowanceEuro" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
<td width="85px" align="right" class="listwhitetext" colspan="0"></td>
<td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.COL_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>
</tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end col -->
		<!-- start trg -->
		<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.TRG_vendorCode)>-1) && dspDetails.TRG_vendorCode !='' && dspDetails.TRG_vendorCode != null }">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TRG')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('trg')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TRG'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="trg">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;" >
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TRG_vendorCode" onchange="checkVendorNameRelo('TRG_','${dspDetails.TRG_vendorCodeEXSO}');chkIsVendorRedSky('TRG_');changeStatus();" readonly="true" size="5" maxlength="10"/></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TRG_','${dspDetails.TRG_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--> </td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.TRG_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','TRG_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TRG_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TRG_serviceStartDate"/></s:text>
			 <td><s:textfield id="TRG_serviceStartDate" cssClass="input-text" name="dspDetails.TRG_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TRG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TRG_serviceStartDate}">
		<td><s:textfield id="TRG_serviceStartDate" cssClass="input-text" name="dspDetails.TRG_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TRG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
  <c:if test="${not empty dspDetails.id}">
 <c:choose>
 <c:when test="${countDSCrossCulturalTrainingNotes == '0' || countDSCrossCulturalTrainingNotes == '' || countDSCrossCulturalTrainingNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSCrossCulturalTrainingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);" ></a></td> 
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSCrossCulturalTrainingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</td>
 
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TRG_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TRG_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TRG_serviceEndDate"/></s:text>
			 <td><s:textfield id="TRG_serviceEndDate" cssClass="input-text" name="dspDetails.TRG_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TRG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TRG_serviceEndDate}">
		<td><s:textfield id="TRG_serviceEndDate" cssClass="input-text" name="dspDetails.TRG_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="TRG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.TRG_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.TRG_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TRG_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedTRG_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TRG_displyOtherVendorCode}">
	 <c:set var="ischeckedTRG_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TRG_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedTRG_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table cellpadding="2" style="margin-left:100px" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext" >Employee Hours/Days Authorized</td>
<td align="left" class="listwhitetext" width="" ><s:textfield cssClass="input-text" key="dspDetails.TRG_employeeDaysAuthorized" readonly="false" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.TRG_employeeTime"	list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
<td width="" ></td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Spouse Hours/Days Authorized </td>
<td align="left" class="listwhitetext" width="" ><s:textfield cssClass="input-text" key="dspDetails.TRG_spouseDaysAuthorized" readonly="false" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.TRG_spouseTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
 <td width="" ></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.TRG_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end trg -->
	<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.TRG_vendorCode)<0) && dspDetails.TRG_vendorCode !='' && dspDetails.TRG_vendorCode != null && dspDetails.TRG_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TRG')>-1}">
													<s:hidden name="dspDetails.TRG_vendorCode" />
												<s:hidden name="dspDetails.TRG_vendorName" />
													 <c:if test="${not empty dspDetails.TRG_serviceStartDate}">
														 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.TRG_serviceStartDate" /></s:text>
															 <s:hidden  name="dspDetails.TRG_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
													 </c:if>
													 <c:if test="${empty dspDetails.TRG_serviceStartDate}">
														 <s:hidden   name="dspDetails.TRG_serviceStartDate"/> 
													 </c:if>

												<s:hidden name="dspDetails.TRG_vendorContact" />
													 <c:if test="${not empty dspDetails.TRG_serviceEndDate}">
														 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.TRG_serviceEndDate" /></s:text>
															 <s:hidden  name="dspDetails.TRG_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
													 </c:if>
													 <c:if test="${empty dspDetails.TRG_serviceEndDate}">
														 <s:hidden   name="dspDetails.TRG_serviceEndDate"/> 
													 </c:if>
												<s:hidden name="dspDetails.TRG_vendorEmail" />
												<s:hidden name="dspDetails.TRG_paymentResponsibility" />
												<s:hidden name="dspDetails.TRG_displyOtherVendorCode" />
												<s:hidden name="dspDetails.TRG_employeeTime" />
												<s:hidden name="dspDetails.TRG_vendorCodeEXSO" />
												<s:hidden name="dspDetails.TRG_employeeDaysAuthorized" />
												<s:hidden name="dspDetails.TRG_spouseDaysAuthorized" />
												<s:hidden name="dspDetails.TRG_spouseTime" />
												<s:hidden name="dspDetails.TRG_comment" />	</c:if>
	</c:if>
		<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.TRG_vendorCode)<0) && dspDetails.TRG_vendorCode !='' && dspDetails.TRG_vendorCode != null && dspDetails.TRG_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'TRG')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('trg')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='TRG'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="trg">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;" >
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.TRG_vendorCode" onchange="checkVendorNameRelo('TRG_','${dspDetails.TRG_vendorCodeEXSO}');chkIsVendorRedSky('TRG_');changeStatus();" readonly="true" size="5" maxlength="10"/></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('TRG_','${dspDetails.TRG_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--> </td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.TRG_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','TRG_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.TRG_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TRG_serviceStartDate"/></s:text>
			 <td><s:textfield id="TRG_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TRG_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TRG_serviceStartDate}">
		<td><s:textfield id="TRG_serviceStartDate" cssClass="input-textUpper" name="dspDetails.TRG_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
		<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
  <c:if test="${not empty dspDetails.id}">
 <c:choose>
 <c:when test="${countDSCrossCulturalTrainingNotes == '0' || countDSCrossCulturalTrainingNotes == '' || countDSCrossCulturalTrainingNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSCrossCulturalTrainingNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);" ></a></td> 
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSCrossCulturalTrainingNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsCrossCulturalTrain&imageId=countDSCrossCulturalTrainingNotesImage&fieldId=countDSCrossCulturalTrainingNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>

</td><tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TRG_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.TRG_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.TRG_serviceEndDate"/></s:text>
			 <td><s:textfield id="TRG_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TRG_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.TRG_serviceEndDate}">
		<td><s:textfield id="TRG_serviceEndDate" cssClass="input-textUpper" name="dspDetails.TRG_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.TRG_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.TRG_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedTRG_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.TRG_displyOtherVendorCode}">
	 <c:set var="ischeckedTRG_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display to other RLO Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.TRG_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedTRG_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table cellpadding="2" style="margin-left:100px" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext" >Employee Hours/Days Authorized</td>
<td align="left" class="listwhitetext" width="" ><s:textfield cssClass="input-textUpper" key="dspDetails.TRG_employeeDaysAuthorized" readonly="true" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.TRG_employeeTime" disabled="true"	list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
<td width="" ></td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Spouse Hours/Days Authorized </td>
<td align="left" class="listwhitetext" width="" ><s:textfield cssClass="input-textUpper" key="dspDetails.TRG_spouseDaysAuthorized" readonly="true" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.TRG_spouseTime" list="%{timeduration}" disabled="true" cssStyle="width:65px" onchange="changeStatus();"/></td>
 <td width="" ></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.TRG_comment" disabled="true" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end trg -->
	<!-- start hom -->
	<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.HOM_vendorCode)>-1) && dspDetails.HOM_vendorCode !='' && dspDetails.HOM_vendorCode != null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOM')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('hom')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='HOM'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="hom">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3"
		border="0">
		<tbody>
			<tr>
				<td align="right" class="listwhitetext" width="83"><fmt:message
					key="accountLine.vendorCode" /></td>
				<td align="left" width="350">
				<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;" >
					<tr>
						<td align="left" class="listwhitetext"><s:textfield
							cssClass="input-textUpper"
							key="dspDetails.HOM_vendorCode" readonly="true"
							size="5" maxlength="10" onchange="checkVendorNameRelo('HOM_','${dspDetails.HOM_vendorCodeEXSO}'),chkIsVendorRedSky('HOM_'),changeStatus();" /></td>
						<td align="left" width="10"><!--<img id="imgId2" align="left"
							class="openpopup" width="17" height="20" onclick="winOpenDest('HOM_','${dspDetails.HOM_vendorCodeEXSO}');"
							src="<c:url value='/images/open-popup.gif'/>" />--> </td>
						<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield
							cssClass="input-text"
							key="dspDetails.HOM_vendorName" readonly="true"
							cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();"/> <img align="top" class="openpopup"
							width="17" height="20" onclick="findAgent(this,'OA','HOM_');"
							src="<c:url value='/images/address2.png'/>" /></td>
					</tr>
				</table>
				<td align="right" width="100px" class="listwhitetext">Service
				Start</td>
				<c:if
					test="${not empty dspDetails.HOM_serviceStartDate}">
					<s:text id="customerFileSubmissionToTranfFormattedValue"
						name="${FormDateValue}">
						<s:param name="value"
							value="dspDetails.HOM_serviceStartDate" />
					</s:text>
					<td><s:textfield id="HOM_serviceStartDate" cssClass="input-text"
						name="dspDetails.HOM_serviceStartDate"
						value="%{customerFileSubmissionToTranfFormattedValue}"
						cssStyle="width:65px" maxlength="11" readonly="true"
						onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
					<td><img id="HOM_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if
					test="${empty dspDetails.HOM_serviceStartDate}">
					<td><s:textfield id="HOM_serviceStartDate" cssClass="input-text"
						name="dspDetails.HOM_serviceStartDate"
						cssStyle="width:65px" maxlength="11" readonly="true"
						onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
					<td><img id="HOM_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
					<c:if test="${empty dspDetails.id}">
					<td align="right" style="width:115px;!width:190px;"><img id="imgId3"
						src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
						HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
				</c:if>
				<c:if test="${not empty dspDetails.id}">
					<c:choose>
						<c:when
							test="${countDSHomeFindingAssistancePurchaseNotes == '0' || countDSHomeFindingAssistancePurchaseNotes == '' || countDSHomeFindingAssistancePurchaseNotes == null}">
							<td align="right" style="width:115px;!width:190px;"><img
								id="countDSHomeFindingAssistancePurchaseNotesImage"
								src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
								HEIGHT=17 WIDTH=50
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);" /><a
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);"></a></td>
						</c:when>
						<c:otherwise>
							<td align="right" style="width:115px;!width:100px;"><img
								id="countDSHomeFindingAssistancePurchaseNotesImage"
								src="${pageContext.request.contextPath}/images/notes_open1.jpg"
								HEIGHT=17 WIDTH=50
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);" /><a
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);"></a></td>
						</c:otherwise>
					</c:choose>
				</c:if> 
			</tr><tr>
				<td align="right" class="listwhitetext" width="83">Vendor
				Contact</td>
				<td align="left" class="listwhitetext"><s:textfield
					cssClass="input-text"
					key="dspDetails.HOM_vendorContact"
					readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
				<td align="right" class="listwhitetext">Service Finish</td>
				<c:if
					test="${not empty dspDetails.HOM_serviceEndDate}">
					<s:text id="customerFileSubmissionToTranfFormattedValue"
						name="${FormDateValue}">
						<s:param name="value"
							value="dspDetails.HOM_serviceEndDate" />
					</s:text>
					<td><s:textfield id="HOM_serviceEndDate" cssClass="input-text"
						name="dspDetails.HOM_serviceEndDate"
						value="%{customerFileSubmissionToTranfFormattedValue}"
						cssStyle="width:65px" maxlength="11" readonly="true"
						onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
					<td><img id="HOM_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty dspDetails.HOM_serviceEndDate}">
					<td><s:textfield id="HOM_serviceEndDate" cssClass="input-text"
						name="dspDetails.HOM_serviceEndDate"
						cssStyle="width:65px" maxlength="11" readonly="true"
						onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
					<td><img id="HOM_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.HOM_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			</tr>
			<tr>
				<td align="right" class="listwhitetext" width="83">Vendor Email</td>
				<td align="left" class="listwhitetext"><s:textfield
					cssClass="input-text"
					key="dspDetails.HOM_vendorEmail" readonly="false"
					size="57" maxlength="65" onchange="changeStatus();"/></td>
					<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
					<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
						<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.HOM_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
					</configByCorp:fieldVisibility>
					<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
            <c:set var="ischeckedHOM_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.HOM_displyOtherVendorCode}">
	 <c:set var="ischeckedHOM_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.HOM_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedHOM_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
		</configByCorp:fieldVisibility>
			</tr>
		</tbody>
	</table>
	<table width="100%" cellpadding="2" class="detailTabLabel">
		<tr>
			<td align="left" class="vertlinedata"></td>
		</tr>
	</table>
	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
	<table width="640" border="0" style="margin-left:19px" class="detailTabLabel">
	
	
	
	<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Code</td>
		<td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
		<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_agentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('HOM');changeStatus();"  /></td>
			<td align="left" class="listwhitetext" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.HOM_agentName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
		<td align="right" width="100px" class="listwhitetext">Initial&nbsp;Contact&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HOM_initialContactDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_initialContactDate"/></s:text>
					 <td><s:textfield id="HOM_initialContactDate" cssClass="input-text" name="dspDetails.HOM_initialContactDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_initialContactDate}">
				<td><s:textfield id="HOM_initialContactDate" cssClass="input-text" name="dspDetails.HOM_initialContactDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	</tr>
	
	<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_agentPhone" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" width="100px" class="listwhitetext">House&nbsp;Hunting&nbsp;Trip&nbsp;Arrival</td>
			    <c:if test="${not empty dspDetails.HOM_houseHuntingTripArrival}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_houseHuntingTripArrival"/></s:text>
					 <td><s:textfield id="HOM_houseHuntingTripArrival" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripArrival" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_houseHuntingTripArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_houseHuntingTripArrival}">
				<td><s:textfield id="HOM_houseHuntingTripArrival" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripArrival" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_houseHuntingTripArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	</tr>	
	
		<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_agentEmail" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" width="100px" class="listwhitetext">House&nbsp;Hunting&nbsp;Trip&nbsp;Departure</td>
			    <c:if test="${not empty dspDetails.HOM_houseHuntingTripDeparture}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_houseHuntingTripDeparture"/></s:text>
					 <td><s:textfield id="HOM_houseHuntingTripDeparture" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripDeparture" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_houseHuntingTripDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_houseHuntingTripDeparture}">
				<td><s:textfield id="HOM_houseHuntingTripDeparture" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripDeparture" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_houseHuntingTripDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	</tr>
	</table>
	</configByCorp:fieldVisibility>
			<table width="" border="0" style="margin-left:18px" class="detailTabLabel">
		<tr>
			<td align="right" width="64px" class="listwhitetext">View Status
			</td>
			<td width="65px"><s:select cssClass="list-menu"
				key="dspDetails.HOM_viewStatus"
				list="%{viewStatus}" headerKey="" headerValue=""
				cssStyle="width:105px" onchange="changeStatus();"/></td>	
          <td align="right" width="132px" class="listwhitetext">Move Date</td>
			<c:if test="${not empty dspDetails.HOM_moveInDate}">
				<s:text id="customerFileSubmissionToTranfFormattedValue1"
					name="${FormDateValue}">
					<s:param name="value"
						value="dspDetails.HOM_moveInDate" />
				</s:text>
				<td width="67"><s:textfield id="HOM_moveInDate" cssClass="input-text"
					name="dspDetails.HOM_moveInDate"
					value="%{customerFileSubmissionToTranfFormattedValue1}"
					cssStyle="width:65px" maxlength="11" readonly="true"
					onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
				<td><img id="HOM_moveInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty dspDetails.HOM_moveInDate}">
				<td width="67"><s:textfield id="HOM_moveInDate" cssClass="input-text"
					name="dspDetails.HOM_moveInDate"
					cssStyle="width:65px" maxlength="11" readonly="true"
					onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td>
				<td><img id="HOM_moveInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">	
		<td align="right" class="listwhitetext" width="119">Estimated&nbsp;HSR&nbsp;Referral&nbsp;$</td>
		<td><s:textfield cssClass="input-text" key="dspDetails.HOM_estimatedHSRReferral" readonly="false" size="7" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  /></td>
</configByCorp:fieldVisibility>		
		</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.HOM_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>	
		<td colspan="3" rowspan="2">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<table>
<tr>
<td align="right" class="listwhitetext" width="151">Actual &nbsp;HSR&nbsp;Referral&nbsp;$</td>
		<td align="left"  colspan="2" class="listwhitetext" width=""><s:textfield cssClass="input-text" key="dspDetails.HOM_actualHSRReferral" readonly="false" size="7" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
		</td>
</tr>
<tr>
	<td align="right" width="100px" class="listwhitetext">Offer&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HOM_offerDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_offerDate"/></s:text>
					 <td><s:textfield id="HOM_offerDate" cssClass="input-text" name="dspDetails.HOM_offerDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_offerDate}">
				<td><s:textfield id="HOM_offerDate" cssClass="input-text" name="dspDetails.HOM_offerDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
</tr>
<tr>
		<td align="right" width="131px" class="listwhitetext">Closing&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HOM_closingDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_closingDate"/></s:text>
					 <td width="50"><s:textfield id="HOM_closingDate" cssClass="input-text" name="dspDetails.HOM_closingDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_closingDate}">
				<td  width="50"><s:textfield id="HOM_closingDate" cssClass="input-text" name="dspDetails.HOM_closingDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
				
</tr>
</table>
</configByCorp:fieldVisibility>	
</td>
</tr>
		<tr><td></td></tr>
	</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end hom -->
		<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.HOM_vendorCode)<0) && dspDetails.HOM_vendorCode !='' && dspDetails.HOM_vendorCode != null && dspDetails.HOM_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOM')>-1}">
			<s:hidden name="dspDetails.HOM_vendorCode" />
		<s:hidden name="dspDetails.HOM_vendorName" />
		<c:if test="${not empty dspDetails.HOM_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.HOM_serviceStartDate" /></s:text>
		<s:hidden  name="dspDetails.HOM_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.HOM_serviceStartDate}">
		 <s:hidden   name="dspDetails.HOM_serviceStartDate"/> 
		</c:if>
		<c:if test="${not empty dspDetails.HOM_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.HOM_serviceEndDate" /></s:text>
		<s:hidden  name="dspDetails.HOM_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.HOM_serviceEndDate}">
				 <s:hidden   name="dspDetails.HOM_serviceEndDate"/> 
		</c:if>
		<s:hidden name="dspDetails.HOM_vendorContact" />
		<s:hidden name="dspDetails.HOM_vendorEmail" />
		<s:hidden name="dspDetails.HOM_paymentResponsibility" />
		<s:hidden name="dspDetails.HOM_displyOtherVendorCode" />
		<s:hidden name="dspDetails.HOM_vendorCodeEXSO" />
		<c:if test="${not empty dspDetails.HOM_moveInDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.HOM_moveInDate" /></s:text>
		<s:hidden  name="dspDetails.HOM_moveInDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.HOM_moveInDate}">
		 <s:hidden   name="dspDetails.HOM_moveInDate"/> 
		</c:if>
		<s:hidden name="dspDetails.HOM_comment" />
        <s:hidden name="dspDetails.HOM_agentCode" />                
		<s:hidden name="dspDetails.HOM_agentName" />                
		<c:if test="${not empty dspDetails.HOM_initialContactDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.HOM_initialContactDate" /></s:text>
		<s:hidden  name="dspDetails.HOM_initialContactDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.HOM_initialContactDate}">
		 <s:hidden   name="dspDetails.HOM_initialContactDate"/> 
		</c:if>      
		<s:hidden name="dspDetails.HOM_agentPhone" />               
		<c:if test="${not empty dspDetails.HOM_houseHuntingTripArrival}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.HOM_houseHuntingTripArrival" /></s:text>
		<s:hidden  name="dspDetails.HOM_houseHuntingTripArrival" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.HOM_houseHuntingTripArrival}">
		 <s:hidden   name="dspDetails.HOM_houseHuntingTripArrival"/> 
		</c:if>
		<s:hidden name="dspDetails.HOM_agentEmail" />  
		<s:hidden name="dspDetails.HOM_estimatedHSRReferral" />
		<s:hidden name="dspDetails.HOM_actualHSRReferral" />
		<c:if test="${not empty dspDetails.HOM_houseHuntingTripDeparture}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.HOM_houseHuntingTripDeparture" /></s:text>
		<s:hidden  name="dspDetails.HOM_houseHuntingTripDeparture" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.HOM_houseHuntingTripDeparture}">
		 <s:hidden   name="dspDetails.HOM_houseHuntingTripDeparture"/> 
		</c:if>
		<c:if test="${not empty dspDetails.HOM_offerDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.HOM_offerDate" /></s:text>
		<s:hidden  name="dspDetails.HOM_offerDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.HOM_offerDate}">
		 <s:hidden   name="dspDetails.HOM_offerDate"/> 
		</c:if>      
		<c:if test="${not empty dspDetails.HOM_closingDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.HOM_closingDate" /></s:text>
		<s:hidden  name="dspDetails.HOM_closingDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.HOM_closingDate}">
		 <s:hidden   name="dspDetails.HOM_closingDate"/> 
		</c:if>        
	</c:if>
	</c:if>
		<!-- start hom  displyOtherVendorCode-->
	<c:if test="${(fn1:indexOf(rloSetVenderCode,dspDetails.HOM_vendorCode)<0) && dspDetails.HOM_vendorCode !='' && dspDetails.HOM_vendorCode != null && dspDetails.HOM_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'HOM')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('hom')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='HOM'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="hom">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3"
		border="0">
		<tbody>
			<tr>
				<td align="right" class="listwhitetext" width="83"><fmt:message
					key="accountLine.vendorCode" /></td>
				<td align="left" width="350">
				<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;" >
					<tr>
						<td align="left" class="listwhitetext"><s:textfield	cssClass="input-textUpper" 	key="dspDetails.HOM_vendorCode" readonly="true" size="5" maxlength="10" onchange="checkVendorNameRelo('HOM_','${dspDetails.HOM_vendorCodeEXSO}'),chkIsVendorRedSky('HOM_'),changeStatus();" /></td>
						<td align="left" width="10"></td>
						<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield cssClass="input-textUpper" 	key="dspDetails.HOM_vendorName" readonly="true"  cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();"/>
							</td>
							</tr>
				</table>
				<td align="right" width="100px" class="listwhitetext">Service Start</td>
				<c:if test="${not empty dspDetails.HOM_serviceStartDate}">
					<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}">
						<s:param name="value" value="dspDetails.HOM_serviceStartDate" />
					</s:text>
					<td><s:textfield id="HOM_serviceStartDate" cssClass="input-textUpper" name="dspDetails.HOM_serviceStartDate"  value="%{customerFileSubmissionToTranfFormattedValue}"  cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();" /></td>
									</c:if>
				<c:if test="${empty dspDetails.HOM_serviceStartDate}">
					<td><s:textfield id="HOM_serviceStartDate" cssClass="input-textUpper" name="dspDetails.HOM_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();" /></td>
				</c:if>
					<c:if test="${empty dspDetails.id}">
					<td align="right" style="width:115px;!width:190px;"><img id="imgId3"
						src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
						HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
				</c:if>
				<c:if test="${not empty dspDetails.id}">
					<c:choose>
						<c:when
							test="${countDSHomeFindingAssistancePurchaseNotes == '0' || countDSHomeFindingAssistancePurchaseNotes == '' || countDSHomeFindingAssistancePurchaseNotes == null}">
							<td align="right" style="width:115px;!width:190px;"><img
								id="countDSHomeFindingAssistancePurchaseNotesImage"
								src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
								HEIGHT=17 WIDTH=50
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);" /><a
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);"></a></td>
						</c:when>
						<c:otherwise>
							<td align="right" style="width:115px;!width:100px;"><img
								id="countDSHomeFindingAssistancePurchaseNotesImage"
								src="${pageContext.request.contextPath}/images/notes_open1.jpg"
								HEIGHT=17 WIDTH=50
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);" /><a
								onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomePurchase&imageId=countDSHomeFindingAssistancePurchaseNotesImage&fieldId=countDSHomeFindingAssistancePurchaseNotes&decorator=popup&popup=true',800,600);"></a></td>
						</c:otherwise>
					</c:choose>
				</c:if> 
			</tr><tr>
				<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
				<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.HOM_vendorContact" 	readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
				<td align="right" class="listwhitetext">Service Finish</td>
				<c:if 	test="${not empty dspDetails.HOM_serviceEndDate}">
					<s:text id="customerFileSubmissionToTranfFormattedValue"
						name="${FormDateValue}">
						<s:param name="value"
							value="dspDetails.HOM_serviceEndDate" />
					</s:text>
					<td><s:textfield id="HOM_serviceEndDate" cssClass="input-textUpper"  name="dspDetails.HOM_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();" /></td>
				</c:if>
				<c:if test="${empty dspDetails.HOM_serviceEndDate}">
					<td><s:textfield id="HOM_serviceEndDate" cssClass="input-textUpper" name="dspDetails.HOM_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();" /></td>
				</c:if>
			</tr>
			<tr>
				<td align="right" class="listwhitetext" width="83">Vendor Email</td>
				<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();"/></td>
            <configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
					<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
						<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.HOM_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
					</configByCorp:fieldVisibility>
            <configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
            <c:set var="ischeckedHOM_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.HOM_displyOtherVendorCode}">
	 <c:set var="ischeckedHOM_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display to other RLO Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.HOM_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedHOM_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
		</configByCorp:fieldVisibility>	
			</tr>
		</tbody>
	</table>
	<table width="100%" cellpadding="2" class="detailTabLabel">
		<tr>
			<td align="left" class="vertlinedata"></td>
		</tr>
	</table>
	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
	<table width="640" border="0" style="margin-left:19px" class="detailTabLabel">
	
	
	
	<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Code</td>
		<td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
		<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_agentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('HOM');changeStatus();"  /></td>
			<td align="left" class="listwhitetext" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.HOM_agentName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
		<td align="right" width="100px" class="listwhitetext">Initial&nbsp;Contact&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HOM_initialContactDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_initialContactDate"/></s:text>
					 <td><s:textfield id="HOM_initialContactDate" cssClass="input-text" name="dspDetails.HOM_initialContactDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_initialContactDate}">
				<td><s:textfield id="HOM_initialContactDate" cssClass="input-text" name="dspDetails.HOM_initialContactDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_initialContactDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	</tr>
	
	<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_agentPhone" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" width="100px" class="listwhitetext">House&nbsp;Hunting&nbsp;Trip&nbsp;Arrival</td>
			    <c:if test="${not empty dspDetails.HOM_houseHuntingTripArrival}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_houseHuntingTripArrival"/></s:text>
					 <td><s:textfield id="HOM_houseHuntingTripArrival" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripArrival" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_houseHuntingTripArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_houseHuntingTripArrival}">
				<td><s:textfield id="HOM_houseHuntingTripArrival" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripArrival" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_houseHuntingTripArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	</tr>	
	
		<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.HOM_agentEmail" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" width="100px" class="listwhitetext">House&nbsp;Hunting&nbsp;Trip&nbsp;Departure</td>
			    <c:if test="${not empty dspDetails.HOM_houseHuntingTripDeparture}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_houseHuntingTripDeparture"/></s:text>
					 <td><s:textfield id="HOM_houseHuntingTripDeparture" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripDeparture" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_houseHuntingTripDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_houseHuntingTripDeparture}">
				<td><s:textfield id="HOM_houseHuntingTripDeparture" cssClass="input-text" name="dspDetails.HOM_houseHuntingTripDeparture" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_houseHuntingTripDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
	</tr>
		
	</table>
	</configByCorp:fieldVisibility>
			<table width="" border="0" style="margin-left:18px" class="detailTabLabel">
		<tr>
			<td align="right" width="64px" class="listwhitetext">View Status
			</td>
			<td width="65px"><s:select cssClass="list-menu" key="dspDetails.HOM_viewStatus" list="%{viewStatus}"  disabled="true"  headerKey="" headerValue="" cssStyle="width:105px" onchange="changeStatus();"/></td> 
			<td align="right" width="132px" class="listwhitetext">Move Date</td>
			<c:if test="${not empty dspDetails.HOM_moveInDate}">
				<s:text id="customerFileSubmissionToTranfFormattedValue1"
					name="${FormDateValue}">
					<s:param name="value" value="dspDetails.HOM_moveInDate" />
				</s:text>
				<td width="67"><s:textfield id="HOM_moveInDate" cssClass="input-textUpper" name="dspDetails.HOM_moveInDate" value="%{customerFileSubmissionToTranfFormattedValue1}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();" /></td>
			</c:if>
			<c:if test="${empty dspDetails.HOM_moveInDate}">
				<td width="67"><s:textfield id="HOM_moveInDate" cssClass="input-textUpper" 	name="dspDetails.HOM_moveInDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();" /></td>
			</c:if>
		</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.HOM_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>	
		<td colspan="3" rowspan="2">
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<table>
<tr>
<td align="right" class="listwhitetext" width="151">Actual &nbsp;HSR&nbsp;Referral&nbsp;$</td>
		<td align="left"  colspan="2" class="listwhitetext" width=""><s:textfield cssClass="input-text" key="dspDetails.HOM_actualHSRReferral" readonly="false" size="7" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event);" onchange="return checkFloat(this);changeStatus();"  />
		</td>
</tr>
<tr>
	<td align="right" width="100px" class="listwhitetext">Offer&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HOM_offerDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_offerDate"/></s:text>
					 <td><s:textfield id="HOM_offerDate" cssClass="input-text" name="dspDetails.HOM_offerDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_offerDate}">
				<td><s:textfield id="HOM_offerDate" cssClass="input-text" name="dspDetails.HOM_offerDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_offerDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
</tr>
<tr>
		<td align="right" width="131px" class="listwhitetext">Closing&nbsp;Date</td>
			    <c:if test="${not empty dspDetails.HOM_closingDate}">
					 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.HOM_closingDate"/></s:text>
					 <td width="50"><s:textfield id="HOM_closingDate" cssClass="input-text" name="dspDetails.HOM_closingDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="HOM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			    <c:if test="${empty dspDetails.HOM_closingDate}">
				<td  width="50"><s:textfield id="HOM_closingDate" cssClass="input-text" name="dspDetails.HOM_closingDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="HOM_closingDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
				
</tr>
</table>
</configByCorp:fieldVisibility>	
</td>
</tr>		
		<tr><td></td></tr>
	</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end hom -->
		<!-- start rnt -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.RNT_vendorCode)>-1 && dspDetails.RNT_vendorCode !='' && dspDetails.RNT_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RNT')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rnt')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RNT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rnt">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0"  cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.RNT_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('RNT_','${dspDetails.RNT_vendorCodeEXSO}'),changeStatus();chkIsVendorRedSky('RNT_');" /></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.RNT_vendorName" readonly="true" cssStyle="width:16.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','RNT_');" src="<c:url value='/images/address2.png'/>" />
	</td>
	<td align="left">
					<div style="float:left;width:52px;">
					<div id="hidDestRUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
					<div id="hidDestAP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
					</div>
				</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RNT_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_serviceStartDate"/></s:text>
			 <td><s:textfield id="RNT_serviceStartDate" cssClass="input-text" name="dspDetails.RNT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_serviceStartDate}">
		<td><s:textfield id="RNT_serviceStartDate" cssClass="input-text" name="dspDetails.RNT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDsHomeRentalNotes == '0' || countDsHomeRentalNotes == '' || countDsHomeRentalNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSHomeRentalNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomeRental&imageId=countDSHomeRentalNotesImage&fieldId=countDsHomeRentalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomeRental&imageId=countDSHomeRentalNotesImage&fieldId=countDsHomeRentalNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSHomeRentalNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsHomeRental&imageId=countDSHomeRentalNotesImage&fieldId=countDsHomeRentalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsHomeRental&imageId=countDSHomeRentalNotesImage&fieldId=countDsHomeRentalNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.RNT_vendorContact" readonly="false" size="51" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.RNT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_serviceEndDate"/></s:text>
			 <td><s:textfield id="RNT_serviceEndDate" cssClass="input-text" name="dspDetails.RNT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_serviceEndDate}">
		<td><s:textfield id="RNT_serviceEndDate" cssClass="input-text" name="dspDetails.RNT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.RNT_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.RNT_vendorEmail" readonly="false" size="51" maxlength="65" onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RNT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedRNT_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.RNT_displyOtherVendorCode}">
	 <c:set var="ischeckedRNT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.RNT_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedRNT_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tr><td align="right" class="listwhitetext">Monthly Rental Allowance</td>
    <td ><s:textfield  cssClass="input-text" name="dspDetails.RNT_monthlyRentalAllowance" cssStyle="width:65px" maxlength="10"  onchange="changeStatus();"/>   </td>
     <td align="right" class="listwhitetext">Currency</td>
		    	<td align="left" colspan="0"><s:select name="dspDetails.RNT_monthlyRentalAllowanceCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
    <td align="right" class="listwhitetext" >Lease Start Date</td>
     <c:if test="${not empty dspDetails.RNT_leaseStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_leaseStartDate"/></s:text>
			 <td width="60"><s:textfield id="RNT_leaseStartDate" cssClass="input-text" name="dspDetails.RNT_leaseStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_leaseStartDate}">
		<td width="60"><s:textfield id="RNT_leaseStartDate" cssClass="input-text" name="dspDetails.RNT_leaseStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="RNT_leaseStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td  align="right" class="listwhitetext" >Security Deposit</td>
    <td ><s:textfield  cssClass="input-text" name="dspDetails.RNT_securityDeposit" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
     <td align="right" class="listwhitetext">Currency</td>
		    	<td align="left" colspan="0"><s:select name="dspDetails.RNT_securityDepositCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
    <td align="right" class="listwhitetext" width="142">Lease Expire Date</td>
   <c:if test="${not empty dspDetails.RNT_leaseExpireDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_leaseExpireDate"/></s:text>
			 <td><s:textfield id="RNT_leaseExpireDate" cssClass="input-text" name="dspDetails.RNT_leaseExpireDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_leaseExpireDate}">
		<td><s:textfield id="RNT_leaseExpireDate" cssClass="input-text" name="dspDetails.RNT_leaseExpireDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_leaseExpireDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
		
		<configByCorp:fieldVisibility componentId="component.tab.dspDetails.reminderDate">
		<td colspan="4"></td>
<td align="right" colspan=""  width="265" class="listwhitetext" >Reminder&nbsp;Date&nbsp;3&nbsp;Mos&nbsp;Prior&nbsp;to&nbsp;Expiry&nbsp;of&nbsp;Lease</td>
		<c:if test="${not empty dspDetails.RNT_reminderDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_reminderDate"/></s:text>
			 <td><s:textfield id="RNT_reminderDate" cssClass="input-text" name="dspDetails.RNT_reminderDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_reminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_reminderDate}">
		<td><s:textfield id="RNT_reminderDate" cssClass="input-text" name="dspDetails.RNT_reminderDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_reminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		
		<configByCorp:fieldVisibility componentId="component.tab.dspDetails.checkInDateShowVOER">
		<td colspan="4"></td>
<td align="right" colspan=""  width="" class="listwhitetext" >Check&nbsp;In&nbsp;Date</td>
<c:if test="${not empty dspDetails.RNT_checkInDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_checkInDate"/></s:text>
			 <td><s:textfield id="RNT_checkInDate" cssClass="input-text" name="dspDetails.RNT_checkInDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_checkInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_checkInDate}">
		<td><s:textfield id="RNT_checkInDate" cssClass="input-text" name="dspDetails.RNT_checkInDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_checkInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" >Negotiated Rent</td>
<td colspan="3" ><s:textarea cssClass="textarea"  rows="2" cols="40" name="dspDetails.RNT_negotiatedRent"  onchange="changeStatus();" /> </td>
<td colspan="4" style="vertical-align:top;">
<table class="detailTabLabel">
<tr>
<td width="84"></td>
<td align="right" width="123" class="listwhitetext">Lease&nbsp;Signee</td>
<td align="left" colspan="0"><s:select name="dspDetails.RNT_leaseSignee" list="%{leaseSignee}" cssClass="list-menu" cssStyle="width:93px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
</tr>
<tr>
<td width="84"></td>
<configByCorp:fieldVisibility componentId="component.Portal.visaExtensionNeeded.showVOER">
<td align="right" class="listwhitetext" >Lease&nbsp;Extension&nbsp;Needed</td>
<td><s:select cssClass="list-menu" name="dspDetails.RNT_leaseExtensionNeeded" list="%{yesno}" cssStyle="width:93px;" /></td>
 </configByCorp:fieldVisibility>
 </tr>
 <tr>
<td width="87"></td>
<configByCorp:fieldVisibility componentId="component.Portal.homeSearchType.showIMSB">
<td align="right" width="126" class="listwhitetext">Home&nbsp;Search&nbsp;Type</td>
<td align="left" colspan="0"><s:select name="dspDetails.RNT_homeSearchType" list="{'Accompanied','Un-Accompanied'}" cssClass="list-menu"  headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
 </configByCorp:fieldVisibility>
 </tr>
 </table>
 </td>

</tr>
<tr>
<td align="right" class="listwhitetext" >Utilities Included</td>
<td colspan="5">
<table class="detailTabLabel">
<tr>
<td><s:select cssClass="list-menu" id="leaseReviewUtilities" name="dspDetails.RNT_utilitiesIncluded" list="%{utilities}" value="%{multiplutilities}" multiple="true" cssStyle="width:246px; height:100px" headerKey="" headerValue="" /></td>
   <td class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;">
   <b>* Use Control + mouse to select multiple Utilities Type</b>
 </td>
 </tr>
 </table>
 </td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Deposit Paid By</td>
<td colspan="4" ><s:select cssClass="list-menu" name="dspDetails.RNT_depositPaidBy" list="%{depositby}" cssStyle="width:246px;" headerKey="" headerValue="" /></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.RNT_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
	<!-- end rnt -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.RNT_vendorCode)<0 && dspDetails.RNT_vendorCode !='' && dspDetails.RNT_vendorCode !=null && dspDetails.RNT_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RNT')>-1}">
			<s:hidden name="dspDetails.RNT_vendorName" />
		<c:if test="${not empty dspDetails.RNT_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.RNT_serviceStartDate" /></s:text>
		<s:hidden  name="dspDetails.RNT_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.RNT_serviceStartDate}">
		 <s:hidden   name="dspDetails.RNT_serviceStartDate"/> 
		</c:if>
		<s:hidden name="dspDetails.RNT_vendorContact" />
		<c:if test="${not empty dspDetails.RNT_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.RNT_serviceEndDate" /></s:text>
		<s:hidden  name="dspDetails.RNT_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.RNT_serviceEndDate}">
		 <s:hidden   name="dspDetails.RNT_serviceEndDate"/> 
		</c:if>
		<s:hidden name="dspDetails.RNT_vendorCodeEXSO" />
		<s:hidden name="dspDetails.RNT_vendorEmail" />
		<s:hidden name="dspDetails.RNT_paymentResponsibility" />
		<s:hidden name="dspDetails.RNT_displyOtherVendorCode" />
		<s:hidden name="dspDetails.RNT_monthlyRentalAllowance" />
		<c:if test="${not empty dspDetails.RNT_leaseStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.RNT_leaseStartDate" /></s:text>
		<s:hidden  name="dspDetails.RNT_leaseStartDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.RNT_leaseStartDate}">
		 <s:hidden   name="dspDetails.RNT_leaseStartDate"/> 
		</c:if>
		<s:hidden name="dspDetails.RNT_securityDeposit" />
		<c:if test="${not empty dspDetails.RNT_leaseExpireDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.RNT_leaseExpireDate" /></s:text>
		<s:hidden  name="dspDetails.RNT_leaseExpireDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.RNT_leaseExpireDate}">
		 <s:hidden   name="dspDetails.RNT_leaseExpireDate"/> 
		</c:if>
		<s:hidden name="dspDetails.RNT_paidCurrency" />
		<c:if test="${not empty dspDetails.RNT_checkInDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.RNT_checkInDate" /></s:text>
		<s:hidden  name="dspDetails.RNT_checkInDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.RNT_checkInDate}">
		 <s:hidden   name="dspDetails.RNT_checkInDate"/> 
		</c:if>
		<c:if test="${not empty dspDetails.RNT_reminderDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.RNT_reminderDate" /></s:text>
		<s:hidden  name="dspDetails.RNT_reminderDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.RNT_reminderDate}">
		 <s:hidden   name="dspDetails.RNT_reminderDate"/> 
		</c:if>
		<s:hidden name="dspDetails.RNT_negotiatedRent" />
		<s:hidden name="dspDetails.RNT_utilitiesIncluded" />
		<s:hidden name="dspDetails.RNT_depositPaidBy" />
		<s:hidden name="dspDetails.RNT_comment" />
		<s:hidden name="dspDetails.RNT_leaseSignee" />
		<s:hidden name="dspDetails.RNT_homeSearchType" />
		<s:hidden name="dspDetails.RNT_leaseExtensionNeeded" />
		 
	</c:if>
	</c:if>
			<!-- start rnt displyOtherVendorCode -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.RNT_vendorCode)<0 && dspDetails.RNT_vendorCode !='' && dspDetails.RNT_vendorCode !=null && dspDetails.RNT_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'RNT')>-1}">	
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('rnt')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='RNT'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="rnt">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0"  cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.RNT_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('RNT_','${dspDetails.RNT_vendorCodeEXSO}'),changeStatus();chkIsVendorRedSky('RNT_');" /></td>
	<td align="left"width="10"><!-- <img align="left" id="imgId1" class="openpopup" width="17" height="20" onclick="winOpenDest('RNT_','${dspDetails.RNT_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" />--> </td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.RNT_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','RNT_');" src="<c:url value='/images/address2.png'/>" />
	</td>
	<td align="left">
					<div style="float:left;width:52px;">
					<div id="hidDestRUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
					<div id="hidDestAP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
					</div>
				</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.RNT_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_serviceStartDate"/></s:text>
			 <td><s:textfield id="RNT_serviceStartDate" cssClass="input-textUpper" name="dspDetails.RNT_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_serviceStartDate}">
		<td><s:textfield id="RNT_serviceStartDate" cssClass="input-textUpper" name="dspDetails.RNT_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.RNT_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.RNT_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_serviceEndDate"/></s:text>
			 <td><s:textfield id="RNT_serviceEndDate" cssClass="input-textUpper" name="dspDetails.RNT_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_serviceEndDate}">
		<td><s:textfield id="RNT_serviceEndDate" cssClass="input-textUpper" name="dspDetails.RNT_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.RNT_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.RNT_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedRNT_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.RNT_displyOtherVendorCode}">
	 <c:set var="ischeckedRNT_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display to other RLO Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.RNT_displyOtherVendorCode" disabled="true" onclick="changeStatus();" value="${ischeckedRNT_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tr><td align="right" class="listwhitetext">Monthly Rental Allowance</td>
    <td ><s:textfield  cssClass="input-textUpper" name="dspDetails.RNT_monthlyRentalAllowance" readonly="true" cssStyle="width:65px" maxlength="10"  onchange="changeStatus();"/>   </td>
    <td align="right" class="listwhitetext">Currency</td>
		    	<td align="left" colspan="0"><s:select name="dspDetails.RNT_monthlyRentalAllowanceCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
  <td align="right" class="listwhitetext" >Lease Start Date</td>
     <c:if test="${not empty dspDetails.RNT_leaseStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_leaseStartDate"/></s:text>
			 <td width="60"><s:textfield id="RNT_leaseStartDate" cssClass="input-textUpper" name="dspDetails.RNT_leaseStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_leaseStartDate}">
		<td width="60"><s:textfield id="RNT_leaseStartDate" cssClass="input-textUpper" name="dspDetails.RNT_leaseStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr><td  align="right" class="listwhitetext" >Security Deposit</td>
    <td ><s:textfield  cssClass="input-textUpper" name="dspDetails.RNT_securityDeposit" readonly="true" cssStyle="width:65px" maxlength="10"    onchange="changeStatus();"/>   </td>
      <td align="right" class="listwhitetext">Currency</td>
		    	<td align="left" colspan="0"><s:select name="dspDetails.RNT_securityDepositCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:60px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="62"/></td>
    <td align="right" class="listwhitetext" width="142">Lease Expire Date</td>
   <c:if test="${not empty dspDetails.RNT_leaseExpireDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_leaseExpireDate"/></s:text>
			 <td><s:textfield id="RNT_leaseExpireDate" cssClass="input-textUpper" name="dspDetails.RNT_leaseExpireDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();" /></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_leaseExpireDate}">
		<td><s:textfield id="RNT_leaseExpireDate" cssClass="input-textUpper" name="dspDetails.RNT_leaseExpireDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();" /></td>
		</c:if>
</tr>
<tr>
<configByCorp:fieldVisibility componentId="component.tab.dspDetails.checkInDateShowVOER">
<td align="right" colspan=""  width="225" class="listwhitetext" >Check&nbsp;In&nbsp;Date</td>
<c:if test="${not empty dspDetails.RNT_checkInDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_checkInDate"/></s:text>
			 <td><s:textfield id="RNT_checkInDate" cssClass="input-text" name="dspDetails.RNT_checkInDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_checkInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_checkInDate}">
		<td><s:textfield id="RNT_checkInDate" cssClass="input-text" name="dspDetails.RNT_checkInDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_checkInDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</configByCorp:fieldVisibility>
		
		<configByCorp:fieldVisibility componentId="component.tab.dspDetails.reminderDate">
<td align="right" colspan=""  width="225" class="listwhitetext" >Reminder&nbsp;Date&nbsp;3&nbsp;Mos&nbsp;Prior&nbsp;to&nbsp;Expiry&nbsp;of&nbsp;Lease</td>
		<c:if test="${not empty dspDetails.RNT_reminderDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.RNT_reminderDate"/></s:text>
			 <td><s:textfield id="RNT_reminderDate" cssClass="input-text" name="dspDetails.RNT_reminderDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_reminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.RNT_reminderDate}">
		<td><s:textfield id="RNT_reminderDate" cssClass="input-text" name="dspDetails.RNT_reminderDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();" /></td><td><img id="RNT_reminderDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		
</tr>
<tr>
<td align="right" class="listwhitetext" >Negotiated Rent</td>
<td colspan="4" ><s:textarea cssClass="textarea"  rows="2" cols="40" readonly="true" name="dspDetails.RNT_negotiatedRent"  onchange="changeStatus();" /> </td>
<td colspan="4" style="vertical-align:top;">
<table class="detailTabLabel">
<tr>
<td width="84"></td>
<td align="right" width="127" class="listwhitetext">Lease&nbsp;Signee</td>
<td align="left" colspan="0"><s:select name="dspDetails.RNT_leaseSignee" list="%{leaseSignee}" cssClass="list-menu" cssStyle="width:90px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
</tr>
<tr>
<td width="84"></td>
<configByCorp:fieldVisibility componentId="component.Portal.visaExtensionNeeded.showVOER">
<td align="right" class="listwhitetext" >Lease&nbsp;Extension&nbsp;Needed</td>
<td><s:select cssClass="list-menu" name="dspDetails.RNT_leaseExtensionNeeded" list="%{yesno}" cssStyle="width:78px;" /></td>
 </configByCorp:fieldVisibility>
 </tr>
 <tr>
<td width="84"></td>
<configByCorp:fieldVisibility componentId="component.Portal.homeSearchType.showIMSB">
<td align="right" class="listwhitetext" >Home&nbsp;Search&nbsp;Type</td>
<td><s:select cssClass="list-menu" name="dspDetails.RNT_homeSearchType" list="{'Accompanied','Un-Accompanied'}" headerKey="" headerValue="" cssStyle="width:78px;" /></td>
 </configByCorp:fieldVisibility>
 </tr>
 
 </table>
 </td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Utilities Included</td>
<td colspan="5">
<table class="detailTabLabel">
<tr>
<td><s:select cssClass="list-menu" id="leaseReviewUtilities" name="dspDetails.RNT_utilitiesIncluded" disabled="true" list="%{utilities}" value="%{multiplutilities}" multiple="true" cssStyle="width:246px; height:100px" headerKey="" headerValue="" /></td>
   <td class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;">
   <b>* Use Control + mouse to select multiple Utilities Type</b>
 </td>
 </tr>
 </table>
 </td>
</tr>
<tr>
<td align="right" class="listwhitetext" >Deposit Paid By</td>
<td colspan="4" ><s:select cssClass="list-menu" name="dspDetails.RNT_depositPaidBy" list="%{depositby}" disabled="true" cssStyle="width:246px;" headerKey="" headerValue="" /></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea" disabled="true" rows="4" cols="40" name="dspDetails.RNT_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
	<!-- end rnt -->
	<!-- start set --> 
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.SET_vendorCode)>-1 && dspDetails.SET_vendorCode !='' && dspDetails.SET_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SET')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('set')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='SET'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="set">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.SET_vendorCode" size="5" maxlength="10" 
onchange="checkVendorNameRelo('SET_','${dspDetails.SET_vendorCodeEXSO}'),chkIsVendorRedSky('SET_'),changeStatus();" readonly="true" /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('SET_','${dspDetails.SET_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.SET_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','SET_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.SET_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_serviceStartDate"/></s:text>
			 <td><s:textfield id="SET_serviceStartDate" cssClass="input-text" name="dspDetails.SET_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onselect="changeStatus();"/></td><td><img id="SET_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_serviceStartDate}">
		<td><s:textfield id="SET_serviceStartDate" cssClass="input-text" name="dspDetails.SET_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onselect="changeStatus();"/></td><td><img id="SET_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>		
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSSettlingInNotes == '0' || countDSSettlingInNotes == '' || countDSSettlingInNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSSettlingInNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsSettlingIn&imageId=countDSSettlingInNotesImage&fieldId=countDSSettlingInNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsSettlingIn&imageId=countDSSettlingInNotesImage&fieldId=countDSSettlingInNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSSettlingInNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsSettlingIn&imageId=countDSSettlingInNotesImage&fieldId=countDSSettlingInNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsSettlingIn&imageId=countDSSettlingInNotesImage&fieldId=countDSSettlingInNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.SET_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.SET_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_serviceEndDate"/></s:text>
			 <td><s:textfield id="SET_serviceEndDate" cssClass="input-text" name="dspDetails.SET_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_serviceEndDate}">
		<td><s:textfield id="SET_serviceEndDate" cssClass="input-text" name="dspDetails.SET_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.SET_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.SET_vendorEmail" readonly="false" size="57" maxlength="65"  onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.SET_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedSET_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.SET_displyOtherVendorCode}">
	 <c:set var="ischeckedSET_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.SET_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedSET_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
<tr> <td></td></tr>
</tbody>
</table> 
<table width="100%" cellpadding="0" cellspacing="0" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
<tr><td align="left" class="subcontenttabChild" style="font-weight:bold;padding-bottom:0px;">Services Assisted</td></tr>
</table >
<table class="detailTabLabel" border="0">
<tr><td></td></tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right"><c:set var="isSET_pPermit" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_pPermit')>-1}">
<c:set var="isSET_pPermit" value="true" />
</c:if>
<s:checkbox name="SET_pPermit" id="SET_pPermit" onclick="changeStatus();" value="${isSET_pPermit}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Parking Permit</td>

	    <c:if test="${not empty dspDetails.SET_parkingPermit}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_parkingPermit"/></s:text>
			 <td width="70"><s:textfield id="SET_parkingPermit" cssClass="input-text" name="dspDetails.SET_parkingPermit" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_parkingPermit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_parkingPermit}">
		<td width="70"><s:textfield id="SET_parkingPermit" cssClass="input-text" name="dspDetails.SET_parkingPermit" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_parkingPermit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td width="40" align="right"><c:set var="isSET_gID" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_gID')>-1}">
<c:set var="isSET_gID" value="true" />
</c:if>
<s:checkbox name="SET_gID" id="SET_gID" onclick="changeStatus();" value="${isSET_gID}" fieldValue="true" /></td>
		<td align="right" class="listwhitetext" >Government&nbsp;ID</td>
	    <c:if test="${not empty dspDetails.SET_governmentID}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_governmentID"/></s:text>
			 <td width="70"><s:textfield id="SET_governmentID" cssClass="input-text" name="dspDetails.SET_governmentID" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_parkingPermit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_governmentID}">
		<td width="70"><s:textfield id="SET_governmentID" cssClass="input-text" name="dspDetails.SET_governmentID" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_governmentID-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<td  width="40" align="right"><c:set var="isSET_initialRequested" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_initialRequested')>-1}">
<c:set var="isSET_initialRequested" value="true" />
</c:if>
<s:checkbox name="SET_initialRequested" id="SET_initialRequested" onclick="changeStatus();" value="${isSET_initialRequested}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Initial&nbsp;date&nbsp;of&nbsp;service&nbsp;requested</td>
	    <c:if test="${not empty dspDetails.SET_initialDateofServiceRequested}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_initialDateofServiceRequested"/></s:text>
			 <td width="70"><s:textfield id="SET_initialDateofServiceRequested" cssClass="input-text" name="dspDetails.SET_initialDateofServiceRequested" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_initialDateofServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_initialDateofServiceRequested}">
		<td width="70"><s:textfield id="SET_initialDateofServiceRequested" cssClass="input-text" name="dspDetails.SET_initialDateofServiceRequested" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_initialDateofServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right">
<c:set var="isSET_bAccount" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_bAccount')>-1}">
<c:set var="isSET_bAccount" value="true" />
</c:if>
<s:checkbox name="SET_bAccount" id="SET_bAccount" onclick="changeStatus();" value="${isSET_bAccount}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Bank Account Opening</td>

	    <c:if test="${not empty dspDetails.SET_bankAccount}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_bankAccount"/></s:text>
			 <td><s:textfield id="SET_bankAccount" cssClass="input-text" name="dspDetails.SET_bankAccount" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_bankAccount-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_bankAccount}">
		<td><s:textfield id="SET_bankAccount" cssClass="input-text" name="dspDetails.SET_bankAccount" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_bankAccount-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td width="40" align="right">
<c:set var="isSET_hCare" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_hCare')>-1}">
<c:set var="isSET_hCare" value="true" />
</c:if>
<s:checkbox name="SET_hCare" id="SET_hCare" onclick="changeStatus();" value="${isSET_hCare}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Heath&nbsp;Care</td>
	    <c:if test="${not empty dspDetails.SET_heathCare}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_heathCare"/></s:text>
			 <td width="70"><s:textfield id="SET_heathCare" cssClass="input-text" name="dspDetails.SET_heathCare" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_heathCare-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_heathCare}">
		<td width="70"><s:textfield id="SET_heathCare" cssClass="input-text" name="dspDetails.SET_heathCare" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_heathCare-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
		<td align="right" class="listwhitetext" >Provider&nbsp;notification&nbsp;sent</td>
	    <c:if test="${not empty dspDetails.SET_providerNotificationSent}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_providerNotificationSent"/></s:text>
			 <td width="70"><s:textfield id="SET_providerNotificationSent" cssClass="input-text" name="dspDetails.SET_providerNotificationSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_providerNotificationSent}">
		<td width="70"><s:textfield id="SET_providerNotificationSent" cssClass="input-text" name="dspDetails.SET_providerNotificationSent" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right"><c:set var="isSET_cHall" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_cHall')>-1}">
<c:set var="isSET_cHall" value="true" />
</c:if>
<s:checkbox name="SET_cHall" id="SET_cHall" onclick="changeStatus();" value="${isSET_cHall}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >City Hall Registration</td>

	    <c:if test="${not empty dspDetails.SET_cityHall}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_cityHall"/></s:text>
			 <td><s:textfield id="SET_cityHall" cssClass="input-text" name="dspDetails.SET_cityHall" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_cityHall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_cityHall}">
		<td><s:textfield id="SET_cityHall" cssClass="input-text" name="dspDetails.SET_cityHall" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_cityHall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td width="40" align="right">	
<c:set var="isSET_utilitie" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_utilitie')>-1}">
<c:set var="isSET_utilitie" value="true" />
</c:if>
<s:checkbox name="SET_utilitie" id="SET_utilitie" onclick="changeStatus();" value="${isSET_utilitie}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Utilities</td>
	    <c:if test="${not empty dspDetails.SET_utilities}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_utilities"/></s:text>
			 <td width="70"><s:textfield id="SET_utilities" cssClass="input-text" name="dspDetails.SET_utilities" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_utilities-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_utilities}">
		<td width="70"><s:textfield id="SET_utilities" cssClass="input-text" name="dspDetails.SET_utilities" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_utilities-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
		<td align="right" class="listwhitetext" >Provider&nbsp;confirmation&nbsp;received</td>
	    <c:if test="${not empty dspDetails.SET_providerConfirmationReceived}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_providerConfirmationReceived"/></s:text>
			 <td width="70"><s:textfield id="SET_providerConfirmationReceived" cssClass="input-text" name="dspDetails.SET_providerConfirmationReceived" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_providerConfirmationReceived}">
		<td width="70"><s:textfield id="SET_providerConfirmationReceived" cssClass="input-text" name="dspDetails.SET_providerConfirmationReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right"><c:set var="isSET_mConnection" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_mConnection')>-1}">
<c:set var="isSET_mConnection" value="true" />
</c:if>
<s:checkbox name="SET_mConnection" id="SET_mConnection" onclick="changeStatus();" value="${isSET_mConnection}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Media Connection</td>

	    <c:if test="${not empty dspDetails.SET_internetConn}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_internetConn"/></s:text>
			 <td><s:textfield id="SET_internetConn" cssClass="input-text" name="dspDetails.SET_internetConn" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_internetConn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_internetConn}">
		<td><s:textfield id="SET_internetConn" cssClass="input-text" name="dspDetails.SET_internetConn" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_internetConn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td width="40" align="right">
<c:set var="isSET_autoRegistration" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_autoRegistration')>-1}">
<c:set var="isSET_autoRegistration" value="true" />
</c:if>
<s:checkbox name="SET_autoRegistration" id="SET_autoRegistration" onclick="changeStatus();" value="${isSET_autoRegistration}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Automobile&nbsp;Registration</td>
	    <c:if test="${not empty dspDetails.SET_automobileRegistration}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_automobileRegistration"/></s:text>
			 <td width="70"><s:textfield id="SET_automobileRegistration" cssClass="input-text" name="dspDetails.SET_automobileRegistration" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_automobileRegistration-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_automobileRegistration}">
		<td width="70"><s:textfield id="SET_automobileRegistration" cssClass="input-text" name="dspDetails.SET_automobileRegistration" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_automobileRegistration-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">		
<td width="40" align="right">		
<c:set var="isSET_dLicense" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_dLicense')>-1}">
<c:set var="isSET_dLicense" value="true" />
</c:if>
<s:checkbox name="SET_dLicense" id="SET_dLicense" onclick="changeStatus();" value="${isSET_dLicense}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Driver's License</td>

	    <c:if test="${not empty dspDetails.SET_driverLicense}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_driverLicense"/></s:text>
			 <td><s:textfield id="SET_driverLicense" cssClass="input-text" name="dspDetails.SET_driverLicense" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_driverLicense-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_driverLicense}">
		<td><s:textfield id="SET_driverLicense" cssClass="input-text" name="dspDetails.SET_driverLicense" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_driverLicense-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
		<td align="right" class="listwhitetext" >Day&nbsp;of&nbsp;service&nbsp;authorized#</td>
	    <c:if test="${not empty dspDetails.SET_dayofServiceAuthorized}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_dayofServiceAuthorized"/></s:text>
			 <td width="70"><s:textfield id="SET_dayofServiceAuthorized" cssClass="input-text" name="dspDetails.SET_dayofServiceAuthorized" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_dayofServiceAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_dayofServiceAuthorized}">
		<td width="70"><s:textfield id="SET_dayofServiceAuthorized" cssClass="input-text" name="dspDetails.SET_dayofServiceAuthorized" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_dayofServiceAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.SET_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end set -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.SET_vendorCode)<0 && dspDetails.SET_vendorCode !='' && dspDetails.SET_vendorCode !=null && dspDetails.SET_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SET')>-1}">
	<s:hidden name="dspDetails.SET_vendorCode" />
	<s:hidden name="dspDetails.SET_vendorName" />
	<s:hidden name="dspDetails.SET_displyOtherVendorCode" />
	<c:if test="${not empty dspDetails.SET_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.SET_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_serviceStartDate}">
		 <s:hidden   name="dspDetails.SET_serviceStartDate"/> 
	</c:if>
	<s:hidden name="dspDetails.SET_vendorCodeEXSO" />
	<s:hidden name="dspDetails.SET_vendorContact" />
	<c:if test="${not empty dspDetails.SET_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.SET_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_serviceEndDate}">
		 <s:hidden   name="dspDetails.SET_serviceEndDate"/> 
	</c:if>
	<s:hidden name="dspDetails.SET_vendorEmail" />
	<s:hidden name="dspDetails.SET_paymentResponsibility" />
	<s:hidden name="SET_pPermit" />
	<c:if test="${not empty dspDetails.SET_parkingPermit}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_parkingPermit" /></s:text>
	<s:hidden  name="dspDetails.SET_parkingPermit" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_parkingPermit}">
		 <s:hidden   name="dspDetails.SET_parkingPermit"/> 
	</c:if>
	<c:if test="${not empty dspDetails.SET_bankAccount}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_bankAccount" /></s:text>
	<s:hidden  name="dspDetails.SET_bankAccount" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_bankAccount}">
		 <s:hidden   name="dspDetails.SET_bankAccount"/> 
	</c:if>
	<c:if test="${not empty dspDetails.SET_cityHall}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_cityHall" /></s:text>
	<s:hidden  name="dspDetails.SET_cityHall" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_cityHall}">
		 <s:hidden   name="dspDetails.SET_cityHall"/> 
	</c:if>
	<c:if test="${not empty dspDetails.SET_internetConn}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_internetConn" /></s:text>
	<s:hidden  name="dspDetails.SET_internetConn" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_internetConn}">
		 <s:hidden   name="dspDetails.SET_internetConn"/> 
	</c:if>
	<c:if test="${not empty dspDetails.SET_driverLicense}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_driverLicense" /></s:text>
	<s:hidden  name="dspDetails.SET_driverLicense" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_driverLicense}">
		 <s:hidden   name="dspDetails.SET_driverLicense"/> 
	</c:if>
	<s:hidden name="dspDetails.SET_comment" />
	<c:if test="${not empty dspDetails.SET_governmentID}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_governmentID" /></s:text>
	<s:hidden  name="dspDetails.SET_governmentID" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_governmentID}">
		 <s:hidden   name="dspDetails.SET_governmentID"/> 
	</c:if>
	<c:if test="${not empty dspDetails.SET_heathCare}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_heathCare" /></s:text>
	<s:hidden  name="dspDetails.SET_heathCare" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_heathCare}">
		 <s:hidden   name="dspDetails.SET_heathCare"/> 
	</c:if>
	<c:if test="${not empty dspDetails.SET_utilities}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_utilities" /></s:text>
	<s:hidden  name="dspDetails.SET_utilities" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_utilities}">
		 <s:hidden   name="dspDetails.SET_utilities"/> 
	</c:if>
	<c:if test="${not empty dspDetails.SET_automobileRegistration}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_automobileRegistration" /></s:text>
	<s:hidden  name="dspDetails.SET_automobileRegistration" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_automobileRegistration}">
		 <s:hidden   name="dspDetails.SET_automobileRegistration"/> 
	</c:if>
	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
	<c:if test="${not empty dspDetails.SET_dayofServiceAuthorized}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_dayofServiceAuthorized" /></s:text>
	<s:hidden  name="dspDetails.SET_dayofServiceAuthorized" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_dayofServiceAuthorized}">
		 <s:hidden   name="dspDetails.SET_dayofServiceAuthorized"/> 
	</c:if>
	</configByCorp:fieldVisibility>
	<c:if test="${not empty dspDetails.SET_initialDateofServiceRequested}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_initialDateofServiceRequested" /></s:text>
	<s:hidden  name="dspDetails.SET_initialDateofServiceRequested" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_initialDateofServiceRequested}">
		 <s:hidden   name="dspDetails.SET_initialDateofServiceRequested"/> 
	</c:if>
	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
	<c:if test="${not empty dspDetails.SET_providerNotificationSent}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_providerNotificationSent" /></s:text>
	<s:hidden  name="dspDetails.SET_providerNotificationSent" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_providerNotificationSent}">
		 <s:hidden   name="dspDetails.SET_providerNotificationSent"/> 
	</c:if>
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
	<c:if test="${not empty dspDetails.SET_providerConfirmationReceived}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.SET_providerConfirmationReceived" /></s:text>
	<s:hidden  name="dspDetails.SET_providerConfirmationReceived" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${empty dspDetails.SET_providerConfirmationReceived}">
		 <s:hidden   name="dspDetails.SET_providerConfirmationReceived"/> 
	</c:if>
	</configByCorp:fieldVisibility>
	</c:if>
	</c:if>
			<!-- start set displyOtherVendorCode--> 
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.SET_vendorCode)<0 && dspDetails.SET_vendorCode !='' && dspDetails.SET_vendorCode !=null && dspDetails.SET_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'SET')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('set')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='SET'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="set">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.SET_vendorCode" size="5" maxlength="10" 
onchange="checkVendorNameRelo('SET_','${dspDetails.SET_vendorCodeEXSO}'),chkIsVendorRedSky('SET_'),changeStatus();" readonly="true" /></td>
	<td align="left"width="10"><!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('SET_','${dspDetails.SET_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.SET_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','SET_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.SET_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_serviceStartDate"/></s:text>
			 <td><s:textfield id="SET_serviceStartDate" cssClass="input-textUpper" name="dspDetails.SET_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onselect="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_serviceStartDate}">
		<td><s:textfield id="SET_serviceStartDate" cssClass="input-textUpper" name="dspDetails.SET_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onselect="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.SET_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.SET_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_serviceEndDate"/></s:text>
			 <td><s:textfield id="SET_serviceEndDate" cssClass="input-textUpper" name="dspDetails.SET_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_serviceEndDate}">
		<td><s:textfield id="SET_serviceEndDate" cssClass="input-textUpper" name="dspDetails.SET_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.SET_vendorEmail" readonly="true" size="57" maxlength="65"  onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.SET_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedSET_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.SET_displyOtherVendorCode}">
	 <c:set var="ischeckedSET_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display to other RLO Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.SET_displyOtherVendorCode" disabled="true" onclick="changeStatus();" value="${ischeckedSET_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
<tr> <td></td></tr>
</tbody>
</table> 
<table width="100%" cellpadding="0" cellspacing="0" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
<tr><td align="left" class="subcontenttabChild" style="font-weight:bold;">Services Assisted</td></tr>
</table >
<table class="detailTabLabel" border="0">
<tr><td></td></tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
	<td width="40" align="right"><c:set var="isSET_pPermit" value="false" />
	<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_pPermit')>-1}">
	<c:set var="isSET_pPermit" value="true" />
	</c:if>
	<s:checkbox name="SET_pPermit" id="SET_pPermit" onclick="changeStatus();" value="${isSET_pPermit}" fieldValue="true" />
	</td>
	</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Parking Permit</td>

	    <c:if test="${not empty dspDetails.SET_parkingPermit}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_parkingPermit"/></s:text>
			 <td width="70"><s:textfield id="SET_parkingPermit" cssClass="input-text" name="dspDetails.SET_parkingPermit" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_parkingPermit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_parkingPermit}">
		<td width="70"><s:textfield id="SET_parkingPermit" cssClass="input-text" name="dspDetails.SET_parkingPermit" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_parkingPermit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td width="40" align="right"><c:set var="isSET_gID" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_gID')>-1}">
<c:set var="isSET_gID" value="true" />
</c:if>
<s:checkbox name="SET_gID" id="SET_gID" onclick="changeStatus();" value="${isSET_gID}" fieldValue="true" /></td>
		<td align="right" class="listwhitetext" >Government&nbsp;ID</td>
	    <c:if test="${not empty dspDetails.SET_governmentID}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_governmentID"/></s:text>
			 <td width="70"><s:textfield id="SET_governmentID" cssClass="input-text" name="dspDetails.SET_governmentID" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_parkingPermit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_governmentID}">
		<td width="70"><s:textfield id="SET_governmentID" cssClass="input-text" name="dspDetails.SET_governmentID" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_governmentID-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<td  width="40" align="right"><c:set var="isSET_initialRequested" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_initialRequested')>-1}">
<c:set var="isSET_initialRequested" value="true" />
</c:if>
<s:checkbox name="SET_initialRequested" id="SET_initialRequested" onclick="changeStatus();" value="${isSET_initialRequested}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Initial&nbsp;date&nbsp;of&nbsp;service&nbsp;requested</td>
	    <c:if test="${not empty dspDetails.SET_initialDateofServiceRequested}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_initialDateofServiceRequested"/></s:text>
			 <td width="70"><s:textfield id="SET_initialDateofServiceRequested" cssClass="input-text" name="dspDetails.SET_initialDateofServiceRequested" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_initialDateofServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_initialDateofServiceRequested}">
		<td width="70"><s:textfield id="SET_initialDateofServiceRequested" cssClass="input-text" name="dspDetails.SET_initialDateofServiceRequested" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_initialDateofServiceRequested-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right">
<c:set var="isSET_bAccount" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_bAccount')>-1}">
<c:set var="isSET_bAccount" value="true" />
</c:if>
<s:checkbox name="SET_bAccount" id="SET_bAccount" onclick="changeStatus();" value="${isSET_bAccount}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Bank Account Opening</td>

	    <c:if test="${not empty dspDetails.SET_bankAccount}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_bankAccount"/></s:text>
			 <td><s:textfield id="SET_bankAccount" cssClass="input-text" name="dspDetails.SET_bankAccount" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_bankAccount-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_bankAccount}">
		<td><s:textfield id="SET_bankAccount" cssClass="input-text" name="dspDetails.SET_bankAccount" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_bankAccount-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td width="40" align="right">
<c:set var="isSET_hCare" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_hCare')>-1}">
<c:set var="isSET_hCare" value="true" />
</c:if>
<s:checkbox name="SET_hCare" id="SET_hCare" onclick="changeStatus();" value="${isSET_hCare}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Heath&nbsp;Care</td>
	    <c:if test="${not empty dspDetails.SET_heathCare}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_heathCare"/></s:text>
			 <td width="70"><s:textfield id="SET_heathCare" cssClass="input-text" name="dspDetails.SET_heathCare" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_heathCare-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_heathCare}">
		<td width="70"><s:textfield id="SET_heathCare" cssClass="input-text" name="dspDetails.SET_heathCare" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_heathCare-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
		<td align="right" class="listwhitetext" >Provider&nbsp;notification&nbsp;sent</td>
	    <c:if test="${not empty dspDetails.SET_providerNotificationSent}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_providerNotificationSent"/></s:text>
			 <td width="70"><s:textfield id="SET_providerNotificationSent" cssClass="input-text" name="dspDetails.SET_providerNotificationSent" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_providerNotificationSent}">
		<td width="70"><s:textfield id="SET_providerNotificationSent" cssClass="input-text" name="dspDetails.SET_providerNotificationSent" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_providerNotificationSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right"><c:set var="isSET_cHall" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_cHall')>-1}">
<c:set var="isSET_cHall" value="true" />
</c:if>
<s:checkbox name="SET_cHall" id="SET_cHall" onclick="changeStatus();" value="${isSET_cHall}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >City Hall Registration</td>

	    <c:if test="${not empty dspDetails.SET_cityHall}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_cityHall"/></s:text>
			 <td><s:textfield id="SET_cityHall" cssClass="input-text" name="dspDetails.SET_cityHall" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_cityHall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_cityHall}">
		<td><s:textfield id="SET_cityHall" cssClass="input-text" name="dspDetails.SET_cityHall" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_cityHall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td width="40" align="right">	
<c:set var="isSET_utilitie" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_utilitie')>-1}">
<c:set var="isSET_utilitie" value="true" />
</c:if>
<s:checkbox name="SET_utilitie" id="SET_utilitie" onclick="changeStatus();" value="${isSET_utilitie}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Utilities</td>
	    <c:if test="${not empty dspDetails.SET_utilities}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_utilities"/></s:text>
			 <td width="70"><s:textfield id="SET_utilities" cssClass="input-text" name="dspDetails.SET_utilities" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_utilities-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_utilities}">
		<td width="70"><s:textfield id="SET_utilities" cssClass="input-text" name="dspDetails.SET_utilities" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_utilities-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
		<td align="right" class="listwhitetext" >Provider&nbsp;confirmation&nbsp;received</td>
	    <c:if test="${not empty dspDetails.SET_providerConfirmationReceived}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_providerConfirmationReceived"/></s:text>
			 <td width="70"><s:textfield id="SET_providerConfirmationReceived" cssClass="input-text" name="dspDetails.SET_providerConfirmationReceived" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_providerConfirmationReceived}">
		<td width="70"><s:textfield id="SET_providerConfirmationReceived" cssClass="input-text" name="dspDetails.SET_providerConfirmationReceived" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_providerConfirmationReceived-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td width="40" align="right"><c:set var="isSET_mConnection" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_mConnection')>-1}">
<c:set var="isSET_mConnection" value="true" />
</c:if>
<s:checkbox name="SET_mConnection" id="SET_mConnection" onclick="changeStatus();" value="${isSET_mConnection}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Media Connection</td>

	    <c:if test="${not empty dspDetails.SET_internetConn}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_internetConn"/></s:text>
			 <td><s:textfield id="SET_internetConn" cssClass="input-text" name="dspDetails.SET_internetConn" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_internetConn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_internetConn}">
		<td><s:textfield id="SET_internetConn" cssClass="input-text" name="dspDetails.SET_internetConn" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_internetConn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
		<td width="40" align="right">
<c:set var="isSET_autoRegistration" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_autoRegistration')>-1}">
<c:set var="isSET_autoRegistration" value="true" />
</c:if>
<s:checkbox name="SET_autoRegistration" id="SET_autoRegistration" onclick="changeStatus();" value="${isSET_autoRegistration}" fieldValue="true" />
</td>
		<td align="right" class="listwhitetext" >Automobile&nbsp;Registration</td>
	    <c:if test="${not empty dspDetails.SET_automobileRegistration}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_automobileRegistration"/></s:text>
			 <td width="70"><s:textfield id="SET_automobileRegistration" cssClass="input-text" name="dspDetails.SET_automobileRegistration" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_automobileRegistration-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_automobileRegistration}">
		<td width="70"><s:textfield id="SET_automobileRegistration" cssClass="input-text" name="dspDetails.SET_automobileRegistration" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_automobileRegistration-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr><configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">		
<td width="40" align="right">		
<c:set var="isSET_dLicense" value="false" />
<c:if test="${fn1:indexOf(dspDetails.reminderServices,'SET_dLicense')>-1}">
<c:set var="isSET_dLicense" value="true" />
</c:if>
<s:checkbox name="SET_dLicense" id="SET_dLicense" onclick="changeStatus();" value="${isSET_dLicense}" fieldValue="true" />
</td>
</configByCorp:fieldVisibility>
<td align="right" class="listwhitetext" >Driver's License</td>

	    <c:if test="${not empty dspDetails.SET_driverLicense}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_driverLicense"/></s:text>
			 <td><s:textfield id="SET_driverLicense" cssClass="input-text" name="dspDetails.SET_driverLicense" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_driverLicense-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_driverLicense}">
		<td><s:textfield id="SET_driverLicense" cssClass="input-text" name="dspDetails.SET_driverLicense" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="SET_driverLicense-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.hideHSRG">
		<td align="right" class="listwhitetext" >Day&nbsp;of&nbsp;service&nbsp;authorized#</td>
	    <c:if test="${not empty dspDetails.SET_dayofServiceAuthorized}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.SET_dayofServiceAuthorized"/></s:text>
			 <td width="70"><s:textfield id="SET_dayofServiceAuthorized" cssClass="input-text" name="dspDetails.SET_dayofServiceAuthorized" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="SET_dayofServiceAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.SET_dayofServiceAuthorized}">
		<td width="70"><s:textfield id="SET_dayofServiceAuthorized" cssClass="input-text" name="dspDetails.SET_dayofServiceAuthorized" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();" /></td><td><img id="SET_dayofServiceAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</configByCorp:fieldVisibility>
</tr>
<tr>
<td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.SET_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end set -->	
	<!-- start lan -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.LAN_vendorCode)>-1 && dspDetails.LAN_vendorCode !='' && dspDetails.LAN_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'LAN')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('lan')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='LAN'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="lan">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.LAN_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('LAN_','${dspDetails.LAN_vendorCodeEXSO}');chkIsVendorRedSky('LAN_');changeStatus();" /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('LAN_','${dspDetails.LAN_vendorCodeEXSO}');changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.LAN_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"   />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','LAN_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.LAN_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_serviceStartDate"/></s:text>
			 <td><s:textfield id="LAN_serviceStartDate" cssClass="input-text" name="dspDetails.LAN_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.LAN_serviceStartDate}">
		<td><s:textfield id="LAN_serviceStartDate" cssClass="input-text" name="dspDetails.LAN_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</td>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
 <c:if test="${not empty dspDetails.id}">
 <c:choose>
 <c:when test="${countDSLanguageNotes == '0' || countDSLanguageNotes == '' || countDSLanguageNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSLanguageNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsLanguage&imageId=countDSLanguageNotesImage&fieldId=countDSLanguageNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsLanguage&imageId=countDSLanguageNotesImage&fieldId=countDSLanguageNotes&decorator=popup&popup=true',800,600);" ></a></td> 
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSLanguageNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsLanguage&imageId=countDSLanguageNotesImage&fieldId=countDSLanguageNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsLanguage&imageId=countDSLanguageNotesImage&fieldId=countDSLanguageNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if> 
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.LAN_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext">Service Finish</td>
			 <c:if test="${not empty dspDetails.LAN_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_serviceEndDate"/></s:text>
			 <td><s:textfield id="LAN_serviceEndDate" cssClass="input-text" name="dspDetails.LAN_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
	         <c:if test="${empty dspDetails.LAN_serviceEndDate}">
		     <td><s:textfield id="LAN_serviceEndDate" cssClass="input-text" name="dspDetails.LAN_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.LAN_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		     </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.LAN_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.LAN_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedLAN_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.LAN_displyOtherVendorCode}">
	 <c:set var="ischeckedLAN_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.LAN_displyOtherVendorCode" disabled="true" onclick="changeStatus();" value="${ischeckedLAN_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata" ></td>
</tr>
</table>
<table width="" cellpadding="2" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext" width="" >Employee Hours/Days Authorized</td>
<td align="left" class="listwhitetext" width="65px" ><s:textfield cssClass="input-text" key="dspDetails.LAN_employeeDaysAuthorized" readonly="false" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.LAN_employeeTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
<td width="100px" align="right" class="listwhitetext"> Notification Date</td>
	    <c:if test="${not empty dspDetails.LAN_notificationDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_notificationDate"/></s:text>
			 <td width="" ><s:textfield id="LAN_notificationDate" cssClass="input-text" name="dspDetails.LAN_notificationDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:67px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.LAN_notificationDate}">
		<td width="" ><s:textfield id="LAN_notificationDate" cssClass="input-text" name="dspDetails.LAN_notificationDate" cssStyle="width:67px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="LAN_notificationDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" >Spouse Hours/Days Authorized </td>
<td align="left" class="listwhitetext" width="70px" ><s:textfield cssClass="input-text" key="dspDetails.LAN_spouseDaysAuthorized" readonly="false" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.LAN_spouseTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext" width="228px" >Children Hours/Days Authorized</td>
<td align="left" class="listwhitetext" width="" ><s:textfield cssClass="input-text" key="dspDetails.LAN_childrenDaysAuthorized" readonly="false" size="8" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.LAN_childrenTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.LAN_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end lan -->
			<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.LAN_vendorCode)<0 && dspDetails.LAN_vendorCode !='' && dspDetails.LAN_vendorCode !=null && dspDetails.LAN_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'LAN')>-1}">	
	<s:hidden name="dspDetails.LAN_vendorCode" />
<s:hidden name="dspDetails.LAN_vendorName" />
<c:if test="${not empty dspDetails.LAN_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.LAN_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.LAN_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.LAN_serviceStartDate}">
		 <s:hidden   name="dspDetails.LAN_serviceStartDate"/> 
</c:if>
<s:hidden name="dspDetails.LAN_vendorCodeEXSO" />
<s:hidden name="dspDetails.LAN_vendorContact" />
<c:if test="${not empty dspDetails.LAN_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.LAN_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.LAN_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.LAN_serviceEndDate}">
		 <s:hidden   name="dspDetails.LAN_serviceEndDate"/> 
</c:if>
<s:hidden name="dspDetails.LAN_displyOtherVendorCode" />
<s:hidden name="dspDetails.LAN_vendorEmail" />
<s:hidden name="dspDetails.LAN_paymentResponsibility" />
<s:hidden name="dspDetails.LAN_employeeDaysAuthorized" />
<s:hidden name="dspDetails.LAN_employeeTime" />
<s:hidden name="dspDetails.LAN_spouseTime" />
<c:if test="${not empty dspDetails.LAN_notificationDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.LAN_notificationDate" /></s:text>
	<s:hidden  name="dspDetails.LAN_notificationDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.LAN_notificationDate}">
		 <s:hidden   name="dspDetails.LAN_notificationDate"/> 
</c:if>
<s:hidden name="dspDetails.LAN_spouseDaysAuthorized" />
<s:hidden name="dspDetails.LAN_childrenDaysAuthorized" />
<s:hidden name="dspDetails.LAN_childrenTime" />
<s:hidden name="dspDetails.LAN_comment" />
	</c:if>
	</c:if>		
		<!-- start lan -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.LAN_vendorCode)<0 && dspDetails.LAN_vendorCode !='' && dspDetails.LAN_vendorCode !=null && dspDetails.LAN_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'LAN')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('lan')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='LAN'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="lan">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.LAN_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('LAN_','${dspDetails.LAN_vendorCodeEXSO}');chkIsVendorRedSky('LAN_');changeStatus();" /></td>
	<td align="left"width="10"> <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('LAN_','${dspDetails.LAN_vendorCodeEXSO}');changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-textUpper" key="dspDetails.LAN_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"   />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.LAN_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_serviceStartDate"/></s:text>
			 <td><s:textfield id="LAN_serviceStartDate" cssClass="input-textUpper" name="dspDetails.LAN_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.LAN_serviceStartDate}">
		<td><s:textfield id="LAN_serviceStartDate" cssClass="input-textUpper" name="dspDetails.LAN_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</td>
</tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.LAN_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext">Service Finish</td>
			 <c:if test="${not empty dspDetails.LAN_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_serviceEndDate"/></s:text>
			 <td><s:textfield id="LAN_serviceEndDate" cssClass="input-textUpper" name="dspDetails.LAN_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		     </c:if>
	         <c:if test="${empty dspDetails.LAN_serviceEndDate}">
		     <td><s:textfield id="LAN_serviceEndDate" cssClass="input-textUpper" name="dspDetails.LAN_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		     </c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.LAN_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();" /></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.LAN_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedLAN_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.LAN_displyOtherVendorCode}">
	 <c:set var="ischeckedLAN_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display to other RLO Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.LAN_displyOtherVendorCode" disabled="true" onclick="changeStatus();" value="${ischeckedLAN_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata" ></td>
</tr>
</table>
<table width="" cellpadding="2" class="detailTabLabel">
<tr>
<td align="right" class="listwhitetext" width="" >Employee Hours/Days Authorized</td>
<td align="left" class="listwhitetext" width="65px" ><s:textfield cssClass="input-textUpper" key="dspDetails.LAN_employeeDaysAuthorized" readonly="true" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.LAN_employeeTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
<td width="100px" align="right" class="listwhitetext"> Notification Date</td>
	    <c:if test="${not empty dspDetails.LAN_notificationDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.LAN_notificationDate"/></s:text>
			 <td width="" ><s:textfield id="LAN_notificationDate" cssClass="input-textUpper" name="dspDetails.LAN_notificationDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:67px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.LAN_notificationDate}">
		<td width="" ><s:textfield id="LAN_notificationDate" cssClass="input-textUpper" name="dspDetails.LAN_notificationDate" cssStyle="width:67px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" >Spouse Hours/Days Authorized </td>
<td align="left" class="listwhitetext" width="70px" ><s:textfield cssClass="input-textUpper" key="dspDetails.LAN_spouseDaysAuthorized" readonly="true" size="7" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" key="dspDetails.LAN_spouseTime" list="%{timeduration}" disabled="true" cssStyle="width:65px" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext" width="228px" >Children Hours/Days Authorized</td>
<td align="left" class="listwhitetext" width="" ><s:textfield cssClass="input-textUpper" key="dspDetails.LAN_childrenDaysAuthorized" readonly="true" size="8" maxlength="7" onchange="changeStatus();" /></td>
<td align="right" width="" class="listwhitetext"><s:select cssClass="list-menu" disabled="true" key="dspDetails.LAN_childrenTime" list="%{timeduration}" cssStyle="width:65px" onchange="changeStatus();"/></td>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea" disabled="true" rows="4" cols="40" name="dspDetails.LAN_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr><td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end lan -->
		<!-- start mmg -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.MMG_vendorCode)>-1 && dspDetails.MMG_vendorCode !='' && dspDetails.MMG_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'MMG')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('mmg')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='MMG'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="mmg">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.MMG_vendorCode" readonly="true" size="5" maxlength="10" 
onchange="checkVendorNameRelo('MMG_','${dspDetails.MMG_vendorCodeEXSO}'),chkIsVendorRedSky('MMG_'),changeStatus();"  /></td>
	<td align="left"width="10"><!-- <img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('MMG_','${dspDetails.MMG_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield cssClass="input-text" key="dspDetails.MMG_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','MMG_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.MMG_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_serviceStartDate"/></s:text>
			 <td><s:textfield id="MMG_serviceStartDate" cssClass="input-text" name="dspDetails.MMG_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MMG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MMG_serviceStartDate}">
		<td><s:textfield id="MMG_serviceStartDate" cssClass="input-text" name="dspDetails.MMG_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="MMG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSMoveMgmtNotes == '0' || countDSMoveMgmtNotes == '' || countDSMoveMgmtNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSMoveMgmtNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsMoveMgmt&imageId=countDSMoveMgmtNotesImage&fieldId=countDSMoveMgmtNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsMoveMgmt&imageId=countDSMoveMgmtNotesImage&fieldId=countDSMoveMgmtNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSMoveMgmtNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsMoveMgmt&imageId=countDSMoveMgmtNotesImage&fieldId=countDSMoveMgmtNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsMoveMgmt&imageId=countDSMoveMgmtNotesImage&fieldId=countDSMoveMgmtNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.MMG_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.MMG_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_serviceEndDate"/></s:text>
			 <td><s:textfield id="MMG_serviceEndDate" cssClass="input-text" name="dspDetails.MMG_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MMG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MMG_serviceEndDate}">
		<td><s:textfield id="MMG_serviceEndDate" cssClass="input-text" name="dspDetails.MMG_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MMG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.MMG_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.MMG_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.MMG_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedMMG_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.MMG_displyOtherVendorCode}">
	 <c:set var="ischeckedMMG_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.MMG_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedMMG_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.MMG_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>
 <c:if test="${cmmDmmAgent || networkAgent}">
 <td align="right" width="100px" class="listwhitetext">Vendor Initiation</td>
	    <c:if test="${not empty dspDetails.MMG_vendorInitiation}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_vendorInitiation"/></s:text>
			 <td><s:textfield id="MMG_vendorInitiation" cssClass="input-text" name="dspDetails.MMG_vendorInitiation" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MMG_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MMG_vendorInitiation}">
		<td><s:textfield id="MMG_vendorInitiation" cssClass="input-text" name="dspDetails.MMG_vendorInitiation" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="MMG_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</c:if>
</tr>
<tr><td></td></tr>
</tbody>
</table> 
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<table width="100%" cellpadding="2" class="detailTabLabel">
		<tr>
			<td align="left" class="vertlinedata"></td>
		</tr>
	</table>
	<table width="640" border="0" class="detailTabLabel">
	<tr>
	 <td align="right" class="listwhitetext" width="83">Origin&nbsp;Agent&nbsp;Code</td>
	 <td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
		<tr>	<td align="left" ><s:textfield cssClass="input-text" key="dspDetails.MMG_originAgentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('dspDetails.MMG_originAgentCode','dspDetails.MMG_originAgentName');changeStatus();"  /></td>
			<td align="left"width="10"></td>
			<td align="left" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.MMG_originAgentName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
		<td align="right" class="listwhitetext" width="83">Destination&nbsp;Agent&nbsp;Code</td>
		<td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
		<tr>	<td align="left" ><s:textfield cssClass="input-text" key="dspDetails.MMG_destinationAgentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('dspDetails.MMG_destinationAgentCode','dspDetails.MMG_destinationAgentName');changeStatus();"  /></td>
			<td align="left"width="10"></td>
			<td align="left" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.MMG_destinationAgentName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_originAgentPhone" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_destinationAgentPhone" readonly="false" size="58" maxlength="58" onchange=""  /></td>
	</tr>	
	
		<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_originAgentEmail" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_destinationAgentEmail" readonly="false" size="58" maxlength="58" onchange=""  /></td>

	</tr>
	<tr></tr>
	</table>
</configByCorp:fieldVisibility>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end mmg -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.MMG_vendorCode)<0 && dspDetails.MMG_vendorCode !='' && dspDetails.MMG_vendorCode !=null && dspDetails.MMG_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'MMG')>-1}">
	<s:hidden name="dspDetails.MMG_vendorCode" />
<s:hidden name="dspDetails.MMG_vendorName" />
	 <c:if test="${not empty dspDetails.MMG_serviceStartDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.MMG_serviceStartDate" /></s:text>
			 <s:hidden  name="dspDetails.MMG_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.MMG_serviceStartDate}">
		 <s:hidden   name="dspDetails.MMG_serviceStartDate"/> 
	 </c:if>
<s:hidden name="dspDetails.MMG_vendorCodeEXSO" />
<s:hidden name="dspDetails.MMG_vendorContact" />
	 <c:if test="${not empty dspDetails.MMG_serviceEndDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.MMG_serviceEndDate" /></s:text>
			 <s:hidden  name="dspDetails.MMG_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.MMG_serviceEndDate}">
		 <s:hidden   name="dspDetails.MMG_serviceEndDate"/> 
	 </c:if>
	 
	 <c:if test="${not empty dspDetails.MMG_vendorInitiation}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="dspDetails.MMG_vendorInitiation" /></s:text>
			 <s:hidden  name="dspDetails.MMG_vendorInitiation" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty dspDetails.MMG_vendorInitiation}">
		 <s:hidden   name="dspDetails.MMG_vendorInitiation"/> 
	 </c:if>
<s:hidden name="dspDetails.MMG_vendorEmail" />
<s:hidden name="dspDetails.MMG_paymentResponsibility" />
<s:hidden name="dspDetails.MMG_comment" />
<s:hidden name="dspDetails.MMG_originAgentCode" />
<s:hidden name="dspDetails.MMG_originAgentName" />
<s:hidden name="dspDetails.MMG_destinationAgentCode" />
<s:hidden name="dspDetails.MMG_destinationAgentName" />
<s:hidden name="dspDetails.MMG_originAgentPhone" />
<s:hidden name="dspDetails.MMG_destinationAgentPhone" />
<s:hidden name="dspDetails.MMG_originAgentEmail" />
<s:hidden name="dspDetails.MMG_destinationAgentEmail" />
<s:hidden name="dspDetails.MMG_displyOtherVendorCode" />
	</c:if>
	</c:if>
	<!-- start mmg displyOtherVendorCode-->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.MMG_vendorCode)<0 && dspDetails.MMG_vendorCode !='' && dspDetails.MMG_vendorCode !=null && dspDetails.MMG_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'MMG')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('mmg')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='MMG'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="mmg">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.MMG_vendorCode" readonly="true" size="5" maxlength="10" 
onchange="checkVendorNameRelo('MMG_','${dspDetails.MMG_vendorCodeEXSO}'),chkIsVendorRedSky('MMG_'),changeStatus();"  /></td>
	<td align="left"width="10"><!-- <img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('MMG_','${dspDetails.MMG_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield cssClass="input-text" key="dspDetails.MMG_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.MMG_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_serviceStartDate"/></s:text>
			 <td><s:textfield id="MMG_serviceStartDate" cssClass="input-textUpper" name="dspDetails.MMG_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MMG_serviceStartDate}">
		<td><s:textfield id="MMG_serviceStartDate" cssClass="input-textUpper" name="dspDetails.MMG_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onchange="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.MMG_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.MMG_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_serviceEndDate"/></s:text>
			 <td><s:textfield id="MMG_serviceEndDate" cssClass="input-textUpper" name="dspDetails.MMG_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MMG_serviceEndDate}">
		<td><s:textfield id="MMG_serviceEndDate" cssClass="input-textUpper" name="dspDetails.MMG_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.MMG_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.MMG_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedMMG_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.MMG_displyOtherVendorCode}">
	 <c:set var="ischeckedMMG_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display to other RLO Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.MMG_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedMMG_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td ><s:textarea cssClass="textarea" disabled="true" rows="4" cols="40" name="dspDetails.MMG_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td>
<c:if test="${cmmDmmAgent || networkAgent}">
 <td align="right" width="100px" class="listwhitetext">Vendor Initiation</td>
	    <c:if test="${not empty dspDetails.MMG_vendorInitiation}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.MMG_vendorInitiation"/></s:text>
			 <td><s:textfield id="MMG_vendorInitiation" cssClass="input-text" name="dspDetails.MMG_vendorInitiation" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="MMG_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.MMG_vendorInitiation}">
		<td><s:textfield id="MMG_vendorInitiation" cssClass="input-text" name="dspDetails.MMG_vendorInitiation" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="MMG_vendorInitiation-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</c:if>
</tr>
<tr><td></td></tr>
</tbody>
</table> 
	</div>
	</td>
	</tr>
	<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<table width="100%" cellpadding="2" class="detailTabLabel">
		<tr>
			<td align="left" class="vertlinedata"></td>
		</tr>
	</table>
	<table width="640" border="0" style="margin-left:19px" class="detailTabLabel">
	<tr>
	 <td align="right" class="listwhitetext" width="83">Origin&nbsp;Agent&nbsp;Code</td>
	 <td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
		<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_originAgentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('dspDetails.MMG_originAgentCode','dspDetails.MMG_originAgentName');changeStatus();"  /></td>
			<td align="left"width="10"></td>
			<td align="left" class="listwhitetext" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.MMG_originAgentName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
		<td align="right" class="listwhitetext" width="83">Destination&nbsp;Agent&nbsp;Code</td>
		<td align="left" colspan="1" width="365">
		<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
		<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_destinationAgentCode" readonly="false" size="5" maxlength="10" onchange="findBillingIdName('dspDetails.MMG_destinationAgentCode','dspDetails.MMG_destinationAgentName');changeStatus();"  /></td>
			<td align="left"width="10"></td>
			<td align="left" class="listwhitetext" style="padding-left:4px">
			<s:textfield	cssClass="input-text" key="dspDetails.MMG_destinationAgentName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
			</td>
		</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_originAgentPhone" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Phone</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_destinationAgentPhone" readonly="false" size="58" maxlength="58" onchange=""  /></td>
	</tr>	
	
		<tr>
		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_originAgentEmail" readonly="false" size="58" maxlength="58" onchange=""  /></td>

		<td align="right" class="listwhitetext" width="83">Agent&nbsp;Email</td>
		<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="dspDetails.MMG_destinationAgentEmail" readonly="false" size="58" maxlength="58" onchange=""  /></td>

	</tr><tr></tr>
	</table>
</configByCorp:fieldVisibility>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end mmg -->
	<!-- start ong -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.ONG_vendorCode)>-1 && dspDetails.ONG_vendorCode !='' && dspDetails.ONG_vendorCode !=null}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'ONG')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('ong')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='ONG'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="ong">
  <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.ONG_vendorCode" readonly="true" size="5" maxlength="10" 
onchange="checkVendorNameRelo('ONG_','${dspDetails.ONG_vendorCodeEXSO}'),chkIsVendorRedSky('ONG_'),changeStatus();"  /></td>
	<td align="left"width="10">  <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('ONG_','${dspDetails.ONG_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-text" key="dspDetails.ONG_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
    <img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','ONG_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.ONG_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_serviceStartDate"/></s:text>
			 <td><s:textfield id="ONG_serviceStartDate" cssClass="input-text" name="dspDetails.ONG_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_serviceStartDate}">
		<td><s:textfield id="ONG_serviceStartDate" cssClass="input-text" name="dspDetails.ONG_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onchange="changeStatus();"/></td><td><img id="ONG_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgId2"  src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSOngoingSupportNotes == '0' || countDSOngoingSupportNotes == '' || countDSOngoingSupportNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSOngoingSupportNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSOngoingSupportNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DsOngoingSupport&imageId=countDSOngoingSupportNotesImage&fieldId=countDSOngoingSupportNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.ONG_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_serviceEndDate"/></s:text>
			 <td><s:textfield id="ONG_serviceEndDate" cssClass="input-text" name="dspDetails.ONG_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_serviceEndDate}">
		<td><s:textfield id="ONG_serviceEndDate" cssClass="input-text" name="dspDetails.ONG_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.ONG_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.ONG_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedONG_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.ONG_displyOtherVendorCode}">
	 <c:set var="ischeckedONG_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.ONG_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedONG_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
<tr><td></td></tr>
</tbody>
</table>
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table>
<tr><td  align="right" class="listwhitetext">Services Assisted (1)</td><td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_dateType1" readonly="false" size="53" maxlength="150" onchange="changeStatus()"/></td>
<td align="right" class="listwhitetext" width="140">Date</td>
     <c:if test="${not empty dspDetails.ONG_date1}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_date1"/></s:text>
			 <td><s:textfield id="ONG_date1" cssClass="input-text" name="dspDetails.ONG_date1" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_date1}">
		<td><s:textfield id="ONG_date1" cssClass="input-text" name="dspDetails.ONG_date1" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date1-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td align="right" class="listwhitetext">(2)</td><td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_dateType2" readonly="false" size="53" maxlength="150" onchange="changeStatus()"/></td>
<td align="right" class="listwhitetext">Date</td>
     <c:if test="${not empty dspDetails.ONG_date2}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_date2"/></s:text>
			 <td><s:textfield id="ONG_date2" cssClass="input-text" name="dspDetails.ONG_date2" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date2-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_date2}">
		<td><s:textfield id="ONG_date2" cssClass="input-text" name="dspDetails.ONG_date2" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date2-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td align="right" class="listwhitetext">(3)</td><td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_dateType3" readonly="false" size="53" maxlength="150" onchange="changeStatus()"/></td>
<td align="right" class="listwhitetext">Date</td>
     <c:if test="${not empty dspDetails.ONG_date3}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_date3"/></s:text>
			 <td><s:textfield id="ONG_date3" cssClass="input-text" name="dspDetails.ONG_date3" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date3-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_date3}">
		<td><s:textfield id="ONG_date3" cssClass="input-text" name="dspDetails.ONG_date3" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="ONG_date3-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<tr><td align="right" class="listwhitetext">Comment</td><td colspan="3">
<s:textarea cssClass="textarea"  rows="3" cols="49" name="dspDetails.ONG_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" />
</td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end ong -->
			<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.ONG_vendorCode)<0 && dspDetails.ONG_vendorCode !='' && dspDetails.ONG_vendorCode !=null && dspDetails.ONG_displyOtherVendorCode==false}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'ONG')>-1}">
	<s:hidden name="dspDetails.ONG_vendorCode" />
<s:hidden name="dspDetails.ONG_vendorName" />
<c:if test="${not empty dspDetails.ONG_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.ONG_serviceStartDate" /></s:text>
	<s:hidden  name="dspDetails.ONG_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.ONG_serviceStartDate}">
		 <s:hidden   name="dspDetails.ONG_serviceStartDate"/> 
</c:if>
<s:hidden name="dspDetails.ONG_vendorCodeEXSO" />
<s:hidden name="dspDetails.ONG_vendorContact" />
<c:if test="${not empty dspDetails.ONG_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.ONG_serviceEndDate" /></s:text>
	<s:hidden  name="dspDetails.ONG_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.ONG_serviceEndDate}">
		 <s:hidden   name="dspDetails.ONG_serviceEndDate"/> 
</c:if>
<s:hidden name="dspDetails.ONG_displyOtherVendorCode" />
<s:hidden name="dspDetails.ONG_vendorEmail" />
<s:hidden name="dspDetails.ONG_paymentResponsibility" />
<s:hidden name="dspDetails.ONG_dateType1" />
<c:if test="${not empty dspDetails.ONG_date1}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.ONG_date1" /></s:text>
	<s:hidden  name="dspDetails.ONG_date1" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.ONG_date1}">
		 <s:hidden   name="dspDetails.ONG_date1"/> 
</c:if>
<s:hidden name="dspDetails.ONG_dateType2" />
<c:if test="${not empty dspDetails.ONG_date2}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.ONG_date2" /></s:text>
	<s:hidden  name="dspDetails.ONG_date2" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.ONG_date2}">
		 <s:hidden   name="dspDetails.ONG_date2"/> 
</c:if>
<s:hidden name="dspDetails.ONG_dateType3" />
<c:if test="${not empty dspDetails.ONG_date3}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
	<s:param name="value" value="dspDetails.ONG_date3" /></s:text>
	<s:hidden  name="dspDetails.ONG_date3" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty dspDetails.ONG_date3}">
		 <s:hidden   name="dspDetails.ONG_date3"/> 
</c:if>
<s:hidden name="dspDetails.ONG_comment" />
	</c:if>
	</c:if>
			<!-- start ong displyOtherVendorCode-->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.ONG_vendorCode)<0 && dspDetails.ONG_vendorCode !='' && dspDetails.ONG_vendorCode !=null && dspDetails.ONG_displyOtherVendorCode}">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'ONG')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('ong')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='ONG'}">
&nbsp;${entry.value}
</c:if>
</c:forEach>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="ong">
  <table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.ONG_vendorCode" readonly="true" size="5" maxlength="10" 
onchange="checkVendorNameRelo('ONG_','${dspDetails.ONG_vendorCodeEXSO}'),chkIsVendorRedSky('ONG_'),changeStatus();"  /></td>
	<td align="left"width="10">  <!--<img id="imgId1" align="left" class="openpopup" width="17" height="20" onclick="winOpenDest('ONG_','${dspDetails.ONG_vendorCodeEXSO}'),changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px">
	<s:textfield	cssClass="input-textUpper" key="dspDetails.ONG_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200" onchange="changeStatus();" />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.ONG_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_serviceStartDate"/></s:text>
			 <td><s:textfield id="ONG_serviceStartDate" cssClass="input-textUpper" name="dspDetails.ONG_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_serviceStartDate}">
		<td><s:textfield id="ONG_serviceStartDate" cssClass="input-textUpper" name="dspDetails.ONG_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onchange="changeStatus();"/></td>
		</c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.ONG_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.ONG_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_serviceEndDate"/></s:text>
			 <td><s:textfield id="ONG_serviceEndDate" cssClass="input-textUpper" name="dspDetails.ONG_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_serviceEndDate}">
		<td><s:textfield id="ONG_serviceEndDate" cssClass="input-textUpper" name="dspDetails.ONG_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.ONG_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus()"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.ONG_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedONG_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.ONG_displyOtherVendorCode}">
	 <c:set var="ischeckedONG_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display to other RLO Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.ONG_displyOtherVendorCode" disabled="true" onclick="changeStatus();" value="${ischeckedONG_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
<tr><td></td></tr>
</tbody>
</table>
<table width="100%" cellpadding="2" class="detailTabLabel">
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table>
<tr><td  align="right" class="listwhitetext">Services Assisted (1)</td><td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_dateType1" readonly="false" size="53" maxlength="150" onchange="changeStatus()"/></td>
<td align="right" class="listwhitetext" width="140">Date</td>
     <c:if test="${not empty dspDetails.ONG_date1}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_date1"/></s:text>
			 <td><s:textfield id="ONG_date1" cssClass="input-textUpper" name="dspDetails.ONG_date1" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_date1}">
		<td><s:textfield id="ONG_date1" cssClass="input-textUpper" name="dspDetails.ONG_date1" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr><td align="right" class="listwhitetext">(2)</td><td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_dateType2" readonly="false" size="53" maxlength="150" onchange="changeStatus()"/></td>
<td align="right" class="listwhitetext">Date</td>
     <c:if test="${not empty dspDetails.ONG_date2}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_date2"/></s:text>
			 <td><s:textfield id="ONG_date2" cssClass="input-textUpper" name="dspDetails.ONG_date2" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_date2}">
		<td><s:textfield id="ONG_date2" cssClass="input-textUpper" name="dspDetails.ONG_date2" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr><td align="right" class="listwhitetext">(3)</td><td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.ONG_dateType3" readonly="false" size="53" maxlength="150" onchange="changeStatus()"/></td>
<td align="right" class="listwhitetext">Date</td>
     <c:if test="${not empty dspDetails.ONG_date3}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.ONG_date3"/></s:text>
			 <td><s:textfield id="ONG_date3" cssClass="input-textUpper" name="dspDetails.ONG_date3" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.ONG_date3}">
		<td><s:textfield id="ONG_date3" cssClass="input-textUpper" name="dspDetails.ONG_date3" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<tr><td align="right" class="listwhitetext">Comment</td><td colspan="3">
<s:textarea cssClass="textarea"  rows="3" cols="49" name="dspDetails.ONG_comment" disabled="true" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" />
</td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</c:if>
	</c:if>
		<!-- end ong -->					
	<!-- start prv -->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.PRV_vendorCode)>-1 && dspDetails.PRV_vendorCode !='' && dspDetails.PRV_vendorCode !=null }">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'PRV')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('prv')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='PRV'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="prv">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.PRV_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('PRV_','${dspDetails.PRV_vendorCodeEXSO}');chkIsVendorRedSky('PRV_');changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img align="left" id="imgid1" class="openpopup" width="17" height="20" onclick="winOpenDest('PRV_','${dspDetails.PRV_vendorCodeEXSO}');changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.PRV_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'OA','PRV_');" src="<c:url value='/images/address2.png'/>" />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.PRV_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_serviceStartDate"/></s:text>
			 <td><s:textfield id="PRV_serviceStartDate" cssClass="input-text" name="dspDetails.PRV_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_serviceStartDate}">
		<td><s:textfield id="PRV_serviceStartDate" cssClass="input-text" name="dspDetails.PRV_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_serviceStartDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</td>
<c:if test="${empty dspDetails.id}">
<td  align="right" style="width:115px;!width:190px;"><img id="imgid2" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty dspDetails.id}">
<c:choose>
<c:when test="${countDSPreviewTripNotes == '0' || countDSPreviewTripNotes == '' || countDSPreviewTripNotes == null}">
<td  align="right" style="width:115px;!width:190px;"><img id="countDSPreviewTripNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Preview&imageId=countDSPreviewTripNotesImage&fieldId=countDSPreviewTripNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=Preview&imageId=countDSPreviewTripNotesImage&fieldId=countDSPreviewTripNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td  align="right" style="width:115px;!width:100px;"><img id="countDSPreviewTripNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Preview&imageId=countDSPreviewTripNotesImage&fieldId=countDSPreviewTripNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=Preview&imageId=countDSPreviewTripNotesImage&fieldId=countDSPreviewTripNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.PRV_vendorContact" readonly="false" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.PRV_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_serviceEndDate"/></s:text>
			 <td><s:textfield id="PRV_serviceEndDate" cssClass="input-text" name="dspDetails.PRV_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_serviceEndDate}">
		<td><s:textfield id="PRV_serviceEndDate" cssClass="input-text" name="dspDetails.PRV_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_serviceEndDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" onclick="setFieldName('dspDetails.PRV_serviceEndDate');" HEIGHT=20 WIDTH=20/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.PRV_vendorEmail" readonly="false" size="57" maxlength="65" onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.PRV_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedPRV_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.PRV_displyOtherVendorCode}">
	 <c:set var="ischeckedPRV_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display&nbsp;to&nbsp;other&nbsp;RLO&nbsp;Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.PRV_displyOtherVendorCode" onclick="changeStatus();" disabled="true" value="${ischeckedPRV_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel" >
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" class="detailTabLabel" >
<tr>
<td align="right" class="listwhitetext">Travel Planner Notification Date </td>
	    <c:if test="${not empty dspDetails.PRV_travelPlannerNotification}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_travelPlannerNotification"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_travelPlannerNotification" cssClass="input-text" name="dspDetails.PRV_travelPlannerNotification" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelPlannerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_travelPlannerNotification}">
		<td width="65px" ><s:textfield id="PRV_travelPlannerNotification" cssClass="input-text" name="dspDetails.PRV_travelPlannerNotification" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelPlannerNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
<td align="right" class="listwhitetext" >Orientation Days Authorized</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="dspDetails.PRV_areaOrientationDays" readonly="false" size="7" maxlength="2"  onchange="changeStatus()"/></td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Travel departure </td>
	    <c:if test="${not empty dspDetails.PRV_travelDeparture}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_travelDeparture"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_travelDeparture" cssClass="input-text" name="dspDetails.PRV_travelDeparture" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_travelDeparture}">
		<td width="65px" ><s:textfield id="PRV_travelDeparture" cssClass="input-text" name="dspDetails.PRV_travelDeparture" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelDeparture-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
<td align="right"  width="230px"  class="listwhitetext">Arrival </td>
	    <c:if test="${not empty dspDetails.PRV_travelArrival}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_travelArrival"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_travelArrival" cssClass="input-text" name="dspDetails.PRV_travelArrival" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_travelArrival}">
		<td width="65px" ><s:textfield id="PRV_travelArrival" cssClass="input-text" name="dspDetails.PRV_travelArrival" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_travelArrival-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>		
</tr>
<tr>
<c:choose>
	<c:when test="${usertype == 'ACCOUNT' and serviceOrder.job == 'RLO'}">
		<configByCorp:fieldVisibility componentId="component.accPortal.status.hideVisibility">
			<td align="right"  class="listwhitetext">Flight # </td>
	        <td width="65px" ><s:textfield id="PRV_flightNumber" cssClass="input-text" name="dspDetails.PRV_flightNumber"  cssStyle="width:65px" maxlength="20"  onfocus="changeStatus();"/></td>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.accPortal.status.hiddenVisibility">
			<td colspan="2"></td>
		</configByCorp:fieldVisibility>
	</c:when>
	<c:otherwise>
		    <td align="right"  class="listwhitetext">Flight # </td>
	        <td width="65px" ><s:textfield id="PRV_flightNumber" cssClass="input-text" name="dspDetails.PRV_flightNumber"  cssStyle="width:65px" maxlength="20"  onfocus="changeStatus();"/></td>
	</c:otherwise>
</c:choose>
<td align="right" colspan="2"  width="170px"  class="listwhitetext">Flight Arrival Time</td>
<td width="65px" ><s:textfield  cssClass="input-text" name="dspDetails.PRV_flightArrivalTime"  cssStyle="width:65px" maxlength="11"  onfocus="changeStatus();" onchange="completeTimeString();changeStatus();"/></td>
	</tr>
<tr>
<td align="right"   class="listwhitetext">Hotel Details </td>
<td colspan="5">
<s:textarea name="dspDetails.PRV_hotelDetails" cols="35" cssClass="textarea" rows="4" onchange="changeStatus()"/>
</td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Provider Notification And Services Authorized </td>
	    <c:if test="${not empty dspDetails.PRV_providerNotificationAndServicesAuthorized}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_providerNotificationAndServicesAuthorized"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_providerNotificationAndServicesAuthorized" cssClass="input-text" name="dspDetails.PRV_providerNotificationAndServicesAuthorized" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_providerNotificationAndServicesAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_providerNotificationAndServicesAuthorized}">
		<td width="65px" ><s:textfield id="PRV_providerNotificationAndServicesAuthorized" cssClass="input-text" name="dspDetails.PRV_providerNotificationAndServicesAuthorized" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_providerNotificationAndServicesAuthorized-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		
<td align="right"  class="listwhitetext"> Meet & Greet At Airport </td>
	    <c:if test="${not empty dspDetails.PRV_meetGgreetAtAirport}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_meetGgreetAtAirport"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_meetGgreetAtAirport" cssClass="input-text" name="dspDetails.PRV_meetGgreetAtAirport" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_meetGgreetAtAirport-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_meetGgreetAtAirport}">
		<td width="65px" ><s:textfield id="PRV_meetGgreetAtAirport" cssClass="input-text" name="dspDetails.PRV_meetGgreetAtAirport" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="PRV_meetGgreetAtAirport-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>		
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  rows="4" cols="40" name="dspDetails.PRV_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr> <td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end prv -->
	<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.PRV_vendorCode)<0 && dspDetails.PRV_vendorCode !='' && dspDetails.PRV_vendorCode !=null && dspDetails.PRV_displyOtherVendorCode==false }">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'PRV')>-1}">			
		<s:hidden name="dspDetails.PRV_vendorCode" />
		<s:hidden name="dspDetails.PRV_vendorName" />
		<s:hidden name="dspDetails.PRV_displyOtherVendorCode" />
		<c:if test="${not empty dspDetails.PRV_serviceStartDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.PRV_serviceStartDate" /></s:text>
		<s:hidden  name="dspDetails.PRV_serviceStartDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.PRV_serviceStartDate}">
		 <s:hidden   name="dspDetails.PRV_serviceStartDate"/> 
		</c:if>
		<s:hidden name="dspDetails.PRV_vendorCodeEXSO" />
		<s:hidden name="dspDetails.PRV_vendorContact" />
		<c:if test="${not empty dspDetails.PRV_serviceEndDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.PRV_serviceEndDate" /></s:text>
		<s:hidden  name="dspDetails.PRV_serviceEndDate" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.PRV_serviceEndDate}">
		 <s:hidden   name="dspDetails.PRV_serviceEndDate"/> 
		</c:if>
		<s:hidden name="dspDetails.PRV_vendorEmail" />
		<s:hidden name="dspDetails.PRV_paymentResponsibility" />
		<c:if test="${not empty dspDetails.PRV_travelPlannerNotification}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.PRV_travelPlannerNotification" /></s:text>
		<s:hidden  name="dspDetails.PRV_travelPlannerNotification" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.PRV_travelPlannerNotification}">
		 <s:hidden   name="dspDetails.PRV_travelPlannerNotification"/> 
		</c:if>
		<s:hidden name="dspDetails.PRV_areaOrientationDays" />
		<c:if test="${not empty dspDetails.PRV_travelDeparture}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.PRV_travelDeparture" /></s:text>
		<s:hidden  name="dspDetails.PRV_travelDeparture" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.PRV_travelDeparture}">
		 <s:hidden   name="dspDetails.PRV_travelDeparture"/> 
		</c:if>
		<c:if test="${not empty dspDetails.PRV_travelArrival}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.PRV_travelArrival" /></s:text>
		<s:hidden  name="dspDetails.PRV_travelArrival" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.PRV_travelArrival}">
		 <s:hidden   name="dspDetails.PRV_travelArrival"/> 
		</c:if>
		<s:hidden name="dspDetails.PRV_flightNumber" />
		<s:hidden name="dspDetails.PRV_flightArrivalTime" />
		<s:hidden name="dspDetails.PRV_hotelDetails" />
		<c:if test="${not empty dspDetails.PRV_providerNotificationAndServicesAuthorized}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.PRV_providerNotificationAndServicesAuthorized" /></s:text>
		<s:hidden  name="dspDetails.PRV_providerNotificationAndServicesAuthorized" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.PRV_providerNotificationAndServicesAuthorized}">
		 <s:hidden   name="dspDetails.PRV_providerNotificationAndServicesAuthorized"/> 
		</c:if>
		<c:if test="${not empty dspDetails.PRV_meetGgreetAtAirport}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="dspDetails.PRV_meetGgreetAtAirport" /></s:text>
		<s:hidden  name="dspDetails.PRV_meetGgreetAtAirport" value="%{customerFileSurveyFormattedValue}" /> 
		</c:if>
		<c:if test="${empty dspDetails.PRV_meetGgreetAtAirport}">
		 <s:hidden   name="dspDetails.PRV_meetGgreetAtAirport"/> 
		</c:if>
		<s:hidden name="dspDetails.PRV_comment" />
	</c:if>
	</c:if>
		<!-- start prv displyOtherVendorCode-->
		<c:if test="${fn1:indexOf(rloSetVenderCode,dspDetails.PRV_vendorCode)<0 && dspDetails.PRV_vendorCode !='' && dspDetails.PRV_vendorCode !=null && dspDetails.PRV_displyOtherVendorCode }">
	<c:if test="${fn1:indexOf(serviceOrder.serviceType,'PRV')>-1}">
	<tr>
	<td>
	<table border="0" cellpadding="0" cellspacing="0" style="padding-bottom: 0; margin-bottom: 0px;" width="100%">
<tr>
	<td height="0" width="100%" align="left" >		
		<div  onClick="javascript:animatedcollapse.toggle('prv')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >
<c:forEach var="entry" items="${serviceRelos}"> 
<c:if test="${entry.key=='PRV'}">
&nbsp;${entry.value}
</c:if>
</c:forEach> 
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>								
  	<div id="prv">
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83"><fmt:message key="accountLine.vendorCode" /></td>
<td align="left" width="350">
<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0"  style="margin:0px;">
<tr>
	<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="dspDetails.PRV_vendorCode" readonly="true" size="5" maxlength="10" 
	onchange="checkVendorNameRelo('PRV_','${dspDetails.PRV_vendorCodeEXSO}');chkIsVendorRedSky('PRV_');changeStatus();"  /></td>
	<td align="left"width="10"> <!--<img align="left" id="imgid1" class="openpopup" width="17" height="20" onclick="winOpenDest('PRV_','${dspDetails.PRV_vendorCodeEXSO}');changeStatus();" src="<c:url value='/images/open-popup.gif'/>" /> --></td>
	<td align="left" class="listwhitetext" style="padding-left:4px"><s:textfield	cssClass="input-text" key="dspDetails.PRV_vendorName" readonly="true" cssStyle="width:18.9em" maxlength="200"  />
	</td>
</tr>
</table>
</td>
</c:if>
<td align="right" width="100px" class="listwhitetext">Service Start</td>
	    <c:if test="${not empty dspDetails.PRV_serviceStartDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_serviceStartDate"/></s:text>
			 <td><s:textfield id="PRV_serviceStartDate" cssClass="input-textUpper" name="dspDetails.PRV_serviceStartDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_serviceStartDate}">
		<td><s:textfield id="PRV_serviceStartDate" cssClass="input-textUpper" name="dspDetails.PRV_serviceStartDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</td>
</tr>
<tr>
<c:if test="${usertype!='ACCOUNT'}">
<td align="right" class="listwhitetext" width="83">Vendor Contact</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.PRV_vendorContact" readonly="true" size="57" maxlength="100" onchange="changeStatus();" /></td>
</c:if>
<td align="right" class="listwhitetext">Service Finish</td>
	    <c:if test="${not empty dspDetails.PRV_serviceEndDate}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_serviceEndDate"/></s:text>
			 <td><s:textfield id="PRV_serviceEndDate" cssClass="input-textUpper" name="dspDetails.PRV_serviceEndDate" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_serviceEndDate}">
		<td><s:textfield id="PRV_serviceEndDate" cssClass="input-textUpper" name="dspDetails.PRV_serviceEndDate" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
</tr>
<c:if test="${usertype!='ACCOUNT'}">
<tr>
<td align="right" class="listwhitetext" width="83">Vendor Email</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.PRV_vendorEmail" readonly="true" size="57" maxlength="65" onchange="changeStatus();"/></td>
<configByCorp:fieldVisibility componentId="component.Portal.paymentResponsibility.showHSRG">
<td align="right" class="listwhitetext" >Payment&nbsp;Responsibility</td>
<td colspan="3" ><s:select cssClass="list-menu" name="dspDetails.PRV_paymentResponsibility" list="%{paymentresponsibility}" cssStyle="width:150px;" headerKey="" headerValue="" /></td>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.Portal.displyOtherVendorCode.showVOER">
<c:set var="ischeckedPRV_displyOtherVendorCode" value="false" />
    <c:if test="${dspDetails.PRV_displyOtherVendorCode}">
	 <c:set var="ischeckedPRV_displyOtherVendorCode" value="true" />
	</c:if>
   <td align="right" class="listwhitetext" width="">Display to other RLO Vendor</td>
  <td class="listwhitetext" width=""><s:checkbox key="dspDetails.PRV_displyOtherVendorCode" disabled="true" onclick="changeStatus();" value="${ischeckedPRV_displyOtherVendorCode}" fieldValue="true" tabindex="17" /></td>
</configByCorp:fieldVisibility>
</tr>
</c:if>
</tbody>
</table> 
<table width="100%" cellpadding="2" class="detailTabLabel" >
<tr>
<td align="left" class="vertlinedata"></td>
</tr>
</table>
<table border="0" class="detailTabLabel" >
<tr>
<td align="right" class="listwhitetext">Travel Planner Notification Date </td>
	    <c:if test="${not empty dspDetails.PRV_travelPlannerNotification}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_travelPlannerNotification"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_travelPlannerNotification" cssClass="input-textUpper" name="dspDetails.PRV_travelPlannerNotification" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_travelPlannerNotification}">
		<td width="65px" ><s:textfield id="PRV_travelPlannerNotification" cssClass="input-textUpper" name="dspDetails.PRV_travelPlannerNotification" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
<td align="right" class="listwhitetext" >Orientation Days Authorized</td>
<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" key="dspDetails.PRV_areaOrientationDays" readonly="true" size="7" maxlength="2"  onchange="changeStatus()"/></td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Travel departure </td>
	    <c:if test="${not empty dspDetails.PRV_travelDeparture}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_travelDeparture"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_travelDeparture" cssClass="input-textUpper" name="dspDetails.PRV_travelDeparture" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_travelDeparture}">
		<td width="65px" ><s:textfield id="PRV_travelDeparture" cssClass="input-textUpper" name="dspDetails.PRV_travelDeparture" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
		
<td align="right"  width="230px"  class="listwhitetext">Arrival </td>
	    <c:if test="${not empty dspDetails.PRV_travelArrival}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_travelArrival"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_travelArrival" cssClass="input-textUpper" name="dspDetails.PRV_travelArrival" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_travelArrival}">
		<td width="65px" ><s:textfield id="PRV_travelArrival" cssClass="input-textUpper" name="dspDetails.PRV_travelArrival" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>		
</tr>
<tr>
<c:choose>
	<c:when test="${usertype == 'ACCOUNT' and serviceOrder.job == 'RLO'}">
		<configByCorp:fieldVisibility componentId="component.accPortal.status.hideVisibility">
			<td align="right"  class="listwhitetext">Flight # </td>
	        <td width="65px" ><s:textfield id="PRV_flightNumber" cssClass="input-textUpper" name="dspDetails.PRV_flightNumber" readonly="true" cssStyle="width:65px" maxlength="20"  onfocus="changeStatus();"/></td>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.accPortal.status.hiddenVisibility">
			<td colspan="2"></td>
		</configByCorp:fieldVisibility>
	</c:when>
	<c:otherwise>
		    <td align="right"  class="listwhitetext">Flight # </td>
	        <td width="65px" ><s:textfield id="PRV_flightNumber" cssClass="input-textUpper" name="dspDetails.PRV_flightNumber" readonly="true" cssStyle="width:65px" maxlength="20"  onfocus="changeStatus();"/></td>
	</c:otherwise>
</c:choose>
<td align="right" colspan="2"  width="170px"  class="listwhitetext">Flight Arrival Time</td>
<td width="65px" ><s:textfield  cssClass="input-textUpper" name="dspDetails.PRV_flightArrivalTime" readonly="true" cssStyle="width:65px" maxlength="11"  onfocus="changeStatus();" onchange="completeTimeString();changeStatus();"/></td>
	</tr>
<tr>
<td align="right"   class="listwhitetext">Hotel Details </td>
<td colspan="5">
<s:textarea name="dspDetails.PRV_hotelDetails" cols="35" cssClass="textarea" disabled="true" rows="4" onchange="changeStatus()"/>
</td>
</tr>
<tr>
<td align="right"  class="listwhitetext">Provider Notification And Services Authorized </td>
	    <c:if test="${not empty dspDetails.PRV_providerNotificationAndServicesAuthorized}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_providerNotificationAndServicesAuthorized"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_providerNotificationAndServicesAuthorized" cssClass="input-textUpper" name="dspDetails.PRV_providerNotificationAndServicesAuthorized" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_providerNotificationAndServicesAuthorized}">
		<td width="65px" ><s:textfield id="PRV_providerNotificationAndServicesAuthorized" cssClass="input-textUpper" name="dspDetails.PRV_providerNotificationAndServicesAuthorized" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
		
<td align="right"  class="listwhitetext"> Meet & Greet At Airport </td>
	    <c:if test="${not empty dspDetails.PRV_meetGgreetAtAirport}">
			 <s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="dspDetails.PRV_meetGgreetAtAirport"/></s:text>
			 <td width="65px" ><s:textfield id="PRV_meetGgreetAtAirport" cssClass="input-textUpper" name="dspDetails.PRV_meetGgreetAtAirport" value="%{customerFileSubmissionToTranfFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>
	    <c:if test="${empty dspDetails.PRV_meetGgreetAtAirport}">
		<td width="65px" ><s:textfield id="PRV_meetGgreetAtAirport" cssClass="input-textUpper" name="dspDetails.PRV_meetGgreetAtAirport" cssStyle="width:65px" maxlength="11" readonly="true"  onfocus="changeStatus();"/></td>
		</c:if>		
</tr>
<tr><td align="right" class="listwhitetext" >Comment</td><td colspan="3"><s:textarea cssClass="textarea"  disabled="true" rows="4" cols="40" name="dspDetails.PRV_comment" onkeyup="textLimit(this,4999)" onblur="textLimit(this,4999)" onchange="changeStatus();" /> </td></tr>
<tr> <td></td></tr>
</table>
	</div>
	</td>
	</tr>
	</table>
	</td>
	</tr></c:if>
	</c:if>
		<!-- end prv -->
<jsp:include flush="true" page="dspDetailsFormSec2.jsp"></jsp:include>		
		