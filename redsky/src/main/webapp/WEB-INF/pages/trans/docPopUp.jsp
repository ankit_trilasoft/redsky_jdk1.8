<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title><fmt:message key="upload.title" /></title>
<meta name="heading" content="<fmt:message key='upload.heading'/>" />
<style type="text/css">
<style>
.blue{color:#15428B;font-family:Arial,Helvetica,sans-serif;font-size:1.25em;font-weight:bold;}
.green{color:#04b915;font-family:Arial,Helvetica,sans-serif;font-size:1.25em;font-weight:bold;}
.red{color:#b90404;font-family:Arial,Helvetica,sans-serif;font-size:1.25em;font-weight:bold;}

div#content {left:0;margin:0 auto 0px 0px; padding:0;position:relative;text-align:left;min-height: 150px;}
</style>
<script>
function closePopUp(){
	parent.window.opener.document.location.reload();
  	window.close();
}
</script>
</head>
<body style="overflow: hidden;">
<s:form id="mailForm" target="_parent" action="" method="post" validate="true" enctype="multipart/form-data" >
	
	<div id="layer1" style="border: solid; border-color: #aacefe; padding: 0; margin: 0; overflow: hidden;">
	<table border="0" width="100%" cellpadding="0" cellspacing="1"  bgcolor="#e0edff" style="margin-bottom:0px;">
		<tbody>
			<tr>
				<td height="50px"></td>
				<td></td>
			</tr>
			<tr>
				<td width="70px"></td>
				<td class="green">Cover page has been stripped from the selected PDF Document.</td>
			</tr>
			<tr>
				<td height="25px" colspan="2"></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="button"  value="Close" onclick="closePopUp();"/></td>
			</tr>
			<tr>
				<td height="50px"></td>
				<td></td>
			</tr>	
		</tbody>
	</table>
	</div>
</s:form>
</body>