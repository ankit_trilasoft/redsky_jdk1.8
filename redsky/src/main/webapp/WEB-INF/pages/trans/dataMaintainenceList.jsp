<%@ include file="/common/taglibs.jsp"%> 
<head>
	<meta name="heading" content="DataMaintainenceList"/> 
	<title>Data Maintenance List</title>
</head>
<s:form id="dataMaintainenceList" name="dataMaintainenceList" action="editDataMaintainence" method="post" validate="true">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Data Maintenance List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<display:table name="dataMaintainenceList" class="table" requestURI="" id="dataMaintainenceList" export="true" defaultsort="2" pagesize="15" style="width:95%" >
<display:column property="id" sortable="true" title="Id" href="editDataMaintainence.html" paramId="id" paramProperty="id" />
<display:column property="corpID" sortable="true" title="CorpId" />
<display:column property="description" sortable="true" title="Description" />
<display:column property="query" sortable="true" title="Query"/>
</display:table>

<input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editDataMaintainence.html"/>'"  value="<fmt:message key="button.add"/>"/>
</s:form>

<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/dataMaintainenceList.html" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>