<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head>
    <title><fmt:message key="itemsJbkEquipDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='itemsJbkEquipDetail.heading'/>"/>
	<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script>
   	  function pick() {
  		parent.window.opener.document.location.reload();
  		window.close();
	}
	</script>

    
</head>
    

<s:form id="itemsJbkEquipForm" name="itemsJbkEquipForm" action='${empty param.popup?"saveItemsJbkEquip.html?btntype=save":"saveItemsJbkEquip.html?btntype=save&decorator=popup&popup=true"}' method="post" validate="true" >   
<s:hidden name="itemsJbkEquip.id" value="%{itemsJbkEquip.id}"/> 
<s:hidden name="itemsJbkEquip.ticket" value="<%=request.getParameter("ticket") %>" />
<s:hidden name="itemsJbkEquip.flag" value="T" />
<s:hidden name="itemsJbkEquip.type" value="<%=request.getParameter("itemType") %>"  />
<c:set var="descript" value="<%=request.getParameter("descript") %>" /> 
<div id="Layer1" style="width: 750px">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Material detail</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
	<table class="mainDetailTable" cellspacing="1" cellpadding="1" border="0" style="width: 750px">
	  <tbody>
	  <tr>
	  	<td align="left" class="listwhitetext">
	  	<table class="detailTabLabel" border="0" style="width: 750px">
			  <tbody>  	
			  	<tr>
			  		<td align="left" height="3px"></td>
			  	</tr>
			  	
			  	<tr>
				  	<td align="right">Description</td>
			  		<c:if test="${empty itemsJbkEquip.descript}">
			  		<td align="left"><s:textfield name="itemsJbkEquip.descript" size="25" value="${descript}" cssClass="input-textUpper" readonly="true"/></td>
			  		</c:if>
			  		<c:if test="${not empty itemsJbkEquip.descript}">
			  		<td align="left"><s:textfield name="itemsJbkEquip.descript" size="25"  cssClass="input-textUpper" readonly="true"/></td>
			  		</c:if>
				  	<td align="right">Act Quantity</td>
			  		<td align="left"><s:textfield name="itemsJbkEquip.actualQty" cssStyle="text-align:right" maxlength="10" size="25" cssClass="input-text" /></td>
			  		<td align="right">Pack Labour</td>
			  		<td align="left"><s:textfield name="itemsJbkEquip.packLabour" cssStyle="text-align:right" maxlength="10" size="25" cssClass="input-text" /></td>
				</tr>
				
				<tr>
					<td align="right">Est Quantity</td>
			  		<td align="left"><s:textfield name="itemsJbkEquip.qty" cssStyle="text-align:right" maxlength="10" size="25" cssClass="input-text" /></td>
			  		<td align="right">Actual Cost</td>
			  		<td align="left"><s:textfield name="itemsJbkEquip.actual" cssStyle="text-align:right" maxlength="10" size="25" cssClass="input-text" /></td>
			  		<td align="right">Destination Charge</td>
			  		<td align="left"><s:textfield name="itemsJbkEquip.dtCharge" cssStyle="text-align:right" maxlength="10" size="25" cssClass="input-text" /></td>
		  		</tr>
				
				<tr>
					<td align="right">Cost</td>
			  		<td align="left"><s:textfield name="itemsJbkEquip.cost" cssStyle="text-align:right" maxlength="10" size="25" cssClass="input-text" /></td>
			  		<td align="right">Charge</td>
			  		<td align="left"><s:textfield name="itemsJbkEquip.charge" cssStyle="text-align:right" maxlength="10" size="25" cssClass="input-text" /></td>
			  		<td align="right">Origin Charge</td>
			  		<td align="left"><s:textfield name="itemsJbkEquip.otCharge" cssStyle="text-align:right" maxlength="10" size="25" cssClass="input-text" /></td>
		  		</tr>
		  		
		  		<tr>
		  			<td align="right">&nbsp;</td>
				  	<td align="left">&nbsp;</td>
			  		<td align="right">Returned Cost</td>
			  		<td align="left"><s:textfield name="itemsJbkEquip.returned" cssStyle="text-align:right" maxlength="10" size="25" cssClass="input-text" /></td>
		  		</tr>
			  		
		  		
			</tbody>
		</table>
	</td>
	</tr>
	</tbody>
	</table>
	
				
<!--<table cellspacing="0" cellpadding="6" border="0">
				<tbody>
					<tr>
						<td valign="top" align="left" class="listwhite">
						<table cellspacing="0" cellpadding="0" border="1">
							<tbody>
								<tr>
									
								</tr>
								<tr>
									<td colspan="6">
									<table cellspacing="3" cellpadding="3" border="0">
										<tbody>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td>Est Quantity</td>
												<td><s:textfield key="itemsJbkEquip.qty" required="false" cssClass="text medium"/>  </td> 
												<td>Act Quantity</td>
												<td><s:textfield key="itemsJbkEquip.actualQty" required="false" cssClass="text medium"/>  </td> 
												<td>Pack Labour</td>
												<td><s:textfield key="itemsJbkEquip.packLabour" required="false" cssClass="text medium"/></td> 
											</tr>
											<tr>
												<td>Cost</td>
												<td><s:textfield key="itemsJbkEquip.cost" required="false" cssClass="text medium"/>  </td>
												<td>Charge</td>
												<td><s:textfield key="itemsJbkEquip.charge" required="false" cssClass="text medium"/> </td> 
												<td>Destination Charge</td>
												<td><s:textfield key="itemsJbkEquip.dtCharge" required="false" cssClass="text medium"/> </td>
											</tr>
											<tr>
												<td>Actual Cost</td>
												<td><s:textfield key="itemsJbkEquip.actual" required="false" cssClass="text medium"/></td>
												<td>Returned Cost</td>
												<td><s:textfield key="itemsJbkEquip.returned" required="false" cssClass="text medium"/></td> 
											</tr>
											<tr>
												
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td>Description</td>
												<c:if test="${empty itemsJbkEquip.descript}">
												<td  colspan="2"><s:textfield key="itemsJbkEquip.descript" value="${descript}" required="false" cssClass="text large"/> </td> 
												</c:if>
												<c:if test="${not empty itemsJbkEquip.descript}">
												<td  colspan="2"><s:textfield key="itemsJbkEquip.descript" required="false" cssClass="text large"/> </td> 
												</c:if>
												</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
--></div>
<s:hidden key="itemsJbkEquip.shipNum" />
<s:hidden key="itemsJbkEquip.gl" />
<s:hidden key="itemsJbkEquip.jobNumber" />
<s:hidden key="itemsJbkEquip.seqNum" />
<s:hidden key="itemsJEquip.otCharge" />
<s:hidden key="itemsJbkEquip.billCrew" />
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>

     <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save" />
        <!--<input type="button" class="cssbutton" style="height:25px;width:110px; font-size: 15" value="Close window" onclick="pick();"/>
    -->
</s:form>
 
<script type="text/javascript"> 
try{
if(document.forms['itemsJbkEquipForm'].elements['btntype'].value=='save'){
pick();
}
}
catch(e){}
    Form.focusFirstElement($("itemsJbkEquipForm"));
</script>