package com.trilasoft.app.dao.hibernate.util;

import java.sql.Types;

import org.hibernate.Hibernate;

public class MySQLInnoDBDialect extends
		org.hibernate.dialect.MySQLInnoDBDialect {
	
	public MySQLInnoDBDialect() {
		super();
		// register additional hibernate types for default use in scalar
		// sqlquery type auto detection
		// http://opensource.atlassian.com/projects/hibernate/browse/HHH-1483
		registerHibernateType(Types.LONGVARCHAR, Hibernate.TEXT.getName());
	}

}
