<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Planning Calendar</title>
<meta name="heading" content="Print Planning Calendar" charset='utf-8'/>	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendarcustom.css'/>" /> 
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/fullcalendar/fullcalendarcustom.print.css'/>" media='print'/> 
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.9.2/themes/cupertino/jquery-ui.css" />	
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/moment.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/lib/jquerycustom.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/styles/fullcalendar/fullcalendarcustom.min.js"></script>
    <link href=‘../css/theme.css’ rel=‘stylesheet’ /> <link href=‘../css/fullcalendar.css’ rel=‘stylesheet’ /> <link href=‘../css/fullcalendar.print.css’ rel=‘stylesheet’ media=‘print’ /> <script src=‘../jquery/jquery-1.9.1.min.js’></script> <script src=’../jquery/jquery-ui-1.10.2.custom.min.js’></script> <script src=‘../scripts/fullcalendar.min.js’></script>
<script type="text/javascript">
$(document).ready(function() {
var start = '<%=request.getParameter("printStart")%>';
var end = '<%=request.getParameter("printEnd")%>';
var startDate=moment(start).format('YYYY-MM-DD');
var endDate=moment(end).format('YYYY-MM-DD');
	
	 $('#calendar').fullCalendar({
	 editable: true,
	 defaultView: '<%=request.getParameter("calendarViewStyle")%>',
	 displayEventEnd :true,
	 theme:true,
     editable: true,
     eventLimit: 30, 
        
        views: {
        agenda: { 
        	slotDuration : { days: 31 },
        columnFormat: ' D/M',
       }
    },
      eventSources: [
      {
    events: [ 
		    				<c:forEach var="entry" items="${planningCalendarViewList}" varStatus="rowCounter"> 
		    				<c:if test="${rowCounter.count>1}">
		    				,
		    				</c:if>
		    	                {
			                    title  : "${entry.title}",
			                    start  : "${entry.start}T00:00:00",
			                    end    : "${entry.end}T23:58:00",
				                color  : "#C3CCF1"
		    	                }
		    	            </c:forEach>
	    	            ]

	    	        }
	    ],
	    eventRender: function(event, element) {
	        element.find(element.find(".fc-title").html("<span style='color:#000;font-size:11px;line-height:14px; '>"+event.title+"</span>"));
	    }
	 });
	 $('#calendar').fullCalendar('gotoDate', startDate);

});
	
</script>	
</head>
</style>
<style type="text/css">

body {
		margin: 40px 10px;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

#calendar {
  max-width: 900px;
  margin: 0 auto;
}
.fc-title{ white-space: normal !important;}
.fc-time{ display: none; }
.out-of-range{
  opacity: .4;
}
.out-of-range-before{
  background: repeating-linear-gradient(
    -45deg,
    #606dbc,
    #606dbc 10px,
    #465298 10px,
    #465298 20px
  );
}
.out-of-range-after{
  background: repeating-linear-gradient(
    45deg,
    #606dbc,
    #606dbc 10px,
    #465298 10px,
    #465298 20px
  );
}
</style>


</head>

<div id="layer1" style="width:950px; margin: 0px auto;">

<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -4px"><span></span></div>
   <div class="center-content">
		<div id="calendar">
		
		</div>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<script type="text/javascript">
try{
	window.print();
}catch(e){}
</script>

