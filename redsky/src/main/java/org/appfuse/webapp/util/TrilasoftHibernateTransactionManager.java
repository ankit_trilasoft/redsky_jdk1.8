package org.appfuse.webapp.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.HibernateTransactionManager;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.transaction.TransactionDefinition;

/**
 * This class is a work-around for <a href="https://github.com/spring-projects/spring-framework/issues/14130">Spring issue 14130</a> where
 * Thread-locals are not removed when an exception occurs while a transaction is being started.  This will ensure that cleanup is properly
 * done.
 */
@SuppressWarnings("serial")
public class TrilasoftHibernateTransactionManager extends HibernateTransactionManager {
	
    protected transient final Log log = LogFactory.getLog(TrilasoftHibernateTransactionManager.class);
    
	public TrilasoftHibernateTransactionManager() {
	}

	public TrilasoftHibernateTransactionManager(SessionFactory sessionFactory) {
		super(sessionFactory);
	}


    protected void doBegin(Object transaction, TransactionDefinition definition) {
    	try {
    		super.doBegin(transaction, definition);
    	} catch (CannotCreateTransactionException e) {
    		log.error("Exception occurred while beginning transaction.  Need to cleanup properly.");
    		super.doCleanupAfterCompletion(transaction);
    		log.error("Cleanup done. Rethrow original exception.");
    		throw e;
    	}
    }
}
