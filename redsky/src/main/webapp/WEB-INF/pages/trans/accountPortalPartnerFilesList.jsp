<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<head>   
    <title><fmt:message key="myFileList.title"/></title>   
    <meta name="heading" content="<fmt:message key='myFileList.heading'/>"/> 
    
<style>  
.table thead th, .tableHeaderTable td {
background:#BCD2EF url(images/bg_listheader.png) repeat-x scroll 0 0;
border-color:#3dafcb #3dafcb #3dafcb -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(images/bg_listheader.png) repeat scroll 0%;
border-color:#3dafcb rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table th.sorted {
    background-color: #BCD2EF;
    color: #15428B;
}

.table tr.odd {
color:#000d47;
}

.table tr.even {
color:#000000;
background-color:{#e4e6f2}
}

.table tr:hover {
color:#0b0148;
background-color:{#fffcd6}
}
div#content {
padding:0px 0px; 
min-height:50px; 
margin-left:0px;
}
</style> 
    
</head>
<div id="Layer5" style="width:100%">
<s:form id="accountFileList"  name="accountFileList" action="" method="post" >
    <s:hidden name="id" value="<%=request.getParameter("sid") %>"/>
    
    <s:hidden name="partnerId" value="${partnerId}"/>
  <%--   <c:set var="partnerCode" value="${partnerCode}"/> --%>
    <c:set var="partnerId" value="${partnerId}"/>
	<c:set var="id1" value="<%= request.getParameter("id")%>" />
	<s:hidden name="id1" value="<%= request.getParameter("id")%>" />
	<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<c:set var="partnerCode" value="<%= request.getParameter("partnerCode")%>" />
	<s:hidden name="partnerCode" value="<%= request.getParameter("partnerCode")%>" />

<c:set var="lastName" value="<%= request.getParameter("lastName")%>" />
<c:set var="status" value="<%= request.getParameter("status")%>" />
<s:hidden name="lastName" value="<%= request.getParameter("lastName")%>" />
<s:hidden name="status" value="<%= request.getParameter("status")%>" />

<c:set var="id"  value="<%=request.getParameter("sid") %>"/>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session" /> 
			    <c:set var="noteID"	value="${serviceOrder.shipNumber}" scope="session" /> 
			    <c:set var="noteFor" value="ServiceOrder" scope="session" /> 
			    <c:if test="${empty serviceOrder.id}">
				<c:set var="isTrue" value="false" scope="request" />
			    </c:if> 
			    <c:if test="${not empty serviceOrder.id}">
				<c:set var="isTrue" value="true" scope="request" />
			    </c:if>
 
		<div id="newmnav">  
				<li><a href="partnerDetailsPage.html"><span>Account Detail</span></a></li> 
				<li><a href="editContractPolicy.html?id=${id1}&partnerType=${partnerType}"><span>Policy</span></a></li>                
               <%--  <c:url value="frequentlyAskedQuestionsList.html" var="url">
                    <c:param name="partnerCode" value="${partner.partnerCode}"/>
                    <c:param name="partnerType" value="AC"/>
                    <c:param name="partnerId" value="${partner.id}"/>
                    <c:param name="lastName" value="${partner.lastName}"/>
                    <c:param name="status" value="${partner.status}"/> --%>
              
                <li><a href="frequentlyAskedQuestionsList.html?partnerCode=${partnerCode}&partnerType=${partnerType}&partnerId=${id1}&lastName=${lastName}&status=${status}"><span>FAQ</span></a></li>
				<li  id="newmnav1" style="background:#FFF "><a class="current"><span>Partner File Cabinet</span></a></li> 
		</div>
	<div class="spn" >&nbsp;</div>		
<div style="!margin-top:7px;">
<display:table name="myFiles" class="table" requestURI="" id="myFileList" pagesize="25" defaultsort="2" style="width:100%;margin-top:2px;" >
   <display:column sortable="true" title="Partner Code" style="width:20px;">
    <c:out value="${ myFileList.fileId}"/> 
	</display:column>
   <display:column property="fileType" sortable="true" titleKey="myFile.fileType" style="width:108px;"/>
   <display:column property="documentCategory" sortable="true" title="Document&nbsp;Category" style="width:108px;"/>
   <display:column property="description" sortable="true" title="Description" style="width:128px;"/>
   <display:column titleKey="myFile.fileFileName"  maxLength="50" style="width:155px;"><a onclick="javascript:openWindow('ImageServletAction.html?id=${myFileList.id}&decorator=popup&popup=true',900,600);"><c:out value="${myFileList.fileFileName}" escapeXml="false"/></a></display:column>
</display:table>
</div>

</s:form>
</div>