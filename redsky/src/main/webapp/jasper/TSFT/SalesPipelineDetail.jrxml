<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="SalesPipeLineDetail"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="963"
		 pageHeight="612"
		 columnWidth="943"
		 columnSpacing="0"
		 leftMargin="2"
		 rightMargin="18"
		 topMargin="18"
		 bottomMargin="18"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="begindate" isForPrompting="true" class="java.util.Date"/>
	<parameter name="enddate" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT
	"SalesPipeLineDetail-UGMY.jrxml",
	CONCAT(app_user.`first_name`," ",app_user.`last_name`)AS Estimator,
	customerfile.`comptetive` AS customerfile_comptetive,
	customerfile.companyDivision AS customerfile_companyDivision,
	customerfile.`job` AS customerfile_Job,
	Date(customerfile.`survey`) AS customerfile_survey,
	customerfile.`salesStatus` AS customerfile_salesStatus,
	Date(customerfile.`priceSubmissionToTranfDate`) AS customerfile_priceSubmissionToTranfDate,
	Date(customerfile.`quoteAcceptenceDate`) AS customerfile_quoteAcceptenceDate,
	Date(customerfile.`moveDate`) AS customerfile_moveDate,
	CONCAT(customerfile.`lastName`,", ",customerfile.`firstName`)AS 'Customer Name',
	customerfile.`origincity` AS customerfile_origincity,
	customerfile.`originstate` AS customerfile_originstate,
	customerfile.`destinationcity` AS customerfile_destinationcity,
	customerfile.`destinationstate` AS customerfile_destinationstate,
	customerfile.`sequenceNumber` AS 'CF Number'
FROM
	`customerfile` customerfile 
	LEFT OUTER JOIN `app_user` app_user ON customerfile.`estimator` = app_user.`username` AND app_user.`corpID` = $P{Corporate ID}

WHERE
 customerfile.`corpID` = $P{Corporate ID}
	AND customerfile.`status` <>'DWNLD'
	AND (customerfile.status <> 'CNCL' OR customerfile.statusReason <> 'CANCELLED')
	AND customerfile.`survey` between $P{begindate} AND $P{enddate}
order by customerfile.companyDivision,Estimator, customerfile_comptetive, customerfile_salesStatus, customerfile_survey, customerfile_moveDate]]></queryString>

	<field name="SalesPipeLineDetail-UGMY.jrxml" class="java.lang.String"/>
	<field name="Estimator" class="java.lang.String"/>
	<field name="customerfile_comptetive" class="java.lang.String"/>
	<field name="customerfile_companyDivision" class="java.lang.String"/>
	<field name="customerfile_Job" class="java.lang.String"/>
	<field name="customerfile_survey" class="java.sql.Date"/>
	<field name="customerfile_salesStatus" class="java.lang.String"/>
	<field name="customerfile_priceSubmissionToTranfDate" class="java.sql.Date"/>
	<field name="customerfile_quoteAcceptenceDate" class="java.sql.Date"/>
	<field name="customerfile_moveDate" class="java.sql.Date"/>
	<field name="Customer Name" class="java.lang.String"/>
	<field name="customerfile_origincity" class="java.lang.String"/>
	<field name="customerfile_originstate" class="java.lang.String"/>
	<field name="customerfile_destinationcity" class="java.lang.String"/>
	<field name="customerfile_destinationstate" class="java.lang.String"/>
	<field name="CF Number" class="java.lang.String"/>

	<sortField name="Estimator" />
	<sortField name="customerfile_comptetive" />
	<sortField name="customerfile_salesStatus" />
	<sortField name="customerfile_survey" />
	<sortField name="customerfile_moveDate" />


		<group  name="estimator" >
			<groupExpression><![CDATA[$F{Estimator}]]></groupExpression>
			<groupHeader>
			<band height="20"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="59"
						y="7"
						width="0"
						height="0"
						key="staticText-110"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Static Text]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1"
						y="1"
						width="61"
						height="18"
						key="staticText-122"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<text><![CDATA[Estimator : ]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="74"
						y="1"
						width="136"
						height="18"
						forecolor="#990033"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Estimator}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="58"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="0"
						width="943"
						height="19"
						key="staticText-118"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Sales Pipeline Detail]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="75"
						y="40"
						width="65"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-123"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Survey Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="48"
						y="40"
						width="27"
						height="18"
						key="staticText-127"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Job]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="608"
						y="40"
						width="78"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-128"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Origin City]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="438"
						y="40"
						width="65"
						height="18"
						key="staticText-130"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Move Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="323"
						y="19"
						width="97"
						height="18"
						key="staticText-131"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Survey between]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="478"
						y="19"
						width="41"
						height="18"
						key="staticText-132"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[  and  ]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="dd MMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="420"
						y="19"
						width="64"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{begindate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd MMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="522"
						y="19"
						width="66"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{enddate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="869"
						y="40"
						width="74"
						height="18"
						key="staticText-133"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[CF Number]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="140"
						y="40"
						width="70"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-134"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Sales Status]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="0"
						y="40"
						width="48"
						height="18"
						key="staticText-135"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Competitive]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="210"
						y="40"
						width="130"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-136"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Price Submission Transferee]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="340"
						y="40"
						width="98"
						height="18"
						key="staticText-137"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Quote Acceptance]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="503"
						y="40"
						width="105"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-138"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Customer Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="686"
						y="40"
						width="52"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-139"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Origin State]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="738"
						y="40"
						width="78"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-140"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Destin City]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="816"
						y="40"
						width="53"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-141"/>
					<box leftPadding="1">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica" size="8" isBold="false" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Destin State]]></text>
				</staticText>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" pattern="dd MMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="75"
						y="0"
						width="65"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="2">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{customerfile_survey}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="48"
						y="0"
						width="27"
						height="18"
						key="textField"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_Job}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd MMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="438"
						y="0"
						width="65"
						height="18"
						key="textField"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{customerfile_moveDate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="608"
						y="0"
						width="78"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_origincity}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="869"
						y="0"
						width="74"
						height="18"
						key="textField"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{CF Number}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="48"
						height="18"
						key="textField-1"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_comptetive}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="140"
						y="0"
						width="70"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_salesStatus}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd MMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="210"
						y="0"
						width="130"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="2">					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{customerfile_priceSubmissionToTranfDate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="dd MMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="340"
						y="0"
						width="98"
						height="18"
						key="textField-4"
						isRemoveLineWhenBlank="true"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$F{customerfile_quoteAcceptenceDate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="503"
						y="0"
						width="105"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Customer Name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="686"
						y="0"
						width="52"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-6"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_originstate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="738"
						y="0"
						width="78"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-7"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_destinationcity}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="816"
						y="0"
						width="53"
						height="18"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-8"/>
					<box leftPadding="1"></box>
					<textElement textAlignment="Left">
						<font fontName="Arial"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_destinationstate}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="23"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="732"
						y="8"
						width="170"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="907"
						y="8"
						width="36"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="9"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd MMM yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="8"
						width="105"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="9"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="106"
						y="8"
						width="626"
						height="15"
						key="staticText-82"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font/>
					</textElement>
				<text><![CDATA[SalesPipelineDetail-UGMY.jrxml]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="1"
						y="4"
						width="941"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="1"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
