<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %> 
 

<head>

<title><fmt:message key="storageList1.title" /></title>
<meta name="heading"
	content="<fmt:message key='storageList1.heading'/>" />
<style type="text/css">
		h2 {background-color: #FBBFFF}
	</style>
</head>

<s:form id="storageForm1" action="saveStorage1" method="post" validate="true"> 
<s:hidden name="storage.id" value="%{storage.id}"/>
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="7" align="right"><img width="7" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Customer
			File</td>
			<td width="7" align="left"><img width="7" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab"><a href=serviceOrders.html>Service Orders</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Quotations</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Notes</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Reports</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Account Policy</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td align="left"></td>
		</tr>
	</tbody>
</table>
  
<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
<tr>
			<td>
			<table cellspacing="0" cellpadding="3" border="0">
			<tbody>
					<tr>	
<td>
</td>
<td>
</td>
<td class="listwhite">Number</td>
  <td class="listwhite"><s:textfield name="storage.idNum"  required="true" cssClass="text medium"/>
  </td>
  <!--  readonly="true"-->
    </tr>
    
 <tr>
<td class="listwhite">Location</td>
  <td class="listwhite"><s:textfield name="storage.locationId"  required="true" cssClass="text medium"/>
  </td>
<td  class="listwhite">Type of Work</td>
  <td class="listwhite"><s:textfield name="storage.type" required="true"   cssClass="text small"/>
  </td>
  
    </tr> 
    <tr>
    <td width="4" align="left" class="listwhite"></td>
						<td align="left" class="listwhite"></td>
						<td align="left" class="listwhite"><fmt:message key='storage.description'/></td>
							</tr>  
     
    <tr>
    <td width="4" align="left" class="listwhite"></td>
					<td width="4" align="left" class="listwhite"></td>
						<td align="left" class="listwhite"> <s:textarea  rows="4" cols="20"  key="storage.description"
						required="true" cssClass="text medium" /> </td>
					</tr>

  
   <tr>
<td>
</td>
<td align="right" class="listwhite">Container ID</td>
<td class="listwhite"><s:textfield name="storage.containerId" required="true" cssClass="text medium"/>
  </td>
<td></td>
  
  
    </tr>
    <tr>
<td>
</td>
<td align="right" class="listwhite">Model</td>
<td class="listwhite"><s:textfield name="storage.model" required="true" cssClass="text medium"/>
  </td>
<td></td>
  
  
    </tr> 
    <tr>
<td>
</td>
<td align="right" class="listwhite">Serial</td>
<td class="listwhite"><s:textfield name="storage.serial" required="true" cssClass="text medium"/>
  </td>
<td></td>
  
  
    </tr> 
    <tr>
<td>
</td>
<td align="right" class="listwhite">Weight</td>
<td class="listwhite"><s:textfield name="storage.measQuantity" required="true" cssClass="text medium"/>
  </td>
<td></td>
  
  <s:hidden name="storage.itemNumber" required="true" cssClass="text medium"/>
    </tr> 
    
     <tr>
<td>
</td>
<td align="right" class="listwhite">Item Tag</td>
<td class="listwhite"><s:textfield name="storage.itemTag" required="true" cssClass="text medium"/>
  </td>
<td></td>
  
  
    </tr> 
     <tr>
<td>
</td>
<td align="right" class="listwhite">Pieces</td>
<td class="listwhite"><s:textfield name="storage.pieces" required="true" cssClass="text medium"/>
  </td>
<td></td>
  
  
    </tr>
     <tr>
<td>
</td>
<td align="right" class="listwhite">Price</td>
<td class="listwhite"><s:textfield name="storage.price" required="true" cssClass="text medium"/>
  </td>
<td></td>
  
  
    </tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
 
  <div align="center" />
    <li class="buttonBar bottom">         
        <s:submit cssClass="button" method="save" key="button.save"/>
        
        <s:reset type="button" key="Reset"/>   
        <c:if test="${not empty storage.id}">  
            
        </c:if> 
        
    </li>
    </s:form> 
 
<script type="text/javascript"> 
    Form.focusFirstElement($("storageForm1")); 
</script> 