 <%@ include file="/common/taglibs.jsp"%> 
 <div id="otabs" style="margin-top:-30px;">
<ul>
<li><a class="current"><span> Room Details</span></a></li>
</ul>
</div>
<div class="spn"></div>
 <display:table name="inventoryRoomsList" id="inventoryRoomsList" class="table" requestURI="" export="false"  pagesize="10" style="width:98%;margin-left:10px;" >
 <display:column title="Ticket #" style="width:100px;" group="1" property="ticket"></display:column>  
		<display:column title="Location" style="width:100px;" property="location"></display:column>
		<display:column title="Rooms" property="rooms"></display:column>
		<display:column title="Floors" property="floors"></display:column>
		<display:column title="Damaged" property="hasDamage" style="width:6px;"></display:column>
		<display:column title="Comments" property="comments"></display:column>
		<display:column title="Photos" style="width:12px;"> 
			<c:if test="${inventoryRoomsList.photos!=null &&inventoryRoomsList.photos!=''}" >
				<img  src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="imageList('${inventoryRoomsList.photos}',this,'roomId')"/>
			</c:if>
		</display:column>
</display:table>