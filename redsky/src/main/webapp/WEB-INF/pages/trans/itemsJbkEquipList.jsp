<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>

<head> 
<c:if test="${itemType=='M'}"> 
    <title>Material List</title> 
</c:if>   
<c:if test="${itemType=='E'}"> 
    <title>Resource List</title> 
</c:if> 
  
    <c:if test="${itemType=='M'}"> 
	    <meta name="heading" content="Material List"/> 
	</c:if>   
	<c:if test="${itemType=='E'}"> 
	    <meta name="heading" content="Resource List"/> 
	</c:if>
	
	<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <script type="text/javascript">
		function mailtovendor1(targetElement) {
	var mailToVendorvalue = document.forms['assignItemsForm'].elements['mailtovendor'].value;
		if(document.forms['assignItemsForm'].elements['mailtovendor'].value == '' ){
			mailToVendorvalue=','+targetElement.value;
		}else{
			if(mailToVendorvalue.indexOf(targetElement.value)>=0){
				if(targetElement.value == '' ){
					mailToVendorvalue = mailToVendorvalue.replace(targetElement.value,"");
				}else{
				mailToVendorvalue = mailToVendorvalue.replace(','+targetElement.value,"");
			}
			}else{
				mailToVendorvalue = document.forms['assignItemsForm'].elements['mailtovendor'].value +','+ targetElement.value;
			}
		}
	document.forms['assignItemsForm'].elements['mailtovendor'].value = 	mailToVendorvalue;
	
	} 

	function sendEmailtoVendor() {
		//document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value = document.forms['customerFileForm'].elements['customerFile.corpID'].value + document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
		var daReferrer = document.referrer; 
		var email = document.forms['assignItemsForm'].elements['mailtovendor'].value; 
		
		
	}
		</script> 
		<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-33px;
padding:2px 0px;
text-align:right;
width:80%;
}
</style>		
</head>

<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
 <s:form id="itemsJbkEquipListForm" action="searchItemsJbkEquips" method="post" >
 <div id="Layer8" style="width:100%">
 <s:hidden name="itemType" value="<%=request.getParameter("itemType") %>" />
 <c:set var="itemType" value="<%=request.getParameter("itemType") %>" />
 <s:hidden name="wcontract" value="<%=request.getParameter("wcontract") %>" />
 <c:set var="wcontract" value="<%=request.getParameter("wcontract") %>" />
 <s:set name="materialsList" value="materialsList" scope="request"/>
  
 <!--  
 
 <table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
      <td id="searchLabelCenter" width="90"><div align="center" class="content-head">Search</div></td>
      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
      <td></td>
    </tr>
</tbody></table>
 <table class="table" width="90%" >
<thead>
<tr>
	<th><fmt:message key="itemsJEquip.type"/></th>
	<th><fmt:message key="itemsJEquip.descript"/></th>
	<th></th>
	</tr></thead>	
	<tbody>
		<tr>
			<td>
			    <s:textfield name="itemsJbkEquip.type" required="true" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="itemsJbkEquip.descript" required="true" cssClass="text medium"/>
			</td>
			<td>
			    <s:submit cssClass="button" method="search" key="button.search"/>   
			</td>
		</tr>
	</tbody>
</table>
-->  

<%-- 
<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
      <td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="130" align="center" class="detailActiveTabLabel content-tab">Assigned</td>
	<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
	
	<td width="0"></td>
      
	<!-- <td width="7" align="left"><img width="7" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
	<td width="1"></td> -->
	<td width="7" align="right"><img width="7" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
	<td width="140" align="center" class="content-tab"><a
						href="itemsJEquipsFromTkt.html?id=${workTicket.id} ">Unassigned
	</td>
	<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
	<td width="110" align="center" class="content-tab"><a href="editWorkTicketUpdate.html?id=${workTicket.id}">Work ticket</td>
	<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
    </tr>
</tbody>
</table>
<table>
<tr>
</tr>
</table>
  --%>  
<!--<s:set name="itemsJbkEquips" value="itemsJbkEquips" scope="request"/> 
	<div id="newmnav">
		    <ul>
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Assigned<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <li><a href="itemsJEquipsFromTkt.html?id=${workTicket.id}&itemType=${itemType }&wcontract=${workTicket.contract} "><span>Unassigned</span></a></li>
			  <li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div><br>
    <display:table name="itemsJbkEquips" class="table" requestURI="" id="itemsJbkEquipList" export="${empty param.popup}" pagesize="10" style="width:800px" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'> 
    <display:column style="width:40px">
	<s:checkbox name="Opt"
				fieldValue="${itemsJbkEquipList.id}"
				onclick="mailtovendor1(this);"
				 />
	</display:column>

    <display:column property="qty" sortable="true" titleKey="itemsJbkEquip.qty" paramId="id" paramProperty="id" style="width:40px"/> 
    <display:column property="actualQty" sortable="true" titleKey="itemsJbkEquip.actualQty" style="width:40px"/> 
    <display:column property="actual" sortable="true" titleKey="itemsJbkEquip.actual" style="width:40px"/> 
    <display:column property="returned" sortable="true" titleKey="itemsJbkEquip.returned" style="width:40px"/> 
    <display:column property="descript" sortable="true" titleKey="itemsJbkEquip.descript"/>
    <display:column property="charge" sortable="true" titleKey="itemsJbkEquip.charge" style="width:40px"/> 
    <display:column property="otCharge" sortable="true" titleKey="itemsJbkEquip.otCharge" style="width:40px"/>
    <display:column property="dtCharge" sortable="true" titleKey="itemsJbkEquip.dtCharge" style="width:40px"/>
    <display:column property="cost" sortable="true" titleKey="itemsJbkEquip.cost" style="width:40px"/> 
    <display:column property="packLabour" sortable="true" titleKey="itemsJbkEquip.packLabour" style="width:40px"/> 
    <display:column style="width:40px"><A onclick="javascript:openWindow('editItemsJbkEquip.html?id=${itemsJbkEquipList.id}&decorator=popup&popup=true');">Edit</A></display:column>
    
    <display:setProperty name="paging.banner.item_name" value="itemsJbkEquip"/> 
    <display:setProperty name="paging.banner.items_name" value="itemsJbkEquip"/>
    
    <display:setProperty name="export.excel.filename" value="ItemsJbkEquip List.xls"/> 
    <display:setProperty name="export.csv.filename" value="ItemsJbkEquip List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="ItemsJbkEquip List.pdf"/> 
</display:table>
 

	
</s:form>
-->
<s:form id="assignItemsForm" action="updatestatusitem" method="post">
	
	<s:hidden name="mailtovendor" />
	<s:hidden name="ticketW" value="%{workTicket.ticket}" />
  	<s:hidden name="itemType" value="<%=request.getParameter("itemType") %>" />
 	<c:set var="itemType" value="<%=request.getParameter("itemType") %>" />
 	<s:hidden name="wcontract" value="<%=request.getParameter("wcontract") %>" />
 	<c:set var="ticket" value="%{workTicket.ticket}" />
 	<c:set var="workTicketID" value="%{workTicket.id}" />
  
<%--<input type="submit" name="forwardBtn3" value="Unassign"
			class="cssbutton" style="width:70px; height:25px" onclick="sendEmailtoVendor()" />
<s:hidden name="itemsJbkEquip.id" value="%{workTicket.id}"/>
--%><c:if test="${assignItem == 'assignItems' }" >
		<c:redirect url="/itemsJbkEquips.html?id=${id}&itemType=${itemType}"/>
</c:if>
</s:form>
<script type="text/javascript">
    highlightTableRows("itemsJbkEquipList"); 
    Form.focusFirstElement($("itemsJbkEquipListForm")); 
</script> 
<script type="text/javascript">
		function search(){
		
		//  var id = document.forms['gridForm'].elements['id'].value;
		var discript = document.forms['gridForm'].elements['itemsJbkEquip.descript'].value;
		var url = "searchItemsJbkEquips.html?discript="+discript;
    	document.forms['gridForm'].action = url;
		document.forms['gridForm'].submit();
		}
function clear_fields()
{
			if(document.forms['gridForm'].elements['itemsJbkEquip.descript'].value!='')
			 {
			    document.forms['gridForm'].elements['itemsJbkEquip.descript'].value = "";
			 }
			
			
}
</script> 
<%
	//String editUrl = "<a onclick=javascript:openWindow('editItemsJbkEquip.html?id=listRow[id]&decorator=popup&popup=true');>Edit</a>";
	String editUrl = "<a onclick=javascript:void('editItemsJbkEquip.html?id=listRow[id]&decorator=popup&popup=true');>Edit</a>";
    String[][] tableMetadata = {
		{"materialsList", "itemsJbkEquip",""}, 	
      { "id", "descript","qty","returned","actualQty","cost","actual"},
      { "long", "text", "int", "float","int", "float", "float"},
      { "readonly", "editable", "editable", "editable","editable","editable","editable"},
      
    };

    request.setAttribute("tableMetadata", tableMetadata);
    
    
%>

<s:form id="gridForm" name="gridForm" action="saveItemsJbkEquipList" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="listData" />
	<s:hidden name="listFieldNames" />	
	<s:hidden name="listFieldEditability" />	
	<s:hidden name="listFieldTypes" />		
	<s:hidden name="listIdField" value="id"/>	
	<s:hidden name="listIdFieldType" value="long"/>	
	<s:hidden name="showAdd" value="Yes" />			
	<s:hidden name="ticket" value="${ticket}" />
	<s:hidden name="workTicketID" value="${workTicketID}" />
	<s:hidden name="itemType" value="<%=request.getParameter("itemType") %>"/>
	<s:hidden name="descript" value="${materialsList}" />
	
	<s:hidden name="itemsJbkEquip.id" value="%{workTicket.id}"/>
    <s:hidden name="wcontract" value="<%=request.getParameter("wcontract") %>" />
	<c:set var="sid" value="<%=request.getParameter("sid") %>" />
	<s:hidden name="sid" value="${sid}" />
	<c:set var="id" value="<%=request.getParameter("id") %>" scope ="session"/>
	<s:hidden name="id"  value="${id}"/>
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
	</configByCorp:fieldVisibility>
<!--<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.serviceorder' }">
	<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accounting' }">
	<c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.forwarding' }">
	<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.domestic' }">
	<c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.status' }">
	<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.billing' }">
	<c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.claims' }">
	<c:redirect url="/claims.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.customerfile' }">
	<c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.workticket' }">
	<c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.material' }">
	<c:redirect url="/itemsJbkEquips.html?id=${workTicket.id}&itemType=M&sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.equipment' }">
	<c:redirect url="/itemsJbkEquips.html?id=${workTicket.id}&itemType=E&sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.crew' }">
	<c:redirect url="/workTicketCrews.html?id=${workTicket.id}&itemType=M&sid=${serviceOrder.id}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.storage' }">
	<c:redirect url="/bookStorages.html?id=${workTicket.id}"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<td>
<div id="newmnav" style="float: left">
		    <ul>
		    <sec-auth:authComponent componentId="module.tab.workTicket.serviceorderTab">
		      	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.serviceorder');return IsValidTime('none');"><span>S/O Details</span></a></li>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.workTicket.billingTab"> 
			  	<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.billing');return IsValidTime('none');"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			 </sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.workTicket.accountingTab">
				  <c:choose>
				     <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
				       <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
				     </c:when>
				     <c:otherwise> 
			           <li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.accounting');return IsValidTime('none');"><span>Accounting</span></a></li> 
			         </c:otherwise>
			      </c:choose> 
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.workTicket.forwardingTab">
			  		<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.forwarding');return IsValidTime('none');"><span>Forwarding</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.domesticTab">
				  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
				  	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.domestic');return IsValidTime('none');"><span>Domestic</span></a></li>
				  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.statusTab">
			  	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.status');return IsValidTime('none');"><span>Status</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.ticketTab">
			  	<li  id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  	<li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.claims');return IsValidTime('none');"><span>Claims</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.customerFileTab">
			 	 <li><a onmouseover="assigntimeairthmaticNew('save');assigntimeairthmaticNew12('save');"onclick="completeTimeString();setReturnString('gototab.customerfile');return IsValidTime('none');"><span>Customer File</span></a></li>
			  </sec-auth:authComponent>

		     </ul>
		</div>
	-->

<div id="newmnav">
		  
		  
		    <ul>
		    <sec-auth:authComponent componentId="module.tab.workTicket.serviceorderTab">
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			</sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.billingTab"> 
			  	<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.accountingTab">
			  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
   	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
			  <sec-auth:authComponent componentId="module.tab.workTicket.forwardingTab">
			  <c:if test="${forwardingTabVal!='Y'}"> 
	   				<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
	  			<c:if test="${forwardingTabVal=='Y'}">
	  				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.domesticTab">
			  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
              <sec-auth:authComponent componentId="module.tab.workTicket.statusTab">
			   <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			</sec-auth:authComponent>	
			<sec-auth:authComponent componentId="module.tab.workTicket.ticketTab">			  
			  <li id="newmnav1" style="background:#FFF"><a class="current""><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  </sec-auth:authComponent>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </sec-auth:authComponent>
			  </configByCorp:fieldVisibility>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.workTicket.customerFileTab">
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			  </sec-auth:authComponent>
			  <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
			</ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div class="spn">&nbsp;</div>
	
	
<table width="100%" style="margin:0px;"><tr><td style="margin:0px;"><%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%></td></tr></table>
</td>		
		
<table width="100%" style="margin:0px;"><tr><td style="margin:0px;">		
<div id="newmnav">
		  <ul>
		    <li><a href="editWorkTicketUpdate.html?id=${id}"><span>Work Ticket</span></a></li>
		    <c:if test="${itemType =='M'}">
		    	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Material</span></a></li>
		    	<sec-auth:authComponent componentId="module.tab.workTicket.equipmentTab">
		    	<li><a href="itemsJbkResourceList.html?id=${id}&itemType=E&sid=${sid}"><span>Resource</span></a></li>
		    	</sec-auth:authComponent>
			</c:if>
			<c:if test="${itemType =='E'}">
				<li><a href="itemsJbkEquips.html?id=${id}&itemType=M&sid=${sid}"><span>Material</span></a></li>
				<li id="newmnav1" style="background:#FFF"><a class="current"><span>Resource</span></a></li>
			</c:if>
		    <li><a href="workTicketCrews.html?id=${id}&sid=${sid}"><span>Crew</span></a></li>
		    <li><a href="truckingOperationsList.html?ticket=${workTicket.ticket}&sid=${serviceOrder.id}&tid=${workTicket.id}"><span>Truck</span></a></li>
		    <configByCorp:fieldVisibility componentId="component.tab.workTicket.whseMgmtTab">
		    <li><a href="bookStorages.html?id=${id} "><span>Whse/Mgmt</span></a></li>
		    </configByCorp:fieldVisibility>
		    <sec-auth:authComponent componentId="module.tab.workTicket.formsTab">
			<li><a onclick="window.open('subModuleReports.html?id=${workTicket.id}&jobNumber=${serviceOrder.shipNumber}&noteID=${workTicket.ticket}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
			</sec-auth:authComponent>
  		</ul><div class="spn">&nbsp;</div>
</div>
</td></tr></table>
		
		
		
		
<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top" style="margin-top:0px;!margin-top: 0px; "><span></span></div>
    <div class="center-content">
<table><tr>
		<td colspan="6">
		<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0">
			<tbody>
				<tr>
					<td align="right" height="30" class="listwhitetext"><fmt:message key='workTicket.ticket' /></td>
					<td align="left" ><s:textfield name="workTicket.ticket"  cssClass="input-textUpper" size="10" readonly="true" /></td>
					<td align="right" class="listwhitetext"><fmt:message key="workTicket.service" /></td>
					<td colspan="3"><s:textfield name="workTicket.service" cssClass="input-textUpper" size="10" readonly="true"/></td>
					<td align="right" class="listwhitetext">Warehouse</td> 
					<td colspan="3"><s:textfield name="workTicket.warehouse1"  value="${wrehouseDesc}" cssClass="input-textUpper" size="50" readonly="true"/></td>
				</tr>
				</tbody>
				</table>
				</td>
				</tr></table></div>
<div class="bottom-header"><span></span></div>
</div>
</div> 								
		
		
		

	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
</table>
<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top" style="margin-top:0px;!margin-top: 0px; "><span></span></div>
    <div class="center-content">
 <table class="table" width="97%" >
		<thead>
			<tr>
				<th><fmt:message key="itemsJEquip.descript"/></th>
				<th></th>
			</tr>
		</thead>	
			<tbody>
				<tr>
				<td>
					    <s:textfield name="itemsJbkEquip.descript" required="true" cssClass="text medium"/>
					</td>
					<td>
					    <input type="button"  class="cssbutton1" value="Search" style="!margin-bottom: 15px;"  onclick="search()"/>   
					    <input type="button"  class="cssbutton1" value="Clear" style="!margin-bottom: 15px;"  onclick="clear_fields();"/>  
					</td>  
				    
				</tr>
		</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>

</s:form>


<%@ include file="/common/enable-dojo-grid.jsp" %>
