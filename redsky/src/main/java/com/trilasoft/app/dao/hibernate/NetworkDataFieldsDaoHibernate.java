package com.trilasoft.app.dao.hibernate;


import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.NetworkDataFieldsDao;
import com.trilasoft.app.model.NetworkDataFields;
import com.trilasoft.app.model.RefMaster;

public class NetworkDataFieldsDaoHibernate extends GenericDaoHibernate<NetworkDataFields,Long> implements NetworkDataFieldsDao{
	public NetworkDataFieldsDaoHibernate() 
    {   
        super(NetworkDataFields.class);   
    
    }
	 public List findForCombo1Map() {
		List list;
		list = getHibernateTemplate().find("select description from RefMaster where parameter='TABLE' order by description");
  	return list;
	}
	public List search(String modelName, String fieldName,String types,String tranType) {
		List list;
		//String query = "select * from networkdatafields where modelName like '%"+ modelName+ "%' AND fieldName like '%"+ fieldName+"%' AND type like '%"+ types+ "%' AND transactionType like '%"+tranType+"%'";
		list = getHibernateTemplate().find("from NetworkDataFields where modelName like '%"+ modelName+ "%' AND fieldName like '%"+ fieldName+"%' AND type like '%"+ types+ "%' AND transactionType like '%"+tranType+"%'");
		return list;
	}
	
	public List getFieldList(String modelName){
		try{
			//String query = "SELECT distinct(TRIM(fieldName))as fieldName from DataCatalog where tableName="+"'"+modelName+"'"+"AND auditable='true' order by fieldName asc";
			return getHibernateTemplate().find("SELECT distinct(TRIM(fieldName))as fieldName from DataCatalog where tableName="+"'"+modelName+"'"+"AND auditable='true' order by fieldName asc ");
		}catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	
	public boolean checkDuplicateRecord(String modelName, String fieldName, String transactionType){
		boolean flag = false;
		List checkRecordList = new ArrayList();
		try{
			//String query="SELECT * FROM networkdatafields where modelName='CustomerFile' and fieldName='lastName' and transactionType='Update'";
			String query="SELECT * FROM networkdatafields where modelName='"+modelName+"' and fieldName='"+fieldName+"' and transactionType='"+transactionType+"'";
			checkRecordList = this.getSession().createSQLQuery(query).list();
			if(checkRecordList.size()<1){
				flag = true;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return flag;
	}
}
