package com.trilasoft.app.dao;


import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.FreightMarkUpControl;


public interface FreightMarkUpDao extends GenericDao<FreightMarkUpControl, Long>{
	
	 public List checkById(Long id);
  
}
