<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="customerFileNotesDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='customerFileNotesDetail.heading'/>"/>  
    <link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>
    <s:head theme="ajax" />
	<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
	<script type="text/javascript">   
    	Form.focusFirstElement($("remindMeForm"));   
	</script> 
</head>
<table>
<tr>
<td>
<s:form id="mailToUserForm">       
	<input type="button" value="Email" onclick="emailForm();" />     
</s:form>
</td>
</tr>
</table>