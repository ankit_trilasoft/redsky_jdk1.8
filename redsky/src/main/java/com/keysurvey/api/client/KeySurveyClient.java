package com.keysurvey.api.client;

 

import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

 

import javax.xml.rpc.ServiceException;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import sun.reflect.ReflectionFactory.GetReflectionFactoryAction;

 

import com.keysurvey.api.v68.form.design.FormDesignManagementLocator;

import com.keysurvey.api.v68.form.design.FormDesignManagementServicePortBindingStub;

import com.keysurvey.api.v68.form.result.FormResultManagementLocator;

import com.keysurvey.api.v68.form.result.FormResultManagementServicePortBindingStub;

import com.keysurvey.api.v68.form.result.WSAnswerPickResponse;

import com.keysurvey.api.v68.form.result.WSAnswerResponse;

import com.keysurvey.api.v68.form.result.WSAnswerTextResponse;

import com.keysurvey.api.v68.form.result.WSFormStatistics;

import com.keysurvey.api.v68.form.result.WSQuestionResponse;

import com.keysurvey.api.v68.form.result.WSRespondent;
import java.sql.ResultSet;
import com.trilasoft.app.model.KeySurveyQuestionMap;
import com.trilasoft.app.model.SurveyResponse;
import com.trilasoft.app.model.SurveyResponseDtl;
import com.trilasoft.app.service.KeySurveyQuestionMapManager;
import com.trilasoft.app.service.SurveyResponseDtlManager;
import com.trilasoft.app.service.SurveyResponseManager;

 

public class KeySurveyClient extends BaseAction {
	static Statement statement;
	static Connection conCF;
	public static ResourceBundle resourceBundle;
	private static String dbUrl;
	private static String db_User;
	private static String db_Password;
	
                
	private SurveyResponse surveyResponse;
	private SurveyResponseManager surveyResponseManager;
	private SurveyResponseDtl surveyResponseDtl;	
	private SurveyResponseDtlManager surveyResponseDtlManager;
	private KeySurveyQuestionMap keySurveyQuestionMap;
	private KeySurveyQuestionMapManager keySurveyQuestionMapManager;
	private String sessionCorpID;
	private String remoteUser;
                private String userName = null;
                private String password = null;
                public KeySurveyClient()
                {
                	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                    User user = (User)auth.getPrincipal();
                    this.sessionCorpID=user.getCorpID();
                    this.remoteUser=user.getUsername();
                }
                public Date sysdate()
                {
                	return new Date();
                }
                public  void callKeySurveyWS() throws RemoteException,

                                                ServiceException {

                                

                                FormDesignManagementLocator designManagementLocator = new FormDesignManagementLocator();

                                FormDesignManagementServicePortBindingStub bindingStubDes = (FormDesignManagementServicePortBindingStub) designManagementLocator.getFormDesignManagementServicePort();

                                bindingStubDes.setPortName(designManagementLocator.getFormDesignManagementServicePortAddress());

                                bindingStubDes.setUsername("w.heijden@uts-intl.com");

                                bindingStubDes.setPassword("Quality2010!");

                                

                                FormResultManagementLocator formResultManagementLocator = new FormResultManagementLocator();

                                System.out.println(formResultManagementLocator.getFormResultManagementServicePortAddress());

                                FormResultManagementServicePortBindingStub bindingStub = (FormResultManagementServicePortBindingStub) formResultManagementLocator.getFormResultManagementServicePort();

                                bindingStub.setPortName(formResultManagementLocator.getFormResultManagementServicePortAddress());

                                bindingStub.setUsername("w.heijden@uts-intl.com");

                                bindingStub.setPassword("Quality2010!");
                                //String surveyID[]={"265842","308624","340829","353780","382757","407666"};

                                int surveyID = 407666;
                                WSFormStatistics formStatistics = bindingStub.getFormStatistics(surveyID);

                                WSRespondent[] wsRespondents = bindingStub.getRespondents(surveyID);
								String shipNumber="";
								String recommend="";
								String destination="";
								String origin="";                                        											
								String preMove="";
								String transferre="";
								String overAll="";
								Date sentDate=null;
								Date sentDate1=null;
                                System.out.println("==============Printing Survey Results For "+surveyID+"==================");
                                for(WSRespondent wsRespondent: wsRespondents){
                                                System.out.println("email: "+wsRespondent.getEmail());
                                                System.out.println("score: "+wsRespondent.getScore());
                                                System.out.println("status: "+wsRespondent.getStatus());
                                                System.out.println("code: "+wsRespondent.getCode());
                                                System.out.println("form id: "+wsRespondent.getFormId());
                                                System.out.println("respondent id: "+wsRespondent.getRespondentId());
                                                System.out.println("submitted date: "+wsRespondent.getSubmitDate().getTime());
                                                
                                    			//surveyResponse = new SurveyResponse();
                                    			Long surveyI=Long.parseLong(surveyID+"");
                                    			String status=wsRespondent.getStatus()+"";
                                    			String score =wsRespondent.getScore()+"";
                                    			Date submitDate =wsRespondent.getSubmitDate().getTime();
                                    			
                                    			StringBuilder sendActualToClientDates=null;
                                    			try{
                                    				
                                    				SimpleDateFormat sendActualToClient = new SimpleDateFormat("yyyy-MM-dd");
                                    	            sendActualToClientDates = new StringBuilder(sendActualToClient.format(submitDate)); 
                                    			}catch(Exception e){} 
                                    			
                                    			Date n=new Date();
                                    			String flag="0";
                                    			String query9 = "select qualitySurveyId from surveyresponse where qualitySurveyId="+ surveyI;
                                    		try {
                                    			statement = conCF.createStatement();
                                    			ResultSet rs = statement.executeQuery(query9);
                                    			while (rs.next()) {
                                    				flag="1";
                                    			}
                                    			statement.close();
                                    		}catch(Exception e){}
                                    		String query ="";
                                    		    if(flag.equalsIgnoreCase("1"))
                                    		    {
                                   		    	 query="update surveyresponse set qualitySurveyId="+surveyI+",status='"+status+"',score='"+score+"',submittedDate='"+sendActualToClientDates.toString()+"',corpId='"+sessionCorpID+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now() where qualitySurveyId='"+surveyI+"'";
                                    		    }else{
        										 query="insert into surveyresponse (id,qualitySurveyId,status,score,submittedDate,corpId,createdBy,createdOn,updatedBy,updatedOn)values(NULL, " +
        										surveyI+",'"+status+"','"+score+"','"+sendActualToClientDates.toString()+"','"+sessionCorpID+"','"+getRequest().getRemoteUser()+"',now(),'"+getRequest().getRemoteUser()+"',now())";
                                    		    }
        										try
        										{
        										statement = conCF.createStatement();
        										int val = statement.executeUpdate(query);
        										statement.close();
        										}catch(Exception e){}
                                                WSQuestionResponse[] responses = bindingStub.getResponses(wsRespondent.getRespondentId());
                                                for(WSQuestionResponse response: responses){
                                                    System.out.println("QuestionId: "+response.getQuestionId());
                                                    System.out.println("RespondentId: "+response.getRespondentId());
                                                    System.out.println("VersionId: "+response.getVersionId());
                                                    
                                                                System.out.println(" Question: "+response.getQuestionId()+" "+bindingStubDes.getQuestion(response.getQuestionId(), true).getText());
                                                                WSAnswerResponse[] answers = response.getAnswerResponses();
                                                                
                                                                for(WSAnswerResponse answer:answers){
                                                					String kType="";
                                                                                if(answer instanceof WSAnswerTextResponse){
                                                                                	
                                                                					kType="TXT";
                                                                                                System.out.println("  Answer: "+((WSAnswerTextResponse) answer ).getText()+" "+bindingStubDes.getAnswer(answer.getAnswerId()).getTitle());

                                                                                }
                                                                                if(answer instanceof WSAnswerPickResponse){
                                                                                	
                                                                                	kType="VAL";
                                                                                                System.out.println("  Answer: "+((WSAnswerPickResponse) answer ).getValue()+" "+bindingStubDes.getAnswer(answer.getAnswerId()).getTitle());
                                                                                }
                                                                                System.out.println("Answer ID:----------->"+answer.getAnswerId());
                                                                    			String flag1="0";
                                                                    			String query10 = "select surveyQuestionId from keysurveyquestionmap where surveyQuestionId="+ response.getQuestionId();
                                                                    		try {
                                                                    			statement = conCF.createStatement();
                                                                    			ResultSet rs = statement.executeQuery(query10);
                                                                    			while (rs.next()) {
                                                                    				flag1="1";
                                                                    			}
                                                                    			statement.close();
                                                                    		}catch(Exception e){}
                                                                    		String query2 ="";
                                                                    		    if(flag1.equalsIgnoreCase("1"))
                                                                    		    {
                                                                   		    	 query2="update keysurveyquestionmap set surveyQuestionId="+response.getQuestionId()+",questionText='"+bindingStubDes.getQuestion(response.getQuestionId(), true).getText()+"',type='"+kType+"',corpId='"+sessionCorpID+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now() where surveyQuestionId="+response.getQuestionId()+"";
                                                                    		    }else{
                                              										query2 = "insert into keysurveyquestionmap (id,surveyQuestionId,questionText,type,corpId,createdBy,createdOn,updatedBy,updatedOn)values(NULL, " +
                                              										response.getQuestionId()+",'"+bindingStubDes.getQuestion(response.getQuestionId(), true).getText()+"','"+kType+"','"+sessionCorpID+"','"+getRequest().getRemoteUser()+"',now(),'"+getRequest().getRemoteUser()+"',now())";
                                                                    		    }
                                        										try
                                        										{
                                        											statement = conCF.createStatement();
                                        										int val = statement.executeUpdate(query2);
                                        										statement.close();
                                        										}catch(Exception e){
                                        											
                                        										}
                                        										
                                        										
                                        										
                                        										
                                        										
                                        										
                                                                    			String flag2="0";
                                                                    			String query11 = "select surveyResponseId from surveyresponsedtl where questionId="+response.getQuestionId()+" and answerId="+answer.getAnswerId();
                                                                    		try {
                                                                    			statement = conCF.createStatement();
                                                                    			ResultSet rs = statement.executeQuery(query11);
                                                                    			while (rs.next()) {
                                                                    				flag2="1";
                                                                    			}
                                                                    			statement.close();
                                                                    		}catch(Exception e){}
                                                                    		String query1 ="";
                                                                    		    if(flag2.equalsIgnoreCase("1"))
                                                                    		    {
                                                                   		    	 query1="update surveyresponsedtl set surveyResponseId="+response.getRespondentId()+",questionId="+response.getQuestionId()+",answerId="+answer.getAnswerId()+",answer='"+bindingStubDes.getAnswer(answer.getAnswerId()).getTitle()+"',answerVersion="+response.getVersionId()+",submittedDate='"+sendActualToClientDates.toString()+"',corpId='"+sessionCorpID+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now() where surveyResponseId="+response.getRespondentId()+",questionId="+response.getQuestionId()+",answerId="+answer.getAnswerId()+"";
                                                                    		    }else{																																																																																																										
                                              										query1 = "insert into surveyresponsedtl (id,surveyResponseId,questionId,answerId,answer,answerVersion,submittedDate,corpId,createdBy,createdOn,updatedBy,updatedOn)values(NULL, " +
                                              										response.getRespondentId()+","+response.getQuestionId()+","+answer.getAnswerId()+",'"+bindingStubDes.getAnswer(answer.getAnswerId()).getTitle()+"',"+response.getVersionId()+",'"+sendActualToClientDates.toString()+"','"+sessionCorpID+"','"+getRequest().getRemoteUser()+"',now(),'"+getRequest().getRemoteUser()+"',now())";
                                                                    		    }
                                        										try
                                        										{
                                        											statement = conCF.createStatement();
                                        										int val = statement.executeUpdate(query1);
                                        										statement.close();
                                        										}catch(Exception e){}
                                        										String formId=wsRespondent.getFormId()+"";
                                        										if(formId.equalsIgnoreCase("407666"))
                                        										{
                                        											
                                        											String answerId=answer.getAnswerId()+"";
                                        											if(answerId.equalsIgnoreCase("47127768"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                                            shipNumber=((WSAnswerTextResponse) answer ).getText();
                                                                                            }
                                                                                            if(answer instanceof WSAnswerPickResponse){
                                                                                                            shipNumber=((WSAnswerPickResponse) answer ).getValue()+"";
                                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("47128010"))
                                        											{
	                                                                                        if(answer instanceof WSAnswerTextResponse){
	                                                                                        	transferre=((WSAnswerTextResponse) answer ).getText();
				                                                                            }
				                                                                            if(answer instanceof WSAnswerPickResponse){
				                                                                            	transferre=((WSAnswerPickResponse) answer ).getValue()+"";
				                                                                            }
                                        												
                                        											}
                                        											if(answerId.equalsIgnoreCase("47128068"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	recommend=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	recommend=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("47127452"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	destination=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	destination=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("47127800"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	origin=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	origin=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("47127515"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	overAll=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	overAll=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("47128038"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	preMove=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	preMove=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                                                        			String flag5="0";
                                                                        			String flag6="0";
                                                                        			String query15 = "select shipNumber from serviceorder where shipNumber='"+shipNumber+"'";
                                                                        			String query25="";
                                                                        		try {
		                                                                        			if(!shipNumber.equalsIgnoreCase(""))
		                                                                        			{
			                                                                        			statement = conCF.createStatement();
			                                                                        			ResultSet rs = statement.executeQuery(query15);
			                                                                        			while (rs.next()) {
			                                                                        				flag5="1";
			                                                                        				query25 = "select lastSent from surveyemailaudit where shipNumber='"+shipNumber+"'";
			                                                                        			}
			                                                                        			statement.close();
			                                                                        			if(!query25.equalsIgnoreCase(""))
			                                                                        			{
			                                                                            			statement = conCF.createStatement();
			                                                                            			ResultSet rs1 = statement.executeQuery(query25);
			                                                                            			while (rs1.next()) {
			                                                                            				flag6="1";
			                                                                            				sentDate=rs1.getDate(1);
			                                                                            				
			                                                                            			}
			                                                                            			statement.close();
			                                                                        			}
			                                                                        			String query115 = "select sentDate from serviceorder where shipNumber='"+shipNumber+"'";
			                                                                        			statement = conCF.createStatement();
			                                                                        			ResultSet rs115 = statement.executeQuery(query115);
			                                                                        			while (rs115.next()) {
			                                                                        				sentDate1=rs115.getDate(1);
			                                                                        				
			                                                                        			}
			                                                                        			statement.close();
		                                                                        			}
                                                                        		    }catch(Exception e){}
                                                                        		
                                                                        		String query16 ="";
                                                                        		    if(flag5.equalsIgnoreCase("1"))
                                                                        		    {
                                                                        		    	if(sentDate1==null)
                                                                        		    	{
                                                                        		    	query16="update serviceorder set preMoveSurvey='"+preMove+"',originServices='"+origin+"',destinationServices='"+destination+"',overAll='"+overAll+"',recommendation="+recommend+",sentDate='"+sentDate+"',transferee='"+transferre+"',returnDate='"+sendActualToClientDates+"' where shipNumber='"+shipNumber+"'";
                                                                        		    	}else{
                                                                       		    		query16="update serviceorder set preMoveSurvey='"+preMove+"',originServices='"+origin+"',destinationServices='"+destination+"',overAll='"+overAll+"',recommendation="+recommend+",transferee='"+transferre+"',returnDate='"+sendActualToClientDates+"' where shipNumber='"+shipNumber+"'";
                                                                        		    	}
                                                                        		    }
                                            										try
                                            										{
                                            											statement = conCF.createStatement();
                                            											if(!query16.equalsIgnoreCase(""))
                                            											{
                                            												int val = statement.executeUpdate(query16);
                                            											}
                                            										statement.close();
                                            										}catch(Exception e){}
                                        										}else if(formId.equalsIgnoreCase("308624")){

                                        											
                                        											String answerId=answer.getAnswerId()+"";
                                        											if(answerId.equalsIgnoreCase("33161737"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                                            shipNumber=((WSAnswerTextResponse) answer ).getText();
                                                                                            }
                                                                                            if(answer instanceof WSAnswerPickResponse){
                                                                                                            shipNumber=((WSAnswerPickResponse) answer ).getValue()+"";
                                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("33162069"))
                                        											{
	                                                                                        if(answer instanceof WSAnswerTextResponse){
	                                                                                        	transferre=((WSAnswerTextResponse) answer ).getText();
				                                                                            }
				                                                                            if(answer instanceof WSAnswerPickResponse){
				                                                                            	transferre=((WSAnswerPickResponse) answer ).getValue()+"";
				                                                                            }
                                        												
                                        											}
                                        											if(answerId.equalsIgnoreCase("33162054"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	recommend=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	recommend=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("33161966"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	destination=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	destination=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("33161887"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	origin=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	origin=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("33162044"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	overAll=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	overAll=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("33161795"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	preMove=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	preMove=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                                                        			String flag5="0";
                                                                        			String flag6="0";
                                                                        			String query15 = "select shipNumber from serviceorder where shipNumber='"+shipNumber+"'";
                                                                        			String query25="";
                                                                        		try {
		                                                                        			if(!shipNumber.equalsIgnoreCase(""))
		                                                                        			{
			                                                                        			statement = conCF.createStatement();
			                                                                        			ResultSet rs = statement.executeQuery(query15);
			                                                                        			while (rs.next()) {
			                                                                        				flag5="1";
			                                                                        				query25 = "select lastSent from surveyemailaudit where shipNumber='"+shipNumber+"'";
			                                                                        			}
			                                                                        			statement.close();
			                                                                        			if(!query25.equalsIgnoreCase(""))
			                                                                        			{
			                                                                            			statement = conCF.createStatement();
			                                                                            			ResultSet rs1 = statement.executeQuery(query25);
			                                                                            			while (rs1.next()) {
			                                                                            				flag6="1";
			                                                                            				sentDate=rs1.getDate(1);
			                                                                            			}
			                                                                            			statement.close();
			                                                                        			}
			                                                                        			String query115 = "select sentDate from serviceorder where shipNumber='"+shipNumber+"'";
			                                                                        			statement = conCF.createStatement();
			                                                                        			ResultSet rs115 = statement.executeQuery(query115);
			                                                                        			while (rs115.next()) {
			                                                                        				sentDate1=rs115.getDate(1);
			                                                                        			}
			                                                                        			statement.close();
		                                                                        			}
                                                                        		    }catch(Exception e){}
                                                                        		
                                                                        		String query16 ="";
                                                                        		    if(flag5.equalsIgnoreCase("1"))
                                                                        		    {
                                                                        		    	if(sentDate1==null)
                                                                        		    	{
                                                                        		    	query16="update serviceorder set preMoveSurvey='"+preMove+"',originServices='"+origin+"',destinationServices='"+destination+"',overAll='"+overAll+"',recommendation="+recommend+",sentDate='"+sentDate+"',transferee='"+transferre+"',returnDate='"+sendActualToClientDates+"' where shipNumber='"+shipNumber+"'";
                                                                        		    	}else{
                                                                       		    		query16="update serviceorder set preMoveSurvey='"+preMove+"',originServices='"+origin+"',destinationServices='"+destination+"',overAll='"+overAll+"',recommendation="+recommend+",transferee='"+transferre+"',returnDate='"+sendActualToClientDates+"' where shipNumber='"+shipNumber+"'";
                                                                        		    	}
                                                                        		    }
                                            										try
                                            										{
                                            											statement = conCF.createStatement();
                                            											if(!query16.equalsIgnoreCase(""))
                                            											{
                                            												int val = statement.executeUpdate(query16);
                                            											}
                                            										statement.close();
                                            										}catch(Exception e){}
                                        										                                        											
                                        										}else if(formId.equalsIgnoreCase("340829")){


                                        											String answerId=answer.getAnswerId()+"";
                                        											if(answerId.equalsIgnoreCase("37716609"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                                            shipNumber=((WSAnswerTextResponse) answer ).getText();
                                                                                            }
                                                                                            if(answer instanceof WSAnswerPickResponse){
                                                                                                            shipNumber=((WSAnswerPickResponse) answer ).getValue()+"";
                                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("37716631"))
                                        											{
	                                                                                        if(answer instanceof WSAnswerTextResponse){
	                                                                                        	transferre=((WSAnswerTextResponse) answer ).getText();
				                                                                            }
				                                                                            if(answer instanceof WSAnswerPickResponse){
				                                                                            	transferre=((WSAnswerPickResponse) answer ).getValue()+"";
				                                                                            }
                                        												
                                        											}
                                        											if(answerId.equalsIgnoreCase("37719009"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	recommend=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	recommend=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("41920192"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	destination=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	destination=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("41920183"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	origin=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	origin=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("42045785"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	overAll=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	overAll=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("37716613"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	preMove=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	preMove=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                                                        			String flag5="0";
                                                                        			String flag6="0";
                                                                        			String query15 = "select shipNumber from serviceorder where shipNumber='"+shipNumber+"'";
                                                                        			String query25="";
                                                                        		try {
		                                                                        			if(!shipNumber.equalsIgnoreCase(""))
		                                                                        			{
			                                                                        			statement = conCF.createStatement();
			                                                                        			ResultSet rs = statement.executeQuery(query15);
			                                                                        			while (rs.next()) {
			                                                                        				flag5="1";
			                                                                        				query25 = "select lastSent from surveyemailaudit where shipNumber='"+shipNumber+"'";
			                                                                        			}
			                                                                        			statement.close();
			                                                                        			if(!query25.equalsIgnoreCase(""))
			                                                                        			{
			                                                                            			statement = conCF.createStatement();
			                                                                            			ResultSet rs1 = statement.executeQuery(query25);
			                                                                            			while (rs1.next()) {
			                                                                            				flag6="1";
			                                                                            				sentDate=rs1.getDate(1);
			                                                                            			}
			                                                                            			statement.close();
			                                                                        			}
			                                                                        			String query115 = "select sentDate from serviceorder where shipNumber='"+shipNumber+"'";
			                                                                        			statement = conCF.createStatement();
			                                                                        			ResultSet rs115 = statement.executeQuery(query115);
			                                                                        			while (rs115.next()) {
			                                                                        				sentDate1=rs115.getDate(1);
			                                                                        			}
			                                                                        			statement.close();
		                                                                        			}
                                                                        		    }catch(Exception e){}
                                                                        		
                                                                        		String query16 ="";
                                                                        		    if(flag5.equalsIgnoreCase("1"))
                                                                        		    {
                                                                        		    	if(sentDate1==null)
                                                                        		    	{
                                                                        		    	query16="update serviceorder set preMoveSurvey='"+preMove+"',originServices='"+origin+"',destinationServices='"+destination+"',overAll='"+overAll+"',recommendation="+recommend+",sentDate='"+sentDate+"',transferee='"+transferre+"',returnDate='"+sendActualToClientDates+"' where shipNumber='"+shipNumber+"'";
                                                                        		    	}else{
                                                                       		    		query16="update serviceorder set preMoveSurvey='"+preMove+"',originServices='"+origin+"',destinationServices='"+destination+"',overAll='"+overAll+"',recommendation="+recommend+",transferee='"+transferre+"',returnDate='"+sendActualToClientDates+"' where shipNumber='"+shipNumber+"'";
                                                                        		    	}
                                                                        		    }
                                            										try
                                            										{
                                            											statement = conCF.createStatement();
                                            											if(!query16.equalsIgnoreCase(""))
                                            											{
                                            												int val = statement.executeUpdate(query16);
                                            											}
                                            										statement.close();
                                            										}catch(Exception e){}
                                        										                                        											
                                        										
                                        										}else if(formId.equalsIgnoreCase("353780")){



                                        											String answerId=answer.getAnswerId()+"";
                                        											if(answerId.equalsIgnoreCase("39455630"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                                            shipNumber=((WSAnswerTextResponse) answer ).getText();
                                                                                            }
                                                                                            if(answer instanceof WSAnswerPickResponse){
                                                                                                            shipNumber=((WSAnswerPickResponse) answer ).getValue()+"";
                                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("39455652"))
                                        											{
	                                                                                        if(answer instanceof WSAnswerTextResponse){
	                                                                                        	transferre=((WSAnswerTextResponse) answer ).getText();
				                                                                            }
				                                                                            if(answer instanceof WSAnswerPickResponse){
				                                                                            	transferre=((WSAnswerPickResponse) answer ).getValue()+"";
				                                                                            }
                                        												
                                        											}
                                        											if(answerId.equalsIgnoreCase("39455650"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	recommend=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	recommend=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("39455646"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	destination=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	destination=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("39455640"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	origin=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	origin=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("39455649"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	overAll=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	overAll=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                        											if(answerId.equalsIgnoreCase("39455634"))
                                        											{
                                                                                            if(answer instanceof WSAnswerTextResponse){
                                                                                            	preMove=((WSAnswerTextResponse) answer ).getText();
    			                                                                            }
    			                                                                            if(answer instanceof WSAnswerPickResponse){
    			                                                                            	preMove=((WSAnswerPickResponse) answer ).getValue()+"";
    			                                                                            }
                                        											}
                                                                        			String flag5="0";
                                                                        			String flag6="0";
                                                                        			String query15 = "select shipNumber from serviceorder where shipNumber='"+shipNumber+"'";
                                                                        			String query25="";
                                                                        		try {
		                                                                        			if(!shipNumber.equalsIgnoreCase(""))
		                                                                        			{
			                                                                        			statement = conCF.createStatement();
			                                                                        			ResultSet rs = statement.executeQuery(query15);
			                                                                        			while (rs.next()) {
			                                                                        				flag5="1";
			                                                                        				query25 = "select lastSent from surveyemailaudit where shipNumber='"+shipNumber+"'";
			                                                                        			}
			                                                                        			statement.close();
			                                                                        			if(!query25.equalsIgnoreCase(""))
			                                                                        			{
			                                                                            			statement = conCF.createStatement();
			                                                                            			ResultSet rs1 = statement.executeQuery(query25);
			                                                                            			while (rs1.next()) {
			                                                                            				flag6="1";
			                                                                            				sentDate=rs1.getDate(1);
			                                                                            			}
			                                                                            			statement.close();
			                                                                        			}
			                                                                        			String query115 = "select sentDate from serviceorder where shipNumber='"+shipNumber+"'";
			                                                                        			statement = conCF.createStatement();
			                                                                        			ResultSet rs115 = statement.executeQuery(query115);
			                                                                        			while (rs115.next()) {
			                                                                        				sentDate1=rs115.getDate(1);
			                                                                        			}
			                                                                        			statement.close();
		                                                                        			}
                                                                        		    }catch(Exception e){}
                                                                        		
                                                                        		String query16 ="";
                                                                        		    if(flag5.equalsIgnoreCase("1"))
                                                                        		    {
                                                                        		    	if(sentDate1==null)
                                                                        		    	{
                                                                        		    	query16="update serviceorder set preMoveSurvey='"+preMove+"',originServices='"+origin+"',destinationServices='"+destination+"',overAll='"+overAll+"',recommendation="+recommend+",sentDate='"+sentDate+"',transferee='"+transferre+"',returnDate='"+sendActualToClientDates+"' where shipNumber='"+shipNumber+"'";
                                                                        		    	}else{
                                                                       		    		query16="update serviceorder set preMoveSurvey='"+preMove+"',originServices='"+origin+"',destinationServices='"+destination+"',overAll='"+overAll+"',recommendation="+recommend+",transferee='"+transferre+"',returnDate='"+sendActualToClientDates+"' where shipNumber='"+shipNumber+"'";
                                                                        		    	}
                                                                        		    }
                                            										try
                                            										{
                                            											statement = conCF.createStatement();
                                            											if(!query16.equalsIgnoreCase(""))
                                            											{
                                            												int val = statement.executeUpdate(query16);
                                            											}
                                            										statement.close();
                                            										}catch(Exception e){}
                                        										}
                                                                }

                                                }

                                }
                                System.out.println("==============Finished Printing Survey Results For "+surveyID+"==================");
                                
                }

 
                
                public static void main(String[] a) throws RemoteException,

                                                ServiceException {

                                //callKeySurveyWS();

                }



				public SurveyResponse getSurveyResponse() {
					return surveyResponse;
				}



				public void setSurveyResponse(SurveyResponse surveyResponse) {
					this.surveyResponse = surveyResponse;
				}



				public SurveyResponseDtl getSurveyResponseDtl() {
					return surveyResponseDtl;
				}



				public void setSurveyResponseDtl(SurveyResponseDtl surveyResponseDtl) {
					this.surveyResponseDtl = surveyResponseDtl;
				}



				public KeySurveyQuestionMap getKeySurveyQuestionMap() {
					return keySurveyQuestionMap;
				}



				public void setKeySurveyQuestionMap(KeySurveyQuestionMap keySurveyQuestionMap) {
					this.keySurveyQuestionMap = keySurveyQuestionMap;
				}



				public void setSurveyResponseManager(SurveyResponseManager surveyResponseManager) {
					this.surveyResponseManager = surveyResponseManager;
				}



				public void setSurveyResponseDtlManager(
						SurveyResponseDtlManager surveyResponseDtlManager) {
					this.surveyResponseDtlManager = surveyResponseDtlManager;
				}
				public void setKeySurveyQuestionMapManager(
						KeySurveyQuestionMapManager keySurveyQuestionMapManager) {
					this.keySurveyQuestionMapManager = keySurveyQuestionMapManager;
				}
				public String getSessionCorpID() {
					return sessionCorpID;
				}
				public void setSessionCorpID(String sessionCorpID) {
					this.sessionCorpID = sessionCorpID;
				}
				public String getRemoteUser() {
					return remoteUser;
				}
				public void setRemoteUser(String remoteUser) {
					this.remoteUser = remoteUser;
				}

 

}
