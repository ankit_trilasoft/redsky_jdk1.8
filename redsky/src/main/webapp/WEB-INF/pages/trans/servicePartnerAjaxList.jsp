<%@ include file="/common/taglibs.jsp"%>  
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@page import="com.trilasoft.app.model.Company" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script language="javascript" type="text/javascript">
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	}
</script>  
<head>   
    <title><fmt:message key="servicePartnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='servicePartnerList.heading'/>"/> 
    <% 
    Company company = (Company)request.getAttribute("company");
	Date currentDateForSP = new Date();
	SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	format1.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	String currentDateForService=format1.format(currentDateForSP);
	
	format1 = new SimpleDateFormat("yyyy-MM-dd"); 
	currentDateForSP =  format1.parse(currentDateForService);
	currentDateForService=format1.format(currentDateForSP);

	%>
    
    <style type="text/css">
<%@ include file="/common/calenderStyle.css"%>

/* collapse */

 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script>
function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
	     location.href = "updateServicePartnerStatus.html?id="+encodeURI(targetElement)+"&sid=${serviceOrder.id}";
     }else{
		return false;
	}
}
  
  function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
   
    function updateServicePartnerDetails(id,fieldName,target,tableName,routId){
    	showOrHide(1);
    	var fieldValue = target.value;    	
		var url='';
		if(soControlFlag=='G'  &&  soGrpStatus=='Finalized'){
			var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
			if(agree){
					url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName+"&updateRecords="+updateRecords;
			}else{
				 	url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName;
			}
		}else if(soGrpId!='' && (soGrpStatus=='Draft'  || soGrpStatus=='Finalized') && (routId!='0' && routId!='')){
			alert("Update is not allowed as this service order is a part of a groupage order.");
			getRoutingDetails();
		}else{
			 		url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName;
		}
    	  http233.open("GET", url, true); 
    	  http233.onreadystatechange = handleHttpResponse233;
    	  http233.send(null);	 
    }
    function handleHttpResponse233(){
    	if (http233.readyState == 4){
                var results = http233.responseText
                results = results.trim();
                getRoutingDetails();
                showOrHide(0);
    	}
    }
    var http233 = getHTTPObject();
    
    function updateServicePartnerDetailsTrans(id,fieldName,fieldValue,tableName,routId){
    	showOrHide(1);
    	var url='';
		if(soControlFlag=='G'  &&  soGrpStatus=='Finalized'){
			var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
			if(agree){
					url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName+"&updateRecords="+updateRecords;
			}else{
				 	url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName;
			}
		}else if(soGrpId!='' && (soGrpStatus=='Draft'  || soGrpStatus=='Finalized') && (routId!='0' && routId!='')){
			alert("Update is not allowed as this service order is a part of a groupage order.");
			getRoutingDetails();
		}else{
			 		url = "updateContainerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&tableName="+tableName;
		}
    	  http233Trans.open("GET", url, true); 
    	  http233Trans.onreadystatechange = handleHttpResponse233Trans;
    	  http233Trans.send(null);	 
    }
    function handleHttpResponse233Trans(){
    	if (http233Trans.readyState == 4){
                var results = http233Trans.responseText
                results = results.trim();
                getRoutingDetails();
                showOrHide(0);
    	}
    }
    var http233Trans = getHTTPObject();
    
    var soGrpId = '${serviceOrder.grpID}';
    var soControlFlag = '${serviceOrder.controlFlag}';
    var soGrpStatus = '${serviceOrder.grpStatus}';
    var updateRecords = 'updateAll';
    var fieldValue ="";
    var fieldValue1 ="";
    var fieldName1 ="";
    var fieldName ="";
    function updateServicePartnerNameAndCodeDetails(){
    	showOrHide(1);
    	if(tempId!=null && tempId!=''){
			var regId=tempId;
			tempId="";
			var tableName = 'servicepartner';
		if(fieldDescription.indexOf("carrierName")!= -1){
			fieldValue = document.getElementById('carrierName'+regId).value;
			fieldName = 'carrierName';
			fieldValue1 = document.getElementById('carrierCode'+regId).value;
			fieldName1 = 'carrierCode';
		}else if(fieldDescription.indexOf("carrierDeparture")!= -1){
			fieldValue = document.getElementById('carrierDeparture'+regId).value;
			fieldName = 'carrierDeparture';
			fieldValue1 = document.getElementById('polCode'+regId).value;
			fieldName1 = 'polCode';
		}else if(fieldDescription.indexOf("carrierArrival")!= -1){
			fieldValue = document.getElementById('carrierArrival'+regId).value;
			fieldName = 'carrierArrival';
			fieldValue1 = document.getElementById('poeCode'+regId).value;
			fieldName1 = 'poeCode';
		}else{
			
		}
		var url='';
		if(soControlFlag=='G'  &&  soGrpStatus=='Finalized'){
			var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
			if(agree){
					url = "updateServicePartnerDetailsAjax.html?ajax=1&id="+regId+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&fieldName1="+fieldName1+"&fieldValue1="+fieldValue1+"&tableName="+tableName+"&updateRecords="+updateRecords;
			}else{
				 	url = "updateServicePartnerDetailsAjax.html?ajax=1&id="+regId+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&fieldName1="+fieldName1+"&fieldValue1="+fieldValue1+"&tableName="+tableName;
			}
		}else if(soGrpId!='' && (soGrpStatus=='Draft'  || soGrpStatus=='Finalized') && (routId!='0' && routId!='')){
			alert("Update is not allowed as this service order is a part of a groupage order.");
			getRoutingDetails();
		}else{
			 		url = "updateServicePartnerDetailsAjax.html?ajax=1&id="+regId+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&fieldName1="+fieldName1+"&fieldValue1="+fieldValue1+"&tableName="+tableName;
		}		
    		httpCodeName.open("GET", url, true); 
    		httpCodeName.onreadystatechange = handleHttpResponseCodeName;
    		httpCodeName.send(null);	
    	}
    }
    function handleHttpResponseCodeName(){
    	if (httpCodeName.readyState == 4){
                var results = httpCodeName.responseText
                results = results.trim();
                showOrHide(0);
    	}
    }
    var httpCodeName = getHTTPObject();

    
    function deleteRoutingDetails(targetElement,targetElementStatus){
    	var massage="";
    	if(targetElementStatus.checked==false){
    		massage="Are you sure you wish to deactivate this row?"
    	}else{
    		massage="Are you sure you wish to activate this row?"
    	}  
 	   var agree=confirm(massage);
 		if (agree){
 			showOrHide(1);
 			var routingStatus=false;
			if(targetElementStatus.checked==false){
				routingStatus=false;	
			}else{
				routingStatus=true;
			}
			var url ='';
			if(soControlFlag=='G'){
				var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
				if(agree){
					 url = "deleteRoutingDetailsAjax.html?ajax=1&id="+encodeURI(targetElement)+"&routingStatus="+routingStatus+"&updateRecords="+updateRecords;
				}else{
					 url = "deleteRoutingDetailsAjax.html?ajax=1&id="+encodeURI(targetElement)+"&routingStatus="+routingStatus;
				}
			}else{
 	   	             url = "deleteRoutingDetailsAjax.html?ajax=1&id="+encodeURI(targetElement)+"&routingStatus="+routingStatus;
			}
 	 	  http112.open("GET", url, true); 
 	 	  http112.onreadystatechange = handleHttpResponse112;
 	 	  http112.send(null);
 		}else{
 			if(targetElementStatus.checked==false){
 				document.getElementById('status'+targetElement).checked=true;
 				}else{
 					document.getElementById('status'+targetElement).checked=false;
 				}
 		}
    }
    function handleHttpResponse112(){
    	if (http112.readyState == 4){
                var results = http112.responseText
                getRoutingDetails();
                showOrHide(0);
    	}
    }
    var http112 = getHTTPObject();
   
    var tempId="";
    var fieldDescription="";
    var routId = "";
    function findCarrierName(carrierCode,carrierName,id,routingId){    	
    	tempId=id;
    	fieldDescription = carrierName;
    	routId = routingId;
 	    openWindow('servicePartnerlistPopUp.html?id=${serviceOrder.id}&partnerType=CR&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=trackingUrl&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+carrierName+'&fld_code='+carrierCode);
    }
    function findPolCode(polCode,carrierDeparture,id,routingId){
    	tempId=id;
    	fieldDescription = carrierDeparture;
    	routId =routingId;
    	openWindow('searchPortList.html?portCode=&portName=&country=&modeType=${serviceOrder.mode}&active=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+carrierDeparture+'&fld_code='+polCode);
    }
    function findPoeCode(poeCode,carrierArrival,id,routingId){
    	tempId=id;
    	fieldDescription = carrierArrival;
    	routId = routingId;
    	openWindow('searchPortList.html?portCode=&portName=&country=&modeType=${serviceOrder.mode}&active=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+carrierArrival+'&fld_code='+poeCode);
    }
    var id="";
    function calcDaysETD(id2,routingId,fieldName2){
    	id=id2;
    	routId = routingId;
    	onClickFieldName = fieldName2;
    	}
    function calcDays() {
    	if(onClickFieldName=='etDepart' || onClickFieldName=='etArrival'){
    	var date1 = document.getElementById('etDepart'+id).value;
    	var date2 = document.getElementById('etArrival'+id).value;
    	
 	    var day = '';
	    var month = '';
	    var year = '';
	    var finalDate ='';
	    var eDate = null;
    	if(date1!='' && date1.indexOf('-')>-1){
    	
    	 var mySplitResult = date1.split("-");
    	    day = mySplitResult[0];
    	    month = mySplitResult[1];
    	    year = mySplitResult[2];
    	  if(month == 'Jan')  { month = "01";
    	   }  else if(month == 'Feb') { month = "02";
    	   } else if(month == 'Mar') { month = "03"
    	   }   else if(month == 'Apr')  { month = "04"
    	   }  else if(month == 'May') { month = "05"
    	   }  else if(month == 'Jun') { month = "06"
    	   }  else if(month == 'Jul') { month = "07"
    	   } else if(month == 'Aug') {month = "08"
    	   } else if(month == 'Sep')  {month = "09"
    	   } else if(month == 'Oct')  {month = "10"
    	   }  else if(month == 'Nov')  { month = "11"
    	   } else if(month == 'Dec')  {month = "12";
    	   }
    	    finalDate = month+"-"+day+"-"+year;
        	date1 = finalDate.split("-");
       	    eDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
    	}
   	   var day2 = '';
	   var month2 = '';
	   var year2 = '';
	   var finalDate2 ='';
	   var sDate = null;
	   if(date2!='' && date2.indexOf('-')>-1){
    	
    	   var mySplitResult2 = date2.split("-");
    	    day2 = mySplitResult2[0];
    	    month2 = mySplitResult2[1];
    	    year2 = mySplitResult2[2];
    	   if(month2 == 'Jan')  { month2 = "01";
    	   }  else if(month2 == 'Feb') { month2 = "02";
    	   } else if(month2 == 'Mar') {month2 = "03"
    	   } else if(month2 == 'Apr') {month2 = "04"
    	   }  else if(month2 == 'May')  { month2 = "05"
    	   }  else if(month2 == 'Jun') { month2 = "06"
    	   } else if(month2 == 'Jul') { month2 = "07"
    	   } else if(month2 == 'Aug')  {month2 = "08"
    	   } else if(month2 == 'Sep')  { month2 = "09"
    	   } else if(month2 == 'Oct')  { month2 = "10"
    	   }  else if(month2 == 'Nov') { month2 = "11"
    	   } else if(month2 == 'Dec')  {month2 = "12";
    	   }
    	   finalDate2 = month2+"-"+day2+"-"+year2;
     	   date2 = finalDate2.split("-");
   	       sDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
	   }
    	if((eDate!=null && sDate!=null) && eDate > sDate){
    		alert("Estimated date of arrival should be greater or equal to Estimated date of departure.")
    		getRoutingDetails();    
    	}else{
    		var yr="20";
    		var time="00:00:00";
    		var etdDate = yr+year+"-"+month+"-"+day+" "+time;
    		if(year=='' || month=='' || day==''){
    			etdDate=null;
    		}    		
    		fieldName = 'etDepart';
    		fieldValue = etdDate;
    		var etaDate = yr+year2+"-"+month2+"-"+day2+" "+time;
    		if(year2=='' || month2=='' || day2==''){
    			etaDate=null;
    		}    		
    		fieldName1 = 'etArrival';
    		fieldValue1 = etaDate;
    		var tableName = 'servicepartner';
    		if(onClickFieldName=='etArrival'){
    			updateServicePartnerETDate(id,fieldName,fieldValue,fieldName1,fieldValue1,tableName,routId);
    		}
    	} 
    	}
    }
    function updateServicePartnerETDate(id,fieldName,fieldValue,fieldName1,fieldValue1,tableName,routId){
    	showOrHide(1);
    	
		var url='';
		if(soControlFlag=='G'  &&  soGrpStatus=='Finalized'){
			var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
			if(agree){
					url = "updateServicePartnerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&fieldName1="+fieldName1+"&fieldValue1="+fieldValue1+"&tableName="+tableName+"&updateRecords="+updateRecords;
			}else{
				 	url = "updateServicePartnerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&fieldName1="+fieldName1+"&fieldValue1="+fieldValue1+"&tableName="+tableName;
			}
		}else if(soGrpId!='' && (soGrpStatus=='Draft'  || soGrpStatus=='Finalized') && (routId!='0' && routId!='')){
			alert("Update is not allowed as this service order is a part of a groupage order.");
			getRoutingDetails();
		}else{
			 		url = "updateServicePartnerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&fieldName1="+fieldName1+"&fieldValue1="+fieldValue1+"&tableName="+tableName;
		}
    	
    	  http23211.open("GET", url, true); 
    	  http23211.onreadystatechange = handleHttpResponse23211;
    	  http23211.send(null);	    	
    }
    function handleHttpResponse23211(){
    	if (http23211.readyState == 4){
                var results = http23211.responseText
                results = results.trim();
                showOrHide(0);
    	}
    }
    var http23211 = getHTTPObject();

    var onClickFieldName = "";
    function calcDaysATA(id2,routingId,fieldName2){
    	id=id2;
    	routId = routingId;
    	onClickFieldName = fieldName2;
    	}
    function calcDays1() {        
    	 if(onClickFieldName=='atDepart' || onClickFieldName=='atArrival'){
    	var date1 = document.getElementById('atDepart'+id).value;
    	var date2 = document.getElementById('atArrival'+id).value;
 	   var day = '';
	   var month = '';
	   var year = '';
	   var finalDate ='';
	   var eDate = null;
    	if(date1!='' && date1.indexOf('-')>-1){
    	 var mySplitResult = date1.split("-");
    	    day = mySplitResult[0];
    	    month = mySplitResult[1];
    	    year = mySplitResult[2];
    	  if(month == 'Jan')  { month = "01";
    	   }  else if(month == 'Feb') { month = "02";
    	   } else if(month == 'Mar') { month = "03"
    	   }   else if(month == 'Apr')  { month = "04"
    	   }  else if(month == 'May') { month = "05"
    	   }  else if(month == 'Jun') { month = "06"
    	   }  else if(month == 'Jul') { month = "07"
    	   } else if(month == 'Aug') {month = "08"
    	   } else if(month == 'Sep')  {month = "09"
    	   } else if(month == 'Oct')  {month = "10"
    	   }  else if(month == 'Nov')  { month = "11"
    	   } else if(month == 'Dec')  {month = "12";
    	   }
    	    finalDate = month+"-"+day+"-"+year;
      	  date1 = finalDate.split("-");
    	   eDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
    	}
  	   var day2 = '';
	   var month2 = '';
	   var year2 = '';
	   var finalDate2 ='';
	   var sDate = null;
	   if(date2!='' && date2.indexOf('-')>-1){
    	   var mySplitResult2 = date2.split("-");
    	    day2 = mySplitResult2[0];
    	    month2 = mySplitResult2[1];
    	    year2 = mySplitResult2[2];
    	   if(month2 == 'Jan')  { month2 = "01";
    	   }  else if(month2 == 'Feb') { month2 = "02";
    	   } else if(month2 == 'Mar') {month2 = "03"
    	   } else if(month2 == 'Apr') {month2 = "04"
    	   }  else if(month2 == 'May')  { month2 = "05"
    	   }  else if(month2 == 'Jun') { month2 = "06"
    	   } else if(month2 == 'Jul') { month2 = "07"
    	   } else if(month2 == 'Aug')  {month2 = "08"
    	   } else if(month2 == 'Sep')  { month2 = "09"
    	   } else if(month2 == 'Oct')  { month2 = "10"
    	   }  else if(month2 == 'Nov') { month2 = "11"
    	   } else if(month2 == 'Dec')  {month2 = "12";
    	   }
    	   finalDate2 = month2+"-"+day2+"-"+year2;
    	  date2 = finalDate2.split("-");
    	   sDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
    	   
	   }
    	
  	    var currentD = '<%= currentDateForService %>';
	    var date3SplitResult = currentD.split("-");
 		var day3 = date3SplitResult[2];
 		var month3 = date3SplitResult[1];
 		var year3 = date3SplitResult[0];
 		var currentDate = new Date(month3+"/"+day3+"/"+year3);
          
          var futureDateVal = ${futureDateFlag};
          var diff =0;
          if(finalDate!='' && finalDate.indexOf('-')>-1){
	          var actArvDate = finalDate.split("-");
	  		  var actArvDD = actArvDate[0];
	  		  var actArvMM = actArvDate[1];
	  		  var actArvYY = actArvDate[2];
	  		  var yr="20";
			  var actArvFinalDate = actArvDD+"/"+actArvMM+"/"+yr+actArvYY;
			  var date5 = new Date(actArvFinalDate)
			  diff = Math.round((date5-currentDate)/86400000);
          }
          var diff1 =0;
          if(finalDate2!='' && finalDate2.indexOf('-')>-1){
			  var finalDate2 = month2+"-"+day2+"-"+year2;
			  var estArvDate = finalDate2.split("-");
			  var estArvDD = estArvDate[0];
			  var estArvMM = estArvDate[1];
			  var estArvYY = estArvDate[2];
			  var yr="20";
			  var estArvFinalDate = estArvDD+"/"+estArvMM+"/"+yr+estArvYY;
			  var date6 = new Date(estArvFinalDate);
			   diff1 = Math.round((date6-currentDate)/86400000);
          }
		
    	if((eDate!=null && sDate!=null) && eDate > sDate){
    		alert("Actual date of arrival should be greater or equal to Actual date of departure.")
    		getRoutingDetails();
    	}else if((futureDateVal==false) && (diff > 0)){
    		alert("System cannot accept actualizing future dates..Please edit Actual Departure date.")
    		getRoutingDetails();
    	}else if((futureDateVal==false) && (diff1 > 0)){
        		alert("System cannot accept actualizing future dates..Please edit Actual Arrival date.")
        		getRoutingDetails();
    	}else{
    		var yr="20";
    		var time="00:00:00";
    		var etdDate = yr+year+"-"+month+"-"+day;
    		if(year=='' || month=='' || day==''){
    			etdDate=null;
    		}
    		fieldName = 'atDepart';
    		fieldValue = etdDate;
    		var etaDate = yr+year2+"-"+month2+"-"+day2;
    		if(year2=='' || month2=='' || day2==''){
    			etaDate=null;
    		}
    		fieldName1 = 'atArrival';
    		fieldValue1 = etaDate;
    		var tableName = 'servicepartner';
    		if(onClickFieldName == 'atDepart'){
    			updateServicePartnerATDate(id,fieldName,fieldValue,fieldName1,fieldValue1,tableName,routId);
    		}
    	} 
    	 }
    }
    function updateServicePartnerATDate(id,fieldName,fieldValue,fieldName1,fieldValue1,tableName,routId){
    	showOrHide(1);
    	
		var url='';
		if(soControlFlag=='G'  &&  soGrpStatus=='Finalized'){
			var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
			if(agree){
					url = "updateServicePartnerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&fieldName1="+fieldName1+"&fieldValue1="+fieldValue1+"&tableName="+tableName+"&updateRecords="+updateRecords;
			}else{
				 	url = "updateServicePartnerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&fieldName1="+fieldName1+"&fieldValue1="+fieldValue1+"&tableName="+tableName;
			}
		}else if(soGrpId!='' && (soGrpStatus=='Draft'  || soGrpStatus=='Finalized') && (routId!='0' && routId!='')){
			alert("Update is not allowed as this service order is a part of a groupage order.");
			getRoutingDetails();
		}else{
			 		url = "updateServicePartnerDetailsAjax.html?ajax=1&id="+id+"&fieldName="+fieldName+"&fieldValue="+fieldValue+"&fieldName1="+fieldName1+"&fieldValue1="+fieldValue1+"&tableName="+tableName;
		}
    	  http232112.open("GET", url, true); 
    	  http232112.onreadystatechange = handleHttpResponse232112;
    	  http232112.send(null);	    	
    }
    function handleHttpResponse232112(){
    	if (http232112.readyState == 4){
                var results = http232112.responseText
                results = results.trim();
                showOrHide(0);
    	}
    }
    var http232112 = getHTTPObject();
    
</script>  
</head> 

<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="ServiceOrderID" value="${serviceOrder.id}"/>
<s:hidden name="ServiceOrderID" value="${serviceOrder.id}"/>
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.registrationNumber" value="%{serviceOrder.registrationNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/> 
<s:hidden name="customerFile.id" />
<c:set var="field"  value="<%=request.getParameter("field") %>"/>
<s:hidden name="field" id="field" value="<%=request.getParameter("field") %>" />
<s:hidden name="field1" id="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
<c:set var="mydate" value="{0,date,dd-MMM-yyyy}"/>
<div id="Layer3" style="width:100%">

<c:set var="buttons">   
     <input type="button" class="cssbutton1" onclick="window.open('editServicePartnerAjax.html?decorator=popup&popup=true&sid=${ServiceOrderID}','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes').focus();" 
       value="Add" style="width:60px; height:25px;margin-bottom:5px;" />    
</c:set>  

<c:set var="buttonsAdd">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="alert('You cannot add as this service order is part of groupage order. OK to proceed.');"  
        value="<fmt:message key="button.add"/>"/>    
</c:set>  
  
<s:set name="servicePartnerss" value="servicePartnerss" scope="request"/>
<display:table name="servicePartnerss" class="table" requestURI="" id="servicePartnerList" export="false" defaultsort="0" style="width:100%; margin-left: 5px;margin:2px 0 10px;">   
     <display:column titleKey="servicePartner.carrierNumber"  >
     <sec-auth:authComponent componentId="module.button.servicepartner.addButton">
     <a href="javascript: void(0)" onclick= "window.open('editServicePartnerAjax.html?id=${servicePartnerList.id}&decorator=popup&popup=true','sqlExtractInputForm','height=500,width=950,top=0, scrollbars=yes,resizable=yes').focus();" style="cursor:pointer">
   	 <c:out value="${servicePartnerList.carrierNumber}"/></a>
   	 </sec-auth:authComponent>
   	 </display:column>
	<display:column  titleKey="servicePartner.carrierName" >
		<s:hidden name="carrierCode${servicePartnerList.id}" id="carrierCode${servicePartnerList.id}" value="${servicePartnerList.carrierCode}" />
		<input type="text" class="input-textUpper" style="text-align:left;width:100px" name="servicePartner.carrierName${servicePartnerList.id}" id="carrierName${servicePartnerList.id}" readonly="true" value="${servicePartnerList.carrierName}"  />
		<img class="openpopup" style="vertical-align:top;" width="15" height="18" onclick="findCarrierName('carrierCode${servicePartnerList.id}','carrierName${servicePartnerList.id}','${servicePartnerList.id}','${servicePartnerList.servicePartnerId}');" src="<c:url value='/images/open-popup.gif'/>" />
	</display:column>
	<display:column  titleKey="servicePartner.carrierDeparture">
		<s:hidden name="polCode${servicePartnerList.id}" id="polCode${servicePartnerList.id}" value="${servicePartnerList.polCode}" />
		<input type="text" class="input-textUpper" style="text-align:left;width:80px" name="servicePartner.carrierDeparture${servicePartnerList.id}" id="carrierDeparture${servicePartnerList.id}" readonly="true" value="${servicePartnerList.carrierDeparture}"  />
		<img class="openpopup" style="vertical-align:top;" width="15" height="18" onclick="findPolCode('polCode${servicePartnerList.id}','carrierDeparture${servicePartnerList.id}','${servicePartnerList.id}','${servicePartnerList.servicePartnerId}');" src="<c:url value='/images/open-popup.gif'/>" />
	</display:column>
    <display:column  titleKey="servicePartner.etDepart" >    
	    <fmt:parseDate pattern="yyyy-MM-dd" value="${servicePartnerList.etDepart}" var="parsedEtDepartDate" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parsedEtDepartDate}" var="formattedEtDepartDate" />    
    	<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="etDepart${servicePartnerList.id}" name="servicePartner.etDepart${servicePartnerList.id}" readonly="true" value="${formattedEtDepartDate}"  />
    	<img id="etDepart${servicePartnerList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="calcDaysETD('${servicePartnerList.id}','${servicePartnerList.servicePartnerId}','etArrival');"/>
    </display:column>
    <display:column  titleKey="servicePartner.atDepart" >
    	 <fmt:parseDate pattern="yyyy-MM-dd" value="${servicePartnerList.atDepart}" var="parsedAtDepartDate" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtDepartDate}" var="formattedAtDepartDate" />    
    	<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="atDepart${servicePartnerList.id}" name="servicePartner.atDepart${servicePartnerList.id}" readonly="true" value="${formattedAtDepartDate}" />
    	<img id="atDepart${servicePartnerList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="calcDaysATA('${servicePartnerList.id}','${servicePartnerList.servicePartnerId}','atDepart');"/>
    </display:column>
	<display:column  titleKey="servicePartner.carrierArrival">
		<s:hidden name="poeCode${servicePartnerList.id}" id="poeCode${servicePartnerList.id}" value="${servicePartnerList.poeCode}" />
		<input type="text" class="input-textUpper" style="text-align:left;width:80px" name="servicePartner.carrierArrival${servicePartnerList.id}" id="carrierArrival${servicePartnerList.id}" readonly="true" value="${servicePartnerList.carrierArrival}"  />
		<img class="openpopup" style="vertical-align:top;" width="15" height="18" onclick="findPoeCode('poeCode${servicePartnerList.id}','carrierArrival${servicePartnerList.id}','${servicePartnerList.id}','${servicePartnerList.servicePartnerId}');" src="<c:url value='/images/open-popup.gif'/>" />
	</display:column>
    <display:column  titleKey="servicePartner.etArrival" >
    	<fmt:parseDate pattern="yyyy-MM-dd" value="${servicePartnerList.etArrival}" var="parsedEtArrivalDate" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parsedEtArrivalDate}" var="formattedEtArrivalDate" />    
    	<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="etArrival${servicePartnerList.id}" name="servicePartner.etArrival${servicePartnerList.id}" readonly="true" value="${formattedEtArrivalDate}" />
    	<img id="etArrival${servicePartnerList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="calcDaysETD('${servicePartnerList.id}','${servicePartnerList.servicePartnerId}','etArrival');"/>
    </display:column>
    <display:column  titleKey="servicePartner.atArrival" >
    	<fmt:parseDate pattern="yyyy-MM-dd" value="${servicePartnerList.atArrival}" var="parsedAtArrivalDate" />
		<fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtArrivalDate}" var="formattedAtArrivalDate" />    
    	<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="atArrival${servicePartnerList.id}" name="servicePartner.atArrival${servicePartnerList.id}" readonly="true" value="${formattedAtArrivalDate}" />
    	<img id="atArrival${servicePartnerList.id}_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="calcDaysATA('${servicePartnerList.id}','${servicePartnerList.servicePartnerId}','atDepart');"/>
    </display:column>

    <display:column  titleKey="servicePartner.transhipped" >
     	<c:choose>
         <c:when test="${servicePartnerList.transhipped}">
    	<input type="hidden" style="text-align:right;width:20px" maxlength="10" name="servicePartner.transhipped${servicePartnerList.id}" id="transhipped${servicePartnerList.id}" value="${servicePartnerList.transhipped}"/>
     	<input type="checkbox"  name="servicePartner.transhipped${servicePartnerList.id}"  checked="checked"  onchange="updateServicePartnerDetailsTrans('${servicePartnerList.id}','transhipped',false,'ServicePartner','${servicePartnerList.servicePartnerId}');"/>                                  
       </c:when>
       <c:otherwise>
       <input type="hidden"  name="servicePartner.transhipped${servicePartnerList.id}" value="${servicePartnerList.transhipped}" id="transhipped${servicePartnerList.id}"/>
      	<input type="checkbox"  name="servicePartner.transhipped${servicePartnerList.id}"  onchange="updateServicePartnerDetailsTrans('${servicePartnerList.id}','transhipped',true,'ServicePartner');"/>                                  
       </c:otherwise>
       </c:choose>
    </display:column>
    <display:column  titleKey="carton.cntnrNumber" >
    	<select name ="servicePartner.cntnrNumber${servicePartnerList.id}" id="cntnrNumber${servicePartnerList.id}" style="width:115px" onchange="updateServicePartnerDetails('${servicePartnerList.id}','cntnrNumber',this,'ServicePartner','${servicePartnerList.servicePartnerId}');" class="list-menu">
						<option value="<c:out value='' />">
							<c:out value=""></c:out>
							</option> 		      
				      <c:forEach var="chrms" items="${containerNumberListRouting}" varStatus="loopStatus">
				           <c:choose>
			                        <c:when test="${chrms == servicePartnerList.cntnrNumber}">
			                        <c:set var="selectedInd" value=" selected"></c:set>
			                        </c:when>
			                    <c:otherwise>
			                        <c:set var="selectedInd" value=""></c:set>
			                     </c:otherwise>
		                    </c:choose>
				      		<option value="<c:out value='${chrms}' />" <c:out value='${selectedInd}' />>
				                    <c:out value="${chrms}"></c:out>
				             </option>
					</c:forEach>		      
			</select>     
    </display:column>
    <display:column  titleKey="servicePartner.bookNumber" style="text-align:right;padding-right: 0.3em;">
    	<input type="text" class="input-text" style="text-align:right;width:80px" id="bookNumber${servicePartnerList.id}" name="servicePartner.bookNumber${servicePartnerList.id}" onchange="updateServicePartnerDetails('${servicePartnerList.id}','bookNumber',this,'ServicePartner','${servicePartnerList.servicePartnerId}');" value="${servicePartnerList.bookNumber}"
    </display:column>
    <c:if test="${serviceOrder.mode == 'Overland' || serviceOrder.mode == 'Truck'}">
    	<display:column  title="PRO #">
    		<input type="text" class="input-text" style="text-align:right;width:80px" id="blNumber${servicePartnerList.id}" name="servicePartner.blNumber${servicePartnerList.id}" onchange="updateServicePartnerDetails('${servicePartnerList.id}','blNumber',this,'ServicePartner','${servicePartnerList.servicePartnerId}');" value="${servicePartnerList.blNumber}"
    	</display:column>
    	   <configByCorp:fieldVisibility componentId="component.section.forwarding.Housebilloflading">
    	   	<display:column  titleKey="servicePartner.hblNumber">
    		<input type="text" class="input-text" style="text-align:right;width:80px" id="hblNumber${servicePartnerList.id}" name="servicePartner.hblNumber${servicePartnerList.id}" onchange="updateServicePartnerDetails('${servicePartnerList.id}','hblNumber',this,'ServicePartner','${servicePartnerList.servicePartnerId}');" value="${servicePartnerList.hblNumber}"
    	</display:column>
    	</configByCorp:fieldVisibility>
    </c:if>
    <c:if test="${serviceOrder.mode != 'Overland' && serviceOrder.mode != 'Truck'}">
    	<display:column  titleKey="servicePartner.blNumber">
    		<input type="text" class="input-text" style="text-align:right;width:80px" id="blNumber${servicePartnerList.id}" name="servicePartner.blNumber${servicePartnerList.id}" onchange="updateServicePartnerDetails('${servicePartnerList.id}','blNumber',this,'ServicePartner','${servicePartnerList.servicePartnerId}');" value="${servicePartnerList.blNumber}"
    	</display:column>
    	        <configByCorp:fieldVisibility componentId="component.section.forwarding.Housebilloflading">
    	   	<display:column  titleKey="servicePartner.hblNumber">
    		<input type="text" class="input-text" style="text-align:right;width:80px" id="hblNumber${servicePartnerList.id}" name="servicePartner.hblNumber${servicePartnerList.id}" onchange="updateServicePartnerDetails('${servicePartnerList.id}','hblNumber',this,'ServicePartner','${servicePartnerList.servicePartnerId}');" value="${servicePartnerList.hblNumber}"
    	</display:column>
    	</configByCorp:fieldVisibility>
    </c:if>
    	<sec-auth:authComponent componentId="module.tab.container.auditTab">
	    <display:column title="Audit" style="width:25px; text-align: center;">
	    	<a><img align="middle" src="images/report-ext.png" style="margin: 0px 0px 0px 0px;" onclick="window.open('auditList.html?id=${servicePartnerList.id}&tableName=servicepartner&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"></a>
	    </display:column>
    </sec-auth:authComponent> 
    <display:column title="Status" style="width:45px;">    
   <c:choose>
   <c:when test="${servicePartnerList.servicePartnerId != null && (serviceOrder.grpStatus=='Draft' || serviceOrder.grpStatus=='Finalized')}">
      <c:if test="${servicePartnerList.status}">
          <input type="checkbox"  checked="checked" onclick="alert('Deactivation is not allowed as this service order is a part of a groupage order.');" disabled="disabled"/>
        </c:if>
       <c:if test="${servicePartnerList.status==false}">
		<input type="checkbox"   onclick="alert('Deactivation is not allowed as this service order is a part of a groupage order.');" disabled="disabled"/>
       </c:if> 
   </c:when>
   <c:otherwise>
   <c:if test="${servicePartnerList.status}">
          <input type="checkbox"  checked="checked"  name="status${servicePartnerList.id}" id="status${servicePartnerList.id}" onclick="deleteRoutingDetails(${servicePartnerList.id},this);" />
        </c:if>
       <c:if test="${servicePartnerList.status==false}">
		<input type="checkbox"  name="status${servicePartnerList.id}" id="status${servicePartnerList.id}"  onclick="deleteRoutingDetails(${servicePartnerList.id},this);" />
       </c:if> 
     
   </c:otherwise>
   </c:choose>	
	</display:column> 
	</display:table>
   <sec-auth:authComponent componentId="module.button.servicepartner.addButton"> 
   <c:out value="${buttons}" escapeXml="false" />  
  </sec-auth:authComponent>  
  </div>
  
  <c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<s:hidden name="id" ></s:hidden>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="trackingUrl"/>
<script type="text/javascript">
<c:set var="DateFieldFunctionEnabled" value="F"/>
<configByCorp:fieldVisibility componentId="component.section.forwarding.InLandAgent">
	<c:set var="DateFieldFunctionEnabled" value="T"/>
</configByCorp:fieldVisibility>
<c:if test="${DateFieldFunctionEnabled == 'T'}" >
setOnSelectBasedMethods(["calcDays(),calcDays1(),updateDateDetailsInlandAgent()"]);
</c:if>
<c:if test="${DateFieldFunctionEnabled == 'F'}" >
setOnSelectBasedMethods(["calcDays(),calcDays1()"]);
</c:if>
setCalendarFunctionality();
</script>

<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>
<script type="text/javascript">
    var fieldName = document.getElementById('field').value;
    var fieldName1 = document.getElementById('field1').value;
	if(fieldName!='' && fieldName.indexOf("servicePartner")!= -1){
		fieldName = fieldName.replace("servicePartner.","");
		document.getElementById(fieldName).className = 'rules-textUpper';
	}
	if(fieldName1!='' && fieldName1.indexOf("servicePartner")!= -1){
		fieldName1 = fieldName1.replace("servicePartner.","");
		document.getElementById(fieldName1).className = 'rules-textUpper';		
	}
 </script> 