<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<head>   
    <title><fmt:message key="previewStorageList.title"/></title>   
    <meta name="heading" content="<fmt:message key='previewStorageList.heading'/>"/> 
 <script language="javascript" type="text/javascript">
  function clear_fields(){
			    document.forms['searchForm'].elements['shipNumbers'].value = "";
			    document.forms['searchForm'].elements['firstName'].value = "";
			    document.forms['searchForm'].elements['cycles'].value = "";
		        document.forms['searchForm'].elements['charges'].value = "";
}

  function quotesRemoval(fieldValue){
	if(fieldValue.value.indexOf("'")>-1){
		var val =document.getElementsByName('shipNumbers')[0].value;
		document.getElementsByName('shipNumbers')[0].value = val.substring(0,val.indexOf("'")) +  val.substring(val.indexOf("'")+1,val.length);
	}

	if(fieldValue.value.indexOf("*")>-1){
		var val =document.getElementsByName('shipNumbers')[0].value;
		document.getElementsByName('shipNumbers')[0].value = val.substring(0,val.indexOf("*")) +  val.substring(val.indexOf("*")+1,val.length);
	}
  }
  function quotesNotAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  if(keyCode==222){
	  return false;
	  }
	  else{
	  return true;
	  }
	}
</script>     
</head>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 150px;
text-align:right;
width:84%;
!width:84%;
}
</style>   
<s:form id="searchForm"  action="previewVatStorageNetworkAgentSearch" method="post" validate="true"  >
<s:set name="previewStorageList" value="previewStorageList" scope="request"/>
<c:if test="${previewStorageList[0].forChecking =='y'}"/>

<div id="">
<ul>
<li><a class="current"><span><b>Total Number of Records are  ${fn:length(previewStorageList)} </b></span></a></li>
</ul>
</div>
<div id="Layer1" style="width: 100%;margin-top:-20px; padding:0px;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<c:set var="searchbuttons">   
			<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" method="previewVatStorageNetworkAgentSearchList" key="button.search" onclick="return quotesRemoval(document.getElementsByName('shipNumbers')[0]);"/>   
    		<input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
		</c:set>
		<div id="content" align="center">
<div id="liquid-round">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
	<table class="table" style="width:100%"  >
	<thead>
		<tr>
		<th><fmt:message key="serviceOrder.shipNumber"/></th>
		<th><fmt:message key="serviceOrder.shipperName"/></th>
		<th><fmt:message key="billing.cycles"/></th>
		<th><fmt:message key="charges.charge"/></th>
        <th></th>
		</tr></thead>	
			<tbody>
				<tr>
				<td width="20" align="left">
			    	<s:textfield name="shipNumbers" required="true" cssClass="input-text" size="12" onkeydown="return quotesNotAllowed(event);"/>
				</td>
				<td width="20" align="left">
			     <s:textfield name="firstName" required="true" cssClass="input-text" size="12"/>
			 </td>
			 
			 <td width="20" align="left">
			     <s:textfield name="cycles" required="true" cssClass="input-text" size="12" onkeydown="return quotesNotAllowed(event);"/>
			 </td>
			 <td width="20" align="left">
			     <s:textfield name="charges" required="true" cssClass="input-text" size="12"/>
			 </td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>
</div>
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Storage Invoice Preview</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<display:table name="previewStorageList" class="table" requestURI="" id="previewStorageListID" style="width:100%" defaultsort="1" export="true"  pagesize="5000" >
    
	<c:choose>
	
	<c:when test="${previewStorageListID.wording=='Contract Mismatch' || previewStorageListID.worldBankAuthorizationNumber=='No Auth avail' || previewStorageListID.cycle=='0'}">
	   <display:column  sortable="true" title="S/O #" style="color:red;">${previewStorageListID.shipNumber}</display:column>
	 	<display:column  sortable="true" title="Lot #" style="color:red;">${previewStorageListID.registrationNumber}</display:column> 
   	    <display:column  sortable="true" title="Shipper"  style="color:red;">${previewStorageListID.lastName}</display:column>
	    <display:column  sortable="true" title="BillTo"  style="color:red;">${previewStorageListID.billToCode}</display:column>
	    <display:column  sortable="true" title="Account Name"  style="color:red;">${previewStorageListID.billToName}</display:column>
		<display:column  title="Billing Amount" style="color:red;text-align: right" >
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" groupingUsed="true" value="${previewStorageListID.storagePerMonth}"/>
	    	 ${previewStorageListID.billingCurrency}
  	    </display:column>
  	    <display:column title="Vat" style="color:red;text-align: right" >
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" groupingUsed="true" value="${previewStorageListID.vatAmount}"/>
  	    </display:column>
  	    <display:column  sortable="true" title="Cycle"  style="color:red;">${previewStorageListID.cycle}</display:column>
	    
	 	<display:column  sortable="true" title="Charge"  style="color:red;">${previewStorageListID.charge}</display:column>
	 	<display:column  sortable="true" title="Description"  style="color:red;">${previewStorageListID.wording}</display:column>
	 	
	 	<display:column  sortable="true" title="Vendor Code"  style="color:red;">${previewStorageListID.vendorCode1}</display:column>
	 	<display:column  sortable="true" title="Vendor Name"  style="color:red;">${previewStorageListID.vendorName1}</display:column>
	 	<display:column  title="Pay Amount"  style="color:red;text-align: right" >
	 		<fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" groupingUsed="true" value="${previewStorageListID.contractExpenseAmount}"/>
	 		${previewStorageListID.vendorBillingCurency}
	 	</display:column>
	 	<display:column title="VAT applied" style="color:red;text-align: right" >
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" groupingUsed="true" value="${previewStorageListID.vendorVatAmount}"/>
  	    </display:column>
	 	<%-- <display:column  sortable="true" title="Vendor Charge"  style="color:red;">${previewStorageListID.insuranceCharge1}</display:column> --%>
	 	
	 	<display:column  sortable="true" title="Auth No."  style="color:red;">${previewStorageListID.billToAuthority}</display:column>
	 	<display:column  sortable="true" title="Dlvr Shipper"  style="color:red;">${previewStorageListID.deliveryShipper}</display:column>
	 	<display:column  sortable="true" title="Dlvr AC"  style="color:red;">${previewStorageListID.deliveryA}</display:column>
	  </c:when>
	  <c:otherwise>
	    <display:column  sortable="true" title="S/O #" style="color:black;">${previewStorageListID.shipNumber}</display:column>
	 	<display:column  sortable="true" title="Lot #" style="color:black;">${previewStorageListID.registrationNumber}</display:column> 
   	    <display:column  sortable="true" title="Shipper"  style="color:black;">${previewStorageListID.lastName}</display:column>
	    <display:column  sortable="true" title="BillTo"  style="color:black;">${previewStorageListID.billToCode}</display:column>
	    <display:column  sortable="true" title="Account Name"  style="color:black;">${previewStorageListID.billToName}</display:column>
		<display:column  title="Billing Amount"  style="color:black;text-align: right;width:100px;">
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" groupingUsed="true" value="${previewStorageListID.contractRevenueAmount}" />
	    	 <c:out value="${previewStorageListID.billingCurrency}"></c:out>
  	    </display:column>
  	    <display:column title="Vat" style="color:black;text-align: right" >
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" groupingUsed="true" value="${previewStorageListID.vatAmount}"/>
  	    </display:column>
  	    <display:column  sortable="true" title="Cycle"  style="color:black;">${previewStorageListID.cycle}</display:column>
	 	<display:column  sortable="true" title="Charge"  style="color:black;">${previewStorageListID.charge}</display:column>
	 	<display:column  sortable="true" title="Description"  style="color:black;">${previewStorageListID.wording}</display:column>
	 	
	 	<display:column  sortable="true" title="Vendor Code"  style="color:black;">
		 		<c:out value="${previewStorageListID.vendorCode1}"></c:out>
	 	</display:column>
	 	<display:column  sortable="true" title="Vendor Name"  style="color:black;">
		 		<c:out value="${previewStorageListID.vendorName1}"></c:out>
		</display:column>
	 	<display:column  title="Pay Amount"  style="color:black;text-align: right;width:100px;" >
	 		<fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" groupingUsed="true" value="${previewStorageListID.contractExpenseAmount}"/>
	 		<c:if test="${not empty previewStorageListID.contractExpenseAmount}">
	 			<c:out value="${previewStorageListID.vendorBillingCurency}"></c:out>
	 		</c:if>
	 	</display:column>
	 	<display:column  title="VAT applied" style="color:black;text-align: right" >
	    	 <fmt:formatNumber type="number" maxFractionDigits="4" minFractionDigits="4" groupingUsed="true" value="${previewStorageListID.vendorVatAmount}"/>
  	    </display:column>
	 	<%-- <display:column  sortable="true" title="Vendor Charge"  style="color:black;">${previewStorageListID.insuranceCharge1}</display:column> --%>
	 	
	 	<display:column  sortable="true" title="Auth No."  style="color:black;">${previewStorageListID.billToAuthority}</display:column>
	 	
	 	<display:column  sortable="true" title="Dlvr Shipper"  style="color:black;">${previewStorageListID.deliveryShipper}</display:column>
	 	<display:column  sortable="true" title="Dlvr AC"  style="color:black;">${previewStorageListID.deliveryA}</display:column>
	</c:otherwise>
	</c:choose> 
	
    <display:setProperty name="export.excel.filename" value="previewStorageListID List.xls"/>   
    <display:setProperty name="export.csv.filename" value="previewStorageListID List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="previewStorageListID List.pdf"/> 
  
</display:table>


	<s:hidden name="radiobExcInc" value='<%=request.getParameter("radiobExcInc") %>'/>
	<s:hidden name="storageBillingList" value='<%=request.getParameter("storageBillingList") %>'/>
	<s:hidden name="radiobilling" value='<%=request.getParameter("radiobilling") %>' />
	<s:hidden name="bookingCodeType" value='<%=request.getParameter("bookingCodeType") %>'/>
	<s:hidden name="billTo2" value='<%=request.getParameter("billTo2") %>'/>
	<s:hidden name="billTo" value='<%=request.getParameter("billTo") %>'/>
	<s:hidden name="radioContractType" value='<%=request.getParameter("radioContractType") %>'/>
</s:form>
<script type="text/javascript"> 
</script>  
				