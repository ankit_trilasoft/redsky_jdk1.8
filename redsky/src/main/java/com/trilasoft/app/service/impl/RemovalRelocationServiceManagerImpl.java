package com.trilasoft.app.service.impl;


import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RemovalRelocationServiceDao;
import com.trilasoft.app.model.RemovalRelocationService;
import com.trilasoft.app.service.RemovalRelocationServiceManager;


public class RemovalRelocationServiceManagerImpl extends GenericManagerImpl<RemovalRelocationService, Long> implements RemovalRelocationServiceManager {   
	RemovalRelocationServiceDao removalRelocationServiceDao;   
  
    public RemovalRelocationServiceManagerImpl(RemovalRelocationServiceDao removalRelocationServiceDao) {   
        super(removalRelocationServiceDao);   
        this.removalRelocationServiceDao = removalRelocationServiceDao;   
    }

}
