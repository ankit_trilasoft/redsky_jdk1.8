package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.UpdateRuleResultDao;
import com.trilasoft.app.model.UpdateRuleResult;
import com.trilasoft.app.service.UpdateRuleResultManager;

public class UpdateRuleResultManagerImpl extends GenericManagerImpl<UpdateRuleResult, Long> implements UpdateRuleResultManager {
	UpdateRuleResultDao updateRuleResultDao;
	public UpdateRuleResultManagerImpl(UpdateRuleResultDao updateRuleResultDao)
			{
		super(updateRuleResultDao);
		this.updateRuleResultDao=updateRuleResultDao;
	}
	
	public List findMaximumId(){
		return updateRuleResultDao.findMaximumId();
	}
}
