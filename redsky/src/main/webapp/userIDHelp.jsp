<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="userIDHelp.title"/></title>
    <meta name="heading" content="<fmt:message key='userIDHelp.heading'/>"/>
    <meta name="menu" content="UserIDHelp"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
  
    <script language="javascript" type="text/javascript">
       function clear_fileds(){
    var i;
			for(i=0;i<=4;i++)
 	{
       	document.forms['userIDHelpForm'].elements['user.firstName'].value = "";
    	document.forms['userIDHelpForm'].elements['user.lastName'].value = "";
    	document.forms['userIDHelpForm'].elements['user.email'].value = "";
    	document.forms['userIDHelpForm'].elements['user.phoneNumber'].value = "";
    }
    }
    </script>
    <style>
    body{ background-color:#dee9f6; text-align:left; margin:0px; padding:0px; background-image:url(images/indexbg-final.gif); background-repeat:repeat-x;}
		#logincontainer{ width:925px; height:417px; background-image:url(images/loginbg-opt.png); background-repeat:no-repeat; margin-top:30px;}
		#errormessage {width:915px; padding-left:20px;}
		#leftcolumn {float:left;margin-top:50px;top:150px;width:450px; }

		#rightcolumn { width:400px; float:right; margin-top:50px; }
    </style>
</head>
<body id="userIDHelp"/>

<form method="post" id="userIDHelpForm" name="userIDHelpForm" action="<c:url value='/j_security_check'/>"
    onsubmit="saveUsername(this);return validateForm(this)">
    
   <div id="wrapper">

<div id="logincontainer">

<div id="indexlogo"><img src="${pageContext.request.contextPath}/images/indexlogo.gif" /></div>

<div id="leftcolumn">
  <table width="360" border="0" align="right" cellpadding="0" cellspacing="0">
  <tr><td height="60px"></td></tr>
    <tr>
     <td align="center" valign="top"><img src="${pageContext.request.contextPath}/images/Id2.gif" HEIGHT=52 WIDTH=292 onclick="notExists();"/></td>
     </tr>
    </table>
</div>

<div id="rightcolumn">
  <table width="300px" border="0" cellspacing="0" cellpadding="0">
  
  <tr>
      <td>&nbsp;</td>
    </tr>
   
    <tr>
      <td width="90" class="content"><label for="j_firstName" class="required desc"><fmt:message key="label.firstName"/></label></td>
      <td colspan="2"> <input type="text" class="textbox" id="user.firstName"  tabindex="1" />
        
     </td>
    </tr>
    <tr>
      <td height="5px"></td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td class="content"> <label for="j_lastName" class="required desc"><fmt:message key="label.lastName"/></label></td>
     			
      <td colspan="2"><input type="text" class="textbox" id="user.lastName" tabindex="2" /> </td>    
    </tr>
    <tr>
      <td height="5px"></td>
      <td colspan="2"></td>
    </tr>
    <tr><td class="content"><label for="j_phoneNumber" class="required desc"><fmt:message key="label.phoneNumber"/>	</label></td>
		<td colspan="2"><input type="text" class="textbox" id="user.phoneNumber" tabindex="3" />  </td>
     </tr>
     <tr>
      <td height="5px"></td>
      <td colspan="2"></td>
    </tr>
    <tr>
    <td class="content"><label for="j_email" class="required desc"><fmt:message key="label.email"/></label></td>
	<td colspan="2"><input type="text" class="textbox" id="user.email" tabindex="4" /></td>
      </label>
    </tr>
     
    <tr>
      <td height="10px"></td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td class="content">&nbsp;</td>
       <td colspan="2">
       <input type="button" onclick="location.href='<c:url value="/passwordHelp.html"/>'" name="submit" value="<fmt:message key='button.submit'/>" tabindex="5" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 " >&nbsp;<input type="button" name="Clear" value="Clear" onclick="clear_fileds();" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 "></td>
    </tr>    
      </table> 
  </div>
</div> 
    
    
    
    
    <!--  
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="24%">&nbsp;</td>
    <td width="52%"><table width="290" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #a2c9f8" >
      <tr>
        <td height="280" valign="top" style="background:url(${pageContext.request.contextPath}/images/loginbg.jpg) top repeat-x #2955b1; "><table width="365" border="0" cellspacing="0" cellpadding="4" style="font:normal 12px arial,verdana; color:#FFFFFF ">
          <tr align="center">
            <td colspan="2"><img src="${pageContext.request.contextPath}/images/rsloginlogo.gif" width="133" height="64"></td>
            </tr>
				<table  class="detailTabLabel" border="0">
	 		  	<tbody>
	 		  			<tr>
	 		  			<td width="100" height="30" align="right"><font color="white"><label for="j_firstName" class="required desc">
            			<fmt:message key="label.firstName"/> <font color="#e30000"><span class="req"><font color="#e30000">*</font></span>
        				</label></font></td>
			 		  	<td align="left"><s:textfield key="user.firstName"  required="true" cssClass="input-text" size="35"/></td>
			 		  			</tr>
			 		  			<tr>
			 		  			<td width="100" height="30" align="right"><font color="white"><label for="j_lastName" class="required desc">
            			<fmt:message key="label.lastName"/> <span class="req"><font color="#e30000">*</font></span>
        				</label></font></td>
			 		  			<td align="left"><s:textfield key="user.lastName" required="true" cssClass="input-text" size="35"/></td>
			 		  			</tr>
			 		  			<tr>
			 		  			<td width="100" height="30" align="right"><font color="white"><label for="j_phoneNumber" class="required desc">
            			<fmt:message key="label.phoneNumber"/> <span class="req"><font color="#e30000">*</font></span>
        				</label></font></td>
			 		  			<td align="left"><s:textfield key="user.phoneNumber" cssClass="input-text" required="true" size="35"/></td>
			 		  			</tr>
			 		  			<tr>
			 		  			<td width="100" height="30" align="right"><font color="white"><label for="j_email" class="required desc">
            			<fmt:message key="label.email"/> <span class="req"><font color="#e30000">*</font></span>
        				</label></font></td>
			 		  			<td align="left"><s:textfield key="user.email" cssClass="input-text" required="true" size="35"/></td>
	 	                </tr> 	  	
	 		  	
	 	  	<tr>
            <td>&nbsp;</td>
          <td><input type="button" onclick="location.href='<c:url value="/passwordHelp.html"/>'" name="submit" value="<fmt:message key='button.submit'/>" tabindex="4" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 " >&nbsp;<input type="button" name="Clear" value="Clear" onclick="clear_fileds();" style="width:60px;font:bold 13px arial,verdana; color:#000; height:28px; padding:0 0 4px 0 "></td>
          </tr>
      </tbody>
      </table>
     
    </table></td>
    <td width="24%">&nbsp;</td>
  </tr>
</table>
-->
</div>
<div class="clear"></div>
</form>
