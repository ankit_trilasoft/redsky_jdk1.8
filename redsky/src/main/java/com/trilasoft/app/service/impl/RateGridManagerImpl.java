package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RateGridDao;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.service.RateGridManager;

public class RateGridManagerImpl extends GenericManagerImpl<RateGrid,Long> implements RateGridManager{
	
	RateGridDao rateGridDao;
	public RateGridManagerImpl(RateGridDao rateGridDao) {
		super(rateGridDao);
		this.rateGridDao=rateGridDao;
		// TODO Auto-generated constructor stub
	}

	public List findMaximumId() {
		return rateGridDao.findMaximumId();
	}
	public List findGridIds(String charge, String contractName, String sessionCorpID){
		return rateGridDao.findGridIds(charge,contractName,sessionCorpID);
	}
	public List getRates(String chid, String contractName,String sessionCorpID){
		return rateGridDao.getRates(chid, contractName,sessionCorpID);
	}
	public int findTwoDNumber(String chargeID, String sessionCorpID){
		return rateGridDao.findTwoDNumber(chargeID, sessionCorpID);
	}
	public List findSaleCommissionRateGrid(String corpId,String contract,String compDiv,String job){
		return rateGridDao.findSaleCommissionRateGrid(corpId, contract, compDiv, job);
	}
	public String findSaleCommissionContractAndCharge(String compDiv,String bucketJobType, String sessionCorpID){
		return rateGridDao.findSaleCommissionContractAndCharge(compDiv, bucketJobType, sessionCorpID);
	}
	
}
