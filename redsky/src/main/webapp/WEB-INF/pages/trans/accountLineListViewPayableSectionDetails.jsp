<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.*"%>
<%@page import="org.appfuse.model.Role"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = userRole+","+role.getName(); 
}
//System.err.println("\n\n\n\n user role---->"+userRole);
%> 

<c:if test="${fn:contains(userRole, 'ROLE_EXECUTIVE')}">
<s:hidden name="userRoleExecutive" value="yes"/>
</c:if>

<c:if test="${not fn:contains(userRole, 'ROLE_EXECUTIVE')}">
<s:hidden name="userRoleExecutive" value="no"/>
</c:if>

<!-- Code Commented by Ankit for 13835 start-->
<%if(userRole.contains("ROLE_SUPERVISOR")){ %>
	<c:set var="roleSupervisorVal" value="Y"/>
<%}else{%>
	<c:set var="roleSupervisorVal" value="N"/>
<%}%>
<!-- Code Commented by Ankit for 13835 End-->

<!-- Code Added by Ankit for 13835 start-->
<%-- <c:if test="${fn:contains(userRole, 'ROLE_SUPERVISOR')}">
<c:set var="roleSupervisorVal" value="Y"/>
</c:if>

<c:if test="${not fn:contains(userRole, 'ROLE_SUPERVISOR')}">
<c:set var="roleSupervisorVal" value="N"/>
</c:if> --%>
<!-- Code Added by Ankit for 13835 End-->

<title>Payable Detail</title>
<meta name="heading" content="Payable Detail" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}
</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>

<style type="text/css">
 #overlay {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>

    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
	<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
	<script type="text/javascript">
		var openDiv = '${clickType}';
		if(openDiv.trim()=='Pay'){
			animatedcollapse.addDiv('payable', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('receivable', 'fade=0,persist=0,hide=1') 
			animatedcollapse.init()
		}else if(openDiv.trim()=='Rec'){
			animatedcollapse.addDiv('payable', 'fade=0,persist=0,hide=1')
			animatedcollapse.addDiv('receivable', 'fade=0,persist=0,show=1')
			animatedcollapse.init()
		}
	</script>
<!-- Modification closed here -->
<SCRIPT LANGUAGE="JavaScript">
function savePayableDetails(){
	 calculatePayRate()
 }
 function savePayableDetailsForms(){
	 	var url = "savePricingPayableDetails.html";
		document.forms['pricingPayableForm'].action =url;
		document.forms['pricingPayableForm'].submit();
		winClose();
 }
 function winClose(){
	 window.close();
 }
</script>


<s:form id="pricingPayableForm" name="pricingPayableForm" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="aid" value="${accountLine.id}" />
<s:hidden name="sid" value="<%=request.getParameter("sid")%>" />
<s:hidden name="masterId"  value="<%=request.getParameter("sid")%>"/>
<s:hidden id="checkForDaysClick" name="checkForDaysClick" />
<s:hidden id="checkReceivedInvoiceDate" name="checkReceivedInvoiceDate" />
<s:hidden id="checkActSenttoclient" name="checkActSenttoclient" />
<s:hidden name="chargeCodeValidationVal" id="chargeCodeValidationVal" />
<s:hidden name="billingCurrencyUpdateFlag" value="NO"/>
<s:hidden id="dateUpdate" name="dateUpdate" />
<s:hidden id="writeOffReasonVal" name="writeOffReasonVal" />
<s:hidden name="username" value="${user.username}" />
<s:hidden name="formStatus"/>

<s:hidden name="accountLine.payVatDescr"/>
<s:hidden name="accountLine.recVatDescr"/>
<s:hidden name="accountLine.actualDiscount"/>
<s:hidden name="accountLine.recQuantity"/>
<s:hidden name="accountLine.recRate"/>
<s:hidden name="accountLine.billToCode"/>
<s:hidden name="accountLine.category"/> 
<s:hidden name="accountLine.actualExpense"/>
<s:hidden name="accountLine.varianceExpenseAmount"/>
<s:hidden name="accountLine.estimateVendorName"/>
<s:hidden name="accountLine.actgCode"/>
<s:hidden name="accountLine.vendorCode"/>
<s:hidden name="accountLine.chargeCode"/>
<s:hidden name="accountLine.actualRevenue"/>
<s:hidden name="accountLine.revisionExpense"/>
<s:hidden name="accountLine.estimateRevenueAmount"/>
<s:hidden name="accountLine.revisionRevenueAmount"/>
<s:hidden name="accountLine.basis"/>
<s:hidden name="accountLine.branchCode"/>
<s:hidden name="accountLine.branchName"/>

<div id="layer1" style="width:100%">
 <div id="pnav">
				  <ul>
				    <li><a class=""><span>Payable&nbsp;&&nbsp;Receivable&nbsp;Detail</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:10px; "><span></span></div>
    <div class="center-content">
    <table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
				<tbody>
				<tr>
					<td height="10" width="100%" align="left" style="margin: 0px">
  					<div onClick="javascript:animatedcollapse.toggle('payable');" style="margin: 0px">
	      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
						<tr>
						<td class="headtab_left">
						</td>
						<td NOWRAP class="headtab_center">&nbsp;&nbsp;Payable&nbsp;Detail
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_center">&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
					</div>
					<div id="payable">
    				<table class="detailTabLabel" style="margin:0px;" border="0" class="">
    				  <tr>
					 <td align="right"  class="listwhitetext">Line #</td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="accountLine.accountLineNumber" cssStyle="width:95px;" /></td>
					<td align="right"  class="listwhitetext">Active</td>
					<td align="left" >
						<c:if test="${accountLine.status}">
							<input type="checkbox" style="margin:0px;" checked="checked" disabled="disabled" />
						</c:if>
						<c:if test="${accountLine.status==false}">
							<input type="checkbox" style="margin:0px;" disabled="disabled" />
						</c:if>
					</td>
					 </tr>
					   
					<sec-auth:authComponent componentId="module.accountLine.section.payableDetail.edit" >
						<%;
						String sectionPayableDetail="true";
						int permissionPayable  = (Integer)request.getAttribute("module.accountLine.section.payableDetail.edit" + "Permission");
					 	if (permissionPayable > 2 ){
					 		sectionPayableDetail = "false";
					 		//System.err.println("\n\n\n\n sectionPayableDetail false---->"+sectionPayableDetail);
					 	}
					 	//System.err.println("\n\n\n\n sectionPayableDetail true---->"+sectionPayableDetail);
					  %>			
					 
					 <tr>
						<td width="90" align="right" class="listwhitetext"><fmt:message key="accountLine.invoiceNumber" /></td>
						<td align="left" class="listwhitetext"><s:textfield readonly="<%=sectionPayableDetail%>" cssClass="input-text" key="accountLine.invoiceNumber" onkeydown="return onlyforInvoice(event)"
							cssStyle="width:95px;" maxlength="25" onchange="checkVendorInvoice('INV');assignInvoiceDate();" tabindex="38" /></td>
						<td align="right" class="listwhitetext"><fmt:message key="accountLine.invoiceDate" /></td> 
						<c:if test="${not empty accountLine.invoiceDate}">
							<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.invoiceDate" /></s:text>
							<c:if test="${empty accountLine.payAccDate}">
							<td><s:textfield tabindex="" cssClass="input-text" id="invoiceDate" name="accountLine.invoiceDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDelForInvoiceDate(event,this)" readonly="true" />
							<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
							<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkAcctStatus(this.id);"/>
						    <%} %>
						    </c:if>
						    <c:if test="${not empty accountLine.payAccDate}">
						    <td><s:textfield tabindex="" cssClass="input-text" id="invoiceDate" name="accountLine.invoiceDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" onkeydown="" readonly="true"/>
							<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
							<img id="invoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
							 <%} %>
							</c:if>
						    </td>
						</c:if>
						<c:if test="${empty accountLine.invoiceDate}">
							<td><s:textfield tabindex="" cssClass="input-text" id="invoiceDate" name="accountLine.invoiceDate" required="true" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDelForInvoiceDate(event,this)" readonly="true"  onfocus="cal.select(document.forms['pricingPayableForm'].invoiceDate,'calender6',document.forms['pricingPayableForm'].dateFormat2.value); return false;"/>
								<c:if test="${empty accountLine.payAccDate}">
								<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
								<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkAcctStatus(this.id);"/>
							<%} %>
							</c:if>
							 <c:if test="${not empty accountLine.payAccDate}">
							 <% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
							<img id="invoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
							 <%} %>
							 </c:if>
							</td>
						</c:if>
						<configByCorp:fieldVisibility componentId="accountLine.serviceTaxInput"> 
						<td align="right" class="listwhitetext">Service&nbsp;Tax&nbsp;Input
						</td></configByCorp:fieldVisibility> 
						<c:set var="serviceTaxInput" value="false" />
					    <c:if test="${accountLine.serviceTaxInput}">
							<c:set var="serviceTaxInput" value="true" />
						</c:if>
						<configByCorp:fieldVisibility componentId="accountLine.serviceTaxInput">
						<td align="left" class="listwhitetext">
						<s:checkbox name="accountLine.serviceTaxInput" cssStyle="margin:0px;" value="${serviceTaxInput}" fieldValue="true" onchange="" />
						</td>
						</configByCorp:fieldVisibility>   															
						<td align="right" class="listwhitetext">Document</td>
						<td align="left" colspan="3"><s:select  cssClass="list-menu" name="accountLine.myFileFileName" cssStyle="width:150px" list="%{myfileDocumentList}" headerKey="" headerValue="" tabindex="" />
						&nbsp;<input id="ViewBtn" type="button" class="pricelistbtn" style="width:50px; height:19px;line-height:12px;" name="ViewBtn" value="View" onclick="detailsView();"  /></td>
	                 </tr>
											                 
						<tr> 
                            <td align="right"  class="listwhitetext"><fmt:message key="accountLine.receivedDate" /></td>
							<c:if test="${not empty accountLine.receivedDate}">
								<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.receivedDate" /></s:text>
								<c:if test="${empty accountLine.payAccDate}">
								<td width="130px"><s:textfield  cssClass="input-text" id="receivedDate" name="accountLine.receivedDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
									
									<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
									<img id="receivedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
								<%} %>
								</c:if>
								<c:if test="${not empty accountLine.payAccDate}">
								<td width="130px"><s:textfield  cssClass="input-text" id="receivedDate" name="accountLine.receivedDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" onkeydown=""readonly="true" />
								<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
								<img id="receivedDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
								 <%} %>
								</c:if>
								</td>
							</c:if>
							<c:if test="${empty accountLine.receivedDate}">
								<td width="130px"><s:textfield  cssClass="input-text" id="receivedDate" name="accountLine.receivedDate" required="true" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)" readonly="true" />
									<c:if test="${empty accountLine.payAccDate}">
									<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
									<img id="receivedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
									<%} %>
									</c:if>
									<c:if test="${not empty accountLine.payAccDate}">
									<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
									<img id="receivedDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
								 <%} %>
								 </c:if>
								</td> 
							</c:if> 
							<td width="63" align="right" class="listwhitetext"><fmt:message key="accountLine.payingStatus" /></td>
							<c:if test="${empty accountLine.payAccDate}">
							<td align="left"><s:select  disabled="<%=sectionPayableDetail%>" cssClass="list-menu" name="accountLine.payingStatus" cssStyle="width:100px" list="%{payingStatus}" headerKey="" headerValue="" tabindex="43" onchange="checkVendorInvoice('PAY');"/></td>
						    </c:if>
						    <c:if test="${not empty accountLine.payAccDate}">
						    <td align="left"><s:select  disabled="<%=sectionPayableDetail%>" cssClass="list-menu" name="accountLine.payingStatus" cssStyle="width:100px" list="%{payingStatus}" headerKey="" headerValue="" tabindex="43" onchange="" onclick="checkPayAcc()"/></td>
						    </c:if>
						    <configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
						    <c:if test="${writeOffReasonFlag}"> 
						    <td width="63" align="right" class="listwhitetext">Write&nbsp;Off&nbsp;Reason</td>
						    <c:if test="${empty accountLine.payPostDate}"> 
						    <td align="left" colspan="2"><s:select list="%{writeoffReasonList}" cssClass="list-menu" cssStyle="width:150px" headerKey=" " headerValue=" " name="accountLine.writeOffReason" onselect="checkwriteOffReasonValidation();" onchange="findBucket2WriteOffReason();"/></td>
						    </c:if> 
						    <c:if test="${not empty accountLine.payPostDate}">
						    <td align="left" class="listwhitetext"><s:textfield readonly="true" cssStyle="width:145px" cssClass="input-textUpper" key="accountLine.writeOffReason"/></td>
						    </c:if>
						     </c:if>
						    </configByCorp:fieldVisibility>
						    <%-- <td colspan="3">
							<div id="payableDeviationShow" > 
                             <table class="detailTabLabel" style="margin:0px;padding:0px;"><tr>
                             <td width="71"> </td>
                             <td align="right" class="listwhitetext" >Buy&nbsp;Deviation</td>
                             <c:if test="${empty accountLine.payAccDate}">
                             <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" readonly="<%=sectionPayableDetail%>" cssStyle="text-align:right" key="accountLine.payDeviation" size="5" maxlength="10" onkeydown="return onlyRateAllowed(event)"  onchange="convertedDeviationAmount();" tabindex="17" />%</td>
                             </c:if>
                              <c:if test="${not empty accountLine.payAccDate}">
                             		<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" readonly="true" cssStyle="text-align:right" key="accountLine.payDeviation" size="5" tabindex="17" />%</td>
                              </c:if>
                             <s:hidden name="oldPayDeviation" value="${accountLine.payDeviation}"/>
                             </tr></table>
                             </div>
							</td> --%>
							<c:choose>
								<c:when test="${(serviceOrder.grpStatus=='Finalized' || serviceOrder.grpStatus=='finalized')  && (serviceOrder.controlFlag=='G') && (accountLine.status) }"> 
								<td align="left" colspan="3">
									<input type="button" class="pricelistbtn"   style="width:155px; height:25px" name="distributePayableStatus" value="Distribute groupage cost" onclick="distributeOrdersAmount('UnderPayableStatus');">
								</td>
								</c:when>
								<c:otherwise>
								<tr></tr>
								</c:otherwise>
							</c:choose>
						</tr>
						
					<c:if test="${contractType}">
						 <tr>
						<td colspan="8" class="vertlinedata"></td>
						 </tr>
						 <tr>															
						 <td align="right" class="listwhitetext" width="12%">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
						<c:if test="${empty accountLine.payAccDate}">
						 <td align="left"><s:select  cssClass="list-menu"  key="accountLine.payableContractCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.payableContractExchangeRate');calculatePayCurrency('accountLine.payableContractRateAmmount','accountLine.payableContractCurrency');" headerValue="" tabindex="12"  /></td>
						 </c:if>
						 <c:if test="${not empty accountLine.payAccDate}">
						 <td align="left"><s:select  cssClass="list-menu"  key="accountLine.payableContractCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onclick="checkPayAccPayableContractCurrency()" onfocus="checkPayAccPayableContractCurrency()" headerValue="" tabindex="12"  /></td>
						 </c:if>
						 <td align="right"   class="listwhitetext">Value&nbsp;Date</td>
						 <c:if test="${not empty accountLine.payableContractValueDate}"> 
	                       <s:text id="accountLineFormattedPayableContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.payableContractValueDate"/></s:text>
	                          <td width="97"><s:textfield id="payableContractValueDate" name="accountLine.payableContractValueDate" value="%{accountLineFormattedPayableContractValueDate}" onkeydown="" readonly="true" cssClass="input-text" size="7"/>
	                          <c:if test="${empty accountLine.payAccDate}">
	                          <img id="payableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	                          </c:if> 
	                           <c:if test="${not empty accountLine.payAccDate}">
	                           <img id="payableContractValueDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')"/>
	                           </c:if>
	                          </td>
	                        </c:if>
	                        <c:if test="${empty accountLine.payableContractValueDate}">
	                      <td width="97"><s:textfield id="payableContractValueDate" name="accountLine.payableContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="7"/>
	                      <c:if test="${empty accountLine.payAccDate}">
	                      <img id="payableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
	                      </c:if>
	                      <c:if test="${not empty accountLine.payAccDate}">
	                      <img id="payableContractValueDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')"/>
	                      </c:if>
	                      </td>
	                        </c:if> 				                                           
	                     <td  align="right" class="listwhitetext" width="">Contract&nbsp;Exchange&nbsp;Rate</td>
						 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.payableContractExchangeRate" size="6" maxlength="10"   onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculatePayCurrency('accountLine.payableContractRateAmmount');" onblur=""/></td>   			
	                                        
	                     <td align="right" class="listwhitetext" width="">&nbsp;Contract&nbsp;Amount</td>
					     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.payableContractRateAmmount" size="8" maxlength="12"  onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculatePayCurrency('accountLine.payableContractRateAmmount');" onblur=""/></td>	 
						</tr>
						<tr>
						<td colspan="8" class="vertlinedata"></td>
						 </tr>
					 </c:if>
							
					<tr>
					   <c:choose>
					   <c:when test="${contractType}">
					   <td align="right" class="listwhitetext"><fmt:message key="accountLine.country" /><font color="red" size="2">*</font></td>
					   </c:when>
					    <c:otherwise>
						<td align="right" class="listwhitetext"><fmt:message key="accountLine.country" /></td>
						</c:otherwise></c:choose>
						<c:if test="${empty accountLine.payAccDate}">
						<td align="left" ><s:select  disabled="<%=sectionPayableDetail%>" cssClass="list-menu"  key="accountLine.country" cssStyle="width:100px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.exchangeRate');calculatePayCurrency('accountLine.payableContractRateAmmount','accountLine.country');" headerValue="" tabindex="40"/></td>
	                        </c:if>
	                        <c:if test="${not empty accountLine.payAccDate}">
	                        <td align="left" ><s:select disabled="<%=sectionPayableDetail%>" cssClass="list-menu"  key="accountLine.country" cssStyle="width:100px" list="%{country}" headerKey="" onchange="" headerValue="" tabindex="40" onclick="checkPayAccCountry();" onfocus="checkPayAccCountry();"/></td>
	                        </c:if>
	                         <td align="right" class="listwhitetext"><fmt:message key="accountLine.valueDate" /></td> 
						<c:if test="${not empty accountLine.valueDate}">
							<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.valueDate" /> </s:text>
							<c:if test="${empty accountLine.payAccDate}">	
							<td><s:textfield  cssClass="input-text" id="valueDate" name="accountLine.valueDate" value="%{accountLineFormattedValue}" cssStyle="width:95px" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
			                   <% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
								<img id="valueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							<%} %>
							</c:if>
							<c:if test="${not empty accountLine.payAccDate}">
							<td><s:textfield  cssClass="input-text" id="valueDate" name="accountLine.valueDate" value="%{accountLineFormattedValue}" cssStyle="width:95px" maxlength="12" onkeydown=""readonly="true" />
							<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
							 <img id="valueDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
							 <%} %>
							</c:if>
							</td>
						</c:if>
						<c:if test="${empty accountLine.valueDate}">
							<td><s:textfield  cssClass="input-text" id="valueDate" name="accountLine.valueDate" required="true" cssStyle="width:95px" maxlength="12" onkeydown="return onlyDel(event,this)" readonly="true"/>
							<c:if test="${empty accountLine.payAccDate}">	
							 <% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
							 <img id="valueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
							<%} %>
							</c:if>
							<c:if test="${not empty accountLine.payAccDate}">
							<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
							<img id="valueDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
							 <%} %>
							</c:if>
						</td>
						</c:if>
						<td width="" align="right" class="listwhitetext"><fmt:message key="accountLine.exchangeRate" /></td>
						<td><s:textfield readonly="<%=sectionPayableDetail%>" cssClass="input-text" cssStyle="text-align:right;width:95px;" key="accountLine.exchangeRate" size="6" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)" onchange="changeStatus();calculatePayCurrency('accountLine.payableContractRateAmmount');" tabindex="39"/></td>
						<td align="right" class="listwhitetext" >Curr&nbsp;Amount</td>
						<td><s:textfield readonly="<%=sectionPayableDetail%>" cssClass="input-text" cssStyle="text-align:right;width:95px;" key="accountLine.localAmount" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculatePayCurrency('accountLine.localAmount');" tabindex="41"/></td>
						<c:if test="${contractType}">
						 <tr><td colspan="8" class="vertlinedata"></td></tr>
						 </c:if>
						</tr>
															
							<tr>
								 <td align="right" class="listwhitetext"><fmt:message key="accountLine.note" /></td>
								<td colspan="3" align="left" class="listwhitetext"><s:textarea readonly="<%=sectionPayableDetail%>" name="accountLine.note" cssStyle="width:297px; height:40px;" tabindex="46"  cssClass="textarea"/></td>
								<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0||fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 
								  <td align="right" class="listwhitetext"><fmt:message key="accountLine.commission" /></td>
								   <td align="left" ><s:select  disabled="<%=sectionPayableDetail%>" cssClass="list-menu" name="accountLine.commission" cssStyle="width:107px" list="%{getCommissionList}" tabindex="44" onchange=""/></td> 
								   <td><input id="commissionBtn" type="button" class="cssbuttonA" style="width:80px; height: 25px;" name="commissionBtn" value="Commission" onclick="getCommissionAmtFromBCode();" tabindex="45" /></td>
							     </c:if>
							     
							     <configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
							     <td align="right"  class="listwhitetext">Manager Approval</td>
							     	<c:if test="${roleSupervisorVal=='Y'}">
										<c:if test="${not empty accountLine.managerApprovalDate}">
											<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.managerApprovalDate" /></s:text>
											<td width=""><s:textfield  cssClass="input-text" id="managerApprovalDate" name="accountLine.managerApprovalDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
												<img id="managerApprovalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="triggerCalendarId(this.id);"/>
										  	</td>
										  </c:if>
										<c:if test="${empty accountLine.managerApprovalDate}">
											<td width=""><s:textfield  cssClass="input-text" id="managerApprovalDate" name="accountLine.managerApprovalDate" required="true" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
												<img id="managerApprovalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="triggerCalendarId(this.id);"/>
											</td>
										</c:if>
									</c:if>
									<c:if test="${roleSupervisorVal!='Y'}">
										<c:if test="${not empty accountLine.managerApprovalDate}">
											<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.managerApprovalDate" /></s:text>
											<td width=""><s:textfield  cssClass="input-text" id="managerApprovalDate" name="accountLine.managerApprovalDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" readonly="true" />
										  	</td>
										  </c:if>
										<c:if test="${empty accountLine.managerApprovalDate}">
											<td width=""><s:textfield  cssClass="input-text" id="managerApprovalDate" name="accountLine.managerApprovalDate" required="true" cssStyle="width:95px;" maxlength="12" readonly="true" />
											</td>
										</c:if>
									</c:if>
									
    								<td align="right" class="listwhitetext">Manager Approved</td>
    								<td align="left"><s:textfield name="accountLine.managerApprovalName" cssClass="input-textUpper" cssStyle="width:95px"  readonly="true"/></td>
							     </configByCorp:fieldVisibility>
								</tr>
								

							<tr>
								 <td align="right" class="listwhitetext" ><fmt:message key="accountLine.payPayableStatus" /></td>
								 <td align="left"><s:select  disabled="<%=sectionPayableDetail%>" list="%{paymentStatus}" cssClass="list-menu"  name="accountLine.payPayableStatus" headerKey="" cssStyle="width:100px" headerValue="" onchange="autoPayPayableDate(this);" tabindex="50"/></td>
								 <td align="right"  class="listwhitetext"><fmt:message key="accountLine.payPayableDate" /></td>
									<c:if test="${not empty accountLine.payPayableDate}">
										<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.payPayableDate" /></s:text>
										<td width=""><s:textfield  cssClass="input-text" id="payPayableDate" name="accountLine.payPayableDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
										<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
										<img id="payPayableDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									  <%} %>
									  </c:if>
									<c:if test="${empty accountLine.payPayableDate}">
										<td width=""><s:textfield  cssClass="input-text" id="payPayableDate" name="accountLine.payPayableDate" required="true" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
										<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
										<img id="payPayableDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									<%} %>
									</c:if>  
									<td align="right" class="listwhitetext" width="107"><fmt:message key="accountLine.payPayableVia" /></td>
									<td><s:textfield cssClass="input-text" key="accountLine.payPayableVia" cssStyle="width:95px;" maxlength="25" tabindex="51"/></td>
									<c:if test="${accountInterface=='Y'}"> 
									<td width="" align=right class="listwhitetext" ><fmt:message key="accountLine.payPayableAmount" /></td>
									<td><s:textfield cssClass="input-text" key="accountLine.payPayableAmount" cssStyle="width:95px;text-align:right;" maxlength="17" tabindex="52"/></td>		
						            </c:if>
						            <c:if test="${accountInterface!='Y'}">
						            <td width="" align="right" class="listwhitetext" ><fmt:message key="accountLine.payPayableAmount" /></td>
									<td><s:textfield cssClass="input-text" key="accountLine.payPayableAmount" cssStyle="width:95px;" maxlength="17" tabindex="52" readonly="true" /></td>		 
						            </c:if>
						            
								</tr> 
		
							</sec-auth:authComponent>
							</table>
									
							<sec-auth:authComponent componentId="module.accountLine.section.payableDetailDate.edit">
							<%;
							String PayableDetailDate="true";
							int permissionPayableDate  = (Integer)request.getAttribute("module.accountLine.section.payableDetailDate.edit" + "Permission");
						 	if (permissionPayableDate > 2 ){
						 		PayableDetailDate = "false";
						 	}	
						  %>
						  <table width="100%" border="0" style="margin:0px;" cellpadding="0" cellspacing="0">
							<tbody><tr><td align="left" class="vertlinedata"></td></tr></tbody>
							</table>
							<table width="100%" class="payablebg" style="margin-bottom:1px;border-radius:3px;" border="0">
							<tr>
								<td width="90px"align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.payGl"><fmt:message key="charges.glCode"/></configByCorp:fieldVisibility></td>  
                                                     <td align="left" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.payGl"><s:textfield cssClass="input-text" key="accountLine.payGl" cssStyle="width:95px;"  maxlength="7" tabindex="52" readonly="true"/></configByCorp:fieldVisibility></td> 
								<c:if test="${accountInterface=='Y'}">
								<td align="right" class="listwhitetext" width="50px"><configByCorp:fieldVisibility componentId="accountLine.payPostDate"><fmt:message key="accountLine.recPostDate"/></configByCorp:fieldVisibility></td>  
                                        <c:if test="${not empty accountLine.payPostDate}">
										<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.payPostDate" /></s:text>
										<c:if test="${empty accountLine.payAccDate}">
										<td class="listwhitetext" width="126"><configByCorp:fieldVisibility componentId="accountLine.payPostDate"><s:textfield cssClass="input-text" id="payPostDate" name="accountLine.payPostDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;"
											maxlength="12"  onkeydown="return onlyDel(event,this)" readonly="true"/>
											<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
											
										<img id="payPostDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
										<%} %>
										</configByCorp:fieldVisibility></td>
										</c:if>
										<c:if test="${not empty accountLine.payAccDate}">
										<td class="listwhitetext" width="126"><configByCorp:fieldVisibility componentId="accountLine.payPostDate"><s:textfield cssClass="input-text" id="payPostDate" name="accountLine.payPostDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;"
											maxlength="12"  onkeydown="" readonly="true"/>
											<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
										<img id="payPostDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" /></td>
										<%} %>
										</configByCorp:fieldVisibility></td>
										</c:if>
									 
										
									 </c:if>
									 <c:if test="${empty accountLine.payPostDate}">
										<td class="listwhitetext" width="126"><configByCorp:fieldVisibility componentId="accountLine.payPostDate"><s:textfield cssClass="input-text" id="payPostDate" name="accountLine.payPostDate" required="true" cssStyle="width:95px;" maxlength="12"
											 onkeydown="return onlyDel(event,this)"readonly="true"/> 
											<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
										<c:if test="${empty accountLine.payAccDate}">	
										<img id="payPostDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
										</c:if>
										<c:if test="${not empty accountLine.payAccDate}">
										<img id="payPostDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" /></td>
										</c:if>
									<%} %> 
										  </configByCorp:fieldVisibility></td>
									</c:if> 
									</c:if>									
									
									</tr>
									
									<tr> 
								<td align="right" class="listwhitetext" ><configByCorp:fieldVisibility componentId="accountLine.accruePayable"><fmt:message key="accountLine.accruePayable" /></configByCorp:fieldVisibility></td> 
										<c:if test="${not empty accountLine.accruePayable}">
										<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accruePayable" /></s:text>
										<td class="listwhitetext" width="100px"><configByCorp:fieldVisibility componentId="accountLine.accruePayable"><s:textfield cssClass="input-text" id="accruePayable" name="accountLine.accruePayable" value="%{accountLineFormattedValue}" cssStyle="width:95px;"
											maxlength="12"  onkeydown="" readonly="true"/>
										</configByCorp:fieldVisibility></td>
									 </c:if>
									 <c:if test="${empty accountLine.accruePayable}">
										<td class="listwhitetext" width="100px"><configByCorp:fieldVisibility componentId="accountLine.accruePayable"><s:textfield cssClass="input-text" id="accruePayable" name="accountLine.accruePayable" required="true" cssStyle="width:95px;" maxlength="12"
											 onkeydown=""readonly="true"/> 
										</configByCorp:fieldVisibility></td>
									</c:if>  
                                 <c:if test="${accountInterface=='Y'}">
								   
                                        <td align="right"  class="listwhitetext" width="68"><fmt:message key="accountLine.accruePayableReverse" /></td>
                                      <c:if test="${not empty accountLine.accruePayable }"> 
                                       <c:if test="${not empty accountLine.accruePayableReverse}"> 
                                       <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accruePayableReverse"/></s:text>
                                       <td><s:textfield id="accruePayableReverse" name="accountLine.accruePayableReverse" value="%{accountLineFormattedValue}" onkeydown="" readonly="true"  cssClass="input-text" cssStyle="width:95px;" />
                                        </td>
                                       </c:if>
                                        <c:if test="${empty accountLine.accruePayableReverse}">
                                           <td><s:textfield id="accruePayableReverse" name="accountLine.accruePayableReverse" onkeydown="" readonly="true" cssClass="input-text"  cssStyle="width:95px;" />
                                        </td>
                                        </c:if>
                                        </c:if>
                                        <c:if test="${empty accountLine.accruePayable }"> 
                                        <c:if test="${not empty accountLine.accruePayableReverse}"> 
                                       <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accruePayableReverse"/></s:text>
                                       <td><s:textfield id="accruePayableReverse" name="accountLine.accruePayableReverse" value="%{accountLineFormattedValue}"  readonly="true" cssClass="input-text"  cssStyle="width:95px;" />
                                       </td>
                                       </c:if>
                                        <c:if test="${empty accountLine.accruePayableReverse}">
                                           <td><s:textfield id="accruePayableReverse" name="accountLine.accruePayableReverse"  readonly="true" cssClass="input-text"  cssStyle="width:95px;" />
                                        </td>
                                        </c:if>
                                        </c:if> 
                                        
                                         <sec-auth:authComponent componentId="module.accountLine.section.payableDetailAccrual.edit">
									<%;
									String payableDetailAccrual="true";
									int permissionPayAccural  = (Integer)request.getAttribute("module.accountLine.section.payableDetailAccrual.edit" + "Permission");
								 	if (permissionPayAccural > 2 ){
								 		payableDetailAccrual = "false";	
								 	}
								  %>
                                         <td align="right"  class="listwhitetext" width="106"><fmt:message key="accountLine.accruePayableManual" /></td>
                                         <c:choose>
                                       <c:when test="${empty accountLine.accruePayable }">
                                      <td align="left" colspan="2"><s:checkbox name="accountLine.accruePayableManual" value="${accountLine.accruePayableManual}" fieldValue="true" cssStyle="margin:0px;" disabled="true" onchange=""/></td>
                                       </c:when>
                                       <c:when test="${not empty accountLine.accruePayable && empty accountLine.accruePayableReverse}">
                                        <td align="left" colspan="2"><s:checkbox name="accountLine.accruePayableManual" value="${accountLine.accruePayableManual}" fieldValue="true" cssStyle="margin:0px;" disabled="<%=payableDetailAccrual%>" onchange=""/></td>
                                       </c:when>
                                       <c:otherwise>
                                       <td align="left" colspan="2"><s:checkbox name="accountLine.accruePayableManual" value="${accountLine.accruePayableManual}" fieldValue="true" cssStyle="margin:0px;" disabled="true" onchange=""/></td>
                                       </c:otherwise>
                                       </c:choose> 
  									</sec-auth:authComponent> 
                                        </c:if> 
										</tr>
										<tr>
										
									
									<c:if test="${accountInterface=='Y'}">
											<td width="" align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.payAccDate"><fmt:message key="accountLine.recAccDate"/></configByCorp:fieldVisibility></td>  
                                                        <c:if test="${not empty accountLine.payAccDate}">
												<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.payAccDate" /></s:text>
												<% if (!(PayableDetailDate.equalsIgnoreCase("false"))){%>
												<td class="listwhitetextAcct" colspan="1" width="125"><configByCorp:fieldVisibility componentId="accountLine.payAccDate"><s:textfield cssClass="input-text" id="payAccDate" name="accountLine.payAccDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;"
													 maxlength="12"   readonly="true"/> 
													 </configByCorp:fieldVisibility></td>
											<%} %>
											<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
											<td class="listwhitetext" colspan="1"  width="125"><configByCorp:fieldVisibility componentId="accountLine.payAccDate"><s:textfield cssClass="input-text" id="payAccDate" name="accountLine.payAccDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;"
													 maxlength="12"  onkeydown="return onlyDel(event,this)" readonly="true"/> 
												<img id="payAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
											</configByCorp:fieldVisibility></td>
											<%} %>
											
											 </c:if>
											 <c:if test="${empty accountLine.payAccDate}">
												<td class="listwhitetext" colspan="1"  width="125"><configByCorp:fieldVisibility componentId="accountLine.payAccDate"><s:textfield cssClass="input-text" id="payAccDate" name="accountLine.payAccDate" required="true" cssStyle="width:95px;" maxlength="12"
													 onkeydown="return onlyDel(event,this)"readonly="true"/> 
												<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
												<img id="payAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
											<%} %>
												  </configByCorp:fieldVisibility></td>
											</c:if> 
											</c:if> 
										
											<td align="right"  class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.payXfer"><fmt:message key="accountLine.recXfer"/></configByCorp:fieldVisibility></td>  
                                            <td align="left" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.payXfer"><s:textfield cssClass="input-text" tabindex="53" key="accountLine.payXfer" cssStyle="width:95px;"  maxlength="20" readonly="<%=PayableDetailDate%>" /></configByCorp:fieldVisibility></td> 	

                                           <c:if test="${accountInterface=='Y'}">
                                            <td height="20" align="right" class="listwhitetext"><fmt:message key="accountLine.payXferUser"/></td>  
                                            <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.payXferUser" tabindex="54" cssStyle="width:95px;"  maxlength="7" readonly="<%=PayableDetailDate%>" /></td> 	 	 
										</c:if>
										<script type="text/javascript"> 
                                          //disableBYPayAcc()    
                                       </script>
							<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.settledDate">Settled Date</configByCorp:fieldVisibility></td>
							<c:if test="${not empty accountLine.settledDate}">
								<s:text id="accountLineSettledDateValue2" name="${FormDateValue}">
									<s:param name="value" value="accountLine.settledDate" />
								</s:text>
								<td width="50"><configByCorp:fieldVisibility componentId="accountLine.settledDate"><s:textfield cssClass="input-text" id="settledDate" readonly="true" name="accountLine.settledDate" value="%{accountLineSettledDateValue2}" cssStyle="width:95px;" maxlength="11" required="true"/></configByCorp:fieldVisibility></td>
								<td align="left" style="padding-top:1px;"><configByCorp:fieldVisibility componentId="accountLine.settledDate"><img id="settledDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
							</c:if>
							<c:if test="${empty accountLine.settledDate}">
								<td width="50"><configByCorp:fieldVisibility componentId="accountLine.settledDate"><s:textfield cssClass="input-text" id="settledDate" readonly="true" name="accountLine.settledDate" cssStyle="width:95px;" maxlength="11" required="true" /></configByCorp:fieldVisibility></td>
								<td align="left" style="padding-top:1px;"><configByCorp:fieldVisibility componentId="accountLine.settledDate"><img id="settledDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
							</c:if>
								<td width="113px">
								<s:hidden name="accountLine.costTransferred" />
								<configByCorp:fieldVisibility componentId="component.accountLine.costTransferred">
									<c:if test="${company.useCostTransferred == true }">
										Cost Xfer&nbsp;&nbsp;<s:checkbox name="costTransferred" value="${accountLine.costTransferred}" disabled="true"  />
									</c:if>
								</configByCorp:fieldVisibility>
								</td>
								 
								</tr> 
								
								<tr>
								<c:if test="${systemDefaultVatCalculation=='true'}">
								<td align="right" width=""  class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.payVatGl">VAT&nbsp;GL&nbsp;Code</configByCorp:fieldVisibility></td>
								<td align="left" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.payVatGl"><s:textfield cssClass="input-text" key="accountLine.payVatGl" cssStyle="width:95px;"  maxlength="7" readonly="true" tabindex="35" /></configByCorp:fieldVisibility></td>
								<td align="right" width=""  class="listwhitetext"><div id="qstPayVatGlLabel">Qst&nbsp;GL&nbsp;Code</div></td>
								<td align="left"><div id="qstPayVatGlId"><s:textfield cssClass="input-text" key="accountLine.qstPayVatGl" cssStyle="width:95px;"  maxlength="7" readonly="true" tabindex="35" /></div></td>
								</c:if>
								</tr>
							</table>	
				</sec-auth:authComponent>
				</div></td></tr></tbody></table>
				<table cellspacing="0" cellpadding="0" border="0" style="width: 100%">
				<tbody>
				<tr>
					<td height="10" width="100%" align="left" style="margin: 0px">
  					<div onClick="javascript:animatedcollapse.toggle('receivable');" style="margin: 0px">
	      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
						<tr>
						<td class="headtab_left">
						</td>
						<td NOWRAP class="headtab_center">&nbsp;&nbsp;Receivable&nbsp;Detail
						</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_center">&nbsp;
						</td>
						<td class="headtab_right">
						</td>
						</tr>
						</table>
					</div>
					<div id="receivable">
						<table class="detailTabLabel" cellspacing="3" cellpadding="2" border="0">
							<tbody>
							<sec-auth:authComponent componentId="module.accountLine.section.receivableDetail.edit">
						<%
						String sectionRreceivableDetail="true";
						int permission  = (Integer)request.getAttribute("module.accountLine.section.receivableDetail.edit" + "Permission");
					 	if (permission > 2 ){
					 		sectionRreceivableDetail = "false";
					 		//System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionRreceivableDetail);
					 	}
					 	//System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionRreceivableDetail);
					  %> 
					<table border="0" style="margin:0px;padding:0px;">
					 <tr>
					 <table class="detailTabLabel" style="margin:0px;" border="0">
					 <tr>
					 <td align="right"  class="listwhitetext">Line #</td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="accountLine.accountLineNumber" cssStyle="width:95px;" /></td>
					<td align="right"  class="listwhitetext">Active</td>
					<td align="left">
						<c:if test="${accountLine.status}">
							<input type="checkbox" style="margin:0px;" checked="checked" disabled="disabled" />
						</c:if>
						<c:if test="${accountLine.status==false}">
							<input type="checkbox" style="margin:0px;" disabled="disabled" />
						</c:if>
					</td>
					 </tr>
					
					 <tr>
					 <td align="right"  class="listwhitetext" style="width:90px;"><fmt:message key="accountLine.invoiceNumber" /></td>
					<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="accountLine.recInvoiceNumber" cssStyle="width:95px;" maxlength="17" readonly="true" tabindex="28"/></td>					 
					 														
					 <td align="right" class="listwhitetext" width="90"><fmt:message key="accountLine.authorization" /></td>
					 <td align="left" class="listwhitetext" width="100">
					 <s:textfield  cssClass="input-text" readonly="<%= sectionRreceivableDetail%>"  key="accountLine.authorization" cssStyle="width:95px;" maxlength="35" />
					 </td>
					 
					 <td width="5">
					<div  id="hhid" style="float:left;">
					<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
					<img id="rateImage"  class="openpopup" width="" height="" align="top" src="${pageContext.request.contextPath}/images/auth-ref.png" onclick="findAuthorizationNo('a');"/>
					<%} %>
					</div>
					</td>
					 
					 
					 <sec-auth:authComponent componentId="module.accountLine.section.receivedInvoice.edit">
					<%
					String sectionReceivedInvoiceDetail="true";
					int permission1  = (Integer)request.getAttribute("module.accountLine.section.receivedInvoice.edit" + "Permission");
				 	if (permission1 > 2 ){
				 		sectionReceivedInvoiceDetail = "false";
				 		//System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionRreceivableDetail);
				 	}
				 	//System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionRreceivableDetail);
				  %>				
					<td align="right"  width="112" class="listwhitetext"><fmt:message key="accountLine.invoiceDate" /></td> 
							<c:if test="${not empty accountLine.receivedInvoiceDate}">
								<s:text id="accountLineFormattedValue" name="${FormDateValue}"> <s:param name="value" value="accountLine.receivedInvoiceDate" /> </s:text>
								<c:if test="${!utsiRecAccDateFlag && !utsiPayAccDateFlag}">
								<c:if test="${(!accNonEditFlag && not empty accountLine.recAccDate) || (accNonEditFlag && ( accountLine.recInvoiceNumber!='' && not  empty accountLine.recInvoiceNumber))}">
								<td><s:textfield cssClass="input-text"	id="receivedInvoiceDate"
									name="accountLine.receivedInvoiceDate"
									value="%{accountLineFormattedValue}" cssStyle="width:95px;"
									maxlength="11" onkeydown="" readonly="true" onselect="updateReceivedInvoiceDate();"/> 
									 
									   <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%> 
									  <c:if test="${(!accNonEditFlag)}">
									   <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled.')" />
								      </c:if>
								      <c:if test="${(accNonEditFlag)}">
									   <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as invoice has been generated.')" />
								      </c:if>
								    <%} %>
								    </c:if>
								    <c:if test="${(!accNonEditFlag && empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber=='' || empty accountLine.recInvoiceNumber ) )}">
									 <td><s:textfield cssClass="input-text"	id="receivedInvoiceDate"
									name="accountLine.receivedInvoiceDate"
									value="%{accountLineFormattedValue}" cssStyle="width:95px;"
									maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" onselect="updateReceivedInvoiceDate();"/> 
									  <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%>  
									   <img id="receivedInvoiceDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="copyReceivedInvoiceDate(); return false;" />
								     <%} %>
								    </c:if>
								    </c:if>
								    <c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
								    <td><s:textfield cssClass="input-text"	id="receivedInvoiceDate"
									name="accountLine.receivedInvoiceDate"
									value="%{accountLineFormattedValue}" cssStyle="width:95px;"
									maxlength="11" onkeydown="" readonly="true" onselect="updateReceivedInvoiceDate();"/> 
									 
									   <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%> 
									   <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as sent to Accounting and/or already Invoiced in UTSI Instance')" />
								    <%} %>
								    </c:if>
								</td>
							</c:if>
							<c:if test="${empty accountLine.receivedInvoiceDate}">
								<td><s:textfield cssClass="input-text"
									id="receivedInvoiceDate"
									name="accountLine.receivedInvoiceDate" required="true"
									cssStyle="width:95px;" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true" onselect="updateReceivedInvoiceDate();"/> 
								  <c:if test="${!utsiRecAccDateFlag && !utsiPayAccDateFlag}">
								   <c:if test="${(!accNonEditFlag && not empty accountLine.recAccDate) || (accNonEditFlag && (accountLine.recInvoiceNumber!='' && not empty accountLine.recInvoiceNumber))}">
									  <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%>   
									   <c:if test="${(!accNonEditFlag)}">
									  <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
								      </c:if>
								      <c:if test="${(accNonEditFlag)}">
									  <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as invoice has been generated.')" />
								      </c:if>
								   <%} %>
								  </c:if>
								   <c:if test="${(!accNonEditFlag && empty accountLine.recAccDate) || (accNonEditFlag && ( accountLine.recInvoiceNumber=='' || empty accountLine.recInvoiceNumber))}">
								      <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%>  
								      <img id="receivedInvoiceDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="copyReceivedInvoiceDate(); return false;" />
								   <%} %>
								  </c:if>
								  </c:if>
								   <c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
								   <% if (sectionReceivedInvoiceDetail.equalsIgnoreCase("false")){%>   
									  <img id="receivedInvoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as sent to Accounting and/or already Invoiced in UTSI Instance')" />
								   <%} %>
								   </c:if>
								</td>
							</c:if>
					</sec-auth:authComponent>					 
					 
					 <td class="listwhitetext" align="right" width="">Invoice Created By</td>
					<td><s:textfield cssClass="input-textUpper" readonly="true" key="accountLine.invoiceCreatedBy" cssStyle="text-align:left;width:95px;"   maxlength="25" /> 
					</td>
					 
					 </tr>
					 
					 <tr> 
					
					<c:if test="${contractType}">
						<tr><td align="left" class="vertlinedata" colspan="16" style="padding-top:3px;"></td></tr>
						<tr> 
						<td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td> 
						<td align="left"><s:select  cssClass="list-menu"  key="accountLine.contractCurrency" cssStyle="width:64px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRateRec(this,'accountLine.contractExchangeRate','accountLine.contractCurrency');" headerValue="" tabindex="18"  /></td>
						<td align="right" class="listwhitetext" >Value&nbsp;Date</td>
						<c:if test="${not empty accountLine.contractValueDate}">
							<s:text id="accountLineFormattedContractValue" name="${FormDateValue}"><s:param name="value" value="accountLine.contractValueDate" /> </s:text>
							<td><s:textfield  cssClass="input-text" id="contractValue" name="accountLine.contractValueDate" value="%{accountLineFormattedContractValue}" size="8" maxlength="12" readonly="true" /></td>
						</c:if>
						<c:if test="${empty accountLine.contractValueDate}">
							<td><s:textfield  cssClass="input-text" id="contractValueDate" name="accountLine.contractValueDate" required="true" size="8" maxlength="12"  readonly="true"/></td> 
						</c:if> 
						</tr>
						<tr>
						<td align="right" class="listwhitetext">Contract&nbsp;Rate</td>
					    <td align="left" class="listwhitetext"><s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" cssStyle="text-align:right" key="accountLine.contractRate" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateRecCurrency('accountLine.contractRate','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');" onblur=""/></td>	 
						<td width="80px" align="right" class="listwhitetext">Contract&nbsp;Exchange&nbsp;Rate</td>
						<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.contractExchangeRate" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculateRecCurrency('accountLine.contractRate','','','');"/></td> 
						<td width="" align="right" class="listwhitetext"colspan="2">Contract&nbsp;Amount</td>
						<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.contractRateAmmount" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></td> 
						</tr>
						<tr><td align="left" class="vertlinedata" colspan="16" style="padding-bottom:3px;"></td></tr>
					</c:if>
					<c:if test="${multiCurrency=='Y'}">
						<tr> 
						<c:choose>
						 <c:when test="${contractType}">
						 <td align="right" class="listwhitetext"><fmt:message key="accountLine.recRateCurrency" /><font color="red" size="2">*</font></td>
						 </c:when>
						 <c:otherwise>
						<td align="right" class="listwhitetext"><fmt:message key="accountLine.recRateCurrency" /></td>
						</c:otherwise></c:choose> 
						<td align="left" colspan="1" ><s:select  cssClass="list-menu"  key="accountLine.recRateCurrency" cssStyle="width:64px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRateRec(this,'accountLine.recRateExchange','accountLine.recRateCurrency');" headerValue="" tabindex="18"  /></td>
						<td align="right" class="listwhitetext" ><fmt:message key="accountLine.racValueDate" />  </td>
						<c:if test="${not empty accountLine.racValueDate}">
							<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.racValueDate" /> </s:text>
							<td><s:textfield  cssClass="input-text" id="racValueDate" name="accountLine.racValueDate" value="%{accountLineFormattedValue}" size="8" maxlength="12" readonly="true" /></td>
							
						</c:if>
						<c:if test="${empty accountLine.racValueDate}">
							<td><s:textfield  cssClass="input-text" id="racValueDate" name="accountLine.racValueDate" required="true" size="8" maxlength="12"  readonly="true"/></td> 
						</c:if> 
							
						</tr>
						<tr>
						<td align="right" class="listwhitetext"><fmt:message key="accountLine.recCurrencyRate" /></td>
					    <td align="left" class="listwhitetext"><s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" cssStyle="text-align:right" key="accountLine.recCurrencyRate" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateRecCurrency('accountLine.recCurrencyRate','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate');" onblur=""/></td>	 
						<td width="" align="right" class="listwhitetext"><fmt:message key="accountLine.recRateExchange" /></td>
						<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.recRateExchange" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)" readonly="${!contractType}" onchange="changeStatus();calculateRecCurrency('accountLine.contractRate','','','');"/></td> 
						<td width="" align="right" class="listwhitetext"colspan="2"><fmt:message key="accountLine.actualRevenueForeign" /></td>
						<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.actualRevenueForeign" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></td> 
						</tr>
						<tr><td align="left" class="vertlinedata" colspan="16" style="padding-bottom:3px;"></td></tr>
					</c:if>
				<sec-auth:authComponent componentId="module.accountLine.section.sectionReceivedInvoiceDescription.edit">
					<%
					String sectionReceivedInvoiceDescription="true";
					int permission2  = (Integer)request.getAttribute("module.accountLine.section.sectionReceivedInvoiceDescription.edit" + "Permission");
				 	if (permission2 > 2 ){
				 		sectionReceivedInvoiceDescription = "false";
				 		//System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionRreceivableDetail);
				 	}
				 	//System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionRreceivableDetail);
				  	%>											
					<td align="right" class="listwhitetext"><fmt:message key="accountLine.recNote" /></td>
					<c:if test="${!utsiRecAccDateFlag  && !utsiPayAccDateFlag}">
					<td align="left" class="listwhitetext" colspan="8"><s:textarea readonly="<%= sectionReceivedInvoiceDescription%>" name="accountLine.description" cssStyle="width: 293px; height: 40px;" tabindex="31" cssClass="textarea"/></td>
					</c:if>
					<c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
					<td align="left" class="listwhitetext" colspan="8"><s:textarea readonly="<%= sectionReceivedInvoiceDescription%>" name="accountLine.description" cssStyle="width: 293px; height: 40px;" tabindex="31" cssClass="textarea"/></td>
					</c:if>
				</sec-auth:authComponent>
					 
					</tr>
					
					<tr>
					
					<td align="right"> 
					  	<div id="creditInvoice">
						  <table style="margin:0px;"><tr>
	                       <td width="85px" align="right" class="listwhitetext">Credit Invoice #</td>
							</tr></table>
						</div>
					  </td>
					<td align="left"><div id="creditInvoice1"><table cellpadding="0" cellspacing="0" style="margin:0px;"><tr>
					<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:95px;" key="accountLine.creditInvoiceNumber" readonly="true" /></td>
					</tr></table></div></td>
					<td colspan="5">
					<table style="margin:0px;" cellpadding="0" cellspacing="0">
					<tr>					
					<c:if test="${systemDefaultVatCalculation=='true'}">
            		<td align="right" class="listwhitetext" width="65"><div id="qstRecVatAmtGlLabel">VAT&nbsp;</div></td>
					<td align="left" class="listwhitetext"><div id="qstRecVatAmtGlId"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:95px;" key="accountLine.qstRecVatAmt" id="accountLine.qstRecVatAmt" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></div></td> 
					</c:if>
					</tr>
					</table>
					 </td>
					 </tr>
					 
					 <tr>
					<s:hidden name="calOpener" value="notOPen" />												    
					<sec-auth:authComponent componentId="module.accountLine.section.paymentStatus.edit">
					<%-- <script language="javascript" type="text/javascript">
						changeCalOpenarvalue();
					</script> --%>
					<%
					String sectionPaymentStatus="true";
					int permissionPaymentStatus  = (Integer)request.getAttribute("module.accountLine.section.paymentStatus.edit" + "Permission");
				 	if (permissionPaymentStatus > 2 ){
				 		sectionPaymentStatus = "false";
				 		//System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionPaymentStatus);
				 	}
				 	//System.err.println("\n\n\n\n sectionRreceivableDetail false---->"+sectionPaymentStatus);
				  %> 													    
															    
				    <td class="listwhitetext" align="right"> <fmt:message key="accountLine.paymentStatus" /></td>  
					<td> <s:select cssClass="list-menu" disabled="<%= sectionPaymentStatus%>" list="%{paymentStatus}" 
					name="accountLine.paymentStatus" headerKey="" cssStyle="width:100px" headerValue=""
					 onchange="autoPopulateStatusDate(this);" tabindex="33"/> </td>
					
					<td align="right" class="listwhitetext"><fmt:message key="accountLine.externalReference" /></td>
					<td align="left" class="listwhitetext" ><s:textfield  readonly="<%= sectionRreceivableDetail%>" cssClass="input-text" key="accountLine.externalReference" 
						cssStyle="width:95px;" maxlength="25" onchange="calReference(); " /></td>
					<td class="listwhitetext" align="right" colspan="2">Rec.&nbsp;Amt</td>
					<td><s:textfield  cssClass="input-text" cssStyle="text-align:right;width:95px;" name="accountLine.receivedAmount"
							required="true"  size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" readonly="<%= sectionPaymentStatus%>" tabindex="34"/>
					</td> 
					<c:if test="${not empty accountLine.statusDate}">
					<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.statusDate" />
					</s:text>
					<td align="right" class="listwhitetext" ><fmt:message key="accountLine.statusDate" /></td>
					<td ><s:textfield  cssClass="input-text"
							id="statusDate" name="accountLine.statusDate"
							value="%{accountLineFormattedValue}" cssStyle="width:95px;"
							maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true"/>
					<% if (sectionPaymentStatus.equalsIgnoreCase("false")){%>
					<img id="statusDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>											
				    <%} %>
				    </td>
					</c:if>
					<c:if test="${empty accountLine.statusDate}">
					<td align="right" class="listwhitetext" ><fmt:message key="accountLine.statusDate" /></td>
					<td ><s:textfield  cssClass="input-text"
							id="statusDate" name="accountLine.statusDate"
							required="true" cssStyle="width:95px;" maxlength="11"
							onkeydown="return onlyDel(event,this)" readonly="true"/>
						<% if (sectionPaymentStatus.equalsIgnoreCase("false")){%>
						<img id="statusDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/> 
						 <%} %> 
						 </td>
					  </c:if> 
			</sec-auth:authComponent>
					 </tr>
					
					<tr>
					<td colspan="12">
					<table style="margin:0px;" cellpadding="0" cellspacing="0">
					
					 <tr>
						<c:if test="${fn1:indexOf(systemDefaultstorage,serviceOrder.job)>=0}">
						<tr> 
						<td align="right" class="listwhitetext"><fmt:message key="accountLine.storageDateRangeFrom" />&nbsp;</td>  
						<c:if test="${not empty accountLine.storageDateRangeFrom}"> 
						<s:text id="accountLineFormattedValue" name="${FormDateValue}">
								<s:param name="value" value="accountLine.storageDateRangeFrom" />
						</s:text>
						<td>
						<s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text"
								id="storageDateRangeFrom" name="accountLine.storageDateRangeFrom"
								value="%{accountLineFormattedValue}" cssStyle="width:95px;"
								maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/>
						<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
						   <img id="storageDateRangeFrom-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" />											
					   <%} %>
					    </td>
						</c:if>
						<c:if test="${empty accountLine.storageDateRangeFrom}">
							<td>
								<s:textfield readonly="<%= sectionRreceivableDetail%>" cssClass="input-text"
								id="storageDateRangeFrom" name="accountLine.storageDateRangeFrom"
								required="true" cssStyle="width:95px;" maxlength="11"
								onkeydown="return onlyDel(event,this)" onselect="calcDays()"/>
							<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
							 <img id="storageDateRangeFrom-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20  onclick="forDays(); return false;" /> 
							 <%} %>
							 </td>
						  </c:if>
						<td align="left" colspan="7" class="listwhitetext" >
					    <table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
					    <tr>  
					    <td width="59"></td>
						<td align="right" class="listwhitetext"><fmt:message key="accountLine.storageDateRangeTo" />&nbsp;</td> 
							
						<c:if test="${not empty accountLine.storageDateRangeTo}">
						
						<s:text id="accountLineFormattedValue" name="${FormDateValue}">
								<s:param name="value" value="accountLine.storageDateRangeTo" />
						</s:text>
						<td>
						<s:textfield  cssClass="input-text"
								id="storageDateRangeTo" name="accountLine.storageDateRangeTo"
								value="%{accountLineFormattedValue}" cssStyle="width:95px;"
								maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays();" readonly="true"/>
						
						   <img id="storageDateRangeTo-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false; " />											
					    </td>
						</c:if>
						<c:if test="${empty accountLine.storageDateRangeTo}">
							<td>
								<s:textfield  cssClass="input-text"
								id="storageDateRangeTo" name="accountLine.storageDateRangeTo"
								required="true" cssStyle="width:95px;" maxlength="11"
								onkeydown="return onlyDel(event,this)" onselect="calcDays();" readonly="true" />
							<% if (sectionRreceivableDetail.equalsIgnoreCase("false")){%>
							 <img id="storageDateRangeTo-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false; " /> 
							 <%} %> 
							 </td>
						  </c:if>
						  <td  align="right" class="listwhitetext" width="48"><fmt:message
							key="accountLine.storageDays" />&nbsp;</td>
						<td align="left" class="listwhitetext"><s:textfield
							 cssClass="input-text"  cssStyle="text-align:right;width:95px;" id="noOfDays" name="accountLine.storageDays"
							maxlength="9"  
							tabindex="12" onselect="calcDays();" onchange="calcDays();"/></td>
							
							<td align="right" class="listwhitetext" width="120"><fmt:message key="billing.onHand"/>&nbsp;</td>
                             <td align="left"><s:textfield name="accountLine.onHand" maxlength="15" cssStyle="text-align:right;width:95px;" onchange="onlyFloat(this);" required="true" cssClass="input-text" size="8" /></td>	 
						 </tr></table></td>
						
						</tr>
						</c:if>
						</tr>
						</table>
						</td>
						</tr>
						
						
						<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
				 			<tr>
				 			<td align="right" class="listwhitetext" >Vanline Settle Date</td>
			 		     	<c:if test="${not empty  accountLine.vanlineSettleDate}">
                                 <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.vanlineSettleDate"/></s:text>
                                 <td><s:textfield cssClass="input-text" id="SettleDate" name="accountLine.vanlineSettleDate" value="%{accountLineFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="" /></td>
                             </c:if>
                             <c:if test="${empty accountLine.vanlineSettleDate}">
                              	<td><s:textfield cssClass="input-text" id="SettleDate" name="accountLine.vanlineSettleDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="" /></td>
                             </c:if>
				 			<td align="right" class="listwhitetext"><fmt:message key="accountLine.glType" /></td>
							<td align="left" >
							<s:select disabled="<%= sectionRreceivableDetail%>" cssClass="list-menu" headerKey="" headerValue="" name="accountLine.glType" cssStyle="width:85px" list="%{getGlTypeList}" onchange="" />
							</td>
							<td></td>
							<td align="center" valign="bottom" class="listwhitetext"><fmt:message key="accountLine.distributionAmount" /></td> 
							<td align="left" class="listwhitetext"><s:textfield  cssClass="input-text" cssStyle="text-align:right" key="accountLine.distributionAmount" size="8" maxlength="15" onkeydown="return onlyRateAllowed(event)" onchange="chechCreditInvoice();driverCommission();"/></td>
							<td><input type="button" class="cssbuttonA" style="width:70px; height: 25px;"   value="Distribution" onclick="getDistributedAmtFromBCode();driverCommission();"/></td>
						</tr>
						</c:if> 
					 
				
					 <tr>
						 <sec-auth:authComponent componentId="module.accountLine.section.receivableDetailDate.edit">
							<%;
							String receivableDetailDate="true";
							int permissionDate  = (Integer)request.getAttribute("module.accountLine.section.receivableDetailDate.edit" + "Permission");
						 	if (permissionDate > 2 ){
						 		receivableDetailDate = "false";
						 	}
						  	%>
						  		 <table width="100%" border="0" style="margin:0px;" cellpadding="0" cellspacing="0">
							<tbody><tr><td align="left" class="vertlinedata"></td></tr></tbody>
							</table>
						  	
								<table width="100%" class="payablebg" style="margin-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px;" border="0">
								<tr style="">
								
								<configByCorp:fieldVisibility componentId="component.accountLine.section.paymentSent">
								<td align="right"  width="93"  class="listwhitetext">
								Payment&nbsp;Sent
								</td>
								</configByCorp:fieldVisibility>
								<sec-auth:authComponent componentId="module.accountLine.section.adminfinance.edit">
								<%;
								String adminFinanceEdit="true";
								int permissionadminfinance  = (Integer)request.getAttribute("module.accountLine.section.adminfinance.edit" + "Permission");
							 	if (permissionadminfinance > 2 ){
							 		adminFinanceEdit = "false";
							 	}
							  	%> 
								<configByCorp:fieldVisibility componentId="component.accountLine.section.paymentSent">
										 <c:if test="${not empty accountLine.paymentSent}">
												<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.paymentSent" /></s:text>
												<td width="124" class="listwhitetext" >
												<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
												<s:textfield cssClass="input-text" id="paymentSent" name="accountLine.paymentSent" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
												<img id="paymentSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
												<%} %>	
												<% if (receivableDetailDate.equalsIgnoreCase("true")){%>
												<s:textfield cssClass="input-text" id="paymentSent" name="accountLine.paymentSent" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" readonly="true" />
												<%} %>	
												</td>
											</c:if>
											<c:if test="${empty  accountLine.paymentSent}">
												<td width="124">
												<s:textfield cssClass="input-text" id="paymentSent" name="accountLine.paymentSent" required="true" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
												<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
												<img id="paymentSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
													<%} %>
											</td>
											</c:if>  
								</configByCorp:fieldVisibility>
								</sec-auth:authComponent>
													
											<td align="right"  width="90" class="listwhitetext">
											<configByCorp:fieldVisibility componentId="accountLine.sendEstToClient">
											<fmt:message key="accountLine.sendEstToClient" /></configByCorp:fieldVisibility></td>
												<c:if test="${not empty accountLine.sendEstToClient}">
													<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.sendEstToClient" /></s:text>
													<td width="124" class="listwhitetext" colspan="1"><configByCorp:fieldVisibility componentId="accountLine.sendEstToClient"><s:textfield cssClass="input-text" id="sendEstToClient" name="accountLine.sendEstToClient" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
														<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
														<img id="sendEstToClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
														<%} %>
														</configByCorp:fieldVisibility>
													</td>
												</c:if>
												<c:if test="${empty accountLine.sendEstToClient}">
													<td width="124" colspan="1"><configByCorp:fieldVisibility componentId="accountLine.sendEstToClient"><s:textfield cssClass="input-text" id="sendEstToClient" name="accountLine.sendEstToClient" required="true" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
													<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
													<img id="sendEstToClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
														<%} %>
														</configByCorp:fieldVisibility></td>
												</c:if> 
											<td align="right" width="67" class="listwhitetext" colspan="1"><configByCorp:fieldVisibility componentId="accountLine.sendActualToClient"><fmt:message key="accountLine.sendActualToClient" /></configByCorp:fieldVisibility></td>
												<c:if test="${not empty accountLine.sendActualToClient}">
													<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.sendActualToClient" /></s:text>
													<td width="125"><configByCorp:fieldVisibility componentId="accountLine.sendActualToClient"><s:textfield cssClass="input-text" id="sendActualToClient" name="accountLine.sendActualToClient" value="%{accountLineFormattedValue}" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)" readonly="true" />
													<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
													<img id="sendActualToClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="triggerCalendarId(this.id),copyActSenttoclient(); return false;" />
													<%} %>		
														</configByCorp:fieldVisibility></td>
												  </c:if>
												<c:if test="${empty accountLine.sendActualToClient}">
													<td width="125"><configByCorp:fieldVisibility componentId="accountLine.sendActualToClient"><s:textfield cssClass="input-text" id="sendActualToClient" name="accountLine.sendActualToClient" required="true" cssStyle="width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)" readonly="true"/>
													<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
													<img id="sendActualToClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="triggerCalendarId(this.id),copyActSenttoclient(); return false;" />
														<%} %>	
														</configByCorp:fieldVisibility></td>
												</c:if> 
											<td align="right" width="108"  class="listwhitetext" colspan="1"><configByCorp:fieldVisibility componentId="accountLine.doNotSendtoClient"><fmt:message key="accountLine.doNotSendtoClient" /></configByCorp:fieldVisibility></td>
												<c:if test="${not empty accountLine.doNotSendtoClient}">
													<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.doNotSendtoClient" /></s:text>
													<td width="113px" colspan="3"><configByCorp:fieldVisibility componentId="accountLine.doNotSendtoClient"><s:textfield cssClass="input-text" id="doNotSendtoClient" name="accountLine.doNotSendtoClient" value="%{accountLineFormattedValue}" size="8" cssStyle="width:60px;width:95px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
													<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
													<img id="doNotSendtoClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
													<%} %>	
														</configByCorp:fieldVisibility></td>
												  </c:if>
												<c:if test="${empty accountLine.doNotSendtoClient}">
													<td width="113px" colspan="3"><configByCorp:fieldVisibility componentId="accountLine.doNotSendtoClient"><s:textfield cssClass="input-text" id="doNotSendtoClient" name="accountLine.doNotSendtoClient" required="true" cssStyle="width:95px;" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
													<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
													<img id="doNotSendtoClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
														<%} %>	
														</configByCorp:fieldVisibility></td>
												</c:if> 
												<c:if test="${systemDefaultVatCalculation=='true'}">
												<td align="right" width=""  class="listwhitetext" colspan="1"><configByCorp:fieldVisibility componentId="accountLine.recVatGL">VAT&nbsp;GL&nbsp;Code</configByCorp:fieldVisibility></td>
												<td align="left" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.recVatGL"><s:textfield cssClass="input-text" key="accountLine.recVatGl" cssStyle="width:95px;"  maxlength="7" readonly="true" tabindex="35" /></configByCorp:fieldVisibility></td> 
												<td align="right" width=""  class="listwhitetext"><div id="qstRecVatGlLabel">Qst GL Code</div></td>
												<td align="left"><div id="qstRecVatGlId"><s:textfield cssClass="input-text" key="accountLine.qstRecVatGl" cssStyle="width:95px;"  maxlength="7" readonly="true" tabindex="35" /></div></td>
												</c:if>
												</tr>
																
											<sec-auth:authComponent componentId="module.accountLine.section.adminfinance.edit">
											<%;
											String adminFinanceEdit="true";
											int permissionadminfinance  = (Integer)request.getAttribute("module.accountLine.section.adminfinance.edit" + "Permission");
										 	if (permissionadminfinance > 2 ){
										 		adminFinanceEdit = "false";
										 	}
										  %>
                                  <tr> 
                                  <td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.recGl"><fmt:message key="charges.glCode"/></configByCorp:fieldVisibility></td>  
                                      <td align="left" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.recGl"><s:textfield cssClass="input-text" key="accountLine.recGl" cssStyle="width:95px;"  maxlength="7" readonly="true" tabindex="35" /></configByCorp:fieldVisibility></td> 
									<td align="right" class="listwhitetext" ><configByCorp:fieldVisibility componentId="accountLine.accrueRevenue"><fmt:message key="accountLine.accrueRevenue" /></configByCorp:fieldVisibility></td> 
										<c:if test="${not empty accountLine.accrueRevenue}">
										<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accrueRevenue" /></s:text>
										<td class="listwhitetext" ><configByCorp:fieldVisibility componentId="accountLine.accrueRevenue"><s:textfield cssClass="input-text" id="accrueRevenue" name="accountLine.accrueRevenue" value="%{accountLineFormattedValue}" cssStyle="width:95px;"
											maxlength="12"  onkeydown="" readonly="true"/>
										</configByCorp:fieldVisibility></td>
									 </c:if>
									 <c:if test="${empty accountLine.accrueRevenue}">
										<td class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.accrueRevenue"><s:textfield cssClass="input-text" id="accrueRevenue" name="accountLine.accrueRevenue" required="true" cssStyle="width:95px;" maxlength="12"  onkeydown=""readonly="true"/> 
										</configByCorp:fieldVisibility></td>
									</c:if>  
									<configByCorp:fieldVisibility componentId="component.field.auditCompleteDateForUTSI">
									
									</configByCorp:fieldVisibility>
                                <c:if test="${accountInterface=='Y'}">								
  
                                     <td align="right"  class="listwhitetext" ><fmt:message key="accountLine.accrueRevenueReverse" /></td>
                                     <c:if test="${not empty accountLine.accrueRevenue }">
                                     <c:if test="${not empty accountLine.accrueRevenueReverse}"> 
                                     <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accrueRevenueReverse"/></s:text>
                                  <td align="left" class="listwhitetext" ><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse" value="%{accountLineFormattedValue}" onkeydown="" readonly="true"  cssClass="input-text" cssStyle="width:95px;"/>
                                    </c:if>
                                   <c:if test="${empty accountLine.accrueRevenueReverse}">
                                  <td align="left" class="listwhitetext" ><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse" onkeydown="" readonly="true" cssClass="input-text" cssStyle="width:95px;"/>
                                 </c:if>
                              	</c:if>
                                 <c:if test="${empty accountLine.accrueRevenue }">
                                 <c:if test="${not empty accountLine.accrueRevenueReverse}"> 
                                   <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accrueRevenueReverse"/></s:text>
                                <td align="left" class="listwhitetext" ><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse" value="%{accountLineFormattedValue}"  readonly="true" cssClass="input-text" cssStyle="width:95px;"/>
                                  </td>
                                  </c:if>
                                 <c:if test="${empty accountLine.accrueRevenueReverse}">
                                <td align="left" class="listwhitetext" ><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse"  readonly="true" cssClass="input-text" cssStyle="width:95px;"/>
                                 </td>
                                 </c:if>
                                 </c:if>
                                 
                                 <sec-auth:authComponent componentId="module.accountLine.section.receivableDetailAccrual.edit">
									<%;
									String receivableDetailAccrual="true";
									int permissionAccural  = (Integer)request.getAttribute("module.accountLine.section.receivableDetailAccrual.edit" + "Permission");
								 	if (permissionAccural > 2 ){
								 		receivableDetailAccrual = "false";
								 	}
									%> 
								     <td align="right"  class="listwhitetext" colspan="2"><fmt:message key="accountLine.accrueRevenueManual" />
                                         <c:choose>
                                         <c:when test="${empty accountLine.accrueRevenue }">
                                         <td align="left"  class="listwhitetext"><s:checkbox name="accountLine.accrueRevenueManual" value="${accountLine.accrueRevenueManual}" fieldValue="true" cssStyle="margin:0px;" disabled="true" onchange=""/></td>
                                         </c:when>
                                         <c:when test="${not empty accountLine.accrueRevenue && empty accountLine.accrueRevenueReverse}">
                                         <td align="left"  class="listwhitetext"> <s:checkbox name="accountLine.accrueRevenueManual" value="${accountLine.accrueRevenueManual}" fieldValue="true" cssStyle="margin:0px;" disabled="<%=receivableDetailAccrual%>" onchange=""/></td>
                                         </c:when>
                                         <c:otherwise>
                                        <td align="left"  class="listwhitetext"> <s:checkbox name="accountLine.accrueRevenueManual" value="${accountLine.accrueRevenueManual}" fieldValue="true" cssStyle="margin:0px;" disabled="true" onchange=""/></td>
                                         </c:otherwise>
                                         </c:choose> 
  										</sec-auth:authComponent>
                                 
                                 </c:if>
                                  </tr> 													
                                 <tr><td align="right"  class="listwhitetext" ><configByCorp:fieldVisibility componentId="accountLine.recPostDate"><fmt:message key="accountLine.recPostDate" /></configByCorp:fieldVisibility></td> 
								<c:if test="${not empty accountLine.recPostDate}">
								<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.recPostDate" /></s:text>
								<td class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.recPostDate"><s:textfield cssClass="input-text" id="recPostDate" name="accountLine.recPostDate" value="%{accountLineFormattedValue}" cssStyle="width:95px;"
									maxlength="12"  onkeydown="" readonly="true"/></configByCorp:fieldVisibility><configByCorp:fieldVisibility componentId="accountLine.recPostDate">
									<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
									<c:if test="${empty accountLine.recAccDate}">
									   <img id="recPostDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['pricingPayableForm'].recPostDate.focus(); return false;" />
									</c:if>
									<c:if test="${not empty accountLine.recAccDate}">
									   <img id="recPostDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
									</c:if>
									<%} %>
									</configByCorp:fieldVisibility>
									</td>
							</c:if>
							<c:if test="${empty accountLine.recPostDate}">
								<td class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.recPostDate"><s:textfield cssClass="input-text" id="recPostDate" name="accountLine.recPostDate" required="true" cssStyle="width:95px;" maxlength="11"
									 onkeydown=""readonly="true"/></configByCorp:fieldVisibility><configByCorp:fieldVisibility componentId="accountLine.recPostDate">
								<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
								<c:if test="${empty accountLine.recAccDate}">
									 <img id="recPostDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['pricingPayableForm'].recPostDate.focus(); return false;" />
								</c:if>	
								<c:if test="${not empty accountLine.recAccDate}">
									 <img id="recPostDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
								</c:if>
								<%} %>
									</configByCorp:fieldVisibility>
								</td>
							</c:if>
							<c:if test="${accountInterface=='Y'}">
							<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.recAccDate"><fmt:message key="accountLine.recAccDate"/></configByCorp:fieldVisibility></td>  
                                                       <c:if test="${not empty accountLine.recAccDate}">
							<s:text id="recAccDate" name="${FormDateValue}"><s:param name="value" value="accountLine.recAccDate" /></s:text>
							<% if (!(adminFinanceEdit.equalsIgnoreCase("false"))){%>
							<td class="listwhitetext" ><configByCorp:fieldVisibility componentId="accountLine.recAccDate"><s:textfield cssClass="input-text" id="recAccDate" name="accountLine.recAccDate" value="%{recAccDate}" cssStyle="width:95px" maxlength="11" readonly="true" />
							</configByCorp:fieldVisibility></td> 
							<%} %> 
							<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
							        <td class="listwhitetext" width="90"><configByCorp:fieldVisibility componentId="accountLine.recAccDate"><s:textfield cssClass="input-text" id="recAccDate" name="accountLine.recAccDate" value="%{recAccDate}" cssStyle="width:95px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" />
									<img id="recAccDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
								    </configByCorp:fieldVisibility></td> 
								<%} %> 
							
							</c:if>
							<c:if test="${empty accountLine.recAccDate}">
							<td class="listwhitetext" width="90"><configByCorp:fieldVisibility componentId="accountLine.recAccDate"><s:textfield cssClass="input-text" id="recAccDate" name="accountLine.recAccDate" cssStyle="width:95px" maxlength="11" readonly="true" />
							<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
									<img id="recAccDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
								 <%} %></configByCorp:fieldVisibility></td>
							</c:if> 
							</c:if>
							
						<td align="right" class="listwhitetext" colspan="1"><configByCorp:fieldVisibility componentId="accountLine.recXfer"><fmt:message key="accountLine.recXfer"/></configByCorp:fieldVisibility></td>  
                                            <td align="left" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.recXfer"><s:textfield cssClass="input-text" key="accountLine.recXfer" cssStyle="width:95px;"  maxlength="20"   readonly="<%= adminFinanceEdit%>" tabindex="36"/></configByCorp:fieldVisibility></td> 
             
                                            <c:if test="${accountInterface=='Y'}">
                                            <td align="right" class="listwhitetext" colspan="2"><fmt:message key="accountLine.recXferUser"/></td>  
                                            <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" key="accountLine.recXferUser" cssStyle="width:95px;"  maxlength="7" readonly="<%= adminFinanceEdit%>" tabindex="37"/></td> 	 
                                            </c:if>
                                             <%-- <script type="text/javascript">
                                               disableBYRecAcc();
                                               checkMultiAuthorization();
                                            </script> --%>
                      <configByCorp:fieldVisibility componentId="component.field.auditCompleteDateForUTSI">
							<c:if test="${accountInterface=='Y'}">
							<sec-auth:authComponent componentId="module.accountLine.section.auditCompleteDate.edit">
								<%;
								String auditCompleteDateEdit="true";
								int permissionAuditCompleteDate  = (Integer)request.getAttribute("module.accountLine.section.auditCompleteDate.edit" + "Permission");
							 	if (permissionAuditCompleteDate > 2 ){
							 		auditCompleteDateEdit = "false";
							 		
							 	}
							 	
							  %>		
							  <tr>																											
							<td align="right" class="listwhitetext" style="width:90px;"><fmt:message key="accountLine.auditCompleteDate"/></td>
							<c:choose>
							<c:when test="${empty accountLine.recInvoiceNumber && (accountLine.actualRevenue!='0.00' && accountLine.actualRevenue!='0' && accountLine.actualRevenue!='0.0') && empty accountLine.recPostDate}" >
								<c:if test="${not empty accountLine.auditCompleteDate}">
								<s:text id="auditCompleteDate" name="${FormDateValue}"><s:param name="value" value="accountLine.auditCompleteDate" /></s:text>
								<% if (!(auditCompleteDateEdit.equalsIgnoreCase("false"))){%>
								<td class="listwhitetext" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" value="%{auditCompleteDate}" cssStyle="width:95px" maxlength="11" readonly="true" />
								</td> 
								<%} %> 
								<% if (auditCompleteDateEdit.equalsIgnoreCase("false")){%>
								<td class="listwhitetext" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" value="%{auditCompleteDate}" cssStyle="width:95px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" />
								<img id="auditCompleteDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
								</td> 
								<%} %> 
								</c:if>
								<c:if test="${empty accountLine.auditCompleteDate}">
								<td class="listwhitetext" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" cssStyle="width:95px" maxlength="11" readonly="true" />
								<% if((auditCompleteDateEdit.equalsIgnoreCase("false"))){%>
								<img id="auditCompleteDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
								<%} %>
								</td>  
								</c:if>
							 </c:when>
							<c:otherwise>
								<c:if test="${not empty accountLine.auditCompleteDate}">
								<s:text id="auditCompleteDate" name="${FormDateValue}"><s:param name="value" value="accountLine.auditCompleteDate" /></s:text>
								<td class="listwhitetext" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" value="%{auditCompleteDate}" cssStyle="width:95px" maxlength="11" readonly="true" />
								</c:if>
								<c:if test="${empty accountLine.auditCompleteDate}">
								<td class="listwhitetext" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" cssStyle="width:95px" maxlength="11" readonly="true" />
								</td>  
								</c:if>
							</c:otherwise>
							</c:choose>
							</tr>
							</sec-auth:authComponent>
							</c:if>
							</configByCorp:fieldVisibility>
                             </tr> 
							</sec-auth:authComponent> 						
						</table> 
						</sec-auth:authComponent>
					 </tr>
					 </table>
					 </tr>
					</table>
					</sec-auth:authComponent>
							
							</tbody>
						</table>
						</div>
					</td>
				</tr>
				</tbody>
				</table>
				
		</div>

<div class="bottom-header" style="margin-top:43px;"><span></span></div>
</div>
</div> 
	</div>

<table>
<tr>
<td><input type="button" class="pricelistbtn" style="width:55px;margin-right:0px !important;margin-left:23px;" name="pricingSave" value="Save" onclick="savePayableDetails();"/></td>
<td><input type="button"  value="Cancel" name="Cancel" class="pricelistbtn" style="width:55px;" onclick="window.close();"></td>
</tr>
</table>
</s:form>

<script type="text/javascript">
setOnSelectBasedMethods(['calcDays(),actSentToclient(),hitAcct(),updateManagerApprovalName()']);
setCalendarFunctionality();
</script>
<script type="text/javascript">
function onlyforInvoice(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;  
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110)||(keyCode==188)||(keyCode==191)||(keyCode==220)||(keyCode==111)||(keyCode==106)||(keyCode==144)||(keyCode==8)||(keyCode==96)||(keyCode==97)||(keyCode==98)||(keyCode==99)||(keyCode==100)||(keyCode==101)||(keyCode==102)||(keyCode==103)||(keyCode==104)||(keyCode==105)||(keyCode==107) ||(keyCode==173); 
	}
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
}	
function onlyRateAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)||(keyCode==189)|| (keyCode==110); 
}
function disableBYPayAcc() {
	<c:if test="${accountInterface=='Y'}">
	 if (document.forms['pricingPayableForm'].elements['accountLine.payAccDate'].value!='') { 
		    document.forms['pricingPayableForm'].elements['accountLine.invoiceNumber'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.invoiceDate'].readOnly=true; 
		    document.forms['pricingPayableForm'].elements['accountLine.myFileFileName'].disabled=true;
		    document.forms['pricingPayableForm'].elements['ViewBtn'].className ='pricelistbtn-dis';
		    document.forms['pricingPayableForm'].elements['accountLine.receivedDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].disabled=true;
		    document.forms['pricingPayableForm'].elements['accountLine.note'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payPayableStatus'].disabled=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payPayableDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payPayableVia'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payPayableAmount'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payGl'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payPostDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.accruePayable'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.accruePayableReverse'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.accruePayableManual'].disabled=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payAccDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payXfer'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payXferUser'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.settledDate'].readOnly=true;
	    
	    <c:if test="${contractType}"> 
		    document.forms['pricingPayableForm'].elements['accountLine.payableContractExchangeRate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payableContractRateAmmount'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.payableContractCurrency'].disabled=true;		
			document.forms['pricingPayableForm'].elements['accountLine.payableContractExchangeRate'].disabled=true;
	    </c:if>
	  }
	</c:if>
	}
	

function checkPayAccPayableContractCurrency(){
	alert('You can not change the Contract Currency as Sent To Acc has been already filled');
	document.forms['pricingPayableForm'].elements['accountLine.payableContractValueDate'].focus();
	//document.getElementById("pricingPayableForm").reset();
}
function checkwriteOffReasonValidation(){
	<c:if test="${not empty accountLine.payPostDate}">
	alert('You can not change the Write Off Reason as Pay Post Date has been already filled');
	document.forms['pricingPayableForm'].elements['accountLine.writeOffReason'].focus();
	</c:if>
}

function payQstHideBlock(){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	var firstPersent="";
	var secondPersent=""; 
	var relo="" 
	     <c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
	        if(relo==""){ 
	        if(document.forms['pricingPayableForm'].elements['accountLine.payVatDescr'].value=='${entry.key}') {
		        var arr='${entry.value}';
		        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
		        	firstPersent=arr.split(':')[0];
		        	secondPersent=arr.split(':')[1];
		        }
	        relo="yes"; 
	       }  }
	    </c:forEach> 
	    if(firstPersent!='' && secondPersent!=''){
	        document.getElementById("qstPayVatGlLabel").style.display="block"; 
	        document.getElementById("qstPayVatGlId").style.display="block";   
	    }else{
	        document.getElementById("qstPayVatGlLabel").style.display="none"; 
	        document.getElementById("qstPayVatGlId").style.display="none";   
	    }
	    </c:if>
	}
	
function activeStatusCheck(target){
	 var receivableAmount = eval(document.forms['pricingPayableForm'].elements['accountLine.actualRevenue'].value);
	 var payableAmount = eval(document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value);
	 <c:if test="${accountInterface!='Y'}">
	 if(receivableAmount!='0'||payableAmount!='0')  {
	    if(document.forms['pricingPayableForm'].elements['accountLine.status'].checked != true) {
	    		alert("Receivable Detail Revenue or Payable Detail Expense is not zero");
	    		return false;
	    	} }
	    </c:if>
	    <c:if test="${accountInterface=='Y'}">
	    var payingStatusCheck=""
	    <configByCorp:fieldVisibility componentId="component.accountLine.approvedPayingStatus.disableStatus">
	    if(document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value=='A'){
	    	payingStatusCheck='yes';
	    } 
	    </configByCorp:fieldVisibility>
	     var accruePayable=document.forms['pricingPayableForm'].elements['accountLine.accruePayable'].value;
	     var accrueRevenue=document.forms['pricingPayableForm'].elements['accountLine.accrueRevenue'].value;
	     var recAccDate=document.forms['pricingPayableForm'].elements['accountLine.recAccDate'].value;
	     var payAccDate=document.forms['pricingPayableForm'].elements['accountLine.payAccDate'].value;
	     var receivedInvoiceDate=document.forms['pricingPayableForm'].elements['accountLine.receivedInvoiceDate'].value;
	    if(accruePayable!=''||accrueRevenue!=''||recAccDate!=''||payAccDate!=''||receivedInvoiceDate!='' || payingStatusCheck!=''){
	    if(document.forms['pricingPayableForm'].elements['accountLine.status'].checked != true) {
	    	if(payingStatusCheck!=''){
	    		alert("This account line cannot be deactivated as either it has been Accrued, Payable Approved, transferred to Accounting and/or Invoiced."); 
	    	}else{ 
	    	   alert("This account line cannot be deactivated as either it has been Accrued, transferred to Accounting and/or Invoiced.");
	    	}
	    	document.forms['accountLineReceivableForm'].elements['accountLine.status'].disabled=true;
	    		return false;
	    	}  }
	    </c:if> 
	    <c:if test="${((trackingStatus.accNetworkGroup) && (billingDMMContractType) && (trackingStatus.soNetworkGroup) )}">
	    <c:if test="${not empty accountLine.id}">
		 //progressBarAutoSave('1');
		 var idss='${accountLine.id}';
	     var url="checkNetworkAgentInvoice.html?ajax=1&decorator=simple&popup=true&networkAgentId=" + encodeURI(idss);
	     httpNetworkAgent.open("GET", url, true);
	     httpNetworkAgent.onreadystatechange = checkNetworkAgentInvoiceResponse; 
	     httpNetworkAgent.send(null);
	     </c:if>  </c:if> 
}
function checkNetworkAgentInvoiceResponse(){
if (httpNetworkAgent.readyState == 4)  { 
    var results = httpNetworkAgent.responseText
    results = results.trim(); 
    if(results=='true'){
 	   alert("This account line cannot be deactivated as either it has been transferred to Accounting and/or Invoiced in UTSI Instance ");
 	   document.forms['pricingPayableForm'].elements['accountLine.status'].checked = true
 	   document.forms['pricingPayableForm'].elements['accountLine.status'].disabled=true;
 	   return false;
    } }   
	  //progressBarAutoSave('0');
}
var httpNetworkAgent= getHTTPObject77();
var http7= getHTTPObject77();
var http1985 = getHTTPObject77();
function getHTTPObject77() {
   var xmlhttp;
   if(window.XMLHttpRequest)  {
       xmlhttp = new XMLHttpRequest();
   }
   else if (window.ActiveXObject)  {
       xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
       if (!xmlhttp)  {
           xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
       } }
   return xmlhttp;
}

function detailsView(){
	var res=document.forms['pricingPayableForm'].elements['accountLine.myFileFileName'].value;
	if(res!=''){		
		res = res.split(":");
	if(res[0]!=null && res[0].trim()!=''){		
		openWindow('ImageServletAction.html?id='+res[0].trim()+'&decorator=popup&popup=true',900,600);
	}
	}else{
		alert("Please Select Document....");
	} 
}

function convertItProperDateFormate(target){ 
	var calArr=target.split("-");
	var year=calArr[2];
	year="20"+year;
	var month=calArr[1];
	  if(month == 'Jan') { month = "01"; }  
	  else if(month == 'Feb') { month = "02"; } 
	  else if(month == 'Mar') { month = "03"; } 
	  else if(month == 'Apr') { month = "04"; } 
	  else if(month == 'May') { month = "05"; } 
	  else if(month == 'Jun') { month = "06"; }  
	  else if(month == 'Jul') { month = "07"; } 
	  else if(month == 'Aug') { month = "08"; }  
	  else if(month == 'Sep') { month = "09"; }  
	  else if(month == 'Oct') { month = "10"; }  
	  else if(month == 'Nov') { month = "11"; }  
	  else if(month == 'Dec') { month = "12"; }	
	var day=calArr[0];
	var date=year+"-"+month+"-"+day;
	return date;
}
var http1981= getHTTPObject77();
function checkVendorInvoice(name) { 
	   var payingStatus=document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value; 
	   var vendorCode=document.forms['pricingPayableForm'].elements['accountLine.vendorCode'].value;
	   vendorCode=vendorCode.trim();
	   var country=document.forms['pricingPayableForm'].elements['accountLine.country'].value;
	   country=country.trim();
	   var invoiceNumber=document.forms['pricingPayableForm'].elements['accountLine.invoiceNumber'].value;
	   invoiceNumber = invoiceNumber.trim();
	   var invoiceDate='';
	   try{
	   invoiceDate=document.forms['pricingPayableForm'].elements['accountLine.invoiceDate'].value;
	   }catch(e){}
	   invoiceDate=invoiceDate.trim();
	   if(invoiceDate!=''){
		   invoiceDate=convertItProperDateFormate(invoiceDate);
	   }
	   invoiceNumber=invoiceNumber.trim();
	   var str='0';
		<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
			str='1';
		</configByCorp:fieldVisibility>
	   if(payingStatus=='A' || payingStatus=='C') {
		     if(invoiceNumber==''){
		         alert("Please fill invoice# in payable detail.");
		         document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value = '${accountLine.payingStatus}';
			}else if(invoiceDate=='') {
	  	       	alert("Please fill Invoice Date in the Payable Details section.");
	  	       	document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value = '${accountLine.payingStatus}';
			}else if(str=='1' && payingStatus=='A'){
				selectWriteOffReason();
			}else{
				<c:choose>
		          <c:when test="${accCorpID=='UTSI'}">
					   <c:if test="${not empty accountLine.id}">
					   var idss='${accountLine.id}';
			            var url="getAllAccListAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&invoiceDateNew="+invoiceDate+"&currencyNew="+country+"&idss="+idss;
					    http1985.open("GET", url, true);
					    http1985.onreadystatechange = handleHttpResponseAccListData;
					    http1985.send(null); 
						</c:if>
					   <c:if test="${empty accountLine.id}">
			            var url="getAllAccListAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&invoiceDateNew="+invoiceDate+"&currencyNew="+country+"&idss=null";
					    http1985.open("GET", url, true);
					    http1985.onreadystatechange = handleHttpResponseAccListData;
					    http1985.send(null); 
						</c:if>
		          </c:when>
		          <c:otherwise>
		          
		          </c:otherwise>
		     </c:choose>	       
			  }    	}   
    <c:if test="${accCorpID=='SSCW' || accCorpID=='SUDD'}">
    if(name=='INV'){
	          if((invoiceNumber!='')&&(vendorCode!='')){
					   <c:if test="${not empty accountLine.id}">
					   var idss='${accountLine.id}';
					            var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss="+idss;
							    http1981.open("GET", url, true);
							    http1981.onreadystatechange = handleHttpResponseVendorInvoiceEnhancement;
							    http1981.send(null); 
					   </c:if>
					   <c:if test="${empty accountLine.id}">
					            var url="vendorInvoiceEnhancementAjax.html?ajax=1&decorator=simple&popup=true&vendorCodeNew="+vendorCode+"&invoiceNumberNew="+invoiceNumber+"&idss=null";
							    http1981.open("GET", url, true);
							    http1981.onreadystatechange = handleHttpResponseVendorInvoiceEnhancement;
							    http1981.send(null); 
					   </c:if>		   	            
	          }   } 
    </c:if>
    if(document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value=='C') {
 	   var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec"; 
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4); 
		document.forms['pricingPayableForm'].elements['accountLine.payPayableDate'].value=datam;
		document.forms['pricingPayableForm'].elements['accountLine.payPayableStatus'].selectedIndex =2;
		document.forms['pricingPayableForm'].elements['accountLine.payPayableVia'].value='Credit Card';
		document.forms['pricingPayableForm'].elements['accountLine.payPayableAmount'].value=document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value;
		document.forms['pricingPayableForm'].elements['accountLine.payPostDate'].value=document.forms['pricingPayableForm'].elements['systemDefaultPostDate1'].value;;
		document.forms['pricingPayableForm'].elements['accountLine.payAccDate'].value=datam;
		document.forms['pricingPayableForm'].elements['accountLine.payXferUser'].value='${updatedUserName}';
		document.forms['pricingPayableForm'].elements['accountLine.payXfer'].value='Credit Card';
    }else{
 	   <c:if test="${empty accountLine.payPayableDate}">
 	document.forms['pricingPayableForm'].elements['accountLine.payPayableDate'].value='${accountLine.payPayableDate}'
 		</c:if>
     document.forms['pricingPayableForm'].elements['accountLine.payPayableStatus'].value='${accountLine.payingStatus}';
     document.forms['pricingPayableForm'].elements['accountLine.payPayableVia'].value='${accountLine.payPayableVia}';
   	document.forms['pricingPayableForm'].elements['accountLine.payPayableAmount'].value='${accountLine.payPayableAmount}';
   	<c:if test="${empty accountLine.payPostDate}">
   	document.forms['pricingPayableForm'].elements['accountLine.payPostDate'].value='${accountLine.payPostDate}';
   	</c:if>
   	document.forms['pricingPayableForm'].elements['accountLine.payAccDate'].value='${accountLine.payAccDate}';
   	document.forms['pricingPayableForm'].elements['accountLine.payXferUser'].value='${accountLine.payXferUser}';
   	document.forms['pricingPayableForm'].elements['accountLine.payXfer'].value='${accountLine.payXfer}';  
    }
}

function handleHttpResponseAccListData(){
    if (http1985.readyState == 4){
        var results = http1985.responseText
        results = results.trim();
        if(results!=''){
	     		  document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value ='';
	    }  } 
    }
    
function handleHttpResponseVendorInvoiceEnhancement(){
    if (http1981.readyState == 4){
        var results = http1981.responseText
        results = results.trim();
        if(results!=''){
        	var catVal = document.forms['pricingPayableForm'].elements['accountLine.category'].value;
        	if(catVal=='Internal Cost'){
        	}else{
        		alert(results);
	               document.forms['pricingPayableForm'].elements['accountLine.invoiceNumber'].value="${accountLine.invoiceNumber}";
	                 } } }	 
    }
    
function assignInvoiceDate() {
 	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec"; 
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = daym+"-"+month+"-"+y.substring(2,4);
	var invoiceNumber=document.forms['pricingPayableForm'].elements['accountLine.invoiceNumber'].value;
 	invoiceNumber=invoiceNumber.trim();
 	document.forms['pricingPayableForm'].elements['accountLine.invoiceNumber'].value=invoiceNumber; 
	if(document.forms['pricingPayableForm'].elements['accountLine.invoiceNumber'].value!='') {
	  document.forms['pricingPayableForm'].elements['accountLine.receivedDate'].value=datam;
  	}else {
  	document.forms['pricingPayableForm'].elements['accountLine.receivedDate'].value='';
 	} 
}

function onlyDelForInvoiceDate(evt,targetElement){
	if(document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value!='A'){
		var keyCode = evt.which ? evt.which : evt.keyCode;
		  if(keyCode==46){
		  		targetElement.value = '';
		  }else{
		  if(keyCode==8){
		  	targetElement.value = '';
		  }
		  	return false;
		  }
	}else{
	  	return false;
	} 
}

function checkPayAcc() {
	alert('You can not change the Status as Sent To Acc has been already filled');
	document.forms['pricingPayableForm'].elements['accountLine.payPayableStatus'].focus();
	//document.getElementById("pricingPayableForm").reset(); 
}
function  checkPayAccCountry(){
	alert('You can not change the Currency as Sent To Acc has been already filled');
	document.forms['pricingPayableForm'].elements['accountLine.country'].focus();
	//document.getElementById("pricingPayableForm").reset();
}
function getCommissionAmtFromBCode(){
 	var commAmt = document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value;
	var agree = true;
	var distAmt ="0.00"
	if(commAmt != 0 || commAmt != 0.00){
		agree=confirm("Continuing with this calculation will overwrite the previous commission Amount");
	} 
 	if (agree){
		document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value="0.00";
		<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		distAmt= document.forms['pricingPayableForm'].elements['accountLine.distributionAmount'].value;
	   </c:if>
		var actualAmt = document.forms['pricingPayableForm'].elements['accountLine.actualRevenue'].value;
	    var glcomm = document.forms['pricingPayableForm'].elements['accountLine.commission'].value;
	    var url="commAmount.html?ajax=1&decorator=simple&popup=true&sid=${serviceOrder.id}&distAmt=" + encodeURI(distAmt)+ "&glcomm=" + encodeURI(glcomm)+"&actualAmt="+encodeURI(actualAmt);
	    http7.open("GET", url, true);
	    http7.onreadystatechange = handleHttpResponse22;
	    http7.send(null);
	}else{
		return false;
	}  
 }
function handleHttpResponse22(){
		var disAmt =0
		<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		disAmt = document.forms['pricingPayableForm'].elements['accountLine.distributionAmount'].value;
		</c:if>
		<c:if test="${fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 
		disAmt = document.forms['pricingPayableForm'].elements['accountLine.actualRevenue'].value;
		</c:if>
		 if(disAmt>0 || disAmt>0.00){
             if (http7.readyState == 4) {
                var results3 = http7.responseText
                results3 = results3.trim();               
                if(results3.length>0) {
               		document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value = results3*1;
               	    var mydate=new Date();
					var daym;
					var year=mydate.getFullYear()
					var y=""+year;
					if (year < 1000)
					year+=1900
					var day=mydate.getDay()
					var month=mydate.getMonth()+1
					if(month == 1)month="Jan";
					if(month == 2)month="Feb";
					if(month == 3)month="Mar";
					if(month == 4)month="Apr";
					if(month == 5)month="May";
					if(month == 6)month="Jun";
					if(month == 7)month="Jul";
					if(month == 8)month="Aug";
					if(month == 9)month="Sep";
					if(month == 10)month="Oct";
					if(month == 11)month="Nov";
					if(month == 12)month="Dec"; 
					var daym=mydate.getDate()
					if (daym<10)
					daym="0"+daym
					var datam = daym+"-"+month+"-"+y.substring(2,4); 
	               		document.forms['pricingPayableForm'].elements['accountLine.invoiceDate'].value = datam;
	               		document.forms['pricingPayableForm'].elements['accountLine.exchangeRate'].value = "1.00";
	               		document.forms['pricingPayableForm'].elements['accountLine.localAmount'].value = results3*1;
                 }else{
	                 document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value="0.00";
					 document.forms['pricingPayableForm'].elements['accountLine.commission'].value="";
			   }   } } 
 }

function autoPayPayableDate(targetElement) {
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec"; 
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = daym+"-"+month+"-"+y.substring(2,4);
	targetElement.form.elements['accountLine.payPayableDate'].value=datam; 
}

function distributeOrdersAmount(targetButton){
	  var agree=confirm("This expense will be distributed to all service orders for this groupage order.  Would you like to continue?" );
	  if(agree){ 
		  document.forms['pricingPayableForm'].action ='distributeOrdersAmount.html';
			document.forms['pricingPayableForm'].submit();
	  }  
}

function updateExchangeRate(currency,field){
	document.forms['pricingPayableForm'].elements[field].value=findExchangeRateGlobal(currency.value);
}
function findExchangeRateGlobal(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function calculatePayCurrency(target,fieldName){
	var basis=document.forms['pricingPayableForm'].elements['accountLine.basis'].value;
	var baseRateVal=1;
	var actualDiscount=document.forms['pricingPayableForm'].elements['accountLine.actualDiscount'].value;	
	var recQuantity=document.forms['pricingPayableForm'].elements['accountLine.recQuantity'].value;	
    var actualDiscount=0.00;
    var actualExpense=0.00;
   
	if(recQuantity=='' ||recQuantity=='0' ||recQuantity=='0.0' ||recQuantity=='0.00') {
		 recQuantity = document.forms['pricingPayableForm'].elements['accountLine.recQuantity'].value=1;
    } 
   if( basis=="cwt" || basis=="%age"){
     	 baseRateVal=100;
   }else if(basis=="per 1000"){
     	 baseRateVal=1000;
   }else{
     	 baseRateVal=1;  	
	}		
 <c:if test="${contractType}">
  if(target=='accountLine.payableContractRateAmmount'){
		var payableContractCurrency=document.forms['pricingPayableForm'].elements['accountLine.payableContractCurrency'].value;		
		var payableContractExchangeRate=document.forms['pricingPayableForm'].elements['accountLine.payableContractExchangeRate'].value;
		var payableContractRateAmmount=document.forms['pricingPayableForm'].elements['accountLine.payableContractRateAmmount'].value;
		var payableContractValueDate=document.forms['pricingPayableForm'].elements['accountLine.payableContractValueDate'].value;
		if(payableContractCurrency!=''){
			if(fieldName!='' && fieldName=='accountLine.payableContractCurrency'){
				document.forms['pricingPayableForm'].elements['accountLine.payableContractValueDate'].value=currentDateGlobal();
			}
		    document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value=payableContractRateAmmount/payableContractExchangeRate;
		}
	   var country=document.forms['pricingPayableForm'].elements['accountLine.country'].value;		
	   var exchangeRate=document.forms['pricingPayableForm'].elements['accountLine.exchangeRate'].value;
	   var localAmount=document.forms['pricingPayableForm'].elements['accountLine.localAmount'].value;
	   var valueDate=document.forms['pricingPayableForm'].elements['accountLine.valueDate'].value;
	   	   actualExpense=document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value;
	   		if(country!=''){
	   			if(fieldName!='' && fieldName=='accountLine.country'){
	   				document.forms['pricingPayableForm'].elements['accountLine.valueDate'].value=currentDateGlobal();
	   			}
	   			localAmount=actualExpense*exchangeRate;
	   	 		document.forms['pricingPayableForm'].elements['accountLine.localAmount'].value=localAmount;   	 		
	    	}
	}else{
		var country=document.forms['pricingPayableForm'].elements['accountLine.country'].value;		
		var exchangeRate=document.forms['pricingPayableForm'].elements['accountLine.exchangeRate'].value;
		var localAmount=document.forms['pricingPayableForm'].elements['accountLine.localAmount'].value;
		var valueDate=document.forms['pricingPayableForm'].elements['accountLine.valueDate'].value;
		if(country!=''){
			if(fieldName!='' && fieldName=='accountLine.country'){
				document.forms['pricingPayableForm'].elements['accountLine.valueDate'].value=currentDateGlobal();
			}
			document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value=localAmount/exchangeRate;
		}
		var payableContractCurrency=document.forms['pricingPayableForm'].elements['accountLine.payableContractCurrency'].value;		
		var payableContractExchangeRate=document.forms['pricingPayableForm'].elements['accountLine.payableContractExchangeRate'].value;
		var payableContractRateAmmount=document.forms['pricingPayableForm'].elements['accountLine.payableContractRateAmmount'].value;
		var payableContractValueDate=document.forms['pricingPayableForm'].elements['accountLine.payableContractValueDate'].value;
			actualExpense=document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value;
		   	if(payableContractCurrency!=''){
		   		if(fieldName!='' && fieldName=='accountLine.payableContractCurrency'){
		   			document.forms['pricingPayableForm'].elements['accountLine.payableContractValueDate'].value=currentDateGlobal();
		   		}
		   		payableContractRateAmmount=actualExpense*payableContractExchangeRate;
		   	 	document.forms['pricingPayableForm'].elements['accountLine.payableContractRateAmmount'].value=payableContractRateAmmount;   	 		
		    }
	}
	
	</c:if>
	<c:if test="${!contractType}">
	if(target=='accountLine.localAmount'){
		var country=document.forms['pricingPayableForm'].elements['accountLine.country'].value;		
		var exchangeRate=document.forms['pricingPayableForm'].elements['accountLine.exchangeRate'].value;
		var localAmount=document.forms['pricingPayableForm'].elements['accountLine.localAmount'].value;
		var valueDate=document.forms['pricingPayableForm'].elements['accountLine.valueDate'].value;
		if(country!=''){
			if(fieldName!='' && fieldName=='accountLine.country'){
				document.forms['pricingPayableForm'].elements['accountLine.valueDate'].value=currentDateGlobal();
			}
		   document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value=localAmount/exchangeRate;
		}
	}else{
		var country=document.forms['pricingPayableForm'].elements['accountLine.country'].value;		
		var exchangeRate=document.forms['pricingPayableForm'].elements['accountLine.exchangeRate'].value;
		var localAmount=document.forms['pricingPayableForm'].elements['accountLine.localAmount'].value;
		var valueDate=document.forms['pricingPayableForm'].elements['accountLine.valueDate'].value;
		if(country!=''){
			if(fieldName!='' && fieldName=='accountLine.country'){
				document.forms['pricingPayableForm'].elements['accountLine.valueDate'].value=currentDateGlobal();
			}
			document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value=localAmount/exchangeRate;
		}
	}
	</c:if>
}
function currentDateGlobal(){
	var mydate=new Date();
   	var daym;
   	var year=mydate.getFullYear()
   	var y=""+year;
   	if (year < 1000)
   	year+=1900
   	var day=mydate.getDay()
   	var month=mydate.getMonth()+1
   	if(month == 1)month="Jan";
   	if(month == 2)month="Feb";
   	if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function checkFloat1(temp,fieldName) { 
	negativeCount = 0;
	var check='';  
	var i;
	var dotcheck=0;
	var s = temp;
	if(s.indexOf(".") == -1){
		dotcheck=eval(s.length)
	}else{
		dotcheck=eval(s.indexOf(".")-1)
	}
	var count = 0;
	var countArth = 0;
	for (i = 0; i < s.length; i++) {   
		var c = s.charAt(i); 
		if(c == '.') {
			count = count+1
		}
		if(c == '-') {
			countArth = countArth+1
		}
		if((i!=0)&&(c=='-')) {
			alert("Invalid data." ); 
			document.forms['pricingPayableForm'].elements[fieldName].select();
			document.forms['pricingPayableForm'].elements[fieldName].value='';
			return false;
		} 	
		if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))  {
			check='Invalid'; 
		}
	}
	if(dotcheck>15){
		check='tolong';
	}
	if(check=='Invalid'){ 
		alert("Invalid data." ); 
		document.forms['pricingPayableForm'].elements[fieldName].select();
		document.forms['pricingPayableForm'].elements[fieldName].value='';
		return false;
	} else if(check=='tolong'){ 
		alert("Data Too Long." ); 
		document.forms['pricingPayableForm'].elements[fieldName].select();
		document.forms['pricingPayableForm'].elements[fieldName].value='';
		return false;
	} else{
		s=Math.round(s*100)/100;
		var value=""+s;
		if(value.indexOf(".") == -1){
			value=value+".00";
		} 
		if((value.indexOf(".")+3 != value.length)){
			value=value+"0";
		}
		value = document.forms['pricingPayableForm'].elements[fieldName].value=value;
		return value;
	}
}
function calculatePayRate(){ 
	var aid=document.forms['pricingPayableForm'].elements['aid'].value;
	var actualExpense = checkFloat1(document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value,'accountLine.actualExpense');
	 window.opener.setFieldValue('actualExpense'+aid,actualExpense);
	 window.opener.setFieldValue('recQuantity'+aid,document.forms['pricingPayableForm'].elements['accountLine.recQuantity'].value);
	 window.opener.setFieldValue('payingStatus'+aid,document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value);
    <c:if test="${contractType}">
	 window.opener.setFieldValue('payableContractCurrency'+aid,document.forms['pricingPayableForm'].elements['accountLine.payableContractCurrency'].value);
	 window.opener.setFieldValue('payableContractExchangeRate'+aid,document.forms['pricingPayableForm'].elements['accountLine.payableContractExchangeRate'].value);
	 window.opener.setFieldValue('payableContractRateAmmount'+aid,document.forms['pricingPayableForm'].elements['accountLine.payableContractRateAmmount'].value);
	 window.opener.setFieldValue('payableContractValueDate'+aid,document.forms['pricingPayableForm'].elements['accountLine.payableContractValueDate'].value);
    </c:if>  
	 window.opener.setFieldValue('country'+aid,document.forms['pricingPayableForm'].elements['accountLine.country'].value);
	 window.opener.setFieldValue('exchangeRate'+aid,document.forms['pricingPayableForm'].elements['accountLine.exchangeRate'].value);
	 window.opener.setFieldValue('localAmount'+aid,document.forms['pricingPayableForm'].elements['accountLine.localAmount'].value);
	 window.opener.setFieldValue('valueDate'+aid,document.forms['pricingPayableForm'].elements['accountLine.valueDate'].value);
	 window.opener.getId(aid);
	 if(document.forms['pricingPayableForm'].elements['formStatus'].value=='2'){
		    window.opener.calculateActualRevenue(aid,'','FX','','','');
		    window.opener.changeStatus();
	}
	calculateRecRate();
}
function calculateRecRate(){ 
	var invoiceNumber = document.forms['pricingPayableForm'].elements['accountLine.recInvoiceNumber'].value; 
    var billingCurrencyUpdateFlag = document.forms['pricingPayableForm'].elements['billingCurrencyUpdateFlag'].value;       
	var aid=document.forms['pricingPayableForm'].elements['aid'].value;
	var actualRevenue = checkFloat1(document.forms['pricingPayableForm'].elements['accountLine.actualRevenue'].value,'accountLine.actualRevenue');
	 window.opener.setFieldValue('actualRevenue'+aid,actualRevenue);
	 window.opener.setFieldValue('recRate'+aid,document.forms['pricingPayableForm'].elements['accountLine.recRate'].value);
	 window.opener.setFieldValue('recQuantity'+aid,document.forms['pricingPayableForm'].elements['accountLine.recQuantity'].value);
    <c:if test="${contractType}">
	 window.opener.setFieldValue('contractCurrency'+aid,document.forms['pricingPayableForm'].elements['accountLine.contractCurrency'].value);
	 window.opener.setFieldValue('contractExchangeRate'+aid,document.forms['pricingPayableForm'].elements['accountLine.contractExchangeRate'].value);
	 window.opener.setFieldValue('contractRate'+aid,document.forms['pricingPayableForm'].elements['accountLine.contractRate'].value);
	 window.opener.setFieldValue('contractRateAmmount'+aid,document.forms['pricingPayableForm'].elements['accountLine.contractRateAmmount'].value);
	 window.opener.setFieldValue('contractValueDate'+aid,document.forms['pricingPayableForm'].elements['accountLine.contractValueDate'].value);
    </c:if>
    <c:if test="${multiCurrency=='Y'}">
	 window.opener.setFieldValue('recRateCurrency'+aid,document.forms['pricingPayableForm'].elements['accountLine.recRateCurrency'].value);
	 window.opener.setFieldValue('recRateExchange'+aid,document.forms['pricingPayableForm'].elements['accountLine.recRateExchange'].value);
	 window.opener.setFieldValue('recCurrencyRate'+aid,document.forms['pricingPayableForm'].elements['accountLine.recCurrencyRate'].value);
	 window.opener.setFieldValue('actualRevenueForeign'+aid,document.forms['pricingPayableForm'].elements['accountLine.actualRevenueForeign'].value);
	 window.opener.setFieldValue('racValueDate'+aid,document.forms['pricingPayableForm'].elements['accountLine.racValueDate'].value);
	 </c:if>
	 window.opener.getId(aid);
    if(document.forms['pricingPayableForm'].elements['formStatus'].value=='2'){
    	window.opener.calculateActualRevenue(aid,'','FX','','','');
    window.opener.changeStatus();
	    <c:if test="${multiCurrency=='Y'}">
		    if(billingCurrencyUpdateFlag=='YES'){
		    	invoiceNumber=invoiceNumber+"~"+aid;
		    	window.opener.appendFieldValue('oldRecRateCurrency',invoiceNumber);
		    }
		 </c:if>
    }
    savePayableDetailsForms();
}
function changeStatus() {
	   document.forms['pricingPayableForm'].elements['formStatus'].value = '2'; 
}

function forDays(){
	 document.forms['pricingPayableForm'].elements['checkForDaysClick'].value ='1';
	}
function findAuthorizationNo(targ) {
	   if (document.forms['pricingPayableForm'].elements['accountLine.recAccDate'].value!='')  {
	    alert("You can not change the authorization No as Sent To Acc has been already filled");
	    }  else { 
		 document.getElementById('rateImage').src = "<c:url value='/images/auth-ref.png'/>"; 
	     if(targ == 'a')  {
	    	 if(document.forms['pricingPayableForm'].elements['accountLine.billToCode'].value =='${billing.billToCode}'){
	    		 if('${billing.billToAuthority}'==''){
	    			 document.forms['pricingPayableForm'].elements['accountLine.authorization'].value="";
	    			 alert("Please select authorization No from billing")
	    		 }else{
	    		 document.forms['pricingPayableForm'].elements['accountLine.authorization'].value='${billing.billToAuthority}';
	    		 }
	    	 }
	    	 if(document.forms['pricingPayableForm'].elements['accountLine.billToCode'].value =='${billing.billTo2Code}'){
	    		 if('${billing.billTo2Authority}'==''){
	    			 document.forms['pricingPayableForm'].elements['accountLine.authorization'].value="";
	    			 alert("please select authorization No from billing (Secondary Billing section) ")
	    		 }else{
	    		 document.forms['pricingPayableForm'].elements['accountLine.authorization'].value='${billing.billTo2Authority}';
	    		 }
	    	}
	    	 document.getElementById('rateImage').src = "<c:url value='/images/auth-ref.png'/>";
	      } else  {
	    	  if(document.forms['pricingPayableForm'].elements['accountLine.billToCode'].value =='${billing.billToCode}'){
	    			document.forms['pricingPayableForm'].elements['accountLine.authorization'].value='${billing.billToAuthority}';
	    		}
	    	 if(document.forms['pricingPayableForm'].elements['accountLine.billToCode'].value =='${billing.billTo2Code}'){
  					document.forms['pricingPayableForm'].elements['accountLine.authorization'].value='${billing.billTo2Authority}';
  				}
	     }
	      
	    } 
	}
	
function copyReceivedInvoiceDate() { 
	   <c:if test="${multiCurrency=='Y'}">
	    document.forms['pricingPayableForm'].elements['checkReceivedInvoiceDate'].value ='1';
	    </c:if>
	}
function copyActSenttoclient() {
	  document.forms['pricingPayableForm'].elements['checkActSenttoclient'].value ='1';
	}
	
var elemtId = '';
function triggerCalendarId(elementId){
	return elemtId = elementId;
}
function actSentToclient(){
	if(elemtId ==  'sendActualToClient-trigger'){
		elemtId='';
		updateActSenttoclient();
	}
}
function updateActSenttoclient() { 
   if(document.forms['pricingPayableForm'].elements['accountLine.sendActualToClient'].value!='' && document.forms['pricingPayableForm'].elements['accountLine.recInvoiceNumber'].value!='') {
    var invoice= document.forms['pricingPayableForm'].elements['accountLine.recInvoiceNumber'].value
    var agree =confirm("Do you want to copy the actual sent to client date to all lines of invoice # ("+invoice+")?");
    if(agree) {
     document.forms['pricingPayableForm'].elements['accountLine.sendActualToClient'].select();
     document.forms['pricingPayableForm'].elements['dateUpdate'].value='Y'; 
    } else{
    document.forms['pricingPayableForm'].elements['dateUpdate'].value='N';
    }  }
 document.forms['pricingPayableForm'].elements['checkActSenttoclient'].value ='';
 }

var varNameAcct = '';
function checkAcctStatus(elementName){
	if(elementName.indexOf('_') > 0){
		varNameAcct = elementName.split('_')[0];
	}else{
		varNameAcct = elementName;
	return varNameAcct;
	}
}
function hitAcct(){
	if(varNameAcct == 'invoiceDate'){
		//checkPurchaseInvoiceProcessing('','INV');
		varNameAcct='';
	}else{
		varNameAcct='';
		return false;
	} 
}
	
function updateReceivedInvoiceDate(){
	  <c:if test="${multiCurrency=='Y'}">
	   if(document.forms['pricingPayableForm'].elements['accountLine.recInvoiceNumber'].value!='') {
	   var invoiceNumber = document.forms['pricingPayableForm'].elements['accountLine.recInvoiceNumber'].value; 
	   alert("Invoice for this line has already been generated, so this date will affect for the all invoice no "+invoiceNumber+".");
	   }
	   document.forms['pricingPayableForm'].elements['checkReceivedInvoiceDate'].value ='';
	    </c:if>
	}

function chechCreditInvoice(){
	 var actualRevenue=eval(document.forms['pricingPayableForm'].elements['accountLine.actualRevenue'].value); 
	 var creditInvoice=document.forms['pricingPayableForm'].elements['accountLine.creditInvoiceNumber'].value;
	 creditInvoice=creditInvoice.trim(); 
	 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
		 var distributionAmount=eval(document.forms['pricingPayableForm'].elements['accountLine.distributionAmount'].value);
		 if((distributionAmount<0) && (creditInvoice!='')){
		 	document.getElementById("creditInvoice").style.display="block";
		 	document.getElementById("creditInvoice1").style.display="block";
		 }else{
		 	document.getElementById("creditInvoice").style.display="none";
		 	document.getElementById("creditInvoice1").style.display="none";
		 }
		 </c:if>
		 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
		 if((actualRevenue<0) && (creditInvoice!='')){
		 	document.getElementById("creditInvoice").style.display="block";
		 	document.getElementById("creditInvoice1").style.display="block";
		 }else{
			document.getElementById("creditInvoice").style.display="none";
			document.getElementById("creditInvoice1").style.display="none";
		}
	</c:if>
	}
function recQstHideBlock(){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	var firstPersent="";
	var secondPersent=""; 
	var relo="" 
	     <c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
	        if(relo==""){ 
	        if(document.forms['pricingPayableForm'].elements['accountLine.recVatDescr'].value=='${entry.key}') {
		        var arr='${entry.value}';
		        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
		        	firstPersent=arr.split(':')[0];
		        	secondPersent=arr.split(':')[1];
		        }
	        relo="yes"; 
	       }  }
	    </c:forEach> 
	    if(firstPersent!='' && secondPersent!=''){
	        document.getElementById("qstRecVatGlLabel").style.display="block"; 
	        document.getElementById("qstRecVatGlId").style.display="block";   
	        document.getElementById("qstRecVatAmtGlLabel").style.display="block"; 
	        document.getElementById("qstRecVatAmtGlId").style.display="block";  
	    } else{
	        document.getElementById("qstRecVatGlLabel").style.display="none"; 
	        document.getElementById("qstRecVatGlId").style.display="none";   
	        document.getElementById("qstRecVatAmtGlLabel").style.display="none"; 
	        document.getElementById("qstRecVatAmtGlId").style.display="none";  			    
	    }
	    </c:if>
	}
	
function calReference() { 
	 if(document.forms['pricingPayableForm'].elements['accountLine.actualRevenue'].value==0 || document.forms['pricingPayableForm'].elements['accountLine.billToCode'].value=='') { 
	    alert("You can not fill Reference# ,Bill to and Revenue must have some values");  
	    document.forms['pricingPayableForm'].elements['accountLine.externalReference'].value='';
	 }   
	}
function changeCalOpenarvalue() {
 	document.forms['pricingPayableForm'].elements['calOpener'].value='open';
 }
 
function disableBYRecAcc() { 
	<c:if test="${accountInterface=='Y'}">
	 if (document.forms['pricingPayableForm'].elements['accountLine.recAccDate'].value!='') { 
		    document.forms['pricingPayableForm'].elements['accountLine.authorization'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.receivedInvoiceDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.description'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.externalReference'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.paymentStatus'].disabled=true;
		    document.forms['pricingPayableForm'].elements['accountLine.receivedAmount'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.statusDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.sendEstToClient'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.sendActualToClient'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.doNotSendtoClient'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.recGl'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.accrueRevenue'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.accrueRevenueReverse'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.accrueRevenueManual'].disabled=true;
		    document.forms['pricingPayableForm'].elements['accountLine.recPostDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.recAccDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.recXfer'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.recXferUser'].readOnly=true;
		    <configByCorp:fieldVisibility componentId="component.field.auditCompleteDateForUTSI">
		    	document.forms['pricingPayableForm'].elements['accountLine.auditCompleteDate'].readOnly=true;
		    </configByCorp:fieldVisibility>
	    <c:if test="${multiCurrency=='Y'}">
		    document.forms['pricingPayableForm'].elements['accountLine.recRateCurrency'].disabled=true;
		    document.forms['pricingPayableForm'].elements['accountLine.racValueDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.recCurrencyRate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.recRateExchange'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.actualRevenueForeign'].readOnly=true;
	    </c:if>
	    <c:if test="${contractType}">
		    document.forms['pricingPayableForm'].elements['accountLine.contractCurrency'].disabled=true;
		    document.forms['pricingPayableForm'].elements['accountLine.contractValueDate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.contractRate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.contractExchangeRate'].readOnly=true;
		    document.forms['pricingPayableForm'].elements['accountLine.contractRateAmmount'].readOnly=true;
	    </c:if>
	  }
	  </c:if>
	}
	
function checkMultiAuthorization() {  
   var billToCode = document.forms['pricingPayableForm'].elements['accountLine.billToCode'].value;  
   var url="checkMultiAuthorization.html?ajax=1&decorator=simple&popup=true&accPartnerCode="+billToCode;  
    http55.open("GET", url, true); 
    http55.onreadystatechange = handleHttpResponse41; 
    http55.send(null); 
} 
function handleHttpResponse41() { 
            if (http55.readyState == 4)  {
               var results = http55.responseText
               results = results.trim();
               results = results.replace('[','');
               results=results.replace(']',''); 
               var recAccDate; 
               <c:if test="${accountInterface=='Y'}"> 
               recAccDate=document.forms['pricingPayableForm'].elements['accountLine.recAccDate'].value;
               recAccDate=recAccDate.trim();
               </c:if> 
               if(results=='true')   {
               document.getElementById("hhid").style.display="none";
               document.forms['pricingPayableForm'].elements['accountLine.authorization'].readOnly=true; 
               }
               else if (recAccDate!='')   { 
               	fillAuthorizationByBillToCode();
               	document.forms['pricingPayableForm'].elements['accountLine.authorization'].readOnly=true;
               } else{  
               	fillAuthorizationByBillToCode();
               	document.getElementById("hhid").style.display="block"; 
               document.forms['pricingPayableForm'].elements['accountLine.authorization'].readOnly=true; 
               } } }
var http55 = getHTTPObject55();
function getHTTPObject55() {
   var xmlhttp;
   if(window.XMLHttpRequest)  {
       xmlhttp = new XMLHttpRequest();
   }
   else if (window.ActiveXObject)  {
       xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
       if (!xmlhttp)  {
           xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
       } }
   return xmlhttp;
}

function fillAuthorizationByBillToCode() { 
	var authorization=document.forms['pricingPayableForm'].elements['accountLine.authorization'].value;
	authorization=authorization.trim();
	if(authorization==''){
	if(document.forms['pricingPayableForm'].elements['accountLine.billToCode'].value =='${billing.billToCode}'){
		document.forms['pricingPayableForm'].elements['accountLine.authorization'].value='${billing.billToAuthority}';
		}
		if(document.forms['pricingPayableForm'].elements['accountLine.billToCode'].value =='${billing.billTo2Code}'){
			document.forms['pricingPayableForm'].elements['accountLine.authorization'].value='${billing.billTo2Authority}';
			}
	}
}

function calcDays() {
	<c:if test="${fn1:indexOf(systemDefaultstorage,serviceOrder.job)>=0}">
	 document.forms['pricingPayableForm'].elements['accountLine.storageDays'].value="";
	 var date2 = document.forms['pricingPayableForm'].elements['accountLine.storageDateRangeFrom'].value;	 
	 var date1 = document.forms['pricingPayableForm'].elements['accountLine.storageDateRangeTo'].value; 
	   var mySplitResult = date1.split("-");
	   var day = mySplitResult[0];
	   var month = mySplitResult[1];
	   var year = mySplitResult[2];
	  if(month == 'Jan') {   
	     month = "01";
	   } else if(month == 'Feb') {
	       month = "02";
	   } else if(month == 'Mar')  {
	       month = "03"
	   } else if(month == 'Apr') {
	       month = "04"
	   }  else if(month == 'May')  {
	       month = "05"
	   } else if(month == 'Jun') {
	       month = "06"
	   }  else if(month == 'Jul')  {
	       month = "07"
	   } else if(month == 'Aug')  {
	       month = "08"
	   }  else if(month == 'Sep')  {
	       month = "09"
	   }  else if(month == 'Oct')  {
	       month = "10"
	   }  else if(month == 'Nov')  {
	       month = "11"
	   }  else if(month == 'Dec') {
	       month = "12";
	   }
	   var finalDate = month+"-"+day+"-"+year;
	   var mySplitResult2 = date2.split("-");
	   var day2 = mySplitResult2[0];
	   var month2 = mySplitResult2[1];
	   var year2 = mySplitResult2[2];
	   if(month2 == 'Jan')  {
	       month2 = "01";
	   }  else if(month2 == 'Feb')  {
	       month2 = "02";
	   }  else if(month2 == 'Mar') {
	       month2 = "03"
	   }  else if(month2 == 'Apr')  {
	       month2 = "04"
	   }  else if(month2 == 'May')  {
	       month2 = "05"
	   }  else if(month2 == 'Jun') {
	       month2 = "06"
	   }   else if(month2 == 'Jul') {
	       month2 = "07"
	   }  else if(month2 == 'Aug') {
	       month2 = "08"
	   } else if(month2 == 'Sep')   {
	       month2 = "09"
	   }  else if(month2 == 'Oct')  {
	       month2 = "10"
	   }  else if(month2 == 'Nov')  {
	       month2 = "11"
	   }  else if(month2 == 'Dec')  {
	       month2 = "12";
	   }
	  var finalDate2 = month2+"-"+day2+"-"+year2;
	  date1 = finalDate.split("-");
	  date2 = finalDate2.split("-");
	  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
	  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
	  var daysApart = Math.round((sDate-eDate)/86400000);
	  document.forms['pricingPayableForm'].elements['accountLine.storageDays'].value = daysApart; 
	  if(daysApart<0) {
	    alert("Storage Date Range must be less than to date");
	    document.forms['pricingPayableForm'].elements['accountLine.storageDays'].value=''; 
	    document.forms['pricingPayableForm'].elements['accountLine.storageDateRangeTo'].value='';
	  }
	  if(document.forms['pricingPayableForm'].elements['accountLine.storageDays'].value=='NaN')  {
	     document.forms['pricingPayableForm'].elements['accountLine.storageDays'].value = '';
	   } 
	 document.forms['pricingPayableForm'].elements['checkForDaysClick'].value = '';
	 </c:if>
	}
	
function autoPopulateStatusDate(targetElement) { 
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec"; 
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = daym+"-"+month+"-"+y.substring(2,4);
	targetElement.form.elements['accountLine.statusDate'].value=datam; 
	<configByCorp:fieldVisibility componentId="component.accountLine.receivedAmount">
	if(document.forms['pricingPayableForm'].elements['accountLine.paymentStatus'].value=='Fully Paid'){
	document.forms['pricingPayableForm'].elements['accountLine.receivedAmount'].readOnly=true;
	document.forms['pricingPayableForm'].elements['accountLine.receivedAmount'].value=document.forms['pricingPayableForm'].elements['accountLine.actualRevenueForeign'].value;
	} else { 
	}
	</configByCorp:fieldVisibility>
}

function convertItProperDateFormate(target){ 
	var calArr=target.split("-");
	var year=calArr[2];
	year="20"+year;
	var month=calArr[1];
	  if(month == 'Jan') { month = "01"; }  
	  else if(month == 'Feb') { month = "02"; } 
	  else if(month == 'Mar') { month = "03"; } 
	  else if(month == 'Apr') { month = "04"; } 
	  else if(month == 'May') { month = "05"; } 
	  else if(month == 'Jun') { month = "06"; }  
	  else if(month == 'Jul') { month = "07"; } 
	  else if(month == 'Aug') { month = "08"; }  
	  else if(month == 'Sep') { month = "09"; }  
	  else if(month == 'Oct') { month = "10"; }  
	  else if(month == 'Nov') { month = "11"; }  
	  else if(month == 'Dec') { month = "12"; }	
	var day=calArr[0];
	var date=year+"-"+month+"-"+day;
	return date;
}

var http6 = getHTTPObject77();
var http100 = getHTTPObject77(); 
function getHTTPObject77() {
   var xmlhttp;
   if(window.XMLHttpRequest)  {
       xmlhttp = new XMLHttpRequest();
   }
   else if (window.ActiveXObject)  {
       xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
       if (!xmlhttp)  {
           xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
       } }
   return xmlhttp;
}
function getDistributedAmtFromBCode(){
	var disAmt = document.forms['pricingPayableForm'].elements['accountLine.distributionAmount'].value;
	var agree = true;
	if(disAmt != 0 || disAmt != 0.00){
		agree=confirm("Continuing with this calculation will overwrite the amount obtained from XML.");
	}
		if (agree){
			document.forms['pricingPayableForm'].elements['accountLine.distributionAmount'].value="0.00";
			var actualRevanue = document.forms['pricingPayableForm'].elements['accountLine.actualRevenue'].value;
		    var glType = document.forms['pricingPayableForm'].elements['accountLine.glType'].value;
		    var branchCode = document.forms['pricingPayableForm'].elements['accountLine.branchCode'].value;
		    var chargeCode = document.forms['pricingPayableForm'].elements['accountLine.chargeCode'].value;
		    var desc = document.forms['pricingPayableForm'].elements['accountLine.description'].value;
		    var url="distAmount.html?ajax=1&decorator=simple&popup=true&sid=${serviceOrder.id}&actualRevanue=" + encodeURI(actualRevanue)+ "&glType=" + encodeURI(glType)+ "&branchCode=" + encodeURI(branchCode)+"&chargeCode=" + encodeURI(chargeCode)+ "&desc=" +encodeURI(desc);
		    http6.open("GET", url, true);
		    http6.onreadystatechange = handleHttpResponse18;
		    http6.send(null);
		}else{
				return false;
		}
		chechCreditInvoice();
}
function handleHttpResponse18(){
            if (http6.readyState == 4) {
               var results1 = http6.responseText
               results1 = results1.trim();                
               if(results1.length>0 && results1.indexOf("DOCTYPE html") == -1 ) {
				var distAmount = 0; 
				if(results1.indexOf(".") == -1) {
					distAmount = results1;
				} else {
					distAmount = results1.substring(0,(results1.indexOf(".")+3));
				} 
              		document.forms['pricingPayableForm'].elements['accountLine.distributionAmount'].value = distAmount;
              		document.forms['pricingPayableForm'].elements['accountLine.distributionAmount'].focus();
                }else{ 
	                 document.forms['pricingPayableForm'].elements['accountLine.distributionAmount'].value="0.00";
					 document.forms['pricingPayableForm'].elements['accountLine.glType'].value="";
			   }          }     
   }
function driverCommission(){
	var chargeCheck=document.forms['pricingPayableForm'].elements['chargeCodeValidationVal'].value;
	if(chargeCheck=='' || chargeCheck==null){
	var actualRevenue="0";
	var distributionAmount="0";
	var estimateRevenueAmount="0";
	var driverCommissionSetUp='${driverCommissionSetUp}';
	var revisionRevenueAmount="0"; 
	var category=document.forms['pricingPayableForm'].elements['accountLine.category'].value;
	var vendorCode=document.forms['pricingPayableForm'].elements['accountLine.vendorCode'].value;
	var chargeCode=document.forms['pricingPayableForm'].elements['accountLine.chargeCode'].value;
	var actualRevenue=document.forms['pricingPayableForm'].elements['accountLine.actualRevenue'].value;
	 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
    distributionAmount=document.forms['pricingPayableForm'].elements['accountLine.distributionAmount'].value;
    </c:if>
    estimateRevenueAmount=document.forms['pricingPayableForm'].elements['accountLine.estimateRevenueAmount'].value;
    revisionRevenueAmount=document.forms['pricingPayableForm'].elements['accountLine.revisionRevenueAmount'].value; 
    var sid='<%=request.getParameter("sid")%>';
	if(category!=null && category!='' && vendorCode!=null && vendorCode!='' && chargeCode!=null && chargeCode!='' && (driverCommissionSetUp=='true' || driverCommissionSetUp==true)){
		 var url="driverCommissionAjax.html?ajax=1&decorator=simple&popup=true&sid="+sid+"&actualRevenue="+actualRevenue+"&accountLineDistribution="+distributionAmount+"&accountEstimateRevenueAmount="+estimateRevenueAmount+"&accountRevisionRevenueAmount="+revisionRevenueAmount+"&chargeCode="+chargeCode+"&vendorCode="+vendorCode;
		http100.open("GET", url, true);
	    http100.onreadystatechange = handleHttpResponseDriverCommission;
	    http100.send(null);
		}} }
function handleHttpResponseDriverCommission() { 
	if (http100.readyState == 4) {
		var results = http100.responseText
		results = results.trim();
	    if(results.length>1 && results!='0.00'){
	    	results=Math.round(results*100)/100;
	    	document.forms['pricingPayableForm'].elements['accountLine.actualExpense'].value=results;
		    document.forms['pricingPayableForm'].elements['accountLine.localAmount'].value=results;
       }    } 
}
function updateExchangeRateRec(currency,field,fieldName){
    var invoiceNumber = document.forms['pricingPayableForm'].elements['accountLine.recInvoiceNumber'].value; 
    invoiceNumber=invoiceNumber.trim();
	if(field=='accountLine.contractExchangeRate'){
		document.forms['pricingPayableForm'].elements[field].value=findExchangeRateGlobal(currency.value);
		calculateRecCurrency('accountLine.contractRate','accountLine.recRateCurrency~accountLine.contractCurrency','accountLine.racValueDate~accountLine.contractValueDate','accountLine.recRateExchange~accountLine.contractExchangeRate',fieldName);
	}else if(field=='accountLine.recRateExchange'){
		if(invoiceNumber!=''){	
		    if(document.forms['pricingPayableForm'].elements['userRoleExecutive'].value=="no"){
			    alert("Invoice for this line has already been generated, You cannot change Billing Currency.");
			    document.forms['pricingPayableForm'].elements['accountLine.recRateCurrency'].value='${accountLine.recRateCurrency}';
		    }else if (invoiceNumber!='' && (currency.value==null || currency.value==undefined || currency.value=='' )){
		    	alert("You cannot select blank Currency as Invoice for this line has already been generated. ")
			    document.forms['pricingPayableForm'].elements['accountLine.recRateCurrency'].value='${accountLine.recRateCurrency}';
		    }else if (document.forms['pricingPayableForm'].elements['userRoleExecutive'].value=="yes"){
			    var agree =confirm("Invoice for this line has already been generated and it will change all corresponding value for the invoice no "+invoiceNumber+".\n\n Are you sure you want to continue with the changes?");
			    if(agree) {
	      			document.forms['pricingPayableForm'].elements['billingCurrencyUpdateFlag'].value='YES';
	    			document.forms['pricingPayableForm'].elements[field].value=findExchangeRateGlobal(currency.value);
	    			calculateRecCurrency('accountLine.contractRate','','','',fieldName);	      			
			    }else{
				    document.forms['pricingPayableForm'].elements['accountLine.recRateCurrency'].value='${accountLine.recRateCurrency}';
			    }
		    }
		}else{
			document.forms['pricingPayableForm'].elements[field].value=findExchangeRateGlobal(currency.value);
			calculateRecCurrency('accountLine.contractRate','','','',fieldName);
		}
	}else{
		
	}
}

function calculateRecCurrency(target,field1,field2,field3,fieldName) {
		 if(field1!='' && field2!='' & field3!=''){
			 rateOnActualizationDate(field1,field2,field3,fieldName);
			}	   
		var basis=document.forms['pricingPayableForm'].elements['accountLine.basis'].value;
		var baseRateVal=1;
		var actualDiscount=document.forms['pricingPayableForm'].elements['accountLine.actualDiscount'].value;	
		var recQuantity=document.forms['pricingPayableForm'].elements['accountLine.recQuantity'].value;	
	   	var actualDiscount=0.00;
	   	var recRate=document.forms['pricingPayableForm'].elements['accountLine.recRate'].value;
	   
		 if(recQuantity=='' ||recQuantity=='0' ||recQuantity=='0.0' ||recQuantity=='0.00') {
			 recQuantity = document.forms['pricingPayableForm'].elements['accountLine.recQuantity'].value=1;
		  } 
	   if( basis=="cwt" || basis=="%age"){
	     	 baseRateVal=100;
	   }else if(basis=="per 1000"){
	     	 baseRateVal=1000;
	   }else{
	     	 baseRateVal=1;  	
		}		
	<c:if test="${contractType}">
	if(target=='accountLine.contractRate'){
		var contractCurrency=document.forms['pricingPayableForm'].elements['accountLine.contractCurrency'].value;		
		var contractExchangeRate=document.forms['pricingPayableForm'].elements['accountLine.contractExchangeRate'].value;
		var contractRate=document.forms['pricingPayableForm'].elements['accountLine.contractRate'].value;
		var contractRateAmmount=document.forms['pricingPayableForm'].elements['accountLine.contractRateAmmount'].value;
		var contractValueDate=document.forms['pricingPayableForm'].elements['accountLine.contractValueDate'].value;
		if(contractCurrency!=''){
			if(fieldName!='' && fieldName=='accountLine.contractCurrency'){
				document.forms['pricingPayableForm'].elements['accountLine.contractValueDate'].value=currentDateGlobal();
			}
			contractRateAmmount=(recQuantity*contractRate)/baseRateVal;
		   document.forms['pricingPayableForm'].elements['accountLine.contractRateAmmount'].value=contractRateAmmount;
		   document.forms['pricingPayableForm'].elements['accountLine.recRate'].value=contractRate/contractExchangeRate;
		}
		   var recRateCurrency=document.forms['pricingPayableForm'].elements['accountLine.recRateCurrency'].value;		
		   var recRateExchange=document.forms['pricingPayableForm'].elements['accountLine.recRateExchange'].value;
		   var recCurrencyRate=document.forms['pricingPayableForm'].elements['accountLine.recCurrencyRate'].value;
		   var actualRevenueForeign=document.forms['pricingPayableForm'].elements['accountLine.actualRevenueForeign'].value;
		   var racValueDate=document.forms['pricingPayableForm'].elements['accountLine.racValueDate'].value;
		   recRate=document.forms['pricingPayableForm'].elements['accountLine.recRate'].value;
		   	if(recRateCurrency!=''){
		   		if(fieldName!='' && fieldName=='accountLine.recRateCurrency'){
		   			document.forms['pricingPayableForm'].elements['accountLine.racValueDate'].value=currentDateGlobal();
		   		}
		   		recCurrencyRate=recRate*recRateExchange;
		   		document.forms['pricingPayableForm'].elements['accountLine.recCurrencyRate'].value=recCurrencyRate;
		   		actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
		   	 document.forms['pricingPayableForm'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeign;   	 		
		}
	}else if(target=='accountLine.recCurrencyRate'){
	   var recRateCurrency=document.forms['pricingPayableForm'].elements['accountLine.recRateCurrency'].value;		
	   var recRateExchange=document.forms['pricingPayableForm'].elements['accountLine.recRateExchange'].value;
	   var recCurrencyRate=document.forms['pricingPayableForm'].elements['accountLine.recCurrencyRate'].value;
	   var actualRevenueForeign=document.forms['pricingPayableForm'].elements['accountLine.actualRevenueForeign'].value;
	   var racValueDate=document.forms['pricingPayableForm'].elements['accountLine.racValueDate'].value;
	   	if(recRateCurrency!=''){
	   		if(fieldName!='' && fieldName=='accountLine.recRateCurrency'){
	   			document.forms['pricingPayableForm'].elements['accountLine.racValueDate'].value=currentDateGlobal();
	   		}
	   		actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
	   	 	document.forms['pricingPayableForm'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeign; 
	   		document.forms['pricingPayableForm'].elements['accountLine.recRate'].value=recCurrencyRate/recRateExchange;	
		}	
		var contractCurrency=document.forms['pricingPayableForm'].elements['accountLine.contractCurrency'].value;		
		var contractExchangeRate=document.forms['pricingPayableForm'].elements['accountLine.contractExchangeRate'].value;
		var contractRate=document.forms['pricingPayableForm'].elements['accountLine.contractRate'].value;
		var contractRateAmmount=document.forms['pricingPayableForm'].elements['accountLine.contractRateAmmount'].value;
		var contractValueDate=document.forms['pricingPayableForm'].elements['accountLine.contractValueDate'].value;
		recRate=document.forms['pricingPayableForm'].elements['accountLine.recRate'].value;
		if(contractCurrency!=''){
			if(fieldName!='' && fieldName=='accountLine.contractCurrency'){
				document.forms['pricingPayableForm'].elements['accountLine.contractValueDate'].value=currentDateGlobal();
			}
			contractRate=recRate*contractExchangeRate;
  			document.forms['pricingPayableForm'].elements['accountLine.contractRate'].value=contractRate;
  			contractRateAmmount=(recQuantity*contractRate)/baseRateVal;
	   		document.forms['pricingPayableForm'].elements['accountLine.contractRateAmmount'].value=contractRateAmmount;
		}
	}else{}
	</c:if>
	<c:if test="${!contractType}">
		var recRateCurrency=document.forms['pricingPayableForm'].elements['accountLine.recRateCurrency'].value;		
		var recRateExchange=document.forms['pricingPayableForm'].elements['accountLine.recRateExchange'].value;
		var recCurrencyRate=document.forms['pricingPayableForm'].elements['accountLine.recCurrencyRate'].value;
		var actualRevenueForeign=document.forms['pricingPayableForm'].elements['accountLine.actualRevenueForeign'].value;
		var racValueDate=document.forms['pricingPayableForm'].elements['accountLine.racValueDate'].value;
		if(recRateCurrency!=''){
			if(fieldName!='' && fieldName=='accountLine.recRateCurrency'){
				document.forms['pricingPayableForm'].elements['accountLine.racValueDate'].value=currentDateGlobal();
			}
	 		actualRevenueForeign=(recQuantity*recCurrencyRate)/baseRateVal;
		    if(actualDiscount!=0.00){
		    	actualRevenueForeign=(actualRevenueForeign*actualDiscount)/100;
		    }
		   document.forms['pricingPayableForm'].elements['accountLine.actualRevenueForeign'].value=actualRevenueForeign;
		   document.forms['pricingPayableForm'].elements['accountLine.recRate'].value=recCurrencyRate/recRateExchange; 		
		}
	</c:if>
}
function rateOnActualizationDate(field1,field2,field3,fieldName){
	<c:if test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
	<c:if test="${contractType}">
		var currency=""; 
		var currentDate=currentDateGlobal();
		try{
			currency=document.forms['pricingPayableForm'].elements[field1.trim().split('~')[0]].value;
			if(fieldName!='' && fieldName=='accountLine.recRateCurrency'){
				document.forms['pricingPayableForm'].elements[field2.trim().split('~')[0]].value=currentDate;
			}
			document.forms['pricingPayableForm'].elements[field3.trim().split('~')[0]].value=findExchangeRateGlobal(currency.value);
		}catch(e){}
		try{
			currency=document.forms['pricingPayableForm'].elements[field1.trim().split('~')[1]].value;
			if(fieldName!='' && fieldName=='accountLine.contractCurrency'){
				document.forms['pricingPayableForm'].elements[field2.trim().split('~')[1]].value=currentDate;
			}
			document.forms['pricingPayableForm'].elements[field3.trim().split('~')[1]].value=findExchangeRateGlobal(currency.value);
		}catch(e){}		
	</c:if>
	</c:if>
}

function selectWriteOffReason(){
	<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
	var payingStatusOld = '${accountLine.payingStatus}';
	var managerApprovalDate = document.forms['pricingPayableForm'].elements['accountLine.managerApprovalDate'].value;
	var payingStatusNew = document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value;
	var writeOffReasonVal = document.forms['pricingPayableForm'].elements['writeOffReasonVal'].value;
	var accruCheckedDate = '${serviceOrder.accrueChecked}';
	if(payingStatusOld == 'VH' && payingStatusNew=='A'){
		var writeOffReason = document.forms['pricingPayableForm'].elements['accountLine.writeOffReason'].value;
		if(writeOffReason.trim()==''){
			alert('Please select write off reason.');
			document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value = '${accountLine.payingStatus}';
		}else if((managerApprovalDate==null || managerApprovalDate=='') && (writeOffReasonVal=='')){
			alert('Please fill Manager Approval Date.');
			document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value = '${accountLine.payingStatus}';
		}
	}else if(payingStatusOld == 'MH' && payingStatusNew=='A'){
		if(managerApprovalDate==null || managerApprovalDate==''){
			alert('You can not change status to Approve, as Manager Approval Date is blank.');
			document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value = '${accountLine.payingStatus}';
		}
	}else if(payingStatusOld == 'AH' && payingStatusNew=='A'){
		if(accruCheckedDate==null || accruCheckedDate==''){
			alert('You can not change status to Approve, as Accrual Checked Date is blank.');
			document.forms['pricingPayableForm'].elements['accountLine.payingStatus'].value = '${accountLine.payingStatus}';
		}
	}else{}
	</configByCorp:fieldVisibility>
}

function updateManagerApprovalName(){
	if(elemtId ==  'managerApprovalDate_trigger'){
		elemtId='';
		document.forms['pricingPayableForm'].elements['accountLine.managerApprovalName'].value = document.forms['pricingPayableForm'].elements['username'].value;
	}
}

function findBucket2WriteOffReason(){
	var writeOffReasonValue = document.forms['pricingPayableForm'].elements['accountLine.writeOffReason'].value;
	new Ajax.Request('getBucket2WriteOffReasonAjax.html?ajax=1&decorator=simple&popup=true&writeOffReasonValue='+writeOffReasonValue,
			{
		method:'get',
		onSuccess: function(transport){
			var response = transport.responseText || "";
			response = response.trim();
			if(response!=null && response!=''){
				document.forms['pricingPayableForm'].elements['writeOffReasonVal'].value=response;
			}
		},
		onFailure: function(){ 
		}
	});
	
}

<c:if test="${((accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE') || (!trackingStatus.accNetworkGroup  && trackingStatus.soNetworkGroup && accountLine.createdBy == 'Networking'))}">
var elementsLen=document.forms['pricingPayableForm'].elements.length;
for(i=0;i<=elementsLen-1;i++){
		if(document.forms['pricingPayableForm'].elements[i].type=='text') {
				document.forms['pricingPayableForm'].elements[i].readOnly =true;
				document.forms['pricingPayableForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['pricingPayableForm'].elements[i].disabled=true;
			}
		document.forms['pricingPayableForm'].elements['pricingSave'].className ='pricelistbtn-dis';
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
<c:if test='${accountLine.status == false || serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD" || serviceOrder.status == "CLSD"}'>
var elementsLen=document.forms['pricingPayableForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['pricingPayableForm'].elements[i].type=='text') {
				document.forms['pricingPayableForm'].elements[i].readOnly =true;
				document.forms['pricingPayableForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['pricingPayableForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>

payQstHideBlock();	
disableBYPayAcc();
chechCreditInvoice();
recQstHideBlock()
disableBYRecAcc();
checkMultiAuthorization();
</script>
<script type="text/javascript">
		var openDiv = '${clickType}';
		if(openDiv.trim()=='Pay'){
			animatedcollapse.addDiv('payable', 'fade=0,persist=0,show=1')
			animatedcollapse.addDiv('receivable', 'fade=0,persist=0,hide=1') 
			animatedcollapse.init()
		}else if(openDiv.trim()=='Rec'){
			animatedcollapse.addDiv('payable', 'fade=0,persist=0,hide=1')
			animatedcollapse.addDiv('receivable', 'fade=0,persist=0,show=1')
			animatedcollapse.init()
		}
	</script>
