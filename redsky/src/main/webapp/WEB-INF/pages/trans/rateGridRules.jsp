<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
<script language="JavaScript">
function checkRules(){
alert('Your tariff has been successfully submitted.'); 
window.opener.submitRateGridRules();
self.close(); 
return true;
}
function message(){ 
	alert('Your tariff has not been submitted.');
	window.close(); 
}
</script>
</head> 
<s:form id="dataExtractSupport" name="dataExtractSupport" method="post"  action="extractSupport.html?&decorator=popup&popup=true" validate="true">
<table border="0" width="100%">
<tr>
<td align="center">
<B><FONT SIZE="3" COLOR="#5C5C5C" style="text-align:justify;font-family:Arial, Helvetica, sans-serif;font-size:16px;" >
Rate Matrix Usage Agreement
</FONT></B>
</td>
</tr>
</table>
<div id="layer1" style="width: 100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style=""><span></span></div>
   <div class="center-content">
<table border="0" cellpadding="2" cellspacing="0" style=" " align="center">
<tr> 
<td align="left" style="" class="" colspan="2">${rateGridRules}
</td>
</tr>
<tr height="3px"></tr>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<table><tr>
<td>
<B><FONT SIZE="3" COLOR="#BB0000" style="text-align:justify;font-family:Arial, Helvetica, sans-serif;font-size:16px;" >
Please indicate your agreement with the above terms:
</FONT></B>
</td>
<td>
<input type="button" class="cssbutton" style="width:92px; height:25px" value="Accept Terms" onClick="return  checkRules();">
</td>
<td>
<input type="button" class="cssbutton" style="width:95px; height:25px" value="Decline Terms" onclick="message();"/> 
</td>
</tr></table>
</div> 
</s:form> 
</html>
