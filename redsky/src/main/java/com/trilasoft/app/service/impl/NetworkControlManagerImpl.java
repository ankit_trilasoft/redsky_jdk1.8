package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.IntegrationLogInfoDao;
import com.trilasoft.app.dao.NetworkControlDao;
import com.trilasoft.app.model.NetworkControl;
import com.trilasoft.app.service.NetworkControlManager;

public class NetworkControlManagerImpl extends GenericManagerImpl<NetworkControl, Long> implements NetworkControlManager {
	NetworkControlDao  networkControlDao;
	public NetworkControlManagerImpl(NetworkControlDao networkControlDao){
		super(networkControlDao);   
        this.networkControlDao = networkControlDao;
	}
}
