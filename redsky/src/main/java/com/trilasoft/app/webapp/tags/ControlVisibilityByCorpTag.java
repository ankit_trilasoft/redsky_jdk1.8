package com.trilasoft.app.webapp.tags;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.util.ExpressionEvaluationUtils;

import com.trilasoft.app.webapp.action.ComponentConfigByCorpUtil;

public class ControlVisibilityByCorpTag extends TagSupport { 

	// ~ Instance fields
	// ================================================================================================

	private String componentId = "";

	private ComponentConfigByCorpUtil componentConfigByCorpUtil = null;

	private String userCorpID;
	
	public ControlVisibilityByCorpTag(){
	}

	// ~ Methods
	// ========================================================================================================

	public int doStartTag() throws JspException {
		
		
		/////   

		if ((null == componentId) || "".equals(componentId)) {
			return Tag.SKIP_BODY;
		}
		
		if((null != componentId) &&  componentId.indexOf(",")>0 ) {
			return Tag.SKIP_BODY;
		}
		
		ServletContext context = pageContext.getServletContext();
		XmlWebApplicationContext ctx = (XmlWebApplicationContext) context
				.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

		componentConfigByCorpUtil = (ComponentConfigByCorpUtil) ctx.getBean("componentConfigByCorpUtil");		
		boolean visible = getComponentVisibilityAttrbute(componentId);
		if ( !visible) return  Tag.SKIP_BODY;
		
		return Tag.EVAL_BODY_INCLUDE;
	}

	private boolean getComponentVisibilityAttrbute(String componentId) {
		Authentication currentUser = SecurityContextHolder.getContext()
				.getAuthentication();

		if (null == currentUser) {
			return false;
		}

		if ((null == currentUser.getAuthorities())
				|| (currentUser.getAuthorities().length < 1)) {
			return false;
		}

		User user = (User) currentUser.getPrincipal();
		String userCorpID = user.getCorpID();
		
		boolean visible = false;
		
		
		if (componentId.startsWith("component.")){
			visible = componentConfigByCorpUtil.
			getComponentVisibilityAttrbute(userCorpID, componentId);
		} else {
			visible = componentConfigByCorpUtil
				.getFieldVisibilityAttrbute(userCorpID, componentId);
		}

		return visible;
	}


	public void setComponentConfigByCorpUtil(
			ComponentConfigByCorpUtil componentConfigByCorpUtil) {
		this.componentConfigByCorpUtil = componentConfigByCorpUtil;
	}

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
}
