package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.RefSurveyEmailBody;

public interface RefSurveyEmailBodyManager extends GenericManager<RefSurveyEmailBody, Long>{

}
