<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title>Standard Address Detail</title>   
    <meta name="heading" content="Standard Address Detail"/>  
<style>
.ajax_tooltip_content {background-color:#fff !important;}
</style>
</head>   

<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr>
	<!-- <td align="left"  style="padding-left:5px;min-width:120px;">
		<b>Standard Address Detail</b>
	</td> -->
	<td align="right"  style="padding-right:5px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table> 
	<table style="margin:5px; padding:0px;width:96%;" cellpadding="3" cellspacing="0" >
					<tbody>
						<tr>											
							<td class="listwhitetext"><b>Job</b></td>
							<td class="listwhitetext"><b>Name</b></td>
							<td class="listwhitetext"><b>Address2</b></td>
							<td class="listwhitetext"><b>Address3</b></td>
							</tr>
							<tr>											
							<td class="listwhitetext">${standardAddresses.job}</td>
							<td class="listwhitetext">${standardAddresses.addressLine1}</td>
							<td class="listwhitetext">${standardAddresses.addressLine2}</td>
							<td class="listwhitetext">${standardAddresses.addressLine3}</td>
							</tr>
							<tr><td style="height:2px;"></td></tr>
							<tr>
							<td class="listwhitetext"><b>City</b></td>
							<td class="listwhitetext"><b>State</b></td>
							<td class="listwhitetext"><b>Country</b></td>
							<td class="listwhitetext"><b>Zip</b></td>
						</tr>
						<tr>
							<td class="listwhitetext">${standardAddresses.city}</td>
							<td class="listwhitetext">${standardAddresses.state}</td>
							<td class="listwhitetext">${standardAddresses.countryCode}</td>
							<td class="listwhitetext">${standardAddresses.zip}</td>
						</tr>
						<tr><td style="height:2px;"></td></tr>
							<tr>
							<td class="listwhitetext"><b>D-Phone</b></td>
							<td class="listwhitetext"><b>H-Phone</b></td>
							<td class="listwhitetext"><b>F-Number</b></td>
							<td class="listwhitetext"><b>M-Phone</b></td>	
							</tr>
							<tr>
							<td class="listwhitetext">${standardAddresses.dayPhone}</td>
							<td class="listwhitetext">${standardAddresses.homePhone}</td>
							<td class="listwhitetext">${standardAddresses.faxPhone}</td>
							<td class="listwhitetext">${standardAddresses.mobPhone}</td>
						</tr>
						<tr><td style="height:2px;"></td></tr>
							<tr>
							<td class="listwhitetext"><b>Email</b></td>
							</tr>
							<tr>
							<td class="listwhitetext">${standardAddresses.email}</td>
						</tr>
						<tr><td style="height:2px;"></td></tr>
							<tr>
							<td class="listwhitetext"><b>C-Person</b></td>
							<td class="listwhitetext"><b>C-Phone</b></td>
							<td class="listwhitetext"><b>C-Email</b></td>
							</tr>
							<tr>
							<td class="listwhitetext">${standardAddresses.contactPerson}</td>
							<td class="listwhitetext">${standardAddresses.contactPersonPhone}</td>
							<td class="listwhitetext">${standardAddresses.contactPersonEmail}</td>
						</tr>
						<tr><td style="height:2px;"></td></tr>
							<tr>
							<td class="listwhitetext"><b>Type</b></td>
							<td colspan="2" class="listwhitetext"><b>Management-Name</b></td>
							<td class="listwhitetext"><b>Payment</b></td>
							</tr>
							<tr>
							<td class="listwhitetext">${standardAddresses.residenceType}</td>
							<td colspan="2" class="listwhitetext">${standardAddresses.residenceMgmtName}</td>
							<td class="listwhitetext">${standardAddresses.residencePayType}</td>
						</tr>
						<tr><td style="height:2px;"></td></tr>
							<tr>
							<td class="listwhitetext"><b>Deposit1</b></td>
							<td class="listwhitetext"><b>Amount</b></td>
							<td colspan="2" class="listwhitetext"><b>Purpose</b></td>
							</tr>
							<tr>
							<td class="listwhitetext">${standardAddresses.residenceDeposit1}</td>
							<td class="listwhitetext">${standardAddresses.depositAmount1}</td>
							<td colspan="2" class="listwhitetext">${standardAddresses.residencePurpose1}</td>
						</tr>  
						<tr><td style="height:2px;"></td></tr>
							<tr>
							
							<td class="listwhitetext"><b>Deposit2</b></td>
							<td class="listwhitetext"><b>Amount</b></td>
							<td colspan="2" class="listwhitetext"><b>Purpose</b></td>
							</tr>
							<tr>
							<td class="listwhitetext">${standardAddresses.residenceDeposit2}</td>
							<td class="listwhitetext">${standardAddresses.depositAmount2}</td>
							<td colspan="2" class="listwhitetext">${standardAddresses.residencePurpose2}</td>
						</tr>
						<tr><td style="height:2px;"></td></tr>
							<tr>
							<td class="listwhitetext"><b>Notes</b></td>	
							</tr>
							<tr>
							<td colspan="7" class="listwhitetext">${standardAddresses.residenceNotes}</td>
						</tr>
					
					</tbody>	
				</table>

 