package org.appfuse.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.SignUpServiceDao;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.SignUpService;

import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.CustomerFile;
public class SignUpServiceImpl implements SignUpService{
	SignUpServiceDao signUpServiceDao;
	
	public SignUpServiceImpl() {
		
	}
	
	public List customerEmail(String email,String fName,String lName,String target) {
		return signUpServiceDao.customerEmail(email,fName,lName,target);
	}
	/**
	 * @param signUpServiceDao the signUpServiceDao to set
	 */
	public void setSignUpServiceDao(SignUpServiceDao signUpServiceDao) {
		this.signUpServiceDao = signUpServiceDao;
	}
	public void resetPassword(String passwordNew,String confirmPasswordNew,String name){
		signUpServiceDao.resetPassword(passwordNew, confirmPasswordNew, name);
	}
	public List customerHintQues(String username){
		return signUpServiceDao.customerHintQues(username);
	}
	public List customerCorpID(String username){
		return signUpServiceDao.customerCorpID(username);
	}
	public List supportEmail(String corpId){
		return signUpServiceDao.supportEmail(corpId);
	}
	public List customerForgotEmail(String username){
		return signUpServiceDao.customerForgotEmail(username);
	}
	public List maxAttempt(String username){
		return signUpServiceDao.maxAttempt(username);
	}
	public void resetAccount(String name){
		signUpServiceDao.resetAccount(name);
	}

	public Map<String, String> findCountry(String parameter) {
		return signUpServiceDao.findCountry(parameter);
	}

	public String saveUser(User user) {
		return signUpServiceDao.saveUser(user);
	}

	public List getAgentPopupList(String partnerType, String sessionCorpID, String userCountry, String string, String string2, String string3, String string4) {
		return signUpServiceDao.getAgentPopupList(partnerType, sessionCorpID, userCountry, string, string2, string3, string4);
	}

	public List checkUser(String userEmail, String sessionCorpID) {
		return signUpServiceDao.checkUser(userEmail, sessionCorpID);
	}

	public List getJobFunction(String parameter) {
		return signUpServiceDao.getJobFunction(parameter);
	}

	public List getPartner(String partnerCode, String sessionCorpID) {
		return signUpServiceDao.getPartner(partnerCode, sessionCorpID);
	}

	public Role getRole(String rolename) {
		return signUpServiceDao.getRole(rolename);
	}

	public List findCustomerPortalListDetails(String oldPortalId) {
		return signUpServiceDao.findCustomerPortalListDetails(oldPortalId);
	}

	public List findPortalId(String oldPortalId) {
		return signUpServiceDao.findPortalId(oldPortalId);
	}

	public boolean findUser(String portalId) {
		return signUpServiceDao.findUser(portalId);
	}

	public Map<String, String> findState(String country, String sessionCorpID) {
		return signUpServiceDao.findState(country, sessionCorpID);
	}

	public List searchAgents(String partnerCode, String partnerLastName, String partnerTerminalCountryCode, String partnerTerminalCountry, String partnerTerminalState, String sessionCorpID) {
		return signUpServiceDao.searchAgents(partnerCode, partnerLastName, partnerTerminalCountryCode, partnerTerminalCountry, partnerTerminalState,  sessionCorpID);
	}
	public CustomerFile getCustomer(Long id)
	{
		return signUpServiceDao.getCustomer(id);
	}
	public AccessInfo getAccessinfo(Long id)
	{
		return signUpServiceDao.getAccessinfo(id);
	}
	public String serviceStateList(String sessionCorpID){
		
		return signUpServiceDao.serviceStateList(sessionCorpID);
	}
	public List findStateService(String country, String sessionCorpID){
		
		return signUpServiceDao.findStateService(country,sessionCorpID);
		
	}
	public List findUserDetails(String userName){
		return signUpServiceDao.findUserDetails(userName);
	}
	public List updateSoCustomerFeedback(String soId,String rtNumber,String customerFeedback,String services,String surveyJob,String sessionCorpID){
		return signUpServiceDao.updateSoCustomerFeedback(soId,rtNumber,customerFeedback,services,surveyJob,sessionCorpID);
	}
	public List getUserEmail(String rtNumber,String sessionCorpID){
		return signUpServiceDao.getUserEmail(rtNumber,sessionCorpID);
	}
	public List getCrewName(String shipNumber,String sessionCorpID){
		return signUpServiceDao.getCrewName( shipNumber, sessionCorpID);
	}
	public List getUserDetails(String userName,String sessionCorpID){
		return signUpServiceDao.getUserDetails(userName,sessionCorpID);
	}
	public List getSurveyEmail(String sessionCorpID){
		return signUpServiceDao.getSurveyEmail(sessionCorpID);
	}
	public List checkCustomerFeedback(String soId,String sessionCorpID){
		return signUpServiceDao.checkCustomerFeedback(soId,sessionCorpID);
	}
	public List getReloDetailsList(String sessionCorpID){
		return signUpServiceDao.getReloDetailsList(sessionCorpID);
	}
	public List checkRLOCustomerFeedback(String soId,String sessionCorpID){
		return signUpServiceDao.checkRLOCustomerFeedback(soId,sessionCorpID);
	}
	public String getDspDetalsData(String services,String dspDetailsValue,String soId,String sessionCorpID){
		return signUpServiceDao.getDspDetalsData(services,dspDetailsValue,soId,sessionCorpID);
	}
	public void getNPSCalculation(String services,String dspDetailsValue,String soId,String sessionCorpID){
		signUpServiceDao.getNPSCalculation(services,dspDetailsValue,soId,sessionCorpID);
	}
	public String getVenderDspDetails(String services,String soId,String shipNumber,String sessionCorpID){
		return signUpServiceDao.getVenderDspDetails( services, soId, shipNumber ,sessionCorpID);
	}
	public List getPostMoveOrderDetails(String id,String sessionCorpID){
		return signUpServiceDao.getPostMoveOrderDetails(id, sessionCorpID);
	}
	public void updateEmailReveived(String id,String contactPhone,String contactEmail,String sessionCorpID,String response){
		signUpServiceDao.updateEmailReveived(id,contactPhone,contactEmail,sessionCorpID,response);
	}
}
