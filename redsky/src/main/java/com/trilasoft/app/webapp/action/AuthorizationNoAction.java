package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.AuthorizationNo;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AuthorizationNoManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.WorkTicketManager;

import org.appfuse.model.User;
import org.appfuse.service.GenericManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AuthorizationNoAction extends BaseAction {
    private AuthorizationNoManager authorizationNoManager;
    private List authorizationNos;
    private AuthorizationNo authorizationNo;
    private Long id;
	private String sessionCorpID;
	private Billing billing;
	private BillingManager billingManager;
	private String shipNumber;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private String authNo;
	private String billingId;
	private Long billId;
	private Long serviceId;
	private String serviceCom;
	private String serviceJob;
	private WorkTicket workTicket;
	private WorkTicketManager workTicketManager;
	private String accountpopup;
	private RefMasterManager refMasterManager;
	private RefMaster refMaster;
	private String popupval;
	private String autoAuthorizationSequenceNumber;
	private List maxAuthorizationNumber;
	private String authorizationSequenceNumber;
	private  Map<String, String> commodit;
    private  Map<String, String> commodits;
    
    private String popup;
	private String authNoSeq;
	private String authNoInvoice;
	private List authNoInvoiceList;
	
	private String hitFlag;
	private String authNum;
	private String authNumSuf;
	private Long workTicketId;
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(AuthorizationNoAction.class);



	 public AuthorizationNoAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}

    public List getAuthorizationNos() {
        return authorizationNos;
    }
    @SkipValidation 
    public String list() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
    	serviceOrder = serviceOrderManager.get(serviceId); 
    	billing = billingManager.get(billId);
    	shipNumber = serviceOrder.getShipNumber();
    	authorizationNos=authorizationNoManager.searchAccAuthorization(shipNumber); 
    	///// authorizationNos = authorizationNoManager.getAll();
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }
    @SkipValidation 
    public String AuthorizationNosList() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
    	serviceOrder = serviceOrderManager.get(serviceId); 
    	billing = billingManager.get(billId);
    	shipNumber = serviceOrder.getShipNumber();
    	authorizationNos=authorizationNoManager.searchAccAuthorization(shipNumber); 
    	///// authorizationNos = authorizationNoManager.getAll();
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public AuthorizationNo getAuthorizationNo() {
        return authorizationNo;
    }

    public void setAuthorizationNo(AuthorizationNo authorizationNo) {
        this.authorizationNo = authorizationNo;
    }

    public String delete() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	authorizationNoManager.remove(id);
       /* 
        if(popup.equalsIgnoreCase("true")){
        saveMessage(getText("authorizationNo.deleted"));
        }*/
        authorizationNos=authorizationNoManager.searchAccAuthorization(shipNumber); 
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }
    public String deleteTicketAuthorization() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	authorizationNoManager.remove(id);
       /* 
        if(popup.equalsIgnoreCase("true")){
        saveMessage(getText("authorizationNo.deleted"));
        }*/
        authorizationNos=authorizationNoManager.searchAccAuthorization(shipNumber); 
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    
    }
    public String deleteAuthNo() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	authorizationNoManager.remove(id);
          saveMessage(getText("defaultAccountLine.deleted"));
          authorizationNos=authorizationNoManager.searchAccAuthorization(shipNumber);
          logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
          return SUCCESS;
      }

    public String edit() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
        if (id != null) {
        	serviceOrder = serviceOrderManager.get(serviceId);
        	billing = billingManager.get(billId);
            authorizationNo = authorizationNoManager.get(id);
            authorizationNo.setCorpID(sessionCorpID);
            authorizationNo.setShipNumber(billing.getShipNumber());
        } else {
        	serviceOrder = serviceOrderManager.get(serviceId);
         	billing = billingManager.get(billId);
            authorizationNo = new AuthorizationNo();
            authorizationNo.setCorpID(sessionCorpID);
            authorizationNo.setShipNumber(billing.getShipNumber());
            authorizationNo.setUpdatedOn(new Date());
            authorizationNo.setCreatedOn(new Date());
            List authNumber =authorizationNoManager.findAuthNumber(billing.getShipNumber(),sessionCorpID);
            if(!authNumber.isEmpty()){
           	Iterator it=authNumber.iterator();		
    		while(it.hasNext()){
    		Object []row= (Object [])it.next();
    		if(row[0]!= null){
    		try {
    			authNum = (row[0].toString()+ (((row[1].toString()).valueOf(String.valueOf(Long.parseLong(row[1].toString()) + 1))).length()==1 ? ("0"+((row[1].toString()).valueOf(String.valueOf(Long.parseLong(row[1].toString()) + 1)))) : ((row[1].toString()).valueOf(String.valueOf(Long.parseLong(row[1].toString()) + 1)))));
    			}  catch (NumberFormatException nfe) {
    			nfe.printStackTrace();
    			authNum = (row[0].toString()+ "01");
    			}
    		 	authorizationNo.setAuthorizationNumber(authNum);
    			}
    		} 
            } else {
    		authorizationNo.setAuthorizationNumber(authNo);	
    		}
            authorizationNo.setShipNumber(billingId);
            authorizationNo.setCommodity(serviceCom);
        	}
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return SUCCESS;
    }
    @SkipValidation 
    public String editAuthorization() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
        if (id != null) {
        	workTicket=workTicketManager.get(workTicketId);
        	serviceOrder = serviceOrderManager.get(serviceId);
        	billing = billingManager.get(billId);
            authorizationNo = authorizationNoManager.get(id);
            authorizationNo.setCorpID(sessionCorpID);
            authorizationNo.setShipNumber(billing.getShipNumber());
        } else {
        	workTicket=workTicketManager.get(workTicketId);
        	serviceOrder = serviceOrderManager.get(serviceId);
         	billing = billingManager.get(billId);
            authorizationNo = new AuthorizationNo();
            authorizationNo.setCorpID(sessionCorpID);
            authorizationNo.setShipNumber(billing.getShipNumber());
            authorizationNo.setUpdatedOn(new Date());
            authorizationNo.setCreatedOn(new Date());
            authorizationNo.setWorkTicketNumber(workTicket.getTicket());
            List authNumber =authorizationNoManager.findAuthNumber(billing.getShipNumber(),sessionCorpID);
            if(!authNumber.isEmpty()){
           	Iterator it=authNumber.iterator();		
    		while(it.hasNext()){
    		Object []row= (Object [])it.next();
    		if(row[0]!= null){
    		try {
    			authNum = (row[0].toString()+ (((row[1].toString()).valueOf(String.valueOf(Long.parseLong(row[1].toString()) + 1))).length()==1 ? ("0"+((row[1].toString()).valueOf(String.valueOf(Long.parseLong(row[1].toString()) + 1)))) : ((row[1].toString()).valueOf(String.valueOf(Long.parseLong(row[1].toString()) + 1)))));
    			}  catch (NumberFormatException nfe) {
    			nfe.printStackTrace();
    			authNum = (row[0].toString()+ "01");
    			}
    		 	authorizationNo.setAuthorizationNumber(authNum);
    			}
    		} 
            } else {
    		authorizationNo.setAuthorizationNumber(authNo);	
    		}
            authorizationNo.setShipNumber(billingId);
            authorizationNo.setCommodity(serviceCom);
        	}
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return SUCCESS;
    }
 	public String searchAuthorization()
    {
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	authorizationNos=authorizationNoManager.searchAuthorizationNo(shipNumber); 
    	accountpopup="true";
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;	
    }
 	 
 	public String findAuthorizationNo ()
 	{
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
 		authNoInvoiceList=authorizationNoManager.findAuthorizationNo(authNoInvoice);
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
 		return SUCCESS;
 	}
 	 
    public String saveWorkTicketAuthorization() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
         boolean isNew = (authorizationNo.getId() == null);
         if(isNew){
        	 authorizationNo.setCreatedOn(new Date());
        	 maxAuthorizationNumber = authorizationNoManager.findMaximum(billingId);
        	 if(maxAuthorizationNumber.get(0) == null ) {
        		 authorizationNo.setAuthorizationSequenceNumber("01");
     		} else {
     			autoAuthorizationSequenceNumber = String.valueOf(Long.parseLong(maxAuthorizationNumber.get(0).toString()) + 1);
     			     			if((autoAuthorizationSequenceNumber.toString()).length()==1){
     			authorizationNo.setAuthorizationSequenceNumber("0"+autoAuthorizationSequenceNumber.toString());
     			     			}
     			else{
     				authorizationNo.setAuthorizationSequenceNumber(autoAuthorizationSequenceNumber.toString());
     				
     			}
     		}
     		}
         authorizationNo.setUpdatedOn(new Date());
         authorizationNo.setUpdatedBy(getRequest().getRemoteUser());
         authorizationNo.setCorpID(sessionCorpID);
         authorizationNo = authorizationNoManager.save(authorizationNo);
         AuthorizationNosList();
         
       if(popup.equalsIgnoreCase("true")){
       if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        	String key = (isNew) ? "Authorization No has been added successfully" : "Authorization No has been updated successfully";
        	saveMessage(getText(key));
        }
     		}
       
        if (!isNew) {
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return INPUT;
        } else {
        	hitFlag ="1";
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;
        }
         }
    public String save() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	getComboList(sessionCorpID);
         boolean isNew = (authorizationNo.getId() == null);
         if(isNew){
        	 authorizationNo.setCreatedOn(new Date());
        	 maxAuthorizationNumber = authorizationNoManager.findMaximum(billingId);
        	 if(maxAuthorizationNumber.get(0) == null ) {
        		//// authorizationNo.setAuthorizationNumber(authNo +"-"+"01");
        		 authorizationNo.setAuthorizationSequenceNumber("01");
     		} else {
     			/*if((maxAuthorizationNumber.get(0).toString()).length()==1)
     			{
     				autoAuthorizationSequenceNumber = Long.parseLong(maxAuthorizationNumber.get(0).toString()) + 01;
     			}else
     			{
     				autoAuthorizationSequenceNumber = Long.parseLong(maxAuthorizationNumber.get(0).toString()) + 1;
     			}*/
     			autoAuthorizationSequenceNumber = String.valueOf(Long.parseLong(maxAuthorizationNumber.get(0).toString()) + 1);
     			///authorizationNo.setAuthorizationSequenceNumber(autoAuthorizationSequenceNumber.toString());
     			////System.out.println("\n\n\n\n bbbbbbbbbbbbbbbbbb-->"+autoAuthorizationSequenceNumber);
     			if((autoAuthorizationSequenceNumber.toString()).length()==1){
     			authorizationNo.setAuthorizationSequenceNumber("0"+autoAuthorizationSequenceNumber.toString());
     			/////authNoSeq = authNo+"-"+"0" + autoAuthorizationSequenceNumber.toString();
     			}
     			else{
     				authorizationNo.setAuthorizationSequenceNumber(autoAuthorizationSequenceNumber.toString());
     				////authNoSeq = authNo+"-"+autoAuthorizationSequenceNumber.toString();	
     			}
     			/////System.out.println("\n\n\n\n AAAAAAAAAAAAAAAAAAAAA-->"+authNoSeq);
     			/////authorizationNo.setAuthorizationNumber(authNoSeq);
     		}
     		}
         authorizationNo.setUpdatedOn(new Date());
         authorizationNo.setUpdatedBy(getRequest().getRemoteUser());
         authorizationNo.setCorpID(sessionCorpID);
         ///authorizationNo.setShipNumber(billingId);
        //authorizationNo.setCommodity(serviceCom);
         authorizationNo = authorizationNoManager.save(authorizationNo);
         list();
         
       if(popup.equalsIgnoreCase("true")){
       if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
        	String key = (isNew) ? "Authorization No has been added successfully" : "Authorization No has been updated successfully";
        	saveMessage(getText(key));
        }
     		}
       /*String key = (isNew) ? "authorizationNo.added" : "authorizationNo.updated";
       saveMessage(getText(key));*/
       
        if (!isNew) {
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return INPUT;
        } else {
        	hitFlag ="1";
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;
        }
         }
    private String gotoPageString;

    public String getGotoPageString() {

    return gotoPageString;

    }

    public void setGotoPageString(String gotoPageString) {

    this.gotoPageString = gotoPageString;

    }

    public String saveOnTabChange() throws Exception {
        String s = save(); 
        return gotoPageString;
    }
    public String getComboList(String corpId){
    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	//commodit = refMasterManager.findByParameter(corpId, "COMMODIT");
    //commodits = refMasterManager.findByParameter(corpId, "COMMODITS");
    commodits = refMasterManager.findByParameter(corpId, "AUTH_COMMODITY");
    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
    }
    
    public  Map<String, String> getCommodit() {
		return commodit;
	}
    public  Map<String, String> getCommodits() {
		return commodits;
	}

	public void setAuthorizationNoManager(
			AuthorizationNoManager authorizationNoManager) {
		this.authorizationNoManager = authorizationNoManager;
	}

	public String getAuthNo() {
		return authNo;
	}

	public void setAuthNo(String authNo) {
		this.authNo = authNo;
	}

	
	public Long getServiceId() {
		return serviceId;
	}
	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public String getAccountpopup() {
		return accountpopup;
	}

	public void setAccountpopup(String accountpopup) {
		this.accountpopup = accountpopup;
	} 

	public void setServiceId(Long serviceId) {
		this.serviceId = serviceId;
	}
	public Billing getBilling() {
		return billing;
	}

	public Long getId() {
		return id;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setAuthorizationNos(List authorizationNos) {
		this.authorizationNos = authorizationNos;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public String getBillingId() {
		return billingId;
	}

	public void setBillingId(String billingId) {
		this.billingId = billingId;
	}

	public String getServiceCom() {
		return serviceCom;
	}

	public void setServiceCom(String serviceCom) {
		this.serviceCom = serviceCom;
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public RefMaster getRefMaster() {
		return refMaster;
	}

	public void setRefMaster(RefMaster refMaster) {
		this.refMaster = refMaster;
	}

	public String getServiceJob() {
		return serviceJob;
	}

	public void setServiceJob(String serviceJob) {
		this.serviceJob = serviceJob;
	}

	public String getPopupval() {
		return popupval;
	}

	public void setPopupval(String popupval) {
		this.popupval = popupval;
	}

	public String getPopup() {
		return popup;
	}

	public void setPopup(String popup) {
		this.popup = popup;
	}

	public List getMaxAuthorizationNumber() {
		return maxAuthorizationNumber;
	}

	public void setMaxAuthorizationNumber(List maxAuthorizationNumber) {
		this.maxAuthorizationNumber = maxAuthorizationNumber;
	}

	public String getAuthNoInvoice() {
		return authNoInvoice;
	}

	public void setAuthNoInvoice(String authNoInvoice) {
		this.authNoInvoice = authNoInvoice;
	}

	public List getAuthNoInvoiceList() {
		return authNoInvoiceList;
	}

	public void setAuthNoInvoiceList(List authNoInvoiceList) {
		this.authNoInvoiceList = authNoInvoiceList;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getAuthNum() {
		return authNum;
	}

	public void setAuthNum(String authNum) {
		this.authNum = authNum;
	}

	public String getAuthNumSuf() {
		return authNumSuf;
	}

	public void setAuthNumSuf(String authNumSuf) {
		this.authNumSuf = authNumSuf;
	}

	public Long getWorkTicketId() {
		return workTicketId;
	}

	public void setWorkTicketId(Long workTicketId) {
		this.workTicketId = workTicketId;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

}