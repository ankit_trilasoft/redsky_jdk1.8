<%@ include file="/common/taglibs.jsp"%> 
 <%@ taglib prefix="s" uri="/struts-tags" %> 
 <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head> 
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:5px;margin-top:-18px;padding:1px 0;text-align:right;width:100%;}
.input-textGreen {background: none repeat scroll 0 0 #90ee90; border: 1px solid rgb(33, 157, 209); color: rgb(0, 0, 0); font-family: arial,verdana; font-size: 12px;
    height: 15px; }
.input-textGreen:hover {background: none repeat scroll 0 0 #90ee90 !important;}
.input-textRed {background: none repeat scroll 0 0 #FF4c4c; border: 1px solid rgb(33, 157, 209); color: rgb(0, 0, 0); font-family: arial,verdana; font-size: 12px;
    height: 15px; }
.input-textRed:hover {background: none repeat scroll 0 0 #FF4c4c !important;}
.input-textNone {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0); border: 1px solid rgb(33, 157, 209);color: rgb(0, 0, 0); font-family: arial,verdana; font-size: 12px;
    height: 15px; }    
.table th.sorted, .table th.order2 {color:#15428B !important;}
form {margin-top:-20px;}
 
</style>
<script language="javascript" type="text/javascript">
function findToolTipDescriptionForProposal(id,position){	
	
		var url="findToolTipProposal.html?ajax=1&decorator=simple&popup=true&id="+id;
		ajax_showTooltip(url,position);	
		return false;
		}
</script>   
<script language="javascript" type="text/javascript">
function autoSaveProposalManagementAjax(fieldName,fieldValTemp,fieldId){
		var fieldVal=document.getElementById(fieldValTemp).value;
		var url="autoSaveProposalManagementAjax.html?ajax=1&decorator=simple&popup=true&fieldName="+encodeURIComponent(fieldName)+"&fieldVal="+encodeURIComponent(fieldVal)+"&fieldId="+encodeURIComponent(fieldId);		    
	 	httpMergeSO24.open("GET", url, true);
	 	httpMergeSO24.onreadystatechange = function(){ handleHttpResponseProposalManagementList(fieldName,fieldVal,fieldId);};
	 	httpMergeSO24.send(null);
}
function handleHttpResponseProposalManagementList(fieldName,fieldVal,fieldId){
	if (httpMergeSO24.readyState == 4){
	    var results=httpMergeSO24.responseText; 
	    <c:if test="${proposalManagements!='[]'}">
	    totalById('billTillDate');
	    totalById('amount');
	    </c:if>
	}
}
var httpMergeSO24 = getHTTPObjectSO23();
function getHTTPObjectSO23()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
</script>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript"
	SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript"
	SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/styles/redsky/jscal2.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/styles/redsky/border-radius.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/styles/redsky/steel.css'/>" />
<title><fmt:message key="proposalManagementList.title" />
</title>
<meta name="heading"
	content="<fmt:message key='proposalManagementList.heading'/>" />
    <title><fmt:message key="proposalManagementList.title"/></title>   
    <meta name="heading" content="<fmt:message key='proposalManagementList.heading'/>"/>     
 </head> 
 
 <s:form name="proposamList" action='searchProposalManagement' method="post" >
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/> 
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="corpID" value="<%= request.getParameter("corpID") %>"/>
<c:set var="corpID" value="<%=request.getParameter("corpID") %>"/>

<c:set var="buttons"> 
    <input type="button" class="cssbuttonA" align="left" 
        onclick="location.href='<c:url value="/editProposalManagement.html','height=400,width=850,top=20, left=100, scrollbars=yes,resizable=yes"/>'" 
        value="<fmt:message key="button.add"/>"/> 
        
</c:set> 
	<div id="Layer1" style="width:100%"> 
	<div id="otabs">
	  <ul>
	    <li><a class="current"><span>Search</span></a></li>
	  </ul>
	</div>
	<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
				<table class="table" width="99%" >
				<thead>
				<tr>
				<th>Module</th>
				<th>Corp&nbsp;Id</th>
				<th>Status</th>
				<th>Start&nbsp;Date</th>
				<th>End&nbsp;Date</th>
				<th>Ticket#</th>
				<!-- <th>Active</th> -->
				<th></th>
				</thead>
				</tr>
				<tr>
				<td>
				<s:select cssClass="list-menu"   name="proposalManagement.module" list="%{moduleReport}" headerKey="" headerValue="" cssStyle="width:150px"/>
				</td>
				<td>
				<s:select cssClass="list-menu"   name="proposalManagement.corpId" list="%{distinctCorpId}" headerKey="" headerValue="" cssStyle="width:150px"/>
				</td>
				<td>
				<s:select cssClass="list-menu"   name="proposalManagement.status" list="%{statusReport}" headerKey="" headerValue="" cssStyle="width:130px"/>
				</td>	
											<c:if test="${not empty proposalManagement.initiationDate}">
				<s:text id="customerFileinitiationDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="proposalManagement.initiationDate" /></s:text>
				<td align="left" style=""><s:textfield cssClass="input-text" id="initiationDate" name="proposalManagement.initiationDate" value="%{customerFileinitiationDateFormattedValue}" 
				required="true" size="10" maxlength="13" onkeydown="return onlyDel(event,this)"  />
				
				<img id="initiationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
				</c:if>

				
				<c:if test="${empty proposalManagement.initiationDate}">
				<td align="left" style="">
				<s:textfield cssClass="input-text" id="initiationDate" name="proposalManagement.initiationDate" required="true" size="10" maxlength="11" 
				onkeydown="return onlyDel(event,this)"  />
			
			<img id="initiationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /></td>
				</c:if>
				
				<c:if test="${not empty proposalManagement.approvalDate}">
				<s:text id="customerFileapprovalDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="proposalManagement.approvalDate" /></s:text>
				<td align="left" style=""><s:textfield cssClass="input-text" id="approvalDate" name="proposalManagement.approvalDate" value="%{customerFileapprovalDateFormattedValue}" size="10" maxlength="11" readonly="true" onkeydown="onlyDel(event,this)" onblur="" onselect="searchWorkTicketsDays();"/>
				<img id="approvalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/></td>
				</c:if>
				<c:if test="${empty proposalManagement.approvalDate}">
				<td align="left" style=""><s:textfield cssClass="input-text" id="approvalDate" name="proposalManagement.approvalDate" required="true" size="10" maxlength="11" readonly="true" onblur="" onkeydown="return onlyDel(event,this)"/>
				<img id="approvalDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/></td>
				</c:if>
				<td><s:textfield name="proposalManagement.ticketNo" cssStyle="width:100px;" cssClass="input-text" /></td>
				<%-- <td><s:checkbox key="activeStatus"  cssStyle="vertical-align:middle; margin:5px 3px 3px 0px;"   /></td> --%>
				<td width="200px">
			    <s:submit cssClass="cssbutton" cssStyle="width:57px; height:25px;" method="searchProposalManagement" key="button.search"  />
		       <input type="button" class="cssbutton" value="Clear" style="width:57px; height:25px;" onclick="clear_fields();"/>   
			</td> 			
			</tr>		
		</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>
 <s:form id="proposamList1" name="proposamList1" action='' method="post" >
<div id="newmnav">
		  <ul>
		     <li><a><span>Proposal List</span></a></li>
		  
		  </ul>
		</div><div class="spn">&nbsp;</div>

<s:set name="proposalManagements" value="proposalManagements" scope="request"/> 
<display:table name="proposalManagements" class="table" requestURI="" id="proposalManagementList" export="false" defaultsort="12"  defaultorder="descending" pagesize="25" style="margin-top:0px;!margin-top:8px; ">
 	<c:if test="${proposalManagements=='[]'}">
 	<display:column property="corpId" sortable="true" title="Corp&nbsp;Id" sortProperty="corpId" url="/editProposalManagement.html" paramId="id" paramProperty="id" style="width:50px;"/>
 	<display:column property="module" title="Module" style="width:70px;"/>
 	<display:column property="subject"  title="Subject" sortable="true" sortProperty="subject"  style="width:200px;" maxLength="100"  />
 	<display:column property="description" sortable="true" title="Description" style="width:350px;" />
 	<display:column property="status" title="Status" style="width:100px;"/>
 	<display:column property="billTillDate" title="BillTillDate" style="width:80px; text-align:right;padding-right:0.1em;" />
 	<display:column property="amountProposed" title="Amount" style="width:80px; text-align:right;padding-right:0.1em;" />
 	<display:column property="isFixed" title="Fixed"   style="width:50px;padding:0px;text-align:center;"/>
	<display:column property="clientApprover"  title="Client&nbsp;Approver"  sortable="true" sortProperty="clientApprover"  style="width:60px;" maxLength="40"/>
	<display:column property="projectManager"  title="Project&nbsp;Manager" sortable="true" sortProperty="projectManager"  style="width:80px;"  maxLength="40"/>	
	<display:column property="initiationDate"  style="width:50px;" title="Initiation&nbsp;Date"  sortable="true" sortProperty="initiationDate" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="approvalDate"  style="width:50px;" title="Approval&nbsp;Date"  sortable="true" sortProperty="approvalDate" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="releaseDate"  style="width:50px;" title="Release&nbsp;Date"  sortable="true" sortProperty="releaseDate" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="ticketNo" title="Ticket&nbsp;#" />
	<display:column property="fileName" title="Proposal&nbsp;Note" style="width:150px;" maxLength="30" />
	<display:column property="approvalFileName" title="Approval&nbsp;Note" style="width:150px;" maxLength="30" />
	<display:column title="Remove" style="width:45px;"/>
 	</c:if>
 <c:if test="${proposalManagements!='[]'}">
 	<display:column property="corpId" sortable="true" title="Corp&nbsp;Id" sortProperty="corpId" url="/editProposalManagement.html" paramId="id" paramProperty="id" style="width:50px;"/>
	<display:column  title="Module" style="width:70px;">
	<c:forEach var="entry" items="${moduleReport}">
	<c:if test="${proposalManagementList.module==entry.key}">
	<c:out value="${entry.value}" />
	</c:if>
	</c:forEach>
	</display:column>
	<display:column property="subject"  title="Subject" sortable="true" sortProperty="subject"  style="width:200px;" maxLength="100"  />
	<display:column sortable="true" title="Description" style="width:350px;" >
	    <c:if test="${proposalManagementList.description != ''}" >
		<c:if test="${fn:length(proposalManagementList.description) > 199}" >
		<c:set var="abc" value="${fn:substring(proposalManagementList.description, 0, 199)}" />
	    <div align="left" onmouseover="findToolTipDescriptionForProposal('${proposalManagementList.id}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${abc}" />&nbsp;.....</div>
		</c:if>
		<c:if test="${fn:length(proposalManagementList.description) <= 199}" >
		<c:set var="abc" value="${fn:substring(proposalManagementList.description, 0, 199)}" />
	    <div align="left" ><c:out value="${abc}" /></div>
		</c:if>
		</c:if>
		</display:column>
	<display:column title="Status" style="width:130px;">
	<select name ="status${proposalManagementList.id}" id ="status${proposalManagementList.id}" style="width:130px" onchange="autoSaveProposalManagementAjax('status','status${proposalManagementList.id}','${proposalManagementList.id}');" class="list-menu">
	<c:forEach var="entry" items="${statusReport}">
   	<c:choose>
    <c:when test="${entry.key == proposalManagementList.status}">
    <c:set var="selectedInd" value=" selected"></c:set>
    </c:when>
   	<c:otherwise>
    <c:set var="selectedInd" value=""></c:set>
   	</c:otherwise>
  	</c:choose>
  	<option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
 	<c:out value="${entry.value}"></c:out>
 	</option>									    
	</c:forEach>
	</select>
	</display:column>
	<c:choose>
	<c:when test="${proposalManagementList.billTillDate != '0.00' && proposalManagementList.billTillDate >= proposalManagementList.amountProposed}">
	<display:column title="BillTillDate" style="background:#90EE90;width:80px; text-align:right;padding-right:0.1em;" >
	<input type="text" name="billTillDate" style="text-align:right;width:70px;padding-right:0em !important;" id="billTillDate${proposalManagementList.id}" value="${proposalManagementList.billTillDate}" class="input-textGreen" onkeydown="return onlyFloatNumsAllowed(event)" onchange="autoSaveProposalManagementAjax('billTillDate','billTillDate${proposalManagementList.id}','${proposalManagementList.id}');" />
    </display:column>
    </c:when>
    <c:when test="${proposalManagementList.billTillDate == '0.00'}">
	<display:column title="BillTillDate" style="background:#FF4c4c;width:80px; text-align:right;padding-right:0.1em;" >
	<input type="text" name="billTillDate" style="text-align:right;width:70px;padding-right:0em !important;" id="billTillDate${proposalManagementList.id}" value="${proposalManagementList.billTillDate}" class="input-textRed" onkeydown="return onlyFloatNumsAllowed(event)" onchange="autoSaveProposalManagementAjax('billTillDate','billTillDate${proposalManagementList.id}','${proposalManagementList.id}');" />
    </display:column>
    </c:when>
    <c:otherwise>
    <display:column title="BillTillDate" style="width:80px; text-align:right;padding-right:0.1em;" >
	<input type="text" name="billTillDate" style="text-align:right;width:70px;padding-right:0em !important;" id="billTillDate${proposalManagementList.id}" value="${proposalManagementList.billTillDate}" class="input-textNone" onkeydown="return onlyFloatNumsAllowed(event)" onchange="autoSaveProposalManagementAjax('billTillDate','billTillDate${proposalManagementList.id}','${proposalManagementList.id}');" />
    </display:column>
    </c:otherwise>
	</c:choose>
	<c:choose>
	<c:when test="${proposalManagementList.amountProposed != '0.00' && proposalManagementList.billTillDate >= proposalManagementList.amountProposed}">
	<display:column title="Amount" style="background:#90EE90;width:80px; text-align:right;padding-right:0.1em;" >
	<input type="text" name="amount" style="text-align:right;width:70px;padding-right:0em !important;" id="amountProposed${proposalManagementList.id}" value="${proposalManagementList.amountProposed}" class="input-textGreen" onkeydown="return onlyFloatNumsAllowed(event)" onchange="autoSaveProposalManagementAjax('amountProposed','amountProposed${proposalManagementList.id}','${proposalManagementList.id}');" />
    </display:column>
	</c:when>
	<c:when test="${proposalManagementList.amountProposed == '0.00'}">
	<display:column title="Amount" style="background:#FF4c4c;width:80px; text-align:right;padding-right:0.1em;" >
	<input type="text" name="amount" style="text-align:right;width:70px;padding-right:0em !important;" id="amountProposed${proposalManagementList.id}" value="${proposalManagementList.amountProposed}" class="input-textRed" onkeydown="return onlyFloatNumsAllowed(event)" onchange="autoSaveProposalManagementAjax('amountProposed','amountProposed${proposalManagementList.id}','${proposalManagementList.id}');" />
    </display:column>
	</c:when>
	<c:otherwise>
	<display:column title="Amount" style="width:80px; text-align:right;padding-right:0.1em;" >
	<input type="text" name="amount" style="text-align:right;width:70px;padding-right:0em !important;" id="amountProposed${proposalManagementList.id}" value="${proposalManagementList.amountProposed}" class="input-textNone" onkeydown="return onlyFloatNumsAllowed(event)" onchange="autoSaveProposalManagementAjax('amountProposed','amountProposed${proposalManagementList.id}','${proposalManagementList.id}');" />
    </display:column>
	</c:otherwise>
	</c:choose>
	<display:column title="Fixed"   style="width:50px;padding:0px;text-align:center;">
	<c:if test="${proposalManagementList.isFixed}">
        <input type="checkbox"   checked="checked" disabled="disabled"   />
        </c:if>
    </display:column>
	<display:column property="clientApprover"  title="Client&nbsp;Approver"  sortable="true" sortProperty="clientApprover"  style="width:60px;" maxLength="40"/>
	<display:column property="projectManager"  title="Project&nbsp;Manager" sortable="true" sortProperty="projectManager"  style="width:80px;"  maxLength="40"/>	
	<display:column property="initiationDate"  style="width:50px;" title="Initiation&nbsp;Date"  sortable="true" sortProperty="initiationDate" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="approvalDate"  style="width:50px;" title="Approval&nbsp;Date"  sortable="true" sortProperty="approvalDate" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="releaseDate"  style="width:50px;" title="Release&nbsp;Date"  sortable="true" sortProperty="releaseDate" format="{0,date,dd-MMM-yyyy}"/>
	<display:column title="Ticket&nbsp;#" >
	<c:set var="string4" value="${fn:split(proposalManagementList.ticketNo, ',')}" />
	<c:forEach var="tmp" items="${string4}">
	<c:set var="string44" value="${fn:trim(tmp)}" />
	<c:if test="${string44 != ''}">
	<INPUT  TYPE=button onclick="window.open('http://bugs.skyrelo.com/bugzilla/show_bug.cgi?id=${string44}&ticket=${string44}')"style="background:none;border:none;text-decoration:underline;cursor:pointer;" VALUE="${string44}" />
   </c:if>
 	 </c:forEach>
	</display:column>   
	    
	<display:column title="Proposal&nbsp;Note" style="width:150px;" maxLength="30" >
	<a onclick="javascript:openWindow('ProposalResourceMgmtImageServletAction.html?id=${proposalManagementList.id}&decorator=popup&popup=true',900,600);">
    <c:out value="${proposalManagementList.fileName}" escapeXml="false"/></a>
    </display:column>
    <display:column title="Approval&nbsp;Note" style="width:150px;" maxLength="30" >
	<a onclick="javascript:openWindow('ProposalApprovalMgmtImageServletAction.html?id=${proposalManagementList.id}&decorator=popup&popup=true',900,600);">
    <c:out value="${proposalManagementList.approvalFileName}" escapeXml="false"/></a>
    </display:column>
    <display:column title="Remove" style="width:45px;">
		<a><img align="middle" onclick="confirmSubmit(${proposalManagementList.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>
	</c:if>
   <display:footer>
   		<tr>
    		<td align="right" style="padding:0.3em;" colspan="5">Total:</td>
   			<td align="left" style="width:80px; text-align:right;padding-right:0.1em;padding:0.3em;">
   			<s:textfield name="billTillDateTotal" readonly="true" value="0.00" cssClass="input-textNone" cssStyle="text-align: right; font-weight:bold; width: 70px; padding-right: 0em ! important;"/>
   			</td>
   			<td align="left" style="width:80px; text-align:right;padding-right:0.1em;padding:0.3em;">
   			<s:textfield name="amountTotal" readonly="true" value="0.00" cssClass="input-textNone" cssStyle="text-align: right; font-weight:bold; width: 70px; padding-right: 0em ! important;" />
   			</td>
   			<td align="left" colspan="11" style="padding:0.3em;"></td>
   		</tr>
   </display:footer>
    </display:table>  
    <tr><td align="left" style="margin-left:50px;"><c:out value="${buttons}" escapeXml="false" /></td></tr>
    </s:form>
 <script type="text/javascript"> 
 
 
 function totalById(idName){
	var total=0.00;
	var temp = 0.00;
	if(idName=='billTillDate'){
	    if(document.forms['proposamList1'].billTillDate!=undefined){
	        if(document.forms['proposamList1'].billTillDate.size!=0 && document.forms['proposamList1'].billTillDate.length!=undefined  && document.forms['proposamList1'].billTillDate.length!=0){
	 	      for (var i=0; i<document.forms['proposamList1'].billTillDate.length; i++){
	 	    	  try{
	 	    		  if(document.forms['proposamList1'].billTillDate[i].value==''){
	 	    			 temp = 0.00; 
	 	    		  }else{
	 	    		 	 temp = parseFloat(document.forms['proposamList1'].billTillDate[i].value);
	 	    		  }
	 	    	  }catch(e){
	 	    		 alert(e);
	 	    		 temp = 0.00;
	 	    	  }
	 	    	 total=total+temp;
	 	      }	
	 	   }else{	       
 	    	  try{
	    		  if(document.forms['proposamList1'].billTillDate.value==''){
 	    			 temp = 0.00; 
 	    		  }else{
 	    		 	 temp = parseFloat(document.forms['proposamList1'].billTillDate.value);
 	    		  }
	 	      }catch(e){
	 	    		  alert(e);
	 	    		 temp = 0.00;
	 	      }
	 	    total=total+temp;
	 	  }	        
	 	}			
		document.forms['proposamList1'].elements['billTillDateTotal'].value = total;
	}else if(idName=='amount'){
	    if(document.forms['proposamList1'].amount!=undefined){
	        if(document.forms['proposamList1'].amount.size!=0 && document.forms['proposamList1'].amount.length!=undefined  && document.forms['proposamList1'].amount.length!=0){
	 	      for (var i=0; i<document.forms['proposamList1'].amount.length; i++){	        	           
 	    	  try{
 	    		  if(document.forms['proposamList1'].amount[i].value==''){
	 	    			 temp = 0.00; 
	 	    		  }else{
	 	    		 	 temp = parseFloat(document.forms['proposamList1'].amount[i].value);
	 	    		  }
	 	    	  }catch(e){
	 	    		 temp = 0.00;
	 	    	  }
	 	    	 total=total+temp;
	 	       }	
	 	    }else{	       
	    	  try{
	    		  if(document.forms['proposamList1'].amount.value==''){
	 	    			 temp = 0.00; 
	 	    		  }else{
	 	    		 	 temp = parseFloat(document.forms['proposamList1'].amount.value);
	 	    		  }
	 	    	  }catch(e){
	 	    		 temp = 0.00;
	 	    	  }
	 	    	 total=total+temp;
	 	    }	        
	 	}			
		document.forms['proposamList1'].elements['amountTotal'].value = total;
	}else{
		
	}	
 }
 <c:if test="${proposalManagements!='[]'}">
 totalById('billTillDate');
 totalById('amount');
 </c:if>
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure to remove this row? ");
	var did = targetElement;
	if (agree){
		location.href="deleteProposalDoc.html?id="+did;
		
		}
	else{
		return false;
	}
}

function clear_fields()
{
	  	document.forms['proposamList'].elements['proposalManagement.module'].value = "";
		document.forms['proposamList'].elements['proposalManagement.corpId'].value = "";
		document.forms['proposamList'].elements['proposalManagement.status'].value = "";
		document.forms['proposamList'].elements['proposalManagement.approvalDate'].value = "";
		document.forms['proposamList'].elements['proposalManagement.initiationDate'].value = "";
		document.forms['proposamList'].elements['proposalManagement.ticketNo'].value = "";
		document.forms['proposamList'].elements['activeStatus'].checked = false;
}
</script>

<script>
function autoPopulate_DateTo() {
	var check = "${autoPopulateDate}";
	var mydate=new Date()
var firstDayOfMonth = new Date(mydate.getFullYear(), mydate.getMonth(), 1);
		
var theyear=mydate.getYear()

if (theyear < 1000)
theyear+=1900
var theday=firstDayOfMonth.getDay()
var themonth=["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][firstDayOfMonth.getMonth()]
if (themonth<10)
themonth="0"+themonth
var theday=firstDayOfMonth.getDate()
if (theday<10)
theday="0"+theday

var displayfirst=theday
var displaysecond=themonth
var displaythird=theyear

if(document.proposamList.initiationDate.value == ''  && check!="yes"){
	document.forms['proposamList'].elements['proposalManagement.initiationDate'].value = displayfirst+"-"+displaysecond+"-"+displaythird;
}

}
</script>

<script>


function autoPopulate_Date() {
	var check = "${autoPopulateDate}";
var mydate=new Date()
var lastDayOfMonth = new Date(mydate.getFullYear(), mydate.getMonth()+1, 0);
var theyear=mydate.getYear()

if (theyear < 1000)
theyear+=1900
var theday=lastDayOfMonth.getDay()
var themonth= ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
               "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][lastDayOfMonth.getMonth()]
if (themonth<10)
themonth="0"+themonth
var theday=lastDayOfMonth.getDate()
if (theday<10)
theday="0"+theday
var displayfirst=theday
var displaysecond=themonth
var displaythird=theyear

if(document.proposamList.approvalDate.value=='' && check!="yes" )
	{
document.forms['proposamList'].elements['proposalManagement.approvalDate'].value = displayfirst+"-"+displaysecond+"-"+displaythird;
	}}
	
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode; 
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) ||  (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
}
</script>

<script type="text/javascript">  
try{
	Form.focusFirstElement($("proposamList")); 
	}catch(e){} 
 </script>


<script type="text/javascript">
	setCalendarFunctionality();
</script>

