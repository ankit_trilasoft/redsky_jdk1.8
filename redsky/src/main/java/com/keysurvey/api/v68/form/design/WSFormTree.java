/**
 * WSFormTree.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSFormTree  extends com.keysurvey.api.v68.form.design.WSForm  implements java.io.Serializable {
    private com.keysurvey.api.v68.form.design.WSQuestion[] questions;

    public WSFormTree() {
    }

    public WSFormTree(
           long versionId,
           long accountId,
           java.util.Calendar creationDate,
           java.lang.String description,
           long id,
           java.lang.String name,
           java.lang.String title,
           com.keysurvey.api.v68.form.design.WSQuestion[] questions) {
        super(
            versionId,
            accountId,
            creationDate,
            description,
            id,
            name,
            title);
        this.questions = questions;
    }


    /**
     * Gets the questions value for this WSFormTree.
     * 
     * @return questions
     */
    public com.keysurvey.api.v68.form.design.WSQuestion[] getQuestions() {
        return questions;
    }


    /**
     * Sets the questions value for this WSFormTree.
     * 
     * @param questions
     */
    public void setQuestions(com.keysurvey.api.v68.form.design.WSQuestion[] questions) {
        this.questions = questions;
    }

    public com.keysurvey.api.v68.form.design.WSQuestion getQuestions(int i) {
        return this.questions[i];
    }

    public void setQuestions(int i, com.keysurvey.api.v68.form.design.WSQuestion _value) {
        this.questions[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSFormTree)) return false;
        WSFormTree other = (WSFormTree) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.questions==null && other.getQuestions()==null) || 
             (this.questions!=null &&
              java.util.Arrays.equals(this.questions, other.getQuestions())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getQuestions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getQuestions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getQuestions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSFormTree.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormTree"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("questions");
        elemField.setXmlName(new javax.xml.namespace.QName("", "questions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
