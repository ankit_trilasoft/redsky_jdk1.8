package com.trilasoft.app.service;

import java.util.HashMap;
import java.util.List;

import org.appfuse.service.GenericManager;


import com.trilasoft.app.model.PartnerRateGridDetails;

public interface PartnerRateGridDetailsManager  extends GenericManager<PartnerRateGridDetails, Long> {

	public List  getRateGridDetais(Long id, String sessionCorpID);

	public int updateRateGridData(Long id, String sessionCorpID, HashMap saveDataDetailsMap); 
	public List checkPartnerRateGridId(Long id, String sessionCorpID);

	public int updateRateGridBasis(Long id, String sessionCorpID, HashMap saveBasisMap);

	public int updateRateGridLabel(Long id, String sessionCorpID, HashMap saveLabelMap);

	public List getRateGridDetaisData(long id, String sessionCorpID);

}
