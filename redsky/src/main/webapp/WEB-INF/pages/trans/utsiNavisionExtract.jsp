<%@ include file="/common/taglibs.jsp"%> 
<head>
	<title>Navision Extracts</title>   
    <meta name="heading" content="Navision Extracts"/> 
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">

</script>
	<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 
</head>
<body>
<s:form id="utsiNavisionForm" name="utsiNavisionForm" action="" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:95% " >
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
   <table cellspacing="1" cellpadding="1" border="0" > 
<tr><td height="10px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >Invoice Master Data Extracts</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px" border="0">
<tr>
<td width="20px"></td>  
<td>
<input type="button" class="cssbutton" style="width:150px;" 
        			onclick="location.href='<c:url value="/customerExtractList.html"/>'"  
        			value="Customer Extract"/> 
        			</td>
<td width="10px"></td>

<td> 
	<input type="button" class="cssbutton" style="width:150px;" 
        			onclick="location.href='<c:url value="/vendorExtractList.html"/>'"  
        			value="Vendor Extract"/> 
   </td>
     <td width="10px"></td>
      <td> 
	<input type="button" class="cssbutton" style="width:150px;" 
        			onclick="location.href='<c:url value="/vendorBankExtractList.html"/>'"  
        			value="Vendor Bank Extract"/> 
       </td>
 </tr> 
</table>
</td>

</tr></table>
<table cellspacing="1" cellpadding="1" border="0" > 
<tr><td height="10px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >Navision Extracts</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px" border="0">
<tr>
<td width="20px"></td>  
<td>
<input type="button" class="cssbutton" style="width:150px;" 
        			onclick="location.href='<c:url value="/utsiInvoiceNavisionProcessing.html"/>'"  
        			value="Sales Invoice Extract"/> 
        			</td>

<td width="10px"></td>
<td> 
	<input type="button" class="cssbutton" style="width:150px;" 
        			onclick="location.href='<c:url value="/utsiPayableNavisionProcessing.html"/>'"  
        			value="Purch. Invoice Extract"/> 
        			</td>
        			<td width="10px"></td>
                  <td> 
	                <input type="button" class="cssbutton" style="width:150px;" 
        			onclick="location.href='<c:url value="/utsiSalesPayableNavisionProcessing.html"/>'"  
        			value="Journal Invoice Extract"/> 
        			</td>
   </tr> 
<!--<tr>     			
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:165px; height:25px" 
        			onclick="location.href='<c:url value="/utsiPayableNavisionProcessing.html"/>'"  
        			value="Purchase Invoice Extract"/></td>
</tr>
--></table>
</td>

</tr></table>



<table cellspacing="1" cellpadding="1" border="0" > 
<tr><td height="10px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >Navision Extracts Maintenance</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px" border="0">
<tr>
<td width="20px"></td>  
<td>
<input type="button" class="cssbutton" style="width:150px;" 
        			onclick="location.href='<c:url value="/resetInvoiceUTSINavisionProcessing.html"/>'"  
        			value="Reset Sales Invoice"/> 
        			</td>

<td width="10px"></td>
<td>
<input type="button" class="cssbutton" style="width:150px;" 
        			onclick="location.href='<c:url value="/resetUTSIPayableNavisionProcessing.html"/>'"  
        			value="Reset Purch. Invoice"/> 
        			</td>

</tr> 
<!--<tr>     			
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:165px; height:25px" 
        			onclick="location.href='<c:url value="/resetUTSIPayableNavisionProcessing.html"/>'"  
        			value="Reset Purchase Invoice"/></td>
        			
<td width="10px"></td>
        			
<td><input type="button" class="cssbutton" style="width:220px; height:25px" 
        			onclick="location.href='<c:url value="/regenerateUTSIPayableNavisionProcessing.html"/>'"  
        			value="Regenerate Purchase Invoice Extract"/> 
        			</td>
        			
</tr>
--></table>
</td></tr></table>
</div>
<div class="bottom-header"><span></span></div> 

</div></div></div></s:form></body> 