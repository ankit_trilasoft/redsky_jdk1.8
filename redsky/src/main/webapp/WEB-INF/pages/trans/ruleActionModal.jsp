<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Email Setup Template Details</title>   
    <meta name="heading" content="Email Setup Template Details"/>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/customtooltip.css'/>" />
    <style type="text/css">
    	.setBG{width: 288px;  background: transparent -moz-linear-gradient(top, #feffff 0%, #cce2ea 100%); 
    	height: auto; max-height: 200px; padding-left:3px;line-height: 18px; border: 1px solid rgb(33, 157, 209);display: inline-block;margin-top: 2px;}
    	.table thead th, .tableHeaderTable td{height:18px;}
    	div.MainFile{ padding:3px 10px; background:#fff; border:1px solid #0099cc; position:relative;
  						color:#0099cc; border-radius:2px; text-align:left;  cursor:pointer; width:110px; font-weight:bold; font-family:arial,helvetica,serif; }
  		div.MainFile i{font-size:15px;vertical-align:tex-top;}
		.hide_file { position: absolute; z-index: 1000; opacity: 0; cursor: pointer; right: 0; top: 0; height: 100%; font-size: 24px; width: 100%;}
		.hidden{display:none;}
		.loading-indicator {position: absolute;background-color: #fff; padding:15px; border-radius:5px;left: 45%;top: 40%;}
	</style>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/redskyditor/tinymce.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>


<style type="text/css">
		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		.modal-body {padding: 0 5px; max-height: calc(100vh - 140px); overflow-y: auto; margin-top: 10px }
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:130px;
  height:35px;
  line-height: 15px;
  padding-top:4px;
  visibility: hidden;
  font-size: 11px;
  text-align: center;
  color: #15428B;     
  border: 1px solid #56bcdd;
  border-radius: 5px;         
  box-shadow: rgba(0, 0, 0, 0.1) 1px 1px 2px 0px;
 background: rgb(254,255,255); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkMmViZjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(254,255,255,1) 0%, rgba(210,235,249,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(254,255,255,1)), color-stop(100%,rgba(210,235,249,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffff', endColorstr='#d2ebf9',GradientType=0 ); /* IE6-8 */
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 60%;
  margin-left: -8px;
  width: 0; height: 0;
  border-color: #56bcdd transparent transparent transparent;
  border-width: 10px;
  border-style: solid;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 1;
  bottom: 34px;
  left: 50%;
  margin-left: -66px;
  z-index: 999;
} 

.cshead {border-bottom:1px dashed #b7b5b5;padding-top:5px;padding-bottom:3px;max-width:250px;}
.qhead {padding-top:10px;padding-bottom:3px;}
		

		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		.modal-body {padding: 0 5px; max-height: calc(100vh - 140px); overflow-y: auto; margin-top: 10px }
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 20.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		
	</style>
	 <style>
  span.pagelinks {
 display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-8px;padding:2px 0px;
 text-align:right;width:99%;
}
.btn-group-sm > .btn, .btn-sm {    
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16),0 2px 10px 0 rgba(0,0,0,.12) !important;
}
.btn.focus, .btn:focus, .btn:hover {outline:none !important;}

.alert { margin-bottom: 0px;}

#modalAlert{
    padding: 10px!important;
    overflow-y: inherit!important;
    margin-top: 0px!important;
}
.loading-indicator {position: absolute;background-color: #fff; padding:15px; border-radius:5px;left: 45%;top: 40%;}
   .key_so_dashboard { background: url("images/key_partner_list.jpg") no-repeat scroll 0 0 transparent; cursor: default; height: 22px; margin-left:22%; }
  
    </style>
</head>
<body>

<div class="modal-dialog modal-lg"  id="myModal" style="width:1200px;height:1200px">
 <div class="modal-content pt-3 pr-3 pl-3 pb-3">
 
     
<div class="modal-body" id="myModal">
<div class="modal fade" id="showErrorModal" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									    <div class="modal-body" style="font-weight:600;">
										    <div class="alert alert-danger" id="getErrorMsg">
											</div>
									    </div>
									    <div class="modal-footer">
									     <button type="button" id="spanghclose" class="btn btn-default" data-dismiss="modal" onclick="refersh();" >Close</button>
									     </div>
								</div>
							</div>
						</div>
<div class="modal fade" id="caseModal" aria-hidden="true">
							
</div>

      <s:form id="rule" name="rule" action="/saveruleAction.html" > 
   
        <s:hidden  name="ruleId" value="<%=request.getParameter("ruleId") %>" />
        <s:hidden name="tdid" value="<%=request.getParameter("tdid") %>"/>
        <s:hidden name="rule.status"/>
         <s:hidden name="rule.id"/>       
           <div class="card mb-2 border-b">
                   <div class="card-header">
                       <h6>Action</h6>
                   </div>
                   <div class="card-body">
                      <div class="row">
                        <div class="col-md-12">    
                      

                     <div class="row">
                        <div class="col-md-3">
                        
                              <label class="label-heading eclipse-text">Message For User<span class="validate-icon">*</span>	</label>
                         
                        </div>

                        <div class="col-md-9">
                         <div class="form-group form-md-line-input">
                           <s:textfield name="rule.messagedisplayed"  required="true"  maxlength="100" size="80"  cssClass="input-text form-control" readonly="false"/>
                     </div>
                     </div>
                       
                  </div>

                   

                   <div class="row">
                      <div class="col-md-3">
                          <label class="label-heading eclipse-text">Display  Message To<span class="validate-icon">*</span></label>
                      </div>

                      <div class="col-md-9">
                       <div class="form-group form-md-line-input">
                      <s:select cssClass="list-menu form-control"   name="rule.rolelist"  headerKey="" headerValue="" list="%{prulerole}"  />
                     </div>
                      </div>
                 </div>

                 <div class="row">
                    <div class="col-md-3">
                        <label class="label-heading eclipse-text">Note Type	</label>
                    </div>

                    <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                       <s:select cssClass="list-menu form-control" id="entity" name="rule.noteType" list="%{notetype}" headerKey="" headerValue=""  onchange="getSubType(this);"/>
                   </div>
                    </div>

                    <div class="col-md-2">
                        <label class="label-heading ">Note Sub Type	</label>
                    </div>

                    <div class="col-md-3">
                    <div class="form-group form-md-line-input">
                        <s:select cssClass="list-menu form-control" id="entity"  name="rule.noteSubType"    list="%{notesubtype}" headerKey="" headerValue="" />
                    </div>
                    </div>

                   
               </div>

              


               

            </div>
            <div class="col-md-12">
               
                <div class="row">
                    <div class="col-md-3">
                        <label class="label-heading ">Email Notification</label>
                    </div>

                    <div class="col-md-9">
                     <div class="form-group form-md-line-input">
                       <s:select cssClass="list-menu form-control" id="emailNotddd" name="rule.emailNotification"  list="%{toEmailList}"  onchange="changeStatus();"/>
                   
                    </div>       
                    </div>
               </div>

               <div class="row">
                  <div class="col-md-3">
                        <label class="label-heading">Manual Email		</label>
                   
                  </div>

                  <div class="col-md-9">
                     <div class="form-group form-md-line-input">
                                        <s:textfield name="rule.manualEmail"    required="true" maxlength="50"  cssClass="input-text  form-control" readonly="false"/>	
                     </div>
               </div>
                 
            </div>
            <div class="row">
                  <div class="col-md-3">
                        <label class="label-heading">Display To		</label>
                   
                  </div>

                  <div class="col-md-9">
                     <div class="form-group form-md-line-input">
                                        <s:select cssClass="list-menu form-control" id="entity"  name="rule.displayToUser"  list="%{taskUser}" headerKey="" headerValue="" />
                     </div>
               </div>
                 
            </div>
          </div>



          </div>
            </div>
            
             <div class="float-right mb-3">
         <button type="button" id="spanghclose" class="btn btn-blue float-right mr-3" data-dismiss="modal" onclick="refersh();" >Close</button>
        <s:submit cssClass="add_field_button btn btn-blue float-right mr-3"  type="button" key="button.save"  id="btnSave" onclick="return modalData();" />  
     
	</div>	
          </div>

         
  			 
	  
      </s:form>
   

</div>
</div> 
</div>



<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
      <!-- Popper JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

 <script type="text/javascript">

 $(document).ready(function(){
	for (i = 0; i < window.testvar.length; i++) {
        txt = txt + "\n" + window.testvar.options[i].value;
       // console.log(y.options[i].value);
			    var x = document.getElementById("emailNotddd");
			    //console.log(x)
			var option = document.createElement("option");
			option.text = window.testvar.options[i].value; 
			x.add(option);
      }
	document.forms['rule'].elements['rule.emailNotification'].value='${rule.emailNotification}'; 
}); 

function modalData()
{
var user =document.forms['rule'].elements['rule.displayToUser'].value
var rolelist =document.forms['rule'].elements['rule.rolelist'].value

		if(document.forms['rule'].elements['rule.messagedisplayed'].value =="" ){
			
		alert("Enter Value for the field Message For User");	
			
		return false;
		}
	   if(document.forms['rule'].elements['rule.rolelist'].value =="")
		{
		
		alert("Enter Value for the field Display Message To");
		return false;
		}
	   if(document.forms['rule'].elements['rule.displayToUser'].value !="")
		{
		
		alert("Please note that the '"+user+"'  will be notified and not the '"+rolelist+"'");
		
		}
	   
	}
            
            function refersh()
            {       
            	
            	window.location.reload(true);
 		     			  
            }
          
            
;         
   
</script>
</body>