package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AccessInfo;

public interface AccessInfoDao extends GenericDao<AccessInfo, Long>{
	public  List getAccessInfo(Long cid);
	public  List getAccessInfoForSO(Long cid);
	public  List getIdBySoID(Long cid);
}
