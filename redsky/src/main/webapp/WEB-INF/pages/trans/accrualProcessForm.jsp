<%@ include file="/common/taglibs.jsp"%> 

<head>

	<meta name="heading" content="<fmt:message key='accural.heading'/>"/> 
	<title><fmt:message key="accural.heading"/></title>
	
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>

</head>
<body style="background-color:#444444;">
<s:form id="accrualForm" name="accrualForm" action="accrualProcessing" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:100% " >
<div id="otabs" class="form_magn">
		  <ul>
		    <li><a class="current"><span>Accrual Processing</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">

 <table class="" cellspacing="1" cellpadding="1" border="0"  >
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody> 		  			  	
		  	
		  	<tr>
		  	<td align="left" style="padding-bottom:5px">Enter month end date for Accrual<font color="red" size="2">*</font>				
				<s:textfield cssClass="input-text" id="date" name="date" value="%{date}" size="8" maxlength="11" /><img id="date_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</td>
			<td align="left" style="padding-left:40px;!padding-left:-5px;"><s:reset cssClass="cssbutton" key="Clear" cssStyle="width:70px; height:25px "/></td>	   
		  	</tr>
		  	<tr>
		  	<td align="left" class="listwhitetext" colspan="5">
  				<fieldset style=" width:222px; !padding-top:2px; ">
					<legend>Payable Processing</legend>
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="3" width="100%" style="margin-bottom: 0px;!margin-top:7px;">							
						<tr>					 				
							<td width="18px" style="padding:0px"><input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:220px; font-size: 15;" onclick="checkDateField('1');"  value="Reverse&nbsp;Accrued"/></td>
						</tr>
						<tr><td style="height:5px;"></td></tr>
						<tr>				
							<td width="18px" style="padding:0px"><input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:220px; font-size: 15;" onclick="checkDateField('2');"  value="New&nbsp;Accruals"/></td>
						</tr>
						<tr><td style="height:5px;"></td></tr>
						<tr>				
							<td width="18px" style="padding:0px"><input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:220px; font-size: 15;" onclick="checkDateField('3');"  value="Balance"/></td>
						</tr>
					</table>
				</fieldset>	
				</td>
				</tr>
				<tr>
				<td>
  				<fieldset style=" width:222px; !padding-top:10px; ">
					<legend>Receivable Processing</legend>
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="3" width="100%" style="margin-bottom:0px;!margin-top:7px;">							
						<tr>					 				
							<td width="18px" style="padding:0px"><input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:220px; font-size: 15;" onclick="checkDateField('4');"  value="Reverse&nbsp;Accrued"/></td>
						</tr>
						<tr><td style="height:5px;"></td></tr>
						<tr>				
							<td width="18px" style="padding:0px"><input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:220px; font-size: 15;" onclick="checkDateField('5');"  value="New&nbsp;Accruals"/></td>
						</tr>
						<tr><td style="height:5px;"></td></tr>
						<tr>				
							<td width="18px" style="padding:0px"><input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:220px; font-size: 15;" onclick="checkDateField('6');"  value="Balance"/></td>
						</tr>
					</table>
				</fieldset>
		  	</td>		  			  	
		  	</tr>
		  	
        	</table>
        	<tr>
		  		<td align="left" height="20px"></td>
		  	</tr>
		</tbody>
	</table>
	        	      </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->
<script language="javascript" type="text/javascript">

function checkDateField(temp){  
	if(document.forms['accrualForm'].elements['date'].value!=''){
	var date1 = document.forms['accrualForm'].elements['date'].value;
	var todayDate = new Date(); 
 	var mySplitResult = date1.split("-");
 	var day = mySplitResult[0];
 	var month = mySplitResult[1];
 	var year = mySplitResult[2];
 	var month1 = 0;
 	var month2 = month;
	if(month == 'Jan'){
     month = "01";
     month1 = 1;
 	 } else if(month == 'Feb'){
	     month = "02";
	     month1 = 2;
	 } else if(month == 'Mar'){
	     month = "03"
	     month1 = 3;
	 } else if(month == 'Apr'){
	     month = "04"
	     month1 = 4;
	 } else if(month == 'May'){
	     month = "05"
	     month1 = 5;
	 } else if(month == 'Jun'){
	     month = "06"
	     month1 = 6;
	 } else if(month == 'Jul'){
	     month = "07"
	     month1 = 7;
	 } else if(month == 'Aug'){
	     month = "08"
	     month1 = 8;
	 } else if(month == 'Sep'){
	     month = "09"
	     month1 = 9;
	 } else if(month == 'Oct'){
	     month = "10"
	     month1 = 10;
	 } else if(month == 'Nov'){
	     month = "11"
	     month1 = 11;
	 } else if(month == 'Dec') {
	     month = "12";
	     month1 = 12;
	 }
	 var year1 = '20'+year;	
	 var finalDate = day+"-"+month+"-"+year1;
	 var edate = finalDate.split("-");
	 var entryDate = new Date(edate[1]+"/"+edate[0]+"/"+edate[2]);
	 var day1 = getLastDayOfMonth(month1,year1); 	 
	 var lastDate = day1+"-"+month+"-"+year1; 
	 var mdate = lastDate.split("-");
	 var monthEndDate = new Date(mdate[1]+"/"+mdate[0]+"/"+mdate[2]);
	 if((entryDate.getTime()!= monthEndDate.getTime()) && (entryDate.getTime()!= todayDate.getTime())){
			alert("Date must be less than system Date and must be month end date! "); 	 
			document.forms['accrualForm'].elements['date'].value = "";			 
	 }else if((entryDate.getTime() == monthEndDate.getTime()) && (entryDate.getTime() <= todayDate.getTime())){
		 document.forms['accrualForm'].elements['date'].value = date1;
	        if(temp=='1') { payableReversedAccrued(); }
	        if(temp=='2'){ payableNewAccruals(); }
	        if(temp=='3') { payableBalance(); }
	        if(temp=='4'){ receivableReversedAccrued(); }
	        if(temp=='5') { receivableNewAccruals(); }
	        if(temp=='6'){ receivableBalance(); }
	  }else{
		  	alert("Date must be less than system Date and must be month end date! "); 	 
			document.forms['accrualForm'].elements['date'].value = "";	
	  }	 
  }else{
	alert('Please enter the date!');	
  }	  
}

function getLastDayOfMonth(month,year){
  var day;
  switch(month){
      case 1 :
      case 3 :
      case 5 :
      case 7 :
      case 8 :
      case 10 :
      case 12 :
          day = 31;
          break;
      case 4 :
      case 6 :
      case 9 :
      case 11 :
             day = 30;
          break;
      case 2 :
          if( ( (year % 4 == 0) && ( year % 100 != 0) )
                         || (year % 400 == 0) )
              day = 29;
          else
              day = 28;
          break;
  }
  return day;
}

function payableReversedAccrued(){
		var monthEndDate = document.forms['accrualForm'].elements['date'].value;
		var agree=confirm("Are you sure you want to do reverse accrued for payable?");
		if (agree){
			 location.href="payableReverseAccrued.html?endDate="+monthEndDate;
		}else{
			return false;
		}		
}
function payableNewAccruals(){
		var monthEndDate = document.forms['accrualForm'].elements['date'].value;
		var agree=confirm("Are you sure you want to do new accruals for payable?");
		if (agree){
			 location.href="payableNewAccruals.html?endDate="+monthEndDate;
		}else{
			return false;
		}		
}
function payableBalance(){
	var monthEndDate = document.forms['accrualForm'].elements['date'].value;
	location.href="payableBalance.html?endDate="+monthEndDate;
}
function receivableReversedAccrued(){
	var monthEndDate = document.forms['accrualForm'].elements['date'].value;
	var agree=confirm("Are you sure you want to do reverse accrued for receivable?");
	if (agree){
		 location.href="receivableReverseAccrued.html?endDate="+monthEndDate;
	}else{
		return false;
	}		
}
function receivableNewAccruals(){
	var monthEndDate = document.forms['accrualForm'].elements['date'].value;
	var agree=confirm("Are you sure you want to do new accrued for receivable?");
	if (agree){
		 location.href="receivableNewAccruals.html?endDate="+monthEndDate;
	}else{
		return false;
	}		
}
function receivableBalance(){
	var monthEndDate = document.forms['accrualForm'].elements['date'].value;
	location.href="receivableBalance.html?endDate="+monthEndDate;
}
</script>
<script type="text/javascript">
	setOnSelectBasedMethods(["calcDays(),calculateDeliveryDate()"]);
	setCalendarFunctionality();
</script>	