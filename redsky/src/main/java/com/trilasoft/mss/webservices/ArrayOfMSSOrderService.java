
package com.trilasoft.mss.webservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfMSSOrderService complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfMSSOrderService">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MSSOrderService" type="{http://www.mss1.com/webservices/}MSSOrderService" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfMSSOrderService", propOrder = {
    "mssOrderService"
})
public class ArrayOfMSSOrderService {

    @XmlElement(name = "MSSOrderService")
    protected List<MSSOrderService> mssOrderService;

    /**
     * Gets the value of the mssOrderService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mssOrderService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMSSOrderService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MSSOrderService }
     * 
     * 
     */
    public List<MSSOrderService> getMSSOrderService() {
        if (mssOrderService == null) {
            mssOrderService = new ArrayList<MSSOrderService>();
        }
        return this.mssOrderService;
    }

}
