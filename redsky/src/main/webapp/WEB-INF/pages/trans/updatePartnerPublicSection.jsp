<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Update Account Details</title>   
    <meta name="heading" content="Update Account Details"/> 
<style>
 

 </style>
<script type="text/javascript">
 function updateChildFromParent()
{
	 try{
	 	 var logoPhotographs = document.getElementById("logoPhotographs");	
	 	var bankCode = document.getElementById("bankCode");
	 	 var defaultBillingCurrency = document.getElementById("defaultBillingCurrency");
	 	var partnerPortalDetail = document.getElementById("partnerPortalDetail");
	 	 }catch(e){}
	 	try{
	 		var Type = document.getElementById("Type");
	 	 }catch(e){
	 	 }
	 	 var checkedOption="";
		 try{
	 	 if(bankCode.checked==true) {
	 		checkedOption=checkedOption+"bankCode";
	 	 }
		 }catch(e){}
		 try{
		 	 if(logoPhotographs.checked==true) {
		 		checkedOption=checkedOption+"logoPhotographs";
		 	 }
			 }catch(e){}		 
		 try{
	 	 if(defaultBillingCurrency.checked==true) {
		 		checkedOption=checkedOption+"defaultBillingCurrency";
	 	 }
		 }catch(e){}
		 try{
			  if(Type.checked==true) {
		 	  checkedOption=checkedOption+"Type";
		 	 }
			}catch(e){}
			if(partnerPortalDetail.checked==true) {
				 checkedOption=checkedOption+"partnerPortalDetail";
				 }
	 	 if(checkedOption.trim()!="") {
			  var url="updateChildFromParent.html?ajax=1&decorator=simple&popup=true&checkedOption="+checkedOption+"&id=${partnerPrivate.id}&pageListType=PUB&partnerCode=${partnerPrivate.partnerCode}";
			  http10.open("GET", url, true); 
			  http10.onreadystatechange = handleHttpResponse; 
		      http10.send(null); 
	 	 }else{
	 		 alert("Please Select One Option.");
	 	 }
}
function handleHttpResponse(){
    if (http10.readyState == 4)
    {
    			  var results = http10.responseText
	               results = results.trim();					               
				if(results!="")	{
				alert(results);	
				self.close();			
				} 
    } 
}

var http10 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function closeWindow()
{
	self.close();
}
</script>
 </head>
<s:form name="updateChildForm" action="" method="post" validate="true">
<c:set var="buttons">   
 <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="updateChildFromParent();" value="Update"/>
 <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="closeWindow();" value="Cancel"/>
</c:set> 

<table class="table">
<tr><td class="subcontenttabChild" colspan="2">Update The Following Sections In Child Accounts.</td></tr>
<configByCorp:fieldVisibility componentId="component.field.partner.partnerTypeDropDown">
<tr><td class="listwhitetext" width="10px"><s:checkbox id="Type" name="Type"/></td><td  align="left" class="listwhitetext" >Type</td></tr>
</configByCorp:fieldVisibility>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="partnerPortalDetail" name="partnerPortalDetail"/></td><td  align="left" class="listwhitetext" >Partner Portal Detail</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="logoPhotographs" name="logoPhotographs"/></td><td  align="left" class="listwhitetext" >Logo/Photographs</td></tr>
<tr><td class="listwhitetext" width="10px"><s:checkbox id="bankCode" name="bankCode"/></td><td  align="left" class="listwhitetext" >Bank Code</td></tr>

<configByCorp:fieldVisibility componentId="component.field.Alternative.BillingCurrency">
<tr><td class="listwhitetext" width="10px"><s:checkbox id="defaultBillingCurrency" name="defaultBillingCurrency"/></td><td  align="left" class="listwhitetext" >Default Billing Currency</td></tr>
</configByCorp:fieldVisibility>
<tr><td colspan="2"></td></tr>   
</table>
<c:out value="${buttons}" escapeXml="false" />
</s:form>
 <script type="text/javascript">
try{ 
//alert('${partnerPrivate.excludeFromParentUpdate}');
}
catch(e){}
</script>