package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.MonthlySalesGoal;


public interface MonthlySalesGoalDao extends GenericDao<MonthlySalesGoal, Long>{

	public MonthlySalesGoal getMonthlySalesGoalDetails(String partnerCode,String sessionCorpID,String yearVal);
}
