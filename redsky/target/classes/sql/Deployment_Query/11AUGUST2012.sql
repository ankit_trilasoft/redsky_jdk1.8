ALTER TABLE `redsky`.`billing` ADD COLUMN `billingCurrency` VARCHAR(45) DEFAULT NULL,ADD COLUMN `vendorStoragePerMonth` VARCHAR(45) DEFAULT NULL,ADD COLUMN `vendorStorageVatDescr` VARCHAR(45) DEFAULT NULL,ADD COLUMN `vendorStorageVatPercent` DECIMAL(19,4) DEFAULT 0.0,ADD COLUMN `vendorBillingCurency` VARCHAR(45) DEFAULT NULL;

ALTER TABLE `redsky`.`charges` ADD COLUMN `payablePriceType` VARCHAR(45) DEFAULT NULL,ADD COLUMN `expensePrice` VARCHAR(45) DEFAULT NULL;

ALTER TABLE `redsky`.`charges` MODIFY COLUMN `expensePrice` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `vanlineSettleDate` DATETIME DEFAULT NULL AFTER `activateAccPortal`;

CREATE TABLE  `redsky`.`proposalmanagement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(82) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `modifyBy` varchar(82) DEFAULT NULL,
  `modifyOn` datetime DEFAULT NULL,
  `corpId` varchar(15) DEFAULT NULL,
  `module` varchar(80) DEFAULT NULL,
  `clientRequestor` varchar(50) DEFAULT NULL,
  `clientApprover` varchar(50) DEFAULT NULL,
  `subject` varchar(125) DEFAULT NULL,
  `amountProposed` decimal(10,0) DEFAULT NULL,
  `projectManager` varchar(25) DEFAULT NULL,
  `isFixed` bit(1) DEFAULT b'1',
  `description` mediumtext,
  `status` varchar(62) DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `location` varchar(450) DEFAULT NULL,
  `ticketNo` varchar(100) DEFAULT NULL,
  `initiationDate` datetime DEFAULT NULL,
  `approvalDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
 
 
 
Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby) values('TSFT', 'New', 'New', 20, 'RSPROPOSALSTATUS','en',now(),'kkumar',now(),'kkumar'),('TSFT', 'Submitted', 'Submitted', 20, 'RSPROPOSALSTATUS','en',now(),'kkumar',now(),'kkumar'),('TSFT', 'Accepted', 'Accepted', 20, 'RSPROPOSALSTATUS','en',now(),'kkumar',now(),'kkumar'),('TSFT', 'Rejected', 'Rejected', 20, 'RSPROPOSALSTATUS','en',now(),'kkumar',now(),'kkumar'),('TSFT', 'Hold', 'Hold', 20, 'RSPROPOSALSTATUS','en',now(),'kkumar',now(),'kkumar'),('TSFT', 'Development', 'Development', 20, 'RSPROPOSALSTATUS','en',now(),'kkumar',now(),'kkumar'),('TSFT', 'Billed', 'Billed', 20, 'RSPROPOSALSTATUS','en',now(),'kkumar',now(),'kkumar'),('TSFT', 'Closed', 'Closed', 20, 'RSPROPOSALSTATUS','en',now(),'kkumar',now(),'kkumar');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','RSPROPOSALSTATUS',20,true,now(),'kkumar',now(),'kkumar',1,1,'');

ALTER TABLE `redsky`.`proposalmanagement` MODIFY COLUMN `amountProposed` DECIMAL(10,2) DEFAULT NULL,ADD COLUMN `currency` VARCHAR(3) AFTER `approvalDate`;

Update accountline set vanlineSettleDate=statusDate where corpid ='SSCW' and status IS TRUE;

ALTER TABLE `redsky`.`reports` ADD COLUMN `emailOut` VARCHAR(3) DEFAULT 'No' AFTER `targetCabinet`,ADD COLUMN `emailBody` VARCHAR(1000) AFTER `emailOut`;

ALTER TABLE `redsky`.`myfile` ADD COLUMN `emailStatus` VARCHAR(1000) AFTER `file`;

update networkdatafields set type='CMM/DMM' where modelName='Billing' and (type='' or type is null);

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type) values('Billing','deductible' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('CustomerFile','originPreferredContactTime' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('CustomerFile','partnerEntitle' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('TrackingStatus','ISFConfirmationNo' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('TrackingStatus','itNumber' ,'Update','amishra',now(),'amishra',now(),false,'','',''),('Miscellaneous','shipmentRegistrationDate' ,'Update','amishra',now(),'amishra',now(),false,'','','');

ALTER TABLE `redsky`.`accountline` ADD COLUMN `onHand` DECIMAL(19,2) DEFAULT '0.00' ;

ALTER TABLE `redsky`.`partnerprivate` MODIFY COLUMN `mc` VARCHAR(15);

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `autoPayablePosting` BIT(1) DEFAULT b'0' AFTER `truckRequired`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `billingCheck` BIT(1) DEFAULT b'0' AFTER `autoPayablePosting`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `readyForInvoicing` BIT(1) DEFAULT b'0' ;

delete from networkdatafields where modelName='Billing' and fieldName ='billComplete';

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type) values('Billing','readyForInvoicing' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),('Billing','readyForInvoicing' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `automaticReconcile` bit(1) DEFAULT false ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `vanlineMinimumAmount` DECIMAL(19,4) DEFAULT NULL AFTER `automaticReconcile`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `POS` bit(1) DEFAULT false ;

update networkdatafields set type='CMM' where modelName='CustomerFile' and fieldName='partnerEntitle';

ALTER TABLE `redsky`.`vanline` ADD COLUMN `accCreatedOn` DATETIME DEFAULT NULL AFTER `accountingAgent`;

ALTER TABLE `redsky`.`entitlement` ADD UNIQUE INDEX `Index_2`(`eoption`, `partnerCode`, `corpID`);

 





 

 


 


 

