package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;   

import com.trilasoft.app.model.NewsUpdate;   
  

import java.util.Date;
import java.util.List;   
  
public interface NewsUpdateDao extends GenericDao<NewsUpdate, Long> {   
    public List<NewsUpdate> findByDetail(String details);  
    public List findUpdatedDate(String corpId);
    public List findDistinct();
    public List findListOnCorpId(String corpId,Boolean addValues);
    public List findListByModuleCorp(String module,String corpid,Date publishedDate,String isVisible,Boolean activeStatus);
    public List findByModule(String module,String corpid,Date publishedDate);
	public String findNewsUpdateDescForToolTip(Long id, String corpId);

}  
