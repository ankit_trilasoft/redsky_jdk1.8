<%@ include file="/common/taglibs.jsp"%>
<tr>
<td><div  onClick="javascript:animatedcollapse.toggle('postMoveSurvey')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Post Move Survey
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>

</table>
</div>
<div id="postMoveSurvey">

<table class="table" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" >
<thead>
<tr><td class="listwhitetext" colspan="4">Email Sent: <b><fmt:formatDate value="${serviceOrder.emailSent}" pattern="${displayDateFormat}" /></b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email Received: <b><fmt:formatDate value="${serviceOrder.emailRecvd}" pattern="${displayDateFormat}" /></b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Additional Assistance: <b><c:if test ="${serviceOrder.additionalAssistance==false}">Not required</c:if><c:if test ="${serviceOrder.additionalAssistance==true}">Required</c:if></b></td></tr>
<tr>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>Rating</th>
<th>Comment</th>
</tr>
</thead>
<tr>
<td class="listwhitetext" style="width:15%">Customer&nbsp;Service</td>
<td class="listwhitetextFC" style="width:20%">${coordinator}</td>
<td class="listwhitetextFC" style="width:2%">${couselorRating}</td>
<td class="listwhitetextFC" style="width:70%">${couselorFeedBack}</td>
</tr>
<tr>
<td class="listwhitetext" >Origin&nbsp;Experience</td>
<td class="listwhitetextFC">${originAgent}</td>
<td class="listwhitetextFC">${oaRating}</td>
<td class="listwhitetextFC">${oaFeedBack}</td>
</tr>
<tr>
<td class="listwhitetext" >Destination&nbsp;Experience</td>
<td class="listwhitetextFC">${destinationAgent}</td>
<td class="listwhitetextFC">${daRating}</td>
<td class="listwhitetextFC">${daFeedBack}</td>
</tr>
<tr>
<td class="listwhitetext" >Overall&nbsp;Experience</td>
<td></td>
<td class="listwhitetextFC">${overAllRating}</td>
<td class="listwhitetextFC">${overAllfeedBack}</td>
</tr>
<tr>
<td class="listwhitetext" >Recommendation</td>
<td></td>
<td class="listwhitetextFC">${recommendation}</td>
<td class="listwhitetextFC">${recommendationFeedBack}</td>
</tr>
<tr>
<td class="listwhitetext">Additional&nbsp;Feedback&nbsp;and&nbsp;Recognition</td>
<td></td>
<td></td>
<td class="listwhitetextFC">${additionalFeedBack}<br></br><br>${additionalSecondFeedBack}</td>
</tr>
</table>




</div>
</td> 
</tr>	
