package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.MssDestinationServiceDao;
import com.trilasoft.app.model.MssDestinationService;
import com.trilasoft.app.service.MssDestinationServiceManager;


public class MssDestinationServiceManagerImpl extends GenericManagerImpl<MssDestinationService, Long> implements MssDestinationServiceManager{
	MssDestinationServiceDao mssDestinationServiceDao;
	
		public MssDestinationServiceManagerImpl(MssDestinationServiceDao mssDestinationServiceDao) {
		super(mssDestinationServiceDao);
		this.mssDestinationServiceDao = mssDestinationServiceDao;
	}
}
