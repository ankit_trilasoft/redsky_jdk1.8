<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<head>
    <title>Weekly XML Summary</title>
    <meta name="heading" content="Weekly XML Summary"/>

   
   <style> span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
!margin-bottom:2px;
margin-top:-22px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}</style>
</head>


<s:form id="weeklyXMLForm" action="" method="post" validate="true">
	<div id="layer5" style="width:63%">
	<div id="newmnav">
		  <ul>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Weekly XML Summary <img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="integrationLogs.html"><span>Integration Log List</span></a></li>
		    <li><a href="memoUpload.html"><span>Memo Upload List</span></a></li>
		  </ul>
	</div>
	<div class="spn" style="height:0px;!margin-bottom:1px;">&nbsp;</div>
	</div>
<s:set name="integrationLogs" value="integrationLogs" scope="request"/>
<display:table name="integrationLogs" class="table" requestURI="" id="integrationLogList" export="true" pagesize="50" style="width:600px" defaultsort="2" defaultorder="descending">
	<display:column property="fileName" sortable="true" title="XML"/>
    <display:column property="processedOn" sortable="true" title="DATE" style="width:200px;" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="count" sortable="true" title="Elements Updated" headerClass="containeralign" style="text-align: right;"/>
    <display:column property="xmlCount" sortable="true" headerClass="containeralign" style="text-align: right;" title="No Of XMLs"/>

    <display:setProperty name="paging.banner.item_name" value="integrationLog"/>
    <display:setProperty name="paging.banner.items_name" value="people"/>

    <display:setProperty name="export.excel.filename" value="Weekly XML Summary.xls"/>
    <display:setProperty name="export.csv.filename" value="Weekly XML Summary.csv"/>
    <display:setProperty name="export.pdf.filename" value="Weekly XML Summary.pdf"/>
</display:table>
</s:form>