package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ForwardingMarkUpControl;
import com.trilasoft.app.model.PricingControl;
import com.trilasoft.app.model.PricingControlDetails;

public interface ForwardingMarkUpDao extends GenericDao<ForwardingMarkUpControl, Long>{
	
	 public List checkById(Long id);
  
}
