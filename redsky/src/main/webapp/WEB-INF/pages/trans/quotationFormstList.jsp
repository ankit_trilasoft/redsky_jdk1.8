<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head> 
    <title><fmt:message key="formsList.title"/></title> 
    <meta name="heading" content="<fmt:message key='formsList.heading'/>"/> 
    
<script language="javascript" type="text/javascript">


</script>	
</head>

<s:form id="searchForm" name="searchForm" action="" method="post" > 

<s:hidden name="sid"/>
<s:hidden name="fileType" />
<s:hidden name="jobNumber" />


<table class="detailTabLabel" cellpadding="1" cellspacing="1" border="0" style="width:100%;">
<tr> 
	<td align="right"  style="padding:3px 5px 0px 0px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>

<display:table name="reportss" class="table" id="reportsList"  style="width:450px;margin-bottom:0px;" >
  	<display:column title="Form Description" style="width:250px;">
  		<a href="javascript:openWindow('viewFormParam.html?id=${reportsList.id}&jobType=${jobType}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&preferredLanguage=${preferredLanguage}&decorator=popup&popup=true','forms','height=800,width=700,top=1, left=120, scrollbars=yes,resizable=yes')">
  			<c:out value="${reportsList.reportComment}" />
  	</display:column>
  	<display:column title="PDF">
	    	<c:if test="${reportsList.type == 'D'}">
		    	<c:if test="${(reportsList.paramReq == 'No' && reportsList.pdf == true)}">
		    		<img alt="PDF File" src="<c:url value='/images/pdf1.gif'/>" onclick="validatefields1('${reportsList.id}','PDF');"/>
		    	</c:if>
		    </c:if>
	</display:column>
  	<display:column title="DOC">
			<c:if test="${reportsList.type == 'D'}">
				<c:if test="${(reportsList.paramReq == 'No' && reportsList.rtf == true)}">
					<img alt="RTF File" src="<c:url value='/images/doc.gif'/>" onclick="validatefields1(${reportsList.id},'RTF');"/>
				</c:if>
			</c:if>
	</display:column>	
	<display:column title="DOCX">
			<c:if test="${reportsList.type == 'D'}">
				<c:if test="${(reportsList.paramReq == 'No' && reportsList.docx == true)}">
					<img alt="DOCX File" src="<c:url value='/images/docx.gif'/>" onclick="validatefields1(${reportsList.id},'DOCX');"/>
				</c:if>
			</c:if>
</display:column>
	<display:column title="Upload" >
		<c:if test="${(reportsList.docsxfer == 'Yes')}">
			<img align="middle" onclick="javascript:window.open('viewFormParam.html?id=${reportsList.id}&jobNumber=${jobNumber}&docsxfer=${reportsList.docsxfer}&reportModule=${reportsList.module}&reportSubModule=${reportsList.subModule}&formReportFlag=F&preferredLanguage=${preferredLanguage}&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=120, scrollbars=yes,resizable=yes')" style="text-align:center;padding-left:5px;" src="${pageContext.request.contextPath}/images/upload.png"/>
		</c:if> 
	</display:column>
  <display:column title="Email" >
						<c:url value="viewFormParamEmailSetup.html" var="url1" >
							<c:param name="id" value="${reportsList.id}"/>
							<c:param name="claimNumber" value="${claimNumber}"/>
							<c:param name="cid" value="${custID}"/>
							<c:param name="jobNumber" value="${jobNumber}"/>
							<c:param name="regNumber" value="${regNumber}"/>
							<c:param name="bookNumber" value="${bookNumber}"/>
							<c:param name="noteID" value="${noteID}"/>
							<c:param name="custID" value="${custID}"/>
							<c:param name="docsxfer" value="${reportsList.emailOut}"/>
							<c:param name="reportModule" value="${reportsList.module}"/>
							<c:param name="reportSubModule" value="${reportsList.subModule}"/>
							<c:param name="formReportFlag" value="F"/>
							<c:param name="formNameVal" value="${reportsList.description}"/>							
							<c:param name="decorator" value="popup"/>
							<c:param name="popup" value="true"/>
						</c:url>		 

			<c:if test="${(reportsList.emailOut == 'Yes')}">
				<a><img align="middle" title="Email" onclick="winClose1('${reportsList.id}','${claimNumber}','${custID}','${jobNumber}','${regNumber}','${bookNumber}','${noteID}','${custID}','${reportsList.emailOut}','${reportsList.module}','${reportsList.subModule}','F','${reportsList.description}','${preferredLanguage}');" src="${pageContext.request.contextPath}/images/email_go.png"/></a>
			</c:if> 
		</display:column>
  
</display:table>
 
</s:form>
 
<script type="text/javascript"> 
    highlightTableRows("reportsList"); 
</script> 