<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>
		<title><fmt:message key="accountContactDetail.title"/></title>   
<meta name="heading" content="<fmt:message key='accountContactDetail.heading'/>"/> 
<%-- Shift 1 --%>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>

<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<style><%@ include file="/common/calenderStyle.css"%></style> 
<%-- Shift 2 --%>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>

<s:form id="accountContactForm" action="saveAccountContact" method="post" validate="true"> 
	
	<s:hidden id="countAccountContactNotes" name="countAccountContactNotes" value="<%=request.getParameter("countAccountContactNotes") %>"/>
	<c:set var="countAccountContactNotes" value="<%=request.getParameter("countAccountContactNotes") %>"/>
	
	
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<%-- Shift 3 --%>
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
<c:set var="pID" value="<%= request.getParameter("pID")%>" />
<s:hidden name="pID" value="<%= request.getParameter("pID")%>" />

<s:hidden name="id" value="<%=request.getParameter("pID") %>"/>

    <s:hidden name="partner.id" />
	<s:hidden name="partner.partnerCode" />
	<s:hidden name="partner.corpID" />
    <s:hidden name="accountContact.id" />
    <s:hidden name="accountContact.corpID" />
    <s:hidden name="accountContact.partnerCode" />

    
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>

<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />

<c:if test="${validateFormNav == 'OK'}">
<c:choose>
		
		<c:when test="${gotoPageString == 'gototab.accountlist' }">
			<c:if test="${paramValue == 'View'}">
				<c:redirect url="/partnerView.html"/>
			</c:if>
			<c:if test="${paramValue != 'View'}">
				<c:redirect url="/partnerPublics.html?partnerType=${partnerType}"/>
			</c:if>
		</c:when>
<c:when test="${gotoPageString == 'gototab.additionalInfo' }">
	<c:redirect url="/editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountdetail' }">
	<c:redirect url="/editPartnerPublic.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountprofile' }">
	<c:redirect url="/editNewAccountProfile.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountcontact' }">
	<c:redirect url="/accountContactList.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.contractpolicy' }">
	<c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="layer4" style="width:100%;">
<div id="newmnav">
				<ul>
				  <c:if test="${param.popup}" >
				    <%-- <li><a href='${empty param.popup?"partners.html":"javascript:history.go(-4)"}' ><span>List</span></a></li>  --%>
				    <li><a href="searchPartner.html?partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account List</span></a></li>
			  	    <li><a href="editPartnerAddFormPopup.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Detail</span></a></li>
			  		<li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Profile</span></a></li>
			  		<li id="newmnav1" style="background:#FFF "><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}" class="current"><span>Account Contact<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  		<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Policy</span></a></li>
			  	  </c:if>
			  	  <c:if test="${empty param.popup}" >
				    <li><a onclick="setReturnString('gototab.accountdetail');return checkdate('none'); "><span>Account Detail</span></a></li>
				    <li><a onclick="setReturnString('gototab.accountprofile');return checkdate('none'); "><span>Account Profile</span></a></li>
				    <c:if test="${sessionCorpID!='TSFT' }">
				    <li><a onclick="setReturnString('gototab.additionalInfo');return checkdate('none');"><span>Additional Info</span></a></li>
				    </c:if>
				    <c:if test="${partnerType == 'AC'}">
						<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					</c:if>
			  		<li><a onclick="setReturnString('gototab.accountcontact');return checkdate('none'); " ><span>Account Contact</span></a></li>
			  		<c:if test="${checkTransfereeInfopackage==true}">
			  			<li><a onclick="setReturnString('gototab.contractpolicy');return checkdate('none');"><span>Policy</span></a></li>
			  		</c:if>
			  		<%-- Added Portal Users Tab By Kunal for ticket number: 6176 --%>
			  		<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
						<c:if test="${usertype!='ACCOUNT'}">
							<c:if test="${partner.partnerPortalActive == true}">
								<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
							</c:if>
						</c:if>
					</sec-auth:authComponent>
					<%-- Modification closed here for ticket number: 6176 --%>
			  		<c:if test='${paramValue == "View"}'>
						<li><a onclick="setReturnString('gototab.accountlist');return checkdate('none');"><span>Partner List</span></a></li>
					</c:if>
					<c:if test='${paramValue != "View"}'>
						<li><a onclick="setReturnString('gototab.accountlist');return checkdate('none');"><span>Partner List</span></a></li>
					</c:if>
					<%-- Added Portal Users Tab By Kunal for ticket number: 6176 --%>
			  		<li><a href="partnerReloSvcs.html?id=${partner.id}&partnerType=${partnerType}"><span>Services</span></a></li>
					<c:if test="${partnerType == 'AC'}">
					 <c:if test="${not empty partner.id}">
					 	<c:url value="frequentlyAskedQuestionsList.html" var="url">
						<c:param name="partnerCode" value="${partner.partnerCode}"/>
						<c:param name="partnerType" value="AC"/>
						<c:param name="partnerId" value="${partner.id}"/>
						<c:param name="lastName" value="${partner.lastName}"/>
						<c:param name="status" value="${partner.status}"/>
						</c:url>
						<li><a href="${url}"><span>FAQ</span></a></li>
						<c:if test="${partner.partnerPortalActive == true}">
							<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
						</c:if>
					  </c:if>
					</c:if>	
			  		<%-- Modification closed here for ticket number: 6176 --%>
			  	  <c:if test="${not empty accountContact.id}">
			  	  <li><a onclick="openNotesPopupTab(this);"><span>Notes</span></a></li>
			  	  </c:if>
			  	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Account Contact Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  	  </c:if>
			  	  <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${accountContact.id}&tableName=accountcontact&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			  	  <!--<li><a href="getNotesForContract.html?id=${partner.id}&notesId=${accountContact.partnerCode}&subType=${accountContact.jobType}&for=List"><span>Notes</span></a></li>
			  	  --><!--<li><a href="accountContactNotes.html?id=${accountContact.id}&notesId=${accountContact.partnerCode}&accountNotesFor=AC&noteFor=Account&subType=${accountContact.jobType}"><span>Notes</span></a></li>
			  	--></ul>
		</div><div class="spn">&nbsp;</div>
		
		</div>  
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:885px;">
	<tbody>
  <tr>
  	<td>
  	
  	<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" style="width:885px;">
		  <tbody> 
		  	<tr>
				<td height="8px"></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext">Account&nbsp;Name</td>
				<td colspan="3"><s:textfield cssClass="input-textUpper" name="partner.lastName" required="true" cssStyle="width:330px;" readonly="true"/></td>
				<td align="right" class="listwhitetext" width="100px"><fmt:message key='partner.partnerCode' /></td>
				<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="8" readonly="true" /></td>
				<td align="right" class="listwhitetext" width="50px">Status</td>
				<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.status" required="true" cssClass="input-textUpper" size="15" readonly="true" /></td>
 			</tr>
 			<tr>
				<td height="5px"></td>
			</tr>
		  	<tr>
				<td align="right" valign="top" class="listwhitetext" width="100px"><fmt:message key='accountContact.jobType'/><font color="red" size="2">*</font></td>
				<td class="listwhitetext" colspan="3" rowspan="3"><s:select cssClass="list-menu" name="accountContact.jobType" list="%{jobtype}" value="%{multiplJobTypes}" multiple="true" cssStyle="width:330px; height:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="1" /></td>				
				<td align="right" valign="top" class="listwhitetext"><fmt:message key='accountContact.jobTypeOwner'/></td>
				<td valign="top" ><s:select cssClass="list-menu" name="accountContact.jobTypeOwner"  cssStyle="width:120px" list="%{owner}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="2"/></td>
				<td align="right" valign="top"  class="listwhitetext"><fmt:message key='accountContact.contactFrequency'/></td>
				<td class="listwhitetext" valign="top" ><s:select cssClass="list-menu"  name="accountContact.contactFrequency" list="%{contact_frequency}" cssStyle="width:105px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="3"/></td>				
			</tr>
			<tr>
				<td colspan="2"></td>
				<td align="right" valign="top" class="listwhitetext" width="100px">User&nbsp;Name</td>
				<td class="listwhitetext" valign="top"><s:select cssClass="list-menu" name="accountContact.userName" list="%{userNameList}" cssStyle="width:120px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
				<c:set var="ischeckedStatus" value="false"/>
				<c:if test="${accountContact.status}">
						<c:set var="ischeckedStatus" value="true"/>
				</c:if>		
				<td align="right" class="listwhitetext">Active</td>
				<td class="listwhitetext"><s:checkbox key="accountContact.status" value="${ischeckedStatus}" fieldValue="true" onclick="changeStatus();" /></td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3" class="listwhitetext" width=""><font style="font-style: italic; font: bold; ">* Use Control + mouse to select multiple <b>Selections</b></font></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountContact.contractType'/></td>
				<td class="listwhitetext" colspan="5"><s:radio name="accountContact.contractType" list="{'Primary','Secondary'}" onclick="changeStatus();" tabindex="4"/></td>
			</tr>
			
			<tr>
				<td align="right" class="listwhitetext">Initial</td>
				<td class="listwhitetext"><s:select cssClass="list-menu"  name="accountContact.salutation" list="%{prifix}" cssStyle="width:55px"  headerKey="" headerValue=""/></td>
				<td align="right" class="listwhitetext">First Name</td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountContact.contactName" maxlength="50" onkeydown="return onlyCharsAllowed(event)" cssStyle="width:120px"/></td>
				<td align="right" class="listwhitetext">Last Name</td>
				<td><s:textfield cssClass="input-text" name="accountContact.contactLastName" maxlength="45"  cssStyle="width:120px" onkeydown=""/></td>
				<td align="right" class="listwhitetext">Lead Source</td>
				<td class="listwhitetext"><s:select cssClass="list-menu"  name="accountContact.leadSource" list="%{crmLeadParameter}" cssStyle="width:120px"  headerKey="" headerValue="" onchange="leadSourceChange();"/></td>
			</tr>
			
			<tr>
				<td align="right" class="listwhitetext">Report To</td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountContact.reportTo" maxlength="45"  onkeydown="return onlyCharsAllowed(event)"/></td>
				<td align="right" class="listwhitetext">Assistant</td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountContact.assistant" maxlength="45"  onkeydown="return onlyCharsAllowed(event)" cssStyle="width:120px"/></td>
				<td align="right" class="listwhitetext">Assistant Phone</td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountContact.assistantPhone" maxlength="45"  onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')" cssStyle="width:120px"/></td>
				<td align="right" class="listwhitetext">Other</td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountContact.leadSourceOther" maxlength="45" cssStyle="width:118px"/></td>
			</tr>
			
			<tr>
				<td align="right" class="listwhitetext">Date Of Birth</td>
				<c:if test="${not empty accountContact.dateOfBirth}">
					<s:text id="dateOfBirthFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountContact.dateOfBirth"/></s:text>
					<td><s:textfield cssClass="input-text" id="dateOfBirth" name="accountContact.dateOfBirth" value="%{dateOfBirthFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="dateOfBirth_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty accountContact.dateOfBirth}">
					<td><s:textfield cssClass="input-text" id="dateOfBirth" name="accountContact.dateOfBirth" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="dateOfBirth_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
				<td align="right" class="listwhitetext"><fmt:message key='accountContact.department'/></td>
				<td><s:textfield cssClass="input-text" name="accountContact.department"  cssStyle="width:120px" maxlength="20" tabindex="6"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountContact.title'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountContact.title" cssStyle="width:120px" maxlength="50" tabindex="7"/></td>
				<c:set var="ischecked" value="false"/>
				<c:if test="${accountContact.doNotCall}">
						<c:set var="ischecked" value="true"/>
				</c:if>		
				<td align="right" class="listwhitetext">Do Not Call</td>
				<td class="listwhitetext"><s:checkbox key="accountContact.doNotCall" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"></td>
				<td class="listwhitetext" colspan="2"></td>
				<td class="listwhitetext">Primary</td>
				<td class="listwhitetext">Opted Out</td>
				<td class="listwhitetext">Invalid</td>
				
			</tr>
			
			<tr>
				<td align="right" class="listwhitetext">Email 1</td>
				<td colspan="2"><s:textfield cssClass="input-text" name="accountContact.contactEmail" maxlength="65"  cssStyle="width:200px" onchange="return validateEmail1(this);"/></td>
				
				<c:if test="${accountContact.emailType == 'p'}">
					<td align="left" class="listwhitetext"><input type="radio" checked="checked" value="p" name="accountContact.emailType"></td>
				</c:if>
				<c:if test="${accountContact.emailType != 'p'}">
					<td align="left" class="listwhitetext"><input type="radio" value="p" name="accountContact.emailType"></td>
				</c:if>
				
				<c:set var="isOptedOut1" value="false"/>
				<c:if test="${accountContact.optedOut1}">
						<c:set var="isOptedOut1" value="true"/>
				</c:if>		
				<td class="listwhitetext"><s:checkbox key="accountContact.optedOut1" value="${isOptedOut1}" fieldValue="true" onclick="changeStatus();" /></td>
				
				<c:set var="invalid1" value="false"/>
				<c:if test="${accountContact.invalid1}">
						<c:set var="invalid1" value="true"/>
				</c:if>
				<td class="listwhitetext"><s:checkbox key="accountContact.invalid1" value="${invalid1}" fieldValue="true" onclick="changeStatus();" /></td>
				<td align="right" class="listwhitetext">Visited On</td>
				<c:if test="${not empty accountContact.visitedOn}">
					<s:text id="visitedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountContact.visitedOn"/></s:text>
					<td><s:textfield cssClass="input-text" id="visitedOn" name="accountContact.visitedOn" value="%{visitedOnFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="visitedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty accountContact.visitedOn}">
					<td><s:textfield cssClass="input-text" id="visitedOn" name="accountContact.visitedOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="visitedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			</tr>
			<tr>
				<td align="right" class="listwhitetext">Email 2</td>
				<td colspan="2"><s:textfield cssClass="input-text" name="accountContact.email2"  cssStyle="width:200px" maxlength="65" onchange="return validateEmail2(this);"/></td>
				
				<c:if test="${accountContact.emailType == 's'}">
					<td align="left" class="listwhitetext"><input type="radio" value="s" checked="checked" name="accountContact.emailType"></td>
				</c:if>
				<c:if test="${accountContact.emailType != 's'}">
					<td align="left" class="listwhitetext"><input type="radio" value="s" name="accountContact.emailType"></td>
				</c:if>
				
				<c:set var="isOptedOut2" value="false"/>
				<c:if test="${accountContact.optedOut2}">
						<c:set var="isOptedOut2" value="true"/>
				</c:if>
				<td class="listwhitetext"><s:checkbox key="accountContact.optedOut2" value="${isOptedOut2}" fieldValue="true" onclick="changeStatus();" /></td>
				
				<c:set var="invalid2" value="false"/>
				<c:if test="${accountContact.invalid2}">
						<c:set var="invalid2" value="true"/>
				</c:if>
				<td class="listwhitetext"><s:checkbox key="accountContact.invalid2" value="${invalid2}" fieldValue="true" onclick="changeStatus();" /></td>
			</tr>
			
			<tr>
				<td class="listwhitetext" align="right">Address</td>
				<td colspan="3"><s:textfield cssClass="input-text" name="accountContact.address"  cssStyle="width:327px" maxlength="50" tabindex="4" /></td>
				<td align="right" class="listwhitetext">Country</td>
				<td ><s:select cssClass="list-menu" name="accountContact.country" list="%{countryDesc}" cssStyle="width:120px"  headerKey="" headerValue="" onchange="autoPopulate_partner_country(this);getState(this);" tabindex="8" /></td>
				<td align="right" class="listwhitetext">State</td>
				<td><s:select name="accountContact.state" id="state" cssClass="list-menu"list="%{states}" cssStyle="width:120px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex="9" /></td>
			</tr>
			<tr>
				<td class="listwhitetext" align="right"></td>
				<td colspan="3"></td>
				<td align="right" class="listwhitetext">City</td>
				<td><s:textfield cssClass="input-text" name="accountContact.city" cssStyle="width:120px" maxlength="20" onkeydown="return onlyCharsAllowed(event)" tabindex="10" /></td>
				<td align="right" class="listwhitetext">Zip</td>
				<td><s:textfield cssClass="input-text" name="accountContact.zip" cssStyle="width:118px"  maxlength="10" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="11" /></td>
			</tr>
			
			<tr>
				
				
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountContact.primaryPhone'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountContact.primaryPhone"  maxlength="25" onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="9"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountContact.primaryPhoneType'/></td>
				<td><s:select cssClass="list-menu" name="accountContact.primaryPhoneType"  cssStyle="width:90px" list="%{phoneTypes}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="10"/></td>
			    <td align="right" width="" class="listwhitetext" >Based&nbsp;At</td>
			 	<td align="left" ><s:textfield name="accountContact.basedAt" cssClass="input-text"  cssStyle="width:90px;" onchange="valid(this,'special');" onblur="findbasedAt();"  maxlength="8" /><img class="openpopup" id="openPopUp1" width="17" height="20" align="top" onclick="javascript:openWindow('searchPartner.html?&partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountContact.basedAtName&fld_code=accountContact.basedAt');" src="<c:url value='/images/open-popup.gif'/>" /></td>
			 	<td align="right" class="listwhitetext" width="">Based&nbsp;At&nbsp;Name</td>
			 	<td align="left"><s:textfield name="accountContact.basedAtName" cssClass="input-text" cssStyle="width:118px;" readonly="true"  maxlength="30" /></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountContact.secondaryPhone'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountContact.secondaryPhone" maxlength="25"  onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="11"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountContact.secondaryPhoneType'/></td>
				<td><s:select cssClass="list-menu" name="accountContact.secondaryPhoneType"  cssStyle="width:90px"  list="%{phoneTypes}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="12"/></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='accountContact.thirdPhone'/></td>
				<td class="listwhitetext"><s:textfield cssClass="input-text"  name="accountContact.thirdPhone" maxlength="25"  onkeydown="return onlyPhoneNumsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="13"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='accountContact.thirdPhoneType'/></td>
				<td><s:select cssClass="list-menu" name="accountContact.thirdPhoneType"  cssStyle="width:90px"  list="%{phoneTypes}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="14"/></td>
				<td colspan="3"></td>
				<c:if test="${empty accountContact.id}">
						<td align="right" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
					</c:if>
					<c:if test="${not empty accountContact.id}">
					<c:choose>
						<c:when test="${countAccountContactNotes == '0' || countAccountContactNotes == '' || countAccountContactNotes == null}">
							<td align="right" valign="bottom"><img id="countAccountContactNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="openNotesPopup(this);"/><a onclick="openNotesPopup(this);" ></a></td>
						</c:when>
						<c:otherwise>
							<td align="right" valign="bottom"><img id="countAccountContactNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="openNotesPopup(this);"/><a onclick="openNotesPopup(this);" ></a></td>
						</c:otherwise>
					</c:choose> 
				</c:if>
			</tr>
			<tr>
				<td align="right" class="listwhitetext"></td>
				<td colspan="6"></td>
				
			</tr>
			<tr><td height="2px;"></td></tr>
			</tbody></table>
		</td>
		</tr>
	</tbody>
</table>
<div style="!margin-top:8px;"></div>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<table style="width:800px" border="0">
	<tr>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='accountContact.createdOn'/></td>
		<fmt:formatDate var="accConCreatedOnFormattedValue" value="${accountContact.createdOn}" pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="accountContact.createdOn" value="${accConCreatedOnFormattedValue}" />
		<td style="width:130px"><fmt:formatDate value="${accountContact.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
		
		<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='accountContact.createdBy' /></b></td>
		<c:if test="${not empty accountContact.id}">
			<s:hidden name="accountContact.createdBy"/>
			<td style="width:125px"><s:label name="createdBy" value="%{accountContact.createdBy}"/></td>
		</c:if>
		<c:if test="${empty accountContact.id}">
			<s:hidden name="accountContact.createdBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:125px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='accountContact.updatedOn'/></b></td>
		<fmt:formatDate var="accContactUpdatedOnFormattedValue" value="${accountContact.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="accountContact.updatedOn" value="${accContactUpdatedOnFormattedValue}" />
		<td width="130px"><fmt:formatDate value="${accountContact.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
		
		<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='accountContact.updatedBy' /></b></td>
		<c:if test="${not empty accountContact.id}">
			<s:hidden name="accountContact.updatedBy"/>
			<td style="width:125px"><s:label name="updatedBy" value="%{accountContact.updatedBy}"/></td>
		</c:if>
		<c:if test="${empty accountContact.id}">
			<s:hidden name="accountContact.updatedBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:125px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
	</tr>
</table> 
<table>
	<tr>
		<td align="left">
			<c:if test="${empty param.popup}" >
				<s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:60px; height:25px;" onclick="return validteFields();"/>    
				<input type="reset" class="cssbutton" value="Reset" style="width:60px; height:25px " onclick="newFunctionForCountryState();"/>   
				<s:hidden name="hitFlag" />
				<c:if test="${hitFlag =='1'}" >
					<c:redirect url="/accountContactList.html?id=${partner.id}&partnerType=${partnerType}"/>
				</c:if>
			</c:if>
			
		</td>
	</tr>
</table>
    <s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
</s:form>

<%-- Script Shifted from Top to Botton on 06-Sep-2012 By Kunal --%>
<%-- Shift 1 --%>
<%-- Modified By Kunal Sharma at 13-Jan-2012 --%> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<%-- Modification closed here --%>
<%-- Shift 2 --%>
<script>
function showOrHide(value) { 
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} 
}  
function notExists(){
	alert("The Account information has not been saved yet, please save account information to continue");
}

function openNotesPopup(targetElement){
	var id = document.forms['accountContactForm'].elements['accountContact.id'].value;
	var notesId = document.forms['accountContactForm'].elements['accountContact.partnerCode'].value;
	var noteSubType = document.forms['accountContactForm'].elements['accountContact.jobType'].value;
	var pId = document.forms['accountContactForm'].elements['partner.id'].value;
	var imgID = targetElement.id;
	openWindow('accountContactNotes.html?id='+id+'&notesId='+notesId+'&accountNotesFor=AC&noteFor=AccountContact&subType='+noteSubType+'&imageId='+imgID+'&pId='+pId+'&for=List&fieldId=countAccountContactNotes&decorator=popup&popup=true',800,600);
	document.forms['accountContactForm'].elements['formStatus'].value = '';
}

function openNotesPopupTab(targetElement){
	var id = document.forms['accountContactForm'].elements['accountContact.id'].value;
	var notesId = document.forms['accountContactForm'].elements['accountContact.partnerCode'].value;
	var noteSubType = document.forms['accountContactForm'].elements['accountContact.jobType'].value;
	var pId = document.forms['accountContactForm'].elements['partner.id'].value;
	var imgID = targetElement.id;
	openWindow('accountContactNotes.html?id='+id+'&notesId='+notesId+'&accountNotesFor=AC&noteFor=AccountContact&subType='+noteSubType+'&imageId='+imgID+'&pId='+pId+'&for=List&fieldId=countAccountContactNotes&fromTab=yes&decorator=popup&popup=true',800,600);
	document.forms['accountContactForm'].elements['formStatus'].value = '';
}

function onlyCharsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
}

function onlyNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36) ; 
}
function autoPopulate_partner_country(targetElement) {	
   	var country = targetElement.value;
   	var countryCode=""
   	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	if(countryCode!=''){
		document.forms['accountContactForm'].elements['accountContact.state'].disabled = false;
		document.forms['accountContactForm'].elements['accountContact.state'].value = '';
	}else{
		document.forms['accountContactForm'].elements['accountContact.state'].disabled = true;
		document.forms['accountContactForm'].elements['accountContact.state'].value = '';
	}
}
function getState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse22;
    http2.send(null);
   
}

function handleHttpResponse22(){
	if (http2.readyState == 4){
        var results = http2.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['accountContactForm'].elements['accountContact.state'];
		targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['accountContactForm'].elements['accountContact.state'].disabled = true;
				document.forms['accountContactForm'].elements['accountContact.state'].options[i].text = '';
				document.forms['accountContactForm'].elements['accountContact.state'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['accountContactForm'].elements['accountContact.state'].disabled = false;
				document.forms['accountContactForm'].elements['accountContact.state'].options[i].text = stateVal[1];
				document.forms['accountContactForm'].elements['accountContact.state'].options[i].value = stateVal[0];
				
			}
		}
		 	
    }
}

function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http2 = getHTTPObject(); 
	
</script>
<script>
function checkdate(clickType){
	var validateFieldJob = validteFields();
	if(validateFieldJob){
		if ('${autoSavePrompt}' == 'No'){
		var id = document.forms['accountContactForm'].elements['accountContact.id'].value;
		var id1 = document.forms['accountContactForm'].elements['partner.id'].value;
		var partnerType = document.forms['accountContactForm'].elements['partnerType'].value;
		if(document.forms['accountContactForm'].elements['formStatus'].value == '1'){
			document.forms['accountContactForm'].action = 'saveAccountContact!saveOnTabChange.html';
			document.forms['accountContactForm'].submit();
		}
		else{
		if(id1 != ''){
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.additionalInfo'){
					location.href = "editPartnerPrivate.html?partnerId="+id1+"&partnerType=${partnerType}&partnerCode=${partner.partnerCode}";
				}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
					location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
					location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
					location.href = 'accountContactList.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
					location.href = 'editContractPolicy.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
							location.href = 'partnerView.html';
						}else{
							location.href = 'partnerPublics.html?partnerType=AC';
						}
					}
				
			}
		}
	}else{
		if(!(clickType == 'save')){
		var id = document.forms['accountContactForm'].elements['accountContact.id'].value;
		var id1 = document.forms['accountContactForm'].elements['partner.id'].value;
		var partnerType = document.forms['accountContactForm'].elements['partnerType'].value;
		
		if (document.forms['accountContactForm'].elements['formStatus'].value == '1'){
			var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='accountContactDetail.heading'/>");
			if(agree){
				document.forms['accountContactForm'].action = 'saveAccountContact!saveOnTabChange.html';
				document.forms['accountContactForm'].submit();
			}else{
				if(id != ''){
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.additionalInfo'){
					location.href = "editPartnerPrivate.html?partnerId="+id1+"&partnerType=${partnerType}&partnerCode=${partner.partnerCode}";
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
					location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
					location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
					location.href = 'accountContactList.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
					location.href = 'editContractPolicy.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
							location.href = 'partnerView.html';
						}else{
							location.href = 'partnerPublics.html?partnerType=AC';
						}
					}
			}
			}
		}else{
		if(id != ''){
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.additionalInfo'){
					location.href = "editPartnerPrivate.html?partnerId="+id1+"&partnerType=${partnerType}&partnerCode=${partner.partnerCode}";
				}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
					location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
					location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
					location.href = 'accountContactList.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
					location.href = 'editContractPolicy.html?id='+id1+'&partnerType='+partnerType;
					}
				if(document.forms['accountContactForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
							location.href = 'partnerView.html';
						}else{
							location.href = 'partnerPublics.html?partnerType=AC';
						}
					}
		}
		}
	}
	}
	} //Closed validatejob if
}
function changeStatus(){
	document.forms['accountContactForm'].elements['formStatus'].value = '1';
}

function validateEmail1(targetElement) {
	var x = targetElement.value;

	if (/^([a-zA-Z0-9_\.\-\'])+@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+(,\s*([a-zA-Z0-9_\.\-\'])+@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4}))*$/.test(x))
	  {
	   
	        return true;
	  }
	       alert("Please enter valid email 1 address")
	        targetElement.value='';
	        return false;
}

function validateEmail2(targetElement) {
	var x = targetElement.value;
	if (/^([a-zA-Z0-9_\.\-\'])+@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+(,\s*([a-zA-Z0-9_\.\-\'])+@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4}))*$/.test(x))
	  {
	   
	        return true;
	  }
	        alert("Please enter valid email 2 address")
	        targetElement.value='';
	        return false;
}

function leadSourceChange(){
	if(document.forms['accountContactForm'].elements['accountContact.leadSource'].value != '' && document.forms['accountContactForm'].elements['accountContact.leadSource'].value == 'Other'){
		document.forms['accountContactForm'].elements['accountContact.leadSourceOther'].disabled = false;
	}else{
		document.forms['accountContactForm'].elements['accountContact.leadSourceOther'].value = '';
		document.forms['accountContactForm'].elements['accountContact.leadSourceOther'].disabled = true;
	}
} 

function validteFields(){
	if(document.forms['accountContactForm'].elements['accountContact.jobType'].value == ''){
		document.forms['accountContactForm'].elements['accountContact.jobType'].focus();
		alert('Please select job type.');
		return false;
	}
	else{
		return true;
	}
}
function findbasedAt(){
	showOrHide(1);
    var basedAtCode = document.forms['accountContactForm'].elements['accountContact.basedAt'].value;
       if(basedAtCode==''){
    	document.forms['accountContactForm'].elements['accountContact.basedAtName'].value="";
    	showOrHide(0);
    	}else{
     var url="basedAtName.html?ajax=1&decorator=simple&popup=true&basedAtCode=" + encodeURI(basedAtCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse2;
     http9.send(null);
    }
}	
function handleHttpResponse2(){
    if (http9.readyState == 4){
       var results = http9.responseText
       results = results.trim();
       results=results.replace("[",'');
       results=results.replace("]",'');
       if(results.length >= 1){ 
	        document.forms['accountContactForm'].elements['accountContact.basedAtName'].value=results;
	        showOrHide(0);
	        return true;
    	}else{
	        alert("Based at code not valid");    
			document.forms['accountContactForm'].elements['accountContact.basedAt'].value="";
			document.forms['accountContactForm'].elements['accountContact.basedAtName'].value="";
			showOrHide(0);
			return false;
    	}
    }
}
var http9 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
</script>
<%-- Shift 3 --%>
<script type="text/javascript">
function validteAdditionalInfo(){
	if(document.forms['accountContactForm'].elements['accountContact.jobType'].value == ''){
		document.forms['accountContactForm'].elements['accountContact.jobType'].focus();
		alert('Please select job type.');
		return false;
	}
	else{
		location.href = "editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}";
	}
}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript"> 
leadSourceChange();
try{
<c:if test="${param.popup}" >
	for(i=0;i<100;i++){
		document.forms['accountContactForm'].elements[i].disabled = true;
	}
	   
</c:if>

Form.focusFirstElement($("accountContactForm"));
}
catch(e){}
</script>

<script type="text/javascript">
	try{
	//autoPopulate_partner_country11(document.forms['accountContactForm'].elements['accountContact.country']);
	getState(document.forms['accountContactForm'].elements['accountContact.country']); 
 }
 catch(e){}

</script>
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">

function newFunctionForCountryState(){ 
	  var originAbc ='${accountContact.country}';
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(originAbc)> -1); 
	  //alert(index);
	  if(index != ''){
		  //alert('1');
	   document.forms['accountContactForm'].elements['accountContact.state'].disabled =false;
	  getOriginStateReset(originAbc);
	  }
	  else{
	   document.forms['accountContactForm'].elements['accountContact.state'].disabled =true;
	  }  
	  }

function getOriginStateReset(target) {
	  var country = target;
	 // alert(country);
	   var countryCode = "";
	  <c:forEach var="entry" items="${countryCod}">
	  if(country=="${entry.value}"){
	   countryCode="${entry.key}";
	  }
	  </c:forEach>
	   var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	      httpState1.open("GET", url, true);
	      httpState1.onreadystatechange = handleHttpResponse555555;
	      httpState1.send(null);
	 }
	           
	  function handleHttpResponse555555(){
	       if (httpState1.readyState == 4){
	          var results = httpState1.responseText
	          results = results.trim();
	          res = results.split("@");
	          targetElement = document.forms['accountContactForm'].elements['accountContact.state'];
	   targetElement.length = res.length;
	    for(i=0;i<res.length;i++) {
	    if(res[i] == ''){
	    document.forms['accountContactForm'].elements['accountContact.state'].options[i].text = '';
	    document.forms['accountContactForm'].elements['accountContact.state'].options[i].value = '';
	    }else{
	    stateVal = res[i].split("#");
	    document.forms['accountContactForm'].elements['accountContact.state'].options[i].text = stateVal[1];
	    document.forms['accountContactForm'].elements['accountContact.state'].options[i].value = stateVal[0];
	    
	    if (document.getElementById("state").options[i].value == '${accountContact.state}'){
	       document.getElementById("state").options[i].defaultSelected = true;
	    } } }
	    document.getElementById("state").value = '${accountContact.state}';
	       }
	  }  
	var httpState1 = getHTTPObject();

	</script>