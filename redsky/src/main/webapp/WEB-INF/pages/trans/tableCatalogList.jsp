<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Table Catalog List</title>   
    <meta name="heading" content="Table Catalog List"/>  
    
<style>
.tab{
	border:1px solid #74B3DC;
}
span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-16px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;

}
</style> 

<script type="text/javascript"> 
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove it?");
	var id = targetElement;
	if (agree){
		location.href="deleteTableCatalog.html?id="+id;
	}else{
		return false;
	}
}
	function clear_fields(){
		document.forms['tableCatalogListPage'].elements['tableN'].value = '';
		document.forms['tableCatalogListPage'].elements['tableD'].value = '';
     }
	function exportData(tableName){
		location.href='exportTableCatalog.html?tableName='+tableName;
	}
</script>
</head>
<s:form id="tableCatalogListPage" action="searchTableCatalogList" method="post" validate="true">

<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="tableName" value="<%=request.getParameter("tableName")%>" />	


<div id="layer1" style="width:100%;">
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Table Name</th>
		<th>Table Description</th>		
		</tr>
		</thead>	
		<tbody>
				<tr>
  				    <td align="left">
					    <s:textfield name="tableN" required="true" cssClass="input-text" size="45"/>
					</td>
					<td align="left">
					    <s:textfield name="tableD" required="true" cssClass="input-text" size="45"/>
					</td>					
				</tr>		
				<tr><td></td><td align="right" colspan="0"><c:out value="${searchbuttons}" escapeXml="false"/></td></tr>			
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<div id="newmnav">
	  <ul>
	  	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Table Catalog List</span></a></li>	  	
				<li><a href="editTableCatalog.html" ><span>Table Catalog Details</span></a></li>					  	
	  </ul>
</div>
<div style="width: 100%" >
<div class="spn">&nbsp;</div>
</div>
<div id="content" align="center">
<div id="Layer1" style="width: 100%;">

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>				
<display:table name="tableCatalogList" class="table" id="tableCatalogList" requestURI="" pagesize="25" style="width:100%;"> 
	<display:column property="id" sortable="true" title="Id" url="/editTableCatalog.html"  paramId="id" paramProperty="id" style="width:30px"/>
	<display:column property="tableName" sortable="true" title="Table Name" url="/searchDataCatalog.html" paramId="tableName" paramProperty="tableName" style="width:30px"/>		
	<display:column property="tableDescription" sortable="true" title="Table Description"  maxLength="100" style="width:210px"/>		
	<display:column property="totalField" sortable="true" title="Total Field" maxLength="15" style="width:110px"/>				
	
	<display:column title="Extract" style="width:50px">
			<a><img title="Extract" align="middle"  style="margin: 0px 0px 0px 8px;" onclick="exportData('${tableCatalogList.tableName}');" src="images/extract-para.png"/></a>						
			</display:column>
		<display:column title="Remove" style="text-align:center; width:20px;">
				<a><img align="middle" onclick="confirmSubmit(${tableCatalogList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>					
					<!--<display:setProperty name="paging.banner.items_name" value="Table Catlaog"/> 				  
				    <display:setProperty name="export.excel.filename" value="Table Catalog List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Table Catlaog List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Table Catalog List.pdf"/>-->
		</display:table>
			</td>
		</tr>
	</tbody>
</table> 
<c:set var="buttons"> 
    <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="location.href='<c:url value="/editTableCatalog.html"/>'" value="<fmt:message key="button.add"/>"/> 
</c:set> 
<c:out value="${buttons}" escapeXml="false" /> 
</div>
</div>
</s:form>