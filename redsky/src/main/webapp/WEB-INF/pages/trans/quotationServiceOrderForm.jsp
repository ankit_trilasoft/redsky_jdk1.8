<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<head>
<title>Quote Detail</title>
<meta name="heading" content="Quote Detail" />
<style type="text/css">h2 {background-color: #FBBFFF}</style>
<style>
div div ul li {   
    list-style-type:none;
}
.table th.RemoveBorder 
{
border-left: hidden;
!border-left-style:none;
}
.table th.FXRight {
text-align:right;
padding-right:10px;
}
.table th.RemoveBorderRight 
{
!border-right-style:none;
}
.upper-case{
text-transform:capitalize;
}
#loader {
position:absolute; z-index:999; margin:0px auto -1px 250px;
}

a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:130px;
  height:35px;
  line-height: 15px;
  padding-top:4px;
  visibility: hidden;
  font-size: 11px;
  text-align: center;  
  color: #15428B;     
  border: 1px solid #56bcdd;
  border-radius: 5px;         
  box-shadow: rgba(0, 0, 0, 0.1) 1px 1px 2px 0px;
 background: rgb(254,255,255); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkMmViZjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(254,255,255,1) 0%, rgba(210,235,249,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(254,255,255,1)), color-stop(100%,rgba(210,235,249,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffff', endColorstr='#d2ebf9',GradientType=0 ); /* IE6-8 */
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 60%;
  margin-left: -8px;
  width: 0; height: 0; 
  border-color: #56bcdd transparent transparent transparent;
  border-width: 10px;
  border-style: solid;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 1;
  bottom: 34px; 
  left: 50%;
  margin-left: -66px;
  z-index: 999;
}
  .ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
</style>

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
		<%@ include file="/common/calenderStyle.css"%>
</script>
<script type="text/javascript"> 
function titleCase(element){ 
	var txt=element.value; var spl=txt.split(" "); 
	var upstring=""; for(var i=0;i<spl.length;i++){ 
	try{ 
	upstring+=spl[i].charAt(0).toUpperCase(); 
	}catch(err){} 
	upstring+=spl[i].substring(1, spl[i].length); 
	upstring+=" ";   
	} 
	element.value=upstring.substring(0,upstring.length-1); 
}
</script> 
<script language="javascript" type="text/javascript">
 function getCarrier(){
	 <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
	 var modeType = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
  if(modeType=='Air'|| modeType=='Sea'){
   document.getElementById('carrierFild').style.display="block";
   document.getElementById('carrierFildLabel').style.display="block";
  }else{
   document.getElementById('carrierFild').style.display="none";
   document.getElementById('carrierFildLabel').style.display="none";
  }
  </configByCorp:fieldVisibility>
}
</script>
			 <script type="text/javascript">
			function getHTTPObjectActualizationDate()
			{
			    var xmlhttp;
			    if(window.XMLHttpRequest)
			    {
			        xmlhttp = new XMLHttpRequest();
			    }
			    else if (window.ActiveXObject)
			    {
			        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			        if (!xmlhttp)
			        {
			            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
			        }
			    }
			    return xmlhttp;
			}
			 var actualizationDate = getHTTPObjectActualizationDate(); 
			 function findActualizationDateOfContract(){
			     var contracts = document.forms['serviceOrderForm'].elements['serviceOrder.contract'].value;
			     <c:if test="${not empty serviceOrder.id}">
			     if(contracts!=null && contracts.trim()!=''){
				     var sid="${serviceOrder.id}";
			 	    var url="findActualizationDateOfContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts) +"&updateFlag=Y&bid=" + encodeURI(sid);
			 	    actualizationDate.open("GET", url, true);
			 	    actualizationDate.onreadystatechange = handleHttpResponseActualizationDate;
			 	    actualizationDate.send(null);
			     }
			     </c:if>
			 }
			 function handleHttpResponseActualizationDate(){
			      if (actualizationDate.readyState == 4) {
			         var results = actualizationDate.responseText
			         results = results.trim();
			         if(results.length>1){
			 	       	 if(results=='TRUE'){
			 	       		document.forms['serviceOrderForm'].elements['billing.fXRateOnActualizationDate'].value=true;
			 	       	 }else{
			 	       		document.forms['serviceOrderForm'].elements['billing.fXRateOnActualizationDate'].value=false;
			 	       	 }
			         }
			      }
			 } 
function fillUsaFlag(){
	 <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
 var modeType = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
  if(modeType=='Air'|| modeType=='Sea'){
	var flagBillTocode = document.forms['serviceOrderForm'].elements['serviceOrder.billToCode'].value;
	if(flagBillTocode!='') {
	    var url="getUsaFlagAjax.html?ajax=1&decorator=simple&popup=true&flagBillTocode="+encodeURI(flagBillTocode);
	    http511.open("GET", url, true); 
	   	http511.onreadystatechange = handleHttp905; 
	   	http511.send(null);	     
     }
   }
  </configByCorp:fieldVisibility>
} 
   function handleHttp905(){
    if (http511.readyState == 4){    	
    	var results = http511.responseText
		if(results!=null && results.trim()!='false'){
			document.forms['serviceOrderForm'].elements['serviceOrder.serviceOrderFlagCarrier'].value='USA';
			}
    	}
       }  
var http511 = getHTTPObject();
function submitSoPage(){
	var incExcype = document.forms['serviceOrderForm'].elements['userQuoteServices'].value;		 			
	if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Separate section'){
			pickCheckBoxData('');
	 }else if(incExcype!=undefined && incExcype !=null && incExcype != '' && incExcype.trim()=='Single section'){
			pickCheckBoxDataSec();
		}else{
		}	
	submit_form();
}

</script>

</head>
<div id="Layer5" style="width:100%">
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<c:set var="fileID" value="%{serviceOrder.id}"/>  
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" value="agentQuotesSO" />
<s:form id="serviceOrderForm" name="serviceOrderForm" action="saveQuotatonServiceOrder" method="post" validate="true" onsubmit ="return submitSoPage();">
<s:hidden name="chargeCodeValidationVal"  id= "chargeCodeValidationVal" />
<s:hidden name="chargeCodeValidationMessage"  id= "chargeCodeValidationMessage" />
	<input type="hidden" name="tempId1" value="">
	<input type="hidden" name="tempId2" value="">
	<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
	</configByCorp:fieldVisibility>
	<s:hidden name="billing.fXRateOnActualizationDate"/>
	 <configByCorp:fieldVisibility componentId="component.field.ServiceOrder.checkCompanyBAA">
	<s:hidden name="checkCompanyBA" value="YES"/>
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.RollUpInInvoice">
		<s:hidden name="rollUpInvoiceFlag" value="True" />
	</configByCorp:fieldVisibility>
	<s:hidden name="corpID" value="${customerFile.corpID}" />
      <s:hidden name="reset1" id="reset1" value=""/>
       <s:hidden name="addAddressState" />
        <s:hidden name="pricingButton" value="<%=request.getParameter("pricingButton") %>"/>
        <c:set var="pricingButton"  value="<%=request.getParameter("pricingButton") %>"/>
        <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}" /> 
        <s:hidden name="serviceOrder.customerFileId" value="%{customerFile.id}"/>
        <s:hidden name="customerFileId" value="%{customerFile.id}"/>
        <s:hidden name="customerFile.contract" value="%{customerFile.contract}"/> 
        <s:hidden name="jobtypeSO" value="${serviceOrder.job}"/>
        <s:hidden name="jobtypeCheck" value="" /> 
        <s:hidden name="routingSO" value="${serviceOrder.routing}" />
         <s:hidden name="modeSO" value="${serviceOrder.mode}"/>
         <s:hidden name="packingModeSO" value="${serviceOrder.packingMode}"/>
         <s:hidden name="commoditySO" value="${serviceOrder.commodity}"/>
         <s:hidden name="serviceTypeSO" value="${serviceOrder.serviceType}"/>
         <s:hidden name="companyDivisionSO" value="${serviceOrder.companyDivision}"></s:hidden>
         <s:hidden name="accountIdCheck"/>
         <s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
	     <s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
         <s:hidden name="accId" />
         <s:hidden name="accEstimateQuantity" />
         <s:hidden name="accEstimateRate" />
         <s:hidden name="accEstimateSellRate" />
         <s:hidden name="accEstimateExpense" />
         <s:hidden name="accEstimateRevenueAmount" />
         <s:hidden name="accEstSellLocalAmount" />
         <s:hidden name="checkConditionForOriginDestin"/>
         <s:hidden  name="billing.contract" value="${serviceOrder.contract}"/>
         <s:hidden name="buyDependSell" />
         <s:hidden name="pricingFooterValue" id="pricingFooterValue" />
         <s:hidden name="SNumber" value="%{serviceOrder.shipNumber}"/>
		 <c:set var="SNumber" value="${serviceOrder.shipNumber}" />
        <%
        String value=(String)pageContext.getAttribute("SNumber") ;
        Cookie cookie = new Cookie("TempnotesId",value);
        cookie.setMaxAge(3600);
        response.addCookie(cookie);
        %>
         <s:hidden name="stdAddDestinState" />
         <s:hidden name="stdAddOriginState" />
         <s:hidden name="unit" id ="unit" value="%{dummyDistanceUnit}" />
	    <s:hidden name="id" value="<%=request.getParameter("id") %>" />
	    <s:hidden name="countOriginDetailNotes" value="<%=request.getParameter("countOriginDetailNotes") %>"/>
		<s:hidden name="countDestinationDetailNotes" value="<%=request.getParameter("countDestinationDetailNotes") %>"/>
		<s:hidden name="countWeightDetailNotes" value="<%=request.getParameter("countWeightDetailNotes") %>"/>
		<s:hidden name="countVipDetailNotes" value="<%=request.getParameter("countVipDetailNotes") %>"/>
		<s:hidden name="countServiceOrderNotes" value="<%=request.getParameter("countServiceOrderNotes") %>"/>
		<s:hidden name="serviceOrder.defaultAccountLineStatus" value="${serviceOrder.defaultAccountLineStatus}"/>
		<s:hidden name="defaultTemplate" id="defaultTemplate"/>
		 <s:hidden  name="serviceOrder.incExcServiceType"/>
		 <s:hidden  name="servIncExcDefault"/> 
		  <s:hidden name="userQuoteServices" />
		<c:set var="countOriginDetailNotes" value="<%=request.getParameter("countOriginDetailNotes") %>" />
		<c:set var="countDestinationDetailNotes" value="<%=request.getParameter("countDestinationDetailNotes") %>" />
		<c:set var="countWeightDetailNotes" value="<%=request.getParameter("countWeightDetailNotes") %>" />
		<c:set var="countVipDetailNotes" value="<%=request.getParameter("countVipDetailNotes") %>" />
		<c:set var="countServiceOrderNotes" value="<%=request.getParameter("countServiceOrderNotes") %>" />
        <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
		<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="miscellaneous.unit1" />
    <s:hidden name="miscellaneous.unit2"/>
    <s:hidden name="serviceOrder.controlFlag"/>
    <s:hidden name="serviceOrder.clientId"/>
	 	<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
	 <c:if test="${serviceOrder.job =='RLO'}">
	 <s:hidden  name="reloServiceType" value="${serviceOrder.serviceType}" />
	 </c:if>
	 <c:if test="${serviceOrder.job !='RLO'}">
	 <s:hidden  name="reloServiceType" value="" />
	 </c:if>	 	
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
	<c:choose>
		<c:when test="${gotoPageString == 'gototab.serviceorder' }">
		   <c:redirect url="/quotationServiceOrders.html?id=${customerFile.id}"/>
		</c:when>
		<c:when test="${gotoPageString == 'gototab.accounting' }">
		   <c:redirect url="/quotationAccountLineList.html?sid=${serviceOrder.id}"/>
		</c:when>
		<c:when test="${gotoPageString == 'gototab.quotation' }">
		   <c:redirect url="/QuotationFileForm.html?id=${customerFile.id}"/>
		</c:when>
		<c:when test="${gotoPageString == 'gototab.OI' }">
	 <c:redirect url="/operationResource.html?id=${serviceOrder.id}&quoteFlag=y"/>
	 </c:when>
		
		<c:when test="${gotoPageString == 'gototab.survey' }">
		   <c:redirect url="/inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}&Quote=y"/>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose> 
</c:if>
<c:if test="${not empty serviceOrder.id}">
  <div id="newmnav" style="float: left;">
	  <ul>
		  <li id="newmnav1" style="background:#FFF "><a onclick="setReturnString('gototab.serviceorder');return saveAuto('none');" class="current"><span>Quotes<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		  <li><a onclick="setReturnString('gototab.quotation');return saveAuto('none');"><span>Quotation File</span></a></li>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
   	  <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  <li><a href="operationResource.html?id=${serviceOrder.id}&quoteFlag=y" onclick="setReturnString('gototab.quotation');return saveAuto('none');"><span>O&I</span></a></li>
	  </sec-auth:authComponent>
	 </c:if>
		        <c:if test="${voxmeIntergartionFlag=='true'}">
		  <li><a onclick="setReturnString('gototab.survey');return saveAuto('none');"><span>Survey Details</span></a></li>		        		 
		     </c:if>
		  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=quote&reportSubModule=quote&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');" tabindex=""><span>Forms</span></a></li>		  
		  <li><a onclick="openWindow('auditList.html?id=${serviceOrder.id}&tableName=serviceorder&decorator=popup&popup=true',750,400)"><span>Audit</span></a></li> 
	  </ul>
  </div>
  <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;height:22px;float: none; "><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}"  >
  		<a><img align="middle" id="navigation1" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" id="navigation2" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" id="navigation3" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" id="navigation4" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>		
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" style="!padding-top:1px;">
		<a><img class="openpopup" id="navigation5" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>		
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="!vertical-align:top;">
  		<a><img align="middle" id="navigation6" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>		
		</c:if>
		</tr>
		</table>
  <div class="spn">&nbsp;</div>
</c:if>
<c:if test="${empty serviceOrder.id}">
  <div id="newmnav">
	  <ul>
		  <li id="newmnav1" style="background:#FFF "><a onclick="setReturnString('gototab.serviceorder');return saveAuto('none');" class="current"><span>Quotes<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		  <li><a onclick="setReturnString('gototab.quotation');return saveAuto('none');"><span>Quotation File</span></a></li>
		  <li><a><span>Survey Details</span></a></li>
		  <li><a><span>Forms</span></a></li>		  
		  <li><a><span>Audit</span></a></li> 
	  </ul>
  </div>
      	
  <div class="spn">&nbsp;</div>
</c:if>
</div>
<div id="Layer1"  onkeydown="changeStatus();" style="width:100%;">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0"  cellpadding="0" border="0" style="width:100%">
		<tbody>
			<tr>
				<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
				<tbody>
				<tr><td height="5px"></td></tr>
					<tr>
							<td align="right" class="listwhitetext" style="width:90px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.prefix'/></td>
							<td align="right" class="listwhitetext" style="width:5px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.firstName'/></td>
							<td align="right" class="listwhitetext" style="width:5px"></td>
							<td align="center" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.mi'/></td>
							<td align="right" class="listwhitetext" style="width:5px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.lastName'/></td>
							<td align="right" class="listwhitetext" style="width:5px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber"><fmt:message key='serviceOrder.socialSecurityNumber'/></configByCorp:fieldVisibility></td>
							<td align="right" class="listwhitetext" style="width:5px"></td>
								<td align="right" class="listwhitetext" style="width:5px"></td>								
								<td align="right" width="66" valign="middle" class="listwhitetext" rowspan="2">
								<%-- Added by kunal for ticket number: 6092 --%>
								<c:set var="isVipFlag" value="false" />
								<c:if test="${serviceOrder.vip or customerFile.vip}">
									<c:set var="isVipFlag" value="true" />
								</c:if>
								<div style="position:absolute;top:10px;margin-left:20px;!margin-left:-35px;">
									<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
								</div>
							</td>
								
						
					</tr>
					<tr>
							<td align="left" style="width:90px"></td>
							<td align="left" style="width:5px"><s:textfield cssClass="input-textUpper" name="serviceOrder.prefix" id="tabindexFlag" cssStyle="width:30px;" maxlength="15" onfocus = "myDate();"  readonly="true" tabindex=""/></td>
							<td align="left" style="width:3px"><td align="left"><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.firstName" onblur="titleCase(this)" onkeypress="" cssStyle="width:180px;" maxlength="80" readonly="true" tabindex=""/> </td>
							<td align="left" style="width:20px"></td>
							<td align="left"><s:textfield cssClass="input-textUpper" name="serviceOrder.mi" cssStyle="width:13px" maxlength="1" readonly="true" /> </td>
							<td align="left" style="width:12px"></td>
							<td align="left"><s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.lastName" onblur="titleCase(this)" onkeypress="" cssStyle="width:155px;" maxlength="80" required="true" readonly="true" tabindex=""/> </td>
							<td align="left" style="width:15px"></td>
							
							<td align="left" class="listwhitetext" >
								<configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber">
					        		<s:textfield cssClass="input-textUpper" key="serviceOrder.socialSecurityNumber" cssStyle="width:112px;" maxlength="9" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')" tabindex=""/>
					        	</configByCorp:fieldVisibility>
					        </td>
														<td align="right"><s:checkbox id="serviceOrder.vip" name="serviceOrder.vip" value="${isVipFlag}" fieldValue="true" disabled="disabled" onclick="changeStatus(); setVipImage();" tabindex="" /></td>
														<td align="left" class="listwhitebox" valign="middle"><fmt:message key='serviceOrder.vip'/></td>
														<td width="100px"></td>
														<c:if test="${empty serviceOrder.id}">
							<td width="153px" align="left"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/>&nbsp;&nbsp;</td>
							</c:if>
							<c:if test="${not empty serviceOrder.id}">
							<c:choose>
								<c:when test="${countVipDetailNotes == '0'}">
								<td width="153px" align="left"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=VipPerson&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=VipPerson&decorator=popup&popup=true',800,600);" ></a>&nbsp;</td>
								</c:when>
								<c:otherwise>
								<td width="153px" align="left"><img src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=VipPerson&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=VipPerson&decorator=popup&popup=true',800,600);" ></a>&nbsp;</td>
								</c:otherwise>
							</c:choose> 
							</c:if>
						</tr>						
						</tbody>
						</table>
						</td>
						</tr>
						<tr>
						<td>						
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					<tbody>
					<tr><td height="5px"></td></tr>
						<tr>
							<td style="width:90px"></td>
								<td align="left" class="listwhitetext">S/O#</td>
								<td align="right" class="listwhitetext" style="width:7px"></td>
								<td align="left" class="listwhitetext">Status</td>
								<td align="right" class="listwhitetext" style="width:7px"></td>
								<td align="left" class="listwhitetext">Status Date</td>
								<td align="left" class="listwhitetext" style="width:7px"></td>
								<td id="reasonHid" align="left" class="listwhitetext">Status Reason</td>
							
                        </tr>
						<tr>
						<td align="left" style="width:90px"></td>
						<td align="left" style="width:7px"><s:textfield cssClass="input-textUpper" name="serviceOrder.shipNumber" cssStyle="width:220px;" maxlength="15" readonly="true"  tabindex=""/> </td>
						<td align="left" style="width:20px"></td>
						<td><s:select cssClass="list-menu" name="serviceOrder.status"  list="%{JOB_STATUS}" onchange="autoPopulate_customerFile_statusDate(this),displayStatusReason(this); changeStatus();"   cssStyle="width:188px"  tabindex=""/></td>
						    						<c:if test="${not empty serviceOrder.statusDate}">
	    						<%--
	    						<fmt:formatDate var="statusDate" value="${serviceOrder.statusDate}" pattern="${displayDateFormat}"/>
	    						 --%>
	    						<s:text id="statusDate" name="${FormDateValue}"><s:param name="value" value="serviceOrder.statusDate" /></s:text>
	    						
								<td align="left" style="width:15px"></td>
								<td><s:textfield cssClass="input-text" name="serviceOrder.statusDate" value="%{statusDate}" cssStyle="width:68px" maxlength="11" readonly="true" /></td>
							</c:if>
							<c:if test="${empty serviceOrder.statusDate}">
							<td align="left" style="width:15px"></td>
							<td><s:textfield cssClass="input-text" name="serviceOrder.statusDate" cssStyle="width:65px" maxlength="11" readonly="true" tabindex=""/></td>
							</c:if>
							<td align="left" class="listwhitetext" style="width:3px"></td>
							<td id="reasonHid1" ><s:select cssClass="list-menu" key="serviceOrder.statusReason" list="%{statusReason}" onchange="changeStatus();" cssStyle="width:150px"  tabindex=""/></td>
                                 
                        </tr>
					</tbody>
				</table>
				</td>
				</tr>
		<tr>
				<td >
				<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
							<tbody>
							<tr><td height="5px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:90px"></td>
									<td align="left" class="listwhitetext">Origin&nbsp;&nbsp;City/State</td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext" style="">Country</td>
									<td align="left" class="listwhitetext" valign="bottom"></td>
									
									<td align="left" class="listwhitetext">Destination&nbsp;City/State</td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext" style="">Country</td>

								</tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:90px"></td>
									<td align="left" class="listwhitetext">
									<s:textfield cssClass="input-textUpper upper-case" name="serviceOrder.originCityCode" cssStyle="width:220px;" maxlength="30" readonly="true" tabindex=""/></td>
									<td width="20px"></td>
									<td align="left" class="listwhitetext">
									<s:textfield cssClass="input-textUpper" name="serviceOrder.originCountryCode" cssStyle="width:45px" maxlength="30" readonly="true" tabindex=""/></td>
									<td align="right" class="listwhitetext" style="width:18px"></td>
									<td align="left" class="listwhitetext" >
									<s:textfield	cssClass="input-textUpper upper-case" key="serviceOrder.destinationCityCode" cssStyle="width:206px;" maxlength="30" readonly="true" tabindex=""/></td>
									<td width="9px"></td>
									<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" name="serviceOrder.destinationCountryCode" cssStyle="width:45px" maxlength="30" readonly="true" tabindex=""/></td>
									
									<c:if test="${not empty serviceOrder.id}">
							 		<c:if test="${jobTypes !='UVL' && jobTypes !='MVL' && jobTypes !='ULL' && jobTypes !='MLL' && jobTypes !='DOM' && jobTypes !='LOC' && serviceOrder.originCountryCode != serviceOrder.destinationCountryCode}">
									<td width="120" align="right"><a><img src="images/viewcustom2.jpg" onclick="findCustomInfo(this);" alt="Customs" title="Customs" /></a></td>
						     		</c:if>
						    		</c:if>
						    		
								</tr>
							</tbody>
						</table>
			</td>
									
			</tr>
			<tr>

				<td>
				<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" >
							<tbody>
							<tr><td height="5px"></td></tr>
							<tr>
							<td style="width:90px; !width: 90px"></td>
							<td align="left" class="listwhitetext">Code</td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:10px">Name</td>
							<td></td>
							<c:if  test="${compDivFlag == 'Yes'}">
							<td align="left" class="listwhitetext" style="width:10px">Company&nbsp;Division<font color="red" size="2">*</font></td>
							</c:if>
							</tr>
						    <tr>
							<td align="right" class="listwhitetext" style="width: 90px">Booking&nbsp;Agent:&nbsp;</td>
							<td><s:textfield cssClass="input-textUpper" id="quotesbookingAgentCodeId" name="serviceOrder.bookingAgentCode" cssStyle="width:140px" maxlength="8" onchange="findBookingAgentName();findCompanyDivisionByBookAg();" onblur="findCompanyDivisionByBookAg();" tabindex=""/></td>
							<td style="width:22px;"><img class="openpopup" width="17" height="20" onclick="openBookingAgentPopWindow(); document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].focus();"	src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left"><s:textfield cssClass="input-text" id="quotesBookingAgentNameId" name="serviceOrder.bookingAgentName" onkeyup="findPartnerDetails('quotesBookingAgentNameId','quotesbookingAgentCodeId','quotesBookingAgentNameDivId',' and (isAccount=true or isAgent=true or isVendor=true )','',event);" onchange="findPartnerDetailsByName('quotesbookingAgentCodeId','quotesBookingAgentNameId')"  cssStyle="width:245px;" maxlength="250" tabindex=""/>
							<div id="quotesBookingAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
							<c:if  test="${compDivFlag == 'Yes'}">
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="left">
							<c:if test="${not empty serviceOrder.id}">
							<s:select cssClass="list-menu" id="companyDivision" name="serviceOrder.companyDivision" list="%{companyDivis}"  tabindex="" cssStyle="width:150px" onchange="getJobList();getContract();changeStatus();" headerKey="" headerValue=""  />
							</c:if>
							<c:if test="${empty serviceOrder.id}">
							<s:select cssClass="list-menu" id="companyDivision" name="serviceOrder.companyDivision" list="%{companyDivis}"  tabindex="" cssStyle="width:150px" onchange="getJobList();getContract();populateInclusionExclusionData('default','1','F');changeStatus();" headerKey="" headerValue=""  />
							</c:if>
							</td>
							</c:if>
							<c:if  test="${compDivFlag != 'Yes'}">
							<s:hidden name="serviceOrder.companyDivision"/>
							</c:if>	
				</table>

			</td>
			</tr>
			<tr>
			<td>
			<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="">
			<tr><td height="5px"></td></tr>
				<tbody> 
					<tr>
						<td align="right" class="listwhitetext" style="width:90px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.registrationNumber'/></td>
							<td align="left" class="listwhitetext" width="17px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.job'/>
							<c:if test="${costElementFlag}">
							<c:if test="${not empty accountLineList}">
							<font color="red" size="2">*</font>
							</c:if></c:if></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.quoteStatus',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='serviceOrder.quoteStatus'/></a></td>
							<td align="left" class="listwhitetext" width="17px"></td>
							<configByCorp:fieldVisibility componentId="component.field.quoteAcceptReason.showUTSI">
							<td id="reasonHid3" align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.quoteAcceptReason'/></td>
					</configByCorp:fieldVisibility>
					</tr>
					<tr>
							<td align="left" class="listwhitetext" style="width:90px"></td>
							<td align="left" style=""><s:textfield cssClass="input-text" name="serviceOrder.registrationNumber" cssStyle="width:140px" maxlength="20" required="true" tabindex="" /></td>
							<td align="left" class="listwhitetext" style="width:17px"></td>
							<td align="left" style="">
							<c:if test="${not empty serviceOrder.id}">
							<s:select id="jobServiceQuote" cssClass="list-menu" key="serviceOrder.job" list="%{job}" cssStyle="width:250px" headerKey="" headerValue="" onchange="getContract();findService();changeStatus();getCommodity();getServiceTypeByJob();fillCommodity();checkGroupAgeJobType();"   onclick="fillCheckJob(this)" tabindex=""/>
							</c:if>
							<c:if test="${empty serviceOrder.id}">
							<s:select id="jobServiceQuote" cssClass="list-menu" key="serviceOrder.job" list="%{job}" cssStyle="width:250px" headerKey="" headerValue="" onchange="populateInclusionExclusionData('default','1','F');getContract();findService();changeStatus();getCommodity();getServiceTypeByJob();fillCommodity();checkGroupAgeJobType();"   onclick="fillCheckJob(this)" tabindex=""/>
							</c:if>
							</td>
			        	    <td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="left" style=""><s:select cssClass="list-menu" key="serviceOrder.quoteStatus"  list="%{QUOTESTATUS}" cssStyle="width:150px" onchange="checkQuotation();changeStatus();quoteAcceptReason(this);"  tabindex=""/></td>
							<td align="right" class="listwhitetext" style="width:5px"></td>
							<configByCorp:fieldVisibility componentId="component.field.quoteAcceptReason.showUTSI">
							<td id="reasonHid4" > <s:select cssClass="list-menu" name="serviceOrder.quoteAcceptReason" list="%{quoteAcceptReason}" onchange="changeStatus();" cssStyle="width:150px"  tabindex=""/></td>
							</configByCorp:fieldVisibility>	
								
                   </tr>
    				<tr><td height="5px"></td></tr>						
                   <tr>
					        <td align="left" class="listwhitetext" style="width:90px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.estimator'/></td>
							<td align="left" class="listwhitetext" style="width:17px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><div id="labelservice"><fmt:message key='serviceOrder.serviceType' /></div></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><div id="routingLabel"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.routing',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='serviceOrder.routing'/>
							<c:if test="${costElementFlag}">
								<c:if test="${not empty accountLineList}">
									<font color="red" size="2">*</font>
								</c:if>
							</c:if>
							</a></div></td>
							<td align="right" class="listwhitetext" style="width:5px"></td>
									  </tr>
  				<tr>
							<td align="left" class="listwhitetext" style="width:90px"></td>
    						<td align="left" style=""><s:select cssClass="list-menu" name="serviceOrder.estimator" list="%{sale}" id="estimator" cssStyle="width:145px" headerKey="" headerValue="" onchange="changeStatus();"  tabindex=""/></td>
							<td align="left" class="listwhitetext" style="width:17px"></td>
    						<td align="left" style=""><div id="Service1"><s:select cssClass="list-menu" name="serviceOrder.serviceType"  list="%{service}" cssStyle="width:250px" onchange="changeStatus();"  tabindex=""/></div></td>
							<td align="left" class="listwhitetext" style="width:15px"></td>
	    					<td align="left" style=""><div id="routingFild">
	    					<c:if test="${not empty serviceOrder.id}">
	    					<s:select cssClass="list-menu" key="serviceOrder.routing"  list="%{routing}" cssStyle="width:150px" onchange="changeStatus();" tabindex="" />
	    					</c:if>
	    					<c:if test="${empty serviceOrder.id}">
	    					<s:select cssClass="list-menu" key="serviceOrder.routing"  list="%{routing}" cssStyle="width:150px" onchange="populateInclusionExclusionData('default','1','F');changeStatus();" tabindex="" />
	    					</c:if>
	    					</div></td>
	    	   				<td align="right" class="listwhitetext" style="width:5px"></td>
	    	   </tr>
	    		<tr><td height="5px"></td></tr>	
						<tr>
							<td width="85px"></td>
							<td align="left" colspan="15">
							<table id="Service2" style="margin:0px;padding:0px;  width:820px; cellpadding="0" cellspacing="0">
							<tr>							
							<td class="listwhitetext" align="left">Relocation Services</td>
							</tr>
							<tr>							
							<td valign="top">
							<fieldset style="margin:0px 0 0 0; padding:2px 0 0 2px; width:100%">                      
                        									<table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0">
                        									<tr>
                          										<c:forEach var="entry" items="${reloService}" varStatus="rowCounter">
                          										<c:if test="${entry.value!=''}">    
                          										<td class="listwhitetext" style="width:650px"><input type="checkbox" style="vertical-align:middle;" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex=""/>${entry.value} &nbsp;</td>
                          										</c:if>
                          										<c:choose>
														          <c:when test="${rowCounter.count % 4 == 0 && rowCounter.count != 0}">
														          </tr><tr>
														          </c:when>
														         <c:when test="${rowCounter.count != -1}">
														          </c:when>
															          <c:otherwise>
															          </c:otherwise>
															        </c:choose>
                          										</c:forEach>
    														   </tr>             										
                          									</table> 
                          	</fieldset>
							</td>
							</tr>
							</table>
																	
                          	</td>
						</tr>	    						
	    	   <tr>
	    	   
							<td align="left" class="listwhitetext" style="width:90px"></td>
							<td align="left" class="listwhitetext" valign="bottom"></td>
							<td align="left" class="listwhitetext" style="width:17px"></td>
					
							<td  align="left" class="listwhitetext" valign="bottom"><div id="labCommudity" ><fmt:message key='serviceOrder.commodity'/></div></td>
							
							<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><div id="modeLabel"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.mode',this);return false" onmouseout="ajax_hideTooltip()">
							<fmt:message key='serviceOrder.mode'/></a></div></td>
							<td align="left" class="listwhitetext" style="width:5px"></td>
							
							
			  </tr>
	    								
			<tr>		<td align="left" class="listwhitetext" style="width:90px"></td>
    						<td></td>
							<td align="left" class="listwhitetext" style="width:17px"></td>
					<c:choose>
							<c:when test="${serviceOrder.job =='STO' || serviceOrder.job =='STL' ||serviceOrder.job =='STF' ||serviceOrder.job =='TPS'}">
							<td align="left" style="">
							<c:if test="${not empty serviceOrder.id}">
							<s:select cssClass="list-menu" name="serviceOrder.commodity"   list="%{commodits}" cssStyle="width:250px" headerKey="" headerValue="" onchange="changeStatus();"  tabindex=""/>
							</c:if>
							<c:if test="${empty serviceOrder.id}">
							<s:select cssClass="list-menu" name="serviceOrder.commodity"   list="%{commodits}" cssStyle="width:250px" headerKey="" headerValue="" onchange="populateInclusionExclusionData('default','1','F');changeStatus();"  tabindex=""/>
							</c:if>
							</td>
					</c:when>
					<c:otherwise>
				
				   		<td align="left" style=""><div id="controlCommudity" >
				   		<c:if test="${not empty serviceOrder.id}">
				   		<s:select cssClass="list-menu" name="serviceOrder.commodity"   list="%{commodit}" cssStyle="width:250px" headerKey="" headerValue=""    onchange="validAutoBoat(); changeStatus();"  tabindex=""/>
				   		</c:if>
				   		<c:if test="${empty serviceOrder.id}">
				   		<s:select cssClass="list-menu" name="serviceOrder.commodity"   list="%{commodit}" cssStyle="width:250px" headerKey="" headerValue=""    onchange="populateInclusionExclusionData('default','1','F');validAutoBoat(); changeStatus();"  tabindex=""/>
				   		</c:if>
				   		</div></td>
							</c:otherwise>
					</c:choose>
							<td align="left" class="listwhitetext" style="width:15px"></td>
    						<td align="left" style=""><div id="modeFild">
    						<c:if test="${not empty serviceOrder.id}">
    						<s:select cssClass="list-menu" key="serviceOrder.mode" list="%{mode}" cssStyle="width:150px" onchange="validateDistanceUnit();changeStatus();getCarrier();fillUsaFlag();"   tabindex=""/>
    						</c:if>
    						<c:if test="${empty serviceOrder.id}">
    						<s:select cssClass="list-menu" key="serviceOrder.mode" list="%{mode}" cssStyle="width:150px" onchange="populateInclusionExclusionData('default','1','F');validateDistanceUnit();changeStatus();getCarrier();fillUsaFlag();"   tabindex=""/>
    						</c:if>
    						</div></td>
    						<td align="right" class="listwhitetext" style="width:5px"></td>    						
						<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
						<td align="right" class="listwhitetext" style="margin-top:4px;width:40px;" id="carrierFildLabel">
          <fmt:message key='trackingStatus.flagCarrier'/>&nbsp;</td>        
          <td align="left" colspan="2"><s:select cssClass="list-menu" id="carrierFild"  list="%{flagCarrierList}" name="serviceOrder.serviceOrderFlagCarrier"  headerKey="" headerValue="" cssStyle="width:105px"  tabindex=""/></td>
   			</configByCorp:fieldVisibility>								
    				</tr>
    				<tr><td height="5px"></td></tr>
  					 <tr>
		                    <td align="left" class="listwhitetext" style="width:90px"></td>
	                    	<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.coordinator'/></td>
								<td align="left" class="listwhitetext" style="width:15px"></td>
							<td align="left" class="listwhitetext" valign="bottom"><div id="packLabel"> <fmt:message key='serviceOrder.packingMode'/></div>
          					</td>
													
							<td align="left" class="listwhitetext" style="width:5px"></td>
							<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.packingService">
							<td align="left" class="listwhitetext" valign="bottom">Packing&nbsp;Service</td>
							</configByCorp:fieldVisibility>
							<td align="left" class="listwhitetext" style="width:5px"></td>
      						
                         </tr>
    						
		                  <tr>
							<td align="left" class="listwhitetext" style="width:90px"></td>
    						<td><s:select cssClass="list-menu" name="serviceOrder.coordinator" list="%{coordinatorList}" id="coordinator" cssStyle="width:145px" headerKey="" headerValue="" onchange="changeStatus();"  tabindex=""/></td>
							
							<%-- 
							<td align="left" style="width:100px">
							
							<s:select cssClass="list-menu" name="serviceOrder.specificParameter" list="%{specific}" cssStyle="width:250px" onchange="changeStatus();"  />
    						
    						</td>
    						--%>
    						<td align="left" class="listwhitetext" style="width:21px"></td>
    						<td align="left" style=""><div id="packFild">
    						<c:if test="${not empty serviceOrder.id}">
    						 <s:select cssClass="list-menu" name="serviceOrder.packingMode"  list="%{pkmode}" cssStyle="width:250px" onchange="changeStatus();"  tabindex=""/>
    						 </c:if>
    						 <c:if test="${empty serviceOrder.id}">
    						 <s:select cssClass="list-menu" name="serviceOrder.packingMode"  list="%{pkmode}" cssStyle="width:250px" onchange="populateInclusionExclusionData('default','1','F');changeStatus();"  tabindex=""/>
    						 </c:if>
    						 </div></td>							
							<td align="left" class="listwhitetext" style="width:15px"></td>
    						<td align="left" style="">
    						<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.packingService">
    						<s:select cssClass="list-menu" name="serviceOrder.packingService"  list="%{packingServiceList}" cssStyle="width:150px"  tabindex="" />
    						</configByCorp:fieldVisibility>
    						</td>
							<td width=""></td>
							<c:if test="${empty serviceOrder.id}">
							<td  colspan="4" align="right" width="220px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty serviceOrder.id}">
							<c:choose>
								<c:when test="${countServiceOrderNotes == '0'}">
								<td  colspan="4" align="right" width="220px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=ServiceOrder&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=ServiceOrder&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td  colspan="4" align="right" width="220px"><img src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=ServiceOrder&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=agentQuotesSO&subType=ServiceOrder&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>				
	</tr>
	<tr><td height="5px"></td></tr>
	<tr><td align="left" class="listwhitetext" style="width:90px"></td>
	<td align="left" class="listwhitetext" colspan="2"><div style="margin:0px;float:left;"><fmt:message key='customerFile.contract'/></div>
	<c:if test="${checkContractChargesMandatory=='1'}"><div id="contractMandatory"  style="margin:0px;"><font color="red" size="2">*</font></div></c:if>
	</td>
	</tr>
	<tr>
	<td align="left" class="listwhitetext" ></td>
	<td colspan="3"><s:select id="contract" cssClass="list-menu" name="serviceOrder.contract" list="%{contracts}" cssStyle="width:145px"  headerKey="" headerValue="" onchange="changeStatus();findActualizationDateOfContract();" tabindex=""/></td>
	</tr>
	<tr><td height="5px"></td></tr>
	</tbody>
</table>
</td>
</tr>
<%-- <c:if test="${serviceOrder.job == 'OFF'}">
	<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
	<tr>
		<td height="10" width="100%" align="left" style="margin:0px">
			<div onClick="javascript:animatedcollapse.toggle('resources');loadResources();" style="margin:0px">
				<table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
					<tr>
						<td class="headtab_left"></td>
						<td NOWRAP class="headtab_center">&nbsp;&nbsp;Resources</td>
						<td width="28" valign="top" class="headtab_bg"></td>
						<td class="headtab_bg_center">&nbsp;</td>
						<td class="headtab_right"></td>
					</tr>
				</table>
			</div>
			<div id="resources" class="switchgroup1">
				<table cellpadding="0" cellspacing="0"  class="detailTabLabel" border="0" style="margin:0px;padding:0px;margin-left:25px;" >
					<tr>
					  	<td>
				 		  	<input type="button" class="cssbutton1" onclick="getDayTemplate();" value="Resource Template" style="width:150px; height:25px; margin:5px 0px " tabindex=""/>
				 		</td>
				 		<td align="left" width="5px"></td>
				 		<td id="createPriceButton">
							<input type="button" class="cssbutton1" onclick="createPrice()" value="Create Pricing" style="width:120px; height:25px;  margin:5px 0px;" tabindex=""/>
						</td>
					</tr> 	
			 	</table>
			 	<div id="resourcMapAjax">
			 	
			 	</div>
			</div>
		</td>
	</tr>
</configByCorp:fieldVisibility>
</c:if> --%>
<tr  id="weightVolSection">
	                     <td height="10" align="left" class="listwhitetext">
	                     <c:if test="${serviceOrder.job !='RLO'}">
	                     <div  onClick="javascript:animatedcollapse.toggle('weight')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Weights & Volume
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
						<div  id="weight">
						
					  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0px;">
					  <tr>
					  <td>
					  <div class="subcontenttabChild"  style="cursor: pointer;margin: 0px;width:100%;">
					
						<c:if test="${weightType == 'lbscft'}">			
						&nbsp;&nbsp;(Pounds & Cft)&nbsp;&nbsp;<INPUT type="radio" name="weightType" id="weightnVolumeRadio" checked="checked" value="lbscft" onclick="check(this);changeStatus();" tabindex=""></div>
						
						</c:if>
						<c:if test="${weightType != 'lbscft'}">			
						&nbsp;&nbsp;(Pounds & Cft)&nbsp;&nbsp;<INPUT type="radio" name="weightType" id="weightnVolumeRadio" value="lbscft" onclick="check(this);changeStatus();" tabindex="">
						</div>
						</c:if>
<table class="detailTabLabel" border="0">
  <tbody><tr><td height="5px"></td></tr>
    	<tr>
	    	<td></td>
						<td colspan="1" align="left" class="listwhitetext"><fmt:message key='labels.entitled'/></td>
						<td colspan="1" align="left" class="listwhitetext"><fmt:message key='labels.estimated'/></td>
    					<td colspan="1" align="left" class="listwhitetext"><fmt:message key='labels.actual1'/></td>
    					<td align="center" class="listwhitetext"><fmt:message key='labels.chargeableWeight'/></td>
    					<td colspan="1" align="left" class="listwhitetext"><fmt:message key='labels.rewgh/fnl$'/></td>
   				 </tr>
    				<tr>
						<td align="right" class="listwhitetext" width="85px"><fmt:message key='labels.grossweight'/></td>
    					<td align="left" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleGrossWeight" size="6" maxlength="10" required="true"  onchange="onlyFloat(this);calcNetWeight1();"  tabindex="130" /></td>
    					<td align="left" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateGrossWeight" size="6" maxlength="10" required="true"  onchange="onlyFloat(this);calcNetWeight2();checkPricePointDetails('lbscft');" tabindex="135" /></td>
    					<td align="left" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualGrossWeight" size="6" maxlength="10" required="true"  onchange="onlyFloat(this);findWeight(),calcNetWeight3();"  tabindex="140" /></td>
    					<td align="left" ><s:textfield cssClass="input-text"  cssStyle="text-align:right" name="miscellaneous.chargeableGrossWeight" size="6" maxlength="10" required="true"   onchange="onlyFloat(this);calcNetWeight5();" tabindex="145" /></td>
						<td align="left" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghGross" size="6" required="true" maxlength="10"  onchange="onlyFloat(this);calcNetWeight4();" tabindex="150" /></td>
						<td width="60px" align="right" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.equipment',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='serviceOrder.equipment'/></a></td>
    					<td><s:select cssClass="list-menu" name="miscellaneous.equipment" list="%{EQUIP}" cssStyle="width:100px" headerKey="" headerValue=" " onchange="changeStatus();" tabindex="" /></td>
    					<td width="65px" align="right" class="listwhitetext">Transit&nbsp;Days</td>
    					<td width="70px"><s:textfield cssClass="input-text" name="serviceOrder.transitDays" maxlength="25" cssStyle="width:50px" tabindex=""/>
						<td width="48px" align="right" class="listwhitetext">Groupage</td>
                     	<td width="40px" colspan="2"><s:checkbox id="serviceOrder.grpPref" name="serviceOrder.grpPref" onchange="" tabindex="" /></td>
  				        <td class="listwhitetext" align="left" colspan="0">Excess Weight Billing
								<configByCorp:customDropDown 	listType="map" list="${excessWeightBillingList}" fieldValue="${miscellaneous.excessWeightBilling}" attribute="id='serviceOrderForm_miscellaneous_excessWeightBilling' class=list-menu name='miscellaneous.excessWeightBilling'  style='width:75px;height:19px;' onchange='' tabindex='' headerKey='' headerValue='' "/>
						 </td>
  				 </tr>
   				<tr>

                        <td align="right" class="listwhitetext"><fmt:message key='labels.tareweight'/></td>
                        <td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleTareWeight" size="6" maxlength="8" required="true" onchange="onlyFloat(this);calcNetWeight1();"  tabindex="131" /></td>                    
                        <td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateTareWeight" size="6" maxlength="8" required="true"  onchange="onlyFloat(this);calcNetWeight2();" tabindex="136" /></td>
                        <td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualTareWeight" size="6" maxlength="8" required="true"  onchange="onlyFloat(this);findTareWeight(),calcNetWeight3();" tabindex="141"  /></td>
                        <td align="left" ><s:textfield cssClass="input-text" cssStyle="text-align:right"  name="miscellaneous.chargeableTareWeight" size="6" maxlength="8" required="true"  onchange="onlyFloat(this);calcNetWeight5();" tabindex="146" /></td>
                        <td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghTare" size="6" required="true" maxlength="8"  onchange="onlyFloat(this);calcNetWeight4();" tabindex="151" /></td>
  				</tr>


  				<tr>
						<td align="right" class="listwhitetext" width="85px"><fmt:message key='labels.netweight'/></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleNetWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetWeightLbs1();"  tabindex="132" /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimatedNetWeight" size="6" maxlength="10" required="true" onchange="onlyFloat(this);calcNetWeightLbs2();" tabindex="137"  /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right"  name="miscellaneous.actualNetWeight" size="6" maxlength="10" required="true"   onchange="onlyFloat(this);findNetWeight();calcNetWeightLbs3();" tabindex="142" /></td>
    					<td align="left" ><s:textfield cssClass="input-text" cssStyle="text-align:right"  name="miscellaneous.chargeableNetWeight" size="6" maxlength="10" onchange="onlyFloat(this);calcNetWeightLbs4();" required="true"  tabindex="147" /></td>
						<td align="right" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNet" size="6" maxlength="10" onchange="onlyFloat(this);calcNetWeightLbs5();" required="true" tabindex="152"  /></td>
						
					<td colspan="4"></td>
							<c:if test="${empty serviceOrder.id}">
							<td width="210px" align="right" colspan="5"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty serviceOrder.id}">
							<c:choose>
								<c:when test="${countWeightDetailNotes == '0' || countWeightDetailNotes == '' || countWeightDetailNotes == null}">
								<td width="210px" align="right" colspan="5"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=agentQuotesSO&subType=WeightDetail&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=agentQuotesSO&subType=WeightDetail&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td width="210px" align="right" colspan="5"><img src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=agentQuotesSO&subType=WeightDetail&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.sequenceNumber }&noteFor=agentQuotesSO&subType=WeightDetail&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if> </tr>
</tbody>
</table>

 <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%">
   <tbody>
   	<tr><tr><td align="center" colspan="10" class="vertlinedata"></td></tr></tr>
 	  </tbody>
 		</table>
 <table class="detailTabLabel" border="0">
   <tbody>
	 <tr>
						<td align="right" class="listwhitetext" width="85px"><fmt:message key='labels.volume1'/></td>
						<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleCubicFeet" size="6" maxlength="6" required="true" onchange="onlyFloat(this);calcGrossVolume1();"  tabindex="133" /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateCubicFeet" size="6" maxlength="6" required="true" onchange="onlyFloat(this);calcGrossVolume2();checkPricePointDetails('lbscft');" tabindex="138" /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualCubicFeet" size="6" maxlength="6" onchange="onlyFloat(this);findVolumeWeight();calcGrossVolume3();" required="true"  tabindex="143" /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableCubicFeet" size="6" maxlength="6" required="true" onchange="onlyFloat(this);calcGrossVolume4();" tabindex="148" /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghCubicFeet" size="6" maxlength="6" required="true" onchange="onlyFloat(this);calcGrossVolume5();" tabindex="153" /></td>
    					
    </tr>
    <tr>
    					<td align="right" class="listwhitetext"><fmt:message key='labels.volume2'/></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEntitleCubicFeet" size="6" maxlength="6" onchange="onlyFloat(this);calcNetVolume1();" required="true"  tabindex="134" /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEstimateCubicFeet" size="6" maxlength="6" onchange="onlyFloat(this);calcNetVolume2();" required="true" tabindex="139" /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netActualCubicFeet" size="6" maxlength="6" onchange="onlyFloat(this);calcNetVolume3();" required="true" tabindex="144"  /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetCubicFeet" size="6" maxlength="6" onchange="onlyFloat(this);calcNetVolume4();" required="true" tabindex="149"  /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNetCubicFeet" size="6" maxlength="6" onchange="onlyFloat(this);calcNetVolume5();" required="true" tabindex="154" /></td>
    					
    </tr>
    <tr><td align="left" height="5px"></td></tr>
</tbody>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0px;">
						<tr>
						<td >
						
						<div class="subcontenttabChild" style="cursor: pointer;margin: 0px; width:100%;">
						
						<c:if test="${weightType == 'kgscbm'}">
 &nbsp;&nbsp;(Metric Units)&nbsp;&nbsp; <INPUT type="radio" name="weightType" id="weightnVolumeRadio2" value="kgscbm" checked="checked" onclick="check(this);changeStatus();" tabindex=""></div>
                        </c:if>
						<c:if test="${weightType != 'kgscbm'}">
 &nbsp;&nbsp;(Metric Units)&nbsp;&nbsp; <INPUT type="radio" name="weightType" id="weightnVolumeRadio2" value="kgscbm"  onclick="check(this);changeStatus();" tabindex=""></div>
                        </c:if>
                        
                        <table class="detailTabLabel" border="0">
							<tbody>
							<tr><td height="5px"></td></tr>
								<tr>
									<td></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.entitled' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.estimated' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.actual1' /></td>
									<td align="right" class="listwhitetext"><fmt:message
										key='labels.chargeableWeight' /></td>
									<td colspan="1" align="right" class="listwhitetext"><fmt:message
										key='labels.rewgh/fnl$' /></td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" width="85px"><fmt:message
										key='labels.grossweight' /></td>
									<td align="left" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleGrossWeightKilo" size="6"
										maxlength="10" required="true"
										 onchange="onlyFloat(this);calcNetWeightKgs1(); " tabindex="155" /></td>
										
												<td align="left" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.estimateGrossWeightKilo" size="6"
											maxlength="10" required="true"
											
											onchange="onlyFloat(this);calcNetWeightKgs2();checkPricePointDetails('kgscbm');" tabindex="163" /></td>
									

																		
										<td align="left" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.actualGrossWeightKilo" size="6"
											maxlength="10" required="true"
										
											onchange="onlyFloat(this);calcNetWeightKgs3();" tabindex="171" /></td>
								
									<td align="left"><s:textfield cssClass="input-text"
										cssStyle="text-align:right"
										name="miscellaneous.chargeableGrossWeightKilo" size="6"
										maxlength="10" required="true"
									
										onchange="onlyFloat(this);calcNetWeightKgs5();" tabindex="179" /></td>
									<td align="left"><s:textfield cssClass="input-text"
										cssStyle="text-align:right" name="miscellaneous.rwghGrossKilo"
										size="6" required="true" maxlength="10"
										
										onchange="onlyFloat(this);calcNetWeightKgs4();" tabindex="187" /></td>
									
								</tr>
								<tr>

									<td align="right" class="listwhitetext"><fmt:message
										key='labels.tareweight' /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleTareWeightKilo" size="6" maxlength="8"
										required="true" 
										 onchange="onlyFloat(this);calcNetWeightKgs1();"  tabindex="156"/></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.estimateTareWeightKilo" size="6" maxlength="8"
										required="true" 
										onchange="onlyFloat(this);calcNetWeightKgs2();" tabindex="164" /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.actualTareWeightKilo" size="6" maxlength="8"
										required="true" 
										onchange="onlyFloat(this);calcNetWeightKgs3();"  tabindex="172" /></td>
									<td align="left"><s:textfield cssClass="input-text"
										cssStyle="text-align:right"
										name="miscellaneous.chargeableTareWeightKilo" size="6"
										maxlength="8" required="true"
										
										onchange="onlyFloat(this);calcNetWeightKgs5();" tabindex="180" /></td>
									<td align="right"><s:textfield cssClass="input-text"
										cssStyle="text-align:right" name="miscellaneous.rwghTareKilo"
										size="6" required="true" maxlength="8"
										
										onchange="onlyFloat(this);calcNetWeightKgs4();" tabindex="188" /></td>
								</tr>


								<tr>
									<td align="right" class="listwhitetext" width=""><fmt:message
										key='labels.netweight' /></td>
									<td align="right" colspan="1"><s:textfield
										cssClass="input-text" cssStyle="text-align:right"
										name="miscellaneous.entitleNetWeightKilo" size="6" maxlength="10"
										required="true"
										onchange="onlyFloat(this);calcNetWeightOnlyKgs1();"  tabindex="157" /></td>
									
										<td align="right" colspan="1"><s:textfield
											cssClass="input-text" cssStyle="text-align:right"
											name="miscellaneous.estimatedNetWeightKilo" size="6"
											maxlength="10" required="true"
											 onchange="onlyFloat(this);calcNetWeightOnlyKgs2();" tabindex="165" /></td>
									
										<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualNetWeightKilo" onchange="onlyFloat(this);calcNetWeightOnlyKgs3();findNetWeightKilo();" size="6" maxlength="10" required="true" 
											tabindex="173"  /></td>
									
									<td align="left"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetWeightKilo" onchange="onlyFloat(this);calcNetWeightOnlyKgs4();" size="6" maxlength="10" required="true"  tabindex="181" /></td>
									<td align="right"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNetKilo" onchange="onlyFloat(this);calcNetWeightOnlyKgs5();" size="6" maxlength="10" required="true" tabindex="189" /></td>
									
								</tr>
							</tbody>
						</table>

						<table class="detailTabLabel" border="0" width="100%">
							<tbody>								
									<tr><td align="center" colspan="10" class="vertlinedata"></td></tr>
							</tbody>
						</table>
						<table class="detailTabLabel" border="0">
							<tbody>
								<tr>
									<td align="right" class="listwhitetext" width="85px"><fmt:message key='labels.volume1' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr1();" size="6" maxlength="10" required="true" tabindex="158" /></td>
									
										<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr2();checkPricePointDetails('kgscbm');" size="6" maxlength="10" required="true"  tabindex="166" /></td>
								
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr3();" size="6" maxlength="10" required="true" tabindex="174" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr4();" size="6" maxlength="10" required="true"  tabindex="182" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right"name="miscellaneous.rwghCubicMtr" onchange="onlyFloat(this); calcGrossVolumeMtr5();" size="6" maxlength="10" required="true"  tabindex="190" /></td>

								</tr>
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key='labels.volume2' /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEntitleCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr1();" size="6" maxlength="10" required="true" tabindex="159" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netEstimateCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr2();" size="6" maxlength="10" required="true" tabindex="167" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.netActualCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr3();" size="6" maxlength="10" required="true" tabindex="175"  /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.chargeableNetCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr4();" size="6" maxlength="10" required="true" tabindex="183" /></td>
									<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.rwghNetCubicMtr" onchange="onlyFloat(this); calcNetVolumeMtr5();" size="6" maxlength="10" required="true" 
									tabindex="191"	/></td>
									
								</tr>
								<tr>
									<td align="left" height="5px"></td>
								</tr>
							</tbody>
						</table>
						
						
						
						</td>
						</tr>
						</table>
  <%--                     <c:if test="${serviceOrder.commodity =='HHG/A' || serviceOrder.commodity =='AUTO' ||serviceOrder.commodity =='BOAT'}">
 
 --%> 
 <div id="hid" >
 <table class="detailTabLabel" border="0" width="100%">
 	  <tbody>
 	  	<tr><tr><td align="center" colspan="10" class="vertlinedata"></td></tr></tr>
     </tbody>
</table>

<table class="detailTabLabel" border="0">
<tbody>
<tr>

						<td align="right" class="listwhitetext"><fmt:message key='labels.autoboat'/></td>
						<td align="right" colspan="1" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleNumberAuto" size="6" maxlength="2" required="true" onkeydown="return onlyNumsAllowed(event)"  tabindex="160" /></td>
					<!--<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.entitleBoatYN" size="1" maxlength="2" required="true" onkeydown="return onlyNumsAllowed(event)"/></td> -->
     					<td align="right" colspan="1" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimateAuto" size="6" maxlength="2" required="true" onkeydown="return onlyNumsAllowed(event)" tabindex="168" /></td>
    				<!--<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.estimateBoat" size="1" maxlength="2" required="true" onkeydown="return onlyNumsAllowed(event)"/></td> -->
    					<td align="right" colspan="1" ><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualAuto" size="6" maxlength="2" required="true" onkeydown="return onlyNumsAllowed(event)"  tabindex="174" /></td>
    				<!--  		<td align="right" ><s:textfield cssClass="input-text" name="miscellaneous.actualBoat" size="1" maxlength="2" required="true" onkeydown="return onlyNumsAllowed(event)" /></td> -->
										
				<td></td>
</tr>

<tr>
						<td align="right" class="listwhitetext" width="85px"><fmt:message key='labels.vehicleweight'/></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.entitleAutoWeight" size="6" maxlength="10"  onkeydown="return onlyFloatNumsAllowed(event)"  tabindex="161" /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.estimatedAutoWeight" size="6" maxlength="10"  onkeydown="return onlyFloatNumsAllowed(event)"  tabindex="169" /></td>
    					<td align="right" colspan="1"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="miscellaneous.actualAutoWeight" size="6" maxlength="10"  onkeydown="return onlyFloatNumsAllowed(event)" tabindex="175" /></td>
    					<td></td>
			</tr>
											
</tbody>
</table>
</div>

 </td>
 </tr></table></div>
 </c:if>
 </td>
 </tr>
 <jsp:include flush="true" page="quotationServiceOrderSecForm.jsp"></jsp:include>
 <%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}"> 
	<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
  		<jsp:include flush="true" page="resourceJavaScript.jsp"></jsp:include>
  	</configByCorp:fieldVisibility>
  </c:if>
  
   <jsp:include page="/WEB-INF/pages/trans/pricingJavaScript.jsp" />

<table width="">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
						    <fmt:formatDate var="serviceCreatedOnFormattedValue" value="${serviceOrder.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<td align="right" class="listwhitetext" style="width:50px"><b><fmt:message key='serviceOrder.createdOn'/></b></td>
							<s:hidden name="serviceOrder.createdOn"  value="${serviceCreatedOnFormattedValue}"/>
							<td style="width:130px; font-size:.90em"><fmt:formatDate value="${serviceOrder.createdOn}" pattern="${displayDateTimeFormat}"/>
							<%-- 
							<s:date name="serviceOrder.createdOn" format="dd-MMM-yyyy HH:mm"/></td>		
							--%>
							<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='serviceOrder.createdBy' /></b></td>
							<c:if test="${not empty serviceOrder.id}">
								<s:hidden name="serviceOrder.createdBy"/>
								<td style="font-size:.90em"><s:label name="createdBy" value="%{serviceOrder.createdBy}"/></td>
							</c:if>
							<c:if test="${empty serviceOrder.id}">
								<s:hidden name="serviceOrder.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="font-size:.90em"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='serviceOrder.updatedOn'/></b></td>
							<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${serviceOrder.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="serviceOrder.updatedOn" value="${serviceUpdatedOnFormattedValue}"/>
							<td style="width:130px;font-size:.90em"><fmt:formatDate value="${serviceOrder.updatedOn}" pattern="${displayDateTimeFormat}"/>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='serviceOrder.updatedBy' /></b></td>
							<c:if test="${not empty serviceOrder.id}">
							<s:hidden name="serviceOrder.updatedBy"/>
							<td style="width:85px;font-size:.90em"><s:label name="updatedBy" value="%{serviceOrder.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty serviceOrder.id}">
							<s:hidden name="serviceOrder.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px;font-size:.90em"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
			
			<input type="button" id="pricingBtnId" name="pricingBtnId" onclick="validatePricing('Save');" class="cssbuttonA" style="width:55px; height:25px" value="Save"  tabindex=""/>
			<div id="submitBtn" style="display: none;float:left;margin-right:4px;">
		  <%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
                <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}"> 
					<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
						<s:submit id="submitButtonIdOFFMOVE" name="submitButtonIdOFFMOVE" cssClass="cssbuttonA" cssStyle="width:55px; height:25px" method="save" key="button.save"  onclick="" tabindex=""/> 
					</configByCorp:fieldVisibility>
				</c:if>
			
				<s:submit id="submitButtonId" name="submitButtonId"  method="save" key="button.save"  onclick="return forwardToMyMessage1('Save');" cssClass="cssbuttonA" cssStyle="width:55px; height:25px;" tabindex=""/> 
 			</div>
		
	<c:if test="${not empty serviceOrder.id}">
		<input type="button" class="cssbutton1" value="Add" style="width:55px; height:25px" onclick="addServiceOrder(this)"/>   
	</c:if> 
        <input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onmousemove="myDate();" onclick="resetPricing();setVipImageReset();changeReset();getJobList();formReset();populateInclusionExclusionData('noDefault','1','T');checkColumn();newFunctionForCountryState();getCarrier();" tabindex=""  />
        <c:if test="${not empty serviceOrder.id}">
			<input type="button" class="cssbutton1" value="Copy Quotes without Pricing" style="width:180px; height:25px" onclick="addCopyServiceOrder();" tabindex=""/>
		</c:if>


<c:if test="${quotesToValidate!='QTG'}">
        <c:if test="${not empty serviceOrder.id}">
		<input type="button" class="cssbutton1" value="Copy Quotes with Pricing" style="width:170px; height:25px" onclick="addCopyPricingServiceOrder();" tabindex=""/>
	</c:if>
	</c:if>
	
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
         <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
        <c:if test="${not empty serviceOrder.id}">
            <input type="button" class="cssbutton1" value="Copy Quotes with Resources" style="width:170px; height:25px" onclick="CopyQuotesWithResources();" tabindex=""/>
        </c:if>
        </sec-auth:authComponent>
        </c:if>
	
	
	
	<s:hidden name="customerFile.sequenceNumber" />
	<s:hidden name="serviceOrder.sequenceNumber" />
	<s:hidden name="serviceOrder.statusNumber" />
	<%-- <s:hidden name="serviceOrder.contract"/> --%>
	<s:hidden name="serviceOrder.ship" />
	<s:hidden name="miscellaneous.id" />
	<s:hidden name="miscellaneous.shipNumber" />
	<s:hidden name="serviceOrder.billToCode" />       
	<s:hidden name="serviceOrder.billToName" />
	<s:hidden name="serviceOrder.orderBy" value="%{customerFile.orderBy}" />
	<s:hidden name="serviceOrder.orderPhone" value="%{customerFile.orderPhone}" />
	<s:hidden name="serviceOrder.payType" value="%{customerFile.billPayMethod}" />
	<s:hidden name="serviceOrder.actualNetWeight" value="%{miscellaneous.actualNetWeight}"/>
	<s:hidden name="serviceOrder.actualGrossWeight" value="%{miscellaneous.actualGrossWeight}"/>
	<s:hidden name="serviceOrder.actualCubicFeet" value="%{miscellaneous.actualCubicFeet}"/>
	<s:hidden name="serviceOrder.estimateCubicFeet" value="%{miscellaneous.estimateCubicFeet}"/>
	<s:hidden name="serviceOrder.estimatedNetWeight" value="%{miscellaneous.estimatedNetWeight}"/>
	<s:hidden name="serviceOrder.estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}"/>
	<s:hidden name="miscellaneous.corpID"/>
	<s:hidden name="miscellaneous.sequenceNumber"/>
	<s:hidden name="miscellaneous.ship"/>
	<s:hidden name="firstDescription" />
	<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="firstDescription" /> 
	<s:hidden name="fifthDescription" />
	<s:hidden name="sixthDescription" />
	<s:hidden name="seventhDescription" /> 
	<s:hidden name="eigthDescription" />
    <s:hidden name="ninthDescription" />
    <s:hidden name="tenthDescription"/>
	<s:hidden name="miscellaneous.pieceNumber"/>
	<s:hidden name="miscellaneous.inventoryStickerNumber"/>
	<s:hidden name="miscellaneous.descriptionStorage"/>
	<s:hidden name="miscellaneous.listLotNumber"/>
	<s:hidden name="miscellaneous.locationStorageLot"/>
	<s:hidden name="miscellaneous.estimateGood"/>
	<s:hidden name="miscellaneous.estimateStorageGood"/>
	<s:hidden name="miscellaneous.actualStorage"/>
	<s:hidden name="miscellaneous.entitleSIT"/>
	<s:hidden name="miscellaneous.estimateSIT"/>
	<s:hidden name="miscellaneous.actualSIT"/>
	<s:hidden name="miscellaneous.bookerSelfPacking"/>
	<s:hidden name="miscellaneous.bookerSelfHauling"/>
	<s:hidden name="miscellaneous.bookerOwnAuthority"/>
	<s:hidden name="miscellaneous.packAuthorize"/>
	<s:hidden name="miscellaneous.packFullPartial"/>
	<s:hidden name="miscellaneous.unPack"/>
	<s:hidden name="miscellaneous.packingBulky"/>
	<s:hidden name="miscellaneous.packingBulkyDescription"/>
	<s:hidden name="miscellaneous.codeHauling"/>
	<s:hidden name="miscellaneous.tarif"/>
	<s:hidden name="miscellaneous.section"/>
	<s:hidden name="miscellaneous.spaceReserved"/>
	<s:hidden name="miscellaneous.applicationNumber3rdParty"/>
	<s:hidden name="miscellaneous.applicationDescription3rdParty"/>
	<s:hidden name="miscellaneous.whatAllOnOrigin"/>
	<s:hidden name="miscellaneous.whatOtherDescription"/>
	<s:hidden name="miscellaneous.insuranceValue"/>
	<s:hidden name="miscellaneous.insuranceValueDescription"/>
	<s:hidden name="miscellaneous.insuranceOption"/>
	<s:hidden name="miscellaneous.insuranceYN"/>
	<s:hidden name="miscellaneous.needWaivesYN"/>
	<s:hidden name="miscellaneous.needWaiveEstimate"/>
	<s:hidden name="miscellaneous.needWaiveSurvey"/>
	<s:hidden name="miscellaneous.needWaivePeakRate"/>
	<s:hidden name="miscellaneous.needWaiveSRA"/>
	<s:hidden name="miscellaneous.needWaiveSIT"/>
	<s:hidden name="miscellaneous.needWaiveSITOADAYN"/>
	<s:hidden name="miscellaneous.authorizeSITDays"/>
	<s:hidden name="miscellaneous.needWaive619"/>
	<s:hidden name="miscellaneous.vanLineOrderNumber"/>
	<s:hidden name="miscellaneous.vanLineContractNumber"/>
	<s:hidden name="miscellaneous.vanLineNationalCode"/>
	<s:hidden name="miscellaneous.originCountyCode"/>
	<s:hidden name="miscellaneous.originStateCode"/>
	<s:hidden name="miscellaneous.originCounty"/>
	<s:hidden name="miscellaneous.originState"/>
	<s:hidden name="miscellaneous.destinationCountyCode"/>
	<s:hidden name="miscellaneous.destinationStateCode"/>
	<s:hidden name="miscellaneous.destinationCounty"/>
	<s:hidden name="miscellaneous.destinationState"/>
	<s:hidden name="miscellaneous.extraStopYN"/>
	<s:hidden name="miscellaneous.mile"/>
	<s:hidden name="miscellaneous.rateSIT"/>
	<s:hidden name="miscellaneous.ratePSTG"/>
	<s:hidden name="miscellaneous.rateInsurance"/>
	<s:hidden name="miscellaneous.entitleAmountDoller"/>
	<s:hidden name="miscellaneous.actualInsuranceAmount"/>
	<s:hidden name="miscellaneous.vanLineNumber"/>
	<s:hidden name="miscellaneous.estimatedRevenue"/>
	<s:hidden name="miscellaneous.noStorageInOut"/>
	<s:hidden name="miscellaneous.packMaterialYN"/>
	<s:hidden name="miscellaneous.warehouse"/>
	<s:hidden name="miscellaneous.overflow"/>
	<s:hidden name="miscellaneous.tag"/>
	<s:hidden name="miscellaneous.onHandWeight"/>
	<s:hidden name="miscellaneous.test"/>
	<s:hidden name="miscellaneous.entitleConsignment"/>
	<s:hidden name="miscellaneous.estimateConsignment"/>
	<s:hidden name="miscellaneous.actualConsignment"/>
	<s:hidden name="miscellaneous.entitleHouseHoldGoodPound"/>
	<s:hidden name="miscellaneous.estimateHouseHoldGoodPound"/>
	<s:hidden name="miscellaneous.actualHouseHoldGood"/>
	<s:hidden name="miscellaneous.entitleHouseHoldGoodKilo"/>
	<s:hidden name="miscellaneous.estimateHouseHoldGoodKilo"/>
	<s:hidden name="miscellaneous.actualHouseHoldGoodKilo"/>
	<s:hidden name="miscellaneous.estimateConsignmentKilo"/>
	<s:hidden name="miscellaneous.entitleConsignmentKilo"/>
	<s:hidden name="miscellaneous.actualConsignmentKilo"/>
	<s:hidden name="miscellaneous.actualRevenueDollor"/>
	<s:hidden name="miscellaneous.agentPerformNonperform"/>
	<s:hidden name="miscellaneous.destination24HR"/>
	<s:hidden name="miscellaneous.haulingStatus"/>
	<s:hidden name="miscellaneous.storageInOut"/>
	<s:hidden name="miscellaneous.insuranceCarrier"/>
	<s:hidden name="miscellaneous.insurancePolicy"/>
	<s:hidden name="miscellaneous.insuranceDate"/>
	<s:hidden name="miscellaneous.storageInOutDate"/>
	<s:hidden name="miscellaneous.storageInOutWeight"/>
	<s:hidden name="miscellaneous.origin24Hr"/>
	<s:hidden name="miscellaneous.vanLinePickupNumber"/>
	<s:hidden name="miscellaneous.vanLineRemark1"/>
	<s:hidden name="miscellaneous.vanLineRemark2"/>
	<s:hidden name="miscellaneous.vanLineRemark3"/>
	<s:hidden name="miscellaneous.vanLineRemark4"/>
	<s:hidden name="miscellaneous.vanLineRemark5"/>
	<s:hidden name="miscellaneous.selfHauling"/>
	<s:hidden name="miscellaneous.specialInstructionCode"/>
	<s:hidden name="miscellaneous.carrier" />
	<s:hidden name="miscellaneous.fromNonTempStorage" />
	<s:hidden name="miscellaneous.weaponsIncluded" />
	<s:hidden name="miscellaneous.storageUnit"/>
		
		
	<s:hidden name="salesCommisionRate" value="${salesCommisionRate}"/>
	<s:hidden name="grossMarginThreshold" value="${grossMarginThreshold}"/>
	<s:hidden name="inEstimatedTotalExpense" value="${serviceOrder.estimatedTotalExpense}"/>
	<s:hidden name="inEstimatedTotalRevenue" value="${serviceOrder.estimatedTotalRevenue}"/>
	<s:hidden name="inRevisedTotalExpense" value="${serviceOrder.revisedTotalExpense}"/>
	<s:hidden name="inRevisedTotalRevenue" value="${serviceOrder.revisedTotalRevenue}"/>
	<s:hidden name="inActualExpense" value="${serviceOrder.actualExpense}"/>
	<s:hidden name="inActualRevenue" value="${serviceOrder.actualRevenue}"/>
	<s:hidden name="inComptetive" value="${customerFile.comptetive}"/>
		
	<configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
	<s:hidden name="setDescriptionChargeCode" value="YES"/>
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
	<s:hidden name="setDefaultDescriptionChargeCode" value="YES"/>
	</configByCorp:fieldVisibility>
		
		
		 <c:if test="${not empty miscellaneous.atlasDown}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.atlasDown" /></s:text>
			 <s:hidden  name="miscellaneous.atlasDown" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.atlasDown}">
		 <s:hidden   name="miscellaneous.atlasDown"/> 
	 </c:if>
	 		<c:if test="${not empty miscellaneous.shipmentRegistrationDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.shipmentRegistrationDate" /></s:text>
			 <s:hidden  name="miscellaneous.shipmentRegistrationDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.shipmentRegistrationDate}">
		 <s:hidden   name="miscellaneous.shipmentRegistrationDate"/> 
	 </c:if>
		
		<c:if test="${not empty miscellaneous.transDocument}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.transDocument" /></s:text>
			 <s:hidden  name="miscellaneous.transDocument" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.transDocument}">
		 <s:hidden   name="miscellaneous.transDocument"/> 
	 </c:if>
		<s:hidden name="miscellaneous.entitleBoatYN"/>
		<s:hidden name="miscellaneous.estimateBoat"/>
		<s:hidden name="miscellaneous.actualBoat"/>
		<s:hidden name="miscellaneous.remarkSA"/>
		<s:hidden name="miscellaneous.remarkSB"/>
		<s:hidden name="miscellaneous.remarkSC"/>
		<s:hidden name="miscellaneous.trailerVolumeWeight"/>
		<s:hidden name="miscellaneous.gate"/>
		<s:hidden name="miscellaneous.trailer"/>
		<s:hidden name="miscellaneous.pendingNumber"/>
		<s:hidden name="miscellaneous.settledVanLineAmount"/>
		<s:hidden name="miscellaneous.vanLineCODAmount"/>
		<s:hidden name="miscellaneous.prepaid"/>
		<s:hidden name="miscellaneous.weightTicket"/>
		<s:hidden name="miscellaneous.domesticInstruction"/>
		<s:hidden name="miscellaneous.peakRate"/>
		<s:hidden name="miscellaneous.createdBy"/>
		<s:hidden name="miscellaneous.createdOn"/>
		<s:hidden name="miscellaneous.updatedBy"/>
		<s:hidden name="miscellaneous.updatedOn"/>
		
		<s:hidden name="miscellaneous.haulingAgentCode"/> 
		<s:hidden name="miscellaneous.haulerName"/> 
		<s:hidden name="miscellaneous.driverId"/> 
		<s:hidden name="miscellaneous.driverName"/> 
		<s:hidden name="miscellaneous.driverCell"/> 
	
		<s:hidden name="customerFile.id"  />
		<s:hidden name="serviceOrder.corpID"/>
		<s:hidden name="serviceOrder.revisedTotalExpense"/>
		<s:hidden name="serviceOrder.revisedTotalRevenue"/>
		<s:hidden name="serviceOrder.revisedGrossMargin"/>
		<s:hidden name="serviceOrder.revisedGrossMarginPercentage"/>
		<s:hidden name="serviceOrder.entitledTotalAmount"/>
		<s:hidden name="serviceOrder.estimatedTotalExpense"/>
		<s:hidden name="serviceOrder.estimatedTotalRevenue"/>
		<s:hidden name="serviceOrder.estimatedGrossMargin"/>
		<s:hidden name="serviceOrder.estimatedGrossMarginPercentage"/>
		<s:hidden name="serviceOrder.estimatedPassPercentageTotal"/> 
		<s:hidden name="serviceOrder.quoteAccept"/> 
		<s:hidden name="serviceOrder.actualExpense" />
		<s:hidden name="serviceOrder.actualRevenue" />
		<s:hidden name="serviceOrder.actualGrossMargin" />
		<s:hidden name="serviceOrder.actualGrossMarginPercentage" />
		<s:hidden name="serviceOrder.projectedGrossMarginPercentage" />
		<s:hidden name="serviceOrder.projectedGrossMargin" />
		
		
		
		
			<s:hidden name="miscellaneous.paperWork" />
								 <c:if test="${not empty miscellaneous.recivedPO}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.recivedPO" /></s:text>
			 <s:hidden  name="miscellaneous.recivedPO" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.recivedPO}">
		 <s:hidden   name="miscellaneous.recivedPO"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.recivedHVI}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.recivedHVI" /></s:text>
			 <s:hidden  name="miscellaneous.recivedHVI" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.recivedHVI}">
		 <s:hidden   name="miscellaneous.recivedHVI"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.confirmOriginAgent}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.confirmOriginAgent" /></s:text>
			 <s:hidden  name="miscellaneous.confirmOriginAgent" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.confirmOriginAgent}">
		 <s:hidden   name="miscellaneous.confirmOriginAgent"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.dd1840}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.dd1840" /></s:text>
			 <s:hidden  name="miscellaneous.dd1840" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.dd1840}">
		 <s:hidden   name="miscellaneous.dd1840"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.quoteOn}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.quoteOn" /></s:text>
			 <s:hidden  name="miscellaneous.quoteOn" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.quoteOn}">
		 <s:hidden   name="miscellaneous.quoteOn"/> 
	 </c:if>
	  <c:if test="${not empty miscellaneous.recivedPayment}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.recivedPayment" /></s:text>
			 <s:hidden  name="miscellaneous.recivedPayment" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.recivedPayment}">
		 <s:hidden   name="miscellaneous.recivedPayment"/> 
	 </c:if>
		
		<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
		<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
		<c:set var="noteFor" value="agentQuotesSO" scope="session"/>
		<c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
		<c:set var="tableName" value="serviceorder" scope="session"/>
				<c:if test="${empty serviceOrder.id}">
			<c:set var="isTrue" value="false" scope="request"/>
		</c:if>
		<c:if test="${not empty serviceOrder.id}">
			<c:set var="isTrue" value="true" scope="request"/>
		</c:if>


</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<c:if test="${pricingButton=='yes'}"> 
<c:redirect url="/editQuotationServiceOrderUpdate.html?id=${serviceOrder.id}"></c:redirect>
</c:if>
</s:form>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
<%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">
<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
		animatedcollapse.addDiv('resources', 'fade=0,persist=0,hide=1')
	</configByCorp:fieldVisibility>
</c:if>
<%--	
	var tempDay='';
	<c:forEach items="${resourceMap1}" var="outerMap">
		animatedcollapse.addDiv('Day${outerMap.key}', 'fade=0,persist=0,hide=1')
		tempDay='${outerMap.key}';
		<c:forEach items="${outerMap.value}" var="innerMap" varStatus="loop">
			animatedcollapse.addDiv('${innerMap.key}${outerMap.key}', 'fade=0,persist=0,hide=1')
		</c:forEach>
	</c:forEach>
	var foc='${dayFocus}'
	var daytempFocus = '${day}'
	var categoryType = '${categoryType}'

	if(((daytempFocus!=null)&&(daytempFocus.trim()!=''))){
		animatedcollapse.addDiv('Day'+daytempFocus, 'fade=0,persist=0,show=1')
	}else if((foc!=null)&&(foc.trim()!='')){
		animatedcollapse.addDiv(foc, 'fade=0,persist=0,show=1')
	}else if(tempDay != ''){
		animatedcollapse.addDiv('Day'+tempDay, 'fade=0,persist=0,show=1')
	}
	if(((categoryType!=null)&&(categoryType.trim()!=''))){
		animatedcollapse.addDiv(categoryType, 'fade=0,persist=0,show=1')
	}
--%>
animatedcollapse.addDiv('weight', 'fade=0,persist=1,hide=1')

<c:if test="${quotesToValidate=='QTG'}"> 
animatedcollapse.addDiv('estpricing', 'fade=0,persist=1,hide=1')  
</c:if>

animatedcollapse.addDiv('address', 'fade=0,persist=1,hide=1')
<%-- <c:if test="${not empty serviceOrder.id}">
	<c:if test="${quotesToValidate!='QTG'}">
		animatedcollapse.addDiv('pricing', 'fade=0,persist=1,hide=1')
	</c:if>
</c:if>
<c:if test="${empty serviceOrder.id}">
	<c:if test="${quotesToValidate!='QTG'}"> 
		animatedcollapse.addDiv('pricing', 'fade=0,persist=0,hide=1')
	</c:if>
	animatedcollapse.addDiv('pricing', 'fade=0,persist=0,hide=1')
</c:if> --%>
var incExcype = document.forms['serviceOrderForm'].elements['userQuoteServices'].value;
	animatedcollapse.addDiv('pricing1', 'fade=0,persist=0,hide=1')	
	if(incExcype!=undefined && incExcype !=null && incExcype != '' && (incExcype.trim()=='Single section' || incExcype.trim()=='Separate section')){
	animatedcollapse.addDiv('incexc', 'fade=0,persist=0,show=1')
	}
 animatedcollapse.init()
</script>

<script type="text/JavaScript">
function formReset(){
	document.getElementById('serviceOrderForm').reset();
	<c:if test="${empty serviceOrder.id}">
	autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
	</c:if>
}
//setTimeout("getPrice()",100);
var count=0;
function getPrice(){
	<c:forEach var="accLine" items="${accountLineList}" varStatus="loopStatus">
		var accLineId = '<c:out value="${accLine.id}" />';
		var charge='';
		var chargCd = 'chargeCode<c:out value="${accLine.id}" />';
		charge = document.forms['serviceOrderForm'].elements[chargCd];
		if(charge != null || charge != undefined){
			charge = charge.value;
			if(charge.length > 0){
//				sleep(4000);
				var dt = new Date();
				dt.setTime(dt.getTime() + ms);
				while (new Date().getTime() < dt.getTime()){
//					findRevisedEstimateQuantitys('chargeCode${accLine.id}','category${accLine.id}','estimateExpense${accLine.id}','basis${accLine.id}','estimateQuantity${accLine.id}','estimateRate${accLine.id}','estimateSellRate${accLine.id}','estimateRevenueAmount${accLine.id}','estimatePassPercentage${accLine.id}','displayOnQuote${accLine.id}','deviation${accLine.id}','estimateDeviation${accLine.id}','estimateSellDeviation${accLine.id}');
				}
			}
		}
  	</c:forEach> 
}

</script>
<script type="text/javascript">
function findCustomerOtherSO(position) {
	 var sid=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
	 var soIdNum=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	 var url="quotationOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
	  ajax_showTooltip(url,position);	
	  } 
		
		  function goToUrl(id)
		{
			location.href = "editQuotationServiceOrderUpdate.html?id="+id;
		}
function goPrev() {
	//progressBarAutoSave('1');
	var soIdNum =document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceOrderForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   } 
 function goNext() {
	//progressBarAutoSave('1');
	var soIdNum =document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceOrderForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   } 
 function handleHttpResponseOtherShip(){
     if (http5.readyState == 4)
     {
  		 var results = http5.responseText
         results = results.trim();
		 var id1=results;	 
         findOtherServiceType(id1);
     }
}     
function findOtherServiceType(id1)      
{
  var soIdNum=id1;
  var url="findOtherServiceType.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum);
  http10.open("GET", url, true); 
  http10.onreadystatechange =function(){ handleHttpResponseOtherShipType(id1);}; 
  http10.send(null); 
}
function handleHttpResponseOtherShipType(id1){
     if (http10.readyState == 4)
     {
     			  var results = http10.responseText
	               results = results.trim();
				if(results=="")	{
				location.href = 'editQuotationServiceOrderUpdate.html?id='+id1;
				}
				else{
				location.href = 'editQuotationServiceOrderUpdate.html?id='+id1;
				} 
     } 
}	
var http10 = getHTTPObject();
</script>

<script type="text/javascript">
window.onload = function() {
	var netRecd = '${isNetworkRecord}';
	if(netRecd != null || netRecd != ''){
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].className = 'input-textUpper';
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].className = 'input-textUpper';
		
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].setAttribute('readonly','readonly');
		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].setAttribute('readonly','readonly');
		var el = document.getElementById('serviceOrder.bookingAgentCode.img');
		el.onclick = false;
	    document.images['serviceOrder.bookingAgentCode.img'].src = 'images/navarrow.gif';
	}
}
   /* var map;
    var gdir;
    var geocoder = null;
    var addressMarker;

    function initialize() {
    
    var terminalCity=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
	var terminalZip=document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
	var terminalState=document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
	
	var fromAddress = terminalCity+','+terminalState+' '+terminalZip;
	
	var destCity=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
	var destZip=document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
	var destState=document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
	
	var toAddress = destCity+','+destState+' '+destZip;
	
      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("map"));
        gdir = new GDirections(map, document.getElementById("directions"));
		
        GEvent.addListener(gdir, "load", onGDirectionsLoad);
        GEvent.addListener(gdir, "error", handleErrors);

        setDirections(fromAddress, toAddress, "en_US");
      }
    }
    
    function setDirections(fromAddress, toAddress, locale) {
      gdir.load("from: " + fromAddress + " to: " + toAddress,
                { "locale": locale });
    }

    function handleErrors(){
	   if (gdir.getStatus().code == G_GEO_UNKNOWN_ADDRESS)
	     alert("No corresponding geographic location could be found for one of the specified addresses. This may be due to the fact that the address is relatively new, or it may be incorrect.\nError code: " + gdir.getStatus().code);
	   else if (gdir.getStatus().code == G_GEO_SERVER_ERROR)
	     alert("A geocoding or directions request could not be successfully processed, yet the exact reason for the failure is not known.\n Error code: " + gdir.getStatus().code);
	   
	   else if (gdir.getStatus().code == G_GEO_MISSING_QUERY)
	     alert("The HTTP q parameter was either missing or had no value. For geocoder requests, this means that an empty address was specified as input. For directions requests, this means that no query was specified in the input.\n Error code: " + gdir.getStatus().code);
  
	   else if (gdir.getStatus().code == G_GEO_BAD_KEY)
	     alert("The given key is either invalid or does not match the domain for which it was given. \n Error code: " + gdir.getStatus().code);

	   else if (gdir.getStatus().code == G_GEO_BAD_REQUEST)
	     alert("A directions request could not be successfully parsed.\n Error code: " + gdir.getStatus().code);
	    
	   else alert("An unknown error occurred.");
	   
	}

	function onGDirectionsLoad()
	{ 
		var distanceInMile=gdir.getDistance().html;
		distanceInMile=distanceInMile.split('&nbsp;');
		document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value=distanceInMile[0].replace(',','');
	if(distanceInMile[1]=='mi'){
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].text = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].value = 'Mile';
		document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].options[2].selected=true;
		}
		//document.getElementById("getDistance").innerHTML = gdir.getDistance().html; 
	} */
	
  </script>
  
  
<script language="javascript" type="text/javascript">
		function change() {
		       if( document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "BOAT" || document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "AUTO" || document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value == "HHG/A") {
		        	document.forms['serviceOrderForm'].elements['miscellaneous.entitleNumberAuto'].disabled = false ;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateAuto'].disabled = false ;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualAuto'].disabled = false ;
			  }else{
			  	document.forms['serviceOrderForm'].elements['miscellaneous.entitleNumberAuto'].disabled = true ;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateAuto'].disabled = true ;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualAuto'].disabled = true ;
			  }
		}
</script>
<SCRIPT LANGUAGE="JavaScript">
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

 function findCityState1(targetElement,targetField){
	 var zipCode = targetElement.value;
	 
     if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
      } else if (targetField=='DZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
     }
     
     var url1="findCityStateFlex.html?ajax=1&decorator=simple&popup=true&countryCodeFlex="+ encodeURI(countryCode);
     http99.open("GET", url1, true);
     http99.onreadystatechange = function(){  handleHttpResponseCityStateFlex(targetElement,targetField);};
    
     http99.send(null);
     }
  
   function handleHttpResponseCityStateFlex(targetElement,targetField){
              if (http99.readyState == 4){
                var results = http99.responseText
                results = results.trim();
                
                if(results.length>0)
                    {
                  
                     if(results=='Y' ){
                     findCityState(targetElement,targetField);
                     }
                   if(results=='N'){                 
                    }
                    } 
                    else{
                    
                    } 
                                               
                   }
                   }
     
 function findCityState(targetElement,targetField){
  
	 var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
     }
     var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
       }
     
function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value;
     
       if(targetField=='OZ'){
       var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
        if(document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }else{
        
         }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
          if( document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             }else{
        
             }
      
        }  
      }   
function handleHttpResponseCityState(targetField){
                  if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                resu = resu.split(",");
				for(var i = 0; i < resu.length+1; i++) {
                var res = resu[i];
                res = res.split("#");
                if(targetField=='OZ'){
	           	if(res[3]=='P') {	            
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value=res[0];
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value=res[2];
	            if (document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value=='') {
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originHomePhone'].value=(res[4].substring(0,19));
	             } 
	              document.getElementById('zipCodeList').style.display = 'none';
	             } else if(res[3]!='P') {
	             document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value=res[0];
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value=res[2];
	            if (document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value=='') {
	           	 document.forms['serviceOrderForm'].elements['serviceOrder.destinationHomePhone'].value=(res[4].substring(0,19));
	           }
	            document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(res[3]!='P') {
	           	document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].focus();
	           	} } } else { }
             }
 function findCityStateNotPrimary(targetField, position){
     if(targetField=='OZ'){
     var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
	 var zipCode = document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
	 } else if (targetField=='DZ'){
	 var countryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
	 var zipCode = document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
	 }
	 var url="findCityStateNotPrimary.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode)+"&zipType="+encodeURI(targetField)+"&countryForZipCode="+encodeURI(countryCode);
      ajax_showTooltip(url,position);	
 }            
             
   function goToUrlZip(id,targetField){
if(targetField=='OZ'){
	document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value = id;
	 } else if (targetField=='DZ'){
	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value = id;
	} }          
  
	   var http33 = getHTTPObject();
      var http99 = getHTTPObject();

    var http2 = getHTTPObject();
     var http6 = getHTTPObject();
     var http54 = getHTTPObject54();
    
	function getHTTPObject54()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
	function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
	function getHTTPObject99()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_originCountry(targetElement) {
		var oriCountry = targetElement.value;
		var oriCountryCode = '';
		if(oriCountry == 'United States')
		{
			oriCountryCode = "USA";
		}
		if(oriCountry == 'India')
		{
			oriCountryCode = "IND";
		}
		if(oriCountry == 'Canada')
		{
			oriCountryCode = "CAN";
		}
		if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = false;
		    document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].focus();
		}else{
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = true;
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value = '';
			if(oriCountry == 'Netherlands'|| oriCountry == 'Austria' ||oriCountry == 'Belgium' ||oriCountry == 'Bulgaria' ||oriCountry == 'Croatia' ||oriCountry == 'Cyprus' ||oriCountry == 'Czech Republic' ||
			oriCountry == 'Denmark' ||oriCountry == 'Estonia' ||oriCountry == 'Finland' ||oriCountry == 'France' ||oriCountry == 'Germany' ||oriCountry == 'Greece' ||oriCountry == 'Hungary' ||
			oriCountry == 'Iceland' ||oriCountry == 'Ireland' ||oriCountry == 'Italy' ||oriCountry == 'Latvia' ||oriCountry == 'Lithuania' ||oriCountry == 'Luxembourg' ||oriCountry == 'Malta' ||
			oriCountry == 'Poland' ||oriCountry == 'Portugal' ||oriCountry == 'Romania' ||oriCountry == 'Slovakia' ||oriCountry == 'Slovenia' ||oriCountry == 'Spain' ||oriCountry == 'Sweden' ||
			oriCountry == 'Turkey' ||oriCountry == 'United Kingdom' ){
				document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].focus();
			}
			autoPopulate_customerFile_originCityCode(oriCountryCode,'special');
		}
	}
</script>


<script type="text/javascript">
	function autoPopulate_customerFile_destinationCountry(targetElement) {
		var dCountry = targetElement.value;
		var dCountryCode = '';
		if(dCountry == 'United States')
		{
			dCountryCode = "USA";
		}
		if(dCountry == 'India')
		{
			dCountryCode = "IND";
		}
		if(dCountry == 'Canada')
		{
			dCountryCode = "CAN";
		}
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = false;
		    //document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].focus();
		}else{
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = true;
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value = '';
			if(dCountry== 'Netherlands'|| dCountry== 'Austria' ||dCountry== 'Belgium' ||dCountry== 'Bulgaria' ||dCountry== 'Croatia' ||dCountry== 'Cyprus' ||dCountry== 'Czech Republic' ||
			dCountry== 'Denmark' ||dCountry== 'Estonia' ||dCountry== 'Finland' ||dCountry== 'France' ||dCountry== 'Germany' ||dCountry== 'Greece' ||dCountry== 'Hungary' ||
			dCountry== 'Iceland' ||dCountry== 'Ireland' ||dCountry== 'Italy' ||dCountry== 'Latvia' ||dCountry== 'Lithuania' ||dCountry== 'Luxembourg' ||dCountry== 'Malta' ||
			dCountry== 'Poland' ||dCountry== 'Portugal' ||dCountry== 'Romania' ||dCountry== 'Slovakia' ||dCountry== 'Slovenia' ||dCountry== 'Spain' ||dCountry== 'Sweden' ||
			dCountry== 'Turkey' ||dCountry== 'United Kingdom' ){
				//document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].focus();
			}
			autoPopulate_customerFile_originCityCode(dCountryCode,'special');
		}
	}
	
	function getOriginCountryCode(){
	var countryName=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
	}
	
	
	function getDestinationCountryCode(){
		var countryName=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
		var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
	    http4.open("GET", url, true);
	    http4.onreadystatechange = httpDestinationCountryName;
	    http4.send(null);
	}
        function httpDestinationCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['serviceOrderForm'].elements['DestinationCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['serviceOrderForm'].elements['DestinationCountryFlex3Value'].value='N';
 					}	
 					document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].select();
				}else{
                     
                 }
             }
        }
     function handleHttpResponseCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res=results.split('#');
                if(res.length>=1){
                	document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value = res[0]; 
                	if(res[1]=='Y'){
                	document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='Y';				
				 	}else{
				 	document.forms['serviceOrderForm'].elements['originCountryFlex3Value'].value='N';
				 	}
				 	document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].select();		     	 
 				}else{
                     
                 }
             }
}

	function getCommodity(){
    var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
    
    var url;
    if(jobType=='STO'|| jobType=='STF'|| jobType=='STL' || jobType=='TPS' ){
      url="getCommodity.html?ajax=1&decorator=simple&popup=true&commodityParameter=" + encodeURI('COMMODITS');
     }
     else
     {
       url="getCommodity.html?ajax=1&decorator=simple&popup=true&commodityParameter=" + encodeURI('COMMODIT')+"&jobType="+encodeURI(jobType);
     }
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse100;
     http2.send(null);
}

function handleHttpResponse100()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                res = results.replace("{",'');
                res = res.replace("}",'');
                res = res.split(",");
                targetElementValue=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'];
				targetElement.length = res.length;
				var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
 				for(i=0;i<res.length;i++)
					{
					  if(res[i] == ''){
					  document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].text = '';
					  document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].value = '';
					}
					else
					{
					
					   stateVal = res[i].replace("=",'+');
					   stateVal=stateVal.split("+");
					   document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].text=stateVal[1];
					   document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[i].value=stateVal[0].trim();
					   if(job == "")
					   {
					   	document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[0].selected=true;
					  	document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].options[0].selected=true;
					   }
					}
             
             }
            document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value=targetElementValue;
       }
   }  	
</script>

<SCRIPT LANGUAGE="JavaScript">
function findBookingAgentName(){
    var bookingAgentCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
    if(bookingAgentCode == ''){
    	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = ""
    	// document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value = ""
    }
    if(bookingAgentCode != ''){
    showOrHide(1);
    var url="QuotationBookingAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
    } 
}
function getState(targetElement) {
	var country = targetElement.value;	
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse9;
     http3.send(null);
	
	}

function getDestinationState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http22.open("GET", url, true);
     http22.onreadystatechange = handleHttpResponse8;
     http22.send(null);
	
	}

function getState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse7;
     http3.send(null);
	
	}
	
function getDestinationState1(targetElement) {
	var countryCode = targetElement.value;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    //alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse8;
     http2.send(null);
	
	}
function findWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse3;
     http2.send(null);
}
}
function findNetWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }else{
     var url="containerNetWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
     }
}
function findTareWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerTareWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse6;
     http2.send(null);
}
}
function findVolumeWeight(){
     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
     if(sid==''){
     }
     else{
     var url="containerVolumeWeightList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);
}
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null")
                {
                 var agree = confirm("Containers/Piece Counts data is available, Still Want to change");
                 if(agree){
                 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value = '${miscellaneous.actualNetWeight}';
                 }
 				}
                 
             }
        }     
function handleHttpResponse6()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null")
                {
                 var agree = confirm("Containers/Piece Counts data is available, Still Want to change");
                 if(agree){
                 
                 }else{
               
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value = '${miscellaneous.actualTareWeight}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value = '${miscellaneous.actualNetWeight}';
                 }
 				}
                 
             }
        }     
        function handleHttpResponse5()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null")
                {
                 var agree = confirm("Containers/Piece Counts data is available, Still Want to change");
                 if(agree){
                 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value = '${miscellaneous.actualCubicFeet}';
                 }
 				}
                 
             }
        }     
function handleHttpResponse3()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace("[","");
                results = results.replace("]","");
                if(results!="null")
                {
                 var agree = confirm("Containers/Piece Counts data is available, Still Want to change");
                 if(agree){
                 
                 }else{
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value = '${miscellaneous.actualGrossWeight}';
                 	document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value = '${miscellaneous.actualNetWeight}';
                 }
 				}
                 
             }
        }     

function handleHttpResponse4(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved' || res[2] == 'New'){
                		showOrHide(0);
	           			document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value = res[1];
	           			// document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value="";
                   		document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
                	}else{
                		showOrHide(0);
	           			alert("Booking Agent code is not approved" ); 
					    document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value="";
					 	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value="";
					 	// document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value="";
					 	document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
	           		}  
                }else{
                	showOrHide(0);
                     alert("Booking Agent code not valid");
                     document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentName'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value="";
					// document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value="";
					 document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();
               }
       }
} 
        
        function handleHttpResponse9()
        {

             if (http3.readyState == 4)
             {
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("originState").value = '${serviceOrder.originState}';
             }
        }  
        
function handleHttpResponse8()
        {

             if (http22.readyState == 4)
             {
                var results = http22.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("destinationState").value = '${serviceOrder.destinationState}';
             }
        }         

	var http22 = getHTTPObject();
    var http2 = getHTTPObject();
    var http4 = getHTTPObject();
    var http5 = getHTTPObject5();
    var http17 = getHTTPObject();
    
 function getHTTPObject5()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
    var http3 = getHTTPObject1();
</script>

<script language="javascript" type="text/javascript">
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	var tim1=document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value; 
	
	if(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value == "null" ){
		document.forms['serviceOrderForm'].elements['serviceOrder.statusDate'].value="";
		}
		
		
		if(document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value="";
	}
	if(document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value="";
	}
	if(document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value="";
	}
	if(document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value="";
	}
	if(document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value="";
	} 
	if(document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value == '0.0' ){
		document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value="";
	}
	var f = document.getElementById('serviceOrderForm'); 
		f.setAttribute("autocomplete", "off");		
	
	var partnerType=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;	
		var el = document.getElementById('hid');		
		if(partnerType == 'AUTO' || partnerType == 'HHG/A' || partnerType == 'BOAT'){
		el.style.visibility = 'visible';
		}else{
		el.style.visibility = 'collapse';
		}
		var distanceUnit=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
		var el = document.getElementById('hidDM');
		if(distanceUnit == 'Overland' || distanceUnit == 'Truck'){
		el.style.display = 'block';
		el.style.visibility = 'visible';		
		}else{
		el.style.display = 'collapse';
		}
	
}

function validateDistanceUnit()
{
      var distanceUnit=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
		var el = document.getElementById('hidDM');
		if(distanceUnit == 'Overland' || distanceUnit == 'Truck'){
			initLoader();
		el.style.display = 'block';
		el.style.visibility = 'visible';		
		}else{
		el.style.display = 'none';
		}
		}
function validAutoBoat()
{
		var partnerType=document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
	
		var el = document.getElementById('hid');
		//alert(document.getElementById('hid').height);
		if(partnerType == 'AUTO' || partnerType == 'HHG/A' || partnerType == 'BOAT'){
		el.style.display = 'block';
		el.style.visibility = 'visible';
		}else{
		el.style.display = 'none';
		}
		}
</script>
<script type="text/javascript">
function validate_email(field)
{
with (field)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert("Not a valid e-mail address!");return false}
else {return true} 
}
}

function calcNetWeight1(){
var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value;
var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value;
Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100

var E1='';
	if(Q1<Q2){
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
	}else if(Q1 != undefined && Q2 != undefined){
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		var Q3 = Q1*0.4536;
		var Q4=Math.round(Q3*10000)/10000;
		var Q5 = Q2*0.4536;
		var Q6=Math.round(Q5*10000)/10000;
		
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=Q4;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=Q6;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
	}else if(Q1 == undefined && Q2 == undefined){
		E1=0;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=0;
	}else if(Q2 == undefined){
		E1=Q1;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E2;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=E4;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E4;
	}else if(Q1 == undefined){
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
        document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
	}
}
function calcNetWeight2(){
var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;
var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value;
Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100
var E1='';
	if(Q1<Q2){
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
	}else if(Q1 != undefined && Q2 != undefined){
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
		var E3 = E2*0.4536;
		var E4=Math.round(E3*10000)/10000;
		var Q3 = Q1*0.4536;
		var Q4=Math.round(Q3*10000)/10000;
		var Q5 = Q2*0.4536;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=Q4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=Q6;
	}else if(Q1 == undefined && Q2 == undefined){
		E1=0;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
		var E3 = E2*0.4536;
        var E4=Math.round(E3*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=0;
	}else if(Q2 == undefined){
		E1=Q1;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E2;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
		var E3 = E2*0.4536;
        var E4=Math.round(E3*10000)/10000;
        document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=E4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E4;
	}else if(Q1 == undefined){
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
        document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
	}
}
function calcNetWeight3(){
			
		var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;
     	var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value;
     	Q1 = Math.round(Q1*100)/100
        Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E2;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=Q4;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=Q6;
			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E2;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
				var E3 = E2*0.4536;
		        var E4=Math.round(E3*10000)/10000;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
			}
}
function calcNetWeight4(){
	
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value;
		var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value;
		Q1 = Math.round(Q1*100)/100
		   Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){	
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=Q4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=Q6;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
			}
}
function calcNetWeight5(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value;
		var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value;
		Q1 = Math.round(Q1*100)/100
		   Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){	
			     E1=Q1-Q2;
				 var E2=Math.round(E1*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
				 var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*0.4536;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*0.4536;
				var Q6=Math.round(Q5*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=Q4;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=Q6;

			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
				var E3 = E2*0.4536;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
			}
	}
function calcGrossVolume1(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicMtr'].value=0;
			}
			
	}
function calcGrossVolume2(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value=0;
			}
			
	}
function calcGrossVolume3(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value=0;
			}
			
	}
function calcGrossVolume4(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicMtr'].value=0;
			}
			
	}
function calcGrossVolume5(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicMtr'].value=0;
			}
	}
function calcNetVolume1(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicMtr'].value=0;
			}
	}
function calcNetVolume2(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value=0;
			}
	}
function calcNetVolume3(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value=0;
			}
	}
function calcNetVolume4(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicMtr'].value=0;
			}
	}
function calcNetVolume5(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.0283;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicMtr'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicMtr'].value=0;
			}
	}
function calcNetVolumeMtr1(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netEntitleCubicFeet'].value=0;
			}
	}
function calcNetVolumeMtr2(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netEstimateCubicFeet'].value=0;
			}
	}
function calcNetVolumeMtr3(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.netActualCubicFeet'].value=0;
			}
	}
function calcNetVolumeMtr4(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetCubicFeet'].value=0;
			}
	}
function calcNetVolumeMtr5(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetCubicFeet'].value=0;
			}
	}
function calcGrossVolumeMtr1(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.entitleCubicFeet'].value=0;
			}
	}
function calcGrossVolumeMtr2(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value=0;
			}
	}
function calcGrossVolumeMtr3(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value=0;
			}
	}
function calcGrossVolumeMtr4(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableCubicFeet'].value=0;
			}
	}
function calcGrossVolumeMtr5(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicMtr'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*35.3147;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].value=E3;
			}
			else{
			 document.forms['serviceOrderForm'].elements['miscellaneous.rwghCubicFeet'].value =0;
			}
			
	}
function calcNetWeightKgs1(){
var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeightKilo'].value;
var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value;
Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100

var E1='';
	if(Q1<Q2){
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
	}else if(Q1 != undefined && Q2 != undefined){
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
		        var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=Q4;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=Q6;
		document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
	}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.entitleTareWeight'].value=0;
			}
}
function calcNetWeightKgs2(){
var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeightKilo'].value;
var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value;
Q1 = Math.round(Q1*100)/100
Q2 = Math.round(Q2*100)/100

var E1='';
	if(Q1<Q2){
		alert("Grossweight should be greater than Tareweight");
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
	}else if(Q1 != undefined && Q2 != undefined){
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
		        var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=Q4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
		document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=Q6;
	}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.estimateTareWeight'].value=0;
			}
}
function calcNetWeightKgs3(){
			
		var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeightKilo'].value;
     	var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value;
     	Q1 = Math.round(Q1*100)/100
        Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=Q4;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E4;
		        document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=Q6;
			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualTareWeight'].value=0;
			}
}

function calcNetWeightKgs4(){
	
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghGrossKilo'].value;
		var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value;
		Q1 = Math.round(Q1*100)/100
		   Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){	
				E1=Q1-Q2;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=Q4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=Q6;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E4;
			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
				var E3 = E2*2.2046;
				var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTareKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghGross'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghTare'].value=0;
			}
}
function calcNetWeightKgs5(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeightKilo'].value;
		var Q2 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value;
		Q1 = Math.round(Q1*100)/100
		   Q2 = Math.round(Q2*100)/100
		var E1='';
			if(Q1<Q2){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
			}else if(Q1 != undefined && Q2 != undefined){	
			     E1=Q1-Q2;
				 var E2=Math.round(E1*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
				 var E3 = E2*2.2046;
				 var E4=Math.round(E3*10000)/10000;
				var Q3 = Q1*2.2046;
				var Q4=Math.round(Q3*10000)/10000;
				var Q5 = Q2*2.2046;
				var Q6=Math.round(Q5*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=Q4;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=Q6;

			}else if(Q1 == undefined && Q2 == undefined){
				E1=0;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
				var E3 = E2*2.2046;
				 var E4=Math.round(E3*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=0;
			}else if(Q2 == undefined){
				E1=Q1;
				var E2=Math.round(E1*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E2;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
				var E3 = E2*2.2046;
				 var E4=Math.round(E3*10000)/10000;
				 document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=E4;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E4;
			}else if(Q1 == undefined){
				alert("Grossweight should be greater than Tareweight");
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeightKilo'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableGrossWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableTareWeight'].value=0;
			}
	}
function calcNetWeightLbs1(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value=0;
			}
	}

function calcNetWeightLbs2(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value=0;
			}
	}
	
	function calcNetWeightLbs3(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value=0;
			}
	}	
	function calcNetWeightLbs4(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value=0;
			}
	}	
	function calcNetWeightLbs5(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*0.4536;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value=0;
			}
	}
function calcNetWeightOnlyKgs1(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeightKilo'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.entitleNetWeight'].value=0;
			}
	}
function calcNetWeightOnlyKgs2(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeightKilo'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value=0;
			}
	}	
	function calcNetWeightOnlyKgs3(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeightKilo'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value=0;
			}
	}	
	function calcNetWeightOnlyKgs4(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeightKilo'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.chargeableNetWeight'].value=0;
			}
	}	
	function calcNetWeightOnlyKgs5(){
        var Q1 = document.forms['serviceOrderForm'].elements['miscellaneous.rwghNetKilo'].value;
        Q1 = Math.round(Q1*100)/100
		var E1='';
			if(Q1 != undefined)
			{
			    var E1=Q1;
			    var E2 = Q1*2.2046;
				var E3=Math.round(E2*10000)/10000;
				document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=E3;
			}
			else{
			document.forms['serviceOrderForm'].elements['miscellaneous.rwghNet'].value=0;
			}
	}
</script>
<script type="text/javascript">
function checkPricePointDetails(targetunit){
	var pricePointFlag='${pricePointFlag}';	
	//alert("target "+targetunit)
	 var storageUnit='${weightType}';
	// alert("storageUnit "+storageUnit)
	 var shipnumber='${serviceOrder.shipNumber}';
	 if(pricePointFlag=='true' || pricePointFlag==true){
	 if(targetunit==storageUnit){
		/// alert(shipnumber)
		 $.get("updatePricPointCheckAjax.html?ajax=1&decorator=simple&popup=true", 
					{shipNumber:shipnumber},
				function(data){	
						//alert(data)			
						data=data.replace('[','');
						data=data.replace(']','');
						//alert(data)	
				if(data.trim()!=''){
					alert("PricePoint quotes  subject to change due to modified weight / volume. \nPlease review and change if required.");
				}
				}); 
	 }
	 }
}
</script>
<script type="text/javascript">
var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";

var minDigitsInIPhoneNumber = 10;

function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   

        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }   
    return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
    {   
    
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function checkInternationalPhone(strPhone){
var s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}

function validatePhone(targetElelemnt){
	var Phone=targetElelemnt.value;
	
	if (checkInternationalPhone(Phone)==false){
		alert("Please Enter a Valid Phone Number")
		targetElelemnt.value="";
		return false;
	}
	return true;
 }
</script>
<script language="JavaScript">
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++) { 	       
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyNumsAllowed1(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	
</script>

<script type="text/javascript">
	function autoPopulate_serviceOrder_originCountry(targetElement) {
		var originCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value=originCountryCode.substring(0,originCountryCode.indexOf(":"));
		targetElement.form.elements['serviceOrder.originCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+1,originCountryCode.length);
	}
</script>
<script type="text/javascript">
	function autoPopulate_serviceOrder_destinationCountry(targetElement) {
		var destinationCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value=destinationCountryCode.substring(0,destinationCountryCode.indexOf(":"));
		targetElement.form.elements['serviceOrder.destinationCountry'].value=destinationCountryCode.substring(destinationCountryCode.indexOf(":")+1,destinationCountryCode.length);
	}
function copyCompanyToDestination(targetElement){
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationCompany'].value = targetElement.value;
	}
</script>

<script>
	function wrongValue(field, event) {
		var key, keyChar;
		if(window.event) {
			key = window.event.keyCode;
		}else if(event) {
			key = event.which;
		}
		else
			return true;
		
		keyChar = String.fromCharCode(key);
		
		if((key==null) || (key==0) || (key==8) ||(key== 9) || (key==13) || (key==27)) {
			window.status="";
			return true;
		}
		else {
		if((("01234567890").indexOf(keyChar)>-1)) {
		
			window.status="";
			return true;
		}else {
		
			window.status="It accepts only numeric value";
			alert("It accepts only numeric value");
			return false;
		}
		}
	}
</script>

<script type="text/javascript">
	function openOriginLocation() {
		var city = document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		var country = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
		var zip = document.forms['serviceOrderForm'].elements['serviceOrder.originZip'].value;
		var address1 = document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine1'].value;
		var address2 = document.forms['serviceOrderForm'].elements['serviceOrder.originAddressLine2'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+ address1+','+address2+ ',' +city+','+zip+','+country);
	}
	
	function openDestinationLocation() {
		var city = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
		var country = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
		var zip = document.forms['serviceOrderForm'].elements['serviceOrder.destinationZip'].value;
		var address1 = document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine1'].value;
		var address2 = document.forms['serviceOrderForm'].elements['serviceOrder.destinationAddressLine2'].value;
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+ ',' +city+','+zip+','+country);
	}
</script>

<script type="text/javascript"> 

function validatePricing(str){
	<c:if test="${checkContractChargesMandatory=='1'}">
	var contractStr = document.forms['serviceOrderForm'].elements['serviceOrder.contract'].value;
	if(contractStr==''){
		alert("Contract is a required field");
		return false;
	}
	</c:if>
	var obj = document.getElementById('accInnerTable');
	  if(obj == null || obj == undefined){
		  saveForm(str);
		  return true;
	  }else{
		  findAllPricingLineId(str);
		  //return false;
	  }
}

function saveForm(str){
	showOrHide(1);
	
	document.getElementById("pricingBtnId").style.display="none";
	document.getElementById("submitBtn").style.display="block";
	
	
	<c:if test="${not empty serviceOrder.id}">
	var obj = document.getElementById('accInnerTable');
	  if(obj != null && obj != undefined){
		  try{
		<%--	var totalExp = validateFieldValueForNumber('estimatedTotalExpense${serviceOrder.id}');
		  	var totalRev = validateFieldValueForNumber('estimatedTotalRevenue${serviceOrder.id}');
		  	var grossMargin = validateFieldValueForNumber('estimatedGrossMargin${serviceOrder.id}');
		  	var grossMarginPercent = validateFieldValueForNumber('estimatedGrossMarginPercentage${serviceOrder.id}');
		  
		 	var pricingFooterValue = totalExp+","+totalRev+","+grossMargin+","+grossMarginPercent;
		 --%>	
		 
			document.getElementById('pricingFooterValue').value = pricingFooterValue;
		  }catch(e){
			  
		  }
	  }
     </c:if> 	  	          
	
	
	var id = document.getElementById('submitButtonIdOFFMOVE');
	if(id == null || id == undefined){
		document.getElementById('submitButtonId').click();
	}else{
		document.getElementById('submitButtonIdOFFMOVE').click();
	}
	
	/* var check = forwardToMyMessage1(str);
	alert(check);
	if(check){
		showOrHide(1);
		 document.forms['serviceOrderForm'].action = 'saveQuotatonServiceOrder.html';  
	    document.forms['serviceOrderForm'].submit();
	} */
}

function forwardToMyMessage1(str){
	    document.forms['serviceOrderForm'].elements['serviceOrder.actualNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualNetWeight'].value;		
		document.forms['serviceOrderForm'].elements['serviceOrder.actualGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualGrossWeight'].value;		
		document.forms['serviceOrderForm'].elements['serviceOrder.actualCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.actualCubicFeet'].value;		
		document.forms['serviceOrderForm'].elements['serviceOrder.estimateCubicFeet'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateCubicFeet'].value;		
		document.forms['serviceOrderForm'].elements['serviceOrder.estimateGrossWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimateGrossWeight'].value;		
		document.forms['serviceOrderForm'].elements['serviceOrder.estimatedNetWeight'].value=document.forms['serviceOrderForm'].elements['miscellaneous.estimatedNetWeight'].value;		
	    return saveValidation(str);
}
		
function notExists(){
	alert("The Job information is not saved yet");
}
function resetAfterSubmit(){
	var goCnt = '-'+document.forms['serviceOrderForm'].elements['submitCnt'].value * 1;
	history.go(goCnt);
}
	</script>

<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
<script type="text/javascript">
	function autoPopulate_customerFile_originCountry1(targetElement) {
		//var originCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value=targetElement.options[targetElement.selectedIndex].value;
		//targetElement.form.elements['customerFile.originCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+2,originCountryCode.length);
		if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = false;
		}else{
			document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = true;
		}
	}
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_destinationCountry1(targetElement) {
		//var destinationCountryCode=targetElement.options[targetElement.selectedIndex].value;
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value=targetElement.options[targetElement.selectedIndex].value;
		//targetElement.form.elements['customerFile.destinationCountry'].value=destinationCountryCode.substring(destinationCountryCode.indexOf(":")+2,destinationCountryCode.length);
	if(targetElement.value == 'USA' || targetElement.value == 'CAN' || targetElement.value == 'IND' ){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = false;
		}else{
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = true;
		}
	}	
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_destinationCityCode(targetElement) {
		if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value != ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value+','+document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
		}
		if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
		}
			}
</script>
<script type="text/javascript">
	function autoPopulate_customerFile_originCityCode(targetElement) {
		if(document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value != ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value+','+document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
		}
		if(document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value == ''){
			document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
		}
		}
</script>
<script type="text/javascript">
	function autoFilter_PartnerType(targetElement) {
		var partnerType=targetElement.options[targetElement.selectedIndex].value;
	
		var el = document.getElementById('hid');
		if(partnerType == 'AUTO' || partnerType == 'HHG/A' || partnerType == 'BOAT'){
		el.style.visibility = 'visible';
		}else{
		el.style.visibility = 'collapse';
		}	
	}
function openBookingAgentPopWindow(){
var first = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
var last = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
javascript:openWindow('bookingAgentQuotationPopup.html?partnerType=AG&firstName='+first+'&lastName='+last+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=serviceOrder.bookingAgentName&fld_code=serviceOrder.bookingAgentCode');
}

function displayStatusReason(targetElement){
	  var statusValue=targetElement.value;
	  if(statusValue=='CNCL'){
	     document.getElementById("reasonHid").style.display="block";
	     document.getElementById("reasonHid1").style.display="block";
	    } else {
	     document.getElementById("reasonHid").style.display="none";
	     document.getElementById("reasonHid1").style.display="none";
	    }	
	}	
function displayStatusReasonOnLoad(statusValue){
	  if(statusValue=='CNCL'){
	     document.getElementById("reasonHid").style.display="block";
	     document.getElementById("reasonHid1").style.display="block";
	    } else {
	     document.getElementById("reasonHid").style.display="none";
	     document.getElementById("reasonHid1").style.display="none";
	    }	
	}	
	
function autoPopulate_customerFile_statusDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['serviceOrder.statusDate'].value=datam;		
	}
function quoteAcceptReason(targetElement){
	  var acceptValue=targetElement.value;
	  <configByCorp:fieldVisibility componentId="component.field.quoteAcceptReason.showUTSI">
	  if(acceptValue=='AC'){
	     document.getElementById("reasonHid3").style.display="block";
	     document.getElementById("reasonHid4").style.display="block";
	    } else {
	     document.getElementById("reasonHid3").style.display="none";
	     document.getElementById("reasonHid4").style.display="none";
	    }
	  </configByCorp:fieldVisibility> 	
	}
function displayQuoteAcceptReasonOnLoad(statusValue){
	 <configByCorp:fieldVisibility componentId="component.field.quoteAcceptReason.showUTSI">
	  if(statusValue=='AC'){
	     document.getElementById("reasonHid3").style.display="block";
	     document.getElementById("reasonHid4").style.display="block";
	    } else {
	     document.getElementById("reasonHid3").style.display="none";
	     document.getElementById("reasonHid4").style.display="none";
	    }
	  </configByCorp:fieldVisibility>	
	}				
</script>

<script>
function saveAuto(clickType){
	var obj = document.getElementById('accInnerTable');
	if(obj != null || obj != undefined){
		//validatePricing('TabChange');
		findAllPricingLineId('TabChange',clickType);
	}else{
		saveAutoTabChange(clickType);
	}
}


function saveAutoTabChange(clickType){
	var check =saveValidation('');
	 if(check){
		 document.getElementById('pricingFooterValue').value = pricingFooterValue;
		progressBarAutoSave('1');
		if ('${autoSavePrompt}' == 'No'){
			
			var noSaveAction;
			var id1 = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
		   	var idc = document.forms['serviceOrderForm'].elements['customerFile.id'].value;
		   	var jobNumber = document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value; 
			if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				noSaveAction = 'quotationServiceOrders.html?id='+idc;
			}
			if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				noSaveAction  = 'quotationAccountLineList.html?sid='+id1;
			}
			if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.quotation'){
				noSaveAction = 'QuotationFileForm.html?id='+idc;
			}
			if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.survey'){
				noSaveAction = 'inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}&Quote=y';
			}
			processAutoSave(document.forms['serviceOrderForm'],'saveQuotatonServiceOrder!saveOnTabChange.html', noSaveAction );
		
		}
		else{
	     if(!(clickType == 'save')){
		 
		   var id1 = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
		   var idc = document.forms['serviceOrderForm'].elements['customerFile.id'].value;
		   var jobNumber = document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value; 
		   if (document.forms['serviceOrderForm'].elements['formStatus'].value == '1'){
		       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the 'Quotation Service Order'");
			       if(agree){
			           document.forms['serviceOrderForm'].action = 'saveQuotatonServiceOrder!saveOnTabChange.html';
			           document.forms['serviceOrderForm'].submit();
			       }else{
			           if(id1 != ''){
			           	   if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				               location.href = 'quotationServiceOrders.html?id='+idc;
				               }
				           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				               location.href = 'quotationAccountLineList.html?sid='+id1;
				               }
			           	   if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.quotation'){
				               location.href = 'QuotationFileForm.html?id='+idc;
				               }
				       	   if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.survey'){
				       		   location.href = 'inventoryDataList.html?cid='+idc+'&id='+id1+'&Quote=y';
					    	   }		               
	      				 }
	       			}
	   			}else{
				   if(id1 != ''){
						  
				   	   	   if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				               location.href = 'quotationServiceOrders.html?id='+idc;
				               }
				           if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.accounting'){
				               location.href = 'quotationAccountLineList.html?sid='+id1;
				               }
			           	   if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.quotation'){
				               location.href = 'QuotationFileForm.html?id='+idc;
				               }
				       	   if(document.forms['serviceOrderForm'].elements['gotoPageString'].value == 'gototab.survey'){
				       		   location.href = 'inventoryDataList.html?cid='+idc+'&id='+id1+'&Quote=y';
					    	   }
				   }
	   			}
			}
			}
	  }
	}

function changeStatus(){
   document.forms['serviceOrderForm'].elements['formStatus'].value = '1';
}
function findCompanyDivisionByBookAg() {
	var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
    var url="findCompanyDivisionByBookAg.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponse4444;
    http6.send(null);
}	
function handleHttpResponse4444() {
		    if (http6.readyState == 4)
             {
                var results = http6.responseText
                results = results.trim();
                var res = results.split("@");                
                var targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'];
					targetElement.length = res.length;
 					for(i=1;i<res.length;i++)
 					{
 						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].text = res[i];
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[i].value = res[i];
						//document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=true;
					}	
					if(res.length == 2){
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[1].selected=true;
					}else if(res.length > 2) {
						document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].options[0].selected=true;			
						}else{
						}						
					getJobList();	
					}
			}       
function check(targetElement){
  var t = targetElement.value;
  if(t=='lbscft'){
  document.forms['serviceOrderForm'].elements['miscellaneous.unit1'].value='Lbs';
  document.forms['serviceOrderForm'].elements['miscellaneous.unit2'].value='Cft';
  }
  if(t=='kgscbm'){
  document.forms['serviceOrderForm'].elements['miscellaneous.unit1'].value='Kgs';
  document.forms['serviceOrderForm'].elements['miscellaneous.unit2'].value='Cbm';
  }
}    

var http50 = getHTTPObject50();
	function getHTTPObject50()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http51 = getHTTPObject51();
	function getHTTPObject51()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http52 = getHTTPObject52();
	function getHTTPObject52()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function getConsultant(){
   if('${roleHitFlag}'!="OK"){
    var job = '${serviceOrder.job}';
   }
   else{
   var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
  }
   	if(job!=''){
     	var url = "findSale.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http52.open("GET", url, true);
     	http52.onreadystatechange = handleHttpResponse52;
     	http52.send(null);
    }    
}
function handleHttpResponse52(){
		
		if (http52.readyState == 4){
                var results = http52.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.salesMan'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("salesMan").value = '${serviceOrder.salesMan}';
        	getNewCoordAndSale();
        }
  }
  function getNewCoordAndSale(){
	 
   if('${roleHitFlag}'!="OK"){
    var job = '${serviceOrder.job}';
   }
   else{
   var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
  }
   if(job!=''){
     	var url = "findCoord.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http50.open("GET", url, true);
     	http50.onreadystatechange = handleHttpResponse50;
     	http50.send(null);
    }    
}
function handleHttpResponse50(){
		
		if (http50.readyState == 4){
                var results = http50.responseText
                results = results.trim();
                res = results.split("~");
              	targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.coordinator'].options[i].value = stateVal[0];
					
					}
					}
					document.getElementById("coordinator").value = '${serviceOrder.coordinator}';
		 }
}

var httpJob = getHTTPObjectJob();
function getHTTPObjectJob(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
function getJobList(){
	var comDiv = "";
	if(document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value != null){
		comDiv = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	}
  	var url = "findJobs.html?ajax=1&decorator=simple&popup=true&compDivision="+encodeURI(comDiv);
   	httpJob.open("GET", url, true);
   	httpJob.onreadystatechange = handleHttpResponseJob;
   	httpJob.send(null);
}
function handleHttpResponseJob(){
	if (httpJob.readyState == 4){
    	var results = httpJob.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.job'];
        targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].text = '';
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].text = stateVal[1];
				document.forms['serviceOrderForm'].elements['serviceOrder.job'].options[i].value = stateVal[0];
			}
		}
		document.getElementById("jobServiceQuote").value = '${serviceOrder.job}';
	}
}
 function addCopyServiceOrder(){
		    var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
		    var url = "addAndCopyQuote.html?pricingButton=yes&cid=${customerFile.id}&sid=${serviceOrder.id}"; 
			document.forms['serviceOrderForm'].action= url;	 
 		    var check =saveValidation('');
 		    if(check){
 		    document.forms['serviceOrderForm'].submit();
 		    }
		return true;
 } 
 
 function addCopyPricingServiceOrder(){
        var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
	    var url = "addCopyPricingServiceOrder.html?pricingButton=yes&cid=${customerFile.id}&sid=${serviceOrder.id}"; 
		document.forms['serviceOrderForm'].action= url;	 
		var check =saveValidation('');
 		if(check){
		document.forms['serviceOrderForm'].submit();
		}	    
 } 
 
 function CopyQuotesWithResources(){
     var customerFileId=document.forms['serviceOrderForm'].elements['customerFile.id'].value;
     var url = "addWithResourcesQuotes.html?pricingButton=yes&cid=${customerFile.id}&sid=${serviceOrder.id}"; 
     document.forms['serviceOrderForm'].action= url;  
     var check =saveValidation('');
     if(check){
     document.forms['serviceOrderForm'].submit();
     }       
} 
 
 function addServiceOrder(target){
		var check =saveValidation('Add');
		if(!check){
			   return false;
		}else {
			var url = "editQuotationServiceOrder.html?id=${customerFile.id}";
			autoPopulate_customerFile_statusDate(target);
			location.href= url;
		}	    
	}
 <%-- var accountLineIdScript=""; 
 function chk(chargeCode,category,aid){
	  accountLineIdScript=aid;
		var val = document.forms['serviceOrderForm'].elements[category].value;
		var quotationContract = document.forms['serviceOrderForm'].elements['billing.contract'].value; 
		var originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
		var destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
		var mode;		
		if(document.forms['serviceOrderForm'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value; }else{mode="";}
		
			recGlVal = 'secondDescription';
			payGlVal = 'thirdDescription'; 
			var estimateContractCurrency='tenthDescription';
			var estimatePayableContractCurrency='fourthDescription';
			<c:if test="${contractType}">  
				estimateContractCurrency='contractCurrencyNew'+aid;
				estimatePayableContractCurrency='payableContractCurrencyNew'+aid;
			</c:if>
		if(val=='Internal Cost'){
			quotationContract="Internal Cost Management";
			javascript:openWindow('chargess.html?contract='+quotationContract+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode);
		    document.forms['serviceOrderForm'].elements[chargeCode].select();
		}
		else {
		if(quotationContract=='' || quotationContract==' '){
			alert("There is no pricing contract in billing: Please select.");
		}
		else{
			javascript:openWindow('chargess.html?contract='+quotationContract+'&originCountry='+originCountry+'&destinationCountry='+destinationCountry+'&serviceMode='+mode+'&decorator=popup&popup=true&fld_seventhDescription=seventhDescription&fld_eigthDescription=eigthDescription&fld_ninthDescription=ninthDescription&fld_tenthDescription='+estimateContractCurrency+'&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription='+estimatePayableContractCurrency+'&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code='+chargeCode);
		}
	}
	document.forms['serviceOrderForm'].elements[chargeCode].focus();
} 
 
 function winOpenForActgCode(vendorCode,vendorName,category,aid)
  {
 	   accountLineIdScript=aid;
	    var sid='${serviceOrder.id}';
	 	var val = document.forms['serviceOrderForm'].elements[category].value;
	 	var finalVal = "OK";
	 	var indexCheck = val.indexOf('Origin'); 
	    openWindow('findVendorCode.html?sid='+sid+'&defaultListFlag=show&partnerType=PA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+vendorName+'&fld_code='+vendorCode+'&fld_seventhDescription=seventhDescription',screen.width-30,500);
	 	document.forms['serviceOrderForm'].elements[vendorCode].select(); 
 } 
 
   function getAccountlineField(aid,basis,quantity,chargeCode,contract,vendorCode){
	  accountLineBasis=document.forms['serviceOrderForm'].elements[basis].value;
	  accountLineEstimateQuantity=document.forms['serviceOrderForm'].elements[quantity].value;
	  chargeCode=document.forms['serviceOrderForm'].elements[chargeCode].value;
	  contract=document.forms['serviceOrderForm'].elements[contract].value;
	  vendorCode=document.forms['serviceOrderForm'].elements[vendorCode].value;
	  <c:if test="${contractType}">
	  var estimatePayableContractCurrencyTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value; 
	  var estimatePayableContractValueDateTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractValueDateNew'+aid].value;
	  var estimatePayableContractExchangeRateTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
	  var estimatePayableContractRateTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
	  var estimatePayableContractRateAmmountTemp=document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value;
	  </c:if>
	  var estCurrencyTemp=document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value; 
	  var estValueDateTemp=document.forms['serviceOrderForm'].elements['estValueDateNew'+aid].value;
	  var estExchangeRateTemp=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
	  var estLocalRateTemp=document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value;
	  var estLocalAmountTemp=document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value;

	  var estimateQuantityTemp=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
	  var estimateRateTemp=document.forms['serviceOrderForm'].elements['estimateRate'+aid].value;
	  var estimateExpenseTemp=document.forms['serviceOrderForm'].elements['estimateExpense'+aid].value;
	  var basisTemp=document.forms['serviceOrderForm'].elements['basis'+aid].value;
	  <c:if test="${contractType}">
	  openWindow('getAccountlineField.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estimatePayableContractCurrencyTemp='+estimatePayableContractCurrencyTemp+'&estimatePayableContractValueDateTemp='+estimatePayableContractValueDateTemp+'&estimatePayableContractExchangeRateTemp='+estimatePayableContractExchangeRateTemp+'&estimatePayableContractRateTemp='+estimatePayableContractRateTemp+'&estimatePayableContractRateAmmountTemp='+estimatePayableContractRateAmmountTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	  </c:if>
	  <c:if test="${!contractType}">
	  openWindow('getAccountlineField.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateRateTemp='+estimateRateTemp+'&estimateExpenseTemp='+estimateExpenseTemp+'&estCurrencyTemp='+estCurrencyTemp+'&estValueDateTemp='+estValueDateTemp+'&estExchangeRateTemp='+estExchangeRateTemp+'&estLocalRateTemp='+estLocalRateTemp+'&estLocalAmountTemp='+estLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	  </c:if>
  }
  
  --%>
  <%--
  function getAccountlineFieldNew(aid,basis,quantity,chargeCode,contract,vendorCode){
	  accountLineBasis=document.forms['serviceOrderForm'].elements[basis].value;
	  accountLineEstimateQuantity=document.forms['serviceOrderForm'].elements[quantity].value;
	  chargeCode=document.forms['serviceOrderForm'].elements[chargeCode].value;
	  contract=document.forms['serviceOrderForm'].elements[contract].value;  
	  vendorCode=document.forms['serviceOrderForm'].elements[vendorCode].value;  
	  <c:if test="${contractType}">
		  var estimateContractCurrencyTemp=document.forms['serviceOrderForm'].elements['estimateContractCurrencyNew'+aid].value; 
		  var estimateContractValueDateTemp=document.forms['serviceOrderForm'].elements['estimateContractValueDateNew'+aid].value;
		  var estimateContractExchangeRateTemp=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value;
		  var estimateContractRateTemp=document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		  var estimateContractRateAmmountTemp=document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value;
	  </c:if>	  
		  var estSellCurrencyTemp=document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value; 
		  var estSellValueDateTemp=document.forms['serviceOrderForm'].elements['estSellValueDateNew'+aid].value;
		  var estSellExchangeRateTemp=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
		  var estSellLocalRateTemp=document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;
		  var estSellLocalAmountTemp=document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value;

		  var estimateQuantityTemp=document.forms['serviceOrderForm'].elements['estimateQuantity'+aid].value;
		  var estimateSellRateTemp=document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value;
		  var estimateRevenueAmountTemp=document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value;
		  var basisTemp=document.forms['serviceOrderForm'].elements['basis'+aid].value;
	  <c:if test="${contractType}">
		  openWindow('getAccountlineFieldNew.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estimateContractCurrencyTemp='+estimateContractCurrencyTemp+'&estimateContractValueDateTemp='+estimateContractValueDateTemp+'&estimateContractExchangeRateTemp='+estimateContractExchangeRateTemp+'&estimateContractRateTemp='+estimateContractRateTemp+'&estimateContractRateAmmountTemp='+estimateContractRateAmmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	  </c:if>
	  <c:if test="${!contractType}">
		  openWindow('getAccountlineFieldNew.html?aid='+aid+'&basisTemp='+basisTemp+'&estimateQuantityTemp='+estimateQuantityTemp+'&estimateSellRateTemp='+estimateSellRateTemp+'&estimateRevenueAmountTemp='+estimateRevenueAmountTemp+'&estSellCurrencyTemp='+estSellCurrencyTemp+'&estSellValueDateTemp='+estSellValueDateTemp+'&estSellExchangeRateTemp='+estSellExchangeRateTemp+'&estSellLocalRateTemp='+estSellLocalRateTemp+'&estSellLocalAmountTemp='+estSellLocalAmountTemp+'&accountLineBasis='+accountLineBasis+'&accountLineEstimateQuantity='+accountLineEstimateQuantity+'&chargeCode='+chargeCode+'&contract='+contract+'&vendorCodeNew='+vendorCode+'&decorator=popup&popup=true',920,350);
	  </c:if>
	  }
   function fillDiscription(discription,chargeCode,category,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate)
{
	var category = document.forms['serviceOrderForm'].elements[category].value;
	var quotationContract = document.forms['serviceOrderForm'].elements['customerFile.contract'].value;
	var chargeCode = document.forms['serviceOrderForm'].elements[chargeCode].value;
	var discriptionValue = document.forms['serviceOrderForm'].elements[discription].value;  
	var jobtype = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
    var routing = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;  
    var companyDivision = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value; 
		var url = "findQuotationDiscription.html?ajax=1&contract="+quotationContract+"&chargeCode="+chargeCode+"&decorator=simple&popup=true&jobType="+jobtype+"&routing1="+routing+"&companyDivision="+companyDivision;
     	http50.open("GET", url, true);
     	http50.onreadystatechange =  function(){fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate);};
     	http50.send(null);
		
}
function fillDiscriptionResponse(discription,recGl,payGl,contractCurrency,contractExchangeRate,contractValueDate,payableContractCurrency,payableContractExchangeRate,payableContractValueDate)
{
   if (http50.readyState == 4)
       {
           var result= http50.responseText
           result = result.trim();   
           result = result.replace('[','');
           result=result.replace(']','');
           var res = result.split("#"); 
           var discriptionValue = document.forms['serviceOrderForm'].elements[discription].value; 
           <configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode"> 
           if(result!='NoDiscription') {
           if(document.forms['serviceOrderForm'].elements[discription].value.trim()==''){     
           document.forms['serviceOrderForm'].elements[discription].value = res[0];
           } 
           } 
           </configByCorp:fieldVisibility>
           <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
           document.forms['serviceOrderForm'].elements[discription].value = res[0];
           </configByCorp:fieldVisibility>
          /*  document.forms['serviceOrderForm'].elements[recGl].value = res[1];     
           document.forms['serviceOrderForm'].elements[payGl].value = res[2];
           document.forms['serviceOrderForm'].elements[contractCurrency].value = res[3]; 
           document.forms['serviceOrderForm'].elements[payableContractCurrency].value = res[4];   */            
        }
/*    <c:if test="${contractType}">
	var contCurrency = document.forms['serviceOrderForm'].elements[contractCurrency].value
	if(contCurrency!=''){
		var url="findExchangeRate.html?decorator=simple&popup=true&country="+encodeURI(contCurrency);
   		http77.open("GET", url, true);
  		http77.onreadystatechange = function(){handleHttpResponseContractRate(contractExchangeRate,contractValueDate);};
  		http77.send(null);
	}
	var payableContractCurrency = document.forms['serviceOrderForm'].elements[payableContractCurrency].value
	if(payableContractCurrency!=''){
		var url="findExchangeRate.html?decorator=simple&popup=true&country="+encodeURI(payableContractCurrency);
   		http777.open("GET", url, true);
  		http777.onreadystatechange = function(){handleHttpResponsePayableContractRate(payableContractExchangeRate,payableContractValueDate);};
  		http777.send(null);
	}
	
</c:if> */
	} --%>
	
	
/* function handleHttpResponseContractRate(contractExchangeRate,contractValueDate) { 
    if (http77.readyState == 4) {
       var results = http77.responseText
       results = results.trim(); 
       results = results.replace('[','');
       results=results.replace(']',''); 
       if(results.length>1) {
         document.forms['serviceOrderForm'].elements[contractExchangeRate].value=results 
         } else {
         document.forms['serviceOrderForm'].elements[contractExchangeRate].value=1;
         }
         var mydate=new Date();
         var daym;
         var year=mydate.getFullYear()
         var y=""+year;
         if (year < 1000)
         year+=1900
         var day=mydate.getDay()
         var month=mydate.getMonth()+1
         if(month == 1)month="Jan";
         if(month == 2)month="Feb";
         if(month == 3)month="Mar";
		  if(month == 4)month="Apr";
		  if(month == 5)month="May";
		  if(month == 6)month="Jun";
		  if(month == 7)month="Jul";
		  if(month == 8)month="Aug";
		  if(month == 9)month="Sep";
		  if(month == 10)month="Oct";
		  if(month == 11)month="Nov";
		  if(month == 12)month="Dec";
		  var daym=mydate.getDate()
		  if (daym<10)
		  daym="0"+daym
		  var datam = daym+"-"+month+"-"+y.substring(2,4); 
		  document.forms['serviceOrderForm'].elements[contractValueDate].value=datam;  
 }   } 

function handleHttpResponsePayableContractRate(payableContractExchangeRate,payableContractValueDate) { 
    if (http777.readyState == 4) {
       var results = http777.responseText
       results = results.trim(); 
       results = results.replace('[','');
       results=results.replace(']',''); 
       if(results.length>1) {
         document.forms['serviceOrderForm'].elements[payableContractExchangeRate].value=results 
         } else {
         document.forms['serviceOrderForm'].elements[payableContractExchangeRate].value=1;
         }
         var mydate=new Date();
         var daym;
         var year=mydate.getFullYear()
         var y=""+year;
         if (year < 1000)
         year+=1900
         var day=mydate.getDay()
         var month=mydate.getMonth()+1
         if(month == 1)month="Jan";
         if(month == 2)month="Feb";
         if(month == 3)month="Mar";
		  if(month == 4)month="Apr";
		  if(month == 5)month="May";
		  if(month == 6)month="Jun";
		  if(month == 7)month="Jul";
		  if(month == 8)month="Aug";
		  if(month == 9)month="Sep";
		  if(month == 10)month="Oct";
		  if(month == 11)month="Nov";
		  if(month == 12)month="Dec";
		  var daym=mydate.getDate()
		  if (daym<10)
		  daym="0"+daym
		  var datam = daym+"-"+month+"-"+y.substring(2,4); 
		  document.forms['serviceOrderForm'].elements[payableContractValueDate].value=datam;  
 }   }  */

var http77 = getHTTPObject77();
var http777 = getHTTPObject77();
function getHTTPObject77()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 function discriptionView(discription){ 
 document.getElementById(discription).style.height="50px"; 
 } 
  
 function discriptionView1(discription){ 
 document.getElementById(discription).style.height="15px"; 
 SetCursorToTextEnd(discription);
 }    
function SetCursorToTextEnd(textControlID) 
{

	var text = document.getElementById(textControlID); 
	text.scrollTop = 1;
}
 function onlyNumberAllowed(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39); 
	}
	var negativeCount=0;
	var decimalCount = 0;

	function clearDecimal(){
		count = 0;
		decimalCount = 0;
	}
	function numbersonly(myfield, e)
	{
		var key;
		var keychar;
		var val = myfield.value;
		if(count > 0){
			decimalCount++;
			if(decimalCount > 2){
				return true;
			}
		}
				if(val.indexOf(".")<0)
				{
				count=0;
				}
	    
		if (window.event)
		   key = window.event.keyCode;
		else if (e)
		   key = e.which;
		else
		   return true;
	
			keychar = String.fromCharCode(key);
		if ((key==null) || (key==0) || (key==8) ||     (key==9) || (key==13) || (key==27) )
		   return true;
		else if ((("0123456789").indexOf(keychar) > -1))
		   return true;
		else if ((count<1)&&(keychar == "."))
		{
			
		   count++;
		   return true;
		   
		}
		else if ((negativeCount<1)&&(keychar == "-"))
		{
			negativeCount++;
		   return true;
		}
		else if ((negativeCount>0)&&(keychar == "-"))
		{
		   return false;
		}
		else if ((count>0)&&(keychar == "."))
		{
		   return false;
		}
		else
		   return false;
	}	
   
 function onlyRateAllowed(evt){	
	 var keyCode = evt.which ? evt.which : evt.keyCode; 
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	  
	}
 function checkFloat1(temp)
 { 
   var check='';  
   var i;
   var dotcheck=0;
	var s = temp.value;
	if(s.indexOf(".") == -1){
	dotcheck=eval(s.length)
	}else{
	dotcheck=eval(s.indexOf(".")-1)
	}
	var fieldName = temp.name;  
	var count = 0;
	var countArth = 0;
   for (i = 0; i < s.length; i++)
   {   
       var c = s.charAt(i);
       
       if(c == '.')
       {
       	count = count+1
       }
       if(c == '-')
       {
       	countArth = countArth+1
       }
       if((i!=0)&&(c=='-'))
      	{
      	  alert("Invalid data." ); 
         document.forms['serviceOrderForm'].elements[fieldName].select();
         document.forms['serviceOrderForm'].elements[fieldName].value='';
      	  return false;
      	}
       	
       if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1'))) 
       {
       	check='Invalid'; 
       }
   }
   if(dotcheck>15){
   check='tolong';
   }
   if(check=='Invalid'){ 
   alert("Invalid data." ); 
   document.forms['serviceOrderForm'].elements[fieldName].select();
   document.forms['serviceOrderForm'].elements[fieldName].value='';
   return false;
   } else if(check=='tolong'){ 
   alert("Data Too Long." ); 
    document.forms['serviceOrderForm'].elements[fieldName].select();
   document.forms['serviceOrderForm'].elements[fieldName].value='';
   return false;
   } else{
   s=Math.round(s*100)/100;
   var value=""+s;
   if(value.indexOf(".") == -1){
   value=value+".00";
   } 
   if((value.indexOf(".")+3 != value.length)){
   value=value+"0";
   }
   document.forms['serviceOrderForm'].elements[fieldName].value=value;
   return true;
   }
   }
	function checkFloat(temp)
  { 
		 negativeCount=0;
    var check='';  
    var i;
    var dotcheck=0;
	var s = temp.value;
	if(s.indexOf(".") == -1){
	dotcheck=eval(s.length)
	}else{
	dotcheck=eval(s.indexOf(".")-1)
	}
	var fieldName = temp.name;  
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if(c == '-')
        {
        	countArth = countArth+1
        }
        if((i!=0)&&(c=='-'))
       	{
       	  alert("Invalid data." ); 
       	document.getElementById(fieldName).select();
       	document.getElementById(fieldName).value='';
          
       	  return false;
       	}
        	
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1'))) 
        {
        	check='Invalid'; 
        }
    }
    if(dotcheck>15){
    check='tolong';
    }
    if(check=='Invalid'){ 
    alert("Invalid data." ); 
    document.getElementById(fieldName).select();
   	document.getElementById(fieldName).value='';
    return false;
    } else if(check=='tolong'){ 
    alert("Data to long." ); 
    document.getElementById(fieldName).select();
   	document.getElementById(fieldName).value='';
    return false;
    } else{
    s=Math.round(s*10000)/10000;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".0000";
    } 
    while((value.indexOf(".")+5 != value.length)){
	    value=value+"0";
	    }
    document.getElementById(fieldName).value=value;
    return true;
    }
    }  
	function checkFloatQuantity(temp)  { 
		negativeCount=0;
	    var check='';  
	    var i;
	    var dotcheck=0;
		var s = temp.value;
		if(s.indexOf(".") == -1){
		dotcheck=eval(s.length)
		}else{
		dotcheck=eval(s.indexOf(".")-1)
		}
		var fieldName = temp.name;  
		var count = 0;
		var countArth = 0;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        
	        if(c == '.')
	        {
	        	count = count+1
	        }
	        if(c == '-')
	        {
	        	countArth = countArth+1
	        }
	        if((i!=0)&&(c=='-'))
	       	{
	       	  alert("Invalid data." ); 
	       	document.getElementById(fieldName).select();
	       	document.getElementById(fieldName).value='';
	          
	       	  return false;
	       	}
	        	
	        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1'))) 
	        {
	        	check='Invalid'; 
	        }
	    }
	    if(dotcheck>15){
	    check='tolong';
	    }
	    if(check=='Invalid'){ 
	    alert("Invalid data." ); 
	    document.getElementById(fieldName).select();
	   	document.getElementById(fieldName).value='';
	    return false;
	    } else if(check=='tolong'){ 
	    alert("Data to long." ); 
	    document.getElementById(fieldName).select();
	   	document.getElementById(fieldName).value='';
	    return false;
	    } else{
	    s=Math.round(s*100)/100;
	    var value=""+s;
	    if(value.indexOf(".") == -1){
	    value=value+".00";
	    } 
	    if((value.indexOf(".")+3 != value.length)){
	    value=value+"0";
	    }
	    document.getElementById(fieldName).value=value;
	    return true;
	    }  } 
    function savePricingLine() {
    document.forms['serviceOrderForm'].action ='savePricingLine.html?pricingButton=yes&sid=${serviceOrder.id}';
    var check =saveValidation('');
    if(check){
    document.forms['serviceOrderForm'].submit();
    }  } 
    function addPricingLine() {
	    document.forms['serviceOrderForm'].action ='addPricingLine.html?pricingButton=yes&sid=${serviceOrder.id}';
	    var check =saveValidation('');
	 	if(check){
	    	document.forms['serviceOrderForm'].submit();
	    }  } 
    function confirmSubmit(targetElement){
		var agree=confirm("Are you sure you wish to remove this row?");
		if (agree){
		    document.forms['serviceOrderForm'].action ='deletePricingLine.html?pricingButton=yes&pid='+encodeURI(targetElement)+'&sid=${serviceOrder.id}';
	        document.forms['serviceOrderForm'].submit(); 
		     //location.href = "deletePricingLine.html?pricingButton=yes&id="+encodeURI(targetElement)+"&sid=${serviceOrder.id}";
	     }else{
			return false;
		} }
	   function updateAccInactive() {
		    var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
		    document.forms['serviceOrderForm'].action ='deletePricingLine.html?pricingButton=yes&sid=${serviceOrder.id}';
		    document.forms['serviceOrderForm'].submit(); 
	    } 
	   function selectAllActive(str){
			 var userCheckStatus='';
			 var temp=document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
			 document.forms['serviceOrderForm'].elements['accountIdCheck'].value='';
				<c:forEach var="accountLineList" items="${accountLineList}">
				temp='recFound';
					if(str.checked==true){
						try{
						document.getElementById("statusCheck${accountLineList.id}").checked=true;
					    userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
					    userCheckStatus=document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( '${accountLineList.id}' , '' );
				        document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
						}catch(e){}
					}else{
						try{
					     document.getElementById("statusCheck${accountLineList.id}").checked=false;
					     userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
					      if(userCheckStatus == ''){
						  	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = '${accountLineList.id}';
					      }
					      else{
					       userCheckStatus=	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus + ',' + '${accountLineList.id}';
					       document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
					      }				
					}catch(e){}
				} </c:forEach>
				if(temp!='recFound'){
					document.forms['serviceOrderForm'].elements['accountIdCheck'].value=temp;
				}else{
			     accountIdCheck();
				}}
	   function inactiveStatusCheck(rowId,targetElement){ 
		   if(targetElement.checked==false){
		      var userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
		      if(userCheckStatus == ''){
			  	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = rowId;
		      }else{
		       var userCheckStatus=	document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus + ',' + rowId;
		      document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( ',,' , ',' );
		      }}
		   if(targetElement.checked){
		     var userCheckStatus = document.forms['serviceOrderForm'].elements['accountIdCheck'].value;
		     var userCheckStatus=document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus.replace( rowId , '' );
		     userCheckStatus=userCheckStatus.replace( ',,' , ',' )
		     var len=userCheckStatus.length-1;
		     if(len==userCheckStatus.lastIndexOf(",")){
		    	 userCheckStatus=userCheckStatus.substring(0,len);
		         }if(userCheckStatus.indexOf(",")==0){
		    	 userCheckStatus=userCheckStatus.substring(1,userCheckStatus.length);}
		     document.forms['serviceOrderForm'].elements['accountIdCheck'].value = userCheckStatus;} 
		     accountIdCheck();}
	   
    
 function findDefaultLine() {
	document.forms['serviceOrderForm'].elements['jobtypeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	document.forms['serviceOrderForm'].elements['routingSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
	document.forms['serviceOrderForm'].elements['modeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
	document.forms['serviceOrderForm'].elements['packingModeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value;
	document.forms['serviceOrderForm'].elements['commoditySO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value;
	document.forms['serviceOrderForm'].elements['serviceTypeSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
	document.forms['serviceOrderForm'].elements['companyDivisionSO'].value = document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	var customerFileContract=document.forms['serviceOrderForm'].elements['customerFile.contract'].value;
	if(customerFileContract=='') { 
	 		alert("Please select  Pricing Contract from Quotation File"); 
	 }
	else {
	  document.forms['serviceOrderForm'].action = 'addPricingDefaultLine.html?pricingButton=yes&sid=${serviceOrder.id}';
	  showOrHide(1);
      var check =saveValidation('');
 	  if(check){
      document.forms['serviceOrderForm'].submit();
      } } }
function defaultLineMassage() {
alert("Default pricing template have already been added.");
} 
function openPricing(){
form_submitted = false;
var checkValidation =  submit_form(); 
 if(checkValidation == true){
document.forms['serviceOrderForm'].action = 'saveQuotatonServiceOrderPricing.html?pricingButton=yes&sid=${serviceOrder.id}';
var check =saveValidation('');
if(check){
document.forms['serviceOrderForm'].submit();
setTimeout("animatedcollapse.toggle('pricing')", 5000);
}
;
}
}  
function checkFloatNew(formName,fieldName,message)
{   var i;
	var s = getValueFromId(fieldName); //document.forms[formName].elements[fieldName].value;
	var count = 0;
	if(s.trim()=='.')
	{
		document.getElementById(fieldName).value='0.00';
		//document.forms[formName].elements[fieldName].value='0.00';
	}else{
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if ((c > '9') && (c != '.') || (count>'1')) 
        {
        	document.getElementById(fieldName).value='0.00';
	        //document.forms[formName].elements[fieldName].value='0.00';
        }  }    } }  
  function fillCheckJob(temp){ 
  document.forms['serviceOrderForm'].elements['jobtypeCheck'].value=temp.value;
  } 

function findService(){ 
    var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
    var hidStatusReg5 = document.getElementById('Service2');
    var hidStatusReg4 = document.getElementById('Service1');
    var labelserviceVar = document.getElementById('labelservice');
    var modehidStatusReg = document.getElementById('modeFild');
    var modelabelserviceVar = document.getElementById('modeLabel');
    var routinghidStatusReg = document.getElementById('routingFild');
    var routinglabelserviceVar = document.getElementById('routingLabel');
    var packhidStatusReg = document.getElementById('packFild');
    var packlabelserviceVar = document.getElementById('packLabel'); 
    var commodityStatusReg = document.getElementById('labCommudity');
    var commoditylabelserviceVar = document.getElementById('controlCommudity');  
    var carrierFildLabel1=document.getElementById('carrierFildLabel');
    var carrierFild1=document.getElementById('carrierFild');
	var mode = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
   	if(job!=''){
   		if(job=='RLO'){
    		   hidStatusReg5.style.display = 'block';
    		   hidStatusReg4.style.display = 'none';
    		   labelserviceVar.style.display = 'none';
    		   modehidStatusReg.style.display = 'none';
    		   modelabelserviceVar.style.display = 'none';
    		   routinghidStatusReg.style.display = 'none';
    		   routinglabelserviceVar.style.display = 'none';
    		   packhidStatusReg.style.display = 'none';
    		   packlabelserviceVar.style.display = 'none';
    		   commodityStatusReg.style.display = 'none';
    		   commoditylabelserviceVar.style.display = 'none';
    		   <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
       		   carrierFildLabel1.style.display = 'none';
       	     carrierFild1.style.display = 'none';
       	  </configByCorp:fieldVisibility> 
    		      }else{
    		   	   hidStatusReg4.style.display = 'block';
    			   hidStatusReg5.style.display = 'none';
    			   labelserviceVar.style.display = 'block';
    			   modehidStatusReg.style.display = 'block';
    			   modelabelserviceVar.style.display = 'block';
    			   routinghidStatusReg.style.display = 'block';
    			   routinglabelserviceVar.style.display = 'block';
    			   packhidStatusReg.style.display = 'block';
    			   packlabelserviceVar.style.display = 'block';	
    			   commodityStatusReg.style.display = 'block';
        		   commoditylabelserviceVar.style.display = 'block';
        		   <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
       			   carrierFildLabel1.style.display = 'block';
                   carrierFild1.style.display = 'block';
                   </configByCorp:fieldVisibility> 
    	    }   } } 
function handleHttpResponse54(){
if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();  
                res = results.split("@"); 
              	targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text = '';
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value = '';
					}else{
					stateVal = res[i].split("#"); 
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text = stateVal[1];
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value = stateVal[0];
					} }
					document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value = ''; 
        }   } 
 function saveValidation(str){
var ChargesMandatory='${checkContractChargesMandatory}';
var acclength=false;
if(idList!=''){
	acclength=true;
}
var routingflag=false;
<c:if test="${costElementFlag}">
routingflag=true;
</c:if>
var routing="";
try{
	  routing=document.forms['serviceOrderForm'].elements['serviceOrder.routing'].value;
}catch(e){}
var status="";
try{
	status=document.forms['serviceOrderForm'].elements['serviceOrder.status'].value;
}catch(e){}
var job="";
try{
	job=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
}catch(e){}
 if((routing=='')&&(job!='RLO')&&(status!='CNCL') && routingflag && (acclength || clickType1=='ADD') && inactiveCheck!='Inactive'){
	 alert("Routing is required field.");
	 clickType1="";
	 inactiveCheck="";
	  showOrHideAutoSave('0');
	 return false;
 }else if(job=='' && routingflag && (acclength || clickType1=='ADD')){
	   alert("Job Type is required field.");
	   clickType1="";
	   inactiveCheck="";
	   showOrHideAutoSave('0');
	   return false;
 }else if(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value==''){
   alert("Origin country is required field. ");
   showOrHideAutoSave('0');
   return false;
 } else if(document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value==''){
   alert("Company Division is required field.");
   showOrHideAutoSave('0');
   return false;
 }else if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value==''){
   alert("Destination country is required field.");
   showOrHideAutoSave('0');
   return false;
 }else if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value==''){
   alert("Destination country is required field.");
   showOrHideAutoSave('0');
   return false;
   }else if((document.forms['serviceOrderForm'].elements['serviceOrder.distance'].value!='')&& (document.forms['serviceOrderForm'].elements['serviceOrder.distanceInKmMiles'].value=='') && (document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value=='Overland'||document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value=='Truck')){
	alert("Distance Unit is required field.");
	showOrHideAutoSave('0');
	return false;  
  }else if((ChargesMandatory==true || ChargesMandatory=='true') && str!='Inactive'){
		if(idList!=''){			
			 var id = idList.split(",");
			 for (var i=0;i<id.length;i++){
				var test=document.getElementById('chargeCode'+id[i].trim()).value;
				if(test.trim()==''){
					var lineNo=document.getElementById('accountLineNumber'+id[i].trim()).value;
					 alert("Please select Charge Code for Pricing line # "+lineNo);
					 showOrHideAutoSave('0');
					 return false;
				}  }
			 return true;
			} else{
				return true;
			}
  } else{
   return true;
   } }  
 function appliedState(){
	    var originCountryCode=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value; 
		var ds1 = document.getElementById('originStateRequiredTrue');
		var ds2 = document.getElementById('originStateRequiredFalse');
		if(originCountryCode == 'United States' || originCountryCode == 'India'){
			ds1.style.display = 'block';		
			ds2.style.display = 'none';		
		}else{
			ds1.style.display = 'none';
			ds2.style.display = 'block';
		} }
</script> 
<script type="text/javascript">
   function sendEmail(target)  {
   		var originEmail = target;
   		var subject = 'Q/S# '+document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;;
   		var firstName = document.forms['serviceOrderForm'].elements['serviceOrder.firstName'].value;
        var lastName = document.forms['serviceOrderForm'].elements['serviceOrder.lastName'].value;
        subject = subject+' '+firstName+' '+lastName;   		   	
		var mailto_link = 'mailto:'+originEmail+'?subject='+subject;		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
		}
   </script> 
<script type="text/javascript"> 
function destinationState11(){
        var destinationCountryCode=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value; 
		var ds3 = document.getElementById('destinationStateRequiredTrue');
		var ds4 = document.getElementById('destinationStateRequiredFalse');	
		if(destinationCountryCode == 'United States' || destinationCountryCode == 'India'){
			ds3.style.display = 'block';		
			ds4.style.display = 'none';		
		}else{
			ds3.style.display = 'none';
			ds4.style.display = 'block';
		} } 
</script> 

<script type="text/javascript">
<%--     function changeVatAmt(estVatDec,estVatPer,revenue,estAmt)
    {
        	 <c:forEach var="entry" items="${estVatPersentList}">
	            if(document.forms['serviceOrderForm'].elements[estVatDec].value=='${entry.key}') {
            		document.forms['serviceOrderForm'].elements[estVatPer].value='${entry.value}'; 
		            calculateVatAmt(estVatPer,revenue,estAmt);
            	}
        	</c:forEach>
          		if(document.forms['serviceOrderForm'].elements[estVatDec].value==''){
          			document.forms['serviceOrderForm'].elements[estVatPer].value=0;
          			calculateVatAmt(estVatPer,revenue,estAmt);
          		}          
    } 
	function calculateVatAmt(estVatPer,revenue,estAmt){		 
		var estVatAmt=0;
		if(document.forms['serviceOrderForm'].elements[estVatPer].value!=''){
		var estVatPercent=eval(document.forms['serviceOrderForm'].elements[estVatPer].value);
		var estRevenueForeign= eval(document.forms['serviceOrderForm'].elements[revenue].value);
		estVatAmt=(estRevenueForeign*estVatPercent)/100;
		estVatAmt=Math.round(estVatAmt*100)/100; 
		document.forms['serviceOrderForm'].elements[estAmt].value=estVatAmt;
		}	    
	}--%> 
	function onlyNumsAllowedPersent(evt){
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function getOnGroup(label) {
		if(label=='dropDown') {
			if(document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value=='GRP') {
			document.forms['serviceOrderForm'].elements['serviceOrder.grpPref'].checked=true;
			} }
		if(label=='checkBox')
		{
			if(document.forms['serviceOrderForm'].elements['serviceOrder.grpPref'].checked==true)
			{
			document.forms['serviceOrderForm'].elements['serviceOrder.packingMode'].value='GRP';
			} } }  
	function openStandardAddressesPopWindow(){
	 	var jobType =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
		javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=serviceOrder.originMobile&fld_ninthDescription=serviceOrder.originCity&fld_eigthDescription=serviceOrder.originFax&fld_seventhDescription=serviceOrder.originHomePhone&fld_sixthDescription=serviceOrder.originDayPhone&fld_fifthDescription=serviceOrder.originZip&fld_fourthDescription=stdAddOriginState&fld_thirdDescription=serviceOrder.originCountry&fld_secondDescription=serviceOrder.originAddressLine3&fld_description=serviceOrder.originAddressLine2&fld_code=serviceOrder.originAddressLine1');
	document.forms['serviceOrderForm'].elements['checkConditionForOriginDestin'].value="originAddSection";
	}
	function openStandardAddressesDestinationPopWindow(){
	 	var jobType =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
		javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=serviceOrder.destinationMobile&fld_ninthDescription=serviceOrder.destinationCity&fld_eigthDescription=serviceOrder.destinationFax&fld_seventhDescription=serviceOrder.destinationHomePhone&fld_sixthDescription=serviceOrder.destinationDayPhone&fld_fifthDescription=serviceOrder.destinationZip&fld_fourthDescription=stdAddDestinState&fld_thirdDescription=serviceOrder.destinationCountry&fld_secondDescription=serviceOrder.destinationAddressLine3&fld_description=serviceOrder.destinationAddressLine2&fld_code=serviceOrder.destinationAddressLine1');
	document.forms['serviceOrderForm'].elements['checkConditionForOriginDestin'].value="destinAddSection";
	}
	 
	 function enableState(){
	var id=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	var AddSection = document.forms['serviceOrderForm'].elements['checkConditionForOriginDestin'].value;
	if(AddSection=='originAddSection'){
	var originCountry = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value
	getOriginCountryCode();
		var countryCode = "";
		if(originCountry == 'United States'){
			countryCode = "USA";
		}
		if(originCountry == 'India'){
			countryCode = "IND";
		}
		if(originCountry == 'Canada'){
			countryCode = "CAN";
		}
		
	if(countryCode == 'IND' || countryCode == 'CAN' || countryCode == 'USA'){
				document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =false;
			
			}else{
				
				document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =true;
				document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value="";
			}
		var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	     http9.open("GET", url, true);
	     http9.onreadystatechange = handleHttpResponse12;
	     http9.send(null);

	}
	if(AddSection=='destinAddSection'){
	var country = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value
		getDestinationCountryCode();
		var countryCode = "";
		if(country == 'United States'){
			countryCode = "USA";
		}
		if(country == 'India'){
			countryCode = "IND";
		}
		if(country == 'Canada'){
			countryCode = "CAN";
		}
	if(countryCode == 'IND' || countryCode == 'CAN' || countryCode == 'USA'){
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =false; 
			}else{ 
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =true;
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value="";
			}
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
	     http9.open("GET", url, true);
	     http9.onreadystatechange = handleHttpResponse11;
	     http9.send(null);
	} } 
	function handleHttpResponse11(){ 
			if (http9.readyState == 4){
	                var results = http9.responseText
	                results = results.trim();
	                res = results.split("@");
	                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
	                var stdAddDestinState=document.forms['serviceOrderForm'].elements['stdAddDestinState'].value;
	                targetElement.length = res.length;
	 				for(i=0;i<res.length;i++)
						{
						if(res[i] == ''){
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
						}else{
						stateVal = res[i].split("#");
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
						document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
						
						if (document.getElementById("destinationState").options[i].value == '${serviceOrder.destinationState}'){
						   document.getElementById("destinationState").options[i].defaultSelected = true;
						}}}
						document.getElementById("destinationState").value = stdAddDestinState;
						if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value != ''){
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value+','+document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
			}
			if(document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value == ''){
				document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
			}   }}  
	function handleHttpResponse12(){ 
			if (http9.readyState == 4){
	                var results = http9.responseText
	                results = results.trim();
	                res = results.split("@");
	                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
	                var stdAddOriginState=document.forms['serviceOrderForm'].elements['stdAddOriginState'].value;
	                targetElement.length = res.length;
	 				for(i=0;i<res.length;i++)
						{
						if(res[i] == ''){
						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
						}else{
						stateVal = res[i].split("#");
						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
						document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0];
						
						if (document.getElementById("originState").options[i].value == '${serviceOrder.originState}'){
						   document.getElementById("originState").options[i].defaultSelected = true;
						}}}
						document.getElementById("originState").value = stdAddOriginState;
	        if(document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value != ''){
				document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value+','+document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
			}
			if(document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value == ''){
				document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value=document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
			}}}

    var http9 = getHTTPObject9(); 
    function getHTTPObject9()
   {
       var xmlhttp;
       if(window.XMLHttpRequest)   {
           xmlhttp = new XMLHttpRequest();
       }
       else if (window.ActiveXObject)  {
           xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
           if (!xmlhttp)
           {
               xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
           }  }
       return xmlhttp;
   } 
	function fillCommodity()
	{
		if(document.forms['serviceOrderForm'].elements['serviceOrder.job'].value=='RLO')
		{
			document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value='RELSE';		
		} }
	function selectColumn(targetElement) 
	   {  
	     var rowId=targetElement.value;  
	     if(targetElement.checked)
	       {
	        var userCheckStatus = document.forms['serviceOrderForm'].elements['reloServiceType'].value;
	        if(userCheckStatus == '')
	        {
	  	  	document.forms['serviceOrderForm'].elements['reloServiceType'].value = rowId;
	        }
	        else
	        {
	         var userCheckStatus=	document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus + '#' + rowId;
	        document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus.replace( '##' , '#' );
	        } }
	     if(targetElement.checked==false)
	      {
	       var userCheckStatus = document.forms['serviceOrderForm'].elements['reloServiceType'].value;
	       var userCheckStatus=document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus.replace( rowId , '' );
	       document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus.replace( '##' , '#' );
	       }   }
	function checkColumn(){
		var userCheckStatus ="";
		userCheckStatus = '${serviceOrder.serviceType}';		  
		if(userCheckStatus!="")
		{
		var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
			document.forms['serviceOrderForm'].elements['reloServiceType'].value="";
			var column = userCheckStatus.split(",");  
			for(i = 0; i<column.length ; i++) {    
			     var userCheckStatus = document.forms['serviceOrderForm'].elements['reloServiceType'].value;  
		    	 var userCheckStatus=	document.forms['serviceOrderForm'].elements['reloServiceType'].value = userCheckStatus + '#' +column[i];
		        	if(column[i]!="")
		        	{
			        	if(job=='RLO'){
		     				document.getElementById(column[i]).checked=true;
		        		} } }  }
		if(userCheckStatus==""){	}
	}    
	function changeReset(){
		   document.getElementById("reset1").value='reset';
		 }
	  function getServiceTypeByJob() {
		    var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
		    if(jobType!='RLO'){
			var url="getServiceTypeByJob.html?ajax=1&decorator=simple&popup=true&jobType="+encodeURI(jobType);
			http5412.open("GET", url, true); 
			http5412.onreadystatechange = handleHttpResponse5412; 
			http5412.send(null);
		    }   }	
		function handleHttpResponse5412(){
			if (http5412.readyState == 4){ 
			            var results = http5412.responseText
			            results = results.trim();
			            res = results.replace("{",'');
			            res = res.replace("}",'');
			            res = res.split(",");
						targetElementValue=document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value;
						targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'];
						targetElement.length = res.length;
						var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
							for(i=0;i<res.length;i++){
							  if(res[i] == ''){
							  document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text = '';
							  document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value = '';
							  }else{
								   stateVal = res[i].replace("=",'+');
								   stateVal=stateVal.split("+");
								   document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].text=stateVal[1];
								   document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[i].value=stateVal[0].trim();
								   if(job == "")
								   {
								   	document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[0].selected=true;
								  	document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].options[0].selected=true;
								   }  }  }
						document.forms['serviceOrderForm'].elements['serviceOrder.serviceType'].value=targetElementValue;
			        }   }	
	    var http5412 = getHTTPObject();

   function enableStateListOrigin(){ 
			var oriCon=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
			  var enbState = '${enbState}';
			  var index = (enbState.indexOf(oriCon)> -1);
			  if(index != ''){
			  		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = false;
			  }else{
				  document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled = true;
			  }	  }
	function enableStateListDestin(){ 
			var desCon=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
			  var enbState = '${enbState}';
			  var index = (enbState.indexOf(desCon)> -1);
			  if(index != ''){
			  		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = false;
			  }else{
				  document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled = true;
			  }	   }
	function newFunctionForCountryState(){
		var destinationAbc ='${serviceOrder.destinationCountry}';
		var originAbc ='${serviceOrder.originCountry}';
		var enbState = '${enbState}';
		var index = (enbState.indexOf(originAbc)> -1);
		var index1 = (enbState.indexOf(destinationAbc)> -1);
		if(index != ''){
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =false;
		getOriginStateReset(originAbc);
		}
		else{
		document.forms['serviceOrderForm'].elements['serviceOrder.originState'].disabled =true;
		}
		if(index1 != ''){
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =false;
		getDestinationStateReset(destinationAbc);
		}
		else{
		document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].disabled =true;
		} }
		function getDestinationStateReset(target){
			var country = target;
		 	var countryCode = "";
			<c:forEach var="entry" items="${countryCod}">
			if(country=="${entry.value}"){
				countryCode="${entry.key}";
			}
			</c:forEach>
			//var countryCode = destinationAbc;
			var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
		     httpDState.open("GET", url, true);
		     httpDState.onreadystatechange = handleHttpResponse6666666666;
		     httpDState.send(null);
		}
		function getOriginStateReset(target) {
			var country = target;
		 	var countryCode = "";
			<c:forEach var="entry" items="${countryCod}">
			if(country=="${entry.value}"){
				countryCode="${entry.key}";
			}
			</c:forEach>
			 //var countryCode = originAbc;
			 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
		     httpState.open("GET", url, true);
		     httpState.onreadystatechange = handleHttpResponse555555;
		     httpState.send(null);
		}
		function handleHttpResponse6666666666(){
				if (httpDState.readyState == 4){
		                var results = httpDState.responseText
		                results = results.trim();
		                res = results.split("@");
		                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'];
						targetElement.length = res.length;
		 				for(i=0;i<res.length;i++)
							{
							if(res[i] == ''){
							document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = '';
							document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = '';
							}else{
							stateVal = res[i].split("#");
							document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].text = stateVal[1];
							document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].options[i].value = stateVal[0];
							
							if (document.getElementById("destinationState").options[i].value == '${serviceOrder.destinationState}'){
							   document.getElementById("destinationState").options[i].defaultSelected = true;
							} } }
							document.getElementById("destinationState").value = '${serviceOrder.destinationState}';
		        } }          
		   function handleHttpResponse555555(){
		             if (httpState.readyState == 4){
		                var results = httpState.responseText
		                results = results.trim();
		                res = results.split("@");
		                targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.originState'];
						targetElement.length = res.length;
		 				for(i=0;i<res.length;i++)
							{
							if(res[i] == ''){
							document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = '';
							document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = '';
							}else{
							stateVal = res[i].split("#");
							document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].text = stateVal[1];
							document.forms['serviceOrderForm'].elements['serviceOrder.originState'].options[i].value = stateVal[0]; 
							if (document.getElementById("originState").options[i].value == '${serviceOrder.originState}'){
							   document.getElementById("originState").options[i].defaultSelected = true;
							} } }
							document.getElementById("originState").value = '${serviceOrder.originState}';
		             }   }
	  var httpState = getHTTPObjectState();
	  var httpDState = getHTTPObjectDState();
		     function getHTTPObjectState(){
		      var xmlhttp;
		      if(window.XMLHttpRequest) {
		          xmlhttp = new XMLHttpRequest();
		      }
		      else if (window.ActiveXObject)  {
		          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		          if (!xmlhttp)
		          {
		              xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		          }   }
		      return xmlhttp;
		  } 
		    function getHTTPObjectDState(){
		      var xmlhttp;
		      if(window.XMLHttpRequest)  {
		          xmlhttp = new XMLHttpRequest();
		      }
		      else if (window.ActiveXObject) {
		          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		          if (!xmlhttp)  {
		              xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		          }  }
		      return xmlhttp;
		  } 
</script> 
<script type="text/javascript">
try{
	appliedState();
destinationState11();
}
catch(e){}
</script>
 <script type="text/javascript"> 
	autoPopulate_customerFile_originCountry(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry']);
  	getState(document.forms['serviceOrderForm'].elements['serviceOrder.originCountry']);
  	 try{
  		 var enbState = '${enbState}';
  		 var oriCon=document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
  		 if(oriCon=="")	 {		
  				document.getElementById('originState').disabled = true;
  				document.getElementById('originState').value ="";
  		 }else{
  			 if(enbState.indexOf(oriCon)> -1){
  					document.getElementById('originState').disabled = false; 
  					document.getElementById('originState').value='${serviceOrder.originState}';
  				}else{
  					document.getElementById('originState').disabled = true;
  					document.getElementById('originState').value ="";
  				}  }  }
  	catch(e){}	 
 </script> 
<script type="text/javascript">
	autoPopulate_customerFile_destinationCountry(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry']);
 	getDestinationState(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry']);
 	try{	
 		 var enbState1 = '${enbState}';	
 		 var desCon=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
 		 if(desCon=="")	 {		
 				document.getElementById('destinationState').disabled = true;
 				document.getElementById('destinationState').value ="";
 		 }else{
 		  		if(enbState1.indexOf(desCon)> -1){
 					document.getElementById('destinationState').disabled = false;		
 					document.getElementById('destinationState').value='${serviceOrder.destinationState}';
 				}else{
 					document.getElementById('destinationState').disabled = true;
 					document.getElementById('destinationState').value ="";
 				} 	} }
 	catch(e){}
 	<c:if test="${empty serviceOrder.id}">
	autoPopulate_customerFile_statusDate(document.forms['serviceOrderForm'].elements['serviceOrder.statusDate']);
	</c:if>  
try{ 
 validateDistanceUnit();
	var f = document.getElementById('serviceOrderForm'); 
	f.setAttribute("autocomplete", "off"); 
}
catch(e){} 
try{
accountIdCheck();
} 
catch(e){}
	try{
	displayStatusReasonOnLoad(document.forms['serviceOrderForm'].elements['serviceOrder.status'].value);
  }
  catch(e){}
  try{
		displayQuoteAcceptReasonOnLoad(document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value);
	  }
	  catch(e){}
  try{
     var bookCode= document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
 }
 catch(e){}
    try{
if('${roleHitFlag}'!="OK")
 {	
//getConsultant();
 }  }
catch(e){}

	setTimeout("getJobList()", 2000);

try{
	//initLoader();
}catch(e){}

try{
    var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
    var hidStatusReg5 = document.getElementById('Service2');
    var hidStatusReg4 = document.getElementById('Service1');
    var labelserviceVar = document.getElementById('labelservice'); 
    var modehidStatusReg = document.getElementById('modeFild');
    var modelabelserviceVar = document.getElementById('modeLabel');
    var routinghidStatusReg = document.getElementById('routingFild');
    var routinglabelserviceVar = document.getElementById('routingLabel');
    var packhidStatusReg = document.getElementById('packFild');
    var packlabelserviceVar = document.getElementById('packLabel');
    var commodityStatusReg = document.getElementById('labCommudity');
    var commoditylabelserviceVar = document.getElementById('controlCommudity');  
   	if(job!=''){
   		if(job=='RLO'){
   		   hidStatusReg5.style.display = 'block';
   		   hidStatusReg4.style.display = 'none';
   		   labelserviceVar.style.display = 'none';
   		   modehidStatusReg.style.display = 'none';
   		   modelabelserviceVar.style.display = 'none';
   		   routinghidStatusReg.style.display = 'none';
   		   routinglabelserviceVar.style.display = 'none';
   		   packhidStatusReg.style.display = 'none';
   		   packlabelserviceVar.style.display = 'none';
   		commodityStatusReg.style.display = 'none';
		   commoditylabelserviceVar.style.display = 'none'; 
   		      }else{
   		   	   hidStatusReg4.style.display = 'block';
   			   hidStatusReg5.style.display = 'none';
   			   labelserviceVar.style.display = 'block';
   			   modehidStatusReg.style.display = 'block';
   			   modelabelserviceVar.style.display = 'block';
   			   routinghidStatusReg.style.display = 'block';
   			   routinglabelserviceVar.style.display = 'block';
   			   packhidStatusReg.style.display = 'block';
   			   packlabelserviceVar.style.display = 'block';	
   			commodityStatusReg.style.display = 'block';
 		   commoditylabelserviceVar.style.display = 'block'; 
   	    } }
}catch(e){}
try{
	 var job = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	 if(job=='RLO'){		
		checkColumn();
	 }
	 <c:if test="${empty serviceOrder.id}">
  	     fillCommodity();
	 </c:if>
}
catch(e){}
</script> 
<script type="text/javascript">
function confirmDelete1(targetElement,day,categoryType){
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
		var url = 'deleteQuotaionResource.html?serviceOrderId=${serviceOrder.id}&id='+encodeURI(targetElement)+'&day='+day+"&categoryType="+categoryType+day;
		location.href = url;
	 }else{
		return false;
	} } 
</script> 
<script type="text/javascript">
function createPrice(){
	var SOcontract = '${customerFile.contract}';
	var id='${serviceOrder.id}';
	if(SOcontract.length > 0){
		var conf = 'Continue with Quotation Contract: '+SOcontract;
		if(confirm(conf)){
			showOrHide(1);
			var url="createPrice.html?tempId="+encodeURI(id)+"&serviceOrderId=${serviceOrder.id}&contractName=" + SOcontract+"&shipNumber=${serviceOrder.shipNumber}";
			document.forms['serviceOrderForm'].action =url;
			document.forms['serviceOrderForm'].submit();
		}
	}else{
		alert('CustomerFile Contract not Found');
	} }
</script> 
<script type="text/javascript">
var http2 = getHTTPObject(); 
var http4 = getHTTPObject(); 
function handleHttpResponse2(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();
            var res = results.split("#"); 
            window.location.reload();
	}  }
function handleHttpResponse18(){
	if (http17.readyState == 4){
		showOrHide(0);
            var results = http17.responseText
            results = results.trim();
//            alert(results);
            if(results == '[]'){
            	document.getElementById(resoure_ID).value='';
            	alert('Resource already exist for this day');
            	resoure_ID='';
            }else if(results == ''){
            	document.getElementById(resoure_ID).value='';
            	alert('Resource already exist for this day');
            } }  }
function handleHttpResponse17(){
	if (http17.readyState == 4){
            var results = http17.responseText
            showOrHide(0);
            results = results.trim();
            if(results == '[]'){
            //	alert('Resource does not Exist.');
            	document.getElementById(resoure_ID).value='';
            	resoure_ID='';
            } }  } 
</script>
<script type="text/javascript">
	/* Methods added for ticket number: 6092*/
	function setVipImageReset(){
			if('${serviceOrder.vip}'.toString() == 'true' || '${customerFile.vip}'.toString() == 'true')
				document.getElementById('serviceOrder.vip').checked = true;	
			else
				document.getElementById('serviceOrder.vip').checked = false;	
			setVipImage();
	}
	function setVipImage(){
		var imgElement = document.getElementById('vipImage');
		if(document.getElementById('serviceOrder.vip').checked == true){
			imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
			imgElement.style.display = 'block';
		}
		else{
			if(('${serviceOrder.vip}'.toString() == 'true'  && document.getElementById('serviceOrder.vip').checked == true) || ('${customerFile.vip}'.toString() == 'true'  && document.getElementById('serviceOrder.vip').checked == true)){
				imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
				imgElement.style.display = 'block';
			}
			else{
				imgElement.src = '';
				imgElement.style.display = 'none';
			} } }
	window.onload = function(){
		<c:if test="${not empty dayRowFocus}">
			window.scrollTo(100,500);
		</c:if>
		<c:if test="${template == 'YES'}">
			window.scrollTo(100,500);
		</c:if>
		<c:if test="${not empty day}">
			window.scrollTo(100,500);
		</c:if> 
		getDestinationState(document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry']);
	}
</script> 
<script type="text/javascript"> 
	function checkValue(result){
		var targetID=document.forms['serviceOrderForm'].elements['tempId1'].value;
		var targetID2=document.forms['serviceOrderForm'].elements['tempId2'].value;
		
		document.getElementById(targetID).value=result.innerHTML;
		document.getElementById(targetID2).style.display = "none";
	}	 
	function checkSPChar(id) {
//		 valid(document.forms['entitlementForm'].elements['entitlement.eOption'],'special');
		 var chArr = new Array("%","&","|","\\","#",":","~","\"","\'");
		 var comments = document.getElementById('comments'+id).value;
	     for ( var i = 0; i < chArr.length; i++ ) {
		 	comments = comments.split(chArr[i]).join(''); 
		 }
		 document.getElementById('comments'+id).value = comments;
		 return true;
	 } 
	var resoure_ID='';
	var autoSave='';
	function autoSaveAjax(id,day,fieldName){
		if(autoSave == '' || fieldName != 'RESOURCE'){
			autoSave='YES';
			var category = document.getElementById('category'+id).value;
			var descript = document.getElementById('descript'+id).value;
			var cost = document.getElementById('cost'+id).value;
			var hour = document.getElementById('hour'+id).value;
			var comments = document.getElementById('comments'+id).value;
					
			if(category.length > 0 && descript.length > 0 && cost.length > 0 && hour.length > 0 ){
				if(checkNumberResource(cost,'cost'+id)){
					resoure_ID='descript'+id;
					showOrHide(1);
					var url = "autoSaveResouceTemplateAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}&day="+day+"&category="+category+"&resource="+encodeURIComponent(descript)+"&cost="+cost+"&comments="+comments+"&tempId="+id+"&estHour="+hour+"&btnType=addrow";
					http17.open("GET", url, true);
				    http17.onreadystatechange = handleHttpResponse18;
				    http17.send(null);
				} } } } 
	function setFlag(){
		autoSave='';
	} 
	function autoSaveAjax1(id,day,fieldName){
		if(autoSave==''){
			document.getElementById('descript'+id).value='';
			return;
		}
//			autoSave='YES';
			var category = document.getElementById('category'+id).value;
			var descript = document.getElementById('descript'+id).value;
			var cost = document.getElementById('cost'+id).value;
			var comments = document.getElementById('comments'+id).value;
					
			if(category.length > 0 && descript.length > 0 && cost.length > 0 ){
				if(checkNumberResource(cost,'cost'+id)){
					resoure_ID='descript'+id;
					var url = "autoSaveResouceTemplateAjax.html?ajax=1&decorator=simple&popup=true&category="+category+"&resource="+descript+"&cost="+cost+"&comments="+comments+"&tempId="+id+"&btnType=addrow";
					http17.open("GET", url, true);
				    http17.onreadystatechange = handleHttpResponse17;
				    http17.send(null);
				} } } 
function checkNumberResource(cost,costId){
	 if(parseInt(cost) == cost){
		 return true;
	 }else if(/^\d+\.\d+$/.test(cost)){
		  return true;
	 }else{
		 alert('Enter valid Number');
		 document.getElementById(costId).value='';
		 return false;
	 }
	return true;
}
</script>
<script type="text/javascript">
try{
	document.getElementById('serviceOrderForm').setAttribute("AutoComplete","off");
}catch(e){}
</script>
<script type="text/JavaScript">
function showOrHide(value) { 
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} } 
</script>
<script type="text/JavaScript">
<c:if test="${not empty dayRowFocus}">
	<c:if test="${dayRowFocus > 0}"> 
		document.getElementById('descript${dayRowFocus}').focus();
	</c:if>
</c:if>
</script>
<script type="text/JavaScript">
try{
	var disableChk = '${disableChk}';
	 if(disableChk == 'N') {		
		document.getElementById('weightnVolumeRadio').disabled = true;	
		document.getElementById('weightnVolumeRadio2').disabled = true;	  		
	 }
}catch(e){}
</script> 
<script type="text/javascript">
var httpTemplate = getHTTPObject(); 
function addResource1(day,listName,rowId){
	var catTempID = "catTempID"+day;
	var catTempVal = document.getElementById(catTempID).value;
	var id = '${serviceOrder.id}';
	if(id.length == 0){
		alert('Please Save Quote first before adding Resource.');
	}else if(catTempVal.trim() == ''){
		alert("Select any Category.");
		document.getElementById(catTempID).focus();
	}else{
		var categoryType='';
		if(catTempVal=='C'){categoryType='Crew'+day}else if(catTempVal=='M'){categoryType='Material'+day}else if(catTempVal=='E'){categoryType='Equipment'+day}
		var url = "addRowToResourceTemplateGrid.html?tempId="+id+"&shipNumber=${serviceOrder.shipNumber}&day="+day+"&type="+catTempVal+"&categoryType="+categoryType;
		document.forms['serviceOrderForm'].action =url;
		document.forms['serviceOrderForm'].submit();
	} }
var catagoryName='';
var dayId='';
function addResource(day,listName,rowId){
	var catTempID = "catTempID"+day;
	var catTempVal = document.getElementById(catTempID).value;
	var id = '${serviceOrder.id}';
	if(id.length == 0){
		alert('Please Save Quote first before adding Resource.');
	}else if(catTempVal.trim() == ''){
		alert("Select any Category for Day "+day);
		document.getElementById(catTempID).focus();
	}else{
		showOrHide(1);
		var categoryType='';
		if(catTempVal=='C'){categoryType='Crew'+day}else if(catTempVal=='M'){categoryType='Material'+day}else if(catTempVal=='E'){categoryType='Equipment'+day}else{categoryType='Vehicle'+day}
		catagoryName=categoryType;
		dayId=day;
		var url = "addRowToResourceTemplateGridAjax.html?ajax=1&tempId="+id+"&shipNumber=${serviceOrder.shipNumber}&day="+day+"&type="+catTempVal+"&categoryType="+categoryType;
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		httpTemplate.send(null);
	} } 
function getDayTemplate1(){
	var id = '${serviceOrder.id}';
	var contract = '${customerFile.contract}';
	if(id.length == 0){
		alert('Please Save Quote first before adding Resource Template.');
	}else {
		var url ="resouceTemplate.html?shipNumber=${serviceOrder.shipNumber}&contract=${customerFile.contract}&tempId=${serviceOrder.id}";
	    document.forms['serviceOrderForm'].action =url;
		document.forms['serviceOrderForm'].submit();
	} }
function findAllResource(){
		var resourceDiv = document.getElementById("resourcMapAjax");
	    //resourceDiv.style.display = 'none';
	    showOrHide(1);
		var url="findAllResourcesAjax.html?ajax=1&decorator=simple&popup=true&shipNumber=${serviceOrder.shipNumber}";
		httpResource.open("POST", url, true);
		httpResource.onreadystatechange = handleHttpResponseForResources;
		httpResource.send(null);
}
function getDayTemplate(){
	var id = '${serviceOrder.id}';
	var contract = '${customerFile.contract}';
	if(id.length == 0){
		alert('Please Save Quote first before adding Resource Template.');
	}else {
		showOrHide(1);
		var url ="resouceTemplateQuotation.html?ajax=1&shipNumber=${serviceOrder.shipNumber}&contract=${customerFile.contract}&tempId=${serviceOrder.id}";
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		httpTemplate.send(null);
	} }
function confirmDeleteResource(targetElement,day,categoryType){
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
		showOrHide(1);
		catagoryName=categoryType+day;
		dayId=day;
		var url = 'deleteQuotaionResourceAjax.html?ajax=1&serviceOrderId=${serviceOrder.id}&id='+encodeURI(targetElement)+'&day='+day+"&categoryType="+categoryType+day;
		httpTemplate.open("GET", url, true);
		httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
		httpTemplate.send(null);
	 }else{
		return false;
	} } 
function handleHttpResponseForTemplate(){
	if (httpTemplate.readyState == 4){
        var results= httpTemplate.responseText;
        findAllResource();
	} }
</script> 
<script type="text/javascript">
function toggle_visibility(id) {
    var e = document.getElementById(id);
    if(e != null && e != undefined){
    	if(e.style.display == 'block')
    	   e.style.display = 'none';
    	else
    	   e.style.display = 'block';
    } }
function setAnimatedColFocus(id){
		toggle_visibility("DayTable"+id);
		<c:if test="${serviceOrder.controlFlag == 'Q'}">
			document.getElementById('date'+id).style.display='none';
		</c:if>
}
function setAnimatedColCatFocus(id,day){
		toggle_visibility(id);
}
</script>
<script type="text/JavaScript">
var httpResource = getHTTPObject();
<%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}"> 
	<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
		setTimeout("volumeDisplay()", 1500); 
	</configByCorp:fieldVisibility>
</c:if> 
	function handleHttpResponseForResources(){
		if (httpResource.readyState == 3){
			volumeDisplay();
		}
		if (httpResource.readyState == 4){
			showOrHide(0);
            var results= httpResource.responseText; 
	 	    var resourceDiv = document.getElementById("resourcMapAjax");
	 	    resourceDiv.innerHTML = results;
	 	   	if(catagoryName !='' && dayId !=''){
	 	   		toggle_visibility("DayTable"+dayId);
	 			toggle_visibility("cat_"+catagoryName);
	 			<c:if test="${serviceOrder.controlFlag == 'Q'}">
	 				var dateHide = document.getElementById('date'+dayId);
		 			if(dateHide != null && dateHide != undefined){
		 				document.getElementById('date'+dayId).style.display='none';
		 			}
				</c:if>
				catagoryName='';
	 			dayId='';
	 	   	} 	} }
	function volumeDisplay(){
		var weightVolSectionId = document.getElementById('weightVolSection');
		var submitButn = document.getElementById('submitButtonId');
		var fieldVal="0";
		<configByCorp:fieldVisibility componentId="component.field.weightVolume.Visibility">
		fieldVal="1";
		</configByCorp:fieldVisibility>
		if(weightVolSectionId != null && weightVolSectionId != undefined){
			if(fieldVal=="0"){
				document.getElementById('weightVolSection').style.display = 'none';
			} }
		if(submitButn != null && submitButn != undefined){
			document.getElementById('submitButtonId').style.display = 'none';
		} }
	var isLoadRes='';
	function loadResources(){
		if(isLoadRes ==''){
			findAllResource();
			isLoadRes='YES';
		} }
	function checkQuotation(){ 
		var bookCode=document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
		 var  quotationStatus=document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value ;
		if(bookCode!=''&& quotationStatus=='AC'){
			var url="findBookAgentCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
			http45.open("GET", url, true);
			http45.onreadystatechange = handleHttpResponse45;
			http45.send(null);	
		} }
		function handleHttpResponse45(){
			if (http45.readyState == 4) {
		          var results = http45.responseText
		          results = results.trim();
		          var res = results.split("@"); 
		       		if(res.size() >= 2){ 
		       			if(res[2] == 'Approved'){
		           		}else{
		           			alert("Quote cannot be accepted as selected partners have not been approved yet");
		           			document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value='${serviceOrder.quoteStatus}';	
		           		}			
		      		}else{
		           		alert("Booking Agent code not valid");
		           		document.forms['serviceOrderForm'].elements['serviceOrder.quoteStatus'].value='${serviceOrder.quoteStatus}';
			   		}  } } 
		var http45 = getHTTPObject45();
		function getHTTPObject45() {
		    var xmlhttp;
		    if(window.XMLHttpRequest) {
		        xmlhttp = new XMLHttpRequest();
		    }
		    else if (window.ActiveXObject)
		    {
		        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		        if (!xmlhttp)
		        {
		            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		        }  }
		    return xmlhttp;
		}
</script>
<script type="text/javascript">
setTimeout("setOriginCityCode()",500);
function setOriginCityCode(){
try{
	document.forms['serviceOrderForm'].elements['serviceOrder.originCityCode'].value ='${serviceOrder.originCityCode}';
	document.forms['serviceOrderForm'].elements['serviceOrder.destinationCityCode'].value ='${serviceOrder.destinationCityCode}';
}catch(e){}
}
</script>
<script type="text/javascript">
 function findCustomInfo(position) {
	    	 var serviceid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	    	 var url="serviceInfo.html?ajax=1&decorator=simple&popup=true&serviceid=" + encodeURI(serviceid)
	    	  ajax_showTooltip(url,position);	
	    	  }
 </script>
 <script type="text/javascript">
 function getContract() {	
		var custJobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
		var contractBillCode = document.forms['serviceOrderForm'].elements['serviceOrder.billToCode'].value; 
		var custCreatedOn = document.forms['serviceOrderForm'].elements['serviceOrder.createdOn'].value; 
	    var custCompanyDivision=document.forms['serviceOrderForm'].elements['serviceOrder.companyDivision'].value;
	    var bookingAgentCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
	    if(custJobType!=''){
		var url="findContractbyJob.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType)+"&contractBillCode="+encodeURI(contractBillCode)+"&custCreatedOn="+encodeURI(custCreatedOn)+"&companyDivision="+encodeURI(custCompanyDivision)+"&bookingAgentCode="+encodeURI(bookingAgentCode);
	     http5.open("GET", url, true);
	     http5.onreadystatechange = handleHttpResponse3;
	     http5.send(null);
		}
	}
 function handleHttpResponse3()        { 
     if (http5.readyState == 4)             { 
        var results = http5.responseText
        results = results.trim();
        res = results.replace("[",'');
        res = res.replace("]",'');
        res = res.split("@"); 
        targetElement = document.forms['serviceOrderForm'].elements['serviceOrder.contract'];
		targetElement.length = res.length; 
			for(i=0;i<res.length;i++) {  		 
			if(res[i] == ''){						 
			document.forms['serviceOrderForm'].elements['serviceOrder.contract'].options[i].text = '';
			document.forms['serviceOrderForm'].elements['serviceOrder.contract'].options[i].value = '';
			}else{						 
			document.forms['serviceOrderForm'].elements['serviceOrder.contract'].options[i].text =res[i];
			document.forms['serviceOrderForm'].elements['serviceOrder.contract'].options[i].value =res[i];
			} }
			<c:if test="${empty serviceOrderForm.id}">	
						  
				//document.forms['serviceOrderForm'].elements['serviceOrderForm.contract'].value=document.forms['serviceOrderForm'].elements['defaultcontract'].value;
			</c:if> 
			<c:if test="${not empty serviceOrderForm.id}">					 	
			document.getElementById("contract").value='${serviceOrder.contract}';
			</c:if> 
     } }
 </script>
 <script language="javascript" type="text/javascript">
	 <configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
	 var modeType = document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value;
	 var jobType = document.forms['serviceOrderForm'].elements['serviceOrder.job'].value;
	 if((modeType=='Air'|| modeType=='Sea') && jobType!='RLO'){
   document.getElementById('carrierFild').style.display="block";
   document.getElementById('carrierFildLabel').style.display="block";
  }else{
   document.getElementById('carrierFild').style.display="none";
   document.getElementById('carrierFildLabel').style.display="none";
  }
  </configByCorp:fieldVisibility>
</script>
 <script type="text/javascript">
 function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
		document.getElementById(autocompleteDivId).style.display = "none";
		if(document.getElementById(paertnerCodeId).value==''){
			document.getElementById(partnerNameId).value="";	
		} } 
		function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
			lastName=lastName.replace("~","'");
			document.getElementById(partnerNameId).value=lastName;
			document.getElementById(paertnerCodeId).value=partnercode;
			document.getElementById(autocompleteDivId).style.display = "none";	
			if(paertnerCodeId=='quotesbookingAgentCodeId'){
				findBookingAgentName();
				findCompanyDivisionByBookAg();
			} }
</script> 
<%--
	<jsp:include page="/WEB-INF/pages/trans/pricingJavaScript.jsp" />
 --%>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
<script type="text/javascript">
function autoCompleterAjaxCallOriCity(){
var stateNameOri = document.forms['serviceOrderForm'].elements['serviceOrder.originState'].value;
var countryName = document.forms['serviceOrderForm'].elements['serviceOrder.originCountry'].value;
var cityNameOri = document.forms['serviceOrderForm'].elements['serviceOrder.originCity'].value;
var countryNameOri = "";
<c:forEach var="entry" items="${countryCod}">
if(countryName=="${entry.value}"){
	countryNameOri="${entry.key}";
}
</c:forEach>	
if(cityNameOri!=''){
	var data = 'leadCaptureAutocompleteOriAjax.html?ajax=1&stateNameOri='+stateNameOri+'&countryNameOri='+countryNameOri+'&cityNameOri='+cityNameOri+'&decorator=simple&popup=true';
		$("#originCity").autocomplete({
			source: data	
		});
} }
function autoCompleterAjaxCallDestCity(){
	var stateNameDes = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
	var countryName = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	var cityNameDes = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
	var countryNameDes = "";
	<c:forEach var="entry" items="${countryCod}">
	if(countryName=="${entry.value}"){
		countryNameDes="${entry.key}";
	}
	</c:forEach>
	if(cityNameDes!=''){
	var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
	$( "#destinationCity" ).autocomplete({				 
	      source: data		      
	    });
	} }
function findQuotationForms(position) { 
	  var url="findQuotationFormsAtQuotesAjax.html?ajax=1&decorator=simple&popup=true&sid=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${serviceOrder.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&formReportFlag=F";
	  ajax_showTooltip(url,position);	
	  }
function validatefields1(id,val){
	var url='viewReportWithParam.html?formFrom=list&id='+id+'&fileType='+val+'&jobNumber=${serviceOrder.shipNumber}&preferredLanguage=${customerFile.customerLanguagePreference}';
	location.href=url;	  
}
function winClose1(id,claimNumber,cid,jobNumber,regNumber,bookNumber,noteID,custID,emailOut,reportModule,reportSubModule,formReportFlag,formNameVal){
	var url = "viewFormParamEmailSetup.html?decorator=popup&popup=true&id="+id+"&jobNumber="+jobNumber+"&regNumber="+regNumber+"&docsxfer="+emailOut+"&reportModule="+reportModule+"&reportSubModule="+reportSubModule+"&formReportFlag="+formReportFlag+"&formNameVal="+formNameVal+"&claimNumber="+claimNumber+"&cid="+cid+"&bookNumber="+bookNumber+"&noteID="+noteID+"&custID="+custID;
	window.open(url,'accountProfileForm','height=650,width=750,top=1,left=200, scrollbars=yes,resizable=yes').focus();
} 
function checkGroupAgeJobType(){
	var job =document.forms['serviceOrderForm'].elements['serviceOrder.job'].value; 
	if(job=='GRP'){
	document.getElementById('serviceOrder.grpPref').checked = true;
	}else{
		document.getElementById('serviceOrder.grpPref').checked = false;
	} }
</script>
<script type="text/javascript">
var anchor = document.getElementsByTagName("a");
for( var i = 0, j =  anchor.length; i < j; i++ ) {
anchor[i].setAttribute( 'tabindex', '-1' );
}
</script>
<script type="text/javascript">
try{  	
		document.getElementById('tabindexFlag').focus();
	}catch(e){}
</script>
<script type="text/javascript">
try{
		<c:if test="${ usertype=='USER'}">
		window.onload =function ModifyPlaceHolder () {
			var input = document.getElementById ("quotesBookingAgentNameId");
		     input.placeholder = "Booking Agent Search";		   
	    }
		</c:if>
	}catch(e){}
	window.onload = function(){
	try{
	setVipImageReset();
	}catch(e){}
	}
</script>
<script language="javascript" type="text/javascript">
var commodityForUser = '${commodityForUser}' ;  
var soCommodity = document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value ;
var soId = '${serviceOrder.id}';
 if( commodityForUser != '' && (soId == null || soId == '')) {
	 if(soCommodity == null || soCommodity == '') {
	 document.forms['serviceOrderForm'].elements['serviceOrder.commodity'].value = commodityForUser ;
			} } 
</script> 