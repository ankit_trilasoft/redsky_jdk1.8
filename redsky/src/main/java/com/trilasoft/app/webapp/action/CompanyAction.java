package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Company;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.MenuItemManager;
import com.trilasoft.app.service.RefMasterManager;

public class CompanyAction extends BaseAction {

	
	private String sessionCorpID;
	private static Map<String,String>timeZone;
	private Map<String, String> multiCurrency;
	private Map<String, String> quoteservicesorder;
	private Map<String, String> redSkyLicenseTypeList;
	private static  Date createdon;
	private static Map<String,String> distanceFrom1;
	private static Map<String,String> distanceFrom2;
	private static Map<String,String> distanceFrom3;
	private static Map<String,String> groupAge;
	private static Map<String,String> accountLineNonEditableList;
	private String countryID;
	//Added for #7189
	private String status;	
    private String companyName;
	private String billGroup;
	private Map<String, String> CountryIdList;
	private static Map<String,String>rskyBillGroup;
	private Map<String, String> vatforInsuranceTax=new LinkedHashMap<String, String>();
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(CompanyAction.class);
	private List statusList;
	private Map<String, String> genericSurveyOnCfOrSoList; 
	 
    public CompanyAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        statusList = new ArrayList();
   	    statusList.add("All");
   	    statusList.add("Active");
   	    statusList.add("Inactive");
   	    status="Active";
		
	}
    
    private CompanyManager companyManager;
    private Company company;

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
    
    public List companies;
	private Long id;
	private Long maxId;
	private RefMasterManager refMasterManager;
	private MenuItemManager menuItemManager;

	public Long getMaxId() {
		return maxId;
	}

	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}

	public Map<String, String> getCountryIdList() {
		return CountryIdList;
	}

	public void setCountryIdList(Map<String, String> countryIdList) {
		CountryIdList = countryIdList;
	}

	public List getCompanies() {
		return companies;
	}

	public void setCompanies(List companies) {
		this.companies = companies;
	}

	public String CompanyList() { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 getComboList( sessionCorpID);

		 if(sessionCorpID == null){sessionCorpID = "";}
			if(companyName == null){companyName = "";}
			if(countryID == null){countryID = "";}
			if(billGroup == null){billGroup = "";}
			if(status == null){status = "";}
			companies = companyManager.getcompanyListBycompanyView(sessionCorpID, companyName, countryID,billGroup, status);
			//System.out.println("\n\n company List are :\t"+companies);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	    }
public String edit() {
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	   getComboList( sessionCorpID);
    	if (id != null)
	     {
    		//Existing Company 
	   		company = companyManager.get(id);
	   		vatforInsuranceTax=refMasterManager.findByParameterforVat(company.getCorpID(), "EUVAT");
      	 }
		else
		 {
			company= new Company();
			company.setCreatedOn(new Date());
			company.setUpdatedOn(new Date());
			vatforInsuranceTax=new LinkedHashMap<String, String>();
		 }
    	//	  System.out.println("\n\n CustomerRecord through ServiceOrder is :\t"+serviceOrder.getCustomerFile());
    	//  System.out.println("\n\n Bill To Name through CSO is :\t"+serviceOrder.getCustomerFile().getBillToName());
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
		}

 public Long getId() {
		return id;
  	}
	
 public void setId(Long id) {
		this.id = id;
  }
 public String save() throws Exception { 
	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	 getComboList( sessionCorpID);
     boolean isNew = (company.getId() == null);   
     company.setUpdatedOn(new Date());
     
   //  companyManager.save(company);   

     String key = (isNew) ? "company.added" : "company.updated";   
     saveMessage(getText(key));   
     if (!isNew) { 
    	 company.setCreatedOn(createdon);
    	 company.setUpdatedOn(new Date());
    	 vatforInsuranceTax=refMasterManager.findByParameter(company.getCorpID(), "EUVAT");
     	 company=companyManager.save(company);
     	 if((company!=null)&&(company.getSecurityChecked()!=null)&&(company.getSecurityChecked())){
     		logger.warn("Security Checked For "+sessionCorpID+">>"+company.getSecurityChecked()+":::::TRUE  Start");
			try{
				 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Populating security URL:::::::::::::: Start");
				menuItemManager.updateCurrentUrlPermission(sessionCorpID);
				 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Populating security URL::::::::::::::: End");
				}catch (Exception e) {}
			logger.warn("Security Checked For "+sessionCorpID+">>"+company.getSecurityChecked()+":::::TRUE  Start");
     	 }
     	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	 return SUCCESS;
     } else {
    	 company.setCreatedOn(new Date());
    	 company.setUpdatedOn(new Date());
     	 company=companyManager.save(company);
     	 if((company!=null)&&(company.getSecurityChecked()!=null)&&(company.getSecurityChecked())){
      		logger.warn("Security Checked For "+sessionCorpID+">>"+company.getSecurityChecked()+":::::TRUE  Start");
			try{
				 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Populating security URL:::::::::::::: Start");
				menuItemManager.updateCurrentUrlPermission(sessionCorpID);
				 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+"Populating security URL:::::::::::::::  End");
				}catch (Exception e) {}
				logger.warn("Security Checked For "+sessionCorpID+">>"+company.getSecurityChecked()+":::::TRUE  Start");
     	 }				
     	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
     	return SUCCESS;
     }
 }
 
 public String getComboList(String corpId){
	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	 multiCurrency=new HashMap<String, String>();
	 multiCurrency.put("Y", "Yes");
	 multiCurrency.put("N", "No");
	 redSkyLicenseTypeList = refMasterManager.findByParameter(corpId, "REDSKYLICENSETYPE");
	 timeZone = refMasterManager.findByParameter(corpId, "TIMEZONE");
	 groupAge=refMasterManager.findByParameter(corpId, "defaultgroupage");
	 distanceFrom1=refMasterManager.findByParameter(corpId, "DISTANCEFROM");
	 distanceFrom2=refMasterManager.findByParameter(corpId, "DISTANCEFROM");
	 distanceFrom3=refMasterManager.findByParameter(corpId, "DISTANCEFROM");
	 CountryIdList=refMasterManager.findByParameterWhareHouse(corpId, "COUNTRY") ;
	 rskyBillGroup = refMasterManager.findByParameter(corpId, "RSKYBillGroup");
	 accountLineNonEditableList= refMasterManager.findByParameter(corpId, "accountLineNonEditableList");
	 quoteservicesorder=new LinkedHashMap<String, String>();
	 quoteservicesorder.put("N/A","N/A");
	 quoteservicesorder.put("Single section","Single section");
	 quoteservicesorder.put("Separate section","Separate section");
	 
	 genericSurveyOnCfOrSoList = new LinkedHashMap<String, String>();
	 genericSurveyOnCfOrSoList.put("CF","Customer File");
	 genericSurveyOnCfOrSoList.put("SO", "Service Order");
	 
	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	 return SUCCESS;
 }

public static Map<String, String> getTimeZone() {
	return timeZone;
}

public static void setTimeZone(Map<String, String> timeZone) {
	CompanyAction.timeZone = timeZone;
}

public void setRefMasterManager(RefMasterManager refMasterManager) {
	this.refMasterManager = refMasterManager;
}
public void setMenuItemManager(MenuItemManager menuItemManager) {
	this.menuItemManager = menuItemManager;
}

public static Date getCreatedon() {
	return createdon;
}

public static void setCreatedon(Date createdon) {
	CompanyAction.createdon = createdon;
}

public Map<String, String> getMultiCurrency() {
	return multiCurrency;
}

public void setMultiCurrency(Map<String, String> multiCurrency) {
	this.multiCurrency = multiCurrency;
}

public static Map<String, String> getDistanceFrom1() {
	return distanceFrom1;
}

public static void setDistanceFrom1(Map<String, String> distanceFrom1) {
	CompanyAction.distanceFrom1 = distanceFrom1;
}

public static Map<String, String> getDistanceFrom2() {
	return distanceFrom2;
}

public static void setDistanceFrom2(Map<String, String> distanceFrom2) {
	CompanyAction.distanceFrom2 = distanceFrom2;
}

public static Map<String, String> getDistanceFrom3() {
	return distanceFrom3;
}

public static void setDistanceFrom3(Map<String, String> distanceFrom3) {
	CompanyAction.distanceFrom3 = distanceFrom3;
}

public static Map<String, String> getGroupAge() {
	return groupAge;
}

public static void setGroupAge(Map<String, String> groupAge) {
	CompanyAction.groupAge = groupAge;
}

public static Map<String, String> getRskyBillGroup() {
	return rskyBillGroup;
}

public static void setRskyBillGroup(Map<String, String> rskyBillGroup) {
	CompanyAction.rskyBillGroup = rskyBillGroup;
}

public Map<String, String> getQuoteservicesorder() {
	return quoteservicesorder;
}

public void setQuoteservicesorder(Map<String, String> quoteservicesorder) {
	this.quoteservicesorder = quoteservicesorder;
}

public static Map<String, String> getAccountLineNonEditableList() {
	return accountLineNonEditableList;
}

public static void setAccountLineNonEditableList(
		Map<String, String> accountLineNonEditableList) {
	CompanyAction.accountLineNonEditableList = accountLineNonEditableList;
}

public Map<String, String> getVatforInsuranceTax() {
	return vatforInsuranceTax;
}

public void setVatforInsuranceTax(Map<String, String> vatforInsuranceTax) {
	this.vatforInsuranceTax = vatforInsuranceTax;
}



public String getCompanyName() {
	return companyName;
}

public void setCompanyName(String companyName) {
	this.companyName = companyName;
}

public String getBillGroup() {
	return billGroup;
}

public void setBillGroup(String billGroup) {
	this.billGroup = billGroup;
}

public String getSessionCorpID() {
	return sessionCorpID;
}

public void setSessionCorpID(String sessionCorpID) {
	this.sessionCorpID = sessionCorpID;
}

public List getStatusList() {
	return statusList;
}

public void setStatusList(List statusList) {
	this.statusList = statusList;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

public String getCountryID() {
	return countryID;
}

public void setCountryID(String countryID) {
	this.countryID = countryID;
}

public Map<String, String> getGenericSurveyOnCfOrSoList() {
	return genericSurveyOnCfOrSoList;
}

public void setGenericSurveyOnCfOrSoList(
		Map<String, String> genericSurveyOnCfOrSoList) {
	this.genericSurveyOnCfOrSoList = genericSurveyOnCfOrSoList;
}

public Map<String, String> getRedSkyLicenseTypeList() {
	return redSkyLicenseTypeList;
}

public void setRedSkyLicenseTypeList(Map<String, String> redSkyLicenseTypeList) {
	this.redSkyLicenseTypeList = redSkyLicenseTypeList;
}


}
