<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/> 
    <style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

span.pagelinks {
display:block;
font-size:0.8em;
margin-bottom:4px;
margin-top:-22px;
padding:2px 0;
text-align:right;
width:100%
}
</style> 
<script type="text/javascript">

    function pick() {
  		//parent.window.opener.document.location.reload();
  		//document.forms['gridForm'].elements['btntype'].value=='';
  		//alert(document.forms['gridForm'].elements['btntype'].value);
  		//window.close();
	} 
	
	function backToBillingWizard()
	{
	var sid = document.forms['gridForm'].elements['sid'].value;
    window.location ="openBillingWizard.html?sid="+sid;
	}
	</script>
	<script type="text/javascript">
	 function getHTTPObject7()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http8 = getHTTPObject7(); 
    function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1(); 
    function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
	</script>
	<script type="text/javascript">
	function  findBookingAgent(temp)
        { 
        if(temp=='invoiceGen')
        {
         // alert("for invoce");
         <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
           //alert("no job validation");
           var serviceOrderId = document.forms['gridForm'].elements['sid'].value;
           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
           http8.open("GET", url, true);
           http8.onreadystatechange = handleHttpResponse1489;
           http8.send(null); 
          </c:if>
          <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
           //alert("if job uvl")
           var serviceOrderId = document.forms['gridForm'].elements['sid'].value;
           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
           http8.open("GET", url, true);
           http8.onreadystatechange = handleHttpResponse1499;
           http8.send(null); 
          </c:if>
        }
        }
       
        function handleHttpResponse1489()
        {
             var bookingagentCode = document.forms['gridForm'].elements['serviceOrder.bookingAgentCode'].value;
             if (http8.readyState == 4)
             {
                var results = http8.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results=='0')
                {
                 alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.")
                }
                else if(results!='0')
                {
                findGenerateInvoice();
                }
             }
        }
        function handleHttpResponse1499()
        {
             var bookingagentCode = document.forms['gridForm'].elements['serviceOrder.bookingAgentCode'].value;
             if (http8.readyState == 4)
             {
                var results = http8.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results=='0')
                {
                 alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.")
                }
                else if(results!='0')
                {
                findDistAmtGenerateInvoice();
                }
             }
        }
         function findGenerateInvoice(){
           var billToCode = document.forms['gridForm'].elements['sid'].value;
           var url="actualInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(billToCode);
           http3.open("GET", url, true);
           http3.onreadystatechange = handleHttpResponse14;
           http3.send(null);
      }

function handleHttpResponse14()
        {

             if (http3.readyState == 4)
             {
                var results = http3.responseText 
                results = results.trim();  
                if(results.length>2)
                { 
                  var res = results.split("@");
                  if(res.size() == 2)
                  { 
                    //alert(res[1]);
                    document.forms['gridForm'].elements['billToCodeForTicket'].value=res[1];
                    findBookingAgentName(res[1]); 
                  }
                  else
                  {
                  	alert("Cannot generate invoice as Multiple Bill Tos found for invoicing, please fix this.")
                  }
                    
 				}
                else 
                 { 
                   alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
                 }
             }
        } 
        function findBookingAgentName(target){
             
             var billToCode=target; 
             var sid = document.forms['gridForm'].elements['sid'].value;
             var url="accountList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(target);
             http2.open("GET", url, true);
             http2.onreadystatechange = handleHttpResponse4;
             http2.send(null);
}
   function handleHttpResponse4()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim(); 
                var res = results.split("@");  
                if(res[1]=='')
                {
                 
                  var shipNumberForTickets=document.forms['gridForm'].elements['serviceOrder.shipNumber'].value;
                  var sid=document.forms['gridForm'].elements['serviceOrder.id'].value;
 				   alert("Cannot Invoice, as Bill to Code is missing"); 		 
 				}
 				else if(res[2]=='')
 				{
 				 alert("Cannot Invoice, as Charge Code is missing");
 				}
 				else if(res[3]=='')
 				{
 				 alert("Cannot Invoice, as GL Code is missing in the 'Receivable Detail' of 'Accounting Transfer Information' section");
 				}
                else
                 {
                    var  jobtypeForTicket=document.forms['gridForm'].elements['serviceOrder.Job'].value ; 
                    var  billToCodeForTicket=document.forms['gridForm'].elements['billToCodeForTicket'].value;
                 	var shipNumberForTickets=document.forms['gridForm'].elements['serviceOrder.shipNumber'].value;
                	var sid=document.forms['gridForm'].elements['serviceOrder.id'].value;
                    //document.forms['gridForm'].elements['buttonType'].value = "invoice";
                    openWindow('activeWorkTickets.html?buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,300);
                 }
             }
        }             
         function findDistAmtGenerateInvoice(){ 
              var sid = document.forms['gridForm'].elements['sid'].value;
              var url="distAmtInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
              http3.open("GET", url, true);
              http3.onreadystatechange = handleHttpResponse1114;
              http3.send(null);
         }

function handleHttpResponse1114()
        { 
         if (http3.readyState == 4)
             {
                var results = http3.responseText 
                results = results.trim();  
                if(results.length>2)
                { 
                  var res = results.split("@");
                  if(res.size() == 2)
                  {  
                    document.forms['gridForm'].elements['billToCodeForTicket'].value=res[1]; 
                    findDistAmtBookAgent(res[1]); 
                  }
                  else
                  {
                  	alert("Cannot generate invoice as Multiple Bill Tos found for invoicing, please fix this.")
                  }
                    
 				}
                else 
                 { 
                   alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
                 }
             }
        }   
  
  
  function findDistAmtBookAgent(target){ 
             
             var billToCode=target; 
             var sid = document.forms['gridForm'].elements['sid'].value;
             var url="distAmtAccountList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(target);
             http2.open("GET", url, true);
             http2.onreadystatechange = handleHttpResponse4444;
             http2.send(null);
     }
function handleHttpResponse4444()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                alert(results);
                var res = results.split("@");  
                if(res[1]=='')
                { 
                  var shipNumberForTickets=document.forms['gridForm'].elements['serviceOrder.shipNumber'].value;
                  var sid=document.forms['gridForm'].elements['serviceOrder.id'].value;
 				   alert("Cannot Invoice, as Bill to Code is missing"); 		 
 				}
 				else if(res[2]=='')
 				{
 				 alert("Cannot Invoice, as Charge Code is missing");
 				}
 				else if(res[3]=='')
 				{
 				 alert("Cannot Invoice, as GL Code is missing in the 'Receivable Detail' of 'Accounting Transfer Information' section");
 				} 
 				else if(res[4]!=res[5])
 				{
 				   alert("Actual revenue total ("+ res[4]+") and distribution amount total ("+res[5]+") are not same. So, cannot generate invoice.")
 				}
                else
                 {
                    var  jobtypeForTicket=document.forms['gridForm'].elements['serviceOrder.Job'].value; 
                    var  billToCodeForTicket=document.forms['gridForm'].elements['billToCodeForTicket'].value;
                 	var shipNumberForTickets=document.forms['gridForm'].elements['serviceOrder.shipNumber'].value;
                	var sid=document.forms['gridForm'].elements['serviceOrder.id'].value;
                    //document.forms['gridForm'].elements['buttonType'].value = "invoice";
                    openWindow('activeWorkTickets.html?buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,300);
                 }
             }
        } 
	</script>
</head> 
<table><tr>
<td>
<div id="newmnav">
  <ul>
  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
   </sec-auth:authComponent>
   <li id="newmnav1" style="background:#FFF "><a href="openBillingWizard.html?sid=${serviceOrder.id}" class="current" ><span>Billing Wizard<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
  <li ><a href="accountLineList.html?sid=${serviceOrder.id}" ><span>Accounting</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <li ><a href="pricingList.html?sid=${serviceOrder.id}" ><span>Accounting</span></a></li>
  </sec-auth:authComponent>
   <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
   <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
     <c:if test="${serviceOrder.job =='INT'}">
      <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
     </c:if>
     </sec-auth:authComponent>
  <c:if test="${serviceOrder.job =='RLO'}">
  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
   <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </configByCorp:fieldVisibility>
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  
</div><div class="spn">&nbsp;</div>

<div style="padding-bottom:3px;"></div>
 <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0" style="width:850px;">
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<tbody>
	<tr> 
	<td align="left" class="listwhitebox" width="90%">
		<table class="detailTabLabel" border="0" width="98%">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td>
		  	<td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"   size="21"  cssClass="input-textUpper" readonly="true"/>
		  	<td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="15" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="21" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="3" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="3" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="5" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.routing"/></td>
		  	<td align="right"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="5" readonly="true"/></td>
		  	</tr>
		  	<tr>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td>
		  	<td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="15" readonly="true"/></td>
		  	<td align="right"><fmt:message key="billing.registrationNo"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="15" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="21" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="3" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="3" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td>
		  	<td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="30" readonly="true"/></td>
		  	</tr>
		  	<tr>
		  	<td align="left" height="5px"></td>
		  	</tr>
   		  </tbody>
	  </table>
	  </td></tr> 
<tr>
<td>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
<tbody>
<tr>
<td align="left" colspan="10"><img height="1px" width="850px" src="<c:url value='/images/vertlinedata.jpg'/>" /></td>
</tr>
											<tr>
											<td align="center" class="listwhitetext"><fmt:message key='labels.estimatedWeight'/></td>
											<c:if test="${serviceOrder.mode =='Air'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}" size="6" required="true" maxlength="10" readonly="true"/></td>
											</c:if>
											<c:if test="${serviceOrder.mode !='Air'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeight}" size="6" maxlength="10" required="true" readonly="true"/></td>
											</c:if>
											<td align="center" class="listwhitetext"><fmt:message key='labels.actualWeight'/></td>
											<c:if test="${serviceOrder.mode =='Air'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" size="6" required="true" maxlength="10" readonly="true" /></td>
											</c:if>
											<c:if test="${serviceOrder.mode !='Air'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeight}" size="6" required="true" maxlength="10" readonly="true"/></td>
											</c:if>
											<td align="left" ><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;" id="unit1" value="%{miscellaneous.unit1}" size="3"  maxlength="10" readonly="true"/></td>
										    <td align="right" class="listwhitetext"><fmt:message key='trackingStatus.beginLoad'/></td>
											<c:if test="${empty trackingStatus.loadA}">
											<c:if test="${empty trackingStatus.beginLoad}"> 
											<td><s:textfield cssClass="input-textUpper" name="loadingDate" value="${trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if>
											<c:if test="${not empty trackingStatus.beginLoad}">
											<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginLoad"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if>  
											</c:if>
											<c:if test="${not empty trackingStatus.loadA}">
											<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.loadA"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if>
											<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.deliveryShipper'/></td>
											<c:if test="${empty trackingStatus.deliveryA}">
											<c:if test="${empty trackingStatus.deliveryShipper}"> 
											<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:73px" maxlength="11" readonly="true"/></td>
											</c:if>
											<c:if test="${not empty trackingStatus.deliveryShipper}">
											<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryShipper"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if>  
											</c:if>
											<c:if test="${not empty trackingStatus.deliveryA}">
											<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryA"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if> 
											<td align="center" class="listwhitetext"><fmt:message key='labels.accountName1'/></td>
											<td align="left"><s:textfield cssClass="input-textUpper" name="customerFile.accountName" size="30"  maxlength="250" readonly="true"/></td>
											</tr>
											<tr>
											<td align="left" colspan="10"><img height="1px" width="850px" src="<c:url value='/images/vertlinedata.jpg'/>" /></td>
											</tr>
											
</tbody>
</table>
</td>
</tr> 
</tbody>
</table>
<table><tr>
<td><input type="button" class="cssbuttonA" style="width:130px; height:25px"   value="Back To Billing Wizard" onclick="backToBillingWizard();"/>


 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
<input type="button" class="cssbuttonA" style="width:110px; height:25px"
		value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/> 
</c:if>	
<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 

<input type="button" class="cssbuttonA" style="width:110px; height:25px"
		value="Generate Invoice" onclick="findBookingAgent('invoiceGen');"/> 
</c:if>	</td>
</tr></table>
</td></tr></table>

<%
	String editUrl = "<a onclick=javascript:void('nonInvListWizard.html?id=listRow[id]');>Edit</a>";
    String[][] tableMetadata = {
		{"nonInvoiceBillingList", "accountLine",""}, 	
      { "id", "basis", "chargeCode","recQuantity","recRate","checkNew","actualRevenue","distributionAmount","revisionRevenueAmount","estimateRevenueAmount","recGl","description"},
      { "long", "string", "string", "BigDecimal", "BigDecimal","string","BigDecimal","BigDecimal","BigDecimal","BigDecimal","string","string"},
      { "readonly", "readonly", "readonly", "editable", "editable","readonly","readonly","editable","readonly","readonly","readonly","editable"},
      
    };

    request.setAttribute("tableMetadata", tableMetadata);
    
    
%>
<script>

	function processOnChange(chgEvtObj){
		//   Typical steps...
		//1. Get current value 
		//2. Get values from other columns in the current row which are required for computation
		//3. Perform computation
		//4. Set result to required column/s in the row 
		
		var inputObj = chgEvtObj.target;
		var inputType = inputObj.type;
		var inputColumnName = getColumnNameFromObj(inputObj);
		var qtyValue = "";
		var returnedValue = "";

		if (inputColumnName == "recQuantity"){
			qtyValue = inputObj.value;
			returnedValue = getColumnValue("recRate");
			basisValue = getColumnValue("basis");
			setColumnValue("actualRevenue", computeActualQty(qtyValue, returnedValue,basisValue ));
		} else if (inputColumnName == "recRate"){
			qtyValue = getColumnValue("recQuantity");
			basisValue = getColumnValue("basis");
			returnedValue = inputObj.value;
			setColumnValue("actualRevenue", computeActualQty(qtyValue, returnedValue,basisValue ));
		}
	}

	function computeActualQty(qtyValue, returnedValue,basisValue){ 
	if(basisValue=='cwt')
	{
	return (qtyValue * returnedValue)/100;
	}
	else
	{
		return qtyValue * returnedValue;
	}
	}

</script>

<s:form id="gridForm" action="saveInvoiceBillingList.html?btntype=yes" method="post">  
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="accountInterface" value="${accountInterface}"/>
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/> 
<s:hidden name="recInvNumBilling" />
<s:hidden name="showAdd" value="No" />
<s:hidden name="listData" />
	<s:hidden name="listFieldNames" />	
	<s:hidden name="listFieldEditability" />	
	<s:hidden name="listFieldTypes" />		
	<s:hidden name="listIdField" value="id"/>	
	<s:hidden name="listIdFieldType" value="long"/>	
<s:hidden name="shipNumber" value="${shipNumber}"/>  
<s:hidden name="basis" value="${nonInvoiceBillingList}"/>  
<s:hidden name="checkNew" value="${nonInvoiceBillingList}"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/> 
<s:hidden name="serviceOrder.bookingAgentCode"/>
<s:hidden name="serviceOrder.shipNumber"/>
<s:hidden name="serviceOrder.id"/>
<s:hidden name="serviceOrder.Job"/> 
<s:hidden name="billToCodeForTicket"/>
<s:set name="nonInvoiceBillingList" value="nonInvoiceBillingList" scope="request"/>  
	<table border="0" style="width:850px"> 
	<tr>
	
	<td>
<!--<display:table name="nonInvoiceBillingList" class="table" requestURI="" id="nonInvoiceBillingList" style="width:100%" defaultsort="1" pagesize="100" >   
 		<display:column property="chargeCode" sortable="true" title="charge Code" style="width:70px" />
		<display:column property="recQuantity" sortable="true" title="Quantity" style="width:70px" />
		<display:column property="recRate" sortable="true" title="Rate" style="width:70px" />
		<display:column property="basis" sortable="true" title="Unit" style="width:70px" />
		<display:column property="checkNew" sortable="true" title="Item(per or X)" style="width:70px" />
		<display:column  sortable="true" title="Amount" style="width:70px" >
		 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${nonInvoiceBillingList.actualRevenue}" /></div>
        </display:column> 
        <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
        <display:column  sortable="true" title="Dist Amt" style="width:70px" >
		   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${nonInvoiceBillingList.distributionAmount}" /></div>
        </display:column>    
        </c:if>         
		<display:column  sortable="true" title="Revised Amt" style="width:70px" >
		   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${nonInvoiceBillingList.revisionRevenueAmount}" /></div>
        </display:column>    
	    <display:column  sortable="true" title="Est Amt" style="width:70px">
	    <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${nonInvoiceBillingList.estimateRevenueAmount}" /></div>
        </display:column> 
	    <display:column property="recGl" sortable="true" title="GL Code" style="width:70px" />
	     <display:column property="description" sortable="true" title="Description" style="width:70px" />
<display:footer>
       <tr>
        <td align="right" colspan="5" style="  "><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
        <td align="right" width="70px" style=" "><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${actRevSumBilling}" /></div></td>
        <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
        <td align="right" width="70px" style=" "><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${distAmtSumBilling}" /></div></td>
        </c:if>          
        <td align="right" width="70px" style=""><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${revRevAmtSumBilling}" /></div></td>
        <td align="right" width="70px" style=" "><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${estRevAmtSumBilling}" /></div></td>
        <td colspan="2"></td>  
        </tr>
 </display:footer>
</display:table> 




--></td>
</tr> 
</table> 
</s:form> 
<script type="text/javascript">   

try{
if(document.forms['gridForm'].elements['btntype'].value=='yes'){
pick();
} 
}
catch(e){}
</script>  

<%@ include file="/common/enable-dojo-grid.jsp" %>