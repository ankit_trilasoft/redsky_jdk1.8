<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Storage List</title>
<meta name="heading" content="Storage List" />
<style type="text/css">
  h2 {background-color: #FBBFFF}
	div.exportlinks {
	margin:-5px 0px 10px 10px;
	padding:2px 4px 2px 0px;
	!padding:2px 4px 18px 0px;
	width:100%;
	}
		
	#otabs{width:500px;}
	form {
margin-top:-40px;
!margin-top:-5px;
}
div#main {
margin:-5px 0 0;
}

</style>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript">
     var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
</script>

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
}
div.error, span.error, li.error, div.message {
width:450px;
}
</style>

<script type="text/javascript">
function dispatchMap(){
var date1 = document.forms['searchForm'].elements['workTicket.date1'].value;
var date2 = document.forms['searchForm'].elements['workTicket.date2'].value;
	if((date1 != '') && (date2 != '')){
		if(date1 == date2){
			window.open("dispathMapWorkTicket.html?date1="+date1+"&date2="+date2+"&decorator=popup&popup=true&from=main","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
		}else{
			alert('Begin date and end date should be equal to view dispatch map.');
			return false;
		}
	}else{
		alert('Begin date and end date should be equal to view dispatch map.');
		return false;
	}
}
</script>
</head>

<s:form id="searchForm" action="searchWorkTickets" method="post">
<div id="Layer1" style="width:100%">
<s:hidden name="formStatus" value=""/>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="shipnum" value="${shipnum}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
<c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
<div style="margin-top:50px; ">
	<table width="100%" border="0" style="margin:0px" cellpadding="0" cellspacing="0">
		<tr>
			<td id="otabs" width="10%"> 
				<ul><li><a class="current"><span>Storage<img src="images/navarrow.gif" align="absmiddle" /></span></a></li></ul>
			</td>
			<td width="90%"> </td>
		</tr>
	</table>
			
	<s:set name="workTicketStorageExt" value="workTicketStorageExt" scope="request" />
	<display:table name="workTicketStorageExt" class="table" requestURI="" id="workTicketList" export="true"  pagesize="50"  style="width:100%;margin-top:0px;!margin-top:0px;">
		<display:column property="shipNumber" sortable="true" title="ShipNumber" style="width:50px"/>
		<display:column property="ticket" sortable="true" titleKey="workTicket.ticket" style="width:30px"/>
		<display:column property="description" sortable="true" title="Description" style="width:80px"/>
		<display:column property="locationId" sortable="true" title="Location" style="width:50px"/>
		<display:column property="pieces" sortable="true" title="Pieces" style="width:20px"/>
		<display:column property="releaseDate" sortable="true" style="width:30px" title="Release Date" format="{0,date,dd-MMM-yyyy}"/>
		<display:column property="containerId" sortable="true" style="width:20px" title="Container" />
		<display:column property="itemNumber" sortable="true" style="width:20px" title="Item Number" />	
		<display:setProperty name="export.excel.filename" value="WorkTicket List.xls" />
		<display:setProperty name="export.csv.filename" value="WorkTicket List.csv" />
		<display:setProperty name="export.pdf.filename" value="WorkTicket List.pdf" />
	</display:table>
</div>

</div>
</s:form>
