package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Userlogfile; 
public interface UserlogfileManager  extends GenericManager<Userlogfile, Long>{

	int updateUserLogOut(String sessionId);

	public List search(String userName, String userType, Date login);

	public List getexcludeIPsList(String CorpID);

	public List getPermission(String CorpID, String userName);

	public List getAssignedSecurity(String sessionCorpID, Long partnerId);

	public List searchuUserLogFile(String sessionCorpID, String userNameSearch, String userTypeSearch, String loginDateSearch, String excludeIPsList);

	public List findLogTime(String sessionCorpID, String userNamePermission, String loginDate);

}
