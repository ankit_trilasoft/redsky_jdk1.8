<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
    <title>Driver Details</title>
    <meta name="heading" content="Driver Details:"/>
    <style><%@ include file="/common/calenderStyle.css"%></style>
   
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	

<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
	</script>

<script>
function truckDriverList(){
	var localNumber = document.forms['driverDetailsForm'].elements['truckNumber'].value;
	var ownerPayTo = document.forms['driverDetailsForm'].elements['partnerCode'].value;
	var truckId = document.forms['driverDetailsForm'].elements['truckId'].value;
	var truckDriverType = document.forms['driverDetailsForm'].elements['truckDriverType'].value;
	var truckDescription = document.forms['driverDetailsForm'].elements['truckDescription'].value;
	var url="driverList.html?truckNumber=" + encodeURI(localNumber) + "&partnerCode="+ encodeURI(ownerPayTo)+ "&truckId="+ encodeURI(truckId)+ "&truckDriverType="+ encodeURI(truckDriverType)+ "&truckDescription="+ encodeURI(truckDescription);
	location.href = url;
	
} 
 
function validation(){
if(document.forms['driverDetailsForm'].elements['truckDrivers.driverCode'].value==''){
   alert("Code is a required field ")
   return false;
 }	
	}

function addDriverList(){
    var truckId = document.forms['driverDetailsForm'].elements['truckId'].value;
	var truckNumber = document.forms['driverDetailsForm'].elements['truckNumber'].value;
	var partnerCode = document.forms['driverDetailsForm'].elements['partnerCode'].value;
	var truckDescription = document.forms['driverDetailsForm'].elements['truckDescription'].value;
	var truckDriverType = document.forms['driverDetailsForm'].elements['truckDriverType'].value;
	
	var url="editDriverDetail.html?truckId=" + encodeURI(truckId) + "&truckNumber="+ encodeURI(truckNumber)+ "&partnerCode="+ encodeURI(partnerCode)+ "&truckDescription="+ encodeURI(truckDescription)+ "&truckDriverType="+ encodeURI(truckDriverType);
	location.href = url;
	
}	

function findDriverFromPartner(){
 document.forms['driverDetailsForm'].elements['validateFlagDriver'].value='OK'; 
 openWindow('truckDriversList.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=truckDrivers.driverName&fld_code=truckDrivers.driverCode');
 }


function validateListForDriver(){
 if(document.forms['driverDetailsForm'].elements['validateFlagDriver'].value=='OK'){
        var parentId=document.forms['driverDetailsForm'].elements['truckDrivers.driverCode'].value;
        var deliveryLastDay=document.forms['driverDetailsForm'].elements['deliveryLastDay'].value;                
	    var url="findControlExpirationsListForDriver.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidateDriver.open("GET", url, true);
	    httpValidateDriver.onreadystatechange = handleHttpResponseDriver;
	    httpValidateDriver.send(null);	    
}
}
function handleHttpResponseDriver(){
		if (httpValidateDriver.readyState == 4){
                var results = httpValidateDriver.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Driver cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['driverDetailsForm'].elements['truckDrivers.driverCode'].value ="";
           		 document.forms['driverDetailsForm'].elements['truckDrivers.driverName'].value ="";
                 }else{
                 var agree =confirm("This Driver has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     document.forms['driverDetailsForm'].elements['truckDrivers.driverName'].select();
			     } 
			     else 
			     {   
			         document.forms['driverDetailsForm'].elements['truckDrivers.driverCode'].value ="";
           		     document.forms['driverDetailsForm'].elements['truckDrivers.driverName'].value ="";
			     }
                 }
                 } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Driver cannot be selected due to the expirations: "+ExpiryFor);                 
			     document.forms['driverDetailsForm'].elements['truckDrivers.driverCode'].value ="";
           		 document.forms['driverDetailsForm'].elements['truckDrivers.driverName'].value ="";
                 }else{                 
                 var agree =confirm("This Driver has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {
			     document.forms['driverDetailsForm'].elements['truckDrivers.driverName'].select();
			     } 
			     else 
			     {   
			         document.forms['driverDetailsForm'].elements['truckDrivers.driverCode'].value ="";
           		     document.forms['driverDetailsForm'].elements['truckDrivers.driverName'].value ="";
			     }
                 }
                 }
                 }               
      }
      document.forms['driverDetailsForm'].elements['validateFlagDriver'].value='';
      
      }
      
var httpValidateDriver = getHTTPObjectValidateDriver();
function getHTTPObjectValidateDriver(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
</script> 	    
</head>

<s:form id="driverDetailsForm" action="saveDriverDetails.html" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="truckDrivers.parentId" value="<%=request.getParameter("parentId") %>"/>
<s:hidden name="truckDrivers.id" />
<s:hidden name="validateString" value="${truckDrivers.driverCode}"/>
<s:hidden name="validateFlagDriver"/>
<s:hidden name="deliveryLastDay"/>

<s:hidden name="primaryFlagId"   value="${primaryFlagId}"/>
<c:set var="primaryFlagId"  value="${primaryFlagId}"/>
<s:hidden name="primaryFlags" value="${primaryFlags}"/>
<s:hidden name="truckNumber" value="<%=request.getParameter("truckNumber") %>" />
<c:set var="truckNumber" value="<%=request.getParameter("truckNumber") %>" />
<s:hidden name="partnerCode" value="<%=request.getParameter("partnerCode") %>" />
<c:set var="partnerCode" value="<%=request.getParameter("partnerCode") %>" />
<s:hidden name="truckId" value="<%=request.getParameter("truckId") %>" />
<c:set var="truckId" value="<%=request.getParameter("truckId") %>" />
<s:hidden name="truckDriverType" value="<%=request.getParameter("truckDriverType") %>" />
<c:set var="truckDriverType" value="<%=request.getParameter("truckDriverType") %>" />
<s:hidden name="truckDescription" value="<%=request.getParameter("truckDescription") %>" />
<c:set var="truckDescription" value="<%=request.getParameter("truckDescription") %>" />
<c:set var="buttons">   
    <input type="button" value="Add" class="cssbuttonA" style="width:55px; height:25px" onclick="addDriverList();"/>
</c:set>
<s:hidden name="id" value="${truckDrivers.id}"/>
<div id="layer1" style="margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Driver Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           <li><a href="editTruck.html?id=${truckId}"><span>Truck Details</span></a></li>
           <li><a href="#" onclick="truckDriverList();"><span>Driver</span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      
<div id="Layer5" style="width:95%" onkeydown="changeStatus();">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="95%" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr><td height="5px"></td></tr>
			<tr>
				<td align="right" class="listwhitetext" width="15px"></td>
		  		<td align="right" class="listwhitetext" width="">Truck #</td>
				<td colspan="2"><s:textfield cssClass="input-textUpper" name="truckDrivers.truckNumber"  value="${truckNumber}" readonly="true" size="40"/></td>
				<td align="right" class="listwhitetext" width="30px"></td>
				<td align="right" class="listwhitetext">Truck&nbsp;Description</td>
				<td><s:textfield value="${truckDescription}" cssClass="input-textUpper" readonly="true" size="40"/></td>
			</tr>			
			<tr>
				<td align="right" class="listwhitetext" width="15px"></td>
		  		<td align="right" class="listwhitetext">Truck&nbsp;Type</td>
				<td colspan="2"><s:textfield  value="${truckDriverType}" cssClass="input-textUpper" size="40" readonly="true"/></td>
				<td align="right" class="listwhitetext" width="30px"></td>
				<td align="right" class="listwhitetext">Owner/Pay To</td>
				<td><s:textfield cssClass="input-textUpper" name="truckDrivers.partnerCode" value="${partnerCode}" readonly="true" size="40"/></td>
			</tr>			
			<tr>
				<td align="right" class="listwhitetext" width="15px"></td>
				<td align="right" class="listwhitetext">Code<font color="red" size="2">*</font></td> 
				<td align="left" class="listwhitetext" valign="top" width="60px"><s:textfield name="truckDrivers.driverCode" maxlength="30" required="true" readonly="true" cssClass="input-textUpper" size="15" /></td>
				<td align="left" width="114px"><img class="openpopup" width="17" height="20" onclick="findDriverFromPartner();" src="<c:url value='/images/open-popup.gif'/>" /></td>
				<td align="right" class="listwhitetext" width="30px"></td>
				<td align="right" class="listwhitetext">Name</td> 
				<td align="left" class="listwhitetext" valign="top"><s:textfield name="truckDrivers.driverName" maxlength="30"  required="true" readonly="true" cssClass="input-textUpper" size="40" /></td>
			</tr>
			<tr>
			<c:if test="${primaryFlagId==truckDrivers.id && primaryFlags=='true'}">
				<c:set var="ischeckedddd" value="false"/>
				<c:if test="${truckDrivers.primaryDriver}">
					<c:set var="ischeckedddd" value="true"/>
				</c:if>
				<td align="right" colspan="6"  class="listwhitetext">Primary</td>
				<td align="left" colspan="1"><s:checkbox key="truckDrivers.primaryDriver" cssStyle="margin:0px;"  value="${ischeckedddd}" fieldValue="true" /></td>
		    </c:if>
		    
		    <c:if test="${primaryFlags=='show' || primaryFlags=='false'}">
				<c:set var="ischeckeddddsss" value="false"/>
				<c:if test="${truckDrivers.primaryDriver}">
					<c:set var="ischeckedddd" value="true"/>
				</c:if>
				<td align="right" colspan="6"  class="listwhitetext">Primary</td>
				<td align="left" colspan="1"><s:checkbox key="truckDrivers.primaryDriver" cssStyle="margin:0px;" value="${ischeckeddddsss}" fieldValue="true" /></td>
		    </c:if>
			</tr>
<tr>
<td align="right" class="listwhitetext" width="15px"></td>						
</tr>
 
<tr><td align="left" height="15px"></td></tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 
<table width="750px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${truckDrivers.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="truckDrivers.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${truckDrivers.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty truckDrivers.id}">
								<s:hidden name="truckDrivers.createdBy"/>
								<td ><s:label name="createdBy" value="%{truckDrivers.createdBy}"/></td>
							</c:if>
							<c:if test="${empty truckDrivers.id}">
								<s:hidden name="truckDrivers.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${truckDrivers.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="truckDrivers.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${truckDrivers.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty truckDrivers.id}">
							<s:hidden name="truckDrivers.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{truckDrivers.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty truckDrivers.id}">
							<s:hidden name="truckDrivers.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div> 
   <table class="detailTabLabel"><tr><td>   
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple" onclick="return validation();"/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
        <c:if test="${not empty truckDrivers.id}">
	<c:out value="${buttons}" escapeXml="false" />
     </c:if>
	</td></tr></table>
</div>
<s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" />
<s:hidden name="seventhDescription" />
</s:form> 
<div id="mydiv" style="position:absolute;top:110px;"></div>
<script type="text/javascript">
 </script>
    