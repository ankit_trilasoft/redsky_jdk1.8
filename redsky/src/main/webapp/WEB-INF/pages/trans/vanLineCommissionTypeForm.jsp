<%@ include file="/common/taglibs.jsp"%> 

<head>
	<meta name="heading" content="<fmt:message key='vanLineCommissionTypeForm.title'/>"/> 
	<title><fmt:message key="vanLineForm.title"/></title>
	
<style type="text/css">
		h2 {background-color: #FBBFFF}		
</style>
<script>
function saveAuto(clickType){
	
	if ('${autoSavePrompt}' == 'No'){
		var noSaveAction;
		if(document.forms['vanLineCommissionTypeForm'].elements['gotoPageString'].value == 'gototab.vanlineglcomm'){
			noSaveAction = 'vanLineCommissionTypeList.html';
		}
		processAutoSave(document.forms['vanLineCommissionTypeForm'],'saveVanLineCommissionType!saveOnTabChange.html', noSaveAction );

	}
	else{
     if(!(clickType == 'save')){
       var id = document.forms['vanLineCommissionTypeForm'].elements['vanLineCommissionType.id'].value;
	   if (document.forms['vanLineCommissionTypeForm'].elements['formStatus'].value == '1'){
	       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='vanLineForm.title'/>");
		       if(agree){
		           document.forms['vanLineCommissionTypeForm'].action = 'saveVanLineCommissionType!saveOnTabChange.html';
		           document.forms['vanLineCommissionTypeForm'].submit();
		       }else{
			      if(id != ''){
			           if(document.forms['vanLineCommissionTypeForm'].elements['gotoPageString'].value == 'gototab.vanlineglcomm'){
				               location.href = 'vanLineCommissionTypeList.html';
				         }
				     }
       			}
   			}else{
   				if(id != ''){
			  	   if(document.forms['vanLineCommissionTypeForm'].elements['gotoPageString'].value == 'gototab.vanlineglcomm'){
			               location.href = 'vanLineCommissionTypeList.html';
			       }
   				}
   			}
   		}
   	}		
  }


function changeStatus(){
   document.forms['vanLineCommissionTypeForm'].elements['formStatus'].value = '1';
}


function checktextArea(){
	var formula=document.forms['vanLineCommissionTypeForm'].elements['vanLineCommissionType.calculation'].value
	formula=formula.trim();
	if(formula=="")
		{
	  		alert('Please enter the formula in the text Area');
	  		return false;
	  		
    	 }
    	 testChargeFormula();
	}
	
function testChargeFormula(){
     var expressionFormula=document.forms['vanLineCommissionTypeForm'].elements['vanLineCommissionType.calculation'].value;
	 var url="testFormulaForVanLine.html?ajax=1&decorator=simple&popup=true&expressionFormula=" +encodeURI(expressionFormula);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponsetestFormula;
     http2.send(null);
	}	
	
function handleHttpResponsetestFormula()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                alert(results);
             }
        }
        
        
        function onlyFloatNumsAllowed(evt)
	    { 
	       var keyCode = evt.which ? evt.which : evt.keyCode;
	        //alert(keyCode);
	      return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||(keyCode==109); 
	    }
	
	
	function percentFloatCheck()
	{ 
	var paercent1=document.forms['vanLineCommissionTypeForm'].elements['vanLineCommissionType.pc'].value 
	if(paercent1.indexOf("-")==0)
	{ 
	  paercent1=paercent1.substring(1,paercent1.length);
	}
	if(paercent1>=1000)
	{
	  alert("Percent value should be less than 1000")
	  document.forms['vanLineCommissionTypeForm'].elements['vanLineCommissionType.pc'].value='0';
	}
     if (checkFloatWithArth('vanLineCommissionTypeForm','vanLineCommissionType.pc','Invalid data in Percent') == false)
           {
              document.forms['vanLineCommissionTypeForm'].elements['vanLineCommissionType.pc'].value='0';
              
              return false
           }
         var percent= document.forms['vanLineCommissionTypeForm'].elements['vanLineCommissionType.pc'].value 
         percentRound=Math.round(percent*100)/100; 
         document.forms['vanLineCommissionTypeForm'].elements['vanLineCommissionType.pc'].value=percentRound;     
   }
   
        
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();

</script>
<style type="text/css">
/* collapse */

</style>

</head>
<body style="background-color:#444444;">
<s:form id="vanLineCommissionTypeForm" name="vanLineCommissionTypeForm" action="saveVanLineCommissionType" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="vanLineCommissionType.id" value="%{vanLineCommissionType.id}"/>

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
	<c:choose>
		<c:when test="${gotoPageString == 'gototab.vanlineglcomm'}">
		   <c:redirect url="/vanLineCommissionTypeList.html"/>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose> 
</c:if>

<div id="Layer1" onkeydown="changeStatus();" style="width:750px" >
<div id="newmnav">
		  <ul>
		  	<%-- <li><a href="vanLineCommissionTypeList.html"><span>Commission Type List</span></a></li> --%>
		  	<li><a onclick="setReturnString('gototab.vanlineglcomm');return saveAuto('none');"><span>Commission Type List</span></a></li>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Commission Type Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		   <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${vanLineCommissionType.id}&tableName=vanlinecommissiontype&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		    
  </ul>
</div><div class="spn">&nbsp;</div>

<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:0px;!margin-top:0px;"><span></span></div>
<div class="center-content">	

  	<table class="detailTabLabel" border="0" width="100%">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="10px"></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  	   	<td align="right" class="listwhitetext"><fmt:message key="vanLineCommissionType.code"/></td>
		  		<td align="left" width="50px">
		  		<c:if test="${empty vanLineCommissionType.id}">
		  			<s:textfield name="vanLineCommissionType.code"  size="20"   cssClass="input-text" cssStyle="width:150px" tabindex="1"/>
		  		</c:if>	
		  		<c:if test="${not empty vanLineCommissionType.id}">
		  			<s:textfield name="vanLineCommissionType.code"  size="20"   cssClass="input-text" cssStyle="width:150px" readonly="true" tabindex="1"/>
		  		</c:if>
		  		</td>
		  		<td align="right" width="23px">
		  		<td align="right" width="15px" class="listwhitetext"><fmt:message key="vanLineCommissionType.type"/></td>
		  		<td align="left"><s:select cssClass="list-menu"   name="vanLineCommissionType.type" list="{' ','D','S','P'}" cssStyle="width:90px" onchange="changeStatus();" tabindex="2"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  		<td align="right" class="listwhitetext"><fmt:message key="vanLineCommissionType.description"/></td>
		  		<td align="left" colspan="5"><s:textfield name="vanLineCommissionType.description"  size="50"  cssClass="input-text" cssStyle="width:300px" tabindex="3"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  	   	<td align="right" class="listwhitetext" ><fmt:message key="vanLineCommissionType.glCode"/></td>
		  		<td align="left" colspan="5"><s:select cssClass="list-menu" name="vanLineCommissionType.glCode" headerKey=" " headerValue=" " list="%{glCode}" cssStyle="width:302px" onchange="changeStatus();" tabindex="4"/></td> 
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  		<td align="right" class="listwhitetext"><fmt:message key="vanLineCommissionType.pc"/></td>
		  		<td align="left" colspan="2"><s:textfield name="vanLineCommissionType.pc"  size="50"  cssClass="input-text" cssStyle="width:150px" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)" tabindex="5"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		  		<td align="right" width="30px">
		  		<td align="right" valign="top" class="listwhitetext"><fmt:message key="vanLineCommissionType.calculation"/></td>
		  		<td align="left" colspan="5" ><s:textarea name="vanLineCommissionType.calculation" cols="85" rows="10" tabindex="6" cssClass="textarea"/></td>
		  		<td align="left" class="listwhitetext"><input type="button" class="cssbutton" name="TestFormula" value="Test Formula" style="width: 100px;height: 30px" onclick="checktextArea();" tabindex="7"/></td>
		  	</tr>
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	
		  	<tr>
		  		<td align="left" height="10px"></td>
		  	</tr>
		</tbody>
	</table> 
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	
	   
 	<table class="detailTabLabel" border="0">

						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLineCommissionType.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="vanLineCommissionTypeCreatedOnFormattedValue" value="${vanLineCommissionType.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="vanLineCommissionType.createdOn" value="${vanLineCommissionTypeCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${vanLineCommissionType.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLineCommissionType.createdBy' /></b></td>
							<c:if test="${not empty vanLineCommissionType.id}">
								<s:hidden name="vanLineCommissionType.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{vanLineCommissionType.createdBy}"/></td>
							</c:if>
							<c:if test="${empty vanLineCommissionType.id}">
								<s:hidden name="vanLineCommissionType.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLineCommissionType.updatedOn'/></b></td>
							<fmt:formatDate var="vanLineCommissionTypeupdatedOnFormattedValue" value="${vanLineCommissionType.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="vanLineCommissionType.updatedOn" value="${vanLineCommissionTypeupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${vanLineCommissionType.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='vanLineCommissionType.updatedBy' /></b></td>
							<c:if test="${not empty vanLineCommissionType.id}">
								<s:hidden name="vanLineCommissionType.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{vanLineCommissionType.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty vanLineCommissionType.id}">
								<s:hidden name="vanLineCommissionType.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
		
<table>
	<tbody> 	
		<tr>
		  	<td align="left" height="3px"></td>
		</tr>	  	
		<tr>
		  	<td align="left"><s:submit cssClass="cssbutton1" type="button" method="save" key="button.save" onmouseover="percentFloatCheck();" onkeydown="percentFloatCheck();"/></td>
       		<td align="right"><s:reset cssClass="cssbutton1"  type="button" key="Reset"/></td>
        </tr>		  	
	</tbody> 
  	</table>		
</div>
</s:form>


		