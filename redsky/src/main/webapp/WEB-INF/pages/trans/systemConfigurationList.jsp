<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<script type="text/javascript">
<!--

//-->
</script>

<script>

function clear_fields(){
	var i;
			for(i=0;i<=8;i++)
			{
					document.forms['systemConfigurationList'].elements[i].value = "";
			}
}

</script>
<head>   
    <title><fmt:message key="systemConfigurationList.title"/></title>   
    <meta name="heading" content="<fmt:message key='systemConfigurationList.heading'/>"/>   
<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:1px;
margin-top:-20px;
!margin-top:-14px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>    
    
</head>
<s:form cssClass="form_magn" id="systemConfigurationList" method="post" action="searchsystemlist" validate="true">
<div id="Layer1" style="width: 100%;">
<s:hidden name="systemConfiguration.id" value="%{systemConfiguration.id}"/>


<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center">
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-5px;"><span></span></div>
<div class="center-content">	
<table class="table" style="width: 100%;">
<thead>
<tr>
<th><fmt:message key="systemConfiguration.tableName"/></th>
<th><fmt:message key="systemConfiguration.fieldName"/></th>
<th><fmt:message key="dataCatalog.description"/></th>
<th><fmt:message key="dataCatalog.defineByToDoRule"/></th>
<th><fmt:message key="dataCatalog.isdateField"/></th>
<th><fmt:message key="dataCatalog.auditable"/></th>
<th><fmt:message key="dataCatalog.configerable"/></th>
<th><fmt:message key="dataCatalog.charge"/></th>
</tr></thead>	
		<tbody>
		<tr>
			<td width="" align="left">
			    <s:textfield name="systemConfigurationtableName" required="true" cssClass="input-text" size="16"/>
			</td>
			<td width="" align="left">
			    <s:textfield name="systemConfigurationfieldName" required="true" cssClass="input-text" size="16"/>
			</td>
			<td>
       			<s:textfield name="description" required="true" cssClass="input-text" size="16"/>
   			</td>
		  		   
		   <td>
		       <s:select name="defineByToDoRule" list="{'true','false'}" cssClass="list-menu"  headerKey="" headerValue="" cssStyle="width:120px;"/>
		   </td>
		   
		   <td>
		       <s:select name="isdateField" list="{'true','false'}" cssClass="list-menu"  headerKey="" headerValue="" cssStyle="width:120px;"/>
		   </td>
		    <td>
		       <s:select name="auditable" list="{'true','false'}" cssClass="list-menu"  headerKey="" headerValue="" cssStyle="width:120px;"/>
		   </td>
		   <td>
		       <s:select name="visible" list="{'true','false'}" cssClass="list-menu"  headerKey="" headerValue="" cssStyle="width:120px;"/>
		   </td>
		   <td>
			   <s:textfield name="charge" required="true" cssClass="input-text" size="16"/>
		   </td>
			</tr>
			<tr>
			<td colspan="7"></td>
			
			<td width="130px" align="center" style="border-left: hidden;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	

  
   
  <c:set var="buttons">  
      <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editsystem.html"/>'"  
        value="<fmt:message key="button.add"/>"/>
   </c:set>
<fmt:setLocale value="en-US" />
 
<s:set name="systemList" value="systemList" scope="request"/>
<div id="otabs" style="!margin-bottom:-15px;">
	   <ul><li><a class="current"><span>System Configuration List</span></a></li></ul>
     </div>
     <div class="spnblk">&nbsp;</div>

      <display:table name="systemList" class="table" requestURI="" id="systemList" defaultsort="1" export="true" pagesize="10" style="width:100%;margin-top: 1px;">   
	  	<display:column property="tableName" sortable="true" titleKey="systemConfiguration.tableName"  style="width:75px" href="editsystem.html" paramId="id" paramProperty="id"/>
    	<display:column property="fieldName" sortable="true" titleKey="systemConfiguration.fieldName" style="width:60px"/>
		<display:column property="description" sortable="true" title="Description / Field Help Text"/> 
		<c:if test="${systemList.defineByToDoRule=='true' }">
 <display:column titleKey="dataCatalog.defineByToDoRule" style="width:5px"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
 </c:if>
 <c:if test="${systemList.defineByToDoRule!='true' }">
 <display:column titleKey="dataCatalog.defineByToDoRule"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
 </c:if>
 <c:if test="${systemList.isdateField=='true' }">
 <display:column titleKey="dataCatalog.isdateField" style="width:5px"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
 </c:if>
 <c:if test="${systemList.isdateField!='true' }">
 <display:column titleKey="dataCatalog.isdateField"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
 </c:if>
 <c:if test="${systemList.auditable=='true' }">
 <display:column titleKey="dataCatalog.auditable" style="width:5px"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
 </c:if>
<c:if test="${systemList.auditable!='true' }">
 <display:column titleKey="dataCatalog.auditable"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
</c:if>
<c:if test="${systemList.visible=='true' }">
 <display:column titleKey="dataCatalog.configerable" style="width:5px"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
 </c:if>
<c:if test="${systemList.visible!='true' }">
 <display:column titleKey="dataCatalog.configerable"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
</c:if>

<display:column property="charge" sortable="true" titleKey="dataCatalog.charge"/> 
    
</display:table>   
 
</div>
</s:form>  

<script type="text/javascript">   
    highlightTableRows("systemList");   
</script>  