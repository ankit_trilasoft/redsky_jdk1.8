<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="org.appfuse.model.User"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User)auth.getPrincipal();
	String sessionCorpID = user.getCorpID();
%>

<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="org.appfuse.model.User"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<head>   
<title><fmt:message key="reportsDetail.title"/></title>   
<meta name="heading" content="<fmt:message key='reportsDetail.heading'/>"/>   
<style type="text/css">
#overlay111 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}

.emailAttach {
background: rgba(255,255,255,1);
background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,255,255,1)), color-stop(100%, rgba(212,226,246,1)));
background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
background: -o-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(212,226,246,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#d4e2f6', GradientType=0 );
}
 input[type="radio"] {margin:0px;}
 img {
cursor:pointer;
}
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>    
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="javascript" type="text/javascript">
window.onload=function ()
{
	<c:if test ="${emailFlag ne null && emailFlag=='1'}">
		window.opener.location.reload(true);
		window.close();
	</c:if>
		}
function validateFields(){
	var partnerCode=document.forms['EmailForm'].elements['partnerCode'].value;
	var recipientTo=document.forms['EmailForm'].elements['recipientTo'].value;
	var reportSubject=document.forms['EmailForm'].elements['reportSubject'].value;
	var reportBody=document.forms['EmailForm'].elements['reportBody'].value;
	 var jobNo='${jobNumber}';
	  var reportModule ='${noteFor}';
	 var attachmentIncludeFlag="FALSE";
	 if((recipientTo.trim()==null)||(recipientTo.trim()=='')){
			alert('Please enter values for Recipient To.');
			  return false;  
		}
	
	if((reportSubject.trim()==null)||(reportSubject.trim()=='')){
		alert('Please enter values for Subject To.');
		  return false;  
	}
	
	var url = 'sendEmailFileCabi.html?decorator=popup&popup=true&invoiceIncludeFlag='+attachmentIncludeFlag+"&checkBoxEmailId=${checkBoxEmailId}"+"&jobNumber="+jobNo+"&reportModule="+reportModule;
	document.forms['EmailForm'].action =url;
	document.forms['EmailForm'].submit();
	return true;
	
}

function refreshParent() {
	window.close();
}
</script>

<script type="text/javascript">
var httpMergeSO = getHTTPObject();
function showSO(target){
	showOrHide(1); 
	var soDiv = document.getElementById("soList");
	 var partnerNumber = target.value;
	 if(partnerNumber!=''){
		 soDiv.style.display = 'none';
		 /* document.getElementById("loader").style.display = "block"; */
	     var reportModule ='${noteFor}';
		 var jobNo='${jobNumber}';
		 var url="linkingUserList.html?ajax=1&decorator=simple&popup=true&partnerCode="+partnerNumber+"&reportModule="+reportModule+"&jobNumber="+jobNo;
		 	httpMergeSO.open("GET", url, true);
		 	httpMergeSO.onreadystatechange =  handleHttpResponseMergeSoList;
			httpMergeSO.send(null);	
	 	}else{
	 		soDiv.style.display = 'none';
	 		showOrHide(0); 
	 	}
	
}
function handleHttpResponseMergeSoList(){
	if (httpMergeSO.readyState == 4){
	    var results= httpMergeSO.responseText; 
	    var soDiv = document.getElementById("soList");      
	    soDiv.style.display = 'block';
	    /* document.getElementById("loader").style.display = "none"; */
	    soDiv.innerHTML = results;
	    showOrHide(0); 
	    }
}

function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay111"].visibility='hide';
        else
           document.getElementById("overlay111").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay111"].visibility='show';
       	else
          document.getElementById("overlay111").style.visibility='visible';
   	}
}
</script>
</head>  
<body>
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="myFileId"  />
<s:hidden name="jobNumber" value="<%=request.getParameter("customerFile.sequenceNumber") %>"/>
	<c:set var="jobNumber" value="<%=request.getParameter("jobNumber") %>" />	
<s:hidden name="myFileModule" value="${myFileModule}"/>
<s:hidden name="reportModule" value="${reportModule}"/>
<s:hidden name="noteFor" value="${myFileId}"/>

<s:hidden name="checkBoxEmailId" value="${checkBoxEmailId}" /> 
<s:form id="EmailForm"   method="post" onsubmit="return submit_form()" validate="true" enctype="multipart/form-data" >
<s:hidden name="myFileIdCheck"/>
<div id="overlay111">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
   </div>
   </div>
   
<table class="notesDetailTable emailAttach" cellspacing="0" cellpadding="0" border="0" style="width:510px;padding-bottom:0px;margin:0px;">
		<tbody>
			<tr>
				<td>
		 			<div class="subcontent-tab">Preview Email</div>
		   				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
		    				<tbody>	
		    					<tr><td style="height:10px;"></td></tr> 
								<tr>
                                <td align="right" class="listwhitetext" width="100px">
                                Partner :&nbsp;&nbsp;</td><td> <s:select name="partnerCode" cssClass="list-menu" list="%{partnerCodeList}" headerKey="" headerValue="" cssStyle="width:170px" onchange="showSO(this);" />
                                </td>
                                <td style="height:10px;"></td>            
								</tr>
								<tr style="height: 10px;"><td></td></tr>
								<tr>
                                <td align="right" class="listwhitetext" width="100px">
                                Recipients :&nbsp;To :<font color="red" size="2">*</font>&nbsp;&nbsp;
                                </td>
                                <td>
                                <s:textfield name="recipientTo" cssClass="input-text"  cssStyle="width:170px" />
                                </td>
                                <td align="right" class="listwhitetext" width="45px">
                                &nbsp;&nbsp;&nbsp;&nbsp;CC :&nbsp;&nbsp;
                                </td>
                                <td>
                                <s:textfield name="recipientCC" cssClass="input-text" cssStyle="width:170px" />
                                </td>
								</tr>
						     </tbody>
						</table>
						<!-- <div style="text-align:center; display:none" id="loader"><img src="/images/ajax-loader.gif" /></div> -->
						<div id="soList"></div>
						<table class="detailTabLabel">
						<tr>
			  			<td class="listwhitetext" width="93" align="right">Subject : </td>
			  			<td colspan="5">
			            <s:textfield name="reportSubject" cssClass="input-text"  cssStyle="width:387px;" />
			            </td>
			  			</tr>
						<tr>
			  			<td class="listwhitetext" width="93" align="right">Body : </td>
			  			<td colspan="5">
			            <s:textarea id="reportBody" name="reportBody" cssClass="input-text"  cssStyle="width:387px;height:50px" />
			            </td>
			  			</tr>
						<tr>
			  			<td class="listwhitetext" width="93" align="right">Signature : </td>
			  			<td colspan="5">
			            <s:textarea name="reportSignaturePart" cssClass="input-text"  cssStyle="width:387px;height:70px" />
			            </td>			            
			  			</tr>
			  			<tr ><td class="" style="height:10px;"></td></tr>  	
						</table>
				</td>	
  			</tr>  		
  		</tbody>
</table>
                
				<table  cellspacing="0" cellpadding="0" border="0" style="width:510px;margin: 0px;padding: 0px;">
				<tbody>				
									<tr ><td class="listwhitetext" style="height:10px;"></td></tr>  								
									
									<tr>
										<td align="center" class="listwhitetext" style="height:10px"></td>
										<td align="center" style="padding-right:20px;" colspan="3" ><input type="button"" name="send" value="Send" style="width:60px;" class="cssbutton"  onclick="return validateFields(); "/>
										<s:submit name="cancel" value="Cancel"  cssClass="cssbutton" cssStyle="width:60px; height:25px;margin-left:10px;" onclick="return refreshParent();"/>
										</td>										
									</tr>
									
								</tbody>
					</table> 		
</s:form>

<script language="javascript" type="text/javascript">
function fillUserEmail(target){
	try{
		if(target.checked==true){ 
		var newIds=document.forms['EmailForm'].elements['recipientTo'].value;
		 if(newIds==''){   
		     newIds=target.value;
		 }else{
		     newIds=newIds + ',' + target.value;
		 }
			document.forms['EmailForm'].elements['recipientTo'].value=newIds;
		}else{
			newIds=document.forms['EmailForm'].elements['recipientTo'].value;
		    if(newIds.indexOf(target.value)>-1){
		        newIds=newIds.replace(target.value,"");
		        newIds=newIds.replace(",,",",");
		    }
		    var len=newIds.length-1;
		    if(len==newIds.lastIndexOf(",")){
		    newIds=newIds.substring(0,len);
		    }
		    if(newIds.indexOf(",")==0){
		    newIds=newIds.substring(1,newIds.length);
		    }
		    document.forms['EmailForm'].elements['recipientTo'].value=newIds;  			
		}
	}catch(e){}	
}
function fillUserEmail1(target){
		try{
			if(target.checked==true){ 
			var newIds=document.forms['EmailForm'].elements['recipientCC'].value;
			 if(newIds==''){   
			     newIds=target.value;
			 }else{
			     newIds=newIds + ',' + target.value;
			 }
				document.forms['EmailForm'].elements['recipientCC'].value=newIds;
			}else{
				newIds=document.forms['EmailForm'].elements['recipientCC'].value;
			    if(newIds.indexOf(target.value)>-1){
			        newIds=newIds.replace(target.value,"");
			        newIds=newIds.replace(",,",",");
			    }
			    var len=newIds.length-1;
			    if(len==newIds.lastIndexOf(",")){
			    newIds=newIds.substring(0,len);
			    }
			    if(newIds.indexOf(",")==0){
			    newIds=newIds.substring(1,newIds.length);
			    }
			    document.forms['EmailForm'].elements['recipientCC'].value=newIds;  			
			}
		}catch(e){}			
}
setTimeout("showOrHide(0)",500);
</script>
</body>	


