<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix = "c"  uri = "http://java.sun.com/jsp/jstl/core" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Docs&nbsp;Information</title>  
<script type="text/javascript">
function poptastic(url)
{
    if(url.length>0)
	{
	newwindow=window.open(url,'name','height=400,width=200,scrollbars=yes,resizable=yes');
	if (window.focus) {newwindow.focus()}
	}
	else
	{    
		//window.open('openPopList.html','name','height=400,width=600,scrollbars=yes');
	}
}

function loadMainWindow(url,add1) 
{  
   var win = window.self; 
   win.opener = window.self; 
   if(add1 !=""){
   var newloc = "omniPopUp.html?countryName="+url; 
   
   
   var ih = (window.window.screen.availHeight - 60) - 100; 
   var iw = (window.window.screen.availWidth - 10) - 100; 
   var it = ((window.window.screen.availHeight - 60) - ih) / 2; 
   var il = ((window.window.screen.availWidth - 10) - iw) / 2; 
   
   var features = 
"directories=no,menubar=no,location=no,url=no,toolbar=no,status=no,scrollbars=yes,resizable=yes" 
+ ",width=" + iw + ",height=" + ih + ",top=" + it + ",left=" + il + ""; 
   v1 = "custom"; 
   v2 = "pg" + v1; 
   
   var oNewWindow = window.open(newloc, v2, features); 
  

   oNewWindow.focus(); 

}
   else{
   alert("No details available for "+url);
   }
   

} 

function popUpDocuument() 
{ 
   try 
   { 
      loadMainWindow("omniPopUp.html?countryName="+url); 
   } 
   catch(e) 
   { 
      popUpMessage.style.display = 'block'; 
   } 
}
 
</script>


<style type="text/css">
.clear { clear:both;}
body { margin:0px; padding:0px;}
</style>
</head>
<body id="dashboard">
<c:set var="noHeader" value="false" scope="session"/>			
			<script type="text/javascript">function setInitTabCol() {
			        if (document.getElementById('idGroupTab') != null) {
			            document.getElementById('idGroupTab').className = "group";
			        }
			        if (document.getElementById('idIndividualTab') != null) {
			            document.getElementById('idIndividualTab').className = "current";
			        }
			    }
			</script>
			<form name="dashboardActionForm" method="post" action="">
				<input name="reportActionEvent" value="" type="hidden">
				<input name="dashboardActionEvent" value="" type="hidden">
				<input name="clickedCtn" value="" type="hidden">
				
			</form>
		<script type="text/javascript">setInitTabCol();
		</script>
		
		<table align="left" style="margin-left:5px;margin:0px;padding:0px;" width="200px" cellspacing="0" cellpadding="0" border="0">
		<tr valign="top"> 	
	<td align="left"><b>Docs&nbsp;Information</b></td>
	<td align="right"  style="width:200px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
		</tr>
		<tr><td valign="top" colspan="2">
<s:form id="searchForm"  action="docInfo" method="post" validate="true" >
<s:set name="findCportalResourceMgmtDocList" value="findCportalResourceMgmtDocList" scope="request"/>
<div style="overflow-x:auto;" >
<display:table name="findCportalResourceMgmtDocList" class="table" requestURI="" id="findCportalResourceMgmtDocList" style="width:100%;" defaultsort="0" >
   	<display:column property="documentType" title="Document Type"  style="width:25px"/>
	 <display:column title="Document Name" maxLength="10" ><c:out value="${findCportalResourceMgmtDocList.documentName}" escapeXml="false"/></display:column>
     <display:column title="File Name"  maxLength="50" style="width:155px;"><a onclick="javascript:openWindow('CportalResourceMgmtImageServletAction.html?id=${findCportalResourceMgmtDocList.id}&param=DWNLD&decorator=popup&popup=true',900,600);"><c:out value="${findCportalResourceMgmtDocList.fileFileName}" escapeXml="false"/></a></display:column>
	</display:table>
</div>
</s:form>
</td></tr></table>
</body> 
		  	