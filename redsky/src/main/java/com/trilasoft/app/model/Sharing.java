
//Created by Bibhash Kumar Pathak

package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="payrollsharing")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class Sharing extends BaseObject{

	    private Long id;
	    private String corpId;
	    private String code;
	    private String description;
	    private String usuallyUsedFor;
	    private String descriptionArea;
	    private String typeOf;
	    
	    private String createdBy;
		private String updatedBy;
		private Date createdOn;
		private Date updatedOn;
		private Date valueDate;
		
		private String companyDivision;
		
		@Override
		public String toString() {
			return new ToStringBuilder(this).append("id", id).append("corpId",
					corpId).append("code", code).append("description", description)
					.append("usuallyUsedFor", usuallyUsedFor).append(
							"descriptionArea", descriptionArea).append("typeOf",
							typeOf).append("createdBy", createdBy).append(
							"updatedBy", updatedBy).append("createdOn", createdOn)
					.append("updatedOn", updatedOn).append("valueDate", valueDate)
					.append("companyDivision", companyDivision).toString();
		}
		@Override
		public boolean equals(final Object other) {
			if (!(other instanceof Sharing))
				return false;
			Sharing castOther = (Sharing) other;
			return new EqualsBuilder().append(id, castOther.id).append(corpId,
					castOther.corpId).append(code, castOther.code).append(
					description, castOther.description).append(usuallyUsedFor,
					castOther.usuallyUsedFor).append(descriptionArea,
					castOther.descriptionArea).append(typeOf, castOther.typeOf)
					.append(createdBy, castOther.createdBy).append(updatedBy,
							castOther.updatedBy).append(createdOn,
							castOther.createdOn).append(updatedOn,
							castOther.updatedOn).append(valueDate,
							castOther.valueDate).append(companyDivision,
							castOther.companyDivision).isEquals();
		}
		@Override
		public int hashCode() {
			return new HashCodeBuilder().append(id).append(corpId).append(code)
					.append(description).append(usuallyUsedFor).append(
							descriptionArea).append(typeOf).append(createdBy)
					.append(updatedBy).append(createdOn).append(updatedOn).append(
							valueDate).append(companyDivision).toHashCode();
		}
		@Column( length=25)
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		@Column( length=82)
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		@Column
		public Date getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
		
				
		@Column( length=82)
		public String getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}
		@Column
		public Date getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}
		@Column( length=25)
		public String getUsuallyUsedFor() {
			return usuallyUsedFor;
		}
		public void setUsuallyUsedFor(String usuallyUsedFor) {
			this.usuallyUsedFor = usuallyUsedFor;
		}
		@Column
		public Date getValueDate() {
			return valueDate;
		}
		public void setValueDate(Date valueDate) {
			this.valueDate = valueDate;
		}
		@Id @GeneratedValue(strategy = GenerationType.AUTO)
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		@Column( length=15)
		public String getCorpId() {
			return corpId;
		}
		public void setCorpId(String corpId) {
			this.corpId = corpId;
		}
		@Column( length=15)
		public String getTypeOf() {
			return typeOf;
		}
		public void setTypeOf(String typeOf) {
			this.typeOf = typeOf;
		}
		@Column( length=2000)
		public String getDescriptionArea() {
			return descriptionArea;
		}
		public void setDescriptionArea(String descriptionArea) {
			this.descriptionArea = descriptionArea;
		}
		@Column( length=225)
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		@Column( length=25)
		public String getCompanyDivision() {
			return companyDivision;
		}
		public void setCompanyDivision(String companyDivision) {
			this.companyDivision = companyDivision;
		}
		
		
}
