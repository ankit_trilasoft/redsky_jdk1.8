package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ScheduleResourceOperation;

public interface ScheduleResourceOperationManager extends GenericManager<ScheduleResourceOperation, Long> {

	public List findTimeSheet(ScheduleResourceOperation scheduleResourceOperation);
	public List findTruckingOperation(ScheduleResourceOperation scheduleResourceOperation); 

}
