package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;






@Entity
@Table(name="partnerprofileinfo")
public class ProfileInfo extends BaseObject {
	
	
	private Long id;
	private String partnerCode;
	private Long partnerPrivateId;
	private String corpId;
	private Boolean dom;
	private Boolean intl;
	private Boolean rlo;
	private Boolean com;
	private Boolean oth;
	private String othComnts;
	private String agentCompanyName;
	private String agentContact;
	private String agentPhone;
	private String agentEmail;
	private String auditfirmCompanyName;
	private String auditfirmContact;
	private String auditfirmPhone;
	private String auditfirmEmail;
	private String hhgProviderDomCompanyName;
	private String hhgProviderDomContact;
	private String hhgProviderDomPhone;
	private String hhgProvideDomEmail;
	private String hhgProviderIntlCompanyName;
	private String hhgProviderIntlContact;
	private String hhgProviderIntlPhone;
	private String hhgProvideIntlEmail;
	private String variousContacts;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String hhgProviderIntlCompanyName2;
	private String hhgProviderIntlContact2;
	private String hhgProviderIntlPhone2;
	private String hhgProvideIntlEmail2;
	private String hhgProviderIntlCompanyName3;
	private String hhgProviderIntlContact3;
	private String hhgProviderIntlPhone3;
	private String hhgProvideIntlEmail3;
	private String hhgProviderIntlCompanyName4;
	private String hhgProviderIntlContact4;
	private String hhgProviderIntlPhone4;
	private String hhgProvideIntlEmail4;
	private String agent;
	

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("partnerCode",partnerCode)
				.append("partnerPrivateId", partnerPrivateId)
				.append("corpId", corpId)
				.append("dom", dom)
				.append("intl", intl)
				.append("rlo", rlo)
				.append("com", com)
				.append("oth", oth)
				.append("othComnts", othComnts)
				.append("agentCompanyName",agentCompanyName)
				.append("agentContact", agentContact)
				.append("agentPhone",agentPhone)
				.append("agentEmail", agentEmail)
				.append("auditfirmCompanyName", auditfirmCompanyName)
				.append("auditfirmContact", auditfirmContact)
				.append("auditfirmPhone", auditfirmPhone)
				.append("auditfirmEmail", auditfirmEmail)
				.append("hhgProviderDomCompanyName", hhgProviderDomCompanyName)
				.append("hhgProviderDomContact", hhgProviderDomContact)
				.append("hhgProviderDomPhone", hhgProviderDomPhone)
				.append("hhgProvideDomEmail", hhgProvideDomEmail)
				.append("hhgProviderIntlCompanyName",hhgProviderIntlCompanyName)
				.append("hhgProviderIntlContact", hhgProviderIntlContact)
				.append("hhgProviderIntlPhone", hhgProviderIntlPhone)
				.append("hhgProvideIntlEmail", hhgProvideIntlEmail)
				.append("variousContacts", variousContacts)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("hhgProviderIntlCompanyName2", hhgProviderIntlCompanyName2)
				.append("hhgProviderIntlCompanyName3", hhgProviderIntlCompanyName3)
				.append("hhgProviderIntlCompanyName4", hhgProviderIntlCompanyName4)
				.append("agent", agent)
				.append("hhgProviderIntlContact2", hhgProviderIntlContact2)
				.append("hhgProviderIntlPhone2", hhgProviderIntlPhone2)
				.append("hhgProvideIntlEmail2", hhgProvideIntlEmail2)
				.append("hhgProviderIntlContact3", hhgProviderIntlContact3)
				.append("hhgProviderIntlPhone3", hhgProviderIntlPhone3)
				.append("hhgProvideIntlEmail3", hhgProvideIntlEmail3)
				.append("hhgProviderIntlContact4", hhgProviderIntlContact4)
				.append("hhgProviderIntlPhone4", hhgProviderIntlPhone4)
				.append("hhgProvideIntlEmail4", hhgProvideIntlEmail4)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof ProfileInfo ))
			return false;
		ProfileInfo castOther=(ProfileInfo)other;
		return new EqualsBuilder().append(id, castOther.id)
		.append(partnerCode,castOther.partnerCode)
		.append(partnerPrivateId, castOther.partnerPrivateId)
		.append(corpId, castOther.corpId)
		.append(dom, castOther.dom)
		.append(intl, castOther.intl)
		.append(rlo, castOther.rlo)
		.append(com, castOther.com)
		.append(oth, castOther.oth)
		.append(othComnts, castOther.othComnts)
		.append(agentCompanyName,castOther.agentCompanyName)
		.append(agentContact, castOther.agentContact)
		.append(agentPhone,castOther.agentPhone)
		.append(agentEmail, castOther.agentEmail)
		.append(auditfirmCompanyName, castOther.auditfirmCompanyName)
		.append(auditfirmContact, castOther.auditfirmContact)
		.append(auditfirmPhone, castOther.auditfirmPhone)
		.append(auditfirmEmail, castOther.auditfirmEmail)
		.append(hhgProviderDomCompanyName, castOther.hhgProviderDomCompanyName)
		.append(hhgProviderDomContact, castOther.hhgProviderDomContact)
		.append(hhgProviderDomPhone, castOther.hhgProviderDomPhone)
		.append(hhgProvideDomEmail, castOther.hhgProvideDomEmail)
		.append(hhgProviderIntlCompanyName,castOther.hhgProviderIntlCompanyName)
		.append(hhgProviderIntlContact, castOther.hhgProviderIntlContact)
		.append(hhgProviderIntlPhone, castOther.hhgProviderIntlPhone)
		.append(hhgProvideIntlEmail, castOther.hhgProvideIntlEmail)
		.append(variousContacts, castOther.variousContacts)
		.append(createdBy, castOther.createdBy)
		.append(createdOn, castOther.createdOn)
		.append(updatedBy, castOther.updatedBy)
		.append(updatedOn, castOther.updatedOn)
		.append(hhgProviderIntlCompanyName2, castOther.hhgProviderIntlCompanyName2)
		.append(hhgProviderIntlCompanyName3, castOther.hhgProviderIntlCompanyName3)
		.append(hhgProviderIntlCompanyName4, castOther.hhgProviderIntlCompanyName4)
		.append(agent, castOther.agent)
		.append(hhgProviderIntlContact2, castOther.hhgProviderIntlContact2)
		.append(hhgProviderIntlPhone2, castOther.hhgProviderIntlPhone2)
		.append(hhgProvideIntlEmail2, castOther.hhgProvideIntlEmail2)
		.append(hhgProviderIntlContact3, castOther.hhgProviderIntlContact3)
		.append(hhgProviderIntlPhone3, castOther.hhgProviderIntlPhone3)
		.append(hhgProvideIntlEmail3, castOther.hhgProvideIntlEmail3)
		.append(hhgProviderIntlContact4, castOther.hhgProviderIntlContact4)
		.append(hhgProviderIntlPhone4, castOther.hhgProviderIntlPhone4)
		.append(hhgProvideIntlEmail4, castOther.hhgProvideIntlEmail4)
		.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id)
		.append(partnerCode)
		.append(partnerPrivateId)
		.append(corpId)
		.append(dom)
		.append(intl)
		.append(rlo)
		.append(com)
		.append(oth)
		.append(othComnts)
		.append(agentCompanyName)
		.append(agentContact)
		.append(agentPhone)
		.append(agentEmail)
		.append(auditfirmCompanyName)
		.append(auditfirmContact)
		.append(auditfirmPhone)
		.append(auditfirmEmail)
		.append(hhgProviderDomCompanyName)
		.append(hhgProviderDomContact)
		.append(hhgProviderDomPhone)
		.append(hhgProvideDomEmail)
		.append(hhgProviderIntlCompanyName)
		.append(hhgProviderIntlContact)
		.append(hhgProviderIntlPhone)
		.append(hhgProvideIntlEmail)
		.append(variousContacts)
		.append(createdBy)
		.append(createdOn)
		.append(updatedBy)
		.append(updatedOn)
		.append(hhgProviderIntlCompanyName2)
		.append(hhgProviderIntlCompanyName3)
		.append(hhgProviderIntlCompanyName4)
		.append(agent)
		.append(hhgProviderIntlContact2)
		.append(hhgProviderIntlPhone2)
		.append(hhgProvideIntlEmail2)
		.append(hhgProviderIntlContact3)
		.append(hhgProviderIntlPhone3)
		.append(hhgProvideIntlEmail3)
		.append(hhgProviderIntlContact4)
		.append(hhgProviderIntlPhone4)
		.append(hhgProvideIntlEmail4)
		.toHashCode();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column
	public Long getPartnerPrivateId() {
		return partnerPrivateId;
	}

	public void setPartnerPrivateId(Long partnerPrivateId) {
		this.partnerPrivateId = partnerPrivateId;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Boolean getDom() {
		return dom;
	}

	public void setDom(Boolean dom) {
		this.dom = dom;
	}
	@Column
	public Boolean getIntl() {
		return intl;
	}

	public void setIntl(Boolean intl) {
		this.intl = intl;
	}
	@Column
	public Boolean getRlo() {
		return rlo;
	}

	public void setRlo(Boolean rlo) {
		this.rlo = rlo;
	}
	@Column
	public Boolean getCom() {
		return com;
	}

	public void setCom(Boolean com) {
		this.com = com;
	}
	@Column
	public Boolean getOth() {
		return oth;
	}

	public void setOth(Boolean oth) {
		this.oth = oth;
	}
	@Column
	public String getOthComnts() {
		return othComnts;
	}

	public void setOthComnts(String othComnts) {
		this.othComnts = othComnts;
	}
	@Column
	public String getAgentCompanyName() {
		return agentCompanyName;
	}

	public void setAgentCompanyName(String agentCompanyName) {
		this.agentCompanyName = agentCompanyName;
	}
	@Column
	public String getAgentContact() {
		return agentContact;
	}

	public void setAgentContact(String agentContact) {
		this.agentContact = agentContact;
	}
	@Column
	public String getAgentPhone() {
		return agentPhone;
	}

	public void setAgentPhone(String agentPhone) {
		this.agentPhone = agentPhone;
	}
	@Column
	public String getAgentEmail() {
		return agentEmail;
	}

	public void setAgentEmail(String agentEmail) {
		this.agentEmail = agentEmail;
	}
	@Column
	public String getAuditfirmCompanyName() {
		return auditfirmCompanyName;
	}

	public void setAuditfirmCompanyName(String auditfirmCompanyName) {
		this.auditfirmCompanyName = auditfirmCompanyName;
	}
	@Column
	public String getAuditfirmContact() {
		return auditfirmContact;
	}

	public void setAuditfirmContact(String auditfirmContact) {
		this.auditfirmContact = auditfirmContact;
	}
	@Column
	public String getAuditfirmPhone() {
		return auditfirmPhone;
	}

	public void setAuditfirmPhone(String auditfirmPhone) {
		this.auditfirmPhone = auditfirmPhone;
	}
	@Column
	public String getAuditfirmEmail() {
		return auditfirmEmail;
	}

	public void setAuditfirmEmail(String auditfirmEmail) {
		this.auditfirmEmail = auditfirmEmail;
	}
	@Column
	public String getHhgProviderDomCompanyName() {
		return hhgProviderDomCompanyName;
	}

	public void setHhgProviderDomCompanyName(String hhgProviderDomCompanyName) {
		this.hhgProviderDomCompanyName = hhgProviderDomCompanyName;
	}
	@Column
	public String getHhgProviderDomContact() {
		return hhgProviderDomContact;
	}

	public void setHhgProviderDomContact(String hhgProviderDomContact) {
		this.hhgProviderDomContact = hhgProviderDomContact;
	}
	@Column
	public String getHhgProviderDomPhone() {
		return hhgProviderDomPhone;
	}

	public void setHhgProviderDomPhone(String hhgProviderDomPhone) {
		this.hhgProviderDomPhone = hhgProviderDomPhone;
	}
	@Column
	public String getHhgProvideDomEmail() {
		return hhgProvideDomEmail;
	}

	public void setHhgProvideDomEmail(String hhgProvideDomEmail) {
		this.hhgProvideDomEmail = hhgProvideDomEmail;
	}
	@Column
	public String getHhgProviderIntlCompanyName() {
		return hhgProviderIntlCompanyName;
	}

	public void setHhgProviderIntlCompanyName(String hhgProviderIntlCompanyName) {
		this.hhgProviderIntlCompanyName = hhgProviderIntlCompanyName;
	}
	@Column
	public String getHhgProviderIntlContact() {
		return hhgProviderIntlContact;
	}

	public void setHhgProviderIntlContact(String hhgProviderIntlContact) {
		this.hhgProviderIntlContact = hhgProviderIntlContact;
	}
	@Column
	public String getHhgProviderIntlPhone() {
		return hhgProviderIntlPhone;
	}

	public void setHhgProviderIntlPhone(String hhgProviderIntlPhone) {
		this.hhgProviderIntlPhone = hhgProviderIntlPhone;
	}
	@Column
	public String getHhgProvideIntlEmail() {
		return hhgProvideIntlEmail;
	}

	public void setHhgProvideIntlEmail(String hhgProvideIntlEmail) {
		this.hhgProvideIntlEmail = hhgProvideIntlEmail;
	}
	@Column
	public String getVariousContacts() {
		return variousContacts;
	}

	public void setVariousContacts(String variousContacts) {
		this.variousContacts = variousContacts;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getHhgProviderIntlCompanyName2() {
		return hhgProviderIntlCompanyName2;
	}

	public void setHhgProviderIntlCompanyName2(String hhgProviderIntlCompanyName2) {
		this.hhgProviderIntlCompanyName2 = hhgProviderIntlCompanyName2;
	}
	@Column
	public String getHhgProviderIntlCompanyName3() {
		return hhgProviderIntlCompanyName3;
	}

	public void setHhgProviderIntlCompanyName3(String hhgProviderIntlCompanyName3) {
		this.hhgProviderIntlCompanyName3 = hhgProviderIntlCompanyName3;
	}
	@Column
	public String getHhgProviderIntlCompanyName4() {
		return hhgProviderIntlCompanyName4;
	}

	public void setHhgProviderIntlCompanyName4(String hhgProviderIntlCompanyName4) {
		this.hhgProviderIntlCompanyName4 = hhgProviderIntlCompanyName4;
	}

	@Column
	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}
	@Column
	public String getHhgProviderIntlContact2() {
		return hhgProviderIntlContact2;
	}

	public void setHhgProviderIntlContact2(String hhgProviderIntlContact2) {
		this.hhgProviderIntlContact2 = hhgProviderIntlContact2;
	}
	@Column
	public String getHhgProviderIntlPhone2() {
		return hhgProviderIntlPhone2;
	}
	@Column
	public void setHhgProviderIntlPhone2(String hhgProviderIntlPhone2) {
		this.hhgProviderIntlPhone2 = hhgProviderIntlPhone2;
	}
	@Column
	public String getHhgProvideIntlEmail2() {
		return hhgProvideIntlEmail2;
	}

	public void setHhgProvideIntlEmail2(String hhgProvideIntlEmail2) {
		this.hhgProvideIntlEmail2 = hhgProvideIntlEmail2;
	}
	@Column
	public String getHhgProviderIntlContact3() {
		return hhgProviderIntlContact3;
	}

	public void setHhgProviderIntlContact3(String hhgProviderIntlContact3) {
		this.hhgProviderIntlContact3 = hhgProviderIntlContact3;
	}
	@Column
	public String getHhgProviderIntlPhone3() {
		return hhgProviderIntlPhone3;
	}

	public void setHhgProviderIntlPhone3(String hhgProviderIntlPhone3) {
		this.hhgProviderIntlPhone3 = hhgProviderIntlPhone3;
	}
	@Column
	public String getHhgProvideIntlEmail3() {
		return hhgProvideIntlEmail3;
	}

	public void setHhgProvideIntlEmail3(String hhgProvideIntlEmail3) {
		this.hhgProvideIntlEmail3 = hhgProvideIntlEmail3;
	}
	@Column
	public String getHhgProviderIntlContact4() {
		return hhgProviderIntlContact4;
	}

	public void setHhgProviderIntlContact4(String hhgProviderIntlContact4) {
		this.hhgProviderIntlContact4 = hhgProviderIntlContact4;
	}
	@Column
	public String getHhgProviderIntlPhone4() {
		return hhgProviderIntlPhone4;
	}

	public void setHhgProviderIntlPhone4(String hhgProviderIntlPhone4) {
		this.hhgProviderIntlPhone4 = hhgProviderIntlPhone4;
	}
	@Column
	public String getHhgProvideIntlEmail4() {
		return hhgProvideIntlEmail4;
	}

	public void setHhgProvideIntlEmail4(String hhgProvideIntlEmail4) {
		this.hhgProvideIntlEmail4 = hhgProvideIntlEmail4;
	}

}
