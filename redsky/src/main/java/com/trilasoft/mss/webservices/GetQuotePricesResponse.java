
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetQuotePricesResult" type="{http://www.mss1.com/webservices/}MSSPricedOrder"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getQuotePricesResult"
})
@XmlRootElement(name = "GetQuotePricesResponse")
public class GetQuotePricesResponse {

    @XmlElement(name = "GetQuotePricesResult", required = true)
    protected MSSPricedOrder getQuotePricesResult;

    /**
     * Gets the value of the getQuotePricesResult property.
     * 
     * @return
     *     possible object is
     *     {@link MSSPricedOrder }
     *     
     */
    public MSSPricedOrder getGetQuotePricesResult() {
        return getQuotePricesResult;
    }

    /**
     * Sets the value of the getQuotePricesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link MSSPricedOrder }
     *     
     */
    public void setGetQuotePricesResult(MSSPricedOrder value) {
        this.getQuotePricesResult = value;
    }

}
