<%@ include file="/common/taglibs.jsp"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User)auth.getPrincipal();
	String sessionCorpID = user.getCorpID();
%>
<head>
<title>Payable Processing</title>
<STYLE type=text/css>
.disable-div {
  pointer-events: none;
}
</style> 


<meta name="heading"
	content="Payable Processing" />
</head>
<div id="otabs" style="margin-bottom:25px;">
		  <ul>		  
		    <li ><a class="current"><span>Payable Processing</span></a></li>
		  </ul>
		</div>
		<c:choose>

<c:when test="${usertype=='USER' && allowAgentInvoiceUpload !=true}">
<s:form id="payableProcessing" name="payableProcessing" action="previewInvoicelines.html" onsubmit="return checkMandatoryField();" method="post" >
  <s:hidden name="secondDescription" /> 
  <s:hidden name="description" />
	<s:hidden name="thirdDescription" /> 
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" /> 
 	<s:hidden name="fieldName"/>
 	<s:hidden name="sortType"/>
		<s:hidden name="htFlag" value="0"/>
		<s:hidden name="cDivision"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>	
<div id="content" align="center" style="width:100%">
<div id="liquid-round-top">
    <div class="top" ><span></span></div>
      <div class="center-content">
      <table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0" width="900px">
				<tbody>
					<tr>
					 <td align="right" class="listwhitebox">Vendor Code<font color="red" size="2">*</font></td>
					 <td><s:textfield name="vendorCode" cssClass="input-text"  size="10" onchange="findBookingAgentCode();"/>
					  <img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="javascript:openBookingAgentPopWindow()" src="<c:url value='/images/open-popup.gif'/>" /></td>
					 <td align="right" class="listwhitebox">Descripton</td>
					 <td><s:textfield name="vendorDescription" cssClass="input-textUpper"  size="35" onchange="" readonly="true" cssStyle="width:150px;"/></td>						 
					 <td align="right" class="listwhitebox">Vendor Invoice#<font color="red" size="2">*</font></td>
					 <td><s:textfield name="vendorInvoice" cssClass="input-text"  size="10" maxlength="17"/></td>					 
					  <td align="right" class="listwhitebox">Invoice Date<font color="red" size="2">*</font></td>
					  <td><s:textfield id="invoiceDateText" name="invoiceDate"  cssClass="input-text" size="8" onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/>
					  <img id="invoiceDateText_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					</tr>
					<tr>
					 <td align="right" class="listwhitebox">Currency</td>
					 <td><s:select name="currency" list="%{country}" cssClass="list-menu"  cssStyle="width:74px" onchange="findExchangeEstRate()" headerKey="" headerValue=""/></td>
					 <td align="right" class="listwhitebox">Exchange Rate</td>
					 <td><s:textfield name="exchangeRate" cssClass="input-text" id="exchangeRate" size="12" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event,this)" cssStyle="text-align:right; width:150px;" /></td>
					  <td align="right" class="listwhitebox">Received Date</td>
					 <td><s:textfield id="recievedDateText" name="recievedDate"  cssClass="input-text" size="10" onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/>
					  <img id="recievedDateText_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					</tr>
					<tr>
					 <td align="right" class="listwhitebox">Amount<font color="red" size="2">*</font></td>
					 <td><s:textfield name="amount" cssClass="input-text"   maxlength="12" size="10" onchange="return onlyFloat(this)" onblur="return checkFloat(this);"/></td>
					 <td align="right" class="listwhitebox">Service Order</td>
					 <td><s:textfield name="shipNo"  id="shipNo"  cssClass="input-text" cssStyle="width:150px;" maxlength="25" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>					 					
					<configByCorp:fieldVisibility componentId="component.field.batchPayable.companyDivision">
					<td align="right" class="listwhitetext" style="">Company&nbsp;Division</td>
					<td align="left"><s:select cssClass="list-menu" id="companyDivision" name="companyDivision" list="%{companyDivis}"  cssStyle="width:80px" headerKey="" headerValue=""  /></td>
					</configByCorp:fieldVisibility>
					</tr>
					<tr>
					 <td align="right"  height="10"></td>				
					</tr>
				</tbody>
	  </table>    
    </div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
<s:submit   cssClass="cssbutton" cssStyle="width:145px; height:25px;margin-left:35px;margin-top:10px;" key="Preview Invoice Lines" />
<input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="this.form.reset();autoPopulate_recievedDate();"/>
</s:form>
</c:when>
<c:otherwise>		
<s:form id="payableProcessing" name="payableProcessing" action="" onsubmit="return checkMandatoryFieldForAgent();" method="post" >
  <s:hidden name="secondDescription" />
  <s:hidden name="description" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" /> 
 	<s:hidden name="fieldName"/>
 	<s:hidden name="sortType"/>
		<s:hidden name="htFlag" value="0"/>
		<s:hidden name="cDivision"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>	
<div id="content" align="center" style="width:100%">
<div id="liquid-round-top">
    <div class="top" ><span></span></div>
      <div class="center-content">
      <table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0" width="900px">
				<tbody>
					<tr>
					 <td align="right" class="listwhitebox">Vendor Code<font color="red" size="2">*</font></td>
					 <td><s:textfield name="vendorCode" cssClass="input-text"  size="10" tabindex="1" onchange="findAgentEmployCode();"/>
					  <img class="openpopup" style="vertical-align:top;" width="17" height="20" tabindex="2"  onclick="javascript:openAgentPopWindow()" src="<c:url value='/images/open-popup.gif'/>" /></td>
					 <td align="right" class="listwhitebox">Descripton</td>
					 <td><s:textfield name="vendorDescription" cssClass="input-textUpper"  tabindex="3"  size="35" onchange="" readonly="true" cssStyle="width:150px;"/></td>						 
					 <td align="right" class="listwhitebox">Vendor Invoice#<font color="red" size="2">*</font></td>
					 <td><s:textfield name="vendorInvoice" cssClass="input-text" tabindex="4"  cssStyle="width:150px;" maxlength="25" onchange="return getInvoiceShipNumber();"/></td> 
					</tr>
					<tr>
					 <td align="right" class="listwhitebox">Currency<font color="red" size="2">*</font></td>
					 <td><s:select name="currency" list="%{country}" tabindex="5"  cssClass="list-menu"  cssStyle="width:86px" onchange="" headerKey="" headerValue=""/></td>
					 <c:if test="${usertype=='USER'}">
					 <td align="right" class="listwhitebox">Received Date<font color="red" size="2">*</font></td>
					 <td ><s:textfield id="recievedDateText" name="recievedDate"    cssClass="input-text" size="10" onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/>
					  <img id="recievedDateText_trigger" style="vertical-align:bottom" tabindex="6"  src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					 </c:if>
					  <c:if test="${usertype!='USER'}">
					 <td align="right" class="listwhitebox">Received Date<font color="red" size="2">*</font></td>
					 <td  id="myDiv"><s:textfield id="recievedDateText" name="recievedDate"    cssClass="input-text" size="10" onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/>
					  <img id="recievedDateText_trigger" style="vertical-align:bottom" tabindex="6"  src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					 </c:if>
					<td align="right" class="listwhitebox">Invoice Date<font color="red" size="2">*</font></td>
					  <td><s:textfield id="invoiceDateText" name="invoiceDate"   cssClass="input-text" size="8" onblur="calcDate();" onkeydown="return onlyDelNoQuotes(event,this);" readonly="true"/>
					  <img id="invoiceDateText_trigger" class="datepicker" style="vertical-align:bottom" tabindex="7" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					</tr>
					<tr>
					 <td align="right" class="listwhitebox">Total Amount<font color="red" size="2">*</font></td>
					 <td><s:textfield name="amount" cssClass="input-text"  tabindex="8" maxlength="12" size="10" onchange="return chckvalue(this);" onblur="return checkFloat(this);"/></td>
					 <td align="right" class="listwhitebox">Vat Amount<font color="red" size="2">*</font></td>
					 <td><s:textfield name="vatAmount" cssClass="input-text"  tabindex="9" maxlength="12" size="10" onchange="" onblur="return checkFloat(this);"/></td>
					 <td align="right" class="listwhitebox">Service Order or Last Name</td>
					 <td><s:textfield name="shipNo"  id="shipNo"  cssClass="input-text" tabindex="10" cssStyle="width:150px;" maxlength="25" onkeyup="valid(this,'special')" onblur="valid(this,'special');"/></td>					 					
					 </td>
     				</tr>
					<tr>
					 <td align="right"  height="10"></td>				
					</tr>
				</tbody>
	  </table>    
    </div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
<s:submit   cssClass="cssbutton" cssStyle="width:145px; height:25px;margin-left:35px;margin-top:10px;" key="Preview Invoice Lines" />
<input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="this.form.reset();autoPopulate_recievedDate();resetShipnumber();"/>
</s:form>
</c:otherwise>
</c:choose>

</div> 

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
   
   
<script type="text/javascript">
 function getHTTPObject77() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
  var http77 = getHTTPObject77();
  var http2=getHTTPObject77();
  var httpvandor=getHTTPObject77();
function winOpen(){	
    javascript:openWindow('bookingAgentPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=vendorCode');
    document.forms['payableProcessing'].elements['vendorCode'].focus();
 }
function openBookingAgentPopWindow(){
	var win = window.open('openPartnerPayableProcessing.html?partnerType=PA&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=vendorCode','','width=1024,height=500');
	var timer = setInterval(function() {   
	    if(win.closed) {  
	        clearInterval(timer);  
	        closed=true; 
	        
	    	if(!closed) 
			{  var vendorId  = document.forms['payableProcessing'].elements['vendorCode'].value; 
	    		 var url="getCurrency.html?ajax=1&decorator=simple&popup=true&vendorCode="+vendorId;
	    		  http3.open("GET", url, true);
	    		  http3.onreadystatechange=handleHttpResponse666;
	    		  http3.send(null);
			}}}, 1000); 
	}
function openAgentPopWindow(){
	
 <c:if test="${usertype=='USER'}">
 openBookingAgentPopWindow();
 </c:if>
 <c:if test="${usertype!='USER'}">

 var win = window.open('openAgentBillToCode.html?fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=vendorCode&decorator=popup&popup=true','','width=1120,height=500');
 var timer = setInterval(function() {   
	    if(win.closed) {  
	        clearInterval(timer);  
	        closed=true; 
	        
	    	if(!closed) 
			{  var vendorId  = document.forms['payableProcessing'].elements['vendorCode'].value; 
	    		 var url="getCurrency.html?ajax=1&decorator=simple&popup=true&vendorCode="+vendorId;
	    		  http3.open("GET", url, true);
	    		  http3.onreadystatechange=handleHttpResponse666;
	    		  http3.send(null);
			}}}, 1000); 
 </c:if>
}
function findExchangeEstRate(){
        var country =document.forms['payableProcessing'].elements['currency'].value; 
        var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(country);
	    http77.open("GET", url, true);
	    http77.onreadystatechange = handleHttpResponseEstRate;
	    http77.send(null);
  }
 function handleHttpResponseEstRate() { 
             if (http77.readyState == 4) {
                var results = http77.responseText
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']','');  
                if(results.length>1) {
                  document.forms['payableProcessing'].elements['exchangeRate'].value=results 
                  } else {
                  document.forms['payableProcessing'].elements['exchangeRate'].value=1;
                  document.forms['payableProcessing'].elements['exchangeRate'].readOnly=false; 
                  }                
             }   } 
 
 function findBookingAgentCode(){    
    var vendorId  = document.forms['payableProcessing'].elements['vendorCode'].value; 
    vendorId=vendorId.trim();  
	if(vendorId!='')
	{
    var url="vendorCodeAndName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse4;
    http2.send(null);
	}else{
		document.forms['payableProcessing'].elements['vendorDescription'].value='';
		document.forms['payableProcessing'].elements['shipNo'].value='';
		document.forms['payableProcessing'].elements['vendorInvoice'].value='';
	}
  }
 
 var httpoops1=getHTTPObject();
 function getHTTPObject() {
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        } }
	    return xmlhttp;
	}
 function getHTTPObject88() {
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        } }
	    return xmlhttp;
	}
 function getHTTPObject89() {
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        } }
	    return xmlhttp;
	}
  function handleHttpResponse4()        {
	  var vandorCode=document.forms['payableProcessing'].elements['vendorCode'].value;
           if (http2.readyState == 4)             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=2)                {
                	  if(results!="NotAct")
                	  {
	                	document.forms['payableProcessing'].elements['vendorDescription'].value=results;
                	  }else{
                           alert("This Vendor Code cannot be processed as the Actg Code for the partner is missing , please contact your accounting dept.");
                           document.forms['payableProcessing'].elements['vendorCode'].value='';
                           document.forms['payableProcessing'].elements['vendorDescription'].value='';
                          }
                	if(vandorCode!='')
          			{
          			getInvoiceShipNumber();
          			}
 				}
                 else                 {
                     alert("Invalid vendor code, please select another");
                     document.forms['payableProcessing'].elements['vendorCode'].value='';
                     document.forms['payableProcessing'].elements['vendorDescription'].value='';
                 }             }    } 
  
  function checkMandatoryFieldForAgent(){
	     var mandatoryField=""
	     var vandorCode=document.forms['payableProcessing'].elements['vendorCode'].value;
	     var amount=document.forms['payableProcessing'].elements['amount'].value;
	     var vatAmount=document.forms['payableProcessing'].elements['vatAmount'].value;
	     var currency=document.forms['payableProcessing'].elements['currency'].value;
	     var vendorInvoice=document.forms['payableProcessing'].elements['vendorInvoice'].value;
	     var invoiceDate=document.forms['payableProcessing'].elements['invoiceDate'].value;	  
	     var recievedDate=document.forms['payableProcessing'].elements['recievedDate'].value;	     
	     var shipNo=document.forms['payableProcessing'].elements['shipNo'].value;
			if(vandorCode.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		    	 document.forms['payableProcessing'].elements['vendorCode'].focus();
		    	 return false;
	       }else if(vendorInvoice.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		       	 document.forms['payableProcessing'].elements['vendorInvoice'].focus();
		    	 return false;
		     }else if(invoiceDate.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		          	 document.forms['payableProcessing'].elements['invoiceDate'].focus();
		        	 return false;
		     }
		     else if(amount=='0' || amount=='0.00' || amount=='0.0000') {
                        alert("Total Amount Can't be zero")
				        document.forms['payableProcessing'].elements['amount'].value="";
		    		    return false;
		     }
		      else if(amount.trim()=='' )
		       {    
		            alert("Please enter all the mandatory fields.");
		    	    document.forms['payableProcessing'].elements['amount'].value=''
		            document.forms['payableProcessing'].elements['amount'].focus();
		            return false;
		     }else if(vatAmount.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		            document.forms['payableProcessing'].elements['vatAmount'].focus();
		            return false;
		     }else if(currency.trim()==''){
		    	 alert("Please enter currency");
		            document.forms['payableProcessing'].elements['currency'].focus();
		            return false;
		     }else if(recievedDate.trim()==''){
		    	 alert("Please enter RecievedDate");
		            document.forms['payableProcessing'].elements['recievedDate'].focus();
		            return false;
		     }
		    
		     else if(shipNo.trim()!='' && shipNo.length >=4){
		    	var checkedSefviceOrders = [];
		    	var checkedcorPid = [];
		      var corpidcheck='<%=sessionCorpID %>';
		    	 var finalship=shipNo.substring(0,4);
		    	 var yu='${soCorpid}'
		    	var allCorpid='${corpIDS}';
		    	var t='';
		    	 if(yu=='')
		    		 {
		    		 var aa1='<%=sessionCorpID %>';
				   	 checkedSefviceOrders.push(aa1);
		    		 }
		    	 else
		    		 {
		        <c:forEach var="setQusList" items="${soCorpid}" varStatus="rowCounter">
		   	  aa1='${setQusList}';
		   	  checkedSefviceOrders.push(aa1);
		   	 	
		   	 	</c:forEach> 
		   	 	
		    		 }
		    	 <c:forEach var="setcorpidList" items="${corpIDS}" varStatus="rowCounter">
			   	  t='${setcorpidList}';
			   	  checkedcorPid.push(t);
			   	 	
			   	 	</c:forEach>
		   	 
		       var n = checkedSefviceOrders.includes(finalship);
		       var h=checkedcorPid.includes(finalship);
		      
		      
		       if(h==false)
		    	   {
		    	  
		    	   document.forms['payableProcessing'].elements['shipNo'].value=shipNo;
		    	  
		    	   }
		       else if(n==false && corpidcheck=='TSFT' )
		       {
		        alert("The RedSky company does not allow invoices to be uploaded via the agent portal")
		        document.forms['payableProcessing'].elements['shipNo'].value="";
		    	return false;
		       }
		      
		       
		    var url="checkRegisteredInvoice.html?ajax=1&decorator=simple&popup=true&vendorCode="+vandorCode+"&vendorInvoice="+vendorInvoice;
    	    http1.open("GET", url, true);
            http1.onreadystatechange =function(){  handleHttpResponse999(shipNo);};;
    	    http1.send(null); 
    	    return false;
		     } 
		     else if(shipNo.trim()==''){
		    	   var agree = confirm("No serviceorder or lastname registered. Retrieving all the lines might take some time. Do you want to continue.");
		    	   if(agree)
		    		   {
		    		    var url="checkRegisteredInvoice.html?ajax=1&decorator=simple&popup=true&vendorCode="+vandorCode+"&vendorInvoice="+vendorInvoice;
		    		    http1.open("GET", url, true);
		    		    http1.onreadystatechange=function(){  handleHttpResponse999(shipNo);};;
		    		    http1.send(null);
			    	    return false;
		    		   }
		    	   else
		    		   {
		    		   return false;
		    		   }
	               
			        } 
		     else {
		    	   url="checkRegisteredInvoice.html?ajax=1&decorator=simple&popup=true&vendorCode="+vandorCode+"&vendorInvoice="+vendorInvoice;
		    	   http1.open("GET", url, true);
		           http1.onreadystatechange=function(){  handleHttpResponse999(shipNo);};;
		    	    http1.send(null);
		     
		        
		     }
			
		     }
 
  var shipNumber;
  var http1=getHTTPObject88();
  var http2=getHTTPObject89();
	  function handleHttpResponse999(shipNumber){  
		
		  if (http1.readyState == 4){
				var results = http1.responseText
	             results = results.trim();
				  results =	results.replace('[','');
		          results =	results.replace(']','');
             
				   results = results.trim();
		       
				   if(results.length>0) {
		        	  
		        	  var res = results.split("#");
		        	
		           if(res[0]>0 && res[2]=='A'  && shipNumber=='' )
		        	   {
		        	  alert("Invoice already Processed");
		        	  return false;
		        	  
		        	   }
		           if(res[0]>0 && res[2]=='A'  && shipNumber!='' )
	        	   {
	        	   alert("Invoice already Registered in '"+res[1]+"' ");
	        	   return false;
	        	   }
		           
		           if(res[0]>0 && shipNumber=='' && res[2]!='A' )
	        	   {
		        	   var agree = confirm("Invoice already registered in ServiceOrder do you want to make changes");
		        	   if(agree)
		        		   {
		        		  
		        		  
						   	location.href= "../previewAgentInvoicelines.html?&flagAgent=true";
						 		document.forms['payableProcessing'].submit();
		        		   }
		        	   else
		        		   {
		        		   return false;
		        		   }
		        	   
	        	   }
		           
		         /*   
				 if(res[0] > 0 && shipNumber=='')
						 {
					 alert("Invoice Number Is already Processesed");
					 return false;
				 } */
				
					 document.forms['payableProcessing'].action="previewAgentInvoicelines.html?&flagAgent=true";
				     document.forms['payableProcessing'].submit();
				    
					   
                         }
		          else
		        	  {
		        	  document.forms['payableProcessing'].action="previewAgentInvoicelines.html?&flagAgent=true";
					  document.forms['payableProcessing'].submit();
					       return true
		        	  
		        	  }
		  
		  }
				  

                      
		        
	  }
	
	
	
   function checkMandatoryField(){
	     var mandatoryField=""
	     var vandorCode=document.forms['payableProcessing'].elements['vendorCode'].value;
	     var amount=document.forms['payableProcessing'].elements['amount'].value;
	     var vendorInvoice=document.forms['payableProcessing'].elements['vendorInvoice'].value;
	     var invoiceDate=document.forms['payableProcessing'].elements['invoiceDate'].value;	     
			if(vandorCode.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		    	 document.forms['payableProcessing'].elements['vendorCode'].focus();
		    	 return false;
	       }else if(vendorInvoice.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		       	 document.forms['payableProcessing'].elements['vendorInvoice'].focus();
		    	 return false;
		     }else if(invoiceDate.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		          	 document.forms['payableProcessing'].elements['invoiceDate'].focus();
		        	 return false;
		     }else if(amount.trim()==''){
		    	 alert("Please enter all the mandatory fields.");
		            document.forms['payableProcessing'].elements['amount'].focus();
		            return false;
		     }else {
		           return feedCompanyDivision();
		      }
	  }  
  function feedCompanyDivision(){
		  var comDiv='Y';
		  try{
		  comDiv=document.forms['payableProcessing'].elements['companyDivision'].value;
		  document.forms['payableProcessing'].elements['cDivision'].value=comDiv;
		  }catch(e){			
			  }
		  if(comDiv=='Y'){
			  document.forms['payableProcessing'].elements['cDivision'].value='N';
		  }
		return true;		  
	  } 
   function clear_fields(){
		document.forms['payableProcessing'].elements['vendorCode'].value = "";
		document.forms['payableProcessing'].elements['vendorDescription'].value = "";		
		document.forms['payableProcessing'].elements['vendorInvoice'].value = "";
		document.forms['payableProcessing'].elements['invoiceDate'].value = "";
		document.forms['payableProcessing'].elements['recievedDate'].value = "";
		document.forms['payableProcessing'].elements['amount'].value = "";
} 
   function onlyDelNoQuotes(evt,targetElement) 
	{
 		var keyCode = evt.which ? evt.which : evt.keyCode;
	  if(keyCode==46 || keyCode==8){
	  		targetElement.value = '';
	  }else{
	 return false;
	  	
	  }
 }
</script>
<script type="text/javascript">
function autoPopulate_recievedDate() {
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym;
	var datam = daym+"-"+month+"-"+y.substring(2,4);	
	document.forms['payableProcessing'].elements['recievedDate'].value=datam;
}
function autoPopulate_invoiceDate() {
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym;
	var datam = daym+"-"+month+"-"+y.substring(2,4);	
	document.forms['payableProcessing'].elements['invoiceDate'].value=datam;
}
function checkFloat(temp)  { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.name;   
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
       	  alert("Invalid data." ); 
          document.forms['payableProcessing'].elements[fieldName].select();
          document.forms['payableProcessing'].elements[fieldName].value=''; 
       	  return false;
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  } 
    if(check=='Invalid'){ 
    alert("Invalid data ." ); 
    document.forms['payableProcessing'].elements[fieldName].select();
    document.forms['payableProcessing'].elements[fieldName].value=''; 
    return false;
    }  else{
    s=Math.round(s*1000)/1000;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.forms['payableProcessing'].elements[fieldName].value=value; 
    return true;
    }  } 
function chckvalue(temp){
			if(temp.value==0.0000)
				{alert("Total Amount Can't be zero")
		        document.forms['payableProcessing'].elements['amount'].value="";
				  return false;
		}
}


</script>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
<script type="text/javascript">

function calcDate()
{ 
	var invoiceDate=document.forms['payableProcessing'].elements['invoiceDate'].value;
	 var date1SplitResult = invoiceDate.split("-");
	 var day1 = date1SplitResult[0];
	 var month1 = date1SplitResult[1];
	 var year1 = date1SplitResult[2];
	   	 year1 = '20'+year1;
	   	 if(month1 == 'Jan'){ month1 = "01";  }
		    else if(month1 == 'Feb'){ month1 = "02";  }
			else if(month1 == 'Mar'){ month1 = "03";  }
			else if(month1 == 'Apr'){ month1 = "04";  }
			else if(month1 == 'May'){ month1 = "05";  }
			else if(month1 == 'Jun'){ month1 = "06";  }
			else if(month1 == 'Jul'){ month1 = "07";  }
			else if(month1 == 'Aug'){ month1 = "08";  }
			else if(month1 == 'Sep'){ month1 = "09";  }
			else if(month1 == 'Oct'){ month1 = "10";  }																					
			else if(month1 == 'Nov'){ month1 = "11";  }
			else if(month1 == 'Dec'){ month1 = "12";  }
	var  current_datetime = new Date()
    var formatted_date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
   

   var currentD ='<%= (new java.text.SimpleDateFormat("yyyy-MM-dd")).format(new java.util.Date()) %>';
 
   var date3SplitResult = currentD.split("-");
		var day3 = date3SplitResult[2];
		var month3 = date3SplitResult[1];
		var year3 = date3SplitResult[0];
		var selectedDate = new Date(month1+"/"+day1+"/"+year1);
		var currentDate = new Date(month3+"/"+day3+"/"+year3);
		
		  var daysApart = Math.round((selectedDate-currentDate)/86400000);
		if(daysApart>0){
			
			 alert("Invoice Date can't be in the future")
			 document.forms['payableProcessing'].elements['invoiceDate'].value="";
		    return false;
			}
	
		
	} 

function findAgentEmployCodeHttp(){

if (http6661.readyState == 4){ 

	 var vandorCode=document.forms['payableProcessing'].elements['vendorCode'].value;
	 var url="getCurrency.html?ajax=1&decorator=simple&popup=true&vendorCode="+vandorCode;
	  http3.open("GET", url, true);
	  http3.onreadystatechange=handleHttpResponse666;
	  http3.send(null);
	var results = http6661.responseText
	results = results.trim();
	results=results.replace("[","");
    results=results.replace("]","");
	if(results.length >1){
		
			document.forms['payableProcessing'].elements['vendorDescription'].value = results;
		if(vandorCode!='')
			{
			getInvoiceShipNumber();
			
			}
	}else{
		
		var billToCode = document.forms['payableProcessing'].elements['vendorCode'].value;
    		alert("You are not authorize to select Vendor Code "+billToCode+ ".")
    		document.forms['payableProcessing'].elements['vendorCode'].value="${vendorCode}";
    		document.forms['payableProcessing'].elements['vendorDescription'].value="${vendorDescription}";
		}
	}
}
var http6661=getHTTPObject61();
function getHTTPObject61(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
try{
	var recievedDate=document.forms['payableProcessing'].elements['recievedDate'].value;
	if(recievedDate.trim()=='')
	{
	autoPopulate_recievedDate();
	}
	   }
	   catch(e){}
	   <c:if test="${usertype!='USER' || allowAgentInvoiceUpload ==true}">
	   try{
			var invoiceDate=document.forms['payableProcessing'].elements['invoiceDate'].value;
			if(invoiceDate.trim()=='')
			{
			//autoPopulate_invoiceDate();
			}
			   }
			   catch(e){}
	   </c:if>
	   var currency; 
$(document).ready(function() {
	  $("#myDiv").addClass("disable-div");
	});
function validateshipNumber()
{	
    	
}
function chkvatAmount(temp){
			if(temp.value<0)
		{
		alert("Enter valid Amount for VAT")
		
        document.forms['payableProcessing'].elements['vatAmount'].value="";
		  return false;
		}
}

function handleHttpResponse978(){
	var vandorCode=document.forms['payableProcessing'].elements['vendorCode'].value;
	 var vendorInvoice=document.forms['payableProcessing'].elements['vendorInvoice'].value;
	if (http2.readyState == 4){   

		var results = http2.responseText
		results = results.trim();
		results=results.replace("[","");
	    results=results.replace("]","");
	    var res = results.split(",");
	  
		if(res.length==1 && results!='' ){
		document.forms['payableProcessing'].elements['shipNo'].value=results;
		document.forms['payableProcessing'].elements['shipNo'].readOnly=true;
		}
		else if(res.length>1 && results!=''){
			{
			alert("Multiple ServiceOrder Registered For invoice number.");
			document.forms['payableProcessing'].elements['shipNo'].value='';
		    document.forms['payableProcessing'].elements['vendorInvoice'].value='';
			return false;
			}}
		
	}}
function resetShipnumber(){
	document.forms['payableProcessing'].elements['shipNo'].readOnly=false;
}
	function getInvoiceShipNumber()
	{

		  var vandorCode=document.forms['payableProcessing'].elements['vendorCode'].value;
          var vendorInvoice=document.forms['payableProcessing'].elements['vendorInvoice'].value;
          if( vandorCode=='')
          {
          alert("Kinldy enter the vendor code");
          document.forms['payableProcessing'].elements['vendorInvoice'].value='';
          document.forms['payableProcessing'].elements['shipNo'].value='';
          return false;
          }
          if(vendorInvoice!='')
        	 {
         var url="getSo.html?ajax=1&decorator=simple&popup=true&vendorCode="+vandorCode+"&vendorInvoice="+vendorInvoice;
    	  http2.open("GET", url, true);
    	  http2.onreadystatechange=handleHttpResponse978;
    	  http2.send(null);
    	  return false;
		}
		
		
	}
	window.onload = function() {
		 <c:if test="${fn:length(partnerPublicChild) ==1}">
		  <c:forEach var="entry" items="${partnerPublicChild}"  varStatus="rscont">
		 
		  var t='${entry.partnerCode}';
		  
		  document.forms['payableProcessing'].elements['vendorCode'].value=t;
		  if(t!='')
			  { 
			  findVendorName();
			  var url="getCurrency.html?ajax=1&decorator=simple&popup=true&vendorCode="+t;
	    	  http3.open("GET", url, true);
	    	  http3.onreadystatechange=handleHttpResponse666;
	    	  http3.send(null);
	    	  return false;
			  
			  } 
	     </c:forEach> 
	</c:if>
		}
	
	function handleHttpResponse666(){
		
		if (http3.readyState == 4){   

			var results = http3.responseText
			results = results.trim();
			
			results=results.replace("[","");
		    results=results.replace("]","");
		    
		    document.forms['payableProcessing'].elements['currency'].value=results
		}}
	var http3=getHTTPObject62();
	function getHTTPObject62(){
	    var xmlhttp;
	    if(window.XMLHttpRequest){
	        xmlhttp = new XMLHttpRequest();
	    }else if (window.ActiveXObject){
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp){
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
	}
	var t= document.forms['payableProcessing'].elements['vendorCode'].value;
	  if(t!='')
		  {
	 var url="getCurrency.html?ajax=1&decorator=simple&popup=true&vendorCode="+t;
	  http3.open("GET", url, true);
	  http3.onreadystatechange=handleHttpResponse666;
	  http3.send(null);
	 
	  }
	  function findAgentEmployCode(){
		
		  <c:if test="${usertype=='USER'}">
			findBookingAgentCode();
			 </c:if>
			 <c:if test="${usertype!='USER'}">
			 
			var bCode = '';
			bCode = document.forms['payableProcessing'].elements['vendorCode'].value;
			bCode=bCode.trim();
		    if(bCode!='') {
			    var url="findAgentBillToCode.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
			    http6661.open("GET", url, true); 
			    http6661.onreadystatechange = function(){findAgentEmployCodeHttp();};
			    http6661.send(null);	     
		     }else{
		    		document.forms['payableProcessing'].elements['vendorCode'].value =""; 
		    }
		   
			 </c:if>
			 
			 
		 }
	  
	  function findVendorName(){ 
		<c:if test="${usertype!='USER'}"> 
			var bCode = '';
			bCode = document.forms['payableProcessing'].elements['vendorCode'].value;
			bCode=bCode.trim();
		    if(bCode!='') {
			    var url="findAgentBillToCode.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
			    http6661.open("GET", url, true); 
			    http6661.onreadystatechange = function(){findVendorNameHttp();};
			    http6661.send(null);	     
		     }else{
		    		document.forms['payableProcessing'].elements['vendorCode'].value =""; 
		    } 
		</c:if> 
		 }
	  function findVendorNameHttp(){ 
		  if (http6661.readyState == 4){ 
		  	var results = http6661.responseText
		  	results = results.trim();
		  	results=results.replace("[","");
		    results=results.replace("]","");
		  	if(results.length >1){ 
		  			document.forms['payableProcessing'].elements['vendorDescription'].value = results; 
		  	} 
		  	}
		    }  
	  
</script>   

<script type="text/javascript">

	setOnSelectBasedMethods([["calcDate()"]]);
	setCalendarFunctionality();
</script> 