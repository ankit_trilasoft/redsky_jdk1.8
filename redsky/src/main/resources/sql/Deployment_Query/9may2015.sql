// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`company`
ADD COLUMN `insurancePremiumTax` BOOLEAN DEFAULT FALSE AFTER `enableMSS`,
ADD COLUMN `insurancePremiumTaxValue` DECIMAL(4,2) DEFAULT 0.00 AFTER `insurancePremiumTax`,
ADD COLUMN `vatInsurancePremiumTax` VARCHAR(30) DEFAULT '' AFTER `insurancePremiumTaxValue`;

//as per sunil sir
alter table creditcard 
add column authorizationDate DATETIME NULL,
add column expiryDate DATETIME NULL;

ALTER TABLE `redsky`.`app_user` 
ADD COLUMN `serviceType` VARCHAR(500) DEFAULT '' AFTER `commodity`;

ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `estimatedTotalExpenseForOI` DECIMAL(19,2) ,
 ADD COLUMN `estimatedTotalRevenueForOI` DECIMAL(19,2) ,
 ADD COLUMN `revisedTotalExpenseForOI` DECIMAL(19,2) ,
 ADD COLUMN `revisedTotalRevenueForOI` DECIMAL(19,2) ,
 ADD COLUMN `estimatedGrossMarginForOI` DECIMAL(19,2) ,
 ADD COLUMN `revisedGrossMarginForOI` DECIMAL(19,2) ,
 ADD COLUMN `estimatedGrossMarginPercentageForOI` DECIMAL(19,2) ,
 ADD COLUMN `revisedGrossMarginPercentageForOI` DECIMAL(19,2) ,
 ADD COLUMN `ticketStatus` VARCHAR(45) ,
 ADD COLUMN `itemsJbkEquipId` BIGINT(20) ;


ALTER TABLE `redsky`.`storage` ADD COLUMN `warehouse` VARCHAR(20) DEFAULT NULL  AFTER `hoTicket`;


Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,flex1,flex2,createdon,createdby,updatedon,updatedby)values
('VOER', 'HOB', 'Hotel Bookings', 5, 'SERVICE','en','Relo','dspDetails.HOB_serviceEndDate',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('VOER', 'FLB', 'Flight Bookings', 5, 'SERVICE','en','Relo','dspDetails.FLB_serviceEndDate',now(),'VOER_SETUP',now(),'VOER_SETUP');



Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,bucket2,createdon,createdby,updatedon,updatedby)values
('TSFT', 'DsHotelBookings', 'DsHotelBookings', 20, 'NOTESUBTYPE','en','Service Order',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('TSFT', 'DsFlightBookings', 'DsFlightBookings', 20, 'NOTESUBTYPE','en','Service Order',now(),'VOER_SETUP',now(),'VOER_SETUP'),
('TSFT', '10', '10', 4, 'COORDINATOREVAL','en','',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', '8', '8', 4, 'COORDINATOREVAL','en','',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', '6', '6', 4, 'COORDINATOREVAL','en','',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', '4', '4', 4, 'COORDINATOREVAL','en','',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', '2', '2', 4, 'COORDINATOREVAL','en','',now(),'TSFT_SETUP',now(),'TSFT_SETUP');



Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,bucket)values
('VOER', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'kkumar',now(),'kkumar','HOB_vendorCode'),
('VOER', 'serviceorder', 'serviceorder', 25, 'DATA_SECURITY_TABLE','en',now(),'kkumar',now(),'kkumar','FLB_vendorCode');


ALTER TABLE `redsky`.`serviceorder`
ADD COLUMN `HOB_vendorCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `projectManager` ,
ADD COLUMN `FLB_vendorCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `HOB_vendorCode`,
ADD COLUMN `OACoordinatorEval` VARCHAR(4) DEFAULT '' AFTER `projectManager`,
ADD COLUMN `DACoordinatorEval` VARCHAR(4) DEFAULT '' AFTER `OACoordinatorEval`;

ALTER TABLE `redsky`.`dspdetails`
    ADD COLUMN `HOB_serviceStartDate` DATETIME NULL DEFAULT NULL  AFTER `MMG_destinationEmailSent` , 
    ADD COLUMN `HOB_serviceEndDate` DATETIME NULL DEFAULT NULL  AFTER `HOB_serviceStartDate` , 
    ADD COLUMN `HOB_hotelName` VARCHAR(100) NULL DEFAULT NULL  AFTER `HOB_serviceEndDate` , 
    ADD COLUMN `HOB_city` VARCHAR(100) NULL DEFAULT NULL  AFTER `HOB_hotelName` , 
    ADD COLUMN `FLB_arrivalDate` DATETIME NULL DEFAULT NULL  AFTER `HOB_city` , 
    ADD COLUMN `FLB_departureDate` DATETIME NULL DEFAULT NULL  AFTER `FLB_arrivalDate` , 
    ADD COLUMN `FLB_additionalBookingReminderDate` DATETIME NULL DEFAULT NULL  AFTER `FLB_departureDate` ,
    ADD COLUMN `FLB_arrivalDate1` DATETIME NULL DEFAULT NULL  AFTER `FLB_additionalBookingReminderDate` , 
    ADD COLUMN `FLB_departureDate1` DATETIME NULL DEFAULT NULL  AFTER `FLB_arrivalDate1` , 
    ADD COLUMN `FLB_additionalBookingReminderDate1` DATETIME NULL DEFAULT NULL  AFTER `FLB_departureDate1` ,
    ADD COLUMN `FLB_addOn` BIT(1) NULL DEFAULT b'0'  AFTER `FLB_additionalBookingReminderDate1`,
    ADD COLUMN `HOB_startDate` DATETIME NULL DEFAULT NULL  AFTER `FLB_addOn` , 
    ADD COLUMN `HOB_endDate` DATETIME NULL DEFAULT NULL  AFTER `HOB_startDate` , 
    ADD COLUMN `HOB_vendorCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `HOB_endDate` , 
    ADD COLUMN `HOB_vendorName` VARCHAR(225) NULL DEFAULT NULL  AFTER `HOB_vendorCode` , 
    ADD COLUMN `HOB_vendorContact` VARCHAR(225) NULL DEFAULT NULL  AFTER `HOB_vendorName` , 
    ADD COLUMN `HOB_vendorEmail` VARCHAR(65) NULL DEFAULT NULL  AFTER `HOB_vendorContact` , 
    ADD COLUMN `HOB_vendorCodeEXSO` VARCHAR(65) NULL DEFAULT NULL  AFTER `HOB_vendorEmail` ,
    ADD COLUMN `FLB_vendorCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `HOB_vendorCodeEXSO` , 
    ADD COLUMN `FLB_vendorName` VARCHAR(225) NULL DEFAULT NULL  AFTER `FLB_vendorCode` , 
    ADD COLUMN `FLB_vendorContact` VARCHAR(225) NULL DEFAULT NULL  AFTER `FLB_vendorName` , 
    ADD COLUMN `FLB_vendorEmail` VARCHAR(65) NULL DEFAULT NULL  AFTER `FLB_vendorContact` , 
    ADD COLUMN `FLB_serviceStartDate` DATETIME NULL DEFAULT NULL  AFTER `FLB_vendorEmail` , 
    ADD COLUMN `FLB_serviceEndDate` DATETIME NULL DEFAULT NULL  AFTER `FLB_serviceStartDate` ,
    ADD COLUMN `FLB_vendorCodeEXSO` VARCHAR(65) NULL DEFAULT NULL  AFTER `FLB_serviceEndDate` ,
    ADD COLUMN `FLB_emailSent` DATETIME NULL DEFAULT NULL  AFTER `FLB_vendorCodeEXSO` , 
    ADD COLUMN `HOB_emailSent` DATETIME NULL DEFAULT NULL  AFTER `FLB_emailSent` ,
    ADD COLUMN `HOB_displyOtherVendorCode` BIT(1) NULL DEFAULT b'0'  AFTER `HOB_emailSent` , 
    ADD COLUMN `FLB_displyOtherVendorCode` BIT(1) NULL DEFAULT b'0'  AFTER `HOB_displyOtherVendorCode` ;



//As per ashish sir

select max(columnsequencenumber) from extractcolumnmgmt where reportname='shipmentAnalysis';

insert into extractcolumnmgmt(corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,
tableName, fieldName, displayName, columnSequenceNumber)
values('TSFT', 'TSFT_SETUP', now(), 'TSFT_SETUP', now(), 'shipmentAnalysis', 'customerfile',
"c.accountcode as ACCOUNT_CODE", 'ACCOUNT_CODE', 1941);



select max(columnsequencenumber) from extractcolumnmgmt where reportname='domesticAnalysis';

insert into extractcolumnmgmt(corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,
tableName, fieldName, displayName, columnSequenceNumber)
values('TSFT', 'TSFT_SETUP', now(), 'TSFT_SETUP', now(), 'domesticAnalysis', 'customerfile',
"c.accountcode as ACCOUNT_CODE", 'ACCOUNT_CODE', 1946);



select max(columnsequencenumber) from extractcolumnmgmt where reportname='shipmentAnalysis';

insert into extractcolumnmgmt(corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,
tableName, fieldName, displayName, columnSequenceNumber)
values('TSFT', 'TSFT_SETUP', now(), 'TSFT_SETUP', now(), 'shipmentAnalysis', 'customerfile',
"c.accountname as ACCOUNT_NAME", 'ACCOUNT_NAME', 1942);



select max(columnsequencenumber) from extractcolumnmgmt where reportname='domesticAnalysis';

insert into extractcolumnmgmt(corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,
tableName, fieldName, displayName, columnSequenceNumber)
values('TSFT', 'TSFT_SETUP', now(), 'TSFT_SETUP', now(), 'domesticAnalysis', 'customerfile',
"c.accountname as ACCOUNT_NAME", 'ACCOUNT_NAME', 1947);


ALTER TABLE `redsky`.`bookstorage` ADD COLUMN `warehouse` VARCHAR(20) DEFAULT NULL AFTER `storageId`;


 
 INSERT INTO
parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','COORDINATOREVAL',4,'',now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');


//As per ravi
ALTER TABLE `redsky`.`dsfamilydetails` CHANGE COLUMN `firstName` `firstName` VARCHAR(80)  NULL DEFAULT NULL  , CHANGE COLUMN `lastName` `lastName` VARCHAR(80)  NULL DEFAULT NULL  ;


 insert into rolebased_comp_permission  (mask, description, role, createdBy, createdOn, updatedBy,updatedOn, componentId, corpID, url)
 values
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','AATL_SETUP',now(),'AATL_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','AATL',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','ADAM_SETUP',now(),'ADAM_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','ADAM',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','BONG_SETUP',now(),'BONG_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','BONG',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','BOUR_SETUP',now(),'BOUR_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','BOUR',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','BRSH_SETUP',now(),'BRSH_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','BRSH',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','CAMP_SETUP',now(),'CAMP_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','CAMP',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','CMLK_SETUP',now(),'CMLK_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','CMLK',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','CWMS_SETUP',now(),'CWMS_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','CWMS',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','DASA_SETUP',now(),'DASA_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','DASA',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','DECA_SETUP',now(),'DECA_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','DECA',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','GERS_SETUP',now(),'GERS_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','GERS',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','HIGH_SETUP',now(),'HIGH_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','HIGH',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','HOLL_SETUP',now(),'HOLL_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','HOLL',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','HSRG_SETUP',now(),'HSRG_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','HSRG',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','ICMG_SETUP',now(),'ICMG_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','ICMG',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','INTM_SETUP',now(),'INTM_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','INTM',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','JOHN_SETUP',now(),'JOHN_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','JOHN',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','KOTA_SETUP',now(),'KOTA_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','KOTA',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','KTMS_SETUP',now(),'KTMS_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','KTMS',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','PUTT_SETUP',now(),'PUTT_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','PUTT',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','RSKY_SETUP',now(),'RSKY_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','RSKY',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','TFMS_SETUP',now(),'TFMS_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','TFMS',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','UGCA_SETUP',now(),'UGCA_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','UGCA',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','UGCN_SETUP',now(),'UGCN_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','UGCN',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','UGHK_SETUP',now(),'UGHK_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','UGHK',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','UGJP_SETUP',now(),'UGJP_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','UGJP',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','UGMY_SETUP',now(),'UGMY_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','UGMY',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','UGSG_SETUP',now(),'UGSG_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','UGSG',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','UGWW_SETUP',now(),'UGWW_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','UGWW',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACC_BASE','UTSI_SETUP',now(),'UTSI_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','UTSI',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','VOAO_SETUP',now(),'VOAO_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','VOAO',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','VOCZ_SETUP',now(),'VOCZ_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','VOCZ',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','VOER_SETUP',now(),'VOER_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','VOER',''),
 ('14','module.tab.notes.serviceOrderDetailsTab','ROLE_CORP_ACCOUNT','VORU_SETUP',now(),'VORU_SETUP',now(),'module.tab.notes.serviceOrderDetailsTab','VORU',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','AATL_SETUP',now(),'AATL_SETUP',now(),'module.tab.notes.summaryTab','AATL',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','ADAM_SETUP',now(),'ADAM_SETUP',now(),'module.tab.notes.summaryTab','ADAM',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','BONG_SETUP',now(),'BONG_SETUP',now(),'module.tab.notes.summaryTab','BONG',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','BOUR_SETUP',now(),'BOUR_SETUP',now(),'module.tab.notes.summaryTab','BOUR',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','BRSH_SETUP',now(),'BRSH_SETUP',now(),'module.tab.notes.summaryTab','BRSH',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','CAMP_SETUP',now(),'CAMP_SETUP',now(),'module.tab.notes.summaryTab','CAMP',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','CMLK_SETUP',now(),'CMLK_SETUP',now(),'module.tab.notes.summaryTab','CMLK',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','CWMS_SETUP',now(),'CWMS_SETUP',now(),'module.tab.notes.summaryTab','CWMS',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','DECA_SETUP',now(),'DECA_SETUP',now(),'module.tab.notes.summaryTab','DECA',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','GERS_SETUP',now(),'GERS_SETUP',now(),'module.tab.notes.summaryTab','GERS',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','HIGH_SETUP',now(),'HIGH_SETUP',now(),'module.tab.notes.summaryTab','HIGH',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','HOLL_SETUP',now(),'HOLL_SETUP',now(),'module.tab.notes.summaryTab','HOLL',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','HSRG_SETUP',now(),'HSRG_SETUP',now(),'module.tab.notes.summaryTab','HSRG',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','ICMG_SETUP',now(),'ICMG_SETUP',now(),'module.tab.notes.summaryTab','ICMG',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','INTM_SETUP',now(),'INTM_SETUP',now(),'module.tab.notes.summaryTab','INTM',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','JOHN_SETUP',now(),'JOHN_SETUP',now(),'module.tab.notes.summaryTab','JOHN',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','KOTA_SETUP',now(),'KOTA_SETUP',now(),'module.tab.notes.summaryTab','KOTA',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','KTMS_SETUP',now(),'KTMS_SETUP',now(),'module.tab.notes.summaryTab','KTMS',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','PUTT_SETUP',now(),'PUTT_SETUP',now(),'module.tab.notes.summaryTab','PUTT',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','RSKY_SETUP',now(),'RSKY_SETUP',now(),'module.tab.notes.summaryTab','RSKY',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','STAR_SETUP',now(),'STAR_SETUP',now(),'module.tab.notes.summaryTab','STAR',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','TFMS_SETUP',now(),'TFMS_SETUP',now(),'module.tab.notes.summaryTab','TFMS',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','UGCA_SETUP',now(),'UGCA_SETUP',now(),'module.tab.notes.summaryTab','UGCA',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','UGCN_SETUP',now(),'UGCN_SETUP',now(),'module.tab.notes.summaryTab','UGCN',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','UGHK_SETUP',now(),'UGHK_SETUP',now(),'module.tab.notes.summaryTab','UGHK',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','UGJP_SETUP',now(),'UGJP_SETUP',now(),'module.tab.notes.summaryTab','UGJP',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','UGMY_SETUP',now(),'UGMY_SETUP',now(),'module.tab.notes.summaryTab','UGMY',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','UGSG_SETUP',now(),'UGSG_SETUP',now(),'module.tab.notes.summaryTab','UGSG',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','UGWW_SETUP',now(),'UGWW_SETUP',now(),'module.tab.notes.summaryTab','UGWW',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACC_BASE','UTSI_SETUP',now(),'UTSI_SETUP',now(),'module.tab.notes.summaryTab','UTSI',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','VOAO_SETUP',now(),'VOAO_SETUP',now(),'module.tab.notes.summaryTab','VOAO',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','VOCZ_SETUP',now(),'VOCZ_SETUP',now(),'module.tab.notes.summaryTab','VOCZ',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','VOER_SETUP',now(),'VOER_SETUP',now(),'module.tab.notes.summaryTab','VOER',''),
 ('14','module.tab.notes.summaryTab','ROLE_CORP_ACCOUNT','VORU_SETUP',now(),'VORU_SETUP',now(),'module.tab.notes.summaryTab','VORU',''),
 ('14','module.tab.serviceorder.claimsTab','ROLE_CORP_ACC_BASE','UTSI_SETUP',now(),'UTSI_SETUP',now(),'module.tab.serviceorder.claimsTab','UTSI',''),
 ('14','module.tab.trackingStatus.claimsTab','ROLE_CORP_ACCOUNT','CWMS_SETUP',now(),'CWMS_SETUP',now(),'module.tab.trackingStatus.claimsTab','CWMS',''),
 ('14','module.tab.trackingStatus.claimsTab','ROLE_CORP_ACC_BASE','UTSI_SETUP',now(),'UTSI_SETUP',now(),'module.tab.trackingStatus.claimsTab','UTSI',''),
 ('14','module.tab.container.claimsTab','ROLE_CORP_ACCOUNT','CWMS_SETUP',now(),'CWMS_SETUP',now(),'module.tab.container.claimsTab','CWMS',''),
 ('14','module.tab.container.claimsTab','ROLE_CORP_ACCOUNT','DASA_SETUP',now(),'DASA_SETUP',now(),'module.tab.container.claimsTab','DASA',''),
 ('14','module.tab.claims.claimsTab','ROLE_CORP_ACCOUNT','CWMS_SETUP',now(),'CWMS_SETUP',now(),'module.tab.claims.claimsTab','CWMS',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','SSCW_SETUP',now(),'SSCW_SETUP',now(),'module.tab.claims.summaryTab','SSCW',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','AATL_SETUP',now(),'AATL_SETUP',now(),'module.tab.claims.summaryTab','AATL',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','ADAM_SETUP',now(),'ADAM_SETUP',now(),'module.tab.claims.summaryTab','ADAM',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','BONG_SETUP',now(),'BONG_SETUP',now(),'module.tab.claims.summaryTab','BONG',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','BOUR_SETUP',now(),'BOUR_SETUP',now(),'module.tab.claims.summaryTab','BOUR',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','BRSH_SETUP',now(),'BRSH_SETUP',now(),'module.tab.claims.summaryTab','BRSH',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','CAMP_SETUP',now(),'CAMP_SETUP',now(),'module.tab.claims.summaryTab','CAMP',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','CMLK_SETUP',now(),'CMLK_SETUP',now(),'module.tab.claims.summaryTab','CMLK',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','CWMS_SETUP',now(),'CWMS_SETUP',now(),'module.tab.claims.summaryTab','CWMS',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','DASA_SETUP',now(),'DASA_SETUP',now(),'module.tab.claims.summaryTab','DASA',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','DECA_SETUP',now(),'DECA_SETUP',now(),'module.tab.claims.summaryTab','DECA',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','GERS_SETUP',now(),'GERS_SETUP',now(),'module.tab.claims.summaryTab','GERS',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','HIGH_SETUP',now(),'HIGH_SETUP',now(),'module.tab.claims.summaryTab','HIGH',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','HOLL_SETUP',now(),'HOLL_SETUP',now(),'module.tab.claims.summaryTab','HOLL',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','HSRG_SETUP',now(),'HSRG_SETUP',now(),'module.tab.claims.summaryTab','HSRG',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','ICMG_SETUP',now(),'ICMG_SETUP',now(),'module.tab.claims.summaryTab','ICMG',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','INTM_SETUP',now(),'INTM_SETUP',now(),'module.tab.claims.summaryTab','INTM',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','JOHN_SETUP',now(),'JOHN_SETUP',now(),'module.tab.claims.summaryTab','JOHN',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','KOTA_SETUP',now(),'KOTA_SETUP',now(),'module.tab.claims.summaryTab','KOTA',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','KTMS_SETUP',now(),'KTMS_SETUP',now(),'module.tab.claims.summaryTab','KTMS',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','PUTT_SETUP',now(),'PUTT_SETUP',now(),'module.tab.claims.summaryTab','PUTT',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','RSKY_SETUP',now(),'RSKY_SETUP',now(),'module.tab.claims.summaryTab','RSKY',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','STAR_SETUP',now(),'STAR_SETUP',now(),'module.tab.claims.summaryTab','STAR',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','TFMS_SETUP',now(),'TFMS_SETUP',now(),'module.tab.claims.summaryTab','TFMS',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','UGCA_SETUP',now(),'UGCA_SETUP',now(),'module.tab.claims.summaryTab','UGCA',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','UGCN_SETUP',now(),'UGCN_SETUP',now(),'module.tab.claims.summaryTab','UGCN',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','UGHK_SETUP',now(),'UGHK_SETUP',now(),'module.tab.claims.summaryTab','UGHK',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','UGJP_SETUP',now(),'UGJP_SETUP',now(),'module.tab.claims.summaryTab','UGJP',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','UGMY_SETUP',now(),'UGMY_SETUP',now(),'module.tab.claims.summaryTab','UGMY',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','UGSG_SETUP',now(),'UGSG_SETUP',now(),'module.tab.claims.summaryTab','UGSG',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','UGWW_SETUP',now(),'UGWW_SETUP',now(),'module.tab.claims.summaryTab','UGWW',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACC_BASE','UTSI_SETUP',now(),'UTSI_SETUP',now(),'module.tab.claims.summaryTab','UTSI',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','VOAO_SETUP',now(),'VOAO_SETUP',now(),'module.tab.claims.summaryTab','VOAO',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','VOCZ_SETUP',now(),'VOCZ_SETUP',now(),'module.tab.claims.summaryTab','VOCZ',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','VOER_SETUP',now(),'VOER_SETUP',now(),'module.tab.claims.summaryTab','VOER',''),
 ('14','module.tab.claims.summaryTab','ROLE_CORP_ACCOUNT','VORU_SETUP',now(),'VORU_SETUP',now(),'module.tab.claims.summaryTab','VORU',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','SSCW_SETUP',now(),'SSCW_SETUP',now(),'module.tab.notes.serviceOrdersTab','SSCW',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','AATL_SETUP',now(),'AATL_SETUP',now(), 'module.tab.notes.serviceOrdersTab','AATL',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','ADAM_SETUP',now(),'ADAM_SETUP',now(), 'module.tab.notes.serviceOrdersTab','ADAM',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','BONG_SETUP',now(),'BONG_SETUP',now(), 'module.tab.notes.serviceOrdersTab','BONG',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','BOUR_SETUP',now(),'BOUR_SETUP',now(), 'module.tab.notes.serviceOrdersTab','BOUR',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','BRSH_SETUP',now(),'BRSH_SETUP',now(), 'module.tab.notes.serviceOrdersTab','BRSH',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','CAMP_SETUP',now(),'CAMP_SETUP',now(), 'module.tab.notes.serviceOrdersTab','CAMP',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','CMLK_SETUP',now(),'CMLK_SETUP',now(), 'module.tab.notes.serviceOrdersTab','CMLK',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','CWMS_SETUP',now(),'CWMS_SETUP',now(), 'module.tab.notes.serviceOrdersTab','CWMS',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','DASA_SETUP',now(),'DASA_SETUP',now(), 'module.tab.notes.serviceOrdersTab','DASA',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','DECA_SETUP',now(),'DECA_SETUP',now(), 'module.tab.notes.serviceOrdersTab','DECA',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','GERS_SETUP',now(),'GERS_SETUP',now(), 'module.tab.notes.serviceOrdersTab','GERS',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','HIGH_SETUP',now(),'HIGH_SETUP',now(), 'module.tab.notes.serviceOrdersTab','HIGH',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','HOLL_SETUP',now(),'HOLL_SETUP',now(), 'module.tab.notes.serviceOrdersTab','HOLL',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','HSRG_SETUP',now(),'HSRG_SETUP',now(), 'module.tab.notes.serviceOrdersTab','HSRG',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','ICMG_SETUP',now(),'ICMG_SETUP',now(), 'module.tab.notes.serviceOrdersTab','ICMG',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','INTM_SETUP',now(),'INTM_SETUP',now(), 'module.tab.notes.serviceOrdersTab','INTM',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','JOHN_SETUP',now(),'JOHN_SETUP',now(), 'module.tab.notes.serviceOrdersTab','JOHN',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','KOTA_SETUP',now(),'KOTA_SETUP',now(), 'module.tab.notes.serviceOrdersTab','KOTA',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','KTMS_SETUP',now(),'KTMS_SETUP',now(), 'module.tab.notes.serviceOrdersTab','KTMS',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','PUTT_SETUP',now(),'PUTT_SETUP',now(), 'module.tab.notes.serviceOrdersTab','PUTT',''),
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','RSKY_SETUP',now(),'RSKY_SETUP',now(), 'module.tab.notes.serviceOrdersTab','RSKY',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','STAR_SETUP',now(),'STAR_SETUP',now(), 'module.tab.notes.serviceOrdersTab','STAR',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','TFMS_SETUP',now(),'TFMS_SETUP',now(), 'module.tab.notes.serviceOrdersTab','TFMS',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','UGCA_SETUP',now(),'UGCA_SETUP',now(), 'module.tab.notes.serviceOrdersTab','UGCA',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','UGCN_SETUP',now(),'UGCN_SETUP',now(), 'module.tab.notes.serviceOrdersTab','UGCN',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','UGHK_SETUP',now(),'UGHK_SETUP',now(), 'module.tab.notes.serviceOrdersTab','UGHK',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','UGJP_SETUP',now(),'UGJP_SETUP',now(), 'module.tab.notes.serviceOrdersTab','UGJP',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','UGMY_SETUP',now(),'UGMY_SETUP',now(), 'module.tab.notes.serviceOrdersTab','UGMY',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','UGSG_SETUP',now(),'UGSG_SETUP',now(), 'module.tab.notes.serviceOrdersTab','UGSG',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','UGWW_SETUP',now(),'UGWW_SETUP',now(), 'module.tab.notes.serviceOrdersTab','UGWW',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACC_BASE','UTSI_SETUP',now(),'UTSI_SETUP',now(), 'module.tab.notes.serviceOrdersTab','UTSI',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','VOAO_SETUP',now(),'VOAO_SETUP',now(), 'module.tab.notes.serviceOrdersTab','VOAO',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','VOCZ_SETUP',now(),'VOCZ_SETUP',now(), 'module.tab.notes.serviceOrdersTab','VOCZ',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','VOER_SETUP',now(),'VOER_SETUP',now(), 'module.tab.notes.serviceOrdersTab','VOER',''), 
 ('14','module.tab.notes.serviceOrdersTab','ROLE_CORP_ACCOUNT','VORU_SETUP',now(),'VORU_SETUP',now(), 'module.tab.notes.serviceOrdersTab','VORU','');
 
 update rolebased_comp_permission set  mask=14 where componentId='module.tab.serviceorder.claimsTab' and role='ROLE_CORP_ACCOUNT';
 
 // as per dilip sir
 
 update corp_comp_permission set description='component.button.exchangerate.dynamic',componentId='component.button.exchangerate.dynamic'
where componentId='component.button.exchangerate.uploadXXXButton'
order by corp_id

// as per mail by sunil sir

ALTER TABLE `redsky`.`trackingstatus` 
CHANGE COLUMN `bookingAgentPhoneNumber` `bookingAgentPhoneNumber` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
CHANGE COLUMN `networkPhoneNumber` `networkPhoneNumber` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
CHANGE COLUMN `originAgentPhoneNumber` `originAgentPhoneNumber` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
CHANGE COLUMN `destinationAgentPhoneNumber` `destinationAgentPhoneNumber` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
CHANGE COLUMN `subOriginAgentPhoneNumber` `subOriginAgentPhoneNumber` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ,
CHANGE COLUMN `subDestinationAgentPhoneNumber` `subDestinationAgentPhoneNumber` VARCHAR(30) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL ;