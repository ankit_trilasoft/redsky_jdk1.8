//----Created By Bibhash---

package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.AuditSetup;
import com.trilasoft.app.model.DataCatalog;

public interface AuditSetupManager extends GenericManager<AuditSetup, Long>{

	public List findMaximumId();
	public List findById(Long id);
	public List<AuditSetup> searchAuditSetup(String corpID, String tableName, String fieldName, String auditable,String isAlertValue);
	public List auditSetupTable(String b);
    public List auditSetupField(String tableName,String b);
    public List getList(String corpID);
	public List checkData(String tableName, String fieldName, String sessionCorpID);
}


