// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','STANDARDEMAILMODULE',20,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'CustomerFile', 'CustomerFile', 20, 'STANDARDEMAILMODULE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP'),
('TSFT', 'ServiceOrder', 'ServiceOrder', 20, 'STANDARDEMAILMODULE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP');


INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','STANDARDEMAILSUBMODULE',20,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'ServiceOrder', 'ServiceOrder', 20, 'STANDARDEMAILSUBMODULE','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP');

ALTER TABLE partnerprivate
ADD COLUMN doNotSayYesEmail  bit(1) DEFAULT b'0' ;
ALTER TABLE `redsky`.`emailsetuptemplate`
CHANGE COLUMN `saveAs` `saveAs` VARCHAR(50) NULL DEFAULT NULL  ,
CHANGE COLUMN `emailBody` `emailBody` TEXT NULL DEFAULT NULL  ;
ALTER TABLE `redsky`.`emailsetuptemplate` ADD COLUMN `orderNumber` INT(3) NOT NULL  AFTER `addSignatureFromAppUser` ;
ALTER TABLE redsky.partnerprivate add COLUMN defaultContact varchar(82) NULL ;

