package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.PartnerAccountRefDao;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerAccountRef;
import com.trilasoft.app.service.PartnerAccountRefManager;

public class PartnerAccountRefManagerImpl extends GenericManagerImpl<PartnerAccountRef, Long> implements PartnerAccountRefManager{

	PartnerAccountRefDao partnerAccountRefDao;
	
	public PartnerAccountRefManagerImpl(PartnerAccountRefDao partnerAccountRefDao){
		super(partnerAccountRefDao);
		this.partnerAccountRefDao=partnerAccountRefDao;
	}
	public List getPartnerAccountReferenceList(String corpId,String partnerCodeForRef){
		return partnerAccountRefDao.getPartnerAccountReferenceList(corpId, partnerCodeForRef);
	
	}
	public List findCompanyDivisionByCorpId(String corpId){
		return partnerAccountRefDao.findCompanyDivisionByCorpId(corpId);
	}
	public List getPartnerList(String partnerCode, String sessionCorpID) {
		return partnerAccountRefDao.getPartnerList(partnerCode, sessionCorpID);
	}
	public List checkCompayCode(String companyDivision, String corpID,String partnerCode ){
		return partnerAccountRefDao.checkCompayCode(companyDivision, corpID,partnerCode);
	}
	public List checkCompayCodeAccount(String companyDivision, String corpID,String partnerCode,String refType){
		return partnerAccountRefDao.checkCompayCodeAccount(companyDivision, corpID,partnerCode,refType);
	}
	public int updateDeleteStatus(Long ids){
		return partnerAccountRefDao.updateDeleteStatus(ids);
	}
	public String findAccountInterface(String sessionCorpID) {
		return partnerAccountRefDao.findAccountInterface(sessionCorpID);
	}
	public Map<String, String> getPayableReffNumber(String sessionCorpID){
		return partnerAccountRefDao.getPayableReffNumber(sessionCorpID);
	}
	public List checkAccountReference(String AccountReference, String corpID, String partnerCode, String refType) {
		return partnerAccountRefDao.checkAccountReference(AccountReference, corpID, partnerCode, refType);
	}
	public Map<String, String> getActgCodeMap(String sessionCorpID) {
		return partnerAccountRefDao.getActgCodeMap(sessionCorpID);
	}
	public List countChkAccountRef(String partnerCode, String refType,String sessionCorpID){
		return partnerAccountRefDao.countChkAccountRef(partnerCode, refType, sessionCorpID);
	}
	public String getAccountingReffCode(String sessionCorpID,String billToCode, String currencyCode, String reftype){
		return partnerAccountRefDao.getAccountingReffCode(sessionCorpID, billToCode, currencyCode, reftype);
	}
	public List getAccrefSameParentForChild(String corpId){
		return partnerAccountRefDao.getAccrefSameParentForChild(corpId);
	}
	public void deletePartnerAccountRefFromChild(String accountcrossReference,String childCorpId,String reftype,String partnerCode){
		 partnerAccountRefDao.deletePartnerAccountRefFromChild(accountcrossReference,childCorpId,reftype,partnerCode);
	}
	public List checkAccountReferenceForChild(String AccountReference, String corpID,String partnerCode,String refType){
		return partnerAccountRefDao.checkAccountReferenceForChild(AccountReference, corpID, partnerCode, refType);
	}
	
	
	
}
