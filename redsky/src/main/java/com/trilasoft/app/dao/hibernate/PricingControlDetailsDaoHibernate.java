package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.PricingControlDetailsDao;
import com.trilasoft.app.model.PricingControlDetails;

public class PricingControlDetailsDaoHibernate  extends GenericDaoHibernate<PricingControlDetails, Long> implements PricingControlDetailsDao{

	public PricingControlDetailsDaoHibernate() {
		super(PricingControlDetails.class);
		
	}

	public List checkById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List getSelectedAgent(Long id, String tariffApplicability, String sessionCorpID) {
		if(tariffApplicability.equalsIgnoreCase("Freight"))
		{
			return getHibernateTemplate().find("from PricingControlDetails where pricingControlID="+id+" and tarrifApplicability='"+tariffApplicability+"' and corpID='"+sessionCorpID+"' and freightSelection=true group by pricingControlID");
		}else{
			return getHibernateTemplate().find("from PricingControlDetails where pricingControlID="+id+" and tarrifApplicability='"+tariffApplicability+"' and corpID='"+sessionCorpID+"' and destinationSelection=true");
		}
	}

	public List getFreightList(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, String originPortRange, String destinationPortRange, String mode, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude) {
		List frightList= new ArrayList();
		try{
			if(originPortRange!=null){
				String originPortTemp=getPorts(originPortRange,mode,originCountry, originLatitude, originLongitude);
				originPOE = originPortTemp.replaceAll(",", "','");
			}
		}catch(Exception ex){
		}
		try{
			if(destinationPortRange!=null){
				String destinationPortTemp=getPorts(destinationPortRange,mode,destinationCountry, destinationLatitude, destinationLongitude);
				destinationPOE = destinationPortTemp.replaceAll(",", "','");
			}
		
		}catch(Exception ex){
		}
		
		
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedExpectedLoadDate = new StringBuilder(dateformatYYYYMMDD1.format(expectedLoadDate));
		
		/*String sql="SELECT distinct r.carrier, concat(originCity,', ',originCountry) 'Origin'," + // 0,1
				"concat(originPortCity,', ',originPortCountry) 'OriginPort'," + //2
				"concat(destinationCity,', ',destinationCountry) 'Destination'," + //3
				"concat(destinationPortCity,', ',destinationPortCountry) 'DestPort', " + //4
				"equipmentType, totalPrice, laneComments, destTHCValue, destTHCDetails, r.id, baseCurrency, " + // 5,6,7,8,9,10,11
				"left(r.contractType,4), po.portCode as originPortCode, pd.portCode as destinationPortCode, " + //12,13,14
				"po.portName as originPortCodeName, pd.portName as destinationPortCodeName " + //15,16
				"FROM reffreightrates r, port po, port pd " + 
				"where r.originCity = po.refAbbreviation " +
				"and r.destinationCity = pd.refAbbreviation " +
				"and po.portCode in ('"+originPOE+"') and pd.portCode in ('"+destinationPOE+"') " +
				"and equipmentType like '"+containerSize+"%' " +
				"and '"+formatedExpectedLoadDate+"' between LineItemEffDate and LineItemExpDate";*/
		
		String sql="SELECT distinct r.id, r.totalPrice, po.portCode as originPortCode, pd.portCode as destinationPortCode " + 
		"FROM reffreightrates r, port po, port pd " + 
		"where r.originCity = po.refAbbreviation " +
		"and r.destinationCity = pd.refAbbreviation " +
		"and po.portCode in ('"+originPOE+"') and pd.portCode in ('"+destinationPOE+"') " +
		"and equipmentType like '"+containerSize+"%' " +
		"and '"+formatedExpectedLoadDate+"' between LineItemEffDate and LineItemExpDate " ;
		
		List allPotentialFreights= this.getSession().createSQLQuery(sql).list();
		
		/*Iterator it=allPotentialFreights.iterator();
		while(it.hasNext())
		{
			Object [] row=(Object[])it.next(); 
			FreightDTO frieghtDTO=new FreightDTO();
			if(row[0] == null){frieghtDTO.setCarrier("");}else{frieghtDTO.setCarrier(row[0]);}
			if(row[1] == null){frieghtDTO.setOrigin("");}else{frieghtDTO.setOrigin(row[1]);}
			if(row[2] == null){frieghtDTO.setOriginPort("");}else{frieghtDTO.setOriginPort(row[2]);}
			if(row[3] == null){frieghtDTO.setDestination("");}else{frieghtDTO.setDestination(row[3]);}
			if(row[4] == null){frieghtDTO.setDestPort("");}else{frieghtDTO.setDestPort(row[4]);}
			if(row[5] == null){frieghtDTO.setEquipmentType("");}else{frieghtDTO.setEquipmentType(row[5]);}
			if(row[6] == null){frieghtDTO.setTotalPrice("");}else{frieghtDTO.setTotalPrice(row[6]);}
			if(row[7] == null){frieghtDTO.setLaneComments("");}else{frieghtDTO.setLaneComments(row[7]);}
			if(row[8] == null){frieghtDTO.setDestTHCValue("");}else{frieghtDTO.setDestTHCValue(row[8]);}
			if(row[9] == null){frieghtDTO.setDestTHCDetails("");}else{frieghtDTO.setDestTHCDetails(row[9]);}
			if(row[10] == null){frieghtDTO.setId("");}else{frieghtDTO.setId(row[10]);}
			if(row[11] == null){frieghtDTO.setBaseCurrency("");}else{frieghtDTO.setBaseCurrency(row[11]);}
			if(row[12] == null){frieghtDTO.setContractType("");}else{frieghtDTO.setContractType(row[12]);}
			if(row[13] == null){frieghtDTO.setOriginPortCode("");}else{frieghtDTO.setOriginPortCode(row[13]);}
			if(row[14] == null){frieghtDTO.setDestinationPortCode("");}else{frieghtDTO.setDestinationPortCode(row[14]);}
			if(row[15] == null){frieghtDTO.setOriginPortCodeName("");}else{frieghtDTO.setOriginPortCodeName(row[15]);}
			if(row[16] == null){frieghtDTO.setDestinationPortCodeName("");}else{frieghtDTO.setDestinationPortCodeName(row[16]);}
			frightList.add(frieghtDTO);
		}
		return frightList;*/
		return allPotentialFreights;
	}

	
	private String getPorts(String portRange, String mode, String country, String latitude, String longitude) { 
		String sql="";
		if(mode.equalsIgnoreCase("Sea"))
		{
			sql = "select portcode, portName, latitude, longitude, id from port where countryCode='"+country+"' and mode='"+mode+"' and latitude!='' and longitude!='' and active=true";
		}
		if(mode.equalsIgnoreCase("Air"))
		{
			sql = "select portcode, portName, latitude, longitude, id from port where countryCode='"+country+"' and mode='"+mode+"' and latitude!='' and longitude!='' and active=true and isCargoAirport=true";
		}
		List allPotentialPorts= this.getSession().createSQLQuery(sql).list();
		StringBuilder ports = new StringBuilder();
		int count = 0;
		Iterator it=allPotentialPorts.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			double distance = getDistance(Double.parseDouble(obj[2].toString()), Double.parseDouble(obj[3].toString()), Double.parseDouble(latitude), Double.parseDouble(longitude));
			if(distance<= Double.parseDouble(portRange)){	
			count++;
				
				if (count > 1) ports.append(","); else ports.append("");
				ports.append(obj[0]);
			}
		}
		return ports.toString();
	}
	
	


	private double getDistance(double portLatitude, double portLongitude, double latitude, double longitude) {
		double distance = distance(latitude,
				longitude, portLatitude, portLongitude, "M");
		return distance;
	}


	private double distance(double latitude, double longitude, double portLatitude, double portLongitude, String unit) {
		double theta = longitude - portLongitude;
		double dist = Math.sin(deg2rad(latitude)) * Math.sin(deg2rad(portLatitude))
				+ Math.cos(deg2rad(latitude)) * Math.cos(deg2rad(portLatitude))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == "K") {
			dist = dist * 1.609344;
		} else if (unit == "N") {
			dist = dist * 0.8684;
		}
		return (dist);
	}
	
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}
	
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}


	public  class FreightDTO
	{
		private Object id;
		private Object carrier;
		private Object origin;
		private Object originPort;
		private Object destination;
		private Object destPort;
		private Object equipmentType;
		private Object totalPrice;
		private Object laneComments;
		private Object destTHCValue;
		private Object destTHCDetails;
		private Object baseCurrency;
		private Object contractType;
		private Object originPortCode;
		private Object destinationPortCode;
		private Object originPortCodeName;
		private Object destinationPortCodeName;
		
		public Object getDestinationPortCode() {
			return destinationPortCode;
		}
		public void setDestinationPortCode(Object destinationPortCode) {
			this.destinationPortCode = destinationPortCode;
		}
		public Object getOriginPortCode() {
			return originPortCode;
		}
		public void setOriginPortCode(Object originPortCode) {
			this.originPortCode = originPortCode;
		}
		public Object getBaseCurrency() {
			return baseCurrency;
		}
		public void setBaseCurrency(Object baseCurrency) {
			this.baseCurrency = baseCurrency;
		}
		
		public Object getCarrier() {
			return carrier;
		}
		public void setCarrier(Object carrier) {
			this.carrier = carrier;
		}
		
		public Object getDestTHCDetails() {
			return destTHCDetails;
		}
		public void setDestTHCDetails(Object destTHCDetails) {
			this.destTHCDetails = destTHCDetails;
		}
		public Object getDestTHCValue() {
			return destTHCValue;
		}
		public void setDestTHCValue(Object destTHCValue) {
			this.destTHCValue = destTHCValue;
		}
		public Object getEquipmentType() {
			return equipmentType;
		}
		public void setEquipmentType(Object equipmentType) {
			this.equipmentType = equipmentType;
		}
		public Object getLaneComments() {
			return laneComments;
		}
		public void setLaneComments(Object laneComments) {
			this.laneComments = laneComments;
		}
		
		public Object getDestination() {
			return destination;
		}
		public void setDestination(Object destination) {
			this.destination = destination;
		}
		public Object getDestPort() {
			return destPort;
		}
		public void setDestPort(Object destPort) {
			this.destPort = destPort;
		}
		public Object getOrigin() {
			return origin;
		}
		public void setOrigin(Object origin) {
			this.origin = origin;
		}
		public Object getOriginPort() {
			return originPort;
		}
		public void setOriginPort(Object originPort) {
			this.originPort = originPort;
		}
		public Object getTotalPrice() {
			return totalPrice;
		}
		public void setTotalPrice(Object totalPrice) {
			this.totalPrice = totalPrice;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getContractType() {
			return contractType;
		}
		public void setContractType(Object contractType) {
			this.contractType = contractType;
		}
		public Object getDestinationPortCodeName() {
			return destinationPortCodeName;
		}
		public void setDestinationPortCodeName(Object destinationPortCodeName) {
			this.destinationPortCodeName = destinationPortCodeName;
		}
		public Object getOriginPortCodeName() {
			return originPortCodeName;
		}
		public void setOriginPortCodeName(Object originPortCodeName) {
			this.originPortCodeName = originPortCodeName;
		}
		
	}


	public List getSelectedRefFreight(Long id, String tariffApplicability, String sessionCorpID) {
		List refId=getHibernateTemplate().find("select reffreightId from PricingFreight where pricingControlID="+id+" and freightSelection is true");
		return getHibernateTemplate().find("from RefFreightRates where id="+refId.get(0)+"");
	}

	public List getSelectedFreightFromDetails(Long id, String sessionCorpID) {
		return getHibernateTemplate().find("select reffreightId from PricingFreight where pricingControlID="+id+" and freightSelection is true and corpID='"+sessionCorpID+"'");
	}

	public void selectMinimumQuoteAgent(Long id, String tarrifApplicability, String sessionCorpID, Boolean isFreight) {
			if(isFreight){
				List minimumQuote=this.getSession().createSQLQuery("select concat(id,'#',rateMarkUp*exchangeRate) from pricingcontroldetails where pricingControlID="+id+" and tarrifApplicability='"+tarrifApplicability+"'  order by rateMarkUp*exchangeRate + discountValue asc limit 1").list();
				String[] sArray=minimumQuote.get(0).toString().split("#");
				String pricingcontroldetailsId=sArray[0];
				getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=true where id="+pricingcontroldetailsId+"");
			}else{
				
				List minimumQuote=this.getSession().createSQLQuery("select concat(id,'#',rateMarkUp*exchangeRate) from pricingcontroldetails where pricingControlID="+id+" and tarrifApplicability='"+tarrifApplicability+"' order by rateMarkUp*exchangeRate + discountValue asc limit 1").list();
				String[] sArray=minimumQuote.get(0).toString().split("#");
				String pricingcontroldetailsId=sArray[0];
				getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=true, preferredPort=true where id="+pricingcontroldetailsId+"");
			}
			
	}

	public void selectMinimumFreight(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, Long pricingControlID, String baseCurrency, String mode, String contract, String originPortRange, String destinationPortRange, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude) {
		/*SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedExpectedLoadDate = new StringBuilder(dateformatYYYYMMDD1.format(expectedLoadDate));
		try{
			String originPortTemp=getPorts(originPortRange,mode,originCountry, originLatitude, originLongitude);
			originPOE = originPortTemp.replaceAll(",", "','");
		}catch(Exception ex){
		}
		try{
			String destinationPortTemp=getPorts(destinationPortRange,mode,destinationCountry, destinationLatitude, destinationLongitude);
			destinationPOE = destinationPortTemp.replaceAll(",", "','");
		}catch(Exception ex){
		}
		String sql="SELECT concat(r.id,'#',totalPrice,'#',baseCurrency)  " +
				"FROM reffreightrates r, port po, port pd " +
				"where r.originCity = po.refAbbreviation " +
				"and r.destinationCity = pd.refAbbreviation " +
				"and po.portCode in( '"+originPOE+"' ) and pd.portCode in ( '"+destinationPOE+"') " +
				"and equipmentType like '"+containerSize+"%' " +
				"and '"+formatedExpectedLoadDate+"' between LineItemEffDate and LineItemExpDate order by totalPrice asc limit 1";
		System.out.println("\n\n\n\n\n\n selectMinimumFreight---->"+sql);
		List potentialFreights= this.getSession().createSQLQuery(sql).list();
		
		String[] sArray=potentialFreights.get(0).toString().split("#");
		
		String id=sArray[0];
		String totalPrice=sArray[1];
		Double markup=0.0;
		String markupSQl="select concat(profitFlat,'#', profitPercent)  from freightmarkupcontrol where "+totalPrice+">=lowCost and "+totalPrice+"<=highCost and contract='"+contract+"' and mode='"+mode+"'";
		List markupList= this.getSession().createSQLQuery(markupSQl).list();
		if(!markupList.isEmpty()){
			String markupListString=markupList.get(0).toString();
			String[] markupArray=markupListString.split("#");
			Double profitFlat=Double.parseDouble(markupArray[0]);
			Double profitPercent=Double.parseDouble(markupArray[1]);
			markup=Double.parseDouble(totalPrice)*(profitPercent/100.00) + profitFlat;
		}
		
		
		String freightCurrency="";
		try{
			freightCurrency=sArray[2];
		}catch(Exception ex)
		{
			freightCurrency="USD";
		}
		Double exchangeRate=1.0;
		if(!baseCurrency.equalsIgnoreCase(freightCurrency))
		{
    		List exchangeRateList=getHibernateTemplate().find("select currencyBaseRate from ExchangeRate where baseCurrency='"+baseCurrency+"' and currency='"+freightCurrency+"'");
    		if(!exchangeRateList.isEmpty())
    		{
    			exchangeRate=Double.parseDouble(exchangeRateList.get(0).toString());
    		}
		}
		getHibernateTemplate().bulkUpdate("update PricingControlDetails set refFreightId="+id+" , freightSelection=true, exchangeRate="+exchangeRate+" , currency='"+freightCurrency+"', frieghtMarkup="+markup+" where pricingControlID="+pricingControlID+" and tarrifApplicability='Freight'");*/
		//String sql="select id from pricingfreight where pricingControlId="+pricingControlID+" and originPortCode='"+originPOE+"' and destinationPortCode='"+destinationPOE+"'order by freightPrice+freightMarkup asc limit 1";
		String sql="select id from pricingfreight where pricingControlId="+pricingControlID+" order by freightPrice+freightMarkup asc limit 1";
		List minimumFreight= this.getSession().createSQLQuery(sql).list();
		if(!minimumFreight.isEmpty()){
			getHibernateTemplate().bulkUpdate("update PricingFreight set freightSelection= true where id="+minimumFreight.get(0).toString()+"");
		}
	}

	public String getPortsCode(String portRange, String mode, String country, String latitude, String longitude) {
		String sql="";
		if(mode.equalsIgnoreCase("Sea"))
		{
			sql = "select portcode, portName, latitude, longitude, id from port where countryCode='"+country+"' and mode='"+mode+"' and latitude!='' and longitude!='' and active=true";
		}
		if(mode.equalsIgnoreCase("Air"))
		{
			sql = "select portcode, portName, latitude, longitude, id from port where countryCode='"+country+"' and mode='"+mode+"' and latitude!='' and longitude!='' and active=true and isCargoAirport=true";
		}
		List allPotentialPorts= this.getSession().createSQLQuery(sql).list();
		StringBuilder ports = new StringBuilder();
		int count = 0;
		Iterator it=allPotentialPorts.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			double distance = getDistance(Double.parseDouble(obj[2].toString()), Double.parseDouble(obj[3].toString()), Double.parseDouble(latitude), Double.parseDouble(longitude));
			if(distance<= Double.parseDouble(portRange)){	
			count++;
				
				if (count > 1) ports.append(","); else ports.append("");
				ports.append(obj[0]);
			}
		}
		return ports.toString();
	}

	public String getMinimumFreightCode(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, Long pricingControlID, String baseCurrency, String mode, String contract, String originPortRange, String destinationPortRange, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude) {
		String portsCode=originPOE.concat("#").concat(destinationPOE);
		/*SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedExpectedLoadDate = new StringBuilder(dateformatYYYYMMDD1.format(expectedLoadDate));
		try{
			String originPortTemp=getPorts(originPortRange,mode,originCountry, originLatitude, originLongitude);
			originPOE = originPortTemp.replaceAll(",", "','");
		}catch(Exception ex){
		}
		try{
			String destinationPortTemp=getPorts(destinationPortRange,mode,destinationCountry, destinationLatitude, destinationLongitude);
			destinationPOE = destinationPortTemp.replaceAll(",", "','");
		}catch(Exception ex){
		}
		
		String sql="SELECT concat(po.portCode,'#',pd.portCode)  " +
				"FROM reffreightrates r, port po, port pd " +
				"where r.originCity = po.refAbbreviation " +
				"and r.destinationCity = pd.refAbbreviation " +
				"and po.portCode in( '"+originPOE+"' ) and pd.portCode in ( '"+destinationPOE+"') " +
				"and equipmentType like '"+containerSize+"%' " +
				"and '"+formatedExpectedLoadDate+"' between LineItemEffDate and LineItemExpDate order by totalPrice asc limit 1";
		System.out.println("\n\n\n\n\n\n selectMinimumFreight---->"+sql);
		List potentialFreights= this.getSession().createSQLQuery(sql).list();
		if(!potentialFreights.isEmpty()){
			portsCode=potentialFreights.get(0).toString();
		}*/
		String sql="SELECT concat(originPortCode,'#',destinationPortCode) from pricingfreight where pricingControlId="+pricingControlID+" order by freightPrice+freightMarkup asc limit 1";
		List potentialFreights= this.getSession().createSQLQuery(sql).list();
		if(!potentialFreights.isEmpty()){
			portsCode=potentialFreights.get(0).toString();
		}
		return portsCode;
	}

	public String getPortsPerFreight(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, Long id, String baseCurrency, String mode, String contract, String originPortRange, String destinationPortRange, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude) {
		String finalString=originPOE.concat("#").concat(destinationPOE);
			int originPortCount = 0;
			int destinationPortCount = 0;
			StringBuilder originPorts = new StringBuilder();
			StringBuilder destinationPorts = new StringBuilder();
			
			String originSql="select distinct originPortCode from pricingfreight where pricingControlId="+id+" ";
			String destinationSql="select distinct destinationPortCode from pricingfreight where pricingControlId="+id+" ";
			
			List allOriginFreights= this.getSession().createSQLQuery(originSql).list();
			List allDestinationFreights= this.getSession().createSQLQuery(destinationSql).list();
			
			if(allOriginFreights.isEmpty() || allDestinationFreights.isEmpty()){
				return finalString;
			}
			
			Iterator it=allOriginFreights.iterator();
			while(it.hasNext())
			{
				String pCode=it.next().toString(); 
				
					originPortCount++;
					if (originPortCount > 1) originPorts.append(","); else originPorts.append("");
					originPorts.append(pCode);
			}
			
			Iterator it1=allDestinationFreights.iterator();
			while(it1.hasNext())
			{
				String pCode=it1.next().toString();
				
				destinationPortCount++;
					if (destinationPortCount > 1) destinationPorts.append(","); else destinationPorts.append("");
					destinationPorts.append(pCode);
			}
		finalString=originPorts.append("#").append(destinationPorts).toString();
		return finalString;
	
}

	public List getMasterFreightList(Long pricingControlId, String sessionCorpID) {
		List frightList= new ArrayList();
		String sql="SELECT distinct r.carrier, concat(originCity,', ',originCountry) 'Origin'," + // 0,1
		"concat(originPortCity,', ',originPortCountry) 'OriginPort'," + //2
		"concat(destinationCity,', ',destinationCountry) 'Destination'," + //3
		"concat(destinationPortCity,', ',destinationPortCountry) 'DestPort', " + //4
		"equipmentType, totalPrice, laneComments, destTHCValue, destTHCDetails, r.id, baseCurrency, " + // 5,6,7,8,9,10,11
		"left(r.contractType,4), po.portCode as originPortCode, pd.portCode as destinationPortCode, " + //12,13,14
		"po.portName as originPortCodeName, pd.portName as destinationPortCodeName " + //15,16
		"FROM reffreightrates r, port po, port pd , pricingfreight pf " + 
		"where r.originCity = po.refAbbreviation " +
		"and r.destinationCity = pd.refAbbreviation " +
		"and r.id=pf.reffreightId " +
		"and pf.pricingControlId="+pricingControlId+"";
		
		List allPotentialFreights= this.getSession().createSQLQuery(sql).list();
		
		Iterator it=allPotentialFreights.iterator();
		while(it.hasNext())
		{
			Object [] row=(Object[])it.next(); 
			FreightDTO frieghtDTO=new FreightDTO();
			if(row[0] == null){frieghtDTO.setCarrier("");}else{frieghtDTO.setCarrier(row[0]);}
			if(row[1] == null){frieghtDTO.setOrigin("");}else{frieghtDTO.setOrigin(row[1]);}
			if(row[2] == null){frieghtDTO.setOriginPort("");}else{frieghtDTO.setOriginPort(row[2]);}
			if(row[3] == null){frieghtDTO.setDestination("");}else{frieghtDTO.setDestination(row[3]);}
			if(row[4] == null){frieghtDTO.setDestPort("");}else{frieghtDTO.setDestPort(row[4]);}
			if(row[5] == null){frieghtDTO.setEquipmentType("");}else{frieghtDTO.setEquipmentType(row[5]);}
			if(row[6] == null){frieghtDTO.setTotalPrice("");}else{frieghtDTO.setTotalPrice(row[6]);}
			if(row[7] == null){frieghtDTO.setLaneComments("");}else{frieghtDTO.setLaneComments(row[7]);}
			if(row[8] == null){frieghtDTO.setDestTHCValue("");}else{frieghtDTO.setDestTHCValue(row[8]);}
			if(row[9] == null){frieghtDTO.setDestTHCDetails("");}else{frieghtDTO.setDestTHCDetails(row[9]);}
			if(row[10] == null){frieghtDTO.setId("");}else{frieghtDTO.setId(row[10]);}
			if(row[11] == null){frieghtDTO.setBaseCurrency("");}else{frieghtDTO.setBaseCurrency(row[11]);}
			if(row[12] == null){frieghtDTO.setContractType("");}else{frieghtDTO.setContractType(row[12]);}
			if(row[13] == null){frieghtDTO.setOriginPortCode("");}else{frieghtDTO.setOriginPortCode(row[13]);}
			if(row[14] == null){frieghtDTO.setDestinationPortCode("");}else{frieghtDTO.setDestinationPortCode(row[14]);}
			if(row[15] == null){frieghtDTO.setOriginPortCodeName("");}else{frieghtDTO.setOriginPortCodeName(row[15]);}
			if(row[16] == null){frieghtDTO.setDestinationPortCodeName("");}else{frieghtDTO.setDestinationPortCodeName(row[16]);}
			frightList.add(frieghtDTO);
		}
		return frightList;
	}

	public void updateRespectiveRecords(Long originAgentRecordId, Long destinationAgentRecordId, Long freightRecordId, Long pricingControlID) {
		
		getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=false,preferredPort=false where pricingControlID="+pricingControlID+" and tarrifApplicability='Origin'");
		getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=false,preferredPort=false where pricingControlID="+pricingControlID+" and tarrifApplicability='Destination'");
		getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=true,preferredPort=true where id="+originAgentRecordId+"");
		getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=true,preferredPort=true where id="+destinationAgentRecordId+"");
		
		getHibernateTemplate().bulkUpdate("update PricingFreight set freightSelection = false where pricingControlID="+pricingControlID+"");
		getHibernateTemplate().bulkUpdate("update PricingFreight set freightSelection = true where id="+freightRecordId+"");
		/*List freightRecord=getHibernateTemplate().find("select id from PricingFreight where pricingControlID="+pricingControlID+" and  reffreightId="+freightRecordId+"");
		if(!freightRecord.isEmpty()){
			getHibernateTemplate().bulkUpdate("update PricingFreight set freightSelection = false where pricingControlID="+pricingControlID+"");
			getHibernateTemplate().bulkUpdate("update PricingFreight set freightSelection = true where id="+freightRecord.get(0).toString()+"");
		}*/
	}

	public void updateFreightDetails(Long pricingControlId, Long freightAgentId) {
		getHibernateTemplate().bulkUpdate("update PricingFreight set freightSelection = false where pricingControlID="+pricingControlId+"");
		getHibernateTemplate().bulkUpdate("update PricingFreight set freightSelection = true where id="+freightAgentId+"");
	}

	public void updatePricingControlDetails(Long pricingControlId, Long agentId, String tariffApplicability) {
		if(tariffApplicability.equalsIgnoreCase("Destination")){
			getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=false,preferredPort=false where pricingControlID="+pricingControlId+" and tarrifApplicability='Destination'");
			getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=true,preferredPort=true where id="+agentId+"");
		}
		if(tariffApplicability.equalsIgnoreCase("Origin")){
			getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=false,preferredPort=false where pricingControlID="+pricingControlId+" and tarrifApplicability='Origin'");
			getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=true,preferredPort=true where id="+agentId+"");
		}

		
	}
}
