<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Agent Detail For SO</title>
<meta name="heading"
	content="Agent Detail For SO" />
	<script type="text/javascript">
function setDestinationDetail(){
	var soSetId = document.forms['popupDAGConformation'].elements['soSetId'].value;
	var ship=document.forms['popupDAGConformation'].elements['shipNumber'].value;
	location.href="copyDestinationAgentDetail.html?soGetId=${serviceOrder.id}&type=destination&soSetId="+soSetId+"&shipNumber="+ship;
}
function setDestinationAgDetail(soSetId,shipNumber)
{
	document.getElementById('okButton').disabled=false;
	document.forms['popupDAGConformation'].elements['soSetId'].value = soSetId ;
	document.forms['popupDAGConformation'].elements['shipNumber'].value = shipNumber ;
}
window.onload = function (){
	<c:if test="${hitFlag == 1}" >
	alert('DA details have been added to ${shipNumber}');
	window.open('editTrackingStatus.html?id=${soSetId}','_blank');
	self.close();
	</c:if>
}
</script>
</head>
<body>
<s:form action="" name="popupDAGConformation">
<s:hidden name="soSetId" />
<s:hidden name="shipNumber" />
</s:form >

	     <div id="otabs" style="margin-top: 10px; margin-bottom:0px;">
		  <ul>			
	  		<li><a class="current"><span>Destination Agent For SO</span></a></li>  				   
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
		<div>
		<table cellspacing="0" cellpadding="0" border="0" class="table"> 
			<thead> 	
	  	    <tr>
	  	      <th></th>
	  	      <th><b>Mode</b></th> 			
	  		  <th><b>Service Order</b></th>	  			
	  		  <th><b>City, State, Country</b></th>
	  		</tr>
	  		</thead>
	  	    <c:forEach var="soList1" items="${soList}">
	  	    <tbody>
	  	    <tr id="solistfor${soList1.id}">
	  	    	<td><input type="radio" class="input-textUpper" name="radioDesAgDetail" id="${soList1.id}" value="${soList1.id}" onclick="setDestinationAgDetail('${soList1.id}','${soList1.shipNumber}');"></td> 	       
	  			<td><input type="text" class="input-textUpper" readonly="readonly" value="${soList1.mode}"></td>
	  			<td><input type="text" class="input-text" readonly="readonly" value="${soList1.shipNumber}" ></td>
	  		    <td><input type="text" class="input-textUpper" style="width:200px;" readonly="readonly" value="${soList1.destinationCity}, ${soList1.destinationState}, ${soList1.destinationCountry}"></td>
	  		</tr>
	  		</tbody>
	  		</c:forEach>
</table>
</div>
<input type="button"  Class="cssbutton" Style="width:60px;" id="okButton" value="OK" disabled="true" onclick="setDestinationDetail()"/>
<input type="button"  Class="cssbutton" Style="width:60px;margin-left:15px;"  value="Cancel" onclick="self.close()"/>
</body>