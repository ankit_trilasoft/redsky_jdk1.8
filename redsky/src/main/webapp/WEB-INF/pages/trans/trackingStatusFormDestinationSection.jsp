<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>

<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" border="0" width="">
				<tbody>
					<tr><td height="5px"></td></tr>
					<tr>
						<c:choose>
							<c:when test="${serviceOrder.destinationCountryCode == 'USA'}" >
							<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.freeEntry32991'/></td>
							<td align="left" colspan="2" ><s:select name="trackingStatus.freeEntry3299"  cssClass="list-menu"   list="%{ENTRYFORM}" cssStyle="width:70px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="94" /></td>
							</c:when>
							<c:otherwise>
							<td align="right" class="listwhitetext"></td>
							<td align="right" class="listwhitetext" colspan="2"></td>
							</c:otherwise>
						</c:choose>
							<td></td><td></td><td width=""></td>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.originBL'/></td>
						<c:if test="${not empty trackingStatus.originBL}">
						<s:text id="trackingStatusOriginBLFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.originBL"/></s:text>
						<td width=""><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_originBL" name="trackingStatus.originBL" value="%{trackingStatusOriginBLFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_originBL-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_originBL" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.originBL}">
						<td width=""><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_originBL" name="trackingStatus.originBL" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_originBL-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_originBL" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td width=""></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.mail3299'/></td>
						<c:if test="${not empty trackingStatus.mail3299}">
						<s:text id="trackingStatusMail3299FormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.mail3299"/></s:text>
						<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_mail3299" name="trackingStatus.mail3299" value="%{trackingStatusMail3299FormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_mail3299-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_mail3299" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.mail3299}">
						<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_mail3299" name="trackingStatus.mail3299" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_mail3299-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_mail3299" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td></td><td></td><td></td>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.recived3299'/></td>
						<c:if test="${not empty trackingStatus.recived3299}">
						<s:text id="trackingStatusRecived3299FormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.recived3299"/></s:text>
						<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_recived3299" name="trackingStatus.recived3299" value="%{trackingStatusRecived3299FormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_recived3299-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_recived3299" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.recived3299}">
						<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_recived3299" name="trackingStatus.recived3299" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_recived3299-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_recived3299" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td width=""></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext" width="160px"><fmt:message key='trackingStatus.sent3299ToBroker'/></td>
						<c:if test="${not empty trackingStatus.sent3299ToBroker}">
						<s:text id="trackingStatusSent3299ToBrokerFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sent3299ToBroker"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sent3299ToBroker" name="trackingStatus.sent3299ToBroker" value="%{trackingStatusSent3299ToBrokerFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true"/></td><td><img id="trackingStatusForm_trackingStatus_sent3299ToBroker-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sent3299ToBroker" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.sent3299ToBroker}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sent3299ToBroker" name="trackingStatus.sent3299ToBroker" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_sent3299ToBroker-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sent3299ToBroker" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td></td><td></td><td></td>
						<td align="right" class="listwhitetext" colspan="1"><fmt:message key='trackingStatus.brokerDeliveryOrder'/></td>
						<c:if test="${not empty trackingStatus.brokerDeliveryOrder}">
						<s:text id="trackingStatusBrokerDeliveryOrderFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.brokerDeliveryOrder"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_brokerDeliveryOrder" name="trackingStatus.brokerDeliveryOrder" value="%{trackingStatusBrokerDeliveryOrderFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_brokerDeliveryOrder-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_brokerDeliveryOrder" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.brokerDeliveryOrder}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_brokerDeliveryOrder" name="trackingStatus.brokerDeliveryOrder" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_brokerDeliveryOrder-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_brokerDeliveryOrder" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.lastFree'/></td>
						<c:if test="${not empty trackingStatus.lastFreeDestination}">
						<s:text id="trackingStatusLastFreeDestinationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.lastFreeDestination"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_lastFreeDestination" name="trackingStatus.lastFreeDestination" value="%{trackingStatusLastFreeDestinationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_lastFreeDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_lastFreeDestination" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.lastFreeDestination}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_lastFreeDestination" name="trackingStatus.lastFreeDestination" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_lastFreeDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_lastFreeDestination" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td></td>
						<td></td>
						<td width=""></td>
						<td align="right" class="listwhitetext" colspan="1"><fmt:message key='trackingStatus.bookerDeliveryNotification'/></td>
						<c:if test="${not empty trackingStatus.bookerDeliveryNotification}">
						<s:text id="trackingStatusBookerDeliveryNotificationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.bookerDeliveryNotification"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_bookerDeliveryNotification" name="trackingStatus.bookerDeliveryNotification" value="%{trackingStatusBookerDeliveryNotificationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_bookerDeliveryNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_bookerDeliveryNotification" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.bookerDeliveryNotification}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_bookerDeliveryNotification" name="trackingStatus.bookerDeliveryNotification" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_bookerDeliveryNotification-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_bookerDeliveryNotification" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.clearCustomTA'/></td>
						<td align="left" colspan="2"><s:select cssClass="list-menu" name="trackingStatus.clearCustomTA" list="%{tan}" cssStyle="width:67px;" onchange="changeStatus();" tabindex="95"/></td>
						<td align="left" colspan="2">
						<table style="margin:0px;" cellspacing="0" cellpadding="0" bor>
						<tr>
						<c:if test="${dateColumnShow=='Y'}">
						<td style="width:3px;"></td>
						</c:if>
						<c:if test="${not empty trackingStatus.clearCustom}">
						<s:text id="trackingStatusClearCustomFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.clearCustom"/></s:text>
						<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_clearCustom" name="trackingStatus.clearCustom" value="%{trackingStatusClearCustomFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="trackingStatusForm_trackingStatus_clearCustom-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_clearCustom" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.clearCustom}">
						<td width="65"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_clearCustom" name="trackingStatus.clearCustom" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="trackingStatusForm_trackingStatus_clearCustom-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_clearCustom" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</tr>
						</table>
						</td>
						<td></td>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.containerRecivedWH'/></td>
						<c:if test="${not empty trackingStatus.containerRecivedWH}">
						<s:text id="trackingStatusContainerRecivedWHFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.containerRecivedWH"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_containerRecivedWH" name="trackingStatus.containerRecivedWH" value="%{trackingStatusContainerRecivedWHFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_containerRecivedWH-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_containerRecivedWH" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.containerRecivedWH}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_containerRecivedWH" name="trackingStatus.containerRecivedWH" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_containerRecivedWH-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_containerRecivedWH" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.pickUp2PierWHTA'/></td>
						<td align="left" colspan="2"><s:select cssClass="list-menu" name="trackingStatus.pickUp2PierWHTADestination" list="%{tan}" cssStyle="width:67px;" onchange="changeStatus();" tabindex="96" /></td>
						
						<td align="left" colspan="2">
						<table style="margin:0px;" cellspacing="0" cellpadding="0">
						<tr>
						<c:if test="${dateColumnShow=='Y'}">
						<td style="width:3px;"></td>
						</c:if>
						<c:if test="${not empty trackingStatus.pickUp2PierWHDestination}">
						<s:text id="trackingStatusPickUp2PierWHDestinationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.pickUp2PierWHDestination"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_pickUp2PierWHDestination" name="trackingStatus.pickUp2PierWHDestination" value="%{trackingStatusPickUp2PierWHDestinationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="trackingStatusForm_trackingStatus_pickUp2PierWHDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_pickUp2PierWHDestination" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.pickUp2PierWHDestination}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_pickUp2PierWHDestination" name="trackingStatus.pickUp2PierWHDestination" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="trackingStatusForm_trackingStatus_pickUp2PierWHDestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_pickUp2PierWHDestination" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</tr>
						</table>
						</td>
						<td></td>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.deliveryFollowCall'/></td>
						<c:if test="${not empty trackingStatus.deliveryFollowCall}">
						<s:text id="trackingStatusDeliveryFollowCallFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryFollowCall"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_deliveryFollowCall" name="trackingStatus.deliveryFollowCall" value="%{trackingStatusDeliveryFollowCallFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_deliveryFollowCall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_deliveryFollowCall" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.deliveryFollowCall}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_deliveryFollowCall" name="trackingStatus.deliveryFollowCall" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_deliveryFollowCall-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_deliveryFollowCall" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td align="center" valign="bottom" class="listwhitetext"><b><fmt:message key='labels.target'/></b></td>
						<td></td>
						<c:if test="${dateColumnShow=='Y'}">
						<td class="listwhitetext"  valign="bottom"><b><fmt:message key='labels.targetPackEnding'/></b></td>
						</c:if>
						<td align="center"  valign="bottom" class="listwhitetext"><b><fmt:message key='labels.actual'/></b></td>
						<c:if test="${dateColumnShow=='Y'}">
						<td  valign="bottom" class="listwhitetext"><b><fmt:message key='labels.beginPackingEnd'/></b></td>
						</c:if>
						<c:if test="${dateColumnShow=='N'}">
						<td colspan="2"></td>
						</c:if>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.deliveryReceiptDate'/></td>
						<c:if test="${not empty trackingStatus.deliveryReceiptDate}">
						<s:text id="trackingStatusDeliveryReceiptDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryReceiptDate"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_deliveryReceiptDate" name="trackingStatus.deliveryReceiptDate" value="%{trackingStatusDeliveryReceiptDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_deliveryReceiptDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_deliveryReceiptDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.deliveryReceiptDate}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_deliveryReceiptDate" name="trackingStatus.deliveryReceiptDate" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_deliveryReceiptDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_deliveryReceiptDate" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.deliveryShipper'/></td>
						<c:if test="${not empty trackingStatus.deliveryShipper}">
						<s:text id="trackingStatusDeliveryShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryShipper"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_deliveryShipper" name="trackingStatus.deliveryShipper" value="%{trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_deliveryShipper-trigger" style="vertical-align:bottom"  src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_deliveryShipper" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.deliveryShipper}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_deliveryShipper" name="trackingStatus.deliveryShipper" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_deliveryShipper-trigger" style="vertical-align:bottom"  src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_deliveryShipper" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${dateColumnShow=='Y'}">
						<td colspan="1">
						<table style="margin:0px;margin-bottom:-2px;padding:0px;" cellpadding="0" cellspacing="3">
						<tr>
						<c:if test="${not empty trackingStatus.targetdeliveryShipper}">
						<s:text id="trackingStatustargetdeliveryShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.targetdeliveryShipper"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_targetdeliveryShipper" name="trackingStatus.targetdeliveryShipper" value="%{trackingStatustargetdeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_targetdeliveryShipper-trigger" style="vertical-align:bottom"  src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_targetdeliveryShipper" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.targetdeliveryShipper}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_targetdeliveryShipper" name="trackingStatus.targetdeliveryShipper" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_targetdeliveryShipper-trigger" style="vertical-align:bottom"  src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_targetdeliveryShipper" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</tr>
						</table>
						</td>
						</c:if>
						<c:if test="${dateColumnShow=='Y'}">
						<td colspan="1">
						<table style="margin:0px;margin-bottom:-2px;padding:0px;" cellpadding="0" cellspacing="3">
						<tr>
						<c:if test="${not empty trackingStatus.actualDeliveryBegin}">
						<s:text id="trackingStatusactualDeliveryBeginFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.actualDeliveryBegin"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_actualDeliveryBegin" name="actualDeliveryBegin" value="%{trackingStatusactualDeliveryBeginFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
						<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
						<td><img id="trackingStatusForm_trackingStatus_actualDeliveryBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualDeliveryBegin" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test='${serviceOrder.status == "CNCL"}'>
						<td><img id="trackingStatusForm_trackingStatus_actualDeliveryBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualDeliveryBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
						</c:if>
						<c:if test='${serviceOrder.status == "HOLD"}'>
						<td><img id="trackingStatusForm_trackingStatus_actualDeliveryBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_actualDeliveryBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
						</c:if>
						<fmt:formatDate var="trackingStatusactualDeliveryBegin" value="${trackingStatus.actualDeliveryBegin}" pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="trackingStatus.actualDeliveryBegin" value="${trackingStatusactualDeliveryBegin}"/>
						</c:if>
						<c:if test="${empty trackingStatus.actualDeliveryBegin}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_actualDeliveryBegin" name="actualDeliveryBegin" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
						<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
						<td><img id="trackingStatusForm_trackingStatus_actualDeliveryBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_actualDeliveryBegin" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test='${serviceOrder.status == "CNCL"}'>
						<td><img id="trackingStatusForm_trackingStatus_actualDeliveryBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_actualDeliveryBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
						</c:if>
						<c:if test='${serviceOrder.status == "HOLD"}'>
						<td><img id="trackingStatusForm_trackingStatus_actualDeliveryBegin-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_actualDeliveryBegin" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
						</c:if>
						</c:if>
						</tr>
						</table>
						</td>
						</c:if>
						<c:if test="${not empty trackingStatus.deliveryA}">
						<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryA"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_deliveryA" name="deliveryA" value="%{trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
						<img id="trackingStatusForm_trackingStatus_deliveryA-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_deliveryA" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test='${serviceOrder.status == "CNCL"}'>
						<img id="trackingStatusForm_trackingStatus_deliveryA-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_deliveryA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
						</c:if>
						<c:if test='${serviceOrder.status == "HOLD"}'>
						<img id="trackingStatusForm_trackingStatus_deliveryA-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_deliveryA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
						</c:if>
						<fmt:formatDate var="trackingStatusDeliveryA" value="${trackingStatus.deliveryA}" pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="trackingStatus.deliveryA" value="${trackingStatusDeliveryA}"/>
						</c:if>
						<c:if test="${empty trackingStatus.deliveryA}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_deliveryA" name="deliveryA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/>
						<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
						<img id="trackingStatusForm_trackingStatus_deliveryA-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_deliveryA" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test='${serviceOrder.status == "CNCL"}'>
						<img id="trackingStatusForm_trackingStatus_deliveryA-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_deliveryA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
						</c:if>
						<c:if test='${serviceOrder.status == "HOLD"}'>
						<img id="trackingStatusForm_trackingStatus_deliveryA-trigger"  style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_deliveryA" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
						</c:if>
						</c:if>
						<c:if test="${dateColumnShow=='N'}">
						<td></td>
						<td></td>
						</c:if>
						<configByCorp:fieldVisibility componentId="component.field.trackingStatus.CDelieveryPODDate">
						<td align="right" class="listwhitetext">Confirmation of Delivery</td>
						<c:if test="${not empty trackingStatus.confirmationOfDelievery}">
						<s:text id="trackingStatusConfirmationOfDelieveryFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.confirmationOfDelievery"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_confirmationOfDelievery" name="trackingStatus.confirmationOfDelievery" value="%{trackingStatusConfirmationOfDelieveryFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_confirmationOfDelievery-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_confirmationOfDelievery" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.confirmationOfDelievery}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_confirmationOfDelievery" name="trackingStatus.confirmationOfDelievery" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_confirmationOfDelievery-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_confirmationOfDelievery" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</configByCorp:fieldVisibility>
						<td width=""></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.returnContainer'/></td>
						<td align="left" colspan="2"><s:select cssClass="list-menu" name="trackingStatus.returnContainerDestination" list="%{tan}" cssStyle="width:67px;" onchange="changeStatus();" tabindex="97"/></td>
						
						<td align="left" colspan="2">
						<table style="margin:0px;" cellspacing="0" cellpadding="0">
						<tr>
						<c:if test="${dateColumnShow=='Y'}">
						<td style="width:105px;"></td>
						</c:if>
						<c:if test="${not empty trackingStatus.returnContainerDateADestination}">
						<s:text id="trackingStatusreturnContainerDateADestinationFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.returnContainerDateADestination"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_returnContainerDateADestination" name="trackingStatus.returnContainerDateADestination" value="%{trackingStatusreturnContainerDateADestinationFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="trackingStatusForm_trackingStatus_returnContainerDateADestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_returnContainerDateADestination" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.returnContainerDateADestination}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_returnContainerDateADestination" name="trackingStatus.returnContainerDateADestination" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td>&nbsp;<img id="trackingStatusForm_trackingStatus_returnContainerDateADestination-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_returnContainerDateADestination" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</tr>
						</table>
						</td>
						<td></td>
						<configByCorp:fieldVisibility componentId="component.field.trackingStatus.CDelieveryPODDate">
						<td align="right" class="listwhitetext">POD to Booker Sent/Rec'd</td>
						<c:if test="${not empty trackingStatus.podToBooker}">
						<s:text id="trackingStatusPodToBookerFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.podToBooker"/></s:text>
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_podToBooker" name="trackingStatus.podToBooker" value="%{trackingStatusPodToBookerFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_podToBooker-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_podToBooker" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.podToBooker}">
						<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_podToBooker" name="trackingStatus.podToBooker" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_podToBooker-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_podToBooker" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</configByCorp:fieldVisibility>
						<td width=""></td>
					</tr>

					<tr>
						<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedFromOA"><fmt:message key='trackingStatus.documentReceivedFromOA'/></configByCorp:fieldVisibility></td>
						<c:if test="${not empty trackingStatus.documentReceivedFromOA}">
						<s:text id="trackingStatusDocumentReceivedFromOAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.documentReceivedFromOA"/></s:text>
						<td><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedFromOA"><s:textfield cssClass="input-text" id="documentReceivedFromOA" name="trackingStatus.documentReceivedFromOA" value="%{trackingStatusDocumentReceivedFromOAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedFromOA"><img id="documentReceivedFromOA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
						</c:if>
						<c:if test="${empty trackingStatus.documentReceivedFromOA}">
						<td><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedFromOA"><s:textfield cssClass="input-text" id="documentReceivedFromOA" name="trackingStatus.documentReceivedFromOA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.documentReceivedFromOA"><img id="documentReceivedFromOA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
						</c:if>
						
						<td colspan="3">
							<div id="hid">
								<table class="detailTabLabel" border="0">
									<tbody>
									<tr>
									<td width="16"></td>								
									<td align="right" class="listwhitetext" width="150px"><fmt:message key='trackingStatus.sentcomp'/></td>
									<c:if test="${not empty trackingStatus.sentcomp}">
									<s:text id="trackingStatusSentcompFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sentcomp"/></s:text>
									<td><s:textfield cssClass="input-text" id="sentcomp" name="trackingStatus.sentcomp" value="%{trackingStatusSentcompFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="sentcomp-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>
									<c:if test="${empty trackingStatus.sentcomp}">
									<td><s:textfield cssClass="input-text" id="sentcomp" name="trackingStatus.sentcomp" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="sentcomp-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>
									</tr>
								</tbody>
							</table>
							</div>
						</td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="trackingStatus.deliveryReceiptSentOA"><fmt:message key='trackingStatus.deliveryReceiptSentOA'/></configByCorp:fieldVisibility></td>
						<c:if test="${not empty trackingStatus.deliveryReceiptSentOA}">
						<s:text id="trackingStatusDeliveryReceiptSentOAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryReceiptSentOA"/></s:text>
						<td><configByCorp:fieldVisibility componentId="trackingStatus.deliveryReceiptSentOA"><s:textfield cssClass="input-text" id="deliveryReceiptSentOA" name="trackingStatus.deliveryReceiptSentOA" value="%{trackingStatusDeliveryReceiptSentOAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.deliveryReceiptSentOA"><img id="deliveryReceiptSentOA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
						</c:if>
						<c:if test="${empty trackingStatus.deliveryReceiptSentOA}">
						<td><configByCorp:fieldVisibility componentId="trackingStatus.deliveryReceiptSentOA"><s:textfield cssClass="input-text" id="deliveryReceiptSentOA" name="trackingStatus.deliveryReceiptSentOA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></configByCorp:fieldVisibility></td><td><configByCorp:fieldVisibility componentId="trackingStatus.deliveryReceiptSentOA"><img id="deliveryReceiptSentOA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
						</c:if>						
					</tr>
					<tr>
					<configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">	
	                    <c:if test="${serviceOrder.job =='INT'}"> 	
						<td align="right" class="listwhitetext">Location Type</td>
                        <td align="left" colspan="5"><configByCorp:customDropDown 	listType="map" list="${locationTypes}" fieldValue="${trackingStatus.destinationLocationType}"
		                attribute="id=trackingStatusForm_trackingStatus_destinationLocationType class=list-menu name=trackingStatus.destinationLocationType  style=width:235px  headerKey='' headerValue=''  tabindex='' "/></td>
		               	</c:if>
						</configByCorp:fieldVisibility>
					</tr>
					<tr>
						<td colspan="8"></td>
						<c:if test="${empty serviceOrder.id}">
						<td align="right" width="285px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
						</c:if>
						<c:if test="${not empty serviceOrder.id}">
						<c:choose>
						<c:when test="${countDestinationStatusNotes == '0' || countDestinationStatusNotes == '' || countDestinationStatusNotes ==null}">
						<td align="right" width="285px"><img id="countDestinationStatusNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DestinationStatus&imageId=countDestinationStatusNotesImage&fieldId=countDestinationStatusNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DestinationStatus&imageId=countDestinationStatusNotesImage&fieldId=countDestinationStatusNotes&decorator=popup&popup=true',800,600);" ></a></td>
						</c:when>
						<c:otherwise>
						<td align="right" width="285px"><img id="countDestinationStatusNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DestinationStatus&imageId=countDestinationStatusNotesImage&fieldId=countDestinationStatusNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DestinationStatus&imageId=countDestinationStatusNotesImage&fieldId=countDestinationStatusNotes&decorator=popup&popup=true',800,600);" ></a></td>
						</c:otherwise>
						</c:choose> 
						</c:if>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
<tr>	
	<td height="10" width="100%" align="left" style="margin:0px;">
	     <c:if test="${from=='rule'}">
	       <c:if test="${field=='trackingStatus.sitDestinationTA'}">
		     <div  onClick="javascript:animatedcollapse.toggle('sitdestination')" style="margin:0px;">
				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
				<tr>
				<td class="headtab_left">
				</td>
				<td NOWRAP class="headtab_center"><font color="red">&nbsp;SIT @ Destination
				</td>
				<td width="28" valign="top" class="headtab_bg"></td>
				<td class="headtab_bg_center">&nbsp;
				</td>
				<td class="headtab_right">
				</td>
				</tr>
				</table>
			</div>
	</c:if>
	     </c:if>
	 <c:if test="${from=='rule'}">
	       <c:if test="${field!='trackingStatus.sitDestinationTA'}">
		     <div  onClick="javascript:animatedcollapse.toggle('sitdestination')" style="margin:0px;">
	    <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">    
	  <tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">&nbsp;SIT @ Destination
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table></div>
	</c:if>
	     </c:if> 
	 <c:if test="${from!='rule'}">       
		     <div  onClick="javascript:animatedcollapse.toggle('sitdestination')" style="margin:0px;">
	    <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">    
	  <tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">&nbsp;SIT @ Destination
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table></div>
	</c:if>
	     </div>
			<div id="sitdestination" class="switchgroup1">		
			<table class="detailTabLabel" border="0" width="" style="margin-left:130px;">
	 		  <tbody>
	 		  <tr><td height="5px"></td></tr>
	<tr>
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.sitAuthorize'/></td>
	<td align="left" ><s:textfield cssClass="input-text" cssStyle="width:65px;"  name="trackingStatus.sitAuthorizeDestination" required="true"  maxlength="10" tabindex="98" /></td>
	<td align="right" class="listwhitetext" colspan="2" width="85"><fmt:message key='trackingStatus.sitDestinationDaysAllowed'/></td>
	<td align="left" ><s:textfield cssClass="input-text"  name="trackingStatus.sitDestinationDaysAllowed" required="true"cssStyle="width:65px" maxlength="10" onchange="onlyNumeric(this);" tabindex="99" /></td>
	<td align="right" class="listwhitetext" colspan="2" width="95"><fmt:message key='trackingStatus.sitDestinationYN'/></td>
	<td align="left"><s:select cssClass="list-menu" name="trackingStatus.sitDestinationYN" list="%{yesno}" cssStyle="width:70px" onchange="changeStatus();" tabindex="100" /></td>
	<td></td>
	<td align="right"class="listwhitetext" width="65"><fmt:message key='trackingStatus.sitDestinationLetterTA'/></td>
	<td align="left" colspan="2"><s:select cssClass="list-menu" name="trackingStatus.sitDestinationLetterTA" list="%{tan}" cssStyle="width:67px" onchange="changeStatus();" tabindex="101" /></td>
	<td></td>
	<td colspan="2">
	<table cellspacing="0" cellpadding="0" class="detailTabLabel" width="92px">
	<tr>
	<c:if test="${not empty trackingStatus.sitDestinationLetter}">
	<s:text id="trackingStatusSitDestinationLetterFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitDestinationLetter"/></s:text>
	<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitDestinationLetter" name="trackingStatus.sitDestinationLetter" value="%{trackingStatusSitDestinationLetterFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_sitDestinationLetter-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationLetter" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<c:if test="${empty trackingStatus.sitDestinationLetter}">
	<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitDestinationLetter" name="trackingStatus.sitDestinationLetter" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="trackingStatusForm_trackingStatus_sitDestinationLetter-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationLetter" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.sitDestinationIn'/></td>
	<td align="left" colspan="3"><s:select cssClass="list-menu" name="trackingStatus.sitDestinationInCondition" list="%{tan}" cssStyle="width:67px" onchange="changeStatus();"  /></td>
	<c:if test="${not empty trackingStatus.sitDestinationIn}">
	<s:text id="trackingStatusSitDestinationInFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitDestinationIn"/></s:text>
	<td width="70"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitDestinationIn" name="trackingStatus.sitDestinationIn" value="%{trackingStatusSitDestinationInFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td>
	<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
	<td><img id="trackingStatusForm_trackingStatus_sitDestinationIn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationIn" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
	</c:if>
	<c:if test='${serviceOrder.status == "CNCL"}'>
	<td><img id="trackingStatusForm_trackingStatus_sitDestinationIn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationIn" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
	</c:if>
	<c:if test='${serviceOrder.status == "HOLD"}'>
	<td><img id="trackingStatusForm_trackingStatus_sitDestinationIn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationIn" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
	</c:if>
	</c:if>
	<c:if test="${empty trackingStatus.sitDestinationIn}">
	<td width="70"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitDestinationIn" name="trackingStatus.sitDestinationIn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
	<c:if test='${serviceOrder.status != "CNCL" && serviceOrder.status != "HOLD"}'>
	<td><img id="trackingStatusForm_trackingStatus_sitDestinationIn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationIn" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
	</c:if>
	<c:if test='${serviceOrder.status == "CNCL"}'>
	<td><img id="trackingStatusForm_trackingStatus_sitDestinationIn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationIn" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is in Cancel status, Please Re-OPEN the status before changing dates.')"/></td>
	</c:if>
	<c:if test='${serviceOrder.status == "HOLD"}'>
	<td><img id="trackingStatusForm_trackingStatus_sitDestinationIn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationIn" HEIGHT=20 WIDTH=20 onclick="javascript:alert('The Shipment is on Hold status, Please Re-OPEN the status before changing dates.')"/></td>
	</c:if>
	</c:if>
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.sitDestinationA'/></td> 
	<td align="left" ><s:select cssClass="list-menu" name="trackingStatus.sitDestinationTA" list="%{tan}" cssStyle="width:70px" onchange="changeStatus();" tabindex="102" /></td>
	<c:if test="${from=='rule'}">
	<c:if test="${field=='trackingStatus.sitDestinationA'}">
	<c:if test="${not empty trackingStatus.sitDestinationA}">
	<s:text id="trackingStatusSitDestinationAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitDestinationA"/></s:text>
	<td width="60"><s:textfield cssClass="rules-textUpper" id="trackingStatusForm_trackingStatus_sitDestinationA" name="trackingStatus.sitDestinationA" value="%{trackingStatusSitDestinationAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td>
	<td><img id="trackingStatusForm_trackingStatus_sitDestinationA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationA" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
	</c:if>
	<c:if test="${empty trackingStatus.sitDestinationA}">
	<td><s:textfield cssClass="rules-textUpper" id="trackingStatusForm_trackingStatus_sitDestinationA" name="trackingStatus.sitDestinationA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td><td><img id="trackingStatusForm_trackingStatus_sitDestinationA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationA" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
	</c:if>
	</c:if></c:if>
	<c:if test="${from=='rule'}">
	<c:if test="${field!='trackingStatus.sitDestinationA'}">
	<c:if test="${not empty trackingStatus.sitDestinationA}">
	<s:text id="trackingStatusSitDestinationAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitDestinationA"/></s:text>
	<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitDestinationA" name="trackingStatus.sitDestinationA" value="%{trackingStatusSitDestinationAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td>
	<td><img id="trackingStatusForm_trackingStatus_sitDestinationA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationA" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
	</c:if>
	<c:if test="${empty trackingStatus.sitDestinationA}">
	<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitDestinationA" name="trackingStatus.sitDestinationA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td><td><img id="trackingStatusForm_trackingStatus_sitDestinationA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationA" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
	</c:if></c:if></c:if>
	<td colspan="2"></td>
	<c:if test="${from!='rule'}">
	<c:if test="${not empty trackingStatus.sitDestinationA}">
	<s:text id="trackingStatusSitDestinationAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitDestinationA"/></s:text>
	<td><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitDestinationA" name="trackingStatus.sitDestinationA" value="%{trackingStatusSitDestinationAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td>
	<td><img id="trackingStatusForm_trackingStatus_sitDestinationA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationA" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
	</c:if>
	<c:if test="${empty trackingStatus.sitDestinationA}">
	<td width="60"><s:textfield cssClass="input-text" id="trackingStatusForm_trackingStatus_sitDestinationA" name="trackingStatus.sitDestinationA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onblur="calcDays()"/></td><td><img id="trackingStatusForm_trackingStatus_sitDestinationA-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_sitDestinationA" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
	</c:if>
	</c:if>
	<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.sitDestinationDays'/></td>
	<td align="left" ><s:textfield cssClass="input-text" id="sitDestDay" name="trackingStatus.sitDestinationDays" required="true" cssStyle="width:66px" maxlength="5" tabindex="103" onselect="calcDays();" onchange="calcDays();"/></td>
	<td align="right" class="listwhitetext" width="65px" >SIT Reason</td>
	<td><s:select name="trackingStatus.destinationSITReason" cssClass="list-menu" list="%{sitDestinationReason}" headerKey="" headerValue="" cssStyle="width:70px" tabindex="104"/></td>
	
	<c:if test="${empty serviceOrder.id}">
	<td align="right" width="100px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
	</c:if>
	<c:if test="${not empty serviceOrder.id}">
	<c:choose>
	<c:when test="${countSitDestinationNotes == '0' || countSitDestinationNotes == '' || countSitDestinationNotes == null}">
	<td align="right" width="100px"><img id="countSitDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SitDestination&imageId=countSitDestinationNotesImage&fieldId=countSitDestinationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SitDestination&imageId=countSitDestinationNotesImage&fieldId=countSitDestinationNotes&decorator=popup&popup=true',800,600);" ></a></td>
	</c:when>
	<c:otherwise>
	<td align="right" width="100px"><img id="countSitDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SitDestination&imageId=countSitDestinationNotesImage&fieldId=countSitDestinationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SitDestination&imageId=countSitDestinationNotesImage&fieldId=countSitDestinationNotes&decorator=popup&popup=true',800,600);" ></a></td>
	</c:otherwise>
	</c:choose> 
	</c:if>
	</tr>
	<tr>
	<td  class="listwhitetext" align="right" height="21px">RequestSIT</td>
	<c:if test="${not empty trackingStatus.requestDestinationSit}">
	<s:text id="RequestDestinationSitFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.requestDestinationSit"/></s:text>
	<td style="width:70px;"><s:textfield id="requestDestinationSit" cssClass="input-text" name="trackingStatus.requestDestinationSit" value="%{RequestDestinationSitFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="requestDestinationSit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<c:if test="${ empty trackingStatus.requestDestinationSit}">
	<td style="width:70px;"><s:textfield id="requestDestinationSit" cssClass="input-text" name="trackingStatus.requestDestinationSit" value="%{RequestDestinationSitFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="requestDestinationSit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<td  class="listwhitetext" align="right" height="21px">GrantedSIT</td>
	<c:if test="${not empty trackingStatus.receivedDestinationSit}">
	<s:text id="ReceivedDestinationSitFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.receivedDestinationSit"/></s:text>
	<td style="width:100px;" colspan="3"><s:textfield id="receivedDestinationSit" cssClass="input-text" name="trackingStatus.receivedDestinationSit" value="%{ReceivedDestinationSitFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="receivedDestinationSit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<c:if test="${ empty trackingStatus.receivedDestinationSit}">
	<td style="width:100px;" colspan="3"><s:textfield id="receivedDestinationSit" cssClass="input-text" name="trackingStatus.receivedDestinationSit" value="%{ReceivedDestinationSitFormattedValue}" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="receivedDestinationSit-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	</tr>
	 <tr><td height="2px"></td></tr>
	</tbody>
	</table>
	</div>
	</td>
<td></td></tr>
<tr>
<td></td></tr>  
</tbody>
</table>