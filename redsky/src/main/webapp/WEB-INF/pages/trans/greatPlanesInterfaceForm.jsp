<%@ include file="/common/taglibs.jsp"%> 
<head>
	<title>Great Plains Interface</title>   
    <meta name="heading" content="Great Plains Interface"/> 
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">

</script>
	<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 

</head>
<body>
<s:form id="imfEstimateNew" name="imfEstimateNew" action="" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:100% " ><!-- sandeep -->
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
<table cellspacing="1" cellpadding="1" border="0" > 
<tr><td height="10px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >Great Plains Interface</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px" border="0">
<tr>
<td width="20px"></td>  
<td>
<input type="button" class="cssbutton" style="width:165px;" 
        			onclick="location.href='<c:url value="/gpInvoiceCompanyCode.html"/>'"  
        			value="Invoice Data Extract"/> 
        			</td>

<td width="10px"></td>
<td><input type="button" class="cssbutton" style="width:165px;" 
        			onclick="location.href='<c:url value="/gpPartnerExtracts.html"/>'"  
        			value="Partner Data Extract"/> </td>


</tr>
<tr>     			
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:165px;" 
        			onclick="location.href='<c:url value="/gpPayableCompanyCode.html"/>'"  
        			value="Payable Data Extract "/></td>
</tr>
<tr>       			
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:165px; " 
        			onclick="location.href='<c:url value="/greatPlainsPmtUpdate.html"/>'"  
        			value="Great Plains Pmt Update "/></td>
</tr>
<tr>
<td width="20px"></td>
<td><input type="button" class="cssbutton" style="width:165px;" 
        			onclick="location.href='<c:url value="/gpSubcontractorExtract.html"/>'"  
        			value="Subcontractor Pay Extract "/></td>
</tr>
</table>
</td></tr></table>


<table cellspacing="1" cellpadding="1" border="0" > 
<tr><td height="10px"></td></tr>
<tr>
<td width="10px;"></td>
<td class="bgblue" >Great Plains Maintenance</td>
</tr>
<tr><td height="5px"></td></tr>
<tr>
<td></td>
<td>
<table style="margin-bottom:5px" border="0">
<tr>
<td width="20px"></td>  

<td> 
	<input type="button" class="cssbutton" style="width:220px;" 
        			onclick="location.href='<c:url value="/regenerateGPInvoiceProcessing.html"/>'"  
        			value="Regenerate Invoice Data Extract"/> 
        			</td>
</tr> 
<tr>     			
<td width="20px"></td>        			
<td><input type="button" class="cssbutton" style="width:220px;" 
        			onclick="location.href='<c:url value="/regenerateGPPayableProcessing.html"/>'"  
        			value="Regenerate Payable Data Extract"/> 
        			</td>
        			</tr>
</table>
</td></tr></table>

</div>
<div class="bottom-header"><span></span></div> 

</div></div></div></s:form></body> 