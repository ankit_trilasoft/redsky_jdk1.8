//--Created By Bibhash Kumar pathak--

package com.trilasoft.app.dao.hibernate;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DataCatalogDao;
import com.trilasoft.app.model.DataCatalog;

public class DataCatalogDaoHibernate extends GenericDaoHibernate<DataCatalog, Long> implements DataCatalogDao{
	
	private List dataCatalogs;
	public DataCatalogDaoHibernate() {   
        super(DataCatalog.class);   
    }  
	
	static String replaceWord(String original, String find, String replacement)
	{
		int i = original.indexOf(find);
	    if (i < 0) {
	        return original;  // return original if 'find' is not in it.
	    }
	  
	    String partBefore = original.substring(0, i);
	    String partAfter  = original.substring(i + find.length());
	  
	    return partBefore + replacement + partAfter;

	}
	
	public List findById(Long id) {
    	return getHibernateTemplate().find("select id from DataCatalog where id=?",id);
    }

	public List findMaximumId() {
    	return getHibernateTemplate().find("select max(id) from DataCatalog");
    }
	
	public List configurableTable(boolean b)
	{
		return getHibernateTemplate().find("SELECT distinct(tableName)  FROM DataCatalog  where configerable=?",b);
	}
	public List configurableField(String tableName,boolean b)
	{
		return getHibernateTemplate().find("SELECT distinct(fieldName) from DataCatalog where tableName="+"'"+tableName+"'"+"AND configerable="+b);
	}

	public List getList() {
		return getHibernateTemplate().find("from DataCatalog where corpID='TSFT' order by tableName");
	}

	public List<DataCatalog> searchdataCatalog(String tableName, String fieldName, String auditable, String description, String usDomestic, String defineByToDoRule,String visible, String charge) {
		   
		//System.out.println("\n\n\n\n\n --- >>from DataCatalog where corpID='TSFT' and tableName like '" +tableName+"%' AND fieldName like '" +  replaceWord(fieldName, "'", "''") + "%' AND auditable like '"+auditable+"%' AND description like '"+description+"%' AND defineByToDoRule like '"+defineByToDoRule+"%' AND isdateField like '"+isdateField+"%' AND visible like '"+visible+"%' AND charge like '"+charge+"%' order by tableName");
		if((tableName==null)||(tableName.equals("")))		{			tableName="";		}
		if((fieldName==null)||(fieldName.equals("")))		{			fieldName="";		}
		if((auditable==null)||(auditable.equals("")))		{			auditable="";		}    
		if((description==null)||(description.equals("")))		{			description="";		}
		if((usDomestic==null)||(usDomestic.equals("")))		{			usDomestic="";		}
		if((defineByToDoRule==null)||(defineByToDoRule.equals("")))		{			defineByToDoRule="";		}
		if((visible==null)||(visible.equals("")))		{			visible="";		}
		if((charge==null)||(charge.equals("")))		{			charge="";		}		

		return getHibernateTemplate().find("from DataCatalog where corpID='TSFT' and tableName like '" +tableName+"%' AND fieldName like '" +  replaceWord(fieldName, "'", "''") + "%' AND auditable like '"+auditable+"%' AND description like '"+description+"%' AND usDomestic like '"+ usDomestic +"%'AND defineByToDoRule like '"+defineByToDoRule+"%' AND visible like '"+visible+"%' AND charge like '"+charge+"%' order by tableName");
		
		//return getHibernateTemplate().find("from DataCatalog where tableName like '" +tableName+"%' AND configerable like '"+configerable+"%' AND auditable like '"+auditable+"%' AND description like '"+description+"%' AND defineByToDoRule like '"+defineByToDoRule+"%' AND isdateField like '"+isdateField+"%' AND fieldName like '" +  replaceWord(fieldName, "'", "''") + "%' AND charge like '"+charge+"%'");
		/*if(auditable.equalsIgnoreCase("")){
			return getHibernateTemplate().find("from DataCatalog where tableName like '" +tableName+"%' AND fieldName like '" +  replaceWord(fieldName, "'", "''") + "%'");
		}else{
			return getHibernateTemplate().find("from DataCatalog where tableName like '" +tableName+"%' AND auditable = "+auditable+" AND fieldName like '" +  replaceWord(fieldName, "'", "''") + "%'");
		}*/
	}
	
	public List getCorpAuthoritiesForComponent() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean getFieldVisibilityAttrbute(String userCorpID, String componentId) {
		List searchResults = this.getSession().createSQLQuery("SELECT ifnull(c.visible,m.visible) " +
				"FROM datacatalog m left outer join datacatalog c " +
				"ON (m.corpid = 'TSFT' and c.corpid = '"+ userCorpID + "'  and m.tableName = c.tableName and m.fieldName = c.fieldName) " +
				"WHERE m.corpid = 'TSFT' and CONCAT(m.tableName, '.', m.fieldName) = '" + componentId+"'").list();
		if (searchResults.isEmpty() || ((String)searchResults.get(0)).equals("false")){
			return false;
		} else {
			return true;
		}
	}
	public List checkData(String tableName, String fieldName, String sessionCorpID) {
		try{
				return getHibernateTemplate().find("from DataCatalog where tableName='"+tableName+"' and fieldName='"+fieldName+"' and corpID='"+sessionCorpID+"'");
			}catch (Exception e) {
				logger.error("Error executing query"+ e,e);
			}
			return null;
		}
	public void mergeDataCatalog(DataCatalog dc){
		getHibernateTemplate().merge(dc);
	}

}
