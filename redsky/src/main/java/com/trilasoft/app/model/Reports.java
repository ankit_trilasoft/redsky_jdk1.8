package com.trilasoft.app.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.appfuse.model.LabelValue;
import org.appfuse.model.Role;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.CompareToBuilder;

@Entity
@Table(name = "reports")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class Reports extends BaseObject implements Comparable<Reports> {
	private Long id;

	private Long reportId;

	private String corpID;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;

	private String module;

	private String reportName;

	private String reportComment;
	private String emailBody;
	private String menu;

	private String description;

	private String enabled;

	private String docsxfer;

	private String formReportFlag;

	private String subModule;

	private File file;

	private String fileType;

	private String fileContentType;

	private String fileFileName;

	private String location;

	private String csv;

	private String xls;

	private String html;

	private String pdf;

	private String rtf;

	private String extract;

	private String secureForm;
	private String emailOut;

	private String runTimeParameterReq;
	private String companyDivision;
	private String job;
	private String billToCode ;
	private Long reportHits=new Long(0);
	private String mode;
	private String targetCabinet;
	private String formFieldName;
	private String formValue;
	private String formCondition;
	private String extractTheme;
	private Boolean autoupload;
	private String attachedURL;
	private String docx;
	private Boolean isMultiLingual;
	private Boolean excludeFromForm;
	private String routing;
	private int sequenceNo;
	private String agentRoles;
	


	protected Set<Role> roles = new HashSet<Role>();

	public int compareTo(final Reports other) {
		return new CompareToBuilder().append(id, other.id)
				.append(reportId, other.reportId).append(corpID, other.corpID)
				.append(createdBy, other.createdBy)
				.append(updatedBy, other.updatedBy)
				.append(createdOn, other.createdOn)
				.append(updatedOn, other.updatedOn)
				.append(module, other.module)
				.append(reportName, other.reportName)
				.append(reportComment, other.reportComment)
				.append(emailBody, other.emailBody).append(menu, other.menu)
				.append(description, other.description)
				.append(enabled, other.enabled)
				.append(docsxfer, other.docsxfer)
				.append(formReportFlag, other.formReportFlag)
				.append(subModule, other.subModule).append(file, other.file)
				.append(fileType, other.fileType)
				.append(fileContentType, other.fileContentType)
				.append(fileFileName, other.fileFileName)
				.append(location, other.location).append(csv, other.csv)
				.append(xls, other.xls).append(html, other.html)
				.append(pdf, other.pdf).append(rtf, other.rtf)
				.append(extract, other.extract)
				.append(secureForm, other.secureForm)
				.append(emailOut, other.emailOut)
				.append(runTimeParameterReq, other.runTimeParameterReq)
				.append(companyDivision, other.companyDivision)
				.append(job, other.job).append(billToCode, other.billToCode)
				.append(reportHits, other.reportHits).append(mode, other.mode)
				.append(targetCabinet, other.targetCabinet)
				.append(formFieldName, other.formFieldName)
				.append(formValue, other.formValue)
				.append(formCondition, other.formCondition)
				.append(extractTheme, other.extractTheme)
				.append(autoupload, other.autoupload)
				.append(attachedURL, other.attachedURL)
				.append(docx, other.docx)
				.append(isMultiLingual, other.isMultiLingual)
				.append(excludeFromForm, other.excludeFromForm)
				.append(routing, other.routing)
				.append(sequenceNo, other.sequenceNo)
				.append(agentRoles, other.agentRoles)
				.toComparison();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("reportId", reportId).append("corpID", corpID)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("module", module).append("reportName", reportName)
				.append("reportComment", reportComment)
				.append("emailBody", emailBody).append("menu", menu)
				.append("description", description).append("enabled", enabled)
				.append("docsxfer", docsxfer)
				.append("formReportFlag", formReportFlag)
				.append("subModule", subModule).append("file", file)
				.append("fileType", fileType)
				.append("fileContentType", fileContentType)
				.append("fileFileName", fileFileName)
				.append("location", location).append("csv", csv)
				.append("xls", xls).append("html", html).append("pdf", pdf)
				.append("rtf", rtf).append("extract", extract)
				.append("secureForm", secureForm).append("emailOut", emailOut)
				.append("runTimeParameterReq", runTimeParameterReq)
				.append("companyDivision", companyDivision).append("job", job)
				.append("billToCode", billToCode)
				.append("reportHits", reportHits).append("mode", mode)
				.append("targetCabinet", targetCabinet)
				.append("formFieldName", formFieldName)
				.append("formValue", formValue)
				.append("formCondition", formCondition)
				.append("extractTheme", extractTheme)
				.append("autoupload", autoupload)
				.append("attachedURL", attachedURL).append("docx", docx)
				.append("isMultiLingual", isMultiLingual)
				.append("excludeFromForm", excludeFromForm)
				.append("routing", routing)
				.append("sequenceNo", sequenceNo)
				.append("agentRoles", agentRoles)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Reports))
			return false;
		Reports castOther = (Reports) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(reportId, castOther.reportId)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(module, castOther.module)
				.append(reportName, castOther.reportName)
				.append(reportComment, castOther.reportComment)
				.append(emailBody, castOther.emailBody)
				.append(menu, castOther.menu)
				.append(description, castOther.description)
				.append(enabled, castOther.enabled)
				.append(docsxfer, castOther.docsxfer)
				.append(formReportFlag, castOther.formReportFlag)
				.append(subModule, castOther.subModule)
				.append(file, castOther.file)
				.append(fileType, castOther.fileType)
				.append(fileContentType, castOther.fileContentType)
				.append(fileFileName, castOther.fileFileName)
				.append(location, castOther.location)
				.append(csv, castOther.csv).append(xls, castOther.xls)
				.append(html, castOther.html).append(pdf, castOther.pdf)
				.append(rtf, castOther.rtf).append(extract, castOther.extract)
				.append(secureForm, castOther.secureForm)
				.append(emailOut, castOther.emailOut)
				.append(runTimeParameterReq, castOther.runTimeParameterReq)
				.append(companyDivision, castOther.companyDivision)
				.append(job, castOther.job)
				.append(billToCode, castOther.billToCode)
				.append(reportHits, castOther.reportHits)
				.append(mode, castOther.mode)
				.append(targetCabinet, castOther.targetCabinet)
				.append(formFieldName, castOther.formFieldName)
				.append(formValue, castOther.formValue)
				.append(formCondition, castOther.formCondition)
				.append(extractTheme, castOther.extractTheme)
				.append(autoupload, castOther.autoupload)
				.append(attachedURL, castOther.attachedURL)
				.append(docx, castOther.docx)
				.append(isMultiLingual, castOther.isMultiLingual)
				.append(excludeFromForm, castOther.excludeFromForm)
				.append(routing, castOther.routing)
				.append(sequenceNo, castOther.sequenceNo)
				.append(agentRoles, castOther.agentRoles)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(reportId).append(corpID)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(module).append(reportName)
				.append(reportComment).append(emailBody).append(menu)
				.append(description).append(enabled).append(docsxfer)
				.append(formReportFlag).append(subModule).append(file)
				.append(fileType).append(fileContentType).append(fileFileName)
				.append(location).append(csv).append(xls).append(html)
				.append(pdf).append(rtf).append(extract).append(secureForm)
				.append(emailOut).append(runTimeParameterReq)
				.append(companyDivision).append(job).append(billToCode)
				.append(reportHits).append(mode).append(targetCabinet)
				.append(formFieldName).append(formValue).append(formCondition)
				.append(extractTheme).append(autoupload).append(attachedURL)
				.append(docx).append(isMultiLingual).append(excludeFromForm)
				.append(routing).append(sequenceNo).append(agentRoles)
				.toHashCode();
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "reports_role", joinColumns = { @JoinColumn(name = "reports_id") }, inverseJoinColumns = @JoinColumn(name = "role_id"))
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Transient
	public List<LabelValue> getRoleList() {
		List<LabelValue> reportsRoles = new ArrayList<LabelValue>();

		if (this.roles != null) {
			for (Role role : roles) {
				reportsRoles.add(new LabelValue(role.getName(), role.getName()));
			}
		}

		return reportsRoles;
	}

	public void addRole(Role role) {
		getRoles().add(role);
	}

	@Column(length = 100)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 3)
	public String getDocsxfer() {
		return docsxfer;
	}

	public void setDocsxfer(String docsxfer) {
		this.docsxfer = docsxfer;
	}

	@Column(length = 10)
	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	@Column(length = 100)
	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	@Column(length = 100)
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	@Column(length = 255)
	public String getReportComment() {
		return reportComment;
	}

	public void setReportComment(String reportComment) {
		this.reportComment = reportComment;
	}

	@Column(length = 100)
	public String getReportName() {
		return reportName;
	}

	public void setReportName(Object object) {
		this.reportName = (String) object;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 3)
	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	@Column(length = 100)
	public String getSubModule() {
		return subModule;
	}

	public void setSubModule(String subModule) {
		this.subModule = subModule;
	}

	@Column
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	@Column(length = 45)
	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	@Column(length = 75)
	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	@Column(length = 25)
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@Column(length = 254)
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	@Column(length = 3)
	public String getFormReportFlag() {
		return formReportFlag;
	}

	public void setFormReportFlag(String formReportFlag) {
		this.formReportFlag = formReportFlag;
	}

	@Column(length = 5)
	public String getCsv() {
		return csv;
	}

	public void setCsv(String csv) {
		this.csv = csv;
	}

	@Column(length = 5)
	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	@Column(length = 5)
	public String getPdf() {
		return pdf;
	}

	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	@Column(length = 5)
	public String getRtf() {
		return rtf;
	}

	public void setRtf(String rtf) {
		this.rtf = rtf;
	}

	@Column(length = 5)
	public String getXls() {
		return xls;
	}

	public void setXls(String xls) {
		this.xls = xls;
	}

	public String getExtract() {
		return extract;
	}

	public void setExtract(String extract) {
		this.extract = extract;
	}
	
	@Column(length = 5)
	public String getDocx() {
		return docx;
	}

	public void setDocx(String docx) {
		this.docx = docx;
	}


	@Column(length = 3)
	public String getSecureForm() {
		return secureForm;
	}

	public void setSecureForm(String secureForm) {
		this.secureForm = secureForm;
	}

	public String getRunTimeParameterReq() {
		return runTimeParameterReq;
	}

	public void setRunTimeParameterReq(String runTimeParameterReq) {
		this.runTimeParameterReq = runTimeParameterReq;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	
	@Column(length = 1000)
	public String getBillToCode() {
	    return billToCode;
	}

	public void setBillToCode(String billToCode) {
	    this.billToCode = billToCode;
	}
	
	
	@Column(length = 20)
	public Long getReportHits() {
	    return reportHits;
	}

	public void setReportHits(Long reportHits) {
	    this.reportHits = reportHits;
	}
	
	@Column(length = 225)
	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column
	public String getTargetCabinet() {
		return targetCabinet;
	}
    
	public void setTargetCabinet(String targetCabinet) {
		this.targetCabinet = targetCabinet;
	}
	@Column
	public String getEmailBody() {
		return emailBody;
	}

	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	@Column(length = 3)
	public String getEmailOut() {
		return emailOut;
	}

	public void setEmailOut(String emailOut) {
		this.emailOut = emailOut;
	}

	public String getFormFieldName() {
		return formFieldName;
	}

	public void setFormFieldName(String formFieldName) {
		this.formFieldName = formFieldName;
	}

	public String getFormValue() {
		return formValue;
	}

	public void setFormValue(String formValue) {
		this.formValue = formValue;
	}

	public String getFormCondition() {
		return formCondition;
	}

	public void setFormCondition(String formCondition) {
		this.formCondition = formCondition;
	}
	
	@Column(length = 20)
	public String getExtractTheme() {
		return extractTheme;
	}

	public void setExtractTheme(String extractTheme) {
		this.extractTheme = extractTheme;
	}
	
	@Column
	public Boolean getAutoupload() {
		return autoupload;
	}

	public void setAutoupload(Boolean autoupload) {
		this.autoupload = autoupload;
	}	
	@Column
	public String getAttachedURL() {
		return attachedURL;
	}

	public void setAttachedURL(String attachedURL) {
		this.attachedURL = attachedURL;
	}
	
	@Column
	public Boolean getIsMultiLingual() {
		return isMultiLingual;
	}

	public void setIsMultiLingual(Boolean isMultiLingual) {
		this.isMultiLingual = isMultiLingual;
	}

	public Boolean getExcludeFromForm() {
		return excludeFromForm;
	}

	public void setExcludeFromForm(Boolean excludeFromForm) {
		this.excludeFromForm = excludeFromForm;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public int getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(int sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getAgentRoles() {
		return agentRoles;
	}

	public void setAgentRoles(String agentRoles) {
		this.agentRoles = agentRoles;
	}
	
}