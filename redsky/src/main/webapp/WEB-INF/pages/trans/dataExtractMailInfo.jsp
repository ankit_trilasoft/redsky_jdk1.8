<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Send Email Information</title>   
    <meta name="heading" content="Send Email Information"/> 
    <style type="text/css"> 
</style> 

</head> 
<c:if test="${dataExtractMailInfo!='[]'}"> 
<div style=" width:100%; overflow-x:auto;">
<div>
 </div>
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Send Email Information</span></a></li>
		  </ul>
		</div>
		<div class="spnblk" style="margin-bottom:-18px;">&nbsp;</div>
 
<s:form id="dataExtractMailInfo" action="" method="post">  
<s:set name="dataExtractMailInfo" value="dataExtractMailInfo" scope="request"/> 
<display:table name="dataExtractMailInfo" class="table" requestURI="" id="dataExtractMailInfo" style="width:750px" defaultsort="1" pagesize="100" >
 <display:column   title="Send Email" sortable="true" sortProperty="lastEmailSent"  property="lastEmailSent" ></display:column>		 
 <display:column   title="Email&nbsp;Status" sortable="true" sortProperty="emailStatus"  property="emailStatus" ></display:column>	
 <display:column   title="Email Message" sortable="true" sortProperty="emailMessage"  property="emailMessage" ></display:column>		    	    
 <display:column   title="Last Mail Date" sortable="true" sortProperty="emailDateTime" style="width:120px" >
    <fmt:formatDate value="${dataExtractMailInfo.emailDateTime}" pattern="${displayDateTimeFormat}"/> 
</display:column> 
</display:table>  
</s:form>
</c:if>
<c:if test="${dataExtractMailInfo=='[]'}">
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>No Mail Info</b></td> 
</tr>
</table>
</c:if>
<table>
<tr>
<td>
<input type="button" name="Submit"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();">
</td>
</tr>
</table>
	  	