<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="IntBillOverDue" pageWidth="849" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="789" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="968fd55c-da2d-4a1f-b071-aa0dceb8eec1">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[select
	"IntBillOverDue-R-STVF.jrxml",
	serviceorder.shipnumber AS serviceorder_shipnumber,
	serviceorder.companyDivision AS serviceorder_companyDivision,
	if((serviceorder.firstName is null || trim(serviceorder.firstName)=''),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))) AS 'Shipper',
	billing.billtocode AS billing_billtocode,
	serviceorder.status AS serviceorder_status,
	serviceorder.routing AS serviceorder_routing,
	date_format(trackingstatus.loadA,'%d-%b-%y') AS trackingstatus_loadA,
	date_format(trackingstatus.deliveryA,'%d-%b-%y')AS trackingstatus_deliveryA,
	lower(billing.personbilling) AS billing_personbilling,
	ifnull(notes.subject,'') As notes_subject,
	date_format(now(),'%d-%b-%y') AS date
FROM serviceorder serviceorder
	INNER JOIN trackingstatus trackingstatus on serviceorder.id = trackingstatus.id	AND trackingstatus.corpid = $P{Corporate ID}
	INNER JOIN billing billing on serviceorder.id = billing.id AND billing.corpid = $P{Corporate ID}
	LEFT OUTER JOIN notes notes ON serviceorder.id = notes.noteskeyId AND notes.noteSubType ='Accounting Issue' AND notes.notestatus <> 'CMP' and notetype = 'Service Order' AND notes.corpid = $P{Corporate ID}
WHERE  serviceorder.job = 'INT'
	AND billing.billComplete is null
	AND serviceorder.status not in ('CNCL','CLSD','DWNLD','OSIT')
	AND serviceorder.corpid = $P{Corporate ID}
	AND trackingstatus.loadA is not null
order by serviceorder.companyDivision,billing.billtocode, trackingstatus.loadA, trackingstatus.deliveryA]]>
	</queryString>
	<field name="IntBillOverDue-R-STVF.jrxml" class="java.lang.String"/>
	<field name="serviceorder_shipnumber" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="billing_billtocode" class="java.lang.String"/>
	<field name="serviceorder_status" class="java.lang.String"/>
	<field name="serviceorder_routing" class="java.lang.String"/>
	<field name="trackingstatus_loadA" class="java.lang.String"/>
	<field name="trackingstatus_deliveryA" class="java.lang.String"/>
	<field name="billing_personbilling" class="java.lang.String"/>
	<field name="notes_subject" class="java.lang.String"/>
	<field name="date" class="java.lang.String"/>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[$F{serviceorder_companyDivision}]]></groupExpression>
		<groupHeader>
			<band height="20" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="102" y="0" width="100" height="18" uuid="a1fcfde9-fde3-47c1-bd85-85def083c974"/>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-12" x="0" y="0" width="102" height="18" uuid="bca6916a-3489-4f5f-94be-27a2263c4456"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="31" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" mode="Transparent" x="0" y="0" width="789" height="30" uuid="b39c1aff-6b6b-49bf-8a18-6bc4e26df1f9"/>
				<box topPadding="5"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="14" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[International Billing Overdue Report]]></text>
			</staticText>
			<line>
				<reportElement key="line-3" x="0" y="30" width="789" height="1" forecolor="#999999" uuid="dc8bda25-fb82-4bae-9cdd-bde8963cda7a"/>
			</line>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="19" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" mode="Opaque" x="0" y="0" width="95" height="18" backcolor="#D2CCCC" uuid="cceaab01-94bc-4138-8ee1-5e6b68837293"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement verticalAlignment="Top">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[S/O #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" mode="Opaque" x="95" y="0" width="124" height="18" backcolor="#D2CCCC" uuid="255d816c-21fb-43c6-9d3a-d72d1a578b1f"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" mode="Opaque" x="219" y="0" width="81" height="18" backcolor="#D2CCCC" uuid="5e6ca11e-e17e-4e91-8d1a-c3bc6a298218"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill Code]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" mode="Opaque" x="300" y="0" width="50" height="18" backcolor="#D2CCCC" uuid="7594d233-de46-4e72-9e76-06f0a762955c"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" mode="Opaque" x="350" y="0" width="58" height="18" backcolor="#D2CCCC" uuid="c3eb0c91-433b-4a1e-b81b-55cda1325792"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Routing]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" mode="Opaque" x="408" y="0" width="65" height="18" backcolor="#D2CCCC" uuid="ca77f64e-9490-4994-9676-b024b76dac7d"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Loading]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" mode="Opaque" x="473" y="0" width="65" height="18" backcolor="#D2CCCC" uuid="78ed26e7-c055-41e7-bf19-4ebccb68ff52"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Delivery]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" mode="Opaque" x="538" y="0" width="85" height="18" backcolor="#D2CCCC" uuid="a202b457-7430-4ceb-b4c5-ed9e280c0f4c"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Billing Person]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" mode="Opaque" x="623" y="0" width="166" height="18" backcolor="#D2CCCC" uuid="a6e8fa44-c6d3-4127-bc6f-9b93d1347e62"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Note]]></text>
			</staticText>
			<line>
				<reportElement key="line-2" x="0" y="18" width="789" height="1" forecolor="#999999" backcolor="#999999" uuid="aa2aaeb0-ba87-47dc-a09e-e04ea0033461"/>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="0" y="0" width="95" height="18" backcolor="#E2E1E1" uuid="a861006b-c39d-473d-9800-e49135ce8b15"/>
				<box topPadding="5" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shipnumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="95" y="0" width="124" height="18" uuid="baeea640-6b2c-4b9d-93c5-dbbe8cd5201b"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="219" y="0" width="81" height="18" backcolor="#E2E1E1" uuid="78222b08-531d-4c7c-bc2e-931a4429376f"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_billtocode}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="300" y="0" width="50" height="18" uuid="9a0ed638-efb9-4c95-aef3-fda3065d3bcc"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_status}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="350" y="0" width="58" height="18" backcolor="#E2E1E1" uuid="c2452059-dcf1-4b6a-a84f-8362bce0ea37"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_routing}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="408" y="0" width="65" height="18" uuid="cd5052c6-ce5f-4dda-b681-5996edf32991"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_loadA}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="473" y="0" width="65" height="18" backcolor="#E2E1E1" uuid="e6c2c4f5-c72f-44e9-826c-1e0479075a9e"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_deliveryA}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="538" y="0" width="85" height="18" uuid="860733a2-461c-4c3d-9924-b67b30cb30a0"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_personbilling}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" mode="Opaque" x="623" y="0" width="166" height="18" backcolor="#E2E1E1" uuid="77a01430-b26b-4ecb-bc70-bf8cc99999ff"/>
				<box topPadding="2" leftPadding="3"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{notes_subject}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="16" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-1" x="647" y="2" width="85" height="14" forecolor="#666666" uuid="5a9da1ca-b3b9-48c4-9c4b-413cc09d4bc2"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" x="730" y="2" width="59" height="14" forecolor="#666666" uuid="c4f07c0c-2af5-48dc-88ba-071e6fe73479"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="9" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="CP1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-11" x="236" y="2" width="317" height="14" forecolor="#666666" uuid="9d465183-79e2-4218-8c25-33a83d19ec45"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="9"/>
				</textElement>
				<text><![CDATA[IntBillOverDue.jrxml]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-3" x="5" y="2" width="150" height="14" uuid="c8b638b0-958a-4687-91cc-9803e1e11721"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{date}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-5" x="0" y="1" width="788" height="1" uuid="d3f5c5ec-8bce-4d42-b158-4a0d9fcdb8cb"/>
				<graphicElement fill="Solid"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
