package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Query;

import com.trilasoft.app.dao.PrintDailyPackageDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.PrintDailyPackage;

public class PrintDailyPackageDaoHibernate extends GenericDaoHibernate<PrintDailyPackage, Long> implements PrintDailyPackageDao {
	
	private HibernateUtil hibernateUtil;
	
	public PrintDailyPackageDaoHibernate() {
		super(PrintDailyPackage.class);
	}

	public List<PrintDailyPackage> getByCorpId(String corpId,String tabId) {
		String condition = " and status is true ";
		if(tabId != null && "waste".equals(tabId)){
			condition = " and status is not true ";
		}
		List<PrintDailyPackage> list = getHibernateTemplate().find("from PrintDailyPackage where corpId='"+corpId+"' and (parentId is null or parentId='') "+condition+" order by printSeq");
		return list;
	}

	public List<PrintDailyPackage> findChildByParentId(long id) {
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().getNamedQuery("findByParentId");
		query.setLong("parentId", id);
		return query.list();
		// return getHibernateTemplate().find("from PrintDailyPackage where parentId="+id);
	}

	public void deleteByParentId(long id) {
		this.getSession().createSQLQuery("delete from printdailypackage where parentId="+id).executeUpdate();
		
	}
	
	public List findAutoCompleterList(String jrxmlName, String corpId){
		return this.getSession().createSQLQuery("select reportName from reports where reportName like'"+jrxmlName+"%' and corpID='"+corpId+"'").list();
		//return getHibernateTemplate().find("from Reports where reportName like'"+jrxmlName+"%' and corpID='"+corpId+"'");
	}

	public List<PrintDailyPackage> searchPrintPackage(String formName,String corpId, String serviceType, String serviceMode,String military) {
		String condition = "";
		if (serviceType != null && !"".equals(serviceType)) {
			condition = " and wtServiceType like '%"+serviceType+"%'";
		}
		String query = "from PrintDailyPackage where formName like '"+formName+"%' and corpId='"+corpId+"' and mode like '"+serviceMode+"%' and military like '"+military+"%'"+condition;
		return getHibernateTemplate().find(query);
	}
	public void deleteChild(String corpId){
		this.getSession().createSQLQuery("delete from printdailypackage where parentId is not null and parentId != '' and ((jrxmlName ='' or jrxmlName is null) or (parameters ='' or parameters is null)) and corpId='"+corpId+"'").executeUpdate();
	}
}
