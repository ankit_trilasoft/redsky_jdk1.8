package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table ( name="mymessage")
public class MyMessage {
	
	private Long id;
	private Long noteId;
	private String corpID;
	private String fromUser;
	private String toUser;
	private String subject;
	private Date sentOn;
	private String message;
	private String status;
	private Date readOn;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("noteId",
				noteId).append("corpID", corpID).append("fromUser", fromUser)
				.append("toUser", toUser).append("subject", subject).append(
						"sentOn", sentOn).append("message", message).append(
						"status", status).append("readOn", readOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MyMessage))
			return false;
		MyMessage castOther = (MyMessage) other;
		return new EqualsBuilder().append(id, castOther.id).append(noteId,
				castOther.noteId).append(corpID, castOther.corpID).append(
				fromUser, castOther.fromUser).append(toUser, castOther.toUser)
				.append(subject, castOther.subject).append(sentOn,
						castOther.sentOn).append(message, castOther.message)
				.append(status, castOther.status).append(readOn,
						castOther.readOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(noteId).append(corpID)
				.append(fromUser).append(toUser).append(subject).append(sentOn)
				.append(message).append(status).append(readOn).toHashCode();
	}
	@Column( length=15 )	
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length=50 )
	public String getFromUser() {
		return fromUser;
	}
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column( length=4000 )
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Column
	public Date getReadOn() {
		return readOn;
	}
	public void setReadOn(Date readOn) {
		this.readOn = readOn;
	}
	@Column
	public Date getSentOn() {
		return sentOn;
	}
	public void setSentOn(Date sentOn) {
		this.sentOn = sentOn;
	}
	@Column( length=250 )
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	@Column( length=50 )
	public String getToUser() {
		return toUser;
	}
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}
	@Column( length=15 )
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column( length=15 )
	public Long getNoteId() {
		return noteId;
	}
	public void setNoteId(Long noteId) {
		this.noteId = noteId;
	}

	
	
}
