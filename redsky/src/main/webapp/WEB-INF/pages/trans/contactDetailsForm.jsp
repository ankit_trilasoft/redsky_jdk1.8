<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>

	<script type="text/javascript">
	function getContactDetails11(){		
		var venderCode='${partnerCode}';
		var prifix='${prifix}';
		//alert(venderCode)
		//alert(prifix)
		var userName=document.getElementById("userName").value;
		//alert(userName)
		var userEmailId=document.getElementById("userEmailId").value;
		//alert(userEmailId)
		$.get("findContactSearchAjax.html?ajax=1&decorator=simple&popup=true", 
				{prifix:prifix,partnerCode:venderCode,userName:userName,userEmailId:userEmailId},
				function(data){
					//document.getElementById("ContactDetails").style.display = "block";
				    //document.getElementById("ContactDetails").className  = "class1";
				    //document.getElementById("overlay").style.display = "block";
					$("#searchData").html(data);			
			});
	}
	function fillContactValue(fullName,email){
		var prifix='${prifix}';
		var vendorContact="";
		var vendorEmail="";
		//alert(prifix)
		if(prifix=='SCH'){
			 vendorContact="SCH_contactPerson";		
			 vendorEmail="SCH_website";		
		}else{			
		 vendorContact=prifix+'_vendorContact';		
		 vendorEmail=prifix+'_vendorEmail';	
		}
		document.getElementById(vendorContact).value=fullName;
		document.getElementById(vendorEmail).value=email;
		document.getElementById("ContactDetails").style.display = "none"; 
		//document.getElementById("overlayDSP").style.display = "none";
	}
	function clear_fields(){
		document.getElementById('userName').value='';
		document.getElementById('userEmailId').value='';
	}

	 
	$(document).ready(function() 
		    { 		
	        
		  $('#userContactList').tablesorter();
	   	 
		    } 
		); 
  </script>
    <div id="Layer1" style="width:100%;">
    <div><img align="right" class="openpopup" onclick="closeMyDiv()" src="<c:url value='/images/closetooltip.gif'/>" /></div>
	<div id="otabs" style="margin-bottom:0px !important;">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>			   
			  </ul>
	</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
	<div id="liquid-round-top">
	   <div class="top" style="margin-top:10px;!margin-top:0px;"><span></span></div>
	   <div class="center-content">		
		<table class="table">
		<thead>
		<tr>
		<th>Name</th>
		<th>Email ID</th>
		<th>&nbsp;</th>
		</tr>
		</thead>	
		<tbody>
			<tr>	
				<td width="" align="left"><s:textfield name="userName" id="userName" size="30" required="true" cssClass="input-text" /></td>
				<td width="" align="left"><s:textfield name="userEmailId" id="userEmailId" size="30" required="true" cssClass="input-text" /></td>		
				<td>
				<input type="button" class="cssbutton1" value="search" style="width:55px; height:25px;!margin-bottom:10px;" onclick="getContactDetails11();"/>
	       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/>       
	   			</td>
				 </tr>			 
			</tbody>
		</table>
		</div>
		<div class="bottom-header"><span></span></div>
	</div>
</div> 
</div>
<div id="searchData" style="overflow-y:auto;">
<s:set name="userContactList" value="userContactList" scope="request"/>  
<display:table name="userContactList" class="table tablesorter" requestURI="" id="userContactList" defaultsort="1"  style="width:100%;" >   
	    <display:column sortable="true" title="Full Name" style="width:65px"> 
	    	<a href="javascript: void(0)" onclick= "fillContactValue('${userContactList.fullName}','${userContactList.email}');">
	    	 <c:out value="${userContactList.fullName}" /></a>
	    </display:column>
        <display:column property="jobFunction" sortable="true" title="Job Function" style="width:65px"/>  
        <display:column title="Relocation Contact" sortable="true" style="width:45px">      
 		<c:if test="${userContactList.relocationContact==false}">				
			<img src="${pageContext.request.contextPath}/images/cancel001.gif" />				 	
		</c:if>
		<c:if test="${userContactList.relocationContact==true}">				
			 <img src="${pageContext.request.contextPath}/images/tick01.gif" />		 	
		</c:if>
		</display:column>	
        <display:column property="userTitle" sortable="true" title="Job Title" style="width:65px"/>
	    <display:column sortable="true" title="Email Address" style="width:65px"> 
	    	<a href="javascript: void(0)" onclick= "sendEmail('${userContactList.email}');">
	    	 <c:out value="${userContactList.email}" /></a>
	    </display:column>
	    <display:column property="NPSscore" sortable="true" title="NPS score" style="width:65px"/> 
	    <display:column sortable="true" title="Phone&nbsp;Number" style="width:65px"> 
	    	  	 <c:out value="${userContactList.phoneNumber}" /></a>
	    </display:column>
</display:table>
</div>