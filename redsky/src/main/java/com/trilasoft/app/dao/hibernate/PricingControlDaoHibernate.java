package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;

import com.trilasoft.app.dao.PricingControlDao;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.model.PricingControl;
import com.trilasoft.app.model.PricingControlDetails;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PricingControlDetailsManager;

public class PricingControlDaoHibernate  extends GenericDaoHibernate<PricingControl, Long> implements PricingControlDao{

	private PricingControlDetailsManager pricingControlDetailsManager;
	private String sessionCorpID;
	
	private PartnerManager partnerManager;
	
	
	public PricingControlDaoHibernate() {
		super(PricingControl.class);
		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
       // User user = (User)auth.getPrincipal();
       // this.sessionCorpID = user.getCorpID();
	}

	public List checkById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List findPricingId(String code) {
		return getHibernateTemplate().find("from PricingControl where pricingID=?", code);
	}

	public List maxPricingIdCode(String sessionCorpID) {
		List pList= new ArrayList();
		List maxIdList=getHibernateTemplate().find("select max(id) from PricingControl where corpID='"+sessionCorpID+"'");
		if(maxIdList!=null ||  maxIdList.get(0)!=null || !maxIdList.isEmpty()){
			String id=maxIdList.get(0).toString();
			pList = getHibernateTemplate().find("select pricingID from PricingControl where id ="+id+"");
		}
		//Long l = Long.parseLong((getHibernateTemplate().find("select max(id) from PricingControl where corpID='"+sessionCorpID+"'")).get(0).toString());
		//List pList = getHibernateTemplate().find("select pricingID from PricingControl where id =?", l);
		return pList;
	}

	public List getAddress(Long serviceOrderId, String tariffApplicability) {
		List addressList= new ArrayList();
		if(tariffApplicability.equalsIgnoreCase("Origin"))
		{
			addressList= getHibernateTemplate().find("select concat(originAddressLine1,'@',originCity,'@',originState,'@',originZip,'@',originCountry,'@',originCountryCode) from ServiceOrder where id="+serviceOrderId+"");
		}
		if(tariffApplicability.equalsIgnoreCase("Destination"))
		{
			addressList= getHibernateTemplate().find("select concat(destinationAddressLine1,'@',destinationCity,'@',destinationState,'@',destinationZip,'@',destinationCountry,'@',destinationCountryCode) from ServiceOrder where id="+serviceOrderId+"");
		}
		return addressList;
	}

	public List getList(String shipNumber) {
		return getHibernateTemplate().find("from PricingControl where shipNumber='"+shipNumber+"'");
	}

	public List search(String pricingID, String shipNumber, String tariffApplicability, String packing, String country, String customerName, String parentAgent) {
		return getHibernateTemplate().find("from PricingControl where parentAgent='"+parentAgent+"' and pricingID like '%"+pricingID.replaceAll("'", "''")+"%' and sequenceNumber like '%"+shipNumber.replaceAll("'", "''")+"%'  and packing like '%"+packing.replaceAll("'", "''")+"%' and customerName like '%"+customerName.replaceAll("'", "''")+"%'");
	}

	public String locateAgents(BigDecimal weight, String latitude, String longitude, String tariffApplicability, String country, String packing, String poe, String fidi, String omni, Double minimumFeedbackRating, String faim, String iso9002, String iso27001, String iso1400, String rim, String dos, String gsa, String military, BigDecimal volume) {
		String queryTemp = "";
		if(fidi != null && fidi.equalsIgnoreCase("true")){
			queryTemp = "AND p.fidiNumber <> '' ";
		}
		if(omni != null && omni.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND p.ominiNumber <> '' ");
		}
		if(faim != null && faim.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND p.qualityCertifications like  '%FAIM%' ");
		}
		if(iso9002 != null && iso9002.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND p.qualityCertifications like  '%ISO 9002%' ");
		}
		if(iso27001 != null && iso27001.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND p.qualityCertifications like  '%ISO 27001%' ");
		}
		if(iso1400 != null && iso1400.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND p.qualityCertifications like  '%ISO 1400%' ");
		}
		if(rim != null && rim.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND p.qualityCertifications like  '%RIM%' ");
		}
		if(dos != null && dos.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND p.serviceLines like  '%DOS%' ");
		}
		if(gsa != null && gsa.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND p.serviceLines like  '%GSA%' ");
		}
		if(military != null && military.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND p.serviceLines like  '%Military%' ");
		}
		String sql = "select p.latitude, p.longitude, p.id, p.lastName, " + //0,1,2,3
		"concat(p.billingAddress1) as address1, " + //4
		"concat(p.billingCity, ', ', p.billingState, ' ', " +
		"p.billingZip) as address2, " +	//5	
		"p.billingPhone, 'www.sscw.com' as partnerUrl, p.location1," + //6,7,8
		"convert(group_concat(concat_ws(',', prgd.gridSection, prgd.label, prgd.basis, prgd.value) separator '!'), char) as chargelist," + //9
		"prg.serviceRangeMiles,IFNULL(prg.serviceRangeMiles2,'0.00'),IFNULL(prg.serviceRangeMiles3,'0.00'), " +//10,11,12
		"IFNULL(prg.serviceRangeMiles4,'0.00'),IFNULL(prg.serviceRangeMiles5,'0.00'),IFNULL(prg.serviceRangeExtra2,'0.00'), IFNULL(prg.serviceRangeExtra3,'0.00'), " +//13,14,15,16
		"IFNULL(prg.serviceRangeExtra4,'0.00'), IFNULL(prg.serviceRangeExtra5,'0.00') " + //17,18 TODO: Handle additional columns cprg.serviceRangeMiles2, prg.serviceRangeMiles3.." //10
		"  from partner p, partnerrategrid prg, partnerrategriddetails prgd where " +
		"p.partnerCode = prg.partnerCode and prg.id = prgd.partnerRateGridId and " +
		"p.latitude is not null and p.longitude is not null and " +
		"p.isAgent=true and p.billingCountryCode like '"+country+"%' and prg.tariffApplicability = '"+tariffApplicability+"'" +
		"and ((prgd.gridSection = 'WEIGHT' and prgd.grid = '"+packing+"' and "+weight+" >= prgd.weight1 and "+weight+" <=  prgd.weight2) or " +
		"(prgd.gridSection = 'CHARGES'  and prgd.grid = '"+packing+"' ) or " +
		"(prgd.gridSection = 'HAULING' and prgd.grid = '"+packing+"' and prgd.label = '"+poe+"' ) )" +
		"and prg.effectivedate  = (select max(effectivedate) from partnerrategrid prg2 Where prg2.effectivedate <= sysdate()  " + //TODO: replace by loading date
		"and prg2.status = 'Submitted')"+
		"and convert(prgd.value, DECIMAL) > 0 " + queryTemp +
		"group by prg.partnercode";
		List allPotentialPartners= this.getSession().createSQLQuery(sql).list();

		StringBuilder partners = new StringBuilder();
		int count = 0;
		Iterator it=allPotentialPartners.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
						
			double distance = getDistance(Double.parseDouble(latitude), Double.parseDouble(longitude), ((BigDecimal) obj[0]).doubleValue(), ((BigDecimal) obj[1]).doubleValue());
			String[] charges = ((String) obj[9]).split("!");
			double[] extraMileageLimits = {Double.parseDouble(obj[11].toString()), Double.parseDouble(obj[12].toString()),Double.parseDouble(obj[13].toString()),Double.parseDouble(obj[14].toString())}; //TODO: hardcoded -- read these values from the table
			double[] extraMileageCharges = {Double.parseDouble(obj[15].toString()), Double.parseDouble(obj[16].toString()),Double.parseDouble(obj[17].toString()),Double.parseDouble(obj[18].toString())}; //TODO: hardcoded -- read these values from the table
			
			double serviceMileage = ((BigDecimal) obj[10]).doubleValue();
			if (distance <= (extraMileageLimits != null?extraMileageLimits[extraMileageLimits.length-1]:serviceMileage)) {
				String chargeBreakup = "";
				count++;
				String quote = calculateQuote(Double.parseDouble(weight.toString()), charges, serviceMileage, extraMileageLimits, extraMileageCharges, distance,chargeBreakup, volume);
				if (count > 1) partners.append(","); else partners.append("");
				partners.append("{\"id\":" + obj[2] + ", \"name\": \"" + obj[3] + "\", \"address1\": \"" + obj[4] + "\", \"address2\": \"" + obj[5] + "\", \"phone\": \"" + 
				obj[6] + "\", \"url\": \"" + obj[7] + "\", \"locationPhotoUrl\": \"" + obj[8] + "\", \"latitude\": " + obj[0] + 
				", \"longitude\": " + obj[1] + ", \"rate\": " + quote + "}");
			}
		}
		String prependStr = "{ \"count\":" + count + ", \"partners\":[";
		String appendStr = "]}";
		String s=prependStr + partners.toString() + appendStr;
		return prependStr + partners.toString() + appendStr;
	
	}

	private double getDistance(double originLatitude, double originLongitude,
			double destinationLatitude, double destinationLongitude) {
		double distance = distance(originLatitude,
				originLongitude, destinationLatitude, destinationLongitude, "M");
		return distance;
	}	

	private boolean isPartnerNear(double originLatitude, double originLongitude,
			double destinationLatitude, double destinationLongitude, int proximity) {
		double distance = distance(originLatitude,
				originLongitude, destinationLatitude, destinationLongitude, "M");
		if (distance <= proximity) return true;
		else return false;
	}   
	
	private static double distance(double lat1, double lon1, double lat2,
			double lon2, String unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == "K") {
			dist = dist * 1.609344;
		} else if (unit == "N") {
			dist = dist * 0.8684;
		}
		return (dist);
	}
	
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts decimal degrees to radians : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts radians to decimal degrees : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}
	
	private String calculateQuote(double weight, String[] charges, double serviceMileage, double[] extraMileageLimits, double[] extraMileageCharges,   
			double distance, String chargeBreakup, BigDecimal volume) {
		double quote = 0;
		double baseQuote = 0;
		double flatQuote = 0;
		double extraMileageCharge = 0;
		double otherCharges = 0;
		double haulingCharge = 0;
		
		weight = applyMinimumWeight(weight, charges);
		
		for (String charge:charges){
			String[] chargeDetail = charge.split(",");
			
			if(chargeDetail[0].equals("WEIGHT") && chargeDetail[1].equals("Flat Rate")){
				flatQuote = Double.parseDouble(chargeDetail[3]);
			}  else if(chargeDetail[0].equals("WEIGHT") && weight >=  Double.parseDouble(chargeDetail[4]) && weight <=  Double.parseDouble(chargeDetail[5])){
				baseQuote =  Double.parseDouble(chargeDetail[3])*(weight/100.00);				  
			} else if(chargeDetail[0].equals("CHARGES")){
				//otherCharges += computeOtherCharges(weight, chargeDetail[2], Double.parseDouble(chargeDetail[3]), volume);
				chargeBreakup += chargeDetail[1]+ ":" + chargeDetail[3] +" "+chargeDetail[2] +" ,";
			}else if(chargeDetail[0].equals("HAULING")){
				//haulingCharge += computeOtherCharges(weight, chargeDetail[2], Double.parseDouble(chargeDetail[3]), volume);  
			} 
		}
		
		extraMileageCharge = computeWeightCharge(weight, distance, serviceMileage,
				extraMileageLimits, extraMileageCharges);
		
		quote += flatQuote>0?flatQuote:baseQuote;
		quote += extraMileageCharge;
		quote += otherCharges;
		quote += haulingCharge;
		
		chargeBreakup = "Weight:"+ ((flatQuote>0?flatQuote:baseQuote) + extraMileageCharge) + "," + chargeBreakup;
		chargeBreakup += "Hauling:" + haulingCharge; 
		return String.valueOf(quote).concat("#").concat(chargeBreakup);
	}
	
	private double applyMinimumWeight(double weight, String[] charges) {
		double appliedWeight = weight;
		for (String charge:charges){
			String[] chargeDetail = charge.split(",");
			
			if(chargeDetail[0].equals("WEIGHT") && chargeDetail[1].equals("Minimum Wts")){
				if (weight < Double.parseDouble(chargeDetail[3])) 
					appliedWeight = Double.parseDouble(chargeDetail[3]); 
			}
		}
		return appliedWeight;

	}

	private double computeOtherCharges(double weight, String basis, double charge, BigDecimal volume, Double exchangeRate) {
		double computedCharge = 0;
		if (basis.trim().equalsIgnoreCase("Flat") || basis.trim().equalsIgnoreCase("Each") ){
			computedCharge = charge; 
		}
		if (basis.trim().equalsIgnoreCase("Per CWT")){
			computedCharge = charge * (weight/100.00); 
		}
		if (basis.trim().equalsIgnoreCase("Per KG")){
			computedCharge = charge * (weight*0.45359237); 
		}
		if (basis.trim().equalsIgnoreCase("Included")){
			computedCharge = 0; 
		}
		if (basis.trim().equalsIgnoreCase("Per CFT")){
			if(volume==null || volume.toString().equals(""))
			{
				volume=new BigDecimal("0.00");
			}
			computedCharge = charge * Double.parseDouble(volume.toString()); 
		}
		if (basis.trim().equalsIgnoreCase("Per CBM")){
			if(volume==null || volume.toString().equals(""))
			{
				volume=new BigDecimal("0.00");
			}
			computedCharge = charge * Double.parseDouble(volume.toString()) * 0.028316; 
		}
		return computedCharge;
	}

	private double computeWeightCharge(double weight, double distance,
			double serviceMileage, double[] extraMileageLimits, double[] extraMileageCharges) {
		double weightCharge = 0;
		if (extraMileageLimits != null && distance > serviceMileage ) { 
			for (int i=0; i < extraMileageLimits.length;i++){
				if (distance <= extraMileageLimits[i]){
					weightCharge += extraMileageCharges[i]*(weight/100.00);
					break;
				}
			}
		}
		return weightCharge;
	}

	public List searchAgents(BigDecimal weight, String destinationLatitude, String destinationLongitude, String tarrifApplicability, String destinationCountry, String packing, String destinationPOE, String sessionCorpID, Long pricingControlID, Date createdOn, String createdBy, String updatedBy , Date updatedon, Date expectedLoadDate, BigDecimal volume, String baseCurrency, String contract, boolean applyCompletemarkUp, String prefferedPortCode) {
		List allPotentialPartners= new ArrayList();
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedExpectedLoadDate = new StringBuilder(dateformatYYYYMMDD1.format(expectedLoadDate));
		if(useStandardHaulingRates(destinationCountry) && useStandardOtherCharges(destinationCountry))
		{
			locateAgentsWithStandardCharges(weight, destinationLatitude, destinationLongitude,  tarrifApplicability,  destinationCountry,  packing,  destinationPOE,  sessionCorpID,  pricingControlID,  createdOn,  createdBy,  updatedBy ,  updatedon,  expectedLoadDate,  volume, baseCurrency, contract,applyCompletemarkUp,prefferedPortCode);
		}else{
			destinationPOE=destinationPOE.replaceAll(",", "','");	
		String sql = "select p.latitude, p.longitude, p.id, p.lastName, " + //0,1,2,3
		"concat(p.billingAddress1) as address1, " + //4
		"concat(p.billingCity, ', ', p.billingState, ' ', " +
		"p.billingZip) as address2, " +	//5	
		"p.billingPhone, 'www.sscw.com' as partnerUrl, p.location1," + //6,7,8
		"convert(group_concat(concat_ws(',', prgd.gridSection, prgd.label, prgd.basis, prgd.value, prgd.weight1, prgd.weight2) separator '!'), char) as chargelist," + //9
		"prg.serviceRangeMiles,IFNULL(prg.serviceRangeMiles2,'0.00'),IFNULL(prg.serviceRangeMiles3,'0.00'), " +//10,11,12
		"IFNULL(prg.serviceRangeMiles4,'0.00'),IFNULL(prg.serviceRangeMiles5,'0.00'),IFNULL(prg.serviceRangeExtra2,'0.00'), IFNULL(prg.serviceRangeExtra3,'0.00'), " +//13,14,15,16
		"IFNULL(prg.serviceRangeExtra4,'0.00'), IFNULL(prg.serviceRangeExtra5,'0.00'), p.partnerCode , p.fidiNumber, " +//17,18,19,20 TODO: Handle additional columns cprg.serviceRangeMiles2, prg.serviceRangeMiles3.." //10
		"p.OMNINumber , p.qualityCertifications, p.serviceLines, prg.currency ,prg.id as rateGridId " + //21,22,23,24,25
		"  from partner p, partnerrategrid prg, partnerrategriddetails prgd where " +
		"p.partnerCode = prg.partnerCode and prg.id = prgd.partnerRateGridId and " +
		"p.latitude is not null and p.longitude is not null and " +
		"p.isAgent=true and p.billingCountryCode like '"+destinationCountry+"%' and prg.tariffApplicability = '"+tarrifApplicability+"'" +
		"and ((prgd.gridSection = 'WEIGHT' and prgd.grid = '"+packing+"' ) or " +
		"(prgd.gridSection = 'CHARGES'  and prgd.grid = '"+packing+"' ) or " +
		"(prgd.gridSection = 'HAULING' and prgd.grid = '"+packing+"' and prgd.label in ('"+destinationPOE+"')) )" +
		"and prg.effectivedate  = (select max(effectivedate) from partnerrategrid prg2 Where prg2.effectivedate <= '"+formatedExpectedLoadDate+"'  " + //TODO: replace by loading date
		"and prg2.status = 'Submitted' and prg.id=prg2.id and prg2.publishTariff is true) "+
		"and ((if(prgd.basis in ('Included') and prgd.gridSection='CHARGES',convert(prgd.value, DECIMAL) >= 0.00,convert(prgd.value, DECIMAL) > 0.00)) or prgd.gridSection='HAULING') " +
		"and prgd.label not like 'Enter Charge%'" +
		"group by prg.partnercode";
		// and prg.id=prg2.id and prg2.publishTariff is true
		//System.out.println("\n\n\n\n\n\n sql query------>"+sql);
		allPotentialPartners= this.getSession().createSQLQuery(sql).list();
		allPotentialPartners=splitForHauling(allPotentialPartners);
		StringBuilder partners = new StringBuilder();
		int count = 0;
		
		Iterator it=allPotentialPartners.iterator();
		try{
		while(it.hasNext())
		{		 
			
			Object []obj=(Object[]) it.next();
			// if(obj[9].toString().contains(destinationPOE)){			
			String portsCode="";
			double distanceFromPort=0.0;
			double distance = getDistance(Double.parseDouble(destinationLatitude), Double.parseDouble(destinationLongitude), ((BigDecimal) obj[0]).doubleValue(), ((BigDecimal) obj[1]).doubleValue());
			String[] charges = ((String) obj[9]).split("!");
			
			String chargesName="";
			if(charges!=null){
				for (String charge:charges){
					if(charge.contains("THC")){
						
					}else{
						chargesName+="!"+charge;
					}
				}
			}
			String[] chargesWithoutTHC= chargesName.split("!");
			if(charges!=null){
				for (String charge:charges){
					String[] chargeDetail = charge.split(",");
					
					if(chargeDetail[0].equals("HAULING")){
						portsCode=chargeDetail[1];
						double[] portCoords = getPortCoords(portsCode, destinationCountry);
						distanceFromPort = getDistance(portCoords[0], portCoords[1], ((BigDecimal) obj[0]).doubleValue(), ((BigDecimal) obj[1]).doubleValue());
					}
				}
			}
			if(!portsCode.equalsIgnoreCase("")){
				
			
			if(obj[9].toString().contains(portsCode)){
			double[] extraMileageLimits = {Double.parseDouble(obj[11].toString()), Double.parseDouble(obj[12].toString()),Double.parseDouble(obj[13].toString()),Double.parseDouble(obj[14].toString())}; 
			double[] extraMileageCharges = {Double.parseDouble(obj[15].toString()), Double.parseDouble(obj[16].toString()),Double.parseDouble(obj[17].toString()),Double.parseDouble(obj[18].toString())}; 
			
			double serviceMileage = ((BigDecimal) obj[10]).doubleValue();
			
			double maxServiceMileage = serviceMileage;
            double exchangeRate=1.0;
            for (double extraMileageLimit:extraMileageLimits){
                if (extraMileageLimit > 0) maxServiceMileage = extraMileageLimit; 
            }
            
            if (distance <= maxServiceMileage) {
            	String chargeBreakup = "";
            	
            	if(!baseCurrency.equalsIgnoreCase(obj[24].toString()))
				{
            		List exchangeRateList=getHibernateTemplate().find("select currencyBaseRate from ExchangeRate where baseCurrency='"+baseCurrency+"' and currency='"+obj[24].toString()+"'");
            		if(!exchangeRateList.isEmpty())
            		{
            			exchangeRate=Double.parseDouble(exchangeRateList.get(0).toString());
            		}
            		
				}
            	
            	
            	double weightQuote = calculateWeightCharge(Double.parseDouble(weight.toString()), charges, serviceMileage, extraMileageLimits, extraMileageCharges, distance, chargeBreakup, volume, exchangeRate);
            	
            	String chargeQuote= calculateOtherCharges(weight, charges, serviceMileage, extraMileageLimits, extraMileageCharges, distance, chargeBreakup, volume,exchangeRate);
            	String chargeValue=chargeQuote.substring(0, chargeQuote.indexOf("#"));
            	String chargeBV=chargeQuote.substring(chargeQuote.indexOf("#")+1, chargeQuote.length());
            	double cv=Double.parseDouble(chargeValue);
            	
            	String chargeQuoteWithoutTHC= calculateOtherCharges(weight, chargesWithoutTHC, serviceMileage, extraMileageLimits, extraMileageCharges, distance, chargeBreakup,volume,exchangeRate);
				String chargeValueWithoutTHC=chargeQuoteWithoutTHC.substring(0, chargeQuoteWithoutTHC.indexOf("#"));
				String chargeBVWithoutTHC=chargeQuoteWithoutTHC.substring(chargeQuoteWithoutTHC.indexOf("#")+1, chargeQuoteWithoutTHC.length());
				double cvWithoutTHC=Double.parseDouble(chargeValueWithoutTHC);
				
            	
            	String haulingQuoteString= calculateHaulingCharge(weight, charges, serviceMileage, extraMileageLimits, extraMileageCharges, distance, chargeBreakup, volume,exchangeRate);
				String hauQuote=haulingQuoteString.substring(0, haulingQuoteString.indexOf("#"));
				String hauQuoteBasis=haulingQuoteString.substring(haulingQuoteString.indexOf("#")+1, haulingQuoteString.length());
            	double haulingQuote=Double.parseDouble(hauQuote);
            	
            	double quote=Math.round((weightQuote+cv+haulingQuote)*100)/100.00;
            	double quoteWithoutTHC=Math.round((weightQuote+cvWithoutTHC+haulingQuote)*100)/100.00;
            	
				String chargeBreakUp="Weight: "+weightQuote+","+chargeBV+",Hauling:"+haulingQuote+"";
				String chargeBreakUpWithoutTHC="Weight: "+weightQuote+","+chargeBVWithoutTHC+",Hauling:"+haulingQuote+"";
				
				String forwardingMarkUp=computeForwardingMarkup(weightQuote, chargeBV, haulingQuote, quote,exchangeRate, contract, applyCompletemarkUp,hauQuoteBasis );
            	String quoteMarkUp=forwardingMarkUp.substring(0, forwardingMarkUp.indexOf("#"));
            	Double quoteMarkUpRounded=(Math.round(Double.parseDouble(quoteMarkUp)*100)/100.00);
               	String chargewithMarkUp=forwardingMarkUp.substring(forwardingMarkUp.indexOf("#")+1,forwardingMarkUp.length());
            	
               	String forwardingMarkUpWithoutTHC=computeForwardingMarkup(weightQuote, chargeBVWithoutTHC, haulingQuote, quoteWithoutTHC,exchangeRate, contract, applyCompletemarkUp,hauQuoteBasis);
            	String quoteMarkUpWithoutTHC=forwardingMarkUpWithoutTHC.substring(0, forwardingMarkUpWithoutTHC.indexOf("#"));
            	Double quoteMarkUpRoundedWithoutTHC=(Math.round(Double.parseDouble(quoteMarkUpWithoutTHC)*100)/100.00);
            	String chargewithMarkUpWithoutTHC=forwardingMarkUpWithoutTHC.substring(forwardingMarkUpWithoutTHC.indexOf("#")+1,forwardingMarkUpWithoutTHC.length());
               	
               	Double discountApplied=computeDiscount(sessionCorpID, weightQuote, chargeBV, haulingQuote ,exchangeRate, contract,obj[25].toString());
               	Double discountAppliedWithoutTHC=computeDiscount(sessionCorpID, weightQuote, chargeBVWithoutTHC, haulingQuote ,exchangeRate, contract,obj[25].toString());
               	
				PricingControlDetails pricingControlDetails= new PricingControlDetails();
				pricingControlDetails.setPartnerID(obj[2].toString());
				pricingControlDetails.setPartnerName(obj[3].toString());
				pricingControlDetails.setAddress1(obj[4].toString());
				pricingControlDetails.setAddress2(obj[5].toString());
				pricingControlDetails.setUrl(obj[7].toString());
				if(obj[8]==null){pricingControlDetails.setLocationPhotoUrl("");}else{
					pricingControlDetails.setLocationPhotoUrl(obj[8].toString());
				}
				try{
					if(quoteBasis.equalsIgnoreCase("Flat")){
						pricingControlDetails.setBasis(quoteBasis);
						pricingControlDetails.setBasisWithoutMarkUp(quoteBasis);
					}else{
						pricingControlDetails.setBasisWithoutMarkUp(quoteBasis);
						pricingControlDetails.setBasis(String.valueOf(Math.round(Double.parseDouble(quoteBasis)*(1+quoteMarkupValue/100.00)*100)/100.00));
						
					}
				}catch(Exception ex){};
				pricingControlDetails.setLatitude(obj[0].toString());
				pricingControlDetails.setLongitude(obj[1].toString());
				pricingControlDetails.setRate(String.valueOf(quote));
				pricingControlDetails.setQuoteWithoutTHC(String.valueOf(quoteWithoutTHC));
				pricingControlDetails.setCorpID(sessionCorpID);
				pricingControlDetails.setPricingControlID(pricingControlID.toString());
				pricingControlDetails.setCreatedBy(createdBy);
				pricingControlDetails.setUpdatedBy(updatedBy);
				pricingControlDetails.setCreatedOn(new Date());
				pricingControlDetails.setUpdatedOn(new Date());
				pricingControlDetails.setTarrifApplicability(tarrifApplicability);
				pricingControlDetails.setChargeDetail(chargeBreakUp);
				pricingControlDetails.setChargeDetailWithoutTHC(chargeBreakUpWithoutTHC);
				pricingControlDetails.setRateMarkUp(quoteMarkUpRounded.toString());
				pricingControlDetails.setQuoteMarkupWithoutTHC(quoteMarkUpRoundedWithoutTHC.toString());
				pricingControlDetails.setChargeDetailMarkUp(chargewithMarkUp);
				pricingControlDetails.setChargeDetailMarkUpWithoutTHC(chargewithMarkUpWithoutTHC);
				pricingControlDetails.setBaseCurrency(baseCurrency);
				pricingControlDetails.setExchangeRate(exchangeRate);
				pricingControlDetails.setDthcFlag(false);
				pricingControlDetails.setHaulingBasis(hauQuoteBasis);
				pricingControlDetails.setAddressDistance(String.valueOf(Math.round(distance*100)/100.00));
				pricingControlDetails.setHaulingDistance(String.valueOf(Math.round(distanceFromPort*100)/100.00));
				if(tarrifApplicability.equalsIgnoreCase("Origin")){
					pricingControlDetails.setOriginPortCode(portsCode);
				}
				if(tarrifApplicability.equalsIgnoreCase("Destination")){
					pricingControlDetails.setDestinationPortCode(portsCode);
				}
				/*if(portsCode.equalsIgnoreCase(prefferedPortCode)){
					pricingControlDetails.setPreferredPort(true);
				}else{
					pricingControlDetails.setPreferredPort(false);
				}*/
				pricingControlDetails.setDestinationSelection(false);
				if(obj[20]==null){pricingControlDetails.setFidiNumber("");}else{pricingControlDetails.setFidiNumber(obj[20].toString());}
				if(obj[21]==null){pricingControlDetails.setOMNINumber("");}else{pricingControlDetails.setOMNINumber(obj[21].toString());}
				if(obj[22]==null){pricingControlDetails.setQualityCertifications("");}else{pricingControlDetails.setQualityCertifications(obj[22].toString());}
				if(obj[23]==null){pricingControlDetails.setServiceLines("");}else{pricingControlDetails.setServiceLines(obj[23].toString());}
				if(obj[24]==null){pricingControlDetails.setCurrency("");}else{pricingControlDetails.setCurrency(obj[24].toString());}
				if(obj[25]==null){pricingControlDetails.setRateGridId(null);}else{pricingControlDetails.setRateGridId(Long.parseLong(obj[25].toString()));}
				pricingControlDetails.setDiscountValue(discountApplied);
				pricingControlDetails.setDiscountValueWithoutTHC(discountAppliedWithoutTHC);				
				try{
					String rating = partnerRatingList(obj[19].toString(), sessionCorpID);
					pricingControlDetails.setFeedbackRating(rating);
				
				
					String activity = partnerActivityList(obj[19].toString(), sessionCorpID);
					pricingControlDetails.setShipmentActivity(activity);
				}catch(Exception ex)
				{
					System.out.println("\n\n\n\n\n rating exception---->"+ex);
					ex.printStackTrace();
				}
            
				pricingControlDetailsManager.save(pricingControlDetails);
				/*if (count > 1) partners.append(","); else partners.append("");
				partners.append("{\"id\":" + obj[2] + ", \"name\": \"" + obj[3] + "\", \"address1\": \"" + obj[4] + "\", \"address2\": \"" + obj[5] + "\", \"phone\": \"" + 
				obj[6] + "\", \"url\": \"" + obj[7] + "\", \"locationPhotoUrl\": \"" + obj[8] + "\", \"latitude\": " + obj[0] + 
				", \"longitude\": " + obj[1] + ", \"rate\": " + quote + "}");*/
			}
			//}
		}
			}
		}
		}catch(Exception ex)
		{
			System.out.println("\n\n\n\n\n whole exception---->"+ex);
			ex.printStackTrace();
		}
		}
		return allPotentialPartners;
	}

	private List splitForHauling(List allPotentialPartners) {
		List newlist = new ArrayList();
		
		Iterator it=allPotentialPartners.iterator();
		while(it.hasNext()){
			List haulingCharges = new ArrayList();
			Object []obj=(Object[]) it.next();
			String initialCharges = obj[9].toString();
			if(initialCharges.contains("HAULING")){
				  String[] chargeList = initialCharges.split("!");
				  String nonHaulingCharges = "";
				  for (String charges: chargeList){
					  String[] chargeDetails = charges.split(",");
					  
					  if ( !chargeDetails[0].equals("HAULING") ) {
						  if (nonHaulingCharges.equals("")) nonHaulingCharges = charges;
						  else nonHaulingCharges += "!" + charges;
					  	}else{
					  		haulingCharges.add(charges);
					  	}
				  }

				/*Iterator it1=haulingCharges.iterator();
				while(it1.hasNext()){
					String c=it1.next().toString();
					 String newCharges = nonHaulingCharges + "!" + c;
					 obj[9]=newCharges;
					 newlist.add(obj.clone());
				}*/
				for(Object hauling:haulingCharges){
					String c=hauling.toString();
					String newCharges = nonHaulingCharges + "!" + c;
					obj[9]=newCharges;
					newlist.add(obj.clone());
				}
			}else{
				newlist.add(obj);
			}
		}
		
		
		return newlist;
	}

	private String computeForwardingMarkup(double weightQuote, String chargeQuote, double haulingQuote, double quote, double exchangeRate, String contract, boolean applyCompletemarkUp, String hauQuoteBasis) {
		// TODO Auto-generated method stub
		Double markUpValue=0.0;
		String resultantMarkup="";
		String chargeMarkUp = "";
		String sql="select concat(profitFlat,'#', profitPercent)  from forwardingmarkupcontrol where "+quote+">=lowCost and "+quote+"<=highCost and contract='"+contract+"'";
		//System.out.println("\n\n\n\n\n\n sql query------>"+sql);
		List markUpList= this.getSession().createSQLQuery(sql).list();
		exchangeRate=1.0;
		String s1=markUpList.get(0).toString();
		Double profitFlat=Double.parseDouble(s1.substring(0, s1.indexOf("#")));
		Double profitPercent=Double.parseDouble(s1.substring(s1.indexOf("#")+1,s1.length()));
		quoteMarkupValue=profitPercent;
		
		markUpValue +=(profitPercent*weightQuote/100.00)*exchangeRate;
		
		//Double weightMarkUp=weightQuote*(1+profitPercent/100.00);//;
		chargeQuote=chargeQuote.trim();
	    if(chargeQuote.trim().length()>0){
	    
		if(chargeQuote.indexOf(",")==0)
		 {
	    	chargeQuote=chargeQuote.substring(1);
		 }
		if(chargeQuote.lastIndexOf(",")==chargeQuote.length()-1)
		 {
			chargeQuote=chargeQuote.substring(0, chargeQuote.length()-1);
		 }
		String[] charges = chargeQuote.split(",");
		if(charges.length >= 1){
			for (String charge:charges){
			String[] chargeDetail = charge.split(":");
			String chargeQuoteTemp=chargeDetail[1].toString();
			String chargeQuotevalue=chargeQuoteTemp.substring(0, chargeQuoteTemp.indexOf(" "));
			String chargeQuoteBasis=chargeQuoteTemp.substring(chargeQuoteTemp.indexOf(" ")+1, chargeQuoteTemp.length());
			
			markUpValue +=(profitPercent*Double.parseDouble(chargeQuotevalue)/100.00)*exchangeRate;
			
			
			//chargeMarkUp += chargeDetail[0]+ ":" + Math.round(Double.parseDouble(chargeQuotevalue)*(1+profitPercent/100.00)*100)/100.00 + " " + chargeQuoteBasis + ",";
			
		}
		}
	    }
		// Double haulingMarkUp=haulingQuote*(1+profitPercent/100.00);
		markUpValue +=(profitPercent*haulingQuote/100.00)*exchangeRate;
		
		
		Double quoteMarkup=0.0;
		if(applyCompletemarkUp){
			quoteMarkup=((quote*(1+profitPercent/100.00))+profitFlat);
		}else{
			quoteMarkup=((quote*(1+profitPercent/100.00))+profitFlat/2);
		}
		//quoteMarkup=((quote*(1+profitPercent/100.00))+profitFlat);
		//String chargeBreakUp="Weight: "+weightQuote+","+chargeBV+",Hauling:"+haulingQuote+"";
		if(applyCompletemarkUp){
			Double forwardingValue=markUpValue+profitFlat;
			resultantMarkup=""+Math.round(quoteMarkup*100)/100.00+"#"+"Forwarding:"+forwardingValue+"";
		}else{
			Double forwardingValue=markUpValue+profitFlat/2;
			resultantMarkup=""+Math.round(quoteMarkup*100)/100.00+"#"+"Forwarding:"+forwardingValue+"";
		}
	   
		return resultantMarkup;
		
	}

	private String calculateHaulingCharge(BigDecimal weight, String[] charges, double serviceMileage, double[] extraMileageLimits, double[] extraMileageCharges, double distance, String chargeBreakup, BigDecimal volume, Double exchangeRate) {
		String returnString="";
		String basis="";
		double quote = 0;
		double haulingCharge = 0;
		
		if(charges!=null){
		
		for (String charge:charges){
			String[] chargeDetail = charge.split(",");
			
			if(chargeDetail[0].equals("HAULING")){
				haulingCharge += computeOtherCharges(Double.parseDouble(weight.toString()), chargeDetail[2], Double.parseDouble(chargeDetail[3]),volume,exchangeRate);
				basis=chargeDetail[2];
			}
		}
		
		}
		quote += haulingCharge;
		returnString=String.valueOf(quote).concat("#").concat(basis);
		return returnString;
	}

	private String calculateOtherCharges(BigDecimal weight, String[] charges, double serviceMileage, double[] extraMileageLimits, double[] extraMileageCharges, double distance, String chargeBreakup, BigDecimal volume, Double exchangeRate) {
		double quote = 0;
		double otherCharges = 0;

		if(charges!=null){
		
		for (String charge:charges){
			String[] chargeDetail = charge.split(",");
			
			if(chargeDetail[0].equals("CHARGES") ){
				otherCharges += computeOtherCharges(Double.parseDouble(weight.toString()), chargeDetail[2], Double.parseDouble(chargeDetail[3]), volume,exchangeRate);  
				chargeBreakup += chargeDetail[1]+ ":" + Math.round(Double.parseDouble(chargeDetail[3])*100)/100.00 +" "+chargeDetail[2] +" ,";
			} 
			
		}
		}
		
		quote += otherCharges;
		
		//chargeBreakup = "Weight:"+ ((flatQuote>0?flatQuote:baseQuote) + extraMileageCharge) + "," + chargeBreakup;
		//chargeBreakup += "Hauling:" + haulingCharge; 
		
		return String.valueOf(Math.round(quote*100)/100.00).concat("#").concat(chargeBreakup);
	}

	
	String quoteBasis="";
	Double quoteMarkupValue=0.0;
	private double calculateWeightCharge(double weight, String[] charges, double serviceMileage, double[] extraMileageLimits, double[] extraMileageCharges, double distance, String chargeBreakup , BigDecimal volume, Double exchangeRate) {
		double quote = 0;
		double baseQuote = 0;
		double flatQuote = 0;
		double extraMileageCharge = 0;
		double minimumWeightQuote = -1;
		
		weight = applyMinimumWeight(weight, charges);
		
		for (String charge:charges){
			String[] chargeDetail = charge.split(",");
			
			if(chargeDetail[0].equals("WEIGHT") && chargeDetail[1].equals("Flat Rate")){
				flatQuote = Double.parseDouble(chargeDetail[3]);
			} else if(chargeDetail[0].equals("WEIGHT") && weight >=  Double.parseDouble(chargeDetail[4]) && weight <=  Double.parseDouble(chargeDetail[5])){
				baseQuote =  Double.parseDouble(chargeDetail[3])*(weight/100.00);
			} else if(chargeDetail[0].equals("WEIGHT") && weight <  Double.parseDouble(chargeDetail[4])) {
				double weightBracketMinQuote = Double.parseDouble(chargeDetail[3])*((Double.parseDouble(chargeDetail[4]))/100.00);
				minimumWeightQuote = minimumWeightQuote==-1?weightBracketMinQuote:(weightBracketMinQuote<minimumWeightQuote?weightBracketMinQuote:minimumWeightQuote);
			}
			
		}
		
		extraMileageCharge = computeWeightCharge(weight, distance, serviceMileage,
				extraMileageLimits, extraMileageCharges);
		
		baseQuote = (minimumWeightQuote != -1 && minimumWeightQuote < baseQuote)?minimumWeightQuote:baseQuote;
		quote += flatQuote>0?flatQuote:baseQuote;
		quote += Math.round(extraMileageCharge*100)/100.00;
		//System.out.println("\n\n\n\n quoteBasis--->"+" baseQuote--->"+baseQuote+"  flatQuote--->"+flatQuote+"  quote--->"+quote+" weight-->"+weight); 
		quoteBasis = flatQuote>0?"Flat":String.valueOf(Math.round(quote/weight*100*100)/100.00);
		//chargeBreakup = "Weight:"+ ((flatQuote>0?flatQuote:baseQuote) + extraMileageCharge) + "," + chargeBreakup;
		//chargeBreakup += "Hauling:" + haulingCharge; 
		
		return quote;
	}

	private List locateAgentsWithStandardCharges(BigDecimal weight, String destinationLatitude, String destinationLongitude, String tarrifApplicability, String destinationCountry, String packing, String destinationPOE, String sessionCorpID, Long pricingControlID, Date createdOn, String createdBy, String updatedBy, Date updatedon, Date expectedLoadDate, BigDecimal volume, String baseCurrency, String contract, boolean applyCompletemarkUp, String prefferedPortCode) {
		List allPotentialPartners= new ArrayList();
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedExpectedLoadDate = new StringBuilder(dateformatYYYYMMDD1.format(expectedLoadDate));
		
		String sql = "select p.latitude, p.longitude, p.id, p.lastName, " + //0,1,2,3
		"concat(p.billingAddress1) as address1, " + //4
		"concat(p.billingCity, ', ', p.billingState, ' ', " +
		"p.billingZip) as address2, " +	//5	
		"p.billingPhone, 'www.sscw.com' as partnerUrl, p.location1," + //6,7,8
		"convert(group_concat(concat_ws(',', prgd.gridSection, prgd.label, prgd.basis, prgd.value, prgd.weight1, prgd.weight2) separator '!'), char) as chargelist," + //9
		"prg.serviceRangeMiles,IFNULL(prg.serviceRangeMiles2,'0.00'),IFNULL(prg.serviceRangeMiles3,'0.00'), " +//10,11,12
		"IFNULL(prg.serviceRangeMiles4,'0.00'),IFNULL(prg.serviceRangeMiles5,'0.00'),IFNULL(prg.serviceRangeExtra2,'0.00'), IFNULL(prg.serviceRangeExtra3,'0.00'), " +//13,14,15,16
		"IFNULL(prg.serviceRangeExtra4,'0.00'), IFNULL(prg.serviceRangeExtra5,'0.00'), p.partnerCode , p.fidiNumber, " +//17,18,19,20 TODO: Handle additional columns cprg.serviceRangeMiles2, prg.serviceRangeMiles3.." //10
		"p.OMNINumber , p.qualityCertifications, p.serviceLines, prg.currency, prg.id as rateGridId" + //21,22,23,24,25
		"  from partner p, partnerrategrid prg, partnerrategriddetails prgd where " +
		"p.partnerCode = prg.partnerCode and prg.id = prgd.partnerRateGridId and " +
		"p.latitude is not null and p.longitude is not null and " +
		"p.isAgent=true and p.billingCountryCode like '"+destinationCountry+"%' and prg.tariffApplicability = '"+tarrifApplicability+"'" +
		"and (prgd.gridSection = 'WEIGHT' and prgd.grid = '"+packing+"' )" +
		//"(prgd.gridSection = 'CHARGES'  and prgd.grid = '"+packing+"' ) or " +
		//"(prgd.gridSection = 'HAULING' and prgd.grid = '"+packing+"' and prgd.label = '"+destinationPOE+"' ) )" +
		// and prg.id=prg2.id and prg2.publishTariff is true
		"and prg.effectivedate  = (select max(effectivedate) from partnerrategrid prg2 Where prg2.effectivedate <= '"+formatedExpectedLoadDate+"'  " + //TODO: replace by loading date
		"and prg2.status = 'Submitted' and prg.id=prg2.id and prg2.publishTariff is true)  "+
		"and ((if(prgd.basis in ('Included') and prgd.gridSection='CHARGES',convert(prgd.value, DECIMAL) >= 0.00,convert(prgd.value, DECIMAL) > 0.00)) or prgd.gridSection='HAULING') " +
		"and prgd.label not like 'Enter Charge%'" +
		"group by prg.partnercode";
		//System.out.println("\n\n\n\n\n\n sql query------>"+sql);
		allPotentialPartners= this.getSession().createSQLQuery(sql).list();
		StringBuilder partners = new StringBuilder();
		int count = 0;
		
		String[] charges = getStandardOtherCharges(destinationCountry, packing, tarrifApplicability);
		String[] chargesWithoutTHC=getStandardOtherChargesWithoutTHC(destinationCountry, packing, tarrifApplicability);
		
		Iterator it=allPotentialPartners.iterator();
		try{
		while(it.hasNext())
		{	
			
			
			Object []obj=(Object[]) it.next();
			String[] portCodeInitial=destinationPOE.split(",");
			for(String portCode:portCodeInitial){			
			
			double distance = getDistance(Double.parseDouble(destinationLatitude), Double.parseDouble(destinationLongitude), ((BigDecimal) obj[0]).doubleValue(), ((BigDecimal) obj[1]).doubleValue());
			double[] portCoords = getPortCoords(portCode, destinationCountry);
			double distanceFromPort = getDistance(portCoords[0], portCoords[1], ((BigDecimal) obj[0]).doubleValue(), ((BigDecimal) obj[1]).doubleValue());
			
			String[] weightCharges = ((String) obj[9]).split("!");
			double[] extraMileageLimits = {Double.parseDouble(obj[11].toString()), Double.parseDouble(obj[12].toString()),Double.parseDouble(obj[13].toString()),Double.parseDouble(obj[14].toString())}; 
			double[] extraMileageCharges = {Double.parseDouble(obj[15].toString()), Double.parseDouble(obj[16].toString()),Double.parseDouble(obj[17].toString()),Double.parseDouble(obj[18].toString())}; 
			
			double serviceMileage = ((BigDecimal) obj[10]).doubleValue();
			
			double maxServiceMileage = serviceMileage;
			Double exchangeRate=1.0;
            for (double extraMileageLimit:extraMileageLimits){
                if (extraMileageLimit > 0) maxServiceMileage = extraMileageLimit; 
            }
            
            if (distance <= maxServiceMileage) {
            	MathContext mc = new MathContext(1);
            	String chargeBreakup = "";
            	
            	if(!baseCurrency.equalsIgnoreCase(obj[24].toString()))
				{
            		List exchangeRateList=getHibernateTemplate().find("select currencyBaseRate from ExchangeRate where baseCurrency='"+baseCurrency+"' and currency='"+obj[24].toString()+"'");
            		if(!exchangeRateList.isEmpty())
            		{
            			exchangeRate=Double.parseDouble(exchangeRateList.get(0).toString());
            		}
				}
            	
            	double weightQuote = calculateWeightCharge(Double.parseDouble(weight.toString()), weightCharges, serviceMileage, extraMileageLimits, extraMileageCharges, distance, chargeBreakup,volume,exchangeRate);
				
            	String chargeQuote= calculateOtherCharges(weight, charges, serviceMileage, extraMileageLimits, extraMileageCharges, distance, chargeBreakup,volume,exchangeRate);
				String chargeValue=chargeQuote.substring(0, chargeQuote.indexOf("#"));
				String chargeBV=chargeQuote.substring(chargeQuote.indexOf("#")+1, chargeQuote.length());
				double cv=Double.parseDouble(chargeValue);
				
				String chargeQuoteWithoutTHC= calculateOtherCharges(weight, chargesWithoutTHC, serviceMileage, extraMileageLimits, extraMileageCharges, distance, chargeBreakup,volume,exchangeRate);
				String chargeValueWithoutTHC=chargeQuoteWithoutTHC.substring(0, chargeQuoteWithoutTHC.indexOf("#"));
				String chargeBVWithoutTHC=chargeQuoteWithoutTHC.substring(chargeQuoteWithoutTHC.indexOf("#")+1, chargeQuoteWithoutTHC.length());
				double cvWithoutTHC=Double.parseDouble(chargeValueWithoutTHC);
				
				String haulingQuoteString= calculateStandardHaulingCharge(Double.parseDouble(weight.toString()), packing, destinationCountry, distanceFromPort,volume, exchangeRate);
				String hauQuote=haulingQuoteString.substring(0, haulingQuoteString.indexOf("#"));
				String hauQuoteBasis=haulingQuoteString.substring(haulingQuoteString.indexOf("#")+1, haulingQuoteString.length()); 
				
				//double haulingQuote= calculateStandardHaulingCharge(Double.parseDouble(weight.toString()), packing, destinationCountry, distanceFromPort,volume, exchangeRate);
				double haulingQuote=Double.valueOf(hauQuote);
				
				double quote=Math.round((weightQuote+cv+haulingQuote)*100)/100.00;
            	double quoteWithoutTHC=Math.round((weightQuote+cvWithoutTHC+haulingQuote)*100)/100.00;
            	            	
            	String chargeBreakUp="Weight: "+weightQuote+","+chargeBV+",Hauling:"+haulingQuote+"";
            	String chargeBreakUpWithoutTHC="Weight: "+weightQuote+","+chargeBVWithoutTHC+",Hauling:"+haulingQuote+"";
            	
				
				String forwardingMarkUp=computeForwardingMarkup(weightQuote, chargeBV, haulingQuote, quote,exchangeRate, contract, applyCompletemarkUp,hauQuoteBasis);
            	
				String quoteMarkUp=forwardingMarkUp.substring(0, forwardingMarkUp.indexOf("#"));
            	Double quoteMarkUpRounded=(Math.round(Double.parseDouble(quoteMarkUp)*100)/100.00);
            	String chargewithMarkUp=forwardingMarkUp.substring(forwardingMarkUp.indexOf("#")+1,forwardingMarkUp.length());
            	
            	String forwardingMarkUpWithoutTHC=computeForwardingMarkup(weightQuote, chargeBVWithoutTHC, haulingQuote, quoteWithoutTHC,exchangeRate, contract, applyCompletemarkUp,hauQuoteBasis);
            	
				String quoteMarkUpWithoutTHC=forwardingMarkUpWithoutTHC.substring(0, forwardingMarkUpWithoutTHC.indexOf("#"));
            	Double quoteMarkUpRoundedWithoutTHC=(Math.round(Double.parseDouble(quoteMarkUpWithoutTHC)*100)/100.00);
            	String chargewithMarkUpWithoutTHC=forwardingMarkUpWithoutTHC.substring(forwardingMarkUpWithoutTHC.indexOf("#")+1,forwardingMarkUpWithoutTHC.length());
            	
            	Double discountApplied=computeDiscount(sessionCorpID, weightQuote, chargeBV, haulingQuote ,exchangeRate, contract,obj[25].toString());
            	
            	Double discountAppliedWithoutTHC=computeDiscount(sessionCorpID, weightQuote, chargeBVWithoutTHC, haulingQuote ,exchangeRate, contract,obj[25].toString());
            	
				PricingControlDetails pricingControlDetails= new PricingControlDetails();
				pricingControlDetails.setPartnerID(obj[2].toString());
				pricingControlDetails.setPartnerName(obj[3].toString());
				pricingControlDetails.setAddress1(obj[4].toString());
				pricingControlDetails.setAddress2(obj[5].toString());
				pricingControlDetails.setUrl(obj[7].toString());
				if(obj[8]==null){pricingControlDetails.setLocationPhotoUrl("");}else{
					pricingControlDetails.setLocationPhotoUrl(obj[8].toString());
				}
				try{
					if(quoteBasis.equalsIgnoreCase("Flat")){
						pricingControlDetails.setBasis(quoteBasis);
						pricingControlDetails.setBasisWithoutMarkUp(quoteBasis);
					}else{
						pricingControlDetails.setBasisWithoutMarkUp(quoteBasis);
						pricingControlDetails.setBasis(String.valueOf(Math.round(Double.parseDouble(quoteBasis)*(1+quoteMarkupValue/100.00)*100)/100.00));
						
					}
				}catch(Exception ex){};
				pricingControlDetails.setLatitude(obj[0].toString());
				pricingControlDetails.setLongitude(obj[1].toString());
				pricingControlDetails.setRate(String.valueOf(quote));
				pricingControlDetails.setQuoteWithoutTHC(String.valueOf(quoteWithoutTHC));
				pricingControlDetails.setCorpID(sessionCorpID);
				pricingControlDetails.setPricingControlID(pricingControlID.toString());
				pricingControlDetails.setCreatedBy(createdBy);
				pricingControlDetails.setUpdatedBy(updatedBy);
				pricingControlDetails.setCreatedOn(new Date());
				pricingControlDetails.setUpdatedOn(new Date());
				pricingControlDetails.setTarrifApplicability(tarrifApplicability);
				pricingControlDetails.setChargeDetail(chargeBreakUp);
				pricingControlDetails.setChargeDetailWithoutTHC(chargeBreakUpWithoutTHC);
				pricingControlDetails.setRateMarkUp(quoteMarkUpRounded.toString());
				pricingControlDetails.setQuoteMarkupWithoutTHC(quoteMarkUpRoundedWithoutTHC.toString());
				pricingControlDetails.setChargeDetailMarkUp(chargewithMarkUp);
				pricingControlDetails.setChargeDetailMarkUpWithoutTHC(chargewithMarkUpWithoutTHC);
				pricingControlDetails.setBaseCurrency(baseCurrency);
				pricingControlDetails.setExchangeRate(exchangeRate);
				pricingControlDetails.setDthcFlag(false);
				pricingControlDetails.setHaulingBasis(hauQuoteBasis);
				pricingControlDetails.setAddressDistance(String.valueOf(Math.round(distance*100)/100.00));
				pricingControlDetails.setHaulingDistance(String.valueOf(Math.round(distanceFromPort*100)/100.00));
				if(tarrifApplicability.equalsIgnoreCase("Origin")){
					pricingControlDetails.setOriginPortCode(portCode);
				}
				if(tarrifApplicability.equalsIgnoreCase("Destination")){
					pricingControlDetails.setDestinationPortCode(portCode);
				}
				
				/*if(portCode.equalsIgnoreCase(prefferedPortCode)){
					pricingControlDetails.setPreferredPort(true);
				}else{
					pricingControlDetails.setPreferredPort(false);
				}*/
				pricingControlDetails.setDestinationSelection(false);
				if(obj[20]==null){pricingControlDetails.setFidiNumber("");}else{pricingControlDetails.setFidiNumber(obj[20].toString());}
				if(obj[21]==null){pricingControlDetails.setOMNINumber("");}else{pricingControlDetails.setOMNINumber(obj[21].toString());}
				if(obj[22]==null){pricingControlDetails.setQualityCertifications("");}else{pricingControlDetails.setQualityCertifications(obj[22].toString());}
				if(obj[23]==null){pricingControlDetails.setServiceLines("");}else{pricingControlDetails.setServiceLines(obj[23].toString());}
				if(obj[24]==null){pricingControlDetails.setCurrency("");}else{pricingControlDetails.setCurrency(obj[24].toString());}
				if(obj[25]==null){pricingControlDetails.setRateGridId(null);}else{pricingControlDetails.setRateGridId(Long.parseLong(obj[25].toString()));}				
				pricingControlDetails.setDiscountValue(discountApplied);
				pricingControlDetails.setDiscountValueWithoutTHC(discountAppliedWithoutTHC);
				try{
					String rating = partnerRatingList(obj[19].toString(), sessionCorpID);
					pricingControlDetails.setFeedbackRating(rating);
				
				
					String activity = partnerActivityList(obj[19].toString(), sessionCorpID);
					pricingControlDetails.setShipmentActivity(activity);
				}catch(Exception ex)
				{
					System.out.println("\n\n\n\n\n rating exception---->"+ex);
					ex.printStackTrace();
				}
            
				pricingControlDetailsManager.save(pricingControlDetails);
				/*if (count > 1) partners.append(","); else partners.append("");
				partners.append("{\"id\":" + obj[2] + ", \"name\": \"" + obj[3] + "\", \"address1\": \"" + obj[4] + "\", \"address2\": \"" + obj[5] + "\", \"phone\": \"" + 
				obj[6] + "\", \"url\": \"" + obj[7] + "\", \"locationPhotoUrl\": \"" + obj[8] + "\", \"latitude\": " + obj[0] + 
				", \"longitude\": " + obj[1] + ", \"rate\": " + quote + "}");*/
			}
		}
		}
		}catch(Exception ex)
		{
			System.out.println("\n\n\n\n\n whole exception---->"+ex);
			ex.printStackTrace();
		}
		
		return allPotentialPartners;
		
	}

	private String[] getStandardOtherChargesWithoutTHC(String destinationCountry, String packing, String tarrifApplicability) {
		String sql="Select convert(group_concat(concat_ws(',', 'CHARGES', label, basis, value, 0, 0) separator '!'), char) as chargelist  from partnerrefcharges where grid = '"+packing+"' and countryCode = '"+destinationCountry+"' and tariffApplicability='"+tarrifApplicability+"' and label!='THC' group by countryCode";
		//System.out.println("\n\n\n\n\n\n sql query------>"+sql);
		String[] charges = null;
		List ls= this.getSession().createSQLQuery(sql).list();
		
		if(!ls.isEmpty()){
			charges = ls.get(0).toString().split("!");
		}
			
     
		return charges;
	}

	private Double computeDiscount(String sessionCorpID, Double  weightQuote, String chargewithMarkUp, Double haulingQuote, Double exchangeRate, String Contract, String rateGridId) {
		Double discountBaseRate=0.0;
		Double discountHaulingCharges=0.0;
		Double discountSITCharges=0.0;
		Double discountAddlCharges=0.0;
		Double discountAutoHandling=0.0;
		
		Double totalDiscount=0.0;
		exchangeRate=1.0;
		
		String sql="select discountBaseRate, discountHaulingCharges, discountSITCharges, discountAddlCharges, " +
		"discountAutoHandling from PartnerRateGridContracts " +
		"where discountCompany='"+sessionCorpID+"' " +
		"and  contract like '%"+Contract+"%' and partnerRateGridID="+rateGridId+"";
		
		List discounts=getHibernateTemplate().find(sql);
		
		Iterator  it =discounts.iterator();
		while(it.hasNext()){
			Object [] row=(Object[])it.next();
			discountBaseRate=Double.valueOf(row[0].toString());
			discountHaulingCharges=Double.valueOf(row[1].toString());
			discountSITCharges=Double.valueOf(row[2].toString());
			discountAddlCharges=Double.valueOf(row[3].toString());
			discountAutoHandling=Double.valueOf(row[4].toString());
		}
		// Weight:2548.0,,Hauling:504.0,Forwarding:175.0
		
		
		totalDiscount +=(discountBaseRate*weightQuote/100.00)*exchangeRate;
		
		 if(chargewithMarkUp.trim().length()>0){
			    
				if(chargewithMarkUp.indexOf(",")==0)
				 {
					chargewithMarkUp=chargewithMarkUp.substring(1);
				 }
				if(chargewithMarkUp.lastIndexOf(",")==chargewithMarkUp.length()-1)
				 {
					chargewithMarkUp=chargewithMarkUp.substring(0, chargewithMarkUp.length()-1);
				 }
				String[] charges = chargewithMarkUp.split(",");
				if(charges.length >= 1){
					for (String charge:charges){
					String[] chargeDetail = charge.split(":");
					if(chargeDetail.length>1){
					
						String chargeQuoteTemp=chargeDetail[1].toString();
						String chargeQuotevalue=chargeQuoteTemp.substring(0, chargeQuoteTemp.indexOf(" "));
						String chargeQuoteBasis=chargeQuoteTemp.substring(chargeQuoteTemp.indexOf(" ")+1, chargeQuoteTemp.length());
						totalDiscount +=(discountAddlCharges*Double.parseDouble(chargeQuotevalue)/100.00)*exchangeRate;
						
					}
					
				}
			}
		 }
		  
		  totalDiscount +=(discountHaulingCharges*haulingQuote/100.00)*exchangeRate;
		
		
		  totalDiscount=Math.round(totalDiscount*100)/100.00;
		return totalDiscount;
	}

	private String  calculateStandardHaulingCharge(double weight, String packing, String destinationCountry, double distanceFromPort, BigDecimal volume, Double exchangeRate) {
		String returnString="";
		String basis="";
		double quote = 0;
		double haulingCharge = 0;
		String sql="";
		String maxDistanceSql= "SELECT highDistance FROM PartnerRefHaulingGrid  where grid = '"+packing+"' and countryCode = '"+destinationCountry+"' order by highDistance desc limit 1  ";
		List ls1=getHibernateTemplate().find(maxDistanceSql);
		if(!ls1.isEmpty() && (distanceFromPort > Double.parseDouble(ls1.get(0).toString()))){
			sql="select concat(rateMile,'#', rateFlat,'#',unit) from partnerrefhaulinggrid where grid = '"+packing+"' and countryCode = '"+destinationCountry+"' order by highDistance desc limit 1";
		}else{
			sql="select concat(rateMile,'#', rateFlat,'#',unit) from partnerrefhaulinggrid where grid = '"+packing+"' and countryCode = '"+destinationCountry+"'  and cast("+distanceFromPort+" as decimal) >= lowDistance and cast("+distanceFromPort+" as decimal) <= highDistance limit 1";
		}
		//System.out.println("\n\n\n\n\n\n calculateStandardHaulingCharge--->"+sql);
		List ls=this.getSession().createSQLQuery(sql).list();;
		if(!ls.isEmpty()){
			String s1=ls.get(0).toString();
			String chargeDetail[]=s1.split("#");
			
			Double rateMile=Double.parseDouble(chargeDetail[0]);
			Double rateFlat=Double.parseDouble(chargeDetail[1]);
			basis=chargeDetail[2];
			haulingCharge +=   rateFlat + distanceFromPort*rateMile;
			
			quote += haulingCharge;
		}
		
		returnString=String.valueOf(quote).concat("#").concat(basis);
		return returnString;
	}

	private double[] getPortCoords(String destinationPOE, String destinationCountry) {
		Double lat=0.0;
		Double lan=0.0;
		List ls= getHibernateTemplate().find("select concat(latitude,'#', longitude) from Port where portCode = '"+destinationPOE+"' and countryCode = '"+destinationCountry+"'"); 
		if(!ls.isEmpty()){
			String s1=ls.get(0).toString();
			lat=Double.parseDouble(s1.substring(0, s1.indexOf("#")));
			lan=Double.parseDouble(s1.substring(s1.indexOf("#")+1,s1.length()));
		}
		
		double[] latLong={lat,lan};
		return latLong;
		
	}

	private String[] getStandardOtherCharges(String destinationCountry, String packing, String tarrifApplicability) {
		//		 Select convert(group_concat(concat_ws(',', 'CHARGES', prgd.label, prgd.basis, prgd.value, 0, 0) separator '!'), char) as chargelist 
		// from partnerrefcharges where grid = gridSection and countryCode = countryCode group by countryCode
		//String [] charges = ((String) obj[0]).split("!");
		String sql="Select convert(group_concat(concat_ws(',', 'CHARGES', label, basis, value, 0, 0) separator '!'), char) as chargelist  from partnerrefcharges where grid = '"+packing+"' and countryCode = '"+destinationCountry+"' and tariffApplicability='"+tarrifApplicability+"' group by countryCode";
		//System.out.println("\n\n\n\n\n\n sql query------>"+sql);
		String[] charges = null;
		List ls= this.getSession().createSQLQuery(sql).list();
		//Iterator it=ls.iterator();
		//while(it.hasNext())
		//{		 
			//Object []obj=(Object[]) it.next();
		if(!ls.isEmpty()){
			charges = ls.get(0).toString().split("!");
		}
			
     	//}
		return charges;
		}
		
	

	private boolean useStandardHaulingRates(String destinationCountry) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    User user = (User)auth.getPrincipal();
	   	List StandardHaulingCountry=getHibernateTemplate().find("select standardCountryHauling from SystemDefault where corpID='"+user.getCorpID()+"'");
		if(StandardHaulingCountry.get(0).toString().equalsIgnoreCase(destinationCountry))
		{
			return true;
		}else{
		return false;
		}
	}

	private boolean useStandardOtherCharges(String destinationCountry) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    User user = (User)auth.getPrincipal();
		List standardCountryCharges=getHibernateTemplate().find("select standardCountryCharges from SystemDefault where corpID='"+user.getCorpID()+"'");
		if(standardCountryCharges.get(0).toString().equalsIgnoreCase(destinationCountry))
		{
			return true;
		}else{
		return false;
		}
	}

	public void setPricingControlDetailsManager(
			PricingControlDetailsManager pricingControlDetailsManager) {
		this.pricingControlDetailsManager = pricingControlDetailsManager;
	}
	
	
	public String partnerRatingList(String partnerCode, String corpID){
		/*List partnerRating = partnerManager.ratingList(partnerCode, corpID);
		SortedMap <String, BigDecimal> yearMapP = new TreeMap<String, BigDecimal>();
		SortedMap <String, BigDecimal> yearMapN = new TreeMap<String, BigDecimal>();
		String rating ="";
		Iterator it = partnerRating.iterator();
		
		String key = new String("");
		String samekey = new String("");
		String year = "";
		BigDecimal pValue = new BigDecimal("0");
		BigDecimal nValue = new BigDecimal("0");
		
		BigDecimal ratingTempPos = new BigDecimal("0");
		BigDecimal ratingTempCount = new BigDecimal("0");
		BigDecimal multiplyVal = new BigDecimal(100);
		
		while(it.hasNext()){	
			Object listObject = it.next();
			if(((HibernateDTO) listObject).getYear() == null){
				year = "";
			}else{
				year =((HibernateDTO) listObject).getYear().toString();
			}
			
			if(((HibernateDTO) listObject).getOaeValuationP() != null){
				pValue = new BigDecimal(((HibernateDTO) listObject).getOaeValuationP().toString());
			}
			
			if(((HibernateDTO) listObject).getOaeValuationN() != null){
				nValue = new BigDecimal(((HibernateDTO) listObject).getOaeValuationN().toString());
			}
			
			ratingTempPos = ratingTempPos.add(pValue);
			ratingTempCount = ratingTempCount.add(pValue).add(nValue.abs());
			
			key = year;
			if (key.equals(samekey)){
				BigDecimal valuePos = yearMapP.get(key);
				if(valuePos != null){
					pValue = pValue.add(valuePos);
				}
				
				BigDecimal valueNag = yearMapN.get(key);
				if(valueNag != null){
					nValue = nValue.add(valueNag);
				}
				
				yearMapP.put(year, pValue);
				yearMapN.put(year, nValue);
			}else{
				yearMapP.put(year, pValue);
				yearMapN.put(year, nValue);
			}
			samekey = year;
		}
		try{
			BigDecimal ratingTemp = (ratingTempPos.divide(ratingTempCount, 3, 0)).multiply(multiplyVal);
			rating = new BigDecimal(Math.round(ratingTemp.floatValue())).toString();
		}catch(Exception ex){}*/
		
		String rating ="";
		BigDecimal ratingTemp1y=new BigDecimal("0");
		BigDecimal multiplyVal = new BigDecimal(100);
		//BigDecimal posSix = new BigDecimal("0");
		//BigDecimal negSix = new BigDecimal("0");
		
		BigDecimal pos1 = new BigDecimal("0");
		BigDecimal neg1 = new BigDecimal("0");
		
		//BigDecimal pos2 = new BigDecimal("0");
		//BigDecimal neg2 = new BigDecimal("0");
		
		/*List partnerRatingSixMonths = partnerManager.ratingListSixMonths(partnerCode, sessionCorpID);
		Iterator it6m = partnerRatingSixMonths.iterator();
		while(it6m.hasNext()){	
			Object [] row = (Object[])it6m.next();
			posSix = posSix.add(new BigDecimal(row[2].toString()));
			negSix = negSix.add(new BigDecimal(row[3].toString()));
		}*/
		
		List partnerRatingOneYr = partnerManager.ratingListOneYr(partnerCode, corpID);
		Iterator it1y = partnerRatingOneYr.iterator();
		while(it1y.hasNext()){	
			Object [] row = (Object[])it1y.next();
			pos1 = pos1.add(new BigDecimal(row[2].toString()));
			neg1 = neg1.add(new BigDecimal(row[3].toString()));
		}

		/*List partnerRatingTwoYr = partnerManager.ratingListTwoYr(partnerCode, sessionCorpID);
		Iterator it2y = partnerRatingTwoYr.iterator();
		while(it2y.hasNext()){	
			Object [] row = (Object[])it2y.next();
			pos2 = pos2.add(new BigDecimal(row[2].toString()));
			neg2 = neg2.add(new BigDecimal(row[3].toString()));
		}*/
		
		//BigDecimal ratingTempCount6m = posSix.add(negSix.abs());
		BigDecimal ratingTempCount1y = pos1.add(neg1.abs());
		//BigDecimal ratingTempCount2y = pos2.add(neg2.abs());
		try{
			//BigDecimal ratingTemp6m = (posSix.divide(ratingTempCount6m, 3, 0)).multiply(multiplyVal);
			//rating6m = new BigDecimal(Math.round(ratingTemp6m.floatValue()));
			
			ratingTemp1y = (pos1.divide(ratingTempCount1y, 3, 0)).multiply(multiplyVal);
			ratingTemp1y = new BigDecimal(Math.round(ratingTemp1y.floatValue()));
			
			//BigDecimal ratingTemp2y = (pos2.divide(ratingTempCount2y, 3, 0)).multiply(multiplyVal);
			//rating2y = new BigDecimal(Math.round(ratingTemp2y.floatValue()));
			
		}catch(Exception ex){}
		
		rating=ratingTemp1y.toString();
				
		return rating;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	
	
	
	public String partnerActivityList(String partnerCode, String corpID){
		List partnerActivity = partnerManager.getPartnerActivityList(partnerCode, corpID);
		BigDecimal activity = new BigDecimal("0");
		Iterator it = partnerActivity.iterator();
		
		while(it.hasNext()){	
			Object listObject = it.next();
			BigDecimal activityTemp = new BigDecimal("0");
			if(((HibernateDTO) listObject).getOaCount() != null){
				activityTemp = new BigDecimal(((HibernateDTO) listObject).getOaCount().toString());
			}
			activity = activity.add(activityTemp);
		}
		
		return activity.toString();
	}

	public String locateAgents1(Long id, String tarrifApplicability, String fidi, String omni, Double minimumFeedbackRating, String faim, String iso9002, String iso27001, String iso1400, String rim, String dos, String gsa, String military) {
		String queryTemp = "";
		if(fidi != null && fidi.equalsIgnoreCase("true")){
			queryTemp = "AND fidiNumber = 'Y' ";
		}
		if(minimumFeedbackRating != null && minimumFeedbackRating!=0.00){
			queryTemp += queryTemp.concat("AND feedbackRating >= "+minimumFeedbackRating+" ");
		}
		
		if(omni != null && omni.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND OMNINumber <> '' ");
		}
		if(faim != null && faim.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%FAIM%' ");
		}
		if(iso9002 != null && iso9002.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%ISO 9002%' ");
		}
		if(iso27001 != null && iso27001.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%ISO 27001%' ");
		}
		if(iso1400 != null && iso1400.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%ISO 1400%' ");
		}
		if(rim != null && rim.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%RIM%' ");
		}
		/*if(dos != null && dos.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND serviceLines like  '%DOS%' ");
		}
		if(gsa != null && gsa.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND serviceLines like  '%GSA%' ");
		}
		if(military != null && military.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND serviceLines like  '%Military%' ");
		}*/
		
		
		
		String sql = "select partnerID, partnerName, address1,address2,rate,url,locationPhotoUrl,latitude, longitude,feedbackRating,shipmentActivity, baseCurrency, id,pricingControlID, originSelection,destinationSelection, chargeDetail,rateMarkUp,chargeDetailMarkUp, exchangeRate, addressDistance, haulingDistance, basis, basisWithoutMarkUp, haulingBasis, originPortCode,destinationPortCode,preferredPort, quoteWithoutTHC, quoteMarkupWithoutTHC, chargeDetailWithoutTHC, chargeDetailMarkUpWithoutTHC from pricingcontroldetails where tarrifApplicability='"+tarrifApplicability+"' and  pricingControlID="+id+" "+ queryTemp +"";
		List allPotentialPartners= this.getSession().createSQLQuery(sql).list();

		StringBuilder partners = new StringBuilder();
		int count = 0;
		Iterator it=allPotentialPartners.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
				count++;
				
				if (count > 1) partners.append(","); else partners.append("");
				partners.append("{\"id\":" + obj[0] + ", \"name\": \"" + obj[1] + "\", \"address1\": \"" + obj[2] + "\", \"address2\": \"" + obj[3] + "\", \"url\": \"" + obj[5] + "\", \"locationPhotoUrl\": \"" + obj[6] + "\", \"latitude\": " + obj[7] + 
				", \"longitude\": " + obj[8] + ", \"rate\": " + obj[4] + ", \"feedbackRating\": \"" + obj[9] + "\", \"shipmentActivity\": \"" + obj[10] + "\", \"currency\": \"" + obj[11] + "\", \"detailID\": \"" + obj[12] + "\", \"pricingControlID\": \"" + obj[13] + "\", \"originSelection\": \"" + obj[14] + "\", \"destinationSelection\": \"" + obj[15] + "\", \"chargeDetail\": \"" + obj[16] + "\", \"rateMarkUp\": \"" + obj[17] + "\", \"chargeDetailMarkUp\": \"" + obj[18] + "\", \"exchangeRate\": \"" + obj[19] + "\", \"addressDistance\": \"" + obj[20] + "\", \"haulingDistance\": \"" + obj[21] + "\", \"basis\": \"" + obj[22] + "\" , \"basisWithoutMarkUp\": \"" + obj[23] + "\", \"haulingBasis\": \"" + obj[24] + "\", \"originPortCode\": \"" + obj[25] + "\", \"destinationPortCode\": \"" + obj[26] + "\", \"preferredPort\": \"" + obj[27] + "\", \"quoteWithoutTHC\": \"" + obj[28] + "\", \"quoteMarkupWithoutTHC\": \"" + obj[29] + "\", \"chargeDetailWithoutTHC\": \"" + obj[30] + "\", \"chargeDetailMarkUpWithoutTHC\": \"" + obj[31] + "\"}");
			
		}
		String prependStr = "{ \"count\":" + count + ", \"partners\":[";
		String appendStr = "]}";
		String s=prependStr + partners.toString() + appendStr;
		return prependStr + partners.toString() + appendStr;
	}

	public void deletePricingDetails(Long id, String tarrifApplicability) {
		getHibernateTemplate().bulkUpdate("delete from PricingControlDetails where tarrifApplicability='"+tarrifApplicability+"' and pricingControlID="+id+"");		
	}

	public void updateSelectedResult(Long pricingDetailsId, Long pricingControlID, String tarrifApplicability) {
		getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=false,preferredPort=false  where pricingControlID="+pricingControlID+" and tarrifApplicability='"+tarrifApplicability+"'");
		getHibernateTemplate().bulkUpdate("update PricingControlDetails set destinationSelection=true,preferredPort=true where id="+pricingDetailsId+"");		
	}

	public int getCount(Long pricingControlID) {
		List count= getHibernateTemplate().find("from PricingControlDetails where pricingControlID="+pricingControlID+"");
		int size=count.size();
		
		return size;
	}

	public String getPorts(String countryCode, String latitude, String longitude, String mode) {
		String sql="";
		if(mode.equalsIgnoreCase("Sea"))
		{
			sql = "select portcode, portName, latitude, longitude, id from port where countryCode='"+countryCode+"' and mode='"+mode+"' and latitude!='' and longitude!='' and active=true";
		}
		if(mode.equalsIgnoreCase("Air"))
		{
			sql = "select portcode, portName, latitude, longitude, id from port where countryCode='"+countryCode+"' and mode='"+mode+"' and latitude!='' and longitude!='' and active=true and isCargoAirport=true";
		}
		
		List allPotentialPorts= this.getSession().createSQLQuery(sql).list();

		StringBuilder ports = new StringBuilder();
		int count = 0;
		Iterator it=allPotentialPorts.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			double distance = getDistance(Double.parseDouble(obj[2].toString()), Double.parseDouble(obj[3].toString()), Double.parseDouble(latitude), Double.parseDouble(longitude));
			if(distance<=400){	
			count++;
				
				if (count > 1) ports.append(","); else ports.append("");
				ports.append("{\"id\":" + obj[4] + ", \"name\": \"" + obj[1] + "\", \"portcode\": \"" + obj[0] + "\", \"latitude\": " + obj[2] + ", \"longitude\": \"" + obj[3] + "\"}");
			}
		}
		String prependStr = "{ \"count\":" + count + ", \"ports\":[";
		String appendStr = "]}";
		String s=prependStr + ports.toString() + appendStr;
		return prependStr + ports.toString() + appendStr;
	}

	public List getContract(String sessionCorpID) {
		return getHibernateTemplate().find("select contract from Contract where corpID='"+sessionCorpID+"' and  displayonAgentTariff is true  and ending is null group by contract");
	}

	public String locateFreightDetails(String originPOE, String destinationPOE, String containerSize, String sessionCorpID) {
		
		List originPortCityList=getHibernateTemplate().find("select refAbbreviation from Port where portCode='"+originPOE+"' and corpID in ('" + sessionCorpID + "','TSFT')");														
		List destinationPortCityList=getHibernateTemplate().find("select refAbbreviation from Port where portCode='"+destinationPOE+"' and corpID in ('" + sessionCorpID + "','TSFT')");
		String originPortCity="";
		String destinationPortCity="";
		if(!originPortCityList.isEmpty())
		{
			originPortCity=originPortCityList.get(0).toString();
		}
		if(!destinationPortCityList.isEmpty())
		{
			destinationPortCity=destinationPortCityList.get(0).toString();
		}
		//originPortCity="Houston, TX";
		//destinationPortCity="Bangkok";
		String sql = "select carrier, originCity,originCountry, originPortCity, destinationCity, " +
				"destinationCountry , totalPrice, laneComments, destTHCValue, destTHCDetails " +
				"from reffreightrates where originCity like '%"+originPortCity+"%' " +
				"and destinationCity like '%"+destinationPortCity+"%' and mode='Sea' and equipmentType like '"+containerSize+"%'";
		
		List allPotentialFreights= this.getSession().createSQLQuery(sql).list();

		StringBuilder freights = new StringBuilder();
		int count = 0;
		Iterator it=allPotentialFreights.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			
			count++;
				
				if (count > 1) freights.append(","); else freights.append("");
				freights.append("{\"carrier\":\"" + obj[0] + "\", \"originCity\": \"" + obj[1] + "\", " +
				"\"originCountry\": \"" + obj[2] + "\", \"originPortCity\": \"" + obj[3] + "\", " +
				"\"destinationCity\": \"" + obj[4] + "\", \"destinationCountry\": \"" + obj[5] + "\", " +
				"\"totalPrice\": \"" + obj[6] + "\", \"laneComments\": \"" + obj[7] + "\", " +
				"\"destTHCValue\": \"" + obj[8] + "\", \"destTHCDetails\": \"" + obj[9] + "\"}");
			
		}
		String prependStr = "{ \"count\":" + count + ", \"freights\":[";
		String appendStr = "]}";
		String s=prependStr + freights.toString() + appendStr;
		return prependStr + freights.toString() + appendStr;
	}

	public void updateFreightResult(Long freightId, Long pricingControlID, String tariffApplicability, String freightCurrency, String baseCurrency, String mode, String contract, String freightPrice) {
		/*Double exchangeRate=1.0;
		Double markup=0.0;
		if(!baseCurrency.equalsIgnoreCase(freightCurrency))
		{
    		List exchangeRateList=getHibernateTemplate().find("select currencyBaseRate from ExchangeRate where baseCurrency='"+baseCurrency+"' and currency='"+freightCurrency+"'");
    		if(!exchangeRateList.isEmpty())
    		{
    			exchangeRate=Double.parseDouble(exchangeRateList.get(0).toString());
    		}
		}
		
		String markupSQl="select concat(profitFlat,'#', profitPercent)  from freightmarkupcontrol where "+freightPrice+">=lowCost and "+freightPrice+"<=highCost and contract='"+contract+"' and mode='"+mode+"'";
		List markupList= this.getSession().createSQLQuery(markupSQl).list();
		if(!markupList.isEmpty()){
			String markupListString=markupList.get(0).toString();
			String[] markupArray=markupListString.split("#");
			Double profitFlat=Double.parseDouble(markupArray[0]);
			Double profitPercent=Double.parseDouble(markupArray[1]);
			markup=Double.parseDouble(freightPrice)*(profitPercent/100.00) + profitFlat;
		}
		
		
		getHibernateTemplate().bulkUpdate("update PricingControlDetails set refFreightId="+freightId+" , freightSelection=true, exchangeRate="+exchangeRate+" , currency='"+freightCurrency+"', frieghtMarkup="+markup+" where pricingControlID="+pricingControlID+" and tarrifApplicability='"+tariffApplicability+"'");*/
		getHibernateTemplate().bulkUpdate("update PricingFreight set freightSelection=false where pricingControlID="+pricingControlID+"");
		getHibernateTemplate().bulkUpdate("update PricingFreight set freightSelection=true where pricingControlID="+pricingControlID+" and reffreightId="+freightId+"" );
	}

	public List getCurrency(String sessionCorpID) {
		return getHibernateTemplate().find("select currency from ExchangeRate where corpID='"+sessionCorpID+"'");
	}

	public List getAcceptedQuotes(String sessionCorpID, String parentAgent) {
		return getHibernateTemplate().find("from PricingControl where corpID='"+sessionCorpID+"'  and parentAgent = '"+parentAgent+"' and (sequenceNumber = '' or sequenceNumber=null)");
	}

	public List getAgentList(String parentAgent, String sessionCorpID) {
		return getHibernateTemplate().find("from PricingControl where corpID='"+sessionCorpID+"' and parentAgent = '"+parentAgent+"'");
	}

	public List getPortLocation(String originPOE, String sessionCorpID) {
		return getHibernateTemplate().find("select concat(latitude,'#',longitude) from Port where portCode='"+originPOE+"' and corpID in ('" + sessionCorpID + "','TSFT')");
	}

	public void updateServiceOrder(Long id, String shipNumber, String sessionCorpID) {
		getHibernateTemplate().bulkUpdate("update PricingControl set shipNumber='"+shipNumber+"' where id="+id+" and corpID in ('" + sessionCorpID + "','TSFT')");
	}

	public List findRateDeptEmailAddress(String sessionCorpID) {
		return getHibernateTemplate().find("select rateDeptEmail from SystemDefault where corpID in ('" + sessionCorpID + "','TSFT')");
	}

	public List getRecentTariffs(String sessionCorpID) {
		List list1 = new ArrayList();
		String sql ="select prg.partnerName, prg.tariffApplicability,prg.terminalCountry,prg.metroCity," + //0,1,2,3
				"prg.effectiveDate,prg.tariffScope, ref.description " + //4,5,6
				"from partnerrategrid prg, refmaster ref where " +
				"prg.terminalCountry=ref.code and ref.parameter='COUNTRY' " +
				"and prg.status = 'Submitted' and prg.publishTariff is true";
		
		List recentTariffs= this.getSession().createSQLQuery(sql).list();
		Iterator it=recentTariffs.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			RecentTariffDTO recentTariffDTO= new RecentTariffDTO();
			recentTariffDTO.setPartnerName(obj[0]);
			recentTariffDTO.setTariffApplicability(obj[1]);
			recentTariffDTO.setTerminalCountry(obj[2]);
			recentTariffDTO.setMetroCity(obj[3]);
			recentTariffDTO.setEffectiveDate(obj[4]);
			recentTariffDTO.setTariffScope(obj[5]);
			recentTariffDTO.setDescription(obj[6]);
			 list1.add(recentTariffDTO);
		}
		return list1;
		//return getHibernateTemplate().find("from PartnerRateGrid where status = 'Submitted' and publishTariff is true");
	}

	public void updateCustomerFileNumber(Long id, String sequenceNumber, String shipNumber, Long serviceOrderId, String sessionCorpID) {
		getHibernateTemplate().bulkUpdate("update PricingControl set sequenceNumber='"+sequenceNumber+"', shipNumber='"+shipNumber+"',serviceOrderId="+serviceOrderId+" where id="+id+" and corpID='"+sessionCorpID+"'");
		
	}

	public void updateDTHCFlag(Long pricingControlID, String tarrifApplicability, Boolean dthcFlag) {
		Boolean dthc=null;
		if(dthcFlag==true){
			dthc=false;
		}
		if(dthcFlag==false){
			dthc=true;
		}
		getHibernateTemplate().bulkUpdate("update PricingControlDetails set dthcFlag="+dthc+" where pricingControlID="+pricingControlID+" and tarrifApplicability='"+tarrifApplicability+"'");
		getHibernateTemplate().bulkUpdate("update PricingControl set dthcFlag="+dthc+" where id="+pricingControlID+"");
		
	}

	public List getPricingControId(String shipnumber, String sessionCorpID) {
		return getHibernateTemplate().find("select id from PricingControl where serviceOrderId='"+shipnumber+"' and corpID='"+sessionCorpID+"'");
	}

	public List checkControlFlag(String sequenceNumber, String sessionCorpID) {
		return getHibernateTemplate().find("select controlFlag from CustomerFile where sequenceNumber='"+sequenceNumber+"' and corpID='"+sessionCorpID+"'");
	}

	public class RecentTariffDTO{
		
		private Object partnerName;
		private Object tariffApplicability;
		private Object terminalCountry;
		private Object metroCity;
		private Object effectiveDate;
		private Object tariffScope;
		private Object description;
		
		
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getEffectiveDate() {
			return effectiveDate;
		}
		public void setEffectiveDate(Object effectiveDate) {
			this.effectiveDate = effectiveDate;
		}
		public Object getMetroCity() {
			return metroCity;
		}
		public void setMetroCity(Object metroCity) {
			this.metroCity = metroCity;
		}
		public Object getPartnerName() {
			return partnerName;
		}
		public void setPartnerName(Object partnerName) {
			this.partnerName = partnerName;
		}
		public Object getTariffApplicability() {
			return tariffApplicability;
		}
		public void setTariffApplicability(Object tariffApplicability) {
			this.tariffApplicability = tariffApplicability;
		}
		public Object getTariffScope() {
			return tariffScope;
		}
		public void setTariffScope(Object tariffScope) {
			this.tariffScope = tariffScope;
		}
		public Object getTerminalCountry() {
			return terminalCountry;
		}
		public void setTerminalCountry(Object terminalCountry) {
			this.terminalCountry = terminalCountry;
		}
		
		
	}

	public List searchRecentTariffs(String sessionCorpID, String let) {
		List list1 = new ArrayList();
		String sql ="select prg.partnerName, prg.tariffApplicability,prg.terminalCountry,prg.metroCity," + //0,1,2,3
				"prg.effectiveDate,prg.tariffScope, ref.description " + //4,5,6
				"from partnerrategrid prg, refmaster ref where " +
				"prg.terminalCountry=ref.code and ref.parameter='COUNTRY' " +
				"and prg.status = 'Submitted' and prg.publishTariff is true and ref.description like '"+let+"%'";
		
		List recentTariffs= this.getSession().createSQLQuery(sql).list();
		Iterator it=recentTariffs.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			RecentTariffDTO recentTariffDTO= new RecentTariffDTO();
			recentTariffDTO.setPartnerName(obj[0]);
			recentTariffDTO.setTariffApplicability(obj[1]);
			recentTariffDTO.setTerminalCountry(obj[2]);
			recentTariffDTO.setMetroCity(obj[3]);
			recentTariffDTO.setEffectiveDate(obj[4]);
			recentTariffDTO.setTariffScope(obj[5]);
			recentTariffDTO.setDescription(obj[6]);
			 list1.add(recentTariffDTO);
		}
		return list1;
	}

	public boolean checkMarkUp(String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude, String tarrifApplicability, String originCountry, String destinationCountry, String packing, String originPOE, String destinationPOE, String sessionCorpID, Date expectedLoadDate, String contract) {
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedExpectedLoadDate = new StringBuilder(dateformatYYYYMMDD1.format(expectedLoadDate));
		boolean completeMarkUp=true;
		boolean originPartner=false;
		boolean destinationPartner=false;
		String OriginPartnerSql="select p.latitude, p.longitude, p.id, p.lastName, " + //0,1,2,3
		"concat(p.billingAddress1) as address1, " + //4
		"concat(p.billingCity, ', ', p.billingState, ' ', " +
		"p.billingZip) as address2, " +	//5	
		"p.billingPhone, 'www.sscw.com' as partnerUrl, p.location1," + //6,7,8
		"convert(group_concat(concat_ws(',', prgd.gridSection, prgd.label, prgd.basis, prgd.value, prgd.weight1, prgd.weight2) separator '!'), char) as chargelist," + //9
		"prg.serviceRangeMiles,IFNULL(prg.serviceRangeMiles2,'0.00'),IFNULL(prg.serviceRangeMiles3,'0.00'), " +//10,11,12
		"IFNULL(prg.serviceRangeMiles4,'0.00'),IFNULL(prg.serviceRangeMiles5,'0.00'),IFNULL(prg.serviceRangeExtra2,'0.00'), IFNULL(prg.serviceRangeExtra3,'0.00'), " +//13,14,15,16
		"IFNULL(prg.serviceRangeExtra4,'0.00'), IFNULL(prg.serviceRangeExtra5,'0.00'), p.partnerCode , p.fidiNumber, " +//17,18,19,20 TODO: Handle additional columns cprg.serviceRangeMiles2, prg.serviceRangeMiles3.." //10
		"p.OMNINumber , p.qualityCertifications, p.serviceLines, prg.currency ,prg.id as rateGridId " + //21,22,23,24,25
		"  from partner p, partnerrategrid prg, partnerrategriddetails prgd where " +
		"p.partnerCode = prg.partnerCode and prg.id = prgd.partnerRateGridId and " +
		"p.latitude is not null and p.longitude is not null and " +
		"p.isAgent=true and p.billingCountryCode like '"+originCountry+"%' and prg.tariffApplicability = 'Origin'" +
		"and ((prgd.gridSection = 'WEIGHT' and prgd.grid = '"+packing+"' ) or " +
		"(prgd.gridSection = 'CHARGES'  and prgd.grid = '"+packing+"' ) or " +
		"(prgd.gridSection = 'HAULING' and prgd.grid = '"+packing+"' and prgd.label = '"+originPOE+"' ) )" +
		"and prg.effectivedate  = (select max(effectivedate) from partnerrategrid prg2 Where prg2.effectivedate <= '"+formatedExpectedLoadDate+"'  " + //TODO: replace by loading date
		"and prg2.status = 'Submitted' and prg.id=prg2.id and prg2.publishTariff is true) "+
		"and ((if(prgd.basis in ('Included') and prgd.gridSection='CHARGES',convert(prgd.value, DECIMAL) >= 0.00,convert(prgd.value, DECIMAL) > 0.00)) or prgd.gridSection='HAULING') " +
		"and prgd.label not like 'Enter Charge%'" +
		"group by prg.partnercode";
		List allPotentialOriginPartners= this.getSession().createSQLQuery(OriginPartnerSql).list();
		if(!allPotentialOriginPartners.isEmpty()){
			originPartner=true;
		}
		
		String destinationPartnerSql="select p.latitude, p.longitude, p.id, p.lastName, " + //0,1,2,3
		"concat(p.billingAddress1) as address1, " + //4
		"concat(p.billingCity, ', ', p.billingState, ' ', " +
		"p.billingZip) as address2, " +	//5	
		"p.billingPhone, 'www.sscw.com' as partnerUrl, p.location1," + //6,7,8
		"convert(group_concat(concat_ws(',', prgd.gridSection, prgd.label, prgd.basis, prgd.value, prgd.weight1, prgd.weight2) separator '!'), char) as chargelist," + //9
		"prg.serviceRangeMiles,IFNULL(prg.serviceRangeMiles2,'0.00'),IFNULL(prg.serviceRangeMiles3,'0.00'), " +//10,11,12
		"IFNULL(prg.serviceRangeMiles4,'0.00'),IFNULL(prg.serviceRangeMiles5,'0.00'),IFNULL(prg.serviceRangeExtra2,'0.00'), IFNULL(prg.serviceRangeExtra3,'0.00'), " +//13,14,15,16
		"IFNULL(prg.serviceRangeExtra4,'0.00'), IFNULL(prg.serviceRangeExtra5,'0.00'), p.partnerCode , p.fidiNumber, " +//17,18,19,20 TODO: Handle additional columns cprg.serviceRangeMiles2, prg.serviceRangeMiles3.." //10
		"p.OMNINumber , p.qualityCertifications, p.serviceLines, prg.currency ,prg.id as rateGridId " + //21,22,23,24,25
		"  from partner p, partnerrategrid prg, partnerrategriddetails prgd where " +
		"p.partnerCode = prg.partnerCode and prg.id = prgd.partnerRateGridId and " +
		"p.latitude is not null and p.longitude is not null and " +
		"p.isAgent=true and p.billingCountryCode like '"+destinationCountry+"%' and prg.tariffApplicability = 'Origin'" +
		"and ((prgd.gridSection = 'WEIGHT' and prgd.grid = '"+packing+"' ) or " +
		"(prgd.gridSection = 'CHARGES'  and prgd.grid = '"+packing+"' ) or " +
		"(prgd.gridSection = 'HAULING' and prgd.grid = '"+packing+"' and prgd.label = '"+destinationPOE+"' ) )" +
		"and prg.effectivedate  = (select max(effectivedate) from partnerrategrid prg2 Where prg2.effectivedate <= '"+formatedExpectedLoadDate+"'  " + //TODO: replace by loading date
		"and prg2.status = 'Submitted' and prg.id=prg2.id and prg2.publishTariff is true) "+
		"and ((if(prgd.basis in ('Included') and prgd.gridSection='CHARGES',convert(prgd.value, DECIMAL) >= 0.00,convert(prgd.value, DECIMAL) > 0.00)) or prgd.gridSection='HAULING') " +
		"and prgd.label not like 'Enter Charge%'" +
		"group by prg.partnercode";
		List allPotentialDestinationPartners= this.getSession().createSQLQuery(destinationPartnerSql).list();
		if(!allPotentialDestinationPartners.isEmpty()){
			destinationPartner=true;
		}
		if(originPartner==true && destinationPartner==true){
			completeMarkUp=false;
		}else{
			completeMarkUp=true;
		}
		return completeMarkUp;
	}

	public void updateFreightMarkUpFlag(Long pricingControlID, String tarrifApplicability, Boolean freightMarkUpFlag) {
		getHibernateTemplate().bulkUpdate("update PricingControl set freightMarkUpControl="+freightMarkUpFlag+" where id="+pricingControlID+"");
	}

	public void updatePricingControlPorts(Long id, String freightOriginPortCode, String freightDestinationPortCode, String sessionCorpID, String freightOriginPortCodeName, String freightDestinationPortCodeName) {
		getHibernateTemplate().bulkUpdate("update PricingControl set originPOE='"+freightOriginPortCode+"', destinationPOE='"+freightDestinationPortCode+"',originPOEName='"+freightOriginPortCodeName+"',destinationPOEName='"+freightDestinationPortCodeName+"'  where id="+id+" and corpID='"+sessionCorpID+"'");
	}

	public void deletePricingFreightDetails(Long pricingControlId) {
		getHibernateTemplate().bulkUpdate("delete from PricingFreight where pricingControlId="+pricingControlId+"");
		
	}

	public List getRecordList(Long pricingControlId, String sessionCorpID, String tarrifApplicability, String fidi, String omni, Double minimumFeedbackRating, String faim, String iso9002, String iso27001, String iso1400, String rim, String dos, String gsa, String military) {
		String queryTemp = "";
		if(fidi != null && fidi.equalsIgnoreCase("true")){
			queryTemp = "AND fidiNumber = 'Y' ";
		}
		if(minimumFeedbackRating != null && minimumFeedbackRating!=0.00){
			queryTemp += queryTemp.concat("AND feedbackRating >= "+minimumFeedbackRating+" ");
		}
		
		if(omni != null && omni.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND OMNINumber <> '' ");
		}
		if(faim != null && faim.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%FAIM%' ");
		}
		if(iso9002 != null && iso9002.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%ISO 9002%' ");
		}
		if(iso27001 != null && iso27001.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%ISO 27001%' ");
		}
		if(iso1400 != null && iso1400.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%ISO 1400%' ");
		}
		if(rim != null && rim.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND qualityCertifications like  '%RIM%' ");
		}
		/*if(dos != null && dos.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND serviceLines like  '%DOS%' ");
		}
		if(gsa != null && gsa.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND serviceLines like  '%GSA%' ");
		}
		if(military != null && military.equalsIgnoreCase("true")){
			queryTemp += queryTemp.concat("AND serviceLines like  '%Military%' ");
		}*/
		
		
		return getHibernateTemplate().find("from PricingControlDetails where pricingControlID="+pricingControlId+" and tarrifApplicability='"+tarrifApplicability+"' "+ queryTemp +""); 
	}

	public String getPortsPerSection(Long id, String tarrifApplicability) {
		String sql="";
		if(tarrifApplicability.equalsIgnoreCase("Origin"))
		{
			sql = "select distinct pd.originPortcode, p.portName, p.latitude, p.longitude, p.id  as portId from pricingcontroldetails as pd, port as p where pd.originPortCode=p.portCode and pd.pricingControlId="+id+"";
		}
		if(tarrifApplicability.equalsIgnoreCase("Destination"))
		{
			sql = "select distinct pd.destinationPortcode, p.portName, p.latitude, p.longitude, p.id  as portId from pricingcontroldetails as pd, port as p where pd.destinationPortCode=p.portCode and pd.pricingControlId="+id+"";
		}
		
		List allPotentialPorts= this.getSession().createSQLQuery(sql).list();

		StringBuilder ports = new StringBuilder();
		int count = 0;
		Iterator it=allPotentialPorts.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			count++;
				
				if (count > 1) ports.append(","); else ports.append("");
				ports.append("{\"id\":\"" + obj[4] + "\", \"name\": \"" + obj[1] + "\", \"portcode\": \"" + obj[0] + "\", \"latitude\": \"" + obj[2] + "\", \"longitude\": \"" + obj[3] + "\"}");
		
		}
		String prependStr = "{ \"count\":" + count + ", \"ports\":[";
		String appendStr = "]}";
		String s=prependStr + ports.toString() + appendStr;
		return prependStr + ports.toString() + appendStr;
	}

}
