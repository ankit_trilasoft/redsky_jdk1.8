package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.google.gson.Gson;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.PreferredAgent;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.PreferredAgentManager;
import com.trilasoft.app.service.ServiceOrderManager;

public class PreferredAgentAction extends BaseAction implements Preparable {

	private String sessionCorpID;
	private List preferredAgentList;
	PreferredAgentManager  preferredAgentManager;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(PreferredAgentAction.class);
	private String partnerCode;
	private Boolean isIgnoreInactive = false;
	private Long id;
	PreferredAgent preferredAgent;
	private String gotoPageString;
	private Boolean activeCheck;
	private String partnerType;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private String partnerId;
	private PartnerPublic partnerPublic;
	private PartnerPublicManager partnerPublicManager;
	private String statusType;
	private String agentCode;
	private List agentGroupList;
	private String agentGroup;
	private List partnerPublicList;
	private Long preIdNum;
	private Boolean status;
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		agentGroupList = preferredAgentManager.findAgentGroupList(sessionCorpID);
		
	}
	
	public PreferredAgentAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	@SkipValidation
	public String list() {
		try{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		isIgnoreInactive = true;
		activeCheck=true;
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		preferredAgentList=preferredAgentManager.getPreferredAgentListByPartnerCode(sessionCorpID, partnerCode);
		//getPreferredAgentListByPartnerCode(sessionCorpID, partnerCode);
		return SUCCESS;
		}catch(NullPointerException np){
			np.printStackTrace();
			return CANCEL;
		}
		catch(Exception ex){
			ex.printStackTrace();
			return CANCEL;
		}
	}
	
	
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		if (id != null) {
			preferredAgent = preferredAgentManager.get(id);
		} else {
			preferredAgent = new PreferredAgent();
			preferredAgent.setCreatedOn(new Date());
			preferredAgent.setUpdatedOn(new Date());
			preferredAgent.setPartnerCode(partnerCode);
			
		}
		
		preferredAgent.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		boolean isNew = (preferredAgent.getId() == null);
		if (isNew) {
			preferredAgent.setCreatedOn(new Date());
		}
		if(preferredAgent.getPreferredAgentCode()== null || preferredAgent.getPreferredAgentCode().equals("")){
			preferredAgent.setPreferredAgentCode("");
			preferredAgent.setPreferredAgentName("");
			String key = "Agent Code is Not Valid";
			errorMessage(getText(key));
			return SUCCESS;
		}
		if(preferredAgent.getPreferredAgentCode() != null && (!preferredAgent.getPreferredAgentCode().equals(""))){
			if(AgentValid() == null){
				preferredAgent.setPreferredAgentCode("");
				preferredAgent.setPreferredAgentName("");
				String key = "Agent Code is Not Valid, please select again.";
				errorMessage(getText(key));
		        return SUCCESS;
				}
			} 
		
		if(!(preferredAgent.getPreferredAgentCode().toString().trim().equals("")) && (preferredAgent.getPreferredAgentCode()!=null)){
			try{
				List list=	serviceOrderManager.findByBooking(preferredAgent.getPreferredAgentCode(), sessionCorpID);
			if(list.size()>0 && !list.isEmpty()){
				preferredAgent.setPreferredAgentName(list.get(0).toString());
			}
			}catch (NullPointerException nullExc){
				nullExc.printStackTrace();						
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
		if(!(preferredAgent.getPreferredAgentCode().toString().trim().equals("")) && (preferredAgent.getPreferredAgentCode()!=null)&&(preferredAgent.getId()==null)){
		try{
			preferredAgentList=preferredAgentManager.checkForPartnerCodeExists(preferredAgent.getPartnerCode().trim(), sessionCorpID,preferredAgent.getPreferredAgentCode().trim());
		if(preferredAgentList.size()>0 && !preferredAgentList.isEmpty()){
			String key = "Agent Code is Already Exists.";
			errorMessage(getText(key));
	        return SUCCESS;
			}
		}
		catch (NullPointerException nullExc){
			nullExc.printStackTrace();						
		}
		catch (Exception e){
			e.printStackTrace();
		}
		}
		
		preferredAgent.setCorpID(sessionCorpID);
		preferredAgent.setUpdatedOn(new Date());
		preferredAgent.setUpdatedBy(getRequest().getRemoteUser());
		preferredAgent=preferredAgentManager.save(preferredAgent);
		
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "preferredAgent.added" : "preferredAgent.updated";
			saveMessage(getText(key));
		}
		return SUCCESS;
	}
	
	@SkipValidation
    public String AgentValid(){
    	String book=preferredAgent.getPreferredAgentCode();
    List list=	serviceOrderManager.findByBooking(book, sessionCorpID);
        if(! list.isEmpty()){
        	return "valid" ;
        }else{
        	return null;
        }
    	
    }
	
	@SkipValidation
	public String searchAgentDetails() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			if(statusType==null){
				statusType="";
			}
			partnerPublic = partnerPublicManager.get(new Long(partnerId));
			if (preferredAgent != null) {
				boolean agentCode = preferredAgent.getPreferredAgentCode()== null;
				boolean agentName = preferredAgent.getPreferredAgentName()== null;
				getRequest().getSession().setAttribute("partnerCodeSession", preferredAgent.getPartnerCode());
				getRequest().getSession().setAttribute("agentCodeSession", preferredAgent.getPreferredAgentCode());
				getRequest().getSession().setAttribute("agentNameSession", preferredAgent.getPreferredAgentName());
				getRequest().getSession().setAttribute("statusSession", statusType);
				getRequest().getSession().setAttribute("agentGroupSession", agentGroup);
   
				if (!agentCode || !agentName || !activeCheck) {
					preferredAgentList = preferredAgentManager.findPreferredAgentsList(partnerCode, preferredAgent.getPreferredAgentCode(), preferredAgent.getPreferredAgentName(),statusType, sessionCorpID,agentGroup);
				}
			}
			else {
				String agentCode = (String) getRequest().getSession().getAttribute("agentCodeSession");
				String agentName = (String) getRequest().getSession().getAttribute("agentNameSession");
				String statusType = (String) getRequest().getSession().getAttribute("statusSession");
				String agentGroup = (String) getRequest().getSession().getAttribute("agentGroupSession");
				preferredAgentList = preferredAgentManager.findPreferredAgentsList(partnerCode, agentCode, agentName, statusType, sessionCorpID,agentGroup);
			}
		} catch (Exception e) {
			list();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}
	private List SearchforAutocompleteListAgentDetails;
	private String SearchforGsonDataAgentDetails;
	private String autocompleteDivId;
	private String searchName;
	private String fieldType;
	@SkipValidation 
	public String searchAgentDetailsAutocompleteList(){
		try {
			if(searchName.length()>=3 || partnerCode.length()>=3){
				SearchforAutocompleteListAgentDetails = preferredAgentManager.findSearchAgentDetailsAutocompleteList(searchName,sessionCorpID,fieldType,partnerCode);
			}
			
			SearchforGsonDataAgentDetails = new Gson().toJson(SearchforAutocompleteListAgentDetails);
			if(SearchforAutocompleteListAgentDetails.isEmpty() && SearchforAutocompleteListAgentDetails.size()<0){
				SearchforAutocompleteListAgentDetails.add("EmptyValue");
			}
		}
		catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
		return SUCCESS;
	}
	
	public String checkAgentCodes()
	{ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if((agentCode==null)||(agentCode.toString().trim().equalsIgnoreCase(""))){
			agentCode="";
		}
		try{
		  if(agentCode!=null)	{
			  preferredAgentList=preferredAgentManager.findPartnerCode(agentCode, sessionCorpID);
		  }
		  
		}
		catch (Exception e) {
			logger.warn(e);
			preferredAgentList=null;
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	@SkipValidation
	public String checkForPartnerCodeExists(){
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	preferredAgentList=preferredAgentManager.checkForPartnerCodeExists(partnerCode.trim(), sessionCorpID,agentCode.trim());
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS; 
	}
	
	@SkipValidation
	public String saveAgentGroup(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partnerPublicList=preferredAgentManager.getAgentGroupDataList(sessionCorpID,agentGroup);
		if(partnerPublicList.isEmpty()){
			String key = "There is no data for selected Group";
			errorMessage(getText(key));
	        return SUCCESS;
			}
		Iterator itrGroupList = partnerPublicList.iterator();
		 while(itrGroupList.hasNext()){
			 Long groupId = Long.parseLong(itrGroupList.next().toString());
			 PartnerPublic partnerpublic = partnerPublicManager.get(groupId);
		PreferredAgent preferredAgent=new PreferredAgent();
		preferredAgent.setCorpID(sessionCorpID);
		preferredAgent.setCreatedOn(new Date());
		preferredAgent.setCreatedBy(getRequest().getRemoteUser());
		preferredAgent.setUpdatedOn(new Date());
		preferredAgent.setUpdatedBy(getRequest().getRemoteUser());
		preferredAgent.setPartnerCode(partnerCode);
		preferredAgent.setAgentGroup(agentGroup);
		if(!(partnerpublic.getPartnerCode().toString().trim().equals("")) && (partnerpublic.getPartnerCode()!=null)){
			try{
				preferredAgentList=preferredAgentManager.checkForPartnerCodeExists(partnerCode.trim(), sessionCorpID,partnerpublic.getPartnerCode().trim());
				List list=	serviceOrderManager.findByBooking(partnerpublic.getPartnerCode(), sessionCorpID);
			if(preferredAgentList.size()>0 && !preferredAgentList.isEmpty()){
				String key = "Agent Group is Already Added.";
				errorMessage(getText(key));
		        return SUCCESS;
			}
			}catch (NullPointerException nullExc){
				nullExc.printStackTrace();						
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
		preferredAgent.setPreferredAgentCode(partnerpublic.getPartnerCode());
		preferredAgent.setPreferredAgentName(partnerpublic.getLastName());
		preferredAgent.setStatus(true);
		preferredAgent.setId(null);
		preferredAgent=preferredAgentManager.save(preferredAgent);
		}
		return SUCCESS; 
	}
	
	 @SkipValidation
	    public String updateAgentStatus(){
		 preferredAgentManager.updateAgentStatus(preIdNum, status);
	    	return SUCCESS;
	    }

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public List getPreferredAgentList() {
		return preferredAgentList;
	}

	public void setPreferredAgentList(List preferredAgentList) {
		this.preferredAgentList = preferredAgentList;
	}

	public Date getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public PreferredAgentManager getPreferredAgentManager() {
		return preferredAgentManager;
	}

	public void setPreferredAgentManager(PreferredAgentManager preferredAgentManager) {
		this.preferredAgentManager = preferredAgentManager;
	}

	public Boolean getIsIgnoreInactive() {
		return isIgnoreInactive;
	}

	public void setIsIgnoreInactive(Boolean isIgnoreInactive) {
		this.isIgnoreInactive = isIgnoreInactive;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PreferredAgent getPreferredAgent() {
		return preferredAgent;
	}

	public void setPreferredAgent(PreferredAgent preferredAgent) {
		this.preferredAgent = preferredAgent;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public Boolean getActiveCheck() {
		return activeCheck;
	}

	public void setActiveCheck(Boolean activeCheck) {
		this.activeCheck = activeCheck;
	}

	public String getAutocompleteDivId() {
		return autocompleteDivId;
	}

	public void setAutocompleteDivId(String autocompleteDivId) {
		this.autocompleteDivId = autocompleteDivId;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public List getSearchforAutocompleteListAgentDetails() {
		return SearchforAutocompleteListAgentDetails;
	}

	public void setSearchforAutocompleteListAgentDetails(
			List searchforAutocompleteListAgentDetails) {
		SearchforAutocompleteListAgentDetails = searchforAutocompleteListAgentDetails;
	}

	public String getSearchforGsonDataAgentDetails() {
		return SearchforGsonDataAgentDetails;
	}

	public void setSearchforGsonDataAgentDetails(
			String searchforGsonDataAgentDetails) {
		SearchforGsonDataAgentDetails = searchforGsonDataAgentDetails;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public ServiceOrderManager getServiceOrderManager() {
		return serviceOrderManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public PartnerPublicManager getPartnerPublicManager() {
		return partnerPublicManager;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public String getStatusType() {
		return statusType;
	}

	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public List getAgentGroupList() {
		return agentGroupList;
	}

	public void setAgentGroupList(List agentGroupList) {
		this.agentGroupList = agentGroupList;
	}

	public String getAgentGroup() {
		return agentGroup;
	}

	public void setAgentGroup(String agentGroup) {
		this.agentGroup = agentGroup;
	}

	public List getPartnerPublicList() {
		return partnerPublicList;
	}

	public void setPartnerPublicList(List partnerPublicList) {
		this.partnerPublicList = partnerPublicList;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Long getPreIdNum() {
		return preIdNum;
	}

	public void setPreIdNum(Long preIdNum) {
		this.preIdNum = preIdNum;
	}


}
