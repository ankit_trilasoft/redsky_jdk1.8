<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
<title><fmt:message key="myFileList.title"/></title>   
<meta name="heading" content="<fmt:message key='myFileList.heading'/>"/>   
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</c:if>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script>
function clear_fields(){  		    	
	document.forms['myFileForm'].elements['docName'].value = "";
	document.forms['myFileForm'].elements['fileTypes'].value = "";
    document.forms['myFileForm'].elements['docSize'].value = "";
	document.forms['myFileForm'].elements['descriptions'].value ="";
	document.forms['myFileForm'].elements['fileIds'].value ="";
	document.forms['myFileForm'].elements['createdBys'].value ="";
	document.forms['myFileForm'].elements['createdOns'].value ="";
	document.forms['myFileForm'].elements['index'].checked = false;
	document.forms['myFileForm'].elements['duplicates'].checked = false;
}

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure! you want to move selected file in Waste Basket?");
	var did = targetElement;
	var index=document.forms['myFileForm'].elements['index'].checked;
	var duplicates=document.forms['myFileForm'].elements['duplicates'].checked;
	
	var fileTypes = document.forms['myFileForm'].elements['fileTypes'].value;
	var descriptions = document.forms['myFileForm'].elements['descriptions'].value;
	var fileIds = document.forms['myFileForm'].elements['fileIds'].value;
	var createdBys = document.forms['myFileForm'].elements['createdBys'].value;
	var createdOns = document.forms['myFileForm'].elements['createdOns'].value;
	
	if (agree){
		location.href="deleteDocPro.html?id="+did+"&index="+index+"&duplicates="+duplicates+"&fileTypes="+fileTypes+"&descriptions="+descriptions+"&fileIds="+fileIds+"&createdBys="+createdBys+"&createdOns="+createdOns;
	}else{
		return false;
	}
}


function confirmRemoveAllDoc(){
var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select the one or more file to remove.');
		}
	else{
	var agree=confirm("Are you sure! you want to move selected file in Waste Basket?");
	var index=document.forms['myFileForm'].elements['index'].checked;
	var duplicates=document.forms['myFileForm'].elements['duplicates'].checked;
	var fileTypes = document.forms['myFileForm'].elements['fileTypes'].value;
	var descriptions = document.forms['myFileForm'].elements['descriptions'].value;
	var fileIds = document.forms['myFileForm'].elements['fileIds'].value;
	var createdBys = document.forms['myFileForm'].elements['createdBys'].value;
	var createdOns = document.forms['myFileForm'].elements['createdOns'].value;
	
	if (agree){
		location.href="removeAllDocs.html?userCheckConfirm="+checkBoxId+"&index="+index+"&duplicates="+duplicates+"&fileTypes="+fileTypes+"&descriptions="+descriptions+"&fileIds="+fileIds+"&createdBys="+createdBys+"&createdOns="+createdOns;
	}else{
		return false;
	}
	}
}


function checkStatusId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse1;
        http22.send(null);		
} 

function handleHttpResponse1(){
      if (http22.readyState == 4){
           var result= http22.responseText         
      }
}	

function checkStatusAccId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileAccStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse2;
        http22.send(null);
} 
function handleHttpResponse2(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}

function checkStatusPartnerId(rowId, targetElement){
		var Status = targetElement.checked;
		var url="updateMyfilePartnerStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse3;
        http22.send(null);
}
function handleHttpResponse3(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}	


var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

// 1 visible, 0 hidden   
function userStatusCheck(targetElement){
    	if(targetElement.checked){
      		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['myFileForm'].elements['userCheck'].value = targetElement.value;
      		}else{
      			var userCheckStatus=	document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus + ',' + targetElement.value;
      			document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( targetElement.value , '' );
     		document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
   		}
}

function downloadDoc(){
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=DWNLD&seqNo=ADMIN';
			location.href=url;
		}
} 

function strippedDoc(){
	var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
	if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select the one or more document to skipped cover page.');
	}else{
		var agree=confirm("This action will remove the cover page of the document.\n Are you sure you want to strip the cover page.");
		if (agree){
			openWindow("strippedFileOpen.html?rId="+checkBoxId+"&decorator=popup&popup=true",'scrollbars=0,width=650,height=250,left=200,top=200');
			
		}else{
			return false;
		}
	}
}

function emailDoc() {
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=EMAIL';
			location.href=url;
		}
}

function checkAll(){
	document.forms['myFileForm'].elements['userCheck'].value = "";
	document.forms['myFileForm'].elements['userCheck'].value = "";
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = true ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
}

function uncheckAll(){
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = false ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
	document.forms['myFileForm'].elements['userCheck'].value="";
}
function show(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}

</script>

   
<style>

#mainPopup {
padding-left:10px;
padding-right:10px;
}
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-18px;
!margin-top:-18px;
padding:2px 0;
text-align:right;
width:50%;
float:right;
position:relative;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}

</style>
</head>


<s:form id="myFileForm" action="searchMyFilesSize" method="post" >

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="userCheck"/> 
<div id="Layer1" style="width:100%;">
	
 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-4px;"><span></span></div>
    <div class="center-content">
	<table class="table" width="99%">
		<thead>
			<tr>
			    <th>File Number</th>
				<th>Description</th>
				<th>Document Name</th>
				<th>Size(>MB)</th>
				<th>Exclude FileResized</th>
				<th>Uploaded By</th>
				<th>Uploaded On</th>
			</tr>
		</thead>
		<tbody>
			<tr>
			<s:hidden name="fileTypes" ></s:hidden>
			<!--
			    <td><s:select name="fileTypes" cssClass="list-menu" list="%{docsList}" cssStyle="width:120px;" headerKey="" headerValue=""/></td>
			    --><td><s:textfield name="fileIds" required="true" cssClass="text medium" cssStyle="width:100px;"/></td>
				<td><s:textfield name="descriptions" required="true" cssClass="text medium" cssStyle="width:100px;"/></td>
				<td><s:textfield name="docName" required="true" cssClass="text medium" cssStyle="width:100px;"/></td>
				<td><s:select name="docSize" list="{'','2','3','4','5','6','7','8'}" cssStyle="width:40px;" cssClass="list-menu"/></td>
				<td width="150px"><s:checkbox key="index" value="${index}" fieldValue="true" /></td>
				<td><s:textfield name="createdBys" required="true" cssClass="text medium" cssStyle="width:100px;"/></td>
				
				<c:if test="${not empty createdOns}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="createdOns" /></s:text>
				<td align="left" style="width:110px;"><s:textfield cssClass="input-text" id="createdOns" name="createdOns" value="%{customerFiledate1FormattedValue}" required="true" size="6" maxlength="11" onkeydown="return onlyDel(event,this)" readonly="true"/><img id="createdOns_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty createdOns}">
				<td align="left" style="width:110px;"><s:textfield cssClass="input-text" id="createdOns" name="createdOns" required="true" size="6" maxlength="11" readonly="true"/><img id="createdOns_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
				
			</tr>
			<tr>
				<td colspan="5"></td>
				<td colspan="2" style="border-left: hidden;width:150px;" align="right"><s:submit cssClass="cssbutton" method="searchMaxSize" cssStyle="width:60px; height:25px;!margin-bottom: 10px;" key="button.search"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/></td> 
				
			</tr>
		</tbody>
	</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<div id="Layer4" style="width:100%;">
	<div id="newmnav" style="float: left;margin-top:-9px;">
					  <ul>
					    <li id="newmnav1" style="background:#FFF "><a class="current" href="#"><span>Document List</span></a></li>
					   
					    </ul>
					</div>
			<div id="chkAllButton"  class="listwhitetext" style="display:none;margin-top:-5px;width:50%" >
				<!--<input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
				<input type="radio"  name="chk" onClick="uncheckAll()"  /><strong>Uncheck All</strong>
			--></div>	
							
	<div class="spn">&nbsp;</div>
	
</div>


<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			<td>
				<s:set name="myFiles" value="myFiles" scope="request"/>  
				<display:table name="myFiles" class="table" requestURI="" id="myFileList" export="true" defaultsort="6" defaultorder="descending" pagesize="25" style="width:100%" > 
				     
				    <display:column property="fileType" sortable="true" titleKey="myFile.fileType" style="width:120px;"/>
				    <display:column property="mapFolder" sortable="true" title="Map" maxLength="20" style="width:120px;"/>
					<display:column property="description" sortable="true" title="Description" maxLength="20" style="width:120px;"/>
					<display:column  sortable="true" title="File Number"  style="width:120px;" >
					<a  onclick="javascript:openWindow('editCompressFileUpload.html?id=${myFileList.id}&decorator=popup&popup=true',900,600)"  ><c:out value="${myFileList.fileId}" escapeXml="false"/></a>
					</display:column>
				    <display:column titleKey="myFile.fileFileName"  maxLength="20" style="width:140px; cursor: pointer">
				    	<a onclick="javascript:openWindow('ImageServletAction.html?id=${myFileList.id}&decorator=simple&popup=true',500,250);"><c:out value="${myFileList.fileFileName}" escapeXml="false"/></a>
				    </display:column>
				     <display:column property="fileSize" sortable="true" title="Size" style="width:100px;"></display:column>
				     
				    <display:column property="createdBy" sortable="true" title="Uploaded&nbsp;By" />
				    <display:column property="createdOn" sortable="true" title="Uploaded&nbsp;On" format="{0,date,dd-MMM-yyyy}" style="width:85px;"/>
				    
				    <display:setProperty name="paging.banner.item_name" value="document"/>   
				    <display:setProperty name="paging.banner.items_name" value="documents"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Document List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Document List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Document List.pdf"/>   
				</display:table>  
  			</td>
		</tr>
	</tbody>
</table>
</div>  
<!--<c:out value="${buttons}" escapeXml="false" />
<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:70px; font-size: 15" name="dwnldBtn"  value="Download" onclick="downloadDoc();"/>
<input type="button" class="cssbutton1" name="forwardBtn" value="Remove Docs" style="margin-right: 5px;height: 28px;width:85px; font-size: 15" onclick="confirmRemoveAllDoc();" />
--></s:form>
<%-- 
<table>
<tr>
	<td>
		<s:form id="myFileFormRemoveAllDocsForm" action="removeAllDocs" method="post">
			<s:textfield name="userCheck" /> 
			<input type="submit" class="cssbutton1" name="forwardBtn" value="Remove Docs" style="width:55px; height:25px" onclick="setFlagValue();" />
			
			<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
			<s:hidden name="hitFlag" />
			<c:if test="${hitFlag == 1}" >
				<c:redirect url="/docProcess.html"/>
			</c:if>
			
		</s:form>
--%>
<script type="text/javascript"> 
	<c:if test="${hitFlag == 1}" >
		<c:redirect url="/docProcess.html"/>
	</c:if>
    var len = document.forms['myFileForm'].elements['DD'].length;
    if(len>1){
    	show('chkAllButton');
    }
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>

