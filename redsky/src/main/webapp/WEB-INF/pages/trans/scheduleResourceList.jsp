<%@ include file="/common/taglibs.jsp"%> 

<head>
<meta name="heading" content="Schedule Resource"/> 
<title>Schedule Resource</title> 
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script language="javascript" type="text/javascript"> 
  
  function searchScheduleResource(){
	  document.forms['timeSheetList'].elements['tktWareHse'].disabled=false;
	  document.forms['timeSheetList'].elements['flag'].value='';
	  if(document.forms['timeSheetList'].elements['workDt'].value == ""){
	 		alert("Select work date to continue.....");
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
	 		alert("Select Crew WareHouse to continue.....");
	 		return false;
	 	}
	 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
			alert("Select Ticket WareHouse  to continue.....");
	 		targetElement.checked = false;
	 		return false;
	 	}
		if(document.forms['timeSheetList'].elements['truckWareHse'].value == ""){
			alert("Select Truck WareHouse  to continue.....");
	 		targetElement.checked = false;
	 		return false;
	 	}
		document.forms['timeSheetList'].action = 'searchScheduleResouce.html?filter=hidden&checkbtn=A';
 		document.forms['timeSheetList'].submit();
		
  }  
</script>
<style>
form {
margin-top:-40px;
!margin-top:-5px;
}
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}
</style>
</head>

<s:form name="timeSheetList" action="searchScheduleResouce.html?filter=hidden&checkbtn=A" onsubmit="return submit_form()" >

<s:hidden name="requestedWorkTkt" value=""/>
<s:hidden name="requestedCrews" value=""/>
<s:hidden name="requestedTrucks" value=""/>
<s:hidden name="requestedSchedule" value=""/>
<s:hidden name="checkUsed" value=""/>
<s:hidden name="editable" value="${editable}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="serviceGroup" value="${serviceGroup}"/>
<s:hidden name="serviceGroup" value="${serviceGroup}"/>
<s:hidden name="checkbtn" value="<%= request.getParameter("checkbtn")%>"/>
<s:hidden name="flag" value="<%= request.getParameter("flag")%>"/>
<s:hidden name="wareHse" value="${wareHse}"/>
<s:hidden name="truckWareHse" value="${truckWareHse}"/>
<s:hidden name="workticketServiceGroup" value="${workticketServiceGroup}"/>
<s:hidden name="workticketService" value="${workticketService}"/>
<s:hidden name="workticketStatus" value="${workticketStatus}"/>
<div id="otabs" style="margin-top:20px;">		  		  
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-5px;"><span></span></div>
<div class="center-content">
	<table class="table" style="width:100%;clear:both;">
		<thead>
			<tr>
				<th>Work Date</th>
				<th>WareHouse</th>
			</tr>
		</thead>
		<tbody>
			<tr>
			<td align="left"><s:textfield id="date1" name="workDt"  readonly="true" cssStyle="width:85px" maxlength="11" cssClass="input-textUpper" /></td>
			<td><s:select name="tktWareHse" list="%{wareHouse}" cssClass="list-menu" cssStyle="width:100px" disabled="true"/></td>
			</tr>			
		</tbody>
	</table>
	<div style="!margin-top:7px;"></div>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<div class="spnblk">&nbsp;</div>
<div style="width:100%;height:250px;overflow-x:auto;float:left; !margin-top:0px">

<div id="newmnav">
			<ul>             
             <li><a onclick="return searchScheduleResource()"><span>Schedule&nbsp;Resource</span></a></li>
             <li id="newmnav1" style="background:#FFF"><a class="current" ><span>Resource List</span></a></li>               
              </ul>                 
       </div>
	<div class="spn spnSF" style="!margin-bottom:10px;">&nbsp;</div>
	<s:set name="resourceList" value="resourceList" scope="request"/>
	<display:table name="resourceList" class="table" defaultsort="1" requestURI="" id="resourceList" pagesize="50" style="margin-top:-10px;">
	<c:choose>
	<c:when test="${resourceList.availLimit == '0'}">
		<display:column sortable="true" sortProperty="category" style="color:green;width:100px;" title="Category">
							<c:if test="${resourceList.category=='C'}">Crew</c:if>
							<c:if test="${resourceList.category=='E'}">Equipment</c:if>
							<c:if test="${resourceList.category=='M'}">Material</c:if>
							<c:if test="${resourceList.category=='V'}">Vehicle</c:if>
		</display:column>
		<display:column property="resource" sortable="true" sortProperty="resource" style="color:green;width:150px;" maxLength="50" title="Resource"/>			
		<display:column headerClass="containeralign" title="Available&nbsp;Limit" style="color:green;width:55px; text-align: right">
		<c:if test="${resourceList.resource!=''}">
			<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${resourceList.availLimit}" /></div>
		</c:if>
		<c:if test="${resourceList.resource==''}">			
		</c:if>
		</display:column>
		<display:column headerClass="containeralign" title="Assigned&nbsp;Limit" style="color:green;width:55px; text-align: right">
		<c:if test="${resourceList.resource!=''}">
			<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${resourceList.requiredLimit}" /></div>
		</c:if>
		<c:if test="${resourceList.resource==''}">
		</c:if>
		</display:column>
	</c:when>
	<c:when test="${resourceList.availLimit < '0' || resourceList.requiredLimit > resourceList.availLimit}">
		<display:column sortable="true" sortProperty="category" style="color:red;width: 100px;" title="Category">
							<c:if test="${resourceList.category=='C'}">Crew</c:if>
							<c:if test="${resourceList.category=='E'}">Equipment</c:if>
							<c:if test="${resourceList.category=='M'}">Material</c:if>
							<c:if test="${resourceList.category=='V'}">Vehicle</c:if>
		</display:column>
		<display:column property="resource" sortable="true" sortProperty="resource" style="color:red;width:150px;" maxLength="50" title="Resource"/>
		<display:column headerClass="containeralign" title="Available&nbsp;Limit" style="color:red;width:55px; text-align: right">
		<c:if test="${resourceList.resource!=''}">
			<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${resourceList.availLimit}" /></div>
		</c:if>
		<c:if test="${resourceList.resource==''}">			
		</c:if>
		</display:column>
		<display:column headerClass="containeralign" title="Assigned&nbsp;Limit" style="color:red;width:55px; text-align: right">
		<c:if test="${resourceList.resource!=''}">
			<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${resourceList.requiredLimit}" /></div>
		</c:if>
		<c:if test="${resourceList.resource==''}">
		</c:if>
		</display:column>
	</c:when>
	<c:otherwise>
		<display:column sortable="true" sortProperty="category" style="width: 100px;" title="Category">
							<c:if test="${resourceList.category=='C'}">Crew</c:if>
							<c:if test="${resourceList.category=='E'}">Equipment</c:if>
							<c:if test="${resourceList.category=='M'}">Material</c:if>
							<c:if test="${resourceList.category=='V'}">Vehicle</c:if>
		</display:column>
		<display:column property="resource" sortable="true" sortProperty="resource" style="width:150px;" maxLength="50" title="Resource"/>			
		<display:column headerClass="containeralign" title="Available&nbsp;Limit" style="width:55px; text-align: right">
		<c:if test="${resourceList.resource!=''}">
			<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${resourceList.availLimit}" /></div>
		</c:if>
		<c:if test="${resourceList.resource==''}">			
		</c:if>
		</display:column>
		<display:column headerClass="containeralign" title="Assigned&nbsp;Limit" style="width:55px; text-align: right">
		<c:if test="${resourceList.resource!=''}">
			<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${resourceList.requiredLimit}" /></div>
		</c:if>
		<c:if test="${resourceList.resource==''}">
		</c:if>
	</display:column>
	</c:otherwise>
	</c:choose>
	</display:table>
</div>
</s:form>
<script>
</script>

