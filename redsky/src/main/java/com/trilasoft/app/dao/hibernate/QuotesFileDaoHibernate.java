package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.QuotesFileDao;
import com.trilasoft.app.model.QuotesFile;

public class QuotesFileDaoHibernate extends GenericDaoHibernate<QuotesFile, Long> implements QuotesFileDao {   
  
    public QuotesFileDaoHibernate() {   
        super(QuotesFile.class);   
    }   
  
    public List<QuotesFile> findByLastName(String lastName) {   
        return getHibernateTemplate().find("from CustomerFile where lastName=?", lastName);   
    }   
	
}