package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.IntegrationLogDao;
import com.trilasoft.app.model.IntegrationLog;
import com.trilasoft.app.service.IntegrationLogManager;

public class IntegrationLogManagerImpl extends GenericManagerImpl<IntegrationLog, Long> implements IntegrationLogManager { 
	IntegrationLogDao integrationLogDao;
   
	public IntegrationLogManagerImpl(IntegrationLogDao integrationLogDao) { 
        super(integrationLogDao); 
        this.integrationLogDao = integrationLogDao; 
	}
	
	public List<IntegrationLog>  findIntegrationLogs(String fileName, Date processedOn, String recordID, String message){
		return integrationLogDao.findIntegrationLogs(fileName, processedOn, recordID, message); 
	}
	
	public List findServiceOrder(String findServiceOrder, String corpID){
		return integrationLogDao.findServiceOrder(findServiceOrder, corpID); 
	}
	
	public List getAllWithServiceOrderId(String corpId) {
		return integrationLogDao.getAllWithServiceOrderId(corpId);
	}
	
	public List findIntegrationCenterLogsWithServiceOrderId(String fileName, Date date, String recordID, String message, String transId,String intType, String corpId){
		return integrationLogDao.findIntegrationCenterLogsWithServiceOrderId(fileName, date, recordID, message, transId,intType, corpId);
	}
	public List findIntegrationLogsWithServiceOrderId(String fileName, Date date, String recordID, String message, String transId,String intType, String corpId){
		return integrationLogDao.findIntegrationLogsWithServiceOrderId(fileName, date, recordID, message, transId,intType, corpId);
	}
	public List findMemoUpload(String fileName, String recordID, String message, String date, String corpId){
		return integrationLogDao.findMemoUpload(fileName, recordID, message, date, corpId);
	}
	
	public List getWeeklyXML(String corpId){
		return integrationLogDao.getWeeklyXML(corpId);
	}
}