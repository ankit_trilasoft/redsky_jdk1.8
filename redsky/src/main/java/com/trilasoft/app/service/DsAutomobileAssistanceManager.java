package com.trilasoft.app.service;
import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.DsAutomobileAssistance;


public interface DsAutomobileAssistanceManager extends GenericManager<DsAutomobileAssistance, Long>{
	List getListById(Long id);
}


