// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

 
ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `endCustomClearance` DATETIME NULL DEFAULT NULL  AFTER `customClearance` ; 

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `surveyType` VARCHAR(50) NULL DEFAULT NULL  AFTER `currentEmploymentName` ;

ALTER TABLE `redsky`.`company` ADD COLUMN `corporateId` VARCHAR(100) NULL DEFAULT NULL  AFTER `oiJob` ;

ALTER TABLE `redsky`.`company` ADD COLUMN `billToExtractDate` DATETIME NULL  AFTER `corporateId` , ADD COLUMN `vendorExtractDate` DATETIME NULL  AFTER `billToExtractDate` ;

CREATE TABLE `passwordOTP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `otpCode` varchar(25) DEFAULT NULL,
  `corpID` varchar(15) DEFAULT NULL,
  `createdBy` varchar(82) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedBy` varchar(82) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `verificationCodeReason` varchar(25) DEFAULT NULL,
  `verifiedOTPCode` varchar(25) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;

