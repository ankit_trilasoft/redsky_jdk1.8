package com.trilasoft.app.model;
import org.appfuse.model.BaseObject;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

@Entity 
@Table(name="networkpartners")
public class NetworkPartners extends BaseObject {
	private Long id;
	private String agentCorpId;
	private String agentPartnerCode;
	private String agentLocalPartnerCode;
	private String agentCompanyDivision;
	private String agentName;
	private String corpId;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("agentCorpId", agentCorpId)
				.append("agentPartnerCode", agentPartnerCode)
				.append("agentLocalPartnerCode", agentLocalPartnerCode)
				.append("agentCompanyDivision", agentCompanyDivision)
				.append("agentName", agentName).append("corpId", corpId)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof NetworkPartners))
			return false;
		NetworkPartners castOther = (NetworkPartners) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(agentCorpId, castOther.agentCorpId)
				.append(agentPartnerCode, castOther.agentPartnerCode)
				.append(agentLocalPartnerCode, castOther.agentLocalPartnerCode)
				.append(agentCompanyDivision, castOther.agentCompanyDivision)
				.append(agentName, castOther.agentName)
				.append(corpId, castOther.corpId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(agentCorpId)
				.append(agentPartnerCode).append(agentLocalPartnerCode)
				.append(agentCompanyDivision).append(agentName).append(corpId)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).toHashCode();
	}
	@Column(length=10)
	public String getAgentCorpId() {
		return agentCorpId;
	}
	public void setAgentCorpId(String agentCorpId) {
		this.agentCorpId = agentCorpId;
	}
	@Column(length=20)
	public String getAgentLocalPartnerCode() {
		return agentLocalPartnerCode;
	}
	public void setAgentLocalPartnerCode(String agentLocalPartnerCode) {
		this.agentLocalPartnerCode = agentLocalPartnerCode;
	}
	@Column(length=255)
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	@Column(length=20)
	public String getAgentPartnerCode() {
		return agentPartnerCode;
	}
	public void setAgentPartnerCode(String agentPartnerCode) {
		this.agentPartnerCode = agentPartnerCode;
	}
	@Column(length=20)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getAgentCompanyDivision() {
		return agentCompanyDivision;
	}
	public void setAgentCompanyDivision(String agentCompanyDivision) {
		this.agentCompanyDivision = agentCompanyDivision;
	}
	
}