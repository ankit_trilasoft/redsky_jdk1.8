package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DataSecurityFilter;

public interface DataSecurityFilterService{
	public List findById(Long id);
	
	public List getAll();

	public List getFilterData(Long userId, String sessionCorpID);


	public DataSecurityFilter save(DataSecurityFilter dataSecurityFilter);

	public List getUsingId(Long id);

	public String getParentCorpId(String sessionCorpID);

	public List PartnerAccessCorpIdList();

	public List getChildAgentCode(String filterValues, String sessionCorpID,String parentCorpID);

	public Map getChildAgentCodeMap();

	


	
}
