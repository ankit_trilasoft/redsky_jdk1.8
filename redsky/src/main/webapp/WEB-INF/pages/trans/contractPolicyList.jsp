<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="contractPolicyDetail.title"/></title>
<style>

</style>   
    <meta name="heading" content="<fmt:message key='contractPolicyDetail.heading'/>"/>  

</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>

<s:form cssClass="form_magn" id="contractPolicyForm" action="saveContractPolicy" method="post" validate="true"> 

												
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
  <s:hidden name="btnType" value="<%= request.getParameter("btnType") %>" />
  <c:set var="btnType" value="<%= request.getParameter("btnType") %>" />
<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
<s:hidden name="contractPolicy.id" />
<s:hidden name="partner.id" />
<s:hidden name="contractPolicy.partnerCode" value="%{partner.partnerCode}"/>
<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>

<div id="layer4" style="width:100%;">
<div id="newmnav" class="nav_tabs">
				<ul>
					<c:url value="frequentlyAskedList.html" var="url">
					<c:param name="partnerCode" value="${partner.partnerCode}"/>
					<c:param name="partnerType" value="AC"/>
					<c:param name="partnerId" value="${partner.id}"/>
					<c:param name="lastName" value="${partner.lastName}"/>
					<c:param name="status" value="${partner.status}"/>
					</c:url>								  	  
			  	  <c:if test="${empty param.popup}" >
			  	  <c:choose>
					<c:when test="${usertype=='ACCOUNT'}">
					<li><a href="searchCPortalMgmts.html"><span>Cportal Docs</span></a></li>
					</c:when>
					<c:otherwise>
					<li><a href="cPortalResourceMgmts.html"><span>Cportal Docs</span></a></li>
					</c:otherwise>
					</c:choose>	
												  	  
   			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Policy<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    <li><a href="${url}"><span>FAQ</span></a></li>
			  	  </c:if>
			  	</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div> 
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;">												 		  	   	 	               	

<div id="liquid-round-top">
<div class="top"><span></span></div>
  <div class="center-content">
    <table style="margin-bottom:3px;width:100%;">
    <tr>						
						<td colspan="20" width="" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('infoPackage')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Info Package
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					</div>
					  <div id="infoPackage">
											<display:table name="policyFileList" id="policyFileList" class="table" pagesize="10" requestURI="" style="margin-top:5px">
											<display:column style="width:5%" title="#"><a onclick=""><c:out value="${policyFileList_rowNum}"/></a></display:column>
											<display:column property="sectionName" title="Section Name" href="addContractPolicy.html" paramId="id" paramProperty="id"/>
											<display:column property="language" title="Language"/>
											<display:column property="updatedOn"  title="Updated On" format="{0,date,dd-MMM-yyyy}" />
											<display:column property="updatedBy"  title="Updated By"/>										
											<c:if test="${usertype!='ACCOUNT'}">	
											<display:column title="Remove" style="width: 15px;" >
												<a><img align="middle" title="Remove" onclick="confirmSubmit('${policyFileList.id}');"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a> 
											</display:column>
											</c:if>
											</display:table>
											</div>
											</td>
											</tr>
											</table><!--
											
  </table><c:if test="${usertype!='ACCOUNT'}">
											<table width="100%">
											<tr align="right">
											<td align="right"><input type="button" class="cssbutton1" value="Update Child Accounts" style="width:145px; height:25px;" onclick="updateChildFromParent();"/></td><td></td>
											</tr>
											<tr align="right">
											<td align="right" class="listwhitetext">Exclude from Parent updates</td>
											<td class="listwhitetext" width="8px" align="right"><s:checkbox key="partnerPrivate.excludeFromParentPolicyUpdate" id="excludeFPU" value="${isExcludeFromParentPolicyUpdate}" onclick="updatePartnerPrivate();" fieldValue="true"/></td>			 		  	   	 	               	
											</tr>												
											</table> 	</c:if> 
 
-->
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:if test="${usertype!='ACCOUNT'}">
<c:if test="${empty param.popup}" >
	<input type="button" class="cssbutton" style="width:55px; height:25px;margin-left:20px;" onclick="contractPolicyFile();" value="<fmt:message key="button.add"/>"/>
</c:if>
</c:if>
</s:form>

<script>
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this Info Package section?");
	var did = targetElement;
	if (agree){
		window.location='policyFileDeleteAdmin.html?id2='+did;
	}else{
		return false;
	}
}
function contractPolicyFile(){
	 location.href="addContractPolicy.html";	
}
function show(){
	var redirect = document.forms['contractPolicyForm'].elements['btnType'].value;
	if(redirect=='del'){
		location.href = "/editContractPolicy.html?id=${partner.id}&partnerType=AC"
	}
}
</script>

<script type="text/javascript"> 
try{
<c:if test="${param.popup}" >
	for(i=0;i<100;i++){
		document.forms['contractPolicyForm'].elements[i].disabled = true;
	}
	Form.focusFirstElement($("contractPolicyForm"));   
</c:if>  
}
catch(e){}
    <c:if test="${not empty partner.id}">
    <c:if test="${hitFlag=='5'}">
     <c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=AC"/>
    </c:if>
    </c:if>
  
    
</script>  
<script type="text/javascript">
try{	
	 <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	 disabledAll();
	 	</sec-auth:authComponent>
}
catch(e){}
try{
	  <c:if test="${not empty partner.id}">
	    <c:if test="${hitFlag=='7'}">
	     <c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=AC"/>
	    </c:if>
	    </c:if>
}catch(e){}
try{
	<c:if test="${not empty partner.id}">
    <c:if test="${hitFlag=='4'}">
     <c:redirect url="/editContractPolicy.html?id=${partner.id}&partnerType=AC"/>
    </c:if>
    </c:if>
}catch(e){
}
</script>
