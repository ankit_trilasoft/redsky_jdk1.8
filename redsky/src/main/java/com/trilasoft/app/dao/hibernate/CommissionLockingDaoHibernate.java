package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CommissionLockingDao;
import com.trilasoft.app.model.CommissionLocking;

public class CommissionLockingDaoHibernate extends GenericDaoHibernate<CommissionLocking, Long> implements CommissionLockingDao{
public CommissionLockingDaoHibernate(){
super(CommissionLocking.class);	
}
public List getCommissionLineId(String cid,String corpId,String commisionGiven,Long aid){
	return getHibernateTemplate().find("FROM CommissionLocking where commissionGiven='"+commisionGiven+"' and commissionAccountLineId="+aid+" and commissionLineId=?",cid);
}
	public String getCommisionLineId(String corpId,Long aid,String commisionGiven){
		String commissionLineId="";
			List l1 =  this.getSession().createSQLQuery("select sum(commissionAmount) FROM commissionlocking where corpId='"+corpId+"' and commissionGiven='"+commisionGiven+"' and commissionAccountLineId='"+aid+"'").list();
			if((l1!=null) && (!(l1.isEmpty())) && (l1.get(0)!=null)  && (!l1.get(0).toString().equalsIgnoreCase(""))){
				commissionLineId= l1.get(0).toString();
			}	
			return commissionLineId;
		}
	public String getSentToCommisionLineId(String corpId,String aidList,String commisionGiven,Long aid){
		String commissionLineId="";
			List l1 =  this.getSession().createSQLQuery("select sum(commissionAmount) FROM commissionlocking where corpId='"+corpId+"' and commissionAccountLineId="+aid+" and commissionGiven='"+commisionGiven+"' and commissionLineId in ("+aidList+")").list();
			if((l1!=null) && (!(l1.isEmpty())) && (l1.get(0)!=null)  && (!l1.get(0).toString().equalsIgnoreCase(""))){
				commissionLineId= l1.get(0).toString();
			}	
			return commissionLineId;
		}
}
