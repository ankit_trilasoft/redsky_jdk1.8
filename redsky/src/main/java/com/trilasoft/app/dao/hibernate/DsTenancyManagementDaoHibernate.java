package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsTenancyManagementDao;
import com.trilasoft.app.model.DsTenancyManagement;


public class DsTenancyManagementDaoHibernate extends GenericDaoHibernate<DsTenancyManagement, Long> implements DsTenancyManagementDao {

	public DsTenancyManagementDaoHibernate() {
		super(DsTenancyManagement.class);
		
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsTenancyManagement where id = "+id+" ");
	}

}
