package org.appfuse.dao.hibernate;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.SignUpServiceDao;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.SignUpService;
import org.hibernate.FlushMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.trilasoft.app.dao.hibernate.UgwwActionTrackerDaoHibernate.SODashboardDTO;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DspDetails;
import com.trilasoft.app.model.RefMaster;
public class SignUpServiceDaoHibernate extends GenericDaoHibernate<SignUpService,Long> implements  SignUpServiceDao{

	public SignUpServiceDaoHibernate() {
		super(SignUpService.class);
		
	}
	protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    }
	public List customerEmail(String email,String fName,String lName,String target) {
		List list =new ArrayList();
		List result =new ArrayList();
		if(target.equalsIgnoreCase("upwd"))
		{
			list= this.getSession().createSQLQuery("select u.username,account_enabled,account_expired,account_locked,credentials_expired from app_user u where u.userType<>'CUSTOMER' and u.first_name='"+fName+"' and u.last_name='"+lName+"' and u.email='"+email+"'").list();
		}
		else
		{
			list= this.getSession().createSQLQuery("select u.email,account_enabled,account_expired,account_locked,credentials_expired from app_user u where u.userType<>'CUSTOMER'  and u.passwordHintQues='"+fName+"' and u.password_hint='"+lName+"' and u.username='"+email+"'").list();
		}
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			UserInfoDTO userDTO = new UserInfoDTO();
			if (row[0] == null) {
				userDTO.setUserInfo("");
			} else {
				userDTO.setUserInfo(row[0].toString());
			}
			if (row[1] == null) {
				userDTO.setAccount_enabled(new Boolean("false"));
			} else {
				userDTO.setAccount_enabled(((Boolean) row[1]).booleanValue());
			}
			if (row[2] == null) {
				userDTO.setAccount_expired(new Boolean("false"));
			} else {
				userDTO.setAccount_expired(((Boolean) row[2]).booleanValue());
			}
			if (row[3] == null) {
				userDTO.setAccount_locked(new Boolean("false"));
			} else {
				userDTO.setAccount_locked(((Boolean) row[3]).booleanValue());
			}
			if (row[4] == null) {
				userDTO.setCredentials_expired(new Boolean("false"));
			} else {
				userDTO.setCredentials_expired(((Boolean) row[4]).booleanValue());
			}
			result.add(userDTO);
		}
		return result;
	}
	public List customerForgotEmail(String username) {
		List list =new ArrayList();
		List result =new ArrayList();
		list= this.getSession().createSQLQuery("select u.email,account_enabled,account_expired,account_locked,credentials_expired from app_user u where u.userType<>'CUSTOMER'  and u.username='"+username+"'").list();		
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			UserInfoDTO userDTO = new UserInfoDTO();
			if (row[0] == null) {
				userDTO.setUserInfo("");
			} else {
				userDTO.setUserInfo(row[0].toString());
			}
			if (row[1] == null) {
				userDTO.setAccount_enabled(new Boolean("false"));
			} else {
				userDTO.setAccount_enabled(((Boolean) row[1]).booleanValue());
			}
			if (row[2] == null) {
				userDTO.setAccount_expired(new Boolean("false"));
			} else {
				userDTO.setAccount_expired(((Boolean) row[2]).booleanValue());
			}
			if (row[3] == null) {
				userDTO.setAccount_locked(new Boolean("false"));
			} else {
				userDTO.setAccount_locked(((Boolean) row[3]).booleanValue());
			}
			if (row[4] == null) {
				userDTO.setCredentials_expired(new Boolean("false"));
			} else {
				userDTO.setCredentials_expired(((Boolean) row[4]).booleanValue());
			}
			result.add(userDTO);
		}
		return result;
	}
	public  class UserInfoDTO
	 {	
		private String userInfo;
		private Boolean account_enabled;
		private Boolean account_expired;
		private Boolean account_locked;
		private Boolean credentials_expired;
		public String getUserInfo() {
			return userInfo;
		}
		public void setUserInfo(String userInfo) {
			this.userInfo = userInfo;
		}
		public Boolean getAccount_enabled() {
			return account_enabled;
		}
		public void setAccount_enabled(Boolean account_enabled) {
			this.account_enabled = account_enabled;
		}
		public Boolean getAccount_expired() {
			return account_expired;
		}
		public void setAccount_expired(Boolean account_expired) {
			this.account_expired = account_expired;
		}
		public Boolean getAccount_locked() {
			return account_locked;
		}
		public void setAccount_locked(Boolean account_locked) {
			this.account_locked = account_locked;
		}
		public Boolean getCredentials_expired() {
			return credentials_expired;
		}
		public void setCredentials_expired(Boolean credentials_expired) {
			this.credentials_expired = credentials_expired;
		}
		
	 }
	
	public  class DTO
	 {	
		private Object description;
		private Object passwordHintQues;
		private Boolean account_enabled;
		private Boolean account_expired;
		private Boolean account_locked;
		private Boolean credentials_expired;
		/**
		 * @return the description
		 */
		public Object getDescription() {
			return description;
		}
		/**
		 * @param description the description to set
		 */
		public void setDescription(Object description) {
			this.description = description;
		}
		/**
		 * @return the passwordHintQues
		 */
		public Object getPasswordHintQues() {
			return passwordHintQues;
		}
		/**
		 * @param passwordHintQues the passwordHintQues to set
		 */
		public void setPasswordHintQues(Object passwordHintQues) {
			this.passwordHintQues = passwordHintQues;
		}
		public Boolean getAccount_enabled() {
			return account_enabled;
		}
		public void setAccount_enabled(Boolean account_enabled) {
			this.account_enabled = account_enabled;
		}
		public Boolean getAccount_expired() {
			return account_expired;
		}
		public void setAccount_expired(Boolean account_expired) {
			this.account_expired = account_expired;
		}
		public Boolean getAccount_locked() {
			return account_locked;
		}
		public void setAccount_locked(Boolean account_locked) {
			this.account_locked = account_locked;
		}
		public Boolean getCredentials_expired() {
			return credentials_expired;
		}
		public void setCredentials_expired(Boolean credentials_expired) {
			this.credentials_expired = credentials_expired;
		}
	 
	 }
	public void resetPassword(String passwordNew,String confirmPasswordNew,String name){
			getHibernateTemplate().bulkUpdate("Update User set updatedBy='"+getRequest().getRemoteUser()+"', updatedOn = now() ,  password = '"+ passwordNew +"', confirmPassword = '"+ confirmPasswordNew +"' ,passwordReset = true where username=?",name);
	}
	public void resetAccount(String name){
		getHibernateTemplate().bulkUpdate("Update User set updatedBy='"+getRequest().getRemoteUser()+"', updatedOn = now() ,  enabled = false,accountExpired = true, accountLocked = true, credentialsExpired = true where username=?",name);
	}
	public List customerHintQues(String username)
	{
		//return getHibernateTemplate().find("select description from User a, refmaster r where username='"+username+"' and a.passwordHintQues=r.code and userType='CUSTOMER'");
		List usernameHintList=new ArrayList();
		List list= this.getSession().createSQLQuery("select r.description,a.passwordHintQues,a.account_enabled,a.account_expired,a.account_locked,a.credentials_expired from app_user a, refmaster r where a.username='"+username+"'  and a.passwordHintQues=r.code and a.userType<>'CUSTOMER'")
		.addScalar("r.description", Hibernate.TEXT)
		.addScalar("a.passwordHintQues", Hibernate.TEXT)
		.addScalar("a.account_enabled", Hibernate.BOOLEAN)
		.addScalar("a.account_expired", Hibernate.BOOLEAN)
		.addScalar("a.account_locked", Hibernate.BOOLEAN)
		.addScalar("a.credentials_expired", Hibernate.BOOLEAN)
		.list();
		Iterator it=list.iterator();
		  DTO dto=null;
		  while(it.hasNext())
		  {
			  Object [] row=(Object[])it.next();
			  dto = new DTO();
			  dto.setDescription(row[0]);
			  dto.setPasswordHintQues(row[1]);
			  if (row[2] == null) {
				  dto.setAccount_enabled(new Boolean("false"));
				} else {
					//System.out.println("account b"+( row[2]));
					//System.out.println("account enableeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"+((Boolean) row[2]).booleanValue());
					dto.setAccount_enabled(((Boolean) row[2]).booleanValue());
				}
				if (row[3] == null) {
					dto.setAccount_expired(new Boolean("false"));
				} else {
					dto.setAccount_expired(((Boolean) row[3]).booleanValue());
				}
				if (row[4] == null) {
					dto.setAccount_locked(new Boolean("false"));
				} else {
					dto.setAccount_locked(((Boolean) row[4]).booleanValue());
				}
				if (row[5] == null) {
					dto.setCredentials_expired(new Boolean("false"));
				} else {
					dto.setCredentials_expired(((Boolean) row[5]).booleanValue());
				}
			  usernameHintList.add(dto);
	}
		  return usernameHintList;
}
	public List customerCorpID(String username){
		return getHibernateTemplate().find("select corpID from User where username='"+username+"'");
	}
	public List supportEmail(String corpId){
		return getHibernateTemplate().find("from SystemDefault where corpID='"+corpId+"'");
	}
	public List maxAttempt(String corpID){
		return getHibernateTemplate().find("select maxAttempt from SystemDefault where corpID='"+corpID+"'");
	}
	public Map<String, String> findCountry(String parameter) {
		String[] params = {parameter };
		List<RefMaster> list;
		list = getHibernateTemplate().find("from RefMaster where parameter= '" + parameter + "' and language in ('en','',null) ORDER BY description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	}
	private String message=new String ("");
	public String saveUser(User user) {
		message=""; 
		//Session session = null;
	    //Transaction tx = null;
	    //SessionFactory sessionFactory = null;
	    try
	    {
	    	 //getHibernateTemplate().saveOrUpdate(user);
	    	 //session = getHibernateTemplate().getSessionFactory().openSession();
	        //session.setFlushMode(FlushMode.COMMIT);
	        //tx = session.beginTransaction();
	       // this.getSession().save(user);
	       // tx.commit();
	        getHibernateTemplate().saveOrUpdate(user);
	        //System.out.println("User  saved!");
	    }
	    catch (Exception e)
	    {
	    	message="Some data issue in form";
	        try
	        {
	            /*if (tx != null)
	            {
	                tx.rollback();
	            }*/
	        }
	        catch (Exception ignore)
	        {
	            // ignore
	        }
	        
	    }
	    finally
	    {/*
	        if (session != null)
	        {
	            try
	            {
	                session.close();
	            }
	            catch (Exception ignore)
	            {
	                // ignore
	            }
	        }
	        if (sessionFactory != null)
	        {
	            try
	            {
	                sessionFactory.close();
	            }
	            catch (Exception e)
	            {
	                // ignore
	            }
	        }
	    */}
	    return message;
	}
	public List getAgentPopupList(String partnerType, String sessionCorpID, String userCountry, String stateCode, String string2, String string3, String string4) {
		if(stateCode==null)
		{
			stateCode="";
		}
		List partnerList=getHibernateTemplate().find("from PartnerPublic where  corpID ='TSFT' and isAgent is true and status = 'Approved' and terminalCountryCode = '"+userCountry+"' and terminalState like '"+stateCode+"%'");
		return partnerList;
	}
	public List checkUser(String userEmail, String sessionCorpID) {
		return getHibernateTemplate().find("from User where email='"+userEmail+"' and corpID='"+sessionCorpID+"'");
	}
	public List getJobFunction(String parameter) {
		List<RefMaster> list;
		list = getHibernateTemplate().find("select description from RefMaster where parameter= '" + parameter + "' and language='en' ORDER BY description ");
		return list;
	}
	public List getPartner(String partnerCode, String sessionCorpID) {
		return getHibernateTemplate().find("from Partner where partnerCode='"+partnerCode+"' ");
	}
	public Role getRole(String rolename) {
		List roles = getHibernateTemplate().find("from Role where name=?", rolename);
        if (roles.isEmpty()) {
            return null;
        } else {
            return (Role) roles.get(0);
        }
	}
	public List findCustomerPortalListDetails(String oldPortalId) {
		return this.getSession().createSQLQuery("select username from app_user where username like '"+oldPortalId+"%' ").list();
	}
	public List findPortalId(String oldPortalId) {
		return this.getSession().createSQLQuery("select count(*) from app_user where username like '" +oldPortalId+"%'").list();
	}
	public boolean findUser(String portalId) {
		List userList = this.getSession().createSQLQuery("select * from app_user where username='"+portalId+"' ").list();
		boolean usr;
		if((userList != null) || !(userList.isEmpty())){
			usr = true;
		}else{
			usr = false;
		}
		return usr;
	}
	public Map<String, String> findState(String country, String sessionCorpID) {
		if(country == null || country.equals("")){
			country = "NoCountry";
		}
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		List<RefMaster> list = getHibernateTemplate().find("from RefMaster where bucket2 like '"+country+"%' order by description");
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
			}
		return parameterMap;
	}
	public List searchAgents(String partnerCode, String partnerLastName, String partnerTerminalCountryCode, String partnerTerminalCountry, String partnerTerminalState, String sessionCorpID) {
		if(partnerCode==null){partnerCode="";}
		if(partnerLastName==null){partnerLastName="";}
		if(partnerTerminalCountryCode==null){partnerTerminalCountryCode="";}
		if(partnerTerminalCountry==null){partnerTerminalCountry="";}
		if(partnerTerminalState==null){partnerTerminalState="";}
		if(sessionCorpID==null){sessionCorpID="";}
		List partnerList=getHibernateTemplate().find("from PartnerPublic where  corpID ='TSFT' and isAgent is true and status = 'Approved' and terminalCountryCode like '"+partnerTerminalCountryCode+"%' and terminalState like '"+partnerTerminalState+"%' and partnerCode like '"+partnerCode+"%' and lastName like '"+partnerLastName+"%' and terminalCountry like '"+partnerTerminalCountry+"%'");
		return partnerList;
	}
	public CustomerFile getCustomer(Long id) {
		List cust = getHibernateTemplate().find("from CustomerFile where id=?", id);
        if (cust.isEmpty()) {
            return null;
        } else {
            return (CustomerFile) cust.get(0);
        }
	}
	
	public AccessInfo getAccessinfo(Long id) {
		List acces = getHibernateTemplate().find("from AccessInfo where id=?", id);
        if (acces.isEmpty()) {
            return null;
        } else {
            return (AccessInfo) acces.get(0);
        }
	}
	public String serviceStateList(String sessionCorpID){
		String countryList="";
		List al= this.getSession().createSQLQuery("select GROUP_CONCAT(code) from refmaster where  parameter='COUNTRY' and state='Y' and corpID in ('TSFT','"+sessionCorpID+"')").list();
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null))
		{
			countryList=al.get(0).toString();
		}
		return countryList;
	}
	public List findStateService(String country, String sessionCorpID){
		if(country == null || country.equals("")){
			country = "NoCountry";
		}
		List<RefMaster> list = getHibernateTemplate().find("select CONCAT(code,'#',description) from RefMaster where  bucket2=? order by description", country);
		
		return list;
	}
		public List findUserDetails(String userName){
			return getHibernateTemplate().find("from User where username=?",userName);
		}
		public List updateSoCustomerFeedback(String soId,String rtNumber,String customerFeedback,String services,String surveyJob,String sessionCorpID){
		List soDetails=new ArrayList();	
		 Date date = new Date();		
		 SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date));
		String Dateto = nowYYYYMMDD1.toString();
		if(surveyJob.equalsIgnoreCase("RLO")){
		/*List sofeedBack=this.getSession().createSQLQuery("select feedBack from serviceorder where id='"+soId+"' and corpID='"+sessionCorpID+"' ").list();
		String feedBackFromSo="";
		  if(sofeedBack!=null && !sofeedBack.isEmpty() && sofeedBack.get(0)!=null && !sofeedBack.get(0).toString().equals("") ){
			  feedBackFromSo=sofeedBack.get(0).toString();
			  feedBackFromSo=feedBackFromSo+"Service:"+services;
			}else{
			 feedBackFromSo="Service:"+services;
			}
	      getSession().createSQLQuery("update serviceorder set customerRating='"+rtNumber+"',feedBack='"+feedBackFromSo.replaceAll("'", "\\\\'").replaceAll(":", "-")+"' where id='"+soId+"' and corpID='"+sessionCorpID+"'").executeUpdate();*/
		}else{
			getSession().createSQLQuery("update serviceorder set customerRating='"+rtNumber+"',feedBack='"+customerFeedback.replaceAll("'", "\\\\'").replaceAll(":", "-")+"' where id='"+soId+"' and corpID='"+sessionCorpID+"'").executeUpdate();
		}		
		soDetails=this.getSession().createSQLQuery("select firstName,lastName,coordinator,salesMan,shipNumber,sequenceNumber from serviceorder where id='"+soId+"' and corpID='"+sessionCorpID+"' ").list();
		return soDetails;
		}
		public List getReloDetailsList(String sessionCorpID){
			List serviceList=this.getSession().createSQLQuery("select distinct code ,description  from refmaster where flex1='Relo' and flex3='SURVEYEMAIL' and parameter='SERVICE' and corpID in ('"+sessionCorpID+"','TSFT') ").list();
			return serviceList;
		}
		public List getUserEmail(String rtNumber,String sessionCorpID){
			List eMailDetails=new ArrayList();
			if(rtNumber.equalsIgnoreCase("10")){
				eMailDetails=this.getSession().createSQLQuery("select email from app_user where corpID='"+sessionCorpID+"' and userType='USER'").list();	
				}else{
			   eMailDetails=this.getSession().createSQLQuery("select email from app_user a , role r , user_role ur where ur.user_id=a.id and ur.role_id=r.id and a.corpID='"+sessionCorpID+"' and r.name='ROLE_EXECUTIVE'").list();
	    	}
			return eMailDetails;
		}
		public List getCrewName(String shipNumber,String sessionCorpID){
			List crewNameDetails=new ArrayList();			
			List ticketList=this.getSession().createSQLQuery("select ticket from workticket where shipNumber='"+shipNumber+"' and corpID='"+sessionCorpID+"'").list();
			for(Object object : ticketList) {
			List ticketCrewName=this.getSession().createSQLQuery("select concat(crewName,'#',if(crewType is null or crewType='','A',crewType)) from timesheet where ticket='"+object.toString()+"' and corpID='"+sessionCorpID+"'").list();
			for (Object object2 : ticketCrewName) {
			crewNameDetails.add(object2.toString());
			}
			}
			return crewNameDetails;
		}
		public List getUserDetails(String userName,String sessionCorpID){
			List userDetails=new ArrayList();
			userDetails=this.getSession().createSQLQuery("select concat(first_name,'#',last_name) from app_user where corpID='"+sessionCorpID+"' and username='"+userName+"'").list();
			return userDetails;
		}
		public List getSurveyEmail(String sessionCorpID){
			List companyDetails=new ArrayList();
			companyDetails=this.getSession().createSQLQuery("select customerSurveyEmail from systemdefault where corpId='"+sessionCorpID+"'").list();
			return companyDetails;
		}
		public List checkCustomerFeedback(String soId,String sessionCorpID){
		List	soDetails=this.getSession().createSQLQuery("select feedBack from serviceorder where id='"+soId+"' and corpID='"+sessionCorpID+"' and customerRating !='' and customerRating is not null ").list();
		return soDetails;
		}
		public List checkRLOCustomerFeedback(String soId,String sessionCorpID){
			List customerSurvey=this.getSession().createSQLQuery("select serviceName from  customersurveyfeedback where  serviceOrderId='"+soId+"' and corpId='"+sessionCorpID+"' ").list();
			return customerSurvey;
			}
		public String getDspDetalsData(String services,String dspDetailsValue,String soId,String sessionCorpID){		
			String collectDspDetails="";
			Class noparams[] = {};
			List<DspDetails> list;
			list = getHibernateTemplate().find("from DspDetails where id='"+soId+"'");
			for (DspDetails dspDetails : list) {
				Class cls=null;
				try {
					cls = Class.forName("com.trilasoft.app.model.DspDetails");
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			String serviceTypeDetails[]=dspDetailsValue.split(",");	 
			for (String serviceTemp: serviceTypeDetails) {
				Method method=null;
				Object obj1=null;
    			try {
				method = cls.getDeclaredMethod( "get"+serviceTemp.trim()+"".trim(), noparams);
				 obj1 = method.invoke(dspDetails, null);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
				if(obj1==null){
	    			break;
	    			}else{
	    				if(collectDspDetails.contains(obj1.toString())){
	    					
	    				}else{
	    					if(collectDspDetails.equals("")){
	    		    			collectDspDetails=obj1+"";
	    		    		   }else{
	    		    		   collectDspDetails=collectDspDetails+","+obj1+"";
	    		    		  }	
	    				}
	    		
	    			}
			}
			
			}
	
			return collectDspDetails;
		}
		public void getNPSCalculation(String venderCodeValue,String vendorEmailValue,String soId,String sessionCorpID){
			int venderLine=0;
			int vendorEmail=0;
			int detractors=0;
			int passive=0;
			int promoters=0;
			
			List venderRating=this.getSession().createSQLQuery("select if(customerRating in (0,1,2,3,4,5,6),'A',if(customerRating in(7,8),'B',if(customerRating in(9,10),'C',''))) from  customersurveyfeedback where  vendorCode='"+venderCodeValue+"' and corpId='"+sessionCorpID+"' ").list();
			if(venderRating!=null && !venderRating.isEmpty()){
				venderLine=venderRating.size();
				//String venderRatingDo=venderRating.get(0).toString();
				//String venderRatingKey[]=venderRatingDo.
				Iterator it = venderRating.iterator();
				while (it.hasNext()) {
				String  row = (String) it.next();
				if(row!=null && row.toString().equalsIgnoreCase("A")){
					detractors++;
				}else if(row!=null && row.toString().equalsIgnoreCase("B")){
					passive++;
				}else if(row!=null && row.toString().equalsIgnoreCase("C")){
					promoters++;
				}				
				}
				float detractorsCorrect = ((float) detractors) / ((float) venderLine);
				  detractorsCorrect=detractorsCorrect*100;
				float promotersCorrect = ((float) promoters) / ((float) venderLine);
				  promotersCorrect=promotersCorrect*100;
				float NPSScore= promotersCorrect-detractorsCorrect;
				
				getSession().createSQLQuery("update partnerprivate set NPSscore='"+NPSScore+"' where partnerCode='"+venderCodeValue+"' and corpID='"+sessionCorpID+"'").executeUpdate();
			}
			int detractors1=0;
			int passive1=0;
			int promoters1=0;
			
			List vendorEmailRating=this.getSession().createSQLQuery("select if(customerRating in (0,1,2,3,4,5,6),'A',if(customerRating in(7,8),'B',if(customerRating in(9,10),'C','')))  from  customersurveyfeedback where  vendorContact='"+vendorEmailValue+"' and corpId='"+sessionCorpID+"' ").list();
			if(vendorEmailRating!=null && !vendorEmailRating.isEmpty()){
				vendorEmail=vendorEmailRating.size();
				Iterator it1 = vendorEmailRating.iterator();
				while (it1.hasNext()) {
					String  row1 = (String) it1.next();
				if(row1!=null && row1.toString().equalsIgnoreCase("A")){
					detractors1++;
				}else if(row1!=null && row1.toString().equalsIgnoreCase("B")){
					passive1++;
				}else if(row1!=null && row1.toString().equalsIgnoreCase("C")){
					promoters1++;
				}
				}
				float detractorsCorrect1 = ((float) detractors1) / ((float) vendorEmail);
				  detractorsCorrect1=detractorsCorrect1*100;
				float promotersCorrect1 = ((float) promoters1) / ((float) vendorEmail);
				  promotersCorrect1=promotersCorrect1*100;
				float NPSScore1= promotersCorrect1-detractorsCorrect1;
				
				getSession().createSQLQuery("update app_user set NPSscore='"+NPSScore1+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where basedAt='"+venderCodeValue+"' and username='"+vendorEmailValue+"'").executeUpdate();
			}
			
		}
		public String getVenderDspDetails(String services,String soId,String shipNumber,String sessionCorpID){
			String venderDsp="";
			String venderCode="";
			String venderContact="";
			if(services!=null && !services.equals("")){
				if(services.equals("SCH")){
					venderCode=services+"_schoolSelected";
					venderContact=services+"_website";
				}else{
					venderCode=services+"_vendorCode";
					venderContact=services+"_vendorEmail";
				}
				//venderCode=services+"_";
				String Query="select concat("+venderCode+",'~',if( "+venderContact+" is null or "+venderContact+"='','A',"+venderContact+")) from dspdetails where serviceOrderId='"+soId+"' and shipNumber='"+shipNumber+"' and corpID='"+sessionCorpID+"'";	
				List companyDetails=this.getSession().createSQLQuery(Query).list();
				if(companyDetails!=null && !companyDetails.isEmpty()){
					venderDsp=companyDetails.get(0).toString();
				}
			}
			return venderDsp;
		}
		
		public List getPostMoveOrderDetails(String id,String sessionCorpID){
			return this.getSession().createSQLQuery("select concat(s.firstName,' ',s.lastName),s.coordinator,s.shipnumber,t.originAgent,t.destinationagent,date_format(now(),'%M %d, %Y') as EvaluationDate,s.emailRecvd from serviceorder s,trackingstatus t where s.id=t.id and s.corpId=t.corpId and s.corpid='"+sessionCorpID+"' and t.corpid='"+sessionCorpID+"' and s.id='"+id+"' ").list();
		}
		
		public void updateEmailReveived(String id,String contactPhone,String contactEmail,String sessionCorpID,String response){
			if(sessionCorpID.equals("HOLL")){
				getSession().createSQLQuery("update serviceorder set emailRecvd=now(),clientOverallResponse='"+response+"',feedBack='"+contactEmail+"' where id='"+id+"' ").executeUpdate();
			}
			else if(sessionCorpID.equals("STVF")){
				String evaluation[] = contactPhone.split("~");
				getSession().createSQLQuery("update serviceorder set emailRecvd=now(),clientOverallResponse='"+response+"',OAEvaluation='"+evaluation[0].toString()+"',DAEvaluation='"+evaluation[1].toString()+"',feedBack='"+contactEmail+"' where id='"+id+"' ").executeUpdate();
			}
			else{
			if(contactPhone!=null && !contactPhone.equals("") && contactEmail!=null && !contactEmail.equals("")){
				getSession().createSQLQuery("update serviceorder set emailRecvd=now(),additionalAssistance=true,contactPhone='"+contactPhone+"',destinationContactEmail='"+contactEmail+"' where id='"+id+"' ").executeUpdate();
			}else 
				{
				getSession().createSQLQuery("update serviceorder set emailRecvd=now() where id='"+id+"' ").executeUpdate();
				}
		}
		}
}
