package com.trilasoft.app.dao.hibernate;

import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.DsOngoingSupportDao;
import com.trilasoft.app.model.DsOngoingSupport;


public class DsOngoingSupportDaoHibernate extends GenericDaoHibernate<DsOngoingSupport, Long> implements DsOngoingSupportDao {

	public DsOngoingSupportDaoHibernate() {
		super(DsOngoingSupport.class);
		
	}
	public List getListById(Long id) {
		return getHibernateTemplate().find("from DsOngoingSupport where id = "+id+" ");
	}

}