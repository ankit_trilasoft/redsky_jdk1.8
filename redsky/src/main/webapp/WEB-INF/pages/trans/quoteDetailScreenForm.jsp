<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Quote Details</title>
<meta name="heading" content="Quote Details" />
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
<style type="text/css">

</style>
<script language="javascript" type="text/javascript">
function checkField(){
 
	if(document.forms['QuoteDetailScreenForm'].elements['customerInformation.quoteId'].value == ''){    	
		alert('Please fill the Quote Id.');
		return false;
	}
}

</script>
</head>
<body>
<s:form id="QuoteDetailScreenForm" name="QuoteDetailScreenForm" action="saveQuoteDetails.html" method="post" validate="true">
	<div id="Layer1" style="width:100%;">
	<div id="newmnav">
     	<ul>
     		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Quote Details <img src="images/navarrow.gif" align="absmiddle" /></span></a></li> 
     		<li><a href="quoteDetails.html"><span>Quote Details List</span></a></li>      
      </ul>
  </div>
  <div class="spn">&nbsp;</div>
  <div id="content" align="center" style="!margin-top:4px;">
   <div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;">
	<tbody>
			<tr>
				<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">	     
				<tbody>	
						<tr>
							<td align="right" class="listwhitetext">First Name</td>
                            <td class="listwhitetext"><s:textfield name="customerInformation.firstName" maxlength="25" readonly="true" cssClass="input-textUpper" cssStyle="width:100px" tabindex="1"/></td>
							<td align="right" width="10px"></td>
							<td align="right" class="listwhitetext">Last Name</td>
                            <td class="listwhitetext"><s:textfield name="customerInformation.lastName" maxlength="25" readonly="true" cssClass="input-textUpper" cssStyle="width:100px" tabindex="2"/></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext">Origin Country</td>
                            <td class="listwhitetext"><s:textfield name="secorQuote.originCountry" maxlength="25" readonly="true" cssClass="input-textUpper" cssStyle="width:100px" tabindex="3"/></td>
							<td align="right" width="10px"></td>
							<td align="right" class="listwhitetext">Origin City</td>
                            <td class="listwhitetext"><s:textfield name="secorQuote.originCity" maxlength="25" readonly="true" cssClass="input-textUpper" cssStyle="width:100px" tabindex="4"/></td>
						    <td align="right" width="10px"></td>
							<td align="right" class="listwhitetext">Origin Zip</td>
                            <td class="listwhitetext"><s:textfield name="secorQuote.originZip" maxlength="25" readonly="true" cssClass="input-textUpper" cssStyle="width:100px;text-align:right" tabindex="5"/></td>
						</tr> 
						
						<tr>
							<td align="right" class="listwhitetext">Destination Country</td>
                            <td class="listwhitetext"><s:textfield name="secorQuote.destinationCountry" maxlength="25" readonly="true" cssClass="input-textUpper" cssStyle="width:100px" tabindex="6"/></td>
							<td align="right" width="10px"></td>
							<td align="right" class="listwhitetext">Destination City</td>
                            <td class="listwhitetext"><s:textfield name="secorQuote.destinationCity" maxlength="25" readonly="true" cssClass="input-textUpper" cssStyle="width:100px" tabindex="7"/></td>
						    <td align="right" width="10px"></td>
							<td align="right" class="listwhitetext">Destination Zip</td>
                            <td class="listwhitetext"><s:textfield name="secorQuote.destinationZip" maxlength="25" readonly="true" cssClass="input-textUpper" cssStyle="width:100px;text-align:right" tabindex="8"/></td>
						</tr>  
						<tr>
                            <td align="right" class="listwhitetext">Mode</td>
                            <td class="listwhitetext"><s:textfield name="quoteRate.mode" maxlength="25" readonly="true" cssClass="input-textUpper" cssStyle="width:100px" tabindex="9"/></td>
						    <td align="right" width="10px"></td>	
				            <td align="right" class="listwhitetext">Total Price</td>
                            <td class="listwhitetext"><s:textfield name="quoteRate.totalPrice" maxlength="10" readonly="true" cssClass="input-textUpper" cssStyle="width:100px;text-align:right" tabindex="13"/></td>					
				       </tr>              
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
 </div>
 
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>
</div>
<div id="Layer5" style="width:100%;">
	
	<div id="newmnav">
		  <ul>
		     <li id="newmnav1" style="background:#FFF "><a class="current"><span>Quote Item List</span></a></li>
		    
		  </ul>
	</div>
	<div class="spn">&nbsp;</div>
	<s:set name="quoteItemList" value="quoteItemList" scope="request" />
	<display:table name="quoteItemList" class="table" requestURI="" id="quoteItemList" pagesize="10" style="margin-top:2px;!margin-top:7px;">
	<display:column property="name" sortable="true" title="Name" maxLength="15" style=""/>
	<display:column property="description" sortable="true" title="Description" maxLength="15" style=""/>
	<display:column property="weight" sortable="true" title="Weight" maxLength="15" style="width:70px;text-align:right"/>
	<display:column property="actualWeight" sortable="true" title="Act.Weight" maxLength="15" style="width:20px;text-align:right"/>
	<display:column property="volume" sortable="true" title="Volume" maxLength="15" style="width:70px;text-align:right"/>
	<display:column property="quantity" sortable="true" title="Quantity" maxLength="15" style="width:70px;text-align:right"/>
	<display:column property="noOfItems" sortable="true" title="No Of Items" maxLength="15" style="width:80px;text-align:right"/>	
	<display:column property="createdOn" sortable="true" title="Created On" maxLength="15" style="width:60px;" format="{0,date,dd-MMM-yyyy}"/>
	<display:footer>
      <tr> 
 	  	 <td align="right" colspan="3"><b><div align="right">Total:</div></b></td>
		 <td align="right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
             groupingUsed="true" value="${totalActualWeight }" /></div></td>
 	  	 <td align="right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${totalVolume}" /></div></td>
         <td></td>
		 <td align="right"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${totalCount}" /></div></td>	 
         <td></td> 	    
      </tr>
    </display:footer>
	<display:setProperty name="export.excel.filename" value="QuoteDetailsList.xls"/>
	<display:setProperty name="export.csv.filename" value="QuoteDetailsList.csv"/>
	</display:table>
</div>
             
</s:form>
</body>