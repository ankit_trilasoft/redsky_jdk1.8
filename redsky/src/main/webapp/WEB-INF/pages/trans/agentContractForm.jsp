<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="agentList.title"/></title>   
    <meta name="heading" content="<fmt:message key='agentList.heading'/>"/> 
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
</head>
<s:form id="agentContractForm" name="agentcontractForm"  method="post" validate="true">
<div id="Layer1" style="width:85%;">
<c:set var="contractid" value="<%=request.getParameter("contractid")%>" scope="session"/>
<s:hidden name="contractAccount.id" />
<s:hidden name="contractAccount.published" />
<s:hidden name="agentTypeValue"/>
<s:hidden name="dummyAgentCode" />
<s:hidden name="contactAgentType" value="${contactAgentType}"/>
<s:hidden name="contractAccount.agentType" value="${contactAgentType}"/>

	<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" />
	<s:hidden name="eigthDescription" />
	<s:hidden name="ninthDescription" />
	<s:hidden name="tenthDescription" />


<div id="newmnav">
     	<ul>
           <li id="newmnav1" style="background:#FFF "><a class="current"><span>Contract Agent Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="agentContractLists.html?id=${contractid}"><span>Contract Agent List</span></a></li>
		  </ul>
	</div>
	<div class="spn" style="!margin-bottom:2px;">&nbsp;</div>
	<div id="content" align="center" >
<div id="liquid-round" style="width: 900px !important">
   <div class="top"><span></span></div>
   <div class="center-content">
 	<table class="detailTabLabel" border="0" style="margin:0px;padding:0px;">
		  <tr>
		  	<td align="right" class="listwhitetext" width="70">Agent Code<font color="red" size="2">*</font></td>
		  	<td align="left" width="83"><s:textfield name="contractAccount.accountCode"   size="6"   cssClass="input-text" readonly="false" onchange="return contractAccounts('fromField'),valid(this,'special');"/>
		  	<img class="openpopup" width="17" height="20" style="vertical-align:top;"
			onclick="javascript:openWindow('contractAgentPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=contractAccount.agentCorpID&fld_description=contractAccount.accountName&fld_thirdDescription=thirdDescription&fld_code=contractAccount.accountCode');"
			src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td align="right" width="70" class="listwhitetext">Agent Name</td>
		  	<td align="left" width="200"><s:textfield name="contractAccount.accountName" size="50"   cssClass="input-textUpper" readonly="true"/></td>
		   <td align="right" class="listwhitetext" width="50">Corp ID</td>
		  	<td align="left"><s:textfield name="contractAccount.agentCorpID" size="5"   cssClass="input-textUpper" readonly="true"/></td>
		  
		   </tr>
		     <tr>
		  <td align="right" class="listwhitetext"></td>		 
		  </tr>
		  <tr>
		  <td align="right" class="listwhitetext" height="5"></td>		 
		  </tr>
		  
		  <tr>
		  <td colspan="10">
		  <table class="detailTabLabel" border="0" style="margin:0px;padding:0px;">		  
		  <tr>
		  <c:if test="${contactAgentType=='CMM'}">
		  <td>		   
		   <fieldset style="padding:2px 8px 5px;width:510px;">
		   	 <legend>CMM Fee</legend>
			<table border="0" class="detailTabLabel" style="width:500px;margin:0px;padding:0px;">
		  <tr >
		  	<td align="right" class="listwhitetext">Threshold</td>
		  <td align="left"><s:textfield name="contractAccount.cmmFeeThreshold" cssClass="input-text" cssStyle="text-align:right" size="5" onchange="onlyFloat(this);" /></td>
		  	<td align="right" class="listwhitetext">Percentage</td>
		  <td align="left"><s:textfield name="contractAccount.cmmFeePCT" cssClass="input-text" size="5" cssStyle="text-align:right" onchange="onlyFloat(this);"/></td>
		  	<td align="right" class="listwhitetext">Minimum</td>
		  <td align="left"><s:textfield name="contractAccount.cmmFeeMin" cssClass="input-text" size="5" cssStyle="text-align:right" onchange="onlyFloat(this);" /></td>
		  	<td align="right" class="listwhitetext">Maximum</td>
		  <td align="left"><s:textfield name="contractAccount.cmmFeeMax" cssClass="input-text" size="5" cssStyle="text-align:right" onchange="onlyFloat(this);"/></td>
		  </tr>
		  </table>
		   </fieldset>
		   
		     </td></c:if>
		 <c:if test="${contactAgentType=='CMM'}">
		  <td>
		  <fieldset style="padding:2px 8px 5px;width:290px;">
		   	 <legend>VAT Setup</legend> 
			<table border="0" class="detailTabLabel" style="width:280px;margin:0px;padding:0px;">
			<tr>
			<td align="right" class="listwhitetext">Vat&nbsp;Desc </td>
		   	 <td><s:select  cssClass="list-menu"  key="contractAccount.vatDesc" id="contractAccount.vatDesc" cssStyle="width:100px" list="%{euVatList}" headerKey="" onchange="getVatPercent();" headerValue="" tabindex="18"  /></td>
			<td width="80px" align="right" class="listwhitetext" >VAT %</td>
            <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="contractAccount.vatPercent" id="contractAccount.vatPercent" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)" readonly="true" onchange="return checkFloat(this);" maxLength="6" /></td>
			</tr>
			</table>
		  </td>
		  </c:if>
		  </tr>
		  </table>
		  </td>
		  </tr>		  
		   <tr>
		  <td align="right" class="listwhitetext" height="10"></td>
		  </tr>
		  </table>	  
		  </div>		  
		<div class="bottom-header" style="!margin-top:45px;"><span></span></div>
		  </div>
		</div>
		</div>	
		
		<table>
			<tbody>
                <tr>
                <td align="left" class="listwhitetext" width="30px"></td>
					<td colspan="5"></td>
				</tr>
					<tr>
						<td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
						<td align="left" class="listwhitetext" style="width:60px"><b><fmt:message key='contractAccount.createdOn'/></b></td>							
						<td valign="top"></td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${contractAccount.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="contractAccount.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${contractAccount.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:65px"><b><fmt:message key='contractAccount.createdBy' /></b></td>
							<c:if test="${not empty contractAccount.id}">
								<s:hidden name="contractAccount.createdBy"/>
								<td style="width:100px"><s:label name="createdBy" value="%{contractAccount.createdBy}"/></td>
							</c:if>
							<c:if test="${empty contractAccount.id}">
								<s:hidden name="contractAccount.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:65px"><b><fmt:message key='contractAccount.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${contractAccount.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="contractAccount.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${contractAccount.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:65px"><b><fmt:message key='contractAccount.updatedBy' /></b></td>
							<c:if test="${not empty contractAccount.id}">
								<s:hidden name="contractAccount.updatedBy"/>
								<td style="width:100px"><s:label name="updatedBy" value="%{contractAccount.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty contractAccount.id}">
								<s:hidden name="contractAccount.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
						</c:if>
					</tr>
				</tbody>
			</table>
							<input type="button" class="cssbutton" style="width:65px;"  value="Save" onclick="return checkCode();"/> 
             	<s:reset type="button" cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-left:10px;" key="Reset" />
	     

	 <c:if test="${flag =='true'}" >
		<c:redirect url="/agentContractLists.html?id=${contractid}"  />
	</c:if>
	</s:form>

<%-- Script Shifted from Top to Botton on 10-Sep-2012 By Kunal --%>
<script language="JavaScript">
function savePage(){
	 document.forms['agentcontractForm'].action ="saveAgentContractForm.html?contractid="+${contractid}+"";
	 document.forms['agentcontractForm'].submit();
	 }
function checkCode(){
	 if(document.forms['agentcontractForm'].elements['contractAccount.accountCode'].value==""){
	   alert("Please fill the value in the Agent Code");
	   return false;
	 }
	 if(document.forms['agentcontractForm'].elements['contractAccount.accountCode'].value.search("'")!=-1){
	   alert("Please fill the valid value in the Agent Code");
	   document.forms['agentcontractForm'].elements['contractAccount.accountName'].value='';
	   return false;
	 }
	 contractAccounts('save');
	}
	
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var http2 = getHTTPObject();
function contractAccounts(temp){
    var code = document.forms['agentcontractForm'].elements['contractAccount.accountCode'].value;
    code=code.trim();
    if(code!="'" && code!=""){
		 var url="getContractAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(code);
		 http2.open("GET", url, true);
	     http2.onreadystatechange = function(){handleHttpResponsegetAccounts(temp);};
	     http2.send(null);
    }else{
    	document.forms['agentcontractForm'].elements['contractAccount.accountCode'].value="";
        document.forms['agentcontractForm'].elements['contractAccount.accountName'].value="";
        document.forms['agentcontractForm'].elements['contractAccount.agentCorpID'].value ="";
    }
}
function handleHttpResponsegetAccounts(temp){
    if (http2.readyState == 4)
    {
           var results = http2.responseText
           results = results.trim();
           var res = results.split("#");
       if(res.length>1){
			document.forms['agentcontractForm'].elements['contractAccount.accountName'].value = res[1];
			document.forms['agentcontractForm'].elements['contractAccount.agentCorpID'].value = res[2];
			if(temp=="save"){ 
			savePage();
			}
			return true;
			
       }
       else {    	   
       	    alert("Agent Code is not valid." ); 
            document.forms['agentcontractForm'].elements['contractAccount.accountCode'].value="";
            document.forms['agentcontractForm'].elements['contractAccount.accountName'].value="";
            document.forms['agentcontractForm'].elements['contractAccount.agentCorpID'].value ="";
            return false; 
       }
    }
}

function agentChrck(){
	var agent=document.forms['agentcontractForm'].elements['contactAgentType'].value;	
	if(agent=='CMM'){
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeeThreshold'].disabled="";
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeePCT'].disabled="";
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeeMin'].disabled="";
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeeMax'].disabled="";		
	}else{
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeeThreshold'].disabled="disabled";
	    document.forms['agentcontractForm'].elements['contractAccount.cmmFeePCT'].disabled="disabled";
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeeMin'].disabled="disabled";
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeeMax'].disabled="disabled";	
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeeThreshold'].className = 'input-textUpper';
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeePCT'].className = 'input-textUpper';
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeeMin'].className = 'input-textUpper';
		document.forms['agentcontractForm'].elements['contractAccount.cmmFeeMax'].className = 'input-textUpper';	
	}
}

/*
function agentChrck(){
	var agent=document.forms['agentcontractForm'].elements['contactAgentType'].value;	
	var showTd=document.getElementById("cmmShow");
	if(agent=='CMM'){
		showTd.style.display = 'block'
	}else{
		showTd.style.display = 'none'
	}
	

}
*/
function checkValue(){
	var agent=document.forms['agentcontractForm'].elements['contractAccount.agentType'].value;
	document.forms['agentcontractForm'].elements['agentTypeValue'].value=agent;
}

function getVatPercent(){  
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){ 
	        if(document.forms['agentcontractForm'].elements['contractAccount.vatDesc'].value=="${entry.key}"){
	       		 document.forms['agentcontractForm'].elements['contractAccount.vatPercent'].value='${entry.value}';  
	       		 relo="yes"; 
	       	}  
	    }
    </c:forEach>
      if(document.forms['agentcontractForm'].elements['contractAccount.vatDesc'].value==''){
   		 document.forms['agentcontractForm'].elements['contractAccount.vatPercent'].value=0; 
      }  
}
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode; 
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
}
function checkFloat(temp)  { 
    var check='';  
    var i; 
	var s = temp.value;
	var fieldName = temp.name;  
	if(temp.value>100){
		alert("You cannot enter more than 100% VAT ")
		document.forms['agentcontractForm'].elements[fieldName].select();
	    document.forms['agentcontractForm'].elements[fieldName].value='0.00'; 
		return false;
	}  
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++) {   
        var c = s.charAt(i); 
        if(c == '.')  {
        	count = count+1
        }
        if(c == '-')    {
        	countArth = countArth+1
        }
        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
       	  alert("Invalid data in vat%." ); 
          document.forms['agentcontractForm'].elements[fieldName].select();
          document.forms['agentcontractForm'].elements[fieldName].value=''; 
       	  return false;
       	} 
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
        	check='Invalid'; 
        }  
    } 
    if(check=='Invalid'){ 
	    alert("Invalid data in vat%." ); 
	    document.forms['agentcontractForm'].elements[fieldName].select();
	    document.forms['agentcontractForm'].elements[fieldName].value='';
	    return false;
    }else{
	    s=Math.round(s*100)/100;
	    var value=""+s;
	    if(value.indexOf(".") == -1){
	   		value=value+".00";
	    } 
	    if((value.indexOf(".")+3 != value.length)){
	    	value=value+"0";
	    }
	    document.forms['agentcontractForm'].elements[fieldName].value=value; 
    	return true;
    }  
} 
</script>
<%-- Shifting Closed Here --%>

<script language="JavaScript">
try{
agentChrck();
}catch(e){
}
</script>