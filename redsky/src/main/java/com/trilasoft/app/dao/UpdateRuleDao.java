package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.UpdateRule;


public interface UpdateRuleDao extends GenericDao<UpdateRule, Long> {
	
	public List getByCorpID( String sessionCorpID);
	public List <DataCatalog> selectfield(String entity, String corpID);
	public List <DataCatalog> selectDatefield(String corpID);
	public List searchById(Long id,String  fieldToUpdate,String  validationContion,String  ruleStatus);
	public List executeRule(String ruleExpression);
	public void changestatus(String status, Long id);
	public <UpdateRuleResult> List getToDoResultByRuleNumber(String ruleNumber);
	public void deleteUpdateByRuleNumber(Long ruleNumber,String corpId);
	public List findNumberOfResultEntered(String sessionCorpID, Long id);
	public List findMaximum(String sessionCorpID);
	public List findParterNameList(String partnerCode,String corpId);
	public void updateResults(Long ruleNumber);
}
