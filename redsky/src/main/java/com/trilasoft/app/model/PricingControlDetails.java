package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@Entity
@Table(name="pricingcontroldetails")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class PricingControlDetails {
	
	private Long id;
	private String corpID;
	private String pricingControlID;
	private String shipNumber;
	private String address1;
	private String address2;
	private String partnerID;
	private String latitude;
	private String longitude;
	private String partnerName;
	private String rate;
	private String url;
	private String locationPhotoUrl;
	private String feedbackRating;
	private String shipmentActivity;
	
	private String fidiNumber;
	private String OMNINumber;
	private String qualityCertifications;
	private String vanLineAffiliation;
	private String serviceLines;	
	
	private String tarrifApplicability;
	private String currency;
	
	private Boolean originSelection;
	private Boolean destinationSelection;
	
	private String chargeDetail;
	
	private String rateMarkUp;
	private String chargeDetailMarkUp;
	
	private Long refFreightId;
	private Boolean freightSelection;
	
	private String baseCurrency;
	
	private Double exchangeRate;
	
	private String addressDistance;
	private String haulingDistance;
	private String basis;
	private String basisWithoutMarkUp;
	private Boolean dthcFlag;
	private Long rateGridId;
	
	private String haulingBasis;
	private BigDecimal frieghtMarkup;
	private Boolean freightMarkUpControl;
	private String originPortCode;
	private String destinationPortCode;
	private Boolean preferredPort;
	
	private Double discountValue;
	
	private String quoteWithoutTHC;
	private String quoteMarkupWithoutTHC;
	
	private String chargeDetailWithoutTHC; 
	private String chargeDetailMarkUpWithoutTHC; 
	
	private Double discountValueWithoutTHC;
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("pricingControlID", pricingControlID).append(
				"shipNumber", shipNumber).append("address1", address1).append(
				"address2", address2).append("partnerID", partnerID).append(
				"latitude", latitude).append("longitude", longitude).append(
				"partnerName", partnerName).append("rate", rate).append("url",
				url).append("locationPhotoUrl", locationPhotoUrl).append(
				"feedbackRating", feedbackRating).append("shipmentActivity",
				shipmentActivity).append("fidiNumber", fidiNumber).append(
				"OMNINumber", OMNINumber).append("qualityCertifications",
				qualityCertifications).append("vanLineAffiliation",
				vanLineAffiliation).append("serviceLines", serviceLines)
				.append("tarrifApplicability", tarrifApplicability).append(
						"currency", currency).append("originSelection",
						originSelection).append("destinationSelection",
						destinationSelection).append("chargeDetail",
						chargeDetail).append("rateMarkUp", rateMarkUp).append(
						"chargeDetailMarkUp", chargeDetailMarkUp).append(
						"refFreightId", refFreightId).append(
						"freightSelection", freightSelection).append(
						"baseCurrency", baseCurrency).append("exchangeRate",
						exchangeRate)
				.append("addressDistance", addressDistance).append(
						"haulingDistance", haulingDistance).append("basis",
						basis).append("basisWithoutMarkUp", basisWithoutMarkUp)
				.append("dthcFlag", dthcFlag).append("rateGridId", rateGridId)
				.append("haulingBasis", haulingBasis).append("frieghtMarkup",
						frieghtMarkup).append("freightMarkUpControl",
						freightMarkUpControl).append("originPortCode",
						originPortCode).append("destinationPortCode",
						destinationPortCode).append("preferredPort",
						preferredPort).append("discountValue", discountValue)
				.append("quoteWithoutTHC", quoteWithoutTHC).append(
						"quoteMarkupWithoutTHC", quoteMarkupWithoutTHC).append(
						"chargeDetailWithoutTHC", chargeDetailWithoutTHC)
				.append("chargeDetailMarkUpWithoutTHC",
						chargeDetailMarkUpWithoutTHC).append(
						"discountValueWithoutTHC", discountValueWithoutTHC)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PricingControlDetails))
			return false;
		PricingControlDetails castOther = (PricingControlDetails) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(pricingControlID,
				castOther.pricingControlID).append(shipNumber,
				castOther.shipNumber).append(address1, castOther.address1)
				.append(address2, castOther.address2).append(partnerID,
						castOther.partnerID).append(latitude,
						castOther.latitude).append(longitude,
						castOther.longitude).append(partnerName,
						castOther.partnerName).append(rate, castOther.rate)
				.append(url, castOther.url).append(locationPhotoUrl,
						castOther.locationPhotoUrl).append(feedbackRating,
						castOther.feedbackRating).append(shipmentActivity,
						castOther.shipmentActivity).append(fidiNumber,
						castOther.fidiNumber).append(OMNINumber,
						castOther.OMNINumber).append(qualityCertifications,
						castOther.qualityCertifications).append(
						vanLineAffiliation, castOther.vanLineAffiliation)
				.append(serviceLines, castOther.serviceLines).append(
						tarrifApplicability, castOther.tarrifApplicability)
				.append(currency, castOther.currency).append(originSelection,
						castOther.originSelection).append(destinationSelection,
						castOther.destinationSelection).append(chargeDetail,
						castOther.chargeDetail).append(rateMarkUp,
						castOther.rateMarkUp).append(chargeDetailMarkUp,
						castOther.chargeDetailMarkUp).append(refFreightId,
						castOther.refFreightId).append(freightSelection,
						castOther.freightSelection).append(baseCurrency,
						castOther.baseCurrency).append(exchangeRate,
						castOther.exchangeRate).append(addressDistance,
						castOther.addressDistance).append(haulingDistance,
						castOther.haulingDistance).append(basis,
						castOther.basis).append(basisWithoutMarkUp,
						castOther.basisWithoutMarkUp).append(dthcFlag,
						castOther.dthcFlag).append(rateGridId,
						castOther.rateGridId).append(haulingBasis,
						castOther.haulingBasis).append(frieghtMarkup,
						castOther.frieghtMarkup).append(freightMarkUpControl,
						castOther.freightMarkUpControl).append(originPortCode,
						castOther.originPortCode).append(destinationPortCode,
						castOther.destinationPortCode).append(preferredPort,
						castOther.preferredPort).append(discountValue,
						castOther.discountValue).append(quoteWithoutTHC,
						castOther.quoteWithoutTHC).append(
						quoteMarkupWithoutTHC, castOther.quoteMarkupWithoutTHC)
				.append(chargeDetailWithoutTHC,
						castOther.chargeDetailWithoutTHC).append(
						chargeDetailMarkUpWithoutTHC,
						castOther.chargeDetailMarkUpWithoutTHC).append(
						discountValueWithoutTHC,
						castOther.discountValueWithoutTHC).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				pricingControlID).append(shipNumber).append(address1).append(
				address2).append(partnerID).append(latitude).append(longitude)
				.append(partnerName).append(rate).append(url).append(
						locationPhotoUrl).append(feedbackRating).append(
						shipmentActivity).append(fidiNumber).append(OMNINumber)
				.append(qualityCertifications).append(vanLineAffiliation)
				.append(serviceLines).append(tarrifApplicability).append(
						currency).append(originSelection).append(
						destinationSelection).append(chargeDetail).append(
						rateMarkUp).append(chargeDetailMarkUp).append(
						refFreightId).append(freightSelection).append(
						baseCurrency).append(exchangeRate).append(
						addressDistance).append(haulingDistance).append(basis)
				.append(basisWithoutMarkUp).append(dthcFlag).append(rateGridId)
				.append(haulingBasis).append(frieghtMarkup).append(
						freightMarkUpControl).append(originPortCode).append(
						destinationPortCode).append(preferredPort).append(
						discountValue).append(quoteWithoutTHC).append(
						quoteMarkupWithoutTHC).append(chargeDetailWithoutTHC)
				.append(chargeDetailMarkUpWithoutTHC).append(
						discountValueWithoutTHC).append(createdBy).append(
						updatedBy).append(createdOn).append(updatedOn)
				.toHashCode();
	}
	@Column
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	@Column
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getFeedbackRating() {
		return feedbackRating;
	}
	public void setFeedbackRating(String feedbackRating) {
		this.feedbackRating = feedbackRating;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	@Column
	public String getLocationPhotoUrl() {
		return locationPhotoUrl;
	}
	public void setLocationPhotoUrl(String locationPhotoUrl) {
		this.locationPhotoUrl = locationPhotoUrl;
	}
	@Column
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	@Column
	public String getPartnerID() {
		return partnerID;
	}
	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}
	@Column
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	@Column
	public String getPricingControlID() {
		return pricingControlID;
	}
	public void setPricingControlID(String pricingControlID) {
		this.pricingControlID = pricingControlID;
	}
	@Column
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	@Column
	public String getShipmentActivity() {
		return shipmentActivity;
	}
	public void setShipmentActivity(String shipmentActivity) {
		this.shipmentActivity = shipmentActivity;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Column
	public String getFidiNumber() {
		return fidiNumber;
	}
	public void setFidiNumber(String fidiNumber) {
		this.fidiNumber = fidiNumber;
	}
	@Column
	public String getOMNINumber() {
		return OMNINumber;
	}
	public void setOMNINumber(String number) {
		OMNINumber = number;
	}
	@Column
	public String getQualityCertifications() {
		return qualityCertifications;
	}
	public void setQualityCertifications(String qualityCertifications) {
		this.qualityCertifications = qualityCertifications;
	}
	@Column
	public String getServiceLines() {
		return serviceLines;
	}
	public void setServiceLines(String serviceLines) {
		this.serviceLines = serviceLines;
	}
	@Column
	public String getVanLineAffiliation() {
		return vanLineAffiliation;
	}
	public void setVanLineAffiliation(String vanLineAffiliation) {
		this.vanLineAffiliation = vanLineAffiliation;
	}
	@Column
	public String getTarrifApplicability() {
		return tarrifApplicability;
	}
	public void setTarrifApplicability(String tarrifApplicability) {
		this.tarrifApplicability = tarrifApplicability;
	}
	@Column
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Column
	public Boolean getDestinationSelection() {
		return destinationSelection;
	}
	public void setDestinationSelection(Boolean destinationSelection) {
		this.destinationSelection = destinationSelection;
	}
	@Column
	public Boolean getOriginSelection() {
		return originSelection;
	}
	public void setOriginSelection(Boolean originSelection) {
		this.originSelection = originSelection;
	}
	@Column
	public String getChargeDetail() {
		return chargeDetail;
	}
	public void setChargeDetail(String chargeDetail) {
		this.chargeDetail = chargeDetail;
	}
	@Column
	public String getChargeDetailMarkUp() {
		return chargeDetailMarkUp;
	}
	public void setChargeDetailMarkUp(String chargeDetailMarkUp) {
		this.chargeDetailMarkUp = chargeDetailMarkUp;
	}
	@Column
	public String getRateMarkUp() {
		return rateMarkUp;
	}
	public void setRateMarkUp(String rateMarkUp) {
		this.rateMarkUp = rateMarkUp;
	}
	@Column
	public Boolean getFreightSelection() {
		return freightSelection;
	}
	public void setFreightSelection(Boolean freightSelection) {
		this.freightSelection = freightSelection;
	}
	@Column
	public Long getRefFreightId() {
		return refFreightId;
	}
	public void setRefFreightId(Long refFreightId) {
		this.refFreightId = refFreightId;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	public Double getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public String getAddressDistance() {
		return addressDistance;
	}
	public void setAddressDistance(String addressDistance) {
		this.addressDistance = addressDistance;
	}
	public String getHaulingDistance() {
		return haulingDistance;
	}
	public void setHaulingDistance(String haulingDistance) {
		this.haulingDistance = haulingDistance;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public Boolean getDthcFlag() {
		return dthcFlag;
	}
	public void setDthcFlag(Boolean dthcFlag) {
		this.dthcFlag = dthcFlag;
	}
	public String getBasisWithoutMarkUp() {
		return basisWithoutMarkUp;
	}
	public void setBasisWithoutMarkUp(String basisWithoutMarkUp) {
		this.basisWithoutMarkUp = basisWithoutMarkUp;
	}
	public Long getRateGridId() {
		return rateGridId;
	}
	public void setRateGridId(Long rateGridId) {
		this.rateGridId = rateGridId;
	}
	public String getHaulingBasis() {
		return haulingBasis;
	}
	public void setHaulingBasis(String haulingBasis) {
		this.haulingBasis = haulingBasis;
	}
	public BigDecimal getFrieghtMarkup() {
		return frieghtMarkup;
	}
	public void setFrieghtMarkup(BigDecimal frieghtMarkup) {
		this.frieghtMarkup = frieghtMarkup;
	}
	public Boolean getFreightMarkUpControl() {
		return freightMarkUpControl;
	}
	public void setFreightMarkUpControl(Boolean freightMarkUpControl) {
		this.freightMarkUpControl = freightMarkUpControl;
	}
	public String getDestinationPortCode() {
		return destinationPortCode;
	}
	public void setDestinationPortCode(String destinationPortCode) {
		this.destinationPortCode = destinationPortCode;
	}
	public String getOriginPortCode() {
		return originPortCode;
	}
	public void setOriginPortCode(String originPortCode) {
		this.originPortCode = originPortCode;
	}
	public Boolean getPreferredPort() {
		return preferredPort;
	}
	public void setPreferredPort(Boolean preferredPort) {
		this.preferredPort = preferredPort;
	}
	public Double getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(Double discountValue) {
		this.discountValue = discountValue;
	}
	public String getChargeDetailMarkUpWithoutTHC() {
		return chargeDetailMarkUpWithoutTHC;
	}
	public void setChargeDetailMarkUpWithoutTHC(String chargeDetailMarkUpWithoutTHC) {
		this.chargeDetailMarkUpWithoutTHC = chargeDetailMarkUpWithoutTHC;
	}
	public String getChargeDetailWithoutTHC() {
		return chargeDetailWithoutTHC;
	}
	public void setChargeDetailWithoutTHC(String chargeDetailWithoutTHC) {
		this.chargeDetailWithoutTHC = chargeDetailWithoutTHC;
	}
	public Double getDiscountValueWithoutTHC() {
		return discountValueWithoutTHC;
	}
	public void setDiscountValueWithoutTHC(Double discountValueWithoutTHC) {
		this.discountValueWithoutTHC = discountValueWithoutTHC;
	}
	public String getQuoteMarkupWithoutTHC() {
		return quoteMarkupWithoutTHC;
	}
	public void setQuoteMarkupWithoutTHC(String quoteMarkupWithoutTHC) {
		this.quoteMarkupWithoutTHC = quoteMarkupWithoutTHC;
	}
	public String getQuoteWithoutTHC() {
		return quoteWithoutTHC;
	}
	public void setQuoteWithoutTHC(String quoteWithoutTHC) {
		this.quoteWithoutTHC = quoteWithoutTHC;
	}
	
	
	
	
	
	
	
	
	}
