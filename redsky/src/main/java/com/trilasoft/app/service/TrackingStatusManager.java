/**
 * Business Service Interface to handle communication between web and persistence layer.
 * Gets TrackingStatus information based on TrackingStatus id.
 * @param             TrackingStatusId the TrackingStatus's id
 * This interface represents the basic actions on "TrackingStatus" object in Redsky that allows for TrackingStatus  of Shipment.
 * @Interface Name	  TrackingStatusManager
 * @Author            Sangeeta Dwivedi
 * @Version           V01.0
 * @Since             1.0
 * @Date              01-Dec-2008
 */

package com.trilasoft.app.service;
import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.TrackingStatus; 
import java.util.Date;
import java.util.List; 
import java.util.Map;
import java.util.Set;

public interface TrackingStatusManager extends GenericManager<TrackingStatus, Long> { 
    public List<AccountLine> findByBooker(Long id);
    public List  findByoriginSubAgentCode(String originSubAgentCode);
    public List<AccountLine> findByOriginAgent(String shipNumber);
	public List<AccountLine> findByDestinationAgent(String shipNumber);
	public List<AccountLine> findByOriginAgentCode(String shipNumber);
	public List<AccountLine> findByDestinationAgentCode(String shipNumber);
	public List findMaximumId();
    public List checkById(Long id);
	public List countForStorage(String shipNumber);
	public int updateTrack(Long id, String blNumber,String hblNumber);
	public List findETD(String shipNumber, String etDeparts);
	public List<TrackingStatus> findByOriginAgentInTrackingStauts(String shipNumber);
	public List<TrackingStatus> findByOriginAgentCodeInTrackingStauts(Long id);
	public List<TrackingStatus> findByDestinationAgentInTrackingStauts(String shipNumber);
    public List<TrackingStatus> findByDestinationAgentCodeInTrackingStauts(Long id);
    public List omniReportExtract(String bookCode,String endDates,String companyCode, String corpId,String jobTypeMulitiple);
	public Boolean getdefaultVendorAccLine(String sessionCorpID);
	public List findSalesStatus(String seuenceNumber);
	public int updateSalesStatus(String seuenceNumber,String userName);
	public List findIdByShipNumber(String shipNumber);
	public List findVanLineCodeList(String partnerCode, String sessionCorpID);
	public List findVanLineAgent(String vanLineCode, String sessionCorpID);
	public boolean checkEmailAlertSent(String forwarderCode, String sessionCorpID);
	public List findBookingAgentCode(String companyCode, String corpID);
	public void getOmniReport (String shipNumber, String corpID);
	public Boolean getIsNetworkBookingAgent(String sessionCorpID,String bookingAgentCode);
	public Boolean getIsNetworkAgent(String sessionCorpID,String agentCode);
	public String getExternalCorpID(String sessionCorpID,String agentCode);
	public void updateExternalSo(Long id, String shipNumber);
	public String getLocalBookingAgentCode(String corpID,String bookingAgentCode);
	public void removeExternalLink(Long customerFileId, String agentExSo,String userName, Long Id, String shipNum, String sessionCorpID, Boolean soNetworkGroup);
	public String getAgentTypeRole(String sessionCorpID, Long id);
	public String[] getLocalAgent(String agentCorpID, String sessionCorpID,String agentCode);
	public String getCompanyDivision(String bookingAgentCode,String sessionCorpID, String externalCorpID);
	public List<TrackingStatus> getNetworkedTrackingStatus(String sequenceNumber);
	public Boolean checkNetworkSection(String bookingAgentShipNumber, String section);
	public List getCountryCode(String fileNumber);
	public List downloadXMLFile(String sessionCorpID,String fileNumber);
	public List omniReportExtractByOmni(String bookCode,String endDates,String companyCode,String corpId,String jobTypeMulitiple);
	public boolean getContractType(String sessionCorpID, String contract);
	public boolean getCMMContractType(String sessionCorpID, String contract);
	public boolean getDMMContractType(String sessionCorpID, String contract);
	public String[] findParentBillTo(String billToCode, String sessionCorpID);
	public List getIsNetworkAgentCorpId(String sessionCorpID, String agentCode) ;
	public String getAgentTypeRoleForm(String sessionCorpID, Long id, String bookingAgentCode, String networkPartnerCode, String originAgentCode, String destinationAgentCode, String originSubAgentCode, String destinationSubAgentCode);
	public void updateDMMAgentBilltocode(String sessionCorpID, Long id, String networkPartnerCode, String networkPartnerName);
	public List getAllBookingAgentList(String sessionCorpID);
	public String checkIsRedSkyUser(String partnerCode);
	public boolean getAgentsExistInRedSky(String partnerCode, String sessionCorpID,String name, String emailId);
	public List<TrackingStatus>  getCurrentDetail(Long soid);
	public void updateNetworkflagDSP(Long id);
	public String findMoveTypePartnerValue(String sessionCorpID, String partnerCode);
	public List findDocumentFromMyFileCF(String seqNumber,String fieldValue,String sessionCorpID);
	public List findDocumentFromMyFileSO(String seqNumber,String shpNumber,String fieldValue,String sessionCorpID);
	//public void updateSOExtFalg(Long id);
	public Map<Long, TrackingStatus> getTrackingStatusObjectMap(Set serviceorderIdSet);
	public List getvenderCodeDetais(String networkVendorCode);
	public List getSOListbyCF(Long cid,Long id);
	public void updateTrackingStatusAgentDetails(StringBuffer query);
	public Map<String,String> findByAgent(String shipNumber,String type);
	public boolean getCMMContractTypeForOtherCorpid(String sessionCorpID, String contract);
	public boolean getDMMContractTypeForOtherCorpid(String sessionCorpID, String contract);
	public String checkNetworkCoordinatorInUser(String cfCoordinatorname, String soCoordinatorname, String sessionCorpID);
	public String getRddDaysFromPartner(String fieldName,String soBillToCode,String sessionCorpID);
	public List validatePreferredAgentCode(String partnerCode,String corpid, String billToCode);
	public void updateLinkedShipNumberforCompletedDate(Set linkedshipNumber,Date serviceCompleteDate,String corpID,String userName);
	public List getEmailFromPortalUser(String partnerCode,String corpid);
	 public  String loadInfoByCode(String originCode,String corpId) ;
	public String checkAllInvoicedLinkedAccountLines(String shipnumber,String linkedshipnumberTS, String contractType);
}  
