package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.PartnerRefChargesDao;
import com.trilasoft.app.model.PartnerRefCharges;

public class PartnerRefChargesDaoHibernate extends GenericDaoHibernate<PartnerRefCharges, Long> implements PartnerRefChargesDao{
	
	public PartnerRefChargesDaoHibernate() {
		super(PartnerRefCharges.class);
	}

	public List getRefCharesList(String value, String tariffApplicability, String parameter, String countryCode, String grid, String basis, String label, String sessionCorpID) {
		// TODO Auto-generated method stub
		return getHibernateTemplate().find("from PartnerRefCharges where value like '%"+value.replaceAll("'", "''").replaceAll(":", "''")+"%'  and tariffApplicability like '%"+tariffApplicability.replaceAll("'", "''").replaceAll(":", "''")+"%' and  parameter like '%"+parameter.replaceAll("'", "''").replaceAll(":", "''")+"%' and countryCode like '%"+countryCode.replaceAll("'", "''").replaceAll(":", "''")+"%'   and grid like '%"+grid.replaceAll("'", "''").replaceAll(":", "''")+"%' and basis like '%"+basis.replaceAll("'", "''").replaceAll(":", "''")+"%' and label like '%"+label.replaceAll("'", "''").replaceAll(":", "''")+"%'  and corpID='"+sessionCorpID+"'");
		
	}
	
	
}
