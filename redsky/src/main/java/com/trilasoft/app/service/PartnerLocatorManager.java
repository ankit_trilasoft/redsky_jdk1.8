package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PartnerLocation;

public interface PartnerLocatorManager extends GenericManager<PartnerLocation, Long> {

	String locatePartners(double inputLatitute, double inputLongitude, String countryCode, int withinMiles);
	
	public String locateAgents(double weight, double latitude, double longitude, String tariffApplicability, String countryCode, String packingMode, String poe );

}
