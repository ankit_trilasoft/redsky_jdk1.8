package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.TimeSheetDaoHibernate.ResourceWorkTickets;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AuditTrail;
import com.trilasoft.app.model.AvailableCrew;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.CategoryRevenue;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DayDistributionHoursDTO;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Payroll;
import com.trilasoft.app.model.PayrollAllocation;
import com.trilasoft.app.model.ScheduleResourceOperation;
import com.trilasoft.app.model.ScrachCard;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TimeSheet;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Truck;
import com.trilasoft.app.model.TruckingOperations;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AuditTrailManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ExpressionManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.PayrollManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ScheduleResourceOperationManager;
import com.trilasoft.app.service.ScrachCardManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TimeSheetManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.TruckManager;
import com.trilasoft.app.service.TruckingOperationsManager;
import com.trilasoft.app.service.WorkTicketManager;

public class TimeSheetAction  extends BaseAction implements Preparable{
	
	private Long id;
	private String pageCorpID;
	private TimeSheetManager timeSheetManager;
	private TimeSheet timeSheet;
	private WorkTicket workTicket;
	private Billing billing;
	private WorkTicketManager workTicketManager;
	private ScheduleResourceOperationManager scheduleResourceOperationManager;
	private PayrollManager payrollManager; 
	private Payroll payroll;
	private PayrollAllocation payrollAllocation;
	private Set<Payroll> payrolls;
	private Set workTickets; 
	private List timeSheets;
	private List timeSheets1;
	private String requestedWorkTkt;
	private String requestedCrews;
	private String requestedTimeSt;
	private String sessionCorpID;
	private String hitFlag;
	private String workticketServiceGroup;
	private List availableCrews;
	private ExpressionManager expressionManager;
	private Boolean volumeUnit;
	private CustomerFileManager customerFileManager;
	private List timetypeFlex1List;
	private String driverId;
	private Boolean driverChecked;
	private String showAbsent="";
	private String checkbtn="A";
	private String truckWareHse;
	private List availableTrucks;
	private String countScheduleResources;
	private List workTicketsList;
	private String selectedWorkTicket;
	private String selectedAvailableCrew;
	private String selectedAvailableTrucks;
	private String filter;
	private String workticketService;
	private String serviceInGrp ="";
	private List<SystemDefault> sysDefaultDetail;
	private User user;
	private String dfltWhse;
	private String defaultVolumeUnit;
	private String shipNumberForCity;
	private String ocityForCity;
	private String dcityForCity;
	
	public String requestedTrucks;
	private Truck truck;
	private TruckManager truckManager;
	private TruckingOperationsManager truckingOperationsManager;
	private TruckingOperations truckingOperations;
		
	private String beginHours;
	private String regularHours;
	private Long id1;
	private String workticketStatus;
	private String flag;
	private List resourceList;
	private AuditTrail auditTrail;
	private AuditTrailManager auditTrailManager;
	private Company company;
	private CompanyManager companyManager;
	
	private Map<String, String> tcktactn;
    private String voxmeIntergartionFlag;
    private MobileMoverAction mobileMoverAction;

	private String truckRequired;
	private String mmValidation = "NO";
	private String mobileMoverCheck;
	private Boolean visibilityForCorpId=false;
	Date currentdate = new Date();
    private String oiJobList; 
    private int vehicleRecords=0;
    private int crewRecords=0;

	static final Logger logger = Logger.getLogger(TimeSheetAction.class);
	
	public TimeSheetAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	
	private boolean checkFieldVisibility1=false;
	private boolean checkFieldVisibility2=false;
	private ScrachCardManager scrachCardManager;
	private ScrachCard scrachCard;
	private String countScrachCard;
	 public void prepare() throws Exception {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				company=companyManager.findByCorpID(sessionCorpID).get(0);
				if(company!=null && company.getOiJob()!=null){
								oiJobList=company.getOiJob();  
							  }
				if(company!=null){
					if(company.getVoxmeIntegration()!=null){
						voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
						}
					if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
						mmValidation= "Yes";
						if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
							mmValidation= "No";
						}
					}
					}
				fieldNameList = new LinkedHashMap<String, String>();
				fieldNameList.put("crewName", "Crew");
				fieldNameList.put("localtruckNumber", "Truck");
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			} 
	 
		// Setting time zero
		public Date getZeroTimeDate(Date dt) {
		    Date res = dt;
		    Calendar calendar = Calendar.getInstance();
		    calendar.setTime(dt);
		    calendar.set(Calendar.HOUR_OF_DAY, 0);
		    calendar.set(Calendar.MINUTE, 0);
		    calendar.set(Calendar.SECOND, 0);
		    calendar.set(Calendar.MILLISECOND, 0);
		    res = calendar.getTime();
		    return res;
		}
	public TimeSheet getTimeSheet() {
		return timeSheet;
	}
	public void setTimeSheet(TimeSheet timeSheet) {
		this.timeSheet = timeSheet;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setTimeSheetManager(TimeSheetManager timeSheetManager) {
		this.timeSheetManager = timeSheetManager;
	}

	public void setExpressionManager(ExpressionManager expressionManager ){
		this.expressionManager = expressionManager;
	}
	public String getRequestedCrews() {
		return requestedCrews;
	}
	public void setRequestedCrews(String requestedCrews) {
		this.requestedCrews = requestedCrews;
	}
	public String getDcityForCity() {
		return dcityForCity;
	}

	public void setDcityForCity(String dcityForCity) {
		this.dcityForCity = dcityForCity;
	}

	public String getRequestedWorkTkt() {
		return requestedWorkTkt;
	}
	public void setRequestedWorkTkt(String requestedWorkTkt) {
		this.requestedWorkTkt = requestedWorkTkt;
	}
	
	public Set<Payroll> getPayrolls() {
		return payrolls;
	}

	public void setPayrolls(Set<Payroll> payrolls) {
		this.payrolls = payrolls;
	}
	
	public Set getWorkTickets() { 
        return workTickets; 
    } 
    
    public List getTimeSheets() {
		return timeSheets;
	}
	public void setTimeSheets(List timeSheets) {
		this.timeSheets = timeSheets;
	}
	public String timeSheets(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		getComboList(sessionCorpID,"a");
    	/*workTickets = new HashSet(workTicketManager.getAll());
    	//payrolls = new HashSet(payrollManager.getAll());
    	timeSheets = timeSheetManager.getAll();
    	availableCrews = timeSheetManager.findCrewList();
    	getSession().setAttribute("countTimeSheet", timeSheets.size());*/		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
        return SUCCESS;
    }
	
	public Payroll getPayroll() {
		return payroll;
	}
	public void setPayroll(Payroll payroll) {
		this.payroll = payroll;
	}
	public WorkTicket getWorkTicket() {
		return workTicket;
	}
	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}
	public void setPayrollManager(PayrollManager payrollManager) {
		this.payrollManager = payrollManager;
	}
	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}
	Long countTimeSheetUpdate = Long.parseLong("0");
	public String buildTimeSheet(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		Date date1 = null;
		if(!workDt.equalsIgnoreCase("")){
		    try {
		    	date1 = new SimpleDateFormat("dd-MMM-yy").parse(workDt);
		    } catch (ParseException e) {
				
				e.printStackTrace();
			}
		}
		//String serType = workTicket.getService();
		int i;
		
		String[] reqWrkTkt = requestedWorkTkt.split("\\,");
		for (int x=0; x<reqWrkTkt.length; x++)
 	     {
			workTicket = workTicketManager.get(Long.parseLong(reqWrkTkt[x]));
			List<Billing> lst = timeSheetManager.getBilling(workTicket.getShipNumber());
			if(lst == null || lst.isEmpty() || lst.get(0) == null){
				billing = new Billing();
			}else{
				billing = (Billing) lst.get(0);
			}
			List<PayrollAllocation> plst = timeSheetManager.findDistAndCalcCode(workTicket.getJobType(), workTicket.getService(), sessionCorpID,workTicket.getJobMode(),workTicket.getCompanyDivision());
			if(plst == null || plst.isEmpty() || plst.get(0) == null){
				payrollAllocation = new PayrollAllocation();
			}else{
				payrollAllocation = (PayrollAllocation) plst.get(0);
			}
		
			String[] reqCrew = requestedCrews.split("\\,");
			
			for (int y=0; y<reqCrew.length; y++)
	 	     {
				payroll = payrollManager.get(Long.parseLong(reqCrew[y]));
				List dateForBeginHours=timeSheetManager.getTimeFromEndHours(workTicket.getTicket(), payroll.getLastName()+", "+payroll.getFirstName(), date1);
				timeSheet = new TimeSheet();
				timeSheet.setContract(billing.getContract());
				timeSheet.setTicket(workTicket.getTicket());
				timeSheet.setWarehouse(payroll.getWarehouse());
				timeSheet.setDistributionCode(payrollAllocation.getCode());
				timeSheet.setCalculationCode(payrollAllocation.getCalculation());
				timeSheet.setCrewName(payroll.getLastName()+", "+payroll.getFirstName());
				timeSheet.setShipper(workTicket.getLastName()+", "+workTicket.getFirstName());
				timeSheet.setUserName(payroll.getUserName());
				timeSheet.setCrewType(payroll.getTypeofWork());
				timeSheet.setCrewEmail(payroll.getCrewEmail());
				timeSheet.setIntegrationTool(payroll.getIntegrationTool());
				timeSheet.setLunch("No lunch");
				timeSheet.setCreatedOn(new Date());
				timeSheet.setDoneForTheDay("false");
				//timeSheet.setUpdatedOn(new Date());
				timeSheet.setCreatedBy(getRequest().getRemoteUser());
				if((!dateForBeginHours.isEmpty()) && (dateForBeginHours.get(0)!=null)){
					timeSheet.setBeginHours(dateForBeginHours.get(0).toString());
				}
				
				if(payroll.getPayhour() == null || payroll.getPayhour().equalsIgnoreCase("")){
					timeSheet.setRegularPayPerHour("0");
					timeSheet.setOverTimePayPerHour("0");
					timeSheet.setDoubleOverTimePayPerHour("0");
					
				}else{
				timeSheet.setRegularPayPerHour(payroll.getPayhour());
				timeSheet.setOverTimePayPerHour(String.valueOf((Double.parseDouble(payroll.getPayhour())*1.5)));
				timeSheet.setDoubleOverTimePayPerHour(String.valueOf((Double.parseDouble(payroll.getPayhour())*2)));
				}
				if(!(timeSheet.getPaidRevenueShare() == null) && timeSheet.getPaidRevenueShare().equals("")){
					timeSheet.setPaidRevenueShare(new BigDecimal(0));
				}
				timeSheet.setAction("C");
				timeSheet.setCorpID(sessionCorpID);
				timeSheet.setWorkDate(date1);
				timeSheet.setIsCrewUsed(true);
				timeSheet.setUpdatedOn(new Date());
				timeSheet.setUpdatedBy(getRequest().getRemoteUser());
				String workTypeFlex1="";
				List typeOfWorkList = refMasterManager.typeOfWorkFlex1(payroll.getTypeofWork(),"CREWTYPE",sessionCorpID);
				if(typeOfWorkList!=null && !typeOfWorkList.isEmpty() && (typeOfWorkList.get(0)!=null)){
					workTypeFlex1 = typeOfWorkList.get(0).toString();	
				}
				if(workTicket.getWarehouse().equalsIgnoreCase("C") && payroll.getVlCode()!= null && !payroll.getVlCode().equalsIgnoreCase("") && workTypeFlex1.equalsIgnoreCase("DRIVER")){
					List pkCodeList = payrollManager.getPkCodeList(payroll.getVlCode(),sessionCorpID);
					if(pkCodeList!=null && !pkCodeList.isEmpty()){
			    	  Iterator it = pkCodeList.iterator();
				         while (it.hasNext()) {
				        	 String partnerCode=(String)it.next();
				        	 miscellaneousManager.updatePackingDriverId(partnerCode,workTicket.getService(),workTicket.getShipNumber(),sessionCorpID);
				       }					    
				    }
				}
	 	    	timeSheet=timeSheetManager.save(timeSheet);
	 	    	
	 	    	countTimeSheetUpdate++;
	 	     }
			timeSheetManager.updateWorkTicketCrew(workTicket.getTicket(),getRequest().getRemoteUser(),sessionCorpID);
 	     }
		//workTicket.setService(serType);
		//search();		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		if(countTimeSheetUpdate == 1 ){
			timeSheet = timeSheetManager.get(timeSheet.getId());
			getComboList(sessionCorpID,"a");
			return INPUT;
		}else{
			search();
			return SUCCESS;
		}
	}
	private List idList=new ArrayList();
public String groupAssignTimeSheet(){
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		Date date1 = null;
		if(!workDt.equalsIgnoreCase("")){
		    try {
		    	date1 = new SimpleDateFormat("dd-MMM-yy").parse(workDt);
		    } catch (ParseException e) {
				
				e.printStackTrace();
			}
		}
		//String serType = workTicket.getService();
		int i;
		String firstId="";
		//List idList=new ArrayList();
		//char[] saveId;
		String[] reqWrkTkt = requestedWorkTkt.split("\\,");
		for (int x=0; x<reqWrkTkt.length; x++)
 	     {
			workTicket = workTicketManager.get(Long.parseLong(reqWrkTkt[x]));
			List<Billing> lst = timeSheetManager.getBilling(workTicket.getShipNumber());
			if(lst == null || lst.isEmpty() || lst.get(0) == null){
				billing = new Billing();
			}else{
				billing = (Billing) lst.get(0);
			}
			List<PayrollAllocation> plst = timeSheetManager.findDistAndCalcCode(workTicket.getJobType(), workTicket.getService(), sessionCorpID,workTicket.getJobMode(),workTicket.getCompanyDivision());
			if(plst == null || plst.isEmpty() || plst.get(0) == null){
				payrollAllocation = new PayrollAllocation();
			}else{
				payrollAllocation = (PayrollAllocation) plst.get(0);
			}
			
			String[] reqCrew = requestedCrews.split("\\,");
			firstId=reqCrew[0];
			//String temp=reqCrew.toString();
			//String firstElement=temp.substring(1,temp.indexOf(","));
			for (int y=0; y<reqCrew.length; y++)
	 	     {
				payroll = payrollManager.get(Long.parseLong(reqCrew[y]));
				List dateForBeginHours=timeSheetManager.getTimeFromEndHours(workTicket.getTicket(), payroll.getLastName()+", "+payroll.getFirstName(), date1);
				//System.out.println("\n\n\n\n dateForBeginHours--->>"+dateForBeginHours);
				timeSheet = new TimeSheet();
				timeSheet.setContract(billing.getContract());
				timeSheet.setTicket(workTicket.getTicket());
				timeSheet.setWarehouse(payroll.getWarehouse());
				timeSheet.setDistributionCode(payrollAllocation.getCode());
				timeSheet.setCalculationCode(payrollAllocation.getCalculation());
				timeSheet.setCrewName(payroll.getLastName()+", "+payroll.getFirstName());
				timeSheet.setShipper(workTicket.getLastName()+", "+workTicket.getFirstName());
				timeSheet.setUserName(payroll.getUserName());
				timeSheet.setCrewType(payroll.getTypeofWork());
				timeSheet.setLunch("No lunch");
				timeSheet.setCreatedOn(new Date());
				//timeSheet.setUpdatedOn(new Date());
				timeSheet.setCreatedBy(getRequest().getRemoteUser());
				if((!dateForBeginHours.isEmpty()) && (dateForBeginHours.get(0)!=null)){
					timeSheet.setBeginHours(dateForBeginHours.get(0).toString());
				}
				
				if(payroll.getPayhour() == null || payroll.getPayhour().equalsIgnoreCase("")){
					timeSheet.setRegularPayPerHour("0");
					timeSheet.setOverTimePayPerHour("0");
					timeSheet.setDoubleOverTimePayPerHour("0");
					
				}else{
				timeSheet.setRegularPayPerHour(payroll.getPayhour());
				timeSheet.setOverTimePayPerHour(String.valueOf((Double.parseDouble(payroll.getPayhour())*1.5)));
				timeSheet.setDoubleOverTimePayPerHour(String.valueOf((Double.parseDouble(payroll.getPayhour())*2)));
				}
				if(!(timeSheet.getPaidRevenueShare() == null) && timeSheet.getPaidRevenueShare().equals("")){
					timeSheet.setPaidRevenueShare(new BigDecimal(0));
				}
				timeSheet.setAction("C");
				timeSheet.setCorpID(sessionCorpID);
				timeSheet.setWorkDate(date1);
				timeSheet.setIsCrewUsed(true);
				timeSheet.setUpdatedOn(new Date());
				timeSheet.setUpdatedBy(getRequest().getRemoteUser());
				if(workTicket.getWarehouse().equalsIgnoreCase("C") && payroll.getVlCode()!= null && !payroll.getVlCode().equalsIgnoreCase("") && payroll.getTypeofWork().equalsIgnoreCase("DR")){
					List pkCodeList = payrollManager.getPkCodeList(payroll.getVlCode(),sessionCorpID);
					if(pkCodeList!=null && !pkCodeList.isEmpty()){
			    	  Iterator it = pkCodeList.iterator();
				         while (it.hasNext()) {
				        	 String partnerCode=(String)it.next();
				        	 miscellaneousManager.updatePackingDriverId(partnerCode,workTicket.getService(),workTicket.getShipNumber(),sessionCorpID);
				       }					    
				    }
				}
	 	    	timeSheetManager.save(timeSheet);
	 	    	String maxid=timeSheetManager.getMaxId(timeSheet.getUserName()).get(0).toString();
	 	    	idList.add(maxid);
	 	    	//System.out.println("\n\n\n\n\n idlist 1---->"+idList);
	 	    	//saveId= timeSheet.getId().toString().toCharArray();
	 	    	countTimeSheetUpdate++;
	 	     }
			timeSheetManager.updateWorkTicketCrew(workTicket.getTicket(),getRequest().getRemoteUser(),sessionCorpID);
 	     }
		
		//workTicket.setService(serType);
		//search();
			//System.out.println("\n\n\n\n firstId-->"+firstId);
			//Long maxId=timeSheetManager.getMaxId();
			timeSheet = timeSheetManager.get(Long.parseLong(idList.get(0).toString()));
			getComboList(sessionCorpID,"a");			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return INPUT;
		
	}
	
	
	private String typeSubmit;
	public String absentsTimeSheet(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		Date date1 = null;
		if(!workDt.equalsIgnoreCase("")){
		    try {
		    	date1 = new SimpleDateFormat("dd-MMM-yy").parse(workDt);
		    } catch (ParseException e) {
				
				e.printStackTrace();
			}
		}
		String[] reqCrew = requestedCrews.split("\\,");
		
		for (int y=0; y<reqCrew.length; y++)
 	     {
			payroll = payrollManager.get(Long.parseLong(reqCrew[y]));
			timeSheet = new TimeSheet();
			timeSheet.setTicket(Long.parseLong("0"));
			timeSheet.setCrewName(payroll.getLastName()+", "+payroll.getFirstName());
			timeSheet.setUserName(payroll.getUserName());
			timeSheet.setWarehouse(payroll.getWarehouse());
			timeSheet.setCorpID(sessionCorpID);
			timeSheet.setCrewType(payroll.getTypeofWork());
			timeSheet.setWorkDate(date1);
			timeSheet.setShipper("");
			timeSheet.setLunch("No lunch");
			timeSheet.setAction("");
			timeSheet.setDoneForTheDay("true");
			if(payroll.getPayhour() == null){
				timeSheet.setRegularPayPerHour("0");
			}else{
			timeSheet.setRegularPayPerHour(payroll.getPayhour());
			}
			List<SystemDefault> sysDefault = timeSheetManager.findSystemDefault(sessionCorpID);
			for(SystemDefault systemDefault : sysDefault){
				timeSheet.setBeginHours(systemDefault.getDayAt1());
				timeSheet.setEndHours(systemDefault.getDayAt2());
			}
			String beginAbsentHrs = timeSheet.getBeginHours();
			String endAbsentHrs = timeSheet.getEndHours();
			Long totalAbsentTimeMnts =  Long.parseLong(endAbsentHrs.substring(0, endAbsentHrs.indexOf(":")))*60 + Long.parseLong(endAbsentHrs.substring(endAbsentHrs.indexOf(":")+1, endAbsentHrs.length())) - Long.parseLong(beginAbsentHrs.substring(0, beginAbsentHrs.indexOf(":")))*60 + Long.parseLong(beginAbsentHrs.substring(beginAbsentHrs.indexOf(":")+1, beginAbsentHrs.length())) ;
			String absentHr = String.valueOf(Double.parseDouble(String.valueOf(totalAbsentTimeMnts))/60);
			// please check it if null
			timeSheet.setRegularHours(new BigDecimal(absentHr));
			timeSheet.setUpdatedOn(new Date());
			timeSheet.setCreatedOn(new Date());
			timeSheet.setUpdatedBy(getRequest().getRemoteUser());
			timeSheet.setCreatedBy(getRequest().getRemoteUser());
 	    	timeSheetManager.save(timeSheet);
 	    	String maxid=timeSheetManager.getMaxId(timeSheet.getUserName()).get(0).toString();
 	    	idList.add(maxid);
 	    	countTimeSheetUpdate++;
 	     }
		//search();
		/*if(countTimeSheetUpdate == 1 ){
			timeSheet = timeSheetManager.findTimeSheetDetail();
			getComboList(sessionCorpID,"a");
			return INPUT;
		}else{
			search();
			return SUCCESS;
		}
		
		*/
		absentId=Long.parseLong(idList.get(0).toString());
		timeSheet = timeSheetManager.get(Long.parseLong(idList.get(0).toString()));
		getComboList(sessionCorpID,"a");		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return INPUT;
	
		
		
	}
	
	public String removeTimeSheet(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		String[] reqRmvTimeSt = requestedTimeSt.split("\\,");
		for (int x=0; x<reqRmvTimeSt.length; x++)
	     {
			String crewName="";
			Date workDate=new Date();
			String lunch="";
			Double childRegHours=0.0;
			String action="";
			
			TimeSheet timeSheetNew=(TimeSheet)timeSheetManager.get(Long.parseLong(reqRmvTimeSt[x].toString()));
			Long ticket=timeSheetNew.getTicket();
			/*if(timeSheetNew.getAction().toString().equalsIgnoreCase("L"))
			{
			if(timeSheetNew!=null)
			{
				crewName=timeSheetNew.getCrewName();
				workDate=timeSheetNew.getWorkDate();
				lunch=timeSheetNew.getLunch();
				if(!timeSheetNew.getRegularHours().equalsIgnoreCase(""))
				{
					childRegHours=Double.parseDouble(timeSheetNew.getRegularHours());
				}
				
				action=timeSheetNew.getAction();
				timeSheetManager.remove(Long.parseLong(reqRmvTimeSt[x]));
				List test=timeSheetManager.getParentTimeSheet(crewName, workDate,lunch);
				if(!test.isEmpty())
				{
					timeSheet=(TimeSheet)timeSheetManager.getParentTimeSheet(crewName, workDate,lunch).get(0);
					Double parentRegHours=Double.parseDouble(timeSheet.getRegularHours());
					Double newRegHours=childRegHours+parentRegHours;
					timeSheetManager.updateRegHours(newRegHours.toString(), crewName,workDate,lunch);
				}
			}
			}*/
			//else
			//{
				
				timeSheetManager.remove(Long.parseLong(reqRmvTimeSt[x]));
			//}
			
			/*if(action.toString().equalsIgnoreCase("L"))
			{
				List test=timeSheetManager.getParentTimeSheet(crewName, workDate,lunch);
				if(!test.isEmpty())
				{
					timeSheet=(TimeSheet)timeSheetManager.getParentTimeSheet(crewName, workDate,lunch).get(0);
					Double parentRegHours=Double.parseDouble(timeSheet.getRegularHours());
					Double newRegHours=childRegHours+parentRegHours;
					timeSheetManager.updateRegHours(newRegHours.toString(), crewName,workDate,lunch);
				}
				
							
			}*/
				timeSheetManager.updateWorkTicketCrew(ticket,getRequest().getRemoteUser(),sessionCorpID);
				
	     }
		
		search();		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		
		return SUCCESS;
	}

	public String assignTimesheetTogether(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		String[] reqRmvTimeSt = requestedTimeSt.split("\\,");
		for (int x=0; x<reqRmvTimeSt.length; x++){		
			TimeSheet timeSheet=(TimeSheet)timeSheetManager.get(Long.parseLong(reqRmvTimeSt[x].toString()));
			idList.add(Long.parseLong(reqRmvTimeSt[x].toString()));
 	    	countTimeSheetUpdate++;
	     }
		timeSheet = timeSheetManager.get(Long.parseLong(idList.get(0).toString()));
		getComboList(sessionCorpID,"a");		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return INPUT;
	}
	
	
	public String getRequestedTimeSt() {
		return requestedTimeSt;
	}

	public void setRequestedTimeSt(String requestedTimeSt) {
		this.requestedTimeSt = requestedTimeSt;
	}

	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		timeSheet = timeSheetManager.get(id);
		
		List regHours= timeSheetManager.findRegularHours(timeSheet.getTicket(),timeSheet.getCrewName(),timeSheet.getWorkDate());
		if(!regHours.isEmpty())
		{
		if(regHours.get(0)==null)
		{
		List beginHoursTime=timeSheetManager.getBiginHoursTime(timeSheet.getTicket(),timeSheet.getCrewName(), timeSheet.getWorkDate());
		if((!beginHoursTime.isEmpty()) && (beginHoursTime.get(0)!=null))
		{
			timeSheet.setBeginHours(beginHoursTime.get(0).toString());
			
		}
		}
		}
		/*List wktLst = timeSheetManager.findTicket(timeSheet.getTicket());
		if(!(wktLst == null || wktLst.isEmpty())){
			workTicket = (WorkTicket) timeSheetManager.findTicket(timeSheet.getTicket()).get(0);
		}*/
		getComboList(sessionCorpID,"a");		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	public String save(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		getComboList(sessionCorpID,"a");
		boolean isNew = (timeSheet.getId() == null) ;
		String test="";
		String tempString="";
		if(!idList.isEmpty())
		{
		Iterator it = idList.iterator();
		while(it.hasNext())
		{
			test=(String)it.next();
				
		}
		
			tempString=test.substring(1, test.length()-1);
		}
			//StringTokenizer st =new StringTokenizer(tempString,",");
			
			//while(st.hasMoreElements()){
			//String idNo = st.nextToken();
							
			if(timeSheet.getLunch() == null){
				timeSheet.setLunch("No lunch");
			}
			if(!(timeSheet.getPaidRevenueShare() == null) && timeSheet.getPaidRevenueShare().equals("")){
				timeSheet.setPaidRevenueShare(new BigDecimal(0));
			}
			timeSheet.setUpdatedOn(new Date());
			timeSheet.setUpdatedBy(getRequest().getRemoteUser());
			if(timeSheet.getAction().equalsIgnoreCase("C")){
			if(timeSheet.getCalculationCode().toString().equalsIgnoreCase(""))
			{
				timeSheet.setCalculationCode("H");
			}
			if(timeSheet.getDistributionCode().toString().equalsIgnoreCase(""))
			{
				timeSheet.setDistributionCode("220");
			}
			}
			timeSheet.setPreviewRegularHours(timeSheet.getRegularHours());
			timeSheetManager.save(timeSheet);
			if(tempString.equalsIgnoreCase(""))
			{
				
				//System.out.println("\n\n\n\n single upperone");
				timeSheetManager.updateDoneForTheDay(timeSheet.getCrewName(),timeSheet.getWorkDate(),timeSheet.getDoneForTheDay());
			
			}
			
			if(!tempString.equalsIgnoreCase(""))
			{
				//System.out.println("\n\n\n\n updateone-->>>");
				timeSheetManager.updateOtherTimeSheet(timeSheet.getBeginHours(),timeSheet.getEndHours(),timeSheet.getRegularHours().toString(),timeSheet.getDoneForTheDay(),timeSheet.getPaidRevenueShare().toString(),tempString.trim(),timeSheet.getAction(), timeSheet.getLunch(),timeSheet.getReasonForAdjustment(),timeSheet.getOverTime().toString(),timeSheet.getDoubleOverTime().toString(), timeSheet.getDifferentPayBasis(),timeSheet.getAdjustmentToRevenue().toString(), timeSheet.getAdjustedBy(), timeSheet.getCalculationCode(), timeSheet.getDistributionCode() );
			}
			
			
			StringTokenizer st1 =new StringTokenizer(tempString,",");
			if(!tempString.equalsIgnoreCase(""))
			{
			while(st1.hasMoreElements()){
				String idNo1 = st1.nextToken();
				TimeSheet timeSheetNew=timeSheetManager.get(Long.parseLong(idNo1.trim()));
			
				//System.out.println("\n\n\n\n upperOne--->");
				timeSheetManager.updateDoneForTheDay(timeSheetNew.getCrewName(),timeSheetNew.getWorkDate(),timeSheet.getDoneForTheDay());
			
			}
			}
			
			StringTokenizer st =new StringTokenizer(tempString,",");
			if(tempString.equalsIgnoreCase(""))
			{/*
				Date wDate = timeSheet.getWorkDate();
				String cName = timeSheet.getCrewName();
				String cType = timeSheet.getCrewType();
				String wHouse = timeSheet.getWarehouse();
				String uName = timeSheet.getUserName();
				String beginHrs = timeSheet.getBeginHours();
				String endHrs = timeSheet.getEndHours();
				String lunchOpt = timeSheet.getLunch();
				String dFD=timeSheet.getDoneForTheDay();
				
				List existsTS = timeSheetManager.existsTimeSheet(wDate,uName,wHouse);
				if(!existsTS.isEmpty())
				{
					timeSheetManager.deleteLunchTicket(wDate, uName, wHouse);
				}
				List existsTS1 = timeSheetManager.existsTimeSheet(wDate,uName,wHouse);
				//System.out.println("\n\n\n\n existsTS--->"+existsTS);
				//System.out.println("\n\n\n\n timeSheet.getLunch()--->"+timeSheet.getLunch());
				if((!timeSheet.getLunch().equalsIgnoreCase("No lunch")) &&(existsTS1 == null || existsTS1.isEmpty() || existsTS1.get(0) == null || (existsTS1.get(0).toString()).equalsIgnoreCase(""))){
					timeSheet = new TimeSheet();
					timeSheet.setWorkDate(wDate);
					timeSheet.setCrewName(cName);
					timeSheet.setWarehouse(wHouse);
					timeSheet.setCrewType(cType);
					timeSheet.setUserName(uName);
					timeSheet.setCorpID(sessionCorpID);
					timeSheet.setAction("L");
					timeSheet.setRegularHours("0.5");
					timeSheet.setLunch(lunchOpt);
					timeSheet.setDistributionCode("999");
					timeSheet.setShipper("");
					timeSheet.setDoneForTheDay(dFD);
					timeSheet.setTicket(Long.parseLong("0"));
					if(timeSheet.getLunch().equalsIgnoreCase("Lunch @ beginning")){
						timeSheet.setBeginHours(beginHrs);
						Long totalBeginTimeMnts =  Long.parseLong(beginHrs.substring(0, beginHrs.indexOf(":")))*60 + Long.parseLong(beginHrs.substring(beginHrs.indexOf(":")+1, beginHrs.length())) + 30 ;
						String endHr = String.valueOf(totalBeginTimeMnts/60)+":"+String.valueOf(totalBeginTimeMnts%60) ;
						//endHr = beginHrs.substring(0, beginHrs.indexOf(":")) + ":" + String.valueOf(Long.parseLong(beginHrs.substring(beginHrs.indexOf(":")+1, beginHrs.length()))+30);
						timeSheet.setEndHours(endHr);
					}else{
						if(timeSheet.getLunch().equalsIgnoreCase("Lunch @ end")){
							Long totalEndTimeMnts =  Long.parseLong(endHrs.substring(0, endHrs.indexOf(":")))*60 + Long.parseLong(endHrs.substring(endHrs.indexOf(":")+1, endHrs.length())) - 30 ;
							String beginHr = String.valueOf(totalEndTimeMnts/60)+":"+String.valueOf(totalEndTimeMnts%60) ;
							//String beginHr = endHrs.substring(0, endHrs.indexOf(":")) + ":" + String.valueOf(Long.parseLong(endHrs.substring(endHrs.indexOf(":")+1, endHrs.length()))-30);
							timeSheet.setBeginHours(beginHr);
							timeSheet.setEndHours(endHrs);
						}
					}
					timeSheet.setCreatedOn(new Date());
					timeSheet.setUpdatedOn(new Date());
					timeSheet.setUpdatedBy(getRequest().getRemoteUser());
					timeSheet.setCreatedBy(getRequest().getRemoteUser());
					if(dFD.equalsIgnoreCase("true")){
						//System.out.println("\n\n\n\n single upperone1");
						timeSheetManager.updateDoneForTheDay(cName,wDate,timeSheet.getDoneForTheDay());
						//System.out.println("\n\n\n\n single upperone11");
					}
					timeSheetManager.save(timeSheet);
					
					if(dFD.equalsIgnoreCase("true")){
						//System.out.println("\n\n\n\n single upperone2");
						timeSheetManager.updateDoneForTheDay(cName,wDate,timeSheet.getDoneForTheDay());
						//System.out.println("\n\n\n\n single upperone22");
					}
				}
				 
		        getComboList(sessionCorpID,"a");
		        SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd-MMM-yyyy");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( timeSheet.getWorkDate() ) );
		        workDt = nowYYYYMMDD.toString();
		        //wareHse = timeSheet.getWarehouse();
			*/}
			if(!tempString.equalsIgnoreCase(""))
			{/*
			while(st.hasMoreElements()){
			String idNo = st.nextToken();
			TimeSheet timeSheetNew=timeSheetManager.get(Long.parseLong(idNo.trim()));
			Date wDate = timeSheetNew.getWorkDate();
			String cName = timeSheetNew.getCrewName();
			String cType = timeSheetNew.getCrewType();
			String wHouse = timeSheetNew.getWarehouse();
			String uName = timeSheetNew.getUserName();
			String beginHrs = timeSheetNew.getBeginHours();
			String endHrs = timeSheetNew.getEndHours();
			String lunchOpt = timeSheet.getLunch();
			String dFD=timeSheet.getDoneForTheDay();
			
			//System.out.println("\n\n\n\n dFD-->"+dFD);
			List existsTS = timeSheetManager.existsTimeSheet(wDate,uName,wHouse);
			//System.out.println("\n\n\n\n existsTS--->"+existsTS);
			//System.out.println("\n\n\n\n timeSheet.getLunch()--->"+timeSheet.getLunch());
			
			if((!timeSheet.getLunch().equalsIgnoreCase("No lunch")) &&(existsTS == null || existsTS.isEmpty() || existsTS.get(0) == null || (existsTS.get(0).toString()).equalsIgnoreCase(""))){
				timeSheet = new TimeSheet();
				timeSheet.setWorkDate(wDate);
				timeSheet.setCrewName(cName);
				timeSheet.setWarehouse(wHouse);
				timeSheet.setCrewType(cType);
				timeSheet.setUserName(uName);
				timeSheet.setCorpID(sessionCorpID);
				timeSheet.setAction("L");
				timeSheet.setRegularHours("0.5");
				timeSheet.setLunch(lunchOpt);
				timeSheet.setDistributionCode("999");
				timeSheet.setShipper("");
				timeSheet.setDoneForTheDay(dFD);
				timeSheet.setTicket(Long.parseLong("0"));
				if(timeSheet.getLunch().equalsIgnoreCase("Lunch @ beginning")){
					timeSheet.setBeginHours(beginHrs);
					Long totalBeginTimeMnts =  Long.parseLong(beginHrs.substring(0, beginHrs.indexOf(":")))*60 + Long.parseLong(beginHrs.substring(beginHrs.indexOf(":")+1, beginHrs.length())) + 30 ;
					String endHr = String.valueOf(totalBeginTimeMnts/60)+":"+String.valueOf(totalBeginTimeMnts%60) ;
					//endHr = beginHrs.substring(0, beginHrs.indexOf(":")) + ":" + String.valueOf(Long.parseLong(beginHrs.substring(beginHrs.indexOf(":")+1, beginHrs.length()))+30);
					timeSheet.setEndHours(endHr);
				}else{
					if(timeSheet.getLunch().equalsIgnoreCase("Lunch @ end")){
						Long totalEndTimeMnts =  Long.parseLong(endHrs.substring(0, endHrs.indexOf(":")))*60 + Long.parseLong(endHrs.substring(endHrs.indexOf(":")+1, endHrs.length())) - 30 ;
						String beginHr = String.valueOf(totalEndTimeMnts/60)+":"+String.valueOf(totalEndTimeMnts%60) ;
						//String beginHr = endHrs.substring(0, endHrs.indexOf(":")) + ":" + String.valueOf(Long.parseLong(endHrs.substring(endHrs.indexOf(":")+1, endHrs.length()))-30);
						timeSheet.setBeginHours(beginHr);
						timeSheet.setEndHours(endHrs);
					}
				}
				timeSheet.setCreatedOn(new Date());
				timeSheet.setUpdatedOn(new Date());
				timeSheet.setUpdatedBy(getRequest().getRemoteUser());
				timeSheet.setCreatedBy(getRequest().getRemoteUser());
				if(dFD.equalsIgnoreCase("true")){
					timeSheetManager.updateDoneForTheDay(cName,wDate,timeSheet.getTicket());
				}
				timeSheetManager.save(timeSheet);
				
				if(dFD.equalsIgnoreCase("true")){
					timeSheetManager.updateDoneForTheDay(cName,wDate,timeSheet.getTicket());
				}
			}
			if(dFD.equalsIgnoreCase("true")){
				//System.out.println("\n\n\n\n lowerone-->");
				timeSheetManager.updateDoneForTheDay(cName,wDate,timeSheet.getDoneForTheDay());
			}
	        getComboList(sessionCorpID,"a");
	        SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("dd-MMM-yyyy");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( timeSheet.getWorkDate() ) );
	        workDt = nowYYYYMMDD.toString();
	        //wareHse = timeSheet.getWarehouse();
	        //tktWareHse=timeSheet.getWarehouse();
			}
			*/}
			String key = (isNew) ? "timeSheet.added" : "timeSheet.updated";   
	        saveMessage(getText(key)); 	        
	        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	    if(whoticket.equalsIgnoreCase("ticketCrews")){
	    	hitFlag="1";
	    	return INPUT;
	    }else{
		return search();
	    }
	}
	private String tktWareHse;
	private String wareHse;
	private String serv;
	private String workDt;
	private String crewNm;
	private String shipper;
	private String tkt;
	private StringBuilder newFwdDate;
	private String newWrkDt;
	private String editable;
	private String whoticket;
	private Long whid;
	private Long absentId;
	public String search() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		try{
			if(absentId!=null){timeSheetManager.remove(absentId);}
		List dateDiff= timeSheetManager.checkStopDate(workDt, sessionCorpID, wareHse);
		if(!dateDiff.isEmpty() && (dateDiff.get(0)!=null))
		{
		if(Long.parseLong(dateDiff.get(0).toString())>=0)
		{
			editable="Yes";
		}
		else
		{
			editable="No";
			saveMessage("Payroll Processing Underway, you can only view the records."); 
		}
		}
		
		List ls = new ArrayList();
		if(!workDt.equalsIgnoreCase("")){
	    try {
	    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
	        newWrkDt = newFwdDate.toString();
	        //System.out.println(newFwdDate.toString());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}   
		}else{
			newWrkDt = "";
		}
	      ls = timeSheetManager.findTickets(tktWareHse,"",newWrkDt,"",tkt,sessionCorpID);
	      availableCrews = timeSheetManager.searchAvailableCrews(wareHse,newWrkDt,crewNm,sessionCorpID);
	    
	    workTickets = new HashSet(ls);
	    //availableCrews = new HashSet(ls1);
    	timeSheets = timeSheetManager.findTimeSheets(wareHse,newWrkDt, "", tkt, crewNm, sessionCorpID);
    	getSession().setAttribute("countTimeSheet", timeSheets.size());
    	getComboList(sessionCorpID,"a");
		}catch(Exception ex)
		{
			ex.printStackTrace();	
		}	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
        return SUCCESS;     
    } 
	private  Map<String, String> timetype;
	private  Map<String, String> crewtype;
	private  Map<String, String> pay_ab;
	private  Map<String, String> distcode;
	private  Map<String, String> revcalc;
	private  Map<String, String> wareHouse;
	private  Map<String, String> tcktservc;
	private  Map<String, String> serviceType;
	private  String serviceGroup;
	private  Map<String, String> tcktGrp;
	private  Map<String, String> crewDeviceList;
	private  Map<String, String> mmDeviceMap;
	private  List lunchOptions;
	private RefMasterManager refMasterManager;
	public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }
	public String getComboList(String corpId, String jobType){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		timetype = refMasterManager.findByParameter(corpId, "TIMETYPE");
		crewtype = refMasterManager.findByParameter(corpId, "CREWTYPE");
		pay_ab = refMasterManager.findByParameter(corpId, "PAY_AB");
		distcode= refMasterManager.findByParameter(corpId, "PAYROLL");
		revcalc= refMasterManager.findByParameter(corpId, "REVCALC");
		wareHouse= refMasterManager.findByParameter(corpId, "HOUSE");
		tcktservc = refMasterManager.findByParameter(corpId, "TCKTSERVC");
		tcktGrp = refMasterManager.findByParameter(corpId, "TICKETGROUP");
		crewDeviceList = refMasterManager.findByParameter(corpId, "CREW-DEVICE");
		tcktactn = refMasterManager.findByParameter(sessionCorpID, "TCKTACTN");
		mmDeviceMap = refMasterManager.getFlex1MapByParameter(corpId, "MM-DEVICE");
		//serviceType= refMasterManager.findByParameterService(sessionCorpID, "TCKTSERVC", "");
		lunchOptions = new ArrayList();
		lunchOptions.add("No&nbsp;lunch");
		lunchOptions.add("Lunch&nbsp;@&nbsp;Included");
		//lunchOptions.add("Lunch&nbsp;@&nbsp;end");		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}

	public  Map<String, String> getTimetype() {
		return timetype;
	}

	public  void setTimetype(Map<String, String> timetype) {
		this.timetype = timetype;
	}

	public  Map<String, String> getCrewtype() {
		return crewtype;
	}

	public  void setCrewtype(Map<String, String> crewtype) {
		this.crewtype = crewtype;
	}

	public  Map<String, String> getPay_ab() {
		return pay_ab;
	}

	public  void setPay_ab(Map<String, String> pay_ab) {
		this.pay_ab = pay_ab;
	}

	public Map<String, String> getDistcode() {
		return distcode;
	}

	public void setDistcode(Map<String, String> distcode) {
		this.distcode = distcode;
	}

	public Map<String, String> getRevcalc() {
		return revcalc;
	}

	public void setRevcalc(Map<String, String> revcalc) {
		this.revcalc = revcalc;
	}
	private String crewCorpId;
	private List timeHours;
	public String sysDefaultData(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		timeHours = timeSheetManager.getTimeHoursFormSysDefault(crewCorpId);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	private String ticket;
	private String existRevenve;
	public String processPayrollRevised(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		existRevenve = (timeSheetManager.totalRevenueShare(Long.parseLong(ticket)).toString());	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}

	public String getCrewCorpId() {
		return crewCorpId;
	}

	public void setCrewCorpId(String crewCorpId) {
		this.crewCorpId = crewCorpId;
	}

	public List getTimeHours() {
		return timeHours;
	}

	public void setTimeHours(List timeHours) {
		this.timeHours = timeHours;
	}

	public List<AvailableCrew> getAvailableCrews() {
		return availableCrews;
	}

	public void setAvailableCrews(List<AvailableCrew> availableCrews) {
		this.availableCrews = availableCrews;
	}
	
	private List dayDistributionHoursDTOs;
	private String totalRevenue;

	private Date workdtTmp;
	private String perHrs;
	// for pay distribution  
	private Double hrsxPerDist;
	private Double percentageFactor;
	private String payment;
	private String disablePayroll;
	private ServiceOrder serviceOrder;
	private MiscellaneousManager miscellaneousManager;
	private BillingManager billingManager;
	private Miscellaneous miscellaneous;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private CustomerFile customerFile;
	private String surveyTool;
	private int moveCount;
	private String emailSetupValue;
	private EmailSetupManager emailSetupManager;
	private  IntegrationLogInfoManager  integrationLogInfoManager;
	public void setIntegrationLogInfoManager(
			IntegrationLogInfoManager integrationLogInfoManager) {
		this.integrationLogInfoManager = integrationLogInfoManager;
	}
	public String findCrewByTicket(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		if(id!=null){
			workTicket = workTicketManager.get(id);
		}else{		
			workTicket = workTicketManager.get(workTicket.getId());
		}
		if(hitFlag!=null && hitFlag.equalsIgnoreCase("payRoll")){
			   processPayroll(); 
		}else if(hitFlag!=null && hitFlag.equalsIgnoreCase("Hourly")){
			   timeSheetManager.updateCrewType(workTicket.getTicket().toString());
			   processPayroll(); 
		}else{
		serviceOrder=workTicket.getServiceOrder();
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(serviceOrder.getId());
		trackingStatus=trackingStatusManager.get(serviceOrder.getId());
		billing = billingManager.get(serviceOrder.getId());
		customerFile = serviceOrder.getCustomerFile(); 
		
		surveyTool = customerFileManager.findSurveyTool(serviceOrder.getCorpID(),serviceOrder.getJob(),serviceOrder.getCompanyDivision());
		surveyTool=surveyTool.substring(1,surveyTool.length()-1);
		if(surveyTool.equalsIgnoreCase("Voxme")){
		emailSetupValue=emailSetupManager.findEmailStatus(sessionCorpID, customerFile.getSequenceNumber());
		}	
		else{
		emailSetupValue=integrationLogInfoManager.findSendMoveCloudStatusCrew(sessionCorpID, customerFile.getSequenceNumber());
		if(emailSetupValue!= null && !emailSetupValue.equals("")){
			moveCount=1;
		}
        }
		
		try{
			List dateDiff= timeSheetManager.checkStopDateForCrew(workTicket.getDate1(), sessionCorpID, workTicket.getWarehouse());
			if(!dateDiff.isEmpty() && (dateDiff.get(0)!=null)){
				if(Long.parseLong(dateDiff.get(0).toString())>=0){
					editable="Yes";
				}else{
					editable="No";
					saveMessage("Payroll Processing Underway, you can only view the records."); 
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();	
		}
		
		String expression;
		Double revShare = 0.0;
		//workTicket = workTicketManager.get(id);
		List<PayrollAllocation> plst = timeSheetManager.findDistAndCalcCode(workTicket.getJobType(), workTicket.getService(), sessionCorpID,serviceOrder.getMode(),workTicket.getCompanyDivision());
		if(plst == null || plst.isEmpty() || plst.get(0) == null){
			payrollAllocation = new PayrollAllocation();
			expression = "0" ;
		}else{
			payrollAllocation = (PayrollAllocation) plst.get(0);
			expression = timeSheetManager.findExpression(payrollAllocation.getCalculation(), sessionCorpID,workTicket.getCompanyDivision());
		}
		//String expression = timeSheetManager.findExpression(payrollAllocation.getCalculation());
		List totalHoursWorked = timeSheetManager.findTotalHoursWorked(workTicket.getTicket());
		if(totalHoursWorked == null || totalHoursWorked.isEmpty() || totalHoursWorked.get(0) == null){
			errorMessage("Hours not allocated to crews timesheets, cannot proceed to process payroll."); 
			disablePayroll = "true";
		}
		//List<TimeSheet> timeSheetList = timeSheetManager.findWorkDays(workTicket.getTicket());
		
		/////////////////////////////////////////////get the list of day distribution and total revenue////////////////////////////////////////////
		
		 List list = new ArrayList();
		 //List totalHoursWorked = findTotalHoursWorked(ticket); 
		 List distHours = timeSheetManager.findDayDistributionHoursList(workTicket.getTicket(), String.valueOf(workTicket.getActualWeight()));		 
		 Iterator it = distHours.iterator();
		 while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           DayDistributionHoursDTO dayDistributionHoursDTO = new DayDistributionHoursDTO();	           
	           if(row[0] == null){
	        	   dayDistributionHoursDTO.setWorkDate(null);
	        	   }else{
	        		   String newWorkDate = row[0].toString();
	    	           newWorkDate=newWorkDate.replace(".0", "");	    	           
	    				try {
	    					workdtTmp = new SimpleDateFormat("yyyy-MM-dd").parse(newWorkDate);
	    				} catch (ParseException e) {
	    					
	    					e.printStackTrace();
	    				}
	        		   dayDistributionHoursDTO.setWorkDate(workdtTmp);
	        		   }
	           
	           if(row[1] == null || row[1].toString().equalsIgnoreCase("0") || row[1].toString().equalsIgnoreCase("0.0")){dayDistributionHoursDTO.setTotalHours("");}else{dayDistributionHoursDTO.setTotalHours(row[1].toString());}
	           if(row[1] == null || row[1].toString().equalsIgnoreCase("0") || row[1].toString().equalsIgnoreCase("0.0")){
	        	   dayDistributionHoursDTO.setPercentHours("0.0");
	        	   perHrs = "0.0";
	           }else{
	        	   perHrs = String.valueOf(Double.parseDouble(row[1].toString())/Double.parseDouble(totalHoursWorked.get(0).toString()));
	        	   dayDistributionHoursDTO.setPercentHours(String.valueOf(Double.parseDouble(perHrs)*100));
	           }
	           if(row[2]==null){
	        	   dayDistributionHoursDTO.setOtHours("0.0");
	           }else{
	        	   dayDistributionHoursDTO.setOtHours(row[2].toString());
	           }
	           Double wtShare=0.0;
	           try{
		           wtShare = Double.parseDouble(perHrs)*workTicket.getActualWeight();
		           dayDistributionHoursDTO.setWeightShare(String.valueOf(wtShare));
		           }catch(Exception ex){
		        	   errorMessage("Payroll cannot be processed as Actual Weight is missing");
		        	   ex.printStackTrace();
		           }
		           //String revShr = String.valueOf(Double.parseDouble(perHrs)*Double.parseDouble(totRev));
		           WorkTicket workTicketNew=(WorkTicket)timeSheetManager.getWorkTicket(workTicket.getTicket(),sessionCorpID).get(0);
		           List billingDiscount=  timeSheetManager.getBillingDiscount(workTicketNew.getShipNumber(), sessionCorpID);
		           Double reven;
		           if(workTicket.getRevenue() == null){
		        	   reven = Double.parseDouble("0.0");
		           }else{
		        	   reven = workTicket.getRevenue();
		           }
		           
		           try{
		            Map values = new HashMap();
					values.put("timeSheet.workDate", workdtTmp);
					values.put("workTicket.actualWeight", wtShare);
					values.put("workTicket.revenue", reven);
					if(billingDiscount.get(0)==null)
					{
						values.put("billing.discount", 0.0);
					}else{
						values.put("billing.discount", Double.parseDouble(billingDiscount.get(0).toString()));
					}
					if(workTicketNew.getExtraPayroll()==null)
					{
						values.put("workTicket.extraPayroll", 0.0);
					}else
					{
						values.put("workTicket.extraPayroll", workTicketNew.getExtraPayroll());
					}
					Object totRev = expressionManager.executeExpression(expression, values);
					revShare = revShare + Double.parseDouble(String.valueOf(totRev));
					//System.out.println(revShare);
		           dayDistributionHoursDTO.setRevenueShare(String.valueOf(totRev));
		           if(row[1] == null || row[1].toString().equalsIgnoreCase("0") || row[1].toString().equalsIgnoreCase("0.0")){
		        	   dayDistributionHoursDTO.setPerHour("");
		           }else{
		        	   dayDistributionHoursDTO.setPerHour(String.valueOf(Double.parseDouble(String.valueOf(totRev))/Double.parseDouble(row[1].toString())));
		           }
	           }catch(Exception ex){
	        	   errorMessage("Error in the Formula");
	        	   ex.printStackTrace();
	           }
	           
	           list.add(dayDistributionHoursDTO);
	       }
		 totalRevenue = String.valueOf(revShare);
		 dayDistributionHoursDTOs = list;
		}
		/////////////////////////////////////////////////////////////////////////////////////////
		getComboList(sessionCorpID,"a");
		timeSheets = timeSheetManager.findTimeSheetByTicket(workTicket.getTicket());
		checkDriver = timeSheetManager.findIssuedDriverCheck(sessionCorpID, workTicket.getTicket());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	private String checkDriver;
	private String calcCode;
	private String actWeight;
	private String rev;
	private String wrkDate;
	private Date newWrkDate;
	private ServiceOrderManager serviceOrderManager;
	
	public String processPayroll(){	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		try{
			Double revShare = 0.0;
			if(rev == null || rev.equalsIgnoreCase("")){
				rev = "0.0";
			}
			getComboList(sessionCorpID,"a");
			//get the list of day distribution and total revenue
			WorkTicket workTicketNew=(WorkTicket)timeSheetManager.getWorkTicket(workTicket.getTicket(),sessionCorpID).get(0);
			serviceOrder = workTicketNew.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			String expression = timeSheetManager.findExpression(calcCode, sessionCorpID,workTicketNew.getCompanyDivision());
			List list = new ArrayList();
			List totalHoursWorked = timeSheetManager.findTotalHoursWorked(workTicket.getTicket());
			List distHours = timeSheetManager.findDayDistributionHoursList(workTicket.getTicket(), String.valueOf(workTicket.getActualWeight()));
			List billingDiscount=  timeSheetManager.getBillingDiscount(workTicketNew.getShipNumber(), sessionCorpID);
			Iterator it = distHours.iterator();
			while(it.hasNext()){
		           Object []row= (Object [])it.next();
		           DayDistributionHoursDTO dayDistributionHoursDTO = new DayDistributionHoursDTO();		           
		           if(row[0] == null){
		        	   dayDistributionHoursDTO.setWorkDate(null);
		           }else{
	        		   String newWorkDate = row[0].toString();
	    	           newWorkDate=newWorkDate.replace(".0", "");		    	           
	    				try {
	    					workdtTmp = new SimpleDateFormat("yyyy-MM-dd").parse(newWorkDate);
	    				} catch (ParseException e) {	    					
	    					e.printStackTrace();
	    				}
		        	   dayDistributionHoursDTO.setWorkDate(workdtTmp);
		           }
		           
		           if(row[1] == null || row[1].toString().equalsIgnoreCase("0") || row[1].toString().equalsIgnoreCase("0.0")){dayDistributionHoursDTO.setTotalHours("");}else{dayDistributionHoursDTO.setTotalHours(row[1].toString());}
		           if(row[1] == null || row[1].toString().equalsIgnoreCase("0") || row[1].toString().equalsIgnoreCase("0.0")){
		        	   dayDistributionHoursDTO.setPercentHours("0.0");
		        	   perHrs = "0.0";
		           }else{
		        	   perHrs = String.valueOf(Double.parseDouble(row[1].toString())/Double.parseDouble(totalHoursWorked.get(0).toString()));
		        	   dayDistributionHoursDTO.setPercentHours(String.valueOf(Double.parseDouble(perHrs)*100));
		           }
		           if(row[2]==null){
		        	   dayDistributionHoursDTO.setOtHours("0.0");
		           }else{
		        	   dayDistributionHoursDTO.setOtHours(row[2].toString());
		           }
		          
		           Double wtShare=0.0;
		           Double tktRevShare=0.0;
		           try{
			           if(workTicket.getActualWeight() == null){
			        	   wtShare = Double.parseDouble(perHrs)*Double.parseDouble(actWeight);
			           }else{
			        	   wtShare = Double.parseDouble(perHrs)*workTicket.getActualWeight();
			           }
			           dayDistributionHoursDTO.setWeightShare(String.valueOf(wtShare));
			           
			           if(workTicket.getRevenue()== null)
			           {
			        	   tktRevShare=Double.parseDouble(perHrs)*Double.parseDouble(rev);
			           }else
			           {
			        	   tktRevShare=Double.parseDouble(perHrs)*workTicket.getRevenue();
			           }
			           dayDistributionHoursDTO.setTicketRevenueShare(String.valueOf(tktRevShare));
			           //Double wtShare = Double.parseDouble(perHrs)*workTicket.getActualWeight();
			           
		           }catch(Exception ex){
		        	   errorMessage("Payroll cannot be processed as Actual Weight is missing");
		        	   ex.printStackTrace();
		           }
		           //String revShr = String.valueOf(Double.parseDouble(perHrs)*Double.parseDouble(totRev));
		           try{
			            Map values = new HashMap();
						values.put("timeSheet.workDate", workdtTmp);
						values.put("workTicket.actualWeight", wtShare);
						if(workTicket.getRevenue() == null){
							values.put("workTicket.revenue", 0.0);
						}else{
							values.put("workTicket.revenue", workTicket.getRevenue());
						}
						if(tktRevShare == null){
							values.put("ticket.revenue", 0.0);
						}else{
							values.put("ticket.revenue", tktRevShare);
						}
						if(billingDiscount.get(0)==null)
						{
							values.put("billing.discount", 0.0);
						}else{
							values.put("billing.discount", Double.parseDouble(billingDiscount.get(0).toString()));
						}
						if(workTicketNew.getExtraPayroll()==null)
						{
							values.put("workTicket.extraPayroll", 0.0);
						}else
						{
							values.put("workTicket.extraPayroll", workTicketNew.getExtraPayroll());
						}
						Object totRev = expressionManager.executeExpression(expression, values);
						revShare = revShare + Double.parseDouble(String.valueOf(totRev));
						//System.out.println(revShare);
			           
						dayDistributionHoursDTO.setRevenueShare(String.valueOf(totRev));
			           if(row[1] == null || row[1].toString().equalsIgnoreCase("0") || row[1].toString().equalsIgnoreCase("0.0")){
			        	   dayDistributionHoursDTO.setPerHour("");
			           }else{
			        	   dayDistributionHoursDTO.setPerHour(String.valueOf(Double.parseDouble(String.valueOf(totRev))/Double.parseDouble(row[1].toString())));
			           }
		           }catch(Exception e){	
		        	   errorMessage("Error in the Formula");
		        	   hitFlag="";
		        	   findCrewByTicket();		           
		           }
		           
	             ////////////////Pay distribution ///////////////////////////
		         List chekCH=timeSheetManager.checkCH(workTicket.getTicket(),workdtTmp,sessionCorpID);
		         if(chekCH.isEmpty()){
		           if(!calcCode.equalsIgnoreCase("H")){
		           List<TimeSheet> timeSheetList = timeSheetManager.findTimeSheetByTicketAndDate(workTicket.getTicket(),workdtTmp);
		          // String compDivision= timeSheetManager.getcompanyDivision(calcCode, sessionCorpID).get(0).toString();
		           List<CategoryRevenue> catRevNormalDist = timeSheetManager.findNormalDist(workTicket.getTicket(),workdtTmp, workTicketNew.getJobType(), workTicketNew.getService(), workTicketNew.getCompanyDivision());
		           if(catRevNormalDist == null || catRevNormalDist.isEmpty() || catRevNormalDist.get(0) == null){
		        	   errorMessage("Cannot do Payroll Calculations as Category Revenue combination is missing");  
		           }else{
		           CategoryRevenue categoryRevenue =  catRevNormalDist.get(0);
		           String percentDistDR = categoryRevenue.getPercentPerPerson1();
		           String percentDistCD = categoryRevenue.getPercentPerPerson2();
		           String percentDistHE = categoryRevenue.getPercentPerPerson3();
		           String percentDistCH = categoryRevenue.getPercentPerPerson4();
		           String percentDistWH = categoryRevenue.getPercentPerPerson5();
		           Double totalHrsxPerDist=0.0;
		           String regHrsTotal;
		           for(TimeSheet timeSheet:timeSheetList){
		        	   if(timeSheet.getRegularHours() == null || timeSheet.getRegularHours().equals("")){
		        		   regHrsTotal = new BigDecimal(0).toString();
		        	   }else{
		        		   regHrsTotal = timeSheet.getRegularHours().toString();
		        	   }
		        	   
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("DR")){hrsxPerDist = (Double.parseDouble(percentDistDR)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CD")){hrsxPerDist = (Double.parseDouble(percentDistCD)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("HE")){hrsxPerDist = (Double.parseDouble(percentDistHE)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CH")){hrsxPerDist = (Double.parseDouble(percentDistCH)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("WH")){hrsxPerDist = (Double.parseDouble(percentDistWH)*Double.parseDouble(regHrsTotal))/100;}
		        	   totalHrsxPerDist = totalHrsxPerDist + hrsxPerDist;
		        	   
		           }
		           for(TimeSheet timeSheet:timeSheetList){
		        	   if(timeSheet.getRegularHours() == null || timeSheet.getRegularHours().equals("")){
		        		   regHrsTotal = new BigDecimal(0).toString();
		        	   }else{
		        		   regHrsTotal = timeSheet.getRegularHours().toString();
		        	   }
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("DR")){hrsxPerDist = (Double.parseDouble(percentDistDR)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CD")){hrsxPerDist = (Double.parseDouble(percentDistCD)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("HE")){hrsxPerDist = (Double.parseDouble(percentDistHE)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CH")){hrsxPerDist = (Double.parseDouble(percentDistCH)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("WH")){hrsxPerDist = (Double.parseDouble(percentDistWH)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(totalHrsxPerDist == 0.0){
		        		   payment = null;
		        	   }else{
			        	   percentageFactor = (hrsxPerDist/totalHrsxPerDist);
			        	   if(dayDistributionHoursDTO.getRevenueShare() == null || dayDistributionHoursDTO.getRevenueShare().equals("")){
			        		   payment = null;
			        	   }else{
			        		   payment = String.valueOf(percentageFactor*Double.parseDouble(dayDistributionHoursDTO.getRevenueShare()));
			        	   }
		        	   }
		        	   
		        	   //Double paymentDouble=Double.parseDouble(payment);
		        	   //Double m=new Double(Math.round((paymentDouble*10000)/10000));
		        	   if(payment!=null){
		        		   timeSheet.setPaidRevenueShare(new BigDecimal(payment));   
		        	   }else{
		        		   timeSheet.setPaidRevenueShare(new BigDecimal(0));
		        	   }
		        	   timeSheet.setCalculationCode(calcCode);
		        	   timeSheetManager.save(timeSheet);
		            }
		          }
		        list.add(dayDistributionHoursDTO);
		      }
		    }else{		    	  
		    	  List<TimeSheet> timeSheetList = timeSheetManager.findTimeSheetByTicketAndDate(workTicket.getTicket(),workdtTmp);
		          //int totalCrew= timeSheetList.size();
		          //List totalCHList=timeSheetManager.checkCH(workTicket.getTicket(),workdtTmp,sessionCorpID);
		          //int totalCH=totalCHList.size();
		          //int nonCH=totalCrew-totalCH;
		          //int adjustAmount=totalCH*6;
		         // int baseFactor=adjustAmount/nonCH;
		    	  //String compDivision= timeSheetManager.getcompanyDivision(calcCode, sessionCorpID).get(0).toString();
		           List<CategoryRevenue> catRevNormalDist = timeSheetManager.findNormalDist(workTicket.getTicket(),workdtTmp, workTicketNew.getJobType(), workTicketNew.getService(), workTicketNew.getCompanyDivision());
		           if(catRevNormalDist == null || catRevNormalDist.isEmpty() || catRevNormalDist.get(0) == null){
		        	   errorMessage("Cannot do Payroll Calculations as Category Revenue combination is missing");  
		        	}else{
		           CategoryRevenue categoryRevenue =  catRevNormalDist.get(0);
		           /*BigDecimal percentDistDRTemp= new BigDecimal(categoryRevenue.getPercentPerPerson1()).add(new BigDecimal(baseFactor));
		           String percentDistDR = percentDistDRTemp.toString();
		           BigDecimal percentDistCDTemp= new BigDecimal(categoryRevenue.getPercentPerPerson2()).add(new BigDecimal(baseFactor));
		           String percentDistCD = percentDistCDTemp.toString();
		           BigDecimal percentDistHETemp= new BigDecimal(categoryRevenue.getPercentPerPerson3()).add(new BigDecimal(baseFactor));
		           String percentDistHE = percentDistHETemp.toString();
		           BigDecimal percentDistWHTemp= new BigDecimal(categoryRevenue.getPercentPerPerson5()).add(new BigDecimal(baseFactor));
		           String percentDistWH = percentDistWHTemp.toString();*/
		           //BigDecimal percentDistCHTemp= new BigDecimal(categoryRevenue.getPercentPerPerson4()).add(new BigDecimal(baseFactor));
		           //String percentDistCH = percentDistCHTemp.toString();
		           String percentDistDR = categoryRevenue.getPercentPerPerson1();
		           String percentDistCD = categoryRevenue.getPercentPerPerson2();
		           String percentDistHE = categoryRevenue.getPercentPerPerson3();
		           String percentDistCH = categoryRevenue.getPercentPerPerson4();
		           String percentDistWH = categoryRevenue.getPercentPerPerson5();
		           Double totalHrsxPerDist=0.0;
		           String regHrsTotal;
		           for(TimeSheet timeSheet:timeSheetList){
		        	   if(timeSheet.getRegularHours() == null || timeSheet.getRegularHours().equals("")){
		        		   regHrsTotal = "0.0";
		        	   }else{
		        		   regHrsTotal = timeSheet.getRegularHours().toString();
		        	   }
		        	   
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("DR")){hrsxPerDist = (Double.parseDouble(percentDistDR)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CD")){hrsxPerDist = (Double.parseDouble(percentDistCD)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("HE")){hrsxPerDist = (Double.parseDouble(percentDistHE)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("WH")){hrsxPerDist = (Double.parseDouble(percentDistWH)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CH")){hrsxPerDist = (Double.parseDouble(percentDistCH)*Double.parseDouble(regHrsTotal))/100;}
		        	   totalHrsxPerDist = totalHrsxPerDist + hrsxPerDist;
		        	   
		        	   
		           }
		           
		           for(TimeSheet timeSheet:timeSheetList){
		        	   if(timeSheet.getRegularHours() == null || timeSheet.getRegularHours().equals("")){
		        		   regHrsTotal = "0.0";
		        	   }else{
		        		   regHrsTotal = timeSheet.getRegularHours().toString();
		        	   }
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("DR")){hrsxPerDist = (Double.parseDouble(percentDistDR)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CD")){hrsxPerDist = (Double.parseDouble(percentDistCD)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("HE")){hrsxPerDist = (Double.parseDouble(percentDistHE)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CH")){hrsxPerDist = (Double.parseDouble(percentDistCH)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("WH")){hrsxPerDist = (Double.parseDouble(percentDistWH)*Double.parseDouble(regHrsTotal))/100;}
		        	   if(totalHrsxPerDist == 0.0){
		        		   payment = null;
		        	   }else{
			        	   percentageFactor = (hrsxPerDist/totalHrsxPerDist);
			        	   if(dayDistributionHoursDTO.getRevenueShare() == null || dayDistributionHoursDTO.getRevenueShare().equals("")){
			        		   payment = null;
			        	   }else{
			        		   payment = String.valueOf(percentageFactor*Double.parseDouble(dayDistributionHoursDTO.getRevenueShare()));
			        	   }
		        	   }
		        	   
		        	   //Double paymentDouble=Double.parseDouble(payment);
		        	   //Double m=new Double(Math.round((paymentDouble*10000)/10000));
		        	   if(payment!=null){
		        		   timeSheet.setPaidRevenueShare(new BigDecimal(payment));   
		        	   }else{
		        		   timeSheet.setPaidRevenueShare(new BigDecimal(0));
		        	   }
		        	   timeSheet.setCalculationCode(calcCode);
		        	   timeSheetManager.save(timeSheet);
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CH") || timeSheet.getCrewType().equalsIgnoreCase("CD") || timeSheet.getCrewType().equalsIgnoreCase("NC")){
		        		   timeSheet.setPaidRevenueShare(new BigDecimal(0));
			        	   timeSheet.setCalculationCode("H");
			        	   timeSheetManager.save(timeSheet);
		        	   }
		           }
		           /*for(TimeSheet timeSheet:timeSheetList){
		        	   if(timeSheet.getRegularHours() == null || timeSheet.getRegularHours().equals("")){
		        		   regHrsTotal = "0.0";
		        	   }else{
		        		   regHrsTotal = timeSheet.getRegularHours();
		        	   }
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("DR")){
		        		   percentageFactor=Double.parseDouble(percentDistDR);
		        		   //hrsxPerDist = (Double.parseDouble(percentDistDR)*Double.parseDouble(regHrsTotal))/100;
		        	   }
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CD")){
		        		   percentageFactor=Double.parseDouble(percentDistCD);
		        		   //hrsxPerDist = (Double.parseDouble(percentDistCD)*Double.parseDouble(regHrsTotal))/100;
		        	   }
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("HE")){
		        		   percentageFactor=Double.parseDouble(percentDistHE);
		        		   //hrsxPerDist = (Double.parseDouble(percentDistHE)*Double.parseDouble(regHrsTotal))/100;
		        		}
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("WH")){
		        		   percentageFactor=Double.parseDouble(percentDistWH);
		        		  // hrsxPerDist = (Double.parseDouble(percentDistWH)*Double.parseDouble(regHrsTotal))/100;
		        		 }
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CH")){
		        		   percentageFactor=Double.parseDouble(percentDistCH);
		        		   //hrsxPerDist = (Double.parseDouble(percentDistCH)*Double.parseDouble(regHrsTotal))/100;
		        		}
		        	 //  if(totalHrsxPerDist == 0.0){
		        		  // payment = null;
		        	   //}else{
			        	  // percentageFactor = (hrsxPerDist/totalHrsxPerDist);
			        	   if(dayDistributionHoursDTO.getRevenueShare() == null || dayDistributionHoursDTO.getRevenueShare().equals("")){
			        		   payment = null;
			        	   }else{
			        		   payment = String.valueOf(percentageFactor*Double.parseDouble(dayDistributionHoursDTO.getRevenueShare())/100);
			        	   }
		        	  // }
		        	   
		        	   //Double paymentDouble=Double.parseDouble(payment);
		        	   //Double m=new Double(Math.round((paymentDouble*10000)/10000));
		        	   timeSheet.setPaidRevenueShare(payment);
		        	   timeSheet.setCalculationCode(calcCode);
		        	   timeSheetManager.save(timeSheet);
		        	   if(timeSheet.getCrewType().equalsIgnoreCase("CH")){
		        		   timeSheet.setPaidRevenueShare("0.0");
			        	   timeSheet.setCalculationCode("H");
			        	   timeSheetManager.save(timeSheet);
		        	   }
		           }*/
		           
		           
		           
		           
		        	}
		           ///////////////////////////////////////////////////////////////////////////////////
		           
		           
		           list.add(dayDistributionHoursDTO);
		    	  
		      }
			 totalRevenue = String.valueOf(revShare);
			 timeSheetManager.updateWorkTicketData(workTicket.getTicket(),totalRevenue, calcCode);
			 workTicket.setRevenueCalculation(calcCode);
			 if(totalRevenue == null || totalRevenue.equalsIgnoreCase("")){
				 totalRevenue = "0.0";
			 }
			 workTicket.setCrewRevenue(Double.parseDouble(totalRevenue));
			 dayDistributionHoursDTOs = list;
		    }
			timeSheets = timeSheetManager.findTimeSheetByTicket(Long.parseLong(tkt));
		}catch(Exception ex){
			errorMessage("PLease select another calculation code, and try again");
			ex.printStackTrace();
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}

	public String findServiceType() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		 getComboList(sessionCorpID,"a");
		 serviceType= refMasterManager.findByParameterService(sessionCorpID, "TCKTSERVC", serviceGroup);		 
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		 return SUCCESS;
    }
	
	
	public PayrollAllocation getPayrollAllocation() {
		return payrollAllocation;
	}

	public void setPayrollAllocation(PayrollAllocation payrollAllocation) {
		this.payrollAllocation = payrollAllocation;
	}

	public List getDayDistributionHoursDTOs() {
		return dayDistributionHoursDTOs;
	}

	public void setDayDistributionHoursDTOs(List dayDistributionHoursDTOs) {
		this.dayDistributionHoursDTOs = dayDistributionHoursDTOs;
	}

	public String getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	public String getCrewNm() {
		return crewNm;
	}

	public void setCrewNm(String crewNm) {
		this.crewNm = crewNm;
	}

	public String getServ() {
		return serv;
	}

	public void setServ(String serv) {
		this.serv = serv;
	}

	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	public String getTkt() {
		return tkt;
	}

	public void setTkt(String tkt) {
		this.tkt = tkt;
	}

	public String getWareHse() {
		return wareHse;
	}

	public void setWareHse(String wareHse) {
		this.wareHse = wareHse;
	}

	public String getWorkDt() {
		return workDt;
	}

	public void setWorkDt(String workDt) {
		this.workDt = workDt;
	}

	public String getActWeight() {
		return actWeight;
	}

	public void setActWeight(String actWeight) {
		this.actWeight = actWeight;
	}

	public String getRev() {
		return rev;
	}

	public void setRev(String rev) {
		this.rev = rev;
	}

	public String getWrkDate() {
		return wrkDate;
	}

	public void setWrkDate(String wrkDate) {
		this.wrkDate = wrkDate;
	}

	public String getCalcCode() {
		return calcCode;
	}

	public void setCalcCode(String calcCode) {
		this.calcCode = calcCode;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public String getExistRevenve() {
		return existRevenve;
	}

	public void setExistRevenve(String existRevenve) {
		this.existRevenve = existRevenve;
	}

	public  List getLunchOptions() {
		return lunchOptions;
	}

	public  void setLunchOptions(List lunchOptions) {
		this.lunchOptions = lunchOptions;
	}

	public  Map<String, String> getWareHouse() {
		return wareHouse;
	}

	public String getDisablePayroll() {
		return disablePayroll;
	}

	public void setDisablePayroll(String disablePayroll) {
		this.disablePayroll = disablePayroll;
	}
	
	// OT process logic
	
	public String processOTForm(){
		getComboList(sessionCorpID,"a");
		return SUCCESS;
	}
	
	public String processOTPreviewForm(){
		getComboList(sessionCorpID,"a");
		return SUCCESS;
	}
	
	
	public String processOTPreview(){
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		getComboList(sessionCorpID,"a");
		List dateDiff= timeSheetManager.checkStopDate(beginDt, sessionCorpID, wareHse);
		if(dateDiff !=null && !dateDiff.isEmpty() && dateDiff.get(0)!=null && !dateDiff.get(0).toString().trim().equals("") && Long.parseLong(dateDiff.get(0).toString())<=0)
		{
			saveMessage("No records found for preview.");
			return SUCCESS;
		}
		else{
		List checkRegularHours= timeSheetManager.checkRegularHours(beginDt, endDt, wareHse);
		String dates="";
		String ticketNo="";
		String crewNames="";
		 Date secondDate = new Date();
		 Date firstDate = new Date();
		int count=0;
	    StringBuilder sb=new StringBuilder();
		if(!checkRegularHours.isEmpty())
		{	
			Iterator it=checkRegularHours.iterator();
			while(it.hasNext())
		       {
				Object []row=(Object [])it.next();
				if(row[0]!=null){
				 firstDate = (Date)row[0];	
				}
				if(row[0]!=null && (!row[0].toString().equals("") && firstDate.compareTo(secondDate)!=0)){
					dates="";
					dates=dates+"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+row[0];
					dates=dates.replaceAll("00:00:00.0", " ");
					//dates=dates.replaceFirst(",", "");
					sb.append(dates);
				}				
				if(row[1]!=null && (!row[1].toString().equals(""))&&(!ticketNo.trim().equals(row[1].toString()))&& firstDate.compareTo(secondDate)!=0){
					ticketNo=" ";
					ticketNo=ticketNo+"&nbsp;&nbsp;"+row[1];
				    sb.append(ticketNo);
				    ticketNo=row[1].toString();
				    secondDate=	(Date)row[0];
				    count=0;
				}
				if(row[1]!=null && (!row[1].toString().equals(""))&&(!ticketNo.trim().equals(row[1].toString()))&& firstDate.compareTo(secondDate)==0){
					ticketNo=" ";
					ticketNo=ticketNo+"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+row[1];
				    sb.append(ticketNo);
				    ticketNo=row[1].toString();
				    secondDate=	(Date)row[0];
				    count=0;
				}
				if(row[2]!=null && (!row[2].toString().equals(""))){
				crewNames="";
				String crew[]=row[2].toString().split(",");
				if(count<2){
					crewNames=crewNames+"&nbsp;&nbsp;"+crew[1]+" "+crew[0];	
					count++;
				}else{
					crewNames=crewNames+"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+crew[1]+" "+crew[0];	
					count=0;
				}
				sb.append(crewNames);
				}
		       }
			
			errorMessage("Regular Hours is not assigned in all the timesheets for Dates "+sb); 
			return SUCCESS;
		}else{
		// *********************************** process OT for unioun crew members *****************************
		timeSheetManager.deleteFromTimeSheet(beginDt,endDt,wareHse,  sessionCorpID);
		List timeSheetList = timeSheetManager.findAllTimeSheets(beginDt,endDt,wareHse, sessionCorpID);
		noOfTimeSheet = String.valueOf(timeSheetList.get(0).toString());
		List otCrewsList = timeSheetManager.findTimeSheetsForOTPreview(beginDt,endDt,wareHse, sessionCorpID);
		Iterator it = otCrewsList.iterator();
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			try{
	        dt = new SimpleDateFormat("yyyy-MM-dd").parse(row[2].toString().replace(".0", ""));
	        }catch (ParseException e) {
				
				e.printStackTrace();
			}
	        String dayNames[] = new DateFormatSymbols().getWeekdays();
	        Calendar date2 = Calendar.getInstance();
	        date2.setTime(dt);
	        //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Today is a " + dayNames[date2.get(Calendar.DAY_OF_WEEK)]);
	        dayNameOfWeek = dayNames[date2.get(Calendar.DAY_OF_WEEK)];
			timeSheet = timeSheetManager.get(Long.parseLong((row[0].toString())));
			//List list = timeSheetManager.checkRevenueSharing(row[1].toString(),row[2].toString(), wareHse); // it will check that the crew is involved in revenue sharing or Not >>Bug 11610 
			String holiday="";
			List companyHolidays=timeSheetManager.getCompanyHolidays(sessionCorpID, row[2].toString());
			if(companyHolidays.get(0).toString().equalsIgnoreCase("1"))
			{
				holiday="Yes";
			}
			else
			{
				holiday="No";
			}
			//if(list.isEmpty()){
				if((row[5] == null || row[5].toString().equalsIgnoreCase("")) && (Double.parseDouble(row[9].toString()) > 0)){}else{
						if(Double.parseDouble(row[5].toString()) > 8){
						otHrs = Double.parseDouble(row[5].toString()) - 8 ;
						if(!dayNameOfWeek.equalsIgnoreCase("SATURDAY")){
						
						if(!dayNameOfWeek.equalsIgnoreCase("SUNDAY") && holiday=="No"){
								adjustPreviewRegularHours(row[2].toString(),wareHse,row[1].toString(), sessionCorpID, otHrs);
						}
						
						} 
						}
						if(dayNameOfWeek.equalsIgnoreCase("SUNDAY") || holiday=="Yes"){
							/*timeSheet.setDoubleOverTime(new BigDecimal(row[5].toString()));
							timeSheet.setRegularHours(new BigDecimal(0));
							timeSheetManager.save(timeSheet);*/
						}
						
						if(dayNameOfWeek.equalsIgnoreCase("SATURDAY")){
							adjustPreviewSaturdayHours(row[2].toString(),wareHse,row[1].toString(),sessionCorpID);
						}
					}
/*	Bug 11610 			}
			else{
				if(row[5] == null || row[5].toString().equalsIgnoreCase("")){}else{
					if(Double.parseDouble(row[5].toString()) > 5){
						otHrs = Double.parseDouble(row[5].toString()) - 5 ;
						if(!dayNameOfWeek.equalsIgnoreCase("SATURDAY")){
						
						if(!dayNameOfWeek.equalsIgnoreCase("SUNDAY") && holiday=="No"){
								adjustPreviewRegularHours1(row[2].toString(),wareHse,row[1].toString(), sessionCorpID, otHrs);
						}
						}
					}
						else if(dayNameOfWeek.equalsIgnoreCase("SUNDAY") || holiday=="Yes"){
							timeSheet.setDoubleOverTime(new BigDecimal(row[5].toString()));
							timeSheet.setRegularHours(new BigDecimal(0));
							timeSheetManager.save(timeSheet);
						}
					if(dayNameOfWeek.equalsIgnoreCase("SATURDAY")){
						adjustPreviewSaturdayHours(row[2].toString(),wareHse,row[1].toString(),sessionCorpID);
					}
				}
			}*/
		}
//		 For Summer/ winter time union members gurantee case 
		List otSummerCrewList = timeSheetManager.findSummerTimeSheetsForOT(beginDt,endDt,wareHse, sessionCorpID);
		
		Iterator itsmr = otSummerCrewList.iterator();
		while(itsmr.hasNext()){
			Object []row1= (Object [])itsmr.next();
			try{
	        dtsm = new SimpleDateFormat("yyyy-MM-dd").parse(row1[2].toString().replace(".0", ""));
	        }catch (ParseException e) {
				e.printStackTrace();
			}
	        List warehouse=timeSheetManager.getWarehouse(row1[0].toString());
	        if(!warehouse.get(0).toString().equalsIgnoreCase("15")){
			//System.err.println("\n\n\n\n-------dtsm----->>"+dtsm);
	        String dayNames[] = new DateFormatSymbols().getWeekdays();
	        Calendar date3 = Calendar.getInstance();
	        Calendar cal = Calendar.getInstance();
	        date3.setTime(dtsm);
	        Double otFactor;
	        if(dayNames[date3.get(Calendar.DAY_OF_WEEK)].equalsIgnoreCase("Sunday")){
	        	otFactor = 2.0;
	        }else{
	        	otFactor = 1.5;
	        }
	        Double nonRevenueHrs;
	     	List nrhList = timeSheetManager.findNonRevenueHrs(row1[1].toString(),row1[2].toString(), wareHse);
	     	if(nrhList == null || nrhList.isEmpty() || nrhList.get(0).toString().equalsIgnoreCase("null")){
	     		nonRevenueHrs = 0.0;
	     	}else{
	     		nonRevenueHrs = Double.parseDouble(nrhList.get(0).toString());
	     	}
	     	Double crewOTHrs;
	     	List cotHrsList = timeSheetManager.findOTHrs(row1[1].toString(),row1[2].toString(), wareHse);
	     	if(cotHrsList == null || cotHrsList.isEmpty() || cotHrsList.get(0).toString().equalsIgnoreCase("")){
	     		crewOTHrs = 0.0;
	     	}else{
	     		crewOTHrs = Double.parseDouble(cotHrsList.get(0).toString());
	     	}
			Double normalPay;
        	Double guaranteePay;
        	Double revShareAmt;
        	Double otAmount;
        	Double nonRHAmount;
        	Double extraPmt=0.00;
        	if(row1[7] == null || row1[7].toString().equalsIgnoreCase("")){normalPay = 0.0;}else{normalPay = Double.parseDouble(row1[7].toString());}
        	
        	if(row1[8] == null || row1[8].toString().equalsIgnoreCase("")){revShareAmt = 0.0;}else{revShareAmt = Double.parseDouble(row1[8].toString());}
        	nonRHAmount = nonRevenueHrs * normalPay;
        	otAmount = crewOTHrs * normalPay * otFactor;
        	
        if((date3.get(date3.MONTH)+1 == 5 && date3.get(date3.DATE) >=15) || ( 5 < (date3.get(date3.MONTH)+1) && (date3.get(date3.MONTH)+1) < 10) || ((date3.get(date3.MONTH)+1) == 10 && date3.get(date3.DATE) <=15)){
        	guaranteePay = normalPay * 8;
        	
        }else{
        	guaranteePay = normalPay * 6;
        	
        }
       /* if( guaranteePay > (revShareAmt+nonRHAmount+otAmount)){
        	extraPmt = guaranteePay - (revShareAmt+nonRHAmount+otAmount);
        	List extraPaidExists = timeSheetManager.checkExtraPaidTimeSheet(row1[2].toString().replace(".0", ""),row1[1].toString());
        	if(!(extraPaidExists == null || extraPaidExists.isEmpty())){
        		timeSheet = ( TimeSheet )extraPaidExists.get(0);
        	}else{
        		timeSheet = new TimeSheet();
        		
        	}
			timeSheet.setWorkDate(date3.getTime());
			timeSheet.setCrewName(row1[9].toString());
			timeSheet.setWarehouse(wareHse);
			timeSheet.setCrewType(row1[10].toString());
			timeSheet.setUserName(row1[1].toString());
			timeSheet.setCorpID(sessionCorpID);
			timeSheet.setAction("E");
			timeSheet.setDoneForTheDay("true");
			timeSheet.setShipper("");
			//Double m=new Double(Math.round((extraPmt)*100)/100);
			timeSheet.setPaidRevenueShare(new BigDecimal(extraPmt.toString()));
			timeSheet.setDistributionCode("180");
			timeSheet.setCalculationCode("");
			timeSheet.setBeginHours("");
			timeSheet.setEndHours("");
			timeSheet.setPreviewRegularHours(new BigDecimal(0));
			timeSheet.setPreviewOverTime(new BigDecimal(0));
			timeSheet.setDoubleOverTime(new BigDecimal(0));
			timeSheet.setUpdatedOn(new Date());
			timeSheet.setUpdatedBy(getRequest().getRemoteUser());
			timeSheet.setCreatedBy(getRequest().getRemoteUser());
			timeSheetManager.save(timeSheet);
        }*/
		}
		}
// For Summer/ winter time union members gurantee case 
//		 *********************************** process OT for unioun crew members *****************************
//		 *********************************** process OT for non unioun crew members *****************************
		
		
		
		List otNUCrewsList = timeSheetManager.findTimeSheetsForNonUnionOT(beginDt,endDt,wareHse, sessionCorpID);
		noOfCrews = String.valueOf(otSummerCrewList.size() + otNUCrewsList.size());
		Iterator itr = otNUCrewsList.iterator();
		while(itr.hasNext()){
			Object []row3= (Object [])itr.next();
			try{
		        dtr = new SimpleDateFormat("yyyy-MM-dd").parse(row3[11].toString().replace(".0", ""));
		        }catch (ParseException e) {
					e.printStackTrace();
				}
				
		        Calendar date4 = Calendar.getInstance();
		        date4.setTime(dtr);
			Double totalHrs;
			Double nuOTHrs = 0.0;
			Double nuOtAmt = 0.0;
			if(row3[5] == null ){totalHrs = 0.0; }else{totalHrs = Double.parseDouble(row3[5].toString());}
			if( totalHrs > 40 ){
			nuOTHrs = totalHrs - 40; nuOtAmt = totalHrs * 1.5;
			Double nuOTHRSADS=nuOTHrs*-1;
			postPreviewOTProcess(beginDt,endDt,wareHse, sessionCorpID, row3[1].toString(), nuOTHrs);
			}
		}
		
//		 *********************************** process OT for non unioun crew members *****************************
		//saveMessage(noOfTimeSheet+"&nbsp;timesheets [for "+noOfCrews+" crew] has been processed."); 
		List noOfCrew=timeSheetManager.getNumberOfCrew(beginDt,endDt,wareHse, sessionCorpID);
		saveMessage(noOfTimeSheet+"&nbsp;timesheets [for "+noOfCrew.size()+" crew] have been processed.");  
		//getComboList(sessionCorpID,"a");
		overTimeProcessList=timeSheetManager.findPreviewOverTimeList(beginDt,endDt,wareHse);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
		}
		}
	}
	String beginDt;
	String endDt;
	Double otHrs;
	Date dt;
	Date dtsm;
	Date dtr;
	private String noOfTimeSheet;
	private String noOfCrews;
	private String dayNameOfWeek;
	private List overTimeProcessList;
	public String processOTCharges(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		getComboList(sessionCorpID,"a");
		List dateDiff= timeSheetManager.checkStopDate(beginDt, sessionCorpID, wareHse);
		if(dateDiff !=null && !dateDiff.isEmpty() && dateDiff.get(0)!=null && !dateDiff.get(0).toString().trim().equals("") && Long.parseLong(dateDiff.get(0).toString())<=0)
		{
			saveMessage("Payroll Processing Underway, you cannot do this operation.");
			return SUCCESS;
		}
		else{
		List checkRegularHours= timeSheetManager.checkRegularHours(beginDt, endDt, wareHse);
		String dates="";
		String ticketNo="";
		String crewNames="";
		 Date secondDate = new Date();
		 Date firstDate = new Date();
		int count=0;
	    StringBuilder sb=new StringBuilder();
		if(!checkRegularHours.isEmpty())
		{	
			Iterator it=checkRegularHours.iterator();
			while(it.hasNext())
		       {
				Object []row=(Object [])it.next();
				if(row[0]!=null){
				 firstDate = (Date)row[0];	
				}
				if(row[0]!=null && (!row[0].toString().equals("") && firstDate.compareTo(secondDate)!=0)){
					dates="";
					dates=dates+"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+row[0];
					dates=dates.replaceAll("00:00:00.0", " ");
					//dates=dates.replaceFirst(",", "");
					sb.append(dates);
				}				
				if(row[1]!=null && (!row[1].toString().equals(""))&&(!ticketNo.trim().equals(row[1].toString()))&& firstDate.compareTo(secondDate)!=0){
					ticketNo=" ";
					ticketNo=ticketNo+"&nbsp;&nbsp;"+row[1];
				    sb.append(ticketNo);
				    ticketNo=row[1].toString();
				    secondDate=	(Date)row[0];
				    count=0;
				}
				if(row[1]!=null && (!row[1].toString().equals(""))&&(!ticketNo.trim().equals(row[1].toString()))&& firstDate.compareTo(secondDate)==0){
					ticketNo=" ";
					ticketNo=ticketNo+"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+row[1];
				    sb.append(ticketNo);
				    ticketNo=row[1].toString();
				    secondDate=	(Date)row[0];
				    count=0;
				}
				if(row[2]!=null && (!row[2].toString().equals(""))){
				crewNames="";
				String crew[]=row[2].toString().split(",");
				if(count<2){
					crewNames=crewNames+"&nbsp;&nbsp;"+crew[1]+" "+crew[0];	
					count++;
				}else{
					crewNames=crewNames+"<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+crew[1]+" "+crew[0];	
					count=0;
				}
				
				
				sb.append(crewNames);
				}
		       }
			
			errorMessage("Regular Hours is not assigned in all the timesheets for Dates "+sb); 
			return SUCCESS;
		}else{
		// *********************************** process OT for unioun crew members *****************************
		timeSheetManager.deleteFromTimeSheet(beginDt,endDt,wareHse,  sessionCorpID);
		List timeSheetList = timeSheetManager.findAllTimeSheets(beginDt,endDt,wareHse, sessionCorpID);
		noOfTimeSheet = String.valueOf(timeSheetList.get(0).toString());
		List otCrewsList = timeSheetManager.findTimeSheetsForOT(beginDt,endDt,wareHse, sessionCorpID);
		Iterator it = otCrewsList.iterator();
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			try{
	        dt = new SimpleDateFormat("yyyy-MM-dd").parse(row[2].toString().replace(".0", ""));
	        }catch (ParseException e) {
				
				e.printStackTrace();
			}
	        
			
	        String dayNames[] = new DateFormatSymbols().getWeekdays();
	        Calendar date2 = Calendar.getInstance();
	        date2.setTime(dt);
	        //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Today is a " + dayNames[date2.get(Calendar.DAY_OF_WEEK)]);
	        dayNameOfWeek = dayNames[date2.get(Calendar.DAY_OF_WEEK)];
			timeSheet = timeSheetManager.get(Long.parseLong((row[0].toString())));
			//List list = timeSheetManager.checkRevenueSharing(row[1].toString(),row[2].toString(), wareHse); // it will check that the crew is involved in revenue sharing or Not >>Bug 11610
			String holiday="";
			List companyHolidays=timeSheetManager.getCompanyHolidays(sessionCorpID, row[2].toString());
			if(companyHolidays.get(0).toString().equalsIgnoreCase("1"))
			{
				holiday="Yes";
			}
			else
			{
				holiday="No";
			}
			//if(list.isEmpty()){ Bug 11610
				if(row[5] == null || row[5].toString().equalsIgnoreCase("")){}else{
						if(Double.parseDouble(row[5].toString()) > 8){
						otHrs = Double.parseDouble(row[5].toString()) - 8 ;
						if(!dayNameOfWeek.equalsIgnoreCase("SATURDAY")){
						
						if(!dayNameOfWeek.equalsIgnoreCase("SUNDAY") && holiday=="No"){
								adjustRegularHours(row[2].toString(),wareHse,row[1].toString(), sessionCorpID, otHrs);
							
							//String workDate, String wareHse2, String userName, String sessionCorpID2, Double otHrs2
							//timeSheet.setOverTime(otHrs.toString());
							//timeSheet.setRegularHours(String.valueOf(Double.parseDouble(timeSheet.getRegularHours()) - otHrs));
						}
						
						} 
						}
						if(dayNameOfWeek.equalsIgnoreCase("SUNDAY") || holiday=="Yes"){
							timeSheet.setDoubleOverTime(new BigDecimal(row[5].toString()));
							timeSheet.setRegularHours(new BigDecimal(0));
							timeSheetManager.save(timeSheet);
						}
						
						if(dayNameOfWeek.equalsIgnoreCase("SATURDAY")){
							adjustSaturdayHours(row[2].toString(),wareHse,row[1].toString(),sessionCorpID);
							//timeSheet.setOverTime(row[5].toString());
							//timeSheet.setRegularHours("0");
							//timeSheetManager.save(timeSheet);
						}
						
						
					}
					
					
			//}
/*Bug 11610			else{
				if(row[5] == null || row[5].toString().equalsIgnoreCase("")){}else{
					if(Double.parseDouble(row[5].toString()) > 5){
						otHrs = Double.parseDouble(row[5].toString()) - 5 ;
						if(!dayNameOfWeek.equalsIgnoreCase("SATURDAY")){
						
						if(!dayNameOfWeek.equalsIgnoreCase("SUNDAY") && holiday=="No"){
								adjustRegularHours1(row[2].toString(),wareHse,row[1].toString(), sessionCorpID, otHrs);
						
							
							
							//timeSheet.setOverTime(otHrs.toString());
							//timeSheet.setRegularHours(String.valueOf(Double.parseDouble(timeSheet.getRegularHours()) - otHrs));
						}
						
						}
					}
						else if(dayNameOfWeek.equalsIgnoreCase("SUNDAY") || holiday=="Yes"){
							timeSheet.setDoubleOverTime(new BigDecimal(row[5].toString()));
							timeSheet.setRegularHours(new BigDecimal(0));
							timeSheetManager.save(timeSheet);
						}
					if(dayNameOfWeek.equalsIgnoreCase("SATURDAY")){
						adjustSaturdayHours(row[2].toString(),wareHse,row[1].toString(),sessionCorpID);
						//timeSheet.setOverTime(row[5].toString());
						//timeSheet.setRegularHours("0");
						//timeSheetManager.save(timeSheet);
					}
						
					
				}
			}*/
		}
//		 For Summer/ winter time union members gurantee case 
		List otSummerCrewList = timeSheetManager.findSummerTimeSheetsForOT(beginDt,endDt,wareHse, sessionCorpID);
		
		Iterator itsmr = otSummerCrewList.iterator();
		while(itsmr.hasNext()){
			Object []row1= (Object [])itsmr.next();
			try{
	        dtsm = new SimpleDateFormat("yyyy-MM-dd").parse(row1[2].toString().replace(".0", ""));
	        }catch (ParseException e) {
				e.printStackTrace();
			}
	        List warehouse=timeSheetManager.getWarehouse(row1[0].toString());
	        if(!warehouse.get(0).toString().equalsIgnoreCase("15")){
			//System.err.println("\n\n\n\n-------dtsm----->>"+dtsm);
	        String dayNames[] = new DateFormatSymbols().getWeekdays();
	        Calendar date3 = Calendar.getInstance();
	        Calendar cal = Calendar.getInstance();
	        date3.setTime(dtsm);
	        Double otFactor;
	        if(dayNames[date3.get(Calendar.DAY_OF_WEEK)].equalsIgnoreCase("Sunday")){
	        	otFactor = 2.0;
	        }else{
	        	otFactor = 1.5;
	        }
	        Double nonRevenueHrs;
	     	List nrhList = timeSheetManager.findNonRevenueHrs(row1[1].toString(),row1[2].toString(), wareHse);
	     	if(nrhList == null || nrhList.isEmpty() || nrhList.get(0).toString().equalsIgnoreCase("null")){
	     		nonRevenueHrs = 0.0;
	     	}else{
	     		nonRevenueHrs = Double.parseDouble(nrhList.get(0).toString());
	     	}
	     	Double crewOTHrs;
	     	List cotHrsList = timeSheetManager.findOTHrs(row1[1].toString(),row1[2].toString(), wareHse);
	     	if(cotHrsList == null || cotHrsList.isEmpty() || cotHrsList.get(0).toString().equalsIgnoreCase("")){
	     		crewOTHrs = 0.0;
	     	}else{
	     		crewOTHrs = Double.parseDouble(cotHrsList.get(0).toString());
	     	}
			Double normalPay;
        	Double guaranteePay;
        	Double revShareAmt;
        	Double otAmount;
        	Double nonRHAmount;
        	Double extraPmt=0.00;
        	if(row1[7] == null || row1[7].toString().equalsIgnoreCase("")){normalPay = 0.0;}else{normalPay = Double.parseDouble(row1[7].toString());}
        	
        	if(row1[8] == null || row1[8].toString().equalsIgnoreCase("")){revShareAmt = 0.0;}else{revShareAmt = Double.parseDouble(row1[8].toString());}
        	nonRHAmount = nonRevenueHrs * normalPay;
        	otAmount = crewOTHrs * normalPay * otFactor;
        	
        if((date3.get(date3.MONTH)+1 == 5 && date3.get(date3.DATE) >=15) || ( 5 < (date3.get(date3.MONTH)+1) && (date3.get(date3.MONTH)+1) < 10) || ((date3.get(date3.MONTH)+1) == 10 && date3.get(date3.DATE) <=15)){
        	guaranteePay = normalPay * 8;
        	
        }else{
        	guaranteePay = normalPay * 6;
        	
        }
        if( guaranteePay > (nonRHAmount+otAmount)){
        	extraPmt = guaranteePay - (nonRHAmount+otAmount); /// need to check on this line if( guaranteePay > (revShareAmt+nonRHAmount+otAmount)){extraPmt = guaranteePay - (revShareAmt+nonRHAmount+otAmount);
        	
        	List extraPaidExists = timeSheetManager.checkExtraPaidTimeSheet(row1[2].toString().replace(".0", ""),row1[1].toString());
        	if(!(extraPaidExists == null || extraPaidExists.isEmpty())){
        		timeSheet = ( TimeSheet )extraPaidExists.get(0);
        	}else{
        		timeSheet = new TimeSheet();
        		
        	}
			timeSheet.setWorkDate(date3.getTime());
			timeSheet.setCrewName(row1[9].toString());
			timeSheet.setWarehouse(wareHse);
			timeSheet.setCrewType(row1[10].toString());
			timeSheet.setUserName(row1[1].toString());
			timeSheet.setCorpID(sessionCorpID);
			timeSheet.setAction("E");
			timeSheet.setDoneForTheDay("true");
			timeSheet.setShipper("");
			//Double m=new Double(Math.round((extraPmt)*100)/100);
			timeSheet.setPaidRevenueShare(new BigDecimal(extraPmt.toString()));//need to confirm from Neha mam
			timeSheet.setDistributionCode("180");
			timeSheet.setCalculationCode("");
			timeSheet.setBeginHours("");
			timeSheet.setEndHours("");
			timeSheet.setRegularHours(new BigDecimal(0));
			timeSheet.setOverTime(new BigDecimal(0));
			timeSheet.setDoubleOverTime(new BigDecimal(0));
			timeSheet.setUpdatedOn(new Date());
			timeSheet.setUpdatedBy(getRequest().getRemoteUser());
			timeSheet.setCreatedBy(getRequest().getRemoteUser());
			timeSheetManager.save(timeSheet);
        }
		}
		}
// For Summer/ winter time union members gurantee case 
//		 *********************************** process OT for unioun crew members *****************************
//		 *********************************** process OT for non unioun crew members *****************************
		
		
		
		List otNUCrewsList = timeSheetManager.findTimeSheetsForNonUnionOT(beginDt,endDt,wareHse, sessionCorpID);
		noOfCrews = String.valueOf(otSummerCrewList.size() + otNUCrewsList.size());
		Iterator itr = otNUCrewsList.iterator();
		while(itr.hasNext()){
			Object []row3= (Object [])itr.next();
			try{
		        dtr = new SimpleDateFormat("yyyy-MM-dd").parse(row3[11].toString().replace(".0", ""));
		        }catch (ParseException e) {
					e.printStackTrace();
				}
				
		        Calendar date4 = Calendar.getInstance();
		        date4.setTime(dtr);
			Double totalHrs;
			Double nuOTHrs = 0.0;
			Double nuOtAmt = 0.0;
			if(row3[5] == null ){totalHrs = 0.0; }else{totalHrs = Double.parseDouble(row3[5].toString());}
			if( totalHrs > 40 ){
			nuOTHrs = totalHrs - 40; nuOtAmt = totalHrs * 1.5;
			Double nuOTHRSADS=nuOTHrs*-1;
			
			postOTProcess(beginDt,endDt,wareHse, sessionCorpID, row3[1].toString(), nuOTHrs);
			}
			
			// Start Earning opportunity for non unions
			
            List list = timeSheetManager.checkRevenueSharing(row3[1].toString(),beginDt, wareHse,sessionCorpID,endDt);
			
			Iterator prvit = list.iterator();
			while(prvit.hasNext()){
				TimeSheet timeSheetRev = timeSheetManager.get(Long.parseLong(prvit.next().toString()));
	        if(!wareHse.equalsIgnoreCase("15")){
        	BigDecimal extraPmt=new BigDecimal(0.00) ;
        try {
			if(timeSheetRev.getPaidRevenueShare()!=null && timeSheetRev.getPaidRevenueShare().doubleValue()>0){
				BigDecimal normalRevShare = (timeSheetRev.getRegularHours().add(timeSheetRev.getOverTime())
						.add(timeSheetRev.getDoubleOverTime())).multiply(new BigDecimal(timeSheetRev.getRegularPayPerHour()));
				if(normalRevShare.doubleValue() > timeSheetRev.getPaidRevenueShare().doubleValue()){
					extraPmt = normalRevShare.subtract(timeSheetRev.getPaidRevenueShare()); 
					List extraPaidExists = timeSheetManager.checkExtraPaidTimeSheet(timeSheetRev.getWorkDate().toString(),timeSheetRev.getUserName());
		        	if(!(extraPaidExists == null || extraPaidExists.isEmpty())){
		        		timeSheet = ( TimeSheet )extraPaidExists.get(0);
		        		extraPmt = extraPmt.add(timeSheet.getPaidRevenueShare());
		        	}else{
		        		timeSheet = new TimeSheet();
		        	}
					timeSheet.setWorkDate(timeSheetRev.getWorkDate());
					timeSheet.setCrewName(timeSheetRev.getCrewName());
					timeSheet.setWarehouse(wareHse);
					timeSheet.setCrewType(timeSheetRev.getCrewType());
					timeSheet.setUserName(timeSheetRev.getUserName());
					timeSheet.setCorpID(sessionCorpID);
					timeSheet.setAction("E");
					timeSheet.setDoneForTheDay("true");
					timeSheet.setShipper("");
					timeSheet.setPaidRevenueShare(extraPmt);
					timeSheet.setDistributionCode("180");
					timeSheet.setCalculationCode("");
					timeSheet.setBeginHours("");
					timeSheet.setEndHours("");
					timeSheet.setRegularHours(new BigDecimal(0));
					timeSheet.setOverTime(new BigDecimal(0));
					timeSheet.setDoubleOverTime(new BigDecimal(0));
					timeSheet.setUpdatedOn(new Date());
					timeSheet.setUpdatedBy(getRequest().getRemoteUser());
					timeSheet.setCreatedBy(getRequest().getRemoteUser());
					timeSheetManager.save(timeSheet);
		        }			
				}
		} 
        catch (Exception e) {
			e.printStackTrace();
		}
		}
			}
			
			//End Earning opportunity for non unions
			
		/*	
			List list = timeSheetManager.checkRevenueSharing(row3[1].toString(),beginDt, wareHse,sessionCorpID,endDt);
			
			Iterator prvit = list.iterator();
			while(prvit.hasNext()){
				timeSheet = timeSheetManager.get(Long.parseLong(prvit.next().toString()));
				try {
					if(timeSheet.getPaidRevenueShare()!=null && timeSheet.getPaidRevenueShare().doubleValue()>0){
						BigDecimal normalRevShare = (timeSheet.getRegularHours().add(timeSheet.getOverTime()).add(timeSheet.getDoubleOverTime())).multiply(new BigDecimal(timeSheet.getRegularPayPerHour()));
						if(normalRevShare.doubleValue() > timeSheet.getPaidRevenueShare().doubleValue()){
							timeSheet.setAdjustmentToRevenue(normalRevShare.subtract(timeSheet.getPaidRevenueShare()));
							timeSheet.setReasonForAdjustment("Earning Opportunity");
							timeSheet.setAdjustedBy("OTProcessing_"+getRequest().getRemoteUser());
							timeSheet.setUpdatedBy("OTProcessing_"+getRequest().getRemoteUser());
							timeSheet.setUpdatedOn(new Date());
							timeSheet=timeSheetManager.save(timeSheet);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}*/
			
			
			//timeSheet = timeSheetManager.get(Long.parseLong((row3[0].toString())));
			//timeSheet.setRegularHours(regularHours);
			/*List actionOExists = timeSheetManager.checkActionOTimeSheet(endDt,row3[1].toString());
			if(!(actionOExists == null || actionOExists.isEmpty())){
        		timeSheet = ( TimeSheet )actionOExists.get(0);
        	}else{
        		timeSheet = new TimeSheet();
        		
        	}
			timeSheet.setDistributionCode("");
			timeSheet.setWorkDate(date4.getTime());
			timeSheet.setCrewName(row3[9].toString());
			timeSheet.setWarehouse(wareHse);
			timeSheet.setCrewType(row3[10].toString());
			timeSheet.setUserName(row3[1].toString());
			timeSheet.setCorpID(sessionCorpID);
			timeSheet.setAction("O");
			timeSheet.setDoneForTheDay("true");
			timeSheet.setShipper("");
			timeSheet.setTicket(new Long(0));
			timeSheet.setOverTime(nuOTHrs.toString());
			//timeSheet.setPaidRevenueShare(nuOtAmt.toString());
			timeSheet.setPaidRevenueShare("0.00");
			timeSheet.setRegularHours(nuOTHRSADS.toString());
			//timeSheet.setRegularHours("0.00");
			timeSheet.setCalculationCode("");
			timeSheet.setBeginHours("00:00");
			timeSheet.setEndHours("00:00");
			//timeSheet.setOverTime("0.0");
			timeSheet.setDoubleOverTime("0.00");
			timeSheet.setUpdatedOn(new Date());
			timeSheet.setUpdatedBy(getRequest().getRemoteUser());
			timeSheet.setCreatedBy(getRequest().getRemoteUser());
			timeSheetManager.save(timeSheet);*/
		}
		
		
//		 *********************************** process OT for non unioun crew members *****************************
		//saveMessage(noOfTimeSheet+"&nbsp;timesheets [for "+noOfCrews+" crew] has been processed."); 
		List noOfCrew=timeSheetManager.getNumberOfCrew(beginDt,endDt,wareHse, sessionCorpID);
		saveMessage(noOfTimeSheet+"&nbsp;timesheets [for "+noOfCrew.size()+" crew] have been processed.");  
		//getComboList(sessionCorpID,"a");
		overTimeProcessList=timeSheetManager.findOverTimeList(beginDt,endDt,wareHse);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
		}
		}
	}
	
	
	private void adjustSaturdayHours(String workDate, String wareHse2, String userName, String sessionCorpID2) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		Double nuRegHours=0.0;
		List crewsList = timeSheetManager.findTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		Iterator it =crewsList.iterator();
		while(it.hasNext())
		{
			Object []row33= (Object [])it.next();
				//nuRegHours = Double.parseDouble(row33[0].toString());
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				timeSheet.setOverTime(new BigDecimal(row33[0].toString()));
				timeSheet.setRegularHours(new BigDecimal(0));
				timeSheet=timeSheetManager.save(timeSheet);
				
				
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	} 

	private void adjustPreviewSaturdayHours(String workDate, String wareHse2, String userName, String sessionCorpID2) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		Double nuRegHours=0.0;
		List crewsList = timeSheetManager.findTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		Iterator it =crewsList.iterator();
		while(it.hasNext())
		{
			Object []row33= (Object [])it.next();
				//nuRegHours = Double.parseDouble(row33[0].toString());
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				timeSheet.setPreviewOverTime(new BigDecimal(row33[0].toString()));
				timeSheet.setPreviewRegularHours(new BigDecimal(0));
				timeSheet=timeSheetManager.save(timeSheet);
				
				
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	}
	private String adjustPreviewRegularHours(String workDate, String wareHse2, String userName, String sessionCorpID2, Double otHrs2) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		Double Ot=0.0;
		
		List otCrewList=timeSheetManager.findOTTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		if(!otCrewList.isEmpty() && otCrewList!=null)
		{
			Ot=Double.parseDouble(otCrewList.get(0).toString()) - 8;
		}
		
		Double nuRegHours=0.0;
		Double tempOT=Ot;
		List crewsList = timeSheetManager.findTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		Iterator it =crewsList.iterator();
		while(it.hasNext())
		{
			Object []row33= (Object [])it.next();
			nuRegHours = Double.parseDouble(row33[0].toString());
			if(tempOT>nuRegHours){
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				timeSheet.setPreviewOverTime(new BigDecimal(nuRegHours.toString()));
				timeSheet.setPreviewRegularHours(new BigDecimal(0));
				timeSheet=timeSheetManager.save(timeSheet);
				tempOT = tempOT-nuRegHours;
				
			}
			else{
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				Double tempRegHrs=nuRegHours-tempOT;
				timeSheet.setPreviewOverTime(new BigDecimal(tempOT.toString()));
				timeSheet.setPreviewRegularHours(new BigDecimal(tempRegHrs.toString()));
				timeSheet=timeSheetManager.save(timeSheet);
				break;
				
			}
			
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	private String adjustRegularHours(String workDate, String wareHse2, String userName, String sessionCorpID2, Double otHrs2) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		Double Ot=0.0;
		
		List otCrewList=timeSheetManager.findOTTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		if(!otCrewList.isEmpty() && otCrewList!=null)
		{
			Ot=Double.parseDouble(otCrewList.get(0).toString()) - 8;
		}
		
		Double nuRegHours=0.0;
		Double tempOT=Ot;
		List crewsList = timeSheetManager.findTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		Iterator it =crewsList.iterator();
		while(it.hasNext())
		{
			Object []row33= (Object [])it.next();
			nuRegHours = Double.parseDouble(row33[0].toString());
			if(tempOT>nuRegHours){
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				timeSheet.setOverTime(new BigDecimal(nuRegHours.toString()));
				timeSheet.setRegularHours(new BigDecimal(0));
				timeSheet=timeSheetManager.save(timeSheet);
				tempOT = tempOT-nuRegHours;
				
			}
			else{
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				Double tempRegHrs=nuRegHours-tempOT;
				timeSheet.setOverTime(new BigDecimal(tempOT.toString()));
				timeSheet.setRegularHours(new BigDecimal(tempRegHrs.toString()));
				timeSheet=timeSheetManager.save(timeSheet);
				break;
				
			}
			
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	private String adjustRegularHours1(String workDate, String wareHse2, String userName, String sessionCorpID2, Double otHrs2) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		Double Ot=0.0;
		
		/*List otCrewList=timeSheetManager.findOTTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		if(!otCrewList.isEmpty() && otCrewList!=null)
		{
			Ot=Double.parseDouble(otCrewList.get(0).toString()) - 5;
		}*/
		
		Double nuRegHours=0.0;
		Double tempOT=otHrs2;
		List crewsList = timeSheetManager.findTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		Iterator it =crewsList.iterator();
		while(it.hasNext())
		{
			Object []row33= (Object [])it.next();
			nuRegHours = Double.parseDouble(row33[0].toString());
			if(tempOT>=nuRegHours){
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				timeSheet.setOverTime(new BigDecimal(nuRegHours.toString()));
				timeSheet.setRegularHours(new BigDecimal(0));
				timeSheet=timeSheetManager.save(timeSheet);
				tempOT = tempOT-nuRegHours;
				
			}
			else{
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				Double tempRegHrs=nuRegHours-tempOT;
				timeSheet.setOverTime(new BigDecimal(tempOT.toString()));
				timeSheet.setRegularHours(new BigDecimal(tempRegHrs.toString()));
				timeSheet=timeSheetManager.save(timeSheet);
				break;
				
			}
			
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}

	private String adjustPreviewRegularHours1(String workDate, String wareHse2, String userName, String sessionCorpID2, Double otHrs2) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		Double Ot=0.0;
		
		/*List otCrewList=timeSheetManager.findOTTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		if(!otCrewList.isEmpty() && otCrewList!=null)
		{
			Ot=Double.parseDouble(otCrewList.get(0).toString()) - 5;
		}*/
		
		Double nuRegHours=0.0;
		Double tempOT=otHrs2;
		List crewsList = timeSheetManager.findTimeSheetsForUnionOTPost(workDate,wareHse2, sessionCorpID, userName);
		Iterator it =crewsList.iterator();
		while(it.hasNext())
		{
			Object []row33= (Object [])it.next();
			nuRegHours = Double.parseDouble(row33[0].toString());
			if(tempOT>=nuRegHours){
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				timeSheet.setPreviewOverTime(new BigDecimal(nuRegHours.toString()));
				timeSheet.setPreviewRegularHours(new BigDecimal(0));
				timeSheet=timeSheetManager.save(timeSheet);
				tempOT = tempOT-nuRegHours;
				
			}
			else{
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				Double tempRegHrs=nuRegHours-tempOT;
				timeSheet.setPreviewOverTime(new BigDecimal(tempOT.toString()));
				timeSheet.setPreviewRegularHours(new BigDecimal(tempRegHrs.toString()));
				timeSheet=timeSheetManager.save(timeSheet);
				break;
				
			}
			
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	private String postOTProcess(String beginDt, String endDt, String wareHse, String sessionCorpID, String userName, Double nuOTHrs){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			
		Double nuRegHours=0.0;
		Double tempOT=nuOTHrs;
		
		List otNUCrewsList = timeSheetManager.findTimeSheetsForNonUnionOTPost(beginDt,endDt,wareHse, sessionCorpID, userName);
		
		Iterator it =otNUCrewsList.iterator();
		
		while(it.hasNext())
		{
			Object []row33= (Object [])it.next();
			nuRegHours = Double.parseDouble(row33[0].toString());
			if(tempOT.doubleValue()>nuRegHours.doubleValue()){
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				timeSheet.setOverTime(new BigDecimal(nuRegHours.toString()));
				timeSheet.setRegularHours(new BigDecimal(0));
				timeSheet=timeSheetManager.save(timeSheet);
				tempOT = tempOT.doubleValue()-nuRegHours.doubleValue();
				
			}
			else{
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				Double tempRegHrs=nuRegHours.doubleValue()-tempOT.doubleValue();
				timeSheet.setOverTime(new BigDecimal(tempOT.toString()));
				timeSheet.setRegularHours(new BigDecimal(tempRegHrs.toString()));
				timeSheet=timeSheetManager.save(timeSheet);
				break;
				
			}
			
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	private String postPreviewOTProcess(String beginDt, String endDt, String wareHse, String sessionCorpID, String userName, Double nuOTHrs){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			
		Double nuRegHours=0.0;
		Double tempOT=nuOTHrs;
		
		List otNUCrewsList = timeSheetManager.findTimeSheetsForNonUnionOTPost(beginDt,endDt,wareHse, sessionCorpID, userName);
		
		Iterator it =otNUCrewsList.iterator();
		
		while(it.hasNext())
		{
			Object []row33= (Object [])it.next();
			nuRegHours = Double.parseDouble(row33[0].toString());
			if(tempOT.doubleValue()>nuRegHours.doubleValue()){
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				timeSheet.setPreviewOverTime(new BigDecimal(nuRegHours.toString()));
				timeSheet.setPreviewRegularHours(new BigDecimal(0));
				timeSheet=timeSheetManager.save(timeSheet);
				tempOT = tempOT.doubleValue()-nuRegHours.doubleValue();
				
			}
			else{
				timeSheet = timeSheetManager.get(Long.parseLong((row33[1].toString())));
				Double tempRegHrs=nuRegHours.doubleValue()-tempOT.doubleValue();
				timeSheet.setPreviewOverTime(new BigDecimal(tempOT.toString()));
				timeSheet.setPreviewRegularHours(new BigDecimal(tempRegHrs.toString()));
				timeSheet=timeSheetManager.save(timeSheet);
				break;
				
			}
			
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	@SkipValidation
	public String checkBeginHours(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
		//System.out.println("\n\n\n\n id1---->"+id1);
		timeSheet = timeSheetManager.get(id1);
		List regHours=timeSheetManager.getBiginHoursTime(timeSheet.getTicket(),timeSheet.getCrewName(), timeSheet.getWorkDate());
		if(!regHours.isEmpty() && regHours.get(0)!=null )
		{
			
				regularHours=timeSheetManager.getBiginHoursTime(timeSheet.getTicket(),timeSheet.getCrewName(), timeSheet.getWorkDate()).toString();
			
			
		}
		
		//System.out.println("\n\n\n\n regularHours-->"+regularHours);
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	 }
	
	@SkipValidation
	public String processSaveHourly(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		//System.out.println("\n\n\n\n ticket-->"+ticket);
		timeSheetManager.updateCrewType(ticket);
		processPayroll();		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	@SkipValidation
	public String excludeEearningOppertunity(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		//System.out.println("\n\n\n\n ticket-->"+ticket);
		
		List ls = new ArrayList();
		if(!workDt.equalsIgnoreCase("")){
	    try {
	    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
	        newWrkDt = newFwdDate.toString();
	        //System.out.println(newFwdDate.toString());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}   
		}else{
			newWrkDt = "";
		}
	      ls = timeSheetManager.findTickets(wareHse,"",newWrkDt,"",tkt, sessionCorpID);
	      availableCrews = timeSheetManager.searchAvailableCrews(wareHse,newWrkDt,crewNm, sessionCorpID);
	    
	    workTickets = new HashSet(ls);
	    //availableCrews = new HashSet(ls1);
    	timeSheets = timeSheetManager.excludeEearningOppertunity(wareHse,newWrkDt,tkt, crewNm);
    	getSession().setAttribute("countTimeSheet", timeSheets.size());
    	getComboList(sessionCorpID,"a");    	
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
        return SUCCESS;
	}
	
	@SkipValidation
	public String doneForDay(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			
		//System.out.println("\n\n\n\n done for the day");
		
		List ls = new ArrayList();
		if(!workDt.equalsIgnoreCase("")){
	    try {
	    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
	        newWrkDt = newFwdDate.toString();
	        //System.out.println(newFwdDate.toString());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}   
		}else{
			newWrkDt = "";
		}
	      ls = timeSheetManager.findTickets(wareHse,"",newWrkDt,"",tkt, sessionCorpID);
	      availableCrews = timeSheetManager.searchAvailableCrews(wareHse,newWrkDt,crewNm, sessionCorpID);
	    
	    workTickets = new HashSet(ls);
	    //availableCrews = new HashSet(ls1);
    	timeSheets = timeSheetManager.doneForDay(wareHse,newWrkDt,tkt, crewNm);
    	getSession().setAttribute("countTimeSheet", timeSheets.size());
    	getComboList(sessionCorpID,"a");    	
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
        return SUCCESS;
	}
	
	@SkipValidation
	public String filterLunch(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		//System.out.println("\n\n\n\n ticket-->"+ticket);
		
		List ls = new ArrayList();
		if(!workDt.equalsIgnoreCase("")){
	    try {
	    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
	        newWrkDt = newFwdDate.toString();
	        //System.out.println(newFwdDate.toString());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}   
		}else{
			newWrkDt = "";
		}
	      ls = timeSheetManager.findTickets(wareHse,"",newWrkDt,"",tkt,sessionCorpID);
	      availableCrews = timeSheetManager.searchAvailableCrews(wareHse,newWrkDt,crewNm, sessionCorpID);
	    
	    workTickets = new HashSet(ls);
	    //availableCrews = new HashSet(ls1);
    	timeSheets = timeSheetManager.filterLunch(wareHse,newWrkDt,tkt, crewNm);
    	getSession().setAttribute("countTimeSheet", timeSheets.size());
    	getComboList(sessionCorpID,"a");    	
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
        return SUCCESS;
	}
	
	
	@SkipValidation
	public String openTimeSheets(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		//System.out.println("\n\n\n\n ticket-->"+ticket);
		
		List ls = new ArrayList();
		if(!workDt.equalsIgnoreCase("")){
	    try {
	    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
	        newWrkDt = newFwdDate.toString();
	        //System.out.println(newFwdDate.toString());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}   
		}else{
			newWrkDt = "";
		}
	      ls = timeSheetManager.findTickets(wareHse,"",newWrkDt,"",tkt, sessionCorpID);
	      availableCrews = timeSheetManager.searchAvailableCrews(wareHse,newWrkDt,crewNm, sessionCorpID);
	    
	    workTickets = new HashSet(ls);
	    //availableCrews = new HashSet(ls1);
    	timeSheets = timeSheetManager.openTimeSheets(wareHse,newWrkDt,tkt, crewNm);
    	getSession().setAttribute("countTimeSheet", timeSheets.size());
    	getComboList(sessionCorpID,"a");    	
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
        return SUCCESS;
	}
	
	private String actionCode;
	private List distributionCodeList;
	private List lockPayrollList;
	@SkipValidation
	public String getDistributionCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		distributionCodeList = timeSheetManager.findDistributionCode(actionCode, sessionCorpID);
		getComboList(sessionCorpID,"a");		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	
	
	@SkipValidation
	public String lockPayrollList()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		//wareHouse= refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		lockPayrollList=timeSheetManager.getLockPayrollList(sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	
	public String scheduleResouce(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		getComboList(sessionCorpID,"a");
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		dfltWhse=user.getDefaultWarehouse();
		serviceType= refMasterManager.findByParameterService(sessionCorpID, "TCKTSERVC", serviceGroup);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	private String defaultSortValueFlag;
	@SkipValidation
	public String searchScheduleResouce(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String permKey = sessionCorpID +"-"+"component.truckingOps.OnBasisOfDotInspDue";
		visibilityForCorpId=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		getComboList(sessionCorpID,"a");
		pageCorpID = sessionCorpID;
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getVolumeUnit().equals("Cft")){
					volumeUnit=true;
					defaultVolumeUnit = systemDefault.getVolumeUnit();
				}else{
					volumeUnit=false;
					defaultVolumeUnit = systemDefault.getVolumeUnit();
				}
			truckRequired=systemDefault.getTruckRequired();
			}
			
		}		
		if(workDt==null){
			workDt="";
		}
		if(filter==null){
			filter="";
		}
		if((defaultSortValueFlag!=null && !defaultSortValueFlag.equalsIgnoreCase("")) && defaultSortValueFlag.equalsIgnoreCase("typeofWork")){
			defaultSortValueFlag = "typeofWork";
		}else{
			defaultSortValueFlag = "username";
		}
		List ls = new ArrayList();
		if(!workDt.equalsIgnoreCase("")){
	    try {
	    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
	        newWrkDt = newFwdDate.toString();
	        //System.out.println(newFwdDate.toString());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}   
		}else{
			newWrkDt = "";
		}

		if(flag!=null && flag.equalsIgnoreCase("Y")){
			  resourceList = timeSheetManager.findResourceItemsList(tktWareHse,newWrkDt,sessionCorpID);
		}else{
			if((workticketService==null) || (workticketService.equals(""))){
				serviceType= refMasterManager.findByParameterService(sessionCorpID, "TCKTSERVC", serviceGroup);
				Iterator it = serviceType.entrySet().iterator();
			    while (it.hasNext()) {	    	
			        Map.Entry entry = (Map.Entry)it.next();
			        String key =(String) entry.getKey();
			        serviceInGrp=serviceInGrp.concat(",").concat("'").concat(key).concat("'");
			    }
			    serviceInGrp=serviceInGrp.replaceFirst(",","");
			}else{
				serviceType= refMasterManager.findByParameterService(sessionCorpID, "TCKTSERVC", serviceGroup);
				serviceInGrp="'"+workticketService.concat("'");
			}		
	
			if(filter.equalsIgnoreCase("ShowAll")){
				  workTicketsList = timeSheetManager.findResourceTickets(tktWareHse,newWrkDt,sessionCorpID,serviceInGrp, workticketStatus);
				  Integer workTicketSize=workTicketsList.size();
				  Integer selectedWorkTicketSize=timeSheetManager.selectedWorkTicketSize(tktWareHse,newWrkDt,sessionCorpID,serviceInGrp, workticketStatus);
				  selectedWorkTicket=selectedWorkTicketSize +" out of "+workTicketSize;
					Iterator crewRecordsIterator = workTicketsList.iterator();
				    while (crewRecordsIterator.hasNext()) {	 
				    	int  crewValue=0;
				    	int  vehicleValue=0;
				    	ResourceWorkTickets row=(ResourceWorkTickets)crewRecordsIterator.next();
				    	if(((ResourceWorkTickets) row).getcQty() !=null && !(((ResourceWorkTickets) row).getcQty().toString().trim().equals(""))){
				    		crewValue =Integer.parseInt(((ResourceWorkTickets) row).getcQty());
				    		crewRecords=crewRecords+crewValue;
				    	}
				    	if(((ResourceWorkTickets) row).getvQty() !=null && !(((ResourceWorkTickets) row).getvQty().toString().trim().equals(""))){
				    		vehicleValue =Integer.parseInt(((ResourceWorkTickets) row).getvQty());
				    		vehicleRecords=vehicleRecords+vehicleValue;
				    	}
				    }
			}else{
				  List totalWorkTicketsList = timeSheetManager.findResourceTickets(tktWareHse,newWrkDt,sessionCorpID,serviceInGrp, workticketStatus);
				  workTicketsList = timeSheetManager.findNonSelectedResourceTickets(tktWareHse,newWrkDt,sessionCorpID,serviceInGrp, workticketStatus);
				  Integer totalWorkTicketsSize=totalWorkTicketsList.size();
				  Integer nonSelectedWorkTicketSize=workTicketsList.size();
				  selectedWorkTicket=nonSelectedWorkTicketSize +" out of "+totalWorkTicketsSize;
					Iterator crewRecordsIterator = totalWorkTicketsList.iterator();
				    while (crewRecordsIterator.hasNext()) {	 
				    	int  crewValue=0;
				    	int  vehicleValue=0;
				    	ResourceWorkTickets row=(ResourceWorkTickets)crewRecordsIterator.next();
				    	if(((ResourceWorkTickets) row).getcQty() !=null && !(((ResourceWorkTickets) row).getcQty().toString().trim().equals(""))){
				    		crewValue =Integer.parseInt(((ResourceWorkTickets) row).getcQty());
				    		crewRecords=crewRecords+crewValue;
				    	}
				    	if(((ResourceWorkTickets) row).getvQty() !=null && !(((ResourceWorkTickets) row).getvQty().toString().trim().equals(""))){
				    		vehicleValue =Integer.parseInt(((ResourceWorkTickets) row).getvQty());
				    		vehicleRecords=vehicleRecords+vehicleValue;
				    	}
				    }
			}
			
		
			  availableCrews = timeSheetManager.searchAvailableResourceCrews(wareHse,newWrkDt, sessionCorpID, showAbsent,defaultSortValueFlag);		
			  Integer availableCrewSize=availableCrews.size();
			  Integer selectedAvailableCrewSize=timeSheetManager.selectedAvailableCrewSize(wareHse,newWrkDt, sessionCorpID);
			  if(showAbsent.equalsIgnoreCase("Absent")){   
				  selectedAvailableCrew= "0 out of "+availableCrewSize;
			  }else{
				  selectedAvailableCrew=selectedAvailableCrewSize +" out of "+availableCrewSize; 
			  }
			  
			  availableTrucks=timeSheetManager.searchAvailableTrucks(truckWareHse,newWrkDt,sessionCorpID,visibilityForCorpId);
			  Integer availableTrucksSize=availableTrucks.size();
			  Integer selectedAvailableTrucksSize=timeSheetManager.selectedAvailableTrucksSize(truckWareHse,newWrkDt,sessionCorpID);
			  selectedAvailableTrucks=selectedAvailableTrucksSize +" out of "+availableTrucksSize;
		  
			  String permKey1 = sessionCorpID +"-"+"component.scheduleResouce.separateCrewDriverList";
			  checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey1) ;
			  if(checkFieldVisibility){
				  timeSheetListMap = timeSheetManager.findSecheDuledResourceMap(wareHse,newWrkDt,sessionCorpID,whereClauseSecheDuledResourceMap,whereClauseSecheDuledResourceMap1);
				  countScheduleResources=String.valueOf(timeSheetListMap.entrySet().size());
				  if(fieldName!=null && !fieldName.equalsIgnoreCase("")){
					  fieldList =  timeSheetManager.findDistinctFieldValue(wareHse,newWrkDt,fieldName,sessionCorpID);
				  }
			  }else{
				  timeSheets1 = timeSheetManager.findSecheDuledResource(wareHse,newWrkDt, "", tkt, "", sessionCorpID);
				  countScheduleResources=String.valueOf(timeSheets1.size());
			  }
		}
		Date date1 = null;
		if(!workDt.equalsIgnoreCase("")){
		    try {
		    	date1 = new SimpleDateFormat("dd-MMM-yy").parse(workDt);
		    } catch (ParseException e) {
				
				e.printStackTrace();
			}}
		try{
			String permKey1 = sessionCorpID +"-"+"component.field.Alternative.showClipboardWithUser";
			checkFieldVisibility1=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey1) ;
			String permKey2 = sessionCorpID +"-"+"component.field.Alternative.showClipboardWithoutUser";
			checkFieldVisibility2=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey2) ;
				if(checkFieldVisibility1){
					List scrachcardList = scrachCardManager.getSchedulerScrachCardByUser(getRequest().getRemoteUser(),date1);
					
					if(scrachcardList == null || scrachcardList.isEmpty() || scrachcardList.get(0) == null || scrachcardList.get(0).toString().equalsIgnoreCase("")){
						countScrachCard="0";
					}else{
						scrachCard = (ScrachCard) scrachCardManager.getSchedulerScrachCardByUser(getRequest().getRemoteUser(),date1).get(0);
						countScrachCard="1";
					}
				}
				else if(checkFieldVisibility2)
				{
					List scrachcardList = scrachCardManager.getSchedulerScrachCardByDate(date1);
					
					if(scrachcardList == null || scrachcardList.isEmpty() || scrachcardList.get(0) == null || scrachcardList.get(0).toString().equalsIgnoreCase("")){
						countScrachCard="0";
					}else{
						scrachCard = (ScrachCard) scrachCardManager.getSchedulerScrachCardByDate(date1).get(0);
						countScrachCard="1";
					}
				}
		
		}catch(Exception e){
				e.printStackTrace();
			}	

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	private String orderByValue;
	private String fieldValueName;
	private String whereClauseSecheDuledResourceMap;
	private String whereClauseSecheDuledResourceMap1;
	private String fieldName;
	private List fieldList = new ArrayList();
	  @SkipValidation
	 	public String findCrewTruckListAjax(){ 
			try {
				if(!workDt.equalsIgnoreCase("")){
					    try {
					    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
					        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
					        newWrkDt = newFwdDate.toString();
						} catch (ParseException e) {
							e.printStackTrace();
						}   
					}else{
						newWrkDt = "";
					}
				fieldList =  timeSheetManager.findDistinctFieldValue(wareHse,newWrkDt,fieldName,sessionCorpID);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			} catch (Exception e) {
		    	e.printStackTrace();
			}
			return SUCCESS;
		}
	
	private Map<String, String> fieldNameList = new LinkedHashMap<String, String>();
	private Map<Map, Map> timeSheetListMap;
	private List fullAddForOCity;
	public List getFullAddForOCity() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return fullAddForOCity;
	}

	public void setFullAddForOCity(List fullAddForOCity) {
		this.fullAddForOCity = fullAddForOCity;
	}

	@SkipValidation
	public String findToolTipServiceForCity(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		fullAddForOCity=timeSheetManager.findFullCityAdd(shipNumberForCity,ocityForCity,sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		
		return SUCCESS;
	}
	@SkipValidation
	public String findToolTipServiceForDestinationCity(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		fullAddForOCity=timeSheetManager.findFullDestinationCityAdd(shipNumberForCity,dcityForCity,sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		
		return SUCCESS;
	}
	@SkipValidation
	public String searchResourceItems(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		flag="Y";
		getComboList(sessionCorpID,"a");
		if(workDt==null){ workDt="";}
		if(filter==null){ filter="";}
		if(!workDt.equalsIgnoreCase("")){
		    try {
		    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
		        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
		        newWrkDt = newFwdDate.toString();
			} catch (ParseException e) {			
				e.printStackTrace();
			}   
		}else{
				newWrkDt = "";
		}
		
		resourceList = timeSheetManager.findResourceItemsList(tktWareHse,newWrkDt,sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	@SkipValidation
	public String hideSelectedTicket(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String permKey = sessionCorpID +"-"+"component.truckingOps.OnBasisOfDotInspDue";
		visibilityForCorpId=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;	
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getVolumeUnit().equals("Cft")){
					volumeUnit=true;
				}else{
					volumeUnit=false;
				}
			}
		}	
		if(workDt==null){
			workDt="";
		}
		if((defaultSortValueFlag!=null && !defaultSortValueFlag.equalsIgnoreCase("")) && defaultSortValueFlag.equalsIgnoreCase("typeofWork")){
			defaultSortValueFlag = "typeofWork";
		}else{
			defaultSortValueFlag = "username";
		}
		getComboList(sessionCorpID,"a");
		if((workticketService==null)||(workticketService.equals(""))){
			serviceType= refMasterManager.findByParameterService(sessionCorpID, "TCKTSERVC", serviceGroup);
			Iterator it = serviceType.entrySet().iterator();
		    while (it.hasNext()) {	    	
		        Map.Entry entry = (Map.Entry)it.next();
		        String key =(String) entry.getKey();
		        serviceInGrp=serviceInGrp.concat(",").concat("'").concat(key).concat("'");
		    }
		    serviceInGrp=serviceInGrp.replaceFirst(",","");
		}else{
			serviceType= refMasterManager.findByParameterService(sessionCorpID, "TCKTSERVC", serviceGroup);
			serviceInGrp="'"+workticketService.concat("'");
		}		
		List ls = new ArrayList();
		if(!workDt.equalsIgnoreCase("")){
	    try {
	    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
	        newWrkDt = newFwdDate.toString();
	        //System.out.println(newFwdDate.toString());
		} catch (ParseException e) {
			
			e.printStackTrace();
		}   
		}else{
			newWrkDt = "";
		}
		  List totalWorkTicketsList = timeSheetManager.findResourceTickets(tktWareHse,newWrkDt,sessionCorpID,serviceInGrp, workticketStatus);
		  workTicketsList = timeSheetManager.findNonSelectedResourceTickets(tktWareHse,newWrkDt,sessionCorpID,serviceInGrp, workticketStatus);
		  Integer totalWorkTicketsSize=totalWorkTicketsList.size();
		  Integer nonSelectedWorkTicketSize=workTicketsList.size();
		  selectedWorkTicket=nonSelectedWorkTicketSize +" out of "+totalWorkTicketsSize;
		  
		  availableCrews = timeSheetManager.searchAvailableResourceCrews(wareHse,newWrkDt, sessionCorpID, showAbsent,defaultSortValueFlag);
		  Integer availableCrewSize=availableCrews.size();
		  Integer selectedAvailableCrewSize=timeSheetManager.selectedAvailableCrewSize(wareHse,newWrkDt, sessionCorpID);
		  if(showAbsent.equalsIgnoreCase("Absent")){   
			  selectedAvailableCrew= "0 out of "+availableCrewSize;
		  }else{
			  selectedAvailableCrew=selectedAvailableCrewSize +" out of "+availableCrewSize;
		  }
		  
		  availableTrucks=timeSheetManager.searchAvailableTrucks(truckWareHse,newWrkDt,sessionCorpID,visibilityForCorpId);
		  Integer availableTrucksSize=availableTrucks.size();
		  Integer selectedAvailableTrucksSize=timeSheetManager.selectedAvailableTrucksSize(truckWareHse,newWrkDt,sessionCorpID);
		  selectedAvailableTrucks=selectedAvailableTrucksSize +" out of "+availableTrucksSize;
	   // workTickets = new HashSet(ls);
	    //availableCrews = new HashSet(ls1);
	    //timeSheets1 = timeSheetManager.getAll();
		  String permKey1 = sessionCorpID +"-"+"component.scheduleResouce.separateCrewDriverList";
		  checkFieldVisibility=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey1) ;
		  if(checkFieldVisibility){
			  timeSheetListMap = timeSheetManager.findSecheDuledResourceMap(wareHse,newWrkDt,sessionCorpID,whereClauseSecheDuledResourceMap,whereClauseSecheDuledResourceMap1);
			  countScheduleResources=String.valueOf(timeSheetListMap.entrySet().size());
			  if(fieldName!=null && !fieldName.equalsIgnoreCase("")){
				  fieldList =  timeSheetManager.findDistinctFieldValue(wareHse,newWrkDt,fieldName,sessionCorpID);
			  }
		  }else{
			  timeSheets1 = timeSheetManager.findSecheDuledResource(wareHse,newWrkDt, "", tkt, "", sessionCorpID);
			  countScheduleResources=String.valueOf(timeSheets1.size());
		  }		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	private Boolean checkFieldVisibility=false;	
	public String buildScheduleResource(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		if(mmValidation.equals("Yes") && mobileMoverCheck != null && mobileMoverCheck.equalsIgnoreCase("ok")){
			mobileMoverAction.setRequestedCW(requestedCrews);
			mobileMoverAction.setRequestedWT(requestedWorkTkt);
			mobileMoverAction.setFileLoc("");
			mobileMoverAction.sendToMobileMoverFromResources();
		}
		String partnerCode="";
		getComboList(sessionCorpID,"a");
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(systemDefault.getVolumeUnit().equals("Cft")){
					volumeUnit=true;
				}else{
					volumeUnit=false;
				}
			}
		}	
		Date date = null;
		if(!workDt.equalsIgnoreCase("")){
		    try {
		    	date = new SimpleDateFormat("dd-MMM-yy").parse(workDt);
		    } catch (ParseException e) {
				
				e.printStackTrace();
			}
		}
		int i;
		String[] reqWrkTkts = requestedWorkTkt.split("\\,");
		String[] reqCrews = requestedCrews.split("\\,");
		String[] reqTrucks=requestedTrucks.split("\\,");
		createTimeSheetEntries(date,reqWrkTkts,reqCrews,reqTrucks);
		for(String reqWrkTkt:reqWrkTkts){
		workTicket = workTicketManager.get(Long.parseLong(reqWrkTkt));
		String ticketWarehouse=workTicket.getWarehouse();
		String tktService=workTicket.getService();
		List label4Value=workTicketManager.getLabel4Value(ticketWarehouse,sessionCorpID);
		for(String reqCrew:reqCrews){
		payroll = payrollManager.get(Long.parseLong(reqCrew));
		if((payroll.getVlCode()!=null) && (!payroll.getVlCode().equals(""))){
		List pkCodeList = payrollManager.getPkCodeList(payroll.getVlCode(),sessionCorpID);
		if(pkCodeList!=null && !pkCodeList.isEmpty()){
			Iterator it = pkCodeList.iterator();
			  while (it.hasNext()) {
		     partnerCode=(String)it.next();
		       }
		if(label4Value!=null && !label4Value.isEmpty() && label4Value.get(0)!=null){
		if(label4Value.get(0).toString().equalsIgnoreCase("CT")){
		workTicketManager.updateResourceMiscellaneous(tktService,partnerCode,workTicket.getSequenceNumber(),sessionCorpID);
		}
		}
		}
		}
		}
	     }
		hideSelectedTicket();		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
		
	}
	private List separateCrewList = new ArrayList();
	private List separateTruckList = new ArrayList();
	private String crewNameCheckBox;
	private String truckNumberCheckBox;
	public String scheduleResourceSeparateCrewTruckListAjax() throws Exception {
		if(!workDt.equalsIgnoreCase("")){
			    try {
			    	SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
			        newFwdDate = new StringBuilder(formats.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ));
			        newWrkDt = newFwdDate.toString();
				} catch (ParseException e) {
					e.printStackTrace();
				}   
			}else{
				newWrkDt = "";
			}
		separateCrewList = timeSheetManager.findScheduleResourceSeparateCrewList(wareHse,newWrkDt, tkt,sessionCorpID,"","");
		separateTruckList = timeSheetManager.findScheduleResourceSeparateTruckList(wareHse,newWrkDt, tkt,sessionCorpID,"","");
		return SUCCESS;
	}
	
	private String updateQuery;
	@SkipValidation
	public void updateAssignedCrewForTruckAjax(){
		timeSheetManager.updateAssignedCrewForTruck(updateQuery);
	}
	
	private void createTimeSheetEntries(Date date, String[] reqWrkTkts, String[] reqCrews, String[] reqTrucks) {		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		for (String reqWrkTkt:reqWrkTkts){
			workTicket = workTicketManager.get(Long.parseLong(reqWrkTkt));
			serviceOrder = workTicket.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			if(workTicket.getTargetActual().equalsIgnoreCase("P") && company.getWorkticketQueue()!=null && company.getWorkticketQueue()){
				workTicket.setTargetActual("T");
				workTicket.setStatusDate(new Date());
				workTicket = workTicketManager.save(workTicket);
				serviceOrder = workTicket.getServiceOrder();
				auditTrail = new AuditTrail();
				auditTrail.setCorpID(workTicket.getCorpID());
				auditTrail.setXtranId(workTicket.getId().toString());
				auditTrail.setTableName("workticket");
				auditTrail.setFieldName(workTicket.getTargetActual());
				auditTrail.setDescription("Coordinator");
				auditTrail.setOldValue("Pending");
				auditTrail.setNewValue("Target");
				auditTrail.setUser(getRequest().getRemoteUser());
				auditTrail.setDated(new Date());
				auditTrail.setAlertRole("Coordinator");
				auditTrail.setAlertShipperName(serviceOrder.getFirstName()+" "+serviceOrder.getLastName());
				auditTrail.setAlertFileNumber(workTicket.getTicket().toString());
				auditTrail.setAlertUser(customerFile.getCoordinator());
				auditTrail.setIsViewed(false);
				auditTrailManager.save(auditTrail);
			}
			List<Billing> lst = timeSheetManager.getBilling(workTicket.getShipNumber());
			if(lst == null || lst.isEmpty() || lst.get(0) == null){
				billing = new Billing();
			}else{
				billing = (Billing) lst.get(0);
			}
			List<PayrollAllocation> plst = timeSheetManager.findDistAndCalcCode(workTicket.getJobType(), workTicket.getService(), sessionCorpID,serviceOrder.getMode(),workTicket.getCompanyDivision());
			if(plst == null || plst.isEmpty() || plst.get(0) == null){
				payrollAllocation = new PayrollAllocation();
			}else{
				payrollAllocation = (PayrollAllocation) plst.get(0);
			}
						
			for (String reqCrew:reqCrews){
				payroll = payrollManager.get(Long.parseLong(reqCrew));
				List dateForBeginHours=timeSheetManager.getTimeFromEndHours(workTicket.getTicket(), payroll.getLastName()+", "+payroll.getFirstName(), date);
				timeSheet = new TimeSheet();
				timeSheet.setContract(billing.getContract());
				timeSheet.setTicket(workTicket.getTicket());
				timeSheet.setWarehouse(payroll.getWarehouse());
				timeSheet.setDistributionCode(payrollAllocation.getCode());
				timeSheet.setCalculationCode(payrollAllocation.getCalculation());
				timeSheet.setCrewName(payroll.getLastName()+", "+payroll.getFirstName());
				timeSheet.setShipper(workTicket.getLastName()+", "+workTicket.getFirstName());
				timeSheet.setUserName(payroll.getUserName());
				timeSheet.setCrewType(payroll.getTypeofWork());
				timeSheet.setCrewEmail(payroll.getCrewEmail());
				if(payroll.getIntegrationTool()!=null && payroll.getIntegrationTool().equals("LISA") && timeSheetManager.getPlannedMove(workTicket.getService(), "TCKTSERVC", workTicket.getCorpID()))
				{
				timeSheet.setIntegrationTool(payroll.getIntegrationTool());
				}
				timeSheet.setLunch("No lunch");
				timeSheet.setCreatedOn(new Date());
				timeSheet.setDoneForTheDay("false");
				//timeSheet.setUpdatedOn(new Date());
				timeSheet.setCreatedBy(getRequest().getRemoteUser());
				if((!dateForBeginHours.isEmpty()) && (dateForBeginHours.get(0)!=null)){
					timeSheet.setBeginHours(dateForBeginHours.get(0).toString());
				}
				
				if(payroll.getPayhour() == null || payroll.getPayhour().equalsIgnoreCase("")){
					timeSheet.setRegularPayPerHour("0");
					timeSheet.setOverTimePayPerHour("0");
					timeSheet.setDoubleOverTimePayPerHour("0");
					
				}else{
				timeSheet.setRegularPayPerHour(payroll.getPayhour());
				timeSheet.setOverTimePayPerHour(String.valueOf((Double.parseDouble(payroll.getPayhour())*1.5)));
				timeSheet.setDoubleOverTimePayPerHour(String.valueOf((Double.parseDouble(payroll.getPayhour())*2)));
				}
				
				if(!(timeSheet.getPaidRevenueShare() == null) && timeSheet.getPaidRevenueShare().equals("")){
					timeSheet.setPaidRevenueShare(new BigDecimal(0));
				}
				timeSheet.setAction("C");
				timeSheet.setCorpID(sessionCorpID);
				timeSheet.setWorkDate(date);
				timeSheet.setUpdatedOn(new Date());
				timeSheet.setUpdatedBy(getRequest().getRemoteUser());
				timeSheet.setIsCrewUsed(true);
	 	    	timeSheet=timeSheetManager.save(timeSheet);
	 	    	timeSheetManager.updateWorkTicketCrew(workTicket.getTicket(),getRequest().getRemoteUser(),sessionCorpID);
	 	    		for(String truckId:reqTrucks){
	 	    			if (!"".equals(truckId)) {
	 	    				truck=truckManager.get(Long.parseLong(truckId));
							truckingOperations=new TruckingOperations();
							truckingOperations.setCorpID(sessionCorpID);
							truckingOperations.setDescription(truck.getDescription());
							truckingOperations.setLocalTruckNumber(truck.getLocalNumber());
							truckingOperations.setTicket(workTicket.getTicket());
							truckingOperations.setCreatedOn(new Date());
							truckingOperations.setUpdatedOn(new Date());
							truckingOperations.setCreatedBy(getRequest().getRemoteUser());
							truckingOperations.setUpdatedBy(getRequest().getRemoteUser());
							truckingOperations.setIsTruckUsed(true);
							truckingOperations.setWorkDate(date);
							truckingOperations.setDriver(truck.getDriver());
							truckingOperations=truckingOperationsManager.save(truckingOperations);
							truckingOperationsManager.updateWorkTicketTrucks(workTicket.getTicket(),workTicket.getTrucks(),workTicket.getTargetActual(),company.getWorkticketQueue(),"Assign",sessionCorpID);
							ScheduleResourceOperation scheduleResourceOperation=new ScheduleResourceOperation();
							 scheduleResourceOperation.setCorpID(sessionCorpID);
							 scheduleResourceOperation.setIntegrationTool(payroll.getIntegrationTool());
							 scheduleResourceOperation.setCrewName(payroll.getLastName()+", " +payroll.getFirstName());
							 scheduleResourceOperation.setWorkDate(date);
							 scheduleResourceOperation.setCreatedOn(new Date());
							 scheduleResourceOperation.setUpdatedOn(new Date());
							 scheduleResourceOperation.setCreatedBy(getRequest().getRemoteUser());
							 scheduleResourceOperation.setUpdatedBy(getRequest().getRemoteUser());
							 scheduleResourceOperation.setTicket(String.valueOf(workTicket.getTicket()));
							 scheduleResourceOperation.setWorkTicketId(workTicket.getId());
							 scheduleResourceOperation.setShipNumber(workTicket.getShipNumber());
							 scheduleResourceOperation.setOriginCity(workTicket.getCity());
							 scheduleResourceOperation.setDestinationCity(workTicket.getDestinationCity());
							 scheduleResourceOperation.setShipper(workTicket.getLastName()+", "+workTicket.getFirstName());
							 scheduleResourceOperation.setBeginDate(workTicket.getDate1());
							 if(workTicket.getEstimatedCubicFeet()!=null && (!(workTicket.getEstimatedCubicFeet().equals("")))){
								 scheduleResourceOperation.setEstimatedCubicFeet(workTicket.getEstimatedCubicFeet().toString());	 
							 }else{
								 scheduleResourceOperation.setEstimatedCubicFeet("0");
							 }
							 if(workTicket.getEstimatedWeight()!=null && (!(workTicket.getEstimatedWeight().equals("")))){
								 scheduleResourceOperation.setEstimatedWeight(workTicket.getEstimatedWeight().toString());	 
							 }else{
								 scheduleResourceOperation.setEstimatedWeight("0");
							 }
							 if(workTicket.getActualVolume()!=null && (!(workTicket.getActualVolume().equals("")))){
								 scheduleResourceOperation.setActualVolume(workTicket.getActualVolume().toString());	 
							 }else{
								 scheduleResourceOperation.setActualVolume("0");
							 }
							 if(workTicket.getActualWeight()!=null && (!(workTicket.getActualWeight().equals("")))){
								 scheduleResourceOperation.setActualWeight(workTicket.getActualWeight().toString());	 
							 }else{
								 scheduleResourceOperation.setActualWeight("0");
							 }
							 scheduleResourceOperation.setService(workTicket.getService());
							 scheduleResourceOperation.setWarehouse(workTicket.getWarehouse());
							 scheduleResourceOperation.setUnit1(workTicket.getUnit1());
							 scheduleResourceOperation.setUnit2(workTicket.getUnit2());
							 scheduleResourceOperation.setDescription(truck.getDescription());
							 scheduleResourceOperation.setLocaltruckNumber(truck.getLocalNumber());
							 scheduleResourceOperation.setTimesheetId(timeSheet.getId().toString());
							 scheduleResourceOperation.setCrewType(timeSheet.getCrewType());
							 scheduleResourceOperation.setTruckingOpsId(truckingOperations.getId().toString());
							 scheduleResourceOperationManager.save(scheduleResourceOperation);	
						}else{
							 truckingOperationsManager.updateWorkTicketTrucks(workTicket.getTicket(),workTicket.getTrucks(),workTicket.getTargetActual(),company.getWorkticketQueue(),"Assign",sessionCorpID);
			 	    	     ScheduleResourceOperation scheduleResourceOperation=new ScheduleResourceOperation();
							 scheduleResourceOperation.setCorpID(sessionCorpID);
							 scheduleResourceOperation.setIntegrationTool(payroll.getIntegrationTool());
							 scheduleResourceOperation.setCrewName(payroll.getLastName()+", " +payroll.getFirstName());
							 scheduleResourceOperation.setWorkDate(date);
							 scheduleResourceOperation.setCreatedOn(new Date());
							 scheduleResourceOperation.setUpdatedOn(new Date());
							 scheduleResourceOperation.setCreatedBy(getRequest().getRemoteUser());
							 scheduleResourceOperation.setUpdatedBy(getRequest().getRemoteUser());
							 scheduleResourceOperation.setTicket(String.valueOf(workTicket.getTicket()));
							 scheduleResourceOperation.setWorkTicketId(workTicket.getId());
							 scheduleResourceOperation.setShipNumber(workTicket.getShipNumber());
							 scheduleResourceOperation.setOriginCity(workTicket.getCity());
							 scheduleResourceOperation.setDestinationCity(workTicket.getDestinationCity());
							 scheduleResourceOperation.setShipper(workTicket.getLastName()+", "+workTicket.getFirstName());
							 scheduleResourceOperation.setBeginDate(workTicket.getDate1());
							 if(workTicket.getEstimatedCubicFeet()!=null && (!(workTicket.getEstimatedCubicFeet().equals("")))){
								 scheduleResourceOperation.setEstimatedCubicFeet(workTicket.getEstimatedCubicFeet().toString());	 
							 }else{
								 scheduleResourceOperation.setEstimatedCubicFeet("0");
							 }
							 if(workTicket.getEstimatedWeight()!=null && (!(workTicket.getEstimatedWeight().equals("")))){
								 scheduleResourceOperation.setEstimatedWeight(workTicket.getEstimatedWeight().toString());	 
							 }else{
								 scheduleResourceOperation.setEstimatedWeight("0");
							 }
							 if(workTicket.getActualVolume()!=null && (!(workTicket.getActualVolume().equals("")))){
								 scheduleResourceOperation.setActualVolume(workTicket.getActualVolume().toString());	 
							 }else{
								 scheduleResourceOperation.setActualVolume("0");
							 }
							 if(workTicket.getActualWeight()!=null && (!(workTicket.getActualWeight().equals("")))){
								 scheduleResourceOperation.setActualWeight(workTicket.getActualWeight().toString());	 
							 }else{
								 scheduleResourceOperation.setActualWeight("0");
							 }
							 scheduleResourceOperation.setService(workTicket.getService());
							 scheduleResourceOperation.setWarehouse(workTicket.getWarehouse());
							 scheduleResourceOperation.setUnit1(workTicket.getUnit1());
							 scheduleResourceOperation.setUnit2(workTicket.getUnit2());
							 scheduleResourceOperation.setDescription("");
							 scheduleResourceOperation.setLocaltruckNumber("");
							 scheduleResourceOperation.setTimesheetId(timeSheet.getId().toString());
							 scheduleResourceOperation.setCrewType(timeSheet.getCrewType());
							 scheduleResourceOperation.setTruckingOpsId("");
							 scheduleResourceOperationManager.save(scheduleResourceOperation);
							
						}
		 	    	}
	 	     }
	     }
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	}
public String requestedSchedule;
	public String removeScheduleResources(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		String[] reqRmvTimeSts = requestedSchedule.split("\\,");		
		try{
			Long truckOpsId=0l;
		for (String reqRmvTimeSt:reqRmvTimeSts)
	     {
			String[] recordsIds=reqRmvTimeSt.split("--");
			Long timeSheetId=Long.parseLong(recordsIds[0]);
			if(recordsIds[1]!=null && !"".equals(recordsIds[1])){
				 truckOpsId=Long.parseLong(recordsIds[1]);	
			}
			String ticket=recordsIds[2];
			Long scheduleResuourceOperationId=Long.parseLong(recordsIds[3]);
			workTicket = workTicketManager.get(Long.parseLong(workTicketManager.findWorkTicketID(recordsIds[2], sessionCorpID).get(0).toString()));
			/*String date11=recordsIds[4];
			try
	        {  
				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
	             Date du = new Date();
	             du = df.parse(date11);
	             df = new SimpleDateFormat("yyyy-MM-dd");
	             date11 = df.format(du);
	             
	        } 
			catch (ParseException e)
	       {
	          e.printStackTrace();
	       }*/
           if(truckOpsId!=0 && !truckOpsId.equals("") ){
        	   List countTruckOpsId=timeSheetManager.getCountForTruckOpsId(ticket,"","",sessionCorpID,truckOpsId);
   			if(countTruckOpsId!=null && Long.parseLong(countTruckOpsId.get(0).toString())==1){
   				try{
   					truckingOperationsManager.remove(truckOpsId);	
   				}catch(Exception ex){
   					System.out.println("\n\n\n\n exception while removing"+ex);
   					ex.printStackTrace();
   				}
   			}  
           }
			
			List countTimeSheetId=timeSheetManager.getCountForTimeSheetId(ticket,"","",sessionCorpID,timeSheetId);
			if(countTimeSheetId!=null && Long.parseLong(countTimeSheetId.get(0).toString())==1){
				try{
					TimeSheet tseet = timeSheetManager.get(timeSheetId);
					if(tseet!=null && tseet.getIntegrationTool()!=null 
							&& tseet.getIntegrationTool().equals("LISA") 
							&& tseet.getIntegrationStatus()!=null){
						System.out.println("\n\n\n\n timsheetid before remove"+timeSheetId);
						TimeSheet tSHeet = new TimeSheet();
						tSHeet.setTicket(timeSheetId);
						tSHeet.setCreatedOn(new Date());
						tSHeet.setUpdatedOn(new Date());
						tSHeet.setCreatedBy(getRequest().getRemoteUser());
						tSHeet.setUpdatedBy(getRequest().getRemoteUser());
						tSHeet.setCrewName(tseet.getCrewName());
						tSHeet.setUserName(tseet.getUserName());
						tSHeet.setWarehouse("");
						tSHeet.setCorpID(sessionCorpID);
						tSHeet.setCrewType("");
						tSHeet.setWorkDate(workTicket.getDate1());
						tSHeet.setShipper("");
						tSHeet.setLunch("");
						tSHeet.setAction("Cancel");
						tSHeet.setIntegrationStatus("");
						tSHeet.setIntegrationTool("LISA");
						tSHeet.setIntegrationStatus("");
						tseet=timeSheetManager.save(tSHeet);
					}
					timeSheetManager.remove(timeSheetId);
				}catch(Exception ex){
					System.out.println("\n\n\n\n exception while removing"+ex);
					ex.printStackTrace();
				}				
			}
			scheduleResourceOperationManager.remove(scheduleResuourceOperationId);
			truckingOperationsManager.updateWorkTicketTrucks(Long.parseLong(ticket),workTicket.getTrucks(),workTicket.getTargetActual(),company.getWorkticketQueue(),"remove",sessionCorpID);
			timeSheetManager.updateWorkTicketCrew(Long.parseLong(ticket),getRequest().getRemoteUser(),sessionCorpID);
			
	     }
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
			truckRequired=systemDefault.getTruckRequired();
			}
			
		}
		}catch(Exception ex){
			System.out.println("\n\n\n\n exception while removing"+ex);
			ex.printStackTrace();
		}
		//searchScheduleResouce();
		hideSelectedTicket();		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	private String deleteTruckNumber;
	private String deleteCrewName;
	@SkipValidation
	public String deleteCrewTruckFromScheduleResource(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			
			company= companyManager.findByCorpID(sessionCorpID).get(0);
			String ticketNo ="";
			if(deleteTruckNumber!=null && !deleteTruckNumber.equalsIgnoreCase("")){
				try{
					String[] truckNumberIdList = deleteTruckNumber.split("~");
					
					for (String truckNumIdList:truckNumberIdList){
						String[] recordsIds=truckNumIdList.split("#");
						
						String timeSheetId = recordsIds[0];
						String truckingOpsId = recordsIds[1];
						ticketNo = recordsIds[2];
						workTicket = workTicketManager.get(Long.parseLong(workTicketManager.findWorkTicketID(ticketNo, sessionCorpID).get(0).toString()));
						String scheduleResourceId = recordsIds[3];
						
						String[] truckingOpsIdArray= truckingOpsId.split(",");
						for(String truckId:truckingOpsIdArray ){
				   				try{
				   					truckingOperationsManager.remove(Long.parseLong(truckId));
				   				}catch(Exception ex){
				   					System.out.println("\n\n\n\n exception while removing"+ex);
				   					ex.printStackTrace();
				   				}
						}
						
						/*String[] timeSheetIdArray = timeSheetId.split(",");
						for(String crewId:timeSheetIdArray){
							List countTimeSheetId=timeSheetManager.getCountForTimeSheetId(ticketNo,"","",sessionCorpID,Long.parseLong(crewId));
							if(countTimeSheetId!=null && Long.parseLong(countTimeSheetId.get(0).toString())==1){
								try{
									timeSheetManager.remove(Long.parseLong(crewId));
								}catch(Exception ex){
									System.out.println("\n\n\n\n exception while removing"+ex);
									ex.printStackTrace();
								}
							}
						}*/
						
						String[] scheduleResourceIdArray = scheduleResourceId.split(",");
						for(String scheduleResourceIdLong:scheduleResourceIdArray){
							try{
								ScheduleResourceOperation scheduleResourceOperation = scheduleResourceOperationManager.get(Long.parseLong(scheduleResourceIdLong));
									if(scheduleResourceOperation.getCrewName()!=null && !scheduleResourceOperation.getCrewName().equalsIgnoreCase("")){
										scheduleResourceOperation.setLocaltruckNumber("");
										scheduleResourceOperationManager.save(scheduleResourceOperation);
									}else{
										scheduleResourceOperationManager.remove(Long.parseLong(scheduleResourceIdLong));
									}
								//scheduleResourceOperationManager.remove(Long.parseLong(scheduleResourceIdLong));
							}catch(Exception ex){
								System.out.println("\n\n\n\n exception while removing"+ex);
								ex.printStackTrace();
							}
						}
						truckingOperationsManager.updateWorkTicketTrucks(Long.parseLong(ticketNo),workTicket.getTrucks(),workTicket.getTargetActual(),company.getWorkticketQueue(),"remove",sessionCorpID);
						timeSheetManager.updateWorkTicketCrew(Long.parseLong(ticketNo),getRequest().getRemoteUser(),sessionCorpID);
					}					
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			
			if(deleteCrewName!=null && !deleteCrewName.equalsIgnoreCase("")){
				try{
					String[] crewNameIdList = deleteCrewName.split("~");
					
					for (String crewNamIdList:crewNameIdList){
						String[] recordsIds=crewNamIdList.split("#");
						
						String timeSheetId = recordsIds[0];
						String truckingOpsId = recordsIds[1];
						ticketNo = recordsIds[2];
						workTicket = workTicketManager.get(Long.parseLong(workTicketManager.findWorkTicketID(ticketNo, sessionCorpID).get(0).toString()));
						String scheduleResourceId = recordsIds[3];
						
						/*String[] truckingOpsIdArray= truckingOpsId.split(",");
						for(String truckId:truckingOpsIdArray ){
				   				try{
				   					truckingOperationsManager.remove(Long.parseLong(truckId));
				   				}catch(Exception ex){
				   					System.out.println("\n\n\n\n exception while removing"+ex);
				   					ex.printStackTrace();
				   				}
						}*/
						
						String[] timeSheetIdArray = timeSheetId.split(",");
						for(String crewId:timeSheetIdArray){
							List countTimeSheetId=timeSheetManager.getCountForTimeSheetId(ticketNo,"","",sessionCorpID,Long.parseLong(crewId));
							if(countTimeSheetId!=null && (Long.parseLong(countTimeSheetId.get(0).toString())>=1)){
								try{
									timeSheetManager.remove(Long.parseLong(crewId));
								}catch(Exception ex){
									System.out.println("\n\n\n\n exception while removing"+ex);
									ex.printStackTrace();
								}
							}
						}
						
						String[] scheduleResourceIdArray = scheduleResourceId.split(",");
						for(String scheduleResourceIdLong:scheduleResourceIdArray){
							try{
								//scheduleResourceOperationManager.remove(Long.parseLong(scheduleResourceIdLong));
								ScheduleResourceOperation scheduleResourceOperation = scheduleResourceOperationManager.get(Long.parseLong(scheduleResourceIdLong));
								if(scheduleResourceOperation.getLocaltruckNumber()!=null && !scheduleResourceOperation.getLocaltruckNumber().equalsIgnoreCase("")){
									scheduleResourceOperation.setCrewName("");
									scheduleResourceOperationManager.save(scheduleResourceOperation);
								}else{
									scheduleResourceOperationManager.remove(Long.parseLong(scheduleResourceIdLong));
								}
							}catch(Exception ex){
								System.out.println("\n\n\n\n exception while removing"+ex);
								ex.printStackTrace();
							}
						}
						truckingOperationsManager.updateWorkTicketTrucks(Long.parseLong(ticketNo),workTicket.getTrucks(),workTicket.getTargetActual(),company.getWorkticketQueue(),"remove",sessionCorpID);
						timeSheetManager.updateWorkTicketCrew(Long.parseLong(ticketNo),getRequest().getRemoteUser(),sessionCorpID);
					}
					
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
			
			sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					truckRequired=systemDefault.getTruckRequired();
				}				
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	 e.printStackTrace();
		}
		hideSelectedTicket();
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS;
	}
	
	public String absentScheduleResource(){	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		Date date1 = null;
		if(!workDt.equalsIgnoreCase("")){
		    try {
		    	date1 = new SimpleDateFormat("dd-MMM-yy").parse(workDt);
		    } catch (ParseException e) {
				
				e.printStackTrace();
			}
		}
		String[] reqCrew = requestedCrews.split("\\,");
		
		for (int y=0; y<reqCrew.length; y++)
 	     {
			payroll = payrollManager.get(Long.parseLong(reqCrew[y]));
			timeSheet = new TimeSheet();
			timeSheet.setTicket(Long.parseLong("0"));
			timeSheet.setCrewName(payroll.getLastName()+", "+payroll.getFirstName());
			timeSheet.setUserName(payroll.getUserName());
			timeSheet.setWarehouse(payroll.getWarehouse());
			timeSheet.setCorpID(sessionCorpID);
			timeSheet.setCrewType(payroll.getTypeofWork());
			timeSheet.setWorkDate(date1);
			timeSheet.setShipper("");
			timeSheet.setLunch("No lunch");
			timeSheet.setAction("Y");
			if(payroll.getPayhour() == null){
				timeSheet.setRegularPayPerHour("0");
			}else{
			timeSheet.setRegularPayPerHour(payroll.getPayhour());
			}
			List<SystemDefault> sysDefault = timeSheetManager.findSystemDefault(sessionCorpID);
			for(SystemDefault systemDefault : sysDefault){
				timeSheet.setBeginHours(systemDefault.getDayAt1());
				timeSheet.setEndHours(systemDefault.getDayAt2());
			}
			String beginAbsentHrs = timeSheet.getBeginHours();
			String endAbsentHrs = timeSheet.getEndHours();
			Long totalAbsentTimeMnts =  Long.parseLong(endAbsentHrs.substring(0, endAbsentHrs.indexOf(":")))*60 + Long.parseLong(endAbsentHrs.substring(endAbsentHrs.indexOf(":")+1, endAbsentHrs.length())) - Long.parseLong(beginAbsentHrs.substring(0, beginAbsentHrs.indexOf(":")))*60 + Long.parseLong(beginAbsentHrs.substring(beginAbsentHrs.indexOf(":")+1, beginAbsentHrs.length())) ;
			String absentHr = String.valueOf(Double.parseDouble(String.valueOf(totalAbsentTimeMnts))/60);

			timeSheet.setRegularHours(new BigDecimal(absentHr));
			timeSheet.setUpdatedOn(new Date());
			timeSheet.setCreatedOn(new Date());
			timeSheet.setUpdatedBy(getRequest().getRemoteUser());
			timeSheet.setCreatedBy(getRequest().getRemoteUser());
 	    	timeSheetManager.save(timeSheet);

 	     }
		getComboList(sessionCorpID,"a");
		
		hideSelectedTicket();		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
public String presentScheduleResource(){	
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		Date date1 = null;
		if(!workDt.equalsIgnoreCase("")){
		    try {
		    	date1 = new SimpleDateFormat("dd-MMM-yy").parse(workDt);
		    } catch (ParseException e) {
				
				e.printStackTrace();
			}
		}
		String[] reqCrew = requestedCrews.split("\\,");		
		for (int y=0; y<reqCrew.length; y++){
			timeSheetManager.remove(Long.parseLong(reqCrew[y]));
 	     }
		getComboList(sessionCorpID,"a");
		
		hideSelectedTicket();		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	@SkipValidation
	public String getTimetypeFlex1(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		 timetypeFlex1List = refMasterManager.getTimetypeFlex1List(actionCode,"TIMETYPE", sessionCorpID);		
		 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;	
	}
	
	@SkipValidation
	public void updateDriver(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		timeSheetManager.updateDriver(driverId, driverChecked);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
	}
		
	private List tooltipList;
	private String type;
	@SkipValidation
	public String findToolTipResourceDetail(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		tooltipList=timeSheetManager.findToolTipResourceDetail(sessionCorpID,ticket, type); 	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	private List crewDetailList;
	private List vehicleDetailList;
	private List equipmentDetailList;
	private List materialDetailList;
	@SkipValidation
	public String findResourceDetails(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");			
		crewDetailList=timeSheetManager.findCrewResourceDetail(sessionCorpID,ticket);
		vehicleDetailList=timeSheetManager.findVehicleResourceDetail(sessionCorpID,ticket);
		equipmentDetailList=timeSheetManager.findEquipmentResourceDetail(sessionCorpID,ticket);
		materialDetailList=timeSheetManager.findMaterialResourceDetail(sessionCorpID,ticket);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
		return SUCCESS;
	}
	
	public String getBeginDt() {
		return beginDt;
	}

	public void setBeginDt(String beginDt) {
		this.beginDt = beginDt;
	}

	public String getEndDt() {
		return endDt;
	}

	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}

	public String getNoOfTimeSheet() {
		return noOfTimeSheet;
	}

	public void setNoOfTimeSheet(String noOfTimeSheet) {
		this.noOfTimeSheet = noOfTimeSheet;
	}

	public String getNoOfCrews() {
		return noOfCrews;
	}

	public void setNoOfCrews(String noOfCrews) {
		this.noOfCrews = noOfCrews;
	}

	public String getBeginHours() {
		return beginHours;
	}

	public void setBeginHours(String beginHours) {
		this.beginHours = beginHours;
	}

	

	public Long getId1() {
		return id1;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public String getRegularHours() {
		return regularHours;
	}

	public void setRegularHours(String regularHours) {
		this.regularHours = regularHours;
	}

	public List getOverTimeProcessList() {
		return overTimeProcessList;
	}

	public void setOverTimeProcessList(List overTimeProcessList) {
		this.overTimeProcessList = overTimeProcessList;
	}

	public List getIdList() {
		return idList;
	}

	public void setIdList(List idList) {
		this.idList = idList;
	}

	public String getTktWareHse() {
		return tktWareHse;
	}

	public void setTktWareHse(String tktWareHse) {
		this.tktWareHse = tktWareHse;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public List getDistributionCodeList() {
		return distributionCodeList;
	}

	public void setDistributionCodeList(List distributionCodeList) {
		this.distributionCodeList = distributionCodeList;
	}

	public String getEditable() {
		return editable;
	}

	public void setEditable(String editable) {
		this.editable = editable;
	}

	public List getLockPayrollList() {
		return lockPayrollList;
	}

	public void setLockPayrollList(List lockPayrollList) {
		this.lockPayrollList = lockPayrollList;
	}

	public String getTruckWareHse() {
		return truckWareHse;
	}

	public void setTruckWareHse(String truckWareHse) {
		this.truckWareHse = truckWareHse;
	}

	public List getAvailableTrucks() {
		return availableTrucks;
	}

	public void setAvailableTrucks(List availableTrucks) {
		this.availableTrucks = availableTrucks;
	}

	public String getRequestedTrucks() {
		return requestedTrucks;
	}

	public void setRequestedTrucks(String requestedTrucks) {
		this.requestedTrucks = requestedTrucks;
	}

	public Truck getTruck() {
		return truck;
	}

	public void setTruck(Truck truck) {
		this.truck = truck;
	}

	public void setTruckManager(TruckManager truckManager) {
		this.truckManager = truckManager;
	}

	public void setTruckingOperationsManager(
			TruckingOperationsManager truckingOperationsManager) {
		this.truckingOperationsManager = truckingOperationsManager;
	}

	public TruckingOperations getTruckingOperations() {
		return truckingOperations;
	}

	public void setTruckingOperations(TruckingOperations truckingOperations) {
		this.truckingOperations = truckingOperations;
	}

	public String getCountScheduleResources() {
		return countScheduleResources;
	}

	public void setCountScheduleResources(String countScheduleResources) {
		this.countScheduleResources = countScheduleResources;
	}

	public String getRequestedSchedule() {
		return requestedSchedule;
	}

	public void setRequestedSchedule(String requestedSchedule) {
		this.requestedSchedule = requestedSchedule;
	}

	public List getWorkTicketsList() {
		return workTicketsList;
	}

	public void setWorkTicketsList(List workTicketsList) {
		this.workTicketsList = workTicketsList;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setScheduleResourceOperationManager(
			ScheduleResourceOperationManager scheduleResourceOperationManager) {
		this.scheduleResourceOperationManager = scheduleResourceOperationManager;
	}

	public List getTimeSheets1() {
		return timeSheets1;
	}

	public void setTimeSheets1(List timeSheets1) {
		this.timeSheets1 = timeSheets1;
	}

	public String getSelectedWorkTicket() {
		return selectedWorkTicket;
	}

	public void setSelectedWorkTicket(String selectedWorkTicket) {
		this.selectedWorkTicket = selectedWorkTicket;
	}

	public String getSelectedAvailableCrew() {
		return selectedAvailableCrew;
	}

	public void setSelectedAvailableCrew(String selectedAvailableCrew) {
		this.selectedAvailableCrew = selectedAvailableCrew;
	}

	public String getSelectedAvailableTrucks() {
		return selectedAvailableTrucks;
	}

	public void setSelectedAvailableTrucks(String selectedAvailableTrucks) {
		this.selectedAvailableTrucks = selectedAvailableTrucks;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public Map<String, String> getTcktservc() {
		return tcktservc;
	}

	public void setTcktservc(Map<String, String> tcktservc) {
		this.tcktservc = tcktservc;
	}

	public String getWorkticketService() {
		return workticketService;
	}

	public void setWorkticketService(String workticketService) {
		this.workticketService = workticketService;
	}

	public String getWhoticket() {
		return whoticket;
	}

	public void setWhoticket(String whoticket) {
		this.whoticket = whoticket;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Long getWhid() {
		return whid;
	}

	public void setWhid(Long whid) {
		this.whid = whid;
	}

	public Map<String, String> getTcktGrp() {
		return tcktGrp;
	}

	public void setTcktGrp(Map<String, String> tcktGrp) {
		this.tcktGrp = tcktGrp;
	}

	
	public Map<String, String> getServiceType() {
		return serviceType;
	}

	public void setServiceType(Map<String, String> serviceType) {
		this.serviceType = serviceType;
	}

	public String getServiceGroup() {
		return serviceGroup;
	}

	public void setServiceGroup(String serviceGroup) {
		this.serviceGroup = serviceGroup;
	}

	public String getServiceInGrp() {
		return serviceInGrp;
	}

	public void setServiceInGrp(String serviceInGrp) {
		this.serviceInGrp = serviceInGrp;
	}

	public String getWorkticketServiceGroup() {
		return workticketServiceGroup;
	}

	public void setWorkticketServiceGroup(String workticketServiceGroup) {
		this.workticketServiceGroup = workticketServiceGroup;
	}

	
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	
	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public Boolean getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(Boolean volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDfltWhse() {
		return dfltWhse;
	}

	public void setDfltWhse(String dfltWhse) {
		this.dfltWhse = dfltWhse;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public List getTimetypeFlex1List() {
		return timetypeFlex1List;
	}

	public void setTimetypeFlex1List(List timetypeFlex1List) {
		this.timetypeFlex1List = timetypeFlex1List;
	}

	public Map<String, String> getCrewDeviceList() {
		return crewDeviceList;
	}

	public void setCrewDeviceList(Map<String, String> crewDeviceList) {
		this.crewDeviceList = crewDeviceList;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public Boolean getDriverChecked() {
		return driverChecked;
	}

	public void setDriverChecked(Boolean driverChecked) {
		this.driverChecked = driverChecked;
	}

	public String getDefaultVolumeUnit() {
		return defaultVolumeUnit;
	}

	public void setDefaultVolumeUnit(String defaultVolumeUnit) {
		this.defaultVolumeUnit = defaultVolumeUnit;
	}

	public String getShowAbsent() {
		return showAbsent;
	}

	public void setShowAbsent(String showAbsent) {
		this.showAbsent = showAbsent;
	}

	public String getCheckbtn() {
		return checkbtn;
	}

	public void setCheckbtn(String checkbtn) {
		this.checkbtn = checkbtn;
	}

	public Map<String, String> getTcktactn() {
		return tcktactn;
	}

	public void setTcktactn(Map<String, String> tcktactn) {
		this.tcktactn = tcktactn;
	}

	public String getWorkticketStatus() {
		return workticketStatus;
	}

	public void setWorkticketStatus(String workticketStatus) {
		this.workticketStatus = workticketStatus;
	}

	public List getResourceList() {
		return resourceList;
	}

	public void setResourceList(List resourceList) {
		this.resourceList = resourceList;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public AuditTrail getAuditTrail() {
		return auditTrail;
	}

	public void setAuditTrail(AuditTrail auditTrail) {
		this.auditTrail = auditTrail;
	}

	public void setAuditTrailManager(AuditTrailManager auditTrailManager) {
		this.auditTrailManager = auditTrailManager;
	}

	public List getTooltipList() {
		return tooltipList;
	}

	public void setTooltipList(List tooltipList) {
		this.tooltipList = tooltipList;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPageCorpID() {
		return pageCorpID;
	}

	public void setPageCorpID(String pageCorpID) {
		this.pageCorpID = pageCorpID;
	}


	public String getTruckRequired() {
		return truckRequired;
	}

	public void setTruckRequired(String truckRequired) {
		this.truckRequired = truckRequired;
	}


	public String getShipNumberForCity() {
		return shipNumberForCity;
	}

	public String getOcityForCity() {
		return ocityForCity;
	}

	public void setShipNumberForCity(String shipNumberForCity) {
		this.shipNumberForCity = shipNumberForCity;
	}

	public void setOcityForCity(String ocityForCity) {
		this.ocityForCity = ocityForCity;
	}

	public String getTypeSubmit() {
		return typeSubmit;
	}

	public void setTypeSubmit(String typeSubmit) {
		this.typeSubmit = typeSubmit;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}



	public String getMmValidation() {
		return mmValidation;
	}



	public void setMmValidation(String mmValidation) {
		this.mmValidation = mmValidation;
	}



	public void setMobileMoverAction(MobileMoverAction mobileMoverAction) {
		this.mobileMoverAction = mobileMoverAction;
	}


	public Map<String, String> getMmDeviceMap() {
		return mmDeviceMap;
	}



	public void setMmDeviceMap(Map<String, String> mmDeviceMap) {
		this.mmDeviceMap = mmDeviceMap;
	}



	public String getMobileMoverCheck() {
		return mobileMoverCheck;
	}



	public void setMobileMoverCheck(String mobileMoverCheck) {
		this.mobileMoverCheck = mobileMoverCheck;
	}



	public Boolean getVisibilityForCorpId() {
		return visibilityForCorpId;
	}



	public void setVisibilityForCorpId(Boolean visibilityForCorpId) {
		this.visibilityForCorpId = visibilityForCorpId;
	}



	public Long getAbsentId() {
		return absentId;
	}



	public void setAbsentId(Long absentId) {
		this.absentId = absentId;
	}



	public Boolean getCheckFieldVisibility() {
		return checkFieldVisibility;
	}



	public void setCheckFieldVisibility(Boolean checkFieldVisibility) {
		this.checkFieldVisibility = checkFieldVisibility;
	}



	public List getSeparateCrewList() {
		return separateCrewList;
	}



	public void setSeparateCrewList(List separateCrewList) {
		this.separateCrewList = separateCrewList;
	}



	public List getSeparateTruckList() {
		return separateTruckList;
	}



	public void setSeparateTruckList(List separateTruckList) {
		this.separateTruckList = separateTruckList;
	}



	public String getDeleteTruckNumber() {
		return deleteTruckNumber;
	}



	public void setDeleteTruckNumber(String deleteTruckNumber) {
		this.deleteTruckNumber = deleteTruckNumber;
	}



	public String getDeleteCrewName() {
		return deleteCrewName;
	}



	public void setDeleteCrewName(String deleteCrewName) {
		this.deleteCrewName = deleteCrewName;
	}



	public String getUpdateQuery() {
		return updateQuery;
	}



	public void setUpdateQuery(String updateQuery) {
		this.updateQuery = updateQuery;
	}


	public String getCrewNameCheckBox() {
		return crewNameCheckBox;
	}



	public void setCrewNameCheckBox(String crewNameCheckBox) {
		this.crewNameCheckBox = crewNameCheckBox;
	}



	public String getTruckNumberCheckBox() {
		return truckNumberCheckBox;
	}



	public void setTruckNumberCheckBox(String truckNumberCheckBox) {
		this.truckNumberCheckBox = truckNumberCheckBox;
	}


	public Map<Map, Map> getTimeSheetListMap() {
		return timeSheetListMap;
	}



	public void setTimeSheetListMap(Map<Map, Map> timeSheetListMap) {
		this.timeSheetListMap = timeSheetListMap;
	}



	public Map<String, String> getFieldNameList() {
		return fieldNameList;
	}



	public void setFieldNameList(Map<String, String> fieldNameList) {
		this.fieldNameList = fieldNameList;
	}



	public static Logger getLogger() {
		return logger;
	}



	public String getFieldName() {
		return fieldName;
	}



	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}



	public List getFieldList() {
		return fieldList;
	}



	public void setFieldList(List fieldList) {
		this.fieldList = fieldList;
	}



	public String getWhereClauseSecheDuledResourceMap() {
		return whereClauseSecheDuledResourceMap;
	}



	public void setWhereClauseSecheDuledResourceMap(
			String whereClauseSecheDuledResourceMap) {
		this.whereClauseSecheDuledResourceMap = whereClauseSecheDuledResourceMap;
	}



	public String getWhereClauseSecheDuledResourceMap1() {
		return whereClauseSecheDuledResourceMap1;
	}



	public void setWhereClauseSecheDuledResourceMap1(
			String whereClauseSecheDuledResourceMap1) {
		this.whereClauseSecheDuledResourceMap1 = whereClauseSecheDuledResourceMap1;
	}



	public String getFieldValueName() {
		return fieldValueName;
	}



	public void setFieldValueName(String fieldValueName) {
		this.fieldValueName = fieldValueName;
	}



	public String getOrderByValue() {
		return orderByValue;
	}



	public void setOrderByValue(String orderByValue) {
		this.orderByValue = orderByValue;
	}



	public List getCrewDetailList() {
		return crewDetailList;
	}



	public void setCrewDetailList(List crewDetailList) {
		this.crewDetailList = crewDetailList;
	}



	public List getVehicleDetailList() {
		return vehicleDetailList;
	}



	public void setVehicleDetailList(List vehicleDetailList) {
		this.vehicleDetailList = vehicleDetailList;
	}



	public List getEquipmentDetailList() {
		return equipmentDetailList;
	}



	public void setEquipmentDetailList(List equipmentDetailList) {
		this.equipmentDetailList = equipmentDetailList;
	}



	public List getMaterialDetailList() {
		return materialDetailList;
	}



	public void setMaterialDetailList(List materialDetailList) {
		this.materialDetailList = materialDetailList;
	}



	public String getDefaultSortValueFlag() {
		return defaultSortValueFlag;
	}



	public void setDefaultSortValueFlag(String defaultSortValueFlag) {
		this.defaultSortValueFlag = defaultSortValueFlag;
	}

	public boolean isCheckFieldVisibility1() {
		return checkFieldVisibility1;
	}

	public void setCheckFieldVisibility1(boolean checkFieldVisibility1) {
		this.checkFieldVisibility1 = checkFieldVisibility1;
	}

	public boolean isCheckFieldVisibility2() {
		return checkFieldVisibility2;
	}

	public void setCheckFieldVisibility2(boolean checkFieldVisibility2) {
		this.checkFieldVisibility2 = checkFieldVisibility2;
	}

	public ScrachCardManager getScrachCardManager() {
		return scrachCardManager;
	}

	public void setScrachCardManager(ScrachCardManager scrachCardManager) {
		this.scrachCardManager = scrachCardManager;
	}

	public ScrachCard getScrachCard() {
		return scrachCard;
	}

	public void setScrachCard(ScrachCard scrachCard) {
		this.scrachCard = scrachCard;
	}

	public String getCountScrachCard() {
		return countScrachCard;
	}

	public void setCountScrachCard(String countScrachCard) {
		this.countScrachCard = countScrachCard;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public String getSurveyTool() {
		return surveyTool;
	}

	public void setSurveyTool(String surveyTool) {
		this.surveyTool = surveyTool;
	}

	public int getMoveCount() {
		return moveCount;
	}

	public void setMoveCount(int moveCount) {
		this.moveCount = moveCount;
	}

	public String getEmailSetupValue() {
		return emailSetupValue;
	}

	public void setEmailSetupValue(String emailSetupValue) {
		this.emailSetupValue = emailSetupValue;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public String getCheckDriver() {
		return checkDriver;
	}

	public void setCheckDriver(String checkDriver) {
		this.checkDriver = checkDriver;
	}

	public int getVehicleRecords() {
		return vehicleRecords;
	}

	public void setVehicleRecords(int vehicleRecords) {
		this.vehicleRecords = vehicleRecords;
	}
	public int getCrewRecords() {
		return crewRecords;
	}

	public void setCrewRecords(int crewRecords) {
		this.crewRecords = crewRecords;
	}


}
