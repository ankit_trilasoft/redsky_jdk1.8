package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="integrationlog")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class IntegrationLog extends BaseObject {
	 private String corpID;
	 private Long id;
	 private String message;
	 private String fileName;
	 private String batchID;
	 private Date processedOn;
	 private String recordID;
	 private String transactionID;
	 private Date filelastModified;
	 private String messageType;
	 private String messageEvent;
	 private String messageStatus;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("message", message)
				.append("fileName", fileName).append("batchID", batchID)
				.append("processedOn", processedOn)
				.append("recordID", recordID)
				.append("transactionID", transactionID)
				.append("filelastModified", filelastModified)
				.append("messageType", messageType)
				.append("messageEvent", messageEvent)
				.append("messageStatus", messageStatus).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof IntegrationLog))
			return false;
		IntegrationLog castOther = (IntegrationLog) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id).append(message, castOther.message)
				.append(fileName, castOther.fileName)
				.append(batchID, castOther.batchID)
				.append(processedOn, castOther.processedOn)
				.append(recordID, castOther.recordID)
				.append(transactionID, castOther.transactionID)
				.append(filelastModified, castOther.filelastModified)
				.append(messageType, castOther.messageType)
				.append(messageEvent, castOther.messageEvent)
				.append(messageStatus, castOther.messageStatus).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(message)
				.append(fileName).append(batchID).append(processedOn)
				.append(recordID).append(transactionID)
				.append(filelastModified).append(messageType)
				.append(messageEvent).append(messageStatus).toHashCode();
	}
	@Column(length=30)
	public String getBatchID() {
		return batchID;
	}
	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=30)
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Id@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=255)
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Column
	public Date getProcessedOn() {
		return processedOn;
	}
	public void setProcessedOn(Date processedOn) {
		this.processedOn = processedOn;
	}
	@Column(length=30)
	public String getRecordID() {
		return recordID;
	}
	public void setRecordID(String recordID) {
		this.recordID = recordID;
	}
	@Column(length=30)
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	@Column
	public Date getFilelastModified() {
		return filelastModified;
	}
	
	public void setFilelastModified(Date filelastModified) {
		this.filelastModified = filelastModified;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getMessageEvent() {
		return messageEvent;
	}
	public void setMessageEvent(String messageEvent) {
		this.messageEvent = messageEvent;
	}
	public String getMessageStatus() {
		return messageStatus;
	}
	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}
			 

}
