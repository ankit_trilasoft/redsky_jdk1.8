<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% response.setHeader("Cache-Control", "no-cache"); %>
<c:if test="${not empty bookingAgencyMap}">
Quote cannot be accepted as selected Booking Agent Code have not been approved yet.
</c:if>
<c:forEach var="item" items="${bookingAgencyMap}" >
 Booking AgentCode: ${item.key}   Ship Number: ${item.value}
</c:forEach>