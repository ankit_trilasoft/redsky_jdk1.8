<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="findAcctRefNumList.title"/></title>   
    <meta name="heading" content="<fmt:message key='findAcctRefNumList.heading'/>"/> 
<style type="text/css">h2 {background-color: #FBBFFF}</style>  
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b>Acct&nbsp;Ref&nbsp;List</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<s:form id="findAcctRefNumList" action="findAcctRefNumList" method="post">  
<s:set name="findAcctRefNumList" value="findAcctRefNumList" scope="request"/>
<display:table name="findAcctRefNumList" class="table" requestURI="" id="findAcctRefNumList" style="width:200px" defaultsort="1" pagesize="100" >   
<c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
<display:column  headerClass="headerColorInv" title="Company&nbsp;Division" style="width:40px" >
${findAcctRefNumList.companyDivision }
</display:column>
</c:if>
<display:column  headerClass="headerColorInv" title="Acct&nbsp;Reference&nbsp;#" style="width:50px" >
${findAcctRefNumList.accountCrossReference }
</display:column>
<display:column  sortable="true" title="Type" style="width:30px" >
	    <c:if test="${findAcctRefNumList.refType=='P'}">
	    <c:out value="Payable" />
 		</c:if>
 		 <c:if test="${findAcctRefNumList.refType=='R'}">
	    <c:out value="Receivable" />
 		</c:if>
 		<c:if test="${findAcctRefNumList.refType=='S'}">
	    <c:out value="Suspense" />
 		</c:if>
 </display:column>
</display:table>
</s:form>
		  	