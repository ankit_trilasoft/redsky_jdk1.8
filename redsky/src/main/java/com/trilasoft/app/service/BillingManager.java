/**
 * Business Service Interface to handle communication between web and persistence layer.
 * Gets Billing information based on Billing id.
 * @param             BillingId the Billing's id
 * This interface represents the basic actions on "Billing" object in Redsky that allows for Billing  of Shipment.
 * @Interface Name	  BillingManager
 * @Author            Sangeeta Dwivedi
 * @Version           V01.0
 * @Since             1.0
 * @Date              01-Dec-2008
 */

package com.trilasoft.app.service; 
import org.appfuse.service.GenericManager; 
import com.trilasoft.app.model.Billing; 
import java.util.Date;
import java.util.List; 
 
public interface BillingManager extends GenericManager<Billing, Long> { 
    public List findByBillToCode(String billToCode, String sessionCorpID);
    public List findByBillTo2Code(String billTo2Code, String sessionCorpID);
    public List findByPrivateBillToCode(String privatePartyBillingCode, String sessionCorpID);
    public List findByVendorCode(String vendorCode, String sessionCorpID);
    public List checkById(Long id);
    public int updateServiceOrder(String billToC, String billToN, String shipNumber, String vendorCode);
    public List findInsurarFromAccLine(String insurar, String vendorCode, String shipNumber);
    public List findDefaultBookingAgentDetail(String sessionCorpID);
    public List findQuantity(String shipNumber);
    public List getChargeRate(String charge,String billingContract,String sessionCorpID);
    public List getInsuranceChargeRate(String charge,String billingContract,String sessionCorpID);
    public int updateTicket(String shipNumber,String storageMeasurement);
    public List insuranceValues(String insuranceOption, String sessionCorpID);
    public List previewStroageBilling(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID);
    public List previewStroageBillingSearch(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String shipNumbers,String firstName,String lastName,String billsTo,String cycles,String charges,String sessionCorpID,String invoiceBillingDate);
    public List<Billing> getShipNumbers(String beginDate, String endDate,String sessionCorpID);
    public List<Billing>getByShipNumber(String shipNumber);
    public List findBillContractAccountList(String historicalContractsValue,String billToCode, String job, Date createdon, String corpID,String companyDivision, String bookingAgentCode);
    public int updateBillingFromStorage( String billThrough,String shipNumber, String loginUser, Date updatedOn);
    public List findDateDifference(Date invoiceBillingDate, Date dateFrom);
	public List priceContract(String contract);
	public List findReviewStatus(String shipNumber);
	public List findCompanyCode(String corpID);
	
	public List findBookingAgent(String corpID);
	public List getPrimaryBillToCode(Long id,String corpID);
	public void updatePresentCount(Double countTotalNumber, String sessionCorpID, Long id);
	public List getPresentCount(String sessionCorpID);
	public List getTotalCount(String sessionCorpID);
	public List getMaxId(String sessionCorpID);
	
	public List getDateAdd(Date invoiceBillingDate);
	
	public List autoStorageBillingProc(String payMentod, String billToCode, String corpId);
	public void updateStorageMeasurementWT(String storageMeasurement, String shipNumber);
	
	public List accRefComDivision(String companyDivision, String billToCode);
	public List contractBillingInstCode(String contract);
	public List getChargePerValue(String charge, String billingContract);
	public List searchByBillingID(String billToAuthority ,String billToReference,String billingId,String billingIdOut);
	public List previewVatStroageBilling(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID);
	public List previewVatStroageBillingSearch(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String shipNumbers,String firstName,String lastName,String billsTo,String cycles,String charges,String sessionCorpID,String invoiceBillingDate);
	public void updateContract(String contract, String sequenceNumber);
	public boolean getweeklyBilling(String sessionCorpID);
	public List dynamicPreviewVatStroageBilling(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID);
	public List dynamicPreviewVatStroageBillingSearch(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String shipNumbers,String firstName,String lastName,String billsTo,String cycles,String charges,String sessionCorpID,String invoiceBillingDate);
	public List baseBuyRate(String insuranceOption, String sessionCorpID);
	public int updateBillingBilltocode(String billingId, String accountCode, String accountName, String billtocode);
	public String findVendorCurrency(String partnerCode, String sessionCorpID);
	public List getBillingCurrencyAndExchangeRate(String sessionCorpID,String shipNumber,String contract);
	public int updateAccountLineBillToCode(String billToCode, String billToName, Long sid,String billToCodeAcc);
	public List previewStorageBillingForNetworkAgent(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID, String radioContractType);
	public List previewStorageBillingForNetworkAgentCount(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String invoiceBillingDate,String sessionCorpID, String radioContractType);
	public List previewStorageBillingNetworkAgentSearch(String beginDate, String endDate,String condition,String conditionInEx,String conditionBookingCodeType,String shipNumbers,String firstName,String lastName,String billsTo,String cycles,String charges,String sessionCorpID,String invoiceBillingDate, String radioContractType);
	public int updateAgentBillingFromStorage(String toDates, String shipNumber, String loginUser, Date date,String externalCorpID);
	public String findBillToCodeFromBilling(Long sid,String sessionCorpID);
	public String findSecBillToCodeFromBilling(Long sid,String sessionCorpID);
	public String findPrivateBillToCodeFromBilling(Long sid,String sessionCorpID);
	public int updateAccountLineBillToCode1(String billToCode, String billToName, Long sid,String billToCodeAcc1);
    public int updateAccountLineBillToCode2(String billToCode, String billToName, Long sid,String billToCodeAcc2);
	public String getPartnerStorageMail(String partnerCode,String sessionCorpID);
	public String findBillingIdNameAjax(String billToCode,String sessionCorpID);
	public boolean getDmmInsurancePolicy(String sessionCorpID, String contract);
	public int updateBillingNoInsurance(String billingId, String noInsurance, String updatedBy);
	public List findReviewStatusAccrue(Long sid);
	public boolean findVendorCode(String corpID, String partnerCode);
	public int updateBillingNetworkBilltocode(String billingId, String accountCode, String accountName, String billtocode);
	public int updateAccNetworkBilltocode(String sameInstanceAccId, String accountCode, String accountName, String billtocode, String sameInstanceBillingId);
	public String getSellBuyRate(String billToCode, String sessionCorpID);
	public void updateBillingRevenueRecognition(String postDate, String shipNumber, String loginUser,String corpId);
	public void updateOIValuation(Double salesTaxForOI, String shipNumber, String loginUser,String corpId);
	public String findBillToCode(Long id, String sessionCorpID);
} 