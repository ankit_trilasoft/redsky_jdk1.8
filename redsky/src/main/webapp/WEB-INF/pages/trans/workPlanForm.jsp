<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="java.util.SortedMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.lang.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="java.util.TreeMap"%>

<head>
<title>Work Planning</title>
<meta name="heading" content="Work Planning" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />	
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
<style> <%@ include file="/common/calenderStyle.css"%> 
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@page import="com.trilasoft.app.model.Company" %>
 </style>
	<% 
    Company company = (Company)request.getAttribute("company");
	Calendar calc = Calendar.getInstance();
	
	calc.setTime(new Date());
	Date from=calc.getTime();
	calc.add(Calendar.DATE, 14);
	Date to=calc.getTime();

	SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yy");
	format1.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
	String fromDate=format1.format(from);
	String toDate=format1.format(to);
	%> 
<style>
	.input-textarea{border:1px solid #219DD1;color:#000000;	font-family:arial,verdana;font-size:12px;height:45px;text-decoration:none;}
.bgblue{background:url(images/blue_band.jpg); height: 30px; width:630px; background-repeat: no-repeat;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #007a8d; padding-left: 40px; }
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       //var cal = new CalendarPopup();
       //cal.showYearNavigation();
       //cal.showYearNavigationInput();   
   </script>
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
function fieldValidate(){
       if(document.forms['workPlanForm'].elements['fromDate'].value==''){
	      	alert("Please select the from date"); 
	     	return false;
	   }if(document.forms['workPlanForm'].elements['toDate'].value==''){
	       	alert("Please select the to date "); 
	       	return false;
	   }
}  


function load()
{      
var fromDate="${fDate}"
var toDate="${tDate}"
var wareHouseUtilization ='No';
//var fromDate = document.forms['workPlanForm'].elements['fromDate'].value;
//var toDate = document.forms['workPlanForm'].elements['toDate'].value;
//var hubID = document.forms['workPlanForm'].elements['hubID'].value;
//var  wHouse = document.forms['workPlanForm'].elements['wHouse'].value
var hubID="${hubID}"
	var wHouse="${wHouse}"
if(("${paraMeter1}")=="false"&&((hubID==null)||(hubID.trim()=='')&&(wHouse==null)||(wHouse=='')))
	
    {
	location.href='workPlanList.html?fromDate='+fromDate+'&toDate='+toDate+'&hubID='+hubID+'&wHouse='+wHouse+'&decorator=popup&popup=true';
	
	}

   if(("${paraMeter1}")=="true"&&((hubID==null)||(hubID.trim()=='')&&(wHouse==null)||(wHouse=='')))
	
    {
	location.href='summaryList.html?fromDate='+fromDate+'&toDate='+toDate+'&hubID='+hubID+'&wHouse='+wHouse+'&decorator=popup&popup=true';
	
	}

    if(("${paraMeter1}")=="false"&&((hubID!=null)||(hubID.trim()!='')&&(wHouse==null)||(wHouse=='')))
 	
     {
 	location.href='workPlanList.html?fromDate='+fromDate+'&toDate='+toDate+'&hubID='+hubID+'&wHouse='+wHouse+'&decorator=popup&popup=true';
 	
 	}

   if(("${paraMeter1}")=="true"&&((hubID!=null)||(hubID.trim()!='')&&(wHouse==null)||(wHouse=='')))
 	
     {
 	location.href='summaryList.html?fromDate='+fromDate+'&toDate='+toDate+'&hubID='+hubID+'&wHouse='+wHouse+'&decorator=popup&popup=true';
 	
 	}



}


function calcDays(){
  document.forms['workPlanForm'].elements['formStatus'].value = '1';

 var date2 = document.forms['workPlanForm'].elements['fromDate'].value;	 
 var date1 = document.forms['workPlanForm'].elements['toDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];

  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }

   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];

   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");

  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);

  if(daysApart < 0){
    alert("Planning Period From Date must be less than To Date");
    //document.forms['workPlanForm'].elements['fromDate'].value='';
    document.forms['workPlanForm'].elements['toDate'].value='';
  } 
 	document.forms['workPlanForm'].elements['checkDaysClickTicket'].value = '';
	
}
function forDays(){
 document.forms['workPlanForm'].elements['checkDaysClickTicket'].value =1; 
}
function opsCalendar(){
	location.href = 'opsCalendar.html?decorator=popup&popup=true';
}
function dynamicPricingForm(){
	location.href = 'dynamicPricingCalenderView.html?decorator=popup&popup=true';
}
function crewCalendarForm(){
	location.href = 'crewCalenderView.html?decorator=popup&popup=true';
}

function planningCalendar(){
	var checkForAppendFields = 'No';
	<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
		checkForAppendFields ="Yes";
	</configByCorp:fieldVisibility>
	location.href = 'planningCalendarList.html?decorator=popup&popup=true&checkForAppendFields='+checkForAppendFields;
}
function printWorkplanCalendar()
{
	var fromDate="${fDate}"
        var toDate="${tDate}"
	
	/*
	document.forms[0].action = "printWorkplanCalendar.html";
	document.forms[0].method = "post"; // 
	document.forms[0].submit();
   
	*/
	
	//var fromDate = document.forms['workPlanForm'].elements['fromDate'].value;
//	var toDate = document.forms['workPlanForm'].elements['toDate'].value;
	//var hubID = document.forms['workPlanForm'].elements['hubID'].value;
	//var  wHouse = document.forms['workPlanForm'].elements['wHouse'].value
	var hubID="${hubID}"
		var wHouse="${wHouse}"
	if(("${paraMeter1}")=="true" || ("${paraMeter1}")=="false")
	//if((hubID!=null)&&(hubID.trim()!=''))
	{
		
	
	
	if("${paraMeter1}"=="true"){
		javascript:openWindow("printWorkplanCalendarSummary.html?hubID="+hubID+"&wHouse="+wHouse+"&fromDate="+fromDate+"&toDate="+toDate+"&decorator=popup&popup=true",1000,600)
		
}
	else
{
		javascript:openWindow("printWorkplanCalendar.html?hubID="+hubID+"&wHouse="+wHouse+"&fromDate="+fromDate+"&toDate="+toDate+"&decorator=popup&popup=true",1000,600)
	
}
	}
	
		

else
{
alert("kindly find workPlanList/summary data");


}
	}


</script>
</head>

<s:form cssClass="form_magn" name="workPlanForm" id="workPlanForm" action='${empty param.popup?"workPlanList.html":"workPlanList.html?decorator=popup&popup=true"}' method="post" validate="true">
<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
	<s:hidden name="wareHouseUtilization" value="Yes" />
</configByCorp:fieldVisibility>
<%--
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/--%>



 <c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 
<s:hidden id="checkDaysClickTicket" name="checkDaysClickTicket" />
<s:hidden name="formStatus" value=""/>
<div id="layer1" style="width:100%;">
<div class="clearfix" style="margin-top:20px;"></div>
<div id="newmnav" class="nav_tabs"><!-- sandeep -->
	<ul>
		<c:if test="${not empty param.popup}">  
		<li><a onclick="opsCalendar();"><span>Ops Calendar</span></a></li>
		<configByCorp:fieldVisibility componentId="component.field.Resource.DynamicPricingView">
		<li><a onclick="dynamicPricingForm();"><span>Price Calendar</span></a></li>
		</configByCorp:fieldVisibility>
		<c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
			<li><a onclick="crewCalendarForm();"><span>Crew Calendar</span></a></li>
		</c:if>
		</c:if>
		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Work Planning</span></a></li>	
		<configByCorp:fieldVisibility componentId="component.field.Resource.OpsCalendarView">
		<c:if test="${not empty param.popup}"> 
		<li><a onclick="planningCalendar();"><span>Planning Calendar</span></a></li>
		</c:if>
		</configByCorp:fieldVisibility>
		
		
		
		
			<div style="float:right;position: relative;top: -8px;right: 20px;">
	<table class="" cellspacing="1" cellpadding="2" border="0" style="margin:0px" width="100%">
	<tr>		  		

		  	<td align="right" width="20px" class="listwhitetext">Hub</td>
	  		<td align="left" class="listwhitetext" width="100px"><s:select cssClass="list-menu" name="hubID" list="%{hub}" cssStyle="width:100px;height:21px;" headerKey="" headerValue="" onchange="makeWarehouseList(this.value);"/></td>
	  		
	  		
			<td align="right" width="63px" class="listwhitetext" >Warehouse</td>		  
	  	  	<td width="160px" > 
	  	  	<s:select cssClass="list-menu" name="wHouse" list="%{house}" cssStyle="width:175px;height:21px;" headerKey="" headerValue="" />	 
		  		</td>
			<!-- <td width="90"> <input type="button" class="cssbutton" style="margin-right: 5px;width:85px;" value="Reload" onclick="refreshCalendar();"/></td>
			<td><input type="button" class="cssbutton" style="margin-right:5px;width:65px;" value="Print" onclick="printPlanningCalendar();"/></td>	 -->
			<td width="70"><button type="button" class="btn btn-xs btn-success btn-blue" style="width:auto;padding:3px 8px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.16),0 2px 5px 0 rgba(0,0,0,.12) !important;"  title="Reload" onclick="load();">Reload <i class="fa fa-refresh"></i></button></td>
		<td><button type="button" class="btn btn-xs btn-success" style="width:auto;padding:6px 8px;box-shadow: 0 2px 3px 0 rgba(0,0,0,.16),0 2px 5px 0 rgba(0,0,0,.12) !important;"  title="Print" onclick="printWorkplanCalendar();"><i class="fa fa-print"></i></button></td>  		

</tr>	 
		 				
</table>
	</div>
		
		
		
		
	</ul>	
		</div>
		
		<div class="spnblk">&nbsp;</div>
		
<div id="content" align="center" >
<div id="liquid-round">
	
   <div class="top" style="margin-top: 10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="2" border="0" style="" width="">	
	
	<tr>
		<td height="5px">&nbsp;</td>
	</tr>	
	<tr>		  		
		<td align="right" width="130px" class="listwhitetext" >Planning Period From</td>		  
	  	<td width="160px" > 	 
		  	<s:textfield cssClass="input-text" id="fromDate" name="fromDate" value="%{fromDate}" size="8" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this);" cssStyle="width:127px;" /> 
		  	<img id="fromDate_trigger" align="top" src="${pageContext.request.contextPath}/images/calender.png"HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" />
		</td>
		<td align="right" class="listwhitetext"> To</td>	
		<td width="160px" align="left" class="listwhitetext" >
		  	<s:textfield cssClass="input-text" id="toDate" name="toDate" value="%{toDate}" size="8" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this)" onselect="calcDays()" cssStyle="width:127px;" /> 
		  	<img id="toDate_trigger" align="top" src="${pageContext.request.contextPath}/images/calender.png"HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" />
		</td>
	 </tr>
	<tr>
		<td height="20px"></td>
	</tr>
	<tr>	
	  	<td></td>			
		<td colspan="2"><s:submit cssClass="cssbutton" cssStyle="width:155px;" align="top" value="Work Plan Detail List" onclick="return fieldValidate();"/></td>
		<td colspan="2"> <input type="button" class="cssbutton" style="margin-right: 5px;width:155px;" value="Work Plan Summary List" onclick="return summaryList();"/></td>
	</tr>
	<tr>
		<td height="10"></td>
	</tr>	 				
</table> 







</div>
<div class="bottom-header"><span></span></div>
</div>

 <c:if test="${not empty workListDetails}">


<c:if test="${workListDetails!='[]'}">
	<display:table name="workListDetails" class="table" requestURI="" id="workListDetails" export="true" pagesize="200" defaultsort="1"> 
	    <display:column property="dateDisplay" group="1" sortable="true" title="Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column property="ticket" sortable="true" title=" Total Ticket"/>
	    <display:column property="date1" sortable="true" title="Begin Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column property="date2" sortable="true" title="End Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column property="warehouse" sortable="true" title="Warehouse"/>
	    <display:column property="estimatedweight" headerClass="containeralign" sortable="true" style="text-align:right;" title="Est. Weight"/>
	    <display:column property="service" sortable="true" title="Service"/>
	    <c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
	    	<display:column property="requiredcrew" sortable="true" title="Crew Required"/>
	    </c:if>
	 	<display:column property="duration" sortable="true" title="Duration"/>
	 	<display:column property="avgWt" sortable="true" headerClass="containeralign" style="text-align:right;" title="Total Avg. Weight"/> 	
	 	<configByCorp:fieldVisibility componentId="component.field.Alternative.showForUghkOnly">		 	
	 	<c:choose>
 			<c:when test="${workListDetails.service=='LD'}">
			     <display:column title="Origin&nbsp;Country" sortable="true" ><c:out value="${workListDetails.originCountry}" /></display:column>
			</c:when>
			<c:otherwise> 
 				<display:column title="Origin&nbsp;Country" sortable="true" ><c:out value=""></c:out></display:column>
		    </c:otherwise>			   
		</c:choose> 
	 	<c:choose>
			<c:when test="${workListDetails.service=='DL'}" >
			   <display:column title="Destination&nbsp;Country" sortable="true" ><c:out value="${workListDetails.destinationCountry}" /></display:column>
		    </c:when>
			<c:otherwise> 
 				<display:column title="Destination&nbsp;Country" sortable="true" ><c:out value=""></c:out></display:column>
		    </c:otherwise>			   
		</c:choose> 
		</configByCorp:fieldVisibility>
	    <display:setProperty name="paging.banner.item_name" value="workListDetails"/>
	    <display:setProperty name="paging.banner.items_name" value="people"/>	
	    <display:setProperty name="export.excel.filename" value="Work Planning List.xls"/>
	    <display:setProperty name="export.csv.filename" value="Work Planning List.csv"/>
	    <display:setProperty name="export.pdf.filename" value="Work Planning List.pdf"/>
	</display:table>
	</c:if>

<c:if test="${workListDetails=='[]'}">
<display:table name="workListDetails" class="table" requestURI="" id="workListDetails" export="true" pagesize="200" defaultsort="1"> 
	    <display:column sortable="true" title="Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column sortable="true" title=" Total Ticket"/>
	    <display:column sortable="true" title="Begin Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column sortable="true" title="End Date" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column sortable="true" title="Warehouse"/>
	    <display:column sortable="true" title="Est. Weight"/>
	    <display:column sortable="true" title="Service"/>
	    <c:if test="${checkCrewListRole == true && hubWarehouseLimit=='Crw'}">
	    	<display:column sortable="true" title="Crew Required"/>
	    </c:if>
	 	<display:column sortable="true" title="Duration"/>
	 	<display:column sortable="true" title="Total Avg. Weight"/> 	 		
	 	<configByCorp:fieldVisibility componentId="component.field.Alternative.showForUghkOnly">
	 	<display:column sortable="true" title="Origin&nbsp;Country"/>
	 	<display:column sortable="true" title="Destination&nbsp;Country"/> 		 	
		</configByCorp:fieldVisibility>
	    <display:setProperty name="paging.banner.item_name" value="workListDetails"/>
	    <display:setProperty name="paging.banner.items_name" value="people"/>	
	    <display:setProperty name="export.excel.filename" value="Work Planning List.xls"/>
	    <display:setProperty name="export.csv.filename" value="Work Planning List.csv"/>
	    <display:setProperty name="export.pdf.filename" value="Work Planning List.pdf"/>
	</display:table>
</c:if>
</c:if>



<c:if test="${not empty workList}">

</div>

<table class="mainDetailTable" cellpadding="0" cellspacing="0" border="0" style="margin:0px; padding:0px; width:100%;!margin-top:3px;">
	<tr style="margin: 0px; padding: 0px;">
		<td class="heading-bg" align="center">Date</td>
		<td class="heading-bg" align="center">Warehouse</td>
		<td class="heading-bg" align="center" width="100px">Count & Sum</td>
		<td width="600px" style="margin: 0px; padding: 0px;">
			<table border="0" cellpadding="0" cellspacing="0" style="margin: 0px; padding: 0px;">
				<tr>
					<td colspan="6" class="heading-bg" align="center">Job Count / Avg. Weight Total</td>
				</tr>
				<tr>
					<td class="heading-bg" width="100px" align="center">DL</td>
					<td class="heading-bg" width="100px" align="center">DU</td>
					<td class="heading-bg" width="100px" align="center">LD</td>
					<td class="heading-bg" width="100px" align="center">PK</td>
					<td class="heading-bg" width="100px" align="center">PL</td>
					<td class="heading-bg" width="100px" align="center">UP</td>
				</tr>
			</table>
		</td>
		<td class="heading-bg" width="90px" align="center">Sub Total</td>
		 <configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
		<td class="heading-bg" width="90px" align="center">Utilization</td>
		</configByCorp:fieldVisibility>
	</tr>
	<% 
		BigDecimal totalTickets = new BigDecimal("0.00");
		BigDecimal totalAmounts = new BigDecimal("0.00");
		
		SortedMap <String, Map> dateMap = (SortedMap <String, Map>)request.getAttribute("dateMap");
		SortedMap <String, String> utilizationMap = (SortedMap <String, String>)request.getAttribute("utilizationMap");
		Map <Map, Map> crewWHAndHubVal = (Map <Map, Map>)request.getAttribute("crewWHAndHubVal");
		SortedMap <String, String> hubMap = (SortedMap <String, String>)request.getAttribute("hubMap");
		
		Iterator dateIterator = dateMap.entrySet().iterator(); 
		while (dateIterator.hasNext()) {
			Map.Entry entry = (Map.Entry) dateIterator.next();
			String date = (String) entry.getKey();
	%>	
	<tr height="10px" style="margin: 0px;  padding: 0px;" >
			<td class="list-columnmain" style="border-right:2px solid #D1D1D1;"><%=date%></td>
		<% 
			SortedMap <String, Map> warehouseMap = (SortedMap <String, Map>)dateMap.get(date);
			Iterator whIterator = warehouseMap.entrySet().iterator(); 
			int wh = 0;
			
			BigDecimal dlSum = new BigDecimal(0);
			BigDecimal duSum = new BigDecimal(0);
			BigDecimal ldSum = new BigDecimal(0);
			BigDecimal pkSum = new BigDecimal(0);
			BigDecimal upSum = new BigDecimal(0);
			BigDecimal plSum = new BigDecimal(0);
			
			BigDecimal dlCrew = new BigDecimal(0);
			BigDecimal duCrew = new BigDecimal(0);
			BigDecimal ldCrew = new BigDecimal(0);
			BigDecimal pkCrew = new BigDecimal(0);
			BigDecimal upCrew = new BigDecimal(0);
			BigDecimal plCrew = new BigDecimal(0);
			
			
			BigDecimal warehouseUtilTicket = new BigDecimal(0);
			BigDecimal warehouseUtilWeight = new BigDecimal(0);
			BigDecimal wareHouseCrewVal = new BigDecimal(0);
			BigDecimal divideVal = new BigDecimal(100);
			BigDecimal wareHouseCrewPercentage = new BigDecimal(0);
			BigDecimal hubCrewVal = new BigDecimal(0);
			BigDecimal hubCrewPercentage = new BigDecimal(0);
			String wareHsCrewTemp="";
			String hubCrewTemp="";
			double wareHsCrewTempInt;
			double hubCrewTempInt;

			Double wh1=null;
			Double wh2=null;
			Double h1=null;
			Double h2=null;
			
			Integer serviceCountTotal = 0;
			BigDecimal serviceSumTotal = new BigDecimal(0);
			BigDecimal serviceCrewTotal = new BigDecimal(0);
			
			
			String grandWHTotal="";
			String fGrandWHTotal="";
			String totalHbWt ="";
			String warehouseUtilA="";
			String warehouseUtilB="";
			
			Integer dlCount = 0;
			Integer duCount = 0;
			Integer ldCount = 0;
			Integer pkCount = 0;
			Integer upCount = 0;
			Integer plCount = 0;
			
			String dlString = "";
			String duString = "";
			String ldString = "";
			String pkString = "";
			String upString = "";
			String plString = "";
			
			String fDLString = "";
			String fDUString = "";
			String fLDString = "";
			String fPKString = "";
			String fUPString = "";
			String fPLString = "";
			
			String fUtilizationHbWt = "";
			String tempAllValue="";
			String dailyHubLimitTemp="";
			String dailyWtLimitTemp="";
			double dailyHubLimitTempInt;
			double dailyWtLimitTempInt;
			Integer wHb = 0;
			Integer wWt = 0;
			
			Double d1=null;
			Double d2=null;
			Double d3=null;
			Double d4=null;
			Double w1=null;
			Double w2=null;
			Double w3=null;
			Double w4=null;
			
			String dailyOpValue="";
			String daliyWtValue="";
			String keyValue = "";
			
			while (whIterator.hasNext()) {
				Map.Entry entryWH = (Map.Entry) whIterator.next();
				String wareHouse = (String) entryWH.getKey();
				wh = wh+1;	
				if(wh>1){%>
					<td class="list-columndate" style="border-right:2px solid #D1D1D1;">&nbsp;</td>
				<%}%>
				<td class="list-columnmain" style="border-right:2px solid #D1D1D1;"><%=wareHouse%></td>
				<td class="list-columnmain" style="border-right:2px solid #D1D1D1; ">Ticket Count :<BR>Avg.Wgt.Sum :
				 	<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
						<BR>Crew Required :
					</configByCorp:fieldVisibility>
				</td>
				<td style="width:600px;!width:598px; margin-top:0px; margin-bottom:0px; padding: 0px; border-right:1px solid #D1D1D1;">
					<table border="0" cellpadding="0" cellspacing="0" style="margin: 0px; padding: 0px;">
						<tr>
						<% 
						if(crewWHAndHubVal!=null && !crewWHAndHubVal.isEmpty()){
							for (Map.Entry<Map, Map> entry5 : crewWHAndHubVal.entrySet()) {
								Map <String, String> crewHub =entry5.getValue();
								for (Map.Entry<String, String> entry2 :crewHub.entrySet()) {
									String hubDate = (String)entry2.getKey();
									if(hubDate.equalsIgnoreCase(date)){
										String hubCrew = (String)entry2.getValue();
								    	hubCrewVal = new BigDecimal(hubCrew);
									}
								}
							    Map <String, String> crewWR = entry5.getKey();
							    for (Map.Entry<String, String> entry1 :crewWR.entrySet()) {
							    	String wareHouseCodeAndDate = (String)entry1.getKey();
							    	String wareHouseDate = wareHouseCodeAndDate.split("~")[0];
							    	if(wareHouseDate.equalsIgnoreCase(date)){
							    		String wareHouseCode = wareHouseCodeAndDate.split("~")[1];
							    	if(wareHouseCode.equalsIgnoreCase(wareHouse)){
							    		String wareHouseCrewValString = (String)entry1.getValue();							    		
							    		wareHouseCrewVal = new BigDecimal(wareHouseCrewValString);
								    	}						    	
							    	}						    
								}
							}
						}
						%>						
						<%
						SortedMap <String, Map> serviceMap = (SortedMap <String, Map>)warehouseMap.get(wareHouse);
						SortedMap sumCountDisplayMap = new TreeMap <String, String>();
						
						
						Integer serviceCount = 0;
						BigDecimal serviceSum = new BigDecimal(0);
						BigDecimal serviceCrewDL = new BigDecimal(0);
						BigDecimal serviceCrewDU = new BigDecimal(0);
						BigDecimal serviceCrewLD = new BigDecimal(0);
						BigDecimal serviceCrewPK = new BigDecimal(0);
						BigDecimal serviceCrewPL = new BigDecimal(0);
						BigDecimal serviceCrewUP = new BigDecimal(0);
						BigDecimal serviceCrewAll = new BigDecimal(0);
						
						Map dlMap = serviceMap.get("DL");
						sumCountDisplayMap.put("DL", dlMap.get("count") + "<BR>" + dlMap.get("sum") + "<BR>" + dlMap.get("crew"));
						
						dlSum = dlSum.add(new BigDecimal(dlMap.get("sum").toString()));
						dlCount = dlCount + new Integer(dlMap.get("count").toString());
						dlCrew = dlCrew.add(new BigDecimal(dlMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(dlMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(dlMap.get("count").toString());
						serviceCrewDL = serviceCrewDL.add(new BigDecimal(dlMap.get("crew").toString()));
						
						Map duMap = serviceMap.get("DU");			
						sumCountDisplayMap.put("DU", duMap.get("count") + "<BR>" + duMap.get("sum") + "<BR>" + duMap.get("crew"));
						
						duSum = duSum.add(new BigDecimal(duMap.get("sum").toString()));
						duCount = duCount + new Integer(duMap.get("count").toString());
						duCrew = duCrew.add(new BigDecimal(duMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(duMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(duMap.get("count").toString());
						serviceCrewDU = serviceCrewDU.add(new BigDecimal(duMap.get("crew").toString()));
						
						Map ldMap = serviceMap.get("LD");	 
						sumCountDisplayMap.put("LD", ldMap.get("count") + "<BR>" + ldMap.get("sum") + "<BR>" + ldMap.get("crew"));
						
						ldSum = ldSum.add(new BigDecimal(ldMap.get("sum").toString()));
						ldCount = ldCount + new Integer(ldMap.get("count").toString());
						ldCrew = ldCrew.add(new BigDecimal(ldMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(ldMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(ldMap.get("count").toString());
						serviceCrewLD = serviceCrewLD.add(new BigDecimal(ldMap.get("crew").toString()));
						
						Map pkMap = serviceMap.get("PK");
						sumCountDisplayMap.put("PK", pkMap.get("count") + "<BR>" + pkMap.get("sum") + "<BR>" + pkMap.get("crew"));
						
						pkSum = pkSum.add(new BigDecimal(pkMap.get("sum").toString()));
						pkCount = pkCount + new Integer(pkMap.get("count").toString());
						pkCrew = pkCrew.add(new BigDecimal(pkMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(pkMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(pkMap.get("count").toString());
						serviceCrewPK = serviceCrewPK.add(new BigDecimal(pkMap.get("crew").toString()));
						
						Map plMap = serviceMap.get("PL");
						sumCountDisplayMap.put("PL", plMap.get("count") + "<BR>" + plMap.get("sum") + "<BR>" + plMap.get("crew"));
						
						plSum = plSum.add(new BigDecimal(plMap.get("sum").toString()));
						plCount = plCount + new Integer(plMap.get("count").toString());
						plCrew = plCrew.add(new BigDecimal(plMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(plMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(plMap.get("count").toString());
						serviceCrewPL = serviceCrewPL.add(new BigDecimal(plMap.get("crew").toString()));
						
						Map upMap = serviceMap.get("UP");
						sumCountDisplayMap.put("UP", upMap.get("count") + "<BR>" + upMap.get("sum") + "<BR>" + upMap.get("crew"));
						
						upSum = upSum.add(new BigDecimal(upMap.get("sum").toString()));
						upCount = upCount + new Integer(upMap.get("count").toString());
						upCrew = upCrew.add(new BigDecimal(upMap.get("crew").toString()));
						
						serviceSum = serviceSum.add(new BigDecimal(upMap.get("sum").toString()));
						serviceCount = serviceCount + new Integer(upMap.get("count").toString());
						serviceCrewUP = serviceCrewUP.add(new BigDecimal(upMap.get("crew").toString()));
						serviceCrewAll = serviceCrewDL.add(serviceCrewUP).add(serviceCrewPL).add(serviceCrewPK).add(serviceCrewDU).add(serviceCrewLD);
						
						wh1=serviceCrewAll.doubleValue();
						wh2=wareHouseCrewVal.doubleValue();
						try{
						wareHsCrewTempInt = (wh1/wh2)*100;	
						wareHsCrewTempInt = ( new Float( Math.round(wareHsCrewTempInt)) );
						}catch(Exception e){
							wareHsCrewTempInt = new Double(0);
						}										
						if(wareHsCrewTempInt > 100){
							wareHsCrewTemp = "<font color='red'>"+wareHsCrewTempInt+"</font>";
						}else{											
							wareHsCrewTemp = "<font color='003366'>"+wareHsCrewTempInt+"</font>";
						}
						wareHsCrewTemp = wareHsCrewTemp.substring(0,wareHsCrewTemp.indexOf("."))+"%";
						
						serviceCountTotal = dlCount + duCount + ldCount + pkCount + plCount + upCount;
						serviceSumTotal = dlSum.add(duSum).add(ldSum).add(pkSum).add(plSum).add(upSum);	
						serviceCrewTotal = dlCrew.add(duCrew).add(ldCrew).add(pkCrew).add(plCrew).add(upCrew);	
						
						h1=serviceCrewTotal.doubleValue();
						h2=hubCrewVal.doubleValue();
						try{
							hubCrewTempInt = (h1/h2)*100;	
							hubCrewTempInt = ( new Float( Math.round(hubCrewTempInt)) );							
							}catch(Exception e){
								hubCrewTempInt = new Double(0);
							}
						if(hubCrewTempInt > 100){
							hubCrewTemp = "<font color='red'>"+hubCrewTempInt+"</font>";
						}else{											
							hubCrewTemp = "<font color='003366'>"+hubCrewTempInt+"</font>";
						}
						hubCrewTemp = hubCrewTemp.substring(0,hubCrewTemp.indexOf("."))+"%";						
												
						String grandTotal = grandTotal = serviceCount+"<BR>"+serviceSum;
						String fGrandTotal = "";
						if(grandTotal.contains(".")){
							fGrandTotal = grandTotal.substring(0, grandTotal.indexOf("."));
						}else{
							fGrandTotal = grandTotal;
						}						
												
						if(utilizationMap!=null && !utilizationMap.isEmpty()){
						tempAllValue = utilizationMap.get(wareHouse);
						dailyHubLimitTemp = tempAllValue.split("~")[0];
						dailyWtLimitTemp = tempAllValue.split("~")[1];
						d1=Double.parseDouble(dailyHubLimitTemp);
						d2=Double.parseDouble(dailyWtLimitTemp);
						d3=Double.parseDouble(serviceCount.toString());
						d4=Double.parseDouble(serviceSum.toString());
						dailyHubLimitTempInt = (d3/d1)*100;
						dailyWtLimitTempInt = (d4/d2)*100;
						dailyHubLimitTempInt = ( new Float( Math.round(dailyHubLimitTempInt)) );
						dailyWtLimitTempInt = ( new Float( Math.round(dailyWtLimitTempInt)) );
						
						if(dailyHubLimitTempInt > 100){
							dailyHubLimitTemp = "<font color='red'>"+dailyHubLimitTempInt+"</font>";
						}else{
							dailyHubLimitTemp = "<font color='#003366'>"+dailyHubLimitTempInt+"</font>";
						}
						
						if(dailyWtLimitTempInt > 100){
							dailyWtLimitTemp = "<font color='red'>"+dailyWtLimitTempInt+"</font>";
						}else{
							dailyWtLimitTemp = "<font color='#003366'>"+dailyWtLimitTempInt+"</font>";
						}
						
						dailyHubLimitTemp = dailyHubLimitTemp.substring(0,dailyHubLimitTemp.indexOf("."))+"%";
						dailyWtLimitTemp = dailyWtLimitTemp.substring(0,dailyWtLimitTemp.indexOf("."))+"%";						
						
						fUtilizationHbWt = dailyHubLimitTemp+"<BR>"+dailyWtLimitTemp;
						
						for (Map.Entry<String, String> entry1 : hubMap.entrySet()) {
						    dailyOpValue = entry1.getKey();
						    daliyWtValue = entry1.getValue();						    
						}
						
						warehouseUtilTicket = new BigDecimal(dailyOpValue);
						warehouseUtilWeight = new BigDecimal(daliyWtValue);
						w1=(warehouseUtilTicket).doubleValue();
						w2=(warehouseUtilWeight).doubleValue();
						w3=(double)serviceCountTotal;
						w4=(serviceSumTotal).doubleValue();
						warehouseUtilTicket = BigDecimal.valueOf((w3/w1)*100);
						warehouseUtilWeight = BigDecimal.valueOf((w4/w2)*100);						
						
						warehouseUtilA = warehouseUtilTicket.setScale(0,warehouseUtilTicket.ROUND_HALF_UP).toPlainString();
						warehouseUtilB = warehouseUtilWeight.setScale(0,warehouseUtilWeight.ROUND_HALF_UP).toPlainString();
						
						wHb = Integer.parseInt(warehouseUtilA);
						if(wHb > 100 ){
							warehouseUtilA = "<font color='red'>"+warehouseUtilA+"%"+"</font>";
						}else{
							warehouseUtilA = "<font color='#F37A03'>"+warehouseUtilA+"%"+"</font>";
						}
						
						wWt = Integer.parseInt(warehouseUtilB);
						if(wWt > 100 ){
							warehouseUtilB = "<font color='red'>"+warehouseUtilB+"%"+"</font>";
						}else{
							warehouseUtilB = "<font color='#F37A03'>"+warehouseUtilB+"%"+"</font>";
						}
						
						totalHbWt = warehouseUtilA+"<BR>"+warehouseUtilB;						
						}
						
						grandWHTotal = serviceCountTotal+"<BR>"+serviceSumTotal;
						if(grandWHTotal.contains(".")){
							fGrandWHTotal = grandWHTotal.substring(0, grandWHTotal.indexOf("."));
						}else{
							fGrandWHTotal = grandWHTotal;
						}
						
						totalTickets = totalTickets.add(new BigDecimal(serviceCount));
						totalAmounts = totalAmounts.add(serviceSum);
					
						dlString = dlCount+"<BR>"+dlSum;
						if(dlString.contains(".")){
							fDLString = dlString.substring(0, dlString.indexOf("."));
						}else{
							fDLString = dlString;
						}
						
						duString = duCount+"<BR>"+duSum;
						if(duString.contains(".")){
							fDUString = duString.substring(0, duString.indexOf("."));
						}else{
							fDUString = duString;
						}
						
						ldString = ldCount+"<BR>"+ldSum;
						if(ldString.contains(".")){
							fLDString = ldString.substring(0, ldString.indexOf("."));
						}else{
							fLDString = ldString;
						}
						
						pkString = pkCount+"<BR>"+pkSum;
						if(pkString.contains(".")){
							fPKString = pkString.substring(0, pkString.indexOf("."));
						}else{
							fPKString = pkString;
						}
						
						plString = plCount+"<BR>"+plSum;
						if(plString.contains(".")){
							fPLString = plString.substring(0, plString.indexOf("."));
						}else{
							fPLString = plString;
						}
						
						upString = upCount+"<BR>"+upSum;
						if(upString.contains(".")){
							fUPString = upString.substring(0, upString.indexOf("."));
						}else{
							fUPString = upString;
						}
						
						
												
						Iterator sumCountDisplayMapIter = sumCountDisplayMap.entrySet().iterator(); 
						while (sumCountDisplayMapIter.hasNext()) {
							Map.Entry entryVal = (Map.Entry) sumCountDisplayMapIter.next();
							String iterateKey = (String) entryVal.getKey();
							String iterateValue = (String) sumCountDisplayMap.get(iterateKey); 
							String fValue = "";
							if(iterateValue.contains(".")){
								fValue = iterateValue.substring(0, iterateValue.indexOf("."));
							}else{
								fValue = iterateValue;
							}
							
							if(iterateValue.equalsIgnoreCase("0<BR>0<BR>0")){%>
								<td class="list-columnmain" width="100px"></td>	
							<%}else{%>
								<td align="right" class="list-columnmain" width="100px" style="background-color: #ededed;"><%=fValue%><br>
								<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
								<%if(iterateKey.equalsIgnoreCase("DL")){%>
									<%=serviceCrewDL%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("LD")){%>
									<%=serviceCrewLD%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("PK")){%>
									<%=serviceCrewPK%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("UP")){%>
									<%=serviceCrewUP%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("PL")){%>
									<%=serviceCrewPL%>									
								<%}%>
								<%if(iterateKey.equalsIgnoreCase("DU")){%>
									<%=serviceCrewDU%>									
								<%}%>
								</configByCorp:fieldVisibility>								
								</td>
									
							<%}
						}%>	
						</tr>
					</table>
				</td>
				<%if(grandTotal.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_last_bg" style="background-color: #d2e1f4"></td>	
				<%}else{%>
					<td align="right" class="list_last_bg" style=""><b><%=fGrandTotal%></b>
						<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
						<br><b><%=serviceCrewAll%> </b>
						</configByCorp:fieldVisibility>
					</td>		
				<%}%>	
				 <configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">			
					<td align="right" class="list_last_bg" style=""><b><%=fUtilizationHbWt%></b><br>
						<b><%=wareHsCrewTemp%></b>
					</td>
			</configByCorp:fieldVisibility>	
			
	</tr>
	<%}%>	
	<tr>
		<td class="list-column" style="border-right:2px solid #D1D1D1;"></td>
		<td colspan="2" class="list_sum_bg" style="border-right:2px solid #D1D1D1;"><b>Warehouse Ticket Count<br>Warehouse Weight Total</b><br>
		<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
		<b>Hub Crew Total</b>
		</configByCorp:fieldVisibility>
		</td>
		<td style="width:600px;!width:598px;margin-top:0px; margin-bottom:0px; padding: 0px; border-right:1px solid #D1D1D1;">
			<table border="0" cellpadding="0" cellspacing="0" style="margin: 0px; padding: 0px; ">
				<tr>
				
				<%if(dlString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fDLString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=dlCrew%></b>
					</configByCorp:fieldVisibility>
					</td>	
				<%}%>
				<%if(duString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fDUString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=duCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				<%if(ldString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fLDString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=ldCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				<%if(pkString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fPKString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=pkCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				<%if(plString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fPLString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=plCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				<%if(upString.equalsIgnoreCase("0<BR>0")){%>
					<td class="list_sum_bg" width="100px" style=""></td>	
				<%}else{%>
					<td align="right" class="list_sum_bg" width="100px" style=""><b><%=fUPString%></b>
					<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
					<br><b><%=upCrew%></b>
					</configByCorp:fieldVisibility>
					</td>
				<%}%>
				
				</tr>
			</table>
		</td>
		<%if(grandWHTotal.equalsIgnoreCase("0<BR>0<BR>0")){%>
				<td class="list_sum_bg" style=""></td>	
		<%}else{%>
				<td align="right" class="list_grand_bg" style=""><b><%=fGrandWHTotal%></b>
				<configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
				<br><b><%=serviceCrewTotal %></b>
				</configByCorp:fieldVisibility>
				</td>
		<%}%>
		 <configByCorp:fieldVisibility componentId="component.field.WorkList.utilizationShow">
		<td align="right" class="list_grand_bg" style=""><b><%=totalHbWt%></b><br>
		<b><%=hubCrewTemp%></b>
		</td>
		</configByCorp:fieldVisibility>
	</tr>
	<%}%>	
	<%
		String totalTKT = totalTickets.toString();
		String totalAMT = totalAmounts.toString();
		
		if(totalTKT.contains(".")){
			totalTKT = totalTKT.substring(0, totalTKT.indexOf("."));
		}
		
		if(totalAMT.contains(".")){
			totalAMT = totalAMT.substring(0, totalAMT.indexOf("."));
		}
		
	%>
	<tr>
		<td colspan="4" class="heading-bg" align="right"><b>Total Tickets&nbsp;&nbsp;</b></td>
		<td align="right" class="list_grand_bg" style=""><b><%=totalTKT%></b></td>
		<td align="right" class="list_grand_bg" style=""></td>
	</tr>
	<tr>
		<td colspan="4" class="heading-bg" align="right"><b>Grand Total&nbsp;&nbsp;</b></td>
		<td align="right" class="list_grand_bg" style=""><b><%=totalAMT%></b></td>
		<td align="right" class="list_grand_bg" style=""></td>
	</tr>
</table>
<configByCorp:fieldVisibility componentId="component.field.reportoninterface" >
<table>
<tr><td height="5"></td></tr>
<tr>
<td align="center" style="margin: 0px; padding: 0px"><img alt="XLS File" src="<c:url value='/images/excel32.png'/>" onclick="javascript:openWindow('viewReportWithParam.html?id=2977&reportName=Work Plan Summary&reportParameter_From Date=<fmt:formatDate pattern="MM/dd/yyyy" value="${fromDate}" />&reportParameter_To Date=<fmt:formatDate pattern="MM/dd/yyyy" value="${toDate}" />&reportParameter_Hub ID=${hubID}&reportParameter_Corporate ID=SSCW&fileType=XLS&formReportFlag=R')" /><a> XLS </a></td>
<td width="5"></td>
<td align="center" style="margin: 0px; padding: 0px"><img alt="PDF File" src="<c:url value='/images/pdf32.png'/>" onclick="javascript:openWindow('viewReportWithParam.html?id=2977&reportName=Work Plan Summary&reportParameter_From Date=<fmt:formatDate pattern="MM/dd/yyyy" value="${fromDate}" />&reportParameter_To Date=<fmt:formatDate pattern="MM/dd/yyyy" value="${toDate}" />&reportParameter_Hub ID=${hubID}&reportParameter_Corporate ID=SSCW&fileType=PDF&formReportFlag=R')" /><a> PDF </a></td>
</tr>
</table>
</configByCorp:fieldVisibility>
</c:if>
</div>
</s:form>


<script type="text/javascript">

function makeWarehouseList(tValue){

	
	var target=tValue;
	var key='';
	var value='';
	var lineFile='';
	var tempFile='';
	<c:forEach var="entry" items="${houseHub}">
     key="${entry.key}";
	value="${entry.value}";
	
	if(key.trim()==target.trim()){
		tempFile=value.split(",");
		for(var i=0;i<tempFile.length;i++){
			if(lineFile==''){
			      value=tempFile[i];
				value=value.replace("'","");
				value=value.replace("'","");
				lineFile=value;
			}else{
				
				value=tempFile[i];
				value=value.replace("'","");
				value=value.replace("'","");
				lineFile=lineFile+","+value;				
			}
		}
	}
	</c:forEach>
	
	
	if(lineFile!=''){
	
		var finalList=lineFile.split(",");
		var targetElement = document.forms['workPlanForm'].elements['wHouse'];
		targetElement.length = finalList.length+1;
		document.forms['workPlanForm'].elements['wHouse'].options[0].text = '';
		document.forms['workPlanForm'].elements['wHouse'].options[0].value = '';
		for(i=0;i<finalList.length;i++){
			 key=finalList[i];
			 value=findValueInHouse(finalList[i]);			
			   document.forms['workPlanForm'].elements['wHouse'].options[i+1].text=value;
			   document.forms['workPlanForm'].elements['wHouse'].options[i+1].value=key;
		}
	}else{
		var targetElement = document.forms['workPlanForm'].elements['wHouse'];
		targetElement.length = houseLength()+1;
		document.forms['workPlanForm'].elements['wHouse'].options[0].text = '';
		document.forms['workPlanForm'].elements['wHouse'].options[0].value = '';
		var j=0;
		<c:forEach var="entry" items="${house}">
				key="${entry.key}";
				value="${entry.value}";
			   document.forms['workPlanForm'].elements['wHouse'].options[j+1].text=value;
			   document.forms['workPlanForm'].elements['wHouse'].options[j+1].value=key;
			   j++;
		</c:forEach>	
		document.forms['workPlanForm'].elements['wHouse'].value='';
	}
	<c:if test="${not empty wHouse}">
	document.forms['workPlanForm'].elements['wHouse'].value='${wHouse}';
	</c:if>
		
		
		
	
	
	
	
	
	
}
function houseLength(){
	var cont=0;
	var key='';
	<c:forEach var="entry" items="${house}">
	key="${entry.key}";
	if(key!=''){
		cont++;
	}	
	</c:forEach>	
	return cont;
}
function findValueInHouse(key1){
	var key='';
	var value='';
	<c:forEach var="entry" items="${house}">
	if(value==''){
		key="${entry.key}";
		if(key==key1){
		value="${entry.value}";	
		}
	}	
	</c:forEach>	
	return value;
}
<%--
<c:if test="${empty hubID}">
document.forms['workPlanForm'].elements['hubID'].value='${defaultHub}';
</c:if>--%>
<c:if test="${not empty hubID}">
	makeWarehouseList('${hubID}');
</c:if>
function summaryList(){
	var wareHouseUtilization ='No';
	var fromDate = document.forms['workPlanForm'].elements['fromDate'].value;
	var toDate = document.forms['workPlanForm'].elements['toDate'].value;
	var hubID = document.forms['workPlanForm'].elements['hubID'].value;
	var  wHouse = document.forms['workPlanForm'].elements['wHouse'].value
	try{
		wareHouseUtilization = document.forms['workPlanForm'].elements['wareHouseUtilization'].value;
	}catch(e){}
	if(fromDate == ''){
	      	alert("Please select the from date"); 
	     	return false;
	}if(toDate == ''){
	       	alert("Please select the to date "); 
	       	return false;
	}
	var url='';
	if(wareHouseUtilization=='Yes'){
	if(fromDate != '' && toDate != ''){
		<c:if test="${empty param.popup}">
		location.href='summaryList.html?fromDate='+fromDate+'&toDate='+toDate+'&hubID='+hubID+'&wHouse='+wHouse+'&wareHouseUtilization='+wareHouseUtilization;
		</c:if>
		<c:if test="${not empty param.popup}">  
		location.href='summaryList.html?fromDate='+fromDate+'&toDate='+toDate+'&hubID='+hubID+'&wHouse='+wHouse+'&wareHouseUtilization='+wareHouseUtilization+'&decorator=popup&popup=true';
		</c:if>

	}
	}else{
	if(fromDate != '' && toDate != ''){
		
		<c:if test="${empty param.popup}">
		location.href='summaryList.html?fromDate='+fromDate+'&toDate='+toDate+'&hubID='+hubID+'&wHouse='+wHouse;
		</c:if>
		<c:if test="${not empty param.popup}">  
		location.href='summaryList.html?fromDate='+fromDate+'&toDate='+toDate+'&hubID='+hubID+'&wHouse='+wHouse+'&decorator=popup&popup=true';
		</c:if>

	}
	}
}




 	var fromDate = '<%= fromDate %>';
	var toDate = '<%= toDate %>';
	//var asd = "2018-09-10";

	if('${fDate}' !=""){ 
		document.forms['workPlanForm'].elements['fromDate'].value='${fDate}'	
	}else{
	document.forms['workPlanForm'].elements['fromDate'].value =fromDate;
	}
	
	if('${tDate}' !=""){
		document.forms['workPlanForm'].elements['toDate'].value ='${tDate}'
	}else{
	document.forms['workPlanForm'].elements['toDate'].value =toDate; 
	}

	<%--var hubvalue='';
	<c:set var="cnt" value="0"/>
	<c:forEach var="entry" items="${hub}" >
	<c:if test="${entry.key!=''}">
	<c:set var="cnt" value="${cnt+1}"/>
	hubvalue='${entry.key}';
	</c:if>
	</c:forEach>
	<c:if test="${cnt==1}"> 
	document.forms['workPlanForm'].elements['hubID'].value=hubvalue;
	</c:if> --%>
</script>
<script type="text/javascript">


	setOnSelectBasedMethods(["calcDays()"]);
	setCalendarFunctionality();
</script>