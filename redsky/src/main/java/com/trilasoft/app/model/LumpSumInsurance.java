package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

@Entity
@Table(name="lumpsumInsurance")
public class LumpSumInsurance {
	private Long id;
	private String corpID;
	private String atricle;
	private Long serviceOrderID;
	private Integer valuation;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private Long networkSynchedId;
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof LumpSumInsurance))
			return false;
		LumpSumInsurance castOther = (LumpSumInsurance) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(atricle, castOther.atricle)
				.append(serviceOrderID, castOther.serviceOrderID)
				.append(valuation, castOther.valuation)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(createdOn, castOther.createdOn)
				.append(networkSynchedId, castOther.networkSynchedId).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(atricle)
				.append(serviceOrderID).append(valuation).append(createdBy)
				.append(updatedBy).append(updatedOn).append(createdOn)
				.append(networkSynchedId).toHashCode();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("atricle", atricle)
				.append("serviceOrderID", serviceOrderID)
				.append("valuation", valuation).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("createdOn", createdOn)
				.append("networkSynchedId", networkSynchedId).toString();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getAtricle() {
		return atricle;
	}
	public void setAtricle(String atricle) {
		this.atricle = atricle;
	}
	@Column
	public Long getServiceOrderID() {
		return serviceOrderID;
	}
	public void setServiceOrderID(Long serviceOrderID) {
		this.serviceOrderID = serviceOrderID;
	}
	@Column
	public Integer getValuation() {
		return valuation;
	}
	public void setValuation(Integer valuation) {
		this.valuation = valuation;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column
	public Long getNetworkSynchedId() {
		return networkSynchedId;
	}
	public void setNetworkSynchedId(Long networkSynchedId) {
		this.networkSynchedId = networkSynchedId;
	}
}
