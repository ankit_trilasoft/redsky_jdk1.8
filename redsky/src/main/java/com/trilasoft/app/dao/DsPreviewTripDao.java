package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsPreviewTrip;

public interface DsPreviewTripDao extends GenericDao<DsPreviewTrip, Long>{
	
	List getListById(Long id);

}
