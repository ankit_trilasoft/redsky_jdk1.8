package com.trilasoft.app.dao.hibernate;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;  
import java.util.Scanner;

import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import com.trilasoft.app.model.Location;   
import com.trilasoft.app.dao.LocationDao;   
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
  
public class LocationDaoHibernate extends GenericDaoHibernate<Location, Long> implements LocationDao {   
    private List locations; 
    private HibernateUtil hibernateUtil;
    
    public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	public LocationDaoHibernate() {   
        super(Location.class);   
    }   
  
	public List<Location> findByLocationWithWarehouse(String locationId, String type,String occupieType, String capacity, String sessionCorpID,String warehouse) {
		if(locationId==null){locationId="";} 
		if(type==null){type="";} 
		if(occupieType==null){occupieType="";}
		if(capacity==null){capacity="";}
		if(warehouse==null){warehouse="";}
		
		List list=new ArrayList();
    	if(occupieType.equalsIgnoreCase("All") || occupieType.equalsIgnoreCase("")){
    		list=this.getSession().createSQLQuery("SELECT l.id, l.locationId, l.capacity, l.cubicFeet, l.occupied, l.type, replace(r.description,'\r\t',''),l.warehouse,l.cubicMeter,l.utilizedVolCft,l.utilizedVolCbm FROM location l,refmaster r where l.type=r.code and r.corpID in('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and l.corpID ='"+sessionCorpID+"' and l.warehouse like '"+warehouse+"%' and r.parameter='LOCTYPE' and  l.locationId like '%"+locationId.replaceAll("'", "''")+"%' and r.code like '"+type.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.capacity like '"+capacity.replaceAll(":", "''").replaceAll("'", "''")+"%'").list(); 
		}
    	if(occupieType.equalsIgnoreCase("Occupied")){
    		list=this.getSession().createSQLQuery("SELECT l.id, l.locationId, l.capacity, l.cubicFeet, l.occupied, l.type, replace(r.description,'\r\t',''),l.warehouse,l.cubicMeter,l.utilizedVolCft,l.utilizedVolCbm FROM location l,refmaster r where l.type=r.code and r.corpID in('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and l.corpID = '"+sessionCorpID+"' and  l.warehouse like '"+warehouse+"%' and r.parameter='LOCTYPE' and  l.locationId like '%"+locationId.replaceAll("'", "''")+"%' and r.code like '"+type.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.capacity like '"+capacity.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.occupied  in ('X')").list(); 
		}
    	if(occupieType.equalsIgnoreCase("Empty")){
    		list=this.getSession().createSQLQuery("SELECT l.id, l.locationId, l.capacity, l.cubicFeet, l.occupied, l.type, replace(r.description,'\r\t',''),l.warehouse,l.cubicMeter,l.utilizedVolCft,l.utilizedVolCbm FROM location l,refmaster r where l.type=r.code and r.corpID in('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') and l.corpID = '"+sessionCorpID+"' and  l.warehouse like '"+warehouse+"%' and r.parameter='LOCTYPE' and  l.locationId like '%"+locationId.replaceAll("'", "''")+"%' and r.code like '"+type.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.capacity like '"+capacity.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.occupied not in ('X')").list();  
		}else{
			
		}
    	LocationDTO locationDTO;
		Iterator it=list.iterator();
		List locations=new ArrayList();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			locationDTO=new LocationDTO();
			locationDTO.setId(obj[0]);
			locationDTO.setLocationId(obj[1]);
			locationDTO.setCapacity(obj[2]);
			locationDTO.setCubicFeet(obj[3]);
			locationDTO.setOccupied(obj[4]);
			locationDTO.setType(obj[5]);
			locationDTO.setDescription(obj[6]);
			locationDTO.setWarehouse(obj[7]);
			locationDTO.setCubicMeter(obj[8]);
			locationDTO.setUtilizedVolCft(obj[9]);
			locationDTO.setUtilizedVolCbm(obj[10]);
			
			locations.add(locationDTO);
		}
		return locations;
    
        
	}  
    
    
    public List<Location> findByLocation(String location,String locationtype,String occupied,String capacity,String corpId) {
    	/*String loc=location+"%";
    	String c=locationtype+"%";
    	String d="X"+"%";
    	Criteria crit = this.getSession().createCriteria(Location.class);
    	crit.add( Property.forName("corpID").in( new String[] { corpId, "XXXX", "TSFT" }));
    	crit.add(Restrictions.ilike("locationId",loc));
    	crit.add(Restrictions.ilike("type",c));*/
    	List list=new ArrayList();
    	if(occupied.equalsIgnoreCase("All") || occupied.equalsIgnoreCase("")){
    		list=this.getSession().createSQLQuery("SELECT l.id, l.locationId, l.capacity, l.cubicFeet, l.occupied, l.type, replace(r.description,'\r\t','') FROM location l,refmaster r where l.type=r.code and l.corpID=r.corpID and l.corpID = '"+corpId+"' and r.parameter='LOCTYPE' and  l.locationId like '%"+location.replaceAll("'", "''")+"%' and r.code like '"+locationtype.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.capacity like '"+capacity.replaceAll(":", "''").replaceAll("'", "''")+"%'").list(); 
		}
    	if(occupied.equalsIgnoreCase("Occupied")){
    		list=this.getSession().createSQLQuery("SELECT l.id, l.locationId, l.capacity, l.cubicFeet, l.occupied, l.type, replace(r.description,'\r\t','') FROM location l,refmaster r where l.type=r.code and l.corpID=r.corpID and l.corpID = '"+corpId+"' and r.parameter='LOCTYPE' and  l.locationId like '%"+location.replaceAll("'", "''")+"%' and r.code like '"+locationtype.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.capacity like '"+capacity.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.occupied  in ('X')").list(); 
		}
    	if(occupied.equalsIgnoreCase("Empty")){
    		list=this.getSession().createSQLQuery("SELECT l.id, l.locationId, l.capacity, l.cubicFeet, l.occupied, l.type, replace(r.description,'\r\t','') FROM location l,refmaster r where l.type=r.code and l.corpID=r.corpID and l.corpID = '"+corpId+"' and r.parameter='LOCTYPE' and  l.locationId like '%"+location.replaceAll("'", "''")+"%' and r.code like '"+locationtype.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.capacity like '"+capacity.replaceAll(":", "''").replaceAll("'", "''")+"%' and l.occupied not in ('X')").list();  
		}else{
			
		}
    	LocationDTO locationDTO;
		Iterator it=list.iterator();
		List locations=new ArrayList();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			locationDTO=new LocationDTO();
			locationDTO.setId(obj[0]);
			locationDTO.setLocationId(obj[1]);
			locationDTO.setCapacity(obj[2]);
			locationDTO.setCubicFeet(obj[3]);
			locationDTO.setOccupied(obj[4]);
			locationDTO.setType(obj[5]);
			locationDTO.setDescription(obj[6]);
			locations.add(locationDTO);
		}
		return locations;
    
        
    }   
    
    public List<Location> findByLocationWH(String locationId, String occupied, String type, String warehouse) {
        if(type==null){
        	type="";
        }
    	if(occupied.equalsIgnoreCase("true")){
			return getHibernateTemplate().find("from Location where locationId like '" + locationId + "%' AND (capacity='3' OR occupied = 'X') AND type like '" + type.replaceAll(":", "''") + "%' AND warehouse like '" + warehouse.replaceAll(":", "''") + "%'"); 
		}else{
			return getHibernateTemplate().find("from Location where locationId like '" + locationId + "%' AND (capacity='3' OR occupied = '') AND type like '" + type.replaceAll(":", "''") + "%' AND warehouse like '" + warehouse.replaceAll(":", "''") + "%'"); 
		}
	}
    
   
    public List<Location> findByLocationWHMove(String locationId, String occupied, String type, String warehouse) {
	    	if(occupied.equalsIgnoreCase("true")){
				return getHibernateTemplate().find("from Location where locationId like '" + locationId + "%' AND (capacity='3' OR occupied = 'X') AND type like '" + type.replaceAll(":", "''") + "%' AND warehouse like '" + warehouse.replaceAll(":", "''") + "%'"); 
			}else{
				return getHibernateTemplate().find("from Location where locationId like '" + locationId + "%' AND (capacity='3' OR occupied = '') AND type like '" + type.replaceAll(":", "''") + "%' AND warehouse like '" + warehouse.replaceAll(":", "''") + "%'"); 
			}
		}
    
    public List getOccupiedList(){
    	return getHibernateTemplate().find("from Location where occupied = ''"); 
    }
    
    public Location getByLocation(String locationId){
    	List list = getHibernateTemplate().find("from Location where locationId like '" +locationId+"%' ");
    	if(list!=null && !list.isEmpty()){
    		return (Location)list.get(0);
    	}else{
    		return null;
    	}
    }
    
    public List locationList(String corpId){
    	getHibernateTemplate().setMaxResults(500);
    	List list= getHibernateTemplate().find("SELECT l.id, l.locationId, l.capacity, l.cubicFeet, l.occupied, l.type,r.description,l.warehouse,l.cubicMeter,l.utilizedVolCft,l.utilizedVolCbm FROM Location l,RefMaster r where l.type=r.code and r.parameter='LOCTYPE' and l.corpID = '"+corpId+"'");
    	LocationDTO locationDTO;
		Iterator it=list.iterator();
		List location=new ArrayList();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			locationDTO=new LocationDTO();
			locationDTO.setId(obj[0]);
			locationDTO.setLocationId(obj[1]);
			locationDTO.setCapacity(obj[2]);
			locationDTO.setCubicFeet(obj[3]);
			locationDTO.setOccupied(obj[4]);
			locationDTO.setType(obj[5]);
			locationDTO.setDescription(obj[6]);
			locationDTO.setWarehouse(obj[7]);
			locationDTO.setCubicMeter(obj[8]);
			locationDTO.setUtilizedVolCft(obj[9]);
			locationDTO.setUtilizedVolCbm(obj[10]);
			location.add(locationDTO);
		}
		return location;
    }
 
    public class LocationDTO {
    	private Object id;
    	private Object locationId;
    	private Object capacity; 
    	private Object warehouse; 
    	private Object cubicFeet;
    	private Object cubicMeter;
    	private Object occupied;
    	private Object type;
    	private Object description;
    	private Object utilizedVolCft;
    	private Object utilizedVolCbm;
		
    	public Object getCapacity() {
			return capacity;
		}
		
		public void setWarehouse(Object warehouse) {
			this.warehouse = warehouse;
			
		}
		public void setCapacity(Object capacity) {
			this.capacity = capacity;
		}
		public Object getCubicFeet() {
			return cubicFeet;
		}
		public void setCubicFeet(Object cubicFeet) {
			this.cubicFeet = cubicFeet;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getLocationId() {
			return locationId;
		}
		public void setLocationId(Object locationId) {
			this.locationId = locationId;
		}
		public Object getOccupied() {
			return occupied;
		}
		public void setOccupied(Object occupied) {
			this.occupied = occupied;
		}
		public Object getType() {
			return type;
		}
		public void setType(Object type) {
			this.type = type;
		}
		public Object getWarehouse() {
			return warehouse;
		}
		public Object getCubicMeter() {
			return cubicMeter;
		}
		public void setCubicMeter(Object cubicMeter) {
			this.cubicMeter = cubicMeter;
		}

		public Object getUtilizedVolCft() {
			return utilizedVolCft;
		}

		public void setUtilizedVolCft(Object utilizedVolCft) {
			this.utilizedVolCft = utilizedVolCft;
		}

		public Object getUtilizedVolCbm() {
			return utilizedVolCbm;
		}

		public void setUtilizedVolCbm(Object utilizedVolCbm) {
			this.utilizedVolCbm = utilizedVolCbm;
		}
    }

	public List findLocationList(String sessionCorpID,String whouse, String code ) {
		return getHibernateTemplate().find("from Location where corpID='"+sessionCorpID+"' AND warehouse='"+whouse+"' AND  type like '%"+code+"%' AND (occupied='' or capacity='3') ");
	}

	public List searchLocationForStorage(String locationId, String type,String sessionCorpID, String warehouse) {
		return getHibernateTemplate().find("from Location where locationId like '" + locationId.replaceAll("'", "''").replaceAll(":", "''") + "%' AND corpID='"+sessionCorpID+"' AND warehouse like '"+warehouse+"%' AND  type like '"+type+"%' AND (occupied='' or capacity='3') ");
	}
	public List<Location> findByLocationRearrList(String sessionCorpID,String locationId,String type, String warehouse){
		return getHibernateTemplate().find("from Location where corpID='"+sessionCorpID+"' and locationId like '" + locationId + "%' and (occupied='' or capacity='3') AND type like '" + type.replaceAll(":", "''") + "%' AND warehouse like '" + warehouse.replaceAll(":", "''") + "%' ");
	}
	public List<Location> findAllLocationId(String sessionCorpID){
		return getHibernateTemplate().find("from Location where corpID=?",sessionCorpID);
		
	}
	public List workTicketByLocation(String locationId){
		//getHibernateTemplate().setMaxResults(500);
		List list= this.getSession().createSQLQuery("select b.id,s.shipNumber,s.ticket,b.what from bookstorage b,storage s where s.locationId = '" +locationId+"' and b.idNum=s.idNum order by 1 desc limit 1").list();
		StorageDto storageDto;
		Iterator it=list.iterator();
		List storage=new ArrayList();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			storageDto=new StorageDto();
			storageDto.setId(obj[0]);
			storageDto.setShipnumber(obj[1]);
			storageDto.setTicket(obj[2]);
			storageDto.setWhat(obj[3]);
			storage.add(storageDto);
		}
		return storage;
	}
	public class StorageDto {
		private Object id;
    	private Object shipnumber;
    	private Object ticket;
    	private Object what;
		public Object getShipnumber() {
			return shipnumber;
		}
		public void setShipnumber(Object shipnumber) {
			this.shipnumber = shipnumber;
		}
		public Object getTicket() {
			return ticket;
		}
		public void setTicket(Object ticket) {
			this.ticket = ticket;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getWhat() {
			return what;
		}
		public void setWhat(Object what) {
			this.what = what;
		}
    	
	}
}  