package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ReportsFieldSection;

public interface ReportsFieldSectionManager extends GenericManager<ReportsFieldSection, Long> {

	public void saveReportFieldSection(ReportsFieldSection reportsFieldSection);
	
	public List reportfieldSectionList(Long id,String sessioncorpid, String type);
	
	public String updateReportsFieldSection(String fieldname,String fieldvalue, String sessioncorpId, String type);

}
