<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Origin&nbsp;Address</title>   
<meta name="heading" content="Tool Tip"/> 
</head> 
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="180px">
<c:forEach var="rows" items="${fullAddForOCity}">
<tr>
<td><b>Origin&nbsp;Address</b></td>
</tr>
<tr>
<td><b>City:</b> ${rows.originCity},<b>Zip:</b> ${rows.originZip}<br></td> 
</tr>
<tr>
<td><c:choose><c:when test='${rows.originState!=""}'><b>State:</b> ${rows.originState}</c:when><c:otherwise><b>Country:</b> ${rows.originCountry}</c:otherwise></c:choose></td>
</tr>
<tr>
<td><c:choose><c:when test='${rows.originState==""}'></c:when><c:otherwise><b>Country:</b> ${rows.originCountry}</c:otherwise></c:choose></td>
</tr>
</c:forEach>
</table>


