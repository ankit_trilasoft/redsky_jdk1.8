
package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;   

import com.trilasoft.app.model.BookStorage;   
import java.util.List;   
  
public interface BookStorageDao extends GenericDao<BookStorage, Long> {   
    public List<BookStorage> findByLocation(String locationId);
    
    public List findMaximum();
    
    public List getStorageList(String ticket, String corpID);

	public List getStorageLibraryList(String ticket, String corpID);
}