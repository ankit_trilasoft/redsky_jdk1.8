<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>SO Routing List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>



<display:table name="servicePartnerSO" class="table" requestURI="" id="servicePartnerList" export="false" defaultsort="1" pagesize="100" style="width:99%; margin-left:5px;margin-top:2px;">   
    <display:column title="Carrier#"><a onclick="goToUrlChild(${servicePartnerList.id});" style="cursor:pointer"><c:out value="${servicePartnerList.carrierNumber}" /></a></display:column>
    <display:column property="carrierName"  titleKey="servicePartner.carrierName" maxLength="7" />
    <display:column property="carrierDeparture"  titleKey="servicePartner.carrierDeparture"/>
    <display:column property="carrierArrival"  titleKey="servicePartner.carrierArrival"/>
</display:table>   