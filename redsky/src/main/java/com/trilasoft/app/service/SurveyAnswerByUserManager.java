package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.SurveyAnswerByUser;

public interface SurveyAnswerByUserManager extends GenericManager<SurveyAnswerByUser, Long>{
	
	public List getRecordsByQuestionId(Long qId,Long sid,Long cid);
	public List getRecordsBySOId(Long sid);
	public List getRecordsByCId(Long cid);
}
