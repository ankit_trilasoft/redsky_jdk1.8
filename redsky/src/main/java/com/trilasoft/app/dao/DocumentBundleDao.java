package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DocumentBundle;


public interface DocumentBundleDao extends GenericDao<DocumentBundle, Long>{
	
	public List findFormsByFormsId(String formsId);
	public List getListByCorpId(String corpId, String tabId);
	public String getReportDescAndIdByJrxmlName(String jrxmlName,String corpId);
	public void updateFormsIdInDocumentBundle(String reportsId,long id);
	public List searchDocumentBundle(String bundleName,String serviceType,String serviceMode,String job,String tableName,String fieldName,String sessionCorpID,String routingType);
	public List getRecordForEmail(String routingType,String serviceType,String serviceMode,String job,String fieldName,String sessionCorpID,String jobNumber,String sequenceNumber,String firstName,String lastName,String clickedField);
	public List findFileFromFileCabinet(String corpId, String jobNumber,String myFileIdTemp);
}
