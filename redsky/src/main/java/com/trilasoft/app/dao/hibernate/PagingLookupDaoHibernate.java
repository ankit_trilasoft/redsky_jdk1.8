package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.UniversalDaoHibernate;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Hibernate;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.dao.PagingLookupDao;
import com.trilasoft.app.webapp.helper.SearchCriterion;

public class PagingLookupDaoHibernate extends UniversalDaoHibernate implements
        PagingLookupDao {

    @SuppressWarnings("unchecked")
    public int getAllRecordsCount(Class clazz, List<SearchCriterion> searchCriteria) {
        DetachedCriteria criteria = DetachedCriteria.forClass(clazz);
        criteria.setProjection(Projections.rowCount());
        if (searchCriteria != null )applyFilterCriteria(searchCriteria, criteria);
        List results = getHibernateTemplate().findByCriteria(criteria);
        int count = ((Integer) results.get(0)).intValue();
        return count;
    }
    
    @SuppressWarnings("unchecked")
    public int getAllRecordsCount(Class clazz) {
        return getAllRecordsCount(clazz, null);
    }    

    @SuppressWarnings("unchecked")
    public List getAllRecordsPage(Class clazz, int firstResult, int maxResults,
            SortOrderEnum sortDirection, String sortCriterion, List<SearchCriterion> searchCriteria) {
        DetachedCriteria criteria = DetachedCriteria.forClass(clazz);
        applySortCriteria(sortDirection, sortCriterion, criteria);
        applyFilterCriteria(searchCriteria, criteria);
        List results  = null;
        try {
	        results = getHibernateTemplate().findByCriteria(criteria,
	                firstResult, maxResults);
        } catch (Exception ex){
        	ex.printStackTrace();
        }
        
        return results;
    }

	private void applyFilterCriteria(List<SearchCriterion> searchCriteria,
			DetachedCriteria criteria) {
		if (searchCriteria != null) {
			for (SearchCriterion searchCriterion : searchCriteria) {

				if (searchCriterion.getFieldType() == SearchCriterion.FIELD_TYPE_SQL) {
					criteria.add(Restrictions
							.sqlRestriction((String) searchCriterion
									.getFieldValue()));
				} else if (searchCriterion.getFieldValue() != null
						&& !searchCriterion.getFieldValue().toString().equals(
								"")) {
					if (searchCriterion.getOperator() == searchCriterion.OPERATOR_LIKE) {
						criteria.add(Restrictions.sqlRestriction("{alias}."
								+ searchCriterion.getFieldName() + " like ?",
								"%" + searchCriterion.getFieldValue().toString()
										+ "%", Hibernate.STRING));
					}else if (searchCriterion.getOperator() == searchCriterion.OPERATOR_START_LIKE) {
						criteria.add(Restrictions.sqlRestriction("{alias}."
								+ searchCriterion.getFieldName() + " like ?",
								searchCriterion.getFieldValue().toString()
										+ "%", Hibernate.STRING));
					}else if (searchCriterion.getOperator() == searchCriterion.OPERATOR_END_LIKE) {
						criteria.add(Restrictions.sqlRestriction("{alias}."
								+ searchCriterion.getFieldName() + " like ?",
								"%" + searchCriterion.getFieldValue().toString()
										, Hibernate.STRING));
					}else if (searchCriterion.getOperator() == searchCriterion.OPERATOR_EQUALS) {
						criteria.add(Restrictions.eq(searchCriterion
								.getFieldName(), searchCriterion
								.getFieldValue()));
					}
					else if (searchCriterion.getOperator() == searchCriterion.OPERATOR_NOT_EQUALS) {
						criteria.add(Restrictions.ne(searchCriterion
								.getFieldName(), searchCriterion
								.getFieldValue()));
					}
					else if (searchCriterion.getOperator() == searchCriterion.OPERATOR_IN) {						        
					        criteria.add(Restrictions.in(searchCriterion.getFieldName(),(Object[]) searchCriterion.getFieldValue()));						
					}
				}
			}
		}
	}

	private void applySortCriteria(SortOrderEnum sortDirection, String sortCriterion, DetachedCriteria criteria) {
		if (sortCriterion != null) {
            if (sortDirection.equals(SortOrderEnum.ASCENDING)) {
                criteria.addOrder(Order.asc(sortCriterion));
            }
            if (sortDirection.equals(SortOrderEnum.DESCENDING)) {
                criteria.addOrder(Order.desc(sortCriterion));
            }
        }
	}
}
