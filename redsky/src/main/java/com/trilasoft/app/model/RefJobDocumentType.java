package com.trilasoft.app.model;
import java.io.File;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table ( name="refjobdocumenttype") 
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class RefJobDocumentType extends BaseObject{
	private Long id;
	private String corpID;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String jobType;
	private Boolean docType;
	private File file;
	private String fileId;
	private String fileType;
	private String fileContentType;
	private String fileFileName;
	private String location;
	private String refJobDocs;

	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("createdBy", createdBy).append("createdOn",
				createdOn).append("updatedBy", updatedBy).append("updatedOn",
				updatedOn).append("jobType", jobType)
				.append("docType", docType).append("file", file).append(
						"fileId", fileId).append("fileType", fileType).append(
						"fileContentType", fileContentType).append(
						"fileFileName", fileFileName).append("location",
						location).append("refJobDocs", refJobDocs).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RefJobDocumentType))
			return false;
		RefJobDocumentType castOther = (RefJobDocumentType) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn).append(updatedBy,
						castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(jobType, castOther.jobType)
				.append(docType, castOther.docType)
				.append(file, castOther.file).append(fileId, castOther.fileId)
				.append(fileType, castOther.fileType).append(fileContentType,
						castOther.fileContentType).append(fileFileName,
						castOther.fileFileName).append(location,
						castOther.location).append(refJobDocs,
						castOther.refJobDocs).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(createdOn).append(updatedBy).append(
						updatedOn).append(jobType).append(docType).append(file)
				.append(fileId).append(fileType).append(fileContentType)
				.append(fileFileName).append(location).append(refJobDocs)
				.toHashCode();
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}
	
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getDocType() {
		return docType;
	}

	public void setDocType(Boolean docType) {
		this.docType = docType;
	}
	@Column
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	@Column(length=45)
	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}
	@Column(length=375)
	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}
	@Column(length=15)
	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	@Column(length=35)
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=3)
	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	@Column(length=254)
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length=470)
	public String getRefJobDocs() {
		return refJobDocs;
	}

	public void setRefJobDocs(String refJobDocs) {
		this.refJobDocs = refJobDocs;
	}
	
}
