<%@ include file="/common/taglibs.jsp"%> 
<head> 
    <title>Document List</title> 
    <meta name="heading" content="Document List"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top" style="!padding-bottom:5px;"> 	
	<td align="left"><b>Document List</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
  <display:table name="myFileList"  class="table"  id="myFileList"  style="width:100%;" > 
		  <display:column property="fileType"  maxLength="20" title="Document&nbsp;Type" style="width:60px;"/>
		<display:column  maxLength="40" title="Description" style="width:60px;"  sortable="true" sortProperty="description">
						<a onclick="javascript:openWindow('ImageServletAction.html?id=${myFileList.id}&decorator=popup&popup=true',900,600);">
						<c:out value="${myFileList.description}" escapeXml="false"/></a>
	</display:column>
	</display:table>  