// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `consumablePercentage` DECIMAL(19,2) NULL  AFTER `billingMoment` ;
ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `estimateConsumablePercentage` BIT(1) NULL  AFTER `revisionScopeOfConsumables` ;
ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `revisionConsumablePercentage` BIT(1) NULL  AFTER `estimateConsumablePercentage` ;
CREATE  TABLE `redsky`.`monthlysalesgoal` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `corpId` VARCHAR(10) NULL ,
  `updatedOn` DATETIME NULL ,
  `updatedBy` VARCHAR(82) NULL ,
  `createdOn` DATETIME NULL ,
  `createdBy` VARCHAR(82) NULL ,
  `partnerCode` VARCHAR(8) NULL ,
  `year` VARCHAR(4) NULL ,
  `januaryImport` DECIMAL(19,2) NULL ,
  `januaryExport` DECIMAL(19,2) NULL ,
  `januaryTotal` DECIMAL(19,2) NULL ,
  `februaryImport` DECIMAL(19,2) NULL ,
  `februaryExport` DECIMAL(19,2) NULL ,
  `februaryTotal` DECIMAL(19,2) NULL ,
  `marchImport` DECIMAL(19,2) NULL ,
  `marchExport` DECIMAL(19,2) NULL ,
  `marchTotal` DECIMAL(19,2) NULL ,
  `aprilImport` DECIMAL(19,2) NULL ,
  `aprilExport` DECIMAL(19,2) NULL ,
  `aprilTotal` DECIMAL(19,2) NULL ,
  `mayImport` DECIMAL(19,2) NULL ,
  `mayExport` DECIMAL(19,2) NULL ,
  `mayTotal` DECIMAL(19,2) NULL ,
  `juneImport` DECIMAL(19,2) NULL ,
  `juneExport` DECIMAL(19,2) NULL ,
  `juneTotal` DECIMAL(19,2) NULL ,
  `julyImport` DECIMAL(19,2) NULL ,
  `julyExport` DECIMAL(19,2) NULL ,
  `julyTotal` DECIMAL(19,2) NULL ,
  `augustImport` DECIMAL(19,2) NULL ,
  `augustExport` DECIMAL(19,2) NULL ,
  `augustTotal` DECIMAL(19,2) NULL ,
  `septemberImport` DECIMAL(19,2) NULL ,
  `septemberExport` DECIMAL(19,2) NULL ,
  `septemberTotal` DECIMAL(19,2) NULL ,
  `octoberImport` DECIMAL(19,2) NULL ,
  `octoberExport` DECIMAL(19,2) NULL ,
  `octoberTotal` DECIMAL(19,2) NULL ,
  `novemberImport` DECIMAL(19,2) NULL ,
  `novemberExport` DECIMAL(19,2) NULL ,
  `novemberTotal` DECIMAL(19,2) NULL ,
  `decemberImport` DECIMAL(19,2) NULL ,
  `decemberExport` DECIMAL(19,2) NULL ,
  `decemberTotal` DECIMAL(19,2) NULL ,
  `allMonthTotal` DECIMAL(19,2) NULL ,
  PRIMARY KEY (`id`) );


INSERT INTO parametercontrol
(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('TSFT','YEAR',4,true,now(),'TSFT_SETUP',now(),'TSFT_SETUP',1,1,'');

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,status)values
('TSFT', '2020', '2020', 4, 'YEAR','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active'),
('TSFT', '2019', '2019', 4, 'YEAR','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active'),
('TSFT', '2018', '2018', 4, 'YEAR','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active'),
('TSFT', '2017', '2017', 4, 'YEAR','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active'),
('TSFT', '2016', '2016', 4, 'YEAR','en',now(),'TSFT_SETUP',now(),'TSFT_SETUP','Active');

ALTER TABLE `redsky`.`monthlysalesgoal` ADD COLUMN `allMonthImportTotal` DECIMAL(19,2) NULL  AFTER `allMonthTotal` ,
ADD COLUMN `allMonthExportTotal` DECIMAL(19,2) NULL  AFTER `allMonthImportTotal` ;