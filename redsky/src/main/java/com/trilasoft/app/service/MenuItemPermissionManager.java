package com.trilasoft.app.service;

import java.util.*;

import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.MenuItemPermission;


public interface MenuItemPermissionManager extends GenericManager<MenuItemPermission, Long> { 
	
	public List<MenuItemPermission> findByCorpID(String corpId);
	public List checkById(Long id);
	public List<MenuItemPermission> findByUser(User userName, Set<Role> roles);
	public List<User> findByUserPermission(String userName);
	public List<Long> findMenuIdsByUser(User user, Set<Role> roles);
}
