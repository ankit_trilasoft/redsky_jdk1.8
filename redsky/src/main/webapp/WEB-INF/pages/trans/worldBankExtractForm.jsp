<%@ include file="/common/taglibs.jsp"%> 
<head>
	<meta name="heading" content="<fmt:message key='worldBankDataExtact.heading'/>"/> 
	<title><fmt:message key="worldBankDataExtact.heading"/></title>
	
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
<!-- Modification closed here -->

</script>
	<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>
<script language="javascript" type="text/javascript">
		
		function valbutton(thisform) {
		myOption = -1;
		for (i=thisform.track.length-1; i > -1; i--) {
		if (thisform.track[i].checked) {
		myOption = i; i = -1;
		}
		}
		if (myOption == -1) {
		alert("You must select billing type ");
		return false;
		}
		
	return checkDateField();
	
}

function checkDateField(){

	 var xx=document.forms['worldBankDataExtactForm'].elements['track'];	 
	 if(xx[2].checked==true)
	 {	 	
	 	return  valFrm();
	 	return true;
	 }
	if(document.forms['worldBankDataExtactForm'].elements['storagedate'].value=='')
	{
		alert('Please enter storage quarterly date');
		return false;
	}
	return  valFrm();
}
		
</script>
<script language="javascript" type="text/javascript">

		function checkRadio (frmName, rbGroupName) {
		var radios = document[frmName].elements[rbGroupName];
		for (var i=0; i <radios.length; i++) {
		if (radios[i].checked) {			
		   return true;
		  }
		 }
		 return false;
		}

			function valFrm() {
			 var xx=document.forms['worldBankDataExtactForm'].elements['track'];

			 var yy=document.forms['worldBankDataExtactForm'].elements['balance'];
			
		 		if(xx[1].checked==true && yy[1].checked==true){
		 			alert("Select Debit only");
		 			 return false;
		 		}
		 			
		 		if (!checkRadio("worldBankDataExtactForm","balance"))
		 		{
		 		 alert("Select Debit/Credit");
		 		  return false;
		 		 }
		 		
		 		else{
		 			
					 return true;
		  		}
		}

function deliversyExtract(){

document.forms['worldBankDataExtactForm'].action = 'worldBankdeliveryExtract.html';
document.forms['worldBankDataExtactForm'].submit();
}
		
</script>
<style type="text/css">
</style>
</head>
<body style="background-color:#444444;">
<s:form id="worldBankDataExtactForm" name="worldBankDataExtactForm" action="worldBankDataExtracts" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:85%">
<div id="otabs">
			  <ul>
			    <li><a class="current"><span>World Bank Invoice</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="1" border="0"  >
  <tbody>
  <tr>    
  <td width="10px"></td>
  	<td align="left" class="listwhitetext" width="320px">
  	<table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="4px"></td>
		  	</tr>
		  	<tr>
		 		 <td align="right" valign="top" rowspan="3" style="padding-bottom:5px; padding-top: 5px ">Billing Type<font color="red" size="2">*</font></td>
		  
		  		
				<td align="left" width="200px" colspan="3"><s:radio name="track"  value="nonQuartelyStorage" list="'Non Quarterly Storage'" onchange="changeStatus();"/>	</td>
			
			
			</tr>
			<tr>
				
				<td align="left" width="" colspan="3"><s:radio name="track"  value="quartelyStorage" list="'Quarterly Storage'" onchange="changeStatus();"/></td>
			
			</tr>
			<tr>
				
				<td align="left" width="" colspan="3"><s:radio name="track"  value="transitJobs" list="'Transit Jobs'" onchange="changeStatus();"/></td>
			  		
		  	</tr>
		  	<tr><td height="10px"></td></tr>
			<tr>	
				
				<td align="right" width="120px" style="padding-bottom:5px">Storage Quarterly Date</td>
							
				<td align="left" colspan="3" style="padding-bottom:5px; padding-left:7px;"><s:textfield cssClass="input-text" id="storagedate" name="storagedate" value="%{storagedate}" size="9" maxlength="11" readonly="true" /> 
				<img id="storagedate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		  	</tr>		  	
		  	
        	<tr>
        	<td></td>				
				<td align="left" width="27px"><s:radio name="balance" value="debit" list="'Debit'"  onchange="changeStatus();"/></td>			
				
				<td align="left" width="60px"><s:radio name="balance"  value="credit"  list="'Credit'" onchange="changeStatus();"/></td>
			  		
		  	</tr>
        	<tr><td height="10px"></td></tr>
        	<tr>
		  				  	  		
	  			<td ></td>			
				<td colspan="4" style="padding-left:6px;">
				<s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " align="top" method="worldBankInvoiceExtract" value="Extract" onclick="return valbutton(worldBankDataExtactForm);" />
				
				<s:reset cssClass="cssbutton" key="Cancel" cssStyle="width:70px; height:25px "/>   
				</td>
        	</tr>        	
        	</table>
  	</td>
  	
  	<td height="10px" class="vertlinedata_vert" rowspan="8"/>
  	
  	
  	<td valign="top">
  	<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0"  style="">
		<tbody>  
		
		<tr><td width="5px"> </td>
<td  class="bgblue" colspan="5">World Bank Delivery Extract</td></tr>
<tr>
<td> </td>
<td align="left" height="120" style="padding-left:100px;">
  <input type="button" Class="cssbutton" Style="width:80px; height:25px " align="top" value="Extract" onclick="deliversyExtract();" />
  	</td>
  	</tr>
  	</tbody>
  	</table>
  	</td>       	
		</tr> 
		<tr><td height="10px"></td></tr>	
		</tbody>
	</table>
</div>
	<div class="bottom-header"><span></span></div>
	</div>
	</div>
	</div>
</s:form>

<script type="text/javascript">

	RANGE_CAL_1 = new Calendar({
        inputField: "storagedate",
        dateFormat: "%d-%b-%y",
        trigger: "storagedate_trigger",
        bottomBar: true,
        animation:true,
        onSelect: function() {                             
            this.hide();
    }
     
});
</script>		