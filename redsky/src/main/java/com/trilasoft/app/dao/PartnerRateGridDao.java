package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao; 
import com.trilasoft.app.model.PartnerRateGrid;

public interface PartnerRateGridDao extends GenericDao<PartnerRateGrid, Long>{
	public List getPartnerRateGridList(String sessionCorpID, String partnerCode);
	public List getRateGridTemplates(String sessionCorpID);
	public Map<String, String> findStateList(String terminalCountryCode, String sessionCorpID);
	public List getStandardInclusionsNote(String corpID);
	public List getStandardDestinationNote(String corpID);
	public int updateRateGridStatus(Long id, String status, String sessionCorpID, String user);
	public int findSubmitCount(String partnerCode, String sessionCorpID, String tariffApplicability);
	public int findUniqueCount(String sessionCorpID, String partnerCode, String tariffApplicability, String metroCity, BigDecimal serviceRangeMiles, String effectiveDate);
	public List getRateGridRules(String corpID); 
	public List findPartnerRateMatrix(String sessionCorpID, String partnerCode, String partnerName, String terminalCountry, String tariffApplicability, String status, boolean publishTariff); 
	public int updatePublisStatus(Long id, String publishTariff); 
	public List getStandardCountryHauling(String corpID); 
	public List getStandardCountryCharges(String corpID); 
	public List searchBaseCurrency(String sessionCorpID); 
	public List getPartnerRateGridListView(String sessionCorpID, String partnerCode); 
}
