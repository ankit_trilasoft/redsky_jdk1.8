package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.BrokerDetailsDao;
import com.trilasoft.app.model.BrokerDetails;
import com.trilasoft.app.service.BrokerDetailsManager;

public class BrokerDetailsManagerImpl extends GenericManagerImpl<BrokerDetails, Long> implements BrokerDetailsManager
{ 
	BrokerDetailsDao brokerDetailsDao; 
    public BrokerDetailsManagerImpl(BrokerDetailsDao brokerDetailsDao)
    { 
        super(brokerDetailsDao); 
        this.brokerDetailsDao = brokerDetailsDao; 
    }
    
    public List getByShipNumber(String shipNumber) {
		return brokerDetailsDao.getByShipNumber(shipNumber);
	}

}
