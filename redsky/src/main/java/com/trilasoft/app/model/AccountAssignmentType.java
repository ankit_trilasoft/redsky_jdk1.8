package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;



@Entity
@Table(name="accountassignmenttype")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class AccountAssignmentType extends BaseObject{
	
	private Long id;
	private String assignment;
	private String description;
	private String updatedBy;
	private String createdBy;
	private Date createdOn;
	private Date updatedOn;
	private Long partnerPrivateId;	
	private String partnerCode;	
	private String corpId;
	private Long parentId=0L;
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AccountAssignmentType))
			return false;
		AccountAssignmentType castOther = (AccountAssignmentType) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(assignment, castOther.assignment)
				.append(description, castOther.description)
				.append(updatedBy, castOther.updatedBy)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(partnerPrivateId, castOther.partnerPrivateId)
				.append(partnerCode, castOther.partnerCode)
				.append(corpId, castOther.corpId)
				.append(parentId, castOther.parentId)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(assignment)
				.append(description).append(updatedBy)
				.append(createdBy).append(createdOn)
				.append(updatedOn).append(partnerPrivateId)
				.append(partnerCode).append(corpId).append(parentId)
				.toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("assignment", assignment)
				.append("description", description)
				.append("updatedBy", updatedBy)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("partnerPrivateId", partnerPrivateId)
				.append("partnerCode", partnerCode).append("corpId", corpId)
				.append("parentId",parentId)
				.toString();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAssignment() {
		return assignment;
	}

	public void setAssignment(String assignment) {
		this.assignment = assignment;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getPartnerPrivateId() {
		return partnerPrivateId;
	}

	public void setPartnerPrivateId(Long partnerPrivateId) {
		this.partnerPrivateId = partnerPrivateId;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	

}
