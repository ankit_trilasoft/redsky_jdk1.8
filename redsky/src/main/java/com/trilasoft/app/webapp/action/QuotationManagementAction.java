package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.bind.JAXBException;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.Address;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.util.StringUtil;
import org.appfuse.webapp.action.BaseAction;
import org.displaytag.properties.SortOrderEnum;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.codehaus.jackson.map.ObjectMapper;




import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Billing;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.InventoryPath;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.UserDTO;
import com.trilasoft.app.service.AccessInfoManager;
import com.trilasoft.app.service.AccountAssignmentTypeManager;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.AdAddressesDetailsManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContractManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.InventoryDataManager;
import com.trilasoft.app.service.InventoryPathManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.QuotationManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;
import com.trilasoft.qtg.wsclient.QtgClientServiceCall;
import com.trilasoft.qtg.wsclient.driver.XMLDto;
import com.trilasoft.qtg.wsclient.response.model.CreateOpportunityReturn;

public class QuotationManagementAction extends BaseAction implements Preparable {
	private String countVipNotes;

	private String countBillingNotes;

	private String countContactNotes;

	private String countEntitlementNotes;

	private String countCportalNotes;

	private String countSpouseNotes;

	private String countDestinationNotes;
	private String bookingAppUserCode; 
	private String countSurveyNotes;
    private PartnerPublicManager partnerPublicManager;
    private String emailAdd = new String();
    private List coordSignature;
	private List coordEmail;
	private String mailStatus;
	private String mailFailure;
	private PartnerPublic partnerPublic;
	private String confirmPasswordNew;
	private String countNotes;
	private TrackingStatusManager trackingStatusManager;
	private String countOriginNotes;

	private String hitFlag;
	
	private String weightUnit;
	
	private String volumeUnit;
	
	private List serviceOrders;
	private PartnerPrivate partnerPrivate;
	private PartnerPrivateManager partnerPrivateManager;
	private Long maxId;
	private String emailTo;

	private CustomerFile customerFile;
	
	private CustomerFile customerFileCombo;
	
	private CustomerFile customerFileTemp;

	private ServiceOrder serviceOrder;
	private ServiceOrder qServiceOrder;
	private CustomerFile qCustomerFile;
	private AccountLine accountLine;
	private ServiceOrder mergeSO;
    
	private Billing  billings;
	
	private Set customerServiceOrders;
	
	private Set customerBillings;

	private User user;

	private CustomerFileManager customerFileManager;

	private ServiceOrderManager serviceOrderManager;

	private RefMasterManager refMasterManager;

	private ContractManager contractManager;

	private NotesManager notesManager;
	
	private AccountLineManager accountLineManager;
	
	private MyFileManager myFileManager;
	
	private AccessInfoManager accessInfoManager;
	
	private InventoryDataManager inventoryDataManager;
	
	private InventoryPathManager inventoryPathManager;
	
	private String sessionCorpID;

	private String defaultcontract ;
	
	private List maxSequenceNumber;

	private List checkSequenceNumberList;
	private Boolean checkGerman;
	private Boolean checkDutch;
	private Boolean checkEnglish;
	private Boolean checkGermanForCustomerSave;
	private Boolean checkEnglishForCustomerSave;
	private Long autoSequenceNumber;

	private List ls;
	private Map<String, String> orderStatus1;
	private String seqNum;
	private String seqNumber;

	private Map<String, String> qc = new LinkedHashMap<String, String>();
    private Map<String, String> statusReason;

	private Long id;
	private Long qfId;
	private Set customerFiles;

	private List contracts = new ArrayList();

	private Calendar surveyDate;

	private Map<String, String> lead;

	private static Map<String, String> state;

	private static Map<String, String> ocountry;

	private static Map<String, String> dcountry;
	
	private static Map<String, String> countryCod;
	
	private Map<String, String> survey;	

	private Map<String, String> paytype;

	private Map<String, String> job;

	private Map<String, String> organiza;

	private Map<String, String> peak;

	private Map<String, String> yesno;

	private Map<String, String> custStatus;

	private Map<String, String> routing;

	private Map<String, String> commodit;

	private Map<String, String> mode;

	private Map<String, String> service;

	private Map<String, String> pkmode;

	private Map<String, String> loadsite;

	private Map<String, String> specific;

	private Map<String, String> jobtype;

	private Map<String, String> military;

	private Map<String, String> JOB_STATUS;

	private Map<String, String> notestatus;

	private Map<String, String> notetype;

	private Map<String, String> notesubtype;

	private Map<String, String> forwardNoteStatus;

	private Map<String, String> QUOTESTATUS;

	private Map<String, String> EQUIP;

	private Map<String, String> preffix;
	private  Map<String, String> sale = new LinkedHashMap<String, String>();

	private Map<String, String> coord= new LinkedHashMap<String, String>();

	private Map<String, String> crew= new LinkedHashMap<String, String>();

	private Map<String, String> pricing= new LinkedHashMap<String, String>();;

	private Map<String, String> billing= new LinkedHashMap<String, String>();;

	private Map<String, String> payable= new LinkedHashMap<String, String>();
	
	private Map<String, String> auditor= new LinkedHashMap<String, String>();
	private  Map<String, String> all_user = new LinkedHashMap<String, String>();

	
	private Map<String, String> exec= new LinkedHashMap<String, String>();
	
	private List companyDivis=new ArrayList();

	private List weightunits;

	private List volumeunits;

	private Map<String, String> assignmentTypes;
	
	private List mergeSOList;

	private List<SystemDefault> sysDefaultDetail;

	private boolean noMoreIfStat;

	private Date sysdate = new Date();

	private int noOfClaims;

	private int noOfStorage;

	private int soStatusNumberMax;

	private int soStatusNumberMin;

	private boolean atLeastOneClaim;

	private boolean atLeastOneStorage;

	private boolean allJobHold;
	private Boolean activeStatus;
	private User coordinatorDetail;

	private User estimatorDetail;

	private Set<Role> roles;

	String coordinatr;

	private Set<CustomerFile> surveyDetails;

	private List lst;
	private List linkedSOMerge;
	private String companyDivision;
	private List<DataCatalog> tooltip;

	private String tableName;

	private String tableFieldName;
	private String supportId; 

	private String fieldName;
	private String bookCode;
	private String quotesId;
	private String serviceOrderID;
	private InventoryDataAction inventoryDataAction;
	
	private static Map<String, String> ostates;
	
    private static Map<String, String> dstates;
    
    private String gotoPageString;
    
    private String validateFormNav;
    private String detailPage;
    
    private CustomerFile addCopyCustomerFile;
    private Long cid;
    private Integer QuotesNumber;

	private Map<String, String> quoteAccept;
	
	private BillingManager billingManager;
	private String stateshitFlag;
	
	private String portalId;
	
	private String userPortalId;
	
	private String oldPortalId; 
	
	private String portalIdCount;
	
	private List newPortalId; 
	private String cPortalId; 
	private List poratalIdCountList;
	 private Boolean portalIdActive; 
	 private Boolean emailTypeFlag;
		private Boolean emailTypeVOERFlag;
		private Boolean emailTypeINTMFlag;
		private Boolean emailTypeEnglishFlag;
		private String userID;
		private String nameOfCompany;
		private static Map<String, String> qfReason;
		private Boolean emailTypeBOURFlag;
		private String csoSortOrder;
		private String orderForCso;	
	private List adAddressesDetailsList = new ArrayList();
	private AdAddressesDetailsManager adAddressesDetailsManager;
	private String companyCode;
    private SystemDefault systemDefault;
    private SystemDefaultManager systemDefaultManager;
    private Map<String,LinkedHashMap<String, String>> parameterMap = new HashMap<String, LinkedHashMap<String,String>>();
    private Map<String,String> localParameter;
    List <UserDTO> allUser=new ArrayList<UserDTO>();
	Date currentdate = new Date();
	private Boolean cportalAccessCompanyDivision;
	 private String emailSetupValue;
	 private int count;
	private String checkContractChargesMandatory= "0";
	private Map<String, String> quoteAcceptReason;
	private Map<String, String> countryDsc;
	private boolean checkAccessQuotation=false;
	private String compDivFlag;
    static final Logger logger = Logger.getLogger(QuotationManagementAction.class);

    
    public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {

    return gotoPageString;

    }

    public void setGotoPageString(String gotoPageString) {

    this.gotoPageString = gotoPageString;

    }
        public String saveOnTabChange(){ 
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        	validateFormNav = "OK";     
        	try {
				String s = saveQuotation();
			} catch (Exception e) {
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				e.printStackTrace();
			}    // else simply navigate to the requested page)
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return gotoPageString;
    }

	

	@SkipValidation
	public String findtoolTip() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			tableName = tableFieldName.substring(0, tableFieldName.indexOf("."));
			fieldName = tableFieldName.substring((tableFieldName.indexOf(".") + 1), tableFieldName.length());
			tooltip = customerFileManager.findToolTip(fieldName, tableName,sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		}
		return SUCCESS;
	}

	

	public QuotationManagementAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		companyCode=user.getCompanyDivision();
		bookingAppUserCode=user.getBookingAgent();
	}

	static String replaceWord(String original, String find, String replacement) {
		int i = original.indexOf(find);
		if (i < 0) {
			return original; // return original if 'find' is not in it.
		}

		String partBefore = original.substring(0, i);
		String partAfter = original.substring(i + find.length());

		return partBefore + replacement + partAfter;

	}

	public String delete() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		customerFileManager.remove(customerFile.getId());
		saveMessage(getText("customerFile.deleted"));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public Map<String,String> findByParameterLocal(String parameter){ 
		localParameter = new HashMap<String, String>();
		if(parameterMap.containsKey(parameter)){
			localParameter = parameterMap.get(parameter);
		}
		return localParameter;
	}

	public String getComboList(String corpId, String jobType) {
		try {
			User usercheck = userManager.getUserByUsername(getRequest().getRemoteUser());
			if(usercheck.getContract()!=null){
				defaultcontract=usercheck.getContract();
			}else {
				defaultcontract="";
			} 
			if(customerFileCombo!=null){
				/*count = emailSetupManager.getEmailSetupCount(sessionCorpID, customerFileCombo.getSequenceNumber());*/
				
			}else{
			try {
				id = new Long(getRequest().getParameter("id").toString());
				 if(id!=null){
					 customerFileCombo = customerFileManager.get(id);
					 count = emailSetupManager.getEmailSetupCount(sessionCorpID, customerFileCombo.getSequenceNumber());
					}
			} catch (Exception e) {
				e.printStackTrace();
				try{
				serviceOrder = serviceOrderManager.get(id);
				customerFileCombo = serviceOrder.getCustomerFile(); 
				}catch (Exception ea) {
					ea.printStackTrace();	
				}
			}
			}
			//companies = companyManager.findByCompanyDivis(sessionCorpID);
			//lead = refMasterManager.findByParameter(corpId, "LEAD");
			//survey = refMasterManager.findByParameter(corpId, "SURVEY");
			//state = refMasterManager.findByParameter(corpId, "STATE");
			//ocountry = refMasterManager.findCountry(corpId, "COUNTRY");
			//dcountry = refMasterManager.findCountry(corpId, "COUNTRY");
			//countryCod = refMasterManager.findByParameter(corpId, "COUNTRY");
			//paytype = refMasterManager.findByParameter(corpId, "PAYTYPE");
			//job = refMasterManager.findByParameter(corpId, "JOB");
			//organiza = refMasterManager.findByParameter(corpId, "ORGANIZA");
			//peak = refMasterManager.findByParameter(corpId, "PEAK");
			//yesno = refMasterManager.findByParameter(corpId, "YESNO");
			//custStatus = refMasterManager.findByParameter(corpId, "CUST_STATUS");
			//routing = refMasterManager.findByParameter(corpId, "ROUTING");
			//commodit = refMasterManager.findByParameter(corpId, "COMMODIT");
			//mode = refMasterManager.findByParameter(corpId, "MODE");
			//service = refMasterManager.findByParameter(corpId, "SERVICE");
			//pkmode = refMasterManager.findByParameter(corpId, "PKMODE");
			//loadsite = refMasterManager.findByParameter(corpId, "LOADSITE");
			//specific = refMasterManager.findByParameter(corpId, "SPECIFIC");
			//jobtype = refMasterManager.findByParameter(corpId, "JOBTYPE");
			//military = refMasterManager.findByParameter(corpId, "MILITARY");
			//JOB_STATUS = refMasterManager.findByParameter(corpId, "JOB_STATUS");
			//EQUIP = refMasterManager.findByParameter(corpId, "EQUIP");
			//preffix = refMasterManager.findByParameter(corpId, "PREFFIX"); 
			//notestatus = refMasterManager.findByParameter(corpId, "NOTESTATUS");
			//notetype = refMasterManager.findByParameter(corpId, "NOTETYPE");
			//notesubtype = refMasterManager.findByParameter(corpId, "NOTESUBTYPE");		
			//forwardNoteStatus = refMasterManager.findByParameter(corpId, "FWDSTATUS");
			//QUOTESTATUS = refMasterManager.findByParameter(corpId, "QUOTESTATUS");
			//quoteAccept = refMasterManager.findByParameter(corpId, "QuoAcce");
			//qfReason=refMasterManager.findByParameter(corpId, "QFSTATUSREASON");
			
			//sale = refMasterManager.findUser(corpId, "ROLE_SALE");
			//qc = refMasterManager.findUser(corpId, "ROLE_QC");
			//crew = refMasterManager.findUser(corpId, "ROLE_CREW");
			//coord = refMasterManager.findUser(corpId, "ROLE_COORD");
			//pricing = refMasterManager.findUser(corpId, "ROLE_PRICING");
			//billing = refMasterManager.findUser(corpId, "ROLE_BILLING");
			//payable = refMasterManager.findUser(corpId, "ROLE_PAYABLE");
			//auditor = refMasterManager.findUser(corpId,"ROLE_AUDITOR");
			all_user = refMasterManager.findUser(corpId, "ALL_USER");
			//exec = refMasterManager.findUser(corpId, "ROLE_EXECUTIVE");
			//estimatorList = refMasterManager.findUser(sessionCorpID,"ROLE_SALE");
			//coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
			//pricingList = refMasterManager.findUser(sessionCorpID,"ROLE_PRICING");
			//billingList = refMasterManager.findUser( sessionCorpID,"ROLE_BILLING");
			//payableList = refMasterManager.findUser(sessionCorpID, "ROLE_PAYABLE");
			//auditorList = refMasterManager.findUser(sessionCorpID, "ROLE_AUDITOR");
			//execList = refMasterManager.findUser(sessionCorpID, "ROLE_EXECUTIVE");
			//estimatorList = refMasterManager.findUser(sessionCorpID,"ROLE_SALE");
			//coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
			//pricingList = refMasterManager.findUser(sessionCorpID,"ROLE_PRICING");
			//billingList = refMasterManager.findUser( sessionCorpID,"ROLE_BILLING");
			//payableList = refMasterManager.findUser(sessionCorpID, "ROLE_PAYABLE");
			//auditorList = refMasterManager.findUser(sessionCorpID, "ROLE_AUDITOR");
			//execList = refMasterManager.findUser(sessionCorpID, "ROLE_EXECUTIVE");
			//estimatorList = refMasterManager.findUser(sessionCorpID,"ROLE_SALE");
			//coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
			//pricingList = refMasterManager.findUser(sessionCorpID,"ROLE_PRICING");
			//billingList = refMasterManager.findUser( sessionCorpID,"ROLE_BILLING");
			//payableList = refMasterManager.findUser(sessionCorpID, "ROLE_PAYABLE");
			//auditorList = refMasterManager.findUser(sessionCorpID, "ROLE_AUDITOR");
			//execList = refMasterManager.findUser(sessionCorpID, "ROLE_EXECUTIVE");
			//estimatorList = refMasterManager.findUser(sessionCorpID,"ROLE_SALE");
			//coordinatorList = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
			//pricingList = refMasterManager.findUser(sessionCorpID,"ROLE_PRICING");
			//billingList = refMasterManager.findUser( sessionCorpID,"ROLE_BILLING");
			//payableList = refMasterManager.findUser(sessionCorpID, "ROLE_PAYABLE");
			//auditorList = refMasterManager.findUser(sessionCorpID, "ROLE_AUDITOR");
			//execList = refMasterManager.findUser(sessionCorpID, "ROLE_EXECUTIVE");
			//exec = refMasterManager.findUser(sessionCorpID, "ROLE_EXECUTIVE");
			
			String roles ="'ROLE_COORD','ROLE_SALE','ROLE_QC','ROLE_PRICING','ROLE_PAYABLE','ROLE_BILLING','ROLE_BILLING_ARM','ROLE_BILLING_CAJ','ROLE_BILLING_SSC','ROLE_CREW','ROLE_AUDITOR','ROLE_EXECUTIVE'";
			allUser = refMasterManager.getAllUser(corpId,roles);
			String jobCombo=getRequest().getParameter("customerFile.job");
			if(jobCombo!=null && (!(jobCombo.toString().equals("")))){
				
			}else{
				if(customerFileCombo!=null){
				jobCombo=customerFileCombo.getJob();
				}
			}
			if(jobCombo !=null && (!(jobCombo.toString().trim().equals("")))){
				
			}else{
				jobCombo="";	
			}
			
			for (UserDTO userDTO : allUser) {
				if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_SALE")){ 
					sale.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					if(jobCombo!=null    && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo) || jobCombo.toString().trim().equals("") ) ){
					estimatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}/*else{
					estimatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());	
					}*/
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_QC")){
					qc.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_CREW")){
					crew.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_COORD")){
					coord.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					if(jobCombo!=null    && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo) || jobCombo.toString().trim().equals("") ) ){
					coordinatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}/*else{
					coordinatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());	
					}*/
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PRICING")){
					pricing.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					if(jobCombo!=null    && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo) || jobCombo.toString().trim().equals("")  ) ){	 
					pricingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}/*else{
					pricingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());	
					}*/
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_ARM") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_CAJ") ||  userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_SSC")  ){
					billing.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					if(jobCombo!=null    && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo) || jobCombo.toString().trim().equals("") ) ){
					billingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}/*else{
					billingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());	
					}*/
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PAYABLE")){
					payable.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					if(jobCombo!=null    && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo) || jobCombo.toString().trim().equals("") ) ){
					payableList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}/*else{
					payableList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());	
					}*/
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_AUDITOR")){
					auditor.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					if(jobCombo!=null    && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo) || jobCombo.toString().trim().equals("") ) ){
					auditorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}/*else{
					auditorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());	
					}*/
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_EXECUTIVE")){
					exec.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					if(jobCombo!=null   && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(jobCombo) || jobCombo.toString().trim().equals("") ) ){
					execList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}/*else{
					execList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());	
					}*/
				}
			}
			weightUnit="Lbs";
			volumeUnit="Cft";
			sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					if(systemDefault.getWeightUnit()!=null && (!(systemDefault.getWeightUnit().toString().equals("")))){
						weightUnit=	systemDefault.getWeightUnit();
					}
					if(systemDefault.getVolumeUnit()!=null &&  (!(systemDefault.getVolumeUnit().toString().equals("")))){
						volumeUnit=	systemDefault.getVolumeUnit();
					}
				}
					
				}
			//weightUnit=customerFileManager.getSystemWeightUnit(sessionCorpID);
			//volumeUnit=customerFileManager.getSystemVolumeUnit(sessionCorpID);
			
			
			ocountry = new LinkedHashMap<String, String>();
			dcountry = new LinkedHashMap<String, String>();
			countryCod= new LinkedHashMap<String, String>();
			countryDsc = new LinkedHashMap<String, String>();
			military = new TreeMap<String, String>();
			ostates=new LinkedHashMap<String, String>();
			dstates=new LinkedHashMap<String, String>();
			orderStatus1 =new LinkedHashMap<String, String>();
			String originCountry="";
			String destCountry ="";
			if(customerFileCombo!=null){
			if(customerFileCombo.getOriginCountry()!=null){
				String temp=getRequest().getParameter("customerFile.originCountry");
				if(temp!=null && (!(temp.toString().equals("")))){
					
				}else{
			     temp = customerFileCombo.getOriginCountry();
				}
				originCountry = returnCountryCode(temp);
			}
			if(customerFileCombo.getDestinationCountry()!=null){
				String temp=getRequest().getParameter("customerFile.destinationCountry");
			    if(temp!=null && (!(temp.toString().equals("")))){
				
			}else{
				temp = customerFileCombo.getDestinationCountry();
			}
				destCountry = returnCountryCode(temp);
			}
			}
			LinkedHashMap <String,String>tempParemeterMap = new LinkedHashMap<String, String>();
			String parameters="'LEAD','SURVEY','STATE','COUNTRY','PAYTYPE','JOB','ORGANIZA','PEAK','YESNO','CUST_STATUS','ROUTING','COMMODIT','MODE','SERVICE','PKMODE','LOADSITE','SPECIFIC','JOBTYPE','MILITARY','JOB_STATUS','EQUIP','PREFFIX','NOTESTATUS','NOTETYPE','NOTESUBTYPE','FWDSTATUS','QUOTESTATUS','QuoAcce','QFSTATUSREASON','ORDERSTATUS1'";
			List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpId,parameters);
			for (RefMasterDTO refObj : allParamValue) {
				if(refObj.getParameter().trim().equals("COUNTRY")){
					ocountry.put(refObj.getDescription().trim(),refObj.getDescription().trim());
					dcountry.put(refObj.getDescription().trim(),refObj.getDescription().trim());
					countryCod.put(refObj.getCode().trim(),refObj.getDescription().trim());
					countryDsc.put(refObj.getDescription().trim(),refObj.getCode().trim());
				}else if (refObj.getParameter().trim().equals("MILITARY")){
			 		military.put(refObj.getCode().trim() ,refObj.getCode().trim() ); 
			 	} 
				else{
					if(refObj.getParameter().trim().equals("STATE")){ 
			 			if(originCountry.equals(refObj.getBucket2().trim()))
			 				ostates.put(refObj.getCode().toUpperCase().trim(),refObj.getDescription().toUpperCase().trim());
			 			if(destCountry.equals(refObj.getBucket2().trim()))
			 				dstates.put(refObj.getCode().toUpperCase().trim(),refObj.getDescription().toUpperCase().trim());
			 		}
				if(parameterMap.containsKey(refObj.getParameter().trim())){
					tempParemeterMap = parameterMap.get(refObj.getParameter().trim());
					tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
					parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
				}else{
					tempParemeterMap = new LinkedHashMap<String, String>();
					tempParemeterMap.put(refObj.getCode().trim(),refObj.getDescription().trim());
					parameterMap.put(refObj.getParameter().trim(), tempParemeterMap);
				}
				}	
			}
			lead = findByParameterLocal("LEAD");
			survey = findByParameterLocal("SURVEY");
			state = findByParameterLocal("STATE");
			paytype = findByParameterLocal("PAYTYPE");
			job = findByParameterLocal("JOB");
			organiza = findByParameterLocal("ORGANIZA");
			peak = findByParameterLocal("PEAK");
			yesno = findByParameterLocal("YESNO");
			custStatus = findByParameterLocal("CUST_STATUS");
			routing = findByParameterLocal("ROUTING");
			commodit = findByParameterLocal("COMMODIT");
			mode = findByParameterLocal("MODE");
			service = findByParameterLocal("SERVICE");
			pkmode = findByParameterLocal("PKMODE");
			loadsite = findByParameterLocal("LOADSITE");
			specific = findByParameterLocal("SPECIFIC");
			jobtype = findByParameterLocal("JOBTYPE");
			JOB_STATUS = findByParameterLocal("JOB_STATUS");
			EQUIP = findByParameterLocal("EQUIP");
			preffix = findByParameterLocal("PREFFIX");
			notestatus = findByParameterLocal("NOTESTATUS");
			notetype = findByParameterLocal("NOTETYPE");
			notesubtype = findByParameterLocal("NOTESUBTYPE");
			forwardNoteStatus = findByParameterLocal("FWDSTATUS");
			QUOTESTATUS = findByParameterLocal("QUOTESTATUS");
			quoteAccept = findByParameterLocal("QuoAcce");
			qfReason=findByParameterLocal("QFSTATUSREASON");
			orderStatus1=findByParameterLocal( "ORDERSTATUS1");
			weightunits = new ArrayList();
			weightunits.add("lbs");
			weightunits.add("kgs");
			volumeunits = new ArrayList();
			volumeunits.add("Cft");
			volumeunits.add("Cbm");
			/*assignmentTypes = new HashMap<String, String>();
			assignmentTypes.put("Recruitment","Recruitment");
			assignmentTypes.put("Reassignment","Reassignment");
			assignmentTypes.put("Termination","Termination");
			assignmentTypes.put("None","None");*/
			assignmentTypes = refMasterManager.findByParameterWithOutASML(corpId, "AssignmentType");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String returnCountryCode(String bucket2) {
		String countryCode = "";
		if (bucket2.equalsIgnoreCase("United States")) {
			countryCode = "USA";
			} else if (bucket2.equalsIgnoreCase("India")) {
				countryCode = "IND";
			} else if (bucket2.equalsIgnoreCase("Canada")) {
				countryCode = "CAN";
			} else {
				countryCode = bucket2;
			}
		return countryCode;
	}
	
	private ExtendedPaginatedList quotationManagementListExt;
	private PagingLookupManager pagingLookupManager;
    private PaginateListFactory paginateListFactory;

	private String companyDivisn;

	private Date custCreatedOn;

	private String contractBillCode;

	private String custJobType;

	private int controlFlag;
    
    public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
        this.pagingLookupManager = pagingLookupManager;
    }

    public PaginateListFactory getPaginateListFactory() {
        return paginateListFactory;
    }

    public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
        this.paginateListFactory = paginateListFactory;
    }



    private List serviceOrderSearchType;  
    private String serviceOrderSearchVal;
    private String defaultCompanyCode="";
	@SkipValidation
	 public String showSearchCriteria() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				//getComboList(sessionCorpID, "");
				activeStatus = true;
				serviceOrderSearchType = new ArrayList();
				serviceOrderSearchType.add("Default Match");
				serviceOrderSearchType.add("Start With");
				serviceOrderSearchType.add("End With");
				serviceOrderSearchType.add("Exact Match");
				if((serviceOrderSearchVal==null)||(serviceOrderSearchVal.trim().equalsIgnoreCase(""))){
					serviceOrderSearchVal="Default Match";
				}
				defaultCompanyCode="1";
			   quotationManagementListExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
			   String key = "Please enter your search criteria below";
			   saveMessage(getText(key));
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}
	   	   return SUCCESS; 
	   }

	@SkipValidation
	public String quotationList() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			user = userManager.getUserByUsername(getRequest().getRemoteUser());
			roles = user.getRoles();

			if ((roles.toString().indexOf("ROLE_PRICING")) > -1) {
				ls = customerFileManager.findByCoordinatorQuotation(getRequest().getRemoteUser());
				coordinatr = getRequest().getRemoteUser();
			} else {
				ls = customerFileManager.quotationList();
				coordinatr = "";
			}
			customerFiles = new HashSet(ls);
			surveyDetails = new HashSet(ls);
			getSession().setAttribute("surveyDetails", surveyDetails);
			//getComboList(sessionCorpID, "");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		}
		return SUCCESS;
	}

	@SkipValidation
	public String searchQuotation() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			boolean last = (customerFile.getLastName() == null);
			boolean first = (customerFile.getFirstName() == null);
			boolean seqNum = (customerFile.getSequenceNumber() == null);
			boolean isBillToC = (customerFile.getBillToName() == null);
			boolean isJob = (customerFile.getJob() == null);
			boolean quotationStatus = (customerFile.getQuotationStatus() == null);
			boolean isCoord = (customerFile.getCoordinator() == null);
			boolean isEstimator = (customerFile.getEstimator() == null);
			boolean isControlFlag = (customerFile.getControlFlag() == null);
			boolean compnayDivision = (customerFile.getCompanyDivision() == null);
			boolean accountName =(customerFile.getAccountName() == null);	
			getRequest().getSession().setAttribute("activeStatusSession", activeStatus);
			serviceOrderSearchType = new ArrayList();
			serviceOrderSearchType.add("Default Match");
			serviceOrderSearchType.add("Start With");
			serviceOrderSearchType.add("End With");
			serviceOrderSearchType.add("Exact Match");
			if((serviceOrderSearchVal==null)||(serviceOrderSearchVal.trim().equalsIgnoreCase(""))){
				serviceOrderSearchVal="Default Match";
			}		
			if (!first || !last || !seqNum || !isBillToC || !isJob || !quotationStatus || !isCoord || !isEstimator || !isControlFlag ||!compnayDivision || !accountName) {
				
				quotationManagementListExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
				List<SearchCriterion> searchCriteria = new ArrayList();
				String sort = (String)getRequest().getParameter("sort");
				if (sort == null || sort.equals("")) {
						if(csoSortOrder ==null || csoSortOrder.equalsIgnoreCase("")){
							quotationManagementListExt.setSortCriterion("lastName");
						}else{
							quotationManagementListExt.setSortCriterion(csoSortOrder);
						}
						if(orderForCso ==null || orderForCso.equalsIgnoreCase("")){
							quotationManagementListExt.setSortDirection(SortOrderEnum.fromName("ascending"));
						}else{
							quotationManagementListExt.setSortDirection(SortOrderEnum.fromName(orderForCso));
						}
				}
				if(activeStatus==true){

						if(serviceOrderSearchVal.equalsIgnoreCase("Start With")){
							
							 searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
						     searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
						     searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
						     searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
						     searchCriteria.add(new SearchCriterion("job", customerFile.getJob(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     searchCriteria.add(new SearchCriterion("estimator", customerFile.getEstimator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     if(customerFile.getQuotationStatus()==null || customerFile.getQuotationStatus().toString().equals("")){
						     searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
						     }else{
						     searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));  
						     }
						     searchCriteria.add(new SearchCriterion("controlFlag", new String[] {"Q","L"}, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_IN));
						     searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
							 searchCriteria.add(new SearchCriterion("quotationStatus", "Cancelled", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
							 searchCriteria.add(new SearchCriterion("companyDivision", customerFile.getCompanyDivision(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							 searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
	
							 
						}else if(serviceOrderSearchVal.equalsIgnoreCase("End With")){
							 searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
						     searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
						     searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
						     searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
						     searchCriteria.add(new SearchCriterion("job", customerFile.getJob(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     searchCriteria.add(new SearchCriterion("estimator", customerFile.getEstimator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						     if(customerFile.getQuotationStatus()==null || customerFile.getQuotationStatus().toString().equals("")){
						     searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
						     }else{
						     searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));  
						     }
						     searchCriteria.add(new SearchCriterion("controlFlag", new String[] {"Q","L"}, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_IN));
						     searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
							 searchCriteria.add(new SearchCriterion("quotationStatus", "Cancelled", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
							 searchCriteria.add(new SearchCriterion("companyDivision", customerFile.getCompanyDivision(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							 searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
 
						}else if(serviceOrderSearchVal.equalsIgnoreCase("Exact Match")){
							   if((customerFile.getLastName()!=null)&&(!customerFile.getLastName().trim().equalsIgnoreCase(""))){
								   searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
								   }else{
								   searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
								   }
							   if((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().trim().equalsIgnoreCase(""))){
								   searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
								   }else{
								   searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
								   }
							   if((customerFile.getSequenceNumber()!=null)&&(!customerFile.getSequenceNumber().trim().equalsIgnoreCase(""))){
								   searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
								   }else{
								   searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
								   }
							   if((customerFile.getBillToName()!=null)&&(!customerFile.getBillToName().trim().equalsIgnoreCase(""))){
								   searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
								   }else{
								   searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
								   }
							      searchCriteria.add(new SearchCriterion("job", customerFile.getJob(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							      searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							      searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							      searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							      searchCriteria.add(new SearchCriterion("estimator", customerFile.getEstimator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));	      
							      if(customerFile.getQuotationStatus()==null || customerFile.getQuotationStatus().toString().equals("")){
							      searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
							      }else{
							      searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));  
							      }
							      searchCriteria.add(new SearchCriterion("controlFlag", new String[] {"Q","L"}, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_IN));
							      searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
								  searchCriteria.add(new SearchCriterion("quotationStatus", "Cancelled", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
								  searchCriteria.add(new SearchCriterion("companyDivision", customerFile.getCompanyDivision(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
								  if((customerFile.getAccountName()!=null)&&(!customerFile.getAccountName().trim().equalsIgnoreCase(""))){
									   searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
									   }else{
									   searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
									   }

						}else{
							  searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
						      searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
						      searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
						      searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
						      searchCriteria.add(new SearchCriterion("job", customerFile.getJob(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      searchCriteria.add(new SearchCriterion("estimator", customerFile.getEstimator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      
						      if(customerFile.getQuotationStatus()==null || customerFile.getQuotationStatus().toString().equals("")){
						      searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
						      }else{
						      searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));  
						      }
						      searchCriteria.add(new SearchCriterion("controlFlag", new String[] {"Q","L"}, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_IN));
						      searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
							  searchCriteria.add(new SearchCriterion("quotationStatus", "Cancelled", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
							  searchCriteria.add(new SearchCriterion("companyDivision", customerFile.getCompanyDivision(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							  searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));

						}				   
				}
				else{

					if(serviceOrderSearchVal.equalsIgnoreCase("Start With")){
						
						 searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
					     searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
					     searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
					     searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));
					     searchCriteria.add(new SearchCriterion("job", customerFile.getJob(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     searchCriteria.add(new SearchCriterion("estimator", customerFile.getEstimator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     if(customerFile.getQuotationStatus()==null || customerFile.getQuotationStatus().toString().equals("")){
					     searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
					     }else{
					     searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));  
					     }
					     searchCriteria.add(new SearchCriterion("controlFlag", new String[] {"Q","L"}, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_IN));
					     searchCriteria.add(new SearchCriterion("companyDivision", customerFile.getCompanyDivision(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						 searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_START_LIKE));

					}else if(serviceOrderSearchVal.equalsIgnoreCase("End With")){
						 searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
					     searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
					     searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
					     searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));
					     searchCriteria.add(new SearchCriterion("job", customerFile.getJob(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     searchCriteria.add(new SearchCriterion("estimator", customerFile.getEstimator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					     if(customerFile.getQuotationStatus()==null || customerFile.getQuotationStatus().toString().equals("")){
					     searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
					     }else{
					     searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));  
					     }
					     searchCriteria.add(new SearchCriterion("controlFlag", new String[] {"Q","L"}, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_IN));
					     searchCriteria.add(new SearchCriterion("companyDivision", customerFile.getCompanyDivision(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
  						 searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_END_LIKE));

					}else if(serviceOrderSearchVal.equalsIgnoreCase("Exact Match")){
						   if((customerFile.getLastName()!=null)&&(!customerFile.getLastName().trim().equalsIgnoreCase(""))){
							   searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							   }else{
							   searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
							   }
						   if((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().trim().equalsIgnoreCase(""))){
							   searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							   }else{
							   searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
							   }
						   if((customerFile.getSequenceNumber()!=null)&&(!customerFile.getSequenceNumber().trim().equalsIgnoreCase(""))){
							   searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							   }else{
							   searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
							   }
						   if((customerFile.getBillToName()!=null)&&(!customerFile.getBillToName().trim().equalsIgnoreCase(""))){
							   searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
							   }else{
							   searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
							   }
						      searchCriteria.add(new SearchCriterion("job", customerFile.getJob(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      searchCriteria.add(new SearchCriterion("estimator", customerFile.getEstimator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));	      
						      if(customerFile.getQuotationStatus()==null || customerFile.getQuotationStatus().toString().equals("")){
						      searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
						      }else{
						      searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));  
						      }
						      searchCriteria.add(new SearchCriterion("controlFlag", new String[] {"Q","L"}, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_IN));
						      searchCriteria.add(new SearchCriterion("companyDivision", customerFile.getCompanyDivision(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						      if((customerFile.getAccountName()!=null)&&(!customerFile.getAccountName().trim().equalsIgnoreCase(""))){
								   searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
								   }else{
								   searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
								   }	
					}else{
						  searchCriteria.add(new SearchCriterion("lastName", customerFile.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
					      searchCriteria.add(new SearchCriterion("firstName", customerFile.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
					      searchCriteria.add(new SearchCriterion("sequenceNumber", customerFile.getSequenceNumber(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
					      searchCriteria.add(new SearchCriterion("billToName", customerFile.getBillToName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
					      searchCriteria.add(new SearchCriterion("job", customerFile.getJob(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					      searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					      searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					      searchCriteria.add(new SearchCriterion("coordinator", customerFile.getCoordinator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					      searchCriteria.add(new SearchCriterion("estimator", customerFile.getEstimator(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
						  searchCriteria.add(new SearchCriterion("accountName", customerFile.getAccountName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));

					      if(customerFile.getQuotationStatus()==null || customerFile.getQuotationStatus().toString().equals("")){
					      searchCriteria.add(new SearchCriterion("quotationStatus", "Rejected", SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_NOT_EQUALS));
					      }else{
					      searchCriteria.add(new SearchCriterion("quotationStatus", customerFile.getQuotationStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));  
					      }
					      searchCriteria.add(new SearchCriterion("controlFlag", new String[] {"Q","L"}, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_IN));
					      searchCriteria.add(new SearchCriterion("companyDivision", customerFile.getCompanyDivision(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_EQUALS));
					}				   
			}
			      pagingLookupManager.getAllRecordsPage(CustomerFile.class, quotationManagementListExt, searchCriteria);
				
			}
			 if(quotationManagementListExt.getFullListSize()==1){
				    detailPage="true";
			    }
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
private  Map<String, String> coordinatorList = new LinkedHashMap<String, String>();
private  Map<String, String> estimatorList = new LinkedHashMap<String, String>();
private  Map<String, String> pricingList = new LinkedHashMap<String, String>();
private  Map<String, String> billingList = new LinkedHashMap<String, String>();
private  Map<String, String> payableList = new LinkedHashMap<String, String>();
private  Map<String, String> auditorList = new LinkedHashMap<String, String>();
private  Map<String, String> execList = new LinkedHashMap<String, String>();
private String roleHitFlag;

private CompanyManager companyManager;
private String quotesToValidate;
private String companies;
private ErrorLogManager errorLogManager;
private String originStateListJson;
private String destinationStateListJson;
private String contractDiscountAddValue;
@SkipValidation
	public String editQuotation() {
	try {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		roleHitFlag="OK";
		if (id != null) {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				customerFile = customerFileManager.get(id);
				List statess1 = customerFileManager.findStateList(customerFile.getOriginCountryCode(), sessionCorpID);
				List statess2 = customerFileManager.findStateList(customerFile.getDestinationCountryCode(), sessionCorpID);
				 ObjectMapper mapper = new ObjectMapper();
				 if((customerFile.getOriginCountry()!=null && !customerFile.getOriginCountry().equalsIgnoreCase(""))||(customerFile.getOriginCountryCode()==null || customerFile.getOriginCountryCode().equalsIgnoreCase(""))){
				     customerFile.setOriginCountryCode(countryDsc.get(customerFile.getOriginCountry()));
				     ostates=customerFileManager.findDefaultStateList(customerFile.getOriginCountryCode(), sessionCorpID);
						String originCityStateCode=customerFile.getOriginCityCode();
						if(originCityStateCode!=null && !(originCityStateCode.equalsIgnoreCase(""))&& originCityStateCode.contains(",") && customerFile.getOriginState()!=null && !(customerFile.getOriginState().equals(""))){
						String originCityStateCode1[]=originCityStateCode.split(",");
						int arrayLength = originCityStateCode1.length;
						if(arrayLength>1){
							for(int i=0;i<arrayLength;i++){
							originCityStateCode=originCityStateCode1[i];
							originCityStateCode=originCityStateCode.trim();
						}
						customerFile.setOriginState(originCityStateCode);
						}
						}
				 }else if((customerFile.getOriginCountryCode()!=null && !customerFile.getOriginCountryCode().equalsIgnoreCase(""))||(customerFile.getOriginCountry()==null || customerFile.getOriginCountry().equalsIgnoreCase(""))){
				     customerFile.setOriginCountry(countryCod.get(customerFile.getOriginCountryCode()));
				 }

				    if((customerFile.getDestinationCountry()!=null && !customerFile.getDestinationCountry().equalsIgnoreCase(""))||(customerFile.getDestinationCountryCode()==null || customerFile.getDestinationCountryCode().equalsIgnoreCase(""))){
				     customerFile.setDestinationCountryCode(countryDsc.get(customerFile.getDestinationCountry()));
				     dstates=customerFileManager.findDefaultStateList(customerFile.getDestinationCountryCode(), sessionCorpID);	
				    	String destinCityStateCode=customerFile.getDestinationCityCode();
						if(destinCityStateCode!=null && !(destinCityStateCode.equals(""))&& destinCityStateCode.contains(",") && customerFile.getDestinationState()!=null  && !(customerFile.getDestinationState().equals(""))){
						String destinCityStateCode1[]=destinCityStateCode.split(",");
						int arrayLength = destinCityStateCode1.length;
						if(arrayLength>1){
						for(int i=0;i<arrayLength;i++){
						destinCityStateCode=destinCityStateCode1[i];
						destinCityStateCode=destinCityStateCode.trim();
						}
						customerFile.setDestinationState(destinCityStateCode);
						}
						}
				    }else if((customerFile.getDestinationCountryCode()!=null && !customerFile.getDestinationCountryCode().equalsIgnoreCase(""))||(customerFile.getDestinationCountry()==null || customerFile.getDestinationCountry().equalsIgnoreCase(""))){
				     customerFile.setDestinationCountry(countryCod.get(customerFile.getDestinationCountryCode()));
				    }
				 originStateListJson = mapper.writeValueAsString(statess1);
				 destinationStateListJson = mapper.writeValueAsString(statess2);
				 assignmentList=accountAssignmentTypeManager.findAssignmentByParentAgent(customerFile.getBillToCode(),sessionCorpID);
				 if(customerFile.getContract()!=null && !(customerFile.getContract().equals(""))){
				 List contractDiscount=billingManager.priceContract(customerFile.getContract());
					String[] str1=null;
					if(contractDiscount!=null && (!(contractDiscount.isEmpty())) && contractDiscount.get(0)!=null){
				       str1 = contractDiscount.get(0).toString().split("#");
					}else{
						
					}
					if(str1!=null && str1[0]!=null && str1[1]!=null && str1[2]!=null && str1[3]!=null && str1[4]!=null && str1[5]!=null && str1[6]!=null){
					contractDiscountAddValue=str1[0]+"#"+str1[1] +"#" +str1[2]+"#"+str1[3]+"#" +str1[4] +"#"+str1[5]+"#"+str1[6];
					}else{
						contractDiscountAddValue="";
					}
				 }
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				customerFileTemp = customerFileManager.get(customerFile.getId());
				customerServiceOrders = customerFileTemp.getServiceOrders();
				QuotesNumber= customerServiceOrders.size();
				emailSetupValue=emailSetupManager.findEmailStatus(sessionCorpID, customerFile.getSequenceNumber());
				//getComboList(sessionCorpID, customerFile.getJob());
				//ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
		        //dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
		        contracts = customerFileManager.findCustJobContract(customerFile.getBillToCode(),customerFile.getJob(),customerFile.getCreatedOn(),sessionCorpID,customerFile.getCompanyDivision(),customerFile.getBookingAgentCode());
		        adAddressesDetailsList= adAddressesDetailsManager.getAdAddressesDetailsList(id,sessionCorpID);
		        companyDivis = customerFileManager.findCompanyDivisionByBookAg(customerFile.getCorpID(),customerFile.getBookingAgentCode());
		        //coordinatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_COORD",sessionCorpID);
		       /* for (UserDTO userDTO : allUser) {

					if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_SALE")){ 
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){	 
						estimatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_COORD")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
							coordinatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase()); 
						}
						 
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PRICING")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){	 
						pricingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_ARM") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_CAJ") ||  userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_SSC")  ){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						billingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PAYABLE")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						payableList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_AUDITOR")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						auditorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_EXECUTIVE")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						execList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}
					
		        }*/
		        if(customerFile.getCoordinator()!=null && !(customerFile.getCoordinator().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getCoordinator());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String aliasName=aliasNameList.get(0).toString();
						String coordCode=customerFile.getCoordinator().toUpperCase();
						String alias = aliasName.toUpperCase();
						if(!coordinatorList.containsValue(coordCode)){
							coordinatorList.put(coordCode,alias );
						}
					}
				}
				
				//estimatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_SALE",sessionCorpID);
				if(customerFile.getSalesMan()!=null && !(customerFile.getSalesMan().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getSalesMan());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String estimatorCode=customerFile.getSalesMan().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!estimatorList.containsValue(estimatorCode)){
							estimatorList.put(estimatorCode,alias );
						}
					}
						
				}
				
				//pricingList = refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_PRICING",sessionCorpID);
				if(customerFile.getPersonPricing()!=null && !(customerFile.getPersonPricing().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getPersonPricing());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String pricingCode=customerFile.getPersonPricing().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!pricingList.containsValue(pricingCode)){
							pricingList.put(pricingCode,alias);
						}
					}
					
				}
				
				//billingList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_BILLING",sessionCorpID);
				if(customerFile.getPersonBilling()!=null && !(customerFile.getPersonBilling().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getPersonBilling());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String billingCode=customerFile.getPersonBilling().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!billingList.containsValue(billingCode)){
							billingList.put(billingCode,alias);
						}
					}
				}
				
				//payableList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_PAYABLE",sessionCorpID);
				if(customerFile.getPersonPayable()!=null && !(customerFile.getPersonPayable().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getPersonPayable());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String payableCode=customerFile.getPersonPayable().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!payableList.containsValue(payableCode)){
							payableList.put(payableCode,alias);
						}
					}
				}
				//auditorList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_AUDITOR",sessionCorpID);
				if(customerFile.getAuditor()!=null && !(customerFile.getAuditor().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getAuditor());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String auditorCode=customerFile.getAuditor().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!auditorList.containsValue(auditorCode)){
							auditorList.put(auditorCode,alias);
						}
					}
				} 
				//execList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_EXECUTIVE",sessionCorpID);
				if(customerFile.getApprovedBy()!=null && !(customerFile.getApprovedBy().equalsIgnoreCase(""))){
					List aliasNameList = userManager.findUserByUsername(customerFile.getApprovedBy());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String execCode=customerFile.getApprovedBy().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!execList.containsValue(execCode)){
							execList.put(execCode,alias);
						}
					}
				}
				customerFile.setCustomerEmployer(customerFile.getBillToName());
				if (customerFile.getOriginState()!=null && !(customerFile.getOriginState().equals(""))) {
					customerFile.setOriginCityCode(customerFile.getOriginCity() + ", " + customerFile.getOriginState());				
				} else {
					customerFile.setOriginCityCode(customerFile.getOriginCity());
				}
				if (customerFile.getDestinationState()!=null && !(customerFile.getDestinationState().equals(""))) {
					customerFile.setDestinationCityCode(customerFile.getDestinationCity() + ", " + customerFile.getDestinationState());
				} else {
					customerFile.setDestinationCityCode(customerFile.getDestinationCity());
				}
				List quotesToValidateList=customerFileManager.getQuotesToValidate(sessionCorpID, customerFile.getJob(),customerFile.getCompanyDivision());
				if(quotesToValidateList!=null && (!quotesToValidateList.isEmpty())&& quotesToValidateList.get(0)!=null){
					quotesToValidate=quotesToValidateList.get(0).toString();
				}
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						if(!systemDefault.getCompanyCode().equalsIgnoreCase("")){
					checkContractChargesMandatory=systemDefault.getContractChargesMandatory();
					}
					}
				}
			} else {
				//getComboList(sessionCorpID, "a");
				ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
		        dstates = customerFileManager.findDefaultStateList("", sessionCorpID);
		        contracts = customerFileManager.findCustJobContract(contractBillCode,custJobType,custCreatedOn,sessionCorpID,companyDivisn,bookingAgentCode);
				
		        customerFile = new CustomerFile();            
				//sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
				if(bookingAppUserCode!=null && !(bookingAppUserCode.equalsIgnoreCase(""))){
					String bookingAppUserName="";
					List bookingAppUserTemp = customerFileManager.findByBillToCode(bookingAppUserCode, sessionCorpID);
					if(bookingAppUserTemp!=null && !bookingAppUserTemp.isEmpty() && bookingAppUserTemp.get(0)!=null)
					    bookingAppUserName=bookingAppUserTemp.get(0).toString();
					
					customerFile.setBookingAgentCode(bookingAppUserCode);
					customerFile.setBookingAgentName(bookingAppUserName);
				}
				else{
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						if(!systemDefault.getCompanyCode().equalsIgnoreCase("")){
						customerFile.setBookingAgentCode(systemDefault.getCompanyCode());
						customerFile.setBookingAgentName(systemDefault.getCompany());
						checkContractChargesMandatory=systemDefault.getContractChargesMandatory();
					}
					}
				}
				}
				customerFile.setCorpID(sessionCorpID);
				companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID, customerFile.getBookingAgentCode());
				//List companyDiv=customerFileManager.findCompanyDivisionByBookAg(customerFile.getCorpID(),customerFile.getBookingAgentCode());;
				if(companyDivis!=null && !companyDivis.isEmpty() && companyDivis.get(0)!=null){
					companyDivisn = companyDivis.get(0).toString();
			    }
				if(customerFile.getBookingAgentCode()!=null && !customerFile.getBookingAgentCode().equals("")){
						customerFile.setCompanyDivision(companyDivisn);
						String jobAndCompDiv = customerFileManager.getCompanyCodeAndJobFromCompanyDivision(sessionCorpID, customerFile.getBookingAgentCode());
						if(jobAndCompDiv!=null && !jobAndCompDiv.equalsIgnoreCase("")){
							String jobCompDiv[] = jobAndCompDiv.split("~");
							if(jobCompDiv[1]!=null && !jobCompDiv[1].equalsIgnoreCase("") && !jobCompDiv[1].equalsIgnoreCase("NA")){
								customerFile.setJob(jobCompDiv[1]);
								getRefJobTypeCountryList=customerFileManager.getRefJobTypeCountry(jobCompDiv[1], sessionCorpID,companyDivisn);
								if(getRefJobTypeCountryList!=null && !getRefJobTypeCountryList.isEmpty() && getRefJobTypeCountryList.get(0)!=null){
									try{
										String tempCountryVal = getRefJobTypeCountryList.get(0).toString();
										String countryVal[] = tempCountryVal.split("~");
										customerFile.setOriginCountryCode(countryVal[0]);
										customerFile.setDestinationCountryCode(countryVal[0]);
										customerFile.setOriginCountry(countryVal[1]);
										customerFile.setDestinationCountry(countryVal[1]);
										ostates = customerFileManager.findDefaultStateList(countryVal[0], sessionCorpID);
										dstates = customerFileManager.findDefaultStateList(countryVal[0], sessionCorpID);
									}catch(Exception e){
										e.printStackTrace();
									}
								}
							}
						}
					}else{
						customerFile.setCompanyDivision("");
					}
				//customerFile.setCompanyDivision(companyDivisn);
				customerFile.setContract(defaultcontract);
				customerFile.setStatus("NEW");
				customerFile.setQuotationStatus("New");
				customerFile.setStatusNumber(1);
				customerFile.setComptetive("Y");
			//	customerFile.setCorpID(sessionCorpID);
				customerFile.setSurveyTime("00:00");
				customerFile.setSurveyTime2("00:00");
				customerFile.setPrefSurveyTime1("00:00");
				customerFile.setPrefSurveyTime2("00:00");
				customerFile.setPrefSurveyTime3("00:00");
				customerFile.setPrefSurveyTime4("00:00");
				customerFile.setAssignmentType("None");
				customerFile.setCreatedOn(new Date());
				customerFile.setSystemDate(new Date());
				customerFile.setUpdatedOn(new Date());
				customerFile.setCreatedBy(getRequest().getRemoteUser());
				customerFile.setUpdatedBy(getRequest().getRemoteUser());
				customerFile.setPersonPricing((getRequest().getRemoteUser()).toUpperCase());
				
				
			}
			user = new User();
			getNotesForIconChange();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	} catch (Exception e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  	 e.printStackTrace();
	  	 return "errorlog";
	}
		return SUCCESS;

	}
private List getRefJobTypeCountryList;
//voxmecodestart
private Company company; 
private String voxmeIntergartionFlag;
public String getVoxmeIntergartionFlag() {
	return voxmeIntergartionFlag;
}

public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
	this.voxmeIntergartionFlag = voxmeIntergartionFlag;
}
public Company getCompany() {
	return company;
}
public void setCompany(Company company) {
	this.company = company;
}
//voxmecodeend
private String enbState;
public void prepare() throws Exception {
	try {
		if(getRequest().getParameter("ajax") == null){
			getComboList(sessionCorpID, "a");
			systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
			companyDivis = customerFileManager.findCompanyDivision(sessionCorpID); 
			enbState = customerFileManager.enableStateList(sessionCorpID);
		  // voxmecode
			List compTemp = companyManager.findByCorpID(sessionCorpID);
			     company= (Company) compTemp.get(0);
			if(company!=null){
				if(company.getVoxmeIntegration()!=null){
					voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
				}
				if(company.getCompanyDivisionFlag()!=null && (!(company.getCompanyDivisionFlag().toString().equals(""))) ){
					companies=	company.getCompanyDivisionFlag();
				}
				if(company.getAccessQuotationFromCustomerFile()!=null){
				checkAccessQuotation=company.getAccessQuotationFromCustomerFile();
				}
				compDivFlag=company.getCompanyDivisionFlag();	
			}
			//voxmecodeend
		}
		sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				if(!systemDefault.getCompanyCode().equalsIgnoreCase("")){
				checkContractChargesMandatory=systemDefault.getContractChargesMandatory();
				}
			}
		}
	} catch (Exception e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  	 e.printStackTrace();
	}
}	

@SkipValidation
	public String findContractbyJob() {
	try {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			 contracts = customerFileManager.findCustJobContract(contractBillCode,custJobType,custCreatedOn,sessionCorpID,companyDivisn,bookingAgentCode);
			 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	} catch (Exception e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  	 e.printStackTrace();
	  	 return "errorlog";
	}
		return SUCCESS;
	}
/** Method used to geting CurrentDate with yyyy-MM-dd format . */ 
private java.util.Date getCurrentDate() {
	DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
	String dateStr = dfm.format(new Date());
	Date currentDate = null;
	try {
		currentDate = dfm.parse(dateStr);
		
	} catch (java.text.ParseException e) {
		logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		e.printStackTrace();
	}
	return currentDate;
	
}
/** End of Method. */

private String systemDefaultStorageValue; 
   // private String companies;
	private String checkCompanyBA;
	public String saveQuotation() throws Exception {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			Long StartTime = System.currentTimeMillis();
			contracts = customerFileManager.findCustJobContract(customerFile.getBillToCode(),customerFile.getJob(),customerFile.getCreatedOn(),sessionCorpID,customerFile.getCompanyDivision(),customerFile.getBookingAgentCode());
			companyDivis = customerFileManager.findCompanyDivisionByBookAg(customerFile.getCorpID(),customerFile.getBookingAgentCode());
			if(!customerFile.getBookingAgentCode().equals("")){
				if(bookingAgentCodeValid().equals("input")){
					String key = "bookingAgentCode is not valid";
					errorMessage(getText(key));
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
					return SUCCESS;
				  }
				}
			if(!customerFile.getBillToCode().equals("")){
				if(billtoCodeValid().equals("input")){
					String key = "billToCode is not valid";
					errorMessage(getText(key));
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
					return SUCCESS;
				  }
				}
			if(!(customerFile.getBillToCode().toString().trim().equals("")) && (customerFile.getBillToCode()!=null)){
				try{
			List billToCodeList=customerFileManager.findByBillToCode(customerFile.getBillToCode(), sessionCorpID);
			if(billToCodeList.size()>0 && !billToCodeList.isEmpty()){
				customerFile.setBillToName(billToCodeList.get(0).toString());
			}
				}catch (NullPointerException nullExc){
					nullExc.printStackTrace();						
				}
				catch (Exception e){
					e.printStackTrace();
				}
			}
		if(!customerFile.getAccountCode().equals("")){
				if(accountCodeValid().equals("input")){
					String key = "accountCode is not valid";
					errorMessage(getText(key));
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
					return SUCCESS;
				  }
				}
			if(((customerFile.getOriginCountry().equalsIgnoreCase("United States")) ) && (customerFile.getOriginState() ==null || customerFile.getOriginState().equalsIgnoreCase(""))&& (!(customerFile.getStatus().equals("CNCL"))) && customerFile.getComptetive().equalsIgnoreCase("N")) {
				String key = "State is a required field in Origin.";
				//validateFormNav = "NOTOK";
				errorMessage(getText(key));
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			    return SUCCESS;
			} 
			if(((customerFile.getDestinationCountry().equalsIgnoreCase("United States")) ) && (customerFile.getDestinationState() == null || customerFile.getDestinationState().equalsIgnoreCase(""))&& (!(customerFile.getStatus().equals("CNCL"))) && customerFile.getComptetive().equalsIgnoreCase("N")) {
				String key = "State is a required field in Destination.";
				//validateFormNav = "NOTOK";
				errorMessage(getText(key));
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			    return SUCCESS;
			}
					
			//companies = companyManager.findByCompanyDivis(sessionCorpID);
			if (companies.equalsIgnoreCase("Yes") && (customerFile.getCompanyDivision().equalsIgnoreCase("") || customerFile.getCompanyDivision().equalsIgnoreCase(null))) {
				String key ="Company Division is required field.";
				errorMessage(getText(key));
				//validateFormNav = "NOTOK";
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
			boolean customercheckBox = ((customerFile.getPortalIdActive()!=null && customerFile.getPortalIdActive()) == (true));
			if(customercheckBox && customerFile.getEmail().equalsIgnoreCase("")){
				String key ="Primary Email address is required in the Origin Address section for generating Customer portal ID.";
				errorMessage(getText(key));
				//validateFormNav = "NOTOK";
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
			if(((customerFile.getQuotationStatus().equalsIgnoreCase("Rejected")) || (customerFile.getQuotationStatus().equalsIgnoreCase("Cancelled"))) && (customerFile.getQfStatusReason()!=null && customerFile.getQfStatusReason().equalsIgnoreCase(""))){
				String key = "Status Reason is a required field.";
				validateFormNav = "NOTOK";
				errorMessage(getText(key));
			    return SUCCESS;
			}
			
			/* Storage Booking agent code for SSCW */
			  if(checkCompanyBA!=null && (!checkCompanyBA.equalsIgnoreCase(""))&&(checkCompanyBA.equalsIgnoreCase("YES"))){
						if((customerFile.getJob()!=null) &&(!customerFile.getJob().equalsIgnoreCase("")) ){
							//sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
							if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
								for (SystemDefault systemDefault : sysDefaultDetail) {
									 systemDefaultStorageValue = systemDefault.getStorage();								
									}
								}
							 if(systemDefaultStorageValue.contains(customerFile.getJob())){
								   List companyDivisionBookAgentValue = serviceOrderManager.findCompanyDivisionBookAgent(sessionCorpID);
							    if(companyDivisionBookAgentValue!=null && (!companyDivisionBookAgentValue.isEmpty()) && (companyDivisionBookAgentValue.get(0)!=null)&& (!companyDivisionBookAgentValue.get(0).toString().equals(""))){
							    	if(!companyDivisionBookAgentValue.contains(customerFile.getBookingAgentCode())){
							    		String defaultBookingAgentCode="";
									     String defaultBookingAgentName="";
									     List bookAgent = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
									     if((bookAgent!=null)&&(!bookAgent.isEmpty())&&(bookAgent.get(0)!=null)){
									    	 CompanyDivision companyDivision  = (CompanyDivision)bookAgent.get(0);
									    	 	defaultBookingAgentCode = companyDivision.getBookingAgentCode();
												defaultBookingAgentName = companyDivision.getDescription();
											}
									     customerFile.setBookingAgentCode(defaultBookingAgentCode);
									     customerFile.setBookingAgentName(defaultBookingAgentName);
									     String key = "Booking agent should be Company Division's Booking Agent";							     
									     String key1= "Booking agent code has been changed to:" + customerFile.getBookingAgentCode(); 
									     saveMessage(getText(key+"<BR>"+key1));
									     //return SUCCESS;
								    }else{     
								    }       
							   }   
							  }
						}
			  }
			
			
			if (cancel != null) {
				return "cancel";
			}
			if (delete != null) {
				return delete();
			}

			boolean isNew = (customerFile.getId() == null);
			if (customerFile.getQuotationStatus().equalsIgnoreCase("ACCEPTED")) {
				if(customerFile.getBookingDate()==null){
					customerFile.setBookingDate(new Date());
					customerFile.setSalesStatus("Booked");
				}
				customerFile.setControlFlag("C");
				customerFile.setMoveType("BookedMove");
				customerFile.setStatus("NEW");
				customerFileTemp = customerFileManager.get(customerFile.getId());
				customerServiceOrders = customerFileTemp.getServiceOrders();
				customerBillings=customerFileTemp.getAgentQuotesbillings();
				if (customerServiceOrders != null) {
			    
					Iterator it = customerServiceOrders.iterator();
					while (it.hasNext()) {
						
						serviceOrder = (ServiceOrder) it.next();
						serviceOrder.setControlFlag("C");
						serviceOrder.setMoveType("BookedMove");
						serviceOrder.setFirstName(customerFile.getFirstName());
						serviceOrder.setLastName(customerFile.getLastName());
			           if(customerServiceOrders.size()==1){
			        	  //Bug 11839 - Possibility to modify Current status to Prior status
			        	   serviceOrder.setStatus("NEW");	
						   serviceOrder.setStatusDate(new Date());
						   serviceOrder.setStatusNumber(1);
						}else{
						if(serviceOrder.getQuoteAccept().equals("A") ){
							serviceOrder.setStatus("NEW");	
							serviceOrder.setStatusDate(new Date());
							serviceOrder.setStatusNumber(1);
						}
						if(serviceOrder.getQuoteAccept().equals("P")|| serviceOrder.getQuoteAccept().equals("")){
							serviceOrder.setStatus("HOLD");
			        		serviceOrder.setStatusDate(new Date());
			        		serviceOrder.setStatusNumber(100); 
						}
						if(serviceOrder.getQuoteAccept().equals("R")){
							serviceOrder.setStatus("CNCL");
			        		serviceOrder.setStatusDate(new Date());
			        		serviceOrder.setStatusNumber(400); 
						}
						}
					}
				}
				if (customerBillings != null) {

					Iterator it = customerBillings.iterator();
					while (it.hasNext()) {

						billings = (Billing) it.next();
						billings.setPersonBilling(customerFile.getPersonBilling());
			        	billings.setPersonPayable(customerFile.getPersonPayable());
			        	billings.setPersonPricing(customerFile.getPersonPricing());
			        	billings.setAuditor(customerFile.getAuditor());
			        	billings.setBillToReference(customerFile.getBillToReference());
						}
				}
			} else {
				customerFile.setStatus("NEW");
				customerFile.setStatusNumber(1);
				customerFile.setControlFlag("Q");
			}
			
			//9134 - To reject all the associated Quotes when QF is rejected
			if ((customerFile.getQuotationStatus().equalsIgnoreCase("Rejected"))||(customerFile.getQuotationStatus().equalsIgnoreCase("Cancelled"))) {
				customerFileTemp = customerFileManager.get(customerFile.getId());
				customerServiceOrders = customerFileTemp.getServiceOrders();
				customerFile.setStatus("CNCL");
				customerFile.setStatusNumber(400);
				if (customerServiceOrders != null) {            
					Iterator it = customerServiceOrders.iterator();
					while (it.hasNext()) {
						serviceOrder = (ServiceOrder) it.next();
						//Bug 11839 - Possibility to modify Current status to Prior status
						serviceOrder.setStatus("CNCL");
			    		serviceOrder.setStatusReason(customerFile.getQfStatusReason());
			    		serviceOrder.setQuoteAccept("R");
			    		serviceOrder.setStatusNumber(400);
			    		serviceOrder.setStatusDate(new Date());
					}
				}
			}
			
			if(isNew){
			roleHitFlag="OK";
			maxSequenceNumber = customerFileManager.findMaximum(sessionCorpID);
			if (maxSequenceNumber.get(0) == null) {
				customerFile.setSequenceNumber(sessionCorpID + "1000001");
			} else {
				autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(sessionCorpID, "")) + 1;
				seqNum = sessionCorpID + autoSequenceNumber.toString();
				customerFile.setSequenceNumber(seqNum);
			}
			checkSequenceNumberList = customerFileManager.findSequenceNumber(customerFile.getSequenceNumber());
			if (!(checkSequenceNumberList.isEmpty()) && isNew) {
				autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(sessionCorpID, "")) + 1;

				seqNum = sessionCorpID + autoSequenceNumber.toString();
				customerFile.setSequenceNumber(seqNum);
			}
			
			
			}

			if (!isNew) {
				roleHitFlag="OK";
				noMoreIfStat = true;
				if (customerFile.getStatusNumber() < 10 && customerFile.getComptetive().equals("Y") && customerFile.getSurvey() != null && customerFile.getSurvey().after(sysdate)) {
					customerFile.setStatus("PEND");
					//customerFile.setQuotationStatus("PEND");
					customerFile.setStatusNumber(10);
					customerFile.setStatusDate(sysdate);
					noMoreIfStat = false;
				}

				customerFileTemp = customerFileManager.get(customerFile.getId());
				customerServiceOrders = customerFileTemp.getServiceOrders();
				noOfClaims = 0;
				noOfStorage = 0;
				soStatusNumberMax = 1;
				soStatusNumberMin = 60;

				if (customerServiceOrders != null) {

					Iterator it = customerServiceOrders.iterator();
					while (it.hasNext()) {

						serviceOrder = (ServiceOrder) it.next();
						if (soStatusNumberMax < serviceOrder.getStatusNumber().intValue()) {
							soStatusNumberMax = serviceOrder.getStatusNumber().intValue();
						}
						if (soStatusNumberMin > serviceOrder.getStatusNumber().intValue()) {
							soStatusNumberMin = serviceOrder.getStatusNumber().intValue();
						}
						if ((serviceOrder.getStatus().equalsIgnoreCase("CLMS") || serviceOrder.getStatus().equalsIgnoreCase("DLVR")) && atLeastOneClaim) {
							if (serviceOrder.getStatus().equalsIgnoreCase("CLMS")) {
								noOfClaims = noOfClaims + 1;
							}
							atLeastOneClaim = true;
						} else {
							atLeastOneClaim = false;
						}
						if ((serviceOrder.getStatus().equalsIgnoreCase("CLSD") || serviceOrder.getStatus().equalsIgnoreCase("STG")) && atLeastOneStorage) {
							if (serviceOrder.getStatus().equalsIgnoreCase("STG")) {
								noOfStorage = noOfStorage + 1;
							}
							atLeastOneStorage = true;
						} else {
							atLeastOneStorage = false;
						}
						if (serviceOrder.getStatus().equalsIgnoreCase("HOLD") && allJobHold) {
							allJobHold = true;
						} else {
							allJobHold = false;
						}
					}
				}
				if (customerFile.getStatusNumber() >= 10 && customerFile.getStatusNumber() < 20 && soStatusNumberMax > 10 && soStatusNumberMax < 60 && noOfClaims >= 1) {
					//Bug 12317 - Delete update Status from Application
					//customerFile.setStatus("ACTIVE");
					//customerFile.setStatusNumber(20);
					//customerFile.setStatusDate(sysdate);

				}

				if (customerFile.getStatusNumber() >= 20 && customerFile.getStatusNumber() < 30 && soStatusNumberMin >= 60 && noOfStorage >= 1) {
					//Bug 12317 - Delete update Status from Application
					//customerFile.setStatus("DELIV");
					//customerFile.setStatusNumber(30);
					//customerFile.setStatusDate(sysdate);

				}

				if (customerFile.getStatusNumber() >= 30 && customerFile.getStatusNumber() < 40 && atLeastOneClaim) {
					//Bug 12317 - Delete update Status from Application
					//customerFile.setStatus("CLAIM");
					//customerFile.setStatusNumber(40);
					//customerFile.setStatusDate(sysdate);

				}

				if (customerFile.getStatusNumber() >= 40 && customerFile.getStatusNumber() < 50 && atLeastOneStorage) {
					//Bug 12317 - Delete update Status from Application
					//customerFile.setStatus("STORG");
					//customerFile.setStatusNumber(50);
					//customerFile.setStatusDate(sysdate);

				}

				if (customerFile.getStatusNumber() >= 50 && customerFile.getStatusNumber() < 500 && allJobHold) {
					customerFile.setStatus("HOLD");
					customerFile.setStatusNumber(customerFile.getStatusNumber() + 1);
					customerFile.setStatusDate(sysdate);

				}
				
				  
				/*for (UserDTO userDTO : allUser) {

					if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_SALE")){ 
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){	 
						estimatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_COORD")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
							coordinatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase()); 
						}
						 
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PRICING")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){	 
						pricingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_ARM") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_CAJ") ||  userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_SSC")  ){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						billingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PAYABLE")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						payableList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_AUDITOR")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						auditorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_EXECUTIVE")){
						if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						execList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
						}
					}
					
			    }*/
				
				//coordinatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_COORD",sessionCorpID);
				if(customerFile.getCoordinator()!=null && !(customerFile.getCoordinator().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getCoordinator());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String aliasName=aliasNameList.get(0).toString();
						String coordCode=customerFile.getCoordinator().toUpperCase();
						String alias = aliasName.toUpperCase();
						if(!coordinatorList.containsValue(coordCode)){
							coordinatorList.put(coordCode,alias );
						}
					}
				}
				
				//estimatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_SALE",sessionCorpID);
				if(customerFile.getSalesMan()!=null && !(customerFile.getSalesMan().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getSalesMan());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String estimatorCode=customerFile.getSalesMan().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!estimatorList.containsValue(estimatorCode)){
							estimatorList.put(estimatorCode,alias );
						}
					}
						
				}
				
				//pricingList = refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_PRICING",sessionCorpID);
				if(customerFile.getPersonPricing()!=null && !(customerFile.getPersonPricing().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getPersonPricing());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String pricingCode=customerFile.getPersonPricing().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!pricingList.containsValue(pricingCode)){
							pricingList.put(pricingCode,alias);
						}
					}
					
				}
				
				//billingList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_BILLING",sessionCorpID);
				if(customerFile.getPersonBilling()!=null && !(customerFile.getPersonBilling().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getPersonBilling());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String billingCode=customerFile.getPersonBilling().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!billingList.containsValue(billingCode)){
							billingList.put(billingCode,alias);
						}
					}
				}
				
				//payableList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_PAYABLE",sessionCorpID);
				if(customerFile.getPersonPayable()!=null && !(customerFile.getPersonPayable().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getPersonPayable());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String payableCode=customerFile.getPersonPayable().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!payableList.containsValue(payableCode)){
							payableList.put(payableCode,alias);
						}
					}
				}
				//auditorList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_AUDITOR",sessionCorpID);
				if(customerFile.getAuditor()!=null && !(customerFile.getAuditor().equalsIgnoreCase(""))){
					List aliasNameList=userManager.findUserByUsername(customerFile.getAuditor());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String auditorCode=customerFile.getAuditor().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!auditorList.containsValue(auditorCode)){
							auditorList.put(auditorCode,alias);
						}
					}
				}
				//execList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_EXECUTIVE",sessionCorpID);
				
				if(customerFile.getApprovedBy()!=null && !(customerFile.getApprovedBy().equalsIgnoreCase(""))){
					List aliasNameList = userManager.findUserByUsername(customerFile.getApprovedBy());
					if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
						String execCode=customerFile.getApprovedBy().toUpperCase();
						String aliasName=aliasNameList.get(0).toString();
						String alias = aliasName.toUpperCase();
						if(!execList.containsValue(execCode)){
							execList.put(execCode,alias);
						}
					}
				}
				
			}

			if (customerFile.getOriginState() == null) {
				customerFile.setOriginState("");
			}
			if (customerFile.getDestinationState() == null) {
				customerFile.setDestinationState("");
			}

			if (isNew) {
				customerFile.setCreatedOn(new Date());
				customerFile.setCreatedBy(getRequest().getRemoteUser());
			} 
			if(customerFile.getOriginAgentName().equals("")){
				customerFile.setOriginAgentCode("");
			}
			customerFile.setUpdatedOn(new Date());
			customerFile.setUpdatedBy(getRequest().getRemoteUser());
			if (customerFile.getCoordinator()==null || customerFile.getCoordinator().equals("")) {
				customerFile.setCoordinatorName(customerFile.getCoordinator());
			} else {
				if (customerFile.getCoordinator() == null) {
					customerFile.setCoordinator("");
					customerFile.setCoordinatorName("");
				} else {
					String coordinatorDetailString = "";
					List fullUserName = customerFileManager.findUserFullName(customerFile.getCoordinator());
					if(null != fullUserName){
						if(!fullUserName.isEmpty() && fullUserName.get(0)!=null){
							coordinatorDetailString = fullUserName.get(0).toString();
						}
					}
					customerFile.setCoordinatorName(coordinatorDetailString);
				}
			}
			 
				if (customerFile.getEstimator() == null) {
					customerFile.setEstimator("");
					customerFile.setEstimatorName("");
				} 
				else if (customerFile.getEstimator().equals("")) {
					customerFile.setEstimatorName(customerFile.getEstimator());
				}
				else {
					String estimatorDetailString = "";
					List fullUserName = customerFileManager.findUserFullName(customerFile.getEstimator());
					if(null != fullUserName){
						if(!fullUserName.isEmpty() && fullUserName.get(0)!=null){
							estimatorDetailString = fullUserName.get(0).toString();
						}
					}
					customerFile.setEstimatorName(estimatorDetailString);
				}
			

			if (!(customerFile.getSurvey() == null)) {
				surveyDate = Calendar.getInstance();
				surveyDate.setTime(customerFile.getSurvey());
				surveyDate.set(Calendar.HOUR, Integer.parseInt((customerFile.getSurveyTime()).substring(0, (customerFile.getSurveyTime()).indexOf(":"))));
				surveyDate.set(Calendar.MINUTE, Integer.parseInt((customerFile.getSurveyTime()).substring((customerFile.getSurveyTime()).indexOf(":") + 1, (customerFile
						.getSurveyTime()).length())));
				Date surveyStartTime = surveyDate.getTime();
				customerFile.setSurvey(surveyStartTime);
			}
			if(customerFile.getComptetive()!=null)
			{
				if(customerFile.getComptetive().equalsIgnoreCase("Y"))
				{
					if((customerFile.getQuoteAcceptenceDate()!=null)){
						if(!customerFile.getQuoteAcceptenceDate().toString().equalsIgnoreCase(""))
						{
								customerFile.setSalesStatus("Booked");
						}
					}		
					
				}
			}
			boolean fname = (customerFile.getFirstName() == null || customerFile.getFirstName().trim().equals(""));
			boolean eName = (customerFile.getEmail()== null || customerFile.getEmail().equalsIgnoreCase(""));
			boolean coordName = (customerFile.getCoordinator() == null || customerFile.getCoordinator().equals(""));
			boolean customercheck = ((customerFile.getPortalIdActive()!=null && customerFile.getPortalIdActive()) == (true));
			if (customerFile.getCustomerPortalId()!=null && customerFile.getCustomerPortalId().equalsIgnoreCase("") && customercheck && (customerFile.getEmailSentDate()==null || customerFile.getEmailSentDate().equalsIgnoreCase(""))) {
					stateshitFlag="11";
				  if (!fname || !eName) {
					if(!coordName){
						 if((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase(""))&& (customerFile.getLastName()!=null)&&(!customerFile.getLastName().trim().equalsIgnoreCase(""))){
							  oldPortalId = customerFile.getFirstName().substring(0, 1).toLowerCase().trim() + customerFile.getLastName().toLowerCase().trim();	  
						  }else{
							  if((customerFile.getEmail()!=null)&&(!customerFile.getEmail().trim().equalsIgnoreCase(""))){
								  oldPortalId =customerFile.getEmail().substring(0,customerFile.getEmail().indexOf("@")).toLowerCase().trim();
							  }
						  }
					if((oldPortalId!=null)&&(!oldPortalId.equalsIgnoreCase(""))){
					oldPortalId = oldPortalId.replace("'", "");
					oldPortalId = oldPortalId.replaceAll(" ", "");
					
					newPortalId = customerFileManager.findPortalId(oldPortalId);
					if(newPortalId!=null && !newPortalId.isEmpty() && newPortalId.get(0)!=null)
					     portalIdCount = newPortalId.get(0).toString();
					
					if (isNew || customerFile.getCustomerPortalId().equals("")) {
						if (portalIdCount.equals("0")) {
							portalIdCount = "";
						}
						portalId = oldPortalId + portalIdCount;
						boolean userExists = customerFileManager.findUser(portalId);
						
						if (userExists) {
							if (portalIdCount.equals("")) {
								portalIdCount = "0";
							}
							portalIdCount = (new Long(Long.parseLong(portalIdCount) + 1)).toString();
							portalId = oldPortalId + portalIdCount;
						}
					
					poratalIdCountList = customerFileManager.findCustomerPortalListDetails(oldPortalId);
					if(!poratalIdCountList.isEmpty()){
						  Iterator it= poratalIdCountList.iterator();
							while(it.hasNext()){
								Object  row =(Object)it.next();
								
								if(row.equals(portalId)){
									portalIdCount = (new Long(Long.parseLong(portalIdCount) + 1)).toString();
									userPortalId = oldPortalId + portalIdCount;
									portalId=userPortalId;
								}
							}
							
					}
					customerFile.setCustomerPortalId(portalId.replaceAll(" ", ""));

						/** Save cportal Id as Username in app_user table**/
						 
							user.setUsername(portalId.replaceAll(" ", ""));

							Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);

							if ("true".equals(getRequest().getParameter("encryptPass")) && (encrypt != null && encrypt)) {
								String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);

								if (algorithm == null) { 
									// should only happen for test case
									log.debug("assuming testcase, setting algorithm to 'SHA'");
									algorithm = "SHA";
								}

								user.setPassword(StringUtil.encodePassword(portalId, algorithm));
								user.setConfirmPassword(user.getPassword());
							}
							Date compareWithDate=null;
							if(company.getUserPasswordExpiryDuration()!=null){
								compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getUserPasswordExpiryDuration().toString()));	
							}else{
								compareWithDate=DateUtils.addDays(getCurrentDate(), (60));
							} 
							user.setCorpID(sessionCorpID);
							user.setFirstName(customerFile.getFirstName());
							user.setLastName(customerFile.getLastName());
							user.setCreatedOn(new Date());
							user.setUpdatedOn(new Date());
							user.setUpdatedBy(getRequest().getRemoteUser());
							user.setCreatedBy(getRequest().getRemoteUser());
							user.setEmail(customerFile.getEmail());
							user.setPhoneNumber(customerFile.getOriginHomePhone());
							user.setFaxNumber(customerFile.getOriginFax());
							user.setWebsite("www.trilasoft.com");
							user.setAccountExpired(false);
							user.setAccountLocked(false);
							user.setCredentialsExpired(false);
							user.setEnabled(true);
							user.setUserType("CUSTOMER");
							user.setPasswordHintQues("");
							user.setPasswordReset(true);
							user.setPwdexpiryDate(compareWithDate);
							user.setSupervisor("");
							Authentication auth112 = SecurityContextHolder.getContext().getAuthentication();
					        User usertemp2 = (User)auth112.getPrincipal();
					        //Company company112= companyManager.findByCorpID(usertemp2.getCorpID()).get(0);
						if(company.getAccessRedSkyCportalapp()!=null && (company.getAccessRedSkyCportalapp())){
							user.setMobile_access(true);
							user.setMacid("");
						}else{
							user.setMacid("ACCOUNT_DISABLED");
							user.setMobile_access(false);	
						}
							Address address = new Address();
							address.setAddress(customerFile.getOriginAddress1());
							address.setCity(customerFile.getOriginCity());
							if(customerFile.getOriginZip().equalsIgnoreCase("")){
								address.setPostalCode("99999");
							}else{
								address.setPostalCode(customerFile.getOriginZip());
							}
							
							address.setProvince(customerFile.getOriginState());
							if(!customerFile.getOriginCountryCode().equalsIgnoreCase(""))
							{
								address.setCountry(customerFile.getOriginCountryCode());
							}else
							{
								address.setCountry(customerFile.getOriginCountryCode());
							} 
							user.setAddress(address);
							user.addRole(roleManager.getRole("ROLE_CUSTOMER"));
							try{
								user.setAlias(customerFile.getFirstName().substring(0, 1).toUpperCase() + " " + customerFile.getLastName().toUpperCase());
							}catch(Exception e){
								logger.error("Exception Occour: "+ e.getStackTrace()[0]);
								user.setAlias(customerFile.getEmail().substring(0,customerFile.getEmail().indexOf("@")).toUpperCase());
							}
							user.setWorkStartTime("00:00");
							user.setWorkEndTime("00:00");
							userManager.saveUser(user);

							/** Save Username in app_user table */ 
					}
					else{
						cPortalId=customerFile.getCustomerPortalId();
						customerFileManager.updateEmailInUser(cPortalId, sessionCorpID, customerFile.getEmail());
					}
					}else{
						errorMessage("Please enter First With Last Name / Email to generate Customer Portal ID.");
						return SUCCESS;
					}
				}
				 else{
					 errorMessage("Please select the Coordinator to generate Customer Portal ID.");
						return SUCCESS;
				     }
				 }
				else {
					errorMessage("Please enter the first name.");
					return SUCCESS;
				}
			}
			//customerFileManager.setFnameLname(customerFile.getSequenceNumber(), customerFile.getFirstName(), customerFile.getLastName());
			//customerFileManager.setPrefix(customerFile.getSequenceNumber(), customerFile.getPrefix());
			//customerFileManager.setFirstName(customerFile.getSequenceNumber(), customerFile.getFirstName());
			if (!isNew) {
			CustomerFile customerFileTemp = customerFileManager.get(customerFile.getId());
			//customerBillings=customerFileTemp.getAgentQuotesbillings();
			customerServiceOrders = customerFileTemp.getServiceOrders();
			/*if (customerBillings != null) { 
				Iterator it = customerBillings.iterator();
				while (it.hasNext()) { 
					billings = (Billing) it.next();
					billings.setBillToCode(customerFile.getBillToCode());
			    	billings.setBillToName(customerFile.getBillToName());
			    	if(billings.getContract() ==null || billings.getContract().toString().trim().equals("") ){
			    	billings.setContract(customerFile.getContract());	
			    	} 
			    	 
					}
			}*/
			if(customerServiceOrders!= null){
				Iterator it = customerServiceOrders.iterator();
				while (it.hasNext()) { 
					serviceOrder = (ServiceOrder) it.next();
					serviceOrder.setLastName(customerFile.getLastName());	
					serviceOrder.setPrefix(customerFile.getPrefix());
					serviceOrder.setFirstName(customerFile.getFirstName());
					serviceOrder.setBillToCode(customerFile.getBillToCode());
					serviceOrder.setBillToName(customerFile.getBillToName());
					serviceOrder.setSocialSecurityNumber(customerFile.getSocialSecurityNumber());
					billings=billingManager.get(serviceOrder.getId());
					billings.setBillToCode(customerFile.getBillToCode());
			    	billings.setBillToName(customerFile.getBillToName());
			    	if(billings.getContract() ==null || billings.getContract().toString().trim().equals("") ){
			    	billings.setContract(customerFile.getContract());	
			    	} 
					 
				}
			}
			}
			//# 8730 - To save the correct Sales Status while generating the QF in which currently it is saving as Blank Start
			if(isNew && (customerFile.getSalesStatus()==null ||customerFile.getSalesStatus().equals(""))){
				customerFile.setSalesStatus("Pending");
			}else if (customerFile.getQuotationStatus()!=null && (customerFile.getSalesStatus()==null ||customerFile.getSalesStatus().equals("")) && ((customerFile.getQuotationStatus().equalsIgnoreCase("Rejected"))||(customerFile.getQuotationStatus().equalsIgnoreCase("Cancelled")))) {
				customerFile.setSalesStatus("LostSale");
			}else{
				
			}
			//# 8730 - To save the correct Sales Status while generating the QF in which currently it is saving as Blank End
			
			
			String langTemp="";
			if(checkGermanForCustomerSave!=null && checkGermanForCustomerSave){
				if(langTemp.equalsIgnoreCase("")){
					langTemp="GERMAN";
				}else{
					langTemp=langTemp+",GERMAN";
				}
			}
			if(checkEnglishForCustomerSave!=null && checkEnglishForCustomerSave){
				if(langTemp.equalsIgnoreCase("")){
					langTemp="ENGLISH";
				}else{
					langTemp=langTemp+",ENGLISH";
				}			
			}
			if(!langTemp.equalsIgnoreCase("")){
				customerFile.setCportalEmailLanguage(langTemp);
			}
			langTemp="";
			if(checkEnglish!=null && checkEnglish){
				if(langTemp.equalsIgnoreCase("")){
					langTemp="ENGLISH";
				}else{
					langTemp=langTemp+",ENGLISH";
				}
			}
			if(checkDutch!=null && checkDutch){
				if(langTemp.equalsIgnoreCase("")){
					langTemp="DUTCH";
				}else{
					langTemp=langTemp+",DUTCH";
				}			
			}
			if(checkGerman!=null && checkGerman){
				if(langTemp.equalsIgnoreCase("")){
					langTemp="GERMAN";
				}else{
					langTemp=langTemp+",GERMAN";
				}			
			}
			
			if(!langTemp.equalsIgnoreCase("")){
				customerFile.setSurveyEmailLanguage(langTemp);
			}
			customerFile=customerFileManager.save(customerFile);
			//customerFileManager.updateServiceOrderBillToCode(customerFile.getBillToCode(), customerFile.getBillToName(), customerFile.getSequenceNumber(), sessionCorpID);
			//CustomerFile customerFileTemp = customerFileManager.get(customerFile.getId());
			//customerBillings=customerFileTemp.getAgentQuotesbillings();
			//customerServiceOrders = customerFileTemp.getServiceOrders();
			//customerFileManager.updateBillingBillToCode(customerFile.getBillToCode(), customerFile.getBillToName(), customerFile.getSequenceNumber(), sessionCorpID);
			/*try{
				String tempContract=customerFile.getContract();
				String tempSequenceNumber=customerFile.getSequenceNumber();
				if((tempContract!=null )&& (!(tempContract.equals("")) )){
					billingManager.updateContract(tempContract,tempSequenceNumber); 
				}
			}catch(Exception e){
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}*/
			
			
			hitFlag = "1";
//		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
				//validateFormNav = "NOTOK";
			String key = (isNew) ? "quotation.added" : "quotation.updated";
			saveMessage(getText(key));
//	}

			//ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
			//dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
			//companyDivis = customerFileManager.findCompanyDivisionByBookAg(customerFile.getCorpID(),customerFile.getBookingAgentCode());
			//contracts = customerFileManager.findCustJobContract(customerFile.getBillToCode(),customerFile.getJob(),customerFile.getCreatedOn(),sessionCorpID,customerFile.getCompanyDivision(),customerFile.getBookingAgentCode());
			if(customerFile.getId() != null){
				adAddressesDetailsList= adAddressesDetailsManager.getAdAddressesDetailsList(customerFile.getId(),sessionCorpID);
			} 
			//customerFileManager.setSSNo(customerFile.getSequenceNumber(),customerFile.getSocialSecurityNumber());
			getNotesForIconChange();
			//getComboList(sessionCorpID, customerFile.getJob());
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\n\n\n\n\n\n Time taken to save QF: "+timeTaken+"\n\n");
			if (customercheck && (customerFile.getEmailSentDate()==null || customerFile.getEmailSentDate().equalsIgnoreCase(""))){
				if(sessionCorpID.equalsIgnoreCase("UTSI")){
					emailTypeFlag=true;
				}else{
					emailTypeFlag=false;
				}
				if(sessionCorpID.equalsIgnoreCase("VOER")){
					emailTypeVOERFlag=true;
				}else{
					emailTypeVOERFlag=false;
				}
				if(sessionCorpID.equalsIgnoreCase("BOUR")){
					emailTypeBOURFlag=true;
				}else{
					emailTypeBOURFlag=false;
				}
				if(checkGermanForCustomerSave!=null && checkGermanForCustomerSave){
					emailTypeINTMFlag=true;
				}else{
					emailTypeINTMFlag=false;
				}
				if(checkEnglishForCustomerSave!=null && checkEnglishForCustomerSave){
					emailTypeEnglishFlag=true;
				}else{
					emailTypeEnglishFlag=false;
				}			
			   if(userID==null ||userID.equalsIgnoreCase("")){
				   userID=customerFile.getCustomerPortalId();
				 }
			   try {
					//customerFile = customerFileManager.get(id);
					String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
				     StringBuffer sb=new StringBuffer();
				     Random r = new Random();
				     int te=0;
				     for(int i=1;i<=8;i++){
				         te=r.nextInt(62);
				         sb.append(str.charAt(te));
				     }
				     String website = "";
				     String link="";
				     String CFcoordinatorName="";
				     //sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
				     if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
						for (SystemDefault systemDefault : sysDefaultDetail) {
							nameOfCompany=systemDefault.getCompany();
							supportId=systemDefault.getEmail();
							website = systemDefault.getWebsite();
							 link = "<a href=\""+website+"\">Home Page</a>";
						}
					}
				   
				     if((emailTo==null || emailTo.equalsIgnoreCase("")) && (customerFile.getEmail()!=null && ! customerFile.getEmail().equalsIgnoreCase(""))){
				    	  emailTo=customerFile.getEmail(); 
				    	 if(customerFile.getEmail2()!=null && ! customerFile.getEmail2().equalsIgnoreCase("") ){
				    		 emailTo=emailTo+","+customerFile.getEmail2(); 
				    	 }
				     }else if((emailTo==null || emailTo.equalsIgnoreCase("")) && (customerFile.getEmail2()!=null && ! customerFile.getEmail2().equalsIgnoreCase(""))){
				    	 emailTo=customerFile.getEmail2();
				     }
				    String subject = "";
				    String msgText1 ="";
				    String urlBinding="";
				    if(emailTypeFlag!=null && emailTypeFlag==true){
				     List list = partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, customerFile.getBillToCode());	     
				     if(list!=null && (!(list.isEmpty())) && list.get(0)!=null){
				    	partnerPrivate = (PartnerPrivate) list.get(0);
				    	List partnerPublicId = partnerPublicManager.findPartnerId(partnerPrivate.getPartnerCode(), sessionCorpID);
				    	if(partnerPublicId!=null && !partnerPublicId.isEmpty() && partnerPublicId.get(0)!=null){
				    	partnerPublic = partnerPublicManager.get(Long.parseLong(partnerPublicId.get(0).toString()));
				    	if(partnerPrivate.getPortalDefaultAccess()!=null && partnerPrivate.getPortalDefaultAccess()==true && partnerPublic.getIsAccount()==true){
					    	msgText1 = "<br>Dear "+customerFile.getPrefix()+" "+customerFile.getLastName()+",";
					    	msgText1 = msgText1 + "<br><br>We are proud that "+partnerPrivate.getLastName()+" has entrusted Harmony Relocation B.V. with the organization of your removal. This e-mail is to acknowledge that we have received the official Household Move Authorization from your employer.";
					    	msgText1 = msgText1 + "<br><br>The professional handling of your removal needs to be arranged in good time. " +
					    	"It is therefore extremely important that you log in to the website mentioned below and fill out the origin and destination address details as soon and as detailed as possible. In case your destination address and city are not yet determined please enter a dummy address (e.g. the country"+"'"+"s capital)";
					    	if(company!=null){
					    		if(company.getCportalAccessCompanyDivisionLevel()!=null && company.getCportalAccessCompanyDivisionLevel()){
					    			List list1 = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
					    			if(list1!=null && !list1.isEmpty() &&  list1.get(0)!=null){
										CompanyDivision companyDivision  = (CompanyDivision)list1.get(0);
										urlBinding=companyDivision.getCportalBrandingURL();
										if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
											}else if(company.getCportalBrandingURL()!=null && !company.getCportalBrandingURL().equals("")){
												msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+company.getCportalBrandingURL()+"'>"+company.getCportalBrandingURL()+"</a>";
											}else{
												msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
											}
									}
									}else{
										urlBinding=company.getCportalBrandingURL();
										if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
											}else{
												msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";	
											}
									}
					    			}
					    	msgText1 = msgText1 + "<br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString();
							msgText1 = msgText1 + "<br>Your File Number : "+customerFile.getSequenceNumber();
							msgText1 = msgText1 + "<br><br>At the time of first log in, you will be prompted to change your password.";
							msgText1 = msgText1 + "<br>If you encounter any problems with entering the data in the Customer Portal, please send us your full name, telephone number(s), origin and destination address details by e-mail to "+supportId+". As soon as your data have been received by Harmony Relocation B.V., we will further initiate the removal process.";
					    	msgText1 = msgText1 + "<br><br>We have developed a web site where you can track the status of your shipment, and relevant information and documents that we publish on behalf of "+partnerPrivate.getLastName()+".";
					    	if(company!=null && company.getAccessRedSkyCportalapp()!=null && company.getAccessRedSkyCportalapp()){
					    		msgText1 = msgText1 + "<br><br>The MovePulse mobile App is now available!  Get instant access to information pertaining to your shipment, at your fingertips. To download and install the mobile interface -";
								msgText1 = msgText1	+ "<br><br><b>MovePulse for iPhone</b> Click here ";
								msgText1 = msgText1 + "<a href='https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8'>https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8</a>";
								msgText1 = msgText1	+ "<br><br><b>MovePulse for Android</b> Click here ";
								msgText1 = msgText1 + "<a href='https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en'>https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en</a>";
						    }
					    	msgText1 = msgText1 +"<br><br>With best regards,";
					    	msgText1 = msgText1 + "<br>We wish you a smooth transfer.";
							msgText1 = msgText1 + "<br><br>Kind Regards,";
							msgText1 = msgText1 + "<br><br>Harmony Relocation B.V.";
							msgText1 = msgText1 + "<br>"+supportId+"";
							msgText1 = msgText1 + "<br><br>Harmony Relocation B.V. is a globally operating multinational company specialized in household goods removals and expatriate relocation services. Should you like to know more about us, please consult our "+link+".";
							subject = "Your upcoming household goods removal with Harmony Relocation B.V.";
					    	emailAdd = ""+supportId+"";
				    	}else if(partnerPrivate.getPortalDefaultAccess()!=null && partnerPrivate.getPortalDefaultAccess()==false && partnerPublic.getIsAccount()==true){
					    	msgText1 = "<br>Dear "+customerFile.getPrefix()+" "+customerFile.getLastName()+",";
					    	msgText1 = msgText1 + "<br><br>We are proud that "+partnerPrivate.getLastName()+" has entrusted Harmony Relocation B.V. with the organization of your removal. This e-mail is to acknowledge that we have received the official Household Move Authorization from your employer.";
					    	if(company!=null){
					    		if(company.getCportalAccessCompanyDivisionLevel()!=null && company.getCportalAccessCompanyDivisionLevel()){
					    			List list1 = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
					    			if(list1!=null && !list1.isEmpty() &&  list1.get(0)!=null){
										CompanyDivision companyDivision  = (CompanyDivision)list1.get(0);
										urlBinding=companyDivision.getCportalBrandingURL();
										if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
											}else if(company.getCportalBrandingURL()!=null && !company.getCportalBrandingURL().equals("")){
												msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+company.getCportalBrandingURL()+"'>"+company.getCportalBrandingURL()+"</a>";
											}else{
												msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
											}
									}
									}else{
										urlBinding=company.getCportalBrandingURL();
										if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
											}else{
												msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";	
											}
									}
					    			}
					    	msgText1 = msgText1 + "<br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString();
							msgText1 = msgText1 + "<br>Your File Number : "+customerFile.getSequenceNumber();
							msgText1 = msgText1 + "<br><br>At the time of first log in, you will be prompted to change your password.";
							msgText1 = msgText1 + "<br>If you have any questions regarding the Customer Portal or have problems logging in, please contact "+supportId+".";
					    	msgText1 = msgText1 + "<br><br>We have developed a web site where you can track the status of your shipment, and relevant information and documents that we publish on behalf of "+partnerPrivate.getLastName()+".";
					    	if(company!=null && company.getAccessRedSkyCportalapp()!=null && company.getAccessRedSkyCportalapp()){
					    		msgText1 = msgText1 + "<br><br>The MovePulse mobile App is now available!  Get instant access to information pertaining to your shipment, at your fingertips. To download and install the mobile interface -";
								msgText1 = msgText1	+ "<br><br><b>MovePulse for iPhone</b> Click here ";
								msgText1 = msgText1 + "<a href='https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8'>https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8</a>";
								msgText1 = msgText1	+ "<br><br><b>MovePulse for Android</b> Click here ";
								msgText1 = msgText1 + "<a href='https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en'>https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en</a>";
						    }
					    	msgText1 = msgText1 +"<br><br>With best regards,";
					    	msgText1 = msgText1 + "<br>We wish you a smooth transfer.";
							msgText1 = msgText1 + "<br><br>Kind Regards,";
							msgText1 = msgText1 + "<br><br>Harmony Relocation B.V.";
							msgText1 = msgText1 + "<br>"+supportId+"";
							msgText1 = msgText1 + "<br><br>Harmony Relocation B.V. is a globally operating multinational company specialized in household goods removals and expatriate relocation services. Should you like to know more about us, please consult our "+link+".";
							subject = "Your upcoming household goods removal with Harmony Relocation B.V.";
					    	emailAdd = ""+supportId+"";
				        }else{
							msgText1 = "<br>Thank you for entrusting your relocation to "+nameOfCompany+". " + " The web site address, user Log In and Password, to track the status of the services through Customer portal, are mentioned below:" ;
							if(company!=null){
					    		if(company.getCportalAccessCompanyDivisionLevel()!=null && company.getCportalAccessCompanyDivisionLevel()){
					    			List list1 = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
									if(list1!=null && !list1.isEmpty() &&  list1.get(0)!=null){
										CompanyDivision companyDivision  = (CompanyDivision)list1.get(0);
										urlBinding=companyDivision.getCportalBrandingURL();
										if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
											}else if(company.getCportalBrandingURL()!=null && !company.getCportalBrandingURL().equals("")){
												msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+company.getCportalBrandingURL()+"'>"+company.getCportalBrandingURL()+"</a>";
											}else{
												msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
											}
									}
									}else{
										urlBinding=company.getCportalBrandingURL();
										if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
											}else{
												msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";	
											}
									}
					    			}
							msgText1 = msgText1 + "<br><br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString() + "<br><br>";
							msgText1 = msgText1	+ "At the time of First log in, you will be prompted to change your password to a more secure and robust one.<br><br>If you have any questions regarding the Customer Portal or have problems logging in, please contact "+supportId+".";
							if(company!=null && company.getAccessRedSkyCportalapp()!=null && company.getAccessRedSkyCportalapp()){
								msgText1 = msgText1 + "<br><br>The MovePulse mobile App is now available!  Get instant access to information pertaining to your shipment, at your fingertips. To download and install the mobile interface -";
								msgText1 = msgText1	+ "<br><br><b>MovePulse for iPhone</b> Click here ";
								msgText1 = msgText1 + "<a href='https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8'>https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8</a>";
								msgText1 = msgText1	+ "<br><br><b>MovePulse for Android</b> Click here ";
								msgText1 = msgText1 + "<a href='https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en'>https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en</a>";
						    }
							msgText1 = msgText1 +"<br><br>Best regards,";
							
							if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
				    			String coordinatorDetailString = "";
								List fullUserName = customerFileManager.findUserFullName(customerFile.getPersonPricing());
									if((fullUserName!=null)&&(!fullUserName.isEmpty())&&(fullUserName.get(0)!=null)){
										coordinatorDetailString = fullUserName.get(0).toString();
										msgText1 = msgText1 + "<br><br>" + coordinatorDetailString;
									}
				    		}else if(customerFile.getCoordinatorName()!=null){
								msgText1 = msgText1 + "<br><br>" + customerFile.getCoordinatorName();
							}
							if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
								coordSignature = customerFileManager.findCoordSignature(customerFile.getPersonPricing());
								String coordinatorSignature = new String();
								if(coordSignature!=null && !coordSignature.isEmpty()){
									coordinatorSignature=coordSignature.get(0).toString();
									msgText1 = msgText1 + "<br>" + coordinatorSignature;
								}
								coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getPersonPricing());
								if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
									emailAdd = coordEmail.get(0).toString().trim();
								}
							
							}else if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equals("")){
								coordSignature = customerFileManager.findCoordSignature(customerFile.getCoordinator());
								String coordinatorSignature = new String();
								if(coordSignature!=null && !coordSignature.isEmpty()){
									coordinatorSignature=coordSignature.get(0).toString();
									msgText1 = msgText1 + "<br>" + coordinatorSignature;
								}
								coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
								if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
									emailAdd = coordEmail.get(0).toString().trim();
								}
							}else{
								emailAdd = "support@redskymobility.com";	
							}
							subject = "CPortal User Information";
					    } 
					    msgText1 = "<html><table><font face='Tahoma' size='2'>"+msgText1+"</font></table></html>";
						String smtpHost = "localhost";
						String from = emailAdd;
						Properties props = System.getProperties();
						props.put("mail.transport.protocol", "smtp");
						props.put("mail.smtp.host", smtpHost);
						Session session = Session.getInstance(props, null);
						session.setDebug(true);
						MimeMessage msg = new MimeMessage(session);
						msg.setFrom(new InternetAddress(from));
						String tempRecipient="";
						String tempRecipientArr[]=emailTo.split(",");
						for(String str1:tempRecipientArr){
							if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
								if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
								tempRecipient += str1;
							}
						}
						emailTo=tempRecipient;
						
						emailSetupManager.globalEmailSetupProcess(from, emailTo, "", "", "", msgText1, subject, sessionCorpID,"",customerFile.getSequenceNumber(),"");
						mailStatus = "sent"; 			    	
				      } 
				     }
				    }
				    if(emailTypeVOERFlag!=null && emailTypeVOERFlag==true){
				    	msgText1="<br>As a Voerman International customer, you are welcome to use our exclusive Customer Portal. This web portal gives you access to all the documentation";
				    	msgText1=msgText1 +"<br>related to your (forthcoming) move. It also allows you to complete information, check and obtain documents and at a later stage also track your move and";
				    	msgText1=msgText1 +"<br>supply us with necessary information";
				    	if(company!=null){
				    		if(company.getCportalAccessCompanyDivisionLevel()!=null && company.getCportalAccessCompanyDivisionLevel()){
				    			List list1 = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
				    			if(list1!=null && !list1.isEmpty() &&  list1.get(0)!=null){
									CompanyDivision companyDivision  = (CompanyDivision)list1.get(0);
									urlBinding=companyDivision.getCportalBrandingURL();
									if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
										msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
										}else if(company.getCportalBrandingURL()!=null && !company.getCportalBrandingURL().equals("")){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+company.getCportalBrandingURL()+"'>"+company.getCportalBrandingURL()+"</a>";
										}else{
											msgText1=msgText1 +"<br><br><br>Please visit: <a href=\"http://voer.skyrelo.com\">http://voer.skyrelo.com</a>";
										}
								}
								}else{
									urlBinding=company.getCportalBrandingURL();
									if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
										msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
										}else{
											msgText1=msgText1 +"<br><br><br>Please visit: <a href=\"http://voer.skyrelo.com\">http://voer.skyrelo.com</a>";
										}
								}
				    			}
				    	msgText1=msgText1 +"<br><br>Username:&nbsp;" + userID + " ";
				    	msgText1=msgText1 +"<br>Password:&nbsp;" + sb.toString() + "&nbsp;(The first time you log in, you will be prompted to change your password)";
				    	msgText1=msgText1 +"<br><br><br>If you have any questions regarding the customer portal or encounter any";
				    	msgText1=msgText1 +"<br>difficulties logging in, please contact: mail@voerman.com.";
				    	msgText1=msgText1 +"<br><br>We look forward to facilitating your move and providing excellent service.";
				    	if(company!=null && company.getAccessRedSkyCportalapp()!=null && company.getAccessRedSkyCportalapp()){
				    		msgText1 = msgText1 + "<br><br>The MovePulse mobile App is now available!  Get instant access to information pertaining to your shipment, at your fingertips. To download and install the mobile interface -";
							msgText1 = msgText1	+ "<br><br><b>MovePulse for iPhone</b> Click here ";
							msgText1 = msgText1 + "<a href='https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8'>https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8</a>";
							msgText1 = msgText1	+ "<br><br><b>MovePulse for Android</b> Click here ";
							msgText1 = msgText1 + "<a href='https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en'>https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en</a>";	
					    }
				    	msgText1 = msgText1 +"<br><br>Best regards,";
				    	/*if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
			    			String coordinatorDetailString = "";
							List fullUserName = customerFileManager.findUserFullName(customerFile.getPersonPricing());
								if((fullUserName!=null)&&(!fullUserName.isEmpty())&&(fullUserName.get(0)!=null)){
									coordinatorDetailString = fullUserName.get(0).toString();
									msgText1 = msgText1 + "<br><br>" + coordinatorDetailString;
								}
			    		}else */if(customerFile.getCoordinatorName()!=null){
							msgText1 = msgText1 + "<br><br>" + customerFile.getCoordinatorName();
							CFcoordinatorName=customerFile.getCoordinatorName();
						}
						
				    	/*if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
							coordSignature = customerFileManager.findCoordSignature(customerFile.getPersonPricing());
							String coordinatorSignature = new String();
							if(coordSignature!=null && !coordSignature.isEmpty()){
								coordinatorSignature=coordSignature.get(0).toString();
								msgText1 = msgText1 + "<br>" + coordinatorSignature;
							}
							coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getPersonPricing());
							if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
								emailAdd = coordEmail.get(0).toString().trim();
							}
						
						}else*/ if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equals("")){
							coordSignature = customerFileManager.findCoordSignature(customerFile.getCoordinator());
							String coordinatorSignature = new String();
							if(coordSignature!=null && !coordSignature.isEmpty() && coordSignature.get(0)!=null){
								coordinatorSignature=coordSignature.get(0).toString();
								msgText1 = msgText1 + "<br>" + coordinatorSignature;
							}
							coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
							if ((coordEmail != null) && (!coordEmail.isEmpty())&& (coordEmail.get(0)!=null)) { 
								emailAdd = coordEmail.get(0).toString().trim();
							}
						}else{
							emailAdd = "support@redskymobility.com";	
						}
						subject = "Customer Portal User Information File# "+customerFile.getSequenceNumber();
					    msgText1 = "<html><table><font face='Tahoma' size='2'>"+msgText1+"</font></table></html>";
						String smtpHost = "localhost";
						String from = emailAdd;
						Properties props = System.getProperties();
						props.put("mail.transport.protocol", "smtp");
						props.put("mail.smtp.host", smtpHost);
						Session session = Session.getInstance(props, null);
						session.setDebug(true);
						MimeMessage msg = new MimeMessage(session);
						msg.setFrom(new InternetAddress(from));
						String tempRecipient="";
						String tempRecipientArr[]=emailTo.split(",");
						for(String str1:tempRecipientArr){
							if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
								if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
								tempRecipient += str1;
							}
						}
						emailTo=tempRecipient;
						
						emailSetupManager.globalEmailSetupProcess(from, emailTo, "", "", "", msgText1, subject, sessionCorpID,"",customerFile.getSequenceNumber(),"");
						mailStatus = "sent"; 			    
				    } 
				    if(emailTypeBOURFlag!=null && emailTypeBOURFlag==true){

				    	msgText1="<br>At Bournes Moves we are pleased to assist you with your upcoming relocation and are proud to provide";
				    	msgText1 = msgText1 +"<br>you with access to our industry leading online Customer Portal. The web site address, user Log In";
				    	msgText1 = msgText1 +"<br>and Password to your unique Customer Portal are provided below.";
				    	msgText1 = msgText1 +"<br><br>The web based customer portal is available to you 24 hours a day, 7 days a week from anywhere that ";
				    	msgText1 = msgText1 +"<br>you have an internet connection and can be accessed via desktop, tablet and mobile devices. In addition";
				    	msgText1 = msgText1 +"<br>to liaising directly with your move coordinator your portal allows you to: ";
				    	msgText1 = msgText1 +"<br><br>&bull; Instantly access your move information electronically all in one place ";
				    	msgText1 = msgText1 +"<br>&bull; Download documents, forms, move guides, FAQ's and other useful information regarding your ";
				    	msgText1 = msgText1 +"<br>&nbsp; move wherever and whenever you need them";
				    	msgText1 = msgText1 +"<br>&bull; Upload completed documents and forms securely whenever its convenient to you";
				    	msgText1 = msgText1 +"<br>&bull; Track your relocation and view the most up to date information on its status throughout your move";
				    	if(company!=null){
				    		if(company.getCportalAccessCompanyDivisionLevel()!=null && company.getCportalAccessCompanyDivisionLevel()){
				    			List list1 = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
								if(list1!=null && !list1.isEmpty() &&  list1.get(0)!=null){
									CompanyDivision companyDivision  = (CompanyDivision)list1.get(0);
									urlBinding=companyDivision.getCportalBrandingURL();
									if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
										msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
										}else if(company.getCportalBrandingURL()!=null && !company.getCportalBrandingURL().equals("")){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+company.getCportalBrandingURL()+"'>"+company.getCportalBrandingURL()+"</a>";
										}else{
											msgText1 = msgText1 +"<br><br>The web site address is: <a href=\"http://bour.skyrelo.com\">http://bour.skyrelo.com</a>";
										}
								}
								}else{
									urlBinding=company.getCportalBrandingURL();
									if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
										msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
										}else{
											msgText1 = msgText1 +"<br><br>The web site address is: <a href=\"http://bour.skyrelo.com\">http://bour.skyrelo.com</a>";
										}
								}
				    			}
				    	msgText1 = msgText1 + "<br><br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString() + "<br>";
				    	msgText1 = msgText1 +"<br>At the time of first log in you will be prompted to change your password to a more secure and memorable one.";
				    	/*msgText1 = msgText1 +"<br><br>You will then be directed to the profile page where you can check and amend if necessary any of your contact details.";*/
				    	msgText1 = msgText1 +"<br>You can then navigate through the portal to view and download customised information and documentation regarding your upcoming move.";
				    	
				    	msgText1 = msgText1 +"<br>If you have any questions regarding the Customer Portal or have problems logging in, please contact "+supportId+"";
				    	if(company!=null && company.getAccessRedSkyCportalapp()!=null && company.getAccessRedSkyCportalapp()){
				    		msgText1 = msgText1 + "<br><br>The MovePulse mobile App is now available!  Get instant access to information pertaining to your shipment, at your fingertips. To download and install the mobile interface -";
							msgText1 = msgText1	+ "<br><br><b>MovePulse for iPhone</b> Click here ";
							msgText1 = msgText1 + "<a href='https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8'>https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8</a>";
							msgText1 = msgText1	+ "<br><br><b>MovePulse for Android</b> Click here ";
							msgText1 = msgText1 + "<a href='https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en'>https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en</a>";	
					    }
				    	msgText1 = msgText1 +" <br><br>Best regards,";
				    	
				    	if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
			    			String coordinatorDetailString = "";
							List fullUserName = customerFileManager.findUserFullName(customerFile.getPersonPricing());
								if((fullUserName!=null)&&(!fullUserName.isEmpty())&&(fullUserName.get(0)!=null)){
									coordinatorDetailString = fullUserName.get(0).toString();
									msgText1 = msgText1 + "<br><br>" + coordinatorDetailString;
								}
			    		}else if(customerFile.getCoordinatorName()!=null){
							msgText1 = msgText1 + "<br><br>" + customerFile.getCoordinatorName();
						}
				    	
				    	if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
							coordSignature = customerFileManager.findCoordSignature(customerFile.getPersonPricing());
							String coordinatorSignature = new String();
							if(coordSignature!=null && !coordSignature.isEmpty()){
								coordinatorSignature=coordSignature.get(0).toString();
								msgText1 = msgText1 + "<br>" + coordinatorSignature;
							}
							coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getPersonPricing());
							if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
								emailAdd = coordEmail.get(0).toString().trim();
							}
						
						}else if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equals("")){
							coordSignature = customerFileManager.findCoordSignature(customerFile.getCoordinator());
							String coordinatorSignature = new String();
							if(coordSignature!=null && !coordSignature.isEmpty() && coordSignature.get(0)!=null){
								coordinatorSignature=coordSignature.get(0).toString();
								msgText1 = msgText1 + "<br>" + coordinatorSignature;
							}
							coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
							if ((coordEmail != null) && (!coordEmail.isEmpty()) && coordEmail.get(0)!=null) { 
								emailAdd = coordEmail.get(0).toString().trim();
							}
						}else{
							emailAdd = "support@redskymobility.com";	
						}
				    	
						subject = "CPortal User Information";
					    msgText1 = "<html><table><font face='Tahoma' size='2'>"+msgText1+"</font></table></html>";
						String smtpHost = "localhost";
						String from = emailAdd;
						Properties props = System.getProperties();
						props.put("mail.transport.protocol", "smtp");
						props.put("mail.smtp.host", smtpHost);
						Session session = Session.getInstance(props, null);
						session.setDebug(true);
						MimeMessage msg = new MimeMessage(session);
						msg.setFrom(new InternetAddress(from));
						String tempRecipient="";
						String tempRecipientArr[]=emailTo.split(",");
						for(String str1:tempRecipientArr){
							if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
								if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
								tempRecipient += str1;
							}
						}
						emailTo=tempRecipient;
						
						emailSetupManager.globalEmailSetupProcess(from, emailTo, "", "", "", msgText1, subject, sessionCorpID,"",customerFile.getSequenceNumber(),"");
						mailStatus = "sent"; 					
				    } 
				    if(emailTypeINTMFlag!=null && emailTypeINTMFlag==true){
				    	msgText1 = "<br>Dear "+customerFile.getPrefix()+" "+customerFile.getLastName()+",";
						msgText1 = msgText1 + "<br>vielen Dank, dass Sie Ihren bevorstehenden Umzug der "+nameOfCompany+" anvertraut haben.<BR>" + "Den Status Ihrer Sendung k�nnen Sie direkt �ber unser Kundenportal selbst verfolgen.<BR><BR>Die n�tigen Log In Informationen sind:" ;
						if(company!=null){
				    		if(company.getCportalAccessCompanyDivisionLevel()!=null && company.getCportalAccessCompanyDivisionLevel()){
				    			List list1 = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
								if(list1!=null && !list1.isEmpty() &&  list1.get(0)!=null){
									CompanyDivision companyDivision  = (CompanyDivision)list1.get(0);
									urlBinding=companyDivision.getCportalBrandingURL();
									if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
										msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
										}else if(company.getCportalBrandingURL()!=null && !company.getCportalBrandingURL().equals("")){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+company.getCportalBrandingURL()+"'>"+company.getCportalBrandingURL()+"</a>";
										}else{
											msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
										}
								}
								}else{
									urlBinding=company.getCportalBrandingURL();
									if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
										msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
										}else{
											msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
										}
								}
				    			}
						msgText1 = msgText1 + "<br>Ihr Log In: " + userID + "<br>Ihr tempor�res Passwort: " + sb.toString() + "<br>";
						msgText1 = msgText1	+ "Bei der ersten Anmeldung werden Sie aufgefordert Ihr Passwort in ein sichereres zu �ndern!<br><br>Falls Sie Fragen zu unserem Kundenportal haben oder Probleme beim Login, setzen Sie sich bitte mit Ihrem Berater Herrn Carsten Just unter "+supportId+" in Verbindung.";
						if(company!=null && company.getAccessRedSkyCportalapp()!=null && company.getAccessRedSkyCportalapp()){
							msgText1 = msgText1 + "<br><br>Die MovePulse mobilen App ist jetzt verf�gbar!  Sofortiger Zugriff auf Informationen in Bezug auf Ihre Sendung, an Ihren Fingerspitzen. Downloaden und installieren Sie die mobile Schnittstelle -";
							msgText1 = msgText1	+ "<br><br><b>MovePulse f�r iPhone</b> Klicken Sie hier ";
							msgText1 = msgText1 + "<a href='https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8'>https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8</a>";
							msgText1 = msgText1	+ "<br><br><b>MovePulse f�r Android</b> Click here ";
							msgText1 = msgText1 + "<a href='https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en'>https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en</a>";
					    }
						msgText1 = msgText1 +"<br><br>Mit freundlichen Gr��en,"; 
						
						if(customerFile.getControlFlag().equalsIgnoreCase("Q")){
								if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
				    			String coordinatorDetailString = "";
								List fullUserName = customerFileManager.findUserFullName(customerFile.getPersonPricing());
									if((fullUserName!=null)&&(!fullUserName.isEmpty())&&(fullUserName.get(0)!=null)){
										coordinatorDetailString = fullUserName.get(0).toString();
										msgText1 = msgText1 + "<br><br>" + coordinatorDetailString;
									}
				    		}else if(customerFile.getCoordinatorName()!=null){
								msgText1 = msgText1 + "<br><br>" + customerFile.getCoordinatorName();
							}
							if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
								coordSignature = customerFileManager.findCoordSignature(customerFile.getPersonPricing());
								String coordinatorSignature = new String();
								if(coordSignature!=null && !coordSignature.isEmpty()){
									coordinatorSignature=coordSignature.get(0).toString();
									msgText1 = msgText1 + "<br>" + coordinatorSignature;
								}
								coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getPersonPricing());
								if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
									emailAdd = coordEmail.get(0).toString().trim();
								}
							
							}else if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equals("")){
								coordSignature = customerFileManager.findCoordSignature(customerFile.getCoordinator());
								String coordinatorSignature = new String();
								if(coordSignature!=null && !coordSignature.isEmpty()){
									coordinatorSignature=coordSignature.get(0).toString();
									msgText1 = msgText1 + "<br>" + coordinatorSignature;
								}
								coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
								if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
									emailAdd = coordEmail.get(0).toString().trim();
								}
							}else{
								emailAdd = "support@redskymobility.com";	
							}}else{
						if(customerFile.getCoordinatorName()!=null){
							msgText1 = msgText1 + "<br><br>" + customerFile.getCoordinatorName();
						}
						if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equals("")){
							coordSignature = customerFileManager.findCoordSignature(customerFile.getCoordinator());
							String coordinatorSignature = new String();
							if(coordSignature!=null && !coordSignature.isEmpty() && coordSignature.get(0)!=null){
								coordinatorSignature=coordSignature.get(0).toString();
								msgText1 = msgText1 + "<br>" + coordinatorSignature;
							}
							coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
							if ((coordEmail != null) && (!coordEmail.isEmpty())&& (coordEmail.get(0)!=null)) { 
								emailAdd = coordEmail.get(0).toString().trim();
							}
						}else{
							emailAdd = "support@redskymobility.com";	
						}
				    }
						subject = "CPortal Benutzer-Informationen";
					    msgText1 = "<html><table><font face='Tahoma' size='2'>"+msgText1+"</font></table></html>";
						String smtpHost = "localhost";
						String from = emailAdd;
						Properties props = System.getProperties();
						props.put("mail.transport.protocol", "smtp");
						props.put("mail.smtp.host", smtpHost);
						Session session = Session.getInstance(props, null);
						session.setDebug(true);
						MimeMessage msg = new MimeMessage(session);
						msg.setFrom(new InternetAddress(from));
						String tempRecipient="";
						String tempRecipientArr[]=emailTo.split(",");
						for(String str1:tempRecipientArr){
							if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
								if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
								tempRecipient += str1;
							}
						}
						emailTo=tempRecipient;
						
						emailSetupManager.globalEmailSetupProcess(from, emailTo, "", "", "", msgText1, subject, sessionCorpID,"",customerFile.getSequenceNumber(),"");
						mailStatus = "sent"; 					
				    }		    	
				    if(emailTypeEnglishFlag!=null && emailTypeEnglishFlag==true){

						msgText1 = "<br>Thank you for entrusting your relocation to "+nameOfCompany+". " + " The web site address, user Log In and Password, to track the status of the services through Customer portal, are mentioned below:" ;
						if(company!=null){
				    		if(company.getCportalAccessCompanyDivisionLevel()!=null && company.getCportalAccessCompanyDivisionLevel()){
				    			List list1 = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
								if(list1!=null && !list1.isEmpty() &&  list1.get(0)!=null){
									CompanyDivision companyDivision  = (CompanyDivision)list1.get(0);
									urlBinding=companyDivision.getCportalBrandingURL();
									if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
										msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
										}else if(company.getCportalBrandingURL()!=null && !company.getCportalBrandingURL().equals("")){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+company.getCportalBrandingURL()+"'>"+company.getCportalBrandingURL()+"</a>";
										}else{
											msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
										}
								}
								}else{
									urlBinding=company.getCportalBrandingURL();
									if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
										msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
										}else{
											msgText1 = msgText1 + "<br><br>The web site: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
										}
								}
				    			}
						msgText1 = msgText1 + "<br><br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString() + "<br><br>";
						msgText1 = msgText1	+ "At the time of First log in, you will be prompted to change your password to a more secure and robust one.<br><br>If you have any questions regarding the Customer Portal or have problems logging in, please contact "+supportId+".";
						if(company!=null && company.getAccessRedSkyCportalapp()!=null && company.getAccessRedSkyCportalapp()){
							msgText1 = msgText1 + "<br><br>The MovePulse mobile App is now available!  Get instant access to information pertaining to your shipment, at your fingertips. To download and install the mobile interface -";
							msgText1 = msgText1	+ "<br><br><b>MovePulse for iPhone</b> Click here ";
							msgText1 = msgText1 + "<a href='https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8'>https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8</a>";
							msgText1 = msgText1	+ "<br><br><b>MovePulse for Android</b> Click here ";
							msgText1 = msgText1 + "<a href='https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en'>https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en</a>";	
					    }
				    	msgText1 = msgText1 +" <br><br>Best regards,";
						if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
			    			String coordinatorDetailString = "";
							List fullUserName = customerFileManager.findUserFullName(customerFile.getPersonPricing());
								if((fullUserName!=null)&&(!fullUserName.isEmpty())&&(fullUserName.get(0)!=null)){
									coordinatorDetailString = fullUserName.get(0).toString();
									msgText1 = msgText1 + "<br><br>" + coordinatorDetailString;
								}
			    		}else if(customerFile.getCoordinatorName()!=null){
							msgText1 = msgText1 + "<br><br>" + customerFile.getCoordinatorName();
						}
						if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals(""))){
							coordSignature = customerFileManager.findCoordSignature(customerFile.getPersonPricing());
							String coordinatorSignature = new String();
							if(coordSignature!=null && !coordSignature.isEmpty()){
								coordinatorSignature=coordSignature.get(0).toString();
								msgText1 = msgText1 + "<br>" + coordinatorSignature;
							}
							coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getPersonPricing());
							if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
								emailAdd = coordEmail.get(0).toString().trim();
							}
						
						}else if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equals("")){
							coordSignature = customerFileManager.findCoordSignature(customerFile.getCoordinator());
							String coordinatorSignature = new String();
							if(coordSignature!=null && !coordSignature.isEmpty() && coordSignature.get(0)!=null){
								coordinatorSignature=coordSignature.get(0).toString();
								msgText1 = msgText1 + "<br>" + coordinatorSignature;
							}
							coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
							if ((coordEmail != null) && (!coordEmail.isEmpty()) && coordEmail.get(0)!=null) { 
								emailAdd = coordEmail.get(0).toString().trim();
							}
						}else{
							emailAdd = "support@redskymobility.com";	
						}
						subject = "CPortal User Information";
					    msgText1 = "<html><table><font face='Tahoma' size='2'>"+msgText1+"</font></table></html>";
						String smtpHost = "localhost";
						String from = emailAdd;
						Properties props = System.getProperties();
						props.put("mail.transport.protocol", "smtp");
						props.put("mail.smtp.host", smtpHost);
						Session session = Session.getInstance(props, null);
						session.setDebug(true);
						MimeMessage msg = new MimeMessage(session);
						msg.setFrom(new InternetAddress(from));
						String tempRecipient="";
						String tempRecipientArr[]=emailTo.split(",");
						for(String str1:tempRecipientArr){
							if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
								if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
								tempRecipient += str1;
							}
						}
						emailTo=tempRecipient;
						
						emailSetupManager.globalEmailSetupProcess(from, emailTo, "", "", "", msgText1, subject, sessionCorpID ,"",customerFile.getSequenceNumber(),"");
						mailStatus = "sent"; 			    			    
				    }
				    if((emailTypeFlag==null || emailTypeFlag==false)&&(emailTypeVOERFlag==null || emailTypeVOERFlag==false)&&(emailTypeBOURFlag==null || emailTypeBOURFlag==false)&&(emailTypeINTMFlag==null || emailTypeINTMFlag==false)&&(emailTypeEnglishFlag==null || emailTypeEnglishFlag==false)){
						
				    	  String coordinatorEmail=""; 
				    	  String companyName="";
				    	   company=companyManager.findByCorpID(sessionCorpID).get(0);
						   if(company.getCportalAccessCompanyDivisionLevel()!=null){
							cportalAccessCompanyDivision =company.getCportalAccessCompanyDivisionLevel();  
						   }
						   if(cportalAccessCompanyDivision){
								coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
								if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
									coordinatorEmail = coordEmail.get(0).toString().trim();
									emailAdd = coordEmail.get(0).toString().trim();
								}else{
									emailAdd = "support@redskymobility.com";
									coordinatorEmail=supportId;
								}
								List list = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
								if(list!=null && !list.isEmpty() &&  list.get(0)!=null){
									CompanyDivision companyDivision  = (CompanyDivision)list.get(0);
									companyName=companyDivision.getDescription();
								}
							    msgText1 = "<br>Thank you for entrusting your relocation to "+companyName+". " + " The web site address, user Log In and Password, to track the status of the services through Customer portal, are mentioned below:" ;
							    if(company!=null){
						    		if(company.getCportalAccessCompanyDivisionLevel()!=null && company.getCportalAccessCompanyDivisionLevel()){
						    			List list1 = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
										if(list1!=null && !list1.isEmpty() &&  list1.get(0)!=null){
											CompanyDivision companyDivision  = (CompanyDivision)list1.get(0);
											urlBinding=companyDivision.getCportalBrandingURL();
											if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
												msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
												}else if(company.getCportalBrandingURL()!=null && !company.getCportalBrandingURL().equals("")){
													msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+company.getCportalBrandingURL()+"'>"+company.getCportalBrandingURL()+"</a>";
												}else{
													msgText1 = msgText1 + "<br><br>The web site address is: <a href=\"http://"+customerFile.getCompanyDivision().toLowerCase()+"."+sessionCorpID.toLowerCase()+".skyrelo.com\">http://"+customerFile.getCompanyDivision().toLowerCase()+"."+sessionCorpID.toLowerCase()+".skyrelo.com</a>";											}
										}
										}else{
											urlBinding=company.getCportalBrandingURL();
											if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
												msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
												}else{
													msgText1 = msgText1 + "<br><br>The web site address is: <a href=\"http://"+customerFile.getCompanyDivision().toLowerCase()+"."+sessionCorpID.toLowerCase()+".skyrelo.com\">http://"+customerFile.getCompanyDivision().toLowerCase()+"."+sessionCorpID.toLowerCase()+".skyrelo.com</a>";											}
										}
						    			}
							    msgText1 = msgText1 + "<br><br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString() + "<br><br>";
								msgText1 = msgText1	+ "At the time of First log in, you will be prompted to change your password to a more secure and robust one.<br><br>If you have any questions regarding the Customer Portal or have problems logging in, please contact "+coordinatorEmail+".";
								if(sessionCorpID.equals("ICMG"))
								{
								msgText1 = msgText1 + "<br><br>Once you gain access to the Customer portal, then click on Tracking and then on the blue, under-lined order number.";
								}
								if(company!=null && company.getAccessRedSkyCportalapp()!=null && company.getAccessRedSkyCportalapp()){
									/*msgText1 = "<br><br>Thank you for entrusting your relocation to Security Storage Company of Washington." + " The web site address, user Log In and Password, to track the status of the services through Customer portal, are mentioned below:" ;
									msgText1 = msgText1 + "<br><br>The web site address is: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
									msgText1 = msgText1 + "<br><br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString() + "<br><br>";
									msgText1 = msgText1	+ "At the time of First log in, you will be prompted to change your password to a more secure and robust one.<br><br>If you have any questions regarding the Customer Portal or have problems logging in, please contact " +supportId+"";*/
									msgText1 = msgText1 + "<br><br>The MovePulse mobile App is now available!  Get instant access to information pertaining to your shipment, at your fingertips. To download and install the mobile interface -";
									msgText1 = msgText1	+ "<br><br><b>MovePulse for iPhone</b> Click here ";
									msgText1 = msgText1 + "<a href='https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8'>https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8</a>";
									msgText1 = msgText1	+ "<br><br><b>MovePulse for Android</b> Click here ";
									msgText1 = msgText1 + "<a href='https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en'>https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en</a>";
							    }						
								
								msgText1 = msgText1 +"<br><br>Best regards,"; 				   
						   }else{
				    	msgText1 = "<br>Thank you for entrusting your relocation to "+nameOfCompany+". " + " The web site address, user Log In and Password, to track the status of the services through Customer portal, are mentioned below:" ;
				    	   if(company!=null){
					    		if(company.getCportalAccessCompanyDivisionLevel()!=null && company.getCportalAccessCompanyDivisionLevel()){
					    			List list1 = customerFileManager.findDefaultBookingAgentCode(customerFile.getCompanyDivision());
									if(list1!=null && !list1.isEmpty() &&  list1.get(0)!=null){
										CompanyDivision companyDivision  = (CompanyDivision)list1.get(0);
										urlBinding=companyDivision.getCportalBrandingURL();
										if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
											}else if(company.getCportalBrandingURL()!=null && !company.getCportalBrandingURL().equals("")){
												msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+company.getCportalBrandingURL()+"'>"+company.getCportalBrandingURL()+"</a>";
											}else{
										    	msgText1 = msgText1 + "<br><br>The web site address is: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
												}
									}
									}else{
										urlBinding=company.getCportalBrandingURL();
										if(urlBinding!=null && (!(urlBinding.equalsIgnoreCase("")))){
											msgText1 = msgText1 + "<br><br>The web site address is: <a href='"+urlBinding+"'>"+urlBinding+"</a>";
											}else{
										    	msgText1 = msgText1 + "<br><br>The web site address is: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
												}
									}
					    			}
				    	msgText1 = msgText1 + "<br><br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString() + "<br><br>";
						msgText1 = msgText1	+ "At the time of First log in, you will be prompted to change your password to a more secure and robust one.<br><br>If you have any questions regarding the Customer Portal or have problems logging in, please contact "+supportId+".";
						if(sessionCorpID.equals("ICMG"))
							{
							msgText1 = msgText1 + "<br><br>Once you gain access to the Customer portal, then click on Tracking and then on the blue, under-lined order number.";
							}
						if(company!=null && company.getAccessRedSkyCportalapp()!=null && company.getAccessRedSkyCportalapp()){
							msgText1 = msgText1 + "<br><br>The MovePulse mobile App is now available!  Get instant access to information pertaining to your shipment, at your fingertips. To download and install the mobile interface -";
							msgText1 = msgText1	+ "<br><br><b>MovePulse for iPhone</b> Click here ";
							msgText1 = msgText1 + "<a href='https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8'>https://itunes.apple.com/us/app/redsky-customer-book/id648705357?mt=8</a>";
							msgText1 = msgText1	+ "<br><br><b>MovePulse for Android</b> Click here ";
							msgText1 = msgText1 + "<a href='https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en'>https://play.google.com/store/apps/details?id=com.redsky.cportal.android&hl=en</a>";	
					    }
				    	msgText1 = msgText1 +" <br><br>Best regards,";
						   }
						if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals("")) && !(sessionCorpID.equals("VOCZ")  || sessionCorpID.equals("VORU") || sessionCorpID.equals("VOBO") || sessionCorpID.equals("VOAO")||sessionCorpID.equals("BONG"))){
			    			String coordinatorDetailString = "";
							List fullUserName = customerFileManager.findUserFullName(customerFile.getPersonPricing());
								if((fullUserName!=null)&&(!fullUserName.isEmpty())&&(fullUserName.get(0)!=null)){
									coordinatorDetailString = fullUserName.get(0).toString();
									msgText1 = msgText1 + "<br><br>" + coordinatorDetailString;
								}
			    		}else if(customerFile.getCoordinatorName()!=null){
							msgText1 = msgText1 + "<br><br>" + customerFile.getCoordinatorName();
						}
						if(customerFile.getPersonPricing()!=null && (!customerFile.getPersonPricing().equals("")) && !(sessionCorpID.equals("VOCZ")  || sessionCorpID.equals("VORU") || sessionCorpID.equals("VOBO") || sessionCorpID.equals("VOAO")||sessionCorpID.equals("BONG"))){
							coordSignature = customerFileManager.findCoordSignature(customerFile.getPersonPricing());
							String coordinatorSignature = new String();
							if(coordSignature!=null && !coordSignature.isEmpty()){
								coordinatorSignature=coordSignature.get(0).toString();
								msgText1 = msgText1 + "<br>" + coordinatorSignature;
							}
							coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getPersonPricing());
							if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
								emailAdd = coordEmail.get(0).toString().trim();
							}
						
						}else if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equals("")){
							coordSignature = customerFileManager.findCoordSignature(customerFile.getCoordinator());
							String coordinatorSignature = new String();
							if(coordSignature!=null && !coordSignature.isEmpty() && coordSignature.get(0)!=null){
								coordinatorSignature=coordSignature.get(0).toString();
								msgText1 = msgText1 + "<br>" + coordinatorSignature;
							}
							coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
							if ((coordEmail != null) && (!coordEmail.isEmpty()) && coordEmail.get(0)!=null) { 
								emailAdd = coordEmail.get(0).toString().trim();
							}
						}else{
							emailAdd = "support@redskymobility.com";	
						}
						  
						subject = "Customer Portal User Information File# "+customerFile.getSequenceNumber();
					    msgText1 = "<html><table><font face='Tahoma' size='2'>"+msgText1+"</font></table></html>";
						String smtpHost = "localhost";
						String from = emailAdd;
						Properties props = System.getProperties();
						props.put("mail.transport.protocol", "smtp");
						props.put("mail.smtp.host", smtpHost);
						Session session = Session.getInstance(props, null);
						session.setDebug(true);
						MimeMessage msg = new MimeMessage(session);
						msg.setFrom(new InternetAddress(from));
						String tempRecipient="";
						String tempRecipientArr[]=emailTo.split(",");
						for(String str1:tempRecipientArr){
							if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
								if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
								tempRecipient += str1;
							}
						}
						emailTo=tempRecipient;
						
						emailSetupManager.globalEmailSetupProcess(from, emailTo, "", "", "", msgText1, subject, sessionCorpID,"",customerFile.getSequenceNumber(),"");
						mailStatus = "sent"; 					
				    }

					try{
						Calendar currentDate = Calendar.getInstance();
						SimpleDateFormat formatter= new SimpleDateFormat("dd-MMM-yy");
						String emailStatus=formatter.format(currentDate.getTime());	
						customerFile.setEmailSentDate(emailStatus);
						customerFileManager.save(customerFile);
					}catch (Exception e) {
						logger.error("Exception Occour: "+ e.getStackTrace()[0]);
					}
					getConfiguration().get(Constants.ENCRYPT_PASSWORD);
			    	String algorithm = (String)
			    	getConfiguration().get(Constants.ENC_ALGORITHM);
			    	if (algorithm == null) { // should only happen for test case
						  log.debug("assuming testcase, setting algorithm to 'SHA'");
						  algorithm = "SHA"; 
					}
			    	String passwordNew = StringUtil.encodePassword(sb.toString(),algorithm);
			    	confirmPasswordNew=passwordNew;
			    	Date compareWithDate=null;
					if(company.getPartnerPasswordExpiryDuration()!=null){
						compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));	
					}else{
						compareWithDate=DateUtils.addDays(getCurrentDate(), (60));
					} 
			    	customerFileManager.resetPassword(passwordNew, confirmPasswordNew, customerFile.getCustomerPortalId(),compareWithDate);

				} catch (Exception mex) {
					logger.error("Exception Occour: "+ mex.getStackTrace()[0]);
					mex.printStackTrace();
					mailStatus = "notSent";
					mailFailure=mex.toString(); 
				}		
			}
			if (!isNew) {
				qfId=customerFile.getId();
				return INPUT;
			} else {
				maxId = Long.parseLong(customerFileManager.findMaximumId().get(0).toString());
				customerFile = customerFileManager.get(maxId);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}
	}
	private EmailSetupManager emailSetupManager;
	
/*	@SkipValidation
	public String sendMail() throws AddressException, MessagingException {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try {
			customerFile = customerFileManager.get(id);
			String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
		     StringBuffer sb=new StringBuffer();
		     Random r = new Random();
		     int te=0;
		     for(int i=1;i<=8;i++){
		         te=r.nextInt(62);
		         sb.append(str.charAt(te));
		     }
		     String website = "";
		     String link="";
		     sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
		     if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					nameOfCompany=systemDefault.getCompany();
					supportId=systemDefault.getEmail();
					website = systemDefault.getWebsite();
					 link = "<a href=\""+website+"\">Home Page</a>";
				}
			}
		   
		     if((emailTo==null || emailTo.equalsIgnoreCase("")) && (customerFile.getEmail()!=null && ! customerFile.getEmail().equalsIgnoreCase(""))){
		    	  emailTo=customerFile.getEmail(); 
		    	 if(customerFile.getEmail2()!=null && ! customerFile.getEmail2().equalsIgnoreCase("") ){
		    		 emailTo=emailTo+","+customerFile.getEmail2(); 
		    	 }
		     }else if((emailTo==null || emailTo.equalsIgnoreCase("")) && (customerFile.getEmail2()!=null && ! customerFile.getEmail2().equalsIgnoreCase(""))){
		    	 emailTo=customerFile.getEmail2();
		     }
		    String subject = "";
		    String msgText1 ="";
		    if(emailTypeFlag!=null && emailTypeFlag==true){
		     List list = partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, customerFile.getBillToCode());	     
		     if(list!=null && (!(list.isEmpty()))){
		    	partnerPrivate = (PartnerPrivate) list.get(0);
		    	List partnerPublicId = partnerPublicManager.findPartnerId(partnerPrivate.getPartnerCode(), sessionCorpID);
		    	if(partnerPublicId!=null && !partnerPublicId.isEmpty()){
		    	partnerPublic = partnerPublicManager.get(Long.parseLong(partnerPublicId.get(0).toString()));
		    	if(partnerPrivate.getPortalDefaultAccess()!=null && partnerPrivate.getPortalDefaultAccess()==true && partnerPublic.getIsAccount()==true){
			    	msgText1 = "<br>Dear "+customerFile.getPrefix()+" "+customerFile.getLastName()+",";
			    	msgText1 = msgText1 + "<br><br>We are proud that "+partnerPrivate.getLastName()+" has entrusted UniGroup UTS with the organization of your removal. This e-mail is to acknowledge that we have received the official Household Move Authorization from your employer.";
			    	msgText1 = msgText1 + "<br><br>The appointed UniGroup UTS removal company will soon contact you to make an appointment for a survey at your residence. There is no need to contact UniGroup UTS at this stage. In case you are not contacted within the next 24 hours, please reply to this e-mail.";
			    	msgText1 = msgText1 + "<br><br>The professional handling of your removal needs to be arranged in good time. " +
			    	"It is therefore extremely important that you log in to the website mentioned below and fill out the origin and destination address details as soon and as detailed as possible. In case your destination address and city are not yet determined please enter a dummy address (e.g. the country"+"'"+"s capital)";
			    	msgText1 = msgText1 + "<br><br>The web site address is: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
					msgText1 = msgText1 + "<br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString();
					msgText1 = msgText1 + "<br>Your File Number : "+customerFile.getSequenceNumber();
					msgText1 = msgText1 + "<br><br>At the time of first log in, you will be prompted to change your password.";
					msgText1 = msgText1 + "<br>If you encounter any problems with entering the data in the Customer Portal, please send us your full name, telephone number(s), origin and destination address details by e-mail to "+supportId+". As soon as your data have been received by UniGroup Worldwide UTS, we will further initiate the removal process.";
			    	msgText1 = msgText1 + "<br><br>We have developed a web site where you can track the status of your shipment, and relevant information and documents that we publish on behalf of "+partnerPrivate.getLastName()+".";
			    	msgText1 = msgText1 + "<br>We wish you a smooth transfer.";
					msgText1 = msgText1 + "<br><br>Kind Regards,";
					msgText1 = msgText1 + "<br><br>UniGroup UTS";
					msgText1 = msgText1 + "<br>"+supportId+"";
					msgText1 = msgText1 + "<br><br>UniGroup Worldwide UTS is a globally operating multinational company specialized in household goods removals and expatriate relocation services. Should you like to know more about us, please consult our "+link+".";
	
					subject = "Your upcoming household goods removal with UniGroup UTS";
			    	emailAdd = ""+supportId+"";
			    	
		    	}else if(partnerPrivate.getPortalDefaultAccess()!=null && partnerPrivate.getPortalDefaultAccess()==false && partnerPublic.getIsAccount()==true){
			    	msgText1 = "<br>Dear "+customerFile.getPrefix()+" "+customerFile.getLastName()+",";
			    	msgText1 = msgText1 + "<br><br>We are proud that "+partnerPrivate.getLastName()+" has entrusted UniGroup UTS with the organization of your removal. This e-mail is to acknowledge that we have received the official Household Move Authorization from your employer.";
			    	msgText1 = msgText1 + "<br><br>The appointed UniGroup UTS removal company will soon contact you to make an appointment for a survey at your residence. There is no need to contact UniGroup UTS at this stage. In case you are not contacted within the next 24 hours, please reply to this e-mail.";
			    	msgText1 = msgText1 + "<br><br>The web site address is: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
					msgText1 = msgText1 + "<br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString();
					msgText1 = msgText1 + "<br>Your File Number : "+customerFile.getSequenceNumber();
					msgText1 = msgText1 + "<br><br>At the time of first log in, you will be prompted to change your password.";
					msgText1 = msgText1 + "<br>If you have any questions regarding the Customer Portal or have problems logging in, please contact "+supportId+".";
			    	msgText1 = msgText1 + "<br><br>We have developed a web site where you can track the status of your shipment, and relevant information and documents that we publish on behalf of "+partnerPrivate.getLastName()+".";
			    	msgText1 = msgText1 + "<br>We wish you a smooth transfer.";
					msgText1 = msgText1 + "<br><br>Kind Regards,";
					msgText1 = msgText1 + "<br><br>UniGroup UTS";
					msgText1 = msgText1 + "<br>"+supportId+"";
					msgText1 = msgText1 + "<br><br>UniGroup Worldwide UTS is a globally operating multinational company specialized in household goods removals and expatriate relocation services. Should you like to know more about us, please consult our "+link+".";

					subject = "Your upcoming household goods removal with UniGroup UTS";
			    	emailAdd = ""+supportId+"";
		        }else{
					msgText1 = "<br>Thank you for entrusting your relocation to "+nameOfCompany+". " + " The web site address, user Log In and Password, to track the status of the services through Customer portal, are mentioned below:" ;
					msgText1 = msgText1 + "<br><br>The web site address is: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
					msgText1 = msgText1 + "<br><br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString() + "<br><br>";
					msgText1 = msgText1	+ "At the time of First log in, you will be prompted to change your password to a more secure and robust one.<br><br>If you have any questions regarding the Customer Portal or have problems logging in, please contact "+supportId+".<br><br>Best regards,";
					if(customerFile.getCoordinatorName()!=null){
						msgText1 = msgText1 + "<br><br>" + customerFile.getCoordinatorName();
					}
					if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equals("")){
						coordSignature = customerFileManager.findCoordSignature(customerFile.getCoordinator());
						String coordinatorSignature = new String();
						if(coordSignature!=null && !coordSignature.isEmpty()){
							coordinatorSignature=coordSignature.get(0).toString();
							msgText1 = msgText1 + "<br>" + coordinatorSignature;
						}
						coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
						if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
							emailAdd = coordEmail.get(0).toString().trim();
						}
					}else{
						emailAdd = "support@redskymobility.com";	
					}
					subject = "CPortal User Information";
			    } 
		      } 
		     }
		    }else{
				msgText1 = "<br>Thank you for entrusting your relocation to "+nameOfCompany+". " + " The web site address, user Log In and Password, to track the status of the services through Customer portal, are mentioned below:" ;
				msgText1 = msgText1 + "<br><br>The web site address is: http://"+sessionCorpID.toLowerCase()+".skyrelo.com";
				msgText1 = msgText1 + "<br><br>Your User Log In : " + userID + "<br>Your Temporary Password : " + sb.toString() + "<br><br>";
				msgText1 = msgText1	+ "At the time of First log in, you will be prompted to change your password to a more secure and robust one.<br><br>If you have any questions regarding the Customer Portal or have problems logging in, please contact "+supportId+".<br><br>Best regards,";
				if(customerFile.getCoordinatorName()!=null){
					msgText1 = msgText1 + "<br><br>" + customerFile.getCoordinatorName();
				}
				if(customerFile.getCoordinator()!=null && !customerFile.getCoordinator().equals("")){
					coordSignature = customerFileManager.findCoordSignature(customerFile.getCoordinator());
					String coordinatorSignature = new String();
					if(coordSignature!=null && !coordSignature.isEmpty()){
						msgText1 = msgText1 + "<br>" + coordinatorSignature;
					}
					coordEmail = customerFileManager.findCoordEmailAddress(customerFile.getCoordinator());
					if ((coordEmail != null) && (!coordEmail.isEmpty())) { 
						emailAdd = coordEmail.get(0).toString().trim();
					}
				}else{
					emailAdd = "support@redskymobility.com";	
				}
				subject = "CPortal User Information";
		    }
		    msgText1 = "<html><table><font face='Tahoma' size='2'>"+msgText1+"</font></table></html>";
			
			String smtpHost = "localhost";
			String from = emailAdd;
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", smtpHost);
			Session session = Session.getInstance(props, null);
			session.setDebug(true);
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			String to[]= emailTo.split(",");
			InternetAddress[] address = new InternetAddress[to.length];
			for (int i = 0; i < to.length; i++)
			address[i] = new InternetAddress(to[i]);
			
			MimeBodyPart mbp1 = new MimeBodyPart();
			mbp1.setContent(msgText1,"text/html");
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp1);
			
			msg.setRecipients(Message.RecipientType.TO, address);    
			msg.setSubject(subject);
			msg.setContent(mp);
			
			msg.setSentDate(new Date());
			mailStatus = "sent"; 
			try{
				Calendar currentDate = Calendar.getInstance();
				SimpleDateFormat formatter= new SimpleDateFormat("dd-MMM-yy");
				String emailStatus=formatter.format(currentDate.getTime());	
				customerFile.setEmailSentDate(emailStatus);
				customerFileManager.save(customerFile);
			}catch (Exception e) {}
			getConfiguration().get(Constants.ENCRYPT_PASSWORD);
	    	String algorithm = (String)
	    	getConfiguration().get(Constants.ENC_ALGORITHM);
	    	if (algorithm == null) { // should only happen for test case
				  log.debug("assuming testcase, setting algorithm to 'SHA'");
				  algorithm = "SHA"; 
			}
	    	String passwordNew = StringUtil.encodePassword(sb.toString(),algorithm);
	    	confirmPasswordNew=passwordNew;
	    	Date compareWithDate=null;
			if(company.getPartnerPasswordExpiryDuration()!=null){
				compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));	
			}else{
				compareWithDate=DateUtils.addDays(getCurrentDate(), (60));
			} 
	    	customerFileManager.resetPassword(passwordNew, confirmPasswordNew, customerFile.getCustomerPortalId(),compareWithDate);

		} catch (MessagingException mex) {
			mex.printStackTrace();
			Exception ex = null;
			if ((ex = mex.getNextException()) != null) {
				ex.printStackTrace(); 
			} else {
			}
			mailStatus = "notSent";
			mailFailure=mex.toString(); 
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;
	}*/
	private List sendToQuotesToGoList;
    private String trackingCodeValue;
    private String messageValue;
	
	private String qtgUrl=(String)AppInitServlet.redskyConfigMap.get("qtg.url1");
public String saveQuotesToGoMethod() throws Exception
{

	try {
		contracts = customerFileManager.findCustJobContract(customerFile.getBillToCode(),customerFile.getJob(),customerFile.getCreatedOn(),sessionCorpID,customerFile.getCompanyDivision(),customerFile.getBookingAgentCode());
		companyDivis = customerFileManager.findCompanyDivisionByBookAg(customerFile.getCorpID(),customerFile.getBookingAgentCode());

		if(!customerFile.getBookingAgentCode().equals("")){
			if(bookingAgentCodeValid().equals("input")){
				String key = "bookingAgentCode is not valid";
				errorMessage(getText(key));
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			  }
			}
		
		
		if(!customerFile.getBillToCode().equals("")){
			if(billtoCodeValid().equals("input")){
				String key = "billToCode is not valid";
				errorMessage(getText(key));
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			  }
			}
		
		
		if(!customerFile.getAccountCode().equals("")){
			if(accountCodeValid().equals("input")){
				String key = "accountCode is not valid";
				errorMessage(getText(key));
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			  }
			}
		//companies = companyManager.findByCompanyDivis(sessionCorpID);
		if (companies.equalsIgnoreCase("Yes") && (customerFile.getCompanyDivision().equalsIgnoreCase("") || customerFile.getCompanyDivision().equalsIgnoreCase(null))) {
			String key ="Company Division is required field.";
			errorMessage(getText(key));
			//validateFormNav = "NOTOK";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
		
		if (cancel != null) {
			return "cancel";
		}
		if (delete != null) {
			return delete();
		}

		
		
		
		
		boolean isNew = (customerFile.getId() == null);
		if (customerFile.getQuotationStatus().equalsIgnoreCase("ACCEPTED")) {
			if(customerFile.getBookingDate()==null){
				customerFile.setBookingDate(new Date());
				customerFile.setSalesStatus("Booked");
			}
			customerFile.setControlFlag("C");	
			customerFile.setMoveType("BookedMove");
			customerFileTemp = customerFileManager.get(customerFile.getId());
			customerServiceOrders = customerFileTemp.getServiceOrders();
			customerBillings=customerFileTemp.getAgentQuotesbillings();
			if (customerServiceOrders != null) {
		    
				Iterator it = customerServiceOrders.iterator();
				while (it.hasNext()) {
					
					serviceOrder = (ServiceOrder) it.next();
					serviceOrder.setControlFlag("C");
					serviceOrder.setMoveType("BookedMove");
					serviceOrder.setFirstName(customerFile.getFirstName());
					serviceOrder.setLastName(customerFile.getLastName());
		           if(customerServiceOrders.size()==1){
		        	 //Bug 11839 - Possibility to modify Current status to Prior status
		        	    serviceOrder.setStatus("NEW");	
						serviceOrder.setStatusDate(new Date());
						serviceOrder.setStatusNumber(1);
					}else{
					if(serviceOrder.getQuoteAccept().equals("A") ){
						serviceOrder.setStatus("NEW");	
						serviceOrder.setStatusDate(new Date());
						serviceOrder.setStatusNumber(1);
					}
					if(serviceOrder.getQuoteAccept().equals("P")|| serviceOrder.getQuoteAccept().equals("")){
						serviceOrder.setStatus("HOLD");
		        		serviceOrder.setStatusDate(new Date());
		        		serviceOrder.setStatusNumber(100); 
					}
					if(serviceOrder.getQuoteAccept().equals("R")){
						serviceOrder.setStatus("CNCL");
		        		serviceOrder.setStatusDate(new Date());
		        		serviceOrder.setStatusNumber(400); 
					}
					}
				}
			}
			if (customerBillings != null) {

				Iterator it = customerBillings.iterator();
				while (it.hasNext()) {

					billings = (Billing) it.next();
					billings.setPersonBilling(customerFile.getPersonBilling());
		        	billings.setPersonPayable(customerFile.getPersonPayable());
		        	billings.setPersonPricing(customerFile.getPersonPricing());
		        	billings.setAuditor(customerFile.getAuditor());
					}
			}
		} else {
			customerFile.setControlFlag("Q");
		}
		
		if(isNew){
		roleHitFlag="OK";
		maxSequenceNumber = customerFileManager.findMaximum(sessionCorpID);
		if (maxSequenceNumber.get(0) == null) {
			customerFile.setSequenceNumber(sessionCorpID + "1000001");
		} else {
			autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(sessionCorpID, "")) + 1;
			seqNum = sessionCorpID + autoSequenceNumber.toString();
			customerFile.setSequenceNumber(seqNum);
		}
		checkSequenceNumberList = customerFileManager.findSequenceNumber(customerFile.getSequenceNumber());
		if (!(checkSequenceNumberList.isEmpty()) && isNew) {
			autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(sessionCorpID, "")) + 1;

			seqNum = sessionCorpID + autoSequenceNumber.toString();
			customerFile.setSequenceNumber(seqNum);
		}
		
		
		}

		if (!isNew) {
			roleHitFlag="OK";
			noMoreIfStat = true;
			if (customerFile.getStatusNumber() < 10 && customerFile.getComptetive().equals("Y") && customerFile.getSurvey() != null && customerFile.getSurvey().after(sysdate)) {
				//customerFile.setStatus("PEND");
				//customerFile.setQuotationStatus("PEND");
				//customerFile.setStatusNumber(10);
				//customerFile.setStatusDate(sysdate);
				noMoreIfStat = false;
			}

			customerFileTemp = customerFileManager.get(customerFile.getId());
			customerServiceOrders = customerFileTemp.getServiceOrders();
			noOfClaims = 0;
			noOfStorage = 0;
			soStatusNumberMax = 1;
			soStatusNumberMin = 60;

			if (customerServiceOrders != null) {

				Iterator it = customerServiceOrders.iterator();
				while (it.hasNext()) {

					serviceOrder = (ServiceOrder) it.next();
					if (soStatusNumberMax < serviceOrder.getStatusNumber().intValue()) {
						soStatusNumberMax = serviceOrder.getStatusNumber().intValue();
					}
					if (soStatusNumberMin > serviceOrder.getStatusNumber().intValue()) {
						soStatusNumberMin = serviceOrder.getStatusNumber().intValue();
					}
					if ((serviceOrder.getStatus().equalsIgnoreCase("CLMS") || serviceOrder.getStatus().equalsIgnoreCase("DLVR")) && atLeastOneClaim) {
						if (serviceOrder.getStatus().equalsIgnoreCase("CLMS")) {
							noOfClaims = noOfClaims + 1;
						}
						atLeastOneClaim = true;
					} else {
						atLeastOneClaim = false;
					}
					if ((serviceOrder.getStatus().equalsIgnoreCase("CLSD") || serviceOrder.getStatus().equalsIgnoreCase("STG")) && atLeastOneStorage) {
						if (serviceOrder.getStatus().equalsIgnoreCase("STG")) {
							noOfStorage = noOfStorage + 1;
						}
						atLeastOneStorage = true;
					} else {
						atLeastOneStorage = false;
					}
					if (serviceOrder.getStatus().equalsIgnoreCase("HOLD") && allJobHold) {
						allJobHold = true;
					} else {
						allJobHold = false;
					}
				}
			}
			if (customerFile.getStatusNumber() >= 10 && customerFile.getStatusNumber() < 20 && soStatusNumberMax > 10 && soStatusNumberMax < 60 && noOfClaims >= 1) {
				//customerFile.setStatus("ACTIVE");
				//customerFile.setStatusNumber(20);
				//customerFile.setStatusDate(sysdate);

			}

			if (customerFile.getStatusNumber() >= 20 && customerFile.getStatusNumber() < 30 && soStatusNumberMin >= 60 && noOfStorage >= 1) {
				//customerFile.setStatus("DELIV");
				//customerFile.setStatusNumber(30);
				//customerFile.setStatusDate(sysdate);

			}

			if (customerFile.getStatusNumber() >= 30 && customerFile.getStatusNumber() < 40 && atLeastOneClaim) {
				//customerFile.setStatus("CLAIM");
				//customerFile.setStatusNumber(40);
				//customerFile.setStatusDate(sysdate);

			}

			if (customerFile.getStatusNumber() >= 40 && customerFile.getStatusNumber() < 50 && atLeastOneStorage) {
				//customerFile.setStatus("STORG");
				//customerFile.setStatusNumber(50);
				//customerFile.setStatusDate(sysdate);

			}

			if (customerFile.getStatusNumber() >= 50 && customerFile.getStatusNumber() < 500 && allJobHold) {
				customerFile.setStatus("HOLD");
				customerFile.setStatusNumber(customerFile.getStatusNumber() + 1);
				customerFile.setStatusDate(sysdate);

			}
			for (UserDTO userDTO : allUser) {

				if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_SALE")){ 
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){	 
					estimatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_COORD")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						coordinatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase()); 
					}
					 
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PRICING")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){	 
					pricingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_ARM") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_CAJ") ||  userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_SSC")  ){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
					billingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PAYABLE")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
					payableList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_AUDITOR")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
					auditorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_EXECUTIVE")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
					execList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}
				
		    }
			//coordinatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_COORD",sessionCorpID);
			if(customerFile.getCoordinator()!=null && !(customerFile.getCoordinator().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getCoordinator());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String aliasName=aliasNameList.get(0).toString();
					String coordCode=customerFile.getCoordinator().toUpperCase();
					String alias = aliasName.toUpperCase();
					if(!coordinatorList.containsValue(coordCode)){
						coordinatorList.put(coordCode,alias );
					}
				}
			}
			
			//estimatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_SALE",sessionCorpID);
			if(customerFile.getSalesMan()!=null && !(customerFile.getSalesMan().equalsIgnoreCase(""))){
				@SuppressWarnings("rawtypes")
				List aliasNameList=userManager.findUserByUsername(customerFile.getSalesMan());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String estimatorCode=customerFile.getSalesMan().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!estimatorList.containsValue(estimatorCode)){
						estimatorList.put(estimatorCode,alias );
					}
				}
					
			}
			
			//pricingList = refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_PRICING",sessionCorpID);
			if(customerFile.getPersonPricing()!=null && !(customerFile.getPersonPricing().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getPersonPricing());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String pricingCode=customerFile.getPersonPricing().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!pricingList.containsValue(pricingCode)){
						pricingList.put(pricingCode,alias);
					}
				}
				
			}
			
			//billingList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_BILLING",sessionCorpID);
			if(customerFile.getPersonBilling()!=null && !(customerFile.getPersonBilling().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getPersonBilling());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String billingCode=customerFile.getPersonBilling().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!billingList.containsValue(billingCode)){
						billingList.put(billingCode,alias);
					}
				}
			}
			
			//payableList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_PAYABLE",sessionCorpID);
			if(customerFile.getPersonPayable()!=null && !(customerFile.getPersonPayable().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getPersonPayable());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String payableCode=customerFile.getPersonPayable().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!payableList.containsValue(payableCode)){
						payableList.put(payableCode,alias);
					}
				}
			}
			//auditorList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_AUDITOR",sessionCorpID);
			if(customerFile.getAuditor()!=null && !(customerFile.getAuditor().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getAuditor());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String auditorCode=customerFile.getAuditor().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!auditorList.containsValue(auditorCode)){
						auditorList.put(auditorCode,alias);
					}
				}
			}
			//execList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_EXECUTIVE",sessionCorpID);
			if(customerFile.getApprovedBy()!=null && !(customerFile.getApprovedBy().equalsIgnoreCase(""))){
				List aliasNameList = userManager.findUserByUsername(customerFile.getApprovedBy());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String execCode=customerFile.getApprovedBy().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!execList.containsValue(execCode)){
						execList.put(execCode,alias);
					}
				}
			}
			
		}

		if (customerFile.getOriginState() == null) {
			customerFile.setOriginState("");
		}
		if (customerFile.getDestinationState() == null) {
			customerFile.setDestinationState("");
		}

		if (isNew) {
			customerFile.setCreatedOn(new Date());
			customerFile.setCreatedBy(getRequest().getRemoteUser());
		} 
		customerFile.setUpdatedOn(new Date());
		customerFile.setUpdatedBy(getRequest().getRemoteUser());
		if (customerFile.getCoordinator().equals("")) {
			customerFile.setCoordinatorName(customerFile.getCoordinator());
		} else {
			if (customerFile.getCoordinator() == null) {
				customerFile.setCoordinator("");
				customerFile.setCoordinatorName("");
			} else {
				String coordinatorDetailString = "";
				List fullUserName = customerFileManager.findUserFullName(customerFile.getCoordinator());
				if(null != fullUserName){
					if(!fullUserName.isEmpty() && fullUserName.get(0)!=null){
						coordinatorDetailString = fullUserName.get(0).toString();
					}
				}
				customerFile.setCoordinatorName(coordinatorDetailString);
			}
		}
		 
			if (customerFile.getEstimator() == null) {
				customerFile.setEstimator("");
				customerFile.setEstimatorName("");
			} 
			else if (customerFile.getEstimator().equals("")) {
				customerFile.setEstimatorName(customerFile.getEstimator());
			}
			else {
				String estimatorDetailString = "";
				List fullUserName = customerFileManager.findUserFullName(customerFile.getEstimator());
				if(null != fullUserName){
					if(!fullUserName.isEmpty() && fullUserName.get(0)!=null){
						estimatorDetailString = fullUserName.get(0).toString();
					}
				}
				customerFile.setEstimatorName(estimatorDetailString);
			}
		

		if (!(customerFile.getSurvey() == null)) {
			surveyDate = Calendar.getInstance();
			surveyDate.setTime(customerFile.getSurvey());
			surveyDate.set(Calendar.HOUR, Integer.parseInt((customerFile.getSurveyTime()).substring(0, (customerFile.getSurveyTime()).indexOf(":"))));
			surveyDate.set(Calendar.MINUTE, Integer.parseInt((customerFile.getSurveyTime()).substring((customerFile.getSurveyTime()).indexOf(":") + 1, (customerFile
					.getSurveyTime()).length())));
			Date surveyStartTime = surveyDate.getTime();
			customerFile.setSurvey(surveyStartTime);
		}
		if(customerFile.getComptetive()!=null)
		{
			if(customerFile.getComptetive().equalsIgnoreCase("Y"))
			{
				if((customerFile.getQuoteAcceptenceDate()!=null)){
					if(!customerFile.getQuoteAcceptenceDate().toString().equalsIgnoreCase(""))
					{
							customerFile.setSalesStatus("Booked");
					}
				}		
				
			}
		}
		
		//customerFileManager.setFnameLname(customerFile.getSequenceNumber(), customerFile.getFirstName(), customerFile.getLastName());
		//customerFileManager.setPrefix(customerFile.getSequenceNumber(), customerFile.getPrefix());
		//customerFileManager.setFirstName(customerFile.getSequenceNumber(), customerFile.getFirstName());
		if (!isNew) {
		customerFileTemp = customerFileManager.get(customerFile.getId());
		customerBillings=customerFileTemp.getAgentQuotesbillings();
		customerServiceOrders = customerFileTemp.getServiceOrders();
		if (customerBillings != null) { 
			Iterator it = customerBillings.iterator();
			while (it.hasNext()) { 
				billings = (Billing) it.next();
				billings.setBillToCode(customerFile.getBillToCode());
		    	billings.setBillToName(customerFile.getBillToName());
		    	if(billings.getContract() ==null || billings.getContract().toString().trim().equals("") ){
		    	billings.setContract(customerFile.getContract()); 
		    	} 
		    	
				}
		}
		if(customerServiceOrders!= null){
			Iterator it = customerServiceOrders.iterator();
			while (it.hasNext()) { 
				serviceOrder = (ServiceOrder) it.next();
				serviceOrder.setLastName(customerFile.getLastName());	
				serviceOrder.setPrefix(customerFile.getPrefix());
				serviceOrder.setFirstName(customerFile.getFirstName());
				serviceOrder.setBillToCode(customerFile.getBillToCode());
				serviceOrder.setBillToName(customerFile.getBillToName());
				serviceOrder.setSocialSecurityNumber(customerFile.getSocialSecurityNumber());
				
			}
		}
		}
		customerFileManager.save(customerFile);
		//customerFileManager.updateServiceOrderBillToCode(customerFile.getBillToCode(), customerFile.getBillToName(), customerFile.getSequenceNumber(), sessionCorpID);
		 //customerFileManager.updateBillingBillToCode(customerFile.getBillToCode(), customerFile.getBillToName(), customerFile.getSequenceNumber(), sessionCorpID);
		/*try{
			String tempContract=customerFile.getContract();
			String tempSequenceNumber=customerFile.getSequenceNumber();
			if((tempContract!=null )&& (!(tempContract.equals("")) )){
				billingManager.updateContract(tempContract,tempSequenceNumber); 
			}
		}catch(Exception e){
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
		}*/
		
		
		hitFlag = "1";
		
		//ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
		//dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
		//companyDivis = customerFileManager.findCompanyDivisionByBookAg(customerFile.getCorpID(),customerFile.getBookingAgentCode());
		if(customerFile.getId() != null){
			adAddressesDetailsList= adAddressesDetailsManager.getAdAddressesDetailsList(customerFile.getId(),sessionCorpID);
		} 
		//contracts = customerFileManager.findCustJobContract(customerFile.getBillToCode(),customerFile.getJob(),customerFile.getCreatedOn(),sessionCorpID,customerFile.getCompanyDivision(),customerFile.getBookingAgentCode());
		//customerFileManager.setSSNo(customerFile.getSequenceNumber(),customerFile.getSocialSecurityNumber());
		getNotesForIconChange();
		//getComboList(sessionCorpID, customerFile.getJob());
		// check job type QTC
		List quotesToValidateList=customerFileManager.getQuotesToValidate(sessionCorpID, customerFile.getJob(), customerFile.getCompanyDivision());
		quotesToValidate=quotesToValidateList.get(0).toString();
		if(quotesToValidate!=null && !quotesToValidate.equals("") && quotesToValidate.equalsIgnoreCase("QTG")){
		sendToQuotesToGoList=customerFileManager.getQuotesToGoList(customerFile.getId(), customerFile.getCorpID(), customerFile.getCompanyDivision(), customerFile.getBookingAgentCode());
		List resourceIdPasswordList=customerFileManager.findResourceIdPassword(customerFile.getBookingAgentCode(),customerFile.getCorpID());
		String qtgUsername="";
		String qtgPassword="";
		if(resourceIdPasswordList!=null && !resourceIdPasswordList.contains(null) && !resourceIdPasswordList.isEmpty()){
			String str[]=resourceIdPasswordList.get(0).toString().split("~");
			qtgUsername=str[0];
			qtgPassword=str[1];
		}
		if(sendToQuotesToGoList!=null && !sendToQuotesToGoList.isEmpty()){
		XMLDto xmldto=new XMLDto();
		try {
			String requestXml= ""; 
			try {
				requestXml=  xmldto.creatXMLRequest(sendToQuotesToGoList);
				String createOpResponse=QtgClientServiceCall.callQtgService(requestXml,qtgUsername,qtgPassword,qtgUrl);
				CreateOpportunityReturn createOpportunityReturn =xmldto.readXmlResponse(createOpResponse);
				trackingCodeValue=createOpportunityReturn.getTrackingCode();
				messageValue=createOpportunityReturn.getResult().getMessages().getReturnMessage();
			} catch (RemoteException e) {
				//logger.warn("Exception: "+currentdate+" ## : "+e.toString());
				logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			}
		} catch (JAXBException e) {
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			e.printStackTrace();
		}finally{
			if(trackingCodeValue!=null && (!(trackingCodeValue.equalsIgnoreCase("")))){
				//messageValue="True"+"~"+trackingCodeValue;
				String key = "Successfully sent to Quotes-To-Go.";
				saveMessage(getText(key));
			customerFileManager.updateCustomerFileRegNumber(trackingCodeValue, customerFile.getId(), sessionCorpID);
			}else{
				//messageValue="False"+"~"+"";
				String key = "Failed to send to Quotes-To-Go. Please contact RedSky team if issue persists.";
				errorMessage(getText(key));
			}
		 }
		}else{
			String key = "Failed to send to Quotes-To-Go. Please contact RedSky team if issue persists.";
			errorMessage(getText(key));
		}
		}else{
			String key = "Failed to send to Quotes-To-Go. Not valid Job Type";
			errorMessage(getText(key));
		}
		if (!isNew) {
			return INPUT;
		} else {
			maxId = Long.parseLong(customerFileManager.findMaximumId().get(0).toString());
			customerFile = customerFileManager.get(maxId);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	} catch (Exception e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  	 e.printStackTrace();
	  	 return "errorlog";
	}

}
	private String bookingAgentCode;
	
	
@SkipValidation
public String mergeServiceOrdersMethod(){
	try {
		mergeSO = serviceOrderManager.get(Long.parseLong(quotesId));
		mergeSOList = customerFileManager.findMergeSOList(sessionCorpID,bookingAgentCode,checkAccessQuotation);
		hitFlag = "N";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	} catch (Exception e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	 e.printStackTrace();
  	 return "errorlog";
	}
	return SUCCESS;
}

@SkipValidation
public String mergeServiceOrderListMethod(){
	try {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Long startTime = System.currentTimeMillis();
		linkedSOMerge =  customerFileManager.findLinkSOtoCF(sessionCorpID,seqNumber);
		Long timeTaken = System.currentTimeMillis()-startTime;
		logger.warn("\n\nTime taken to find ServiceOrderList for merge: "+timeTaken+"\n\n");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	} catch (Exception e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  	 e.printStackTrace();
	  	 return "errorlog";
	}
	return SUCCESS;
}
@SkipValidation
public String mergeQFtoSoMethod(){
	try {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		BigDecimal totalExp = new BigDecimal(0.00);
		BigDecimal totalRev = new BigDecimal(0.00);
		serviceOrder = serviceOrderManager.get(Long.parseLong(serviceOrderID));
		qServiceOrder = serviceOrderManager.get(Long.parseLong(quotesId));
		Billing billing = billingManager.get(Long.parseLong(serviceOrderID));
		customerFile = customerFileManager.get(serviceOrder.getCustomerFileId());
		qCustomerFile = customerFileManager.get(qServiceOrder.getCustomerFileId());
		TrackingStatus ts = trackingStatusManager.get(Long.parseLong(serviceOrderID));
		
		WebApplicationContext  ctx=WebApplicationContextUtils.getWebApplicationContext(ServletActionContext.getServletContext()); 
		inventoryDataAction = (InventoryDataAction) ctx.getBean("inventoryDataAction");
		inventoryDataAction.setCompany(company);
		inventoryDataAction.setInventoryDataManager(inventoryDataManager);
		inventoryDataAction.setAccessInfoManager(accessInfoManager);
		inventoryDataAction.setSystemDefault(systemDefault);
		
		//Copying of pricing estimate
		List accLineIdList = customerFileManager.getQFLinkAccLine(sessionCorpID,qServiceOrder.getId());
		if(accLineIdList!=null && !accLineIdList.isEmpty()){
			Iterator itrAccId = accLineIdList.iterator();
			while(itrAccId.hasNext()){
				Long tempAccId = Long.parseLong(itrAccId.next().toString());
				AccountLine qPriceEst = accountLineManager.get(tempAccId);
				AccountLine sPriceEst = new AccountLine();
				try{
				BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
				beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtils.copyProperties(sPriceEst, qPriceEst);
				sPriceEst.setId(null);
				totalExp = totalExp.add(qPriceEst.getEstimateExpense());
				totalRev = totalRev.add(qPriceEst.getEstimateRevenueAmount());
				sPriceEst.setServiceOrderId(serviceOrder.getId());
				sPriceEst.setServiceOrder(serviceOrder);
				sPriceEst.setSequenceNumber(serviceOrder.getSequenceNumber());
				sPriceEst.setShipNumber(serviceOrder.getShipNumber());
				accountLineManager.save(sPriceEst);
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
		}
		
		//Copying of QF file cabinet  to CF file cabinet
		List QFileCabId = customerFileManager.getQFFilesId(sessionCorpID,qCustomerFile.getSequenceNumber());
		if(QFileCabId!=null && !QFileCabId.isEmpty()){
			Iterator itrFileId = QFileCabId.iterator();
			while(itrFileId.hasNext()){
				Long tempFileId = Long.parseLong(itrFileId.next().toString());
				MyFile qMyFile = myFileManager.get(tempFileId);
				MyFile cMyFile = new MyFile();
				try{
				BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
				beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtils.copyProperties(cMyFile, qMyFile);
				cMyFile.setId(null);
				cMyFile.setCustomerNumber(customerFile.getSequenceNumber());
				cMyFile.setFileId(customerFile.getSequenceNumber());
				myFileManager.save(cMyFile);
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
		}
		
		//Copying of Quotes file cabinet  to SO file cabinet
		List QuoteFileCabId = customerFileManager.getQFFilesId(sessionCorpID,qServiceOrder.getShipNumber());
		if(QuoteFileCabId!=null && !QuoteFileCabId.isEmpty()){
			Iterator itrFileId = QuoteFileCabId.iterator();
			while(itrFileId.hasNext()){
				Long tempFileId = Long.parseLong(itrFileId.next().toString());
				MyFile quotesMyFile = myFileManager.get(tempFileId);
				MyFile sMyFile = new MyFile();
				try{
				BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
				beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtils.copyProperties(sMyFile, quotesMyFile);
				sMyFile.setId(null);
				sMyFile.setCustomerNumber(serviceOrder.getSequenceNumber());
				sMyFile.setFileId(serviceOrder.getShipNumber());
				myFileManager.save(sMyFile);
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
		}


		//Copying of QF Notes  to CF Notes
		List qfNotesId= customerFileManager.getQFNotesId(sessionCorpID,qCustomerFile.getSequenceNumber(),qCustomerFile.getId());
		if(qfNotesId!=null && !qfNotesId.isEmpty()){
			Iterator itrNotesId = qfNotesId.iterator();
			while(itrNotesId.hasNext()){
				Long tempFileId = Long.parseLong(itrNotesId.next().toString());
				Notes qfNotes = notesManager.get(tempFileId);
				Notes cfNotes = new Notes();
				try{
				BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
				beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtils.copyProperties(cfNotes, qfNotes);
				cfNotes.setId(null);
				cfNotes.setNotesId(customerFile.getSequenceNumber());
				cfNotes.setCustomerNumber(customerFile.getSequenceNumber());
				cfNotes.setNotesKeyId(customerFile.getId());
				notesManager.save(cfNotes);
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
		}
		
		//Copying of Quotes Notes  to SO Notes
		List quotesNotesId= customerFileManager.getQFNotesId(sessionCorpID,qServiceOrder.getShipNumber(),qServiceOrder.getId());
		if(quotesNotesId!=null && !quotesNotesId.isEmpty()){
			Iterator itrNotesId = quotesNotesId.iterator();
			while(itrNotesId.hasNext()){
				Long tempFileId = Long.parseLong(itrNotesId.next().toString());
				Notes quotesNotes = notesManager.get(tempFileId);
				Notes soNotes = new Notes();
				try{
				BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
				beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtils.copyProperties(soNotes, quotesNotes);
				soNotes.setId(null);
				soNotes.setNotesId(serviceOrder.getShipNumber());
				soNotes.setCustomerNumber(serviceOrder.getSequenceNumber());
				soNotes.setNotesKeyId(serviceOrder.getId());
				notesManager.save(soNotes);
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
		}
		
		//Copying of QF AccessInfo  to CF AccessInfo
		List QFAccessInfo =  customerFileManager.getQFAccessInfo(sessionCorpID,qCustomerFile.getId());
		if(QFAccessInfo!=null && !QFAccessInfo.isEmpty() && QFAccessInfo.get(0)!=null && !(QFAccessInfo.get(0).toString().trim().equals(""))){
			String idData=QFAccessInfo.get(0).toString().trim();
			Long id=Long.parseLong(idData);
			AccessInfo qfAccessInfo= accessInfoManager.get(id);
			AccessInfo cfAccessInfo  = new AccessInfo();
			try{
				BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
				beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtils.copyProperties(cfAccessInfo, qfAccessInfo);
				cfAccessInfo.setId(null);
				cfAccessInfo.setNetworkSynchedId(null);
				cfAccessInfo.setCustomerFileId(customerFile.getId());
				cfAccessInfo = accessInfoManager.save(cfAccessInfo);
				
				
				List localAccessinfo = inventoryPathManager.getListByLocId(qfAccessInfo.getId(), "accessInfoId");
				if(localAccessinfo!=null && !localAccessinfo.isEmpty()){
					for (Object object : localAccessinfo) {
						InventoryPath qfInvPath = (InventoryPath)object;
						InventoryPath cfInvPath = new InventoryPath();
						try{
							BeanUtilsBean beanUtilsInvPath = BeanUtilsBean.getInstance();
							beanUtilsInvPath.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
							beanUtilsInvPath.copyProperties(cfInvPath, qfInvPath);
							cfInvPath.setId(null);
							cfInvPath.setInventoryDataId(null);
							cfInvPath.setAccessInfoId(cfAccessInfo.getId());
							cfInvPath = inventoryPathManager.save(cfInvPath);
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
					}
				}
				
				if(customerFile.getIsNetworkRecord()!=null && customerFile.getIsNetworkRecord() && ((customerFile.getContractType()!=null && (customerFile.getContractType().equals("CMM") ||customerFile.getContractType().equals("DMM"))) || company.getSurveyLinking())){
					List linkedSequenceNumber=inventoryDataAction.findLinkerSequenceNumber(customerFile);
					Set set = new HashSet(linkedSequenceNumber);
					List linkedSeqNum = new ArrayList(set);
					List<Object> records=inventoryDataAction.findRecords(linkedSeqNum,customerFile); 
					cfAccessInfo.setNetworkSynchedId(cfAccessInfo.getId());
					records.remove(customerFile);
					cfAccessInfo=accessInfoManager.save(cfAccessInfo);
					inventoryDataAction.synchornizeAccessInfoCF(records,cfAccessInfo,true); 
				}
			}catch(Exception ex){
				logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
			}
		}
		
		
		//Copying of Quotes AccessInfo  to SO AccessInfo
		QFAccessInfo =null;
		QFAccessInfo =  accessInfoManager.getAccessInfoForSO(qServiceOrder.getId());
		if(QFAccessInfo!=null && !QFAccessInfo.isEmpty() && QFAccessInfo.get(0)!=null && !(QFAccessInfo.get(0).toString().trim().equals(""))){
			AccessInfo qfAccessInfo= (AccessInfo) QFAccessInfo.get(0);
			AccessInfo cfAccessInfo  = new AccessInfo();
			try{
				BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
				beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtils.copyProperties(cfAccessInfo, qfAccessInfo);
				cfAccessInfo.setId(null);
				cfAccessInfo.setNetworkSynchedId(null);
				cfAccessInfo.setCustomerFileId(null);
				cfAccessInfo.setServiceOrderId(serviceOrder.getId());
				cfAccessInfo = accessInfoManager.save(cfAccessInfo);
				
				
				List localAccessinfo = inventoryPathManager.getListByLocId(qfAccessInfo.getId(), "accessInfoId");
				if(localAccessinfo!=null && !localAccessinfo.isEmpty()){
					for (Object object : localAccessinfo) {
						InventoryPath qfInvPath = (InventoryPath)object;
						InventoryPath cfInvPath = new InventoryPath();
						try{
							BeanUtilsBean beanUtilsInvPath = BeanUtilsBean.getInstance();
							beanUtilsInvPath.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
							beanUtilsInvPath.copyProperties(cfInvPath, qfInvPath);
							cfInvPath.setId(null);
							cfInvPath.setInventoryDataId(null);
							cfInvPath.setAccessInfoId(cfAccessInfo.getId());
							cfInvPath = inventoryPathManager.save(cfInvPath);
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
					}
				}
				
				   if(serviceOrder.getIsNetworkRecord() && (ts.getSoNetworkGroup() || company.getSurveyLinking())){
						List linkedShipNumber=inventoryDataAction.findLinkerShipNumber(serviceOrder);
						linkedShipNumber.remove(serviceOrder.getShipNumber());
						List<Object> serviceOrderRecords=inventoryDataAction.findServiceOrderRecords(linkedShipNumber,serviceOrder);
						serviceOrderRecords.remove(serviceOrder);
						cfAccessInfo.setNetworkSynchedId(cfAccessInfo.getId());
						cfAccessInfo=accessInfoManager.save(cfAccessInfo);
						inventoryDataAction.synchornizeAccessInfoSO(serviceOrderRecords,cfAccessInfo,true); 
					}
			}catch(Exception ex){
				logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
			}
		}
		
		
		/*// copy of Quotes Inventorydata  to SO inventorydata
		if(qServiceOrder.getMode().equals(serviceOrder.getMode())){
			List quotesInvntoryDataList =  customerFileManager.getQuotesInventoryData(sessionCorpID,qServiceOrder.getCustomerFileId(),qServiceOrder.getId(),qServiceOrder.getMode());
			if(quotesInvntoryDataList!=null && !quotesInvntoryDataList.isEmpty()){
				Iterator itrInventoryId = quotesInvntoryDataList.iterator();
				while(itrInventoryId.hasNext()){
					Long tempFileId = Long.parseLong(itrInventoryId.next().toString());
					InventoryData quotesInventoryData = inventoryDataManager.get(tempFileId);
					InventoryData soInventoryData  = new InventoryData();
					try{
					BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
					beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					beanUtils.copyProperties(soInventoryData, quotesInventoryData);
					soInventoryData.setId(null);
					soInventoryData.setCustomerFileID(serviceOrder.getCustomerFileId());
					soInventoryData.setServiceOrderID(serviceOrder.getId());
					inventoryDataManager.save(soInventoryData);
					}catch(Exception ex){
						System.out.println(ex);
					}
				}
			}
		}*/
		
		// copy of QF Inventorydata  to CF inventorydata
		List QFInvntoryDataList =  customerFileManager.getQFInventoryData(sessionCorpID,qCustomerFile.getId());
		if(QFInvntoryDataList!=null && !QFInvntoryDataList.isEmpty()){
			Iterator itrInventoryId = QFInvntoryDataList.iterator();
			while(itrInventoryId.hasNext()){
				Long tempFileId = Long.parseLong(itrInventoryId.next().toString());
				InventoryData qfInventoryData = inventoryDataManager.get(tempFileId);
				InventoryData cfInventoryData  = new InventoryData();
				try{
				BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
				beanUtils.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				beanUtils.copyProperties(cfInventoryData, qfInventoryData);
				cfInventoryData.setId(null);
				cfInventoryData.setCustomerFileID(customerFile.getId());
				if(qfInventoryData.getServiceOrderID()!=null){
					// copy of Quotes Inventorydata  to SO inventorydata
					if(qServiceOrder.getMode().equalsIgnoreCase(serviceOrder.getMode())){
						cfInventoryData.setServiceOrderID(serviceOrder.getId());
					}else{
						cfInventoryData.setServiceOrderID(null);
					}
				}else{
					cfInventoryData.setServiceOrderID(null);
				}
				cfInventoryData.setNetworkSynchedId(null);
				cfInventoryData = inventoryDataManager.save(cfInventoryData);
				
				
				// Copy of QF inventorypath to CF inventory path
				List QFInventoryPath = customerFileManager.getQFInventoryPathList(sessionCorpID,qfInventoryData.getId(),qCustomerFile.getId());
				if(QFInventoryPath!=null && !QFInventoryPath.isEmpty()){
					Iterator itrInventoryPathId = QFInventoryPath.iterator();
					while(itrInventoryPathId.hasNext()){
						Long tempInvPathId = Long.parseLong(itrInventoryPathId.next().toString());
						InventoryPath qfInvPath = inventoryPathManager.get(tempInvPathId);
						InventoryPath cfInvPath = new InventoryPath();
						try{
							BeanUtilsBean beanUtilsInvPath = BeanUtilsBean.getInstance();
							beanUtilsInvPath.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
							beanUtilsInvPath.copyProperties(cfInvPath, qfInvPath);
							cfInvPath.setId(null);
							cfInvPath.setInventoryDataId(cfInventoryData.getId());
							//cfInvPath.setAccessInfoId(customerFile.getId());
							inventoryPathManager.save(cfInvPath);
						}catch(Exception ex){
							logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
						}
					}
				}
				
				if(cfInventoryData.getServiceOrderID()!=null && !cfInventoryData.getServiceOrderID().equals("")){
					if(serviceOrder!=null){
						inventoryDataManager.updateWeghtAndVol(serviceOrder.getCustomerFileId(),serviceOrder.getId(),sessionCorpID);
						if(serviceOrder.getIsNetworkRecord() && (company.getSurveyLinking() || ts.getSoNetworkGroup())){
							cfInventoryData.setNetworkSynchedId(cfInventoryData.getId());
							cfInventoryData = inventoryDataManager.save(cfInventoryData);
								List linkedShipNumber=inventoryDataAction.findLinkerShipNumber(serviceOrder);
								Set set = new HashSet(linkedShipNumber);
								List linkedShip = new ArrayList(set);
								List<Object> serviceOrderRecords=inventoryDataAction.findServiceOrderRecords(linkedShip,serviceOrder);
								serviceOrderRecords.remove(serviceOrder);
								inventoryDataAction.synchornizeInventoryData(serviceOrderRecords,cfInventoryData,serviceOrder,ts ,true,"SO");

						}
					}
				}else{
					if(customerFile.getIsNetworkRecord()!=null && customerFile.getIsNetworkRecord() && ((customerFile.getContractType()!=null && (customerFile.getContractType().equals("CMM") ||customerFile.getContractType().equals("DMM"))) || company.getSurveyLinking())){
						cfInventoryData.setNetworkSynchedId(cfInventoryData.getId());
						cfInventoryData = inventoryDataManager.save(cfInventoryData);
						ServiceOrder so = new ServiceOrder();
						TrackingStatus ts1 = new TrackingStatus();
							List linkedSequenceNumber=inventoryDataAction.findLinkerSequenceNumber(customerFile);
							Set set = new HashSet(linkedSequenceNumber);
							List linkedSeqNum = new ArrayList(set);
							List<Object> records=inventoryDataAction.findRecords(linkedSeqNum,customerFile);
							records.remove(customerFile);
							inventoryDataAction.synchornizeInventoryData(records,cfInventoryData,so,ts1 ,true,"CF");

					}	
				}
				
				
				
				}catch(Exception ex){
					logger.error("Exception Occour: "+ ex.getStackTrace()[0]);
				}
			}
		}
		// copying of quotes to serviceOrder
		serviceOrder.setCoordinator(qServiceOrder.getCoordinator());
		serviceOrder.setPackingService(qServiceOrder.getPackingService());
		serviceOrder.setPackingMode(qServiceOrder.getPackingMode());
		serviceOrder.setContract(qServiceOrder.getContract());
		if(serviceOrder.getEstimatedTotalExpense()!=null && !serviceOrder.getEstimatedTotalExpense().toString().trim().equals(""))
			totalExp = totalExp.add(serviceOrder.getEstimatedTotalExpense());
		serviceOrder.setEstimatedTotalExpense(totalExp);
		if(serviceOrder.getEstimatedTotalRevenue()!=null && !(serviceOrder.getEstimatedTotalRevenue().toString().trim().equals("")))
			totalRev = totalRev.add(serviceOrder.getEstimatedTotalRevenue());
		serviceOrder.setEstimatedTotalRevenue(totalRev);
		BigDecimal estGrossMargine = totalRev.subtract(totalExp);
		serviceOrder.setEstimatedGrossMargin(estGrossMargine);
		BigDecimal estGrossMarginePer = new BigDecimal(0);
		if(totalRev.compareTo(new BigDecimal(0))!= 0)
			estGrossMarginePer = (estGrossMargine.multiply(new BigDecimal(100))).divide(totalRev,2);
		serviceOrder.setVip(qServiceOrder.getVip());
		serviceOrder.setEstimatedGrossMarginPercentage(estGrossMarginePer);
		serviceOrder = serviceOrderManager.save(serviceOrder);
		
		
		//copying Quotation to Customerfile
		customerFile.setCoordinator(qCustomerFile.getCoordinator());
		customerFile.setCoordinatorName(qCustomerFile.getCoordinatorName());
		customerFile.setContract(qCustomerFile.getContract());
		customerFile.setContractEnd(qCustomerFile.getContractEnd());
		customerFile.setContractStart(qCustomerFile.getContractStart());
		customerFile.setPersonPricing(qCustomerFile.getPersonPricing());
		customerFile.setPersonBilling(qCustomerFile.getPersonBilling());
		customerFile.setPersonPayable(qCustomerFile.getPersonPayable());
		customerFile.setAuditor(qCustomerFile.getAuditor());
		// 8250 - Changes in Merging Quotation Files to Linked Service Orders
			customerFile.setBillPayMethod(qCustomerFile.getBillPayMethod());
		customerFile = customerFileManager.save(customerFile);
		
		
		// papulating billing value 
		// 8250 - Changes in Merging Quotation Files to Linked Service Orders
			billing.setContract(serviceOrder.getContract());
			billing.setPersonPricing(qCustomerFile.getPersonPricing());
			billing.setPersonBilling(qCustomerFile.getPersonBilling());
			billing.setPersonPayable(qCustomerFile.getPersonPayable());
			billing.setAuditor(qCustomerFile.getAuditor());
			billing.setBillTo1Point(qCustomerFile.getBillPayMethod());
		billing= billingManager.save(billing);
		
		
		//Updating QUOTATION FILE
		qCustomerFile.setStatus("CNCL");
		qCustomerFile.setStatusReason("Merged");
		qCustomerFile.setQuotationStatus("Cancelled");
		qCustomerFile.setQfStatusReason("Merged");
		qCustomerFile = customerFileManager.save(qCustomerFile);
		
		
		//Updating Quotes
		
		//Bug 11839 - Possibility to modify Current status to Prior status
		qServiceOrder.setStatus("CNCL");
		qServiceOrder.setQuoteStatus("M");
		qServiceOrder.setStatusReason("Merged");
		qServiceOrder.setQuoteAccept("M");
		qServiceOrder = serviceOrderManager.save(qServiceOrder);
		
		saveMessage(qServiceOrder.getShipNumber()+" is merge with  "+serviceOrder.getShipNumber());
		mergeSOList = customerFileManager.findMergeSOList(sessionCorpID, bookingAgentCode,checkAccessQuotation);
		hitFlag = "Y";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	} catch (Exception e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	  	 e.printStackTrace();
	  	 return "errorlog";
	}
	return SUCCESS;
	
}
	
	@SkipValidation
	public String addAndCopyCustomerFile() throws Exception {
		try {
			if (id == null) {
				//getComboList(sessionCorpID, "a");
				customerFile = new CustomerFile();
				addCopyCustomerFile = (CustomerFile) customerFileManager.get(cid);
				//companies = companyManager.findByCompanyDivis(sessionCorpID);
				ostates = customerFileManager.findDefaultStateList(addCopyCustomerFile.getOriginCountryCode(), sessionCorpID);
				dstates = customerFileManager.findDefaultStateList(addCopyCustomerFile.getDestinationCountryCode(), sessionCorpID);
				companyDivis = customerFileManager.findCompanyDivisionByBookAg(sessionCorpID, bookCode);
				contracts = customerFileManager.findCustJobContract(addCopyCustomerFile.getBillToCode(), addCopyCustomerFile.getJob(), addCopyCustomerFile.getCreatedOn(), sessionCorpID,customerFile.getCompanyDivision(),customerFile.getBookingAgentCode());
				customerFile.setAccountCode(addCopyCustomerFile.getAccountCode());
				customerFile.setLastName(addCopyCustomerFile.getLastName());
				customerFile.setCorpID(addCopyCustomerFile.getCorpID());
				customerFile.setStatus("NEW");
				customerFile.setSalesMan(addCopyCustomerFile.getSalesMan());
				customerFile.setPrefix(addCopyCustomerFile.getPrefix());
				customerFile.setFirstName(addCopyCustomerFile.getFirstName());
				customerFile.setMiddleInitial(addCopyCustomerFile.getMiddleInitial());
				customerFile.setContactName(addCopyCustomerFile.getContactName());
				customerFile.setContactNameExt(addCopyCustomerFile.getContactNameExt());
				customerFile.setContactPhone(addCopyCustomerFile.getContactPhone());
				customerFile.setBillToCode(addCopyCustomerFile.getBillToCode());
				customerFile.setBillToName(addCopyCustomerFile.getBillToName());
				customerFile.setBillToReference(addCopyCustomerFile.getBillToReference());
				customerFile.setBillToAuthorization(addCopyCustomerFile.getBillToAuthorization());
				customerFile.setSurveyTime(addCopyCustomerFile.getSurveyTime());
				customerFile.setSurveyTime2(addCopyCustomerFile.getSurveyTime2());
				customerFile.setPrefSurveyTime1(addCopyCustomerFile.getPrefSurveyTime1());
				customerFile.setPrefSurveyTime2(addCopyCustomerFile.getPrefSurveyTime2());
				customerFile.setPrefSurveyTime3(addCopyCustomerFile.getPrefSurveyTime3());
				customerFile.setPrefSurveyTime4(addCopyCustomerFile.getPrefSurveyTime4());
				customerFile.setSourceCode(addCopyCustomerFile.getSourceCode());
				customerFile.setSource(addCopyCustomerFile.getSource());
				customerFile.setComptetive(addCopyCustomerFile.getComptetive());
				customerFile.setEntitled(addCopyCustomerFile.getEntitled());
				customerFile.setBillPayMethod(addCopyCustomerFile.getBillPayMethod());
				customerFile.setOriginAddress1(addCopyCustomerFile.getOriginAddress1());
				customerFile.setOriginAddress2(addCopyCustomerFile.getOriginAddress2());
				customerFile.setOriginAddress3(addCopyCustomerFile.getOriginAddress3());
				customerFile.setOriginCity(addCopyCustomerFile.getOriginCity());
				customerFile.setOriginState(addCopyCustomerFile.getOriginState());
				customerFile.setOriginZip(addCopyCustomerFile.getOriginZip());
				customerFile.setOriginPreferredContactTime(addCopyCustomerFile.getOriginPreferredContactTime());
				customerFile.setOriginCountryCode(addCopyCustomerFile.getOriginCountryCode());
				customerFile.setOriginCountry(addCopyCustomerFile.getOriginCountry());
				customerFile.setDestinationAddress1(addCopyCustomerFile.getDestinationAddress1());
				customerFile.setDestinationAddress2(addCopyCustomerFile.getDestinationAddress2());
				customerFile.setDestinationAddress3(addCopyCustomerFile.getDestinationAddress3());
				customerFile.setDestinationCity(addCopyCustomerFile.getDestinationCity());
				customerFile.setDestinationState(addCopyCustomerFile.getDestinationState());
				customerFile.setDestinationZip(addCopyCustomerFile.getDestinationZip());
				customerFile.setDestPreferredContactTime(addCopyCustomerFile.getDestPreferredContactTime());
				customerFile.setDestinationCountryCode(addCopyCustomerFile.getDestinationCountryCode());
				customerFile.setDestinationCountry(addCopyCustomerFile.getDestinationCountry());
				customerFile.setOriginDayPhone(addCopyCustomerFile.getOriginDayPhone());
				customerFile.setOriginHomePhone(addCopyCustomerFile.getOriginHomePhone());
				customerFile.setOriginFax(addCopyCustomerFile.getOriginFax());
				customerFile.setDestinationDayPhone(addCopyCustomerFile.getDestinationDayPhone());
				customerFile.setDestinationHomePhone(addCopyCustomerFile.getDestinationHomePhone());
				customerFile.setDestinationFax(addCopyCustomerFile.getDestinationFax());
				customerFile.setJob(addCopyCustomerFile.getJob());
				customerFile.setContactEmail(addCopyCustomerFile.getContactEmail());
				customerFile.setContract(addCopyCustomerFile.getContract());
				customerFile.setSocialSecurityNumber(addCopyCustomerFile.getSocialSecurityNumber());
				customerFile.setCoordinator(addCopyCustomerFile.getCoordinator());
				customerFile.setEstimator(addCopyCustomerFile.getEstimator());
				customerFile.setOrderBy(addCopyCustomerFile.getOrderBy());
				customerFile.setOrderPhone(addCopyCustomerFile.getOrderPhone());
				customerFile.setEmail(addCopyCustomerFile.getEmail());
				customerFile.setOrganization(addCopyCustomerFile.getOrganization());
				customerFile.setOriginCompany(addCopyCustomerFile.getOriginCompany());
				customerFile.setDestinationCompany(addCopyCustomerFile.getDestinationCompany());
				customerFile.setEmail2(addCopyCustomerFile.getEmail2());
				customerFile.setOriginDayPhoneExt(addCopyCustomerFile.getOriginDayPhoneExt());
				customerFile.setDestinationDayPhoneExt(addCopyCustomerFile.getDestinationDayPhoneExt());
				customerFile.setStatusReason("");
				customerFile.setSalesStatus("");
				customerFile.setCustomerEmployer(addCopyCustomerFile.getCustomerEmployer());
				customerFile.setDestinationEmail(addCopyCustomerFile.getDestinationEmail());
				customerFile.setDestinationEmail2(addCopyCustomerFile.getDestinationEmail2());
				customerFile.setCustPartnerLastName(addCopyCustomerFile.getCustPartnerLastName());
				customerFile.setCustPartnerFirstName(addCopyCustomerFile.getCustPartnerFirstName());
				customerFile.setCustPartnerPrefix(addCopyCustomerFile.getCustPartnerPrefix());
				customerFile.setCustPartnerEmail(addCopyCustomerFile.getCustPartnerEmail());
				customerFile.setOriginCityCode(addCopyCustomerFile.getOriginCityCode());
				customerFile.setDestinationCityCode(addCopyCustomerFile.getDestinationCityCode());
				customerFile.setVip(addCopyCustomerFile.getVip());
				customerFile.setDestinationContactName(addCopyCustomerFile.getDestinationContactName());
				customerFile.setDestinationContactNameExt(addCopyCustomerFile.getDestinationContactNameExt());
				customerFile.setDestinationContactPhone(addCopyCustomerFile.getDestinationContactPhone());
				customerFile.setDestinationContactEmail(addCopyCustomerFile.getDestinationContactEmail());
				customerFile.setDestinationOrganization(addCopyCustomerFile.getDestinationOrganization());
				customerFile.setOriginMobile(addCopyCustomerFile.getOriginMobile());
				customerFile.setDestinationMobile(addCopyCustomerFile.getDestinationMobile());
				customerFile.setNoCharge(addCopyCustomerFile.getNoCharge());
				customerFile.setCustPartnerContact(addCopyCustomerFile.getCustPartnerContact());
				customerFile.setCustPartnerMobile(addCopyCustomerFile.getCustPartnerMobile());
				customerFile.setPersonPricing(addCopyCustomerFile.getPersonPricing());
				customerFile.setPersonBilling(addCopyCustomerFile.getPersonBilling());
				customerFile.setPersonPayable(addCopyCustomerFile.getPersonPayable());
				customerFile.setAuditor(addCopyCustomerFile.getAuditor());
				customerFile.setCoordinatorName(addCopyCustomerFile.getCoordinatorName());
				customerFile.setEstimatorName(addCopyCustomerFile.getEstimatorName());
				customerFile.setAssignmentType(addCopyCustomerFile.getAssignmentType());
				customerFile.setPrivileges(addCopyCustomerFile.getPrivileges());
				customerFile.setBookingAgentCode(addCopyCustomerFile.getBookingAgentCode());
				customerFile.setBookingAgentName(addCopyCustomerFile.getBookingAgentName());
				customerFile.setRank(addCopyCustomerFile.getRank());
				customerFile.setControlFlag(addCopyCustomerFile.getControlFlag());
				customerFile.setQuotationStatus("New");
				customerFile.setApprovedBy(addCopyCustomerFile.getApprovedBy());
				customerFile.setAccountCode(addCopyCustomerFile.getAccountCode());
				customerFile.setAccountName(addCopyCustomerFile.getAccountName());
				customerFile.setCompanyDivision(addCopyCustomerFile.getCompanyDivision());
				customerFile.setSurvey(addCopyCustomerFile.getSurvey());
				customerFile.setPrefSurveyDate1(addCopyCustomerFile.getPrefSurveyDate1());
				customerFile.setPrefSurveyDate2(addCopyCustomerFile.getPrefSurveyDate2());
				customerFile.setApprovedOn(addCopyCustomerFile.getApprovedOn());
				customerFile.setMoveDate(addCopyCustomerFile.getMoveDate());
				customerFile.setBillApprovedDate(addCopyCustomerFile.getBillApprovedDate());
				customerFile.setBookingDate(addCopyCustomerFile.getBookingDate());
				customerFile.setInitialContactDate(addCopyCustomerFile.getInitialContactDate());
				customerFile.setPriceSubmissionToAccDate(addCopyCustomerFile.getPriceSubmissionToAccDate());
				customerFile.setPriceSubmissionToTranfDate(addCopyCustomerFile.getPriceSubmissionToTranfDate());
				customerFile.setQuoteAcceptenceDate(addCopyCustomerFile.getQuoteAcceptenceDate());
				customerFile.setSystemDate(addCopyCustomerFile.getSystemDate()); 
				customerFile.setStatusNumber(1);
				customerFile.setStatusDate(new Date());
				customerFile.setRegistrationNumber(addCopyCustomerFile.getRegistrationNumber());
				customerFile.setEstimateNumber("");
				customerFile.setCorpID(sessionCorpID);
				customerFile.setCreatedOn(new Date());
				customerFile.setUpdatedOn(new Date());
				customerFile.setCreatedBy(getRequest().getRemoteUser());
				customerFile.setUpdatedBy(getRequest().getRemoteUser());
				customerFile.setOriginAgentCode(addCopyCustomerFile.getOriginAgentCode()); 
				customerFile.setOriginAgentName(addCopyCustomerFile.getOriginAgentName()); 
				//customerFile.setPortalIdActive(addCopyCustomerFile.getPortalIdActive());
				//customerFile.setCustomerPortalId(addCopyCustomerFile.getCustomerPortalId());
				try{
				saveQuotation();
				}catch(Exception e){
					logger.error("Exception Occour: "+ e.getStackTrace()[0]);
				}
				}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}
	return SUCCESS;
	}
	
	// Validation of bookingAgentCode
	
	public String bookingAgentCodeValid()throws Exception{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			String booking=	customerFile.getBookingAgentCode();
			booking=booking.trim();
			if(customerFileManager.findByBillToCode(booking, sessionCorpID).isEmpty()){
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return INPUT;
			}
			else{
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}
		
		}
		
	
	
	public String billtoCodeValid()throws Exception{
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			String billTo=	customerFile.getBillToCode();
			billTo=billTo.trim();
			if(customerFileManager.findByBillToCode(billTo, sessionCorpID).isEmpty()){
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return INPUT;
			}
			else{
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}
		
		}
	
	public String accountCodeValid()throws Exception{
		try {
			String accountCo=	customerFile.getAccountCode();
			accountCo=accountCo.trim();
			if(customerFileManager.findByBillToCode(accountCo, sessionCorpID).isEmpty()){
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return INPUT;
			}
			else{
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}
		
		}
	private String quotesAcceptStatus; 

	@SkipValidation
	public String quoteServiceOrdersList() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			//getComboList(sessionCorpID, "");
			customerFile = customerFileManager.get(id);
			serviceOrders =customerFileManager.getQuotationServiceOrderlist(id,sessionCorpID);
			statusReason = refMasterManager.findByParameter(sessionCorpID, "STATUSREASON");
			quoteAcceptReason=refMasterManager.findByParameter(sessionCorpID, "QuoteAcceptReason");
			int l=customerFileManager.quoteReasonStatus(sessionCorpID, customerFile.getId());
			if(l>0){
				quotesAcceptStatus="R";
				quotesAcceptStatus="A";
			}else{
				quotesAcceptStatus="None";
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String editQuotationFile() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			serviceOrder = serviceOrderManager.get(id);
			customerFile = serviceOrder.getCustomerFile();
			getNotesForIconChange();
			roleHitFlag="OK";
			adAddressesDetailsList= adAddressesDetailsManager.getAdAddressesDetailsList(customerFile.getId(),sessionCorpID);
			ostates = customerFileManager.findDefaultStateList(customerFile.getOriginCountry(), sessionCorpID);
			dstates = customerFileManager.findDefaultStateList(customerFile.getDestinationCountry(), sessionCorpID);
			contracts = customerFileManager.findCustJobContract(customerFile.getBillToCode(),customerFile.getJob(),customerFile.getCreatedOn(),sessionCorpID,customerFile.getCompanyDivision(),customerFile.getBookingAgentCode());
			//getComboList(sessionCorpID, customerFile.getJob());
			for (UserDTO userDTO : allUser) {

				if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_SALE")){ 
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){	 
					estimatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_COORD")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
						coordinatorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase()); 
					}
					 
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PRICING")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){	 
					pricingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_ARM") || userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_CAJ") ||  userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_BILLING_SSC")  ){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
					billingList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_PAYABLE")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
					payableList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_AUDITOR")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
					auditorList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}else if(userDTO.getRoleName().trim().equalsIgnoreCase("ROLE_EXECUTIVE")){
					if(customerFile!=null && customerFile.getJob()!=null  && (userDTO.getJobType()==null || userDTO.getJobType().equals("") || userDTO.getJobType().contains(customerFile.getJob())  ) ){
					execList.put(userDTO.getUserName().trim().toUpperCase(), userDTO.getAlias().trim().toUpperCase());
					}
				}
				
			}
			//coordinatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_COORD",sessionCorpID);
			if(customerFile.getCoordinator()!=null && !(customerFile.getCoordinator().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getCoordinator());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String aliasName=aliasNameList.get(0).toString();
					String coordCode=customerFile.getCoordinator().toUpperCase();
					String alias = aliasName.toUpperCase();
					if(!coordinatorList.containsValue(coordCode)){
						coordinatorList.put(coordCode,alias );
					}
				}
			}
			
			//estimatorList=refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_SALE",sessionCorpID);
			if(customerFile.getSalesMan()!=null && !(customerFile.getSalesMan().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getSalesMan());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String estimatorCode=customerFile.getSalesMan().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!estimatorList.containsValue(estimatorCode)){
						estimatorList.put(estimatorCode,alias );
					}
				}
					
			}
			
			//pricingList = refMasterManager.findCordinaor(customerFile.getJob(),"ROLE_PRICING",sessionCorpID);
			if(customerFile.getPersonPricing()!=null && !(customerFile.getPersonPricing().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getPersonPricing());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String pricingCode=customerFile.getPersonPricing().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!pricingList.containsValue(pricingCode)){
						pricingList.put(pricingCode,alias);
					}
				}
				
			}
			
			//billingList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_BILLING",sessionCorpID);
			if(customerFile.getPersonBilling()!=null && !(customerFile.getPersonBilling().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getPersonBilling());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String billingCode=customerFile.getPersonBilling().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!billingList.containsValue(billingCode)){
						billingList.put(billingCode,alias);
					}
				}
			}
			
			//payableList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_PAYABLE",sessionCorpID);
			if(customerFile.getPersonPayable()!=null && !(customerFile.getPersonPayable().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getPersonPayable());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String payableCode=customerFile.getPersonPayable().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!payableList.containsValue(payableCode)){
						payableList.put(payableCode,alias);
					}
				}
			}
			//auditorList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_AUDITOR",sessionCorpID);
			if(customerFile.getAuditor()!=null && !(customerFile.getAuditor().equalsIgnoreCase(""))){
				List aliasNameList=userManager.findUserByUsername(customerFile.getAuditor());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String auditorCode=customerFile.getAuditor().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!auditorList.containsValue(auditorCode)){
						auditorList.put(auditorCode,alias);
					}
				}
			}
			//execList = refMasterManager.findCordinaor(customerFile.getJob(), "ROLE_EXECUTIVE",sessionCorpID);
			if(customerFile.getApprovedBy()!=null && !(customerFile.getApprovedBy().equalsIgnoreCase(""))){
				List aliasNameList = userManager.findUserByUsername(customerFile.getApprovedBy());
				if(aliasNameList!=null && !aliasNameList.isEmpty() && aliasNameList.get(0)!=null){
					String execCode=customerFile.getApprovedBy().toUpperCase();
					String aliasName=aliasNameList.get(0).toString();
					String alias = aliasName.toUpperCase();
					if(!execList.containsValue(execCode)){
						execList.put(execCode,alias);
					}
				}
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String getNotesForIconChange() {
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			Map<String, String> soDetailNotes = new LinkedHashMap<String, String>(); 
			String noteSubType="'Customer','Origin','Destination','Survey','VipReason','Billing','Entitlement','AccountContact','Spouse','cportal'";
			soDetailNotes = notesManager.countSoDetailNotes(customerFile.getSequenceNumber(),noteSubType,sessionCorpID);
			
			//List m = notesManager.countForNotes(customerFile.getSequenceNumber());
			//List mo = notesManager.countForOriginNotes(customerFile.getSequenceNumber());
			//List md = notesManager.countForDestinationNotes(customerFile.getSequenceNumber());
			//List ms = notesManager.countForSurveyNotes(customerFile.getSequenceNumber());
			//List mv = notesManager.countForVipNotes(customerFile.getSequenceNumber());
			//List mb = notesManager.countForBillingNotes(customerFile.getSequenceNumber());
			//List me = notesManager.countForEntitlementNotes(customerFile.getSequenceNumber());
			//List mc = notesManager.countForContactNotes(customerFile.getSequenceNumber());
			//List msd = notesManager.countForSpouseNotes(customerFile.getSequenceNumber());
			//List mp = notesManager.countForCportalNotes(customerFile.getSequenceNumber());
			
			if(soDetailNotes.containsKey("Customer")){
				countNotes = soDetailNotes.get("Customer");
			}else {
				countNotes = "0" ;
			} 
			if (soDetailNotes.containsKey("Origin")) {
				countOriginNotes =soDetailNotes.get("Origin"); 
			} else {
				countOriginNotes ="0";
			}

			if (soDetailNotes.containsKey("Destination") ) {
				countDestinationNotes = soDetailNotes.get("Destination"); 
			} else {
				countDestinationNotes = "0";
			}

			if (soDetailNotes.containsKey("Survey") ) {
				countSurveyNotes = soDetailNotes.get("Survey"); 
			} else {
				countSurveyNotes = "0";
			}

			if (soDetailNotes.containsKey("Billing")) {
				countBillingNotes =soDetailNotes.get("Billing");  
			} else {
				countBillingNotes ="0";
			}

			if (soDetailNotes.containsKey("Entitlement") ) {
				countEntitlementNotes = soDetailNotes.get("Entitlement");  
			} else {
				countEntitlementNotes = "0";
			}

			if (soDetailNotes.containsKey("AccountContact") ) {
				countContactNotes =soDetailNotes.get("AccountContact");  
			} else {
				countContactNotes = "0";
			}

			if (soDetailNotes.containsKey("Spouse")) {
				countSpouseNotes = soDetailNotes.get("Spouse");  
			} else {
				countSpouseNotes = "0";
			}

			if (soDetailNotes.containsKey("cportal")) {
				countCportalNotes = soDetailNotes.get("cportal"); 
			} else {
				countCportalNotes = "0";
			}

			if (soDetailNotes.containsKey("VipReason")  ) {
				countVipNotes = soDetailNotes.get("VipReason"); 
			} else {
				countVipNotes="0";
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
/*private String fname;
private String lastname;
private String originDayPhone;
private String originHomePhone;
private String originMobile;*/

	
/*	@SkipValidation
	public String updateCustomerFieldMethod(){
		customerFileManager.updateCustomerRequierdField(id, sessionCorpID, fname, lastname, originDayPhone, originHomePhone, originMobile);
		return SUCCESS;
	}*/
	private AccountAssignmentTypeManager accountAssignmentTypeManager;
	private List assignmentList;
	private String bCode;
	@SkipValidation
    public String findAssignmentAjax(){
    	try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			assignmentList=accountAssignmentTypeManager.findAssignmentByParentAgent(bCode,sessionCorpID);            	      		
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}
    return SUCCESS;
    }
	
	private String mc;
	@SkipValidation
	public String fillBillToAuthority(){
		try {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			 mc = partnerPrivateManager.getBillMc(bCode,sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return "errorlog";
		}    	
		return SUCCESS;
	}
	
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setContractManager(ContractManager contractManager) {
		this.contractManager = contractManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public Set getCustomerFiles() {
		return customerFiles;
	}

	public List getServiceOrders() {
		return serviceOrders;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<CustomerFile> getSurveyDetails() {
		return surveyDetails;
	}

	public void setSurveyDetails(Set<CustomerFile> surveyDetails) {
		this.surveyDetails = surveyDetails;
	}

	public List getWeightunits() {
		return weightunits;
	}

	public List getVolumeunits() {
		return volumeunits;
	}

	public Map<String, String> getForwardNoteStatus() {
		return forwardNoteStatus;
	}

	public Map<String, String> getCustStatus() {
		return custStatus;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public Map<String, String> getLead() {
		return lead;
	}

	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public Map<String, String> getDcountry() {
		return dcountry;
	}

	public Map<String, String> getOrganiza() {
		return organiza;
	}

	public Map<String, String> getPaytype() {
		return paytype;
	}

	public Map<String, String> getPeak() {
		return peak;
	}

	
	public Map<String, String> getState() {
		return state;
	}

	public Map<String, String> getEQUIP() {
		return EQUIP;
	}

	public Map<String, String> getPreffix() {
		return preffix;
	}

	public Map<String, String> getSurvey() {
		return survey;
	}

	public Map<String, String> getYesno() {
		return yesno;
	}

	public Map<String, String> getRouting() {
		return routing;
	}

	public Map<String, String> getCommodit() {
		return commodit;
	}

	public Map<String, String> getMode() {
		return mode;
	}

	public Map<String, String> getService() {
		return service;
	}

	public Map<String, String> getPkmode() {
		return pkmode;
	}

	public Map<String, String> getLoadsite() {
		return loadsite;
	}

	public Map<String, String> getSpecific() {
		return specific;
	}

	public Map<String, String> getJobtype() {
		return jobtype;
	}

	public Map<String, String> getMilitary() {
		return military;
	}

	public Map<String, String> getJOB_STATUS() {
		return JOB_STATUS;
	}

	public Map<String, String> getNotetype() {
		return notetype;
	}

	public Map<String, String> getNotesubtype() {
		return notesubtype;
	}

	public Map<String, String> getNotestatus() {
		return notestatus;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public Map<String, String> getQc() {
		return qc;
	}

	public Map<String, String> getCrew() {
		return crew;
	}

	public Map<String, String> getCoord() {
		return coord;
	}

	public Map<String, String> getAll_user() {
		return all_user;
	}

	public Map<String, String> getQUOTESTATUS() {
		return QUOTESTATUS;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountNotes() {
		return countNotes;
	}

	public String getCountDestinationNotes() {
		return countDestinationNotes;
	}

	public String getCountOriginNotes() {
		return countOriginNotes;
	}

	public String getCountSurveyNotes() {
		return countSurveyNotes;
	}

	public String getCountVipNotes() {
		return countVipNotes;
	}

	public String getCountBillingNotes() {
		return countBillingNotes;
	}

	public String getCountContactNotes() {
		return countContactNotes;
	}

	public String getCountCportalNotes() {
		return countCportalNotes;
	}

	public String getCountEntitlementNotes() {
		return countEntitlementNotes;
	}

	public String getCountSpouseNotes() {
		return countSpouseNotes;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	
	public void setCountBillingNotes(String countBillingNotes) {
		this.countBillingNotes = countBillingNotes;
	}

	public void setCountContactNotes(String countContactNotes) {
		this.countContactNotes = countContactNotes;
	}

	public void setCountCportalNotes(String countCportalNotes) {
		this.countCportalNotes = countCportalNotes;
	}

	public void setCountDestinationNotes(String countDestinationNotes) {
		this.countDestinationNotes = countDestinationNotes;
	}

	public void setCountEntitlementNotes(String countEntitlementNotes) {
		this.countEntitlementNotes = countEntitlementNotes;
	}

	public void setCountNotes(String countNotes) {
		this.countNotes = countNotes;
	}

	public void setCountOriginNotes(String countOriginNotes) {
		this.countOriginNotes = countOriginNotes;
	}

	public void setCountSpouseNotes(String countSpouseNotes) {
		this.countSpouseNotes = countSpouseNotes;
	}

	public void setCountSurveyNotes(String countSurveyNotes) {
		this.countSurveyNotes = countSurveyNotes;
	}

	public void setCountVipNotes(String countVipNotes) {
		this.countVipNotes = countVipNotes;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCoordinatr() {
		return coordinatr;
	}

	public void setCoordinatr(String coordinatr) {
		this.coordinatr = coordinatr;
	}

	public List<DataCatalog> getTooltip() {
		return tooltip;
	}

	public void setTooltip(List<DataCatalog> tooltip) {
		this.tooltip = tooltip;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableFieldName() {
		return tableFieldName;
	}

	public void setTableFieldName(String tableFieldName) {
		this.tableFieldName = tableFieldName;
	}

	public Map<String, String> getBilling() {
		return billing;
	}

	public Map<String, String> getPayable() {
		return payable;
	}

	public Map<String, String> getPricing() {
		return pricing;
	}
	
	public Map<String, String> getAuditor() {
		return auditor;
	}
	public static Map<String, String> getDstates() {
		return dstates;
	}

	public static void setDstates(Map<String, String> dstates) {
		QuotationManagementAction.dstates = dstates;
	}

	public static Map<String, String> getOstates() {
		return ostates;
	}

	public static void setOstates(Map<String, String> ostates) {
		QuotationManagementAction.ostates = ostates;
	}

	public Map<String, String> getExec() {
		return exec;
	}	

	public ExtendedPaginatedList getQuotationManagementListExt() {
		return quotationManagementListExt;
	}

	public void setQuotationManagementListExt(
			ExtendedPaginatedList quotationManagementListExt) {
		this.quotationManagementListExt = quotationManagementListExt;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public List setCompanyDivis(List companyDivis) {
		return (companyDivis!=null && !companyDivis.isEmpty())?companyDivis:customerFileManager.findCompanyDivisionByBookAg(sessionCorpID, bookCode);
	}

	public String getBookCode() {
		return bookCode;
	}

	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}

	public String getCompanyDivisn() {
		return companyDivisn;
	}

	public void setCompanyDivisn(String companyDivisn) {
		this.companyDivisn = companyDivisn;
	}

	
	public String getContractBillCode() {
		return contractBillCode;
	}

	public void setContractBillCode(String contractBillCode) {
		this.contractBillCode = contractBillCode;
	}

	public Date getCustCreatedOn() {
		return custCreatedOn;
	}

	public void setCustCreatedOn(Date custCreatedOn) {
		this.custCreatedOn = custCreatedOn;
	}

	public String getCustJobType() {
		return custJobType;
	}

	public void setCustJobType(String custJobType) {
		this.custJobType = custJobType;
	}

	public List getContracts() {
		if(defaultcontract!=null && customerFile.getId()==null){
			contracts.add(defaultcontract);
			return contracts;
		}else{
		return contracts;
		}
	}

	public List setContracts(List contracts) {
		return (contracts!=null && !contracts.isEmpty())?contracts:customerFileManager.findCustJobContract(contractBillCode, custJobType, custCreatedOn, sessionCorpID,companyDivision,bookingAgentCode);
	}

	public String getDetailPage() {
		return detailPage;
	}

	public void setDetailPage(String detailPage) {
		this.detailPage = detailPage;
	}

	/**
	 * @return the billings
	 */
	public Billing getBillings() {
		return billings;
	}

	/**
	 * @param billings the billings to set
	 */
	public void setBillings(Billing billings) {
		this.billings = billings;
	}

	public Map<String, String> getBillingList() {
		return billingList;
	}

	public Map<String, String> getCoordinatorList() {
		return coordinatorList;
	}

	public Map<String, String> getEstimatorList() {
		return estimatorList;
	}

	public Map<String, String> getPayableList() {
		return payableList;
	}

	public Map<String, String> getPricingList() {
		return pricingList;
	}
	
	public Map<String, String> getAuditorList() {
		return auditorList;
	}
	
	public Map<String, String> getExecList() {
		return execList;
	}

	public String getRoleHitFlag() {
		return roleHitFlag;
	}

	public void setRoleHitFlag(String roleHitFlag) {
		this.roleHitFlag = roleHitFlag;
	}

	public void setCoordinatorList(Map<String, String> coordinatorList) {
		this.coordinatorList = coordinatorList;
	}

	/**
	 * @return the activeStatus
	 */
	public Boolean getActiveStatus() {
		return activeStatus;
	}

	/**
	 * @param activeStatus the activeStatus to set
	 */
	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Map<String, String> getQuoteAccept() {
		return quoteAccept;
	}

	public void setQuoteAccept(Map<String, String> quoteAccept) {
		this.quoteAccept = quoteAccept;
	}

	public int getControlFlag() {
		return controlFlag;
	}

	public void setControlFlag(int controlFlag) {
		this.controlFlag = controlFlag;
	}

	

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public CustomerFile getAddCopyCustomerFile() {
		return addCopyCustomerFile;
	}

	public void setAddCopyCustomerFile(CustomerFile addCopyCustomerFile) {
		this.addCopyCustomerFile = addCopyCustomerFile;
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getCompanies() {
		return companies;
	}

	public void setCompanies(String companies) {
		this.companies = companies;
	}
public String getBookingAppUserCode() {
		return bookingAppUserCode;
	}

	public void setBookingAppUserCode(String bookingAppUserCode) {
		this.bookingAppUserCode = bookingAppUserCode;
	}
	public String getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	public String getWeightUnit() {
		return weightUnit;
	}

	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public Integer getQuotesNumber() {
		return QuotesNumber;
	}

	public void setQuotesNumber(Integer quotesNumber) {
		QuotesNumber = quotesNumber;
	}
	
	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public String getQuotesToValidate() {
		return quotesToValidate;
	}

	public void setQuotesToValidate(String quotesToValidate) {
		this.quotesToValidate = quotesToValidate;
	}

/*	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getOriginDayPhone() {
		return originDayPhone;
	}

	public void setOriginDayPhone(String originDayPhone) {
		this.originDayPhone = originDayPhone;
	}

	public String getOriginHomePhone() {
		return originHomePhone;
	}

	public void setOriginHomePhone(String originHomePhone) {
		this.originHomePhone = originHomePhone;
	}

	public String getOriginMobile() {
		return originMobile;
	}

	public void setOriginMobile(String originMobile) {
		this.originMobile = originMobile;
	}*/

	public List getSendToQuotesToGoList() {
		return sendToQuotesToGoList;
	}


	public void setSendToQuotesToGoList(List sendToQuotesToGoList) {
		this.sendToQuotesToGoList = sendToQuotesToGoList;
	}

	public String getTrackingCodeValue() {
		return trackingCodeValue;
	}

	public void setTrackingCodeValue(String trackingCodeValue) {
		this.trackingCodeValue = trackingCodeValue;
	}

	public String getMessageValue() {
		return messageValue;
	}

	public void setMessageValue(String messageValue) {
		this.messageValue = messageValue;
	}

	public String getDefaultcontract() {
		return defaultcontract;
	}

	public void setDefaultcontract(String defaultcontract) {
		this.defaultcontract = defaultcontract;
	}

	public Map<String, String> getStatusReason() {
		return statusReason;
	}

	public void setStatusReason(Map<String, String> statusReason) {
		this.statusReason = statusReason;
	}

	public String getQuotesAcceptStatus() {
		return quotesAcceptStatus;
	}

	public void setQuotesAcceptStatus(String quotesAcceptStatus) {
		this.quotesAcceptStatus = quotesAcceptStatus;
	}

	public void setAdAddressesDetailsManager(
			AdAddressesDetailsManager adAddressesDetailsManager) {
		this.adAddressesDetailsManager = adAddressesDetailsManager;
	}
	public void setAdAddressesDetailsList(List adAddressesDetailsList) {
		this.adAddressesDetailsList = adAddressesDetailsList;
	}
	
	public List getAdAddressesDetailsList() {
		return (adAddressesDetailsList!=null && !adAddressesDetailsList.isEmpty())?adAddressesDetailsList:adAddressesDetailsManager.getAdAddressesDetailsList(id,sessionCorpID);
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public static Map<String, String> getCountryCod() {
		return countryCod;
	}

	public static void setCountryCod(Map<String, String> countryCod) {
		QuotationManagementAction.countryCod = countryCod;
	}

	public List getMergeSOList() {
		return mergeSOList;
	}

	public void setMergeSOList(List mergeSOList) {
		this.mergeSOList = mergeSOList;
	}

	public String getSeqNumber() {
		return seqNumber;
	}

	public void setSeqNumber(String seqNumber) {
		this.seqNumber = seqNumber;
	}

	public List getLinkedSOMerge() {
		return linkedSOMerge;
	}

	public void setLinkedSOMerge(List linkedSOMerge) {
		this.linkedSOMerge = linkedSOMerge;
	}

	public String getQuotesId() {
		return quotesId;
	}

	public void setQuotesId(String quotesId) {
		this.quotesId = quotesId;
	}

	public String getServiceOrderID() {
		return serviceOrderID;
	}

	public void setServiceOrderID(String serviceOrderID) {
		this.serviceOrderID = serviceOrderID;
	}

	public ServiceOrder getqServiceOrder() {
		return qServiceOrder;
	}

	public void setqServiceOrder(ServiceOrder qServiceOrder) {
		this.qServiceOrder = qServiceOrder;
	}

	public CustomerFile getqCustomerFile() {
		return qCustomerFile;
	}

	public void setqCustomerFile(CustomerFile qCustomerFile) {
		this.qCustomerFile = qCustomerFile;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public AccountLine getAccountLine() {
		return accountLine;
	}

	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}

	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}

	public void setInventoryDataManager(InventoryDataManager inventoryDataManager) {
		this.inventoryDataManager = inventoryDataManager;
	}

	public void setAccessInfoManager(AccessInfoManager accessInfoManager) {
		this.accessInfoManager = accessInfoManager;
	}

	public String getStateshitFlag() {
		return stateshitFlag;
	}

	public void setStateshitFlag(String stateshitFlag) {
		this.stateshitFlag = stateshitFlag;
	}

	public String getPortalId() {
		return portalId;
	}

	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	public String getUserPortalId() {
		return userPortalId;
	}

	public void setUserPortalId(String userPortalId) {
		this.userPortalId = userPortalId;
	}

	public String getOldPortalId() {
		return oldPortalId;
	}

	public void setOldPortalId(String oldPortalId) {
		this.oldPortalId = oldPortalId;
	}

	public String getPortalIdCount() {
		return portalIdCount;
	}

	public void setPortalIdCount(String portalIdCount) {
		this.portalIdCount = portalIdCount;
	}

	public List getNewPortalId() {
		return newPortalId;
	}

	public void setNewPortalId(List newPortalId) {
		this.newPortalId = newPortalId;
	}

	public String getcPortalId() {
		return cPortalId;
	}

	public void setcPortalId(String cPortalId) {
		this.cPortalId = cPortalId;
	}

	public List getPoratalIdCountList() {
		return poratalIdCountList;
	}

	public void setPoratalIdCountList(List poratalIdCountList) {
		this.poratalIdCountList = poratalIdCountList;
	}

	public Boolean getPortalIdActive() {
		return portalIdActive;
	}

	public void setPortalIdActive(Boolean portalIdActive) {
		this.portalIdActive = portalIdActive;
	}

	public String getBookingAgentCode() {
		return bookingAgentCode;
	}

	public void setBookingAgentCode(String bookingAgentCode) {
		this.bookingAgentCode = bookingAgentCode;
	}

	public ServiceOrder getMergeSO() {
		return mergeSO;
	}

	public void setMergeSO(ServiceOrder mergeSO) {
		this.mergeSO = mergeSO;
	}

	public List getServiceOrderSearchType() {
		return serviceOrderSearchType;
	}

	public void setServiceOrderSearchType(List serviceOrderSearchType) {
		this.serviceOrderSearchType = serviceOrderSearchType;
	}

	public String getServiceOrderSearchVal() {
		return serviceOrderSearchVal;
	}

	public void setServiceOrderSearchVal(String serviceOrderSearchVal) {
		this.serviceOrderSearchVal = serviceOrderSearchVal;
	}

	public List getAssignmentList() {
		return assignmentList;
	}

	public void setAssignmentList(List assignmentList) {
		this.assignmentList = assignmentList;
	}

	public String getbCode() {
		return bCode;
	}

	public void setbCode(String bCode) {
		this.bCode = bCode;
	}

	public void setAccountAssignmentTypeManager(
			AccountAssignmentTypeManager accountAssignmentTypeManager) {
		this.accountAssignmentTypeManager = accountAssignmentTypeManager;
	}

	public Map<String, String> getAssignmentTypes() {
		return assignmentTypes;
	}

	public void setAssignmentTypes(Map<String, String> assignmentTypes) {
		this.assignmentTypes = assignmentTypes;
	}

	public Boolean getEmailTypeFlag() {
		return emailTypeFlag;
	}

	public void setEmailTypeFlag(Boolean emailTypeFlag) {
		this.emailTypeFlag = emailTypeFlag;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getNameOfCompany() {
		return nameOfCompany;
	}

	public void setNameOfCompany(String nameOfCompany) {
		this.nameOfCompany = nameOfCompany;
	}

	public String getSupportId() {
		return supportId;
	}

	public void setSupportId(String supportId) {
		this.supportId = supportId;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}

	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public String getEmailAdd() {
		return emailAdd;
	}

	public void setEmailAdd(String emailAdd) {
		this.emailAdd = emailAdd;
	}

	public List getCoordSignature() {
		return coordSignature;
	}

	public void setCoordSignature(List coordSignature) {
		this.coordSignature = coordSignature;
	}

	public List getCoordEmail() {
		return coordEmail;
	}

	public void setCoordEmail(List coordEmail) {
		this.coordEmail = coordEmail;
	}

	public String getMailStatus() {
		return mailStatus;
	}

	public void setMailStatus(String mailStatus) {
		this.mailStatus = mailStatus;
	}

	public String getMailFailure() {
		return mailFailure;
	}

	public void setMailFailure(String mailFailure) {
		this.mailFailure = mailFailure;
	}

	public String getConfirmPasswordNew() {
		return confirmPasswordNew;
	}

	public void setConfirmPasswordNew(String confirmPasswordNew) {
		this.confirmPasswordNew = confirmPasswordNew;
	}

	public static Map<String, String> getQfReason() {
		return qfReason;
	}

	public static void setQfReason(Map<String, String> qfReason) {
		QuotationManagementAction.qfReason = qfReason;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getDefaultCompanyCode() {
		return defaultCompanyCode;
	}

	public void setDefaultCompanyCode(String defaultCompanyCode) {
		this.defaultCompanyCode = defaultCompanyCode;
	}

	public String getMc() {
		return mc;
	}

	public void setMc(String mc) {
		this.mc = mc;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public Boolean getEmailTypeVOERFlag() {
		return emailTypeVOERFlag;
	}

	public void setEmailTypeVOERFlag(Boolean emailTypeVOERFlag) {
		this.emailTypeVOERFlag = emailTypeVOERFlag;
	}

	public Boolean getEmailTypeBOURFlag() {
		return emailTypeBOURFlag;
	}

	public void setEmailTypeBOURFlag(Boolean emailTypeBOURFlag) {
		this.emailTypeBOURFlag = emailTypeBOURFlag;
	}

	public String getCheckCompanyBA() {
		return checkCompanyBA;
	}

	public void setCheckCompanyBA(String checkCompanyBA) {
		this.checkCompanyBA = checkCompanyBA;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	

	public Map<String, String> getLocalParameter() {
		return localParameter;
	}

	public void setLocalParameter(Map<String, String> localParameter) {
		this.localParameter = localParameter;
	}

	public List<UserDTO> getAllUser() {
		return allUser;
	}

	public void setAllUser(List<UserDTO> allUser) {
		this.allUser = allUser;
	}

	public Long getQfId() {
		return qfId;
	}

	public void setQfId(Long qfId) {
		this.qfId = qfId;
	}

	public Map<String, LinkedHashMap<String, String>> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(
			Map<String, LinkedHashMap<String, String>> parameterMap) {
		this.parameterMap = parameterMap;
	}

	public CustomerFile getCustomerFileCombo() {
		return customerFileCombo;
	}

	public void setCustomerFileCombo(CustomerFile customerFileCombo) {
		this.customerFileCombo = customerFileCombo;
	}
	public Boolean getCheckGerman() {
		return checkGerman;
	}
	public void setCheckGerman(Boolean checkGerman) {
		this.checkGerman = checkGerman;
	}
	public Boolean getEmailTypeINTMFlag() {
		return emailTypeINTMFlag;
	}
	public void setEmailTypeINTMFlag(Boolean emailTypeINTMFlag) {
		this.emailTypeINTMFlag = emailTypeINTMFlag;
	}	
	public String getOrderForCso() {
		return orderForCso;
	}

	public void setOrderForCso(String orderForCso) {
		this.orderForCso = orderForCso;
	}

	public String getCsoSortOrder() {
		return csoSortOrder;
	}

	public void setCsoSortOrder(String csoSortOrder) {
		this.csoSortOrder = csoSortOrder;
	}

	public Boolean getCheckGermanForCustomerSave() {
		return checkGermanForCustomerSave;
	}

	public void setCheckGermanForCustomerSave(Boolean checkGermanForCustomerSave) {
		this.checkGermanForCustomerSave = checkGermanForCustomerSave;
	}

	public Boolean getCheckEnglishForCustomerSave() {
		return checkEnglishForCustomerSave;
	}

	public void setCheckEnglishForCustomerSave(Boolean checkEnglishForCustomerSave) {
		this.checkEnglishForCustomerSave = checkEnglishForCustomerSave;
	}

	public Boolean getCheckDutch() {
		return checkDutch;
	}

	public void setCheckDutch(Boolean checkDutch) {
		this.checkDutch = checkDutch;
	}

	public Boolean getCheckEnglish() {
		return checkEnglish;
	}

	public void setCheckEnglish(Boolean checkEnglish) {
		this.checkEnglish = checkEnglish;
	}

	public Boolean getEmailTypeEnglishFlag() {
		return emailTypeEnglishFlag;
	}

	public void setEmailTypeEnglishFlag(Boolean emailTypeEnglishFlag) {
		this.emailTypeEnglishFlag = emailTypeEnglishFlag;
	}

	public Boolean getCportalAccessCompanyDivision() {
		return cportalAccessCompanyDivision;
	}

	public void setCportalAccessCompanyDivision(Boolean cportalAccessCompanyDivision) {
		this.cportalAccessCompanyDivision = cportalAccessCompanyDivision;
	}

	public String getEmailSetupValue() {
		return emailSetupValue;
	}

	public void setEmailSetupValue(String emailSetupValue) {
		this.emailSetupValue = emailSetupValue;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}


	public Map<String, String> getQuoteAcceptReason() {
		return quoteAcceptReason;
	}

	public void setQuoteAcceptReason(Map<String, String> quoteAcceptReason) {
		this.quoteAcceptReason = quoteAcceptReason;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setInventoryDataAction(InventoryDataAction inventoryDataAction) {
		this.inventoryDataAction = inventoryDataAction;
	}

	public void setInventoryPathManager(InventoryPathManager inventoryPathManager) {
		this.inventoryPathManager = inventoryPathManager;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public String getOriginStateListJson() {
		return originStateListJson;
	}

	public void setOriginStateListJson(String originStateListJson) {
		this.originStateListJson = originStateListJson;
	}

	public String getDestinationStateListJson() {
		return destinationStateListJson;
	}

	public void setDestinationStateListJson(String destinationStateListJson) {
		this.destinationStateListJson = destinationStateListJson;
	}

	public String getContractDiscountAddValue() {
		return contractDiscountAddValue;
	}

	public void setContractDiscountAddValue(String contractDiscountAddValue) {
		this.contractDiscountAddValue = contractDiscountAddValue;
	}

	public Map<String, String> getCountryDsc() {
		return countryDsc;
	}

	public void setCountryDsc(Map<String, String> countryDsc) {
		this.countryDsc = countryDsc;
	}

	public boolean isCheckAccessQuotation() {
		return checkAccessQuotation;
	}

	public void setCheckAccessQuotation(boolean checkAccessQuotation) {
		this.checkAccessQuotation = checkAccessQuotation;
	}

	public String getCompDivFlag() {
		return compDivFlag;
	}

	public void setCompDivFlag(String compDivFlag) {
		this.compDivFlag = compDivFlag;
	}

	public String getCheckContractChargesMandatory() {
		return checkContractChargesMandatory;
	}

	public void setCheckContractChargesMandatory(
			String checkContractChargesMandatory) {
		this.checkContractChargesMandatory = checkContractChargesMandatory;
	}

	public List getGetRefJobTypeCountryList() {
		return getRefJobTypeCountryList;
	}

	public void setGetRefJobTypeCountryList(List getRefJobTypeCountryList) {
		this.getRefJobTypeCountryList = getRefJobTypeCountryList;
	}

	public Map<String, String> getOrderStatus1() {
		return orderStatus1;
	}

	public void setOrderStatus1(Map<String, String> orderStatus1) {
		this.orderStatus1 = orderStatus1;
	}

	
}
