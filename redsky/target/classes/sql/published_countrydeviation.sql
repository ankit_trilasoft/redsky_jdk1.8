DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`published_countrydeviation`$$
CREATE DEFINER=`root`@`%` PROCEDURE  `redsky`.`published_countrydeviation`(l_contract varchar(500), fromcorpid varchar(4), tocorpid varchar(4))
BEGIN
DECLARE bDone INT;
DECLARE l_ID INT;
DECLARE l_toID INT;


declare curs CURSOR FOR select  distinct a.id, c.id
from countrydeviation a,charges b, charges c
where contractName=l_contract and a.corpid=tocorpid and a.charge=b.id and b.contract=l_contract and b.corpid=fromcorpid
and c.corpid=tocorpid and b.charge=c.charge and b.contract=c.contract;

declare CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

OPEN curs;

  SET bDone = 0;
  REPEAT
    FETCH curs INTO l_ID,l_toID;

    update countrydeviation set charge=l_toID where id=l_ID;
   -- select l_fromID, l_toID;
  UNTIL bDone END REPEAT;

  CLOSE curs;


END $$

DELIMITER ;