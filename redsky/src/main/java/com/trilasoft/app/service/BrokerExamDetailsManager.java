package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.BrokerExamDetails;

public interface BrokerExamDetailsManager extends GenericManager<BrokerExamDetails, Long> {
	public List findByBrokerExamDetailsList(String shipNumber);
	public void updateBrokerDetailsAjaxFromList(Long id,String fieldName,String fieldValue,String sessionCorpID,String shipNumber, String userName,String fieldType);
}
