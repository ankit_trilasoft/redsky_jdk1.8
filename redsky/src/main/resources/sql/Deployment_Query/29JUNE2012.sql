ALTER TABLE `redsky`.`newsupdate` ADD COLUMN `fileName` VARCHAR(255) DEFAULT NULL AFTER `visible`;

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', '', '', 50, 'TABLE',now(),'rchandra',now(),'rchandra');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', 'Billing', 'Billing', 50, 'TABLE',now(),'rchandra',now(),'rchandra');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', 'CustomerFile', 'CustomerFile', 50, 'TABLE',now(),'rchandra',now(),'rchandra');
 
INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', 'TrackingStatus', 'TrackingStatus', 50, 'TABLE',now(),'rchandra',now(),'rchandra');
 
INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', 'ServiceOrder', 'ServiceOrder', 50, 'TABLE',now(),'rchandra',now(),'rchandra');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', 'Miscellaneous', 'Miscellaneous', 50, 'TABLE',now(),'rchandra',now(),'rchandra');

INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('TSFT', 'AccountLine', 'AccountLine', 50, 'TABLE',now(),'rchandra',now(),'rchandra');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','TABLE',50,true,now(),'rchandra',now(),'rchandra',1,1,'');
 
INSERT into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby)
values  ('STAR', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('SSCW', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('SSCW', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('TSFT', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('TSFT', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('VOER', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('VOER', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('VORU', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('VORU', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('VOCZ', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('VOCZ', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('ADAM', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('ADAM', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UTSI', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UTSI', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGHK', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGHK', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('BOUR', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('BOUR', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('JOHN', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('JOHN', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGCN', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGCN', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('VOAO', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('VOAO', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('CWMS', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('CWMS', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGMY', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGMY', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGSG', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGSG', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('BONG', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('BONG', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGWW', 'Y', 'Y', 10, 'customerfeedback',now(),'naveen',now(),'naveen'),
('UGWW', 'N', 'N', 10, 'customerfeedback',now(),'naveen',now(),'naveen');

ALTER TABLE `redsky`.`defaultaccountline` ADD COLUMN `estVatPercent` VARCHAR(7) DEFAULT '' AFTER `companyDivision`,ADD COLUMN `estVatDescr` VARCHAR(30) DEFAULT '' AFTER `estVatPercent`,ADD COLUMN `estVatAmt` DECIMAL(19,2) DEFAULT 0.00 AFTER `estVatDescr`;
