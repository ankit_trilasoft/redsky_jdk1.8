/**
 * Business Service Interface to handle communication between web and persistence layer.
 * Gets CustomerFile information based on CustomerFile id.
 * @param 			  CustomerFileId the CustomerFile's id
 * This interface represents the basic actions on "CustomerFile" object in Redsky that allows for CustomerFile management.
 * @Interface Name	  CustomerFileManager
 * @Author            Sangeeta Dwivedi
 * @Version           V01.0
 * @Since             1.0
 * @Date              01-Dec-2008
 */

package com.trilasoft.app.service;

import java.util.ArrayList; 
import java.util.Date;
import java.util.List;
import java.util.Map;  

import org.appfuse.model.User;
import org.appfuse.service.GenericManager; 

import com.trilasoft.app.dao.hibernate.dto.ScheduledSurveyDTO;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;


public interface CustomerFileManager extends GenericManager<CustomerFile, Long> {
	
	public List findByBillToCode(String billToCode, String sessionCorpID); 
    public List findByBillToCodeSQLQuery(String billToCode, String sessionCorpID); 
	public List<CustomerFile> findBySurveyDetail(String estimator, String originCity, Date survey); 
	public List<CustomerFile> findByCSODetail(String last, String first, String seqNum, String billToC, String job, String status,String coord); 
	public List findMaximum(String corpID);
	public List findMaximumId();
	public List findPortalId(String portalId);
	public List findMaximumShip(String seqNum, String job,String CorpID);
	public List findMinimumShip(String seqNum, String job,String CorpID);
	public List findCountShip(String seqNum, String job,String CorpID);
	public List findMaximumShip12(String seqNum);
	public List findAllRecords();
	public List findSequenceNumber(String seqNum);
	public List findByUserName(String userName);
	public List findCustomerDetails(String userName);
	public List findUserFullName(String userName);
	public List<User> findUserForRoleTransfer(String userName);
	public boolean findUser(String portalId);
	public boolean findUserEmail(String email);
	public List<CustomerFile> getSurveyDetails();
	public List<CustomerFile> findByCoordinator( String userName );
	public List<DataCatalog> findToolTip(String fieldName,String tableName,String corpId);
	public List<SystemDefault> findDefaultBookingAgentDetail(String corpID);
	public List findStateList(String bucket2, String corpId);
	public List findActiveStateList(String bucket2, String corpId);
	public List<CustomerFile> searchQuotationFile(String last, String first, String seqNum, String billToC, String job, String quotationStatus,String coord);
	public List<CustomerFile> customerList();
	public List<CustomerFile> findByCoordinatorQuotation( String userName );
	public List<CustomerFile> quotationList();
	public Map<String, String> findDefaultStateList(String bucket2, String corpId);
	public List<ScheduledSurveyDTO> findAllScheduledSurveys(String corpID, String consult);
	public List<ScheduledSurveyDTO> scheduledSurveys(String sessionCorpID,String consult,String surveyFrom,String surveyTo,String surveyCity,String surveyJob);
	public List driverEventList(String sessionCorpID,String eventFrom,String eventTo);
	public List<ScheduledSurveyDTO> searchAllScheduledSurveys(String sessionCorpID,String consult,String surveyFrom,String surveyTo,String surveyCity,String surveyJob);
	public List searchAllScheduledEvent(String sessionCorpID,String eventFrom,String eventTo,String driverCode,String driverFirstName,String driverlastName);
	public List findServiceOrderList(String sequenceNumber, String corpId);
	public List findPartnerQuotesList(String sequenceNumber, String corpId);
	public List findBookingCode();
	public List findDefaultBookingAgentCode(String companyDivision);
	public List findBillToCode();
	public List findCustJobContract(String billToCode, String job, Date createdon, String corpID,String companyDivision, String bookingAgentCode);
	public List findCompanyDivision(String corpId);
	public List findCompanyDivisionByJob(String corpId, String job);
	public List findCompanyDivisionByBookAg(String corpId, String bookAg);
	public int setStatus(String seqNum, String status,String statusNumber);
	public int setFnameLname(String shipNum, String firstName,String lastName);
	public List<CompanyDivision> defBookingAgent(String companyDivision,String corpID);
	public List findByCorpId(String corpID);
	public ArrayList getServiceOrderClosedStatus(String sequenceNumberForStatus,String corpID);
	public int workTicketUpdate(String shipNum, String firstName,String lastName);
	public int setFirstName(String shipNum, String firstName);
	public int setSSNo(String shipNum, String socialSecurityNumber);
	public int workTicketFirstName(String shipNum, String firstName);
	public List findCoordEmailAddress(String userName);
	public List findCoordSignature(String userName);
	public List addAndCopyCustomerFile(Long cid,String corpID);
	public void updateEmailInUser(String cPortalId, String corpID,String emailId);
	public void resetPassword(String passwordNew,String confirmPasswordNew,String email,Date compareWithDate);
	public int setSalesStatus(String seqNum);
	public ArrayList getServiceOrderCancelledStatus(String sequenceNumberForStatus,String corpID);
	public void resetAccount(String name,String corpID);
	public void resetUpdateAccount(String name,String corpID);
	public List findCustomerPortalListDetails(String userName);
	public List findCompanyName(String corpID);
	public int updateServiceOrderOriginAddress(String originAddress1, String originAddress2,String originAddress3,String originCompany,String originCountry,String originState,String originCity,String originZip,String originPreferredContact, String originDayPhone,String originDayPhoneExt,String originHomePhone,String originMobile,String originFax,String email,String email2,String originCityCode,String originCountryCode,String sequenceNumber, String userName,String linkedUpdate);
	public int updateWorkTicketOriginAddress(String originAddress1, String originAddress2,String originAddress3,String originCompany,String originCountry,String originState,String originCity,String originZip,String originPreferredContact, String originDayPhone,String originDayPhoneExt,String originHomePhone,String originMobile,String originFax,String sequenceNumber, String userName);
	public int updateServiceOrderDestinationAddress(String destinationAddress1,String destinationAddress2,String destinationAddress3,String destinationCompany,String destinationCountry,String destinationState,String destinationCity,String destinationZip,String destPreferredContact,String destinationDayPhone,String destinationDayPhoneExt,String destinationHomePhone,String destinationMobile,String destinationFax,String destinationEmail,String destinationEmail2,String destinationCityCode,String destinationCountryCode,String sequenceNumber, String userName,String linkedUpdate);
	public int updateWorkTicketDestinationAddress(String destinationAddress1,String destinationAddress2,String destinationAddress3,String destinationCompany,String destinationCountry,String destinationState,String destinationCity,String destinationZip,String destPreferredContact, String destinationDayPhone,String destinationDayPhoneExt,String destinationHomePhone,String destinationMobile,String destinationFax,String sequenceNumber, String userName,String destinationCountryCode);
	public List findCityState(String zipCode, String corpID);
	public String findCityStateFlex(String countryCodeFlex, String corpID);
	public List findCityStateNotPrimary(String zipCode, String corpID,String country);
	public List surveySchedule(String job, String corpID,String DateInterval,String fromDate,String toDate,String surveyTime,String surveyTime2,String consultant);
	public int setPrefix(String shipNum, String prefix);
	public List findsurveyScheduleList(String estimator,String surveyDate,String consultantTime,String consultantTime2,String corpID);
	public List findCordinaorListRef(String custJobType, String sessionCorpID);
	public List findEstimatorListRef(String custJobType, String sessionCorpID);
	public List findEstimatorName(String estimatorEmail, String sessionCorpID);
	public List findEstimatorFirstName(String estimatorEmail, String sessionCorpID);
	public List findUserPhone(String remoteUser, String sessionCorpID);
	public List findWebsite(String remoteUser, String sessionCorpID);
	public List surveyScheduleFromSurveyList(String job, String corpID,String DateInterval,String fromDate,String toDate,String surveyTime,String surveyTime2,String consultant,String surveyCity);
	public ArrayList getWorkTicketStatusClosedStatus(String sequenceNumberForStatus, String sessionCorpID);
	public int updateSOStatus(String sequenceNumberForStatus, String statusReas, String sessionCorpID,String userName);
	public Map<String, String> findExistingStateList(String bucket2, String corpId,String stateCode);
	public List getPartnerDitailList(String sessionCorpID, String parentAgent);
	public List getPartnerAccountmanager(String sessionCorpID, String parentAgent);
	public List getBookingAgentDetailList(String sessionCorpID, String parentAgent);
	public int updateOrderInitiationStatus(Long id, String sessionCorpID, String quotationStatus);
	public int updateOrderManagementStatus(Long id, String sessionCorpID, String quotationStatus,String orderAction,boolean accessQuotationFromCustomerFile); 
	public List getRefJobTypeCountry(String job,String sessionCorpID,String compDivision);
	public List findMaximumSequenceNumber(String externalCorpID);
	public List findMaximumShipExternal(String sequenceNumber, String externalCorpID);
	public Boolean checkNetworkRecord(String sessionCorpID,	String bookingAgentCode);
	public void updateIsNetworkFlag(Long customerFileId, String corpID, Long serviceOrderId, String contractType, Boolean isNetworkGroup);
	public List getLinkedSequenceNumber(String sequenceNumber, String recordType);
	public Long findRemoteCustomerFile(String sequenceNumber);
	public List findFieldToSync(String modalClass, String fieldType, String uniqueFieldType);
	public List findFieldToSyncCreate(String modalClass, String fieldType, String uniqueFieldType);  
	public List findAddressList(String sequenceNumber, String sessionCorpID);
	public List<ServiceOrder> findAddress(String countryName, String sessionCorpID);
	public List<ServiceOrder> findCportalResourceMgmtDoc(String seq,String sessionCorpID,String usertype);
	public List<ServiceOrder> findCustomerResourceMgmtDoc(String seq,String sessionCorpID,String usertype);
	public Boolean isNetworkedRecord(Long customerFileId);
	public List findResponsiblePerson(String custJobType, String companyDivision, String sessionCorpID);
	public List findCityStateByCountry(String zipCode, String corpID ,String country);
    public List universalSearch(String firstName, String lastName, String sessionCorpID, String sequenceNumber,
			String billToName, String job, String coordinator, String companyDivision, String registrationNumber,
			Boolean activeStatus, String status, String personPricing,
			String serviceOrderSearchVal, String usertype, String email, String accountName,
			String cityCountryZipOption, String originCityOrZip, String destinationCityOrZip, String originCountry,
			String destinationCountry);
	public String getNetworkCoordinator(String externalCorpID);
	public String getSystemWeightUnit(String sessionCorpID);
	public String getSystemVolumeUnit(String sessionCorpID);
	public List getQuotationServiceOrderlist(Long id, String sessionCorpID);
	public int updateSOStatusREOPEN(String sequenceNumber,  String sessionCorpID);
	public String findRemovalReloCheck(String customerFileId);
    public List getQuotesToGoList(Long id, String sessionCorpID,String companyDivision,String bookingAgentCode);
	public List getQuotesToValidate(String sessionCorpID,String jobCode,String companyDivision);
	public void updateCustomerFileRegNumber(String trackingCodeValue,Long customerFileId, String corpID);
	public void updateVoxmeFlag(Long id);
	public String findSurveyTool(String corpID, String job, String companyDivision);
	public List findResourceIdPassword(String bookingAgentCode,String corpId);
	public String getMessageText(String userPhone, String companyDivision, String sessionCorpID);
	public void updateServiceOrderBillToCode(String billTocode,String billToName,String sequenceNumber, String corpID);
	public void updateBillingBillToCode(String billTocode,String billToName,String sequenceNumber, String corpID);
	public int quoteReasonStatus(String corpID,Long id); 
	public Long findNewRemoteCustomerFile(String sequenceNumber, String externalCorpID);
	 public List  BookingAgentName(String partnerCode,String corpid,Boolean cmmDmmFlag,String pType );
	public List findAccFieldToSyncCreate(String modalClass, String type); 
	public List findAccFieldToSync(String modalClass, String type); 
	public List findBillingFieldToSyncCreate(String modalClass, String fieldType, String uniqueFieldType); 
	public List findBillingFieldToSync(String modalClass, String fieldType, String uniqueFieldType); 
	public List findAccDMMNetWorkFieldToSync(String modalClass, String type);
	public List getFlatFileExtraction(String fileType, String parentAgent1, String corpId,String fromDt, String toDt,String addChargesType,boolean checkFieldVisibility);
	public boolean getQualitySurvey(String corpId);
	public List<ServiceOrder> getServiceOrderList(Long id);
	public String enableStateList(String corpId);
	public String enableStateList1(String corpId);
	public List getFilterData(Long userId, String sessionCorpID);
	public List getOrderInitiationBillToCode(String sessionCorpID, String parentCode);
	public List searchOrderInitiationBillToCode(String sessionCorpID, String parentCode, String partnerCode, String description,String billingCountryCode,String billingCountry,String extReference, String billingCity);
	public List findMergeSOList(String sessionCorpID, String bookingAgentCode, Boolean checkAccessQuotation);
	public List findLinkSOtoCF(String sessionCorpID, String seqNumber);
	public List getQFLinkAccLine(String sessionCorpID, Long sid);
	public List getQFFilesId(String sessionCorpID, String fileId);
	public List getQFNotesId(String sessionCorpID, String notesId,Long notesKeyId);
	public List getQFInventoryData(String sessionCorpID, Long cid);
	public List getQuotesInventoryData(String sessionCorpID,Long customerFileId, Long sid, String mode);
	public List getQFInventoryPathList(String sessionCorpID, Long invId, Long accInfoId);
	public List getQFAccessInfo(String sessionCorpID, Long id);
    public void getUpdateBilling(String noInsurance,String seqNumber,String sessionCorpID);
	public List getServiceOrderListRemote(Long id);
	public String findPrivatepartyBilltoCode(String billToCode, String corpID);
	public List getInsuranceValue(String billToCode, String corpID);
	public Boolean checkInsurance(String billToCode, String corpID);
	public Boolean getInsurancebooking(String partnerCode,String billToCode, String corpID);
	public List findMaximumShipCF(String seqNum, String job,String CorpID);
	public List getAdditionalChargesExtraction(String fileType, String parentAgent1, String chargeCode, String corpId);
	public String findAgentparentByBillToCode(String billToCode, String corpID);
	public List findAgentAccFieldToSyncCreate(String modalClass, String type);
	public List findOrderInitiationBillToCode(String sessionCorpID,String parentCode, String bCode);
	public List getUserURL(String userRole, String sessionCorpID);
	public void updateInAllSoCordName(Long custid,String cordName,String sessionCorpID);
	public List getChildAgentCode(String parentCode, String sessionCorpID);
	public List getPartnerDetails(String parentAgent ,String sessionCorpID);
	public List findLeadAutocompleteList(String stateNameOri,String countryNameOri, String cityNameOri,String corpId);
	public List findLeadAutocompleteListDest(String stateNameDes,String countryNameDes, String cityNameDes,String corpId);
	public String findParentByAccCode(String accCode, String sessionCorpID);
	public List findLeadCaptureList(String sequenceNumber,String lastName,String firstName,Date createdOn, String job,String salesPerson,String status,Date moveDate,String serviceOrderSearchType, String sessionCorpID);
	public String findCountryNameByCode(String countyName,String sessionCorpID,String parameter);
	public String getCompanyDivisionNetworkCoordinator(String companyDivision, String externalCorpID);
	public List surveyCalendarList(String corpID,String consultant);	
	public List findPartnerDetailsForAutoComplete(String partnerName,String partnerNameType,String corpid,Boolean cmmDmmFlag);
	public List findBillDetailsForAutoComplete(String partnerName,String partnerNameType,String corpid);
	//public void updateSOExtractFieldOnCFSave(Long id);
	public void updateUpdaterOnCFSave(Long id);
	public List getEstimatorNumber(String estimator,String corpid);
	public String findDefaultValFromPartner(String bCode, String sessionCorpID);
	public void updateSODashboardSurveyDt(CustomerFile cf);
	public List findNotesValueForOutlook(String seqNumber,Long id, String sessionCorpID);
	public List getAllUserContactListByPartnerCode(String billToCode , String sessionCorpID , String name , String emailId);
    public void updateServiceOrderLinkedField(Long id, String lastName, String prefix, String rank, String firstName, String socialSecurityNumber, Boolean vip,String userName);
    public List findBilltoNameAutocompleteList(String billtoName,String corpId);
	public void updateLinkedCustomerFileStatus(String status, int statusNumber, Date statusDate, Long customerFileId,String userName);
	public String getNetworkUnit(String corpID);
	public void updateBillingNetworkBillToCode(String sequenceNumber, String accountCode, String accountName, String oldCustomerFileAccountCode);
	public List findCompanyDivisionByBookAgAndCompanyDivision(String corpId, String bookAg,String companyDivision);
	public List findCompanyDivisionWithoutBookAgAndCompanyDivision(String corpId);
	public String checkNetworkCoordinatorInUser(String cfContractType, String sessionCorpID);
	public String checkCorpIdInCompDiv(String bookAg);
	public void updateBillToName(String billToCodeVal, String billToNameVal, String sessionCorpID, Long id, String sequenceNo);
	public List findOpenCompanyDivision(String corpid);
	public Boolean findCustPortalId(String linkedSeqNumber,	String sequenceNumber);
	public String getCompanyCodeAndJobFromCompanyDivision(String sessionCorpID, String bookAg);
	public List checkPrivatePartyFromPartner(String billToCode,String sessionCorpID,String bookingAgentCode);
	public String getSurveyMessageText(String parameter, String language, String sessionCorpID);
	public String getPreffixFromRefmaster(String parameter, String language, String code);
	public String getDataUpdateDiffLanguage(String services, String corpId,String changedLanguage,Long serviceId);
	public Map<String, String> getContractAccountMap(String sessionCorpID);
	public String getContractAccountDetail(String externalCorpID, String contract);
	public List findAccEstimateRevisionFieldToSync(String modalClass, String type);
	public List findIkeaOrderPartnerDetailsForAutoComplete(String partnerName,String partnerCodes,String corpid);
	public List getIkeaInfo(String sessionCorpID,String parentCode,String partnerCode);
	public List findUserSignature(String fromEmail);
	public String findRegNumber(String corpId,Long id);
	public Map<String, String> parentAccountCodeMap(String sessionCorpID);
	public Map<String, List> childAccountCodeMap(String sessionCorpID);
	public List findAgentVendors( String parentCode);
	public List findAgentBillToCode(String parentCode, String bCode) ;
	public List searchAgentVendors(String sessionCorpID, String parentCode, String partnerCode, String description,String billingCountryCode,String billingCountry,String extReference, String billingCity);
	
}
