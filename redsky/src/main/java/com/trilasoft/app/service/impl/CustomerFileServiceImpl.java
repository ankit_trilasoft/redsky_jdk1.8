package com.trilasoft.app.service.impl;

import java.util.List;

import com.trilasoft.app.dao.CustomerFileServiceDao;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.service.CustomerFileService;

public class CustomerFileServiceImpl implements  CustomerFileService{
	
	CustomerFileServiceDao  customerFileServiceDao;
	public CustomerFileServiceImpl(){
		
	}
	public String saveCustomerFile(CustomerFile customerFile) {
		return customerFileServiceDao.saveCustomerFile(customerFile);
		
	}
	
	public void setCustomerFileServiceDao(CustomerFileServiceDao customerFileServiceDao) {
		this.customerFileServiceDao = customerFileServiceDao;
	}
	public List findMaximum(String corpId) { 
		return customerFileServiceDao.findMaximum(corpId);
	}
	public List getSurveyExternalIPsList(String corpId) { 
		return customerFileServiceDao.getSurveyExternalIPsList(corpId);
	}

}
