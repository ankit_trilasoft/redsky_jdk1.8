<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<style type="text/css">
.pr-f11{font-family:arial,verdana;font-size:11px;}
.textarea-comment {border:1px solid #219dd1;color:#000;height:17px; width:100px}
.table tbody tr:hover, .table tr.over, .contribTable tr:hover div {background-color:transparent !important;}
.hidden { display: none;}
</style>

	<s:set name="taskDetailsList" value="taskDetailsList" scope="request" />
	 <c:if test="${taskDetailsList!='[]'}">
	<display:table name="taskDetailsList" class="table" id="taskDetailsList" style="width:100%;margin-bottom:0px;">
		<display:column title="#" style="width:2%;" headerClass="containeralign" >
		<c:out value="${taskDetailsList_rowNum}"/>
		</display:column>
		<c:choose>
		<c:when test="${flag}">
		<display:column title="Task Details" style="width:25%;">
            <input type="text" class="input-textUpper pr-f11" readonly="true"
                id="details${taskDetailsList.id}" name="details"
                value="${taskDetailsList.details}" maxlength="100" style="width:250px" onchange="saveInTaskDetails(this,'details','${taskDetailsList.corpId}','${taskDetailsList.id}','${taskDetailsList.workorder}','${taskDetailsList.shipNumber}')"/>
        </display:column>
		 
		 <display:column title="Category" style="width:20%;">
             <input type="text" class="input-textUpper pr-f11"
             id="category${taskDetailsList.id}" name="category" readonly="true"
             value="${taskDetailsList.category}" style="width:70px" />
         </display:column>
		 
		 <c:if test="${taskDetailsList.status == 'Transfer'}"> 
		<display:column title="Status" style="width:20%;">
   <input type="text" class="input-textUpper pr-f11"
                id="status${taskDetailsList.id}" name="status" readonly="true"
                value="${taskDetailsList.status}" style="width:70px" />
   &nbsp;&nbsp;<input type="text" class="input-textUpper pr-f11"
                id="wo${taskDetailsList.id}" name="workorder" readonly="true" 
                value="${taskDetailsList.workorder}" style="width:70px" />
		</display:column>
</c:if>   
    
     <c:if test="${taskDetailsList.status != 'Transfer'}"> 
        <display:column title="Status" style="width:5%;">
   <input type="text" class="input-textUpper pr-f11"
                id="status${taskDetailsList.id}" name="status" readonly="true"
                value="${taskDetailsList.status}" style="width:70px" />
                </display:column>
            </c:if>   

		<display:column title='Date' style="width:60px;">
			<fmt:parseDate pattern="yyyy-MM-dd" value="${taskDetailsList.date}"
				var="parsedAtDepartDate" />
			<fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtDepartDate}"
				var="formattedAtDepartDate" /> 
			<input type="text" class="input-textUpper pr-f11" id="date${taskDetailsList.id}" name="date${taskDetailsList.id}" value="${formattedAtDepartDate}" onkeydown="return onlyDel(event,this)" required="true" style="width:60px" maxlength="11" readonly="true"  />
			<%-- <img id="date${taskDetailsList.id}-trigger"	style="vertical-align: bottom" onclick="setFieldId('${taskDetailsList.id}','${taskDetailsList.corpId}')" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /> --%>
		</display:column>

		<display:column title='Time' style="width:83px;">
			<input type="text" class="input-textUpper pr-f11" cssStyle="text-align:right" readonly="true"
				name="startTime" id="startTime${taskDetailsList.id}" size="5"
				maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" value="${taskDetailsList.time}"
				onchange="completeTimeString('${taskDetailsList.id}');saveInTaskDetails(this,'time','${taskDetailsList.corpId}','${taskDetailsList.id}','${taskDetailsList.workorder}','${taskDetailsList.shipNumber}')" />
			<img src="${pageContext.request.contextPath}/images/time.png"
				HEIGHT=20 WIDTH=40 align="top" style="cursor: default;">
		</display:column>
     <display:column  title="Comment" style="width:35px;"> 
    <textarea class="input-textUpper pr-f11" style="width:250px;" id="comment${taskDetailsList.id}" title="<c:out value="${taskDetailsList.comment}"></c:out>" maxlength="200" name="comment${taskDetailsList.id}" readonly="true"><c:out value='${taskDetailsList.comment}'/></textarea>
 </display:column>
 </c:when>
<c:otherwise>
<display:column title="Task Details" style="width:22%;">
            <input type="text" class="input-text pr-f11"
                id="details${taskDetailsList.id}" name="details"
                value="${taskDetailsList.details}" maxlength="100" style="width:250px" onchange="saveInTaskDetails(this,'details','${taskDetailsList.corpId}','${taskDetailsList.id}','${taskDetailsList.workorder}','${taskDetailsList.shipNumber}','${currentId}')"/>
        </display:column>
        <display:column title="Category" style="width:8%;">
            <select id="category${taskDetailsList.id}" name ="category${taskDetailsList.id}" style="width:90px" class="list-menu pr-f11"  
                onchange="saveInTaskDetails(this,'category','${taskDetailsList.corpId}','${taskDetailsList.id}','${taskDetailsList.workorder}','${taskDetailsList.shipNumber}','${currentId}')" > 
               <option value=""></option>
                <c:forEach var="chrms" items="${category}" varStatus="loopStatus">
                <c:choose>
                <c:when test="${chrms.key == taskDetailsList.category}">
                <c:set var="selectedInd" value=" selected"></c:set>
                </c:when>
                <c:otherwise>
                <c:set var="selectedInd" value=""></c:set>
                </c:otherwise>
                </c:choose>
                <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                <c:out value="${chrms.value}"></c:out>
                </option>
                </c:forEach> 
                </select>
        </display:column>
        
      <c:if test="${(taskDetailsList.status == 'Transfer')}"> 
     <display:column title="Status" style="width:5%;">
        <c:if test="${(taskDetailsList.workorder==null || taskDetailsList.workorder== '')}">
         <select id="status${taskDetailsList.id}" name ="status${taskDetailsList.id}" style="width:75px" class="list-menu pr-f11" 
                onchange="saveInTaskDetails(this,'status','${taskDetailsList.corpId}','${taskDetailsList.id}','${taskDetailsList.workorder}','${taskDetailsList.shipNumber}','${currentId}')" > 
			    <c:forEach var="chrms" items="${taskStatus}" varStatus="loopStatus">
			    <c:choose>
			    <c:when test="${chrms.key == taskDetailsList.status}">
			    <c:set var="selectedInd" value=" selected"></c:set>
			    </c:when>
			    <c:otherwise>
			    <c:set var="selectedInd" value=""></c:set>
			    </c:otherwise>
			    </c:choose>
			    <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			    <c:out value="${chrms.value}"></c:out>
			    </option>
			    </c:forEach> 
			    </select>
			     &nbsp;&nbsp;<select id="wo${taskDetailsList.id}" name ="wo${taskDetailsList.id}" style="width:75px" class="list-menu pr-f11" 
			                onchange="saveInTaskDetails(this,'workorder','${taskDetailsList.corpId}','${taskDetailsList.id}','${taskDetailsList.workorder}','${taskDetailsList.shipNumber}','${currentId}')"> 
			    <option value=""></option>
			    <c:forEach var="tempData" items="${distictWOList}" varStatus="loopStatus">
			    <option>
			    <c:out value="${tempData}"></c:out>
			    </option>
			    </c:forEach> 
			    </select>
        </c:if> 
        <c:if test="${(taskDetailsList.workorder !=null && taskDetailsList.workorder !='')}">
        <input type="text" class="input-textUpper pr-f11"
                id="status${taskDetailsList.id}" name="status" readonly="true"
                value="${taskDetailsList.status}" style="width:70px" />
   &nbsp;&nbsp;<input type="text" class="input-textUpper pr-f11"
                id="wo${taskDetailsList.id}" name="workorder" readonly="true"
                value="${taskDetailsList.workorder}" style="width:70px" />
        </c:if>
        </display:column>
           </c:if>  
    
     <c:if test="${(taskDetailsList.status != 'Transfer')}">  
     <display:column title="Status" style="width:5%;">
				    <select id="status${taskDetailsList.id}" name ="status${taskDetailsList.id}" style="width:75px" class="list-menu pr-f11" 
				                onchange="saveInTaskDetails(this,'status','${taskDetailsList.corpId}','${taskDetailsList.id}','${taskDetailsList.workorder}','${taskDetailsList.shipNumber}','${currentId}')" > 
				    <c:forEach var="chrms" items="${taskStatus}" varStatus="loopStatus">
				    <c:choose>
				    <c:when test="${chrms.key == taskDetailsList.status}">
				    <c:set var="selectedInd" value=" selected"></c:set>
				    </c:when>
				    <c:otherwise>
				    <c:set var="selectedInd" value=""></c:set>
				    </c:otherwise>
				    </c:choose>
				    <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
				    <c:out value="${chrms.value}"></c:out>
				    </option>
				    </c:forEach> 
				    </select>
                <input type="hidden" id="wo${taskDetailsList.id}" name="workorder" value="${taskDetailsList.workorder}"> 
      </display:column>
        </c:if>
        <display:column title='Date' style="width:60px;">
            <fmt:parseDate pattern="yyyy-MM-dd" value="${taskDetailsList.date}"
                var="parsedAtDepartDate" />
            <fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtDepartDate}"
                var="formattedAtDepartDate" /> 
            <input type="text" disabled="true" class="input-textUpper pr-f11" id="date${taskDetailsList.id}" name="date${taskDetailsList.id}" value="${formattedAtDepartDate}" onkeydown="return onlyDel(event,this)" required="true" style="width:60px" maxlength="11" readonly="true"  />
            <%-- <img id="date${taskDetailsList.id}-trigger"    style="vertical-align: bottom" onclick="setFieldId('${taskDetailsList.id}','${taskDetailsList.corpId}')" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 /> --%>
        </display:column>

        <display:column title='Time' style="width:83px;">
            <input type="text" class="input-textUpper pr-f11" cssStyle="text-align:right" readonly="true" disabled="true"
                name="startTime" id="startTime${taskDetailsList.id}" size="5"
                maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" value="${taskDetailsList.time}"
                onchange="completeTimeString('${taskDetailsList.id}');saveInTaskDetails(this,'time','${taskDetailsList.corpId}','${taskDetailsList.id}','${taskDetailsList.workorder}','${taskDetailsList.shipNumber}')" />
            <img src="${pageContext.request.contextPath}/images/time.png"
                HEIGHT=20 WIDTH=40 align="top" style="cursor: default;">
        </display:column>
     <display:column  title="Comment" style="width:35px;"> 
    <textarea class="textarea-comment pr-f11" style="width:250px;" id="comment${taskDetailsList.id}" title="<c:out value="${taskDetailsList.comment}"></c:out>" maxlength="200" name="comment${taskDetailsList.id}" onchange="saveInTaskDetails(this,'comment','${taskDetailsList.corpId}','${taskDetailsList.id}','${taskDetailsList.workorder}','${taskDetailsList.shipNumber}','${currentId}')" onmouseover="" onmouseout="checkMsg(this);" onclick="checkMsg1(this)"><c:out value='${taskDetailsList.comment}'/></textarea>
 </display:column>
 
 
 
   <display:column title="Audit" style="text-align:center;width:30px;"> 
    <a><img align="middle" onclick="auditSetUp('${taskDetailsList.id}');" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/report-ext.png"/></a>
    </display:column>
</c:otherwise>
</c:choose>
	</display:table>
	</c:if>
	 <c:if test="${taskDetailsList =='[]'}">
	<display:table name="taskDetailsList" class="table" id="taskDetailsList" style="width:100%;margin-bottom:0px;">
	 <display:column property="details" title="Task Details" style="width:25%;" />
	 <display:column property="category" title="Category" style="width:20%;" />
        <display:column property="status" title="Status" style="width:20%;" />
            <display:column property="date" title='Date' style="width:60px;" />
        <display:column property="time" title='Time' style="width:83px;" />
     <display:column property="comment" title="Comment" style="width:35px;" /> 
	 </display:table>
	 </c:if>
        <c:choose>
        <c:when test="${flag}">
		</c:when>
		<c:otherwise>
		<input type="button" name="add" value="Add"
        onclick="addTaskDetails('${ticket}','${workTicketId}','${workorder}','${shipNumber}','${currentId}');"
        class="update-btn-OI" style="width: 60px; margin:5px 0px;"
        tabindex="" />
		</c:otherwise>
		</c:choose>