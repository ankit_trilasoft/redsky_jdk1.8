<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
       <title> Redsky Billng</title> 
    	<meta name="heading" content="Redsky Billng"/> 
 </head>
 
 <s:form id="billingList" action="" method="post" validate="true">
 	<display:table name="billingList" requestURI="" defaultsort="2" id="billingList" class="table" pagesize="50" style="width:100%;margin-top: 1px;" >  
    <display:column property="corpID" escapeXml="true" sortable="true" title="CorpID" style="width: 5%; cursor: default;"/>
    <display:column property="companyDivision" escapeXml="true" sortable="true" title="Divn" style="width: 2%; cursor: default;"/>
    <display:column property="job" escapeXml="true" sortable="true" title="Job" style="width: 5%; cursor: default;"/>
    <display:column property="routing" escapeXml="true" sortable="true" title="Routing" style="width: 5%; cursor: default;"/>
    <display:column property="redskyBillDate" escapeXml="true" sortable="true" title="RS&nbsp;Bill&nbsp;Date" style="width: 10%; cursor: default;"/>
    <display:column property="shipNumber" escapeXml="true" sortable="true" title="SO#" style="width: 8%; cursor: default;"/>
    <display:column property="shipper" escapeXml="true" sortable="true" title="Shipper" style="width: 10%; cursor: default;"/>
    <display:column property="billtoName" escapeXml="true" sortable="true" title="Biller" style="width: 10%; cursor: default;"/>
    <display:column property="packingA" escapeXml="true" sortable="true" title="Packing Date" style="width: 4%; cursor: default;"/>
    <display:column property="loadA" escapeXml="true" sortable="true" title="Load Date" style="width: 4%; cursor: default;"/>
    <display:column property="deliveryA" escapeXml="true" sortable="true" title="Delivery Date" style="width: 4%; cursor: default;"/>
    <display:column property="invoiceNumber" escapeXml="true" sortable="true" title="Invoice Number" style="width: 4%; cursor: default;"/>
    <display:column property="invoiceDate" escapeXml="true" sortable="true" title="Invoice Date" style="width: 4%; cursor: default;"/>
    </display:table>  
    </s:form>    
          