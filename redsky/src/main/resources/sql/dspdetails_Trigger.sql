DELIMITER $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_dspdetails` $$
create trigger redsky.trigger_add_history_dspdetails BEFORE UPDATE on redsky.dspdetails
for each row BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  DECLARE unit LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  SET unit = " ";

IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'dspdetails.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;

IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
      select CONCAT(fieldNameList,'dspdetails.serviceOrderId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
  END IF;
IF (OLD.shipNumber <> NEW.shipNumber ) THEN
      select CONCAT(fieldNameList,'dspdetails.shipNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
  END IF;
IF (OLD.RNT_comment <> NEW.RNT_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_comment,'~') into newValueList;
  END IF;
IF (OLD.LAN_childrenDaysAuthorized <> NEW.LAN_childrenDaysAuthorized ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_childrenDaysAuthorized~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_childrenDaysAuthorized,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_childrenDaysAuthorized,'~') into newValueList;
  END IF;
IF (OLD.TAC_monthlyRentalAllowance <> NEW.TAC_monthlyRentalAllowance ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_monthlyRentalAllowance~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_monthlyRentalAllowance,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_monthlyRentalAllowance,'~') into newValueList;
  END IF;
IF (OLD.TAC_securityDeposit <> NEW.TAC_securityDeposit ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_securityDeposit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_securityDeposit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_securityDeposit,'~') into newValueList;
  END IF;
IF (OLD.TAC_negotiatedRent <> NEW.TAC_negotiatedRent ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_negotiatedRent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_negotiatedRent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_negotiatedRent,'~') into newValueList;
  END IF;
  IF (OLD.TEN_exceptionAddedValue <> NEW.TEN_exceptionAddedValue ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_exceptionAddedValue~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_exceptionAddedValue,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_exceptionAddedValue,'~') into newValueList;
  END IF;
IF (OLD.TAC_utilitiesIncluded <> NEW.TAC_utilitiesIncluded ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_utilitiesIncluded~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_utilitiesIncluded,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_utilitiesIncluded,'~') into newValueList;
  END IF;
IF (OLD.TAC_depositPaidBy <> NEW.TAC_depositPaidBy ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_depositPaidBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_depositPaidBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_depositPaidBy,'~') into newValueList;
  END IF;
IF (OLD.TAC_comment <> NEW.TAC_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_comment,'~') into newValueList;
  END IF;
IF (OLD.ONG_dateType1 <> NEW.ONG_dateType1 ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_dateType1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_dateType1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_dateType1,'~') into newValueList;
  END IF;
IF (OLD.ONG_dateType2 <> NEW.ONG_dateType2 ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_dateType2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_dateType2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_dateType2,'~') into newValueList;
  END IF;
IF (OLD.ONG_dateType3 <> NEW.ONG_dateType3 ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_dateType3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_dateType3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_dateType3,'~') into newValueList;
  END IF;
IF (OLD.ONG_comment <> NEW.ONG_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_comment,'~') into newValueList;
  END IF;
IF (OLD.SET_vendorCode <> NEW.SET_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SET_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SET_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.SET_vendorName <> NEW.SET_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SET_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SET_vendorName,'~') into newValueList;
  END IF;
IF (OLD.SET_vendorContact <> NEW.SET_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SET_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SET_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.SET_vendorEmail <> NEW.SET_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SET_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SET_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.SET_comment <> NEW.SET_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SET_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SET_comment,'~') into newValueList;
  END IF;
IF (OLD.RPT_damageCost <> NEW.RPT_damageCost ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_damageCost~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_damageCost,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_damageCost,'~') into newValueList;
  END IF;
IF (OLD.RPT_securityDeposit <> NEW.RPT_securityDeposit ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_securityDeposit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_securityDeposit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_securityDeposit,'~') into newValueList;
  END IF;
IF (OLD.RPT_depositReturnedTo <> NEW.RPT_depositReturnedTo ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_depositReturnedTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_depositReturnedTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_depositReturnedTo,'~') into newValueList;
  END IF;
IF (OLD.SCH_city <> NEW.SCH_city ) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCH_city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCH_city,'~') into newValueList;
  END IF;
IF (OLD.SCH_noOfChildren <> NEW.SCH_noOfChildren ) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_noOfChildren~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCH_noOfChildren,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCH_noOfChildren,'~') into newValueList;
  END IF;
IF (OLD.VIS_assignees <> NEW.VIS_assignees ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_assignees~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_assignees,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_assignees,'~') into newValueList;
  END IF;
IF (OLD.VIS_employers <> NEW.VIS_employers ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_employers~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_employers,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_employers,'~') into newValueList;
  END IF;
IF (OLD.VIS_convenant <> NEW.VIS_convenant ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_convenant~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_convenant,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_convenant,'~') into newValueList;
  END IF;
IF (OLD.VIS_residencePermitHolderNameRP <> NEW.VIS_residencePermitHolderNameRP ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_residencePermitHolderNameRP~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_residencePermitHolderNameRP,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_residencePermitHolderNameRP,'~') into newValueList;
  END IF;
IF (OLD.VIS_primaryEmail <> NEW.VIS_primaryEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_primaryEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_primaryEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_primaryEmail,'~') into newValueList;
  END IF;
IF (OLD.RNT_paidCurrency <> NEW.RNT_paidCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_paidCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_paidCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_paidCurrency,'~') into newValueList;
  END IF;
IF (OLD.TRG_employeeTime <> NEW.TRG_employeeTime ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_employeeTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_employeeTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_employeeTime,'~') into newValueList;
  END IF;
IF (OLD.TRG_spouseTime <> NEW.TRG_spouseTime ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_spouseTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_spouseTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_spouseTime,'~') into newValueList;
  END IF;
IF (OLD.LAN_employeeTime <> NEW.LAN_employeeTime ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_employeeTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_employeeTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_employeeTime,'~') into newValueList;
  END IF;
IF (OLD.LAN_spouseTime <> NEW.LAN_spouseTime ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_spouseTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_spouseTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_spouseTime,'~') into newValueList;
  END IF;
IF (OLD.LAN_childrenTime <> NEW.LAN_childrenTime ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_childrenTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_childrenTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_childrenTime,'~') into newValueList;
  END IF;
IF (OLD.CAR_vendorCode <> NEW.CAR_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAR_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAR_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.CAR_vendorName <> NEW.CAR_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAR_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAR_vendorName,'~') into newValueList;
  END IF;
IF (OLD.CAR_vendorContact <> NEW.CAR_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAR_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAR_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.CAR_vendorEmail <> NEW.CAR_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAR_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAR_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.COL_vendorCode <> NEW.COL_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.COL_vendorName <> NEW.COL_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_vendorName,'~') into newValueList;
  END IF;
IF (OLD.COL_vendorContact <> NEW.COL_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.COL_vendorEmail <> NEW.COL_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.TRG_vendorCode <> NEW.TRG_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.TRG_vendorName <> NEW.TRG_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_vendorName,'~') into newValueList;
  END IF;
IF (OLD.TRG_vendorContact <> NEW.TRG_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.TRG_vendorEmail <> NEW.TRG_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.TRG_employeeDaysAuthorized <> NEW.TRG_employeeDaysAuthorized ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_employeeDaysAuthorized~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_employeeDaysAuthorized,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_employeeDaysAuthorized,'~') into newValueList;
  END IF;
IF (OLD.TRG_spouseDaysAuthorized <> NEW.TRG_spouseDaysAuthorized ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_spouseDaysAuthorized~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_spouseDaysAuthorized,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_spouseDaysAuthorized,'~') into newValueList;
  END IF;
IF (OLD.HOM_vendorCode <> NEW.HOM_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.HOM_vendorName <> NEW.HOM_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_vendorName,'~') into newValueList;
  END IF;
IF (OLD.HOM_vendorContact <> NEW.HOM_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.HOM_vendorEmail <> NEW.HOM_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.HOM_viewStatus <> NEW.HOM_viewStatus ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_viewStatus~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_viewStatus,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_viewStatus,'~') into newValueList;
  END IF;
IF (OLD.RNT_vendorCode <> NEW.RNT_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.RNT_vendorName <> NEW.RNT_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_vendorName,'~') into newValueList;
  END IF;
IF (OLD.RNT_vendorContact <> NEW.RNT_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.RNT_vendorEmail <> NEW.RNT_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.RNT_monthlyRentalAllowance <> NEW.RNT_monthlyRentalAllowance ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_monthlyRentalAllowance~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_monthlyRentalAllowance,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_monthlyRentalAllowance,'~') into newValueList;
  END IF;
IF (OLD.RNT_negotiatedRent <> NEW.RNT_negotiatedRent ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_negotiatedRent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_negotiatedRent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_negotiatedRent,'~') into newValueList;
  END IF;
IF (OLD.RNT_utilitiesIncluded <> NEW.RNT_utilitiesIncluded ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_utilitiesIncluded~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_utilitiesIncluded,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_utilitiesIncluded,'~') into newValueList;
  END IF;
IF (OLD.RNT_securityDeposit <> NEW.RNT_securityDeposit ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_securityDeposit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_securityDeposit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_securityDeposit,'~') into newValueList;
  END IF;
IF (OLD.RNT_depositPaidBy <> NEW.RNT_depositPaidBy ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_depositPaidBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_depositPaidBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_depositPaidBy,'~') into newValueList;
  END IF;
IF (OLD.LAN_vendorCode <> NEW.LAN_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.LAN_vendorName <> NEW.LAN_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_vendorName,'~') into newValueList;
  END IF;
IF (OLD.LAN_vendorContact <> NEW.LAN_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.LAN_vendorEmail <> NEW.LAN_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.LAN_employeeDaysAuthorized <> NEW.LAN_employeeDaysAuthorized ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_employeeDaysAuthorized~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_employeeDaysAuthorized,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_employeeDaysAuthorized,'~') into newValueList;
  END IF;
IF (OLD.LAN_spouseDaysAuthorized <> NEW.LAN_spouseDaysAuthorized ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_spouseDaysAuthorized~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_spouseDaysAuthorized,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_spouseDaysAuthorized,'~') into newValueList;
  END IF;
IF (OLD.MMG_vendorCode <> NEW.MMG_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.MMG_vendorName <> NEW.MMG_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_vendorName,'~') into newValueList;
  END IF;
IF (OLD.MMG_vendorContact <> NEW.MMG_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.MMG_vendorEmail <> NEW.MMG_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.ONG_vendorCode <> NEW.ONG_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.ONG_vendorName <> NEW.ONG_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_vendorName,'~') into newValueList;
  END IF;
IF (OLD.ONG_vendorContact <> NEW.ONG_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.ONG_vendorEmail <> NEW.ONG_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.PRV_vendorCode <> NEW.PRV_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.PRV_vendorName <> NEW.PRV_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_vendorName,'~') into newValueList;
  END IF;
IF (OLD.PRV_vendorContact <> NEW.PRV_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.PRV_vendorEmail <> NEW.PRV_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.PRV_hotelDetails <> NEW.PRV_hotelDetails ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_hotelDetails~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_hotelDetails,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_hotelDetails,'~') into newValueList;
  END IF;
IF (OLD.PRV_areaOrientationDays <> NEW.PRV_areaOrientationDays ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_areaOrientationDays~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_areaOrientationDays,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_areaOrientationDays,'~') into newValueList;
  END IF;
IF (OLD.PRV_flightNumber <> NEW.PRV_flightNumber ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_flightNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_flightNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_flightNumber,'~') into newValueList;
  END IF;
IF (OLD.PRV_flightArrivalTime <> NEW.PRV_flightArrivalTime ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_flightArrivalTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_flightArrivalTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_flightArrivalTime,'~') into newValueList;
  END IF;
IF (OLD.AIO_airlineFlight <> NEW.AIO_airlineFlight ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_airlineFlight~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_airlineFlight,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_airlineFlight,'~') into newValueList;
  END IF;
IF (OLD.AIO_contactInformation <> NEW.AIO_contactInformation ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_contactInformation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_contactInformation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_contactInformation,'~') into newValueList;
  END IF;
IF (OLD.AIO_serviceAuthorized <> NEW.AIO_serviceAuthorized ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_serviceAuthorized~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_serviceAuthorized,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_serviceAuthorized,'~') into newValueList;
  END IF;
IF (OLD.AIO_vendorCode <> NEW.AIO_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.AIO_vendorName <> NEW.AIO_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_vendorName,'~') into newValueList;
  END IF;
IF (OLD.AIO_vendorContact <> NEW.AIO_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.AIO_vendorEmail <> NEW.AIO_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.EXP_vendorCode <> NEW.EXP_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EXP_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EXP_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.EXP_vendorName <> NEW.EXP_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EXP_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EXP_vendorName,'~') into newValueList;
  END IF;
IF (OLD.EXP_vendorContact <> NEW.EXP_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EXP_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EXP_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.EXP_vendorEmail <> NEW.EXP_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EXP_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EXP_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.EXP_paidStatus <> NEW.EXP_paidStatus ) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_paidStatus~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EXP_paidStatus,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EXP_paidStatus,'~') into newValueList;
  END IF;
IF (OLD.RPT_vendorCode <> NEW.RPT_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.RPT_vendorName <> NEW.RPT_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_vendorName,'~') into newValueList;
  END IF;
IF (OLD.RPT_vendorContact <> NEW.RPT_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.RPT_vendorEmail <> NEW.RPT_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.RPT_comment <> NEW.RPT_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_comment,'~') into newValueList;
  END IF;
IF (OLD.SCH_schoolSelected <> NEW.SCH_schoolSelected ) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_schoolSelected~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCH_schoolSelected,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCH_schoolSelected,'~') into newValueList;
  END IF;
IF (OLD.SCH_schoolName <> NEW.SCH_schoolName ) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_schoolName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCH_schoolName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCH_schoolName,'~') into newValueList;
  END IF;
IF (OLD.SCH_contactPerson <> NEW.SCH_contactPerson ) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_contactPerson~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCH_contactPerson,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCH_contactPerson,'~') into newValueList;
  END IF;
IF (OLD.SCH_website <> NEW.SCH_website ) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_website~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCH_website,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCH_website,'~') into newValueList;
  END IF;
IF (OLD.TAX_vendorCode <> NEW.TAX_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAX_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAX_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.TAX_vendorName <> NEW.TAX_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAX_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAX_vendorName,'~') into newValueList;
  END IF;
IF (OLD.TAX_vendorContact <> NEW.TAX_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAX_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAX_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.TAX_vendorEmail <> NEW.TAX_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAX_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAX_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.TAC_vendorCode <> NEW.TAC_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.TAC_vendorName <> NEW.TAC_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_vendorName,'~') into newValueList;
  END IF;
IF (OLD.TAC_vendorContact <> NEW.TAC_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.TAC_vendorEmail <> NEW.TAC_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.TAC_monthsWeeksDaysAuthorized <> NEW.TAC_monthsWeeksDaysAuthorized ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_monthsWeeksDaysAuthorized~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_monthsWeeksDaysAuthorized,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_monthsWeeksDaysAuthorized,'~') into newValueList;
  END IF;
IF (OLD.TEN_vendorCode <> NEW.TEN_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.TEN_vendorName <> NEW.TEN_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_vendorName,'~') into newValueList;
  END IF;
IF (OLD.TEN_vendorContact <> NEW.TEN_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.TEN_vendorEmail <> NEW.TEN_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.VIS_vendorCode <> NEW.VIS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.VIS_vendorName <> NEW.VIS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_vendorName,'~') into newValueList;
  END IF;
IF (OLD.VIS_vendorContact <> NEW.VIS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.VIS_vendorEmail <> NEW.VIS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.VIS_workPermitVisaHolderName <> NEW.VIS_workPermitVisaHolderName ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_workPermitVisaHolderName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_workPermitVisaHolderName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_workPermitVisaHolderName,'~') into newValueList;
  END IF;
IF (OLD.CAR_vendorCodeEXSO <> NEW.CAR_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAR_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAR_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.COL_vendorCodeEXSO <> NEW.COL_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.TRG_vendorCodeEXSO <> NEW.TRG_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.HOM_vendorCodeEXSO <> NEW.HOM_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.RNT_vendorCodeEXSO <> NEW.RNT_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.LAN_vendorCodeEXSO <> NEW.LAN_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.MMG_vendorCodeEXSO <> NEW.MMG_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.ONG_vendorCodeEXSO <> NEW.ONG_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.PRV_vendorCodeEXSO <> NEW.PRV_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.AIO_vendorCodeEXSO <> NEW.AIO_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.EXP_vendorCodeEXSO <> NEW.EXP_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EXP_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EXP_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.RPT_vendorCodeEXSO <> NEW.RPT_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.SCH_vendorCodeEXSO <> NEW.SCH_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCH_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCH_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.TAX_vendorCodeEXSO <> NEW.TAX_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAX_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAX_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.TAC_vendorCodeEXSO <> NEW.TAC_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.TEN_vendorCodeEXSO <> NEW.TEN_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.VIS_vendorCodeEXSO <> NEW.VIS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.SET_vendorCodeEXSO <> NEW.SET_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SET_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SET_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.COL_estimatedAllowanceEuro <> NEW.COL_estimatedAllowanceEuro ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_estimatedAllowanceEuro~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_estimatedAllowanceEuro,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_estimatedAllowanceEuro,'~') into newValueList;
  END IF;
IF (OLD.CAR_comment <> NEW.CAR_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAR_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAR_comment,'~') into newValueList;
  END IF;
IF (OLD.COL_comment <> NEW.COL_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_comment,'~') into newValueList;
  END IF;
IF (OLD.TRG_comment <> NEW.TRG_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_comment,'~') into newValueList;
  END IF;
IF (OLD.HOM_comment <> NEW.HOM_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_comment,'~') into newValueList;
  END IF;
IF (OLD.LAN_comment <> NEW.LAN_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_comment,'~') into newValueList;
  END IF;
IF (OLD.MMG_comment <> NEW.MMG_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_comment,'~') into newValueList;
  END IF;
IF (OLD.PRV_comment <> NEW.PRV_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_comment,'~') into newValueList;
  END IF;
IF (OLD.AIO_comment <> NEW.AIO_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_comment,'~') into newValueList;
  END IF;
IF (OLD.EXP_comment <> NEW.EXP_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EXP_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EXP_comment,'~') into newValueList;
  END IF;
IF (OLD.SCH_comment <> NEW.SCH_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCH_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCH_comment,'~') into newValueList;
  END IF;
IF (OLD.TAX_comment <> NEW.TAX_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAX_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAX_comment,'~') into newValueList;
  END IF;
IF (OLD.TEN_comment <> NEW.TEN_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_comment,'~') into newValueList;
  END IF;
IF (OLD.VIS_comment <> NEW.VIS_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_comment,'~') into newValueList;
  END IF;
IF (OLD.WOP_vendorCode <> NEW.WOP_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.WOP_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.WOP_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.WOP_vendorName <> NEW.WOP_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.WOP_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.WOP_vendorName,'~') into newValueList;
  END IF;
IF (OLD.WOP_vendorContact <> NEW.WOP_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.WOP_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.WOP_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.WOP_vendorEmail <> NEW.WOP_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.WOP_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.WOP_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.WOP_workPermitHolderName <> NEW.WOP_workPermitHolderName ) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_workPermitHolderName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.WOP_workPermitHolderName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.WOP_workPermitHolderName,'~') into newValueList;
  END IF;
IF (OLD.REP_vendorCodeEXSO <> NEW.REP_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.REP_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.REP_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.RLS_vendorCode <> NEW.RLS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.RLS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RLS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RLS_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.RLS_vendorName <> NEW.RLS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.RLS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RLS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RLS_vendorName,'~') into newValueList;
  END IF;
IF (OLD.RLS_vendorCodeEXSO <> NEW.RLS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.RLS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RLS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RLS_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.RLS_vendorContact <> NEW.RLS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.RLS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RLS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RLS_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.RLS_vendorEmail <> NEW.RLS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.RLS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RLS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RLS_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.CAT_vendorCode <> NEW.CAT_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAT_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAT_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.CAT_vendorName <> NEW.CAT_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAT_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAT_vendorName,'~') into newValueList;
  END IF;
IF (OLD.CAT_vendorCodeEXSO <> NEW.CAT_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAT_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAT_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.CAT_vendorContact <> NEW.CAT_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAT_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAT_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.CAT_vendorEmail <> NEW.CAT_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAT_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAT_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.CLS_vendorCode <> NEW.CLS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CLS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CLS_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.CLS_vendorName <> NEW.CLS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CLS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CLS_vendorName,'~') into newValueList;
  END IF;
IF (OLD.CLS_vendorCodeEXSO <> NEW.CLS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CLS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CLS_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.CLS_vendorContact <> NEW.CLS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CLS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CLS_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.CLS_vendorEmail <> NEW.CLS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CLS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CLS_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.CHS_vendorCode <> NEW.CHS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CHS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CHS_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.CHS_vendorName <> NEW.CHS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CHS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CHS_vendorName,'~') into newValueList;
  END IF;
IF (OLD.CHS_vendorCodeEXSO <> NEW.CHS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CHS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CHS_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.CHS_vendorContact <> NEW.CHS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CHS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CHS_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.CHS_vendorEmail <> NEW.CHS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CHS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CHS_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.DPS_vendorCode <> NEW.DPS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DPS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DPS_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.DPS_vendorName <> NEW.DPS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DPS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DPS_vendorName,'~') into newValueList;
  END IF;
IF (OLD.DPS_vendorCodeEXSO <> NEW.DPS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DPS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DPS_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.DPS_vendorContact <> NEW.DPS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DPS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DPS_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.DPS_vendorEmail <> NEW.DPS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DPS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DPS_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.HSM_vendorCode <> NEW.HSM_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.HSM_vendorName <> NEW.HSM_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_vendorName,'~') into newValueList;
  END IF;
IF (OLD.HSM_vendorCodeEXSO <> NEW.HSM_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.HSM_vendorContact <> NEW.HSM_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.HSM_vendorEmail <> NEW.HSM_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.PDT_vendorCode <> NEW.PDT_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PDT_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PDT_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.PDT_vendorName <> NEW.PDT_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PDT_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PDT_vendorName,'~') into newValueList;
  END IF;
IF (OLD.PDT_vendorCodeEXSO <> NEW.PDT_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PDT_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PDT_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.PDT_vendorContact <> NEW.PDT_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PDT_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PDT_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.PDT_vendorEmail <> NEW.PDT_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PDT_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PDT_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.RCP_vendorCode <> NEW.RCP_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RCP_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RCP_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.RCP_vendorName <> NEW.RCP_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RCP_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RCP_vendorName,'~') into newValueList;
  END IF;
IF (OLD.RCP_vendorCodeEXSO <> NEW.RCP_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RCP_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RCP_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.RCP_vendorContact <> NEW.RCP_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RCP_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RCP_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.RCP_vendorEmail <> NEW.RCP_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RCP_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RCP_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.SPA_vendorCode <> NEW.SPA_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SPA_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SPA_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.SPA_vendorName <> NEW.SPA_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SPA_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SPA_vendorName,'~') into newValueList;
  END IF;
IF (OLD.SPA_vendorCodeEXSO <> NEW.SPA_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SPA_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SPA_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.SPA_vendorContact <> NEW.SPA_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SPA_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SPA_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.SPA_vendorEmail <> NEW.SPA_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SPA_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SPA_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.TCS_vendorCode <> NEW.TCS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TCS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TCS_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.TCS_vendorName <> NEW.TCS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TCS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TCS_vendorName,'~') into newValueList;
  END IF;
IF (OLD.TCS_vendorCodeEXSO <> NEW.TCS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TCS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TCS_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.TCS_vendorContact <> NEW.TCS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TCS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TCS_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.TCS_vendorEmail <> NEW.TCS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TCS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TCS_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.REP_comment <> NEW.REP_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.REP_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.REP_comment,'~') into newValueList;
  END IF;
IF (OLD.WOP_vendorCodeEXSO <> NEW.WOP_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.WOP_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.WOP_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.WOP_comment <> NEW.WOP_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.WOP_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.WOP_comment,'~') into newValueList;
  END IF;
IF (OLD.REP_vendorCode <> NEW.REP_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.REP_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.REP_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.REP_vendorName <> NEW.REP_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.REP_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.REP_vendorName,'~') into newValueList;
  END IF;
IF (OLD.REP_vendorContact <> NEW.REP_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.REP_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.REP_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.REP_vendorEmail <> NEW.REP_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.REP_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.REP_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.REP_workPermitHolderName <> NEW.REP_workPermitHolderName ) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_workPermitHolderName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.REP_workPermitHolderName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.REP_workPermitHolderName,'~') into newValueList;
  END IF;
IF (OLD.RNT_monthlyRentalAllowanceCurrency <> NEW.RNT_monthlyRentalAllowanceCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_monthlyRentalAllowanceCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_monthlyRentalAllowanceCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_monthlyRentalAllowanceCurrency,'~') into newValueList;
  END IF;
IF (OLD.RNT_securityDepositCurrency <> NEW.RNT_securityDepositCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_securityDepositCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_securityDepositCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_securityDepositCurrency,'~') into newValueList;
  END IF;
IF (OLD.MTS_vendorCode <> NEW.MTS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.MTS_vendorName <> NEW.MTS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_vendorName,'~') into newValueList;
  END IF;
IF (OLD.MTS_vendorCodeEXSO <> NEW.MTS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.MTS_vendorContact <> NEW.MTS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.MTS_vendorEmail <> NEW.MTS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.MTS_paymentResponsibility <> NEW.MTS_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.RLS_paymentResponsibility <> NEW.RLS_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.RLS_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RLS_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RLS_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.CAT_paymentResponsibility <> NEW.CAT_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAT_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAT_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.CLS_paymentResponsibility <> NEW.CLS_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CLS_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CLS_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.CHS_paymentResponsibility <> NEW.CHS_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CHS_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CHS_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.DPS_paymentResponsibility <> NEW.DPS_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DPS_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DPS_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.HSM_paymentResponsibility <> NEW.HSM_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.PDT_paymentResponsibility <> NEW.PDT_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PDT_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PDT_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.RCP_paymentResponsibility <> NEW.RCP_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RCP_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RCP_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.SPA_paymentResponsibility <> NEW.SPA_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SPA_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SPA_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.TCS_paymentResponsibility <> NEW.TCS_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TCS_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TCS_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.COL_estimatedTaxAllowance <> NEW.COL_estimatedTaxAllowance ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_estimatedTaxAllowance~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_estimatedTaxAllowance,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_estimatedTaxAllowance,'~') into newValueList;
  END IF;
IF (OLD.COL_estimatedHousingAllowance <> NEW.COL_estimatedHousingAllowance ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_estimatedHousingAllowance~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_estimatedHousingAllowance,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_estimatedHousingAllowance,'~') into newValueList;
  END IF;
IF (OLD.COL_estimatedTransportationAllowance <> NEW.COL_estimatedTransportationAllowance ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_estimatedTransportationAllowance~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_estimatedTransportationAllowance,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_estimatedTransportationAllowance,'~') into newValueList;
  END IF;
IF (OLD.RNT_leaseSignee <> NEW.RNT_leaseSignee ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_leaseSignee~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_leaseSignee,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_leaseSignee,'~') into newValueList;
  END IF;
IF (OLD.TAX_allowance <> NEW.TAX_allowance ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_allowance~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAX_allowance,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAX_allowance,'~') into newValueList;
  END IF;
IF (OLD.VIS_immigrationStatus <> NEW.VIS_immigrationStatus ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_immigrationStatus~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_immigrationStatus,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_immigrationStatus,'~') into newValueList;
  END IF;
IF (OLD.VIS_portofEntry <> NEW.VIS_portofEntry ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_portofEntry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_portofEntry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_portofEntry,'~') into newValueList;
  END IF;
IF (OLD.DSS_vendorCode <> NEW.DSS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DSS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DSS_vendorCode,'~') into newValueList;
  END IF;
IF (OLD.DSS_vendorName <> NEW.DSS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DSS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DSS_vendorName,'~') into newValueList;
  END IF;
IF (OLD.DSS_vendorCodeEXSO <> NEW.DSS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DSS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DSS_vendorCodeEXSO,'~') into newValueList;
  END IF;
IF (OLD.DSS_vendorContact <> NEW.DSS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DSS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DSS_vendorContact,'~') into newValueList;
  END IF;
IF (OLD.DSS_vendorEmail <> NEW.DSS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DSS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DSS_vendorEmail,'~') into newValueList;
  END IF;
IF (OLD.DSS_paymentResponsibility <> NEW.DSS_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DSS_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DSS_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.mailServiceType <> NEW.mailServiceType ) THEN
      select CONCAT(fieldNameList,'dspdetails.mailServiceType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailServiceType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailServiceType,'~') into newValueList;
  END IF;
IF (OLD.DPS_serviceType <> NEW.DPS_serviceType ) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_serviceType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DPS_serviceType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DPS_serviceType,'~') into newValueList;
  END IF;
IF (OLD.DPS_comment <> NEW.DPS_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DPS_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DPS_comment,'~') into newValueList;
  END IF;
IF (OLD.HSM_brokerCode <> NEW.HSM_brokerCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_brokerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_brokerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_brokerCode,'~') into newValueList;
  END IF;
IF (OLD.HSM_brokerName <> NEW.HSM_brokerName ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_brokerName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_brokerName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_brokerName,'~') into newValueList;
  END IF;
IF (OLD.HSM_brokerContact <> NEW.HSM_brokerContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_brokerContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_brokerContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_brokerContact,'~') into newValueList;
  END IF;
IF (OLD.HSM_brokerEmail <> NEW.HSM_brokerEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_brokerEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_brokerEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_brokerEmail,'~') into newValueList;
  END IF;
IF (OLD.HSM_status <> NEW.HSM_status ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_status~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_status,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_status,'~') into newValueList;
  END IF;
IF (OLD.HSM_comment <> NEW.HSM_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_comment,'~') into newValueList;
  END IF;
IF (OLD.PDT_serviceType <> NEW.PDT_serviceType ) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_serviceType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PDT_serviceType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PDT_serviceType,'~') into newValueList;
  END IF;
IF (OLD.MTS_lender <> NEW.MTS_lender ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_lender~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_lender,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_lender,'~') into newValueList;
  END IF;
IF (OLD.MTS_status <> NEW.MTS_status ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_status~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_status,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_status,'~') into newValueList;
  END IF;
IF (OLD.MTS_mortgageTerm <> NEW.MTS_mortgageTerm ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_mortgageTerm~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_mortgageTerm,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_mortgageTerm,'~') into newValueList;
  END IF;
IF (OLD.TEN_landlordName <> NEW.TEN_landlordName ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_landlordName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_landlordName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_landlordName,'~') into newValueList;
  END IF;
IF (OLD.TEN_landlordContactNumber <> NEW.TEN_landlordContactNumber ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_landlordContactNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_landlordContactNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_landlordContactNumber,'~') into newValueList;
  END IF;
IF (OLD.TEN_landlordEmailAddress <> NEW.TEN_landlordEmailAddress ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_landlordEmailAddress~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_landlordEmailAddress,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_landlordEmailAddress,'~') into newValueList;
  END IF;
IF (OLD.TEN_propertyAddress <> NEW.TEN_propertyAddress ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_propertyAddress~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_propertyAddress,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_propertyAddress,'~') into newValueList;
  END IF;
IF (OLD.TEN_accountHolder <> NEW.TEN_accountHolder ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_accountHolder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_accountHolder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_accountHolder,'~') into newValueList;
  END IF;
IF (OLD.TEN_bankName <> NEW.TEN_bankName ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_bankName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_bankName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_bankName,'~') into newValueList;
  END IF;
IF (OLD.TEN_bankAddress <> NEW.TEN_bankAddress ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_bankAddress~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_bankAddress,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_bankAddress,'~') into newValueList;
  END IF;
IF (OLD.TEN_bankAccountNumber <> NEW.TEN_bankAccountNumber ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_bankAccountNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_bankAccountNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_bankAccountNumber,'~') into newValueList;
  END IF;
IF (OLD.TEN_bankIbanNumber <> NEW.TEN_bankIbanNumber ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_bankIbanNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_bankIbanNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_bankIbanNumber,'~') into newValueList;
  END IF;
IF (OLD.TEN_abaNumber <> NEW.TEN_abaNumber ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_abaNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_abaNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_abaNumber,'~') into newValueList;
  END IF;
IF (OLD.TEN_accountType <> NEW.TEN_accountType ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_accountType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_accountType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_accountType,'~') into newValueList;
  END IF;
IF (OLD.TEN_monthlyRentalAllowance <> NEW.TEN_monthlyRentalAllowance ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_monthlyRentalAllowance~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_monthlyRentalAllowance,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_monthlyRentalAllowance,'~') into newValueList;
  END IF;
IF (OLD.TEN_securityDeposit <> NEW.TEN_securityDeposit ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_securityDeposit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_securityDeposit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_securityDeposit,'~') into newValueList;
  END IF;
IF (OLD.TEN_leaseCurrency <> NEW.TEN_leaseCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_leaseCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_leaseCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_leaseCurrency,'~') into newValueList;
  END IF;
IF (OLD.TEN_leaseSignee <> NEW.TEN_leaseSignee ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_leaseSignee~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_leaseSignee,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_leaseSignee,'~') into newValueList;
  END IF;
IF (OLD.HOM_agentCode <> NEW.HOM_agentCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_agentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_agentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_agentCode,'~') into newValueList;
  END IF;
IF (OLD.HOM_agentName <> NEW.HOM_agentName ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_agentName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_agentName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_agentName,'~') into newValueList;
  END IF;
IF (OLD.HOM_agentPhone <> NEW.HOM_agentPhone ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_agentPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_agentPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_agentPhone,'~') into newValueList;
  END IF;
IF (OLD.HOM_agentEmail <> NEW.HOM_agentEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_agentEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_agentEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_agentEmail,'~') into newValueList;
  END IF;
IF (OLD.HOM_brokerage <> NEW.HOM_brokerage ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_brokerage~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_brokerage,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_brokerage,'~') into newValueList;
  END IF;
IF (OLD.PRV_paymentResponsibility <> NEW.PRV_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PRV_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PRV_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.VIS_paymentResponsibility <> NEW.VIS_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.TAX_paymentResponsibility <> NEW.TAX_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAX_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAX_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.RNT_paymentResponsibility <> NEW.RNT_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.AIO_paymentResponsibility <> NEW.AIO_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AIO_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AIO_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.TRG_paymentResponsibility <> NEW.TRG_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TRG_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TRG_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.LAN_paymentResponsibility <> NEW.LAN_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LAN_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LAN_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.RPT_paymentResponsibility <> NEW.RPT_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RPT_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RPT_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.COL_paymentResponsibility <> NEW.COL_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.COL_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.COL_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.TAC_paymentResponsibility <> NEW.TAC_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.HOM_paymentResponsibility <> NEW.HOM_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOM_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOM_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.SCH_paymentResponsibility <> NEW.SCH_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SCH_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SCH_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.EXP_paymentResponsibility <> NEW.EXP_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EXP_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EXP_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.ONG_paymentResponsibility <> NEW.ONG_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ONG_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ONG_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.TEN_paymentResponsibility <> NEW.TEN_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.MMG_paymentResponsibility <> NEW.MMG_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.CAR_paymentResponsibility <> NEW.CAR_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAR_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAR_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.SET_paymentResponsibility <> NEW.SET_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SET_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SET_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.WOP_paymentResponsibility <> NEW.WOP_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.WOP_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.WOP_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.REP_paymentResponsibility <> NEW.REP_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.REP_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.REP_paymentResponsibility,'~') into newValueList;
  END IF;
IF (OLD.HSM_agentCode <> NEW.HSM_agentCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_agentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_agentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_agentCode,'~') into newValueList;
  END IF;
IF (OLD.HSM_agentName <> NEW.HSM_agentName ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_agentName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_agentName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_agentName,'~') into newValueList;
  END IF;
IF (OLD.HSM_brokerage <> NEW.HSM_brokerage ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_brokerage~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_brokerage,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_brokerage,'~') into newValueList;
  END IF;
IF (OLD.HSM_agentPhone <> NEW.HSM_agentPhone ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_agentPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_agentPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_agentPhone,'~') into newValueList;
  END IF;
IF (OLD.HSM_agentEmail <> NEW.HSM_agentEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_agentEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HSM_agentEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HSM_agentEmail,'~') into newValueList;
  END IF;
IF (OLD.reminderServices <> NEW.reminderServices ) THEN
      select CONCAT(fieldNameList,'dspdetails.reminderServices~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reminderServices,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reminderServices,'~') into newValueList;
  END IF;
IF (OLD.CAT_comment <> NEW.CAT_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CAT_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CAT_comment,'~') into newValueList;
  END IF;
IF (OLD.CLS_comment <> NEW.CLS_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CLS_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CLS_comment,'~') into newValueList;
  END IF;
IF (OLD.CHS_comment <> NEW.CHS_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.CHS_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.CHS_comment,'~') into newValueList;
  END IF;
IF (OLD.PDT_comment <> NEW.PDT_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PDT_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PDT_comment,'~') into newValueList;
  END IF;
IF (OLD.RCP_comment <> NEW.RCP_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RCP_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RCP_comment,'~') into newValueList;
  END IF;
IF (OLD.SPA_comment <> NEW.SPA_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.SPA_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.SPA_comment,'~') into newValueList;
  END IF;
IF (OLD.TCS_comment <> NEW.TCS_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TCS_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TCS_comment,'~') into newValueList;
  END IF;
IF (OLD.MTS_comment <> NEW.MTS_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MTS_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MTS_comment,'~') into newValueList;
  END IF;
IF (OLD.DSS_comment <> NEW.DSS_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.DSS_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.DSS_comment,'~') into newValueList;
  END IF;
IF (OLD.TAC_timeAuthorized <> NEW.TAC_timeAuthorized ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_timeAuthorized~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAC_timeAuthorized,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAC_timeAuthorized,'~') into newValueList;
  END IF;
  
IF (OLD.TEN_zipCode <> NEW.TEN_zipCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_zipCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_zipCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_zipCode,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_addressLine1 <> NEW.TEN_addressLine1 ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_addressLine1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_addressLine1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_addressLine1,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_addressLine2 <> NEW.TEN_addressLine2 ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_addressLine2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_addressLine2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_addressLine2,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_state <> NEW.TEN_state ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_state~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_state,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_state,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_country <> NEW.TEN_country ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_country~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_country,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_country,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_leasedBy <> NEW.TEN_leasedBy ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_leasedBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_leasedBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_leasedBy,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_rentCurrency <> NEW.TEN_rentCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_rentCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_rentCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_rentCurrency,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_allowanceCurrency <> NEW.TEN_allowanceCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_allowanceCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_allowanceCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_allowanceCurrency,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_utilitiesIncluded <> NEW.TEN_utilitiesIncluded ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_utilitiesIncluded~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_utilitiesIncluded,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_utilitiesIncluded,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_followUpNeeded <> NEW.TEN_followUpNeeded ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_followUpNeeded~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_followUpNeeded,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_followUpNeeded,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_termOfNotice <> NEW.TEN_termOfNotice ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_termOfNotice~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_termOfNotice,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_termOfNotice,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_rentPaidTo <> NEW.TEN_rentPaidTo ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_rentPaidTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_rentPaidTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_rentPaidTo,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_rentalComment <> NEW.TEN_rentalComment ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_rentalComment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_rentalComment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_rentalComment,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_propertyName <> NEW.TEN_propertyName ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_propertyName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_propertyName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_propertyName,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_city <> NEW.TEN_city ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_city,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_swiftCode <> NEW.TEN_swiftCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_swiftCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_swiftCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_swiftCode,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_paymentDescription <> NEW.TEN_paymentDescription ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_paymentDescription~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_paymentDescription,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_paymentDescription,'~') into newValueList;
  END IF;
  
     
    
  
  
  IF (OLD.TEN_exceptionComments <> NEW.TEN_exceptionComments ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_exceptionComments~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_exceptionComments,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_exceptionComments,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_depositCurrency <> NEW.TEN_depositCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_depositCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_depositCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_depositCurrency,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_depositReturned <> NEW.TEN_depositReturned ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_depositReturned~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_depositReturned,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_depositReturned,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_depositPaidBy <> NEW.TEN_depositPaidBy ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_depositPaidBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_depositPaidBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_depositPaidBy,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_depositReturnedCurrency <> NEW.TEN_depositReturnedCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_depositReturnedCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_depositReturnedCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_depositReturnedCurrency,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_depositComment <> NEW.TEN_depositComment ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_depositComment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_depositComment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_depositComment,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_refundableTo <> NEW.TEN_refundableTo ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_refundableTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_refundableTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_refundableTo,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_toBePaidBy <> NEW.TEN_toBePaidBy ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_toBePaidBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_toBePaidBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_toBePaidBy,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_BIC_SWIFT <> NEW.TEN_BIC_SWIFT ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_BIC_SWIFT~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_BIC_SWIFT,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_BIC_SWIFT,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_IBAN_BankAccountNumber <> NEW.TEN_IBAN_BankAccountNumber ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_IBAN_BankAccountNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_IBAN_BankAccountNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_IBAN_BankAccountNumber,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_description <> NEW.TEN_description ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_description,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_housingRentVatDesc <> NEW.TEN_housingRentVatDesc ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_housingRentVatDesc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_housingRentVatDesc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_housingRentVatDesc,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_housingRentVatPercent <> NEW.TEN_housingRentVatPercent ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_housingRentVatPercent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_housingRentVatPercent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_housingRentVatPercent,'~') into newValueList;
  END IF;
  
  IF (OLD.TEN_assigneeContributionCurrency <> NEW.TEN_assigneeContributionCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_assigneeContributionCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TEN_assigneeContributionCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TEN_assigneeContributionCurrency,'~') into newValueList;
  END IF;
  
IF ((date_format(OLD.TAC_leaseStartDate,'%Y-%m-%d') <> date_format(NEW.TAC_leaseStartDate,'%Y-%m-%d')) or (date_format(OLD.TAC_leaseStartDate,'%Y-%m-%d') is null and date_format(NEW.TAC_leaseStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAC_leaseStartDate,'%Y-%m-%d') is not null and date_format(NEW.TAC_leaseStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_leaseStartDate~') into fieldNameList;
    IF(OLD.TAC_leaseStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAC_leaseStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAC_leaseStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.TAC_leaseStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.TAC_leaseStartDate is not null) THEN
		select CONCAT(newValueList,NEW.TAC_leaseStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TAC_leaseExpireDate,'%Y-%m-%d') <> date_format(NEW.TAC_leaseExpireDate,'%Y-%m-%d')) or (date_format(OLD.TAC_leaseExpireDate,'%Y-%m-%d') is null and date_format(NEW.TAC_leaseExpireDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAC_leaseExpireDate,'%Y-%m-%d') is not null and date_format(NEW.TAC_leaseExpireDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_leaseExpireDate~') into fieldNameList;
    IF(OLD.TAC_leaseExpireDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAC_leaseExpireDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAC_leaseExpireDate is not null) THEN
		select CONCAT(oldValueList,OLD.TAC_leaseExpireDate,'~') into oldValueList;
	END IF;
	IF(NEW.TAC_leaseExpireDate is not null) THEN
		select CONCAT(newValueList,NEW.TAC_leaseExpireDate,'~') into newValueList;
	END IF;
   END IF;
   
   IF ((date_format(OLD.TEN_billThroughDate,'%Y-%m-%d') <> date_format(NEW.TEN_billThroughDate,'%Y-%m-%d')) or (date_format(OLD.TEN_billThroughDate,'%Y-%m-%d') is null and date_format(NEW.TEN_billThroughDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_billThroughDate,'%Y-%m-%d') is not null and date_format(NEW.TEN_billThroughDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_billThroughDate~') into fieldNameList;
    IF(OLD.TEN_billThroughDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_billThroughDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_billThroughDate is not null) THEN
		select CONCAT(oldValueList,OLD.TAC_leaseExpireDate,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_billThroughDate is not null) THEN
		select CONCAT(newValueList,NEW.TEN_billThroughDate,'~') into newValueList;
	END IF;
   END IF;
   
   IF ((date_format(OLD.TEN_rentalIncreaseDate,'%Y-%m-%d') <> date_format(NEW.TEN_rentalIncreaseDate,'%Y-%m-%d')) or (date_format(OLD.TEN_rentalIncreaseDate,'%Y-%m-%d') is null and date_format(NEW.TEN_rentalIncreaseDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_rentalIncreaseDate,'%Y-%m-%d') is not null and date_format(NEW.TEN_rentalIncreaseDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_rentalIncreaseDate~') into fieldNameList;
    IF(OLD.TEN_rentalIncreaseDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_rentalIncreaseDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_rentalIncreaseDate is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_rentalIncreaseDate,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_rentalIncreaseDate is not null) THEN
		select CONCAT(newValueList,NEW.TEN_rentalIncreaseDate,'~') into newValueList;
	END IF;
   END IF;
   
   IF ((date_format(OLD.TEN_checkInMoveIn,'%Y-%m-%d') <> date_format(NEW.TEN_checkInMoveIn,'%Y-%m-%d')) or (date_format(OLD.TEN_checkInMoveIn,'%Y-%m-%d') is null and date_format(NEW.TEN_checkInMoveIn,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_checkInMoveIn,'%Y-%m-%d') is not null and date_format(NEW.TEN_checkInMoveIn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_checkInMoveIn~') into fieldNameList;
    IF(OLD.TEN_checkInMoveIn is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_checkInMoveIn is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_checkInMoveIn is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_checkInMoveIn,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_checkInMoveIn is not null) THEN
		select CONCAT(newValueList,NEW.TEN_checkInMoveIn,'~') into newValueList;
	END IF;
   END IF;
   
   IF ((date_format(OLD.TEN_preCheckOut,'%Y-%m-%d') <> date_format(NEW.TEN_preCheckOut,'%Y-%m-%d')) or (date_format(OLD.TEN_preCheckOut,'%Y-%m-%d') is null and date_format(NEW.TEN_preCheckOut,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_preCheckOut,'%Y-%m-%d') is not null and date_format(NEW.TEN_preCheckOut,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_preCheckOut~') into fieldNameList;
    IF(OLD.TEN_preCheckOut is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_preCheckOut is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_preCheckOut is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_preCheckOut,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_preCheckOut is not null) THEN
		select CONCAT(newValueList,NEW.TEN_preCheckOut,'~') into newValueList;
	END IF;
   END IF;
   
   IF ((date_format(OLD.TEN_checkOutMoveOut,'%Y-%m-%d') <> date_format(NEW.TEN_checkOutMoveOut,'%Y-%m-%d')) or (date_format(OLD.TEN_checkOutMoveOut,'%Y-%m-%d') is null and date_format(NEW.TEN_checkOutMoveOut,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_checkOutMoveOut,'%Y-%m-%d') is not null and date_format(NEW.TEN_checkOutMoveOut,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_checkOutMoveOut~') into fieldNameList;
    IF(OLD.TEN_checkOutMoveOut is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_checkOutMoveOut is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_checkOutMoveOut is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_preCheckOut,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_checkOutMoveOut is not null) THEN
		select CONCAT(newValueList,NEW.TEN_preCheckOut,'~') into newValueList;
	END IF;
   END IF;
   
IF ((date_format(OLD.TAC_expiryReminderPriorToExpiry,'%Y-%m-%d') <> date_format(NEW.TAC_expiryReminderPriorToExpiry,'%Y-%m-%d')) or (date_format(OLD.TAC_expiryReminderPriorToExpiry,'%Y-%m-%d') is null and date_format(NEW.TAC_expiryReminderPriorToExpiry,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAC_expiryReminderPriorToExpiry,'%Y-%m-%d') is not null and date_format(NEW.TAC_expiryReminderPriorToExpiry,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_expiryReminderPriorToExpiry~') into fieldNameList;
    IF(OLD.TAC_expiryReminderPriorToExpiry is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAC_expiryReminderPriorToExpiry is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAC_expiryReminderPriorToExpiry is not null) THEN
		select CONCAT(oldValueList,OLD.TAC_expiryReminderPriorToExpiry,'~') into oldValueList;
	END IF;
	IF(NEW.TAC_expiryReminderPriorToExpiry is not null) THEN
		select CONCAT(newValueList,NEW.TAC_expiryReminderPriorToExpiry,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.ONG_date1,'%Y-%m-%d') <> date_format(NEW.ONG_date1,'%Y-%m-%d')) or (date_format(OLD.ONG_date1,'%Y-%m-%d') is null and date_format(NEW.ONG_date1,'%Y-%m-%d') is not null)
      or (date_format(OLD.ONG_date1,'%Y-%m-%d') is not null and date_format(NEW.ONG_date1,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_date1~') into fieldNameList;
    IF(OLD.ONG_date1 is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.ONG_date1 is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.ONG_date1 is not null) THEN
		select CONCAT(oldValueList,OLD.ONG_date1,'~') into oldValueList;
	END IF;
	IF(NEW.ONG_date1 is not null) THEN
		select CONCAT(newValueList,NEW.ONG_date1,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.ONG_date2,'%Y-%m-%d') <> date_format(NEW.ONG_date2,'%Y-%m-%d')) or (date_format(OLD.ONG_date2,'%Y-%m-%d') is null and date_format(NEW.ONG_date2,'%Y-%m-%d') is not null)
      or (date_format(OLD.ONG_date2,'%Y-%m-%d') is not null and date_format(NEW.ONG_date2,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_date2~') into fieldNameList;
    IF(OLD.ONG_date2 is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.ONG_date2 is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.ONG_date2 is not null) THEN
		select CONCAT(oldValueList,OLD.ONG_date2,'~') into oldValueList;
	END IF;
	IF(NEW.ONG_date2 is not null) THEN
		select CONCAT(newValueList,NEW.ONG_date2,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.ONG_date3,'%Y-%m-%d') <> date_format(NEW.ONG_date3,'%Y-%m-%d')) or (date_format(OLD.ONG_date3,'%Y-%m-%d') is null and date_format(NEW.ONG_date3,'%Y-%m-%d') is not null)
      or (date_format(OLD.ONG_date3,'%Y-%m-%d') is not null and date_format(NEW.ONG_date3,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_date3~') into fieldNameList;
    IF(OLD.ONG_date3 is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.ONG_date3 is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.ONG_date3 is not null) THEN
		select CONCAT(oldValueList,OLD.ONG_date3,'~') into oldValueList;
	END IF;
	IF(NEW.ONG_date3 is not null) THEN
		select CONCAT(newValueList,NEW.ONG_date3,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.SET_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.SET_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.SET_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.SET_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_serviceStartDate~') into fieldNameList;
    IF(OLD.SET_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.SET_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.SET_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.SET_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.SET_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.SET_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.SET_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.SET_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_serviceEndDate~') into fieldNameList;
    IF(OLD.SET_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.SET_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.SET_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.SET_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_parkingPermit,'%Y-%m-%d') <> date_format(NEW.SET_parkingPermit,'%Y-%m-%d')) or (date_format(OLD.SET_parkingPermit,'%Y-%m-%d') is null and date_format(NEW.SET_parkingPermit,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_parkingPermit,'%Y-%m-%d') is not null and date_format(NEW.SET_parkingPermit,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_parkingPermit~') into fieldNameList;
    IF(OLD.SET_parkingPermit is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_parkingPermit is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_parkingPermit is not null) THEN
		select CONCAT(oldValueList,OLD.SET_parkingPermit,'~') into oldValueList;
	END IF;
	IF(NEW.SET_parkingPermit is not null) THEN
		select CONCAT(newValueList,NEW.SET_parkingPermit,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_bankAccount,'%Y-%m-%d') <> date_format(NEW.SET_bankAccount,'%Y-%m-%d')) or (date_format(OLD.SET_bankAccount,'%Y-%m-%d') is null and date_format(NEW.SET_bankAccount,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_bankAccount,'%Y-%m-%d') is not null and date_format(NEW.SET_bankAccount,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_bankAccount~') into fieldNameList;
    IF(OLD.SET_bankAccount is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_bankAccount is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_bankAccount is not null) THEN
		select CONCAT(oldValueList,OLD.SET_bankAccount,'~') into oldValueList;
	END IF;
	IF(NEW.SET_bankAccount is not null) THEN
		select CONCAT(newValueList,NEW.SET_bankAccount,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_cityHall,'%Y-%m-%d') <> date_format(NEW.SET_cityHall,'%Y-%m-%d')) or (date_format(OLD.SET_cityHall,'%Y-%m-%d') is null and date_format(NEW.SET_cityHall,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_cityHall,'%Y-%m-%d') is not null and date_format(NEW.SET_cityHall,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_cityHall~') into fieldNameList;
    IF(OLD.SET_cityHall is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_cityHall is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_cityHall is not null) THEN
		select CONCAT(oldValueList,OLD.SET_cityHall,'~') into oldValueList;
	END IF;
	IF(NEW.SET_cityHall is not null) THEN
		select CONCAT(newValueList,NEW.SET_cityHall,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_internetConn,'%Y-%m-%d') <> date_format(NEW.SET_internetConn,'%Y-%m-%d')) or (date_format(OLD.SET_internetConn,'%Y-%m-%d') is null and date_format(NEW.SET_internetConn,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_internetConn,'%Y-%m-%d') is not null and date_format(NEW.SET_internetConn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_internetConn~') into fieldNameList;
    IF(OLD.SET_internetConn is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_internetConn is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_internetConn is not null) THEN
		select CONCAT(oldValueList,OLD.SET_internetConn,'~') into oldValueList;
	END IF;
	IF(NEW.SET_internetConn is not null) THEN
		select CONCAT(newValueList,NEW.SET_internetConn,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_driverLicense,'%Y-%m-%d') <> date_format(NEW.SET_driverLicense,'%Y-%m-%d')) or (date_format(OLD.SET_driverLicense,'%Y-%m-%d') is null and date_format(NEW.SET_driverLicense,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_driverLicense,'%Y-%m-%d') is not null and date_format(NEW.SET_driverLicense,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_driverLicense~') into fieldNameList;
    IF(OLD.SET_driverLicense is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_driverLicense is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_driverLicense is not null) THEN
		select CONCAT(oldValueList,OLD.SET_driverLicense,'~') into oldValueList;
	END IF;
	IF(NEW.SET_driverLicense is not null) THEN
		select CONCAT(newValueList,NEW.SET_driverLicense,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_providerNotificationDateRP,'%Y-%m-%d') <> date_format(NEW.VIS_providerNotificationDateRP,'%Y-%m-%d')) or (date_format(OLD.VIS_providerNotificationDateRP,'%Y-%m-%d') is null and date_format(NEW.VIS_providerNotificationDateRP,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_providerNotificationDateRP,'%Y-%m-%d') is not null and date_format(NEW.VIS_providerNotificationDateRP,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_providerNotificationDateRP~') into fieldNameList;
    IF(OLD.VIS_providerNotificationDateRP is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_providerNotificationDateRP is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_providerNotificationDateRP is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_providerNotificationDateRP,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_providerNotificationDateRP is not null) THEN
		select CONCAT(newValueList,NEW.VIS_providerNotificationDateRP,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_visaExpiryDateRP,'%Y-%m-%d') <> date_format(NEW.VIS_visaExpiryDateRP,'%Y-%m-%d')) or (date_format(OLD.VIS_visaExpiryDateRP,'%Y-%m-%d') is null and date_format(NEW.VIS_visaExpiryDateRP,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_visaExpiryDateRP,'%Y-%m-%d') is not null and date_format(NEW.VIS_visaExpiryDateRP,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_visaExpiryDateRP~') into fieldNameList;
    IF(OLD.VIS_visaExpiryDateRP is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_visaExpiryDateRP is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_visaExpiryDateRP is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_visaExpiryDateRP,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_visaExpiryDateRP is not null) THEN
		select CONCAT(newValueList,NEW.VIS_visaExpiryDateRP,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_expiryReminder3MosPriorToExpiryRP,'%Y-%m-%d') <> date_format(NEW.VIS_expiryReminder3MosPriorToExpiryRP,'%Y-%m-%d')) or (date_format(OLD.VIS_expiryReminder3MosPriorToExpiryRP,'%Y-%m-%d') is null and date_format(NEW.VIS_expiryReminder3MosPriorToExpiryRP,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_expiryReminder3MosPriorToExpiryRP,'%Y-%m-%d') is not null and date_format(NEW.VIS_expiryReminder3MosPriorToExpiryRP,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_expiryReminder3MosPriorToExpiryRP~') into fieldNameList;
    IF(OLD.VIS_expiryReminder3MosPriorToExpiryRP is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_expiryReminder3MosPriorToExpiryRP is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_expiryReminder3MosPriorToExpiryRP is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_expiryReminder3MosPriorToExpiryRP,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_expiryReminder3MosPriorToExpiryRP is not null) THEN
		select CONCAT(newValueList,NEW.VIS_expiryReminder3MosPriorToExpiryRP,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_residenceExpiryRP,'%Y-%m-%d') <> date_format(NEW.VIS_residenceExpiryRP,'%Y-%m-%d')) or (date_format(OLD.VIS_residenceExpiryRP,'%Y-%m-%d') is null and date_format(NEW.VIS_residenceExpiryRP,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_residenceExpiryRP,'%Y-%m-%d') is not null and date_format(NEW.VIS_residenceExpiryRP,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_residenceExpiryRP~') into fieldNameList;
    IF(OLD.VIS_residenceExpiryRP is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_residenceExpiryRP is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_residenceExpiryRP is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_residenceExpiryRP,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_residenceExpiryRP is not null) THEN
		select CONCAT(newValueList,NEW.VIS_residenceExpiryRP,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_questionnaireSentDate,'%Y-%m-%d') <> date_format(NEW.VIS_questionnaireSentDate,'%Y-%m-%d')) or (date_format(OLD.VIS_questionnaireSentDate,'%Y-%m-%d') is null and date_format(NEW.VIS_questionnaireSentDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_questionnaireSentDate,'%Y-%m-%d') is not null and date_format(NEW.VIS_questionnaireSentDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_questionnaireSentDate~') into fieldNameList;
    IF(OLD.VIS_questionnaireSentDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_questionnaireSentDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_questionnaireSentDate is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_questionnaireSentDate,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_questionnaireSentDate is not null) THEN
		select CONCAT(newValueList,NEW.VIS_questionnaireSentDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CAR_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.CAR_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.CAR_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.CAR_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAR_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.CAR_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_serviceStartDate~') into fieldNameList;
    IF(OLD.CAR_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAR_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAR_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.CAR_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.CAR_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.CAR_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CAR_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.CAR_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.CAR_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.CAR_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAR_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.CAR_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_serviceEndDate~') into fieldNameList;
    IF(OLD.CAR_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAR_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAR_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.CAR_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.CAR_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.CAR_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.COL_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.COL_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.COL_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.COL_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.COL_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.COL_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_serviceStartDate~') into fieldNameList;
    IF(OLD.COL_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.COL_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.COL_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.COL_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.COL_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.COL_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.COL_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.COL_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.COL_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.COL_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.COL_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.COL_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_serviceEndDate~') into fieldNameList;
    IF(OLD.COL_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.COL_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.COL_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.COL_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.COL_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.COL_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.COL_notificationDate,'%Y-%m-%d') <> date_format(NEW.COL_notificationDate,'%Y-%m-%d')) or (date_format(OLD.COL_notificationDate,'%Y-%m-%d') is null and date_format(NEW.COL_notificationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.COL_notificationDate,'%Y-%m-%d') is not null and date_format(NEW.COL_notificationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_notificationDate~') into fieldNameList;
    IF(OLD.COL_notificationDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.COL_notificationDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.COL_notificationDate is not null) THEN
		select CONCAT(oldValueList,OLD.COL_notificationDate,'~') into oldValueList;
	END IF;
	IF(NEW.COL_notificationDate is not null) THEN
		select CONCAT(newValueList,NEW.COL_notificationDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TRG_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.TRG_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.TRG_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.TRG_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TRG_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.TRG_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_serviceStartDate~') into fieldNameList;
    IF(OLD.TRG_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TRG_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TRG_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.TRG_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.TRG_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.TRG_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TRG_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.TRG_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.TRG_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.TRG_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TRG_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.TRG_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_serviceEndDate~') into fieldNameList;
    IF(OLD.TRG_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TRG_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TRG_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.TRG_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.TRG_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.TRG_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HOM_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.HOM_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.HOM_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.HOM_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOM_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.HOM_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_serviceStartDate~') into fieldNameList;
    IF(OLD.HOM_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOM_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOM_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOM_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOM_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.HOM_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HOM_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.HOM_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.HOM_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.HOM_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOM_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.HOM_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_serviceEndDate~') into fieldNameList;
    IF(OLD.HOM_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOM_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOM_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOM_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOM_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.HOM_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HOM_moveInDate,'%Y-%m-%d') <> date_format(NEW.HOM_moveInDate,'%Y-%m-%d')) or (date_format(OLD.HOM_moveInDate,'%Y-%m-%d') is null and date_format(NEW.HOM_moveInDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOM_moveInDate,'%Y-%m-%d') is not null and date_format(NEW.HOM_moveInDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_moveInDate~') into fieldNameList;
    IF(OLD.HOM_moveInDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOM_moveInDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOM_moveInDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOM_moveInDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOM_moveInDate is not null) THEN
		select CONCAT(newValueList,NEW.HOM_moveInDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RNT_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.RNT_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.RNT_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.RNT_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RNT_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.RNT_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_serviceStartDate~') into fieldNameList;
    IF(OLD.RNT_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RNT_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RNT_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.RNT_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.RNT_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.RNT_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RNT_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.RNT_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.RNT_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.RNT_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RNT_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.RNT_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_serviceEndDate~') into fieldNameList;
    IF(OLD.RNT_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RNT_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RNT_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.RNT_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.RNT_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.RNT_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RNT_leaseStartDate,'%Y-%m-%d') <> date_format(NEW.RNT_leaseStartDate,'%Y-%m-%d')) or (date_format(OLD.RNT_leaseStartDate,'%Y-%m-%d') is null and date_format(NEW.RNT_leaseStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RNT_leaseStartDate,'%Y-%m-%d') is not null and date_format(NEW.RNT_leaseStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_leaseStartDate~') into fieldNameList;
    IF(OLD.RNT_leaseStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RNT_leaseStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RNT_leaseStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.RNT_leaseStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.RNT_leaseStartDate is not null) THEN
		select CONCAT(newValueList,NEW.RNT_leaseStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RNT_leaseExpireDate,'%Y-%m-%d') <> date_format(NEW.RNT_leaseExpireDate,'%Y-%m-%d')) or (date_format(OLD.RNT_leaseExpireDate,'%Y-%m-%d') is null and date_format(NEW.RNT_leaseExpireDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RNT_leaseExpireDate,'%Y-%m-%d') is not null and date_format(NEW.RNT_leaseExpireDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_leaseExpireDate~') into fieldNameList;
    IF(OLD.RNT_leaseExpireDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RNT_leaseExpireDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RNT_leaseExpireDate is not null) THEN
		select CONCAT(oldValueList,OLD.RNT_leaseExpireDate,'~') into oldValueList;
	END IF;
	IF(NEW.RNT_leaseExpireDate is not null) THEN
		select CONCAT(newValueList,NEW.RNT_leaseExpireDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RNT_reminderDate,'%Y-%m-%d') <> date_format(NEW.RNT_reminderDate,'%Y-%m-%d')) or (date_format(OLD.RNT_reminderDate,'%Y-%m-%d') is null and date_format(NEW.RNT_reminderDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RNT_reminderDate,'%Y-%m-%d') is not null and date_format(NEW.RNT_reminderDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_reminderDate~') into fieldNameList;
    IF(OLD.RNT_reminderDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RNT_reminderDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RNT_reminderDate is not null) THEN
		select CONCAT(oldValueList,OLD.RNT_reminderDate,'~') into oldValueList;
	END IF;
	IF(NEW.RNT_reminderDate is not null) THEN
		select CONCAT(newValueList,NEW.RNT_reminderDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.LAN_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.LAN_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.LAN_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.LAN_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.LAN_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.LAN_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_serviceStartDate~') into fieldNameList;
    IF(OLD.LAN_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.LAN_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.LAN_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.LAN_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.LAN_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.LAN_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.LAN_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.LAN_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.LAN_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.LAN_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.LAN_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.LAN_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_serviceEndDate~') into fieldNameList;
    IF(OLD.LAN_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.LAN_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.LAN_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.LAN_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.LAN_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.LAN_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.LAN_notificationDate,'%Y-%m-%d') <> date_format(NEW.LAN_notificationDate,'%Y-%m-%d')) or (date_format(OLD.LAN_notificationDate,'%Y-%m-%d') is null and date_format(NEW.LAN_notificationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.LAN_notificationDate,'%Y-%m-%d') is not null and date_format(NEW.LAN_notificationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_notificationDate~') into fieldNameList;
    IF(OLD.LAN_notificationDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.LAN_notificationDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.LAN_notificationDate is not null) THEN
		select CONCAT(oldValueList,OLD.LAN_notificationDate,'~') into oldValueList;
	END IF;
	IF(NEW.LAN_notificationDate is not null) THEN
		select CONCAT(newValueList,NEW.LAN_notificationDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.MMG_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.MMG_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.MMG_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.MMG_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.MMG_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.MMG_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_serviceStartDate~') into fieldNameList;
    IF(OLD.MMG_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.MMG_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.MMG_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.MMG_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.MMG_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.MMG_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.MMG_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.MMG_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.MMG_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.MMG_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.MMG_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.MMG_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_serviceEndDate~') into fieldNameList;
    IF(OLD.MMG_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.MMG_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.MMG_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.MMG_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.MMG_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.MMG_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.ONG_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.ONG_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.ONG_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.ONG_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.ONG_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.ONG_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_serviceStartDate~') into fieldNameList;
    IF(OLD.ONG_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.ONG_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.ONG_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.ONG_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.ONG_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.ONG_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.ONG_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.ONG_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.ONG_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.ONG_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.ONG_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.ONG_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_serviceEndDate~') into fieldNameList;
    IF(OLD.ONG_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.ONG_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.ONG_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.ONG_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.ONG_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.ONG_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.PRV_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.PRV_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.PRV_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.PRV_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.PRV_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.PRV_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_serviceStartDate~') into fieldNameList;
    IF(OLD.PRV_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PRV_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PRV_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.PRV_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.PRV_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.PRV_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.PRV_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.PRV_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.PRV_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.PRV_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.PRV_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.PRV_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_serviceEndDate~') into fieldNameList;
    IF(OLD.PRV_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PRV_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PRV_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.PRV_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.PRV_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.PRV_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.PRV_travelPlannerNotification,'%Y-%m-%d') <> date_format(NEW.PRV_travelPlannerNotification,'%Y-%m-%d')) or (date_format(OLD.PRV_travelPlannerNotification,'%Y-%m-%d') is null and date_format(NEW.PRV_travelPlannerNotification,'%Y-%m-%d') is not null)
      or (date_format(OLD.PRV_travelPlannerNotification,'%Y-%m-%d') is not null and date_format(NEW.PRV_travelPlannerNotification,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_travelPlannerNotification~') into fieldNameList;
    IF(OLD.PRV_travelPlannerNotification is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PRV_travelPlannerNotification is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PRV_travelPlannerNotification is not null) THEN
		select CONCAT(oldValueList,OLD.PRV_travelPlannerNotification,'~') into oldValueList;
	END IF;
	IF(NEW.PRV_travelPlannerNotification is not null) THEN
		select CONCAT(newValueList,NEW.PRV_travelPlannerNotification,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.PRV_travelDeparture,'%Y-%m-%d') <> date_format(NEW.PRV_travelDeparture,'%Y-%m-%d')) or (date_format(OLD.PRV_travelDeparture,'%Y-%m-%d') is null and date_format(NEW.PRV_travelDeparture,'%Y-%m-%d') is not null)
      or (date_format(OLD.PRV_travelDeparture,'%Y-%m-%d') is not null and date_format(NEW.PRV_travelDeparture,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_travelDeparture~') into fieldNameList;
    IF(OLD.PRV_travelDeparture is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PRV_travelDeparture is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PRV_travelDeparture is not null) THEN
		select CONCAT(oldValueList,OLD.PRV_travelDeparture,'~') into oldValueList;
	END IF;
	IF(NEW.PRV_travelDeparture is not null) THEN
		select CONCAT(newValueList,NEW.PRV_travelDeparture,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.PRV_travelArrival,'%Y-%m-%d') <> date_format(NEW.PRV_travelArrival,'%Y-%m-%d')) or (date_format(OLD.PRV_travelArrival,'%Y-%m-%d') is null and date_format(NEW.PRV_travelArrival,'%Y-%m-%d') is not null)
      or (date_format(OLD.PRV_travelArrival,'%Y-%m-%d') is not null and date_format(NEW.PRV_travelArrival,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_travelArrival~') into fieldNameList;
    IF(OLD.PRV_travelArrival is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PRV_travelArrival is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PRV_travelArrival is not null) THEN
		select CONCAT(oldValueList,OLD.PRV_travelArrival,'~') into oldValueList;
	END IF;
	IF(NEW.PRV_travelArrival is not null) THEN
		select CONCAT(newValueList,NEW.PRV_travelArrival,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.PRV_providerNotificationAndServicesAuthorized,'%Y-%m-%d') <> date_format(NEW.PRV_providerNotificationAndServicesAuthorized,'%Y-%m-%d')) or (date_format(OLD.PRV_providerNotificationAndServicesAuthorized,'%Y-%m-%d') is null and date_format(NEW.PRV_providerNotificationAndServicesAuthorized,'%Y-%m-%d') is not null)
      or (date_format(OLD.PRV_providerNotificationAndServicesAuthorized,'%Y-%m-%d') is not null and date_format(NEW.PRV_providerNotificationAndServicesAuthorized,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_providerNotificationAndServicesAuthorized~') into fieldNameList;
    IF(OLD.PRV_providerNotificationAndServicesAuthorized is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PRV_providerNotificationAndServicesAuthorized is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PRV_providerNotificationAndServicesAuthorized is not null) THEN
		select CONCAT(oldValueList,OLD.PRV_providerNotificationAndServicesAuthorized,'~') into oldValueList;
	END IF;
	IF(NEW.PRV_providerNotificationAndServicesAuthorized is not null) THEN
		select CONCAT(newValueList,NEW.PRV_providerNotificationAndServicesAuthorized,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.PRV_meetGgreetAtAirport,'%Y-%m-%d') <> date_format(NEW.PRV_meetGgreetAtAirport,'%Y-%m-%d')) or (date_format(OLD.PRV_meetGgreetAtAirport,'%Y-%m-%d') is null and date_format(NEW.PRV_meetGgreetAtAirport,'%Y-%m-%d') is not null)
      or (date_format(OLD.PRV_meetGgreetAtAirport,'%Y-%m-%d') is not null and date_format(NEW.PRV_meetGgreetAtAirport,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_meetGgreetAtAirport~') into fieldNameList;
    IF(OLD.PRV_meetGgreetAtAirport is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PRV_meetGgreetAtAirport is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PRV_meetGgreetAtAirport is not null) THEN
		select CONCAT(oldValueList,OLD.PRV_meetGgreetAtAirport,'~') into oldValueList;
	END IF;
	IF(NEW.PRV_meetGgreetAtAirport is not null) THEN
		select CONCAT(newValueList,NEW.PRV_meetGgreetAtAirport,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.AIO_flightArrival,'%Y-%m-%d') <> date_format(NEW.AIO_flightArrival,'%Y-%m-%d')) or (date_format(OLD.AIO_flightArrival,'%Y-%m-%d') is null and date_format(NEW.AIO_flightArrival,'%Y-%m-%d') is not null)
      or (date_format(OLD.AIO_flightArrival,'%Y-%m-%d') is not null and date_format(NEW.AIO_flightArrival,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_flightArrival~') into fieldNameList;
    IF(OLD.AIO_flightArrival is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.AIO_flightArrival is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.AIO_flightArrival is not null) THEN
		select CONCAT(oldValueList,OLD.AIO_flightArrival,'~') into oldValueList;
	END IF;
	IF(NEW.AIO_flightArrival is not null) THEN
		select CONCAT(newValueList,NEW.AIO_flightArrival,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.AIO_flightDeparture,'%Y-%m-%d') <> date_format(NEW.AIO_flightDeparture,'%Y-%m-%d')) or (date_format(OLD.AIO_flightDeparture,'%Y-%m-%d') is null and date_format(NEW.AIO_flightDeparture,'%Y-%m-%d') is not null)
      or (date_format(OLD.AIO_flightDeparture,'%Y-%m-%d') is not null and date_format(NEW.AIO_flightDeparture,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_flightDeparture~') into fieldNameList;
    IF(OLD.AIO_flightDeparture is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.AIO_flightDeparture is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.AIO_flightDeparture is not null) THEN
		select CONCAT(oldValueList,OLD.AIO_flightDeparture,'~') into oldValueList;
	END IF;
	IF(NEW.AIO_flightDeparture is not null) THEN
		select CONCAT(newValueList,NEW.AIO_flightDeparture,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.AIO_initialDateOfServiceRequested,'%Y-%m-%d') <> date_format(NEW.AIO_initialDateOfServiceRequested,'%Y-%m-%d')) or (date_format(OLD.AIO_initialDateOfServiceRequested,'%Y-%m-%d') is null and date_format(NEW.AIO_initialDateOfServiceRequested,'%Y-%m-%d') is not null)
      or (date_format(OLD.AIO_initialDateOfServiceRequested,'%Y-%m-%d') is not null and date_format(NEW.AIO_initialDateOfServiceRequested,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_initialDateOfServiceRequested~') into fieldNameList;
    IF(OLD.AIO_initialDateOfServiceRequested is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.AIO_initialDateOfServiceRequested is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.AIO_initialDateOfServiceRequested is not null) THEN
		select CONCAT(oldValueList,OLD.AIO_initialDateOfServiceRequested,'~') into oldValueList;
	END IF;
	IF(NEW.AIO_initialDateOfServiceRequested is not null) THEN
		select CONCAT(newValueList,NEW.AIO_initialDateOfServiceRequested,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.AIO_prearrivalDiscussionOfProviderAndFamily,'%Y-%m-%d') <> date_format(NEW.AIO_prearrivalDiscussionOfProviderAndFamily,'%Y-%m-%d')) or (date_format(OLD.AIO_prearrivalDiscussionOfProviderAndFamily,'%Y-%m-%d') is null and date_format(NEW.AIO_prearrivalDiscussionOfProviderAndFamily,'%Y-%m-%d') is not null)
      or (date_format(OLD.AIO_prearrivalDiscussionOfProviderAndFamily,'%Y-%m-%d') is not null and date_format(NEW.AIO_prearrivalDiscussionOfProviderAndFamily,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_prearrivalDiscussionOfProviderAndFamily~') into fieldNameList;
    IF(OLD.AIO_prearrivalDiscussionOfProviderAndFamily is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.AIO_prearrivalDiscussionOfProviderAndFamily is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.AIO_prearrivalDiscussionOfProviderAndFamily is not null) THEN
		select CONCAT(oldValueList,OLD.AIO_prearrivalDiscussionOfProviderAndFamily,'~') into oldValueList;
	END IF;
	IF(NEW.AIO_prearrivalDiscussionOfProviderAndFamily is not null) THEN
		select CONCAT(newValueList,NEW.AIO_prearrivalDiscussionOfProviderAndFamily,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.AIO_providerConfirmationReceived,'%Y-%m-%d') <> date_format(NEW.AIO_providerConfirmationReceived,'%Y-%m-%d')) or (date_format(OLD.AIO_providerConfirmationReceived,'%Y-%m-%d') is null and date_format(NEW.AIO_providerConfirmationReceived,'%Y-%m-%d') is not null)
      or (date_format(OLD.AIO_providerConfirmationReceived,'%Y-%m-%d') is not null and date_format(NEW.AIO_providerConfirmationReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_providerConfirmationReceived~') into fieldNameList;
    IF(OLD.AIO_providerConfirmationReceived is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.AIO_providerConfirmationReceived is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.AIO_providerConfirmationReceived is not null) THEN
		select CONCAT(oldValueList,OLD.AIO_providerConfirmationReceived,'~') into oldValueList;
	END IF;
	IF(NEW.AIO_providerConfirmationReceived is not null) THEN
		select CONCAT(newValueList,NEW.AIO_providerConfirmationReceived,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.AIO_providerNotificationSent,'%Y-%m-%d') <> date_format(NEW.AIO_providerNotificationSent,'%Y-%m-%d')) or (date_format(OLD.AIO_providerNotificationSent,'%Y-%m-%d') is null and date_format(NEW.AIO_providerNotificationSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.AIO_providerNotificationSent,'%Y-%m-%d') is not null and date_format(NEW.AIO_providerNotificationSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_providerNotificationSent~') into fieldNameList;
    IF(OLD.AIO_providerNotificationSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.AIO_providerNotificationSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.AIO_providerNotificationSent is not null) THEN
		select CONCAT(oldValueList,OLD.AIO_providerNotificationSent,'~') into oldValueList;
	END IF;
	IF(NEW.AIO_providerNotificationSent is not null) THEN
		select CONCAT(newValueList,NEW.AIO_providerNotificationSent,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.AIO_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.AIO_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.AIO_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.AIO_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.AIO_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.AIO_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_serviceStartDate~') into fieldNameList;
    IF(OLD.AIO_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.AIO_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.AIO_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.AIO_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.AIO_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.AIO_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.AIO_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.AIO_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.AIO_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.AIO_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.AIO_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.AIO_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_serviceEndDate~') into fieldNameList;
    IF(OLD.AIO_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.AIO_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.AIO_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.AIO_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.AIO_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.AIO_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.EXP_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.EXP_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.EXP_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.EXP_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.EXP_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.EXP_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_serviceStartDate~') into fieldNameList;
    IF(OLD.EXP_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.EXP_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.EXP_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.EXP_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.EXP_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.EXP_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.EXP_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.EXP_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.EXP_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.EXP_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.EXP_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.EXP_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_serviceEndDate~') into fieldNameList;
    IF(OLD.EXP_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.EXP_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.EXP_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.EXP_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.EXP_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.EXP_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.EXP_providerNotification,'%Y-%m-%d') <> date_format(NEW.EXP_providerNotification,'%Y-%m-%d')) or (date_format(OLD.EXP_providerNotification,'%Y-%m-%d') is null and date_format(NEW.EXP_providerNotification,'%Y-%m-%d') is not null)
      or (date_format(OLD.EXP_providerNotification,'%Y-%m-%d') is not null and date_format(NEW.EXP_providerNotification,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_providerNotification~') into fieldNameList;
    IF(OLD.EXP_providerNotification is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.EXP_providerNotification is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.EXP_providerNotification is not null) THEN
		select CONCAT(oldValueList,OLD.EXP_providerNotification,'~') into oldValueList;
	END IF;
	IF(NEW.EXP_providerNotification is not null) THEN
		select CONCAT(newValueList,NEW.EXP_providerNotification,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RPT_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.RPT_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.RPT_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.RPT_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RPT_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.RPT_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_serviceStartDate~') into fieldNameList;
    IF(OLD.RPT_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RPT_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RPT_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.RPT_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.RPT_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.RPT_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RPT_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.RPT_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.RPT_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.RPT_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RPT_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.RPT_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_serviceEndDate~') into fieldNameList;
    IF(OLD.RPT_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RPT_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RPT_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.RPT_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.RPT_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.RPT_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RPT_landlordPaidNotificationDate,'%Y-%m-%d') <> date_format(NEW.RPT_landlordPaidNotificationDate,'%Y-%m-%d')) or (date_format(OLD.RPT_landlordPaidNotificationDate,'%Y-%m-%d') is null and date_format(NEW.RPT_landlordPaidNotificationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RPT_landlordPaidNotificationDate,'%Y-%m-%d') is not null and date_format(NEW.RPT_landlordPaidNotificationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_landlordPaidNotificationDate~') into fieldNameList;
    IF(OLD.RPT_landlordPaidNotificationDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RPT_landlordPaidNotificationDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RPT_landlordPaidNotificationDate is not null) THEN
		select CONCAT(oldValueList,OLD.RPT_landlordPaidNotificationDate,'~') into oldValueList;
	END IF;
	IF(NEW.RPT_landlordPaidNotificationDate is not null) THEN
		select CONCAT(newValueList,NEW.RPT_landlordPaidNotificationDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RPT_professionalCleaningDate,'%Y-%m-%d') <> date_format(NEW.RPT_professionalCleaningDate,'%Y-%m-%d')) or (date_format(OLD.RPT_professionalCleaningDate,'%Y-%m-%d') is null and date_format(NEW.RPT_professionalCleaningDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RPT_professionalCleaningDate,'%Y-%m-%d') is not null and date_format(NEW.RPT_professionalCleaningDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_professionalCleaningDate~') into fieldNameList;
    IF(OLD.RPT_professionalCleaningDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RPT_professionalCleaningDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RPT_professionalCleaningDate is not null) THEN
		select CONCAT(oldValueList,OLD.RPT_professionalCleaningDate,'~') into oldValueList;
	END IF;
	IF(NEW.RPT_professionalCleaningDate is not null) THEN
		select CONCAT(newValueList,NEW.RPT_professionalCleaningDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RPT_utilitiesShutOff,'%Y-%m-%d') <> date_format(NEW.RPT_utilitiesShutOff,'%Y-%m-%d')) or (date_format(OLD.RPT_utilitiesShutOff,'%Y-%m-%d') is null and date_format(NEW.RPT_utilitiesShutOff,'%Y-%m-%d') is not null)
      or (date_format(OLD.RPT_utilitiesShutOff,'%Y-%m-%d') is not null and date_format(NEW.RPT_utilitiesShutOff,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_utilitiesShutOff~') into fieldNameList;
    IF(OLD.RPT_utilitiesShutOff is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RPT_utilitiesShutOff is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RPT_utilitiesShutOff is not null) THEN
		select CONCAT(oldValueList,OLD.RPT_utilitiesShutOff,'~') into oldValueList;
	END IF;
	IF(NEW.RPT_utilitiesShutOff is not null) THEN
		select CONCAT(newValueList,NEW.RPT_utilitiesShutOff,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RPT_moveOutDate,'%Y-%m-%d') <> date_format(NEW.RPT_moveOutDate,'%Y-%m-%d')) or (date_format(OLD.RPT_moveOutDate,'%Y-%m-%d') is null and date_format(NEW.RPT_moveOutDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RPT_moveOutDate,'%Y-%m-%d') is not null and date_format(NEW.RPT_moveOutDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_moveOutDate~') into fieldNameList;
    IF(OLD.RPT_moveOutDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RPT_moveOutDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RPT_moveOutDate is not null) THEN
		select CONCAT(oldValueList,OLD.RPT_moveOutDate,'~') into oldValueList;
	END IF;
	IF(NEW.RPT_moveOutDate is not null) THEN
		select CONCAT(newValueList,NEW.RPT_moveOutDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RPT_negotiatedDepositReturnedDate,'%Y-%m-%d') <> date_format(NEW.RPT_negotiatedDepositReturnedDate,'%Y-%m-%d')) or (date_format(OLD.RPT_negotiatedDepositReturnedDate,'%Y-%m-%d') is null and date_format(NEW.RPT_negotiatedDepositReturnedDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RPT_negotiatedDepositReturnedDate,'%Y-%m-%d') is not null and date_format(NEW.RPT_negotiatedDepositReturnedDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_negotiatedDepositReturnedDate~') into fieldNameList;
    IF(OLD.RPT_negotiatedDepositReturnedDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RPT_negotiatedDepositReturnedDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RPT_negotiatedDepositReturnedDate is not null) THEN
		select CONCAT(oldValueList,OLD.RPT_negotiatedDepositReturnedDate,'~') into oldValueList;
	END IF;
	IF(NEW.RPT_negotiatedDepositReturnedDate is not null) THEN
		select CONCAT(newValueList,NEW.RPT_negotiatedDepositReturnedDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SCH_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.SCH_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.SCH_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.SCH_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.SCH_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.SCH_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_serviceStartDate~') into fieldNameList;
    IF(OLD.SCH_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SCH_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SCH_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.SCH_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.SCH_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.SCH_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SCH_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.SCH_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.SCH_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.SCH_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.SCH_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.SCH_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_serviceEndDate~') into fieldNameList;
    IF(OLD.SCH_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SCH_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SCH_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.SCH_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.SCH_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.SCH_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SCH_prearrivalDiscussionBetweenProviderAndFamily,'%Y-%m-%d') <> date_format(NEW.SCH_prearrivalDiscussionBetweenProviderAndFamily,'%Y-%m-%d')) or (date_format(OLD.SCH_prearrivalDiscussionBetweenProviderAndFamily,'%Y-%m-%d') is null and date_format(NEW.SCH_prearrivalDiscussionBetweenProviderAndFamily,'%Y-%m-%d') is not null)
      or (date_format(OLD.SCH_prearrivalDiscussionBetweenProviderAndFamily,'%Y-%m-%d') is not null and date_format(NEW.SCH_prearrivalDiscussionBetweenProviderAndFamily,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_prearrivalDiscussionBetweenProviderAndFamily~') into fieldNameList;
    IF(OLD.SCH_prearrivalDiscussionBetweenProviderAndFamily is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SCH_prearrivalDiscussionBetweenProviderAndFamily is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SCH_prearrivalDiscussionBetweenProviderAndFamily is not null) THEN
		select CONCAT(oldValueList,OLD.SCH_prearrivalDiscussionBetweenProviderAndFamily,'~') into oldValueList;
	END IF;
	IF(NEW.SCH_prearrivalDiscussionBetweenProviderAndFamily is not null) THEN
		select CONCAT(newValueList,NEW.SCH_prearrivalDiscussionBetweenProviderAndFamily,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SCH_admissionDate,'%Y-%m-%d') <> date_format(NEW.SCH_admissionDate,'%Y-%m-%d')) or (date_format(OLD.SCH_admissionDate,'%Y-%m-%d') is null and date_format(NEW.SCH_admissionDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.SCH_admissionDate,'%Y-%m-%d') is not null and date_format(NEW.SCH_admissionDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_admissionDate~') into fieldNameList;
    IF(OLD.SCH_admissionDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SCH_admissionDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SCH_admissionDate is not null) THEN
		select CONCAT(oldValueList,OLD.SCH_admissionDate,'~') into oldValueList;
	END IF;
	IF(NEW.SCH_admissionDate is not null) THEN
		select CONCAT(newValueList,NEW.SCH_admissionDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TAX_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.TAX_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.TAX_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.TAX_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAX_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.TAX_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_serviceStartDate~') into fieldNameList;
    IF(OLD.TAX_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAX_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAX_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.TAX_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.TAX_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.TAX_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TAX_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.TAX_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.TAX_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.TAX_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAX_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.TAX_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_serviceEndDate~') into fieldNameList;
    IF(OLD.TAX_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAX_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAX_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.TAX_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.TAX_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.TAX_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TAX_notificationdate,'%Y-%m-%d') <> date_format(NEW.TAX_notificationdate,'%Y-%m-%d')) or (date_format(OLD.TAX_notificationdate,'%Y-%m-%d') is null and date_format(NEW.TAX_notificationdate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAX_notificationdate,'%Y-%m-%d') is not null and date_format(NEW.TAX_notificationdate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_notificationdate~') into fieldNameList;
    IF(OLD.TAX_notificationdate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAX_notificationdate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAX_notificationdate is not null) THEN
		select CONCAT(oldValueList,OLD.TAX_notificationdate,'~') into oldValueList;
	END IF;
	IF(NEW.TAX_notificationdate is not null) THEN
		select CONCAT(newValueList,NEW.TAX_notificationdate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TAC_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.TAC_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.TAC_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.TAC_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAC_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.TAC_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_serviceStartDate~') into fieldNameList;
    IF(OLD.TAC_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAC_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAC_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.TAC_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.TAC_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.TAC_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TAC_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.TAC_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.TAC_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.TAC_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAC_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.TAC_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_serviceEndDate~') into fieldNameList;
    IF(OLD.TAC_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAC_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAC_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.TAC_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.TAC_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.TAC_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TEN_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.TEN_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.TEN_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.TEN_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.TEN_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_serviceStartDate~') into fieldNameList;
    IF(OLD.TEN_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.TEN_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TEN_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.TEN_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.TEN_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.TEN_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.TEN_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_serviceEndDate~') into fieldNameList;
    IF(OLD.TEN_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.TEN_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.VIS_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.VIS_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.VIS_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.VIS_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_serviceStartDate~') into fieldNameList;
    IF(OLD.VIS_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.VIS_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.VIS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.VIS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.VIS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.VIS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_serviceEndDate~') into fieldNameList;
    IF(OLD.VIS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.VIS_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_providerNotificationDate,'%Y-%m-%d') <> date_format(NEW.VIS_providerNotificationDate,'%Y-%m-%d')) or (date_format(OLD.VIS_providerNotificationDate,'%Y-%m-%d') is null and date_format(NEW.VIS_providerNotificationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_providerNotificationDate,'%Y-%m-%d') is not null and date_format(NEW.VIS_providerNotificationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_providerNotificationDate~') into fieldNameList;
    IF(OLD.VIS_providerNotificationDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_providerNotificationDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_providerNotificationDate is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_providerNotificationDate,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_providerNotificationDate is not null) THEN
		select CONCAT(newValueList,NEW.VIS_providerNotificationDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_visaExpiryDate,'%Y-%m-%d') <> date_format(NEW.VIS_visaExpiryDate,'%Y-%m-%d')) or (date_format(OLD.VIS_visaExpiryDate,'%Y-%m-%d') is null and date_format(NEW.VIS_visaExpiryDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_visaExpiryDate,'%Y-%m-%d') is not null and date_format(NEW.VIS_visaExpiryDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_visaExpiryDate~') into fieldNameList;
    IF(OLD.VIS_visaExpiryDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_visaExpiryDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_visaExpiryDate is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_visaExpiryDate,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_visaExpiryDate is not null) THEN
		select CONCAT(newValueList,NEW.VIS_visaExpiryDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_workPermitExpiry,'%Y-%m-%d') <> date_format(NEW.VIS_workPermitExpiry,'%Y-%m-%d')) or (date_format(OLD.VIS_workPermitExpiry,'%Y-%m-%d') is null and date_format(NEW.VIS_workPermitExpiry,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_workPermitExpiry,'%Y-%m-%d') is not null and date_format(NEW.VIS_workPermitExpiry,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_workPermitExpiry~') into fieldNameList;
    IF(OLD.VIS_workPermitExpiry is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_workPermitExpiry is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_workPermitExpiry is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_workPermitExpiry,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_workPermitExpiry is not null) THEN
		select CONCAT(newValueList,NEW.VIS_workPermitExpiry,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_expiryReminder3MosPriorToExpiry,'%Y-%m-%d') <> date_format(NEW.VIS_expiryReminder3MosPriorToExpiry,'%Y-%m-%d')) or (date_format(OLD.VIS_expiryReminder3MosPriorToExpiry,'%Y-%m-%d') is null and date_format(NEW.VIS_expiryReminder3MosPriorToExpiry,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_expiryReminder3MosPriorToExpiry,'%Y-%m-%d') is not null and date_format(NEW.VIS_expiryReminder3MosPriorToExpiry,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_expiryReminder3MosPriorToExpiry~') into fieldNameList;
    IF(OLD.VIS_expiryReminder3MosPriorToExpiry is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_expiryReminder3MosPriorToExpiry is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_expiryReminder3MosPriorToExpiry is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_expiryReminder3MosPriorToExpiry,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_expiryReminder3MosPriorToExpiry is not null) THEN
		select CONCAT(newValueList,NEW.VIS_expiryReminder3MosPriorToExpiry,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CAR_notificationDate,'%Y-%m-%d') <> date_format(NEW.CAR_notificationDate,'%Y-%m-%d')) or (date_format(OLD.CAR_notificationDate,'%Y-%m-%d') is null and date_format(NEW.CAR_notificationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAR_notificationDate,'%Y-%m-%d') is not null and date_format(NEW.CAR_notificationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_notificationDate~') into fieldNameList;
    IF(OLD.CAR_notificationDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAR_notificationDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAR_notificationDate is not null) THEN
		select CONCAT(oldValueList,OLD.CAR_notificationDate,'~') into oldValueList;
	END IF;
	IF(NEW.CAR_notificationDate is not null) THEN
		select CONCAT(newValueList,NEW.CAR_notificationDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CAR_pickUpDate,'%Y-%m-%d') <> date_format(NEW.CAR_pickUpDate,'%Y-%m-%d')) or (date_format(OLD.CAR_pickUpDate,'%Y-%m-%d') is null and date_format(NEW.CAR_pickUpDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAR_pickUpDate,'%Y-%m-%d') is not null and date_format(NEW.CAR_pickUpDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_pickUpDate~') into fieldNameList;
    IF(OLD.CAR_pickUpDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAR_pickUpDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAR_pickUpDate is not null) THEN
		select CONCAT(oldValueList,OLD.CAR_pickUpDate,'~') into oldValueList;
	END IF;
	IF(NEW.CAR_pickUpDate is not null) THEN
		select CONCAT(newValueList,NEW.CAR_pickUpDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CAR_registrationDate,'%Y-%m-%d') <> date_format(NEW.CAR_registrationDate,'%Y-%m-%d')) or (date_format(OLD.CAR_registrationDate,'%Y-%m-%d') is null and date_format(NEW.CAR_registrationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAR_registrationDate,'%Y-%m-%d') is not null and date_format(NEW.CAR_registrationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_registrationDate~') into fieldNameList;
    IF(OLD.CAR_registrationDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAR_registrationDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAR_registrationDate is not null) THEN
		select CONCAT(oldValueList,OLD.CAR_registrationDate,'~') into oldValueList;
	END IF;
	IF(NEW.CAR_registrationDate is not null) THEN
		select CONCAT(newValueList,NEW.CAR_registrationDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CAR_deliveryDate,'%Y-%m-%d') <> date_format(NEW.CAR_deliveryDate,'%Y-%m-%d')) or (date_format(OLD.CAR_deliveryDate,'%Y-%m-%d') is null and date_format(NEW.CAR_deliveryDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAR_deliveryDate,'%Y-%m-%d') is not null and date_format(NEW.CAR_deliveryDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_deliveryDate~') into fieldNameList;
    IF(OLD.CAR_deliveryDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAR_deliveryDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAR_deliveryDate is not null) THEN
		select CONCAT(oldValueList,OLD.CAR_deliveryDate,'~') into oldValueList;
	END IF;
	IF(NEW.CAR_deliveryDate is not null) THEN
		select CONCAT(newValueList,NEW.CAR_deliveryDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_applicationDateWork,'%Y-%m-%d') <> date_format(NEW.VIS_applicationDateWork,'%Y-%m-%d')) or (date_format(OLD.VIS_applicationDateWork,'%Y-%m-%d') is null and date_format(NEW.VIS_applicationDateWork,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_applicationDateWork,'%Y-%m-%d') is not null and date_format(NEW.VIS_applicationDateWork,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_applicationDateWork~') into fieldNameList;
    IF(OLD.VIS_applicationDateWork is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_applicationDateWork is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_applicationDateWork is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_applicationDateWork,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_applicationDateWork is not null) THEN
		select CONCAT(newValueList,NEW.VIS_applicationDateWork,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_permitStartDateWork,'%Y-%m-%d') <> date_format(NEW.VIS_permitStartDateWork,'%Y-%m-%d')) or (date_format(OLD.VIS_permitStartDateWork,'%Y-%m-%d') is null and date_format(NEW.VIS_permitStartDateWork,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_permitStartDateWork,'%Y-%m-%d') is not null and date_format(NEW.VIS_permitStartDateWork,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_permitStartDateWork~') into fieldNameList;
    IF(OLD.VIS_permitStartDateWork is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_permitStartDateWork is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_permitStartDateWork is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_permitStartDateWork,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_permitStartDateWork is not null) THEN
		select CONCAT(newValueList,NEW.VIS_permitStartDateWork,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_applicationDateResidence,'%Y-%m-%d') <> date_format(NEW.VIS_applicationDateResidence,'%Y-%m-%d')) or (date_format(OLD.VIS_applicationDateResidence,'%Y-%m-%d') is null and date_format(NEW.VIS_applicationDateResidence,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_applicationDateResidence,'%Y-%m-%d') is not null and date_format(NEW.VIS_applicationDateResidence,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_applicationDateResidence~') into fieldNameList;
    IF(OLD.VIS_applicationDateResidence is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_applicationDateResidence is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_applicationDateResidence is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_applicationDateResidence,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_applicationDateResidence is not null) THEN
		select CONCAT(newValueList,NEW.VIS_applicationDateResidence,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_permitStartDateResidence,'%Y-%m-%d') <> date_format(NEW.VIS_permitStartDateResidence,'%Y-%m-%d')) or (date_format(OLD.VIS_permitStartDateResidence,'%Y-%m-%d') is null and date_format(NEW.VIS_permitStartDateResidence,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_permitStartDateResidence,'%Y-%m-%d') is not null and date_format(NEW.VIS_permitStartDateResidence,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_permitStartDateResidence~') into fieldNameList;
    IF(OLD.VIS_permitStartDateResidence is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_permitStartDateResidence is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_permitStartDateResidence is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_permitStartDateResidence,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_permitStartDateResidence is not null) THEN
		select CONCAT(newValueList,NEW.VIS_permitStartDateResidence,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.serviceCompleteDate,'%Y-%m-%d') <> date_format(NEW.serviceCompleteDate,'%Y-%m-%d')) or (date_format(OLD.serviceCompleteDate,'%Y-%m-%d') is null and date_format(NEW.serviceCompleteDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.serviceCompleteDate,'%Y-%m-%d') is not null and date_format(NEW.serviceCompleteDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.serviceCompleteDate~') into fieldNameList;
    IF(OLD.serviceCompleteDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.serviceCompleteDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.serviceCompleteDate is not null) THEN
		select CONCAT(oldValueList,OLD.serviceCompleteDate,'~') into oldValueList;
	END IF;
	IF(NEW.serviceCompleteDate is not null) THEN
		select CONCAT(newValueList,NEW.serviceCompleteDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_visaStartDate,'%Y-%m-%d') <> date_format(NEW.VIS_visaStartDate,'%Y-%m-%d')) or (date_format(OLD.VIS_visaStartDate,'%Y-%m-%d') is null and date_format(NEW.VIS_visaStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_visaStartDate,'%Y-%m-%d') is not null and date_format(NEW.VIS_visaStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_visaStartDate~') into fieldNameList;
    IF(OLD.VIS_visaStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_visaStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_visaStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_visaStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_visaStartDate is not null) THEN
		select CONCAT(newValueList,NEW.VIS_visaStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.WOP_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.WOP_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.WOP_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.WOP_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.WOP_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.WOP_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_serviceStartDate~') into fieldNameList;
    IF(OLD.WOP_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.WOP_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.WOP_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.WOP_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.WOP_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.WOP_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.WOP_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.WOP_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.WOP_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.WOP_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.WOP_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.WOP_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_serviceEndDate~') into fieldNameList;
    IF(OLD.WOP_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.WOP_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.WOP_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.WOP_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.WOP_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.WOP_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.WOP_providerNotificationDate,'%Y-%m-%d') <> date_format(NEW.WOP_providerNotificationDate,'%Y-%m-%d')) or (date_format(OLD.WOP_providerNotificationDate,'%Y-%m-%d') is null and date_format(NEW.WOP_providerNotificationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.WOP_providerNotificationDate,'%Y-%m-%d') is not null and date_format(NEW.WOP_providerNotificationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_providerNotificationDate~') into fieldNameList;
    IF(OLD.WOP_providerNotificationDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.WOP_providerNotificationDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.WOP_providerNotificationDate is not null) THEN
		select CONCAT(oldValueList,OLD.WOP_providerNotificationDate,'~') into oldValueList;
	END IF;
	IF(NEW.WOP_providerNotificationDate is not null) THEN
		select CONCAT(newValueList,NEW.WOP_providerNotificationDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.WOP_permitStartDate,'%Y-%m-%d') <> date_format(NEW.WOP_permitStartDate,'%Y-%m-%d')) or (date_format(OLD.WOP_permitStartDate,'%Y-%m-%d') is null and date_format(NEW.WOP_permitStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.WOP_permitStartDate,'%Y-%m-%d') is not null and date_format(NEW.WOP_permitStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_permitStartDate~') into fieldNameList;
    IF(OLD.WOP_permitStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.WOP_permitStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.WOP_permitStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.WOP_permitStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.WOP_permitStartDate is not null) THEN
		select CONCAT(newValueList,NEW.WOP_permitStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.REP_permitExpiryDate,'%Y-%m-%d') <> date_format(NEW.REP_permitExpiryDate,'%Y-%m-%d')) or (date_format(OLD.REP_permitExpiryDate,'%Y-%m-%d') is null and date_format(NEW.REP_permitExpiryDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.REP_permitExpiryDate,'%Y-%m-%d') is not null and date_format(NEW.REP_permitExpiryDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_permitExpiryDate~') into fieldNameList;
    IF(OLD.REP_permitExpiryDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.REP_permitExpiryDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.REP_permitExpiryDate is not null) THEN
		select CONCAT(oldValueList,OLD.REP_permitExpiryDate,'~') into oldValueList;
	END IF;
	IF(NEW.REP_permitExpiryDate is not null) THEN
		select CONCAT(newValueList,NEW.REP_permitExpiryDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.REP_reminder3MosPriorToExpiry,'%Y-%m-%d') <> date_format(NEW.REP_reminder3MosPriorToExpiry,'%Y-%m-%d')) or (date_format(OLD.REP_reminder3MosPriorToExpiry,'%Y-%m-%d') is null and date_format(NEW.REP_reminder3MosPriorToExpiry,'%Y-%m-%d') is not null)
      or (date_format(OLD.REP_reminder3MosPriorToExpiry,'%Y-%m-%d') is not null and date_format(NEW.REP_reminder3MosPriorToExpiry,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_reminder3MosPriorToExpiry~') into fieldNameList;
    IF(OLD.REP_reminder3MosPriorToExpiry is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.REP_reminder3MosPriorToExpiry is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.REP_reminder3MosPriorToExpiry is not null) THEN
		select CONCAT(oldValueList,OLD.REP_reminder3MosPriorToExpiry,'~') into oldValueList;
	END IF;
	IF(NEW.REP_reminder3MosPriorToExpiry is not null) THEN
		select CONCAT(newValueList,NEW.REP_reminder3MosPriorToExpiry,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.WOP_permitExpiryDate,'%Y-%m-%d') <> date_format(NEW.WOP_permitExpiryDate,'%Y-%m-%d')) or (date_format(OLD.WOP_permitExpiryDate,'%Y-%m-%d') is null and date_format(NEW.WOP_permitExpiryDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.WOP_permitExpiryDate,'%Y-%m-%d') is not null and date_format(NEW.WOP_permitExpiryDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_permitExpiryDate~') into fieldNameList;
    IF(OLD.WOP_permitExpiryDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.WOP_permitExpiryDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.WOP_permitExpiryDate is not null) THEN
		select CONCAT(oldValueList,OLD.WOP_permitExpiryDate,'~') into oldValueList;
	END IF;
	IF(NEW.WOP_permitExpiryDate is not null) THEN
		select CONCAT(newValueList,NEW.WOP_permitExpiryDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.WOP_reminder3MosPriorToExpiry,'%Y-%m-%d') <> date_format(NEW.WOP_reminder3MosPriorToExpiry,'%Y-%m-%d')) or (date_format(OLD.WOP_reminder3MosPriorToExpiry,'%Y-%m-%d') is null and date_format(NEW.WOP_reminder3MosPriorToExpiry,'%Y-%m-%d') is not null)
      or (date_format(OLD.WOP_reminder3MosPriorToExpiry,'%Y-%m-%d') is not null and date_format(NEW.WOP_reminder3MosPriorToExpiry,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_reminder3MosPriorToExpiry~') into fieldNameList;
    IF(OLD.WOP_reminder3MosPriorToExpiry is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.WOP_reminder3MosPriorToExpiry is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.WOP_reminder3MosPriorToExpiry is not null) THEN
		select CONCAT(oldValueList,OLD.WOP_reminder3MosPriorToExpiry,'~') into oldValueList;
	END IF;
	IF(NEW.WOP_reminder3MosPriorToExpiry is not null) THEN
		select CONCAT(newValueList,NEW.WOP_reminder3MosPriorToExpiry,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.REP_questionnaireSentDate,'%Y-%m-%d') <> date_format(NEW.REP_questionnaireSentDate,'%Y-%m-%d')) or (date_format(OLD.REP_questionnaireSentDate,'%Y-%m-%d') is null and date_format(NEW.REP_questionnaireSentDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.REP_questionnaireSentDate,'%Y-%m-%d') is not null and date_format(NEW.REP_questionnaireSentDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_questionnaireSentDate~') into fieldNameList;
    IF(OLD.REP_questionnaireSentDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.REP_questionnaireSentDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.REP_questionnaireSentDate is not null) THEN
		select CONCAT(oldValueList,OLD.REP_questionnaireSentDate,'~') into oldValueList;
	END IF;
	IF(NEW.REP_questionnaireSentDate is not null) THEN
		select CONCAT(newValueList,NEW.REP_questionnaireSentDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RLS_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.RLS_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.RLS_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.RLS_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RLS_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.RLS_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RLS_serviceStartDate~') into fieldNameList;
    IF(OLD.RLS_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RLS_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RLS_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.RLS_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.RLS_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.RLS_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RLS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.RLS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.RLS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.RLS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RLS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.RLS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RLS_serviceEndDate~') into fieldNameList;
    IF(OLD.RLS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RLS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RLS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.RLS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.RLS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.RLS_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CAT_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.CAT_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.CAT_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.CAT_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAT_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.CAT_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_serviceStartDate~') into fieldNameList;
    IF(OLD.CAT_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAT_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAT_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.CAT_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.CAT_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.CAT_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CAT_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.CAT_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.CAT_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.CAT_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAT_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.CAT_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_serviceEndDate~') into fieldNameList;
    IF(OLD.CAT_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAT_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAT_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.CAT_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.CAT_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.CAT_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CLS_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.CLS_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.CLS_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.CLS_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CLS_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.CLS_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_serviceStartDate~') into fieldNameList;
    IF(OLD.CLS_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CLS_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CLS_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.CLS_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.CLS_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.CLS_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CLS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.CLS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.CLS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.CLS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CLS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.CLS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_serviceEndDate~') into fieldNameList;
    IF(OLD.CLS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CLS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CLS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.CLS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.CLS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.CLS_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CHS_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.CHS_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.CHS_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.CHS_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CHS_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.CHS_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_serviceStartDate~') into fieldNameList;
    IF(OLD.CHS_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CHS_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CHS_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.CHS_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.CHS_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.CHS_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.CHS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.CHS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.CHS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.CHS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.CHS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.CHS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_serviceEndDate~') into fieldNameList;
    IF(OLD.CHS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CHS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CHS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.CHS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.CHS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.CHS_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.DPS_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.DPS_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.DPS_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.DPS_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.DPS_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.DPS_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_serviceStartDate~') into fieldNameList;
    IF(OLD.DPS_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.DPS_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.DPS_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.DPS_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.DPS_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.DPS_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.DPS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.DPS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.DPS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.DPS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.DPS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.DPS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_serviceEndDate~') into fieldNameList;
    IF(OLD.DPS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.DPS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.DPS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.DPS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.DPS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.DPS_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HSM_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.HSM_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.HSM_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.HSM_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HSM_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.HSM_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_serviceStartDate~') into fieldNameList;
    IF(OLD.HSM_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HSM_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HSM_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.HSM_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.HSM_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.HSM_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HSM_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.HSM_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.HSM_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.HSM_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HSM_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.HSM_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_serviceEndDate~') into fieldNameList;
    IF(OLD.HSM_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HSM_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HSM_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.HSM_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.HSM_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.HSM_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.PDT_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.PDT_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.PDT_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.PDT_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.PDT_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.PDT_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_serviceStartDate~') into fieldNameList;
    IF(OLD.PDT_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PDT_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PDT_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.PDT_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.PDT_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.PDT_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.PDT_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.PDT_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.PDT_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.PDT_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.PDT_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.PDT_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_serviceEndDate~') into fieldNameList;
    IF(OLD.PDT_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PDT_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PDT_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.PDT_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.PDT_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.PDT_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RCP_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.RCP_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.RCP_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.RCP_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RCP_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.RCP_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_serviceStartDate~') into fieldNameList;
    IF(OLD.RCP_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RCP_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RCP_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.RCP_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.RCP_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.RCP_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RCP_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.RCP_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.RCP_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.RCP_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RCP_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.RCP_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_serviceEndDate~') into fieldNameList;
    IF(OLD.RCP_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RCP_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RCP_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.RCP_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.RCP_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.RCP_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SPA_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.SPA_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.SPA_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.SPA_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.SPA_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.SPA_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_serviceStartDate~') into fieldNameList;
    IF(OLD.SPA_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SPA_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SPA_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.SPA_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.SPA_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.SPA_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SPA_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.SPA_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.SPA_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.SPA_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.SPA_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.SPA_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_serviceEndDate~') into fieldNameList;
    IF(OLD.SPA_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SPA_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SPA_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.SPA_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.SPA_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.SPA_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TCS_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.TCS_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.TCS_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.TCS_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TCS_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.TCS_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_serviceStartDate~') into fieldNameList;
    IF(OLD.TCS_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TCS_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TCS_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.TCS_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.TCS_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.TCS_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TCS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.TCS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.TCS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.TCS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TCS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.TCS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_serviceEndDate~') into fieldNameList;
    IF(OLD.TCS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TCS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TCS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.TCS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.TCS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.TCS_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.WOP_questionnaireSentDate,'%Y-%m-%d') <> date_format(NEW.WOP_questionnaireSentDate,'%Y-%m-%d')) or (date_format(OLD.WOP_questionnaireSentDate,'%Y-%m-%d') is null and date_format(NEW.WOP_questionnaireSentDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.WOP_questionnaireSentDate,'%Y-%m-%d') is not null and date_format(NEW.WOP_questionnaireSentDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_questionnaireSentDate~') into fieldNameList;
    IF(OLD.WOP_questionnaireSentDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.WOP_questionnaireSentDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.WOP_questionnaireSentDate is not null) THEN
		select CONCAT(oldValueList,OLD.WOP_questionnaireSentDate,'~') into oldValueList;
	END IF;
	IF(NEW.WOP_questionnaireSentDate is not null) THEN
		select CONCAT(newValueList,NEW.WOP_questionnaireSentDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.REP_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.REP_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.REP_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.REP_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.REP_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.REP_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_serviceStartDate~') into fieldNameList;
    IF(OLD.REP_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.REP_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.REP_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.REP_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.REP_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.REP_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.REP_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.REP_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.REP_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.REP_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.REP_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.REP_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_serviceEndDate~') into fieldNameList;
    IF(OLD.REP_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.REP_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.REP_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.REP_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.REP_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.REP_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.REP_providerNotificationDate,'%Y-%m-%d') <> date_format(NEW.REP_providerNotificationDate,'%Y-%m-%d')) or (date_format(OLD.REP_providerNotificationDate,'%Y-%m-%d') is null and date_format(NEW.REP_providerNotificationDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.REP_providerNotificationDate,'%Y-%m-%d') is not null and date_format(NEW.REP_providerNotificationDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_providerNotificationDate~') into fieldNameList;
    IF(OLD.REP_providerNotificationDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.REP_providerNotificationDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.REP_providerNotificationDate is not null) THEN
		select CONCAT(oldValueList,OLD.REP_providerNotificationDate,'~') into oldValueList;
	END IF;
	IF(NEW.REP_providerNotificationDate is not null) THEN
		select CONCAT(newValueList,NEW.REP_providerNotificationDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.REP_permitStartDate,'%Y-%m-%d') <> date_format(NEW.REP_permitStartDate,'%Y-%m-%d')) or (date_format(OLD.REP_permitStartDate,'%Y-%m-%d') is null and date_format(NEW.REP_permitStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.REP_permitStartDate,'%Y-%m-%d') is not null and date_format(NEW.REP_permitStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_permitStartDate~') into fieldNameList;
    IF(OLD.REP_permitStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.REP_permitStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.REP_permitStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.REP_permitStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.REP_permitStartDate is not null) THEN
		select CONCAT(newValueList,NEW.REP_permitStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.MTS_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.MTS_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.MTS_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.MTS_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.MTS_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.MTS_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_serviceStartDate~') into fieldNameList;
    IF(OLD.MTS_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.MTS_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.MTS_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.MTS_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.MTS_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.MTS_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.MTS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.MTS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.MTS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.MTS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.MTS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.MTS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_serviceEndDate~') into fieldNameList;
    IF(OLD.MTS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.MTS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.MTS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.MTS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.MTS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.MTS_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.RNT_checkInDate,'%Y-%m-%d') <> date_format(NEW.RNT_checkInDate,'%Y-%m-%d')) or (date_format(OLD.RNT_checkInDate,'%Y-%m-%d') is null and date_format(NEW.RNT_checkInDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.RNT_checkInDate,'%Y-%m-%d') is not null and date_format(NEW.RNT_checkInDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_checkInDate~') into fieldNameList;
    IF(OLD.RNT_checkInDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RNT_checkInDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RNT_checkInDate is not null) THEN
		select CONCAT(oldValueList,OLD.RNT_checkInDate,'~') into oldValueList;
	END IF;
	IF(NEW.RNT_checkInDate is not null) THEN
		select CONCAT(newValueList,NEW.RNT_checkInDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_governmentID,'%Y-%m-%d') <> date_format(NEW.SET_governmentID,'%Y-%m-%d')) or (date_format(OLD.SET_governmentID,'%Y-%m-%d') is null and date_format(NEW.SET_governmentID,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_governmentID,'%Y-%m-%d') is not null and date_format(NEW.SET_governmentID,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_governmentID~') into fieldNameList;
    IF(OLD.SET_governmentID is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_governmentID is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_governmentID is not null) THEN
		select CONCAT(oldValueList,OLD.SET_governmentID,'~') into oldValueList;
	END IF;
	IF(NEW.SET_governmentID is not null) THEN
		select CONCAT(newValueList,NEW.SET_governmentID,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_heathCare,'%Y-%m-%d') <> date_format(NEW.SET_heathCare,'%Y-%m-%d')) or (date_format(OLD.SET_heathCare,'%Y-%m-%d') is null and date_format(NEW.SET_heathCare,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_heathCare,'%Y-%m-%d') is not null and date_format(NEW.SET_heathCare,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_heathCare~') into fieldNameList;
    IF(OLD.SET_heathCare is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_heathCare is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_heathCare is not null) THEN
		select CONCAT(oldValueList,OLD.SET_heathCare,'~') into oldValueList;
	END IF;
	IF(NEW.SET_heathCare is not null) THEN
		select CONCAT(newValueList,NEW.SET_heathCare,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_utilities,'%Y-%m-%d') <> date_format(NEW.SET_utilities,'%Y-%m-%d')) or (date_format(OLD.SET_utilities,'%Y-%m-%d') is null and date_format(NEW.SET_utilities,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_utilities,'%Y-%m-%d') is not null and date_format(NEW.SET_utilities,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_utilities~') into fieldNameList;
    IF(OLD.SET_utilities is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_utilities is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_utilities is not null) THEN
		select CONCAT(oldValueList,OLD.SET_utilities,'~') into oldValueList;
	END IF;
	IF(NEW.SET_utilities is not null) THEN
		select CONCAT(newValueList,NEW.SET_utilities,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_automobileRegistration,'%Y-%m-%d') <> date_format(NEW.SET_automobileRegistration,'%Y-%m-%d')) or (date_format(OLD.SET_automobileRegistration,'%Y-%m-%d') is null and date_format(NEW.SET_automobileRegistration,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_automobileRegistration,'%Y-%m-%d') is not null and date_format(NEW.SET_automobileRegistration,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_automobileRegistration~') into fieldNameList;
    IF(OLD.SET_automobileRegistration is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_automobileRegistration is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_automobileRegistration is not null) THEN
		select CONCAT(oldValueList,OLD.SET_automobileRegistration,'~') into oldValueList;
	END IF;
	IF(NEW.SET_automobileRegistration is not null) THEN
		select CONCAT(newValueList,NEW.SET_automobileRegistration,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_dayofServiceAuthorized,'%Y-%m-%d') <> date_format(NEW.SET_dayofServiceAuthorized,'%Y-%m-%d')) or (date_format(OLD.SET_dayofServiceAuthorized,'%Y-%m-%d') is null and date_format(NEW.SET_dayofServiceAuthorized,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_dayofServiceAuthorized,'%Y-%m-%d') is not null and date_format(NEW.SET_dayofServiceAuthorized,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_dayofServiceAuthorized~') into fieldNameList;
    IF(OLD.SET_dayofServiceAuthorized is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_dayofServiceAuthorized is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_dayofServiceAuthorized is not null) THEN
		select CONCAT(oldValueList,OLD.SET_dayofServiceAuthorized,'~') into oldValueList;
	END IF;
	IF(NEW.SET_dayofServiceAuthorized is not null) THEN
		select CONCAT(newValueList,NEW.SET_dayofServiceAuthorized,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_initialDateofServiceRequested,'%Y-%m-%d') <> date_format(NEW.SET_initialDateofServiceRequested,'%Y-%m-%d')) or (date_format(OLD.SET_initialDateofServiceRequested,'%Y-%m-%d') is null and date_format(NEW.SET_initialDateofServiceRequested,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_initialDateofServiceRequested,'%Y-%m-%d') is not null and date_format(NEW.SET_initialDateofServiceRequested,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_initialDateofServiceRequested~') into fieldNameList;
    IF(OLD.SET_initialDateofServiceRequested is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_initialDateofServiceRequested is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_initialDateofServiceRequested is not null) THEN
		select CONCAT(oldValueList,OLD.SET_initialDateofServiceRequested,'~') into oldValueList;
	END IF;
	IF(NEW.SET_initialDateofServiceRequested is not null) THEN
		select CONCAT(newValueList,NEW.SET_initialDateofServiceRequested,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_providerNotificationSent,'%Y-%m-%d') <> date_format(NEW.SET_providerNotificationSent,'%Y-%m-%d')) or (date_format(OLD.SET_providerNotificationSent,'%Y-%m-%d') is null and date_format(NEW.SET_providerNotificationSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_providerNotificationSent,'%Y-%m-%d') is not null and date_format(NEW.SET_providerNotificationSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_providerNotificationSent~') into fieldNameList;
    IF(OLD.SET_providerNotificationSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_providerNotificationSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_providerNotificationSent is not null) THEN
		select CONCAT(oldValueList,OLD.SET_providerNotificationSent,'~') into oldValueList;
	END IF;
	IF(NEW.SET_providerNotificationSent is not null) THEN
		select CONCAT(newValueList,NEW.SET_providerNotificationSent,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.SET_providerConfirmationReceived,'%Y-%m-%d') <> date_format(NEW.SET_providerConfirmationReceived,'%Y-%m-%d')) or (date_format(OLD.SET_providerConfirmationReceived,'%Y-%m-%d') is null and date_format(NEW.SET_providerConfirmationReceived,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_providerConfirmationReceived,'%Y-%m-%d') is not null and date_format(NEW.SET_providerConfirmationReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_providerConfirmationReceived~') into fieldNameList;
    IF(OLD.SET_providerConfirmationReceived is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_providerConfirmationReceived is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_providerConfirmationReceived is not null) THEN
		select CONCAT(oldValueList,OLD.SET_providerConfirmationReceived,'~') into oldValueList;
	END IF;
	IF(NEW.SET_providerConfirmationReceived is not null) THEN
		select CONCAT(newValueList,NEW.SET_providerConfirmationReceived,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.VIS_arrivalDate,'%Y-%m-%d') <> date_format(NEW.VIS_arrivalDate,'%Y-%m-%d')) or (date_format(OLD.VIS_arrivalDate,'%Y-%m-%d') is null and date_format(NEW.VIS_arrivalDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_arrivalDate,'%Y-%m-%d') is not null and date_format(NEW.VIS_arrivalDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_arrivalDate~') into fieldNameList;
    IF(OLD.VIS_arrivalDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_arrivalDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_arrivalDate is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_arrivalDate,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_arrivalDate is not null) THEN
		select CONCAT(newValueList,NEW.VIS_arrivalDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.DSS_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.DSS_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.DSS_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.DSS_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.DSS_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.DSS_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_serviceStartDate~') into fieldNameList;
    IF(OLD.DSS_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.DSS_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.DSS_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.DSS_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.DSS_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.DSS_serviceStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.DSS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.DSS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.DSS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.DSS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.DSS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.DSS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_serviceEndDate~') into fieldNameList;
    IF(OLD.DSS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.DSS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.DSS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.DSS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.DSS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.DSS_serviceEndDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.DSS_initialContactDate,'%Y-%m-%d') <> date_format(NEW.DSS_initialContactDate,'%Y-%m-%d')) or (date_format(OLD.DSS_initialContactDate,'%Y-%m-%d') is null and date_format(NEW.DSS_initialContactDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.DSS_initialContactDate,'%Y-%m-%d') is not null and date_format(NEW.DSS_initialContactDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_initialContactDate~') into fieldNameList;
    IF(OLD.DSS_initialContactDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.DSS_initialContactDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.DSS_initialContactDate is not null) THEN
		select CONCAT(oldValueList,OLD.DSS_initialContactDate,'~') into oldValueList;
	END IF;
	IF(NEW.DSS_initialContactDate is not null) THEN
		select CONCAT(newValueList,NEW.DSS_initialContactDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.DPS_completionDate,'%Y-%m-%d') <> date_format(NEW.DPS_completionDate,'%Y-%m-%d')) or (date_format(OLD.DPS_completionDate,'%Y-%m-%d') is null and date_format(NEW.DPS_completionDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.DPS_completionDate,'%Y-%m-%d') is not null and date_format(NEW.DPS_completionDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_completionDate~') into fieldNameList;
    IF(OLD.DPS_completionDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.DPS_completionDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.DPS_completionDate is not null) THEN
		select CONCAT(oldValueList,OLD.DPS_completionDate,'~') into oldValueList;
	END IF;
	IF(NEW.DPS_completionDate is not null) THEN
		select CONCAT(newValueList,NEW.DPS_completionDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HSM_marketingPlanNBMAReceived,'%Y-%m-%d') <> date_format(NEW.HSM_marketingPlanNBMAReceived,'%Y-%m-%d')) or (date_format(OLD.HSM_marketingPlanNBMAReceived,'%Y-%m-%d') is null and date_format(NEW.HSM_marketingPlanNBMAReceived,'%Y-%m-%d') is not null)
      or (date_format(OLD.HSM_marketingPlanNBMAReceived,'%Y-%m-%d') is not null and date_format(NEW.HSM_marketingPlanNBMAReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_marketingPlanNBMAReceived~') into fieldNameList;
    IF(OLD.HSM_marketingPlanNBMAReceived is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HSM_marketingPlanNBMAReceived is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HSM_marketingPlanNBMAReceived is not null) THEN
		select CONCAT(oldValueList,OLD.HSM_marketingPlanNBMAReceived,'~') into oldValueList;
	END IF;
	IF(NEW.HSM_marketingPlanNBMAReceived is not null) THEN
		select CONCAT(newValueList,NEW.HSM_marketingPlanNBMAReceived,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.MTS_initialContact,'%Y-%m-%d') <> date_format(NEW.MTS_initialContact,'%Y-%m-%d')) or (date_format(OLD.MTS_initialContact,'%Y-%m-%d') is null and date_format(NEW.MTS_initialContact,'%Y-%m-%d') is not null)
      or (date_format(OLD.MTS_initialContact,'%Y-%m-%d') is not null and date_format(NEW.MTS_initialContact,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_initialContact~') into fieldNameList;
    IF(OLD.MTS_initialContact is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.MTS_initialContact is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.MTS_initialContact is not null) THEN
		select CONCAT(oldValueList,OLD.MTS_initialContact,'~') into oldValueList;
	END IF;
	IF(NEW.MTS_initialContact is not null) THEN
		select CONCAT(newValueList,NEW.MTS_initialContact,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TEN_leaseStartDate,'%Y-%m-%d') <> date_format(NEW.TEN_leaseStartDate,'%Y-%m-%d')) or (date_format(OLD.TEN_leaseStartDate,'%Y-%m-%d') is null and date_format(NEW.TEN_leaseStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_leaseStartDate,'%Y-%m-%d') is not null and date_format(NEW.TEN_leaseStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_leaseStartDate~') into fieldNameList;
    IF(OLD.TEN_leaseStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_leaseStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_leaseStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_leaseStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_leaseStartDate is not null) THEN
		select CONCAT(newValueList,NEW.TEN_leaseStartDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TEN_leaseExpireDate,'%Y-%m-%d') <> date_format(NEW.TEN_leaseExpireDate,'%Y-%m-%d')) or (date_format(OLD.TEN_leaseExpireDate,'%Y-%m-%d') is null and date_format(NEW.TEN_leaseExpireDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_leaseExpireDate,'%Y-%m-%d') is not null and date_format(NEW.TEN_leaseExpireDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_leaseExpireDate~') into fieldNameList;
    IF(OLD.TEN_leaseExpireDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_leaseExpireDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_leaseExpireDate is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_leaseExpireDate,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_leaseExpireDate is not null) THEN
		select CONCAT(newValueList,NEW.TEN_leaseExpireDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.TEN_expiryReminderPriorToExpiry,'%Y-%m-%d') <> date_format(NEW.TEN_expiryReminderPriorToExpiry,'%Y-%m-%d')) or (date_format(OLD.TEN_expiryReminderPriorToExpiry,'%Y-%m-%d') is null and date_format(NEW.TEN_expiryReminderPriorToExpiry,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_expiryReminderPriorToExpiry,'%Y-%m-%d') is not null and date_format(NEW.TEN_expiryReminderPriorToExpiry,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_expiryReminderPriorToExpiry~') into fieldNameList;
    IF(OLD.TEN_expiryReminderPriorToExpiry is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_expiryReminderPriorToExpiry is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_expiryReminderPriorToExpiry is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_expiryReminderPriorToExpiry,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_expiryReminderPriorToExpiry is not null) THEN
		select CONCAT(newValueList,NEW.TEN_expiryReminderPriorToExpiry,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HOM_initialContactDate,'%Y-%m-%d') <> date_format(NEW.HOM_initialContactDate,'%Y-%m-%d')) or (date_format(OLD.HOM_initialContactDate,'%Y-%m-%d') is null and date_format(NEW.HOM_initialContactDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOM_initialContactDate,'%Y-%m-%d') is not null and date_format(NEW.HOM_initialContactDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_initialContactDate~') into fieldNameList;
    IF(OLD.HOM_initialContactDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOM_initialContactDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOM_initialContactDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOM_initialContactDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOM_initialContactDate is not null) THEN
		select CONCAT(newValueList,NEW.HOM_initialContactDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HOM_houseHuntingTripArrival,'%Y-%m-%d') <> date_format(NEW.HOM_houseHuntingTripArrival,'%Y-%m-%d')) or (date_format(OLD.HOM_houseHuntingTripArrival,'%Y-%m-%d') is null and date_format(NEW.HOM_houseHuntingTripArrival,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOM_houseHuntingTripArrival,'%Y-%m-%d') is not null and date_format(NEW.HOM_houseHuntingTripArrival,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_houseHuntingTripArrival~') into fieldNameList;
    IF(OLD.HOM_houseHuntingTripArrival is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOM_houseHuntingTripArrival is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOM_houseHuntingTripArrival is not null) THEN
		select CONCAT(oldValueList,OLD.HOM_houseHuntingTripArrival,'~') into oldValueList;
	END IF;
	IF(NEW.HOM_houseHuntingTripArrival is not null) THEN
		select CONCAT(newValueList,NEW.HOM_houseHuntingTripArrival,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HOM_houseHuntingTripDeparture,'%Y-%m-%d') <> date_format(NEW.HOM_houseHuntingTripDeparture,'%Y-%m-%d')) or (date_format(OLD.HOM_houseHuntingTripDeparture,'%Y-%m-%d') is null and date_format(NEW.HOM_houseHuntingTripDeparture,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOM_houseHuntingTripDeparture,'%Y-%m-%d') is not null and date_format(NEW.HOM_houseHuntingTripDeparture,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_houseHuntingTripDeparture~') into fieldNameList;
    IF(OLD.HOM_houseHuntingTripDeparture is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOM_houseHuntingTripDeparture is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOM_houseHuntingTripDeparture is not null) THEN
		select CONCAT(oldValueList,OLD.HOM_houseHuntingTripDeparture,'~') into oldValueList;
	END IF;
	IF(NEW.HOM_houseHuntingTripDeparture is not null) THEN
		select CONCAT(newValueList,NEW.HOM_houseHuntingTripDeparture,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HOM_offerDate,'%Y-%m-%d') <> date_format(NEW.HOM_offerDate,'%Y-%m-%d')) or (date_format(OLD.HOM_offerDate,'%Y-%m-%d') is null and date_format(NEW.HOM_offerDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOM_offerDate,'%Y-%m-%d') is not null and date_format(NEW.HOM_offerDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_offerDate~') into fieldNameList;
    IF(OLD.HOM_offerDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOM_offerDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOM_offerDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOM_offerDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOM_offerDate is not null) THEN
		select CONCAT(newValueList,NEW.HOM_offerDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HOM_closingDate,'%Y-%m-%d') <> date_format(NEW.HOM_closingDate,'%Y-%m-%d')) or (date_format(OLD.HOM_closingDate,'%Y-%m-%d') is null and date_format(NEW.HOM_closingDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOM_closingDate,'%Y-%m-%d') is not null and date_format(NEW.HOM_closingDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_closingDate~') into fieldNameList;
    IF(OLD.HOM_closingDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOM_closingDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOM_closingDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOM_closingDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOM_closingDate is not null) THEN
		select CONCAT(newValueList,NEW.HOM_closingDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HSM_offerDate,'%Y-%m-%d') <> date_format(NEW.HSM_offerDate,'%Y-%m-%d')) or (date_format(OLD.HSM_offerDate,'%Y-%m-%d') is null and date_format(NEW.HSM_offerDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HSM_offerDate,'%Y-%m-%d') is not null and date_format(NEW.HSM_offerDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_offerDate~') into fieldNameList;
    IF(OLD.HSM_offerDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HSM_offerDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HSM_offerDate is not null) THEN
		select CONCAT(oldValueList,OLD.HSM_offerDate,'~') into oldValueList;
	END IF;
	IF(NEW.HSM_offerDate is not null) THEN
		select CONCAT(newValueList,NEW.HSM_offerDate,'~') into newValueList;
	END IF;
   END IF;
IF ((date_format(OLD.HSM_closingDate,'%Y-%m-%d') <> date_format(NEW.HSM_closingDate,'%Y-%m-%d')) or (date_format(OLD.HSM_closingDate,'%Y-%m-%d') is null and date_format(NEW.HSM_closingDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HSM_closingDate,'%Y-%m-%d') is not null and date_format(NEW.HSM_closingDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_closingDate~') into fieldNameList;
    IF(OLD.HSM_closingDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HSM_closingDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HSM_closingDate is not null) THEN
		select CONCAT(oldValueList,OLD.HSM_closingDate,'~') into oldValueList;
	END IF;
	IF(NEW.HSM_closingDate is not null) THEN
		select CONCAT(newValueList,NEW.HSM_closingDate,'~') into newValueList;
	END IF;
   END IF;

IF (OLD.VIS_authorizationLetter <> NEW.VIS_authorizationLetter ) THEN
       select CONCAT(fieldNameList,'dspdetails.VIS_authorizationLetter~') into fieldNameList;
       IF(OLD.VIS_authorizationLetter = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.VIS_authorizationLetter = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.VIS_authorizationLetter = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.VIS_authorizationLetter = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.VIS_copyResidence <> NEW.VIS_copyResidence ) THEN
       select CONCAT(fieldNameList,'dspdetails.VIS_copyResidence~') into fieldNameList;
       IF(OLD.VIS_copyResidence = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.VIS_copyResidence = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.VIS_copyResidence = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.VIS_copyResidence = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.CAR_displyOtherVendorCode <> NEW.CAR_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.CAR_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.CAR_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.CAR_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.CAR_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.CAR_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.COL_displyOtherVendorCode <> NEW.COL_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.COL_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.COL_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.COL_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.COL_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.COL_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.TRG_displyOtherVendorCode <> NEW.TRG_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.TRG_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.TRG_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TRG_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TRG_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TRG_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.HOM_displyOtherVendorCode <> NEW.HOM_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.HOM_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.HOM_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.HOM_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.HOM_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.HOM_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.RNT_displyOtherVendorCode <> NEW.RNT_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.RNT_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.RNT_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.RNT_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.RNT_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.RNT_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.SET_displyOtherVendorCode <> NEW.SET_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.SET_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.SET_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.SET_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.SET_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.SET_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.LAN_displyOtherVendorCode <> NEW.LAN_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.LAN_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.LAN_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.LAN_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.LAN_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.LAN_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.MMG_displyOtherVendorCode <> NEW.MMG_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.MMG_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.MMG_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.MMG_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.MMG_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.MMG_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.ONG_displyOtherVendorCode <> NEW.ONG_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.ONG_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.ONG_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.ONG_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.ONG_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.ONG_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.PRV_displyOtherVendorCode <> NEW.PRV_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.PRV_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.PRV_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.PRV_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.PRV_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.PRV_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.AIO_displyOtherVendorCode <> NEW.AIO_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.AIO_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.AIO_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.AIO_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.AIO_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.AIO_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.EXP_displyOtherVendorCode <> NEW.EXP_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.EXP_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.EXP_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.EXP_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.EXP_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.EXP_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.RPT_displyOtherVendorCode <> NEW.RPT_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.RPT_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.RPT_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.RPT_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.RPT_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.RPT_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.SCH_displyOtherSchoolSelected <> NEW.SCH_displyOtherSchoolSelected ) THEN
       select CONCAT(fieldNameList,'dspdetails.SCH_displyOtherSchoolSelected~') into fieldNameList;
       IF(OLD.SCH_displyOtherSchoolSelected = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.SCH_displyOtherSchoolSelected = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.SCH_displyOtherSchoolSelected = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.SCH_displyOtherSchoolSelected = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.TAX_displyOtherVendorCode <> NEW.TAX_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.TAX_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.TAX_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TAX_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TAX_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TAX_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.TAC_displyOtherVendorCode <> NEW.TAC_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.TAC_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.TAC_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TAC_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TAC_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TAC_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.VIS_displyOtherVendorCode <> NEW.VIS_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.VIS_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.VIS_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.VIS_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.VIS_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.VIS_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.REP_authorizationLetter <> NEW.REP_authorizationLetter ) THEN
       select CONCAT(fieldNameList,'dspdetails.REP_authorizationLetter~') into fieldNameList;
       IF(OLD.REP_authorizationLetter = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.REP_authorizationLetter = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.REP_authorizationLetter = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.REP_authorizationLetter = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Gas_Water <> NEW.TEN_Gas_Water ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Gas_Water~') into fieldNameList;
       IF(OLD.TEN_Gas_Water = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Gas_Water = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Gas_Water = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Gas_Water = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Electricity <> NEW.TEN_Electricity ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Electricity~') into fieldNameList;
       IF(OLD.TEN_Electricity = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Electricity = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Electricity = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Electricity = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Miscellaneous <> NEW.TEN_Miscellaneous ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Miscellaneous~') into fieldNameList;
       IF(OLD.TEN_Miscellaneous = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Miscellaneous = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Miscellaneous = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Miscellaneous = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_Gas <> NEW.TEN_Utility_Gas ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_Gas~') into fieldNameList;
       IF(OLD.TEN_Utility_Gas = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_Gas = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Gas = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Gas = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_Electricity <> NEW.TEN_Utility_Electricity ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_Electricity~') into fieldNameList;
       IF(OLD.TEN_Utility_Electricity = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_Electricity = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Electricity = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Electricity = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_Miscellaneous <> NEW.TEN_Utility_Miscellaneous ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_Miscellaneous~') into fieldNameList;
       IF(OLD.TEN_Utility_Miscellaneous = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_Miscellaneous = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Miscellaneous = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Miscellaneous = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_Gas_Water <> NEW.TEN_Utility_Gas_Water ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_Gas_Water~') into fieldNameList;
       IF(OLD.TEN_Utility_Gas_Water = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_Gas_Water = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Gas_Water = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Gas_Water = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
      
   IF (OLD.TEN_Utility_TV_Internet_Phone <> NEW.TEN_Utility_TV_Internet_Phone ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_TV_Internet_Phone~') into fieldNameList;
       IF(OLD.TEN_Utility_TV_Internet_Phone = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_TV_Internet_Phone = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_TV_Internet_Phone = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_TV_Internet_Phone = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_mobilePhone <> NEW.TEN_Utility_mobilePhone ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_mobilePhone~') into fieldNameList;
       IF(OLD.TEN_Utility_mobilePhone = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_mobilePhone = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_mobilePhone = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_mobilePhone = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_furnitureRental <> NEW.TEN_Utility_furnitureRental ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_furnitureRental~') into fieldNameList;
       IF(OLD.TEN_Utility_furnitureRental = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_furnitureRental = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_furnitureRental = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_furnitureRental = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_cleaningServices <> NEW.TEN_Utility_cleaningServices ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_cleaningServices~') into fieldNameList;
       IF(OLD.TEN_Utility_cleaningServices = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_cleaningServices = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_cleaningServices = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_cleaningServices = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_parkingPermit <> NEW.TEN_Utility_parkingPermit ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_parkingPermit~') into fieldNameList;
       IF(OLD.TEN_Utility_parkingPermit = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_parkingPermit = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_parkingPermit = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_parkingPermit = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_communityTax <> NEW.TEN_Utility_communityTax ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_communityTax~') into fieldNameList;
       IF(OLD.TEN_Utility_communityTax = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_communityTax = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_communityTax = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_communityTax = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_insurance <> NEW.TEN_Utility_insurance ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_insurance~') into fieldNameList;
       IF(OLD.TEN_Utility_insurance = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_insurance = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_insurance = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_insurance = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_Utility_gardenMaintenance <> NEW.TEN_Utility_gardenMaintenance ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_gardenMaintenance~') into fieldNameList;
       IF(OLD.TEN_Utility_gardenMaintenance = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_gardenMaintenance = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_gardenMaintenance = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_gardenMaintenance = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
      
   IF (OLD.TEN_TV_Internet_Phone <> NEW.TEN_TV_Internet_Phone ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_TV_Internet_Phone~') into fieldNameList;
       IF(OLD.TEN_TV_Internet_Phone = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_TV_Internet_Phone = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_TV_Internet_Phone = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_TV_Internet_Phone = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_mobilePhone <> NEW.TEN_mobilePhone ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_mobilePhone~') into fieldNameList;
       IF(OLD.TEN_mobilePhone = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_mobilePhone = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_mobilePhone = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_mobilePhone = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_furnitureRental <> NEW.TEN_furnitureRental ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_furnitureRental~') into fieldNameList;
       IF(OLD.TEN_furnitureRental = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_furnitureRental = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_furnitureRental = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_furnitureRental = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_cleaningServices <> NEW.TEN_cleaningServices ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_cleaningServices~') into fieldNameList;
       IF(OLD.TEN_cleaningServices = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_cleaningServices = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_cleaningServices = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_cleaningServices = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_parkingPermit <> NEW.TEN_parkingPermit ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_parkingPermit~') into fieldNameList;
       IF(OLD.TEN_parkingPermit = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_parkingPermit = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_parkingPermit = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_parkingPermit = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_communityTax <> NEW.TEN_communityTax ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_communityTax~') into fieldNameList;
       IF(OLD.TEN_communityTax = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_communityTax = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_communityTax = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_communityTax = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_insurance <> NEW.TEN_insurance ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_insurance~') into fieldNameList;
       IF(OLD.TEN_insurance = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_insurance = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_insurance = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_insurance = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_gardenMaintenance <> NEW.TEN_gardenMaintenance ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_gardenMaintenance~') into fieldNameList;
       IF(OLD.TEN_gardenMaintenance = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_gardenMaintenance = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_gardenMaintenance = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_gardenMaintenance = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   IF (OLD.TEN_directDebit <> NEW.TEN_directDebit ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_directDebit~') into fieldNameList;
       IF(OLD.TEN_directDebit = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_directDebit = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_directDebit = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_directDebit = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
IF (OLD.REP_copyResidence <> NEW.REP_copyResidence ) THEN
       select CONCAT(fieldNameList,'dspdetails.REP_copyResidence~') into fieldNameList;
       IF(OLD.REP_copyResidence = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.REP_copyResidence = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.REP_copyResidence = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.REP_copyResidence = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.REP_displyOtherVendorCode <> NEW.REP_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.REP_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.REP_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.REP_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.REP_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.REP_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.RLS_displyOtherVendorCode <> NEW.RLS_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.RLS_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.RLS_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.RLS_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.RLS_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.RLS_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.CAT_displyOtherVendorCode <> NEW.CAT_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.CAT_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.CAT_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.CAT_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.CAT_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.CAT_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.CLS_displyOtherVendorCode <> NEW.CLS_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.CLS_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.CLS_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.CLS_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.CLS_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.CLS_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.CHS_displyOtherVendorCode <> NEW.CHS_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.CHS_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.CHS_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.CHS_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.CHS_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.CHS_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.DPS_displyOtherVendorCode <> NEW.DPS_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.DPS_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.DPS_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.DPS_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.DPS_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.DPS_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.HSM_displyOtherVendorCode <> NEW.HSM_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.HSM_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.HSM_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.HSM_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.HSM_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.HSM_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.PDT_displyOtherVendorCode <> NEW.PDT_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.PDT_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.PDT_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.PDT_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.PDT_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.PDT_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.RCP_displyOtherVendorCode <> NEW.RCP_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.RCP_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.RCP_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.RCP_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.RCP_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.RCP_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.SPA_displyOtherVendorCode <> NEW.SPA_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.SPA_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.SPA_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.SPA_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.SPA_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.SPA_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.TCS_displyOtherVendorCode <> NEW.TCS_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.TCS_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.TCS_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TCS_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TCS_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TCS_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.WOP_authorizationLetter <> NEW.WOP_authorizationLetter ) THEN
       select CONCAT(fieldNameList,'dspdetails.WOP_authorizationLetter~') into fieldNameList;
       IF(OLD.WOP_authorizationLetter = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.WOP_authorizationLetter = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.WOP_authorizationLetter = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.WOP_authorizationLetter = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.WOP_copyResidence <> NEW.WOP_copyResidence ) THEN
       select CONCAT(fieldNameList,'dspdetails.WOP_copyResidence~') into fieldNameList;
       IF(OLD.WOP_copyResidence = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.WOP_copyResidence = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.WOP_copyResidence = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.WOP_copyResidence = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.WOP_displyOtherVendorCode <> NEW.WOP_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.WOP_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.WOP_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.WOP_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.WOP_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.WOP_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.MTS_displyOtherVendorCode <> NEW.MTS_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.MTS_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.MTS_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.MTS_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.MTS_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.MTS_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.DSS_displyOtherVendorCode <> NEW.DSS_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.DSS_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.DSS_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.DSS_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.DSS_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.DSS_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF (OLD.TEN_displyOtherVendorCode <> NEW.TEN_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.TEN_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
IF ((OLD.PDT_budget <> NEW.PDT_budget) or (OLD.PDT_budget is null and NEW.PDT_budget is not null)
      or (OLD.PDT_budget is not null and NEW.PDT_budget is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.PDT_budget~') into fieldNameList;
        IF(OLD.PDT_budget is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.PDT_budget is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.PDT_budget is not null) then
            select CONCAT(oldValueList,OLD.PDT_budget,'~') into oldValueList;
        END IF;
        IF(NEW.PDT_budget is not null) then
            select CONCAT(newValueList,NEW.PDT_budget,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.RCP_estimateCost <> NEW.RCP_estimateCost) or (OLD.RCP_estimateCost is null and NEW.RCP_estimateCost is not null)
      or (OLD.RCP_estimateCost is not null and NEW.RCP_estimateCost is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.RCP_estimateCost~') into fieldNameList;
        IF(OLD.RCP_estimateCost is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.RCP_estimateCost is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.RCP_estimateCost is not null) then
            select CONCAT(oldValueList,OLD.RCP_estimateCost,'~') into oldValueList;
        END IF;
        IF(NEW.RCP_estimateCost is not null) then
            select CONCAT(newValueList,NEW.RCP_estimateCost,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.TEN_contributionAmount <> NEW.TEN_contributionAmount) or (OLD.TEN_contributionAmount is null and NEW.TEN_contributionAmount is not null)
      or (OLD.TEN_contributionAmount is not null and NEW.TEN_contributionAmount is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.TEN_contributionAmount~') into fieldNameList;
        IF(OLD.TEN_contributionAmount is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_contributionAmount is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.TEN_contributionAmount is not null) then
            select CONCAT(oldValueList,OLD.RCP_estimateCost,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_contributionAmount is not null) then
            select CONCAT(newValueList,NEW.TEN_contributionAmount,'~') into newValueList;
        END IF;
    END IF;
    
IF ((OLD.RCP_actualCost <> NEW.RCP_actualCost) or (OLD.RCP_actualCost is null and NEW.RCP_actualCost is not null)
      or (OLD.RCP_actualCost is not null and NEW.RCP_actualCost is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.RCP_actualCost~') into fieldNameList;
        IF(OLD.RCP_actualCost is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.RCP_actualCost is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.RCP_actualCost is not null) then
            select CONCAT(oldValueList,OLD.RCP_actualCost,'~') into oldValueList;
        END IF;
        IF(NEW.RCP_actualCost is not null) then
            select CONCAT(newValueList,NEW.RCP_actualCost,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.MTS_mortgageAmount <> NEW.MTS_mortgageAmount) or (OLD.MTS_mortgageAmount is null and NEW.MTS_mortgageAmount is not null)
      or (OLD.MTS_mortgageAmount is not null and NEW.MTS_mortgageAmount is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.MTS_mortgageAmount~') into fieldNameList;
        IF(OLD.MTS_mortgageAmount is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.MTS_mortgageAmount is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.MTS_mortgageAmount is not null) then
            select CONCAT(oldValueList,OLD.MTS_mortgageAmount,'~') into oldValueList;
        END IF;
        IF(NEW.MTS_mortgageAmount is not null) then
            select CONCAT(newValueList,NEW.MTS_mortgageAmount,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.MTS_mortgageRate <> NEW.MTS_mortgageRate) or (OLD.MTS_mortgageRate is null and NEW.MTS_mortgageRate is not null)
      or (OLD.MTS_mortgageRate is not null and NEW.MTS_mortgageRate is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.MTS_mortgageRate~') into fieldNameList;
        IF(OLD.MTS_mortgageRate is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.MTS_mortgageRate is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.MTS_mortgageRate is not null) then
            select CONCAT(oldValueList,OLD.MTS_mortgageRate,'~') into oldValueList;
        END IF;
        IF(NEW.MTS_mortgageRate is not null) then
            select CONCAT(newValueList,NEW.MTS_mortgageRate,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.TEN_rentAmount <> NEW.TEN_rentAmount) or (OLD.TEN_rentAmount is null and NEW.TEN_rentAmount is not null)
      or (OLD.TEN_rentAmount is not null and NEW.TEN_rentAmount is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.TEN_rentAmount~') into fieldNameList;
        IF(OLD.TEN_rentAmount is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_rentAmount is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.TEN_rentAmount is not null) then
            select CONCAT(oldValueList,OLD.TEN_rentAmount,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_rentAmount is not null) then
            select CONCAT(newValueList,NEW.TEN_rentAmount,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.TEN_rentAllowance <> NEW.TEN_rentAllowance) or (OLD.TEN_rentAllowance is null and NEW.TEN_rentAllowance is not null)
      or (OLD.TEN_rentAllowance is not null and NEW.TEN_rentAllowance is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.TEN_rentAllowance~') into fieldNameList;
        IF(OLD.TEN_rentAllowance is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_rentAllowance is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.TEN_rentAllowance is not null) then
            select CONCAT(oldValueList,OLD.TEN_rentAllowance,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_rentAllowance is not null) then
            select CONCAT(newValueList,NEW.TEN_rentAllowance,'~') into newValueList;
        END IF;
    END IF;
    
        
    IF ((OLD.TEN_depositAmount <> NEW.TEN_depositAmount) or (OLD.TEN_depositAmount is null and NEW.TEN_depositAmount is not null)
      or (OLD.TEN_depositAmount is not null and NEW.TEN_depositAmount is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.TEN_depositAmount~') into fieldNameList;
        IF(OLD.TEN_depositAmount is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_depositAmount is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.TEN_depositAmount is not null) then
            select CONCAT(oldValueList,OLD.TEN_depositAmount,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_depositAmount is not null) then
            select CONCAT(newValueList,NEW.TEN_depositAmount,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.TEN_depositReturnedAmount <> NEW.TEN_depositReturnedAmount) or (OLD.TEN_depositReturnedAmount is null and NEW.TEN_depositReturnedAmount is not null)
      or (OLD.TEN_depositReturnedAmount is not null and NEW.TEN_depositReturnedAmount is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.TEN_depositReturnedAmount~') into fieldNameList;
        IF(OLD.TEN_depositReturnedAmount is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_depositReturnedAmount is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.TEN_depositReturnedAmount is not null) then
            select CONCAT(oldValueList,OLD.TEN_depositAmount,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_depositReturnedAmount is not null) then
            select CONCAT(newValueList,NEW.TEN_depositReturnedAmount,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.TEN_assigneeContributionAmount <> NEW.TEN_assigneeContributionAmount) or (OLD.TEN_assigneeContributionAmount is null and NEW.TEN_assigneeContributionAmount is not null)
      or (OLD.TEN_assigneeContributionAmount is not null and NEW.TEN_assigneeContributionAmount is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.TEN_assigneeContributionAmount~') into fieldNameList;
        IF(OLD.TEN_assigneeContributionAmount is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_assigneeContributionAmount is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.TEN_assigneeContributionAmount is not null) then
            select CONCAT(oldValueList,OLD.TEN_depositAmount,'~') into oldValueList;
        END IF;
        IF(NEW.TEN_assigneeContributionAmount is not null) then
            select CONCAT(newValueList,NEW.TEN_assigneeContributionAmount,'~') into newValueList;
        END IF;
    END IF;
    
       
    
IF ((OLD.HSM_estimatedHSRReferral <> NEW.HSM_estimatedHSRReferral) or (OLD.HSM_estimatedHSRReferral is null and NEW.HSM_estimatedHSRReferral is not null)
      or (OLD.HSM_estimatedHSRReferral is not null and NEW.HSM_estimatedHSRReferral is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.HSM_estimatedHSRReferral~') into fieldNameList;
        IF(OLD.HSM_estimatedHSRReferral is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.HSM_estimatedHSRReferral is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.HSM_estimatedHSRReferral is not null) then
            select CONCAT(oldValueList,OLD.HSM_estimatedHSRReferral,'~') into oldValueList;
        END IF;
        IF(NEW.HSM_estimatedHSRReferral is not null) then
            select CONCAT(newValueList,NEW.HSM_estimatedHSRReferral,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.HSM_actualHSRReferral <> NEW.HSM_actualHSRReferral) or (OLD.HSM_actualHSRReferral is null and NEW.HSM_actualHSRReferral is not null)
      or (OLD.HSM_actualHSRReferral is not null and NEW.HSM_actualHSRReferral is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.HSM_actualHSRReferral~') into fieldNameList;
        IF(OLD.HSM_actualHSRReferral is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.HSM_actualHSRReferral is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.HSM_actualHSRReferral is not null) then
            select CONCAT(oldValueList,OLD.HSM_actualHSRReferral,'~') into oldValueList;
        END IF;
        IF(NEW.HSM_actualHSRReferral is not null) then
            select CONCAT(newValueList,NEW.HSM_actualHSRReferral,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.HOM_estimatedHSRReferral <> NEW.HOM_estimatedHSRReferral) or (OLD.HOM_estimatedHSRReferral is null and NEW.HOM_estimatedHSRReferral is not null)
      or (OLD.HOM_estimatedHSRReferral is not null and NEW.HOM_estimatedHSRReferral is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.HOM_estimatedHSRReferral~') into fieldNameList;
        IF(OLD.HOM_estimatedHSRReferral is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.HOM_estimatedHSRReferral is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.HOM_estimatedHSRReferral is not null) then
            select CONCAT(oldValueList,OLD.HOM_estimatedHSRReferral,'~') into oldValueList;
        END IF;
        IF(NEW.HOM_estimatedHSRReferral is not null) then
            select CONCAT(newValueList,NEW.HOM_estimatedHSRReferral,'~') into newValueList;
        END IF;
    END IF;
IF ((OLD.HOM_actualHSRReferral <> NEW.HOM_actualHSRReferral) or (OLD.HOM_actualHSRReferral is null and NEW.HOM_actualHSRReferral is not null)
      or (OLD.HOM_actualHSRReferral is not null and NEW.HOM_actualHSRReferral is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.HOM_actualHSRReferral~') into fieldNameList;
        IF(OLD.HOM_actualHSRReferral is null) THEN
            select CONCAT(oldValueList,0,'~') into oldValueList;
        END IF;
        IF(NEW.HOM_actualHSRReferral is null) THEN
            select CONCAT(newValueList,0,'~') into newValueList;
        END IF;

        IF(OLD.HOM_actualHSRReferral is not null) then
            select CONCAT(oldValueList,OLD.HOM_actualHSRReferral,'~') into oldValueList;
        END IF;
        IF(NEW.HOM_actualHSRReferral is not null) then
            select CONCAT(newValueList,NEW.HOM_actualHSRReferral,'~') into newValueList;
        END IF;
    END IF;


IF ((date_format(OLD.FLB_additionalBookingReminderDate,'%Y-%m-%d') <> date_format(NEW.FLB_additionalBookingReminderDate,'%Y-%m-%d')) or (date_format(OLD.FLB_additionalBookingReminderDate,'%Y-%m-%d') is null and date_format(NEW.FLB_additionalBookingReminderDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.FLB_additionalBookingReminderDate,'%Y-%m-%d') is not null and date_format(NEW.FLB_additionalBookingReminderDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_additionalBookingReminderDate~') into fieldNameList;
    IF(OLD.FLB_additionalBookingReminderDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FLB_additionalBookingReminderDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FLB_additionalBookingReminderDate is not null) THEN
		select CONCAT(oldValueList,OLD.FLB_additionalBookingReminderDate,'~') into oldValueList;
	END IF;
	IF(NEW.FLB_additionalBookingReminderDate is not null) THEN
		select CONCAT(newValueList,NEW.FLB_additionalBookingReminderDate,'~') into newValueList;
	END IF;
   END IF;


IF ((date_format(OLD.FLB_additionalBookingReminderDate1,'%Y-%m-%d') <> date_format(NEW.FLB_additionalBookingReminderDate1,'%Y-%m-%d')) or (date_format(OLD.FLB_additionalBookingReminderDate1,'%Y-%m-%d') is null and date_format(NEW.FLB_additionalBookingReminderDate1,'%Y-%m-%d') is not null)
      or (date_format(OLD.FLB_additionalBookingReminderDate1,'%Y-%m-%d') is not null and date_format(NEW.FLB_additionalBookingReminderDate1,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_additionalBookingReminderDate1~') into fieldNameList;
    IF(OLD.FLB_additionalBookingReminderDate1 is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FLB_additionalBookingReminderDate1 is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FLB_additionalBookingReminderDate1 is not null) THEN
		select CONCAT(oldValueList,OLD.FLB_additionalBookingReminderDate1,'~') into oldValueList;
	END IF;
	IF(NEW.FLB_additionalBookingReminderDate1 is not null) THEN
		select CONCAT(newValueList,NEW.FLB_additionalBookingReminderDate1,'~') into newValueList;
	END IF;
   END IF;


IF ((OLD.WOP_leaseExNeeded <> NEW.WOP_leaseExNeeded) or (OLD.WOP_leaseExNeeded is null and NEW.WOP_leaseExNeeded is not null)
      or (OLD.WOP_leaseExNeeded is not null and NEW.WOP_leaseExNeeded is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.WOP_leaseExNeeded~') into fieldNameList;
        IF(OLD.WOP_leaseExNeeded is null) THEN
            select CONCAT(oldValueList,'~') into oldValueList;
        END IF;
        IF(NEW.WOP_leaseExNeeded is null) THEN
            select CONCAT(newValueList,'~') into newValueList;
        END IF;

        IF(OLD.WOP_leaseExNeeded is not null) then
            select CONCAT(oldValueList,OLD.WOP_leaseExNeeded,'~') into oldValueList;
        END IF;
        IF(NEW.WOP_leaseExNeeded is not null) then
            select CONCAT(newValueList,NEW.WOP_leaseExNeeded,'~') into newValueList;
        END IF;
    END IF;

IF ((OLD.REP_leaseExNeeded <> NEW.REP_leaseExNeeded) or (OLD.REP_leaseExNeeded is null and NEW.REP_leaseExNeeded is not null)
      or (OLD.REP_leaseExNeeded is not null and NEW.REP_leaseExNeeded is null)) THEN
        select CONCAT(fieldNameList,'dspdetails.REP_leaseExNeeded~') into fieldNameList;
        IF(OLD.REP_leaseExNeeded is null) THEN
            select CONCAT(oldValueList,'~') into oldValueList;
        END IF;
        IF(NEW.REP_leaseExNeeded is null) THEN
            select CONCAT(newValueList,'~') into newValueList;
        END IF;

        IF(OLD.REP_leaseExNeeded is not null) then
            select CONCAT(oldValueList,OLD.REP_leaseExNeeded,'~') into oldValueList;
        END IF;
        IF(NEW.REP_leaseExNeeded is not null) then
            select CONCAT(newValueList,NEW.REP_leaseExNeeded,'~') into newValueList;
        END IF;
    END IF;
 
    IF (OLD.MMG_originAgentCode <> NEW.MMG_originAgentCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_originAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_originAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_originAgentCode,'~') into newValueList;
END IF;

IF (OLD. MMG_originAgentName <> NEW. MMG_originAgentName ) THEN
      select CONCAT(fieldNameList,'MMG_originAgentName~') into fieldNameList;
      select CONCAT(oldValueList,OLD. MMG_originAgentName,'~') into oldValueList;
      select CONCAT(newValueList,NEW. MMG_originAgentName,'~') into newValueList;
END IF;

IF (OLD.MMG_originAgentPhone <> NEW.MMG_originAgentPhone ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_originAgentPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_originAgentPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_originAgentPhone,'~') into newValueList;
END IF;

IF (OLD.MMG_originAgentEmail <> NEW.MMG_originAgentEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_originAgentEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_originAgentEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_originAgentEmail,'~') into newValueList;
END IF;

IF (OLD.MMG_destinationAgentCode <> NEW.MMG_destinationAgentCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_destinationAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_destinationAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_destinationAgentCode,'~') into newValueList;
END IF;

IF (OLD.MMG_destinationAgentName <> NEW.MMG_destinationAgentName ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_destinationAgentName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_destinationAgentName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_destinationAgentName,'~') into newValueList;
END IF;

IF (OLD.MMG_destinationAgentPhone <> NEW.MMG_destinationAgentPhone ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_destinationAgentPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_destinationAgentPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_destinationAgentPhone,'~') into newValueList;
END IF;

IF (OLD.MMG_destinationAgentEmail <> NEW.MMG_destinationAgentEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_destinationAgentEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.MMG_destinationAgentEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.MMG_destinationAgentEmail,'~') into newValueList;
END IF;

IF (OLD.resendEmailServiceName <> NEW.resendEmailServiceName ) THEN
      select CONCAT(fieldNameList,'dspdetails.resendEmailServiceName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.resendEmailServiceName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.resendEmailServiceName,'~') into newValueList;
END IF;

IF (OLD.HOB_hotelName <> NEW.HOB_hotelName ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_hotelName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOB_hotelName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOB_hotelName,'~') into newValueList;
END IF;

IF (OLD.HOB_city <> NEW.HOB_city ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOB_city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOB_city,'~') into newValueList;
END IF;

IF (OLD.HOB_vendorCode <> NEW.HOB_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOB_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOB_vendorCode,'~') into newValueList;
END IF;

IF (OLD.HOB_vendorName <> NEW.HOB_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOB_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOB_vendorName,'~') into newValueList;
END IF;

IF (OLD.HOB_vendorContact <> NEW.HOB_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOB_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOB_vendorContact,'~') into newValueList;
END IF;

IF (OLD.HOB_vendorEmail <> NEW.HOB_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOB_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOB_vendorEmail,'~') into newValueList;
END IF;

IF (OLD.HOB_vendorCodeEXSO <> NEW.HOB_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.HOB_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.HOB_vendorCodeEXSO,'~') into newValueList;
END IF;

IF (OLD.FLB_vendorCode <> NEW.FLB_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FLB_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FLB_vendorCode,'~') into newValueList;
END IF;

IF (OLD.FLB_vendorName <> NEW.FLB_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FLB_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FLB_vendorName,'~') into newValueList;
END IF;

IF (OLD.FLB_vendorContact <> NEW.FLB_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FLB_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FLB_vendorContact,'~') into newValueList;
END IF;

IF (OLD.FLB_vendorEmail <> NEW.FLB_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FLB_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FLB_vendorEmail,'~') into newValueList;
END IF;

IF (OLD.FLB_vendorCodeEXSO <> NEW.FLB_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FLB_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FLB_vendorCodeEXSO,'~') into newValueList;
END IF;

IF (OLD.VIS_visaExtensionNeeded <> NEW.VIS_visaExtensionNeeded ) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_visaExtensionNeeded~') into fieldNameList;
      select CONCAT(oldValueList,OLD.VIS_visaExtensionNeeded,'~') into oldValueList;
      select CONCAT(newValueList,NEW.VIS_visaExtensionNeeded,'~') into newValueList;
END IF;

IF (OLD.RNT_leaseExtensionNeeded <> NEW.RNT_leaseExtensionNeeded ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_leaseExtensionNeeded~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_leaseExtensionNeeded,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_leaseExtensionNeeded,'~') into newValueList;
END IF;

IF (OLD.RNT_homeSearchType <> NEW.RNT_homeSearchType ) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_homeSearchType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.RNT_homeSearchType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.RNT_homeSearchType,'~') into newValueList;
END IF;

IF (OLD.FRL_vendorCode <> NEW.FRL_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_vendorCode,'~') into newValueList;
END IF;

IF (OLD.FRL_vendorName <> NEW.FRL_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_vendorName,'~') into newValueList;
END IF;

IF (OLD.FRL_vendorCodeEXSO <> NEW.FRL_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_vendorCodeEXSO,'~') into newValueList;
END IF;

IF (OLD.FRL_vendorContact <> NEW.FRL_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_vendorContact,'~') into newValueList;
END IF;

IF (OLD.FRL_vendorEmail <> NEW.FRL_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_vendorEmail,'~') into newValueList;
END IF;

IF (OLD.FRL_paymentResponsibility <> NEW.FRL_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_paymentResponsibility,'~') into newValueList;
END IF;

IF (OLD.FRL_rentalRate <> NEW.FRL_rentalRate ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_rentalRate~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_rentalRate,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_rentalRate,'~') into newValueList;
END IF;

IF (OLD.FRL_deposit <> NEW.FRL_deposit ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_deposit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_deposit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_deposit,'~') into newValueList;
END IF;

IF (OLD.FRL_rentalCurrency <> NEW.FRL_rentalCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_rentalCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_rentalCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_rentalCurrency,'~') into newValueList;
END IF;

IF (OLD.FRL_depositCurrency <> NEW.FRL_depositCurrency ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_depositCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_depositCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_depositCurrency,'~') into newValueList;
END IF;

IF (OLD.FRL_month <> NEW.FRL_month ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_month~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_month,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_month,'~') into newValueList;
END IF;

IF (OLD.FRL_terminationNotice <> NEW.FRL_terminationNotice ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_terminationNotice~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_terminationNotice,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_terminationNotice,'~') into newValueList;
END IF;

IF (OLD.APU_vendorCode <> NEW.APU_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_vendorCode,'~') into newValueList;
END IF;

IF (OLD.APU_vendorName <> NEW.APU_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_vendorName,'~') into newValueList;
END IF;

IF (OLD.APU_vendorCodeEXSO <> NEW.APU_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_vendorCodeEXSO,'~') into newValueList;
END IF;

IF (OLD.APU_vendorContact <> NEW.APU_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_vendorContact,'~') into newValueList;
END IF;

IF (OLD.APU_vendorEmail <> NEW.APU_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_vendorEmail,'~') into newValueList;
END IF;

IF (OLD.APU_paymentResponsibility <> NEW.APU_paymentResponsibility ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_paymentResponsibility~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_paymentResponsibility,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_paymentResponsibility,'~') into newValueList;
END IF;

IF (OLD.APU_flightDetails <> NEW.APU_flightDetails ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_flightDetails~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_flightDetails,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_flightDetails,'~') into newValueList;
END IF;

IF (OLD.APU_driverName <> NEW.APU_driverName ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_driverName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_driverName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_driverName,'~') into newValueList;
END IF;

IF (OLD.APU_driverPhoneNumber <> NEW.APU_driverPhoneNumber ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_driverPhoneNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_driverPhoneNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_driverPhoneNumber,'~') into newValueList;
END IF;

IF (OLD.APU_carMake <> NEW.APU_carMake ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_carMake~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_carMake,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_carMake,'~') into newValueList;
END IF;

IF (OLD.APU_carModel <> NEW.APU_carModel ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_carModel~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_carModel,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_carModel,'~') into newValueList;
END IF;

IF (OLD.APU_carColor <> NEW.APU_carColor ) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_carColor~') into fieldNameList;
      select CONCAT(oldValueList,OLD.APU_carColor,'~') into oldValueList;
      select CONCAT(newValueList,NEW.APU_carColor,'~') into newValueList;
END IF;

IF (OLD.INS_vendorCode <> NEW.INS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.INS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INS_vendorCode,'~') into newValueList;
END IF;

IF (OLD.INS_vendorName <> NEW.INS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.INS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INS_vendorName,'~') into newValueList;
END IF;

IF (OLD.INS_vendorCodeEXSO <> NEW.INS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.INS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INS_vendorCodeEXSO,'~') into newValueList;
END IF;

IF (OLD.INS_vendorEmail <> NEW.INS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.INS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INS_vendorEmail,'~') into newValueList;
END IF;

IF (OLD.INS_vendorContact <> NEW.INS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.INS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INS_vendorContact,'~') into newValueList;
END IF;

IF (OLD.INP_vendorCode <> NEW.INP_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.INP_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INP_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INP_vendorCode,'~') into newValueList;
END IF;

IF (OLD.INP_vendorName <> NEW.INP_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.INP_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INP_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INP_vendorName,'~') into newValueList;
END IF;

IF (OLD.INP_vendorCodeEXSO <> NEW.INP_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.INP_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INP_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INP_vendorCodeEXSO,'~') into newValueList;
END IF;

IF (OLD.INP_vendorContact <> NEW.INP_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.INP_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INP_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INP_vendorContact,'~') into newValueList;
END IF;

IF (OLD.INP_vendorEmail <> NEW.INP_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.INP_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INP_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INP_vendorEmail,'~') into newValueList;
END IF;

IF (OLD.EDA_vendorCode <> NEW.EDA_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.EDA_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EDA_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EDA_vendorCode,'~') into newValueList;
END IF;

IF (OLD.EDA_vendorName <> NEW.EDA_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.EDA_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EDA_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EDA_vendorName,'~') into newValueList;
END IF;

IF (OLD.EDA_vendorCodeEXSO <> NEW.EDA_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.EDA_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EDA_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EDA_vendorCodeEXSO,'~') into newValueList;
END IF;

IF (OLD.EDA_vendorContact <> NEW.EDA_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.EDA_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EDA_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EDA_vendorContact,'~') into newValueList;
END IF;

IF (OLD.EDA_vendorEmail <> NEW.EDA_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.EDA_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EDA_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EDA_vendorEmail,'~') into newValueList;
END IF;

IF (OLD.TAS_vendorCode <> NEW.TAS_vendorCode ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAS_vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAS_vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAS_vendorCode,'~') into newValueList;
END IF;

IF (OLD.TAS_vendorName <> NEW.TAS_vendorName ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAS_vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAS_vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAS_vendorName,'~') into newValueList;
END IF;

IF (OLD.TAS_vendorCodeEXSO <> NEW.TAS_vendorCodeEXSO ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAS_vendorCodeEXSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAS_vendorCodeEXSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAS_vendorCodeEXSO,'~') into newValueList;
END IF;

IF (OLD.TAS_vendorContact <> NEW.TAS_vendorContact ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAS_vendorContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAS_vendorContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAS_vendorContact,'~') into newValueList;
END IF;

IF (OLD.TAS_vendorEmail <> NEW.TAS_vendorEmail ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAS_vendorEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAS_vendorEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAS_vendorEmail,'~') into newValueList;
END IF;

IF ((date_format(OLD.PRV_emailSent,'%Y-%m-%d') <> date_format(NEW.PRV_emailSent,'%Y-%m-%d')) or (date_format(OLD.PRV_emailSent,'%Y-%m-%d') is null and date_format(NEW.PRV_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.PRV_emailSent,'%Y-%m-%d') is not null and date_format(NEW.PRV_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PRV_emailSent~') into fieldNameList;
    IF(OLD.PRV_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PRV_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PRV_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.PRV_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.PRV_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.PRV_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.TAX_emailSent,'%Y-%m-%d') <> date_format(NEW.TAX_emailSent,'%Y-%m-%d')) or (date_format(OLD.TAX_emailSent,'%Y-%m-%d') is null and date_format(NEW.TAX_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAX_emailSent,'%Y-%m-%d') is not null and date_format(NEW.TAX_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAX_emailSent~') into fieldNameList;
    IF(OLD.TAX_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAX_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAX_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.TAX_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.TAX_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.TAX_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.AIO_emailSent,'%Y-%m-%d') <> date_format(NEW.AIO_emailSent,'%Y-%m-%d')) or (date_format(OLD.AIO_emailSent,'%Y-%m-%d') is null and date_format(NEW.AIO_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.AIO_emailSent,'%Y-%m-%d') is not null and date_format(NEW.AIO_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.AIO_emailSent~') into fieldNameList;
    IF(OLD.AIO_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.AIO_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.AIO_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.AIO_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.AIO_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.AIO_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.TRG_emailSent,'%Y-%m-%d') <> date_format(NEW.TRG_emailSent,'%Y-%m-%d')) or (date_format(OLD.TRG_emailSent,'%Y-%m-%d') is null and date_format(NEW.TRG_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.TRG_emailSent,'%Y-%m-%d') is not null and date_format(NEW.TRG_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TRG_emailSent~') into fieldNameList;
    IF(OLD.TRG_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TRG_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TRG_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.TRG_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.TRG_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.TRG_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.LAN_emailSent,'%Y-%m-%d') <> date_format(NEW.LAN_emailSent,'%Y-%m-%d')) or (date_format(OLD.LAN_emailSent,'%Y-%m-%d') is null and date_format(NEW.LAN_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.LAN_emailSent,'%Y-%m-%d') is not null and date_format(NEW.LAN_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.LAN_emailSent~') into fieldNameList;
    IF(OLD.LAN_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.LAN_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.LAN_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.LAN_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.LAN_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.LAN_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.RPT_emailSent,'%Y-%m-%d') <> date_format(NEW.RPT_emailSent,'%Y-%m-%d')) or (date_format(OLD.RPT_emailSent,'%Y-%m-%d') is null and date_format(NEW.RPT_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.RPT_emailSent,'%Y-%m-%d') is not null and date_format(NEW.RPT_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RPT_emailSent~') into fieldNameList;
    IF(OLD.RPT_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RPT_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RPT_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.RPT_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.RPT_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.RPT_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.COL_emailSent,'%Y-%m-%d') <> date_format(NEW.COL_emailSent,'%Y-%m-%d')) or (date_format(OLD.COL_emailSent,'%Y-%m-%d') is null and date_format(NEW.COL_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.COL_emailSent,'%Y-%m-%d') is not null and date_format(NEW.COL_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.COL_emailSent~') into fieldNameList;
    IF(OLD.COL_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.COL_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.COL_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.COL_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.COL_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.COL_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.TAC_emailSent,'%Y-%m-%d') <> date_format(NEW.TAC_emailSent,'%Y-%m-%d')) or (date_format(OLD.TAC_emailSent,'%Y-%m-%d') is null and date_format(NEW.TAC_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAC_emailSent,'%Y-%m-%d') is not null and date_format(NEW.TAC_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAC_emailSent~') into fieldNameList;
    IF(OLD.TAC_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAC_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAC_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.TAC_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.TAC_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.TAC_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.HOM_emailSent,'%Y-%m-%d') <> date_format(NEW.HOM_emailSent,'%Y-%m-%d')) or (date_format(OLD.HOM_emailSent,'%Y-%m-%d') is null and date_format(NEW.HOM_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOM_emailSent,'%Y-%m-%d') is not null and date_format(NEW.HOM_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOM_emailSent~') into fieldNameList;
    IF(OLD.HOM_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOM_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOM_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.HOM_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.HOM_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.HOM_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.SCH_emailSent,'%Y-%m-%d') <> date_format(NEW.SCH_emailSent,'%Y-%m-%d')) or (date_format(OLD.SCH_emailSent,'%Y-%m-%d') is null and date_format(NEW.SCH_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.SCH_emailSent,'%Y-%m-%d') is not null and date_format(NEW.SCH_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SCH_emailSent~') into fieldNameList;
    IF(OLD.SCH_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SCH_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SCH_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.SCH_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.SCH_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.SCH_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.EXP_emailSent,'%Y-%m-%d') <> date_format(NEW.EXP_emailSent,'%Y-%m-%d')) or (date_format(OLD.EXP_emailSent,'%Y-%m-%d') is null and date_format(NEW.EXP_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.EXP_emailSent,'%Y-%m-%d') is not null and date_format(NEW.EXP_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.EXP_emailSent~') into fieldNameList;
    IF(OLD.EXP_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.EXP_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.EXP_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.EXP_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.EXP_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.EXP_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.ONG_emailSent,'%Y-%m-%d') <> date_format(NEW.ONG_emailSent,'%Y-%m-%d')) or (date_format(OLD.ONG_emailSent,'%Y-%m-%d') is null and date_format(NEW.ONG_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.ONG_emailSent,'%Y-%m-%d') is not null and date_format(NEW.ONG_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.ONG_emailSent~') into fieldNameList;
    IF(OLD.ONG_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.ONG_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.ONG_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.ONG_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.ONG_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.ONG_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.TEN_emailSent,'%Y-%m-%d') <> date_format(NEW.TEN_emailSent,'%Y-%m-%d')) or (date_format(OLD.TEN_emailSent,'%Y-%m-%d') is null and date_format(NEW.TEN_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.TEN_emailSent,'%Y-%m-%d') is not null and date_format(NEW.TEN_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TEN_emailSent~') into fieldNameList;
    IF(OLD.TEN_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TEN_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TEN_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.TEN_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.TEN_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.TEN_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.MMG_orginEmailSent,'%Y-%m-%d') <> date_format(NEW.MMG_orginEmailSent,'%Y-%m-%d')) or (date_format(OLD.MMG_orginEmailSent,'%Y-%m-%d') is null and date_format(NEW.MMG_orginEmailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.MMG_orginEmailSent,'%Y-%m-%d') is not null and date_format(NEW.MMG_orginEmailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_orginEmailSent~') into fieldNameList;
    IF(OLD.MMG_orginEmailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.MMG_orginEmailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.MMG_orginEmailSent is not null) THEN
		select CONCAT(oldValueList,OLD.MMG_orginEmailSent,'~') into oldValueList;
	END IF;
	IF(NEW.MMG_orginEmailSent is not null) THEN
		select CONCAT(newValueList,NEW.MMG_orginEmailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.CAR_emailSent,'%Y-%m-%d') <> date_format(NEW.CAR_emailSent,'%Y-%m-%d')) or (date_format(OLD.CAR_emailSent,'%Y-%m-%d') is null and date_format(NEW.CAR_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAR_emailSent,'%Y-%m-%d') is not null and date_format(NEW.CAR_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAR_emailSent~') into fieldNameList;
    IF(OLD.CAR_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAR_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAR_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.CAR_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.CAR_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.CAR_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.SET_emailSent,'%Y-%m-%d') <> date_format(NEW.SET_emailSent,'%Y-%m-%d')) or (date_format(OLD.SET_emailSent,'%Y-%m-%d') is null and date_format(NEW.SET_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.SET_emailSent,'%Y-%m-%d') is not null and date_format(NEW.SET_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SET_emailSent~') into fieldNameList;
    IF(OLD.SET_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SET_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SET_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.SET_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.SET_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.SET_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.CAT_emailSent,'%Y-%m-%d') <> date_format(NEW.CAT_emailSent,'%Y-%m-%d')) or (date_format(OLD.CAT_emailSent,'%Y-%m-%d') is null and date_format(NEW.CAT_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.CAT_emailSent,'%Y-%m-%d') is not null and date_format(NEW.CAT_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CAT_emailSent~') into fieldNameList;
    IF(OLD.CAT_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CAT_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CAT_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.CAT_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.CAT_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.CAT_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.CLS_emailSent,'%Y-%m-%d') <> date_format(NEW.CLS_emailSent,'%Y-%m-%d')) or (date_format(OLD.CLS_emailSent,'%Y-%m-%d') is null and date_format(NEW.CLS_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.CLS_emailSent,'%Y-%m-%d') is not null and date_format(NEW.CLS_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CLS_emailSent~') into fieldNameList;
    IF(OLD.CLS_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CLS_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CLS_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.CLS_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.CLS_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.CLS_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.CHS_emailSent,'%Y-%m-%d') <> date_format(NEW.CHS_emailSent,'%Y-%m-%d')) or (date_format(OLD.CHS_emailSent,'%Y-%m-%d') is null and date_format(NEW.CHS_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.CHS_emailSent,'%Y-%m-%d') is not null and date_format(NEW.CHS_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.CHS_emailSent~') into fieldNameList;
    IF(OLD.CHS_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.CHS_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.CHS_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.CHS_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.CHS_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.CHS_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.DPS_emailSent,'%Y-%m-%d') <> date_format(NEW.DPS_emailSent,'%Y-%m-%d')) or (date_format(OLD.DPS_emailSent,'%Y-%m-%d') is null and date_format(NEW.DPS_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.DPS_emailSent,'%Y-%m-%d') is not null and date_format(NEW.DPS_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.DPS_emailSent~') into fieldNameList;
    IF(OLD.DPS_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.DPS_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.DPS_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.DPS_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.DPS_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.DPS_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.HSM_emailSent,'%Y-%m-%d') <> date_format(NEW.HSM_emailSent,'%Y-%m-%d')) or (date_format(OLD.HSM_emailSent,'%Y-%m-%d') is null and date_format(NEW.HSM_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.HSM_emailSent,'%Y-%m-%d') is not null and date_format(NEW.HSM_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HSM_emailSent~') into fieldNameList;
    IF(OLD.HSM_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HSM_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HSM_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.HSM_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.HSM_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.HSM_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.PDT_emailSent,'%Y-%m-%d') <> date_format(NEW.PDT_emailSent,'%Y-%m-%d')) or (date_format(OLD.PDT_emailSent,'%Y-%m-%d') is null and date_format(NEW.PDT_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.PDT_emailSent,'%Y-%m-%d') is not null and date_format(NEW.PDT_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.PDT_emailSent~') into fieldNameList;
    IF(OLD.PDT_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.PDT_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.PDT_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.PDT_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.PDT_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.PDT_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.RCP_emailSent,'%Y-%m-%d') <> date_format(NEW.RCP_emailSent,'%Y-%m-%d')) or (date_format(OLD.RCP_emailSent,'%Y-%m-%d') is null and date_format(NEW.RCP_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.RCP_emailSent,'%Y-%m-%d') is not null and date_format(NEW.RCP_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RCP_emailSent~') into fieldNameList;
    IF(OLD.RCP_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RCP_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RCP_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.RCP_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.RCP_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.RCP_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.SPA_emailSent,'%Y-%m-%d') <> date_format(NEW.SPA_emailSent,'%Y-%m-%d')) or (date_format(OLD.SPA_emailSent,'%Y-%m-%d') is null and date_format(NEW.SPA_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.SPA_emailSent,'%Y-%m-%d') is not null and date_format(NEW.SPA_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_emailSent~') into fieldNameList;
    IF(OLD.SPA_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SPA_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SPA_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.SPA_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.SPA_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.SPA_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.TCS_emailSent,'%Y-%m-%d') <> date_format(NEW.TCS_emailSent,'%Y-%m-%d')) or (date_format(OLD.TCS_emailSent,'%Y-%m-%d') is null and date_format(NEW.TCS_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.TCS_emailSent,'%Y-%m-%d') is not null and date_format(NEW.TCS_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TCS_emailSent~') into fieldNameList;
    IF(OLD.TCS_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TCS_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TCS_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.TCS_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.TCS_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.TCS_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.MTS_emailSent,'%Y-%m-%d') <> date_format(NEW.MTS_emailSent,'%Y-%m-%d')) or (date_format(OLD.MTS_emailSent,'%Y-%m-%d') is null and date_format(NEW.MTS_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.MTS_emailSent,'%Y-%m-%d') is not null and date_format(NEW.MTS_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.MTS_emailSent~') into fieldNameList;
    IF(OLD.MTS_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.MTS_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.MTS_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.MTS_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.MTS_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.MTS_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.DSS_emailSent,'%Y-%m-%d') <> date_format(NEW.DSS_emailSent,'%Y-%m-%d')) or (date_format(OLD.DSS_emailSent,'%Y-%m-%d') is null and date_format(NEW.DSS_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.DSS_emailSent,'%Y-%m-%d') is not null and date_format(NEW.DSS_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.DSS_emailSent~') into fieldNameList;
    IF(OLD.DSS_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.DSS_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.DSS_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.DSS_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.DSS_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.DSS_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.VIS_emailSent,'%Y-%m-%d') <> date_format(NEW.VIS_emailSent,'%Y-%m-%d')) or (date_format(OLD.VIS_emailSent,'%Y-%m-%d') is null and date_format(NEW.VIS_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.VIS_emailSent,'%Y-%m-%d') is not null and date_format(NEW.VIS_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.VIS_emailSent~') into fieldNameList;
    IF(OLD.VIS_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.VIS_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.VIS_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.VIS_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.VIS_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.VIS_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.RNT_emailSent,'%Y-%m-%d') <> date_format(NEW.RNT_emailSent,'%Y-%m-%d')) or (date_format(OLD.RNT_emailSent,'%Y-%m-%d') is null and date_format(NEW.RNT_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.RNT_emailSent,'%Y-%m-%d') is not null and date_format(NEW.RNT_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_emailSent~') into fieldNameList;
    IF(OLD.RNT_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RNT_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RNT_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.RNT_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.RNT_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.RNT_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.NET_emailSent,'%Y-%m-%d') <> date_format(NEW.NET_emailSent,'%Y-%m-%d')) or (date_format(OLD.NET_emailSent,'%Y-%m-%d') is null and date_format(NEW.NET_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.NET_emailSent,'%Y-%m-%d') is not null and date_format(NEW.NET_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.NET_emailSent~') into fieldNameList;
    IF(OLD.NET_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.NET_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.NET_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.NET_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.NET_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.NET_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.WOP_emailSent,'%Y-%m-%d') <> date_format(NEW.WOP_emailSent,'%Y-%m-%d')) or (date_format(OLD.WOP_emailSent,'%Y-%m-%d') is null and date_format(NEW.WOP_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.WOP_emailSent,'%Y-%m-%d') is not null and date_format(NEW.WOP_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.WOP_emailSent~') into fieldNameList;
    IF(OLD.WOP_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.WOP_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.WOP_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.WOP_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.WOP_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.WOP_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.REP_emailSent,'%Y-%m-%d') <> date_format(NEW.REP_emailSent,'%Y-%m-%d')) or (date_format(OLD.REP_emailSent,'%Y-%m-%d') is null and date_format(NEW.REP_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.REP_emailSent,'%Y-%m-%d') is not null and date_format(NEW.REP_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.REP_emailSent~') into fieldNameList;
    IF(OLD.REP_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.REP_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.REP_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.REP_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.REP_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.REP_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.MMG_destinationEmailSent,'%Y-%m-%d') <> date_format(NEW.MMG_destinationEmailSent,'%Y-%m-%d')) or (date_format(OLD.MMG_destinationEmailSent,'%Y-%m-%d') is null and date_format(NEW.MMG_destinationEmailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.MMG_destinationEmailSent,'%Y-%m-%d') is not null and date_format(NEW.MMG_destinationEmailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_destinationEmailSent~') into fieldNameList;
    IF(OLD.MMG_destinationEmailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.MMG_destinationEmailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.MMG_destinationEmailSent is not null) THEN
		select CONCAT(oldValueList,OLD.MMG_destinationEmailSent,'~') into oldValueList;
	END IF;
	IF(NEW.MMG_destinationEmailSent is not null) THEN
		select CONCAT(newValueList,NEW.MMG_destinationEmailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.HOB_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.HOB_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.HOB_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.HOB_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOB_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.HOB_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_serviceStartDate~') into fieldNameList;
    IF(OLD.HOB_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOB_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOB_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOB_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOB_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.HOB_serviceStartDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.HOB_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.HOB_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.HOB_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.HOB_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOB_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.HOB_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_serviceEndDate~') into fieldNameList;
    IF(OLD.HOB_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOB_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOB_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOB_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOB_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.HOB_serviceEndDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FLB_arrivalDate,'%Y-%m-%d') <> date_format(NEW.FLB_arrivalDate,'%Y-%m-%d')) or (date_format(OLD.FLB_arrivalDate,'%Y-%m-%d') is null and date_format(NEW.FLB_arrivalDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.FLB_arrivalDate,'%Y-%m-%d') is not null and date_format(NEW.FLB_arrivalDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_arrivalDate~') into fieldNameList;
    IF(OLD.FLB_arrivalDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FLB_arrivalDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FLB_arrivalDate is not null) THEN
		select CONCAT(oldValueList,OLD.FLB_arrivalDate,'~') into oldValueList;
	END IF;
	IF(NEW.FLB_arrivalDate is not null) THEN
		select CONCAT(newValueList,NEW.FLB_arrivalDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FLB_departureDate,'%Y-%m-%d') <> date_format(NEW.FLB_departureDate,'%Y-%m-%d')) or (date_format(OLD.FLB_departureDate,'%Y-%m-%d') is null and date_format(NEW.FLB_departureDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.FLB_departureDate,'%Y-%m-%d') is not null and date_format(NEW.FLB_departureDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_departureDate~') into fieldNameList;
    IF(OLD.FLB_departureDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FLB_departureDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FLB_departureDate is not null) THEN
		select CONCAT(oldValueList,OLD.FLB_departureDate,'~') into oldValueList;
	END IF;
	IF(NEW.FLB_departureDate is not null) THEN
		select CONCAT(newValueList,NEW.FLB_departureDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FLB_arrivalDate1,'%Y-%m-%d') <> date_format(NEW.FLB_arrivalDate1,'%Y-%m-%d')) or (date_format(OLD.FLB_arrivalDate1,'%Y-%m-%d') is null and date_format(NEW.FLB_arrivalDate1,'%Y-%m-%d') is not null)
      or (date_format(OLD.FLB_arrivalDate1,'%Y-%m-%d') is not null and date_format(NEW.FLB_arrivalDate1,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_arrivalDate1~') into fieldNameList;
    IF(OLD.FLB_arrivalDate1 is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FLB_arrivalDate1 is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FLB_arrivalDate1 is not null) THEN
		select CONCAT(oldValueList,OLD.FLB_arrivalDate1,'~') into oldValueList;
	END IF;
	IF(NEW.FLB_arrivalDate1 is not null) THEN
		select CONCAT(newValueList,NEW.FLB_arrivalDate1,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FLB_departureDate1,'%Y-%m-%d') <> date_format(NEW.FLB_departureDate1,'%Y-%m-%d')) or (date_format(OLD.FLB_departureDate1,'%Y-%m-%d') is null and date_format(NEW.FLB_departureDate1,'%Y-%m-%d') is not null)
      or (date_format(OLD.FLB_departureDate1,'%Y-%m-%d') is not null and date_format(NEW.FLB_departureDate1,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_departureDate1~') into fieldNameList;
    IF(OLD.FLB_departureDate1 is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FLB_departureDate1 is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FLB_departureDate1 is not null) THEN
		select CONCAT(oldValueList,OLD.FLB_departureDate1,'~') into oldValueList;
	END IF;
	IF(NEW.FLB_departureDate1 is not null) THEN
		select CONCAT(newValueList,NEW.FLB_departureDate1,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.HOB_startDate,'%Y-%m-%d') <> date_format(NEW.HOB_startDate,'%Y-%m-%d')) or (date_format(OLD.HOB_startDate,'%Y-%m-%d') is null and date_format(NEW.HOB_startDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOB_startDate,'%Y-%m-%d') is not null and date_format(NEW.HOB_startDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_startDate~') into fieldNameList;
    IF(OLD.HOB_startDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOB_startDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOB_startDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOB_startDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOB_startDate is not null) THEN
		select CONCAT(newValueList,NEW.HOB_startDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.HOB_endDate,'%Y-%m-%d') <> date_format(NEW.HOB_endDate,'%Y-%m-%d')) or (date_format(OLD.HOB_endDate,'%Y-%m-%d') is null and date_format(NEW.HOB_endDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOB_endDate,'%Y-%m-%d') is not null and date_format(NEW.HOB_endDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_endDate~') into fieldNameList;
    IF(OLD.HOB_endDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOB_endDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOB_endDate is not null) THEN
		select CONCAT(oldValueList,OLD.HOB_endDate,'~') into oldValueList;
	END IF;
	IF(NEW.HOB_endDate is not null) THEN
		select CONCAT(newValueList,NEW.HOB_endDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FLB_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.FLB_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.FLB_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.FLB_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.FLB_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.FLB_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_serviceStartDate~') into fieldNameList;
    IF(OLD.FLB_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FLB_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FLB_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.FLB_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.FLB_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.FLB_serviceStartDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FLB_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.FLB_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.FLB_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.FLB_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.FLB_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.FLB_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_serviceEndDate~') into fieldNameList;
    IF(OLD.FLB_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FLB_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FLB_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.FLB_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.FLB_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.FLB_serviceEndDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FLB_emailSent,'%Y-%m-%d') <> date_format(NEW.FLB_emailSent,'%Y-%m-%d')) or (date_format(OLD.FLB_emailSent,'%Y-%m-%d') is null and date_format(NEW.FLB_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.FLB_emailSent,'%Y-%m-%d') is not null and date_format(NEW.FLB_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FLB_emailSent~') into fieldNameList;
    IF(OLD.FLB_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FLB_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FLB_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.FLB_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.FLB_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.FLB_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.HOB_emailSent,'%Y-%m-%d') <> date_format(NEW.HOB_emailSent,'%Y-%m-%d')) or (date_format(OLD.HOB_emailSent,'%Y-%m-%d') is null and date_format(NEW.HOB_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.HOB_emailSent,'%Y-%m-%d') is not null and date_format(NEW.HOB_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.HOB_emailSent~') into fieldNameList;
    IF(OLD.HOB_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.HOB_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.HOB_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.HOB_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.HOB_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.HOB_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FRL_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.FRL_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.FRL_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.FRL_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.FRL_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.FRL_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_serviceStartDate~') into fieldNameList;
    IF(OLD.FRL_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FRL_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FRL_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.FRL_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.FRL_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.FRL_serviceStartDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FRL_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.FRL_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.FRL_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.FRL_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.FRL_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.FRL_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_serviceEndDate~') into fieldNameList;
    IF(OLD.FRL_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FRL_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FRL_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.FRL_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.FRL_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.FRL_serviceEndDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FRL_emailSent,'%Y-%m-%d') <> date_format(NEW.FRL_emailSent,'%Y-%m-%d')) or (date_format(OLD.FRL_emailSent,'%Y-%m-%d') is null and date_format(NEW.FRL_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.FRL_emailSent,'%Y-%m-%d') is not null and date_format(NEW.FRL_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_emailSent~') into fieldNameList;
    IF(OLD.FRL_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FRL_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FRL_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.FRL_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.FRL_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.FRL_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FRL_rentalStartDate,'%Y-%m-%d') <> date_format(NEW.FRL_rentalStartDate,'%Y-%m-%d')) or (date_format(OLD.FRL_rentalStartDate,'%Y-%m-%d') is null and date_format(NEW.FRL_rentalStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.FRL_rentalStartDate,'%Y-%m-%d') is not null and date_format(NEW.FRL_rentalStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_rentalStartDate~') into fieldNameList;
    IF(OLD.FRL_rentalStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FRL_rentalStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FRL_rentalStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.FRL_rentalStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.FRL_rentalStartDate is not null) THEN
		select CONCAT(newValueList,NEW.FRL_rentalStartDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.FRL_rentalEndDate,'%Y-%m-%d') <> date_format(NEW.FRL_rentalEndDate,'%Y-%m-%d')) or (date_format(OLD.FRL_rentalEndDate,'%Y-%m-%d') is null and date_format(NEW.FRL_rentalEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.FRL_rentalEndDate,'%Y-%m-%d') is not null and date_format(NEW.FRL_rentalEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_rentalEndDate~') into fieldNameList;
    IF(OLD.FRL_rentalEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.FRL_rentalEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.FRL_rentalEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.FRL_rentalEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.FRL_rentalEndDate is not null) THEN
		select CONCAT(newValueList,NEW.FRL_rentalEndDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.APU_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.APU_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.APU_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.APU_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.APU_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.APU_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_serviceStartDate~') into fieldNameList;
    IF(OLD.APU_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.APU_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.APU_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.APU_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.APU_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.APU_serviceStartDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.APU_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.APU_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.APU_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.APU_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.APU_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.APU_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_serviceEndDate~') into fieldNameList;
    IF(OLD.APU_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.APU_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.APU_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.APU_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.APU_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.APU_serviceEndDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.APU_emailSent,'%Y-%m-%d') <> date_format(NEW.APU_emailSent,'%Y-%m-%d')) or (date_format(OLD.APU_emailSent,'%Y-%m-%d') is null and date_format(NEW.APU_emailSent,'%Y-%m-%d') is not null)
      or (date_format(OLD.APU_emailSent,'%Y-%m-%d') is not null and date_format(NEW.APU_emailSent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_emailSent~') into fieldNameList;
    IF(OLD.APU_emailSent is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.APU_emailSent is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.APU_emailSent is not null) THEN
		select CONCAT(oldValueList,OLD.APU_emailSent,'~') into oldValueList;
	END IF;
	IF(NEW.APU_emailSent is not null) THEN
		select CONCAT(newValueList,NEW.APU_emailSent,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.APU_arrivalDate,'%Y-%m-%d') <> date_format(NEW.APU_arrivalDate,'%Y-%m-%d')) or (date_format(OLD.APU_arrivalDate,'%Y-%m-%d') is null and date_format(NEW.APU_arrivalDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.APU_arrivalDate,'%Y-%m-%d') is not null and date_format(NEW.APU_arrivalDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.APU_arrivalDate~') into fieldNameList;
    IF(OLD.APU_arrivalDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.APU_arrivalDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.APU_arrivalDate is not null) THEN
		select CONCAT(oldValueList,OLD.APU_arrivalDate,'~') into oldValueList;
	END IF;
	IF(NEW.APU_arrivalDate is not null) THEN
		select CONCAT(newValueList,NEW.APU_arrivalDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.INS_vendorInitiation,'%Y-%m-%d') <> date_format(NEW.INS_vendorInitiation,'%Y-%m-%d')) or (date_format(OLD.INS_vendorInitiation,'%Y-%m-%d') is null and date_format(NEW.INS_vendorInitiation,'%Y-%m-%d') is not null)
      or (date_format(OLD.INS_vendorInitiation,'%Y-%m-%d') is not null and date_format(NEW.INS_vendorInitiation,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.INS_vendorInitiation~') into fieldNameList;
    IF(OLD.INS_vendorInitiation is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.INS_vendorInitiation is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.INS_vendorInitiation is not null) THEN
		select CONCAT(oldValueList,OLD.INS_vendorInitiation,'~') into oldValueList;
	END IF;
	IF(NEW.INS_vendorInitiation is not null) THEN
		select CONCAT(newValueList,NEW.INS_vendorInitiation,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.INS_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.INS_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.INS_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.INS_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.INS_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.INS_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.INS_serviceStartDate~') into fieldNameList;
    IF(OLD.INS_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.INS_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.INS_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.INS_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.INS_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.INS_serviceStartDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.INS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.INS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.INS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.INS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.INS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.INS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.INS_serviceEndDate~') into fieldNameList;
    IF(OLD.INS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.INS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.INS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.INS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.INS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.INS_serviceEndDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.INP_vendorInitiation,'%Y-%m-%d') <> date_format(NEW.INP_vendorInitiation,'%Y-%m-%d')) or (date_format(OLD.INP_vendorInitiation,'%Y-%m-%d') is null and date_format(NEW.INP_vendorInitiation,'%Y-%m-%d') is not null)
      or (date_format(OLD.INP_vendorInitiation,'%Y-%m-%d') is not null and date_format(NEW.INP_vendorInitiation,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.INP_vendorInitiation~') into fieldNameList;
    IF(OLD.INP_vendorInitiation is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.INP_vendorInitiation is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.INP_vendorInitiation is not null) THEN
		select CONCAT(oldValueList,OLD.INP_vendorInitiation,'~') into oldValueList;
	END IF;
	IF(NEW.INP_vendorInitiation is not null) THEN
		select CONCAT(newValueList,NEW.INP_vendorInitiation,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.INP_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.INP_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.INP_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.INP_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.INP_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.INP_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.INP_serviceStartDate~') into fieldNameList;
    IF(OLD.INP_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.INP_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.INP_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.INP_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.INP_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.INP_serviceStartDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.INP_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.INP_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.INP_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.INP_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.INP_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.INP_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.INP_serviceEndDate~') into fieldNameList;
    IF(OLD.INP_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.INP_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.INP_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.INP_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.INP_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.INP_serviceEndDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.EDA_vendorInitiation,'%Y-%m-%d') <> date_format(NEW.EDA_vendorInitiation,'%Y-%m-%d')) or (date_format(OLD.EDA_vendorInitiation,'%Y-%m-%d') is null and date_format(NEW.EDA_vendorInitiation,'%Y-%m-%d') is not null)
      or (date_format(OLD.EDA_vendorInitiation,'%Y-%m-%d') is not null and date_format(NEW.EDA_vendorInitiation,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.EDA_vendorInitiation~') into fieldNameList;
    IF(OLD.EDA_vendorInitiation is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.EDA_vendorInitiation is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.EDA_vendorInitiation is not null) THEN
		select CONCAT(oldValueList,OLD.EDA_vendorInitiation,'~') into oldValueList;
	END IF;
	IF(NEW.EDA_vendorInitiation is not null) THEN
		select CONCAT(newValueList,NEW.EDA_vendorInitiation,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.EDA_serviceStartDate,'%Y-%m-%d') <> date_format(NEW.EDA_serviceStartDate,'%Y-%m-%d')) or (date_format(OLD.EDA_serviceStartDate,'%Y-%m-%d') is null and date_format(NEW.EDA_serviceStartDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.EDA_serviceStartDate,'%Y-%m-%d') is not null and date_format(NEW.EDA_serviceStartDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.EDA_serviceStartDate~') into fieldNameList;
    IF(OLD.EDA_serviceStartDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.EDA_serviceStartDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.EDA_serviceStartDate is not null) THEN
		select CONCAT(oldValueList,OLD.EDA_serviceStartDate,'~') into oldValueList;
	END IF;
	IF(NEW.EDA_serviceStartDate is not null) THEN
		select CONCAT(newValueList,NEW.EDA_serviceStartDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.EDA_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.EDA_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.EDA_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.EDA_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.EDA_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.EDA_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.EDA_serviceEndDate~') into fieldNameList;
    IF(OLD.EDA_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.EDA_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.EDA_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.EDA_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.EDA_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.EDA_serviceEndDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.SPA_vendorInitiation,'%Y-%m-%d') <> date_format(NEW.SPA_vendorInitiation,'%Y-%m-%d')) or (date_format(OLD.SPA_vendorInitiation,'%Y-%m-%d') is null and date_format(NEW.SPA_vendorInitiation,'%Y-%m-%d') is not null)
      or (date_format(OLD.SPA_vendorInitiation,'%Y-%m-%d') is not null and date_format(NEW.SPA_vendorInitiation,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.SPA_vendorInitiation~') into fieldNameList;
    IF(OLD.SPA_vendorInitiation is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.SPA_vendorInitiation is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.SPA_vendorInitiation is not null) THEN
		select CONCAT(oldValueList,OLD.SPA_vendorInitiation,'~') into oldValueList;
	END IF;
	IF(NEW.SPA_vendorInitiation is not null) THEN
		select CONCAT(newValueList,NEW.SPA_vendorInitiation,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.TAS_vendorInitiation,'%Y-%m-%d') <> date_format(NEW.TAS_vendorInitiation,'%Y-%m-%d')) or (date_format(OLD.TAS_vendorInitiation,'%Y-%m-%d') is null and date_format(NEW.TAS_vendorInitiation,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAS_vendorInitiation,'%Y-%m-%d') is not null and date_format(NEW.TAS_vendorInitiation,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAS_vendorInitiation~') into fieldNameList;
    IF(OLD.TAS_vendorInitiation is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAS_vendorInitiation is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAS_vendorInitiation is not null) THEN
		select CONCAT(oldValueList,OLD.TAS_vendorInitiation,'~') into oldValueList;
	END IF;
	IF(NEW.TAS_vendorInitiation is not null) THEN
		select CONCAT(newValueList,NEW.TAS_vendorInitiation,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.TAS_serviceEndDate,'%Y-%m-%d') <> date_format(NEW.TAS_serviceEndDate,'%Y-%m-%d')) or (date_format(OLD.TAS_serviceEndDate,'%Y-%m-%d') is null and date_format(NEW.TAS_serviceEndDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.TAS_serviceEndDate,'%Y-%m-%d') is not null and date_format(NEW.TAS_serviceEndDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.TAS_serviceEndDate~') into fieldNameList;
    IF(OLD.TAS_serviceEndDate is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.TAS_serviceEndDate is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.TAS_serviceEndDate is not null) THEN
		select CONCAT(oldValueList,OLD.TAS_serviceEndDate,'~') into oldValueList;
	END IF;
	IF(NEW.TAS_serviceEndDate is not null) THEN
		select CONCAT(newValueList,NEW.TAS_serviceEndDate,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.MMG_vendorInitiation,'%Y-%m-%d') <> date_format(NEW.MMG_vendorInitiation,'%Y-%m-%d')) or (date_format(OLD.MMG_vendorInitiation,'%Y-%m-%d') is null and date_format(NEW.MMG_vendorInitiation,'%Y-%m-%d') is not null)
      or (date_format(OLD.MMG_vendorInitiation,'%Y-%m-%d') is not null and date_format(NEW.MMG_vendorInitiation,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.MMG_vendorInitiation~') into fieldNameList;
    IF(OLD.MMG_vendorInitiation is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.MMG_vendorInitiation is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.MMG_vendorInitiation is not null) THEN
		select CONCAT(oldValueList,OLD.MMG_vendorInitiation,'~') into oldValueList;
	END IF;
	IF(NEW.MMG_vendorInitiation is not null) THEN
		select CONCAT(newValueList,NEW.MMG_vendorInitiation,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.RNT_rsfReceived,'%Y-%m-%d') <> date_format(NEW.RNT_rsfReceived,'%Y-%m-%d')) or (date_format(OLD.RNT_rsfReceived,'%Y-%m-%d') is null and date_format(NEW.RNT_rsfReceived,'%Y-%m-%d') is not null)
      or (date_format(OLD.RNT_rsfReceived,'%Y-%m-%d') is not null and date_format(NEW.RNT_rsfReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_rsfReceived~') into fieldNameList;
    IF(OLD.RNT_rsfReceived is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RNT_rsfReceived is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RNT_rsfReceived is not null) THEN
		select CONCAT(oldValueList,OLD.RNT_rsfReceived,'~') into oldValueList;
	END IF;
	IF(NEW.RNT_rsfReceived is not null) THEN
		select CONCAT(newValueList,NEW.RNT_rsfReceived,'~') into newValueList;
	END IF;
END IF;

IF ((date_format(OLD.RNT_moveInspection,'%Y-%m-%d') <> date_format(NEW.RNT_moveInspection,'%Y-%m-%d')) or (date_format(OLD.RNT_moveInspection,'%Y-%m-%d') is null and date_format(NEW.RNT_moveInspection,'%Y-%m-%d') is not null)
      or (date_format(OLD.RNT_moveInspection,'%Y-%m-%d') is not null and date_format(NEW.RNT_moveInspection,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'dspdetails.RNT_moveInspection~') into fieldNameList;
    IF(OLD.RNT_moveInspection is null) THEN
		select CONCAT(oldValueList," ",'~') into oldValueList;
	END IF;
	IF(NEW.RNT_moveInspection is null) THEN
		select CONCAT(newValueList," ",'~') into newValueList;
	END IF;
	IF(OLD.RNT_moveInspection is not null) THEN
		select CONCAT(oldValueList,OLD.RNT_moveInspection,'~') into oldValueList;
	END IF;
	IF(NEW.RNT_moveInspection is not null) THEN
		select CONCAT(newValueList,NEW.RNT_moveInspection,'~') into newValueList;
	END IF;
END IF;

IF (OLD.FLB_addOn <> NEW.FLB_addOn ) THEN
       select CONCAT(fieldNameList,'dspdetails.FLB_addOn~') into fieldNameList;
       IF(OLD.FLB_addOn = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.FLB_addOn = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.FLB_addOn = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.FLB_addOn = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.HOB_displyOtherVendorCode <> NEW.HOB_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.HOB_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.HOB_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.HOB_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.HOB_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.HOB_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.FLB_displyOtherVendorCode <> NEW.FLB_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.FLB_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.FLB_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.FLB_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.FLB_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.FLB_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.FRL_displyOtherVendorCode <> NEW.FRL_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.FRL_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.FRL_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.FRL_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.FRL_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.FRL_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.APU_displyOtherVendorCode <> NEW.APU_displyOtherVendorCode ) THEN
       select CONCAT(fieldNameList,'dspdetails.APU_displyOtherVendorCode~') into fieldNameList;
       IF(OLD.APU_displyOtherVendorCode = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.APU_displyOtherVendorCode = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.APU_displyOtherVendorCode = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.APU_displyOtherVendorCode = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.TEN_Gas_Electric <> NEW.TEN_Gas_Electric ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Gas_Electric~') into fieldNameList;
       IF(OLD.TEN_Gas_Electric = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Gas_Electric = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Gas_Electric = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Gas_Electric = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.TEN_Utility_Gas_Electric <> NEW.TEN_Utility_Gas_Electric ) THEN
       select CONCAT(fieldNameList,'dspdetails.TEN_Utility_Gas_Electric~') into fieldNameList;
       IF(OLD.TEN_Utility_Gas_Electric = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.TEN_Utility_Gas_Electric = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Gas_Electric = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.TEN_Utility_Gas_Electric = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   IF (OLD.FRL_comment <> NEW.FRL_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.FRL_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.FRL_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.FRL_comment,'~') into newValueList;
  END IF;

  IF (OLD.INS_comment <> NEW.INS_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.INS_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INS_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INS_comment,'~') into newValueList;
  END IF;

  IF (OLD.INP_comment <> NEW.INP_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.INP_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.INP_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.INP_comment,'~') into newValueList;
  END IF;

  IF (OLD.EDA_comment <> NEW.EDA_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.EDA_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.EDA_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.EDA_comment,'~') into newValueList;
  END IF;

  IF (OLD.TAS_comment <> NEW.TAS_comment ) THEN
      select CONCAT(fieldNameList,'dspdetails.TAS_comment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.TAS_comment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.TAS_comment,'~') into newValueList;
  END IF;



   IF (OLD.totalDspDays <> NEW.totalDspDays ) THEN
      select CONCAT(fieldNameList,'dspdetails.totalDspDays~') into fieldNameList;
      select CONCAT(oldValueList,OLD.totalDspDays,'~') into oldValueList;
      select CONCAT(newValueList,NEW.totalDspDays,'~') into newValueList;
  END IF;



if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') <> date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
		CALL add_tblHistory (OLD.id,"dspdetails", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
	end if;
if(date_format(OLD.updatedOn,'%Y-%m-%d %H:%i:%s') = date_format(NEW.updatedOn,'%Y-%m-%d %H:%i:%s'))then
		CALL add_tblHistory (OLD.id,"dspdetails", fieldNameList, oldValueList, newValueList, 'System', OLD.corpID, now());
	end if;

END $$
DELIMITER;