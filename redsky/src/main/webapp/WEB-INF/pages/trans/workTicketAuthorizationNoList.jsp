<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="authorizationNoList.title"/></title>
    <meta name="heading" content="<fmt:message key='authorizationNoList.heading'/>"/>
    <script language="javascript" type="text/javascript">
    function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Authorization No?");
	var did = targetElement;
	if (agree){
		location.href="workTicketAuthorizationNosdelete.html?id="+did+'&decorator=popup&popup=true&shipNumber=${billingId}';
	}else{
		return false;
	}
}

function doNotSubmit(){

alert("Cannot delete this Authorization# as it has already been invoiced");
}
</script>

</head>
<s:form id="authorizationListForm" method="post" >
<c:if test="${accountpopup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	<s:hidden name="workTicketId" value="${workTicketId}" ></s:hidden>
</c:if> 
<c:if test="${!accountpopup}">
<c:set var="buttons">
<input type="button" class="cssbutton" style="width:70px; height:25px"
onclick="javascript:openWindow('editWorkTicketAuthorizationNo.html?billId=${billId}&billingId=${billingId}&authNo=${authNo}&serviceJob=${serviceJob}&serviceId=${serviceId}&serviceCom=${serviceCom}&workTicketId=${workTicketId}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=billing.billToAuthority');"
   value="<fmt:message key="button.add"/>"/> 
<input type="button" name="closeWin" class="cssbutton" value="Close Window" style="width:120px; height:25px" onclick="window.close()" />
</c:set>
</c:if>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Authorization Numbers</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		</tbody>
</table>

<s:set name="authorizationNos" value="authorizationNos" scope="request"/>
<display:table name="authorizationNos" class="table" requestURI="" id="authorizationNoList" export="true" defaultsort="1" pagesize="25"  decorator='${!accountpopup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >
	<c:if test="${!accountpopup}">
    <display:column titleKey="authorizationNo.authorizationSequenceNumber" sortable="true" sortProperty="authorizationSequenceNumber" ><a href="editWorkTicketAuthorizationNo.html?id=${authorizationNoList.id}&billId=${billId}&billingId=${billingId}&authNo=${authNo}&serviceJob=${serviceJob}&serviceId=${serviceId}&serviceCom=${serviceCom}&workTicketId=${workTicketId}&decorator=popup&popup=true"><c:out value="${authorizationNoList.authorizationSequenceNumber}" /></a></display:column>
	<display:column property="shipNumber" sortable="true" titleKey="authorizationNo.shipNumber"/>
	<display:column property="authorizationNumber" sortable="true" titleKey="authorizationNo.authorizationNumber" />
	</c:if> 
    <c:if test="${accountpopup}"> 
       <display:column property="listLinkParams" sortable="true" titleKey="authorizationNo.authorizationNumber"/>  
       <display:column property="shipNumber" sortable="true" titleKey="authorizationNo.shipNumber"/>
    </c:if>
    <display:column property="commodity" sortable="true" titleKey="authorizationNo.commodity"/>
	<display:column property="invoiceNumber" sortable="true" titleKey="authorizationNo.invoiceNumber"/>
	<display:column title="Ticket#" property="workTicketNumber"></display:column>
    <display:column property="invoiceDate" sortable="true" titleKey="authorizationNo.invoiceDate" format="{0,date,dd-MMM-yyyy}"/>
    <c:if test="${!accountpopup}">
   	<display:column title="Remove" >
   	<c:if test="${authorizationNoList.invoiceDate=='' || authorizationNoList.invoiceNumber==''}">
   	<a><img align="middle" onclick="return confirmSubmit(${authorizationNoList.id});" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/></a>
   	</c:if>
   	<c:if test="${authorizationNoList.invoiceDate!='' && authorizationNoList.invoiceNumber!=''}">
   	<a><img align="middle" onclick="return doNotSubmit();" style="margin: 0px 0px 0px 8px;"  src="images/recyle-trans.gif"/></a>
   	</c:if>
   	</display:column>
   	</c:if>	 
   
    <display:setProperty name="paging.banner.item_name" value="authorizationNo"/>
    <display:setProperty name="paging.banner.items_name" value="people"/>

    <display:setProperty name="export.excel.filename" value="AuthorizationNo List.xls"/>
    <display:setProperty name="export.csv.filename" value="AuthorizationNo List.csv"/>
    <display:setProperty name="export.pdf.filename" value="AuthorizationNo List.pdf"/>
</display:table>
 <c:if test="${!accountpopup}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>

<s:hidden name="billId" value="${billId}"/>
<c:set var="billId" value="${billId}" scope="session"/>
<s:hidden name="billingId" value="${billingId}"/>
<c:set var="billingId" value="${billingId}" scope="session"/>
<s:hidden name="serviceId" value="${serviceId}"/>
<c:set var="serviceId" value="${serviceId}" scope="session"/>
<s:hidden name="serviceCom" value="${serviceCom}"/>
<c:set var="serviceCom" value="${serviceCom}" scope="session"/>
<s:hidden name="authNo" value="${authNo}"/>
<c:set var="authNo" value="${authNo}" scope="session"/>
<s:hidden name="serviceJob" value="${serviceJob}"/>
<c:set var="serviceJob" value="${serviceJob}" scope="session"/>
<s:hidden name="popup" value="true"/>
</s:form>
<script type="text/javascript">
	try{
	<c:if test="${!param.popup}"> 
		parent.window.opener.document.location.reload();
	  	window.close();
  	</c:if>
  	}
  	catch(e){}
  ////  highlightTableRows("authorizationNoList");
</script>