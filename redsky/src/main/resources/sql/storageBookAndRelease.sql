DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`storageBookAndRelease` $$
CREATE PROCEDURE `storageBookAndRelease`(
IN locationCode varchar(40),
IN what char(1) ,
Rdt  date,
RUpdatedate datetime,
RupdatedBy varchar(50),
Tvolume  decimal(19,2),
l_corpid varchar(4),
l_volUnit varchar(3),
IN storageCode varchar(100),
IN l_oldStorage varchar(100),
IN l_idNum INT)

BEGIN
DECLARE TotalCapacity decimal(19,2);
DECLARE totalCapacity_cubicMeter decimal(19,2);
DECLARE  utilizedVolume decimal(19,2);
DECLARE  releasedVolume decimal(19,2);

DECLARE stg_TotalCapacity decimal(19,2);
DECLARE stg_totalCapacity_cubicMeter decimal(19,2);
DECLARE stg_utilizedVolume decimal(19,2);
DECLARE stg_releasedVolume decimal(19,2);

DECLARE stg_volume decimal(19,2);
DECLARE s_volume decimal(19,2);

/*insert into test (l_value) values(Tvolume);
insert into test (l_value) values(l_oldStorage);
insert into test (l_value) values(what);
insert into test (l_value) values(l_volUnit);*/

SELECT cubicFeet,cubicMeter
INTO TotalCapacity,totalCapacity_cubicMeter
FROM location
where locationid  = locationCode and corpid=l_corpid limit 1;

SELECT volumeCft,volumeCbm
INTO stg_TotalCapacity,stg_totalCapacity_cubicMeter
FROM storagelibrary
where storageId  = storageCode and corpid=l_corpid limit 1;

if(l_volUnit='Cft') then
	if (what ='L' or what ='R') then
		SELECT  COALESCE(SUM(s.volume),0)
		INTO utilizedVolume
		FROM bookstorage bs
		inner join storage s
		on  bs.corpid=s.corpid
		and bs.locationid=s.locationid
		and bs.idNum=s.idNum
		-- and bs.ticket=s.ticket
		and bs.what='L'
		and s.releaseDate is null
		where s.locationid  = locationCode and s.corpid=l_corpid and bs.corpid=l_corpid;-- and bs.idNum=l_idNum;
	
		/*update location
		set utilizedVolCft= case when utilizedVolume <=TotalCapacity then utilizedVolume else case when TotalCapacity=0 then utilizedVolume else case when Tvolume =0 then utilizedVolume
		else  utilizedVolCft+Tvolume end end end
		-- occupied= case when utilizedVolume <TotalCapacity then ''
		-- when utilizedVolume >=TotalCapacity then 'X'
		-- end  
		where locationid  = locationCode and corpid=l_corpid;
		
		update storagelibrary
		set usedVolumeCft= case when usedVolumeCft <=stg_TotalCapacity then usedVolumeCft else case when stg_TotalCapacity=0 then usedVolumeCft else case when Tvolume =0 then usedVolumeCft
		else  usedVolumeCft+Tvolume end end end, availVolumeCft=volumeCft-usedVolumeCft
		where storageId  = storageCode and corpid=l_corpid;*/
		
		update location	set utilizedVolCft= utilizedVolume where locationid  = locationCode and corpid=l_corpid;
		
		select if(sum(volume) is null,0,sum(volume)) into s_volume from storage where storageid=storageCode and corpid=l_corpid and releaseDate is null;
				
		update storagelibrary set usedVolumeCft=s_volume, availVolumeCft=volumeCft-usedVolumeCft
		where storageId  = storageCode and corpid=l_corpid;
	
	end if;
	/*if ( Rdt  is  not null ) then
	
		SELECT  COALESCE(SUM(s.volume),0)
		INTO releasedVolume
		FROM bookstorage bs
		inner join storage s
		on  bs.corpid=s.corpid
		and bs.locationid=s.locationid
		and bs.idNum=s.idNum
		and bs.ticket=s.ticket
		and bs.what='R'
		where s.locationid  = locationCode
		and s.corpid=l_corpid
		and bs.corpid=l_corpid
		and s.releaseDate=Rdt
		and s.updatedOn=RUpdatedate;
		
		update location
		set utilizedVolCft= case when utilizedVolCft>0 then utilizedVolCft-releasedVolume
		else utilizedVolCft end
		where locationid  = locationCode and corpid=l_corpid;
		
		update storagelibrary
		set usedVolumeCft= case when usedVolumeCft>0 then usedVolumeCft-releasedVolume
		else usedVolumeCft end
		where storageId  = storageCode and corpid=l_corpid;
	end if;*/
	
	
	update location
	set utilizedVolCbm= case when utilizedVolCft>0 then utilizedVolCft * 0.0283168466 else 0 end,updatedBy=RupdatedBy,UpdatedOn=RUpdatedate
	where locationid  = locationCode and corpid=l_corpid;
	
	update storagelibrary
	set usedVolumeCbm= case when usedVolumeCft>0 then usedVolumeCft * 0.0283168466 else 0 end,updatedBy=RupdatedBy,UpdatedOn=RUpdatedate,
	availVolumeCbm=volumeCbm-usedVolumeCbm
	where storageId  = storageCode and corpid=l_corpid;

end if;

if(l_volUnit='Cbm') then
	if (what ='L' or what ='R') then
		
		SELECT  COALESCE(SUM(s.volume),0)
		INTO utilizedVolume
		FROM bookstorage bs
		inner join storage s
		on  bs.corpid=s.corpid
		and bs.locationid=s.locationid
		and bs.idNum=s.idNum
		-- and bs.ticket=s.ticket
		and bs.what='L'
		and s.releaseDate is null
		where s.locationid  = locationCode and s.corpid=l_corpid and bs.corpid=l_corpid;-- and bs.idNum=l_idNum;stg_volume
				
		update location	set utilizedVolCbm= utilizedVolume where locationid  = locationCode and corpid=l_corpid;
		
		select if(sum(volume) is null,0,sum(volume)) into s_volume from storage where storageid=storageCode and corpid=l_corpid and releaseDate is null;
				
		update storagelibrary set usedVolumeCbm=s_volume, availVolumeCbm=volumeCbm-usedVolumeCbm
		where storageId  = storageCode and corpid=l_corpid;
		
	end if;
	if (what='M' and Rdt is null) then
		SELECT  COALESCE(SUM(s.volume),0)
		INTO stg_volume
		FROM bookstorage bs
		inner join storage s
		on  bs.corpid=s.corpid
		and bs.locationid=s.locationid
		and bs.idNum=s.idNum
		-- and bs.ticket=s.ticket
		and bs.what='L'
		and s.releaseDate is null
		where s.locationid  = locationCode and s.corpid=l_corpid and bs.corpid=l_corpid and bs.storageid=l_oldStorage;
		
		select sum(volume) into s_volume from storage where storageid=storageCode and corpid=l_corpid and releaseDate is null;
		
		update storagelibrary set usedVolumeCbm=s_volume, availVolumeCbm=volumeCbm-usedVolumeCbm
		where storageId  = storageCode and corpid=l_corpid;
		
		update storagelibrary set usedVolumeCbm=(usedVolumeCbm-stg_volume),availVolumeCbm=availVolumeCbm+stg_volume,usedVolumeCft= usedVolumeCbm * 35.3147 
		where storageId  = l_oldStorage and corpid=l_corpid;

	end if;
	/*if ( Rdt is not null) then
	
		SELECT  COALESCE(SUM(s.volume),0)
		INTO releasedVolume
		FROM bookstorage bs
		inner join storage s
		on  bs.corpid=s.corpid
		and bs.locationid=s.locationid
		and bs.idNum=s.idNum
		and bs.ticket=s.ticket
		and bs.what='L'
		where s.locationid  = locationCode
		and s.corpid=l_corpid
		and bs.corpid=l_corpid;
		
		update location
		set utilizedVolCbm= case when utilizedVolCbm>0 then utilizedVolCbm-releasedVolume
		else utilizedVolCbm end
		where locationid  = locationCode and corpid=l_corpid;
		
		update storagelibrary
		set usedVolumeCbm= case when usedVolumeCbm>0 then usedVolumeCbm-releasedVolume
		else usedVolumeCbm end
		where storageId  = storageCode and corpid=l_corpid;
		
	end if;*/
	
	update location
	set utilizedVolCft= case when utilizedVolCbm>0 then utilizedVolCbm * 35.3147 else 0 end,updatedBy=RupdatedBy,UpdatedOn=RUpdatedate
	where locationid  = locationCode and corpid=l_corpid;
	
	update storagelibrary
	set usedVolumeCft= case when usedVolumeCbm>0 then usedVolumeCbm * 35.3147 else 0 end,updatedBy=RupdatedBy,UpdatedOn=RUpdatedate,
	availVolumeCft=volumeCft-usedVolumeCft
	where storageId  = storageCode and corpid=l_corpid;


end if;


END $$

DELIMITER ;