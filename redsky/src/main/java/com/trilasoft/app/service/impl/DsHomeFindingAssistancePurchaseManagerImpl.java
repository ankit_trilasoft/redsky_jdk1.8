package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsHomeFindingAssistancePurchaseDao;
import com.trilasoft.app.model.DsHomeFindingAssistancePurchase;
import com.trilasoft.app.service.DsHomeFindingAssistancePurchaseManager;

public class DsHomeFindingAssistancePurchaseManagerImpl extends GenericManagerImpl<DsHomeFindingAssistancePurchase, Long> implements DsHomeFindingAssistancePurchaseManager {
	
	DsHomeFindingAssistancePurchaseDao dsHomeFindingAssistancePurchaseDao;
	
	public DsHomeFindingAssistancePurchaseManagerImpl(DsHomeFindingAssistancePurchaseDao dsHomeFindingAssistancePurchaseDao) {
		super(dsHomeFindingAssistancePurchaseDao); 
        this.dsHomeFindingAssistancePurchaseDao = dsHomeFindingAssistancePurchaseDao; 
	}
	public List getListById(Long id) {
		return dsHomeFindingAssistancePurchaseDao.getListById(id);
	}
}
