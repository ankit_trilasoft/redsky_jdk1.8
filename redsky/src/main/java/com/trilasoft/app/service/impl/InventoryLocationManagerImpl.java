/**
 * 
 */
package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.InventoryLocationDao;
import com.trilasoft.app.model.InventoryLocation;
import com.trilasoft.app.service.InventoryLocationManager;

/**
 * @author GVerma
 *
 */
public class InventoryLocationManagerImpl extends GenericManagerImpl<InventoryLocation, Long> implements InventoryLocationManager{

	InventoryLocationDao inventoryLocationDao;
	public InventoryLocationManagerImpl(InventoryLocationDao inventoryLocationDao) {
		super(inventoryLocationDao);
		this.inventoryLocationDao = inventoryLocationDao;
		// TODO Auto-generated constructor stub
	}
	public List getAllLocation(String corpId,String shipNumber) {
		return inventoryLocationDao.getAllLocation(corpId,shipNumber);
	}
	public List checkInventoryLocation(String corpID, String locType,String ticket){
		return inventoryLocationDao.checkInventoryLocation(corpID, locType, ticket);
	}
	
}
