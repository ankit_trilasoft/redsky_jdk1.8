/**
 * Implementation of <strong>ModelSupport</strong> that contains convenience methods.
 * This class represents the basic model on "Vehicle" object in Redsky that allows for Job management.
 * @Class Name	Vehicle
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */


package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
//import javax.persistence.OneToOne;
import javax.persistence.Table;
//import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.model.ServiceOrder;
import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="vehicle")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class Vehicle extends BaseObject{
	
	private String corpID;
	private Long id;
	private Long serviceOrderId;
	private String sequenceNumber;
	private String ship;
	private String shipNumber;
	private short auto;
	private Integer year=new  Integer(0);
	private String make;
	private String model;
	private String container;
	private String serial;
	private BigDecimal volume;
	private BigDecimal length;
	private BigDecimal width;
	private BigDecimal height;
	private BigDecimal weight;
	private String proNumber;
	private String licNumber;
	private Date title;
	private Date inventory;
	private String totalLine;
	private String titleNumber;
	private String cntnrNumber;
	private String idNumber;
	private String color;
	private String classEPA;
	private short doors;
	private short cylinders;
	///new data
	private Date systemDate;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String vehicleType;
	//regarding units
	private String unit1; 
	private String unit2;
	private String unit3;
	private ServiceOrder serviceOrder;
	private String ugwIntId;
	private Integer valuation=new  Integer(0);
	private String engineNumber;
	private String valuationCurrency;
	private String type;
	private boolean status = true  ;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("serviceOrderId", serviceOrderId)
				.append("sequenceNumber", sequenceNumber).append("ship", ship)
				.append("shipNumber", shipNumber).append("auto", auto)
				.append("year", year).append("make", make)
				.append("model", model).append("container", container)
				.append("serial", serial).append("volume", volume)
				.append("length", length).append("width", width)
				.append("height", height).append("weight", weight)
				.append("proNumber", proNumber).append("licNumber", licNumber)
				.append("title", title).append("inventory", inventory)
				.append("totalLine", totalLine)
				.append("titleNumber", titleNumber)
				.append("cntnrNumber", cntnrNumber)
				.append("idNumber", idNumber).append("color", color)
				.append("classEPA", classEPA).append("doors", doors)
				.append("cylinders", cylinders)
				.append("systemDate", systemDate)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("vehicleType", vehicleType).append("unit1", unit1)
				.append("unit2", unit2).append("unit3", unit3)
				.append("serviceOrder", serviceOrder)
				.append("ugwIntId", ugwIntId)
				.append("valuation", valuation)
				.append("valuationCurrency", valuationCurrency)
				.append("type", type)
				.append("engineNumber", engineNumber)
				.append("status", status).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Vehicle))
			return false;
		Vehicle castOther = (Vehicle) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(auto, castOther.auto).append(year, castOther.year)
				.append(make, castOther.make).append(model, castOther.model)
				.append(container, castOther.container)
				.append(serial, castOther.serial)
				.append(volume, castOther.volume)
				.append(length, castOther.length)
				.append(width, castOther.width)
				.append(height, castOther.height)
				.append(weight, castOther.weight)
				.append(proNumber, castOther.proNumber)
				.append(licNumber, castOther.licNumber)
				.append(title, castOther.title)
				.append(inventory, castOther.inventory)
				.append(totalLine, castOther.totalLine)
				.append(titleNumber, castOther.titleNumber)
				.append(cntnrNumber, castOther.cntnrNumber)
				.append(idNumber, castOther.idNumber)
				.append(color, castOther.color)
				.append(classEPA, castOther.classEPA)
				.append(doors, castOther.doors)
				.append(cylinders, castOther.cylinders)
				.append(systemDate, castOther.systemDate)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(vehicleType, castOther.vehicleType)
				.append(unit1, castOther.unit1).append(unit2, castOther.unit2)
				.append(unit3, castOther.unit3)
				.append(serviceOrder, castOther.serviceOrder)
				.append(ugwIntId, castOther.ugwIntId)
				.append(valuation, castOther.valuation)
				.append(engineNumber, castOther.engineNumber)
				.append(valuationCurrency, castOther.valuationCurrency)
				.append(type, castOther.type)
				.append(status, castOther.status)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(serviceOrderId).append(sequenceNumber).append(ship)
				.append(shipNumber).append(auto).append(year).append(make)
				.append(model).append(container).append(serial).append(volume)
				.append(length).append(width).append(height).append(weight)
				.append(proNumber).append(licNumber).append(title)
				.append(inventory).append(totalLine).append(titleNumber)
				.append(cntnrNumber).append(idNumber).append(color)
				.append(classEPA).append(doors).append(cylinders)
				.append(systemDate).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(vehicleType)
				.append(unit1).append(unit2).append(unit3).append(serviceOrder)
				.append(ugwIntId).append(valuationCurrency).append(type)
				.append(valuation).append(engineNumber)
				.append(status).toHashCode();
	}
	@ManyToOne
	@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,
			referencedColumnName="id")
			public ServiceOrder getServiceOrder() {
			        return serviceOrder;
			    }
			    
				 public void setServiceOrder(ServiceOrder serviceOrder) {
						this.serviceOrder = serviceOrder;
				}  
	
	
	@Column(length=5)
	public short getAuto() {
		return auto;
	}
	public void setAuto(short auto) {
		this.auto = auto;
	}
	@Column(length=50)
	public String getCntnrNumber() {
		return cntnrNumber;
	}
	public void setCntnrNumber(String cntnrNumber) {
		this.cntnrNumber = cntnrNumber;
	}
	@Column(length=10)
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	@Column(length=1)
	public String getContainer() {
		return container;
	}
	public void setContainer(String container) {
		this.container = container;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=5)
	public short getCylinders() {
		return cylinders;
	}
	public void setCylinders(short cylinders) {
		this.cylinders = cylinders;
	}
	@Column(length=5)
	public short getDoors() {
		return doors;
	}
	public void setDoors(short doors) {
		this.doors = doors;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Date getInventory() {
		return inventory;
	}
	public void setInventory(Date inventory) {
		this.inventory = inventory;
	}
	@Column(length=20)
	public BigDecimal getLength() {
		return length;
	}
	public void setLength(BigDecimal length) {
		this.length = length;
	}
	@Column(length=10)
	public String getLicNumber() {
		return licNumber;
	}
	public void setLicNumber(String licNumber) {
		this.licNumber = licNumber;
	}
	@Column(length=10)
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	@Column
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	@Column(length=10)
	public String getProNumber() {
		return proNumber;
	}
	public void setProNumber(String proNumber) {
		this.proNumber = proNumber;
	}
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=30)
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	@Column(length=2)
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public Date getTitle() {
		return title;
	}
	public void setTitle(Date title) {
		this.title = title;
	}
	@Column(length=30)
	public String getTitleNumber() {
		return titleNumber;
	}
	public void setTitleNumber(String titleNumber) {
		this.titleNumber = titleNumber;
	}
	@Column(length=1)
	public String getTotalLine() {
		return totalLine;
	}
	public void setTotalLine(String totalLine) {
		this.totalLine = totalLine;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=20)
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	@Column(length=20)
	public BigDecimal getWeight() {
		return weight;
	}
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}
	@Column(length=20)
	public BigDecimal getWidth() {
		return width;
	}
	public void setWidth(BigDecimal width) {
		this.width = width;
	}
	@Column(length=5)
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Date getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(Date systemDate) {
		this.systemDate = systemDate;
	}
	@Column(length=10)
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	@Column(length=10)
	public String getUnit2() {
		return unit2;
	}
	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	@Column(length=10)
	public String getUnit3() {
		return unit3;
	}
	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}
	/**
	 * @return the idNumber
	 */
	@Column(length=10)
	public String getIdNumber() {
		return idNumber;
	}
	/**
	 * @param idNumber the idNumber to set
	 */
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	/**
	 * @return the serviceOrderId
	 */
	@Column(length=20, insertable=false, updatable=false)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	/**
	 * @param serviceOrderId the serviceOrderId to set
	 */
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	
	@Column(length=20)
	public BigDecimal getHeight() {
		return height;
	}
	public void setHeight(BigDecimal height) {
		this.height = height;
	}
	@Column(length=10)
	public String getClassEPA() {
		return classEPA;
	}
	public void setClassEPA(String classEPA) {
		this.classEPA = classEPA;
	}
	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}
	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}
	@Column
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	@Column
	public Integer getValuation() {
		return valuation;
	}
	public void setValuation(Integer valuation) {
		this.valuation = valuation;
	}
	@Column
	public String getEngineNumber() {
		return engineNumber;
	}
	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}
	@Column
	public String getValuationCurrency() {
		return valuationCurrency;
	}
	public void setValuationCurrency(String valuationCurrency) {
		this.valuationCurrency = valuationCurrency;
	}
	@Column
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Column
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
}
