<%@ include file="/common/taglibs.jsp"%>  

 
<head>   
    <title><fmt:message key="payrollAllocationList.title"/></title>   
    <meta name="heading" content="<fmt:message key='payrollAllocationList.title'/>"/>   
    
<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=3;i++)
			{
					document.forms['payrollAllocationList'].elements[i].value = "";
			}
}			
</script>
<style>
span.pagelinks 
{
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-17px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
}

form {
margin-top:-40px;
!margin-top:-15px;
}

div#main {
margin:-5px 0 0;

}
</style>
</head>   


<s:form id="payrollAllocationList" name="payrollAllocationList" action="searchPayrollAllocation"  validate="true">   
<div id="Layer1" style="width: 100%;">  

<div id="otabs">
  <ul>
    <li><a class="current"><span>Search</span></a></li>
  </ul>
</div>
<div class="spnblk">&nbsp;</div>

<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/payrollAllocationForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px;" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields(this);"/> 
    
</c:set> 
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top"  style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width: 99%;"  >
<thead>
<tr>

<th><fmt:message key="payrollAllocation.code"/></th>
<th><fmt:message key="payrollAllocation.job"/></th>
<th><fmt:message key="payrollAllocation.service"/></th>
<th><fmt:message key="payrollAllocation.calculation"/></th>
<th></th>
<!--  <th><fmt:message key="payrollAllocation.discription"/></th>-->


</tr></thead>	
		<tbody>
		<tr>
		    <td width="20" align="left"><s:textfield name="payrollAllocation.code" required="true" cssClass="input-text" size="7"/></td>
			<td width="20" align="left"><s:select name="payrollAllocation.job" list="%{job}" cssClass="list-menu" cssStyle="width:220px" headerKey="" headerValue="" /></td>
			<td width="20" align="left"><s:select name="payrollAllocation.service" list="%{service}"  cssClass="list-menu" cssStyle="width:220px" headerKey="" headerValue="" /></td>
			<td width="20" align="left"><s:textfield name="payrollAllocation.calculation" required="true" cssClass="input-text" size="7"/></td>
			<td width="130px" align="center"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:40px;"><span></span></div>
</div>
</div>

<c:out value="${searchresults}" escapeXml="false" />  

		<div id="otabs" style="margin-top:-15px;">
		  <ul>
		    <li><a class="current"><span> Payroll Allocation List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

 
	<display:table name="payrollAllocationList" class="table" requestURI="" id="payrollAllocationSetId" export="true" defaultsort="1" pagesize="10" style="width:100%" >   
    
    <display:column property="code" headerClass="containeralign" style="text-align: right" href="payrollAllocationForm.html" paramId="id" paramProperty="id" sortable="true" titleKey="payrollAllocation.code"/>
   	
    <display:column property="job" sortable="true" titleKey="payrollAllocation.job" />
    <display:column property="service" sortable="true" titleKey="payrollAllocation.service" />
    <display:column property="calculation" sortable="true" titleKey="payrollAllocation.calculation" />
    <display:column property="rateType" sortable="true" title="Rate Type" />
    <display:column property="mode" sortable="true" title="Mode" />
    
    
    <display:setProperty name="paging.banner.item_name" value="payrollAllocation"/>   
    <display:setProperty name="paging.banner.items_name" value="customers"/>   
  
    <display:setProperty name="export.excel.filename" value="Payroll Allocation List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Payroll Allocation List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Payroll Allocation List.pdf"/>   
</display:table>   
  
<c:out value="${buttons}" escapeXml="false" />   
<c:set var="isTrue" value="false" scope="session"/>

</div>
</s:form>  
<script type="text/javascript"> 

highlightTableRows("payrollAllocationSetId"); 

</script>
