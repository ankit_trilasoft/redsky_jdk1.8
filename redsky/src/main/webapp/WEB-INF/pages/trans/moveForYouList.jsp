<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<head>
    <title>Move4u</title>
    <meta name="heading" content="Move4u"/>

   
  <style> 
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:5px; !margin-bottom:2px; margin-top:-22px; padding:2px 0px; text-align:right;
width:100%; !width:100%;
}
</style>
</head>
<s:form id="moveForUXMLForm" action="" method="post" validate="true"> 
 <!-- <p style="font-size:11px; font-weight:bold; margin-bottom:10px">Integration XML Summary</p>   -->
 <p style="font: bold 12px/2.5em arial,verdana; margin-bottom: 10px;" class="bgblue">Integration Centre</p>
	<div id="layer5" style="width:100%">	
	<div id="newmnav">
		  <ul>
		         <c:if test="${vanlineEnabled==true }">
			 <li><a href="integrationCenterList.html"><span>Vanline / Docs Downloads</span></a></li>
		      <li><a href="centreVanlineUpload.html"><span>Vanline / Docs Uploads</span></a></li>
		      <li><a href="centerIntegrationLogs.html"><span>Vanline Integration Log</span></a></li>
		       </c:if>
		         <c:if test="${enableMSS==true }">
		       <li><a href="mssLogList.html"><span>MSS Orders</span></a></li>
		       </c:if>
		         <c:if test="${voxmeIntegration==true }">
		          <li><a href="voxmeEstimatorList.html"><span>Voxme&nbsp;Estimator</span></a></li>
		         </c:if>
		          <c:if test="${enablePricePoint==true}">
		          <li><a href="pricePointList.html"><span>Price&nbsp;Point</span></a></li>
		          </c:if>
		           <c:if test="${enableMoveForYou==true }">
		         <li style="background:#FFF " id="newmnav1"><a class="current"><span>Move4u</span></a></li>
		        </c:if>
		  </ul>
	</div>
	<div class="spn" style="height:0px;!margin-bottom:1px;">&nbsp;</div>
	<div class="spn" style="height:0px;!margin-bottom:1px;">&nbsp;</div>
<s:set name="moveForUList" value="moveForUList" scope="request"/>
<display:table name="moveForUList" class="table" requestURI="" id="moveForUList" export="true" pagesize="30" style="width:100%" defaultsort="2" defaultorder="descending" >
    
	<display:column property="message" style="text-align: left;width:500px;" sortable="true" title="XML"/>
    <display:column property="date" style="text-align: left;width:500px;"  sortable="true" title="DATE"  format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="count" headerClass="containeralign" style="text-align: right;width:500px;" title="No Of XMLs / Files Downloaded"/>
    <display:setProperty name="paging.banner.first" value=""/>

    <display:setProperty name="export.excel.filename" value="Weekly XML Summary.xls"/>
    <display:setProperty name="export.csv.filename" value="Weekly XML Summary.csv"/>
    <display:setProperty name="export.pdf.filename" value="Weekly XML Summary.pdf"/> 
</display:table>
</div>
</s:form>
<script type="text/javascript">
var enableMoveForYou = ${enableMoveForYou};
<script type="text/javascript">
var vanlineEnabled = ${vanlineEnabled};
if(enableMoveForYou == false)
	{
	window.location = '/centerintegrationLogList.html';
	}
</script>