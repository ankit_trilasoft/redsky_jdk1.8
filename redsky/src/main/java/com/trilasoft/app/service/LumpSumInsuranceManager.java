package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.LumpSumInsurance;

public interface LumpSumInsuranceManager extends GenericManager<LumpSumInsurance, Long> {
	public List getItemList(Long sid,String sessionCorpID);
	public List getVehicle(Long sid,String sessionCorpID);
	public void updateInsurance(String newField,String fieldValue,Long id,Long sid,Long cid,String sessionCorpID,String updatedBy);
	public void updateVehicle(String newField,String fieldValue,Long id,Long sid,String vehicleIdNumber,String sessionCorpID);
	public String getLinkedLumpSumInsuranceData(Long id, Long sid);
	public void deleteNetworkLumpsuminsurance(Long id, Long networkSynchedId);
	public void updateLinkedInsurance(String newField, String fieldValue, Long networkSynchedId, Long sid);
	public void updateLinkedVehicleData(String newField, String fieldValue, Long id, String idNumber);
}
