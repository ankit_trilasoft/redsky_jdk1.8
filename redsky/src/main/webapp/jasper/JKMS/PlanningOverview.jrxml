<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="PlanningOverview"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="860"
		 pageHeight="775"
		 columnWidth="800"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.zoom" value="1.0" />
	<property name="ireport.x" value="0" />
	<property name="ireport.y" value="0" />
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="WorkDate" isForPrompting="true" class="java.util.Date"/>
	<parameter name="Warehouse" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT	'PlanningOverview-R-PDF-JKMS.jrxml',
	TRIM(TRIM(BOTH ',' from trim(scheduleresourceoperation.crewName))) AS scheduleresourceoperation_crewName,
	scheduleresourceoperation.localtruckNumber AS scheduleresourceoperation_localtruckNumber,
	workticket.ticket AS workticket_ticket,
	workticket.timeFrom AS workticket_timeFrom,
	workticket.scheduledArrive AS workticket_scheduledArrive,
	workticket.shipNumber AS workticket_shipNumber,
	if((trim(workticket.prefix)="" || workticket.prefix is null),if((trim(workticket.firstName)="" || workticket.firstName is null),trim(workticket.lastName),concat(trim(workticket.firstName)," ",trim(workticket.lastName))),concat(trim(workticket.prefix)," ", if((trim(workticket.firstName)="" || workticket.firstName is null),trim(workticket.lastName),concat(trim(workticket.firstName)," ",trim(workticket.lastName))))) AS ShipperName,
	DATE_FORMAT(workticket.`date2`,'%d-%b-%y') AS 'End Date',
	trim(workticket.service) AS workticket_service,
	workticket.estimatedCubicFeet AS workticket_estimatedCubicFeet,
	DATE_FORMAT($P{WorkDate},'%d-%b-%y') AS Date_Format,
	trim(workticket.destinationCity) AS workticket_destinationCity,
	trim(workticket.city) AS workticket_city,
	DATE_FORMAT(now(),'%d-%b-%y') AS currentdate

FROM scheduleresourceoperation scheduleresourceoperation
	left outer join workticket workticket ON workticket.ticket= scheduleresourceoperation.ticket AND workticket.corpID = $P{Corporate ID}
where scheduleresourceoperation.corpId=$P{Corporate ID}
	AND scheduleresourceoperation.workDate=$P{WorkDate} 
	AND  scheduleresourceoperation.warehouse like $P{Warehouse}]]></queryString>

	<field name="PlanningOverview-R-PDF-JKMS.jrxml" class="java.lang.String"/>
	<field name="scheduleresourceoperation_crewName" class="java.lang.String"/>
	<field name="scheduleresourceoperation_localtruckNumber" class="java.lang.String"/>
	<field name="workticket_ticket" class="java.lang.Long"/>
	<field name="workticket_timeFrom" class="java.lang.String"/>
	<field name="workticket_scheduledArrive" class="java.lang.String"/>
	<field name="workticket_shipNumber" class="java.lang.String"/>
	<field name="ShipperName" class="java.lang.String"/>
	<field name="End Date" class="java.lang.String"/>
	<field name="workticket_service" class="java.lang.String"/>
	<field name="workticket_estimatedCubicFeet" class="java.lang.Double"/>
	<field name="Date_Format" class="java.lang.String"/>
	<field name="workticket_destinationCity" class="java.lang.String"/>
	<field name="workticket_city" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>

	<sortField name="scheduleresourceoperation_localtruckNumber" />


		<group  name="Truck" >
			<groupExpression><![CDATA[$F{scheduleresourceoperation_localtruckNumber}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<group  name="TCKT" >
			<groupExpression><![CDATA[$F{workticket_ticket}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="43"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="3"
						width="800"
						height="38"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="18" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Planning Overview]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="487"
						y="3"
						width="188"
						height="38"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font pdfFontName="Helvetica-Bold" size="18" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Date_Format}]]></textFieldExpression>
				</textField>
			</band>
		</title>
		<pageHeader>
			<band height="1"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="18"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="4"
						width="57"
						height="14"
						key="staticText-2"/>
					<box rightPadding="4"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[Truck #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="425"
						y="4"
						width="42"
						height="14"
						key="staticText-4"/>
					<box rightPadding="4"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[Service]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="341"
						y="4"
						width="84"
						height="14"
						key="staticText-7"/>
					<box rightPadding="4"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[SO #]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="516"
						y="4"
						width="100"
						height="14"
						key="staticText-10"/>
					<box leftPadding="5"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[Shipper]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="708"
						y="4"
						width="92"
						height="14"
						key="staticText-12"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[Destination City]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="2"
						width="800"
						height="1"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="57"
						y="4"
						width="92"
						height="14"
						key="staticText-14"/>
					<box rightPadding="4"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[Crew]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="213"
						y="4"
						width="64"
						height="14"
						key="staticText-15"/>
					<box rightPadding="4"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[Dept. Time]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="277"
						y="4"
						width="64"
						height="14"
						key="staticText-16"/>
					<box rightPadding="4"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[Arrival Time]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="616"
						y="4"
						width="92"
						height="14"
						key="staticText-17"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[Origin City]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="149"
						y="4"
						width="64"
						height="14"
						key="staticText-18"/>
					<box leftPadding="2"></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[End Date]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="467"
						y="4"
						width="49"
						height="14"
						key="staticText-19"/>
					<box></box>
					<textElement textAlignment="Left" verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[Volume]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="22"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="0"
						y="4"
						width="57"
						height="18"
						key="textField-1"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="TCKT"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{scheduleresourceoperation_localtruckNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="341"
						y="4"
						width="84"
						height="18"
						key="textField-2"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="TCKT"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_shipNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="425"
						y="4"
						width="42"
						height="18"
						key="textField-5"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="TCKT"/>
					<box leftPadding="7">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_service}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="518"
						y="4"
						width="100"
						height="18"
						key="textField-6"
						stretchType="RelativeToTallestObject"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="TCKT"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ShipperName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="708"
						y="4"
						width="92"
						height="18"
						key="textField-10"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="TCKT"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_destinationCity}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="57"
						y="4"
						width="92"
						height="18"
						key="textField-14"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{scheduleresourceoperation_crewName}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="277"
						y="4"
						width="64"
						height="18"
						key="textField-16"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="TCKT"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_scheduledArrive}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="616"
						y="4"
						width="92"
						height="18"
						key="textField-17"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="TCKT"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_city}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="213"
						y="4"
						width="64"
						height="18"
						key="textField"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="TCKT"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{workticket_timeFrom}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="2"
						width="800"
						height="1"
						key="line-3"
						isPrintRepeatedValues="false"
						printWhenGroupChanges="Truck"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="149"
						y="4"
						width="64"
						height="18"
						key="textField-18"/>
					<box leftPadding="2">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{End Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="467"
						y="4"
						width="49"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.Double"><![CDATA[$F{workticket_estimatedCubicFeet}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="4"
						width="152"
						height="14"
						key="textField-11"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{currentdate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="149"
						y="4"
						width="502"
						height="14"
						key="staticText-13"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="8"/>
					</textElement>
				<text><![CDATA[PlanningOverview.jrxml]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="685"
						y="4"
						width="72"
						height="14"
						key="textField-12"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="757"
						y="4"
						width="43"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-13"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="2"
						width="797"
						height="1"
						key="line-4"/>
					<graphicElement stretchType="NoStretch" fill="Solid" />
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
