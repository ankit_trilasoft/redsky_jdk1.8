<%@ include file="/common/taglibs.jsp"%> 
<script type="text/JavaScript">     
	    function validateInv(name,position){
	    var url='viewReceipt.html?jasperName=PackingInventory.jrxml&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Work Ticket Number='+encodeURI(name)+'&reportParameter_Corporate ID=${sessionCorpID}&fileType=PDF';
		document.forms['ticketInventoryDataForm'].action =url;
		document.forms['ticketInventoryDataForm'].submit();
		return true;
		}
     
	</script> 
<s:form id="ticketInventoryDataForm">
<table name="inventroDetailTab" width="98%" class="table" cellspacing="0" cellpadding="0">
	<tr class="InvHead">
		<td rowspan="2" style="text-align:center; width:65px;">Ticket #</td>
		<td rowspan="2" style="text-align:center ; width:100px;" >Location</td>
		<td rowspan="2" style="text-align:center; width:80px;">Item # </td>
		<td rowspan="2" style="text-align:center; width:4px;">Qty</td>
		<td rowspan="2" style="text-align:center; width:150px;">Item Description</td>
		<td rowspan="2" style="text-align:center; width:30px;">Packing Code</td>
		<td rowspan="2" style="text-align:center">Carton Content</td>
		<td style="text-align: center; width: 25%;" colspan="3">Origin</td>
		<td style="text-align: center; width: 10%;" colspan="">Destination</td>
		<td rowspan="2" style="text-align:center ;width: 12px;">Delivered</td>
		</tr>
		<tr class="InvHead">
			<td style="width:160px;">Room</td>
			<td style="width:150px;">Item Condition </td>
			<td  style="width:200px;">Comments</td>
			<td>Item Condition </td>
		</tr>
		<c:set var ="wtNo" value="" />
		<c:forEach  var="invList" items="${ticketInvData}" varStatus="status" >
		<tr>
			<td>
				<c:if test ="${ invList.ticket != wtNo }">
					${invList.ticket}
					
					<img title="Print Inventory Report" src="${pageContext.request.contextPath}/images/print-blue.gif" style="vertical-align: middle;" alt=""  onClick="return validateInv('${invList.ticket}',this);"/>
					<c:set var ="wtNo" value="${invList.ticket}" />
				</c:if>
 			</td>
			<td>${invList.location}</td>
			<td>${invList.inventory}</td>
			<td  style="text-align: right">${invList.quantity}</td>
			<td align="center">
				${invList.inventoryDesc}
				<c:if test="${invList.oPhotos!=null && invList.oPhotos!=''}" >
					<img title="Inventory Image" src="${pageContext.request.contextPath}/images/cameraupload.png" style="vertical-align: middle;" alt="" width="20" height="20" onclick="imageList('${invList.oPhotos}',this,'workticketInventoryDataId')"/>
				</c:if>
			</td>
			<td>${invList.packingCode}</td>
			<td>${invList.cartonContent}</td>
			<td>${invList.oRooms}</td>
			<td>${invList.oItemCondition}</td>
			<td>${invList.oComments}</td>
			<td>${invList.dItemCondition}</td>
			<td style="text-align: center">
				<c:if test="${invList.delivered!=null && invList.delivered=='Y'}" >
					<img src="${pageContext.request.contextPath}/images/tick01.gif" />
				</c:if>
				<c:if test="${invList.delivered==null || invList.delivered=='N'}" >
					<img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
				</c:if>
			</td>
		</tr>	
		</c:forEach>
</table>
</s:form>