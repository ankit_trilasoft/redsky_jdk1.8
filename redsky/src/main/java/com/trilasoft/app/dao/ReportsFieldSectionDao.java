package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.ReportsFieldSection;

public interface ReportsFieldSectionDao extends GenericDao<ReportsFieldSection, Long> {
	public void saveReportFieldSection(ReportsFieldSection reportsFieldSection);
	public List reportfieldSectionList(Long id,String sessionCorpId, String type);
	public String updateReportsFieldSection(String fieldName,String fieldValue,	String sessionCorpID, String type);


}
