<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
   
<style>
.urlbutton a {
    width: 70px; height:21px; 
    overflow: hidden;
    float: left;
}
.urlbutton img {
    border: none;
}
.urlbutton a:hover img {
    margin-top: -21px;
}
#overlayForAgentMarket { 
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}

</style>
<style type="text/css">
@media screen and (-webkit-min-device-pixel-ratio:0) {
.shiftExt { margin-right:5px; }
.shiftExtRight { margin-right: -3px; }
.shiftExtSO { padding-right:21px !important; }
.shiftExtSO-Org { padding-right:10px !important; }

}
} 
</style>
<s:hidden name="buttonType" />
<s:hidden name="agentSearchValidation" />
<s:hidden name='oADAAgentCheck'></s:hidden>
<c:set var="salesPortalAccess" value="false" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>
<c:set var="clientdirectiveWinOpen" value="N" />
<configByCorp:fieldVisibility componentId="component.trackingstatus.field.agentsearch.clientdirective">
<c:set var="clientdirectiveWinOpen" value="Y" />
</configByCorp:fieldVisibility>
<c:if test="${!networkAgent}">
 <c:if test="${isNetworkBookingAgent==false && trackingStatus.destinationAgentExSO== '' && (trackingStatus.networkPartnerCode== null || trackingStatus.networkPartnerCode=='')}">
 	<s:hidden name="trackingStatus.destinationAgentExSO" />
 </c:if>
  <c:if test="${isNetworkBookingAgent==false && trackingStatus.originAgentExSO == '' && (trackingStatus.networkPartnerCode== null || trackingStatus.networkPartnerCode=='')}">
 	<s:hidden name="trackingStatus.originAgentExSO" />
 </c:if>
  <c:if test="${isNetworkBookingAgent==false && trackingStatus.networkAgentExSO == '' && (trackingStatus.networkPartnerCode== null || trackingStatus.networkPartnerCode=='')}">
  	<s:hidden name="trackingStatus.networkAgentExSO" />
 </c:if> 
 <c:if test="${trackingStatus.destinationSubAgentExSO == '' && isNetworkBookingAgent==false && isNetworkDestinationAgent==false}"> 
 <s:hidden name="trackingStatus.destinationSubAgentExSO" />
 </c:if>
 <c:if test="${trackingStatus.originSubAgentExSO == '' && isNetworkBookingAgent==false && isNetworkOriginAgent==false}">
 <s:hidden name="trackingStatus.originSubAgentExSO" />
 </c:if> 
 <c:if test="${!networkAgent && trackingStatus.bookingAgentExSO == '' && (trackingStatus.networkPartnerCode== null || trackingStatus.networkPartnerCode=='')}">
 <s:hidden name="trackingStatus.bookingAgentExSO" />
 </c:if> 
 </c:if>
 <c:set var="FormDateValue1" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat1" name="dateFormat1" value="dd-NNN-yy"/>
<div id="agentroles" class="switchgroup1">
		<table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1" style="width:915px;">
 		  <tbody>
 		  <c:if test="${serviceOrder.isNetworkRecord  &&  (trackingStatus.createdBy=='Networking' || serviceOrder.bookingAgentShipNumber!='')  }">
 		  <tr>
 		  <td align="right" class="listwhitetext" width="152"> Parent Network S/O#</td> 
		<td colspan="4" >
			<s:textfield cssClass="input-textUpper"  name="serviceOrder.bookingAgentShipNumber" cssStyle="width:216px;" required="true" readonly="true" tabindex=""/>
		 </td>
 		  </tr>
 		  </c:if>
 		  <tr>
		<td align="right" class="listwhitetext" style="width:152px;!width:37%;"><fmt:message key='partner.lastName4'/></td> 
		<td colspan="4" >
			<s:textfield cssClass="input-textUpper"  name="serviceOrder.bookingAgentName" cssStyle="width:215px; margin-left:1px;" required="true" readonly="true" tabindex=""/>
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'BA');" src="<c:url value='/images/address2.png'/>" />&nbsp;	
		</td> 
		<td align="right" class="listwhitetext" >Network&nbsp;</td>
		<td colspan="3">
		<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield cssClass="input-text" id="networkPartnerCodeId" key="trackingStatus.networkPartnerCode" cssStyle="width:45px;" maxlength="10" onkeypress="return disableEnterKey(event)" onchange="valid(this,'special');checkNetworkPartnerName();" tabindex="" onblur=" " onselect="getVanlineCode('OA', this);"  onfocus=" "/></td>
			<td width="17"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenPartnerNetwork();changeStatus();" id="openpopupnetwork.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td><s:textfield  cssClass="input-text"  id="networkPartnerNameId" key="trackingStatus.networkPartnerName" onchange="checkNetworkPartnerName();findPartnerDetailsByName('networkPartnerCodeId','networkPartnerNameId');" onkeyup="findPartnerDetails('networkPartnerNameId','networkPartnerCodeId','tsNetworkPartnerNameDivId',' and isAgent=true ','',event);" cssStyle="width:145px;" tabindex="" />
			<div id="tsNetworkPartnerNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
			</td>
			<td valign="top"><div id="hidImageNT" style="display: none;"><img align="left"  class="openpopup" width="17" height="20" onclick="findAgent(this,'NT');"  src="<c:url value='/images/address2.png'/>" /></div></td>
			<td align="left">
				<div id="hidNTAgent" style="vertical-align:middle; display: none;"><a><img id="navigation7" class="openpopup" onclick="getPartnerAlert(document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value,'hidNTAgent');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
			</td> 
			</tr>
			</table>
		</td>
	</tr>
		<tr><td align="right" class="listwhitetext" >Booking Agent Contact&nbsp;</td>			
			<td colspan="4">
			<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield  cssClass="input-text" key="trackingStatus.bookingAgentContact" onchange="resetEmail('BA',this.value);" cssStyle="width:215px;" tabindex="" /></td>
			<td width="56"><img align="left" class="openpopup" width="17" height="20" onclick="getContact('BA',document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value);" id="openpopupnetwork.img" src="<c:url value='/images/plus-small.png'/>" /></td>
			</tr>
			</table>
			</td>			
			<td width="180" align="right" class="listwhitetext" >Network Contact&nbsp;</td>			
			<td colspan="4">
			<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield  cssClass="input-text" key="trackingStatus.networkContact" cssStyle="width:215px;" tabindex="" /></td>			
			</tr>
			</table>
			</td>
	</tr>
	<tr>
	<td align="right" class="listwhitetext" >Booking Agent Email&nbsp;</td>
	<td colspan="4">
	<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;">
	<tr>	
	<td>			
	<s:textfield  cssClass="input-text" key="trackingStatus.bookingAgentEmail" cssStyle="width:215px;" onchange="checkEmail('trackingStatus.bookingAgentEmail');" onblur="showEmailImage();" tabindex="" /></td>
	<td><div id="hidEmailImage" style="vertical-align:middle; "><img class="openpopup" id="email1" onclick="sendEmail(document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.bookingAgentEmail}"/></td>
	<configByCorp:fieldVisibility componentId="component.field.document.documentbundleview"><td align="left"><a><img onclick="checkFromDocumentBundle('trackingStatus.bookingAgentEmail',this,'Booking Agent');" style="margin: 0px 0px 0px 1px;" src="${pageContext.request.contextPath}/images/email-attach-16.png"/></a></td></configByCorp:fieldVisibility>
	</tr>
	</table>
	</td>			<td width="180" align="right" class="listwhitetext" >Network Email&nbsp;</td>			
			<td colspan="4">
			<table cellpadding="0" cellspacing="1" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield  cssClass="input-text" name="trackingStatus.networkEmail" onchange="checkEmail('trackingStatus.networkEmail');" onblur="showNetworkEmailImage();" cssStyle="width:215px;" tabindex="" /></td>			
			<td align="left"><div id="hidNetworkEmailImage" style="display:none; vertical-align:middle; "><img align="top" class="openpopup" onclick="sendEmail(document.forms['trackingStatusForm'].elements['trackingStatus.networkEmail'].value)" id="email2" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.networkEmail}"/></td>
			<configByCorp:fieldVisibility componentId="component.field.document.documentbundleview"><td align="left"><a><img onclick="checkFromDocumentBundle('trackingStatus.networkEmail',this,'Network Agent');" style="margin: 0px 0px 0px 1px;" src="${pageContext.request.contextPath}/images/email-attach-16.png"/></a></td></configByCorp:fieldVisibility>
			</tr>
			</table>
			</td>
		</tr>
			<tr>
			<td align="right" class="listwhitetext" >Contact&nbsp;#&nbsp;</td>
					<td colspan="4"><s:textfield  cssClass="input-text" key="trackingStatus.bookingAgentPhoneNumber" maxlength="30" cssStyle="width:215px;margin-left:1px;"  onchange="resetEmail('BA',this.value);"  tabindex="" /></td>
					<td align="right" class="listwhitetext" >Contact&nbsp;#&nbsp;</td>
					<td colspan="4">
					<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
					<tr>
					<td><s:textfield  cssClass="input-text" key="trackingStatus.networkPhoneNumber" maxlength="30" cssStyle="width:215px;margin-left:1px;"    tabindex="" /></td>
					</tr>
					</table>
					</td>
			</tr>	
	<sec-auth:authComponent componentId="module.button.trackingStatus.pricingButton">
	</sec-auth:authComponent>
	<tr>
	<c:if test="${(!networkAgent && (trackingStatus.bookingAgentExSO==null || trackingStatus.bookingAgentExSO=='') &&(trackingStatus.networkPartnerCode== null || trackingStatus.networkPartnerCode==''))}">
	<td colspan="5">
	<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;" border="0">
	<tr>
	<configByCorp:fieldVisibility componentId="component.link.trackingStatus.vanline">
			 <c:if test="${serviceOrder.job=='DOM' || serviceOrder.job=='UVL' || serviceOrder.job == 'MVL'}">
			 	<td align="right" class="listwhitetext" width="16">&nbsp;</td>
				<td align="right" class="listwhitetext" >Booking Agent Vanline Code&nbsp;</td>
				<td><s:textfield cssClass="input-textUpper" key="serviceOrder.bookingAgentVanlineCode" cssStyle="width:45px;" maxlength="8" onchange="getVanlineAgent('OA');" readonly="true" tabindex=""/></td>
			</c:if>
		</configByCorp:fieldVisibility>	
	</tr>
	</table>
	</td>
	</c:if>
	
	<c:if test="${networkAgent || (trackingStatus.bookingAgentExSO!=null && trackingStatus.bookingAgentExSO!='')|| (trackingStatus.networkPartnerCode!= null && trackingStatus.networkPartnerCode!='') }">
	<td colspan="4" align="left"> 
	<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;width:389px;text-align:right;" border="0">
			<tr>
			<td width="10" style="min-width:17px;">&nbsp;</td>			
			<c:if test="${(networkAgent)}">
			<td width="70px"><div class="link-up" id="linkup_bookingAgent"  onclick="return  checkAgentAlreadyLinkUp('linkup_bookingAgent')" >&nbsp;</div></td>		
			</c:if>
			<c:if test="${(!networkAgent)}">
			<td width="71px"></td>
			</c:if>
			<td align="right" class="listwhitetext" width="60">Ext SO #&nbsp;</td>
			<td align="left"><s:textfield name="trackingStatus.bookingAgentExSO"  cssClass="input-textUpper shiftExtRight" cssStyle="width:215px;" readonly="true" tabindex=""/></td>
			</tr>
			</table> 
	</td>
	<c:if test="${not empty serviceOrder.id}">
		<c:choose>
			<c:when test="${countBookingAgentNotes == '0' || countBookingAgentNotes == '' || countBookingAgentNotes == null}">
				<td  align="right" width="50"><img id="countBookingAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BookingAgentNotes&imageId=countBookingAgentNotesImage&fieldId=countBookingAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.bookingAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=BookingAgentNotes&imageId=countBookingAgentNotesImage&fieldId=countBookingAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.bookingAgent',800,600);" ></a></td>
			</c:when>
			<c:otherwise>
				<td  align="right" width="50"><img id="countBookingAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BookingAgentNotes&imageId=countBookingAgentNotesImage&fieldId=countBookingAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.bookingAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=BookingAgentNotes&imageId=countBookingAgentNotesImage&fieldId=countBookingAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.bookingAgent',800,600);" ></a></td>
			</c:otherwise>
		</c:choose>
	</c:if>			
	</c:if>	
	
	
	        <c:choose>
			<c:when test="${(isNetworkBookingAgent==true || networkAgent || (trackingStatus.networkAgentExSO!=null && trackingStatus.networkAgentExSO!='')|| (trackingStatus.networkPartnerCode!= null && trackingStatus.networkPartnerCode!='')) }">
			<td align="right">				
			<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;">
			<tr>
			<c:if test="${(isNetworkBookingAgent==true && !(networkAgent))}">			
			<td width="70"><div class="link-up" id="linkup_Network" onclick="return  checkAgentAlreadyLinkUp('linkup_Network');" >&nbsp;</div></td> 
			</c:if>
			<c:if test="${(isNetworkBookingAgent==false || networkAgent)}">
			<td width="71px"></td>
			</c:if>
			<td style="width:1px">&nbsp;</td>
			<td align="right" class="listwhitetext" width="">Ext&nbsp;SO&nbsp;#&nbsp;</td> 
			</tr>
			</table>
			</td>
			<td colspan="3"><s:textfield name="trackingStatus.networkAgentExSO"  cssClass="input-textUpper shiftExtRight" cssStyle="width:215px;margin-left:1px;" readonly="true" tabindex=""/></td>
			
			</c:when>
			<c:otherwise>
			<td align="left" colspan="4"></td>
			</c:otherwise>
			</c:choose>		
			<c:if test="${not empty serviceOrder.id}">
				<c:choose>
					<c:when test="${countNetworkAgentNotes == '0' || countNetworkAgentNotes == '' || countNetworkAgentNotes == null}">
						<td  align="right" width="50"><img id="countNetworkAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=NetworkAgentNotes&imageId=countNetworkAgentNotesImage&fieldId=countNetworkAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.networkAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=NetworkAgentNotes&imageId=countNetworkAgentNotesImage&fieldId=countNetworkAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.networkAgent',800,600);" ></a></td>
					</c:when>
					<c:otherwise>
						<td  align="right" width="50"><img id="countNetworkAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=NetworkAgentNotes&imageId=countNetworkAgentNotesImage&fieldId=countNetworkAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.networkAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=NetworkAgentNotes&imageId=countNetworkAgentNotesImage&fieldId=countNetworkAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.networkAgent',800,600);" ></a></td>
					</c:otherwise>
				</c:choose>
			</c:if>
			
	</tr>
	<tr>
	<c:if test="${(networkAgent || (trackingStatus.networkPartnerCode!= null && trackingStatus.networkPartnerCode!=''))}">
	<td colspan="4">
	<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;" border="0">
	<tr>
	<configByCorp:fieldVisibility componentId="component.link.trackingStatus.vanline">
			 <c:if test="${serviceOrder.job=='DOM' || serviceOrder.job=='UVL' || serviceOrder.job == 'MVL'}">
			 	<td align="right" class="listwhitetext" width="24">&nbsp;</td>
				<td align="right" class="listwhitetext" >Booking Agent Vanline Code&nbsp;</td>
				<td><s:textfield cssClass="input-textUpper" key="serviceOrder.bookingAgentVanlineCode" cssStyle="width:45px;" maxlength="8" onchange="getVanlineAgent('OA');" readonly="true" tabindex=""/></td>
			</c:if>
		</configByCorp:fieldVisibility>	
	</tr>
	</table>
	</td>
	</c:if>
	</tr>	
	<tr height="10px"></tr>	
 	<tr>
 	<td align="right" class="listwhitetext" ><fmt:message key='partner.lastName1'/>&nbsp;</td>
				<td colspan="4" >
				<table  cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;">
				<tr>
				<c:if test="${agentStatusOrigin=='editable'  || agentStatusOrigin == ''}"> 
				        <c:if test="${accountInterface!='Y'}">
						<td width="2">
						<c:if test="${from=='rule'}">
						<c:if test="${field=='trackingStatus.originAgentCode'}">
						<s:textfield cssClass="rules-textUpper" key="trackingStatus.originAgentCode" id="trackingOriginAgentCodeId" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');getVanlineCode('OA', this);changeStatus();chkIsRedSky();copyOADADetail('OA');" tabindex="" onblur="showAddressImage();showOriginAgentEmailImage();" onselect="getVanlineCode('OA', this);"  onkeypress="return disableEnterKey(event);okValidateListFromTrailer();" onfocus="showPartnerAlert('onload',this.value,'hidTSOriginAgent');getEmailFromPortalUser('OA');"/>
						</c:if></c:if>
						<c:if test="${from=='rule'}">
						<c:if test="${field!='trackingStatus.originAgentCode'}">
						<s:textfield cssClass="input-text" key="trackingStatus.originAgentCode" id="trackingOriginAgentCodeId" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');getVanlineCode('OA', this);changeStatus();chkIsRedSky();copyOADADetail('OA');" tabindex="" onblur="showAddressImage();showOriginAgentEmailImage();" onselect="getVanlineCode('OA', this);"  onkeypress="return disableEnterKey(event);okValidateListFromTrailer();" onfocus="showPartnerAlert('onload',this.value,'hidTSOriginAgent');getEmailFromPortalUser('OA');"/>
						</c:if></c:if>
						
						<c:if test="${from!='rule'}">
						<s:textfield cssClass="input-text" key="trackingStatus.originAgentCode" id="trackingOriginAgentCodeId" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');getVanlineCode('OA', this);changeStatus();chkIsRedSky();copyOADADetail('OA');" tabindex="" onblur="showAddressImage();showOriginAgentEmailImage();" onselect="getVanlineCode('OA', this);"  onkeypress="return disableEnterKey(event);okValidateListFromTrailer();" onfocus="showPartnerAlert('onload',this.value,'hidTSOriginAgent');getEmailFromPortalUser('OA');"/>
						</c:if>
						</td>
						</c:if>
						<c:if test="${accountInterface=='Y'}">
						<td width="32">
						<c:if test="${from=='rule'}">
						<c:if test="${field=='trackingStatus.originAgentCode'}">
						<s:textfield cssClass="rules-textUpper" key="trackingStatus.originAgentCode" id="trackingOriginAgentCodeId" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');getVanlineCode('OA', this);changeStatus();chkIsRedSky();copyOADADetail('OA');" tabindex="" onblur="showAddressImage();showOriginAgentEmailImage();" onselect="getVanlineCode('OA', this);"  onkeypress="return disableEnterKey(event);okValidateListFromTrailer();" onfocus="showPartnerAlert('onload',this.value,'hidTSOriginAgent');getEmailFromPortalUser('OA');"/>
						</c:if></c:if>
						<c:if test="${from=='rule'}">
						<c:if test="${field!='trackingStatus.originAgentCode'}">
						<s:textfield cssClass="input-text" key="trackingStatus.originAgentCode" id="trackingOriginAgentCodeId" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');getVanlineCode('OA', this);changeStatus();chkIsRedSky();copyOADADetail('OA');" tabindex="" onblur="showAddressImage();showOriginAgentEmailImage();" onselect="getVanlineCode('OA', this);" onkeypress="return disableEnterKey(event);okValidateListFromTrailer();" onfocus="showPartnerAlert('onload',this.value,'hidTSOriginAgent');getEmailFromPortalUser('OA');"/>
						</c:if></c:if>
						<c:if test="${from!='rule'}">
						<s:textfield cssClass="input-text" key="trackingStatus.originAgentCode" id="trackingOriginAgentCodeId" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');getVanlineCode('OA', this);changeStatus();chkIsRedSky();copyOADADetail('OA');" tabindex="" onblur="showAddressImage();showOriginAgentEmailImage();" onselect="getVanlineCode('OA', this);"  onkeypress="return disableEnterKey(event);okValidateListFromTrailer();" onfocus="showPartnerAlert('onload',this.value,'hidTSOriginAgent');getEmailFromPortalUser('OA');"/>
						</c:if>
						</td>
						</c:if>
						<c:if test="${accountInterface!='Y'}">
						<c:if test="${clientdirectiveWinOpen=='Y'}">
						<td width="17">
						<img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromTrailer();winOpenOriginNew();changeStatus();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" />
						</td>
						</c:if>	
						<c:if test="${clientdirectiveWinOpen!='Y'}">
						<td width="17">
						<img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromTrailer();winOpenOrigin();changeStatus();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" />
						</td>
						</c:if>		
					    </c:if>
					   <c:if test="${accountInterface=='Y'}">
					   <c:if test="${clientdirectiveWinOpen=='Y'}">
					   <td>
					   <img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromTrailer();winOpenOriginNew();changeStatus();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" />
					   </td>
					   </c:if>
					   <c:if test="${clientdirectiveWinOpen!='Y'}">
					   <td>
					   <img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromTrailer();winOpenOrigin();changeStatus();" id="openpopup1.img" src="<c:url value='/images/open-popup.gif'/>" />
					   </td>
					   </c:if>			
					   </c:if>
					</c:if>
					<c:if test="${agentStatusOrigin=='noneditable'}">
						<td>
						<c:if test="${from=='rule'}">
						<c:if test="${field=='trackingStatus.originAgentCode'}">
						<s:textfield cssClass="rules-textUpper" key="trackingStatus.originAgentCode" id="trackingOriginAgentCodeId" readonly="true" cssStyle="width:45px;" maxlength="10" onchange="changeStatus();" tabindex="" />
						</c:if></c:if>
						<c:if test="${from=='rule'}">
						<c:if test="${field!='trackingStatus.originAgentCode'}">
						<s:textfield cssClass="input-text" key="trackingStatus.originAgentCode" id="trackingOriginAgentCodeId" readonly="true" cssStyle="width:45px;" maxlength="10" onchange="changeStatus();" tabindex=""/>
						</c:if></c:if>
						<c:if test="${from!='rule'}">
						<s:textfield cssClass="input-text" key="trackingStatus.originAgentCode" id="trackingOriginAgentCodeId" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');getVanlineCode('OA', this);changeStatus();chkIsRedSky();copyOADADetail('OA');" tabindex="" onblur="showAddressImage();showOriginAgentEmailImage();" onselect="getVanlineCode('OA', this);"  onkeypress="return disableEnterKey(event);okValidateListFromTrailer();" onfocus="showPartnerAlert('onload',this.value,'hidTSOriginAgent');getEmailFromPortalUser('OA');"/>
					</c:if>
						</td>
						<td width="17"><img align="left" class="openpopup" width="17" height="20" onclick="alert('Origin agent is already assigned.');" id="openpopup3.img" src="<c:url value='/images/open-popup.gif'/>" /></td>			
					</c:if>
				<c:if test="${clientdirectiveWinOpen=='Y'}">
				<td> 
				<c:if test="${agentSearchValidation}">
	         <s:textfield cssStyle="width:147px"   cssClass="input-text"  id="trackingOriginAgentNameId" key="trackingStatus.originAgent"  required="true" onchange="resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');chkIsRedSky();findPartnerDetailsByName('trackingOriginAgentCodeId','trackingOriginAgentNameId');" onkeyup="findPartnerDetails('trackingOriginAgentNameId','trackingOriginAgentCodeId','trackingOriginAgentNameDivId',' and (isAgent=true)','',event);"  onblur="showAddressImage();showOriginAgentEmailImage();" tabindex="" />
				</c:if>
				<c:if test="${!agentSearchValidation}">
		     <s:textfield cssStyle="width:147px"   cssClass="input-text"  id="trackingOriginAgentNameId" key="trackingStatus.originAgent"  required="true" onchange="resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');chkIsRedSky();findPartnerDetailsByName('trackingOriginAgentCodeId','trackingOriginAgentNameId');" onkeyup="findPartnerDetails('trackingOriginAgentNameId','trackingOriginAgentCodeId','trackingOriginAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);"  onblur="showAddressImage();showOriginAgentEmailImage();" tabindex="" />
				</c:if>
				<div id="trackingOriginAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</c:if>	
				<c:if test="${clientdirectiveWinOpen!='Y'}">
				<td> 
				<c:if test="${agentSearchValidation}">
	        <s:textfield cssStyle="width:147px"   cssClass="input-text"  id="trackingOriginAgentNameId" key="trackingStatus.originAgent"  required="true" onchange="resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');chkIsRedSky();findPartnerDetailsByName('trackingOriginAgentCodeId','trackingOriginAgentNameId');" onkeyup="findPartnerDetails('trackingOriginAgentNameId','trackingOriginAgentCodeId','trackingOriginAgentNameDivId',' and (isAgent=true)','',event);"  onblur="showAddressImage();showOriginAgentEmailImage();" tabindex="" />
				</c:if>
				<c:if test="${!agentSearchValidation}">
		       <s:textfield cssStyle="width:147px"   cssClass="input-text"  id="trackingOriginAgentNameId" key="trackingStatus.originAgent"  required="true" onchange="resetContactEmail('OA');populateAgentCodeAlert('OA','trackingOriginAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSOriginAgent');chkIsRedSky();findPartnerDetailsByName('trackingOriginAgentCodeId','trackingOriginAgentNameId');" onkeyup="findPartnerDetails('trackingOriginAgentNameId','trackingOriginAgentCodeId','trackingOriginAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);"  onblur="showAddressImage();showOriginAgentEmailImage();" tabindex="" />
				</c:if>
				<div id="trackingOriginAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</c:if>
				<td valign="top"><div id="hidImage"><img align="left"  class="openpopup" width="17" height="20" onclick="findAgent(this,'OA');"  src="<c:url value='/images/address2.png'/>" /></div></td>
				<td align="left">
					<div id="hidTSOriginAgent" style="vertical-align:middle;"><a><img id="navigation7" class="openpopup" onclick="getPartnerAlert(document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value,'hidTSOriginAgent');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
				</td>
				<td align="left">
				<div style="float:left;width:52px;">
					<div id="hidRUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation101" style="cursor:default!important;" class="openpopup"  src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
					<div id="hidAP" style="vertical-align:middle;display:none;float:left;"><img id="navigation201" style="cursor:default!important;" class="openpopup"  src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
					
				</div>
				</td>
				<script type="text/javascript">
					showPartnerAlert('onload','${trackingStatus.originAgentCode}','hidTSOriginAgent');					
				</script>
				</tr>
				</table>
				</td>
          		<td align="right" class="listwhitetext" ><fmt:message key='partner.lastName2'/>&nbsp;</td>
          			<td colspan="9"><table style="margin:0px;" cellpadding="0" cellspacing="1"><tr>
					<c:if test="${agentStatusDestination=='editable'  || agentStatusDestination == ''}">
						<c:if test="${accountInterface!='Y'}">
							<td width="9">
								<c:if test="${from=='rule'}">
									<c:if test="${field=='trackingStatus.destinationAgentCode'}">
										<s:textfield cssClass="rules-textUpper" key="trackingStatus.destinationAgentCode" id="trackingDestinationAgentCodeId" cssStyle="width:46px; margin-left:1px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');getVanlineCode('DA', this);changeStatus();chkIsDestinRedSky();copyOADADetail('DA');" tabindex=""  onkeypress="return disableEnterKey(event);okValidateListFromDestin();" onblur="showAddressImage1();showDestinationAgentEmailImage();" onselect="getVanlineCode('DA', this);"  onfocus="showPartnerAlert('onload',this.value,'hidTSDestinAgent');getEmailFromPortalUser('DA');"/>
									</c:if>
								</c:if>
								<c:if test="${from=='rule'}">
									<c:if test="${field!='trackingStatus.destinationAgentCode'}">
										<s:textfield cssClass="input-text" key="trackingStatus.destinationAgentCode" id="trackingDestinationAgentCodeId" cssStyle="width:46px; margin-left:1px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');getVanlineCode('DA', this);changeStatus();chkIsDestinRedSky();copyOADADetail('DA');" tabindex="" onkeypress="return disableEnterKey(event);okValidateListFromDestin();"  onblur="showAddressImage1();showDestinationAgentEmailImage();" onselect="getVanlineCode('DA', this);" onfocus="showPartnerAlert('onload',this.value,'hidTSDestinAgent');getEmailFromPortalUser('DA');"/>
									</c:if>
								</c:if>
								<c:if test="${from!='rule'}">
									<s:textfield cssClass="input-text" key="trackingStatus.destinationAgentCode" id="trackingDestinationAgentCodeId" cssStyle="width:46px; margin-left:1px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');getVanlineCode('DA', this);changeStatus();chkIsDestinRedSky();copyOADADetail('DA');" tabindex="" onkeypress="return disableEnterKey(event);okValidateListFromDestin();" onblur="showAddressImage1();showDestinationAgentEmailImage();" onselect="getVanlineCode('DA', this);" onfocus="showPartnerAlert('onload',this.value,'hidTSDestinAgent');getEmailFromPortalUser('DA');"/>
									</c:if>
							</td>
						</c:if>
						<c:if test="${accountInterface=='Y'}">
							<td width="8">
								<c:if test="${from=='rule'}">
									<c:if test="${field=='trackingStatus.destinationAgentCode'}">
										<s:textfield cssClass="rules-textUpper" key="trackingStatus.destinationAgentCode" id="trackingDestinationAgentCodeId" cssStyle="width:46px; margin-left:1px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');getVanlineCode('DA', this);changeStatus();chkIsDestinRedSky();copyOADADetail('DA');" tabindex="" onkeypress="return disableEnterKey(event);okValidateListFromDestin();"  onblur="showAddressImage1();showDestinationAgentEmailImage();" onselect="getVanlineCode('DA', this);"  onfocus="showPartnerAlert('onload',this.value,'hidTSDestinAgent');getEmailFromPortalUser('DA');"/>
									</c:if>
								</c:if>
								<c:if test="${from=='rule'}">
									<c:if test="${field!='trackingStatus.destinationAgentCode'}">
										<s:textfield cssClass="input-text" key="trackingStatus.destinationAgentCode" id="trackingDestinationAgentCodeId" cssStyle="width:46px; margin-left:1px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');getVanlineCode('DA', this);changeStatus();chkIsDestinRedSky();copyOADADetail('DA');" tabindex="" onkeypress="return disableEnterKey(event);okValidateListFromDestin();"  onblur="showAddressImage1();showDestinationAgentEmailImage();" onselect="getVanlineCode('DA', this);" onfocus="showPartnerAlert('onload',this.value,'hidTSDestinAgent');getEmailFromPortalUser('DA');"/>
									</c:if>
								</c:if>
								<c:if test="${from!='rule'}">
									<s:textfield cssClass="input-text" key="trackingStatus.destinationAgentCode" id="trackingDestinationAgentCodeId" cssStyle="width:46px; margin-left:1px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');getVanlineCode('DA', this);changeStatus();chkIsDestinRedSky();copyOADADetail('DA');" tabindex="" onkeypress="return disableEnterKey(event);okValidateListFromDestin();" onblur="showAddressImage1();showDestinationAgentEmailImage();" onselect="getVanlineCode('DA', this);" onfocus="showPartnerAlert('onload',this.value,'hidTSDestinAgent');getEmailFromPortalUser('DA');"/>
								</c:if>
							</td> 
						</c:if>
						<c:if test="${accountInterface!='Y'}">
						<c:if test="${clientdirectiveWinOpen=='Y'}">
							<td width="17"><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromDestin();winOpenDestNew();changeStatus();" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
						</c:if>	
						<c:if test="${clientdirectiveWinOpen!='Y'}">
					   		<td width="17"><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromDestin();winOpenDest();changeStatus();" id="openpopup4.img"  src="<c:url value='/images/open-popup.gif'/>" /></td>
					   	</c:if>		
					   	</c:if>
					   	<c:if test="${accountInterface=='Y'}">
					   	<c:if test="${clientdirectiveWinOpen=='Y'}">
							<td width="17"><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromDestin();winOpenDestNew();changeStatus();" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
						</c:if>
					   	<c:if test="${clientdirectiveWinOpen!='Y'}">
					   		<td width="17"><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromDestin();winOpenDest();changeStatus();" id="openpopup4.img"  src="<c:url value='/images/open-popup.gif'/>" /></td>
					   	</c:if>
					   	</c:if>
					</c:if>
					<td colspan="3" style="margin: 0px;padding: 0px;">
					<table width="100%" cellpadding="0" cellspacing="0" style="margin: 0px;padding:0px;"><tr>
					<c:if test="${agentStatusDestination == 'noneditable'}">	
					<td>
						<table cellpadding="0" cellspacing="0" style="margin: 0px;padding: 0px;"><tr>
						<td>
						<s:textfield cssClass="input-text" key="trackingStatus.destinationAgentCode" id="trackingDestinationAgentCodeId" readonly="true" cssStyle="width:45px;" maxlength="10" tabindex=""/>
						</td>
						<td><img align="left" class="openpopup" width="17" height="20" onclick="alert('Destination agent is already assigned.');" id="openpopup6.img" src="<c:url value='/images/open-popup.gif'/>" /></td>		
						</tr>
						</table>					
					</td>
					</c:if>
					<td>					
					<table  cellpadding="1" cellspacing="1" style="margin: 0px;padding: 0px;"><tr>
						<c:if test="${clientdirectiveWinOpen=='Y'}">
					<td width="6" align="left">
					<c:if test="${agentSearchValidation}">
					<s:textfield   cssStyle="width:145px" cssClass="input-text" id="trackingDestinationAgentNameId"  key="trackingStatus.destinationAgent" onchange="resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');chkIsDestinRedSky();findPartnerDetailsByName('trackingDestinationAgentCodeId','trackingDestinationAgentNameId');" onkeyup="findPartnerDetails('trackingDestinationAgentNameId','trackingDestinationAgentCodeId','trackingDestinationAgentNameDivId',' and (isAgent=true)','',event);" onblur="showAddressImage1();showDestinationAgentEmailImage();" tabindex="" /> 
					</c:if>
					<c:if test="${!agentSearchValidation}">
					<s:textfield   cssStyle="width:145px" cssClass="input-text" id="trackingDestinationAgentNameId"  key="trackingStatus.destinationAgent" onchange="resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');chkIsDestinRedSky();findPartnerDetailsByName('trackingDestinationAgentCodeId','trackingDestinationAgentNameId');" onkeyup="findPartnerDetails('trackingDestinationAgentNameId','trackingDestinationAgentCodeId','trackingDestinationAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);" onblur="showAddressImage1();showDestinationAgentEmailImage();" tabindex="" /> 
					</c:if>
					<div id="trackingDestinationAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
					</td>
					</c:if>
					<c:if test="${clientdirectiveWinOpen!='Y'}">
					<td width="6" align="left">
					<c:if test="${agentSearchValidation}">
					<s:textfield   cssStyle="width:145px" cssClass="input-text" id="trackingDestinationAgentNameId"  key="trackingStatus.destinationAgent" onchange="resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');chkIsDestinRedSky();findPartnerDetailsByName('trackingDestinationAgentCodeId','trackingDestinationAgentNameId');" onkeyup="findPartnerDetails('trackingDestinationAgentNameId','trackingDestinationAgentCodeId','trackingDestinationAgentNameDivId',' and (isAgent=true)','',event);" onblur="showAddressImage1();showDestinationAgentEmailImage();" tabindex="" /> 
					</c:if>
					<c:if test="${!agentSearchValidation}">
					<s:textfield   cssStyle="width:145px" cssClass="input-text" id="trackingDestinationAgentNameId"  key="trackingStatus.destinationAgent" onchange="resetContactEmail('DA');populateAgentCodeAlert('DA','trackingDestinationAgentNameDivId');showPartnerAlert('onload',this.value,'hidTSDestinAgent');chkIsDestinRedSky();findPartnerDetailsByName('trackingDestinationAgentCodeId','trackingDestinationAgentNameId');" onkeyup="findPartnerDetails('trackingDestinationAgentNameId','trackingDestinationAgentCodeId','trackingDestinationAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);" onblur="showAddressImage1();showDestinationAgentEmailImage();" tabindex="" /> 
					</c:if>
					<div id="trackingDestinationAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
					</td>
					</c:if>
					<td valign="top"><div id="hidImage1"><img align="left"  class="openpopup" width="17" height="20" onclick="findAgent(this,'DA');"  src="<c:url value='/images/address2.png'/>" /></div></td>
					<td align="left">
						<div id="hidTSDestinAgent" style="vertical-align:middle;"><a><img id="navigation8" class="openpopup" onclick="getPartnerAlert(document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value,'hidTSDestinAgent');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
					</td>
					<td align="left">
					<div style="float:left;width:52px;">
					<div id="hidDestRUC" style="vertical-align:middle;display:none;float:left;"><img id="navigation102" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
					<div id="hidDestAP" style="vertical-align:middle;display:none;float:left;"><img id="navigation202" class="openpopup"  style="cursor:default!important;" src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
					
					</div>
				</td>
					<script type="text/javascript">
					setTimeout("showPartnerAlert('onload','${trackingStatus.destinationAgentCode}','hidTSDestinAgent')", 1000);
					</script>
					</tr>
					</table>
					</td>
					</tr>
					</table>
					</td>
					</tr></table></td>
		 	</tr>
		 	<configByCorp:fieldVisibility componentId="component.link.trackingStatus.vanline">
			 	<c:if test="${serviceOrder.job=='DOM' || serviceOrder.job=='UVL' || serviceOrder.job == 'MVL'}">
				 	<tr>
							<td align="right" class="listwhitetext" >Origin Agent Vanline Code&nbsp;</td>
							<td colspan="3">
							<table cellpadding="1" cellspacing="1" style="margin:0px;padding:0px;">
							<tr>
							<td width="45"><s:textfield cssClass="input-text" key="trackingStatus.originAgentVanlineCode" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special');getVanlineAgent('OA');" tabindex=""/></td>
							<td align="left">
								<a><img class="openpopup" id="navigationOA" onclick="getVanlineCode('OA', this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Vanline List" title="Vanline List" /></a> 
							</td>
							</tr>
							</table>
							</td>
							<td width="56"></td>
							<td align="right" class="listwhitetext" >Dest Agent Vanline Code&nbsp;</td>
							<td colspan="3">
							<table cellpadding="1" cellspacing="1" style="margin:0px;padding:0px;">
							<tr>
							<td width="45"><s:textfield cssClass="input-text" key="trackingStatus.destinationAgentVanlineCode" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special');getVanlineAgent('DA');" tabindex=""/></td>
							<td align="left">
								<a><img class="openpopup" id="navigationDA" onclick="getVanlineCode('DA', this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Vanline List" title="Vanline List" /></a> 
							</td>
							</tr>
							</table>
							</td>
					</tr>
				</c:if>
			</configByCorp:fieldVisibility>
 			<tr>
					<td align="right" class="listwhitetext" >Origin Agent Contact&nbsp;</td>
					<s:hidden name="trackingStatus.originAgentContactImgFlag"></s:hidden>
					<s:hidden name="trackingStatus.destinationAgentContactImgFlag"></s:hidden>
					<s:hidden name="trackingStatus.subOriginAgentContactImgFlag"></s:hidden>
					<s:hidden name="trackingStatus.subDestinationAgentImgFlag"></s:hidden>
					<td colspan="4">
					<table cellpadding="1" cellspacing="1" style="margin:0px;padding:0px;">
					<tr>
					<td><s:textfield  cssClass="input-text" key="trackingStatus.originAgentContact" onchange="resetEmail('OA',this.value);" cssStyle="width:215px;" tabindex="" /></td>
					<td width="">					
						<div id="hidImagePlus" style="vertical-align:middle;float:left; "><img class="openpopup" width="17" height="20" onclick="getContact('OA',document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value);" src="<c:url value='/images/plus-small.png'/>" /></div>
					    <div id="hidActiveAP" style="vertical-align:middle;display:none;float:left;"><img id="navigation2011" style="cursor:default!important;" class="openpopup"  src="${pageContext.request.contextPath}/images/agent-user.png" alt="ACTIVE AGENT PORTAL" title="ACTIVE AGENT PORTAL" /></div>
					</td>
					</tr>
					</table>
					</td>
					<script type="text/javascript">
				 		showAddressImage(); 
	          		</script>
					<td align="right" class="listwhitetext" >Dest Agent Contact&nbsp;</td>
					<td colspan="4">
						<table cellpadding="1" cellspacing="1" style="margin:0px;padding:0px;">
							<tr>
								<td><s:textfield  cssClass="input-text" key="trackingStatus.destinationAgentContact" onchange="resetEmail('DA',this.value);" cssStyle="width:215px;" tabindex="" /></td>
								<td valign="bottom">								
								<div id="hidImagePlus1" style="float:left;"><img align="left"   class="openpopup" width="17" height="20" onclick="getContact('DA',document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value);"  src="<c:url value='/images/plus-small.png'/>" /></div>
							    <div id="hidDestinActiveAP" style="vertical-align:middle;display:none;float:left;"><img id="navigation2011" style="cursor:default!important;" class="openpopup"  src="${pageContext.request.contextPath}/images/agent-user.png" alt="ACTIVE AGENT PORTAL" title="ACTIVE AGENT PORTAL" /></div>
							</td>
							</tr>
						</table>
					</td>
					<script type="text/javascript">
					 	showAddressImage1(); 
          		 	</script>
			</tr>
 			<tr>	<td align="right" class="listwhitetext" >Origin Agent Email&nbsp;</td>
					<td colspan="4">
					<table cellpadding="1" cellspacing="1" style="margin:0px;padding:0px;">
					<tr>
					<td>
					<s:textfield  cssClass="input-text" id="trackingStatus.originAgentEmail" name="trackingStatus.originAgentEmail" cssStyle="width:215px;" onchange="checkEmail('trackingStatus.originAgentEmail');" onblur="showOriginAgentEmailImage();" tabindex="" /></td>
					<td align="left"><div id="hidOriginAgentEmailImage" style="vertical-align:middle; "><img align="top" class="openpopup" onclick="sendEmail(document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value)" id="email2" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.originAgentEmail}"/></td>
					<configByCorp:fieldVisibility componentId="component.field.document.documentbundleview"><td align="left"><a><img onclick="checkFromDocumentBundle('trackingStatus.originAgentEmail',this,'Origin Agent');" style="margin: 0px 0px 0px 1px;" src="${pageContext.request.contextPath}/images/email-attach-16.png"/></a></td></configByCorp:fieldVisibility>
					</tr>
					</table>
					</td>
					<td align="right" class="listwhitetext" >Dest Agent Email&nbsp;</td>
					<td colspan="4">
					<table cellpadding="1" cellspacing="1" style="margin:0px;padding:0px;">
					<tr>
					<td><s:textfield  cssClass="input-text" key="trackingStatus.destinationAgentEmail" id="trackingStatus.destinationAgentEmail" cssStyle="width:215px;"  onchange="checkEmail('trackingStatus.destinationAgentEmail');" onblur="showDestinationAgentEmailImage();" tabindex="" /></td>
					<td align="left"><div id="hidDestinationAgentEmailImage" style="vertical-align:middle; "><img class="openpopup" onclick="sendEmail(document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value)" id="email3" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.destinationAgentEmail}"/></td>
					<configByCorp:fieldVisibility componentId="component.field.document.documentbundleview"><td align="left"><a><img onclick="checkFromDocumentBundle('trackingStatus.destinationAgentEmail',this,'Destination Agent');" style="margin: 0px 0px 0px 1px;" src="${pageContext.request.contextPath}/images/email-attach-16.png"/></a></td></configByCorp:fieldVisibility>
					</tr>
					</table>
					</td>
			</tr>
			<tr>
			<td align="right" class="listwhitetext" >Contact&nbsp;#&nbsp;</td>
					<td colspan="4">
					<table style="margin:0px;padding:0px;" cellpadding="0">
					<tr>
					<td>
					<s:textfield  cssClass="input-text" key="trackingStatus.originAgentPhoneNumber" maxlength="30" cssStyle="width:215px;"  onchange="resetEmail('OA',this.value);"  tabindex="" />
					</td>
					<c:if test="${salesPortalAccess=='false'}">
					<configByCorp:fieldVisibility componentId="component.trackingStatus.copyOADADetail">
					<td>
					<c:if test="${serviceOrdersSize>1 }">
					<div id="hidCopyOADetailImage" style="display: block;">	
					<input class="copy-OA-Btn" type="button" onclick="agentDetailTransfer('OA');"  value="Copy OA to SO"/>
					</div>
					</c:if>
					</td>
					</configByCorp:fieldVisibility>
					</c:if>
					</tr>
					</table>
					</td>
					<td align="right" class="listwhitetext" >Contact&nbsp;#&nbsp;</td>
					<td colspan="4">
					<table cellpadding="1" cellspacing="1" style="margin:0px;padding:0px;">
					<tr>
					<td><s:textfield  cssClass="input-text" key="trackingStatus.destinationAgentPhoneNumber" maxlength="30" cssStyle="width:215px;"  onchange="resetEmail('DA',this.value);"  tabindex="" />
					</td>
					<c:if test="${salesPortalAccess=='false'}">
					<configByCorp:fieldVisibility componentId="component.trackingStatus.copyOADADetail">
					<td>
					<c:if test="${serviceOrdersSize>1}">
					<div id="hidCopyDADetailImage" style="display: block;">						
					<input class="copy-OA-Btn" type="button" tabindex="" onclick="agentDetailTransfer('DA');"  value="Copy DA to SO"/>					
					</div>
					</c:if>	
					</td>
					</configByCorp:fieldVisibility>
					</c:if>
					</td>
					</tr>
					</table>
					</td>
			</tr>
			<tr>
	<td colspan="4" align="left">
	<c:if test="${(isNetworkBookingAgent==true || networkAgent || (trackingStatus.originAgentExSO!=null && trackingStatus.originAgentExSO !='' )||(trackingStatus.networkPartnerCode!= null && trackingStatus.networkPartnerCode!=''))}">
	<table cellpadding="1" cellspacing="0" border="0" style="margin:0px; padding:0px;width:389px;text-align:right;">
			<tr>
			<td width="10" style="min-width:17px;">&nbsp;</td>			
			
			<c:if test="${(isNetworkBookingAgent==true || networkAgent)}">
			<td width="70"><div class="link-up" id="linkup_originAgentExSO"  onclick="return  checkAgentAlreadyLinkUp('linkup_originAgentExSO');" >&nbsp;</div></td>		
			</c:if>			
			
			<c:if test="${(isNetworkBookingAgent==false && !networkAgent)}">
			<td width="71px"></td>
			</c:if>
			<td align="right" class="listwhitetext" width="60">Ext&nbsp;SO&nbsp;#&nbsp;</td>
			<td align="left"><s:textfield name="trackingStatus.originAgentExSO"  cssClass="input-textUpper shiftExtRight" cssStyle="width:215px;margin-left:3px;" readonly="true" tabindex=""/></td>
			</tr>
			</table>
	</c:if>
	</td>
	<c:if test="${not empty serviceOrder.id}">
				<c:choose>
					<c:when test="${countOriginAgentNotes == '0' || countOriginAgentNotes == '' || countOriginAgentNotes == null}">
						<td  align="right" width="50"><img id="countOriginAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginAgentNotes&imageId=countOriginAgentNotesImage&fieldId=countOriginAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.originAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=OriginAgentNotes&imageId=countOriginAgentNotesImage&fieldId=countOriginAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.originAgent',800,600);" ></a></td>
					</c:when>
					<c:otherwise>
						<td  align="right" width="50"><img id="countOriginAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=OriginAgentNotes&imageId=countOriginAgentNotesImage&fieldId=countOriginAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.originAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=OriginAgentNotes&imageId=countOriginAgentNotesImage&fieldId=countOriginAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.originAgent',800,600);" ></a></td>
					</c:otherwise>
				</c:choose>
			</c:if>
			
			<c:choose>		
			<c:when test="${(isNetworkBookingAgent==true || networkAgent || (trackingStatus.destinationAgentExSO!= null && trackingStatus.destinationAgentExSO!='') || (trackingStatus.networkPartnerCode!= null && trackingStatus.networkPartnerCode!=''))}">
			<td align="right">
			<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;" border="0">
			<tr>			
			<c:if test="${(isNetworkBookingAgent==true || networkAgent)}">
			<td width="70"><div class="link-up" id="linkup_destinationAgentExSO" onclick="return checkAgentAlreadyLinkUp('linkup_destinationAgentExSO');">&nbsp;</div></td>
			</c:if>
			<c:if test="${(isNetworkBookingAgent==false && !networkAgent)}">
			<td width="70px"></td>
			</c:if>
			<td style="width:1px">&nbsp;</td>
			<td align="right" class="listwhitetext" >Ext&nbsp;SO&nbsp;#&nbsp;</td>				
			</tr>
			</table>
			</td>
			<td colspan="3"><s:textfield name="trackingStatus.destinationAgentExSO"  cssClass="input-textUpper shiftExt" cssStyle="width:215px;margin-left:2px;" readonly="true" tabindex=""/></td>
			</c:when>
			<c:otherwise>
			<td align="left" colspan="4"></td>
			</c:otherwise>
				</c:choose>			
			<c:if test="${not empty serviceOrder.id}">
				<c:choose>
					<c:when test="${countDestinAgentNotes == '0' || countDestinAgentNotes == '' || countDestinAgentNotes == null}">
						<td  align="right" width="50"><img id="countDestinAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DestinAgentNotes&imageId=countDestinAgentNotesImage&fieldId=countDestinAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.destinationAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DestinAgentNotes&imageId=countDestinAgentNotesImage&fieldId=countDestinAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.destinationAgent',800,600);" ></a></td>
					</c:when>
					<c:otherwise>
						<td  align="right" width="50"><img id="countDestinAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DestinAgentNotes&imageId=countDestinAgentNotesImage&fieldId=countDestinAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.destinationAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=DestinAgentNotes&imageId=countDestinAgentNotesImage&fieldId=countDestinAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.destinationAgent',800,600);" ></a></td>
					</c:otherwise>
				</c:choose>
			</c:if>			
	</tr>		
			<tr height="10px"></tr>
			<configByCorp:fieldVisibility componentId="trackingStatus.originSubAgentCode">
			<tr>
				<td align="right" class="listwhitetext">Sub&nbsp;Origin&nbsp;Agent</td>
				<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
    			<tbody>
     			 <tr>
				<td><s:textfield cssClass="input-text" id="trackingOriginSubAgentCodeId" key="trackingStatus.originSubAgentCode" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special');resetContactEmail('SOA');checkVendorNameSub();showPartnerAlert('onload',this.value,'hidTSOriSubAgent');chkIsOSubRedSky();changeStatus();" onkeypress="return disableEnterKey(event);okValidateListFromSubOaAg();" onblur="showAddressImageSub1();showSubOriginAgentEmailImage();" onfocus="showPartnerAlert('onload',this.value,'hidTSOriSubAgent');getEmailFromPortalUserSubOa();" tabindex="" /></td>
				<c:if test="${clientdirectiveWinOpen=='Y'}">
				<td><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromSubOaAg();javascript:winOpenOriginSubNew();changeStatus(); document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
				</c:if>
				<c:if test="${clientdirectiveWinOpen!='Y'}">
				<td><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromSubOaAg();javascript:winOpenOriginSub();changeStatus(); document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].focus();" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
				</c:if>
				<c:if test="${clientdirectiveWinOpen=='Y'}">			
				<td>
				<c:if test="${agentSearchValidation}">
				<s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingOriginSubAgentNameId" key="trackingStatus.originSubAgent" onkeyup="findPartnerDetailsNew('trackingOriginSubAgentNameId','trackingOriginSubAgentCodeId','trackingOriginSubAgentNameDivId',' and (isAgent=true)','',event);" onchange="resetContactEmail('SOA');checkVendorNameSub();showPartnerAlert('onload',this.value,'hidTSOriSubAgent');chkIsOSubRedSky();findPartnerDetailsByName('trackingOriginSubAgentCodeId','trackingOriginSubAgentNameId');" onblur="showSubOriginAgentEmailImage()" required="true" tabindex="" />
				</c:if>
					<c:if test="${!agentSearchValidation}">
				<s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingOriginSubAgentNameId" key="trackingStatus.originSubAgent" onkeyup="findPartnerDetailsNew('trackingOriginSubAgentNameId','trackingOriginSubAgentCodeId','trackingOriginSubAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);" onchange="resetContactEmail('SOA');checkVendorNameSub();showPartnerAlert('onload',this.value,'hidTSOriSubAgent');chkIsOSubRedSky();findPartnerDetailsByName('trackingOriginSubAgentCodeId','trackingOriginSubAgentNameId');" onblur="showSubOriginAgentEmailImage()" required="true" tabindex="" />
				</c:if>
				<div id="trackingOriginSubAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</c:if>
				<c:if test="${clientdirectiveWinOpen!='Y'}">			
				<td>
				<c:if test="${agentSearchValidation}">
				<s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingOriginSubAgentNameId" key="trackingStatus.originSubAgent" onkeyup="findPartnerDetails('trackingOriginSubAgentNameId','trackingOriginSubAgentCodeId','trackingOriginSubAgentNameDivId',' and (isAgent=true)','',event);" onchange="resetContactEmail('SOA');checkVendorNameSub();showPartnerAlert('onload',this.value,'hidTSOriSubAgent');chkIsOSubRedSky();findPartnerDetailsByName('trackingOriginSubAgentCodeId','trackingOriginSubAgentNameId');" onblur="showSubOriginAgentEmailImage()" required="true" tabindex="" />
				</c:if>
				<c:if test="${!agentSearchValidation}">
				<s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingOriginSubAgentNameId" key="trackingStatus.originSubAgent" onkeyup="findPartnerDetails('trackingOriginSubAgentNameId','trackingOriginSubAgentCodeId','trackingOriginSubAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true)','',event);" onchange="resetContactEmail('SOA');checkVendorNameSub();showPartnerAlert('onload',this.value,'hidTSOriSubAgent');chkIsOSubRedSky();findPartnerDetailsByName('trackingOriginSubAgentCodeId','trackingOriginSubAgentNameId');" onblur="showSubOriginAgentEmailImage()" required="true" tabindex="" />
				</c:if>
				<div id="trackingOriginSubAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</c:if>
				<td align="left" valign="top">
					<div id="hidImageSub1">
						<img align="left"  class="openpopup" width="17" height="20" onclick="findAgent(this,'SOA');"  src="<c:url value='/images/address2.png'/>" />
					</div>
				</td>
				<td>	
					<div id="hidTSOriSubAgent" style="vertical-align:middle;"><a><img id="navigation9" class="openpopup" onclick="getPartnerAlert(document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value,'hidTSOriSubAgent');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
				</td>
				<td align="left">
				<div style="float:left;width:52px;">
					<div id="hidsubORUC" style="vertical-align:middle;display:none;float:left;"><img style="cursor:default!important;" id="navigation103" class="openpopup"  src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
					<div id="hidsubOAP" style="vertical-align:middle;display:none;float:left;"><img style="cursor:default!important;" id="navigation203" class="openpopup"  src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
					
				</div>
				</td>
				</tr>
				</tbody>
				</table>
				</td>
				<script type="text/javascript">
				setTimeout("showPartnerAlert('onload','${trackingStatus.originSubAgentCode}','hidTSOriSubAgent')", 2000);
					
				</script>
				<td align="right" class="listwhitetext">Sub&nbsp;Destination&nbsp;Agent</td>
				<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
    			<tbody>
     			 <tr>
				<td>
				<s:textfield cssClass="input-text"  key="trackingStatus.destinationSubAgentCode" id="trackingDestinationSubAgentCodeId" cssStyle="width:45px;" maxlength="8" onchange="valid(this,'special');resetContactEmail('SDA');checkVendorNameDestSub();showPartnerAlert('onload',this.value,'hidTSDestinSubAgent');chkIsDSubRedSky();changeStatus();" onkeypress="return disableEnterKey(event);okValidateListFromSubDaAg();" onblur="showAddressImageSub2();showSubDestinationAgentEmailImage();" onfocus="showPartnerAlert('onload',this.value,'hidTSDestinSubAgent');getEmailFromPortalUserSubDa();" tabindex=""/>
				</td>
				<c:if test="${clientdirectiveWinOpen=='Y'}">
				<td width="17"><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromSubDaAg();javascript:winOpenDestSubNew();changeStatus();document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].focus();"  id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
				</c:if>
				<c:if test="${clientdirectiveWinOpen!='Y'}">
				<td width="17"><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromSubDaAg();javascript:winOpenDestSub();changeStatus();document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].focus();"  id="openpopup8.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
				</c:if>
				<c:if test="${clientdirectiveWinOpen=='Y'}">		
				<td align="left">
				<c:if test="${agentSearchValidation}">
				<s:textfield  cssStyle="width:145px"  cssClass="input-text" id="trackingDestinationSubAgentNameId" key="trackingStatus.destinationSubAgent"  required="true"  tabindex=""  onkeyup="findPartnerDetailsNew('trackingDestinationSubAgentNameId','trackingDestinationSubAgentCodeId','trackingDestinationSubAgentNameDivId',' and (isAgent=true)','',event);" onchange="resetContactEmail('SDA');checkVendorNameDestSub();showPartnerAlert('onload',this.value,'hidTSDestinSubAgent');chkIsDSubRedSky();findPartnerDetailsByName('trackingDestinationSubAgentCodeId','trackingDestinationSubAgentNameId');" onblur="showAddressImageSub2();showSubDestinationAgentEmailImage();"/> 
				</c:if>
					<c:if test="${!agentSearchValidation}">
				<s:textfield  cssStyle="width:145px"  cssClass="input-text" id="trackingDestinationSubAgentNameId" key="trackingStatus.destinationSubAgent"  required="true"  tabindex=""  onkeyup="findPartnerDetailsNew('trackingDestinationSubAgentNameId','trackingDestinationSubAgentCodeId','trackingDestinationSubAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);" onchange="resetContactEmail('SDA');checkVendorNameDestSub();showPartnerAlert('onload',this.value,'hidTSDestinSubAgent');chkIsDSubRedSky();findPartnerDetailsByName('trackingDestinationSubAgentCodeId','trackingDestinationSubAgentNameId');" onblur="showAddressImageSub2();showSubDestinationAgentEmailImage();"/> 
				</c:if>
				<div id="trackingDestinationSubAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</c:if>
				<c:if test="${clientdirectiveWinOpen!='Y'}">		
				<td align="left">
				<c:if test="${agentSearchValidation}">
				<s:textfield  cssStyle="width:145px"  cssClass="input-text" id="trackingDestinationSubAgentNameId" key="trackingStatus.destinationSubAgent"  required="true"  tabindex=""  onkeyup="findPartnerDetails('trackingDestinationSubAgentNameId','trackingDestinationSubAgentCodeId','trackingDestinationSubAgentNameDivId',' and (isAgent=true)','',event);" onchange="resetContactEmail('SDA');checkVendorNameDestSub();showPartnerAlert('onload',this.value,'hidTSDestinSubAgent');chkIsDSubRedSky();findPartnerDetailsByName('trackingDestinationSubAgentCodeId','trackingDestinationSubAgentNameId');" onblur="showAddressImageSub2();showSubDestinationAgentEmailImage();"/> 
				</c:if>
				<c:if test="${!agentSearchValidation}">
				<s:textfield  cssStyle="width:145px"  cssClass="input-text" id="trackingDestinationSubAgentNameId" key="trackingStatus.destinationSubAgent"  required="true"  tabindex=""  onkeyup="findPartnerDetails('trackingDestinationSubAgentNameId','trackingDestinationSubAgentCodeId','trackingDestinationSubAgentNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);" onchange="resetContactEmail('SDA');checkVendorNameDestSub();showPartnerAlert('onload',this.value,'hidTSDestinSubAgent');chkIsDSubRedSky();findPartnerDetailsByName('trackingDestinationSubAgentCodeId','trackingDestinationSubAgentNameId');" onblur="showAddressImageSub2();showSubDestinationAgentEmailImage();"/> 
				</c:if>
				<div id="trackingDestinationSubAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
				</td>
				</c:if>
				<td align="left" valign="top">
					<div id="hidImageSub2" >
						<img align="left"  class="openpopup" width="17" height="20" onclick="findAgent(this,'SDA');"  src="<c:url value='/images/address2.png'/>" />
					</div>
					</td>
					<td>
					<div id="hidTSDestinSubAgent" style="vertical-align:middle;"><a><img id="navigation10" class="openpopup" onclick="getPartnerAlert(document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value,'hidTSDestinSubAgent');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
				</td>
				<td align="left">
					<div style="float:left;width:52px;">	
					<div id="hidsubDRUC" style="vertical-align:middle;display:none;float:left;"><img style="cursor:default!important;" id="navigation104" class="openpopup"  src="${pageContext.request.contextPath}/images/rslogo_small.png" alt="RUC" title="RUC" /></div>
					<div id="hidsubDAP" style="vertical-align:middle;display:none;float:left;"><img style="cursor:default!important;" id="navigation204" class="openpopup"  src="${pageContext.request.contextPath}/images/portal_small_img.png" alt="AGENT PORTAL" title="AGENT PORTAL" /></div>
					
				</div>
				</td>
				</tr>
				</tbody>
				</table>
				</td>
				<script type="text/javascript">
				setTimeout("showPartnerAlert('onload','${trackingStatus.destinationSubAgentCode}','hidTSDestinSubAgent')", 3000);
				</script>
			</tr>
			<tr>
				<td align="right" class="listwhitetext" >Sub Origin Agent Contact&nbsp;</td>
				
				<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">    			
     			 <tr>
				<td><s:textfield  cssClass="input-text" key="trackingStatus.subOriginAgentContact" onchange="resetEmail('SOA',this.value);" cssStyle="width:215px;" tabindex="" /></td>
				<td width="" valign="middle">				
					<div id="hidImageSubPlus1" style="float:left;">
						<img align="left"  class="openpopup" width="17" height="20" onclick="getContact('SOA',document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value);"  src="<c:url value='/images/plus-small.png'/>" />
					</div>&nbsp;
					<div id="hidSubOrigActiveAP" style="vertical-align:middle;display:none;float:left;"><img id="navigation2011" style="cursor:default!important;" class="openpopup"  src="${pageContext.request.contextPath}/images/agent-user.png" alt="ACTIVE AGENT PORTAL" title="ACTIVE AGENT PORTAL" /></div>
				</td>
				</tr>
				</table>
				</td>
				<script type="text/javascript">
					 showAddressImageSub1(); 
          		</script> 
				<td align="right" class="listwhitetext" >Sub&nbsp;Dest&nbsp;Agent&nbsp;Contact&nbsp;</td>
				<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
    			<tbody>
     			 <tr>
				<td ><s:textfield  cssClass="input-text" key="trackingStatus.subDestinationAgentAgentContact" onchange="resetEmail('SDA',this.value);" cssStyle="width:215px;" tabindex="" /></td>
				<td width="" valign="middle">				
					<div id="hidImageSubPlus2" style="float:left;">
						<img align="left"  class="openpopup" width="17" height="20" onclick="getContact('SDA',document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value);"  src="<c:url value='/images/plus-small.png'/>" />
					</div>
					<div id="hidSubDestinActiveAP" style="vertical-align:middle;display:none;float:left;"><img id="navigation2011" style="cursor:default!important;" class="openpopup"  src="${pageContext.request.contextPath}/images/agent-user.png" alt="ACTIVE AGENT PORTAL" title="ACTIVE AGENT PORTAL" /></div>
				</td>
				</tr>
				</tbody>
				</table>
				</td>
				<script type="text/javascript">
					 showAddressImageSub2(); 
          		</script> 
			</tr>
			<tr>
				<td align="right" class="listwhitetext" >Sub Origin Agent Email&nbsp;</td>
				<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
     			 <tr>
				<td><s:textfield  cssClass="input-text" key="trackingStatus.subOriginAgentEmail"  id="trackingStatus.subOriginAgentEmail" cssStyle="width:215px;" onchange="checkEmail('trackingStatus.subOriginAgentEmail');" onblur="showSubOriginAgentEmailImage();" tabindex="" /></td>
				<td><div id="hidSubOriginAgentEmailImage" style="vertical-align:middle; "><img class="openpopup" onclick="sendEmail(document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value)" id="email4" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.subOriginAgentEmail}"/></td>
				<configByCorp:fieldVisibility componentId="component.field.document.documentbundleview"><td align="left"><a><img onclick="checkFromDocumentBundle('trackingStatus.subOriginAgentEmail',this,'Sub Origin Agent');" style="margin: 0px 0px 0px 1px;" src="${pageContext.request.contextPath}/images/email-attach-16.png"/></a></td></configByCorp:fieldVisibility>
				</tr>
				</table>
				</td>
				<td align="right" class="listwhitetext" >Sub Dest Agent Email&nbsp;</td>
				<td colspan="3">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
    			<tbody>
     			 <tr>
				<td><s:textfield  cssClass="input-text" key="trackingStatus.subDestinationAgentEmail" id="trackingStatus.subDestinationAgentEmail" cssStyle="width:215px;" onchange="checkEmail('trackingStatus.subDestinationAgentEmail');" onblur="showSubDestinationAgentEmailImage();" tabindex="" /></td>
				<td><div id="hidSubDestinationAgentEmailImage" style="vertical-align:middle; "><img class="openpopup" onclick="sendEmail(document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value)" id="email5" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.subDestinationAgentEmail}"/></td>
				<configByCorp:fieldVisibility componentId="component.field.document.documentbundleview"><td align="left"><a><img onclick="checkFromDocumentBundle('trackingStatus.subDestinationAgentEmail',this,'Sub Destination Agent');" style="margin: 0px 0px 0px 1px;" src="${pageContext.request.contextPath}/images/email-attach-16.png"/></a></td></configByCorp:fieldVisibility>
				</tr>
				</tbody>
				</table>
				</td>
			</tr>			
			<tr>
			<td align="right" class="listwhitetext" >Contact&nbsp;#&nbsp;</td>
					<td colspan="4"><s:textfield  cssClass="input-text" key="trackingStatus.subOriginAgentPhoneNumber" maxlength="30" cssStyle="width:215px;margin-left:1px;"  onchange="resetEmail('SOA',this.value);"  tabindex="" /></td>
					<td align="right" class="listwhitetext" >Contact&nbsp;#&nbsp;</td>
					<td colspan="4">
					<table cellpadding="1" cellspacing="1" style="margin:0px;padding:0px;">
					<tr>
					<td><s:textfield  cssClass="input-text" key="trackingStatus.subDestinationAgentPhoneNumber" maxlength="30" cssStyle="width:214px; "  onchange="resetEmail('SDA',this.value);"  tabindex="" /></td>
					</tr>
					</table>
					</td>			</tr>		  
	 	<tr>
	<td colspan="4">
	<c:if test="${(isNetworkBookingAgent==true || isNetworkOriginAgent==true || networkAgent || (trackingStatus.originSubAgentExSO!=null && trackingStatus.originSubAgentExSO!=''))}">
	<table cellpadding="1" cellspacing="0" style="margin:0px; padding:0px;width:389px;text-align:right;">
			<tr>
			<td width="10" style="min-width:17px;">&nbsp;</td>
			<td width="70"><div class="link-up" id="linkup_originSubAgentExSO" onclick="return  checkAgentAlreadyLinkUp('linkup_originSubAgentExSO')" >&nbsp;</div></td>
			
			<td align="right" class="listwhitetext" width="60" >Ext&nbsp;SO&nbsp;#&nbsp;</td>
			<td align="left"><s:textfield name="trackingStatus.originSubAgentExSO"  cssClass="input-textUpper shiftExtRight" cssStyle="width:215px;margin-left:2px;" readonly="true" tabindex=""/></td>
			</tr>
			</table>
	</c:if>
	</td>	
	<c:if test="${not empty serviceOrder.id}">
				<c:choose>
					<c:when test="${countSubOriginAgentNotes == '0' || countSubOriginAgentNotes == '' || countSubOriginAgentNotes == null}">
						<td  align="right" width="50"><img id="countSubOriginAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SubOriginAgentNotes&imageId=countSubOriginAgentNotesImage&fieldId=countSubOriginAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.subOriginAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=SubOriginAgentNotes&imageId=countSubOriginAgentNotesImage&fieldId=countSubOriginAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.subOriginAgent',800,600);" ></a></td>
					</c:when>
					<c:otherwise>
						<td  align="right" width="50"><img id="countSubOriginAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SubOriginAgentNotes&imageId=countSubOriginAgentNotesImage&fieldId=countSubOriginAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.subOriginAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=SubOriginAgentNotes&imageId=countSubOriginAgentNotesImage&fieldId=countSubOriginAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.subOriginAgent',800,600);" ></a></td>
					</c:otherwise>
				</c:choose>
			</c:if>
			
			<c:choose>
			<c:when test="${(isNetworkBookingAgent==true || isNetworkDestinationAgent==true || networkAgent || (trackingStatus.destinationSubAgentExSO!=null && trackingStatus.destinationSubAgentExSO!=''))}">
			<td align="right">
			<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;">
			<tr>
			<td width="40"><div class="link-up" id="linkup_destinationSubAgentExSO" onclick="return  checkAgentAlreadyLinkUp('linkup_destinationSubAgentExSO')" >&nbsp;</div></td>
			<td style="width:1px;">&nbsp;</td>
			<td align="right" class="listwhitetext" >Ext&nbsp;SO&nbsp;#&nbsp;</td>
			</tr>
			</table>
			</td>
			<td align="left" colspan="3"><s:textfield name="trackingStatus.destinationSubAgentExSO"  cssClass="input-textUpper shiftExt" cssStyle="width:215px;margin-left:1px;" readonly="true" tabindex=""/></td>	
			
			</c:when>
			<c:otherwise>
			<td align="left" colspan="4"></td>
			</c:otherwise>
			</c:choose>
						
			<c:if test="${not empty serviceOrder.id}">
				<c:choose>
					<c:when test="${countSubDestinAgentNotes == '0' || countSubDestinAgentNotes == '' || countSubDestinAgentNotes == null}">
						<td  align="right" width="50"><img id="countSubDestinAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SubDestinAgentNotes&imageId=countSubDestinAgentNotesImage&fieldId=countSubDestinAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.subDestinationAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=SubDestinAgentNotes&imageId=countSubDestinAgentNotesImage&fieldId=countSubDestinAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.subDestinationAgent',800,600);" ></a></td>
					</c:when>
					<c:otherwise>
						<td  align="right" width="50"><img id="countSubDestinAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=SubDestinAgentNotes&imageId=countSubDestinAgentNotesImage&fieldId=countSubDestinAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.subDestinationAgent',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id}&notesId=${serviceOrder.shipNumber}&noteFor=ServiceOrder&subType=SubDestinAgentNotes&imageId=countSubDestinAgentNotesImage&fieldId=countSubDestinAgentNotes&decorator=popup&popup=true&NetwokAccess=notes.subDestinationAgent',800,600);" ></a></td>
					</c:otherwise>
				</c:choose>
			</c:if>	
	</tr>		
	</configByCorp:fieldVisibility>	
 			<tr height="10px"></tr>
			<tr>
			<td align="right" class="listwhitetext" style="padding-top:13px;" ><fmt:message key='trackingStatus.brokerName'/></td>
	<td colspan="4">
				<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
    			<tbody>
     			 <tr>
<td><s:textfield cssClass="input-text" id="trackingBrokerCodeId" key="trackingStatus.brokerCode" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('BC');changeStatus();checkBroker();showPartnerAlert('onload',this.value,'hidTSBroker')" onkeypress="return disableEnterKey(event);okValidateListFromBroker();" tabindex="" onblur="showAddressImage2();showBrokerEmailImage();" onfocus=""/></td>
<td><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromBroker();javascript:openWindow('venderPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.brokerEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.brokerName&fld_code=trackingStatus.brokerCode');document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].focus();changeStatus()" id="openpopup9.img" src="<c:url value='/images/open-popup.gif'/>" /></td>		
<td><s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingBrokerNameId"  key="trackingStatus.brokerName" onchange="resetContactEmail('BC');showPartnerAlert('onload',this.value,'hidTSBroker');findPartnerDetailsByName('trackingBrokerCodeId','trackingBrokerNameId');" onkeyup="findPartnerDetails('trackingBrokerNameId','trackingBrokerCodeId','trackingBrokerNameDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);" required="true" onblur="showAddressImage2();showBrokerEmailImage();" tabindex=""/>
<div id="trackingBrokerNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
</td>&nbsp;
<td align="left" valign="top">
	<div id="hidImage2">
			<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'BK');" src="<c:url value='/images/address2.png'/>" />
	</div>
	</td>
	<td>
	<div id="hidTSBroker" style="vertical-align:middle;"><a><img id="navigation11" class="openpopup" onclick="getPartnerAlert(document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value,'hidTSBroker');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
</td>
</tr>
</tbody>
</table>
</td>
<script type="text/javascript">
	setTimeout("showPartnerAlert('onload','${trackingStatus.brokerCode}','hidTSBroker')", 4000);
</script>	
<configByCorp:fieldVisibility componentId="component.section.trackingStatus.forwarder">
<td align="right" class="listwhitetext" >Forwarder</td>
<td colspan="9"><table style="margin:0px;" cellpadding="1" cellspacing="0"><tr>
<td><s:textfield cssClass="input-text" id="trackingforwarderCodeId" key="trackingStatus.forwarderCode" cssStyle="width:45px;" maxlength="10" onchange="valid(this,'special');resetContactEmail('FC');changeStatus();checkForwarderName();checkForwarder();showPartnerAlert('onload',this.value,'hidTSForwarder');" onkeypress="return disableEnterKey(event);okValidateListFromForwarder();" tabindex="" onblur="showAddressForwarderCodeImage();showForwarderEmailImage();checkForwarder();" onfocus=""/></td>
<td><img align="left" class="openpopup" width="17" height="20" onclick="okValidateListFromForwarder();javascript:openWindow('venderPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=trackingStatus.forwarderEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=trackingStatus.forwarder&fld_code=trackingStatus.forwarderCode');document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].focus();changeStatus()" id="openpopup10.img" src="<c:url value='/images/open-popup.gif'/>" /></td>		

<td colspan="3">
<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
 <tbody>
<tr>
<td><s:textfield cssStyle="width:145px"  cssClass="input-text" id="trackingForwarderId"  key="trackingStatus.forwarder"  required="true" onchange="resetContactEmail('FC');showPartnerAlert('onload',this.value,'hidTSForwarder');findPartnerDetailsByName('trackingforwarderCodeId','trackingForwarderId');" onkeyup="findPartnerDetails('trackingForwarderId','trackingforwarderCodeId','trackingForwarderDivId',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','',event);" onblur="showAddressForwarderCodeImage();checkForwarder();" tabindex=""/>
<div id="trackingForwarderDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
</td>
<td align="left" width="">
	<div id="hidImageforwarderCode" style="vertical-align:top;margin: 0px; padding: 0px; ">
	<img align="top" class="openpopup" width="17" height="20" onclick="findAgent(this,'FWD');" src="<c:url value='/images/address2.png'/>" />
	</div>
	</td>
	<td align="left">
	<div id="hidTSForwarder" style="vertical-align:middle;"><a><img id="navigation12" class="openpopup" onclick="getPartnerAlert(document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value,'hidTSForwarder');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr></table></td>
<script type="text/javascript">
	setTimeout("showPartnerAlert('onload','${trackingStatus.forwarderCode}','hidTSForwarder')", 5000);
</script>
</configByCorp:fieldVisibility>
</tr>
<tr>			<td align="right" class="listwhitetext" >Broker Contact&nbsp;</td>
			<td colspan="4">
			<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield  cssClass="input-text" key="trackingStatus.brokerContact" onchange="resetEmail('BC',this.value);" cssStyle="width:215px;" tabindex="" /></td>
			<td style="margin: 0px; padding: 0px">
			<div id="hidImagePlus2" style="vertical-align:top;margin: 0px ; padding: 0px;">
				<img align="top" class="openpopup" width="17" height="20" onclick="getContact('BC',document.forms['trackingStatusForm'].elements['trackingStatus.brokerCode'].value);" src="<c:url value='/images/plus-small.png'/>" />&nbsp;
			</div>
			</td>
			</tr>
			</table>
			</td>
			<script type="text/javascript">
				showAddressImage2(); 
			</script>
	<configByCorp:fieldVisibility componentId="component.section.trackingStatus.forwarder">
	<td align="right" class="listwhitetext" >Forwarder Contact&nbsp;</td>
			<td colspan="3">
			<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield  cssClass="input-text" key="trackingStatus.forwarderContact" onchange="resetEmail('FC',this.value);" cssStyle="width:215px;" tabindex="" /></td>
			<td style="margin: 0px; padding: 0px">
				<div id="hidImageforwarderCodePlus" style="vertical-align:top;margin: 0px; padding: 0px; ">
					<img align="top" class="openpopup" width="17" height="20" onclick="getContact('FC',document.forms['trackingStatusForm'].elements['trackingStatus.forwarderCode'].value);" src="<c:url value='/images/plus-small.png'/>" />&nbsp;
				</div>
			</td>
			</tr>
			</table>
			</td>
			<script type="text/javascript">
				showAddressForwarderCodeImage(); 
			</script>
	</configByCorp:fieldVisibility>
</tr>
<tr>			
			<td align="right" class="listwhitetext" >Broker Email&nbsp;</td>
			<td colspan="4">
			<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield  cssClass="input-text" key="trackingStatus.brokerEmail" cssStyle="width:215px;" onchange="checkEmail('trackingStatus.brokerEmail');" onblur="showBrokerEmailImage();" tabindex="" /></td>
			<td width="23"><div id="hidBrokerEmailImage" style="vertical-align:middle; "><img class="openpopup" onclick="sendEmail(document.forms['trackingStatusForm'].elements['trackingStatus.brokerEmail'].value)" id="email6" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.brokerEmail}"/></td>
			<configByCorp:fieldVisibility componentId="component.field.document.documentbundleview"><td align="left"><a><img onclick="checkFromDocumentBundle('trackingStatus.brokerEmail',this,'Broker Agent');" style="margin: 0px 0px 0px 1px;" src="${pageContext.request.contextPath}/images/email-attach-16.png"/></a></td></configByCorp:fieldVisibility>
			<td colspan="4">
			<c:if test="${not empty serviceOrder.id}">
			<c:choose>
			<c:when test="${countBrokerAgentNotes == '0' || countBrokerAgentNotes == '' || countBrokerAgentNotes == null}">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<td  align="right" width="50"><img id="countBrokerAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BrokerAgent&imageId=countBrokerAgentNotesImage&fieldId=countBrokerAgentNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BrokerAgent&imageId=countBrokerAgentNotesImage&fieldId=countBrokerAgentNotes&decorator=popup&popup=true',800,600);" ></a></td>
			</c:when>
			<c:otherwise>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<td  align="right" width="50"><img id="countBrokerAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BrokerAgent&imageId=countBrokerAgentNotesImage&fieldId=countBrokerAgentNotesImage&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=BrokerAgent&imageId=countBrokerAgentNotesImage&fieldId=countBrokerAgentNotes&decorator=popup&popup=true',800,600);" ></a></td>
			</c:otherwise>
			</c:choose>
			</c:if>
			</td>
			</tr>
			</table>
			</td>
		    <configByCorp:fieldVisibility componentId="component.section.trackingStatus.forwarder">
			<td align="right" class="listwhitetext" >Forwarder Email&nbsp;</td>
			<td colspan="4">
			<table cellspacing="0" cellpadding="1" style="margin:0px; padding:0px;">
			<tr>
			<td><s:textfield  cssClass="input-text" key="trackingStatus.forwarderEmail" cssStyle="width:215px;" onchange="checkEmail('trackingStatus.forwarderEmail');" onblur="showForwarderEmailImage();" tabindex="" /></td>
			<td width="27"><div id="hidForwarderEmailImage" style="vertical-align:middle; "><img class="openpopup" onclick="sendEmail(document.forms['trackingStatusForm'].elements['trackingStatus.forwarderEmail'].value)" id="email7" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${trackingStatus.forwarderEmail}"/></td>
			<configByCorp:fieldVisibility componentId="component.field.document.documentbundleview"><td align="left"><a><img onclick="checkFromDocumentBundle('trackingStatus.forwarderEmail',this,'Forwarder Agent');" style="margin: 0px 0px 0px 1px;" src="${pageContext.request.contextPath}/images/email-attach-16.png"/></a></td></configByCorp:fieldVisibility>
			<td colspan="4">
			<c:if test="${not empty serviceOrder.id}">
			<c:choose>
			<c:when test="${countForwarderAgentNotes == '0' || countForwarderAgentNotes == '' || countForwarderAgentNotes == null}">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<td  align="right" width="50"><img id="countForwarderAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ForwarderAgent&imageId=countForwarderAgentNotesImage&fieldId=countForwarderAgentNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ForwarderAgent&imageId=countForwarderAgentNotesImage&fieldId=countForwarderAgentNotes&decorator=popup&popup=true',800,600);"></a></td>
			</c:when>
			<c:otherwise>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<td  align="right" width="50"><img id="countForwarderAgentNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ForwarderAgent&imageId=countForwarderAgentNotesImage&fieldId=countForwarderAgentNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('noteList.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ForwarderAgent&imageId=countForwarderAgentNotesImage&fieldId=countForwarderAgentNotes&decorator=popup&popup=true',800,600);"></a></td>
			</c:otherwise>
			</c:choose>
			</c:if></td>
			</tr>
			</table>
			</td>
		</configByCorp:fieldVisibility>	
			</tr> 
<tr height="5px"></tr>			 
 <tr>
<td align="right" class="listwhitetext">Omni&nbsp;Report</td>
<c:if test="${not empty trackingStatus.omniReport}">
<s:text id="trackingStatusOmniReportFormattedValue" name="${FormDateValue1}"><s:param name="value" value="trackingStatus.omniReport"/></s:text>
<td colspan="4"><s:textfield cssClass="input-text" id="omniReport" name="trackingStatus.omniReport" value="%{trackingStatusOmniReportFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="onlyDel(event,this)" tabindex="" /> <img id="omniReport-trigger" style="vertical-align:bottom" textelementname="omniReport" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty trackingStatus.omniReport}">
<td colspan="4"><s:textfield cssClass="input-text" id="omniReport" name="trackingStatus.omniReport" required="true" cssStyle="width:65px" maxlength="11"  onkeydown="onlyDel(event,this)"  tabindex=""/>  <img id="omniReport-trigger" style="vertical-align:bottom" textelementname="omniReport" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td></td>
<c:choose>
<c:when test="${serviceOrdersSize == 1 || not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
<td align="left" colspan="4">
<a class="update-btn-dis" onclick="return false">Update Agent Roles Across S\O's </a></td>
</c:when>
<c:otherwise>
<td align="left" colspan="4">
<a class="update-btn" onclick="updateAllAgentDetails();">Update Agent Roles Across S\O's </a></td>
</c:otherwise>
</c:choose>
</tr>	
</tbody>
</table>
</div>
<script type="text/javascript">
var http = getHTTPObjectst();
		var http7 = getHTTPObjectst();
		var http345 = getHTTPObjectst();
		var http456 = getHTTPObjectst();
		var http567 = getHTTPObjectst();
		var http678 = getHTTPObjectst();
		var http789 = getHTTPObjectst();
		var http891 = getHTTPObjectst();
		var htpp912 = getHTTPObjectst();
		
		var http187 = getHTTPObjectst();
		var http190 = getHTTPObjectst();
		var http156 = getHTTPObjectst();
		var http195 = getHTTPObjectst();
		var http187 = getHTTPObjectst();
		var http176 = getHTTPObjectst();
		var http165 = getHTTPObjectst();

		function getHTTPObjectst() {
	    var xmlhttp;
	    if(window.XMLHttpRequest) {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)  {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)  {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }   } 
	    return xmlhttp;
	} 
function openDiv(){
	try{
		var ff = '${isRedSky}';
		var ff1 = '${isAgentPortal}';
		//alert(ff)
		//alert(ff1)
		var olinkButton=document.getElementById("linkup_originAgentExSO");	
		var dlinkButton=document.getElementById("linkup_destinationAgentExSO");
		var oslinkButton=document.getElementById("linkup_originSubAgentExSO");
		var dslinkButton=document.getElementById("linkup_destinationSubAgentExSO");		
		if(ff=='RUC'){
		
			if(olinkButton!=undefined && olinkButton!=null)	{
			document.getElementById("linkup_originAgentExSO").style.display="block";
			}
			document.getElementById("hidRUC").style.display="block";
			document.getElementById("hidAP").style.display="none";
			if(document.getElementById("hidActiveAP")!=null)	{
				document.getElementById("hidActiveAP").style.display="none"
			}		
		}else if(ff=="AP"){
			//alert("AP")
			if(olinkButton!=undefined && olinkButton!=null){
			document.getElementById("linkup_originAgentExSO").style.display="none";
			}	
			document.getElementById("hidAP").style.display="block";
			document.getElementById("hidRUC").style.display="none";
			 if(ff1!=null &&  ff1=='AG'){
				 //alert("aggggggg")
			document.getElementById("hidActiveAP").style.display="block";
			}else if((ff1==null || ff1=='') && document.getElementById("hidActiveAP")!=null){
				//alert("false ")
				document.getElementById("hidActiveAP").style.display="none";
			}   
			    
		     
	}else if(ff==null || ff==""){
		if(olinkButton!=undefined && olinkButton!=null){
		document.getElementById("linkup_originAgentExSO").style.display="none";	
		}
		document.getElementById("hidAP").style.display="none";
		document.getElementById("hidRUC").style.display="none";	
		if((ff1==null || ff1=='') && document.getElementById("hidActiveAP")!=null){
			//alert("false ")
			document.getElementById("hidActiveAP").style.display="none";
		}		
	}	
	}catch(e){} 

	try{
		var ff = '${isRedSkyDestinAg}';	
		var ff1 = '${isDestinAgentPortal}';
		if(ff=='RUC'){			
			if(dlinkButton!=undefined && dlinkButton!=null ){		
			document.getElementById("linkup_destinationAgentExSO").style.display="block";
			}
			
			document.getElementById("hidDestRUC").style.display="block";
			document.getElementById("hidDestAP").style.display="none";	
			if(document.getElementById("hidDestinActiveAP")!=null)	{
				document.getElementById("hidDestinActiveAP").style.display="none"
			}	
		}else if(ff=="AP"){
			//alert("AP")
			if(dlinkButton!=undefined && dlinkButton!=null){
			document.getElementById("linkup_destinationAgentExSO").style.display="none";
			}	
			document.getElementById("hidDestAP").style.display="block";
			document.getElementById("hidDestRUC").style.display="none";
			 if(ff1!=null &&  ff1=='AG'){
					document.getElementById("hidDestinActiveAP").style.display="block";
				}else if((ff1==null || ff1=='') && (document.getElementById("hidDestinActiveAP")!=null)){
					document.getElementById("hidDestinActiveAP").style.display="none";
				}
	}else if(ff==null || ff==""){
		if(dlinkButton!=undefined && dlinkButton!=null){
		document.getElementById("linkup_destinationAgentExSO").style.display="none";	
		}
		document.getElementById("hidDestAP").style.display="none";
		document.getElementById("hidDestRUC").style.display="none";
		if((ff1==null || ff1=='') && (document.getElementById("hidDestinActiveAP")!=null)){
			document.getElementById("hidDestinActiveAP").style.display="none";
		}	
	}
	}catch(e){} 

	try{
		var ff = '${isRedSkySubOrigAg}';
		var ff1 = '${isSubOrigAgentPortal}';	
		if(ff=='RUC'){					
			if(oslinkButton!=undefined && oslinkButton!=null){
			document.getElementById("linkup_originSubAgentExSO").style.display="block";
			}
			document.getElementById("hidsubORUC").style.display="block";
			document.getElementById("hidsubOAP").style.display="none";	
			if(document.getElementById("hidSubOrigActiveAP")!=null)	{
				document.getElementById("hidSubOrigActiveAP").style.display="none"
			}	
		}else if(ff=="AP"){			
			if(oslinkButton!=undefined && oslinkButton!=null){
			document.getElementById("linkup_originSubAgentExSO").style.display="none";	
			}
			document.getElementById("hidsubOAP").style.display="block";
			document.getElementById("hidsubORUC").style.display="none";
			 if(ff1!=null &&  ff1=='AG'){
					document.getElementById("hidSubOrigActiveAP").style.display="block";
				}else if((ff1==null || ff1=='') && (document.getElementById("hidSubOrigActiveAP")!=null)){
					document.getElementById("hidSubOrigActiveAP").style.display="none";
				}
	}else if(ff==null || ff==""){
		if(oslinkButton!=undefined && oslinkButton!=null){
		document.getElementById("linkup_originSubAgentExSO").style.display="none";
		}	
		document.getElementById("hidsubOAP").style.display="none";
		document.getElementById("hidsubORUC").style.display="none";	
		if((ff1==null || ff1=='') && (document.getElementById("hidSubOrigActiveAP")!=null)){
			document.getElementById("hidSubOrigActiveAP").style.display="none";
		}
	}
	}catch(e){}

	try{
		var ff = '${isRedSkySubDestinAg}';
		var ff1 = '${isSubDestinAgentPortal}';	
		if(ff=='RUC'){				
			if(dslinkButton!=undefined && dslinkButton!=null){	
			document.getElementById("linkup_destinationSubAgentExSO").style.display="block";
			}
			document.getElementById("hidsubDRUC").style.display="block";
			document.getElementById("hidsubDAP").style.display="none";	
			if(document.getElementById("hidSubDestinActiveAP")!=null)	{
				document.getElementById("hidSubDestinActiveAP").style.display="none"
			}	
		}else if(ff=="AP"){			
			if(dslinkButton!=undefined && dslinkButton!=null){	
			document.getElementById("linkup_destinationSubAgentExSO").style.display="none";	
			}
			document.getElementById("hidsubDAP").style.display="block";
			document.getElementById("hidsubDRUC").style.display="none";
			 if(ff1!=null &&  ff1=='AG'){
					document.getElementById("hidSubDestinActiveAP").style.display="block";
				}else if((ff1==null || ff1=='') && document.getElementById("hidSubDestinActiveAP")!=null){
					document.getElementById("hidSubDestinActiveAP").style.display="none";
				}
	}else if(ff==null || ff==""){
		if(dslinkButton!=undefined && dslinkButton!=null)	{
		document.getElementById("linkup_destinationSubAgentExSO").style.display="none";
		}
		document.getElementById("hidsubDRUC").style.display="none";
		document.getElementById("hidsubDAP").style.display="none";
		if((ff1==null || ff1=='') && document.getElementById("hidSubDestinActiveAP")!=null){
			document.getElementById("hidSubDestinActiveAP").style.display="none";
		}		
	}
	}catch(e){}
	
}
function chkIsRedSky(){
	var tt=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
	if(tt==''){
		document.getElementById("hidRUC").style.display="none";
		document.getElementById("hidAP").style.display="none";
		document.getElementById("linkup_originAgentExSO").style.display="none";	
	}else{
	var url = "chkPartnerRUC.html?ajax=1&decorator=simple&popup=true&partCode="+tt;
	http234.open("GET", url, true);
	http234.onreadystatechange = function(){chkPartnerRUC()};
	http234.send(null); 
	}
}
function chkPartnerRUC(buttonType){
	var olinkButton=document.getElementById("linkup_originAgentExSO");
	if (http234.readyState == 4){
		var results = http234.responseText
        results = results.trim();	
		if(results==''){
			if(olinkButton!=undefined && olinkButton!=null){
			document.getElementById("linkup_originAgentExSO").style.display="none";	
			}
			document.getElementById("hidAP").style.display="none";
			document.getElementById("hidRUC").style.display="none";
			if(document.getElementById("hidActiveAP")!=null)	{
				document.getElementById("hidActiveAP").style.display="none"
			}
		}else{			
			if(results=='RUC'){					
				if(olinkButton!=undefined && olinkButton!=null){	
				document.getElementById("linkup_originAgentExSO").style.display="block";
				}
				document.getElementById("hidRUC").style.display="block";
				document.getElementById("hidAP").style.display="none";	
				if(document.getElementById("hidActiveAP")!=null)	{
					document.getElementById("hidActiveAP").style.display="none"
				}	
			}else if(results=="AP"){				
				if(olinkButton!=undefined && olinkButton!=null){
				document.getElementById("linkup_originAgentExSO").style.display="none";			
				var tt=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
				var url = "chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+tt;
	             http345.open("GET", url, true);
	              http345.onreadystatechange = function(){chkAgentPartnerRUC()};
	                 http345.send(null); 					
				}
				document.getElementById("hidAP").style.display="block";
				document.getElementById("hidRUC").style.display="none";
		}
			} 
		} 
	}

function chkAgentPartnerRUC(){
	if (htt345.readyState == 4){
		var results = http345.responseText
        results = results.trim();
		if(results=='AG'){			
		}
	}
}
function chkIsDestinRedSky(){
	var tt=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
	if(tt==''){
		document.getElementById("hidDestRUC").style.display="none";
		document.getElementById("hidDestAP").style.display="none";
		document.getElementById("linkup_destinationAgentExSO").style.display="none";	
	}else{	
	var url = "chkPartnerRUC.html?ajax=1&decorator=simple&popup=true&partCode="+tt;
	http456.open("GET", url, true);
	http456.onreadystatechange = function(){chkPartnerDestinRUC()};
	http456.send(null); 
	}
}
function chkPartnerDestinRUC(buttonType){
	//alert("res");
	var dlinkButton=document.getElementById("linkup_destinationAgentExSO");
	if (http456.readyState == 4){
		var results = http456.responseText
        results = results.trim();
		if(results==''){			
			if(dlinkButton!=undefined && dlinkButton!=null){
			document.getElementById("linkup_destinationAgentExSO").style.display="none";	
			}
			document.getElementById("hidDestAP").style.display="none";
			document.getElementById("hidDestRUC").style.display="none";
			if(document.getElementById("hidDestinActiveAP")!=null)	{
				document.getElementById("hidDestinActiveAP").style.display="none"
			}
		}else{			
			if(results=='RUC'){				
				if(dlinkButton!=undefined && dlinkButton!=null){	
				document.getElementById("linkup_destinationAgentExSO").style.display="block";
				}
				document.getElementById("hidDestRUC").style.display="block";
				document.getElementById("hidDestAP").style.display="none";	
				if(document.getElementById("hidDestinActiveAP")!=null)	{
					document.getElementById("hidDestinActiveAP").style.display="none"
				}	
			}else if(results=="AP"){				
				if(dlinkButton!=undefined && dlinkButton!=null){
				document.getElementById("linkup_destinationAgentExSO").style.display="none";				
				
				}	
				document.getElementById("hidDestAP").style.display="block";
				document.getElementById("hidDestRUC").style.display="none";
				var tt=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
				var url = "chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+tt;
	             http567.open("GET", url, true);
	              http567.onreadystatechange = function(){chkAgentDestinPartnerRUC()};
	                 http567.send(null);
		}
			} 
		} 
	}


			 function chkAgentDestinPartnerRUC(){
	if (http567.readyState == 4){
		var results = http567.responseText
        results = results.trim();
		if(results=='AG'){			
		}
	}
}

function chkIsOSubRedSky(){
	var tt=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
	if(tt==''){
		document.getElementById("hidsubORUC").style.display="none";
		document.getElementById("hidsubOAP").style.display="none";
		document.getElementById("linkup_originSubAgentExSO").style.display="none";	
	}else{	
	var url = "chkPartnerRUC.html?ajax=1&decorator=simple&popup=true&partCode="+tt;
	http678.open("GET", url, true);
	http678.onreadystatechange = function(){chkPartnerOSubRUC()};
	http678.send(null);
	} 
}
function chkPartnerOSubRUC(buttonType){
	var oslinkButton=document.getElementById("linkup_originSubAgentExSO");
	if (http678.readyState == 4){
		var results = http678.responseText
        results = results.trim();
		if(results==''){			
			
		if(oslinkButton!=undefined && oslinkButton!=null)	{
			document.getElementById("linkup_originSubAgentExSO").style.display="none";
		}	
			document.getElementById("hidsubOAP").style.display="none";
			document.getElementById("hidsubORUC").style.display="none";
			if(document.getElementById("hidSubOrigActiveAP")!=null)	{
				document.getElementById("hidSubOrigActiveAP").style.display="none"
			}
		}else{
			//alert(results);
			if(results=='RUC'){
				//alert("RUC");	
					if(oslinkButton!=undefined && oslinkButton!=null)	{	
				document.getElementById("linkup_originSubAgentExSO").style.display="block";
					}
				document.getElementById("hidsubORUC").style.display="block";
				document.getElementById("hidsubOAP").style.display="none";	
				if(document.getElementById("hidSubOrigActiveAP")!=null)	{
					document.getElementById("hidSubOrigActiveAP").style.display="none"
				}	
			}else if(results=="AP"){				
					if(oslinkButton!=undefined && oslinkButton!=null)	{
				document.getElementById("linkup_originSubAgentExSO").style.display="none";				
				
					}	
				document.getElementById("hidsubOAP").style.display="block";
				document.getElementById("hidsubORUC").style.display="none";
				var tt=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
				var url = "chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+tt;
	             http789.open("GET", url, true);
	              http789.onreadystatechange = function(){chkAgentSubPartnerRUC()};
	                 http789.send(null);
		}
			} 
		} 
	}
//
			 function chkAgentSubPartnerRUC(){
	if (http789.readyState == 4){
		var results = http789.responseText
        results = results.trim();
		if(results=='AG'){			
		}
	}
}
function chkIsDSubRedSky(){
	var tt=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
	if(tt==''){
		document.getElementById("hidsubDRUC").style.display="none";
		document.getElementById("hidsubDAP").style.display="none";
		document.getElementById("linkup_destinationSubAgentExSO").style.display="none";	
	}else{		
	var url = "chkPartnerRUC.html?ajax=1&decorator=simple&popup=true&partCode="+tt;
	http891.open("GET", url, true);
	http891.onreadystatechange = function(){chkPartnerDSubRUC()};
	http891.send(null);
	} 
}
function chkPartnerDSubRUC(buttonType){	
	var dslinkButton=document.getElementById("linkup_destinationSubAgentExSO");
	if (http891.readyState == 4){
		var results = http891.responseText
        results = results.trim();
		if(results==''){
			if(dslinkButton!=undefined && dslinkButton!=null){
			document.getElementById("linkup_destinationSubAgentExSO").style.display="none";	
			}
			document.getElementById("hidsubDAP").style.display="none";
			document.getElementById("hidsubDRUC").style.display="none";
			if(document.getElementById("hidSubDestinActiveAP")!=null)	{
				document.getElementById("hidSubDestinActiveAP").style.display="none"
			}
		}else{		
			if(results=='RUC'){				
				if(dslinkButton!=undefined && dslinkButton!=null){		
				document.getElementById("linkup_destinationSubAgentExSO").style.display="block";
				}
				document.getElementById("hidsubDRUC").style.display="block";
				document.getElementById("hidsubDAP").style.display="none";	
				if(document.getElementById("hidSubDestinActiveAP")!=null)	{
					document.getElementById("hidSubDestinActiveAP").style.display="none"
				}
					
			}else if(results=="AP"){			
				if(dslinkButton!=undefined && dslinkButton!=null){
				document.getElementById("linkup_destinationSubAgentExSO").style.display="none";	
				}
				document.getElementById("hidsubDAP").style.display="block";
				document.getElementById("hidsubDRUC").style.display="none";
				//
				var tt=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
				var url = "chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+tt;
	             http912.open("GET", url, true);
	              http912.onreadystatechange = function(){chkAgentSunDePartnerRUC()};
	                 http912.send(null);
				//
		}
			} 
		} 
	}

			 function chkAgentSunDePartnerRUC(){
	if (http912.readyState == 4){
		var results = http912.responseText
        results = results.trim();
		if(results=='AG'){
			
		}
	}
}			 			 
function checkAgentAlreadyLinkUp(buttonType){
var soStatus = '${serviceOrder.status}';
	if(soStatus!='CNCL'){
		document.getElementById("overlayForAgentMarket").style.display = "block";
		var url = "checkAgentAlreadyLinkUp.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}&linkUpType="+buttonType;				 
		http198.open("GET", url, true);
		http198.onreadystatechange = function(){handleHttpResponseAgentAlreadyLinkUp(buttonType)};
		http198.send(null);
	}else{
		alert('Can not process for Link-Up, Because SO status is Cancelled.')
	}
}		
function handleHttpResponseAgentAlreadyLinkUp(buttonType){
	if (http198.readyState == 4){
		var results = http198.responseText;
		results = results.trim();
			if(results!='' && results.length>0 && buttonType=='linkup_bookingAgent'){				
				document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentExSO'].value=results.split("~")[0];
				alert("Link file ("+results.split("~")[0]+") already exists for this SO#.");
				document.getElementById("overlayForAgentMarket").style.display = "none";
				return false;
			}else if(results!='' && results.length>0 && buttonType=='linkup_Network'){
				document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value=results.split("~")[0];
				alert("Link file ("+results.split("~")[0]+") already exists for this SO#.");
				document.getElementById("overlayForAgentMarket").style.display = "none";
				return false;
			}else if(results!='' && results.length>0 && buttonType=='linkup_originAgentExSO'){
				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value=results.split("~")[0];
				alert("Link file ("+results.split("~")[0]+") already exists for this SO#.");
				document.getElementById("overlayForAgentMarket").style.display = "none";
				return false;
			}else if(results!='' && results.length>0 && buttonType=='linkup_destinationAgentExSO'){
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value=results.split("~")[0];
				alert("Link file ("+results.split("~")[0]+") already exists for this SO#.");
				document.getElementById("overlayForAgentMarket").style.display = "none";
				return false;
			}else if(results!='' && results.length>0 && buttonType=='linkup_originSubAgentExSO'){
				document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value=results.split("~")[0];
				alert("Link file ("+results.split("~")[0]+") already exists for this SO#.");
				document.getElementById("overlayForAgentMarket").style.display = "none";
				return false;
			}else if(results!='' && results.length>0 && buttonType=='linkup_destinationSubAgentExSO'){				
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value=results.split("~")[0];
				alert("Link file ("+results.split("~")[0]+") already exists for this SO#.");
				document.getElementById("overlayForAgentMarket").style.display = "none";
				return false;
			}else{
				checkAgentRole(buttonType);
			}
	}
}
var http555 = getHTTPObjectst();
function checkAgentRole(buttonType){	
	var bookingAgentCode=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
	var networkPartnerCode=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
	var originAgentCode=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
	var destinationAgentCode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
	var originSubAgentCode=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
	var destinationSubAgentCode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
	var url = "chkAgentRoleType.html?ajax=1&decorator=simple&popup=true&id=${serviceOrder.id}&bookingAgentCode="+bookingAgentCode+"&networkPartnerCode="+networkPartnerCode+"&originAgentCode="+originAgentCode+"&destinationAgentCode="+destinationAgentCode+"&originSubAgentCode="+originSubAgentCode+"&destinationSubAgentCode="+destinationSubAgentCode;
	http555.open("GET", url, true);
	http555.onreadystatechange = function(){handleHttpResponseChkAgentRole(buttonType)};
	http555.send(null); 
} 
function handleHttpResponseChkAgentRole(buttonType){
	if (http555.readyState == 4){
		var results = http555.responseText
        results = results.trim();
		if(results==''){
			alert("Kindly define your agent role.");
			  document.getElementById("overlayForAgentMarket").style.display = "none";
			return false;
		}else{
			var venderCo ='';
			var moveTypePartner='';
			if(buttonType=='linkup_Network'){
				moveTypePartner = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
			}else if(buttonType=='linkup_destinationAgentExSO'){
				moveTypePartner = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
			}else if(buttonType=='linkup_originAgentExSO'){
				moveTypePartner = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
			}else if(buttonType=='linkup_originSubAgentExSO'){
				moveTypePartner = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value; 
			}else if(buttonType=='linkup_destinationSubAgentExSO'){
				moveTypePartner = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
			}else if(buttonType=='linkup_bookingAgent'){
				moveTypePartner = document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
			}else{
			}
			$.get("networkCodeCheckCompanydivisionAjax.html?ajax=1&decorator=simple&popup=true", 
					{networkVendorCode: moveTypePartner},
					function(data){
						venderCo=data;
						if(venderCo!=''){
			venderCo=venderCo.trim().replace("[", "").replace("]", "");					
			var moveType = document.forms['trackingStatusForm'].elements['moveType'].value;
			var quotationAccessCheck = document.forms['trackingStatusForm'].elements['quotationAccessCheck'].value;
			if(moveType=='Quote' && quotationAccessCheck=='true'){
		
				
				var url="checkPartnerForMoveType.html?ajax=1&decorator=simple&popup=true&moveTypePartner="+encodeURI(moveTypePartner);
				httpMoveType.open("GET", url, true);
				httpMoveType.onreadystatechange =function(){ handleHttpResponseP(buttonType,moveTypePartner,venderCo)};
				httpMoveType.send(null);
			}else{ 
				var CMMDMMAgentList='${CMMDMMAgentList}';
				if(buttonType=='linkup_bookingAgent'){
					createExternalEntriesBookingAgent(venderCo);
				}else if(buttonType=='linkup_Network'){
					checkAgentTypeFun('networkPartner');
				}else if(buttonType=='linkup_originAgentExSO'){
				var	bookingCode=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
				var OACode=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
					if(CMMDMMAgentList!=null && CMMDMMAgentList!='' && CMMDMMAgentList.indexOf(bookingCode)>=0 && CMMDMMAgentList.indexOf(OACode)>=0){
						agree= confirm("You are linking with CorpIDs which are CMM/DMM agents via Non-CMM/DMM linking. Do you want to continue?");
						if(agree){
							createExternalEntriesOrigin(venderCo);	
						}else{
							document.getElementById("overlayForAgentMarket").style.display = "none";
							return false;}
					}else{
					createExternalEntriesOrigin(venderCo);
					}
				}else if(buttonType=='linkup_destinationAgentExSO'){
					var	bookingCode=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
					var DACode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
						if(CMMDMMAgentList!=null && CMMDMMAgentList!='' && CMMDMMAgentList.indexOf(bookingCode)>=0 && CMMDMMAgentList.indexOf(DACode)>=0){
							agree= confirm("You are linking with CorpIDs which are CMM/DMM agents via Non-CMM/DMM linking. Do you want to continue?");
							if(agree){
								createExternalEntriesDestination(venderCo);	
							}else{
								document.getElementById("overlayForAgentMarket").style.display = "none";
								return false;}
						}else{
					createExternalEntriesDestination(venderCo);
						}
				}else if(buttonType=='linkup_originSubAgentExSO'){
					var	bookingCode=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
					var subOACode=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
						if(CMMDMMAgentList!=null && CMMDMMAgentList!='' && CMMDMMAgentList.indexOf(bookingCode)>=0 && CMMDMMAgentList.indexOf(subOACode)>=0){
							agree= confirm("You are linking with CorpIDs which are CMM/DMM agents via Non-CMM/DMM linking. Do you want to continue?");
							if(agree){
								createExternalEntriesOriginSub(venderCo);
							}else{
								document.getElementById("overlayForAgentMarket").style.display = "none";
								return false;}
						}else{
					createExternalEntriesOriginSub(venderCo);
						}
				}else if(buttonType=='linkup_destinationSubAgentExSO'){
					var	bookingCode=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
					var subDACode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
					if(CMMDMMAgentList!=null && CMMDMMAgentList!='' && CMMDMMAgentList.indexOf(bookingCode)>=0 && CMMDMMAgentList.indexOf(subDACode)>=0){
						agree= confirm("You are linking with CorpIDs which are CMM/DMM agents via Non-CMM/DMM linking. Do you want to continue?");
						if(agree){
							createExternalEntriesDestinationSub(venderCo);
						}else{
							document.getElementById("overlayForAgentMarket").style.display = "none";
							return false;}
					}
					else{
					createExternalEntriesDestinationSub(venderCo);
					}
				}else{
					alert("Kindly define your agent role.");
					 document.getElementById("overlayForAgentMarket").style.display = "none";
					return false;
				}		
			} 
						}});
			
			} }}
function handleHttpResponseP(buttonType,moveTypePartner,venderCo){
	if (httpMoveType.readyState == 4){
		var results = httpMoveType.responseText
        results = results.trim();        
		if(results=='T'){
			if(buttonType=='linkup_bookingAgent'){
				createExternalEntriesBookingAgent(venderCo);
			}else if(buttonType=='linkup_Network'){
				checkAgentTypeFun('networkPartner');
			}else if(buttonType=='linkup_originAgentExSO'){
				createExternalEntriesOrigin(venderCo);
			}else if(buttonType=='linkup_destinationAgentExSO'){
				createExternalEntriesDestination(venderCo);
			}else if(buttonType=='linkup_originSubAgentExSO'){
				createExternalEntriesOriginSub(venderCo);
			}else if(buttonType=='linkup_destinationSubAgentExSO'){
				createExternalEntriesDestinationSub(venderCo);
			}else{
				alert("Kindly define your agent role.");
				 document.getElementById("overlayForAgentMarket").style.display = "none";
				return false;
			} 
		}else{			
			alert("Can't link this file, As Quote level linking is not enabled for - "+moveTypePartner+".");
			document.getElementById("overlayForAgentMarket").style.display = "none";
			return false;
			}
	}
}
function closeBoxDetails(){
	 document.getElementById("overlayForAgentMarket").style.display = "none";
}
var httpMoveType = getHTTPObject();
	function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
	}
var vendorChangeStatus =''
function checkAlreadylinkedUp(savedVendorCode,vendorCode,vendorCodeEXSo,presentEXSO,savedEXSO,venderCo){
	if(venderCo==''){
		venderCo=vendorCode;
	}
	var networkCode=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
	var networkEXso = document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value; 
	var OACode=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
	var OACodeEXso = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value; 
	var subOACode=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
	var subOACodeEXso = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value; 
	var DACode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
	var DACodeEXso = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value; 
	var subDACode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
	var subDACodeEXso = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value; 
	var bookingCode="";
	var bookingEXso=""
try{
	bookingCode=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
	bookingEXso = document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentExSO'].value; 
}catch(e){}	  
	if( vendorChangeStatus == 'YES' && presentEXSO == savedEXSO && vendorCode != savedVendorCode){
		alert('Please Save the Service Order before Link Up');
		return true;
	} 
					if(networkEXso != '' && venderCo.indexOf(networkCode)!=-1){
						alert("Link file ("+networkEXso+") already exists for agent "+vendorCode+".")
						document.forms['trackingStatusForm'].elements[vendorCodeEXSo].value=networkEXso;
						return true;
					}else if(OACodeEXso != '' && venderCo.indexOf(OACode)!=-1){						
						alert("Link file ("+OACodeEXso+") already exists for agent "+vendorCode+".")
						document.forms['trackingStatusForm'].elements[vendorCodeEXSo].value=OACodeEXso;
						return true;
					}else if(subOACodeEXso != '' && venderCo.indexOf(subOACode)!=-1){
						alert("Link file ("+subOACodeEXso+") already exists for agent "+vendorCode+".")
						document.forms['trackingStatusForm'].elements[vendorCodeEXSo].value=subOACodeEXso;
						return true;
					}else if(DACodeEXso != '' && venderCo.indexOf(DACode)!=-1){
						alert("Link file ("+DACodeEXso+") already exists for agent "+vendorCode+".")
						document.forms['trackingStatusForm'].elements[vendorCodeEXSo].value=DACodeEXso;
						return true;
					}else if(subDACodeEXso != '' && venderCo.indexOf(subDACode)!=-1){
						alert("Link file ("+subDACodeEXso+") already exists for agent "+vendorCode+".")
						document.forms['trackingStatusForm'].elements[vendorCodeEXSo].value=subDACodeEXso;
						return true;
					}else if(bookingEXso != '' && venderCo.indexOf(bookingCode)!=-1){
						alert("Link file ("+bookingEXso+") already exists for agent "+vendorCode+".")
						document.forms['trackingStatusForm'].elements[vendorCodeEXSo].value=bookingEXso;
						return true;
					}else{
						return false;
					} 

}
function createExternalEntriesBookingAgent(venderCo){
	document.getElementById("linkup_bookingAgent").style.display="none";
	var bookingAgentCode=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
	var networkPartnerCode=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
	var billingContractType=document.forms['trackingStatusForm'].elements['billingContractType'].value;
	if(networkPartnerCode.trim()==''){
		alert('Please enter a valid Network Code');
		document.getElementById("linkup_bookingAgent").style.display="block";
		 document.getElementById("overlayForAgentMarket").style.display = "none";
		return false; 
	}
	if(bookingAgentCode.trim()==''){
		alert('Please enter a valid Booking Agent Code in service Order details');
		document.getElementById("linkup_bookingAgent").style.display="block";
		 document.getElementById("overlayForAgentMarket").style.display = "none";
		return false; 
	}
	if(billingContractType!='true'){
	    alert('Pricing Contract in the Billing section is not of CMM/ DMM type.');
	    document.getElementById("linkup_bookingAgent").style.display="block";
	    document.getElementById("overlayForAgentMarket").style.display = "none";
		return false; 
	}
	else{
		var vendorCode = document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
		var savedVendorCode = '${serviceOrder.bookingAgentCode}';
		var presentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentExSO'].value;
		if(checkAlreadylinkedUp(savedVendorCode,vendorCode,'trackingStatus.bookingAgentExSO',presentEXSO,'${trackingStatus.bookingAgentExSO}',venderCo)){
			document.getElementById("linkup_bookingAgent").style.display="block";
			document.getElementById("overlayForAgentMarket").style.display = "none";
			return false;
		} }  
	//agree= confirm("Do you want to link this record with Booking Agent "+bookingAgentCode);
	if(true){
		document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpBookingAgent';
		var bookingAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentExSO'].value;
			var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(bookingAgentCode)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
			http187.open("GET", url, true);
			http187.onreadystatechange = handleHttpResponseBookingAgentCodeNetworkGroup;
			http187.send(null); 
	}else{document.getElementById("linkup_bookingAgent").style.display="block";document.getElementById("overlayForAgentMarket").style.display = "none";}
}
		
	function handleHttpResponseBookingAgentCodeNetworkGroup(){
		if (http187.readyState == 4){
			var results = http187.responseText
            results = results.trim();
			if(results=='true'){
				var bookingAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentExSO'].value;
				if(bookingAgentExSO==''){
					document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpBookingAgent';
					IsValidTime('LinkUpBookingAgent');
					openWindowMethod('linkup_bookingAgent','notes.bookingAgent','BookingAgentNotes','${trackingStatus.sequenceNumber}','${trackingStatus.shipNumber}','bookingAgentExSO');			
					
	        		
				}else{
					var savedAgent='${serviceOrder.bookingAgentCode}';
					var presentAgent=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
					var findKey=findCorpIdOfCode(savedAgent);
					var sameCompanyCodeflag=false;
					if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
						sameCompanyCodeflag=true;
					}
					if(savedAgent!=presentAgent){
						if(!sameCompanyCodeflag){
							agree= confirm("Changing the agent will remove the current RedSky Network partner link");
							if(agree){
								document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpBookingAgent';
								document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentExSO'].value='remove'+bookingAgentExSO;
								IsValidTime('LinkUpBookingAgent');
								var url="saveTrackingStatus.html";
				        		document.forms['trackingStatusForm'].action = url;
				        		document.forms['trackingStatusForm'].submit();
							}
						}
					}else{
						alert('Already linked Up');
						document.forms['trackingStatusForm'].elements['buttonType'].value="";
						document.getElementById("linkup_bookingAgent").style.display="block";
						document.getElementById("overlayForAgentMarket").style.display = "none";
					}
				} 
            }else if(results=='PARENT'){
            	var presentAgent=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
				alert('Agent '+presentAgent+' is try to Link Up with Parent.');
				document.forms['trackingStatusForm'].elements['buttonType'].value="";
				document.getElementById("linkup_bookingAgent").style.display="block";
				document.getElementById("overlayForAgentMarket").style.display = "none";
            }else{
            	var presentAgent=document.forms['trackingStatusForm'].elements['serviceOrder.bookingAgentCode'].value;
				alert('Agent '+presentAgent+' is not a network partner');
				document.forms['trackingStatusForm'].elements['buttonType'].value="";
				document.getElementById("linkup_bookingAgent").style.display="block";
				document.getElementById("overlayForAgentMarket").style.display = "none";
            } }  }
	function getHTTPObjectNWAgent() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
	}
    var httpNWAgent = getHTTPObjectNWAgent();
    var httpNWAgent1 = getHTTPObjectNWAgent();
    var httpAutoLinkUp = getHTTPObjectNWAgent(); 
    var  httpAutoLinkUp1= getHTTPObjectNWAgent();

function checkAgentTypeFun(buttonType){
	var networkPartnerCode='';
	if(buttonType=='networkPartner'){
		networkPartnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
	}else if(buttonType=='destinationPartner'){
		networkPartnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
	}else if(buttonType=='orginPartner'){
		networkPartnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
	}else{ 
	}
	var url="checkNetworkPartnerType.html?ajax=1&decorator=simple&popup=true&networkPartnerCode="+encodeURI(networkPartnerCode);
	httpNWAgent.open("GET", url, true);
	httpNWAgent.onreadystatechange =	function(){ handleHttpResponseNetworkPartnerNetworkGroupUGWW(buttonType);};
	httpNWAgent.send(null);
}
function handleHttpResponseNetworkPartnerNetworkGroupUGWW(buttonType){
	if (httpNWAgent.readyState == 4){
		var results = httpNWAgent.responseText
        results = results.trim();
		if(results=="T"){
			if(buttonType=='networkPartner'){
				createExternalEntriesUGWWNetworkGroup();
			}else{
				networkPartnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
				var url="checkNetworkPartnerType.html?ajax=1&decorator=simple&popup=true&networkPartnerCode="+encodeURI(networkPartnerCode);
				httpNWAgent1.open("GET", url, true);
				httpNWAgent1.onreadystatechange =	function(){ handleHttpResponseNetworkPartnerChk(buttonType);};
				httpNWAgent1.send(null);
			}
		}else{
			if(buttonType=='networkPartner'){
				//createExternalEntriesNetworkGroup();
				checkNetworkCoordinators();
			}else if(buttonType=='destinationPartner'){
				var networkPartnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
				//agree= confirm("Do you want to link this record with Network Agent "+networkPartnerCode);
				if(true){ 
					var url="createLineInNetworkControl.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(networkPartnerCode)+"&shipNumber=${serviceOrder.shipNumber}";
					httpAutoLinkUp1.open("GET", url, true);
					httpAutoLinkUp1.onreadystatechange = function(){handleHttpResponseNetworkControlCreate(buttonType);};
					httpAutoLinkUp1.send(null);	
				}
			}else if(buttonType=='orginPartner'){
				var networkPartnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
				//agree= confirm("Do you want to link this record with Network Agent "+networkPartnerCode);
				if(true){
					var url="createLineInNetworkControl.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(networkPartnerCode)+"&shipNumber=${serviceOrder.shipNumber}";
					httpAutoLinkUp1.open("GET", url, true);
					httpAutoLinkUp1.onreadystatechange = function(){handleHttpResponseNetworkControlCreate(buttonType);};
					httpAutoLinkUp1.send(null);		
				} } } } }


function createExternalEntriesUGWWNetworkGroup(){
	document.getElementById("linkup_Network").style.display="none";
	var networkPartnerCode=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
	if(networkPartnerCode.trim()==''){
		alert('Please enter a valid Agent Code');
		document.getElementById("linkup_Network").style.display="block";
		document.getElementById("overlayForAgentMarket").style.display = "none";
		return false;
	}
	else{
		var vendorCode = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
		var savedVendorCode = '${trackingStatus.networkPartnerCode}';
		var presentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
		if(checkAlreadylinkedUp(savedVendorCode,vendorCode,'trackingStatus.networkAgentExSO',presentEXSO,'${trackingStatus.networkAgentExSO}','')){
			document.getElementById("linkup_Network").style.display="block";
			document.getElementById("overlayForAgentMarket").style.display = "none";
			return false;
		} } 
	//agree= confirm("Do you want to link this record with Network Agent "+networkPartnerCode);
	if(true){
		document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpUGWWNetworkGroup';
		var networkAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
		var networkPartnerCode=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
		
		var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(networkPartnerCode)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
			http190.open("GET", url, true);
			http190.onreadystatechange = handleHttpResponseNetworkPartnerUGWWNetworkGroup;
			http190.send(null);
		
	}else{document.getElementById("linkup_Network").style.display="block";document.getElementById("overlayForAgentMarket").style.display = "none";} 
}

function handleHttpResponseNetworkPartnerChk(buttonType){
	if (httpNWAgent1.readyState == 4){
		var results = httpNWAgent1.responseText
        results = results.trim();
		//if(results=='T'){
			 if(buttonType=='destinationPartner'){
					var networkPartnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
					//agree= confirm("Do you want to link this record with Network Agent "+networkPartnerCode);
					if(true){ 
						var url="createLineInNetworkControl.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(networkPartnerCode)+"&shipNumber=${serviceOrder.shipNumber}";
						httpAutoLinkUp1.open("GET", url, true);
						httpAutoLinkUp1.onreadystatechange = function(){handleHttpResponseNetworkControlCreate(buttonType);};
						httpAutoLinkUp1.send(null);	
					} 
					
				}
				if(buttonType=='orginPartner'){
					var networkPartnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
					//agree= confirm("Do you want to link this record with Network Agent "+networkPartnerCode);
					if(true){
						var url="createLineInNetworkControl.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(networkPartnerCode)+"&shipNumber=${serviceOrder.shipNumber}";
						httpAutoLinkUp1.open("GET", url, true);
						httpAutoLinkUp1.onreadystatechange = function(){handleHttpResponseNetworkControlCreate(buttonType);};
						httpAutoLinkUp1.send(null);		
					} 	
				} 
		/* }else{
			alert('To complete the link up with UGWW please select \n UGWW in the Network role and click link up under Network.')
		} */ } }
		
	function handleHttpResponseNetworkPartnerUGWWNetworkGroup(){
		if (http190.readyState == 4){
			var results = http190.responseText
            results = results.trim();
			if(results=='true'){
				var networkAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
				if(networkAgentExSO==''){
					document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpUGWWNetworkGroup';
					openWindowMethod('linkup_Network','notes.networkAgent','NetworkAgentNotes','${trackingStatus.sequenceNumber}','${trackingStatus.shipNumber}','networkAgentExSO');
					
				}else{
					var savedAgent='${trackingStatus.networkPartnerCode}';
					var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
					var findKey=findCorpIdOfCode(savedAgent);
					var sameCompanyCodeflag=false;
					if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
						sameCompanyCodeflag=true;
					}
					if(savedAgent!=presentAgent){
						if(!sameCompanyCodeflag){
							agree= confirm("Changing the agent will remove the current RedSky Network partner link");
							if(agree){
							document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpUGWWNetworkGroup';
							document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value='remove'+networkAgentExSO;
							var url="saveTrackingStatus.html";
			        		document.forms['trackingStatusForm'].action = url;
			        		document.forms['trackingStatusForm'].submit();
							}
						}
					}else{
						alert('Already linked Up');
						document.forms['trackingStatusForm'].elements['buttonType'].value="";
						document.getElementById("linkup_Network").style.display="block";
						document.getElementById("overlayForAgentMarket").style.display = "none";
					}
				} 
            }else if(results=='PARENT'){
            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
				alert('Agent '+presentAgent+' is try to Link Up with Parent.');
				document.forms['trackingStatusForm'].elements['buttonType'].value="";
				document.getElementById("linkup_Network").style.display="block";
				 document.getElementById("overlayForAgentMarket").style.display = "none";
            }else{
            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
				alert('Agent '+presentAgent+' is not a network partner');
				document.forms['trackingStatusForm'].elements['buttonType'].value="";
				document.getElementById("linkup_Network").style.display="block";
				document.getElementById("overlayForAgentMarket").style.display = "none";
            }	 }  } 
	
	function checkAutomaticLinkup(buttonType){ 
		if(buttonType=='destinationPartner'){
			var destinationAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
			if(destinationAgent==''){
				alert('Please enter a valid Agent Code.');
				return false;
			}else{
				var url="findAutomaticLinkUpFlag.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(destinationAgent);
				httpAutoLinkUp.open("GET", url, true);
				httpAutoLinkUp.onreadystatechange =function(){ handleHttpResponseChkAutomaticLinkUp(buttonType);};
				httpAutoLinkUp.send(null);
			}  }
		else if(buttonType=='orginPartner'){
			var originAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
			if(originAgent==''){
				alert('Please enter a valid Agent Code.');
				return false;
			}else{
				var url="findAutomaticLinkUpFlag.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(originAgent);
				httpAutoLinkUp.open("GET", url, true);
				httpAutoLinkUp.onreadystatechange = function(){handleHttpResponseChkAutomaticLinkUp(buttonType);};
				httpAutoLinkUp.send(null);
			} 
		}else if(buttonType == 'networkPartner'){
			var networkAgent=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
			if(networkAgent==''){
				alert('Please enter a valid Agent Code.');
				return false;
			}else{
				var url="findAutomaticLinkUpFlag.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(networkAgent);
				httpAutoLinkUp.open("GET", url, true);
				httpAutoLinkUp.onreadystatechange = function(){handleHttpResponseChkAutomaticLinkUp(buttonType);};
				httpAutoLinkUp.send(null);
			}
		}else{ 	}
	}

	function handleHttpResponseChkAutomaticLinkUp(buttonType){
		if (httpAutoLinkUp.readyState == 4){
			var results = httpAutoLinkUp.responseText;
	        results = results.trim();
	        if(results=='F'){
	        	if(buttonType == 'networkPartner'){
	        		var networkPartnerCode = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
					agree= confirm("Do you want to link this record with Network Agent "+networkPartnerCode);
					if(agree){ 
						var url="createLineInNetworkControl.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(networkPartnerCode)+"&shipNumber=${serviceOrder.shipNumber}";
						httpAutoLinkUp1.open("GET", url, true);
						httpAutoLinkUp1.onreadystatechange = function(){handleHttpResponseNetworkControlCreate(buttonType);};
						httpAutoLinkUp1.send(null);	
					}
	        	}else{
	        	 	checkAgentTypeFun(buttonType);
	        	}
			}else{
				if(buttonType=='networkPartner'){
					 checkAgentTypeFun(buttonType);
				}else if(buttonType=='orginPartner'){
					createExternalEntriesOrigin('');
				}else if(buttonType=='destinationPartner'){
					createExternalEntriesDestination('');
				}else{
					
				} } } }
	
	function handleHttpResponseNetworkControlCreate(buttonType){
		if (httpAutoLinkUp1.readyState == 4){
			var results = httpAutoLinkUp1.responseText;
	        results = results.trim();
			if(results=='Y'){
				if(buttonType=='destinationPartner'){
					document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value = "Sent to Agent";
				}else if(buttonType=='orginPartner'){
					document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value = "Sent to Agent";
				}else if(buttonType=='networkPartner'){
					document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value = "Sent to Agent";
				}else{ 
				}
				openWindowMethod('linkup_Network','notes.networkAgent','NetworkAgentNotes');
				
			} } } 

function createExternalEntriesNetworkGroup(){
	document.getElementById("linkup_Network").style.display="none";
	var networkPartnerCode=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
	var billingContractType=document.forms['trackingStatusForm'].elements['billingContractType'].value; 
	if(networkPartnerCode.trim()==''){
		alert('Please enter a valid Agent Code');
		document.getElementById("linkup_Network").style.display="block";
		document.getElementById("overlayForAgentMarket").style.display = "none";
		return false;
	}
	
	if(billingContractType !='true'){
	    alert('Pricing Contract in the Billing section is not of CMM/ DMM type.');
	    document.getElementById("linkup_Network").style.display="block";
	    document.getElementById("overlayForAgentMarket").style.display = "none";
		return false;
	}else{
		var vendorCode = document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
		var savedVendorCode = '${trackingStatus.networkPartnerCode}';
		var presentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
		if(checkAlreadylinkedUp(savedVendorCode,vendorCode,'trackingStatus.networkAgentExSO',presentEXSO,'${trackingStatus.networkAgentExSO}','')){
			document.getElementById("linkup_Network").style.display="block";
			document.getElementById("overlayForAgentMarket").style.display = "none";
			return false;
		} } 
	//agree= confirm("Do you want to link this record with Network Agent "+networkPartnerCode);
	if(true){
		document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpNetworkGroup';
		var networkAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
			var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(networkPartnerCode)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
			http156.open("GET", url, true);
			http156.onreadystatechange = handleHttpResponseNetworkPartnerNetworkGroup;
			http156.send(null); 
	}else{document.getElementById("linkup_Network").style.display="block";document.getElementById("overlayForAgentMarket").style.display = "none";}

}

 function checkNetworkCoordinators(){
	 var cfCoordinatorname="${customerFile.coordinator}";
 var soCoordinatorname="${serviceOrder.coordinator}";	
	$.ajax({
        type: "POST",
        url: "checkNetworkCoordinator.html?ajax=1&decorator=simple&popup=true",
        data: {cfCoordinatorname:cfCoordinatorname,soCoordinatorname:soCoordinatorname},
        success: function (data, textStatus, jqXHR) {
            if(data.trim() != "true")
            {
                alert("LinkUp to Network is only allowed if C/F and S/O coordinator are Network Coordinators");
                document.getElementById("overlayForAgentMarket").style.display = "none";
            }else{
            	createExternalEntriesNetworkGroup();
            }
        },
      error: function (data, textStatus, jqXHR) {
              }
      });
}

	function handleHttpResponseNetworkPartnerNetworkGroup(){
		if (http156.readyState == 4){
			var results = http156.responseText
            results = results.trim();
			if(results=='true'){
				var networkAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value;
				if(networkAgentExSO==''){
					document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpNetworkGroup';
					IsValidTime('LinkUpNetworkGroup');
					openWindowMethod('linkup_Network','notes.networkAgent','NetworkAgentNotes','${trackingStatus.sequenceNumber}','${trackingStatus.shipNumber}','networkAgentExSO');
					
				}else{
					var savedAgent='${trackingStatus.networkPartnerCode}';
					var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
					var findKey=findCorpIdOfCode(savedAgent);
					var sameCompanyCodeflag=false;
					if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
						sameCompanyCodeflag=true;
					}
					if(savedAgent!=presentAgent){
						if(!sameCompanyCodeflag){
							agree= confirm("Changing the agent will remove the current RedSky Network partner link");
							if(agree){
							document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpNetworkGroup';
							document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value='remove'+networkAgentExSO;
							IsValidTime('LinkUpNetworkGroup');
							var url="saveTrackingStatus.html";
			        		document.forms['trackingStatusForm'].action = url;
			        		document.forms['trackingStatusForm'].submit();
							}
						}
					}else{
						alert('Already linked Up');
						document.forms['trackingStatusForm'].elements['buttonType'].value="";
						document.getElementById("linkup_Network").style.display="block";
					} } 
            }else if(results=='PARENT'){
            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
				alert('Agent '+presentAgent+' is try to Link Up with Parent.');
				document.forms['trackingStatusForm'].elements['buttonType'].value="";
				document.getElementById("linkup_Network").style.display="block";
				 document.getElementById("overlayForAgentMarket").style.display = "none";
            }else{
            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].value;
				alert('Agent '+presentAgent+' is not a network partner');
				document.forms['trackingStatusForm'].elements['buttonType'].value="";
				document.getElementById("linkup_Network").style.display="block";
				document.getElementById("overlayForAgentMarket").style.display = "none";
            }	 }  } 

function createExternalEntriesDestination(venderCo){	
	document.getElementById("linkup_destinationAgentExSO").style.display="none";
	var destinationAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
	
	if(destinationAgent.trim()==''){
		alert('Please enter a valid Agent Code');
		document.getElementById("linkup_destinationAgentExSO").style.display="block";
		 document.getElementById("overlayForAgentMarket").style.display = "none";
		return false;
	}else{
		var vendorCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
		var savedVendorCode = '${trackingStatus.destinationAgentCode}';
		var presentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value;
		if(checkAlreadylinkedUp(savedVendorCode,vendorCode,'trackingStatus.destinationAgentExSO',presentEXSO,'${trackingStatus.destinationAgentExSO}',venderCo)){
			document.getElementById("linkup_destinationAgentExSO").style.display="block";
			 document.getElementById("overlayForAgentMarket").style.display = "none";
			return false;
		}
	}
	
	// agree= confirm("Do you want to link this record with Network Agent "+destinationAgent);
	if(true){
		
		document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpDest';
		var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value;
			var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(destinationAgent)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
			http195.open("GET", url, true);
			http195.onreadystatechange = handleHttpResponseNetworkPartnerDestination;
			http195.send(null);
		
	}else{document.getElementById("linkup_destinationAgentExSO").style.display="block"; document.getElementById("overlayForAgentMarket").style.display = "none";}

} 	

	function handleHttpResponseNetworkPartnerDestination(){
		if (http195.readyState == 4){
			var results = http195.responseText
            results = results.trim();
			if(results=='true'){
				var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value;
				if(destinationAgentExSo==''){
					document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpDest';
					IsValidTime('LinkUpDest');
					openWindowMethod('linkup_destinationAgentExSO','notes.destinationAgent','DestinAgentNotes','${trackingStatus.sequenceNumber}','${trackingStatus.shipNumber}','destinationAgentExSO');
					
				}else{
					var savedAgent='${trackingStatus.destinationAgentCode}';
					var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
					var findKey=findCorpIdOfCode(savedAgent);
					var sameCompanyCodeflag=false;
					if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
						sameCompanyCodeflag=true;
					}
					if(savedAgent!=presentAgent){
						if(!sameCompanyCodeflag){					
							agree= confirm("Changing the agent will remove the current RedSky Network partner link");
							if(agree){
							document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpDest';
							document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value='remove'+destinationAgentExSo;
							IsValidTime('LinkUpDest');
							var url="saveTrackingStatus.html";
			        		document.forms['trackingStatusForm'].action = url;
			        		document.forms['trackingStatusForm'].submit();
							}
						}
					}else{
						alert('Already linked Up');
						document.forms['trackingStatusForm'].elements['buttonType'].value="";
						document.getElementById("linkup_destinationAgentExSO").style.display="block";
					} } 
 
            }else if(results=='PARENT'){
            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
				alert('Agent '+presentAgent+' is try to Link Up with Parent.');
				document.forms['trackingStatusForm'].elements['buttonType'].value="";
				document.getElementById("linkup_destinationAgentExSO").style.display="block";
				 document.getElementById("overlayForAgentMarket").style.display = "none";
            }else{
            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value;
				alert('Agent '+presentAgent+' is not a network partner');
				document.forms['trackingStatusForm'].elements['buttonType'].value="";
				document.getElementById("linkup_destinationAgentExSO").style.display="block";
				 document.getElementById("overlayForAgentMarket").style.display = "none";
            } } }
	
	function createExternalEntriesOrigin(venderCo){
		document.getElementById("linkup_originAgentExSO").style.display="none";
		var destinationAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
		
		if(destinationAgent.trim()==''){
			alert('Please enter a valid Agent Code');
			document.getElementById("linkup_originAgentExSO").style.display="block";
			 document.getElementById("overlayForAgentMarket").style.display = "none";
			return false;
		}else{
			var vendorCode = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
			var savedVendorCode = '${trackingStatus.originAgentCode}';
			var presentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value;
			if(checkAlreadylinkedUp(savedVendorCode,vendorCode,'trackingStatus.originAgentExSO',presentEXSO,'${trackingStatus.originAgentExSO}',venderCo)){
				document.getElementById("linkup_originAgentExSO").style.display="block";
				 document.getElementById("overlayForAgentMarket").style.display = "none";
				return false;
			} }
		//agree= confirm("Do you want to link this record with Network Agent: "+destinationAgent);
		if(true){
			
			document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpOrigin';
			var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value;
				var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(destinationAgent)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
				http187.open("GET", url, true);
				http187.onreadystatechange = handleHttpResponseNetworkPartnerOrigin;
				http187.send(null);
			
		}else{document.getElementById("linkup_originAgentExSO").style.display="block"; document.getElementById("overlayForAgentMarket").style.display = "none";}
	
}
	
	function linkUP(subType){
		var url="saveTrackingStatus.html?linkUp=linkUPNotes&trackingStatusId=${trackingStatus.id}&notesSubType="+subType;
		document.forms['trackingStatusForm'].action = url;
		document.forms['trackingStatusForm'].submit();
	}
	
	function openWindowMethod(id,checkId,linkUpSubType,seqNumber,shpNumber,buttonType){		
		document.getElementById(id).style.display="block";
		window.open("linkUpNotes.html?ajax=1&decorator=popup&popup=true&id=${trackingStatus.id}&checkId="+checkId+"&linkUpSubType="+linkUpSubType+"&seqNumber="+seqNumber+"&shpNumber="+shpNumber+"&linkUpButtonType="+buttonType,"mywindow","menubar=0,resizable=1,top=100,left=380,scrollbars=1,width=800,height=630");
	}

	 function handleHttpResponseNetworkPartnerOrigin(){
			if (http187.readyState == 4){
				var results = http187.responseText
	            results = results.trim();
				if(results=='true'){
					var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value;
					if(destinationAgentExSo==''){
						document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpOrigin';
						IsValidTime('LinkUpOrigin');
						openWindowMethod('linkup_originAgentExSO','notes.originAgent','OriginAgentNotes','${trackingStatus.sequenceNumber}','${trackingStatus.shipNumber}','originAgentExSO');
						
					}else{
						var savedAgent='${trackingStatus.originAgentCode}';
						var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
						var findKey=findCorpIdOfCode(savedAgent);
						var sameCompanyCodeflag=false;
						if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
							sameCompanyCodeflag=true;
						}
						if(savedAgent!=presentAgent){
							if(!sameCompanyCodeflag){
								agree= confirm("Changing the agent will remove the current RedSky Network partner link");
								if(agree){
								document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpOrigin';
								document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value='remove'+destinationAgentExSo;
								IsValidTime('LinkUpOrigin');
								var url="saveTrackingStatus.html";
				        		document.forms['trackingStatusForm'].action = url;
				        		document.forms['trackingStatusForm'].submit();
								}
							}
						}else{
							alert('Already linked Up');
							document.forms['trackingStatusForm'].elements['buttonType'].value="";
							document.getElementById("linkup_originAgentExSO").style.display="block";
						} } 
	            }else if(results=='PARENT'){
	            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
					alert('Agent '+presentAgent+' is try to Link Up with Parent.');
					document.forms['trackingStatusForm'].elements['buttonType'].value="";
					document.getElementById("linkup_originAgentExSO").style.display="block";
					 document.getElementById("overlayForAgentMarket").style.display = "none";
	            }	
				else{
	            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value;
					alert('Agent '+presentAgent+' is not a network partner');
					document.forms['trackingStatusForm'].elements['buttonType'].value="";
					document.getElementById("linkup_originAgentExSO").style.display="block";
					 document.getElementById("overlayForAgentMarket").style.display = "none";
	            } } } 
		function createExternalEntriesDestinationSub(venderCo){
			document.getElementById("linkup_destinationSubAgentExSO").style.display="none";
			var destinationAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
			if(destinationAgent.trim()==''){
				alert('Please enter a valid Agent Code');
				document.getElementById("linkup_destinationSubAgentExSO").style.display="block";
				 document.getElementById("overlayForAgentMarket").style.display = "none";
				return false;
			}else{
				var vendorCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
				var savedVendorCode = '${trackingStatus.destinationSubAgentCode}';
				var presentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value;
				if(checkAlreadylinkedUp(savedVendorCode,vendorCode,'trackingStatus.destinationSubAgentExSO',presentEXSO,'${trackingStatus.destinationSubAgentExSO}',venderCo)){
					document.getElementById("linkup_destinationSubAgentExSO").style.display="block";
					 document.getElementById("overlayForAgentMarket").style.display = "none";
					return false;
				}
			}
			// agree= confirm("Do you want to link this record with Network Agent "+destinationAgent);
			if(true){
				var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value;
				document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpDestSub';	
				var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(destinationAgent)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
					http176.open("GET", url, true);
					http176.onreadystatechange = handleHttpResponseNetworkPartnerDestinationSub;
					http176.send(null);
				
			}else{document.getElementById("linkup_destinationSubAgentExSO").style.display="block"; document.getElementById("overlayForAgentMarket").style.display = "none";}
		}
				
			function handleHttpResponseNetworkPartnerDestinationSub(){
				if (http176.readyState == 4){
					var results = http176.responseText
		            results = results.trim();
					if(results=='true'){
						var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value;
						if(destinationAgentExSo==''){
							document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpDestSub';
							IsValidTime('LinkUpDestSub');
							
							openWindowMethod('linkup_destinationSubAgentExSO','notes.subDestinationAgent','SubDestinAgentNotes','${trackingStatus.sequenceNumber}','${trackingStatus.shipNumber}','destinationSubAgentExSO');
							
							
						}else{
							var savedAgent='${trackingStatus.destinationSubAgentCode}';
							var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
							var findKey=findCorpIdOfCode(savedAgent);
							var sameCompanyCodeflag=false;
							if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
								sameCompanyCodeflag=true;
							}

							if(savedAgent!=presentAgent){
								if(!sameCompanyCodeflag){
									agree= confirm("Changing the agent will remove the current RedSky Network partner link");
									if(agree){
										document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpDestSub';
										document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value='remove'+destinationAgentExSo;
										IsValidTime('LinkUpDestSub');
										var url="saveTrackingStatus.html";
					        		document.forms['trackingStatusForm'].action = url;
					        		document.forms['trackingStatusForm'].submit();
									}
								}
							}else{
								alert('Already linked Up');
								document.forms['trackingStatusForm'].elements['buttonType'].value="";
								document.getElementById("linkup_destinationSubAgentExSO").style.display="block";
								 document.getElementById("overlayForAgentMarket").style.display = "none";
							} }
		            }else if(results=='PARENT'){
		            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
						alert('Agent '+presentAgent+' is try to Link Up with Parent.');
						document.forms['trackingStatusForm'].elements['buttonType'].value="";
						document.getElementById("linkup_destinationSubAgentExSO").style.display="block";
						 document.getElementById("overlayForAgentMarket").style.display = "none";
		            }else{
		            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
						alert('Agent '+presentAgent+' is not a network partner');
						document.forms['trackingStatusForm'].elements['buttonType'].value="";
						document.getElementById("linkup_destinationSubAgentExSO").style.display="block";
						 document.getElementById("overlayForAgentMarket").style.display = "none";
		            } } } 
			
			function createExternalEntriesOriginSub(venderCo){
				document.getElementById("linkup_originSubAgentExSO").style.display="none";
				var destinationAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
				if(destinationAgent.trim()==''){
					alert('Please enter a valid Agent Code');
					document.getElementById("linkup_originSubAgentExSO").style.display="block";
					 document.getElementById("overlayForAgentMarket").style.display = "none";
					return false;
				}else{
					var vendorCode = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
					var savedVendorCode = '${trackingStatus.originSubAgentCode}';
					var presentEXSO = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value;
					if(checkAlreadylinkedUp(savedVendorCode,vendorCode,'trackingStatus.originSubAgentExSO',presentEXSO,'${trackingStatus.originSubAgentExSO}',venderCo)){
						document.getElementById("linkup_originSubAgentExSO").style.display="block";
						 document.getElementById("overlayForAgentMarket").style.display = "none";
						return false;
					} }
				// agree= confirm("Do you want to link this record with Network Agent "+destinationAgent);
				if(true){
					var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value;
					document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpOrigSub';
					var url="networkPartner.html?ajax=1&decorator=simple&popup=true&respectiveNetworkAgent=" + encodeURI(destinationAgent)+"&parentShipNumber=${serviceOrder.bookingAgentShipNumber}";
						http165.open("GET", url, true);
						http165.onreadystatechange = handleHttpResponseNetworkPartnerOriginSub;
						http165.send(null);
					
				}else{document.getElementById("linkup_originSubAgentExSO").style.display="block"; document.getElementById("overlayForAgentMarket").style.display = "none";}
			}
					
				function handleHttpResponseNetworkPartnerOriginSub(){
					if (http165.readyState == 4){
						var results = http165.responseText
			            results = results.trim();
						if(results=='true'){
							var destinationAgentExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value;
							if(destinationAgentExSo==''){
								document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpOrigSub';
								IsValidTime('LinkUpOrigSub');
								openWindowMethod('linkup_originSubAgentExSO','notes.subOriginAgent','SubOriginAgentNotes','${trackingStatus.sequenceNumber}','${trackingStatus.shipNumber}','originSubAgentExSO');
								
							}else{
								var savedAgent='${trackingStatus.originSubAgentCode}';
								var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
								var findKey=findCorpIdOfCode(savedAgent);
								var sameCompanyCodeflag=false;
								if(presentAgent!='' && findKey!="" && findKey.indexOf("^")>-1 && findKey.split("^")[1].indexOf(presentAgent)>-1){
									sameCompanyCodeflag=true;
								}
								if(savedAgent!=presentAgent){
										if(!sameCompanyCodeflag){									
										agree= confirm("Changing the agent will remove the current RedSky Network partner link");
										if(agree){
											document.forms['trackingStatusForm'].elements['buttonType'].value='LinkUpOrigSub';
											document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value='remove'+destinationAgentExSo;
											IsValidTime('LinkUpOrigSub');
											var url="saveTrackingStatus.html";
						        		document.forms['trackingStatusForm'].action = url;
						        		document.forms['trackingStatusForm'].submit();
										}
									}
								}else{
									alert('Already linked Up');
									document.forms['trackingStatusForm'].elements['buttonType'].value="";
									document.getElementById("linkup_originSubAgentExSO").style.display="block";
								}
							}
			            	
			            }else if(results=='PARENT'){
			            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
							alert('Agent '+presentAgent+' is try to Link Up with Parent.');
							document.forms['trackingStatusForm'].elements['buttonType'].value="";
							document.getElementById("linkup_originSubAgentExSO").style.display="block";
							 document.getElementById("overlayForAgentMarket").style.display = "none";
			            }else{
			            	var presentAgent=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value;
							alert('Agent '+presentAgent+' is not a network partner');
							document.forms['trackingStatusForm'].elements['buttonType'].value="";
							document.getElementById("linkup_originSubAgentExSO").style.display="block";
							 document.getElementById("overlayForAgentMarket").style.display = "none";
			            } } }
		String.prototype.trim = function() {
		    return this.replace(/^\s+|\s+$/g,"");
		}
		String.prototype.ltrim = function() {
		    return this.replace(/^\s+/,"");
		}
		String.prototype.rtrim = function() {
		    return this.replace(/\s+$/,"");
		}
		String.prototype.lntrim = function() {
		    return this.replace(/^\s+/,"","\n");
		} 
		var http234 = getHTTPObject234();
	    var http198 = getHTTPObjectst();
		function getHTTPObject234()
		{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject) {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp) {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        } }
	    return xmlhttp;
		}
	    
try{
	    <c:if test="${isNetworkBookingAgent==true || isNetworkOriginAgent==true ||  isNetworkDestinationAgent==true || networkAgent }">
	    	var destExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value;
	    	var destSubExSo=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value;
	    	var originExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value;
	    	var originSubExSo=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value;
	    	var networkAgentExSO=document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value; 
	    	var isRemovedestExSo = destExSo.indexOf('emove');
	    	if(isRemovedestExSo>0)
	    	{
	    		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentExSO'].value="";
	    	} 
	    	var isRemovedestSubExSo = destSubExSo.indexOf('emove');
	    	if(isRemovedestSubExSo>0)
	    	{
	    		document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentExSO'].value="";
	    	} 
	    	var isRemoveOriginExSo = originExSo.indexOf('emove');
	    	if(isRemoveOriginExSo>0)
	    	{
	    		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentExSO'].value="";
	    	} 
	    	var isRemoveOriginSubExSo = originSubExSo.indexOf('emove');
	    	if(isRemoveOriginSubExSo>0)
	    	{
	    		document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentExSO'].value="";
	    	}
	    	var isRemoveNetworkAgentExSO = networkAgentExSO.indexOf('emove');
	    	if(isRemoveNetworkAgentExSO>0)
	    	{
	    		document.forms['trackingStatusForm'].elements['trackingStatus.networkAgentExSO'].value="";
	    	}
	  </c:if>  
	  <c:if test="${isNetworkBookingAgent==false}">	  	
	  <c:if test="${isNetworkOrginAgentSection==true}">
	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].className = 'input-textUpper';
	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].className = 'input-textUpper';
	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentPhoneNumber'].className = 'input-textUpper';
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].className = 'input-textUpper';
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].className = 'input-textUpper';
		
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].setAttribute('readonly','readonly');
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgent'].setAttribute('readonly','readonly');
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentPhoneNumber'].setAttribute('readonly','readonly');
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].setAttribute('readonly','readonly');
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].setAttribute('readonly','readonly');
		var el = document.getElementById('openpopup1.img');
		el.onclick = false;
	    document.images['openpopup1.img'].src = 'images/navarrow.gif';
	  </c:if> 	  
	  <c:if test="${isNetworkDestinationAgentSection==true}">
	  	document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].className = 'input-textUpper';
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].className = 'input-textUpper';
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentPhoneNumber'].className = 'input-textUpper';
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContact'].className = 'input-textUpper';
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].className = 'input-textUpper';
		
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].setAttribute('readonly','readonly');
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgent'].setAttribute('readonly','readonly');
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentPhoneNumber'].setAttribute('readonly','readonly');		
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContact'].setAttribute('readonly','readonly');
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].setAttribute('readonly','readonly');
		var e2 = document.getElementById('openpopup4.img');
		e2.onclick = false;
	    document.images['openpopup4.img'].src = 'images/navarrow.gif';	  
	  </c:if> 
	  <c:if test="${isNetworkNetworkPartnerCodeSection==true && !networkAgent}">
	  	document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].className = 'input-textUpper';
		document.forms['trackingStatusForm'].elements['trackingStatus.networkContact'].className = 'input-textUpper';
		document.forms['trackingStatusForm'].elements['trackingStatus.networkEmail'].className = 'input-textUpper';
		document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].setAttribute('readonly','readonly');
		document.forms['trackingStatusForm'].elements['trackingStatus.networkContact'].setAttribute('readonly','readonly');
		document.forms['trackingStatusForm'].elements['trackingStatus.networkEmail'].setAttribute('readonly','readonly');
		var e2 = document.getElementById('openpopupnetwork.img');
		e2.onclick = false;
	    document.images['openpopupnetwork.img'].src = 'images/navarrow.gif';	  
	  </c:if>       	  
	  </c:if>
	  <c:if test="${(isNetworkNetworkPartnerCodeSection==true )|| (networkAgent && (trackingStatus.networkAgentExSO!=null && trackingStatus.networkAgentExSO!=''))}">
      document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].className = 'input-textUpper';
	  document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerCode'].setAttribute('readonly','readonly');
	  document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].className = 'input-textUpper';
	  document.forms['trackingStatusForm'].elements['trackingStatus.networkPartnerName'].setAttribute('readonly','readonly');
	  var e2 = document.getElementById('openpopupnetwork.img');
		e2.onclick = false;
	    document.images['openpopupnetwork.img'].src = 'images/navarrow.gif';	
	  </c:if>   
}catch(e){}	  
</script>
<script type="text/javascript">
function resetEmail(agentType,str){
	str=str.trim();
	if(str.length == 0){
		resetContactEmail(agentType);
	} 
	//closing portal icon if contact is blank
	if(agentType == 'OA'){
		//alert("OA"+con.length);
		var con=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value ;
		con=con.trim();
		//alert(con.length);
		var email=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value ;
		var pcode=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value ;
		if(con!=null &&  con.length>0){			
			var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
			http234.open("GET", url, true);
			http234.onreadystatechange = handleHttpchkPartnerAgentRUC;
			http234.send(null);
		}else{
			document.getElementById("hidActiveAP").style.display="none";
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value='';
		}
		
	}else if(agentType == 'DA'){
		var con=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContact'].value ;
		con=con.trim();
		//alert(con);
		var email=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value ;
		var pcode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value ;
		if(con!=null &&  con.length>0){			
			var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
			http234.open("GET", url, true);
			http234.onreadystatechange = handleHttpchkDestinationPartnerAgentRUC;
			http234.send(null);
		}else{
			document.getElementById("hidDestinActiveAP").style.display="none";
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value='';	
		}
		
	}else if(agentType == 'SOA'){
		var con=document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContact'].value ;
		con=con.trim();
		//alert(con);
		var email=document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value ;
		var pcode=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value ;
		if(con!=null &&  con.length>0){			
			var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
			http234.open("GET", url, true);
			http234.onreadystatechange = handleHttpchkSAPartnerAgentRUC;
			http234.send(null);
		}else{
			document.getElementById("hidSubOrigActiveAP").style.display="none";	
			document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value='';
		}
		
	}else if(agentType == 'SDA'){
		var con=document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentAgentContact'].value ;
		con=con.trim();
		//alert(con);
		var email=document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value ;
		var pcode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value ;
		if(con!=null &&  con.length>0){			
			var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
			http234.open("GET", url, true);
			http234.onreadystatechange = handleHttpchkDAPartnerAgentRUC;
			http234.send(null);
		}else{
			document.getElementById("hidSubDestinActiveAP").style.display="none";
			document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value='';	
		}
		
	//
	}
}
function handleHttpchkDAPartnerAgentRUC(){
	//alert("resssssss")
	if (http234.readyState == 4){
		var results = http234.responseText
        results = results.trim();		
			if(results=='AG'){	
				document.getElementById("hidSubDestinActiveAP").style.display="block";
				document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value='true';	
			}else{
				document.getElementById("hidSubDestinActiveAP").style.display="none";
				document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value='';	
			}
		
	
}
}
function handleHttpchkSAPartnerAgentRUC(){
	//alert("resssssss")
	if (http234.readyState == 4){
		var results = http234.responseText
        results = results.trim();		
			if(results=='AG'){	
				document.getElementById("hidSubOrigActiveAP").style.display="block";
				document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value='true';	
			}else{
				document.getElementById("hidSubOrigActiveAP").style.display="none";	
				document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value='';	
			}
		
	
}
}
function handleHttpchkDestinationPartnerAgentRUC(){
	//alert("resssssss")
	if (http234.readyState == 4){
		var results = http234.responseText
        results = results.trim();		
			if(results=='AG'){	
				document.getElementById("hidDestinActiveAP").style.display="block";
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value='true';		
			}else{
				document.getElementById("hidDestinActiveAP").style.display="none";
				document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value='';		
			}
		
	
}
}
function handleHttpchkPartnerAgentRUC(){
	//alert("resssssss")
	if (http234.readyState == 4){
		var results = http234.responseText
        results = results.trim();		
			if(results=='AG'){	
				document.getElementById("hidActiveAP").style.display="block";	
				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value='true';
			}else{
				document.getElementById("hidActiveAP").style.display="none";
				document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value='';	
			}
		
	
}
}
function resetContactEmail(agentType){
	if(agentType == 'OA'){
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentPhoneNumber'].value = '';
		document.getElementById("hidOriginAgentEmailImage").style.display="none";
		document.getElementById("hidActiveAP").style.display="none";
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContactImgFlag'].value = '';
	}else if(agentType == 'DA'){
		//alert("hiiiiiiiiiiiiiiiiiiiiiiiiii")
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContact'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentPhoneNumber'].value = '';
		 document.getElementById("hidDestinationAgentEmailImage").style.display="none";
		 document.getElementById("hidDestinActiveAP").style.display="none";
		 document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContactImgFlag'].value = '';
	}else if(agentType == 'SOA'){
		document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContact'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentPhoneNumber'].value = '';
		document.getElementById("hidSubOriginAgentEmailImage").style.display="none";
		document.getElementById("hidSubOrigActiveAP").style.display="none";
		document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContactImgFlag'].value = '';
	}else if(agentType == 'SDA'){
		document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentAgentContact'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentPhoneNumber'].value = '';
		document.getElementById("hidSubDestinationAgentEmailImage").style.display="none";
		document.getElementById("hidSubDestinActiveAP").style.display="none";
		document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentImgFlag'].value = '';
	}else if(agentType == 'BC'){
		document.forms['trackingStatusForm'].elements['trackingStatus.brokerContact'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.brokerEmail'].value = '';
		document.getElementById("hidBrokerEmailImage").style.display="none";
	}else if(agentType == 'FC'){
		document.forms['trackingStatusForm'].elements['trackingStatus.forwarderContact'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.forwarderEmail'].value = '';
		document.getElementById("hidForwarderEmailImage").style.display="none";
	}else if(agentType == 'BA'){
		document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentContact'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentEmail'].value = '';
		document.forms['trackingStatusForm'].elements['trackingStatus.bookingAgentPhoneNumber'].value = '';		
		document.getElementById("hidEmailImage").style.display="none";
	}
}
function getContactInfo(agentType,code)
{
	var allBookingAgentList='${allBookingAgentList}'
    if(agentType == 'OA'||agentType == 'DA'||agentType == 'SOA'||agentType == 'SDA'){
        var url="getContactInfo.html?ajax=1&decorator=simple&popup=true&originAgentInfo="+code;
	    http7.open("GET", url, true);
	    http7.onreadystatechange = function(){ handleHttpResponseDefaultContact(agentType);};
	    http7.send(null);
}}
function setContactEmail(agentType){
	var allBookingAgentList='${allBookingAgentList}'
	var userDetailList='${userDetailList}'   
	userDetailList = userDetailList.replace("[","");
	userDetailList = userDetailList.replace("]",""); 
	if(agentType == 'OA'){
		var originAgentCode = document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value 
		if(allBookingAgentList.indexOf(originAgentCode)>=0 && originAgentCode.length>4 && userDetailList!='')
        {  
		var res = userDetailList.split("~");
		var originAgentContact = res[0]+" "+ res[1]  
		var originAgentEmail =  res[2] 
		var originAgentPhoneNumber =""
			if(res[3]!='NO'){originAgentPhoneNumber=res[3]}
		if(originAgentContact!=undefined || originAgentContact!="undefined"){
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value = originAgentContact;	
		}else{
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value ="";
		}
		if(originAgentEmail!=undefined || originAgentEmail!="undefined"){
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value = originAgentEmail;	
		}else{
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value ="";
		}
		if(originAgentPhoneNumber!=undefined || originAgentPhoneNumber!="undefined"){
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentPhoneNumber'].value = originAgentPhoneNumber;	
		}else{
			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentPhoneNumber'].value ="";
		}
		//
		var con=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value ;
		con=con.trim();
		//alert(con.length);
		var email=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value ;
		var pcode=document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].value ;
		if(con!=null &&  con.length>0){			
			var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
			http234.open("GET", url, true);
			http234.onreadystatechange = handleHttpchkPartnerAgentRUC;
			http234.send(null);
		}else{
			document.getElementById("hidActiveAP").style.display="none";
		}
		//
        }else{
        	getContactInfo('OA',originAgentCode);
        } 
	}else if(agentType == 'DA'){
		var destinationAgentCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value
		if(allBookingAgentList.indexOf(destinationAgentCode)>=0 && destinationAgentCode.length>4 && userDetailList!='')
        { 
		var res = userDetailList.split("~");
		var destinationAgentContact = res[0]+" "+ res[1] 
		var destinationAgentEmail=   res[2] 
		var destinationAgentPhoneNumber =""
			if(res[3]!='NO'){destinationAgentPhoneNumber=res[3]}
		if(destinationAgentContact!=undefined || destinationAgentContact!="undefined"){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContact'].value = destinationAgentContact;	
		}else{
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContact'].value ="";
		}
		if(destinationAgentEmail!=undefined || destinationAgentEmail!="undefined"){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value = destinationAgentEmail;	
		}else{
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value ="";
		}
		if(destinationAgentPhoneNumber!=undefined || destinationAgentPhoneNumber!="undefined"){
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentPhoneNumber'].value = destinationAgentPhoneNumber;	
		}else{
			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentPhoneNumber'].value ="";
		}
		//
		var con=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContact'].value ;
		con=con.trim();
		//alert(con);
		var email=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value ;
		var pcode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].value ;
		if(con!=null &&  con.length>0){			
			var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
			http234.open("GET", url, true);
			http234.onreadystatechange = handleHttpchkDestinationPartnerAgentRUC;
			http234.send(null);
		}else{
			document.getElementById("hidDestinActiveAP").style.display="none";	
		}
		//
        }
		else{
		      getContactInfo('DA',destinationAgentCode);
        } 
	}else if(agentType == 'SOA'){
		var originSubAgentCode = document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value
		if(allBookingAgentList.indexOf(originSubAgentCode)>=0 && originSubAgentCode.length>4 && userDetailList!='')
        { 
		var res = userDetailList.split("~");
		var originSubAgentContact = res[0]+" "+ res[1] 
		var originSubAgentEmail=   res[2] 
		var originSubAgentPhoneNumber =""
			if(res[3]!='NO'){originSubAgentPhoneNumber=res[3]}  
		document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContact'].value = originSubAgentContact;
		document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value = originSubAgentEmail;
		document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentPhoneNumber'].value = originSubAgentPhoneNumber;
		//
		var con=document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContact'].value ;
		con=con.trim();
		//alert(con);
		var email=document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value ;
		var pcode=document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].value ;
		if(con!=null &&  con.length>0){			
			var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
			http234.open("GET", url, true);
			http234.onreadystatechange = handleHttpchkSAPartnerAgentRUC;
			http234.send(null);
		}else{
			document.getElementById("hidSubOrigActiveAP").style.display="none";	
		}
		//
        }
		else{
        	getContactInfo('SOA',originSubAgentCode);
        } 
	}else if(agentType == 'SDA'){
		var destinationSubAgentCode = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value
		if(allBookingAgentList.indexOf(destinationSubAgentCode)>=0 && destinationSubAgentCode.length>4 && userDetailList!='')
        { 
		var res = userDetailList.split("~");
		var destinationSubAgentContact = res[0]+" "+ res[1] 
		var destinationSubAgentEmail=   res[2]  
		var destinationSubAgentPhoneNumber =""
			if(res[3]!='NO'){destinationSubAgentPhoneNumber=res[3]}   
		document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentAgentContact'].value = destinationSubAgentContact;
		document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = destinationSubAgentEmail;
		document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentPhoneNumber'].value = destinationSubAgentPhoneNumber;
		//
		var con=document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentAgentContact'].value ;
		con=con.trim();
		//alert(con);
		var email=document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value ;
		var pcode=document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value ;
		if(con!=null &&  con.length>0){			
			var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
			http234.open("GET", url, true);
			http234.onreadystatechange = handleHttpchkDAPartnerAgentRUC;
			http234.send(null);
		}else{
			document.getElementById("hidSubDestinActiveAP").style.display="none";	
		}
		//
        }
		else{
        	getContactInfo('SDA',destinationSubAgentCode);
        } 
	} 
}
</script>
<script language="javascript">
function checkEmail(fieldId) {
var email = document.forms['trackingStatusForm'].elements[fieldId];
var filter = /^([a-zA-Z0-9_\.\-\'])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (!filter.test(email.value)) {
		alert('Please provide a valid email address');
		document.forms['trackingStatusForm'].elements[fieldId].value='';
		document.forms['trackingStatusForm'].elements[fieldId].focus();
		return false;
	} }
openDiv();
function populateAgentCodeAlert(agentType,oADADivName1){
	var agent='${oADAAgentCheck}';
	var agentCode='';
	var formAgentCode='';
	if(agentType=='OA'){
	agent=agent.split("~")[0];
	agentCode='${trackingStatus.originAgentCode}';
	formAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.originAgentCode"].value;
	if(agent!="F" && agent==agentCode && agentCode!='' && formAgentCode==''){
		alert('Agent code cannot be removed from OA/DA as account line for the same exists, it is required to deactivate the account line before removing the agent code from the status page');
		var oADADivName=document.getElementById(oADADivName1);
			oADADivName.style.display = "none";
		document.forms["trackingStatusForm"].elements["trackingStatus.originAgentCode"].value='${trackingStatus.originAgentCode}';
		document.forms["trackingStatusForm"].elements["trackingStatus.originAgent"].value="${trackingStatus.originAgent}";
		document.forms["trackingStatusForm"].elements["trackingStatus.originAgentContact"].value='${trackingStatus.originAgentContact}';
		document.forms["trackingStatusForm"].elements["trackingStatus.originAgentEmail"].value="${trackingStatus.originAgentEmail}";
		document.forms["trackingStatusForm"].elements["trackingStatus.originAgentPhoneNumber"].value='${trackingStatus.originAgentPhoneNumber}';
	}else{
		checkVendorName();
	}
	}else if(agentType=='DA'){
		agent=agent.split("~")[1];
		agentCode='${trackingStatus.destinationAgentCode}';
		formAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentCode"].value;
		if(agent!="F" && agent==agentCode && agentCode!='' && formAgentCode==''){
			alert('Agent code cannot be removed from OA/DA as account line for the same exists, it is required to deactivate the account line before removing the agent code from the status page');
			var oADADivName=document.getElementById(oADADivName1);
			oADADivName.style.display = "none";
			document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentCode"].value='${trackingStatus.destinationAgentCode}';
			document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgent"].value="${trackingStatus.destinationAgent}";
			document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentContact"].value="${trackingStatus.destinationAgentContact}";
			document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentEmail"].value="${trackingStatus.destinationAgentEmail}";
			document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentPhoneNumber"].value='${trackingStatus.destinationAgentPhoneNumber}';
			
	}else{
		checkVendorNameDest();
		}
	}
}
/*function popupAgentCodeAlert(agentType){
	var agent='${oADAAgentCheck}';
	var agentCode='';
	var formAgentCode='';
	if(agentType=='OA'){
	agent=agent.split("~")[0];
	agentCode='${trackingStatus.originAgentCode}';
	formAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.originAgentCode"].value;
	if(agent!="F" && agent==agentCode && agentCode!='' && formAgentCode==''){
		alert('Agent code cannot be removed from OA/DA as account line for the same exists, it is required to deactivate the account line before removing the agent code from the status page');
	}else{
		javascript:winOpenOrigin();
	}
	}else if(agentType=='DA'){
		agent=agent.split("~")[1];
		formAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentCode"].value;
		agentCode='${trackingStatus.destinationAgentCode}';
		if(agent!="F" && agent==agentCode && agentCode!='' && formAgentCode==''){
			alert('Agent code cannot be removed from OA/DA as account line for the same exists, it is required to deactivate the account line before removing the agent code from the status page');
		}else{
			javascript:winOpenDest();
		}	
}
	}*/
<configByCorp:fieldVisibility componentId="component.trackingStatus.copyOADADetail">
copyOADADetail('OA');
copyOADADetail('DA');
function agentDetailTransfer(agentType){
	var agentCode='';
	var agent='';
	var agentContact='';
	var agentEmail='';
	var agentPhoneNumber='';
	var copyType='';
	var saveAgentCode='';
	var saveAgent='';
	var saveAgentContact='';
	var saveAgentEmail='';
	var saveAgentPhoneNumber='';
	if(agentType=='OA'){
	agentCode=document.forms["trackingStatusForm"].elements["trackingStatus.originAgentCode"].value;
	agent=document.forms["trackingStatusForm"].elements["trackingStatus.originAgent"].value;
	agentContact=document.forms["trackingStatusForm"].elements["trackingStatus.originAgentContact"].value;
	agentEmail=document.forms["trackingStatusForm"].elements["trackingStatus.originAgentEmail"].value;
	agentPhoneNumber=document.forms["trackingStatusForm"].elements["trackingStatus.originAgentPhoneNumber"].value;
	saveAgentCode='${trackingStatus.originAgentCode}';
	saveAgent="${trackingStatus.originAgent}";
	saveAgentContact='${trackingStatus.originAgentContact}';
	saveAgentEmail='${trackingStatus.originAgentEmail}';
	saveAgentPhoneNumber='${trackingStatus.originAgentPhoneNumber}';
	copyType='origin';
	}else if(agentType=='DA'){
		agentCode=document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentCode"].value;
		agent=document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgent"].value;
		agentContact=document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentContact"].value;
		agentEmail=document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentEmail"].value;
		agentPhoneNumber=document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentPhoneNumber"].value;
		saveAgentCode='${trackingStatus.destinationAgentCode}';
		saveAgent="${trackingStatus.destinationAgent}";
		saveAgentContact="${trackingStatus.destinationAgentContact}";
		saveAgentEmail="${trackingStatus.destinationAgentEmail}";
		saveAgentPhoneNumber='${trackingStatus.destinationAgentPhoneNumber}';
		copyType='destination';
	}	
	if(agentCode!=saveAgentCode || agent!=saveAgent || agentContact!=saveAgentContact || agentEmail!=saveAgentEmail || agentPhoneNumber!=saveAgentPhoneNumber ){
		alert('The page has not been saved yet, please save first to continue');
	}else{
	openWindow("addAndCopyAgentDetail.html?cid=${customerFile.id}&id=${serviceOrder.id}&copyType="+copyType+"&decorator=popup&popup=true");
	}	
}
</configByCorp:fieldVisibility>

function checkFromDocumentBundle(fieldName,position,clickedField){
	var job = '${serviceOrder.job}';
	var serviceType = '${serviceOrder.serviceType}';
	var serviceMode = '${serviceOrder.mode}';
	var routingType = '${serviceOrder.routing}';
	var jobNumber = '${serviceOrder.shipNumber}';
	var sequenceNumber = '${serviceOrder.sequenceNumber}';
	var firstName ="${serviceOrder.firstName}";
	var lastName = "${serviceOrder.lastName}";
	var url="getRecordFromDocumentBundleAjax.html?ajax=1&decorator=simple&popup=true&routingType="+routingType+"&serviceType="+serviceType+"&serviceMode="+serviceMode+"&job="+job+"&fieldName="+fieldName+"&jobNumber="+jobNumber+"&sequenceNumber="+sequenceNumber+"&firstName="+firstName+"&lastName="+lastName+"&clickedField="+clickedField;
	ajax_showTooltip(url,position);		
	}
function documentBundleWinOpen(emailValue,shipNumber,firstName,lastName,formsId,dbId,sequenceNumber,clickedField){
	var receipientTo = emailValue;
	var recipientCC = '';
	var url = "openEmailPopUp.html?decorator=popup&popup=true&receipientTo="+receipientTo+"&recipientCC="+recipientCC+"&jobNumber="+shipNumber+"&firstName="+firstName+"&lastName="+lastName+"&reportsId="+formsId+"&dbId="+dbId+"&sequenceNumber="+sequenceNumber+"&clickedField="+clickedField;
	window.openWindow(url,height=800,width=600);
}
function findPartnerDetailsNew(e,t,n,r,i,s){
	var billtoCode='${billing.billToCode}';
	var o=s.which?s.which:s.keyCode;
	if(o!=9){
		if(i!=""){
			r=r+" and (companyDivision='"+i+"' or companyDivision='' or companyDivision is null)"
			}
		var u=document.getElementById(e).value;
		if(u.length<=3){
			document.getElementById(t).value=""
			}if(u.length>=3){
				$.get("partnerDetailsAutocompleteAjaxNew.html?ajax=1&decorator=simple&popup=true",
						{
							partnerNameAutoCopmlete:u,
							partnerNameId:e,
							paertnerCodeId:t,
							autocompleteDivId:n,
							autocompleteCondition:r,
							billToCode:billtoCode
					},function(e){
						document.getElementById(n).style.display="block";
						$("#"+n).html(e)})
						}
			else{
				document.getElementById(n).style.display="none"
				}
			}else{
				document.getElementById(n).style.display="none"
				}
	}

function copyPartnerDetailsNew(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
	lastName=lastName.replace("~","'");
	document.getElementById(partnerNameId).value=lastName;
	document.getElementById(paertnerCodeId).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";	
	if(paertnerCodeId=='networkPartnerCodeId'){
		checkNetworkPartnerName();
	}else if(paertnerCodeId=='trackingOriginAgentCodeId'){
		resetContactEmail('OA');
		checkVendorName();
		chkIsRedSky();
	}else if(paertnerCodeId=='trackingDestinationAgentCodeId'){
		resetContactEmail('DA');
		checkVendorNameDest();
		chkIsDestinRedSky();
	}else if(paertnerCodeId=='trackingOriginSubAgentCodeId'){
		resetContactEmail('SOA');
		checkVendorNameSub();
		chkIsOSubRedSky();
	}else if(paertnerCodeId=='trackingDestinationSubAgentCodeId'){
		resetContactEmail('SDA');
		checkVendorNameDestSub();
         chkIsDSubRedSky();
	}else if(paertnerCodeId=='trackingBrokerCodeId'){
		resetContactEmail('BC');
		checkBroker();
		okValidateListFromBroker();
	}else if(paertnerCodeId=='trackingforwarderCodeId'){
		resetContactEmail('FC');
		checkForwarderName();
		checkForwarder();
		okValidateListFromForwarder();
	}
}
function closeMyDivNew(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
	}
function validPreferredAgentSubDA(){
    var vendorId = document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value;
    if(vendorId == ''){
    	document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value='';
    }
    if(vendorId!=''){ 
	    var url="checkPreferredAgentCode.html?ajax=1&decorator=simple&popup=true&billToCodeAcc=${billing.billToCode}&partnerCode=" + encodeURI(vendorId);
	    http2SubDA.open("GET", url, true);
	    http2SubDA.onreadystatechange = handleHttpResponseSubDA;
	    http2SubDA.send(null);
    }
} 
function handleHttpResponseSubDA(){
	if (http2SubDA.readyState == 4){
                var results = http2SubDA.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>1){
                	if(res[2] == 'Y'){
	           				document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
                		
	           		}else{
	           			/* alert("Selected Sub Destination Agent is not part of Preferred  agent list. Kindly select another agent." ); 
	           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";
					    document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = "";
					    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select(); */

	           		}
               	}else{
               		alert("Sub Destination Agent code not valid" ); 
               		document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgent'].value="";
				    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].value="";
				    document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = "";
				    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();

			   }    } 
	//getEmailFromPortalUserSubDa();
	}
			   
function disableEnterKey(e){ 
	var key; 
	    if(window.event){ 
	    key = window.event.keyCode; 
	    } else { 
	    key = e.which;      
	    } 
	   // alert("ganesh--"+key)
	    if(key == 13){ 
	    return false; 
	    } else { 
	    return true; 
	    } 
	} 
	
function getEmailFromPortalUserSubDa(){
	var SubDaAgentCode="";
	
	SubDaAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.destinationSubAgentCode"].value;
	
    if(SubDaAgentCode!=''){ 
	    var url="getEmailFromPortalUserAjax.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(SubDaAgentCode);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponseGetEmailSubDa;
	    http2.send(null);
    }
}
 function handleHttpResponseGetEmailSubDa(){
		if (http2.readyState == 4){
	                var results = http2.responseText
	                results = results.trim();
	                var res = results.split("#");
	                if(res.length>1){
	                		if(res[1]!=''){
			           			document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value =res[1];
						 	    document.forms['trackingStatusForm'].elements['trackingStatus.destinationSubAgentCode'].select();
	                			return true;
	                			
	                			
	                		}else{
	                			
	                		}
	                		
		           		}else{
		           			
		           		} 
	               	} }
 
 function getEmailFromPortalUserSubOa(){
		var SuboriginAgentCode="";
		
		SuboriginAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.originSubAgentCode"].value;
		
	    if(SuboriginAgentCode!=''){ 
		    var url="getEmailFromPortalUserAjax.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(SuboriginAgentCode);
		    http2.open("GET", url, true);
		    http2.onreadystatechange = handleHttpResponseGetEmailSubOa;
		    http2.send(null);
	    }
	}
	 function handleHttpResponseGetEmailSubOa(){
			if (http2.readyState == 4){
		                var results = http2.responseText
		                results = results.trim();
		                var res = results.split("#");
		                if(res.length>1){
		                		if(res[1]!=''){
				           			document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value =res[1];
							 	    document.forms['trackingStatusForm'].elements['trackingStatus.originSubAgentCode'].select();
		                			return true;
		                			
		                			
		                		}else{
		                			
		                		}
		                		
			           		}else{
			           			
			           		} 
		               	} }
	 
	 function getEmailFromPortalUser(agentType){
			var originAgentCode="";
			if(agentType=="OA"){
				 originAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.originAgentCode"].value;
			}
			if(agentType=="DA"){
				 originAgentCode=document.forms["trackingStatusForm"].elements["trackingStatus.destinationAgentCode"].value;
			}
		    if(originAgentCode!=''){ 
			    var url="getEmailFromPortalUserAjax.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(originAgentCode);
			    http2.open("GET", url, true);
			    http2.onreadystatechange = function() {handleHttpResponseGetEmail(agentType);};
			    http2.send(null);
		    }
		}
		 function handleHttpResponseGetEmail(agentType){
				if (http2.readyState == 4){
			                var results = http2.responseText
			                results = results.trim();
			                var res = results.split("#");
			                if(res.length>1){
			                		if(res[1]!=''){
			                			if(agentType=="OA"){
					           			document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value =res[1];
								 	    document.forms['trackingStatusForm'].elements['trackingStatus.originAgentCode'].select();
			                			return true;
			                			}
			                			if(agentType=="DA"){
						           			document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value =res[1];
									 	    document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentCode'].select();
				                			return true;
				                			}
			                		}else{
			                			
			                		}
			                		
				           		}else{
				           			
				           		} 
			               	} }
       
       function handleHttpResponseDefaultContact(agentType) {  
    	  
    	  if (http7.readyState == 4) {
    		       var results = http7.responseText ;  
		        	results = results.trim();   
	                var res = results.split("~"); 
	              if(res.length>1 )
	            	   {
	            	   if(agentType=='OA')
	            		 {            		   
	                   if(res[0]!=undefined  && res[0]!="undefined" &&  res[0] !="" ){
	                	   document.forms['trackingStatusForm'].elements['trackingStatus.originAgentContact'].value = res[0];
	           		      }
	           		 if(res[1]!=undefined && res[1]!="undefined" &&  res[1] !=""){
	                	   document.forms['trackingStatusForm'].elements['trackingStatus.originAgentEmail'].value = res[1];
	           		      }	           		
	           		if(res[2]!=undefined && res[2]!="undefined" && res[2]!="" ){
	                	   document.forms['trackingStatusForm'].elements['trackingStatus.originAgentPhoneNumber'].value = res[2];
	           		     }}
	            	   else if(agentType=='DA')
	            		   {
	           		if(res[0]!=undefined && res[0]!="undefined" && res[0]!=""){
	                	   document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentContact'].value = res[0];
	           		       }
	           		if(res[1]!=undefined && res[1]!="undefined" && res[1]!=""){
	                	   document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentEmail'].value = res[1];
	           		       }
	           		if(res[2]!=undefined  && res[2]!="undefined" && res[2]!="" ){
	                	   document.forms['trackingStatusForm'].elements['trackingStatus.destinationAgentPhoneNumber'].value = res[2];
	           		      }}
	            	   else  if(agentType=='SOA')
            		   {
           		if(res[0]!=undefined  && res[0]!="undefined" && res[0]!=""){
                	   document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentContact'].value = res[0];
           		       } 
           		if(res[1]!=undefined  && res[1]!="undefined" && res[1]!=""){
                	   document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentEmail'].value = res[1];
           		       }
           		if(res[2]!=undefined  && res[2]!="undefined" && res[2]!="" ){
                	   document.forms['trackingStatusForm'].elements['trackingStatus.subOriginAgentPhoneNumber'].value = res[2];
           		      }}
	            	   else  if(agentType=='SDA')
            		   {
           		if(res[0]!=undefined  && res[0]!="undefined" && res[0]!=""){
                	   document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentAgentContact'].value = res[0];
           		       }
           		if(res[1]!=undefined  && res[1]!="undefined" && res[1]!=""){
                	   document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentEmail'].value = res[1];
           		       }
           		if(res[2]!=undefined   && res[2]!="undefined" && res[2]!="" ){
                	   document.forms['trackingStatusForm'].elements['trackingStatus.subDestinationAgentPhoneNumber'].value = res[2];
           		      }}
		         }   }} 		
		
</script>