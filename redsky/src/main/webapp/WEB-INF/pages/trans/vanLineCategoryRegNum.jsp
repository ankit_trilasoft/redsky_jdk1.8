<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title>vanLine List</title>   
    <meta name="heading" content="vanLine List"/>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;
margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;

}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   
<s:form id="vanForm" name="vanStCateForm" method="post" validate="true" >
<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
<configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
	<s:hidden name="glCodeFlag" value="YES"/>
</configByCorp:fieldVisibility>
<!--<table class="detailTabLabel" cellpadding="1" cellspacing="1" border="0" style="width:100%;">
<tr>
 <td align="left" class="listwhitetext" ><b>Order List</b></td>
	<td align="right"  style="padding:3px 5px 0px 0px;">
		<img align="right" class="openpopup" onclick="hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
--><display:table name="vanLineRegNumList" class="table" requestURI="" id="vanLineRegNumList" pagesize="100" style="width:100%;margin-bottom:0px;" partialList="true" size="100" defaultsort="1" defaultorder="ascending"> 
   <%-- <c:choose>
	<c:when test="${vanLineRegNumList.regNum!='' && vanLineRegNumList.regNum!=null}">	
	<display:column property="regNum"  title="Reg #" style="width:22%;"/>	
	</c:when>
	<c:otherwise>
	<display:column property="distributionCodeDescription"  titleKey="vanLine.description" style="width:22%;"/>
	</c:otherwise>
	</c:choose> --%>
    <c:if test="${vanLineRegNumList.regNum!=null &&  vanLineRegNumList.regNum!=''  && (vanLineRegNumList.statementCategory=='DSTR')}">
    <display:column  style="width:5%">	
	<img id="reg-${vanLineRegNumList_rowNum}" onclick ="displayResultList('${vanLineRegNumList.regNum}','${vanLineRegNumList.statementCategory}','${vanLineRegNumList.agent}','${vanLineRegNumList.weekEnding}',this,this.id);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	</display:column>
	<c:if test="${vanLineRegNumList.soId!=''}">
    <c:choose>
    <c:when test="${newAccountline=='Y'}">
    <display:column  style="width:8%;" title="Reg #">
    <a href="javascript:;" onclick="window.open('pricingList.html?sid=${vanLineRegNumList.soId}')" ><c:out value="${vanLineRegNumList.regNum}" /></a> 
    </display:column>
    </c:when>
    <c:otherwise>
    <display:column  style="width:8%;" title="Reg #">
    <a href="javascript:;" onclick="window.open('accountLineList.html?sid=${vanLineRegNumList.soId}')" ><c:out value="${vanLineRegNumList.regNum}" /></a> 
    </display:column>
    </c:otherwise>
    </c:choose>
    </c:if>    
    <c:if test="${vanLineRegNumList.soId==''}">
    <display:column  style="width:8%;" title="Reg #">
   <c:out value="${vanLineRegNumList.regNum}" /> 
    </display:column>
    </c:if> 
    </c:if>
    <c:if test="${vanLineRegNumList.regNum!=null &&  vanLineRegNumList.regNum!='' && (vanLineRegNumList.statementCategory=='MCLM')}">    
    <c:if test="${vanLineRegNumList.soId!=''}">
     <c:choose>
    <c:when test="${newAccountline=='Y'}">
    <display:column style="width:8%;" title="Reg #">  
    <a href="javascript:;" onclick="window.open('pricingList.html?sid=${vanLineRegNumList.soId}')" ><c:out value="${vanLineRegNumList.regNum}" /></a> 
    </display:column>
    </c:when>
    <c:otherwise>
    <display:column style="width:8%;" title="Reg #">  
    <a href="javascript:;" onclick="window.open('accountLineList.html?sid=${vanLineRegNumList.soId}')" ><c:out value="${vanLineRegNumList.regNum}" /></a> 
    </display:column>
    </c:otherwise>
    </c:choose>  
    </c:if>
    <c:if test="${vanLineRegNumList.soId==''}">
    <display:column style="width:8%;" title="Reg #">  
    <c:out value="${vanLineRegNumList.regNum}" /> 
    </display:column>
    </c:if>
    </c:if>
    <display:column property="tranDate"  title="Transaction Date" style="width:8%" format="{0,date,dd-MMM-yyyy}"/>	
	<c:if test="${vanLineRegNumList.statementCategory!=null &&  vanLineRegNumList.statementCategory!='' && vanLineRegNumList.statementCategory!='DSTR'}">
	<c:choose>
	<c:when test="${vanLineRegNumList.description!='' && vanLineRegNumList.description!=null}">	
	<display:column property="description"  titleKey="vanLine.description" style="width:37%;"/>	
	</c:when>
	<c:otherwise>
	<display:column property="distributionCodeDescription"  titleKey="vanLine.description" style="width:37%;"/>
	</c:otherwise>
	</c:choose>
	</c:if>
	 <c:if test="${vanLineRegNumList.statementCategory!=null &&  vanLineRegNumList.statementCategory!='' && (vanLineRegNumList.statementCategory!='DSTR' && vanLineRegNumList.statementCategory!='MCLM')}">
	<display:column property="driverName" style="width:20%;" titleKey="vanLine.driverName"/>
	</c:if>	
	 <c:if test="${vanLineRegNumList.statementCategory!=null &&  vanLineRegNumList.statementCategory!='' && (vanLineRegNumList.statementCategory=='MCLM' || vanLineRegNumList.statementCategory=='DSTR')}">
	<display:column property="driverName" style="width:20%;" titleKey="vanLine.driverName"/>
	</c:if>
	<c:if test="${vanLineRegNumList.statementCategory!=null &&  vanLineRegNumList.statementCategory!='' && (vanLineRegNumList.statementCategory=='DSTR' || vanLineRegNumList.statementCategory=='MCLM')}">
	<display:column property="shipper" style="width:20%;" title="Shipper"/> 
	</c:if>
	<c:if test="${vanLineRegNumList.statementCategory=='DSTR'}">
	<display:column property="reconcileStatus"  title="Reconcile Status" />
	</c:if>
	<c:choose>
	<c:when test="${(vanLineRegNumList.statementCategory!='DSTR' && vanLineRegNumList.statementCategory!='BALF')}">
	<display:column titleKey="vanLine.amtDueAgent" style="text-align: right; width:12%;">
	<a href="javascript:;" onclick="setUrl(this,${vanLineRegNumList.id})" ><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${vanLineRegNumList.amtDueAgent}"  /></a> 
	</display:column>
	</c:when>
	<c:otherwise>
	<display:column  titleKey="vanLine.amtDueAgent" style="text-align: right; width:12%;">
<%--	<a href="javascript:;" onclick="window.open('editVanLine.html?id=${vanLineRegNumList.id}')" ><c:out value="${vanLineRegNumList.amtDueAgent}" /></a> --%> 
<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${vanLineRegNumList.amtDueAgent}"  />
	</display:column>
	</c:otherwise>
	</c:choose>
	<display:column   titleKey="vanLine.reconcileAmount" style="text-align: right; width:11%;">	
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${vanLineRegNumList.reconcileAmount}"  />
	</display:column>
	<c:if test="${(vanLineRegNumList.statementCategory!='DSTR' && vanLineRegNumList.statementCategory!='BALF')}">
	<display:column property="reconcileStatus"  titleKey="vanLine.reconcileStatus" style="width:12%;"/>	
	</c:if>
 <%-- <c:if test="${vanLineRegNumList.statementCategory!=null &&  vanLineRegNumList.statementCategory!='' && (vanLineRegNumList.statementCategory=='DSTR'|| vanLineRegNumList.statementCategory=='MCLM')}">
	<display:column property="glType"  titleKey="vanLine.glType" style="width:8%;"/> 
	</c:if>  --%>
   <%-- <display:column property="glType"  titleKey="vanLine.glType" style="width:8%;"/> --%>
	<%-- <display:column property="reconcileStatus"  titleKey="vanLine.reconcileStatus" style="width:12%;"/> --%>
	<%--  <c:if test="${vanLineRegNumList.statementCategory!=null &&  vanLineRegNumList.statementCategory!='' && (vanLineRegNumList.statementCategory!='DSTR' && vanLineRegNumList.statementCategory!='MCLM')}">
	<display:column property="driverName" style="width:15%;" titleKey="vanLine.driverName"/>
	</c:if>	
	<c:if test="${vanLineRegNumList.statementCategory!=null &&  vanLineRegNumList.statementCategory!='' && vanLineRegNumList.statementCategory!='DSTR'}">
	<c:choose>
	<c:when test="${vanLineRegNumList.description!='' && vanLineRegNumList.description!=null}">	
	<display:column property="description"  titleKey="vanLine.description" style="width:22%;"/>	
	</c:when>
	<c:otherwise>
	<display:column property="distributionCodeDescription"  titleKey="vanLine.description" style="width:22%;"/>
	</c:otherwise>
	</c:choose>
	</c:if> --%>
</display:table>
</s:form>