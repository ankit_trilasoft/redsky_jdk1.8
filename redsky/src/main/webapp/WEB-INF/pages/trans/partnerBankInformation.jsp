 <%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
	<head>   
	    <title>BANK INFO LIST</title>   
	    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
	</head> 
			<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;font-size:11px;">
			<tr valign="top"> 	
				<td align="left"><b>Add Bank Information</b></td>
				<td align="right"  style="width:30px;">
				<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
			</tr>
			</table>   

				    <s:form name="partnerBankInfoDetail" method="post" >
					 <s:hidden name="bankPartnerCode" value="<%=request.getParameter("bankPartnerCode") %>"></s:hidden>
					<div id="layer1" style="width:100%">					 
					<div style="width:50px;margin-bottom:5px;" class="spn">&nbsp;</div>
					<s:set name="partnerBankInfoList" value="partnerBankInfoList" scope="request"/>

  					<table class="table"  id="dataTable" style="margin:0px;width:auto;">
					 <tr>
					 <td align="right" class="listwhitetext" style="min-width:77px;">Currency</td>
					 <td align="right" class="listwhitetext" style="min-width:120px;">Bank Account #</td>
					 <td align="right" class="listwhitetext" style="min-width:30px;">Status</td>		
					 </tr>		
					 <c:if test="${partnerBankInfoList!='[]'}">
					 <c:forEach var="individualItem" items="${partnerBankInfoList}" >
					  <tr>
						 <td>
						 <c:if test="${agentCheck==true || agentCheck=='true'}">
						 <s:select cssClass="list-menu" list="%{billingCurrency}"  value="'${individualItem.currency}'"  name="currencyList" id="cur${individualItem.id}" headerKey="" headerValue="" onchange="updateBankRecord('${individualItem.id}')" cssStyle="width:60px;"/> 
						 </c:if>
						 <c:if test="${agentCheck!=true && agentCheck!='true'}">
						 <s:select cssClass="list-menu" list="%{billingCurrency}"  value="'${individualItem.currency}'"  name="currencyList" id="cur${individualItem.id}" headerKey="" headerValue="" disabled="true" cssStyle="width:60px;"/>
						 </c:if>
						 </td> 
				   		<td>
				   		<c:if test="${agentCheck==true || agentCheck=='true'}">
				   		<input type="text" value="${individualItem.bankAccountNumber}" class="input-text" name="bankNoList" id="ban${individualItem.id}" onchange="updateBankRecord('${individualItem.id}')" maxlength="20" style="width: 100px;"/> 
				   		</c:if>
				   		<c:if test="${agentCheck!=true && agentCheck!='true'}">
				   		<input type="text" value="${individualItem.bankAccountNumber}" class="input-text" name="bankNoList" id="ban${individualItem.id}" readonly="true" style="width: 100px;"/>
				   		</c:if>
				   		</td>
				   		
				   		
				   	 <td>
				   	 <c:if test="${agentCheck==true || agentCheck=='true'}">
				   	 <c:if test="${individualItem.status==true}">
				   	 <input value="${individualItem.status}" id="chk${individualItem.id}" style="width: 30px;" name="checkList" onclick="updateBankRecord('${individualItem.id}')"  type="checkbox" checked="checked">
				   	 </c:if>
				   	 <c:if test="${individualItem.status!=true}">
				   	 <input value="${individualItem.status}" id="chk${individualItem.id}" style="width: 30px;" name="checkList" onclick="updateBankRecord('${individualItem.id}')"  type="checkbox">
				   	 </c:if>
				   	 </c:if>
				   	 <c:if test="${agentCheck!=true && agentCheck!='true'}">
				   	 
				   	 <c:if test="${individualItem.status==true}">
				   	 <input value="${individualItem.status}" id="chk${individualItem.id}" style="width: 30px;" name="checkList" disabled="true"  type="checkbox" checked="checked">
				   	 </c:if>
				   	 <c:if test="${individualItem.status!=true}">
				   	 <input value="${individualItem.status}" id="chk${individualItem.id}" style="width: 30px;" name="checkList" disabled="true" type="checkbox">
				   	 </c:if>
				   	 
				   	 </c:if>
				   	 </td> 
					 </tr>					 
					 </c:forEach>
				   </c:if> 
				   </table>
				   <c:if test="${agentCheck==true || agentCheck=='true'}">
					<table style="margin:0px;">
					   <tr>
 							<td><input type="button" class="cssbutton1" id="addLine" name="save&addLine" style="font-size: 11px; height: 23px; width: 100px;" value="Save & AddLine" onclick="addRow('dataTable','onlysave');" /></td>
 							<td><input type="button" class="cssbutton1" id="save" name="save" style="font-size: 11px; height: 23px; width: 44px;" value="Save" onclick="addRow('dataTable','');" /></td>
 						</tr>
 					</table>
 					</c:if>
				</div>
		</s:form>
<script language="javascript" type="text/javascript">

</script>