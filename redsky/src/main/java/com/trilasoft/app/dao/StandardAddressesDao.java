package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.StandardAddresses;

public interface StandardAddressesDao extends GenericDao<StandardAddresses, Long> {  
	
	public List searchByDetail(String job,String addressLine1,String city,String countryCode,String state); 
	public List getByJobType(String jobType); 
	public List searchByDetailForPopup(String jobType, String addressLine1, String city, String countryCode, String state);
	public List getAddressLine1(String jobType, String corpid);
	public StandardAddresses getStandardAddressDetail(String residenceName);
}
