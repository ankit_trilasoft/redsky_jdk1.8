package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

public interface AppInitServletDao extends GenericDao {

	public void populatePermissionCache(Map  pageActionUrls);

	public void populateRolebasedComponentPermissions(Map permissionMap);
	public List getListForSurveyCalander(String corpid,String fmdt,String todt,String consult,String surveycity,String job);
	public void populatePageFieldVisibilityPermissions(Map pageFieldVisibilityComponentMap);

}
