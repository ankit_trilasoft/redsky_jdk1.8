package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CoraxLog;

public interface CoraxLogManager extends GenericManager<CoraxLog, Long>  {

	public List getByCorpID( String sessionCorpID);
}
