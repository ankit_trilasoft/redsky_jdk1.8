<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Standard Deductions Preview List</title>   
    <meta name="heading" content="Standard Deductions Preview List"/>  
    
<style>
.tab{
border:1px solid #74B3DC;
}
span.pagelinks {
margin-top:-13px;
}
</style> 
<script language="javascript" type="text/javascript">
function userStatusCheck(target){
		var targetElement = target;
		var ids = targetElement.value;
		if(targetElement.checked){
      		var userCheckStatus = document.forms['standardDeductionListForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['standardDeductionListForm'].elements['userCheck'].value = ids;
      		}else{
      			var userCheckStatus=	document.forms['standardDeductionListForm'].elements['userCheck'].value = userCheckStatus + ',' + ids;
      			document.forms['standardDeductionListForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
      	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['standardDeductionListForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['standardDeductionListForm'].elements['userCheck'].value = userCheckStatus.replace( ids , '' );
     		document.forms['standardDeductionListForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
     	}
}


function fieldValidate(){
   if(document.forms['standardDeductionListForm'].elements['userCheck'].value == '' || document.forms['standardDeductionListForm'].elements['userCheck'].value == ','){
      	alert("Please select atleast one driver for finalize the deductions."); 
     	return false;
   }  
}
		
</script>
</head>
<s:form id="standardDeductionListForm" action="finalizeDeduction" method="post" validate="true">
<s:hidden name="userCheck"/>
<s:hidden name="userCheckFile"/>
<c:if test="${not empty fromDate}">
	<fmt:formatDate var="fromDateFormattedValue" value="${fromDate}" pattern="${displayDateTimeFormat}"/>
	<s:hidden name="fromDate" value="${fromDateFormattedValue}"/>
</c:if>	
<s:hidden name="companyDivision"/>
<div id="newmnav">
	  <ul>
	  	<li><a href="deductionProcess.html"><span>Standard Deductions</span></a></li>
	    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Standard Deductions Preview</span></a></li>
	  </ul>
</div>
<div style="width: 100%" >
	<div class="spn">&nbsp;</div>
</div>
<div id="Layer1" style="width: 100%;">
<c:set var="buttons">   
    <td colspan="3"><s:submit cssClass="cssbutton" cssStyle="width:155px; height:25px" align="top" value="Finalize Deductions" onclick="return fieldValidate();"/></td>
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td >				
				<s:set name="standardDeductionPreview" value="standardDeductionPreview" scope="request"/>  
				<display:table name="standardDeductionPreview" class="table" requestURI="" id="standardDeductionPreviewList" export="true" defaultsort="2" defaultorder="descending" pagesize="50" style="width: 100%; " >   
					<display:column title=" " group="1"><input type="checkbox" style="margin-left:5px;" id="checkboxId" name="DD" value="${standardDeductionPreviewList.code}" onclick="userStatusCheck(this)"/></display:column>
					<display:column property="code" sortable="true" style="width: 60px;" title="Account"/>
					<display:column property="name" sortable="true" title="Account Name" style="width: 170px;"/>
					<display:column property="tractTrailer" sortable="true" title="Tractor/Trailer" style="width: 170px;"/>
					<display:column property="registrationNo" sortable="true" title="Reg. Number"/>
					<display:column property="shipNum" sortable="true" title="S/O #"/>
					<display:column property="payPostDate" sortable="true" title="Posted" style="width:85px"/>
					<display:column property="chargeCode" sortable="true" style="width: 50px;" title="Rev.Code" />
					<display:column property="branch" sortable="true" title="Agency" />
					<display:column property="approvedDate" sortable="true" title="Approved Date" style="width:85px"/>
					
					<c:choose>
						<c:when test='${standardDeductionPreviewList.balanceForward == true && standardDeductionPreviewList.tempFlag == false}'>
							<display:column sortable="true" title="Description" style="width: 180px;">
								<c:out value="Std. Ded.: Previous Carry Forward Balanace"></c:out>
							</display:column>
						</c:when>
						<c:otherwise>
							<display:column sortable="true" title="Description" style="width: 180px;">
								<c:out value="${standardDeductionPreviewList.description}"></c:out>
							</display:column>
						</c:otherwise>
					</c:choose>
					
					<display:column sortable="true" title="Owe To" headerClass="containeralign">
						<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${standardDeductionPreviewList.actualExpense}" /></div>
                  	</display:column>
                  	
					<display:column sortable="true" title="Amount" headerClass="containeralign">
						<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${standardDeductionPreviewList.amount}" /></div>
                  	</display:column>
					
				    <display:setProperty name="paging.banner.item_name" value="Standard Deductions Preview"/>   
				    <display:setProperty name="paging.banner.items_name" value="Standard Deductions Preview"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Standard Deductions Preview List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Standard Deductions Preview List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Standard Deductions Preview List.pdf"/>   
				</display:table>  
			</td>
		</tr>
	</tbody>
</table> 
<c:if test="${process=='preview'}">
	<c:out value="${buttons}" escapeXml="false" />
</c:if>
<table>
	<tr>
		<td style="height:70px; !height:100px"></td>
	</tr>
</table>
</div>
</s:form> 
