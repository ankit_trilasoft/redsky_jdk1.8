package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@Entity
@Table(name="pricingcontrol")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class PricingControl {
	
	private Long id;
	private String corpID;
	private String pricingID;
	private String sequenceNumber;
	private String shipNumber;
	private Long serviceOrderId;
	private String originAddress1;
	private String originCity;
	private String originState;
	private String originZip;
	private String originCountry;
	private String originLatitude;
	private String originLongitude;
	private String originPOE;
	private String originPOEName;
	
	private String destinationAddress1;
	private String destinationCity;
	private String destinationState;
	private String destinationZip;
	private String destinationCountry;
	private String destinationLatitude;
	private String destinationLongitude;
	private String destinationPOE;
	private String destinationPOEName;
	
	private Boolean isOrigin;
	private Boolean isFreight;
	private Boolean isDestination;
	
	private String mode;
	private BigDecimal weight;
	private BigDecimal volume;
	private String unitType;
	private String packing;
	private String containerSize;
	private String lclFclFlag;
	private String groupageFlag;
	private Date expectedLoadDate;
	
	private String contract;
	private String baseCurrency;
	private String originPortRange;
	private String destinationPortRange;
	
	private String customerName;
	private String parentAgent;
	private String scope;
	private Boolean dthcFlag;
	private Boolean freightMarkUpControl;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("pricingID", pricingID).append("sequenceNumber",
				sequenceNumber).append("shipNumber", shipNumber).append(
				"serviceOrderId", serviceOrderId).append("originAddress1",
				originAddress1).append("originCity", originCity).append(
				"originState", originState).append("originZip", originZip)
				.append("originCountry", originCountry).append(
						"originLatitude", originLatitude).append(
						"originLongitude", originLongitude).append("originPOE",
						originPOE).append("originPOEName", originPOEName)
				.append("destinationAddress1", destinationAddress1).append(
						"destinationCity", destinationCity).append(
						"destinationState", destinationState).append(
						"destinationZip", destinationZip).append(
						"destinationCountry", destinationCountry).append(
						"destinationLatitude", destinationLatitude).append(
						"destinationLongitude", destinationLongitude).append(
						"destinationPOE", destinationPOE).append(
						"destinationPOEName", destinationPOEName).append(
						"isOrigin", isOrigin).append("isFreight", isFreight)
				.append("isDestination", isDestination).append("mode", mode)
				.append("weight", weight).append("volume", volume).append(
						"unitType", unitType).append("packing", packing)
				.append("containerSize", containerSize).append("lclFclFlag",
						lclFclFlag).append("groupageFlag", groupageFlag)
				.append("expectedLoadDate", expectedLoadDate).append(
						"contract", contract).append("baseCurrency",
						baseCurrency)
				.append("originPortRange", originPortRange).append(
						"destinationPortRange", destinationPortRange).append(
						"customerName", customerName).append("parentAgent",
						parentAgent).append("scope", scope).append("dthcFlag",
						dthcFlag).append("freightMarkUpControl",
						freightMarkUpControl).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PricingControl))
			return false;
		PricingControl castOther = (PricingControl) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(pricingID, castOther.pricingID)
				.append(sequenceNumber, castOther.sequenceNumber).append(
						shipNumber, castOther.shipNumber).append(
						serviceOrderId, castOther.serviceOrderId).append(
						originAddress1, castOther.originAddress1).append(
						originCity, castOther.originCity).append(originState,
						castOther.originState).append(originZip,
						castOther.originZip).append(originCountry,
						castOther.originCountry).append(originLatitude,
						castOther.originLatitude).append(originLongitude,
						castOther.originLongitude).append(originPOE,
						castOther.originPOE).append(originPOEName,
						castOther.originPOEName).append(destinationAddress1,
						castOther.destinationAddress1).append(destinationCity,
						castOther.destinationCity).append(destinationState,
						castOther.destinationState).append(destinationZip,
						castOther.destinationZip).append(destinationCountry,
						castOther.destinationCountry).append(
						destinationLatitude, castOther.destinationLatitude)
				.append(destinationLongitude, castOther.destinationLongitude)
				.append(destinationPOE, castOther.destinationPOE).append(
						destinationPOEName, castOther.destinationPOEName)
				.append(isOrigin, castOther.isOrigin).append(isFreight,
						castOther.isFreight).append(isDestination,
						castOther.isDestination).append(mode, castOther.mode)
				.append(weight, castOther.weight).append(volume,
						castOther.volume).append(unitType, castOther.unitType)
				.append(packing, castOther.packing).append(containerSize,
						castOther.containerSize).append(lclFclFlag,
						castOther.lclFclFlag).append(groupageFlag,
						castOther.groupageFlag).append(expectedLoadDate,
						castOther.expectedLoadDate).append(contract,
						castOther.contract).append(baseCurrency,
						castOther.baseCurrency).append(originPortRange,
						castOther.originPortRange).append(destinationPortRange,
						castOther.destinationPortRange).append(customerName,
						castOther.customerName).append(parentAgent,
						castOther.parentAgent).append(scope, castOther.scope)
				.append(dthcFlag, castOther.dthcFlag).append(
						freightMarkUpControl, castOther.freightMarkUpControl)
				.append(createdBy, castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(pricingID).append(sequenceNumber).append(shipNumber)
				.append(serviceOrderId).append(originAddress1).append(
						originCity).append(originState).append(originZip)
				.append(originCountry).append(originLatitude).append(
						originLongitude).append(originPOE)
				.append(originPOEName).append(destinationAddress1).append(
						destinationCity).append(destinationState).append(
						destinationZip).append(destinationCountry).append(
						destinationLatitude).append(destinationLongitude)
				.append(destinationPOE).append(destinationPOEName).append(
						isOrigin).append(isFreight).append(isDestination)
				.append(mode).append(weight).append(volume).append(unitType)
				.append(packing).append(containerSize).append(lclFclFlag)
				.append(groupageFlag).append(expectedLoadDate).append(contract)
				.append(baseCurrency).append(originPortRange).append(
						destinationPortRange).append(customerName).append(
						parentAgent).append(scope).append(dthcFlag).append(
						freightMarkUpControl).append(createdBy).append(
						updatedBy).append(createdOn).append(updatedOn)
				.toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getContainerSize() {
		return containerSize;
	}
	public void setContainerSize(String containerSize) {
		this.containerSize = containerSize;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getDestinationAddress1() {
		return destinationAddress1;
	}
	public void setDestinationAddress1(String destinationAddress1) {
		this.destinationAddress1 = destinationAddress1;
	}
	@Column
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	@Column
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	@Column
	public String getDestinationLatitude() {
		return destinationLatitude;
	}
	public void setDestinationLatitude(String destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}
	@Column
	public String getDestinationLongitude() {
		return destinationLongitude;
	}
	public void setDestinationLongitude(String destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}
	@Column
	public String getDestinationPOE() {
		return destinationPOE;
	}
	public void setDestinationPOE(String destinationPOE) {
		this.destinationPOE = destinationPOE;
	}
	@Column
	public String getDestinationState() {
		return destinationState;
	}
	public void setDestinationState(String destinationState) {
		this.destinationState = destinationState;
	}
	@Column
	public String getDestinationZip() {
		return destinationZip;
	}
	public void setDestinationZip(String destinationZip) {
		this.destinationZip = destinationZip;
	}
	@Column
	public Date getExpectedLoadDate() {
		return expectedLoadDate;
	}
	public void setExpectedLoadDate(Date expectedLoadDate) {
		this.expectedLoadDate = expectedLoadDate;
	}
	@Column
	public String getGroupageFlag() {
		return groupageFlag;
	}
	public void setGroupageFlag(String groupageFlag) {
		this.groupageFlag = groupageFlag;
	}
	@Column
	public String getLclFclFlag() {
		return lclFclFlag;
	}
	public void setLclFclFlag(String lclFclFlag) {
		this.lclFclFlag = lclFclFlag;
	}
	@Column
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column
	public String getOriginAddress1() {
		return originAddress1;
	}
	public void setOriginAddress1(String originAddress1) {
		this.originAddress1 = originAddress1;
	}
	@Column
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	@Column
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	@Column
	public String getOriginLatitude() {
		return originLatitude;
	}
	public void setOriginLatitude(String originLatitude) {
		this.originLatitude = originLatitude;
	}
	@Column
	public String getOriginLongitude() {
		return originLongitude;
	}
	public void setOriginLongitude(String originLongitude) {
		this.originLongitude = originLongitude;
	}
	@Column
	public String getOriginPOE() {
		return originPOE;
	}
	public void setOriginPOE(String originPOE) {
		this.originPOE = originPOE;
	}
	@Column
	public String getOriginState() {
		return originState;
	}
	public void setOriginState(String originState) {
		this.originState = originState;
	}
	@Column
	public String getOriginZip() {
		return originZip;
	}
	public void setOriginZip(String originZip) {
		this.originZip = originZip;
	}
	@Column
	public String getPacking() {
		return packing;
	}
	public void setPacking(String packing) {
		this.packing = packing;
	}
	@Column
	public String getPricingID() {
		return pricingID;
	}
	public void setPricingID(String pricingID) {
		this.pricingID = pricingID;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	
	@Column
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	@Column
	public BigDecimal getWeight() {
		return weight;
	}
	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}
	@Column
	public Boolean getIsDestination() {
		return isDestination;
	}
	public void setIsDestination(Boolean isDestination) {
		this.isDestination = isDestination;
	}
	@Column
	public Boolean getIsFreight() {
		return isFreight;
	}
	public void setIsFreight(Boolean isFreight) {
		this.isFreight = isFreight;
	}
	@Column
	public Boolean getIsOrigin() {
		return isOrigin;
	}
	public void setIsOrigin(Boolean isOrigin) {
		this.isOrigin = isOrigin;
	}
	@Column
	public String getDestinationPOEName() {
		return destinationPOEName;
	}
	public void setDestinationPOEName(String destinationPOEName) {
		this.destinationPOEName = destinationPOEName;
	}
	@Column
	public String getOriginPOEName() {
		return originPOEName;
	}
	public void setOriginPOEName(String originPOEName) {
		this.originPOEName = originPOEName;
	}
	@Column
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	@Column
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	@Column
	public String getParentAgent() {
		return parentAgent;
	}
	public void setParentAgent(String parentAgent) {
		this.parentAgent = parentAgent;
	}
	@Column
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	@Column
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public Boolean getDthcFlag() {
		return dthcFlag;
	}
	public void setDthcFlag(Boolean dthcFlag) {
		this.dthcFlag = dthcFlag;
	}
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	public Boolean getFreightMarkUpControl() {
		return freightMarkUpControl;
	}
	public void setFreightMarkUpControl(Boolean freightMarkUpControl) {
		this.freightMarkUpControl = freightMarkUpControl;
	}
	public String getDestinationPortRange() {
		return destinationPortRange;
	}
	public void setDestinationPortRange(String destinationPortRange) {
		this.destinationPortRange = destinationPortRange;
	}
	public String getOriginPortRange() {
		return originPortRange;
	}
	public void setOriginPortRange(String originPortRange) {
		this.originPortRange = originPortRange;
	}	
	
	
	
	

}
