<%@ include file="/common/taglibs.jsp"%> 


<script language="javascript" type="text/javascript">
function addRefQuotesService()
{
	location.href = 'refQuoteServiceForm.html';
	}
function inclusionExclusioReport(){
		document.forms['refQuoteServiceList'].action = 'inclusionExclusioReport.html';
    	document.forms['refQuoteServiceList'].submit();
}
function refQuoteServiceListSearch(){
	document.forms['refQuoteServiceList'].action = 'refQuoteServiceListSearch.html';
	document.forms['refQuoteServiceList'].submit();
}
function autoSaveRefQuotesSevisesAjax(fieldName,fieldValTemp,fieldId){
	    var fieldVal=document.forms['refQuoteServiceNew'].elements[fieldValTemp].value;
		var url="autoSaveRefQuotesSevisesAjax.html?ajax=1&decorator=simple&popup=true&fieldName="+encodeURIComponent(fieldName)+"&fieldVal="+encodeURIComponent(fieldVal)+"&fieldId="+encodeURIComponent(fieldId);		    
	 	httpMergeSO24.open("GET", url, true);
	 	httpMergeSO24.onreadystatechange = function(){ handleHttpResponseMergeSoList24(fieldName,fieldVal,fieldId);};
	 	httpMergeSO24.send(null);
}
function handleHttpResponseMergeSoList24(){
	if (httpMergeSO24.readyState == 4){
	    var results=httpMergeSO24.responseText; 
	}
}
var httpMergeSO24 = getHTTPObjectSO23();
function getHTTPObjectSO23()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function onlyNumsAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
}
function isNumeric(targetElement)
{   var i;
    var s = targetElement.value;
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Enter valid Number");
        targetElement.value="";
        return false;
        }
    }
    return true;
}
</script>
<head> 
<meta name="heading" content="Service List"/>
<title>Service List</title>
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:5px;margin-top:-18px;
padding:1px 0;text-align:right;width:100%;
}
.table th.sorted, .table th.order2 {
color:#15428B !important;
} 
div.exportlinks{
margin-left:0px;
}
</style>
</head>
<s:form   cssClass="form_magn" id="refQuoteServiceList" name="refQuoteServiceList" action=""	method="post">
 <c:set var="documentVal" value="N" />
<configByCorp:fieldVisibility componentId="component.field.refQuote.document">		
		<c:set var="documentVal" value="Y" />
</configByCorp:fieldVisibility>
<div id="Layer1" style="width:100%"> 
	<div id="otabs">
	  <ul>
	    <li><a class="current"><span>Search</span></a></li>
	  </ul>
	</div>
	<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
    <div class="center-content" style="padding-left:15px;">
				<table class="table" style="width:100%"  >
				<thead>
				<tr>		
					<th>Job</th>
					<th>Mode</th>
					<th>Routing</th>
					<th>Service&nbsp;Type</th>
					<th>Language</th>
					<th>Pack&nbsp;Mode</th>
					<th>Description</th>
					
				</tr>							
				</thead>
                  <tbody>
                <tr>
				<td> <s:select cssClass="list-menu" name="refQuoteServices.job" list="%{job}" cssStyle="width:135px" headerKey="" headerValue="" /></td>
				<td> <s:select cssClass="list-menu" name="refQuoteServices.mode" list="%{modeMap}" cssStyle="width:115px"  /></td>
				<td> <s:select cssClass="list-menu" name="refQuoteServices.routing" list="%{routing}" cssStyle="width:130px"  /></td>
			    <td><s:select cssClass="list-menu" name="refQuoteServices.serviceType" list="%{service}" cssStyle="width:130px;margin-top:2px;" /></td>
			   
				<td> <s:select cssClass="list-menu" name="refQuoteServices.langauge" list="%{lang}" cssStyle="width:130px" headerKey="" headerValue="" /></td>
				<td> <s:select cssClass="list-menu" name="refQuoteServices.packingMode" list="%{pkmode}" cssStyle="width:110px" /></td>
				<td> <s:textfield name="refQuoteServices.description" required="true" cssStyle="width:160px;" cssClass="input-text"/></td>
				</tr>
               <tr>
              <c:if test="${documentVal=='N'}">
				<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">
				<td style="border:none;" class="listwhitetext">Include/Exclude<br>
				<s:select cssClass="list-menu" name="refQuoteServices.checkIncludeExclude" list="%{checkIncludeExcludeList}" cssStyle="width:135px" headerKey="" headerValue="" />
				</td>
				</c:if></c:if>
				<c:if test="${documentVal=='Y'}">
				<c:if test="${userQuoteServices!=null && userQuoteServices!='' && (userQuoteServices=='Separate section' || userQuoteServices=='Single section')}">
				<td style="border:none;" class="listwhitetext">Include/Exclude/Document</th>
				<s:select cssClass="list-menu" name="refQuoteServices.checkIncludeExclude" list="%{checkIncludeExcludeDocumentList}" cssStyle="width:135px" headerKey="" headerValue="" />
				</c:if></c:if>   
			<td style="border:none;" class="listwhitetext">Origin&nbsp;Country&nbsp;<br>
			<s:select cssClass="list-menu" name="refQuoteServices.originCountry" list="%{ocountry}" cssStyle="width:135px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			<td style="border:none;border:0px solid #FFFFFF;" class="listwhitetext">Destination&nbsp;Country&nbsp;<br>
			<s:select cssClass="list-menu" name="refQuoteServices.destinationCountry" list="%{ocountry}" cssStyle="width:120px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			<td style="border:none;border:0px solid #FFFFFF;" class="listwhitetext">Commodity&nbsp;<br>
			 <s:select cssClass="list-menu" name="refQuoteServices.commodity" list="%{commodits}" cssStyle="width:130px;margin-top:2px;" />
			</td>
			<td style="border:none;border:0px solid #FFFFFF;" class="listwhitetext">Contract&nbsp;<br>
			 <s:select cssClass="list-menu" name="refQuoteServices.contract" list="%{contract}" cssStyle="width:130px;margin-top:2px;" headerKey="" headerValue=""/>
			</td>
			<td style="border:none;" colspan="1" class="listwhitetext">Company&nbsp;Division&nbsp;<br>
			<s:select cssClass="list-menu" name="refQuoteServices.companyDivision" list="%{companyDivis}" cssStyle="width:110px;margin-top:2px;" headerKey="" headerValue=""/>
			</td>
			
		    <td style="border: medium none;border:0px solid #FFFFFF; width: 120px;" colspan="3">
   			<input type="button" class="cssbutton1" value="Search" style="width:55px;" onclick="refQuoteServiceListSearch();"/> 
   			<input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
			</td>
			</tr>			
           </tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>
	<s:form id="refQuoteServiceNew" name="refQuoteServiceNew" action=""	method="post">
		<div id="newmnav">
		  <ul>
		     <li><a><span>Service List</span></a></li>
		  
		  </ul>
		</div><div class="spn">&nbsp;</div>
						
		<display:table name="refQuoteServiceList"  id ="refQuoteServiceList1"  class="table"  requestURI="" pagesize="25" style="width:100%;" >
			<display:column title="#" style="width:10px;">
			<a href="editRefQuoteServiceForm.html?id=${refQuoteServiceList1.id}"><c:out value="${refQuoteServiceList1_rowNum}"/></a>
			</display:column>        						
			 <display:column  title="Job&nbsp;Type"  style="width:60px">
				<select name ="job${refQuoteServiceList1.id}" id ="job${refQuoteServiceList1.id}" style="width:60px" onchange="autoSaveRefQuotesSevisesAjax('job','job${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" class="list-menu">
				<option value=""></option> 
				<c:forEach var="entry" items="${job}">
			        <c:choose>
			         <c:when test="${entry.key == refQuoteServiceList1.job}">
			         <c:set var="selectedInd" value=" selected"></c:set>
			         </c:when>
			        <c:otherwise>
			         <c:set var="selectedInd" value=""></c:set>
			        </c:otherwise>
			        </c:choose>
			       <option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${entry.value}"></c:out>
			      </option>									    
				</c:forEach>
				</select>
			</display:column>
			<display:column title="Mode" style="width:50px;">
				<select name ="mode${refQuoteServiceList1.id}" id ="mode${refQuoteServiceList1.id}" style="width:50px" onchange="autoSaveRefQuotesSevisesAjax('mode','mode${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" class="list-menu">
				<c:forEach var="entry" items="${modeMap}">
			        <c:choose>
			         <c:when test="${entry.key == refQuoteServiceList1.mode}">
			         <c:set var="selectedInd" value=" selected"></c:set>
			         </c:when>
			        <c:otherwise>
			         <c:set var="selectedInd" value=""></c:set>
			        </c:otherwise>
			        </c:choose>
			       <option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${entry.value}"></c:out>
			      </option>									    
				</c:forEach>
				</select>								
			</display:column>
			<display:column title="Route" style="width:50px;">
				<select name ="routing${refQuoteServiceList1.id}" id ="routing${refQuoteServiceList1.id}" style="width:60px" onchange="autoSaveRefQuotesSevisesAjax('routing','routing${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" class="list-menu">
				<c:forEach var="entry" items="${routing}">
			        <c:choose>
			         <c:when test="${entry.key == refQuoteServiceList1.routing}">
			         <c:set var="selectedInd" value=" selected"></c:set>
			         </c:when>
			        <c:otherwise>
			         <c:set var="selectedInd" value=""></c:set>
			        </c:otherwise>
			        </c:choose>
			       <option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${entry.value}"></c:out>
			      </option>									    
				</c:forEach>
				</select>								
			</display:column>	
			<display:column title="ServiceType" style="width:50px;">
				<select name ="serviceType${refQuoteServiceList1.id}" id ="serviceType${refQuoteServiceList1.id}" style="width:150px" onchange="autoSaveRefQuotesSevisesAjax('serviceType','serviceType${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');"  class="list-menu">
				<c:forEach var="entry" items="${service}">
			        <c:choose>
			         <c:when test="${entry.key == refQuoteServiceList1.serviceType}">
			         <c:set var="selectedInd" value=" selected"></c:set>
			         </c:when>
			        <c:otherwise>
			         <c:set var="selectedInd" value=""></c:set>
			        </c:otherwise>
			        </c:choose>
			       <option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${entry.value}"></c:out>
			      </option>									    
				</c:forEach>
				</select>								
			</display:column>  							
			<display:column property="langauge" title="Lan" sortable="true" style="text-align: center;width:10px;"/>
			<display:column sortProperty="description" title="Description" sortable="true" style="width:450px;">
			<input type="text" style="text-align:left;width: 100%;" name="description${refQuoteServiceList1.id}" value="${refQuoteServiceList1.description}" class="input-text" onchange="autoSaveRefQuotesSevisesAjax('description','description${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" />
			</display:column> 
			<display:column sortProperty="displayOrder" title="Order By" sortable="true" style="width:30px;">
			<input type="text" style="text-align:left;width:30px;" name="displayOrder${refQuoteServiceList1.id}" value="${refQuoteServiceList1.displayOrder}" class="input-text" onchange="isNumeric(this);autoSaveRefQuotesSevisesAjax('displayOrder','displayOrder${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" onkeydown="return onlyNumsAllowed(event)"/>
			</display:column>
			<c:if test="${documentVal=='N'}">								
			<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">
			<display:column  title="Incl/Excl" sortable="true" sortProperty="checkIncludeExclude" style="width:50px ;text-align: center"> 
				<c:if test="${refQuoteServiceList1.checkIncludeExclude ne null && refQuoteServiceList1.checkIncludeExclude == 'N'  }">
					<c:out value="Exclude" />
				</c:if>
				<c:if test="${refQuoteServiceList1.checkIncludeExclude ne null && refQuoteServiceList1.checkIncludeExclude == 'Y'  }">
					<c:out value="Include" />
				</c:if>
				<c:if test="${refQuoteServiceList1.checkIncludeExclude eq null || refQuoteServiceList1.checkIncludeExclude == '' }">
				</c:if>
			</display:column>
            </c:if></c:if>
            <c:if test="${documentVal=='Y'}">								
			<c:if test="${userQuoteServices!=null && userQuoteServices!='' && (userQuoteServices=='Separate section' || userQuoteServices=='Single section')}">
			<display:column  title="Incl/Excl/Doc" sortable="true" sortProperty="checkIncludeExclude" style="width:50px ;text-align: center"> 
				<c:if test="${refQuoteServiceList1.checkIncludeExclude ne null && refQuoteServiceList1.checkIncludeExclude == 'N'  }">
					<c:out value="Exclude" />
				</c:if>
				<c:if test="${refQuoteServiceList1.checkIncludeExclude ne null && refQuoteServiceList1.checkIncludeExclude == 'Y'  }">
					<c:out value="Include" />
				</c:if>
				<c:if test="${refQuoteServiceList1.checkIncludeExclude ne null && refQuoteServiceList1.checkIncludeExclude == 'D'  }">
					<c:out value="Document" />
				</c:if>
				<c:if test="${refQuoteServiceList1.checkIncludeExclude eq null || refQuoteServiceList1.checkIncludeExclude == '' }">
				</c:if>
			</display:column>
            </c:if></c:if>
             <display:column title="Pack&nbsp;Mode" style="width:50px;">
				<select name ="packingMode${refQuoteServiceList1.id}" id ="packingMode${refQuoteServiceList1.id}" style="width:70px" onchange="autoSaveRefQuotesSevisesAjax('packingMode','packingMode${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" class="list-menu">
				<c:forEach var="entry" items="${pkmode}">
			        <c:choose>
			         <c:when test="${entry.key == refQuoteServiceList1.packingMode}">
			         <c:set var="selectedInd" value=" selected"></c:set>
			         </c:when>
			        <c:otherwise>
			         <c:set var="selectedInd" value=""></c:set>
			        </c:otherwise>
			        </c:choose>
			       <option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${entry.value}"></c:out>
			      </option>									    
				</c:forEach>
				</select>								
			</display:column>
				<display:column title="Commodity" style="width:50px;">
				<select name ="commodity${refQuoteServiceList1.id}" id ="commodity${refQuoteServiceList1.id}" style="width:70px" onchange="autoSaveRefQuotesSevisesAjax('commodity','commodity${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" class="list-menu">
				<c:forEach var="entry" items="${commodits}">
			        <c:choose>
			         <c:when test="${entry.key == refQuoteServiceList1.commodity}">
			         <c:set var="selectedInd" value=" selected"></c:set>
			         </c:when>
			        <c:otherwise>
			         <c:set var="selectedInd" value=""></c:set>
			        </c:otherwise>
			        </c:choose>
			       <option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${entry.value}"></c:out>
			      </option>									    
				</c:forEach>
				</select>								
			</display:column>
			<display:column title="Contract" style="width:50px;">
				<select name ="contract${refQuoteServiceList1.id}" id ="contract${refQuoteServiceList1.id}" style="width:70px" onchange="autoSaveRefQuotesSevisesAjax('contract','contract${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" class="list-menu" headerKey="" headerValue="">
				<option value=""></option> 
				<c:forEach var="entry" items="${contract}">
			        
			       <option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${entry.value}"></c:out>
			      </option>									    
				</c:forEach>
				</select>								
			</display:column>
			<display:column title="Origin&nbsp;Country" style="width:60px;">
				<select name ="originCountry${refQuoteServiceList1.id}" id ="originCountry${refQuoteServiceList1.id}" style="width:100px"  onchange="autoSaveRefQuotesSevisesAjax('originCountry','originCountry${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" class="list-menu" headerKey="" headerValue="">
				<option value=""><c:out value=""></c:out></option>
				<c:forEach var="entry" items="${ocountry}">
			        <c:choose>
			         <c:when test="${entry.key == refQuoteServiceList1.originCountry}">
			         <c:set var="selectedInd" value=" selected"></c:set>
			         </c:when>
			        <c:otherwise>
			         <c:set var="selectedInd" value=""></c:set>
			        </c:otherwise>
			        </c:choose>
			       <option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${entry.value}"></c:out>
			      </option>									    
				</c:forEach>
				</select>								
			</display:column>
			<display:column title="Destination&nbsp;Country" style="width:60px;">
				<select name ="destinationCountry${refQuoteServiceList1.id}" id ="destinationCountry${refQuoteServiceList1.id}" style="width:100px" onchange="autoSaveRefQuotesSevisesAjax('destinationCountry','destinationCountry${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" class="list-menu"  headerKey="" headerValue="">
				<option value=""><c:out value=""></c:out></option>
				<c:forEach var="entry" items="${ocountry}">
			        <c:choose>
			         <c:when test="${entry.key == refQuoteServiceList1.destinationCountry}">
			         <c:set var="selectedInd" value=" selected"></c:set>
			         </c:when>
			        <c:otherwise>
			         <c:set var="selectedInd" value=""></c:set>
			        </c:otherwise>
			        </c:choose>
			       <option value="<c:out value='${entry.key}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${entry.value}"></c:out>
			      </option>									    
				</c:forEach>
				</select>								
			</display:column>
			<display:column title="Company Division" style="width:50px;">
				<select name ="companyDivision${refQuoteServiceList1.id}" id ="companyDivision${refQuoteServiceList1.id}" style="width:60px" onchange="autoSaveRefQuotesSevisesAjax('companyDivision','companyDivision${refQuoteServiceList1.id}','${refQuoteServiceList1.id}');" class="list-menu"  headerKey="" headerValue="">
				<option value=""><c:out value=""></c:out></option>
				<c:forEach var="companyDivis1" items="${companyDivis}">
			        <c:choose>
			         <c:when test="${companyDivis1 == refQuoteServiceList1.companyDivision}">
			         <c:set var="selectedInd" value=" selected"></c:set>
			         </c:when>
			        <c:otherwise>
			         <c:set var="selectedInd" value=""></c:set>
			        </c:otherwise>
			        </c:choose>
			       <option value="<c:out value='${companyDivis1}' />" <c:out value='${selectedInd}' />>
			      <c:out value="${companyDivis1}"></c:out>
			      </option>									    
				</c:forEach>
				</select>		
			</display:column>
			<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">
			<display:column  title="Serv. Default" sortable="true" style="width:35px ;text-align: center" >
			<c:if test="${refQuoteServiceList1.refQuoteServicesDefault eq true}">
                   <img src="${pageContext.request.contextPath}/images/tick01.gif" />
                      </c:if>
                      <c:if test="${refQuoteServiceList1.refQuoteServicesDefault eq null || refQuoteServiceList1.refQuoteServicesDefault ne true }">
                      <img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
                      </c:if> 
                      </display:column>
                      </c:if>
           <         
			<display:column title="Remove" style="width:20px;"> 
			<a><img align="middle" onclick="confirmSubmit(${refQuoteServiceList1.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
			</display:column>
		</display:table>
		 
		<div class="exportlinks">Export options: <a onclick="inclusionExclusioReport();"><span class="export excel">Excel </span></a></div>
		</s:form>
		<input type="button" class="cssbuttonA" align="left" onclick="return addRefQuotesService();"  value="Add"/> 
        
        <script type="text/javascript"> 
function clear_fields(){
	  	document.forms['refQuoteServiceList'].elements['refQuoteServices.job'].value = "";
		document.forms['refQuoteServiceList'].elements['refQuoteServices.mode'].value = "";
		document.forms['refQuoteServiceList'].elements['refQuoteServices.routing'].value = "";
		document.forms['refQuoteServiceList'].elements['refQuoteServices.langauge'].value = "";
		<c:if test="${userQuoteServices!=null && userQuoteServices!='' && userQuoteServices=='Separate section'}">
		document.forms['refQuoteServiceList'].elements['refQuoteServices.checkIncludeExclude'].value = "";
		</c:if>
		document.forms['refQuoteServiceList'].elements['refQuoteServices.description'].value = "";
		document.forms['refQuoteServiceList'].elements['refQuoteServices.companyDivision'].value = "";
		document.forms['refQuoteServiceList'].elements['refQuoteServices.commodity'].value = "";
		document.forms['refQuoteServiceList'].elements['refQuoteServices.packingMode'].value = "";
		document.forms['refQuoteServiceList'].elements['refQuoteServices.originCountry'].value = "";
		document.forms['refQuoteServiceList'].elements['refQuoteServices.destinationCountry'].value = "";
}
</script>
<script type="text/javascript"> 
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure to remove this row? ");
	var did = targetElement;
	if (agree){
		location.href="deleterefQuoteServices.html?id="+did;
		
		}
	else{
		return false;
	}
}
</script>