<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<head>
    <title><fmt:message key="authorizationNoDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='authorizationNoDetail.heading'/>"/>
    <style><%@ include file="/common/calenderStyle.css"%></style>

</head>

<s:form id="authorizationNoForm" action="saveWorkTicketAuthorizationNo.html?decorator=popup&popup=true" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat1" value="dd-NNN-yy"/>
<s:hidden name="authorizationNo.id" value="<%=request.getParameter("id") %>"/>
<s:hidden name="billId" value="${billId}"/>
<s:set name="billId" value="billId" scope="session"/>
<s:hidden name="billingId" value="${billingId}"/>
<s:set name="billingId" value="billingId" scope="session"/>
<s:hidden name="serviceId" value="${serviceId}"/>
<s:set name="serviceId" value="serviceId" scope="session"/>
<s:hidden name="serviceJob" value="${serviceJob}"/>
<s:set name="serviceJob" value="serviceJob" scope="session"/>
<s:hidden name="serviceCom" value="${serviceCom}"/>
<s:set name="serviceCom" value="serviceCom" scope="session"/>
<s:hidden name="authNo" value="${authNo}"/>
<s:set name="authNo" value="authNo" scope="session"/>
<s:hidden name="billing.id"/>
<s:hidden name="authorizationNo.authorizationSequenceNumber" />
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="popup" value="true"/>
<s:hidden name="authorizationNo.serviceOrderId" value="${serviceOrder.id}"/>
<s:set name="workTicketId" value="<%=request.getParameter("workTicketId")%>" scope="session"/>
<s:hidden name="workTicketId" value="<%=request.getParameter("workTicketId")%>"/>

<c:choose>
<c:when test="${gotoPageString == 'gototab.authorizationNos' }">
	<c:redirect url="/WorkTicketAuthorizationNos.html?billId=${billing.id}&billingId=${billing.shipNumber}&authNo=${authNo}&serviceJob=${serviceOrder.job}&serviceId=${serviceOrder.id}&serviceCom=${serviceOrder.commodity}&workTicketId=${workTicketId}&decorator=popup&popup=true"/>
	
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
<div id="layer1" style="width:750px; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li><a onclick="setReturnString('gototab.authorizationNos');return checkdate('none');"><span>Authorization Numbers</span></a></li><!--
         
          	  <li><a onclick=""><span>Authorization Numbers</span></a></li>
              --><li id="newmnav1" style="background:#FFF"><a class="current"><span>Authorization Number Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      
<div id="Layer5" style="width:100%" onkeydown="changeStatus();">
<table width="100%" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" >
<tbody>
<tr><td height="5px"></td></tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="authorizationNo.shipNumber"/></td>
<td align="left" class="listwhitetext"><s:textfield name="authorizationNo.shipNumber" maxlength="14"  required="true" cssClass="input-textUpper" size="25" readonly="true"/></td>
<td align="right" class="listwhitetext"><fmt:message key="authorizationNo.authorizationNumber"/></td>
<td align="left" class="listwhitetext"><s:textfield name="authorizationNo.authorizationNumber" maxlength="27"  required="true" cssClass="input-text" size="25" /></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="authorizationNo.commodity"/></td>
<td  align="left"><s:select cssClass="list-menu" name="authorizationNo.commodity" list="%{commodits}" cssStyle="width:155px"  headerKey="" headerValue="" onchange="changeStatus();"/></td>					
<td align="right" class="listwhitetext">Ticket#</td>
<td align="left" class="listwhitetext"><s:textfield name="authorizationNo.workTicketNumber" maxlength="14"  required="true" cssClass="input-textUpper" size="25" readonly="true"/></td>
</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="authorizationNo.invoiceDate"/></td>
 <c:if test="${not empty authorizationNo.invoiceDate}">
	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="authorizationNo.invoiceDate"/></s:text>
	<td align="left" width="200px">
	<table cellspacing="0" cellpadding="0" border="0" class="detailTabLabel">
	<tr>
	<td>
	<s:textfield id="invoiceDate" name="authorizationNo.invoiceDate" value="%{FormatedInvoiceDate}" onkeydown="return onlyDel(event,this)" cssClass="input-text" size="7" readonly="true"/>
	</td>
	 <td>
	<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	
	</tr>
	</table>
	</td>
 </c:if>
 <c:if test="${empty authorizationNo.invoiceDate}">
	<td align="left" width="200px">
	<table cellspacing="0" cellpadding="0" class="detailTabLabel">
	<tr>
	<td>
	<s:textfield id="invoiceDate" name="authorizationNo.invoiceDate" onkeydown="return onlyDel(event,this)"  cssClass="input-text" size="7" readonly="true"/>
	</td>
	
	<td>
	<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
    
    </tr>
    </table>
    
    </td>
 </c:if>
 <td align="right" class="listwhitetext"><fmt:message key="authorizationNo.invoiceNumber"/></td>
 <td align="left"><s:textfield name="authorizationNo.invoiceNumber" maxlength="14"  required="true" cssClass="input-textUpper" size="9" readonly="true"  /></td>
				
<%-- <td align="left"><s:textfield name="authorizationNo.invoiceDate" maxlength="12"  required="true" cssClass="input-textUpper" size="9" readonly="true"/></td>--%>
</tr> 
<tr><td align="left" height="15px"></td></tr>
</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>


</div>
</td>
</tr>
</tbody>
</table> 
<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${authorizationNo.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="authorizationNo.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td ><fmt:formatDate value="${authorizationNo.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty authorizationNo.id}">
								<s:hidden name="authorizationNo.createdBy"/>
								<td ><s:label name="createdBy" value="%{authorizationNo.createdBy}"/></td>
							</c:if>
							<c:if test="${empty authorizationNo.id}">
								<s:hidden name="authorizationNo.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${authorizationNo.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="authorizationNo.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td ><fmt:formatDate value="${authorizationNo.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty authorizationNo.id}">
							<s:hidden name="authorizationNo.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{authorizationNo.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty authorizationNo.id}">
							<s:hidden name="authorizationNo.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div> 
   <table><tr><td>   
        <s:submit cssClass="cssbutton1"  cssStyle="width:55px; height:25px" method="save" key="button.save" theme="simple"/>
        <s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" />
	</td></tr></table>
</div>
</s:form>
<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
<s:hidden name="hitFlag" />
<c:if test="${hitFlag == 1}" >
	<c:redirect url="/WorkTicketAuthorizationNos.html?billId=${billId}&billingId=${billingId}&authNo=${authNo}&serviceJob=${serviceJob}&serviceId=${serviceId}&serviceCom=${serviceCom}&workTicketId=${workTicketId}&decorator=popup&popup=true"/>
</c:if>
<div id="mydiv" style="position:absolute;top:110px;"></div>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->   
<script>
function checkdate(clickType){
if ('${autoSavePrompt}' == 'No'){
	document.forms['authorizationNoForm'].action = 'saveWorkTicketAuthorizationNo!saveOnTabChange.html';
           document.forms['authorizationNoForm'].submit();
}else{
     if(!(clickType == 'save')){
    var id1 = document.forms['authorizationNoForm'].elements['billId'].value;
       if (document.forms['authorizationNoForm'].elements['formStatus'].value == '1'){
        var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='authorizationNoDetail.heading'/>");
       
        //// var agree = confirm("Do you want to save the AuthorizationNo Info and continue? press OK to continue with save OR press CANCEL");
        if(agree){
           document.forms['authorizationNoForm'].action = 'saveWorkTicketAuthorizationNo!saveOnTabChange.html';
           document.forms['authorizationNoForm'].submit();
       }else{
           if(id1 != ''){
           if(document.forms['authorizationNoForm'].elements['gotoPageString'].value == 'gototab.authorizationNos'){
               location.href = 'WorkTicketAuthorizationNos.html';
               }
                 }
       }
   }else{
   	   if(id1 != ''){
       if(document.forms['authorizationNoForm'].elements['gotoPageString'].value == 'gototab.authorizationNos'){
               location.href = 'WorkTicketAuthorizationNos.html';
               }
   }
   }
   }
  }
}

function changeStatus(){
   document.forms['authorizationNoForm'].elements['formStatus'].value = '1';
}
</script> 	    

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
    Form.focusFirstElement($("workTicketAuthorizationForm"));
 </script>
    