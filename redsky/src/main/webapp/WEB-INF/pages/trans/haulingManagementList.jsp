<%@ include file="/common/taglibs.jsp"%> 
 <style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:22px;
!margin-bottom:2px;
margin-top:-38px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
}

</style> 
<head> 
    <title>Hauling Management List</title> 
    <meta name="heading" content="Hauling Management List"/> 
   <style type="text/css">

</style>     

<script language="javascript" type="text/javascript">
function openURL(sURL) {
opener.document.location = sURL;
} 
</script>

</head> 
<s:form id="haulingMgtList"> 
<div id="layer1" style="width: 100%;margin: 0px;">

<s:set name="haulingMgt" value="haulingMgt" scope="request"/> 

<div id="otabs" style="padding-top:20px;margin-top:-35px;">
			  <ul>
			    <li><a class="current"><span>Hauling Management List</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
			
<display:table name="haulingMgt" class="table" requestURI="" id="haulingMgtList" export="false" pagesize="15" style="width:980px;margin-top:-18px;!margin-top:-2px;"> 
  	<display:column sortable="true" title="Driver" style="width:100px" sortProperty="driver">
  	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.driver}"/></a>
  	</display:column>
  	<display:column sortable="true" title="Shipper" style="width:100px" sortProperty="shiper">
  	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.shiper}" /></a>
  	</display:column>
  	<display:column sortable="true" title="Job" style="width:100px" sortProperty="job">
  	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.job}"/></a>
  	</a></display:column>
  	<display:column  sortable="true" title="Order No" style="width:100px" sortProperty="orderNo">
  		<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.orderNo}"/></a>
  	</display:column>
	<display:column sortable="true" title="Origin" style="width:100px" sortProperty="origin">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.origin}"/></a>
	</display:column>
	<display:column sortable="true" title="Destination" style="" sortProperty="destination">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.destination}"/></a>
	</display:column>
	<display:column sortable="true" title="Load Beg" style="width:100px"  format="{0,date,dd-MMM-yyyy}" sortProperty="loadBeg">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.loadBeg}"/></a>
	</display:column>
	<display:column sortable="true" title="Load End" style="width:100px"  format="{0,date,dd-MMM-yyyy}" sortProperty="loadEnd">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.loadEnd}"/></a>
	</display:column>
	<display:column  sortable="true" title="Delivery Beg" style=""  format="{0,date,dd-MMM-yyyy}" sortProperty="deliveryBeg">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.deliveryBeg}"/></a>
	</display:column>
	<display:column sortable="true" title="Delivery End" style=""  format="{0,date,dd-MMM-yyyy}" sortProperty="deliveryEnd">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.deliveryEnd}"/></a>
	</display:column>
	<display:column sortable="true" title="ETA" style=""  format="{0,date,dd-MMM-yyyy}" sortProperty="ETA">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.ETA}"/></a>
	</display:column>
	<display:column sortable="true" title="Driver Paper" style="" sortProperty="driverPaper">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');"><c:out value="${haulingMgtList.driverPaper}"/></a>
	</display:column>
	<display:column sortable="true" title="Est.Rev." style="">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');">
	<div align="right" style="margin:0px; text-align:right;width:100%;">
	<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${haulingMgtList.estimatedRevenue}" />
    </div></a>
	</display:column>
	<display:column sortable="true" title="Est.Wgt." style="">
	<a href="javascript:openURL('editMiscellaneous.html?id=${haulingMgtList.id}');">
	<div align="right" style="margin:0px; text-align:right;width:100%;">
	<fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${haulingMgtList.estWgt}" />
    </div></a>
	</display:column>
	
  
  
	
</display:table>

</div>	
</s:form> 