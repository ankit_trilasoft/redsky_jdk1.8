package com.trilasoft.app.webapp.action;

import java.util.*;
import java.math.BigDecimal;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.service.MiscellaneousManager;
import java.text.SimpleDateFormat;

import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;

public class QuotationManagementAccountLineAction extends BaseAction {

	private Long sid;

	private ServiceOrderManager serviceOrderManager;

	private AccountLineManager accountLineManager;

	ServicePartnerManager servicePartnerManager;

	PartnerManager partnerManager;

	private MiscellaneousManager miscellaneousManager;

	private BillingManager billingManager;

	private RefMasterManager refMasterManager;

	private NotesManager notesManager;

	private ServiceOrder serviceOrder;

	ServicePartner servicePartner;

	private AccountLine accountLine;

	private List accountLineList;

	private List accountLineEstimateList;

	private List accountLineListBillToCode;// for bill to codes

	private List partners;

	private List systemDefaultList;

	private Long id;

	public int billingFlag;

	private CustomerFile customerFile;

	private Miscellaneous miscellaneous;

	private String sessionCorpID;

	private List maxLineNumber;

	private String accountLineNumber;

	private Long autoLineNumber;

	private Long newInvoiceNumber;

	private Billing billing;

	private Charges charges;

	private ChargesAction chargeAction;

	private BigDecimal totalEstimateExpenseCwt;

	private BigDecimal totalRevisionExpenseCwt;

	private BigDecimal totalEstimateExpenseKg;

	private BigDecimal totalRevisionExpenseKg;

	private BigDecimal totalEstimateExpenseCft;

	private BigDecimal totalRevisionExpenseCft;

	private BigDecimal totalEstimateExpenseCbm;

	private BigDecimal totalRevisionExpenseCbm;

	private BigDecimal totalEstimateExpenseEach;

	private BigDecimal totalRevisionExpenseEach;

	private BigDecimal totalEstimateExpenseFlat;

	private BigDecimal totalRevisionExpenseFlat;

	private BigDecimal totalEstimateExpenseHour;

	private BigDecimal totalRevisionExpenseHour;

	private BigDecimal actualRevenue;

	private String countCategoryTypeDetailNotes;

	private String countEstimateDetailNotes;

	private String countPayableDetailNotes;

	private String countRevesionDetailNotes;

	private String countEntitleDetailOrderNotes;

	private String countReceivableDetailNotes;

	private List revisedChargeList;

	private List revisedEntitleChargeList;

	private List revisedChargeListRadio;

	private String billingContract;
	private String accountInterface;

	private String charge;

	private String shipNum;

	public String newDefaultDate = new String("");

	private List quantity;

	private String revisedChargeValue;

	private SystemDefault systemDefault;
	private String quotationContract;

	public String newSysDefaultDate = new String("");

	private static Map<String, String> category;

	private static Map<String, String> basis;

	private static Map<String, String> country;

	private static Map<String, String> payingStatus;
	private static Map<String,String>paymentStatus;
	private ArrayList getGlTypeList;
	private ArrayList getCommissionList;
	
	private String gotoPageString;

	public String getGotoPageString() {

	   return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

	   this.gotoPageString = gotoPageString;

	}

	private String validateFormNav;
    
    public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	
	public String saveOnTabChange() throws Exception {
	       //if (option enabled for this company in the system parameters table then call save) {
		validateFormNav = "OK";
	    String s = saveQuotationAccountLine();    // else simply navigate to therequested page)
	    return gotoPageString;
	}

	public QuotationManagementAccountLineAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	@SkipValidation
	public String getNotesForIconChange() {
		//System.out.println("\n\n\n\n\n\n\n\n\n" + accountLine.getShipNumber());
		//System.out.println("\n\n\n\n\n\n\n\n\n" + serviceOrder.getShipNumber() + "\n\n\n\n\n\n\n\n\n");
		List m = notesManager.countForCategoryTypeNotes(accountLine.getId());
		List mo = notesManager.countForEntitleDetailNotes(accountLine.getId());
		List md = notesManager.countForEstimateDetailNotes(accountLine.getId());
		List ms = notesManager.countForRevisionDetailNotes(accountLine.getId());
		List mso = notesManager.countForPayableDetailNotes(accountLine.getId());
		List msdo = notesManager.countForReceivableDetailNotes(accountLine.getId());

		if (m.isEmpty()) {
			countCategoryTypeDetailNotes = "0";
		} else {
			countCategoryTypeDetailNotes = ((m).get(0)).toString();
		}

		if (md.isEmpty()) {
			countEstimateDetailNotes = "0";
		} else {
			countEstimateDetailNotes = ((md).get(0)).toString();
		}

		if (ms.isEmpty()) {
			countRevesionDetailNotes = "0";
		} else {
			countRevesionDetailNotes = ((ms).get(0)).toString();
		}
		if (mo.isEmpty()) {
			countEntitleDetailOrderNotes = "0";
		} else {
			countEntitleDetailOrderNotes = ((mo).get(0)).toString();
		}

		if (mso.isEmpty()) {
			countPayableDetailNotes = "0";
		} else {
			countPayableDetailNotes = ((mso).get(0)).toString();
		}
		if (msdo.isEmpty()) {
			countReceivableDetailNotes = "0";
		} else {
			countReceivableDetailNotes = ((msdo).get(0)).toString();
		}

		return SUCCESS;
	}

	@SkipValidation
	public String updateAccountLine(ServiceOrder serviceOrder) {

		//System.out.println("I am in updateAccountLine top");
		BigDecimal cuDivisormar = new BigDecimal(100);
		billingFlag = 1;
		serviceOrder.setEstimatedTotalRevenue(new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		BigDecimal divisormarEst = new BigDecimal(((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()));

		if (serviceOrder.getEstimatedTotalRevenue() == null || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))
				|| serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00"))|| serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.0000"))) {
			serviceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
		} else {
			serviceOrder.setEstimatedGrossMarginPercentage((((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString())
			.subtract(new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(
					divisormarEst, 2));
		}

		serviceOrder.setEstimatedGrossMargin((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract(new BigDecimal(
				(accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
		serviceOrder.setRevisedGrossMargin((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract(new BigDecimal(
				(accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
		serviceOrder.setRevisedTotalRevenue(new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		BigDecimal divisormarEstPer = new BigDecimal(((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));

		if (serviceOrder.getRevisedTotalRevenue() == null || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))
				|| serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.0000"))) {
			serviceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
		} else {
			serviceOrder.setRevisedGrossMarginPercentage((((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString())
			.subtract(new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(
					divisormarEstPer, 2));
		}

		serviceOrder.setRevisedTotalExpense(new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()));

		serviceOrder.setEstimatedTotalExpense(new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()));

		serviceOrder.setEntitledTotalAmount(new BigDecimal((accountLineManager.getEntitleSum(serviceOrder.getShipNumber())).get(0).toString()));

		serviceOrder.setActualExpense(new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()));
		serviceOrder.setActualRevenue(new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));

		serviceOrder.setActualGrossMargin((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract(new BigDecimal(
				(accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
		BigDecimal divisormar = new BigDecimal(((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		if (serviceOrder.getActualRevenue() == null || serviceOrder.getActualRevenue().equals(new BigDecimal("0"))
				|| serviceOrder.getActualRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getActualRevenue().equals(new BigDecimal("0.0000"))) {
			serviceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
		} else {
			serviceOrder
			.setActualGrossMarginPercentage((((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString())
			.subtract(new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(
					divisormar, 2));
		}
		/*# 7784 - Always show actual gross margin in accountline overview Start*/
		try{
		serviceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"EXP")));
		if(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").doubleValue()!=0){
			serviceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((serviceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV")).doubleValue())*100));
		}else{
			serviceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
		}
		}catch(Exception e){}
		/*# 7784 - Always show actual gross margin in accountline overview End*/		
		serviceOrderManager.updateFromAccountLine(serviceOrder);
		serviceOrder = serviceOrderManager.get(sid);
		//accountLineList = new ArrayList( serviceOrder.getAccountLine() );

		accountLineList = new ArrayList(serviceOrder.getAccountLines());
		getSession().setAttribute("accountLines", accountLineList);
		return SUCCESS;
	}

	public String getComboList(String corpID) {
		category = refMasterManager.findByParameter(corpID, "ACC_CATEGORY");
		basis = refMasterManager.findByParameter(corpID, "ACC_BASIS");
		country = refMasterManager.findCodeOnleByParameter(corpID, "CURRENCY");
		payingStatus = refMasterManager.findByParameter(corpID, "ACC_STATUS");
		paymentStatus=refMasterManager.findByParameter(corpID, "PAY_STATUS");
		return SUCCESS;
	}
	@SkipValidation
    public String getGlTypeList(){
    	 getGlTypeList=new ArrayList(accountLineManager.findGlTypeList(billingContract, charge));
    	 if(getGlTypeList.isEmpty()||getGlTypeList==null)
    	 {
    		 getGlTypeList=new ArrayList();
    		 
    	 }  
    	return SUCCESS;	
    }
	@SkipValidation
    public String getCommissionList(){
    	 getCommissionList =new ArrayList(accountLineManager.findCommissionList(billingContract, charge));
    	if(getCommissionList.isEmpty()||getCommissionList==null)
    	{
    		getCommissionList=new ArrayList(); 	
    	} 
    	return SUCCESS;	
    }

	@SkipValidation
	public String editQuoteAcc() {
		getComboList(sessionCorpID);
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();
		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
		BigDecimal multiplyer=new BigDecimal(2.2046);
		BigDecimal divisor=new BigDecimal(2.2046);
		BigDecimal cuMultiplyer=new BigDecimal(35.314);
		BigDecimal cuDivisor=new BigDecimal(35.314);  
		serviceOrder=serviceOrderManager.get(sid);
		customerFile = serviceOrder.getCustomerFile();
	if (id != null)
	     {  
		
			accountLine = accountLineManager.get(id);
			serviceOrder = accountLine.getServiceOrder();
			accountLine.setCorpID(sessionCorpID); 
			}	    	
		    	else
		    	{   
		    		accountLine = new AccountLine(); 
		    		accountLine.setCorpID(sessionCorpID); 
		    		maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
		            if ( maxLineNumber.get(0) == null ) {          
		             	accountLineNumber = "001";
		             }else {
		             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
		                 if((autoLineNumber.toString()).length() == 2) {
		                 	accountLineNumber = "0"+(autoLineNumber.toString());
		                 }
		                 else if((autoLineNumber.toString()).length() == 1) {
		                 	accountLineNumber = "00"+(autoLineNumber.toString());
		                 } 
		                 else {
		                 	accountLineNumber=autoLineNumber.toString();
		                 }
		             }
		            accountLine.setAccountLineNumber(accountLineNumber);
		    		accountLine.setShipNumber(serviceOrder.getShipNumber()); 
		    		accountLine.setCreatedOn(new Date());
		    		accountLine.setUpdatedOn(new Date());
		    		accountLine.setStatus(true);
		    		accountLine.setEstimateQuantity(new BigDecimal("1.0"));
		    		accountLine.setBasis("flat"); 
		    	}
	quotationContract=customerFile.getContract(); 
	getNotesForIconChange();
    	return SUCCESS;
    }

	public String saveQuotationAccountLine() throws Exception {
		if (!serviceOrderManager.findBySysDefault(sessionCorpID).isEmpty()) {
			newSysDefaultDate = serviceOrderManager.findBySysDefault(sessionCorpID).get(0).toString();
			newSysDefaultDate = newSysDefaultDate.replace(".0", "");

		} else {
			newSysDefaultDate = new String("");//newSysDefaultDate.replace(".0", "");
		}
		Date dtTmp = new SimpleDateFormat("yyyy-MM-dd").parse(newSysDefaultDate);
		//String strOutDt = new SimpleDateFormat("yyyy-MM-dd").format(dtTmp);

		if (!(accountLine.getRecPostDate() == null)) {
			if (dtTmp.after(accountLine.getRecPostDate())) {
				errorMessage("Posted Date Can't be less than  " + dtTmp);
				return INPUT;
			}
		}
		billingFlag = 1;
		boolean isNew = (accountLine.getId() == null);
		accountLine.setServiceOrder(serviceOrder);
		accountLine.setUpdatedOn(new Date());
		accountLine.setUpdatedBy(getRequest().getRemoteUser());
		accountLine.setCorpID(sessionCorpID);
		getComboList(sessionCorpID);
		//System.out.println("\n\n Billing records going to save :\t" + accountLine);
		//System.out.println("\n\n Billing id at Save time :\t"+accountLine.getId());
		accountLineManager.updateAccountLine(accountLine.getInvoiceNumber(), accountLine.getVendorCode(),serviceOrder.getShipNumber());

		if (isNew) {
			accountLine.setCreatedOn(new Date());
		} 
		autoLineNumber=Long.parseLong(accountLine.getAccountLineNumber());
	        if((autoLineNumber.toString()).length() == 2) {
	            accountLineNumber = "0"+(autoLineNumber.toString());
	         }
	        else if((autoLineNumber.toString()).length() == 1) {
	            accountLineNumber = "00"+(autoLineNumber.toString());
	         } 
	        else {
	            accountLineNumber=autoLineNumber.toString();
	         }  
             accountLine.setAccountLineNumber(accountLineNumber); 
      
	    accountLineManager.save(accountLine);
		/*if (isNew) {
			if (accountLine.getCategory().equalsIgnoreCase("Origin Agent") || accountLine.getCategory().equalsIgnoreCase("Destination Agent")) {
				servicePartner = new ServicePartner();
				if (accountLine.getCategory().equalsIgnoreCase("Origin Agent")) {
					servicePartner.setCorpID(sessionCorpID);
					servicePartner.setShipNumber(serviceOrder.getShipNumber());
					servicePartner.setShip(serviceOrder.getShip());
					servicePartner.setSequenceNumber(serviceOrder.getSequenceNumber());
					servicePartner.setPartnerType("OA");
					servicePartner.setCarrierName(accountLine.getEstimateVendorName());
				}
				if (accountLine.getCategory().equalsIgnoreCase("Destination Agent")) {
					servicePartner.setCorpID(sessionCorpID);
					servicePartner.setShipNumber(serviceOrder.getShipNumber());
					servicePartner.setShip(serviceOrder.getShip());
					servicePartner.setSequenceNumber(serviceOrder.getSequenceNumber());
					servicePartner.setPartnerType("DA");
					servicePartner.setCarrierName(accountLine.getEstimateVendorName());
				}
				servicePartner.setServiceOrder(serviceOrder);
				servicePartnerManager.save(servicePartner);
			}
		}
*/
		serviceOrder = serviceOrderManager.get(sid);
		//System.out.println("\n\n ServiceOrder record After accountLine  save method :\t" + serviceOrder);
		accountLineList = new ArrayList(serviceOrder.getAccountLines());
		getSession().setAttribute("accountLines", accountLineList);
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "accountLine.added" : "accountLine.updated";
		saveMessage(getText(key));
		}
		updateAccountLine(serviceOrder);
		return SUCCESS;
	}

	@SkipValidation
	public String updateQuotaionAccountLine(ServiceOrder serviceOrder) {
		accountInterface=serviceOrderManager.findAccountInterface(sessionCorpID).get(0).toString();
		BigDecimal cuDivisormar = new BigDecimal(100);
		billingFlag = 1;
		serviceOrder.setEstimatedTotalRevenue(new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		BigDecimal divisormarEst = new BigDecimal(((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()));

		if (serviceOrder.getEstimatedTotalRevenue() == null || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0"))
				|| serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getEstimatedTotalRevenue().equals(new BigDecimal("0.0000"))) {
			serviceOrder.setEstimatedGrossMarginPercentage(new BigDecimal("0"));
		} else {
			serviceOrder.setEstimatedGrossMarginPercentage((((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString())
			.subtract(new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(
					divisormarEst, 2));
		}

		serviceOrder.setEstimatedGrossMargin((new BigDecimal((accountLineManager.getEstimateRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract(new BigDecimal(
				(accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
		serviceOrder.setRevisedGrossMargin((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract(new BigDecimal(
				(accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
		serviceOrder.setRevisedTotalRevenue(new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		BigDecimal divisormarEstPer = new BigDecimal(((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString()));

		if (serviceOrder.getRevisedTotalRevenue() == null || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0"))
				|| serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getRevisedTotalRevenue().equals(new BigDecimal("0.0000")) ) {
			serviceOrder.setRevisedGrossMarginPercentage(new BigDecimal("0"));
		} else {
			serviceOrder.setRevisedGrossMarginPercentage((((new BigDecimal((accountLineManager.getRevisionRevSum(serviceOrder.getShipNumber())).get(0).toString())
			.subtract(new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(
					divisormarEstPer, 2));
		}

		serviceOrder.setRevisedTotalExpense(new BigDecimal((accountLineManager.getRevisionExpSum(serviceOrder.getShipNumber())).get(0).toString()));

		serviceOrder.setEstimatedTotalExpense(new BigDecimal((accountLineManager.getEstimateExpSum(serviceOrder.getShipNumber())).get(0).toString()));

		serviceOrder.setEntitledTotalAmount(new BigDecimal((accountLineManager.getEntitleSum(serviceOrder.getShipNumber())).get(0).toString()));

		serviceOrder.setActualExpense(new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()));
		serviceOrder.setActualRevenue(new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));

		serviceOrder.setActualGrossMargin((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()).subtract(new BigDecimal(
				(accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString()))));
		BigDecimal divisormar = new BigDecimal(((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString()));
		if (serviceOrder.getActualRevenue() == null || serviceOrder.getActualRevenue().equals(new BigDecimal("0"))
				|| serviceOrder.getActualRevenue().equals(new BigDecimal("0.00")) || serviceOrder.getActualRevenue().equals(new BigDecimal("0.0000"))) {
			serviceOrder.setActualGrossMarginPercentage(new BigDecimal("0"));
		} else {
			serviceOrder
			.setActualGrossMarginPercentage((((new BigDecimal((accountLineManager.getActualRevSum(serviceOrder.getShipNumber())).get(0).toString())
			.subtract(new BigDecimal((accountLineManager.getActualExpSum(serviceOrder.getShipNumber())).get(0).toString())))).multiply(cuDivisormar)).divide(
					divisormar, 2));
		}
		/*# 7784 - Always show actual gross margin in accountline overview Start*/
		try{
		serviceOrder.setProjectedGrossMargin(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").subtract (accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"EXP")));
		if(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV").doubleValue()!=0){
			serviceOrder.setProjectedGrossMarginPercentage(BigDecimal.valueOf(((serviceOrder.getProjectedGrossMargin()).doubleValue()/(accountLineManager.getProjectedSum(serviceOrder.getShipNumber(),"REV")).doubleValue())*100));
		}else{
			serviceOrder.setProjectedGrossMarginPercentage(new BigDecimal("0.00"));
		}
		}catch(Exception e){}
		/*# 7784 - Always show actual gross margin in accountline overview End*/
		serviceOrderManager.updateFromAccountLine(serviceOrder);
		serviceOrder = serviceOrderManager.get(sid);
		//accountLineList = new ArrayList( serviceOrder.getAccountLine() );

		accountLineList = new ArrayList(serviceOrder.getAccountLines());
		getSession().setAttribute("accountLines", accountLineList);
		return SUCCESS;
	}
	
	

	public ChargesAction getChargeAction() {
		return chargeAction;
	}

	public void setChargeAction(ChargesAction chargeAction) {
		this.chargeAction = chargeAction;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AccountLine getAccountLine() {
		return accountLine;
	}

	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;

	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getCategory() {
		return category;
	}

	public Map<String, String> getBasis() {
		return basis;
	}

	public Map<String, String> getPayingStatus() {
		return payingStatus;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public List getAccountLineList() {
		return accountLineList;
	}

	public void setAccountLineList(List<AccountLine> accountLineList) {
		this.accountLineList = accountLineList;
	}

	public void setEstimateAccountList(List<AccountLine> accountLineEstimateList) {
		this.accountLineEstimateList = accountLineEstimateList;
	}

	public List getEstimateAccountList() {
		return accountLineEstimateList;
	}

	public Long getNewInvoiceNumber() {
		return newInvoiceNumber;
	}

	public void setNewInvoiceNumber(Long newInvoiceNumber) {
		this.newInvoiceNumber = newInvoiceNumber;
	}

	public BigDecimal getTotalEstimateExpenseCbm() {
		return totalEstimateExpenseCbm;
	}

	public void setTotalEstimateExpenseCbm(BigDecimal totalEstimateExpenseCbm) {
		this.totalEstimateExpenseCbm = totalEstimateExpenseCbm;
	}

	public BigDecimal getTotalEstimateExpenseCft() {
		return totalEstimateExpenseCft;
	}

	public void setTotalEstimateExpenseCft(BigDecimal totalEstimateExpenseCft) {
		this.totalEstimateExpenseCft = totalEstimateExpenseCft;
	}

	public BigDecimal getTotalEstimateExpenseCwt() {
		return totalEstimateExpenseCwt;
	}

	public void setTotalEstimateExpenseCwt(BigDecimal totalEstimateExpenseCwt) {
		this.totalEstimateExpenseCwt = totalEstimateExpenseCwt;
	}

	public BigDecimal getTotalEstimateExpenseEach() {
		return totalEstimateExpenseEach;
	}

	public void setTotalEstimateExpenseEach(BigDecimal totalEstimateExpenseEach) {
		this.totalEstimateExpenseEach = totalEstimateExpenseEach;
	}

	public BigDecimal getTotalEstimateExpenseKg() {
		return totalEstimateExpenseKg;
	}

	public void setTotalEstimateExpenseKg(BigDecimal totalEstimateExpenseKg) {

		this.totalEstimateExpenseKg = totalEstimateExpenseKg;

	}

	public BigDecimal getTotalRevisionExpenseCbm() {
		return totalRevisionExpenseCbm;
	}

	public void setTotalRevisionExpenseCbm(BigDecimal totalRevisionExpenseCbm) {
		this.totalRevisionExpenseCbm = totalRevisionExpenseCbm;
	}

	public BigDecimal getTotalRevisionExpenseCft() {
		return totalRevisionExpenseCft;
	}

	public void setTotalRevisionExpenseCft(BigDecimal totalRevisionExpenseCft) {
		this.totalRevisionExpenseCft = totalRevisionExpenseCft;
	}

	public BigDecimal getTotalRevisionExpenseCwt() {
		return totalRevisionExpenseCwt;
	}

	public void setTotalRevisionExpenseCwt(BigDecimal totalRevisionExpenseCwt) {
		this.totalRevisionExpenseCwt = totalRevisionExpenseCwt;
	}

	public BigDecimal getTotalRevisionExpenseEach() {
		return totalRevisionExpenseEach;
	}

	public void setTotalRevisionExpenseEach(BigDecimal totalRevisionExpenseEach) {
		this.totalRevisionExpenseEach = totalRevisionExpenseEach;
	}

	public BigDecimal getTotalRevisionExpenseKg() {
		return totalRevisionExpenseKg;
	}

	public void setTotalRevisionExpenseKg(BigDecimal totalRevisionExpenseKg) {
		this.totalRevisionExpenseKg = totalRevisionExpenseKg;
	}

	public BigDecimal getTotalEstimateExpenseFlat() {
		return totalEstimateExpenseFlat;
	}

	public void setTotalEstimateExpenseFlat(BigDecimal totalEstimateExpenseFlat) {
		this.totalEstimateExpenseFlat = totalEstimateExpenseFlat;
	}

	public BigDecimal getTotalEstimateExpenseHour() {
		return totalEstimateExpenseHour;
	}

	public void setTotalEstimateExpenseHour(BigDecimal totalEstimateExpenseHour) {
		this.totalEstimateExpenseHour = totalEstimateExpenseHour;
	}

	public BigDecimal getTotalRevisionExpenseFlat() {
		return totalRevisionExpenseFlat;
	}

	public void setTotalRevisionExpenseFlat(BigDecimal totalRevisionExpenseFlat) {
		this.totalRevisionExpenseFlat = totalRevisionExpenseFlat;
	}

	public BigDecimal getTotalRevisionExpenseHour() {
		return totalRevisionExpenseHour;
	}

	public void setTotalRevisionExpenseHour(BigDecimal totalRevisionExpenseHour) {
		this.totalRevisionExpenseHour = totalRevisionExpenseHour;
	}

	public BigDecimal getActualRevenue() {
		return actualRevenue;
	}

	public void setActualRevenue(BigDecimal actualRevenue) {
		this.actualRevenue = actualRevenue;
	}

	public int getBillingFlag() {
		return billingFlag;
	}

	public void setBillingFlag(int billingFlag) {
		this.billingFlag = billingFlag;
	}

	public String getCountCategoryTypeDetailNotes() {
		return countCategoryTypeDetailNotes;
	}

	public String getCountEntitleDetailOrderNotes() {
		return countEntitleDetailOrderNotes;
	}

	public String getCountEstimateDetailNotes() {
		return countEstimateDetailNotes;
	}

	public String getCountPayableDetailNotes() {
		return countPayableDetailNotes;
	}

	public String getCountReceivableDetailNotes() {
		return countReceivableDetailNotes;
	}

	public String getCountRevesionDetailNotes() {
		return countRevesionDetailNotes;
	}

	public ServicePartner getServicePartner() {
		return servicePartner;
	}

	public void setServicePartner(ServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}

	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}

	public List getAccountLineListBillToCode() {
		return accountLineListBillToCode;
	}

	public void setAccountLineListBillToCode(List accountLineListBillToCode) {
		this.accountLineListBillToCode = accountLineListBillToCode;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public List getPartners() {
		return partners;
	}

	public void setPartners(List partners) {
		this.partners = partners;
	}

	public List getRevisedChargeList() {
		return revisedChargeList;
	}

	public void setRevisedChargeList(List revisedChargeList) {
		this.revisedChargeList = revisedChargeList;
	}

	public Charges getCharges() {
		return charges;
	}

	public void setCharges(Charges charges) {
		this.charges = charges;
	}

	public String getBillingContract() {
		return billingContract;
	}

	public void setBillingContract(String billingContract) {
		this.billingContract = billingContract;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public List getSystemDefaultList() {
		return systemDefaultList;
	}

	public void setSystemDefaultList(List systemDefaultList) {
		this.systemDefaultList = systemDefaultList;
	}

	public List getQuantity() {
		return quantity;
	}

	public void setQuantity(List quantity) {
		this.quantity = quantity;
	}

	public String getShipNum() {
		return shipNum;
	}

	public void setShipNum(String shipNum) {
		this.shipNum = shipNum;
	}

	public String getRevisedChargeValue() {
		return revisedChargeValue;
	}

	public void setRevisedChargeValue(String revisedChargeValue) {
		this.revisedChargeValue = revisedChargeValue;
	}

	public List getRevisedChargeListRadio() {
		return revisedChargeListRadio;
	}

	public void setRevisedChargeListRadio(List revisedChargeListRadio) {
		this.revisedChargeListRadio = revisedChargeListRadio;
	}

	public List getRevisedEntitleChargeList() {
		return revisedEntitleChargeList;
	}

	public void setRevisedEntitleChargeList(List revisedEntitleChargeList) {
		this.revisedEntitleChargeList = revisedEntitleChargeList;
	}

	public ArrayList getGetCommissionList() {
		return getCommissionList;
	}

	public void setGetCommissionList(ArrayList getCommissionList) {
		this.getCommissionList = getCommissionList;
	}

	public ArrayList getGetGlTypeList() {
		return getGlTypeList;
	}

	public void setGetGlTypeList(ArrayList getGlTypeList) {
		this.getGlTypeList = getGlTypeList;
	}

	public static Map<String, String> getPaymentStatus() {
		return paymentStatus;
	}

	public static void setPaymentStatus(Map<String, String> paymentStatus) {
		QuotationManagementAccountLineAction.paymentStatus = paymentStatus;
	}

	public String getQuotationContract() {
		return quotationContract;
	}

	public void setQuotationContract(String quotationContract) {
		this.quotationContract = quotationContract;
	}

	public String getAccountInterface() {
		return accountInterface;
	}

	public void setAccountInterface(String accountInterface) {
		this.accountInterface = accountInterface;
	}

}
