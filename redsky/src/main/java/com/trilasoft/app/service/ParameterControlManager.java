package com.trilasoft.app.service;
import java.util.List;

import com.trilasoft.app.model.ParameterControl;
import org.appfuse.service.GenericManager; 

public interface ParameterControlManager extends GenericManager<ParameterControl, Long>{
	public List findUseCorpId();

	public Boolean checkTsftFlag(String parameter2, String sessionCorpID2);

	public Boolean checkHybridFlag(String parameter2, String sessionCorpID2);
	
	public List<ParameterControl> findByFields(String parameter,String customType,Boolean active,Boolean tsftFlag,Boolean hybridFlag);
	List getAllParameter(String paramater);
	public String getDescription(String parameter,String corpId);

}
