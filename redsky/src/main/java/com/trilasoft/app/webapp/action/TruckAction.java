package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Truck;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.TruckManager;

public class TruckAction extends BaseAction implements Preparable {

	private Truck truck;

	private TruckManager truckManager;
	
	private RefMasterManager refMasterManager;
	
	private CustomerFileManager customerFileManager;

	private String sessionCorpID;
	
    private String partnerCode;//Enhancement for ControlExpirations    
    private String parentId;//Enhancement for ControlExpirations
    private String hitFlag;
	private static Date createdon;
	private Map<String, String> fleet; 
	private List truckList;
	private List truckListForCount;
	private List ownerOperatorList = new ArrayList();
	private Long id;
	private String bucket2;
	private String gotoPageString;
	private String truckNumber;
	private String validateFormNav;
	private String truckType;
	private Date deliveryLastDay;
	private List statess;
	private static Map<String, String> yesno;
	private static Map<String, String> states;
	private Map<String, String> glCode;
	private Map<String, String> type;
	private static Map<String, String> house;
	private static Map<String, String> glType;
	private static Map<String, String> trailertype;
	private static Map<String, String> condition;
	private static Map<String, String> trailerTactorType=new LinkedHashMap<String, String>();
	private Map<String, String> ocountry;
	private List driverList;
	private List truckDriverList;
	private List truckDescriptionList;
	private List carrierListInDomestic;
	private List controlExpirationsListInDomestic;
	private String truckStatus;
	private String carrierTruckAgency;
	private List tractorAgentVanNameList;
	private String truckVanLineCode;
	private List vanList;
	private String vanTruckAgency;
	private String enbState;
    private Map<String, String> countryCod;
	Date currentdate = new Date();
	private boolean dotInspFlag = true;
	private int myListFor;
	int counterSixty=0;
	int counterThirty=0;
	int counterDueDate=0;
	static final Logger logger = Logger.getLogger(TruckAction.class);

	public String getVanTruckAgency() {
		return vanTruckAgency;
	}

	public void setVanTruckAgency(String vanTruckAgency) {
		this.vanTruckAgency = vanTruckAgency;
	}

	public String getTruckVanLineCode() {
		return truckVanLineCode;
	}

	public void setTruckVanLineCode(String truckVanLineCode) {
		this.truckVanLineCode = truckVanLineCode;
	}

	public List getTractorAgentVanNameList() {
		return tractorAgentVanNameList;
	}

	public void setTractorAgentVanNameList(List tractorAgentVanNameList) {
		this.tractorAgentVanNameList = tractorAgentVanNameList;
	}

	public String getCarrierTruckAgency() {
		return carrierTruckAgency;
	}

	public void setCarrierTruckAgency(String carrierTruckAgency) {
		this.carrierTruckAgency = carrierTruckAgency;
	}
	
	public void prepare() throws Exception {
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){  
			enbState = customerFileManager.enableStateList(sessionCorpID);
		}
	}
	
	private Map<String,String> driverAgencyList;
	public TruckAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String saveOnTabChange() throws Exception {
		String s = save();
		validateFormNav = "OK";
		return gotoPageString;
	}
	@SkipValidation
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		truck=new Truck();
		truck.setTruckStatus("A");
		truckStatus="A";
		String permKey = sessionCorpID +"-"+"component.truckingOps.OnBasisOfDotInspDue";
		Boolean dotInspection = AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		if(!dotInspection){
			dotInspFlag = false;
		}
		if(dotInspFlag){
		truckList = truckManager.searchTruck("", "", "", "",sessionCorpID, "", "", "",truckStatus,"true");
		}else{
			truckList = truckManager.searchTruck("", "", "", "",sessionCorpID, "", "", "",truckStatus,"");	
		}
		truckListForCount = truckManager.searchTruckDotInsp("", "", "", "",sessionCorpID, "", "", "",truckStatus);
		Date dueDate = null;
		Iterator it = truckListForCount.iterator();
		while(it.hasNext())
		{
			Truck truck123 = (Truck) it.next();
			 dueDate = truck123.getDotInspDue();
			 if(dueDate!=null){
			 long diffInDays = (long)(((dueDate.getTime()) - new java.util.Date().getTime())  / (1000 * 60 * 60 * 24));
			if(diffInDays < 60 && diffInDays > 29)
			 {
				counterSixty++ ;
			 }
			if(diffInDays > -1 && diffInDays < 30)
			 {
				counterThirty++ ;
			 }
			
			if(diffInDays <=-1)
			 {
				counterDueDate++ ;
			 }
	                
		}}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String truckTrailerlist() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		truckList = truckManager.findTruckTrailer(sessionCorpID);
		Iterator B=type.entrySet().iterator();
		int mapsize=type.size();			
		//Iterator keyValuePairs1 = aMap.entrySet().iterator();
		for (int i = 0; i < mapsize; i++)
		{
		  Map.Entry entry = (Map.Entry) B.next();
		  Object key = entry.getKey();
		  Object value = entry.getValue();
		  if(key.equals("TT") || key.equals("TRL"))
		  trailerTactorType.put(key.toString(), value.toString());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String carrierListInDomestic() {		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		carrierListInDomestic = truckManager.findCarrierListInDomestic(truckNumber, sessionCorpID);
		if(carrierListInDomestic!=null && (!carrierListInDomestic.isEmpty()) && carrierListInDomestic.get(0)!=null){
			String argTruckAgency[]=carrierListInDomestic.get(0).toString().split("~");
			if(argTruckAgency[1].equalsIgnoreCase("1")){
				argTruckAgency[1]="";
			}
			carrierTruckAgency=argTruckAgency[0]+"~"+argTruckAgency[1];
		}
	 logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String vanListDomestic(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		vanList=truckManager.findVanListInDomestic(truckNumber, sessionCorpID);
		if(vanList!=null && (!vanList.isEmpty()) && vanList.get(0)!=null){
			String argVanTruckAgency[]=vanList.get(0).toString().split("~");
			if(argVanTruckAgency[1].equalsIgnoreCase("1")){
				argVanTruckAgency[1]="";
			}
			vanTruckAgency=argVanTruckAgency[0]+"~"+argVanTruckAgency[1];
		}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	public List getVanList() {
		return vanList;
	}

	public void setVanList(List vanList) {
		this.vanList = vanList;
	}

	@SkipValidation
	public String trailerCodeInDomestic() {	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		carrierListInDomestic = truckManager.findTrailerListInDomestic(truckNumber, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	//Validate Control Expirations
	@SkipValidation
	public String controlExpirationsListInDomestic() {	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		controlExpirationsListInDomestic = truckManager.findControlExpirationsListInDomestic(parentId,deliveryLastDay, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
//	Validate Control Expirations Driver
	@SkipValidation
	public String findControlExpirationsListForDriver() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		controlExpirationsListInDomestic = truckManager.findControlExpirationsListForDriver(parentId,deliveryLastDay, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	//method for truck description 
	
	@SkipValidation
	public String truckDescription() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		truckDescriptionList = truckManager.findTruckDescriptionType(truckType, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	// Method for driver List
	@SkipValidation
	public String driverList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		truckDriverList = truckManager.findTruckDriverList(partnerCode, truckNumber, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private Company company;
	private CompanyManager companyManager;
	private String currencySign;
     //Enhancement for ControlExpirations
	@SkipValidation
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		List companyTemp = companyManager.findByCorpID(sessionCorpID);
		if(companyTemp!=null && !companyTemp.isEmpty() && companyTemp.get(0)!=null)
		company= (Company) companyTemp.get(0);
		currencySign=company.getCurrencySign();		
		
		if (id != null) {
			truck = truckManager.get(id);
			states = customerFileManager.findDefaultStateList(truck.getCountry(), sessionCorpID);
			parentId=truck.getLocalNumber();
		} else {
			truck = new Truck();
			states = customerFileManager.findDefaultStateList("", sessionCorpID);
			truck.setCorpID(sessionCorpID);
            truck.setTruckStatus("A");
			truck.setCreatedOn(new Date());
			truck.setUpdatedOn(new Date());
		}
		driverList =truckManager.findDriverList(truck.getWarehouse(),sessionCorpID);
		ownerOperatorList=truckManager.findOwnerOperatorList(parentId, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	private String truckNo;
	public String save() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		truck.setCorpID(sessionCorpID);
		boolean isNew = (truck.getId() == null);
		List isExisted = truckManager.isExisted(truck.getLocalNumber(),sessionCorpID);
		if (!isExisted.isEmpty()&&(truckNo!=null)&&(!truckNo.equalsIgnoreCase(truck.getLocalNumber()))){
			errorMessage("Truck Number {"+truck.getLocalNumber()+"} already exist.");
			truck.setLocalNumber(truckNo);
			return ERROR;
		}
		if (!isExisted.isEmpty()&& isNew){
			errorMessage("Truck Number {"+truck.getLocalNumber()+"} already exist.");
			truck.setLocalNumber(truckNo);
			return ERROR;
		}
		if (isNew) {
			truck.setCreatedOn(new Date());
		} 
		truck.setUpdatedOn(new Date());
		truck.setUpdatedBy(getRequest().getRemoteUser());
		if(truck.getState()==null){
			truck.setState("");
		}
		truck=truckManager.save(truck);
		states = customerFileManager.findDefaultStateList(truck.getCountry(), sessionCorpID);
		driverList =truckManager.findDriverList(truck.getWarehouse(),sessionCorpID);
		ownerOperatorList=truckManager.findOwnerOperatorList(parentId, sessionCorpID);
		
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "truck.added" : "truck.updated";
			saveMessage(getText(key));
		}
		hitFlag = "1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}

	@SkipValidation
	public String search() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		if(truck!=null){
			if(truck.getTruckStatus().equalsIgnoreCase("true")){
				truckStatus="A";
			}
			else{
				truckStatus="I";
			}
		}
		String permKey = sessionCorpID +"-"+"component.truckingOps.OnBasisOfDotInspDue";
		Boolean dotInspection = AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		if(dotInspection){
		if(dotInspFlag==false){
			if(myListFor == 60 || myListFor == 30 || myListFor == 1){
		truckList = truckManager.searchTruckDueSixty(truck.getWarehouse().replaceAll("'", "''"), truck.getLocalNumber().replaceAll("'", "''"), truck.getDescription().replaceAll("'", "''"), truck.getTagNumber().replaceAll("'", "''"), sessionCorpID,truck.getType(),truck.getState().replaceAll("'", "''"),truck.getOwnerPayTo().replaceAll("'", "''"),truckStatus,myListFor);
		}else{
		truckList = truckManager.searchTruck(truck.getWarehouse().replaceAll("'", "''"), truck.getLocalNumber().replaceAll("'", "''"), truck.getDescription().replaceAll("'", "''"), truck.getTagNumber().replaceAll("'", "''"), sessionCorpID,truck.getType(),truck.getState().replaceAll("'", "''"),truck.getOwnerPayTo().replaceAll("'", "''"),truckStatus,"");
		}}else{
			if(myListFor == 60 || myListFor == 30 || myListFor == 1){
		truckList = truckManager.searchTruckDueSixty(truck.getWarehouse().replaceAll("'", "''"), truck.getLocalNumber().replaceAll("'", "''"), truck.getDescription().replaceAll("'", "''"), truck.getTagNumber().replaceAll("'", "''"), sessionCorpID,truck.getType(),truck.getState().replaceAll("'", "''"),truck.getOwnerPayTo().replaceAll("'", "''"),truckStatus,myListFor);
		}else{
			truckList = truckManager.searchTruckDotInsp(truck.getWarehouse().replaceAll("'", "''"), truck.getLocalNumber().replaceAll("'", "''"), truck.getDescription().replaceAll("'", "''"), truck.getTagNumber().replaceAll("'", "''"), sessionCorpID,truck.getType(),truck.getState().replaceAll("'", "''"),truck.getOwnerPayTo().replaceAll("'", "''"),truckStatus);
		}}
		}else{
			truckList = truckManager.searchTruck(truck.getWarehouse().replaceAll("'", "''"), truck.getLocalNumber().replaceAll("'", "''"), truck.getDescription().replaceAll("'", "''"), truck.getTagNumber().replaceAll("'", "''"), sessionCorpID,truck.getType(),truck.getState().replaceAll("'", "''"),truck.getOwnerPayTo().replaceAll("'", "''"),truckStatus,"");
		}
		truckListForCount = truckManager.searchTruckDotInsp(truck.getWarehouse().replaceAll("'", "''"), truck.getLocalNumber().replaceAll("'", "''"), truck.getDescription().replaceAll("'", "''"), truck.getTagNumber().replaceAll("'", "''"), sessionCorpID,truck.getType(),truck.getState().replaceAll("'", "''"),truck.getOwnerPayTo().replaceAll("'", "''"),truckStatus);
		Date dueDate = null;
		Iterator it = truckListForCount.iterator();
		while(it.hasNext())
		{
			Truck truck123 = (Truck) it.next();
			 dueDate = truck123.getDotInspDue();
			 if(dueDate!=null){
			 long diffInDays = (long)(((dueDate.getTime()) - new java.util.Date().getTime())  / (1000 * 60 * 60 * 24));
			 if(diffInDays < 60 && diffInDays > 29)
			 {
				counterSixty++ ;
			 }
			if(diffInDays > -1 && diffInDays < 30)
			 {
				counterThirty++ ;
			 }
			
			if(diffInDays <=-1)
			 {
				counterDueDate++ ;
			 }
	                
		}}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String searchTrailer() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		Iterator B=type.entrySet().iterator();
		int mapsize=type.size();			
		//Iterator keyValuePairs1 = aMap.entrySet().iterator();
		for (int i = 0; i < mapsize; i++)
		{
		  Map.Entry entry = (Map.Entry) B.next();
		  Object key = entry.getKey();
		  Object value = entry.getValue();
		  if(key.equals("TT") || key.equals("TRL"))
		  trailerTactorType.put(key.toString(), value.toString());
		}
		truckList = truckManager.searchTruckTrailer(truck.getWarehouse().replaceAll("'", "''"), truck.getLocalNumber().replaceAll("'", "''"), truck.getDescription().replaceAll("'", "''"), truck.getTagNumber().replaceAll("'", "''"), sessionCorpID,truck.getType(),truck.getState().replaceAll("'", "''"),truck.getOwnerPayTo().replaceAll("'", "''"));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		driverAgencyList=refMasterManager.findByParameter(sessionCorpID,"DRIVERAGENCY");
		yesno = refMasterManager.findByParameter(sessionCorpID, "YESNO");
		ocountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		glCode = refMasterManager.findByParameter(sessionCorpID, "GLCODES");
		house = refMasterManager.findByParameter(sessionCorpID, "HOUSE");
		trailertype= refMasterManager.findByParameter(sessionCorpID, "TRAILERTYPE");
		condition= refMasterManager.findByParameter(sessionCorpID, "CONDITION");
		glType = refMasterManager.findGlType(sessionCorpID);
		type = refMasterManager.findByParameter(sessionCorpID, "TRUCK_TYPE");	
		fleet=refMasterManager.findByParameter(sessionCorpID, "FLEET");
		countryCod = refMasterManager.findByParameter(corpId, "COUNTRY");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findStateList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		statess = customerFileManager.findStateList(bucket2, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findActiveStateList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		statess = customerFileManager.findActiveStateList(bucket2, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findDriverList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		driverList =truckManager.findDriverList(bucket2,sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findTractorAgentVanCodeMethod(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		tractorAgentVanNameList=truckManager.findAgentVanName(truckVanLineCode,sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public static Date getCreatedon() {
		return createdon;
	}

	public static void setCreatedon(Date createdon) {
		TruckAction.createdon = createdon;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Truck getTruck() {
		return truck;
	}

	public void setTruck(Truck truck) {
		this.truck = truck;
	}

	public void setTruckManager(TruckManager truckManager) {
		this.truckManager = truckManager;
	}

	public List getTruckList() {
		return truckList;
	}

	public void setTruckList(List truckList) {
		this.truckList = truckList;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public static Map<String, String> getYesno() {
		return yesno;
	}

	public static void setYesno(Map<String, String> yesno) {
		TruckAction.yesno = yesno;
	}

	public static Map<String, String> getStates() {
		return states;
	}
	public Map<String, String> getOcountry() {
		return ocountry;
	}
	public static void setStates(Map<String, String> states) {
		TruckAction.states = states;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public Map<String, String> getGlCode() {
		return glCode;
	}

	public void setGlCode(Map<String, String> glCode) {
		this.glCode = glCode;
	}

	public static Map<String, String> getHouse() {
		return house;
	}

	public static void setHouse(Map<String, String> house) {
		TruckAction.house = house;
	}

	public static Map<String, String> getGlType() {
		return glType;
	}

	public static void setGlType(Map<String, String> glType) {
		TruckAction.glType = glType;
	}

	public Map<String, String> getType() {
		return type;
	}

	public void setType(Map<String, String> type) {
		this.type = type;
	}

	/**
	 * @return the bucket2
	 */
	public String getBucket2() {
		return bucket2;
	}

	/**
	 * @param bucket2 the bucket2 to set
	 */
	public void setBucket2(String bucket2) {
		this.bucket2 = bucket2;
	}

	/**
	 * @return the statess
	 */
	public List getStatess() {
		return statess;
	}

	/**
	 * @param statess the statess to set
	 */
	public void setStatess(List statess) {
		this.statess = statess;
	}

	/**
	 * @return the driverList
	 */
	public List getDriverList() {
		return driverList;
	}

	/**
	 * @param driverList the driverList to set
	 */
	public void setDriverList(List driverList) {
		this.driverList = driverList;
	}

	/**
	 * @return the partnerCode
	 */
	public String getPartnerCode() {
		return partnerCode;
	}

	/**
	 * @param partnerCode the partnerCode to set
	 */
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the ownerOperatorList
	 */
	public List getOwnerOperatorList() {
		return ownerOperatorList;
	}

	/**
	 * @param ownerOperatorList the ownerOperatorList to set
	 */
	public void setOwnerOperatorList(List ownerOperatorList) {
		this.ownerOperatorList = ownerOperatorList;
	}

	/**
	 * @return the truckDriverList
	 */
	public List getTruckDriverList() {
		return truckDriverList;
	}

	/**
	 * @param truckDriverList the truckDriverList to set
	 */
	public void setTruckDriverList(List truckDriverList) {
		this.truckDriverList = truckDriverList;
	}

	/**
	 * @return the truckNumber
	 */
	public String getTruckNumber() {
		return truckNumber;
	}

	/**
	 * @param truckNumber the truckNumber to set
	 */
	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}

	/**
	 * @return the truckDescriptionList
	 */
	public List getTruckDescriptionList() {
		return truckDescriptionList;
	}

	/**
	 * @param truckDescriptionList the truckDescriptionList to set
	 */
	public void setTruckDescriptionList(List truckDescriptionList) {
		this.truckDescriptionList = truckDescriptionList;
	}

	/**
	 * @return the truckType
	 */
	public String getTruckType() {
		return truckType;
	}

	/**
	 * @param truckType the truckType to set
	 */
	public void setTruckType(String truckType) {
		this.truckType = truckType;
	}

	/**
	 * @return the hitFlag
	 */
	public String getHitFlag() {
		return hitFlag;
	}

	/**
	 * @param hitFlag the hitFlag to set
	 */
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	/**
	 * @return the carrierListInDomestic
	 */
	public List getCarrierListInDomestic() {
		return carrierListInDomestic;
	}

	/**
	 * @param carrierListInDomestic the carrierListInDomestic to set
	 */
	public void setCarrierListInDomestic(List carrierListInDomestic) {
		this.carrierListInDomestic = carrierListInDomestic;
	}

	/**
	 * @return the controlExpirationsListInDomestic
	 */
	public List getControlExpirationsListInDomestic() {
		return controlExpirationsListInDomestic;
	}

	/**
	 * @param controlExpirationsListInDomestic the controlExpirationsListInDomestic to set
	 */
	public void setControlExpirationsListInDomestic(
			List controlExpirationsListInDomestic) {
		this.controlExpirationsListInDomestic = controlExpirationsListInDomestic;
	}

	/**
	 * @return the deliveryLastDay
	 */
	public Date getDeliveryLastDay() {
		return deliveryLastDay;
	}

	/**
	 * @param deliveryLastDay the deliveryLastDay to set
	 */
	public void setDeliveryLastDay(Date deliveryLastDay) {
		this.deliveryLastDay = deliveryLastDay;
	}

	/**
	 * @return the trailerTactorType
	 */
	public static Map<String, String> getTrailerTactorType() {
		return trailerTactorType;
	}

	/**
	 * @param trailerTactorType the trailerTactorType to set
	 */
	public static void setTrailerTactorType(Map<String, String> trailerTactorType) {
		TruckAction.trailerTactorType = trailerTactorType;
	}

	public static Map<String, String> getTrailertype() {
		return trailertype;
	}

	public static void setTrailertype(Map<String, String> trailertype) {
		TruckAction.trailertype = trailertype;
	}

	public static Map<String, String> getCondition() {
		return condition;
	}

	public static void setCondition(Map<String, String> condition) {
		TruckAction.condition = condition;
	}

	public Map<String, String> getFleet() {
		return fleet;
	}

	public void setFleet(Map<String, String> fleet) {
		this.fleet = fleet;
	}

	public String getTruckStatus() {
		return truckStatus;
	}

	public void setTruckStatus(String truckStatus) {
		this.truckStatus = truckStatus;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getCurrencySign() {
		return currencySign;
	}

	public void setCurrencySign(String currencySign) {
		this.currencySign = currencySign;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public Map<String, String> getDriverAgencyList() {
		return driverAgencyList;
	}
	public void setDriverAgencyList(Map<String, String> driverAgencyList) {
		this.driverAgencyList = driverAgencyList;
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}

	public String getTruckNo() {
		return truckNo;
	}

	public void setTruckNo(String truckNo) {
		this.truckNo = truckNo;
	}

	public boolean isDotInspFlag() {
		return dotInspFlag;
	}

	public void setDotInspFlag(boolean dotInspFlag) {
		this.dotInspFlag = dotInspFlag;
	}

	public int getMyListFor() {
		return myListFor;
	}

	public void setMyListFor(int myListFor) {
		this.myListFor = myListFor;
	}

	public int getCounterSixty() {
		return counterSixty;
	}

	public void setCounterSixty(int counterSixty) {
		this.counterSixty = counterSixty;
	}

	public int getCounterThirty() {
		return counterThirty;
	}

	public void setCounterThirty(int counterThirty) {
		this.counterThirty = counterThirty;
	}

	public int getCounterDueDate() {
		return counterDueDate;
	}

	public void setCounterDueDate(int counterDueDate) {
		this.counterDueDate = counterDueDate;
	}

	public List getTruckListForCount() {
		return truckListForCount;
	}

	public void setTruckListForCount(List truckListForCount) {
		this.truckListForCount = truckListForCount;
	}
}
