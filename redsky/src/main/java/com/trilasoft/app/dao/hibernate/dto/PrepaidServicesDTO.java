package com.trilasoft.app.dao.hibernate.dto;

import java.text.SimpleDateFormat;

public class PrepaidServicesDTO {
	// Ticket No.6951 Prepaid Services extraction in account portal
	 
	private Object transferee;
	private Object initated;  
	private Object move;
	private Object job;
	private Object status;
	private Object invoices;
	public Object getTransferee() {
		return transferee;
	}
	public Object getInitated() {
		return initated;
	}
	public Object getMove() {
		return move;
	}
	public Object getJob() {
		return job;
	}
	public Object getStatus() {
		return status;
	}
	public Object getInvoices() {
		return invoices;
	}
	public void setTransferee(Object transferee) {
		this.transferee = transferee;
	}
	public void setInitated(Object initated) {
		this.initated = initated;
	}
	public void setMove(Object move) {
		this.move = move;
	}
	public void setJob(Object job) {
		this.job = job;
	}
	public void setStatus(Object status) {
		this.status = status;
	}
	public void setInvoices(Object invoices) {
		this.invoices = invoices;
	}

	@Override
	public String toString() {
		String returnString = transferee + "seprator" + ((initated!=null)?new SimpleDateFormat("yyyy/MM/dd").format(initated):"") + "seprator" + move + "seprator"
		+ job + "seprator" +  status + "seprator" + invoices ;
	return returnString;
	}
	
}
