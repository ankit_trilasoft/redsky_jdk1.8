<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head> 
<title><fmt:message key="formsList.title"/></title> 
<meta name="heading" content="<fmt:message key='formsList.heading'/>"/> 

<style type="text/css">
div#page {
margin:15px 0px 0px 10px;
padding:0pt;
text-align:center;
}
#mainPopup {
padding-left:0px;
padding-right:0px;
}

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
!margin-top:-18px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
   
<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['searchForm'].elements['reports.menu'].value = "";
	document.forms['searchForm'].elements['reports.description'].value = "";
}	

function userStatusCheck(target){
		var targetElement = target;
		var ids = targetElement.value;
		
		if(targetElement.checked){
      		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['searchForm'].elements['userCheck'].value = ids;
      		}else{
      			var userCheckStatus=	document.forms['searchForm'].elements['userCheck'].value = userCheckStatus + ',' + ids;
      			document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ids , '' );
     		document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
     	}
}

function checkAll(){
	document.forms['searchForm'].elements['userCheck'].value = "";
	
	var len = document.forms['searchForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['searchForm'].elements['DD'][i].checked = true ;
		userStatusCheck(searchForm.forms['searchForm'].elements['DD'][i]);
	}
}

function uncheckAll(){
	var len = document.forms['searchForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['searchForm'].elements['DD'][i].checked = false ;
		userStatusCheck(document.forms['searchForm'].elements['DD'][i]);
	}
	document.forms['searchForm'].elements['userCheck'].value="";
}


function show(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}
function printForms(){
	var checkBoxId = document.forms['searchForm'].elements['userCheck'].value;
	var jobNumber = document.forms['searchForm'].elements['jobNumber'].value;
	var claimNumber = document.forms['searchForm'].elements['claimNumber'].value;
	var noteID = document.forms['searchForm'].elements['noteID'].value;
	var custID = document.forms['searchForm'].elements['custID'].value;
	var regNumber = document.forms['searchForm'].elements['regNumber'].value;
	var invoiceNumber = document.forms['searchForm'].elements['invoiceNumber'].value;
	var bookNumber = document.forms['searchForm'].elements['bookNumber'].value;
	var preferredLanguage = document.forms['searchForm'].elements['preferredLanguage'].value;
	if(checkBoxId =='' || checkBoxId ==','){
		alert('Please select one or more document to print.');
	}else{
		var url = 'printForms.html?rId='+checkBoxId+'&jobNumber='+jobNumber+'&claimNumber='+claimNumber+'&noteID='+noteID+'&custID='+custID+'&preferredLanguage='+preferredLanguage+'&regNumber='+regNumber+'&invoiceNumber='+invoiceNumber+'&bookNumber='+bookNumber;
		location.href=url;
	}
}

function validatefields(id,targetElement,val){
	document.forms['searchForm'].elements['id'].value=id;
	document.forms['searchForm'].elements['fileType'].value = val;
	var url="viewReportWithParam.html?formFrom=list";
	document.forms['searchForm'].action = url;
	document.forms['searchForm'].submit();
	return true;	  
}

function search(){
	var url="searchReportsModule.html?decorator=popup&popup=true";
	document.forms['searchForm'].action = url;
	document.forms['searchForm'].submit();
	return true;
}
</script>	
</head>
<s:form id="searchForm" name="searchForm" action="" method="post" validate="true">   
<s:hidden name="userCheck"/>
<s:hidden name="fileType" />
<s:hidden name="docFileType" />
<s:hidden name="billing.id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="customerFile.id" />
<s:hidden name="billing.shipNumber" value="%{serviceOrder.shipNumber}"/> 
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>
<s:hidden name="jobType" value="<%=request.getParameter("jobType") %>"/>
<c:set var="jobType" value="<%=request.getParameter("jobType") %>" />	
<s:hidden name="docsxfer" value="<%=request.getParameter("docsxfer") %>" />
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="module" value="%{reports.module}"/>
<s:hidden name="reportName" value="${reports.description}"/>
<s:hidden name="reportModule" value="${reportModule}"/>
<s:hidden name="reportSubModule" value="${reportSubModule}"/>
<s:hidden name="billToCode" value="<%=request.getParameter("billToCode") %>"/>
<c:set var="billToCode" value="<%=request.getParameter("billToCode") %>"/>
<s:hidden name="modes" value="<%=request.getParameter("modes") %>"/>
<c:set var="modes" value="<%=request.getParameter("modes") %>"/>
<s:hidden name="routingfilter" value="<%=request.getParameter("routingfilter") %>"/>
<s:hidden name="companyDivision" value="<%=request.getParameter("companyDivision") %>"/>
<c:set var="companyDivision" value="<%=request.getParameter("companyDivision") %>"/>

<s:hidden name="preferredLanguage" value="<%=request.getParameter("preferredLanguage") %>"/>
<c:set var="preferredLanguage" value="<%=request.getParameter("preferredLanguage") %>" />

<s:hidden name="jobNumber" value="<%=request.getParameter("customerFile.sequenceNumber") %>"/>
<c:set var="jobNumber" value="<%=request.getParameter("jobNumber") %>" />


<s:hidden name="claimNumber" value="<%=request.getParameter("claimNumber") %>" />
<c:set var="claimNumber" value="<%=request.getParameter("claimNumber") %>" />

<s:hidden name="bookNumber" value="<%=request.getParameter("bookNumber") %>" />
<c:set var="bookNumber" value="<%=request.getParameter("bookNumber") %>" />

<s:hidden name="noteID" value="<%=request.getParameter("noteID") %>" />
<c:set var="noteID" value="<%=request.getParameter("noteID") %>" />

<s:hidden name="custID" value="<%=request.getParameter("custID") %>" />
<c:set var="custID" value="<%=request.getParameter("custID") %>" />

<s:hidden name="cid" value="<%=request.getParameter("cid") %>" />
<s:set name="cid" value="cid" scope="session"/>

<s:hidden name="sid" value="<%=request.getParameter("sid") %>" />
<s:set name="sid" value="sid" scope="session"/>
	
<s:hidden name="invoiceNumber" value="<%=request.getParameter("invoiceNumber") %>" />
<c:set var="invoiceNumber" value="<%=request.getParameter("invoiceNumber") %>" />

<s:hidden name="regNumber" value="<%=request.getParameter("regNumber")%>"/>
<c:set var="regNumber" value="<%=request.getParameter("regNumber") %>" />

<div id="Layer1" align="left" style="width:720px;border:1px solid #fff;">
<div id="otabs" style="margin-left:40px;">
	<ul>
		<li><a class=""><span>Search</span></a></li>
	</ul>
</div>
<div class="spnblk"></div>
<div style="padding-bottom: 0px;!padding-bottom: 7px;"></div>
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top:-8px;!margin-top:0px; "><span></span></div>
<div class="center-content">
	<table class="table" style="width:98%;position: relative;" >
		<thead>
			<tr>
				<th align="center">Form Category</th>
				<th align="center" colspan="2">Form Name</th>	
			</tr>
		</thead> 
		<tbody>
		 	<tr>
			   <td><s:textfield name="reports.menu" required="true" cssClass="input-text" size="33"/></td>
			   <td><s:textfield name="reports.description" required="true" cssClass="input-text" size="33"/></td>
				<td>
			       <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" key="button.search" onclick="search()"/>
			       <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
			   </td>
		  	</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" />
 
<s:set name="reportss" value="reportss" scope="request"/>
<div id="Layer1" style="width:720px">
	<div id="newmnav">
		<ul>
		    <li id="newmnav1" style="background:#FFF "><a href="javascript:window.open('subModuleReports.html?id=${id}&claimNumber=${claimNumber}&jobNumber=${jobNumber}&regNumber=${regNumber}&noteID=${noteID}&custID=${custID}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&bookNumber=${bookNumber}&decorator=popup&popup=true','forms',740,800)" class="current"><span>Forms List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <!--<li><a href="javascript:window.open('relatedForms.html?id=${id}&claimNumber=${claimNumber}&jobNumber=${jobNumber}&regNumber=${regNumber}&noteID=${noteID}&custID=${custID}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&bookNumber=${bookNumber}&decorator=popup&popup=true','forms',740,800)"><span>Related Forms</span></a></li>-->
		 </ul>
	</div>
	<div id="chkAllButton"  class="listwhitetext" style="display:none;" >
		<input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
		<input type="radio"  name="chk" onClick="uncheckAll()"  /><strong>Uncheck All</strong>
	</div>
	<div class="spn">&nbsp;</div>
			
	<display:table name="reportss" class="table" requestURI="" id="reportsList"  pagesize="50" style="width:720px;margin-top:2px;!margin-top:6px;">
	    <display:column title="">
			<c:if test="${reportsList.pdf}">
				<input type="checkbox" style="margin-left:5px;" id="checkboxId" name="DD" value="${reportsList.id}" onclick="userStatusCheck(this)"/>
			</c:if>
			<c:if test="${(reportsList.pdf == 'false')}">
				<input type="checkbox" style="margin-left:5px;" id="checkboxId" name="DD" value="${reportsList.id}" disabled/>
			</c:if>
	    </display:column>
	    
	    <display:column title="Form Category" sortProperty="menu"  sortable="true"  style="width:170px" ><a href="javascript:openWindow('viewFormParam.html?id=${reportsList.id}&jobType=${jobType}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&preferredLanguage=${preferredLanguage}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true','forms',900,650)"><c:out value="${reportsList.menu}" /></display:column>
	    <display:column property="subModule"   titleKey="reports.subModule"/>
	    <display:column property="description"  title="Form Name"  sortable="true" style="width:150px"/>
	    <display:column property="reportComment"  title="Form Description" style="width:230px"/>
	   
	    <display:column>
	    	<c:if test="${reportsList.type == 'D'}">
		    	<c:if test="${(reportsList.paramReq == 'No' && reportsList.pdf == true)}">
		    		<img alt="PDF File" src="<c:url value='/images/pdf1.gif'/>" onclick="return validatefields(${reportsList.id},'id=${reportsList.id}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true', 'PDF');"/>
		    	</c:if>
		    	<%-- 
		    	<c:if test="${(reportsList.paramReq == 'Yes' ||  reportsList.paramReq == '' ||  reportsList.paramReq == null) && reportsList.pdf == true}">
		    		<img alt="PDF File" src="<c:url value='/images/pdf1.gif'/>" onclick="javascript:window.open('viewFormParam.html?id=${reportsList.id}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true','forms',900,650)"/>
		    	</c:if>
		    	--%>
	    	</c:if>
	    	<%--
	    	<c:if test="${reportsList.type == 'R'}">
	    		<c:if test="${(reportsList.pdf == true)}">
		    		<img alt="PDF File" src="<c:url value='/images/pdf1.gif'/>" onclick="javascript:window.open('viewFormParam.html?id=${reportsList.id}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true','forms',900,650)"/>
		    	</c:if>
	    	</c:if>
	    	--%>
	    </display:column>
	    
		<display:column>
			<c:if test="${reportsList.type == 'D'}">
				<c:if test="${(reportsList.paramReq == 'No' && reportsList.rtf == true)}">
					<img alt="RTF File" src="<c:url value='/images/doc.gif'/>" onclick="return validatefields(${reportsList.id},'id=${reportsList.id}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true', 'RTF');"/>
				</c:if>
				<%--
				<c:if test="${(reportsList.paramReq == 'Yes' ||  reportsList.paramReq == '' ||  reportsList.paramReq == null) && reportsList.rtf == true}">
					<img alt="RTF File" src="<c:url value='/images/doc.gif'/>" onclick="javascript:window.open('viewFormParam.html?id=${reportsList.id}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true','forms',900,650)"/>
				</c:if>
				--%>
			</c:if>
			<%--
			<c:if test="${reportsList.type == 'R'}">
				<c:if test="${reportsList.rtf == true}">
					<img alt="RTF File" src="<c:url value='/images/doc.gif'/>" onclick="javascript:window.open('viewFormParam.html?id=${reportsList.id}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true','forms',900,650)"/>
				</c:if>
			</c:if>
			--%>
		</display:column>
		<display:column>
			<c:if test="${reportsList.type == 'D'}">
				<c:if test="${(reportsList.paramReq == 'No' && reportsList.docx == true)}">
					<img alt="DOCX File" src="<c:url value='/images/docx.gif'/>" onclick="return validatefields(${reportsList.id},'id=${reportsList.id}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&preferredLanguage=${preferredLanguage}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true', 'DOCX');"/>
				</c:if>
			</c:if>
		</display:column>
		<display:column title="Upload" >
			<c:if test="${(reportsList.docsxfer == 'Yes')}">
				<input type="button" class="cssbutton" onclick="javascript:window.open('viewFormParam.html?id=${reportsList.id}&claimNumber=${claimNumber}&cid=${custID}&jobNumber=${jobNumber}&regNumber=${regNumber}&bookNumber=${bookNumber}&noteID=${noteID}&preferredLanguage=${preferredLanguage}&custID=${custID}&docsxfer=${reportsList.docsxfer}&reportModule=${reportModule}&reportSubModule=${reportSubModule}&formReportFlag=F&decorator=popup&popup=true','forms',900,650)" value="<fmt:message key="button.upload" />"  />
			</c:if> 
		</display:column>
<display:column title="Email" >
						<c:url value="viewFormParamEmailSetup.html" var="url1" >
							<c:param name="id" value="${reportsList.id}"/>
							<c:param name="claimNumber" value="${claimNumber}"/>
							<c:param name="cid" value="${custID}"/>
							<c:param name="jobNumber" value="${jobNumber}"/>
							<c:param name="regNumber" value="${regNumber}"/>
							<c:param name="bookNumber" value="${bookNumber}"/>
							<c:param name="noteID" value="${noteID}"/>
							<c:param name="custID" value="${custID}"/>
							<c:param name="docsxfer" value="${reportsList.emailOut}"/>
							<c:param name="reportModule" value="${reportModule}"/>
							<c:param name="reportSubModule" value="${reportSubModule}"/>
							<c:param name="formReportFlag" value="F"/>
							<c:param name="formNameVal" value="${reportsList.description}"/>							
							<c:param name="decorator" value="popup"/>
							<c:param name="popup" value="true"/>
						</c:url>		 

			<c:if test="${(reportsList.emailOut == 'Yes')}">
				<input type="button" class="cssbutton" onclick="winClose('${reportsList.id}','${claimNumber}','${custID}','${jobNumber}','${regNumber}','${bookNumber}','${noteID}','${custID}','${reportsList.emailOut}','${reportModule}','${reportSubModule}','F','${reportsList.description}','${preferredLanguage}');" value="Email"  />
			</c:if> 
		</display:column>
		<display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
	    <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
	    <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/> 
	</display:table>
</div> 
<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:70px; font-size: 15" name="printBtn"  value="Print" onclick="printForms();"/>
</s:form>
<script type="text/javascript"> 
    //highlightTableRows("reportsList");  
</script> 
<script type="text/javascript">
    function winClose(id,claimNumber,cid,jobNumber,regNumber,bookNumber,noteID,custID,emailOut,reportModule,reportSubModule,formReportFlag,formNameVal,preferredLanguage){
        if(reportModule!=null && reportModule=='workTicket'){
        	reportModule="serviceOrder";
        }
    	window.opener.emailSetUpPopUp(id,claimNumber,cid,jobNumber,regNumber,bookNumber,noteID,custID,emailOut,reportModule,reportSubModule,formReportFlag,formNameVal,preferredLanguage);
        window.close();
    }
</script> 