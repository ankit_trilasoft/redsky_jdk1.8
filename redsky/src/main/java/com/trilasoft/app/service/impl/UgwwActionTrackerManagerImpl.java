package com.trilasoft.app.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.UgwwActionTrackerDao;
import com.trilasoft.app.model.IntegrationLogInfoDTO;
import com.trilasoft.app.model.UgwwActionTracker;
import com.trilasoft.app.service.UgwwActionTrackerManager;

public class UgwwActionTrackerManagerImpl extends GenericManagerImpl<UgwwActionTracker, Long> implements UgwwActionTrackerManager{
	 UgwwActionTrackerDao ugwwActionTrackerDao;
	
	
public UgwwActionTrackerManagerImpl(UgwwActionTrackerDao ugwwActionTrackerDao) {
	super(ugwwActionTrackerDao);
	// TODO Auto-generated constructor stub
	this.ugwwActionTrackerDao= ugwwActionTrackerDao;
}

	public List getAllugwwactiontrackeList() {
		return ugwwActionTrackerDao.getAllugwwactiontrackeList();
	}
	  public List findId(String sequenceNumber,String corpId){
		return ugwwActionTrackerDao.findId(sequenceNumber,corpId);
	  }
	  
	  public List findsoId(String sequenceNumber,String corpId){
			return ugwwActionTrackerDao.findsoId(sequenceNumber,corpId);
		  }

	public List findRateRequestId(String sequenceNumber,String corpId) {
		return ugwwActionTrackerDao.findRateRequestId(sequenceNumber,corpId);
	}

	public List findAccountPolicyUrlId(String sequenceNumber,String corpId) {
		return ugwwActionTrackerDao.findAccountPolicyUrlId(sequenceNumber,corpId);
	}

	public List findFormPageUrlId(String sequenceNumber,String corpId) {
		return ugwwActionTrackerDao.findFormPageUrlId(sequenceNumber,corpId);
	}
	
	public List findsodId(String shipNumber,String corpId){
		return ugwwActionTrackerDao.findsodId(shipNumber,corpId);
		
	}
	
	public List findAccountLineId(String shipNumber ,String corpId,Boolean accessQFromCF){
		return ugwwActionTrackerDao.findAccountLineId(shipNumber,corpId,accessQFromCF);
		
	}
	public List findFormPageId(String shipNumber ,String corpId){
		return ugwwActionTrackerDao.findFormPageId(shipNumber,corpId);
		
	}
	public List findServiceAuditPageId(String shipNumber ,String corpId){
		return ugwwActionTrackerDao.findServiceAuditPageId(shipNumber,corpId);
		
	}
	 public List findCustID(String shipNumber,String corpId){
		 return ugwwActionTrackerDao.findCustID(shipNumber,corpId);
	 }
	 

		public List getTrackerList(String soNumber, String action, String userName, Date actionTime, String corpID){
			 return ugwwActionTrackerDao.getTrackerList(soNumber,action,userName,actionTime,corpID);
		 }
	 
	 public List findQuotesID(String shipNumber,String corpId){
		 return ugwwActionTrackerDao.findQuotesID(shipNumber,corpId);
	 }
	 
	 
	 
	 public List findServiceQuotesID(String shipNumber,String corpId){
		 return ugwwActionTrackerDao.findServiceQuotesID(shipNumber,corpId);
	 }
	 public List findPartnerID(String searchstr, String sessionCorpID){
	
		 return ugwwActionTrackerDao.findPartnerID(searchstr,sessionCorpID);
	 }
	 public List getPartner(String partnerCode, String sessionCorpID){
		 return ugwwActionTrackerDao.getPartner(partnerCode,sessionCorpID);
	 }
	 public List findTicketDetailsId(String searchstr, String sessionCorpID){
		 return ugwwActionTrackerDao.findTicketDetailsId(searchstr,sessionCorpID);
	 }
	 public List findClaimDetailsId(String searchstr, String sessionCorpID){
		 
		 return ugwwActionTrackerDao.findClaimDetailsId(searchstr,sessionCorpID);
	 }
	  public List findQuotesList(String searchstr, String sessionCorpID){
		  return ugwwActionTrackerDao.findQuotesList(searchstr,sessionCorpID);
	  }
	  public List findQuotesListId(String searchstr, String sessionCorpID){
		  return ugwwActionTrackerDao.findQuotesListId(searchstr,sessionCorpID);
	  }
	  public List findOrderId(String searchstr, String sessionCorpID){
		  return ugwwActionTrackerDao.findOrderId(searchstr,sessionCorpID);
	  }

	  public List<IntegrationLogInfoDTO> getErrorLogList(String integrationId,String operation, String orderComplete,Date effectiveDate,String statusCode,String so,String corpID)
		 {
	  return ugwwActionTrackerDao.getErrorLogList(integrationId,operation,orderComplete,effectiveDate,statusCode,so,corpID);
}
	  public List getWeeklyIntegrationCenterXML(String corpId){
		   return ugwwActionTrackerDao.getWeeklyIntegrationCenterXML(corpId);
	  }
	  public List getMssLogList(String corpId){
		   return ugwwActionTrackerDao.getMssLogList(corpId);
	  }
	  public List getCentreVanlineList(String corpId){
		   return ugwwActionTrackerDao.getCentreVanlineList(corpId);
	  }

	  public List findDriverDashboardList(Date beginDate,Date endDate,String driverId,String driverAgency,String corpId){
		  return ugwwActionTrackerDao.findDriverDashboardList(beginDate,endDate,driverId,driverAgency,corpId);
	  }
	  public List findDriverDBChildList(Date beginDate,Date endDate,String driverId,String corpId){
		  return ugwwActionTrackerDao.findDriverDBChildList(beginDate,endDate,driverId,corpId);
	  }
	  public List findDriverDBSoDetails(Date beginDate,Date endDate,String driverId,String corpId){
		  return ugwwActionTrackerDao.findDriverDBSoDetails(beginDate,endDate,driverId,corpId);
	  }
	  public List findDriverDetails(String driverId,String corpId){
		  return ugwwActionTrackerDao.findDriverDetails(driverId,corpId);
	  }

	  public List findSoDashboardList(String corpId){
		   return ugwwActionTrackerDao.findSoDashboardList(corpId);
	  }
	  public List soDashboardSearchList(String shipNumber,String registrationNumber,String job1,String coordinator,String status,String corpId,String role,String lastName,String firstName,String cmpdiv,String billtoName,String originCity,String destinCity,Boolean actstatus){
		   return ugwwActionTrackerDao.soDashboardSearchList(shipNumber,registrationNumber,job1, coordinator,status,corpId,role,lastName,firstName,cmpdiv,billtoName,originCity,destinCity, actstatus);
	  }
	  public Map<String,String>  getFieldsNameToDisplay(){
		   return ugwwActionTrackerDao.getFieldsNameToDisplay();
	  }
	  public List getMoveForYouList(String corpId)
	  {
		  return ugwwActionTrackerDao.getMoveForYouList(corpId);
	  }
	  
	  public List getVoxmeEstimatList(String corpId)
	  {
		  return ugwwActionTrackerDao.getVoxmeEstimatList(corpId);
	  }
	  public List getPricePointListForIntegration(String corpId)
	  {
		  return ugwwActionTrackerDao.getPricePointListForIntegration(corpId); 
	  }
	  public List UGWWListForIntegration(String corpId)
	  {
		  return ugwwActionTrackerDao.UGWWListForIntegration(corpId); 
	  }
	  public List<IntegrationLogInfoDTO> getLisaLogList(String integrationId,String operation, String orderComplete, Date effectiveDate,String statusCode,String so,String corpID){
		  return ugwwActionTrackerDao.getLisaLogList(integrationId, operation, orderComplete, effectiveDate, statusCode, so, corpID);
	  }

	public List findsoFullId(String shipNumber, String corpId) {
		return ugwwActionTrackerDao.findsoFullId(shipNumber, corpId);
	}
}
