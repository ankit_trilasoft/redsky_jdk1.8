package com.trilasoft.app.model;

import java.util.Date;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class DayDistributionHoursDTO {
	
	private Date workDate;
	private String totalHours;
	private String percentHours;
	private String weightShare;
	private String revenueShare;
	private String perHour;
	private String ticketRevenueShare;
	private String otHours;
	private String totalSumHours;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("workDate", workDate).append(
				"totalHours", totalHours).append("percentHours", percentHours)
				.append("weightShare", weightShare).append("revenueShare",
						revenueShare).append("perHour", perHour).append(
						"ticketRevenueShare", ticketRevenueShare).append(
						"otHours", otHours).append("totalSumHours",
						totalSumHours).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DayDistributionHoursDTO))
			return false;
		DayDistributionHoursDTO castOther = (DayDistributionHoursDTO) other;
		return new EqualsBuilder().append(workDate, castOther.workDate).append(
				totalHours, castOther.totalHours).append(percentHours,
				castOther.percentHours).append(weightShare,
				castOther.weightShare).append(revenueShare,
				castOther.revenueShare).append(perHour, castOther.perHour)
				.append(ticketRevenueShare, castOther.ticketRevenueShare)
				.append(otHours, castOther.otHours).append(totalSumHours,
						castOther.totalSumHours).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(workDate).append(totalHours)
				.append(percentHours).append(weightShare).append(revenueShare)
				.append(perHour).append(ticketRevenueShare).append(otHours)
				.append(totalSumHours).toHashCode();
	}
	public String getPercentHours() {
		return percentHours;
	}
	public void setPercentHours(String percentHours) {
		this.percentHours = percentHours;
	}
	public String getPerHour() {
		return perHour;
	}
	public void setPerHour(String perHour) {
		this.perHour = perHour;
	}
	public String getRevenueShare() {
		return revenueShare;
	}
	public void setRevenueShare(String revenueShare) {
		this.revenueShare = revenueShare;
	}
	public String getTotalHours() {
		return totalHours;
	}
	public void setTotalHours(String totalHours) {
		this.totalHours = totalHours;
	}
	public String getWeightShare() {
		return weightShare;
	}
	public void setWeightShare(String weightShare) {
		this.weightShare = weightShare;
	}
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}
	public String getTicketRevenueShare() {
		return ticketRevenueShare;
	}
	public void setTicketRevenueShare(String ticketRevenueShare) {
		this.ticketRevenueShare = ticketRevenueShare;
	}
	public String getOtHours() {
		return otHours;
	}
	public void setOtHours(String otHours) {
		this.otHours = otHours;
	}
	public String getTotalSumHours() {
		return totalSumHours;
	}
	public void setTotalSumHours(String totalSumHours) {
		this.totalSumHours = totalSumHours;
	}
	
	

}
