package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table ( name="removalrelocationservice") 
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")

public class RemovalRelocationService extends BaseObject{
	private Long id;
	private Long customerFileId;
	private String corpId;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;

	private Boolean isIta60;
	private Boolean isIta1m3;
	private Boolean isIta2m3;
	private Boolean isIta3m3;
	private Boolean isFtExpat20;
	private Boolean isFtExpat40;
	private Boolean isKgExpat30;
	private Boolean isTransportAllow;
	private Boolean isFtSingle20;
	private Boolean isFtFamily40;
	private Boolean isStorage;
	private Boolean isOrientation8;
	private Boolean isOrientation16;
	private Boolean isOrientation24;
	private Boolean isHomeSearch8;
	private Boolean isHomeSearch16;
	private Boolean isHomeSearch24;
	private Boolean isHomeFurnished;
	private Boolean isHomeUnfurnished;
	private Boolean isUsdollar;
	private Boolean isDollar;
	private Boolean isOtherCurrency;
	private Boolean isSchoolSearch8;
	private Boolean isSchoolSearch16;
	private Boolean isSchoolSearch24;
	private Boolean isSelfInSearch8;
	private Boolean isSelfInSearch16;
	private Boolean isSelfInSearch24;
	private String usdollarAmounts;
	private String dollarAmounts;
	private String otherCurrencyAmounts;
	private String otherServicesRequired; 
	private String ftExpat40ExceptionalItems; 
	private String kgExpat30ExceptionalItems;
	private String transportAllowExceptionalItems;
	private Boolean days2;
	private Boolean days3;
	private Boolean days4;
	private Boolean orientationTrip;
	private Boolean homeSearch ;
	private Boolean settlingInAssistance;
	private Boolean schoolSearch ;
	private String authorizedDays ;
	private String additionalNotes;
	private Boolean houseRentalFlag;
	private Boolean requestedlocationFlag;
	private Boolean rentalAmountFlag;
	private Boolean expectedMoveDateFlag;
	private Boolean utilitycostFlag;
	private Boolean cleaning;
	private Boolean telephoneConnection;
	private Boolean internetConnectionFlag;
	private Boolean cableConnectionFlag;
	private Boolean laundryCostFlag;
	private Boolean temporaryCarFlag;
	private Boolean gpaNavigationFlag;
	private Boolean carContractFlag;
	private Boolean taxRulingFlag;
	private Boolean taxAssistance;
	private String taxAssistanceYear;
	private String jobLevel;
	private String temporaryCarMonths;
	private String telephoneCallsCost;
	private String requestedArea;
	private String rentAmount;
	private String houseRentalMonths;
	private Date expextedMoveDate;
	private Boolean furnished;
	private Boolean unFurnished;
	private Boolean entryVisa;
	private Boolean longTermStayVisa;
	private Boolean workPermit;
	
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"customerFileId", customerFileId).append("corpId", corpId)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("isIta60", isIta60).append("isIta1m3", isIta1m3).append("isIta2m3",isIta2m3)
				.append("isIta3m3", isIta3m3)
				.append("isFtExpat20", isFtExpat20).append("isFtExpat40",
						isFtExpat40).append("isKgExpat30", isKgExpat30).append(
						"isTransportAllow", isTransportAllow).append(
						"isFtSingle20", isFtSingle20).append("isFtFamily40",
						isFtFamily40).append("isStorage", isStorage).append(
						"isOrientation8", isOrientation8).append(
						"isOrientation16", isOrientation16).append(
						"isOrientation24", isOrientation24).append(
						"isHomeSearch8", isHomeSearch8).append(
						"isHomeSearch16", isHomeSearch16).append(
						"isHomeSearch24", isHomeSearch24).append(
						"isHomeFurnished", isHomeFurnished).append(
						"isHomeUnfurnished", isHomeUnfurnished).append(
						"isUsdollar", isUsdollar).append("isDollar", isDollar)
				.append("isOtherCurrency", isOtherCurrency).append(
						"isSchoolSearch8", isSchoolSearch8).append(
						"isSchoolSearch16", isSchoolSearch16).append(
						"isSchoolSearch24", isSchoolSearch24).append(
						"isSelfInSearch8", isSelfInSearch8).append(
						"isSelfInSearch16", isSelfInSearch16).append(
						"isSelfInSearch24", isSelfInSearch24).append(
						"usdollarAmounts", usdollarAmounts).append(
						"dollarAmounts", dollarAmounts).append(
						"otherCurrencyAmounts", otherCurrencyAmounts).append(
						"otherServicesRequired", otherServicesRequired).append(
						"ftExpat40ExceptionalItems", ftExpat40ExceptionalItems)
				.append("kgExpat30ExceptionalItems", kgExpat30ExceptionalItems)
				.append("transportAllowExceptionalItems",
						transportAllowExceptionalItems).append("days2", days2)
				.append("days3", days3).append("days4", days4).append(
						"orientationTrip", orientationTrip).append(
						"homeSearch", homeSearch).append(
						"settlingInAssistance", settlingInAssistance).append(
						"schoolSearch", schoolSearch).append("authorizedDays",
						authorizedDays).append("additionalNotes",
						additionalNotes).append("houseRentalFlag",
						houseRentalFlag).append("requestedlocationFlag",
						requestedlocationFlag).append("rentalAmountFlag",
						rentalAmountFlag).append("expectedMoveDateFlag",
						expectedMoveDateFlag).append("utilitycostFlag",
						utilitycostFlag).append("cleaning", cleaning).append(
						"telephoneConnection", telephoneConnection).append(
						"internetConnectionFlag", internetConnectionFlag)
				.append("cableConnectionFlag", cableConnectionFlag).append(
						"laundryCostFlag", laundryCostFlag).append(
						"temporaryCarFlag", temporaryCarFlag).append(
						"gpaNavigationFlag", gpaNavigationFlag).append(
						"carContractFlag", carContractFlag).append(
						"taxRulingFlag", taxRulingFlag).append("taxAssistance",
						taxAssistance).append("taxAssistanceYear",
						taxAssistanceYear).append("jobLevel", jobLevel).append(
						"temporaryCarMonths", temporaryCarMonths).append(
						"telephoneCallsCost", telephoneCallsCost).append(
						"requestedArea", requestedArea).append("rentAmount",
						rentAmount).append("houseRentalMonths",
						houseRentalMonths).append("expextedMoveDate",
						expextedMoveDate).append("furnished", furnished)
				.append("unFurnished", unFurnished).append("entryVisa",
						entryVisa).append("longTermStayVisa", longTermStayVisa)
				.append("workPermit", workPermit).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RemovalRelocationService))
			return false;
		RemovalRelocationService castOther = (RemovalRelocationService) other;
		return new EqualsBuilder().append(id, castOther.id).append(
				customerFileId, castOther.customerFileId).append(corpId,
				castOther.corpId).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(isIta60, castOther.isIta60)
				.append(isIta1m3, castOther.isIta1m3).append(isIta2m3, castOther.isIta2m3).append(isIta3m3,
						castOther.isIta3m3).append(isFtExpat20,
						castOther.isFtExpat20).append(isFtExpat40,
						castOther.isFtExpat40).append(isKgExpat30,
						castOther.isKgExpat30).append(isTransportAllow,
						castOther.isTransportAllow).append(isFtSingle20,
						castOther.isFtSingle20).append(isFtFamily40,
						castOther.isFtFamily40).append(isStorage,
						castOther.isStorage).append(isOrientation8,
						castOther.isOrientation8).append(isOrientation16,
						castOther.isOrientation16).append(isOrientation24,
						castOther.isOrientation24).append(isHomeSearch8,
						castOther.isHomeSearch8).append(isHomeSearch16,
						castOther.isHomeSearch16).append(isHomeSearch24,
						castOther.isHomeSearch24).append(isHomeFurnished,
						castOther.isHomeFurnished).append(isHomeUnfurnished,
						castOther.isHomeUnfurnished).append(isUsdollar,
						castOther.isUsdollar).append(isDollar,
						castOther.isDollar).append(isOtherCurrency,
						castOther.isOtherCurrency).append(isSchoolSearch8,
						castOther.isSchoolSearch8).append(isSchoolSearch16,
						castOther.isSchoolSearch16).append(isSchoolSearch24,
						castOther.isSchoolSearch24).append(isSelfInSearch8,
						castOther.isSelfInSearch8).append(isSelfInSearch16,
						castOther.isSelfInSearch16).append(isSelfInSearch24,
						castOther.isSelfInSearch24).append(usdollarAmounts,
						castOther.usdollarAmounts).append(dollarAmounts,
						castOther.dollarAmounts).append(otherCurrencyAmounts,
						castOther.otherCurrencyAmounts).append(
						otherServicesRequired, castOther.otherServicesRequired)
				.append(ftExpat40ExceptionalItems,
						castOther.ftExpat40ExceptionalItems).append(
						kgExpat30ExceptionalItems,
						castOther.kgExpat30ExceptionalItems).append(
						transportAllowExceptionalItems,
						castOther.transportAllowExceptionalItems).append(days2,
						castOther.days2).append(days3, castOther.days3).append(
						days4, castOther.days4).append(orientationTrip,
						castOther.orientationTrip).append(homeSearch,
						castOther.homeSearch).append(settlingInAssistance,
						castOther.settlingInAssistance).append(schoolSearch,
						castOther.schoolSearch).append(authorizedDays,
						castOther.authorizedDays).append(additionalNotes,
						castOther.additionalNotes).append(houseRentalFlag,
						castOther.houseRentalFlag).append(
						requestedlocationFlag, castOther.requestedlocationFlag)
				.append(rentalAmountFlag, castOther.rentalAmountFlag).append(
						expectedMoveDateFlag, castOther.expectedMoveDateFlag)
				.append(utilitycostFlag, castOther.utilitycostFlag).append(
						cleaning, castOther.cleaning).append(
						telephoneConnection, castOther.telephoneConnection)
				.append(internetConnectionFlag,
						castOther.internetConnectionFlag).append(
						cableConnectionFlag, castOther.cableConnectionFlag)
				.append(laundryCostFlag, castOther.laundryCostFlag).append(
						temporaryCarFlag, castOther.temporaryCarFlag).append(
						gpaNavigationFlag, castOther.gpaNavigationFlag).append(
						carContractFlag, castOther.carContractFlag).append(
						taxRulingFlag, castOther.taxRulingFlag).append(
						taxAssistance, castOther.taxAssistance).append(
						taxAssistanceYear, castOther.taxAssistanceYear).append(
						jobLevel, castOther.jobLevel).append(
						temporaryCarMonths, castOther.temporaryCarMonths)
				.append(telephoneCallsCost, castOther.telephoneCallsCost)
				.append(requestedArea, castOther.requestedArea).append(
						rentAmount, castOther.rentAmount).append(
						houseRentalMonths, castOther.houseRentalMonths).append(
						expextedMoveDate, castOther.expextedMoveDate).append(
						furnished, castOther.furnished).append(unFurnished,
						castOther.unFurnished).append(entryVisa,
						castOther.entryVisa).append(longTermStayVisa,
						castOther.longTermStayVisa).append(workPermit,
						castOther.workPermit).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(customerFileId).append(
				corpId).append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(isIta60).append(isIta1m3).append(isIta2m3).append(
						isIta3m3).append(isFtExpat20).append(isFtExpat40)
				.append(isKgExpat30).append(isTransportAllow).append(
						isFtSingle20).append(isFtFamily40).append(isStorage)
				.append(isOrientation8).append(isOrientation16).append(
						isOrientation24).append(isHomeSearch8).append(
						isHomeSearch16).append(isHomeSearch24).append(
						isHomeFurnished).append(isHomeUnfurnished).append(
						isUsdollar).append(isDollar).append(isOtherCurrency)
				.append(isSchoolSearch8).append(isSchoolSearch16).append(
						isSchoolSearch24).append(isSelfInSearch8).append(
						isSelfInSearch16).append(isSelfInSearch24).append(
						usdollarAmounts).append(dollarAmounts).append(
						otherCurrencyAmounts).append(otherServicesRequired)
				.append(ftExpat40ExceptionalItems).append(
						kgExpat30ExceptionalItems).append(
						transportAllowExceptionalItems).append(days2).append(
						days3).append(days4).append(orientationTrip).append(
						homeSearch).append(settlingInAssistance).append(
						schoolSearch).append(authorizedDays).append(
						additionalNotes).append(houseRentalFlag).append(
						requestedlocationFlag).append(rentalAmountFlag).append(
						expectedMoveDateFlag).append(utilitycostFlag).append(
						cleaning).append(telephoneConnection).append(
						internetConnectionFlag).append(cableConnectionFlag)
				.append(laundryCostFlag).append(temporaryCarFlag).append(
						gpaNavigationFlag).append(carContractFlag).append(
						taxRulingFlag).append(taxAssistance).append(
						taxAssistanceYear).append(jobLevel).append(
						temporaryCarMonths).append(telephoneCallsCost).append(
						requestedArea).append(rentAmount).append(
						houseRentalMonths).append(expextedMoveDate).append(
						furnished).append(unFurnished).append(entryVisa)
				.append(longTermStayVisa).append(workPermit).toHashCode();
	}
	@Column
	public Boolean getDays2() {
		return days2;
	}
	public void setDays2(Boolean days2) {
		this.days2 = days2;
	}
	
	@Column
	public Boolean getDays3() {
		return days3;
	}
	public void setDays3(Boolean days3) {
		this.days3 = days3;
	}
	
	@Column
	public Boolean getDays4() {
		return days4;
	}
	public void setDays4(Boolean days4) {
		this.days4 = days4;
	}
	
	@Column
	public Boolean getHomeSearch() {
		return homeSearch;
	}
	public void setHomeSearch(Boolean homeSearch) {
		this.homeSearch = homeSearch;
	}
	
	@Column
	public Boolean getOrientationTrip() {
		return orientationTrip;
	}
	public void setOrientationTrip(Boolean orientationTrip) {
		this.orientationTrip = orientationTrip;
	}
	
	@Column
	public Boolean getSchoolSearch() {
		return schoolSearch;
	}
	public void setSchoolSearch(Boolean schoolSearch) {
		this.schoolSearch = schoolSearch;
	}
	
	@Column
	public Boolean getSettlingInAssistance() {
		return settlingInAssistance;
	}
	public void setSettlingInAssistance(Boolean settlingInAssistance) {
		this.settlingInAssistance = settlingInAssistance;
	}
	@Column(length = 15)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length = 20)
	public Long getCustomerFileId() {
		return customerFileId;
	}	
	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	@Id 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Boolean getIsDollar() {
		return isDollar;
	}
	public void setIsDollar(Boolean isDollar) {
		this.isDollar = isDollar;
	}
	@Column
	public Boolean getIsFtExpat20() {
		return isFtExpat20;
	}	
	public void setIsFtExpat20(Boolean isFtExpat20) {
		this.isFtExpat20 = isFtExpat20;
	}
	@Column
	public Boolean getIsFtExpat40() {
		return isFtExpat40;
	}
	public void setIsFtExpat40(Boolean isFtExpat40) {
		this.isFtExpat40 = isFtExpat40;
	}
	@Column
	public Boolean getIsFtFamily40() {
		return isFtFamily40;
	}
	public void setIsFtFamily40(Boolean isFtFamily40) {
		this.isFtFamily40 = isFtFamily40;
	}
	@Column
	public Boolean getIsFtSingle20() {
		return isFtSingle20;
	}
	public void setIsFtSingle20(Boolean isFtSingle20) {
		this.isFtSingle20 = isFtSingle20;
	}
	@Column
	public Boolean getIsHomeFurnished() {
		return isHomeFurnished;
	}
	public void setIsHomeFurnished(Boolean isHomeFurnished) {
		this.isHomeFurnished = isHomeFurnished;
	}
	@Column
	public Boolean getIsHomeSearch16() {
		return isHomeSearch16;
	}
	public void setIsHomeSearch16(Boolean isHomeSearch16) {
		this.isHomeSearch16 = isHomeSearch16;
	}
	@Column
	public Boolean getIsHomeSearch24() {
		return isHomeSearch24;
	}
	public void setIsHomeSearch24(Boolean isHomeSearch24) {
		this.isHomeSearch24 = isHomeSearch24;
	}
	@Column
	public Boolean getIsHomeSearch8() {
		return isHomeSearch8;
	}
	public void setIsHomeSearch8(Boolean isHomeSearch8) {
		this.isHomeSearch8 = isHomeSearch8;
	}
	@Column
	public Boolean getIsHomeUnfurnished() {
		return isHomeUnfurnished;
	}
	public void setIsHomeUnfurnished(Boolean isHomeUnfurnished) {
		this.isHomeUnfurnished = isHomeUnfurnished;
	}
	@Column
	public Boolean getIsIta1m3() {
		return isIta1m3;
	}
	public void setIsIta1m3(Boolean isIta1m3) {
		this.isIta1m3 = isIta1m3;
	}
	@Column
	public Boolean getIsIta3m3() {
		return isIta3m3;
	}
	public void setIsIta3m3(Boolean isIta3m3) {
		this.isIta3m3 = isIta3m3;
	}
	@Column
	public Boolean getIsIta60() {
		return isIta60;
	}
	public void setIsIta60(Boolean isIta60) {
		this.isIta60 = isIta60;
	}
	@Column
	public Boolean getIsKgExpat30() {
		return isKgExpat30;
	}
	public void setIsKgExpat30(Boolean isKgExpat30) {
		this.isKgExpat30 = isKgExpat30;
	}
	@Column
	public Boolean getIsOrientation16() {
		return isOrientation16;
	}
	public void setIsOrientation16(Boolean isOrientation16) {
		this.isOrientation16 = isOrientation16;
	}
	@Column
	public Boolean getIsOrientation24() {
		return isOrientation24;
	}
	public void setIsOrientation24(Boolean isOrientation24) {
		this.isOrientation24 = isOrientation24;
	}
	@Column
	public Boolean getIsOrientation8() {
		return isOrientation8;
	}
	public void setIsOrientation8(Boolean isOrientation8) {
		this.isOrientation8 = isOrientation8;
	}
	@Column
	public Boolean getIsOtherCurrency() {
		return isOtherCurrency;
	}
	public void setIsOtherCurrency(Boolean isOtherCurrency) {
		this.isOtherCurrency = isOtherCurrency;
	}
	@Column
	public Boolean getIsSchoolSearch16() {
		return isSchoolSearch16;
	}
	public void setIsSchoolSearch16(Boolean isSchoolSearch16) {
		this.isSchoolSearch16 = isSchoolSearch16;
	}
	@Column
	public Boolean getIsSchoolSearch24() {
		return isSchoolSearch24;
	}
	public void setIsSchoolSearch24(Boolean isSchoolSearch24) {
		this.isSchoolSearch24 = isSchoolSearch24;
	}
	@Column
	public Boolean getIsSchoolSearch8() {
		return isSchoolSearch8;
	}
	public void setIsSchoolSearch8(Boolean isSchoolSearch8) {
		this.isSchoolSearch8 = isSchoolSearch8;
	}
	@Column
	public Boolean getIsSelfInSearch16() {
		return isSelfInSearch16;
	}
	public void setIsSelfInSearch16(Boolean isSelfInSearch16) {
		this.isSelfInSearch16 = isSelfInSearch16;
	}
	@Column
	public Boolean getIsSelfInSearch24() {
		return isSelfInSearch24;
	}
	public void setIsSelfInSearch24(Boolean isSelfInSearch24) {
		this.isSelfInSearch24 = isSelfInSearch24;
	}
	@Column
	public Boolean getIsSelfInSearch8() {
		return isSelfInSearch8;
	}
	public void setIsSelfInSearch8(Boolean isSelfInSearch8) {
		this.isSelfInSearch8 = isSelfInSearch8;
	}
	@Column
	public Boolean getIsStorage() {
		return isStorage;
	}
	public void setIsStorage(Boolean isStorage) {
		this.isStorage = isStorage;
	}
	@Column
	public Boolean getIsTransportAllow() {
		return isTransportAllow;
	}
	public void setIsTransportAllow(Boolean isTransportAllow) {
		this.isTransportAllow = isTransportAllow;
	}
	@Column
	public Boolean getIsUsdollar() {
		return isUsdollar;
	}
	public void setIsUsdollar(Boolean isUsdollar) {
		this.isUsdollar = isUsdollar;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column(length=6)
	public String getDollarAmounts() {
		return dollarAmounts;
	}
	public void setDollarAmounts(String dollarAmounts) {
		this.dollarAmounts = dollarAmounts;
	}
	
	@Column(length=6)
	public String getOtherCurrencyAmounts() {
		return otherCurrencyAmounts;
	}
	public void setOtherCurrencyAmounts(String otherCurrencyAmounts) {
		this.otherCurrencyAmounts = otherCurrencyAmounts;
	}
	
	@Column(length=5000)
	public String getOtherServicesRequired() {
		return otherServicesRequired;
	}
	public void setOtherServicesRequired(String otherServicesRequired) {
		this.otherServicesRequired = otherServicesRequired;
	}
	
	@Column(length=45)
	public String getFtExpat40ExceptionalItems() {
		return ftExpat40ExceptionalItems;
	}
	public void setFtExpat40ExceptionalItems(String ftExpat40ExceptionalItems) {
		this.ftExpat40ExceptionalItems = ftExpat40ExceptionalItems;
	}
	
	@Column(length=45)
	public String getKgExpat30ExceptionalItems() {
		return kgExpat30ExceptionalItems;
	}
	public void setKgExpat30ExceptionalItems(String kgExpat30ExceptionalItems) {
		this.kgExpat30ExceptionalItems = kgExpat30ExceptionalItems;
	}
	
	@Column(length=45)
	public String getTransportAllowExceptionalItems() {
		return transportAllowExceptionalItems;
	}
	public void setTransportAllowExceptionalItems(
			String transportAllowExceptionalItems) {
		this.transportAllowExceptionalItems = transportAllowExceptionalItems;
	}
	
	@Column(length=6)
	public String getUsdollarAmounts() {
		return usdollarAmounts;
	}
	public void setUsdollarAmounts(String usdollarAmounts) {
		this.usdollarAmounts = usdollarAmounts;
	}
	
	@Column(length=30)
	public String getAuthorizedDays() {
		return authorizedDays;
	}
	public void setAuthorizedDays(String authorizedDays) {
		this.authorizedDays = authorizedDays;
	}
	
	@Column(length=5000)
	public String getAdditionalNotes() {
		return additionalNotes;
	}
	public void setAdditionalNotes(String additionalNotes) {
		this.additionalNotes = additionalNotes;
	}
	@Column
	public Boolean getCableConnectionFlag() {
		return cableConnectionFlag;
	}
	public void setCableConnectionFlag(Boolean cableConnectionFlag) {
		this.cableConnectionFlag = cableConnectionFlag;
	}
	@Column
	public Boolean getCarContractFlag() {
		return carContractFlag;
	}
	public void setCarContractFlag(Boolean carContractFlag) {
		this.carContractFlag = carContractFlag;
	}
	@Column
	public Boolean getCleaning() {
		return cleaning;
	}
	public void setCleaning(Boolean cleaning) {
		this.cleaning = cleaning;
	}
	@Column
	public Boolean getExpectedMoveDateFlag() {
		return expectedMoveDateFlag;
	}
	public void setExpectedMoveDateFlag(Boolean expectedMoveDateFlag) {
		this.expectedMoveDateFlag = expectedMoveDateFlag;
	}
	@Column
	public Date getExpextedMoveDate() {
		return expextedMoveDate;
	}
	public void setExpextedMoveDate(Date expextedMoveDate) {
		this.expextedMoveDate = expextedMoveDate;
	}
	@Column
	public Boolean getGpaNavigationFlag() {
		return gpaNavigationFlag;
	}
	public void setGpaNavigationFlag(Boolean gpaNavigationFlag) {
		this.gpaNavigationFlag = gpaNavigationFlag;
	}
	@Column
	public Boolean getHouseRentalFlag() {
		return houseRentalFlag;
	}
	public void setHouseRentalFlag(Boolean houseRentalFlag) {
		this.houseRentalFlag = houseRentalFlag;
	}
	@Column
	public String getHouseRentalMonths() {
		return houseRentalMonths;
	}
	public void setHouseRentalMonths(String houseRentalMonths) {
		this.houseRentalMonths = houseRentalMonths;
	}
	@Column
	public Boolean getInternetConnectionFlag() {
		return internetConnectionFlag;
	}
	public void setInternetConnectionFlag(Boolean internetConnectionFlag) {
		this.internetConnectionFlag = internetConnectionFlag;
	}
	@Column
	public String getJobLevel() {
		return jobLevel;
	}
	public void setJobLevel(String jobLevel) {
		this.jobLevel = jobLevel;
	}
	@Column
	public Boolean getLaundryCostFlag() {
		return laundryCostFlag;
	}
	public void setLaundryCostFlag(Boolean laundryCostFlag) {
		this.laundryCostFlag = laundryCostFlag;
	}
	@Column
	public Boolean getRentalAmountFlag() {
		return rentalAmountFlag;
	}
	public void setRentalAmountFlag(Boolean rentalAmountFlag) {
		this.rentalAmountFlag = rentalAmountFlag;
	}
	@Column
	public String getRentAmount() {
		return rentAmount;
	}
	public void setRentAmount(String rentAmount) {
		this.rentAmount = rentAmount;
	}
	@Column
	public String getRequestedArea() {
		return requestedArea;
	}
	public void setRequestedArea(String requestedArea) {
		this.requestedArea = requestedArea;
	}
	@Column
	public Boolean getRequestedlocationFlag() {
		return requestedlocationFlag;
	}
	public void setRequestedlocationFlag(Boolean requestedlocationFlag) {
		this.requestedlocationFlag = requestedlocationFlag;
	}
	@Column
	public Boolean getTaxAssistance() {
		return taxAssistance;
	}
	public void setTaxAssistance(Boolean taxAssistance) {
		this.taxAssistance = taxAssistance;
	}
	@Column
	public String getTaxAssistanceYear() {
		return taxAssistanceYear;
	}
	public void setTaxAssistanceYear(String taxAssistanceYear) {
		this.taxAssistanceYear = taxAssistanceYear;
	}
	@Column
	public Boolean getTaxRulingFlag() {
		return taxRulingFlag;
	}
	public void setTaxRulingFlag(Boolean taxRulingFlag) {
		this.taxRulingFlag = taxRulingFlag;
	}
	@Column
	public String getTelephoneCallsCost() {
		return telephoneCallsCost;
	}
	public void setTelephoneCallsCost(String telephoneCallsCost) {
		this.telephoneCallsCost = telephoneCallsCost;
	}
	@Column
	public Boolean getTelephoneConnection() {
		return telephoneConnection;
	}
	public void setTelephoneConnection(Boolean telephoneConnection) {
		this.telephoneConnection = telephoneConnection;
	}
	@Column
	public Boolean getTemporaryCarFlag() {
		return temporaryCarFlag;
	}
	public void setTemporaryCarFlag(Boolean temporaryCarFlag) {
		this.temporaryCarFlag = temporaryCarFlag;
	}
	@Column
	public String getTemporaryCarMonths() {
		return temporaryCarMonths;
	}
	public void setTemporaryCarMonths(String temporaryCarMonths) {
		this.temporaryCarMonths = temporaryCarMonths;
	}
	@Column
	public Boolean getUtilitycostFlag() {
		return utilitycostFlag;
	}
	public void setUtilitycostFlag(Boolean utilitycostFlag) {
		this.utilitycostFlag = utilitycostFlag;
	}
	@Column
	public Boolean getFurnished() {
		return furnished;
	}
	public void setFurnished(Boolean furnished) {
		this.furnished = furnished;
	}
	@Column
	public Boolean getUnFurnished() {
		return unFurnished;
	}
	public void setUnFurnished(Boolean unFurnished) {
		this.unFurnished = unFurnished;
	}
	@Column
	public Boolean getEntryVisa() {
		return entryVisa;
	}
	public void setEntryVisa(Boolean entryVisa) {
		this.entryVisa = entryVisa;
	}
	@Column
	public Boolean getLongTermStayVisa() {
		return longTermStayVisa;
	}
	public void setLongTermStayVisa(Boolean longTermStayVisa) {
		this.longTermStayVisa = longTermStayVisa;
	}
	@Column
	public Boolean getWorkPermit() {
		return workPermit;
	}
	public void setWorkPermit(Boolean workPermit) {
		this.workPermit = workPermit;
	}
	@Column
	public Boolean getIsIta2m3() {
		return isIta2m3;
	}
	public void setIsIta2m3(Boolean isIta2m3) {
		this.isIta2m3 = isIta2m3;
	}
	
	

}
