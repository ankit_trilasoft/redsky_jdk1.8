ALTER TABLE `redsky`.`accountline` ADD COLUMN `t20Process` BIT(1) DEFAULT false ;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `CAR_notificationDate` DATETIME AFTER `SET_vendorCodeEXSO`,
ADD COLUMN `CAR_pickUpDate` DATETIME AFTER `CAR_notificationDate`,
ADD COLUMN `CAR_registrationDate` DATETIME AFTER `CAR_pickUpDate`,
ADD COLUMN `CAR_deliveryDate` DATETIME AFTER `CAR_registrationDate`,
ADD COLUMN `COL_estimatedAllowanceEuro` VARCHAR(10) AFTER `CAR_deliveryDate`,
ADD COLUMN `VIS_applicationDateWork` DATETIME AFTER `COL_estimatedAllowanceEuro`,
ADD COLUMN `VIS_permitStartDateWork` DATETIME AFTER `VIS_applicationDateWork`,
ADD COLUMN `VIS_applicationDateResidence` DATETIME AFTER `VIS_permitStartDateWork`,
ADD COLUMN `VIS_permitStartDateResidence` DATETIME AFTER `VIS_applicationDateResidence`;
 
 
ALTER TABLE `redsky`.`dspdetails` MODIFY COLUMN `id` BIGINT(20) NOT NULL,
ADD COLUMN `CAR_comment` MEDIUMTEXT AFTER `VIS_permitStartDateResidence`,
ADD COLUMN `COL_comment` MEDIUMTEXT AFTER `CAR_comment`,
ADD COLUMN `TRG_comment` MEDIUMTEXT AFTER `COL_comment`,
ADD COLUMN `HOM_comment` MEDIUMTEXT AFTER `TRG_comment`,
ADD COLUMN `LAN_comment` MEDIUMTEXT AFTER `HOM_comment`,
ADD COLUMN `MMG_comment` MEDIUMTEXT AFTER `LAN_comment`,
ADD COLUMN `PRV_comment` MEDIUMTEXT AFTER `MMG_comment`,
ADD COLUMN `AIO_comment` MEDIUMTEXT AFTER `PRV_comment`,
ADD COLUMN `EXP_comment` MEDIUMTEXT AFTER `AIO_comment`,
ADD COLUMN `SCH_comment` MEDIUMTEXT AFTER `EXP_comment`,
ADD COLUMN `TAX_comment` MEDIUMTEXT AFTER `SCH_comment`,
ADD COLUMN `TEN_comment` MEDIUMTEXT AFTER `TAX_comment`,
ADD COLUMN `VIS_comment` MEDIUMTEXT AFTER `TEN_comment`;

update networkdatafields set transactionType ='Update'   where fieldName in ('serviceAir','serviceSurface','serviceAuto','serviceStorage','serviceDomestic','visaRequired','orderComment') and modelName ='CustomerFile';

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `unitVariable` VARCHAR(3) default 'No' AFTER `validation` ;

ALTER TABLE accountline ADD COLUMN activateAccPortal BIT(1) DEFAULT TRUE;

ALTER TABLE `redsky`.`accountline` MODIFY COLUMN `invoiceNumber` VARCHAR(25);

update networkdatafields set type='CMM/DMM' where modelName ='TrackingStatus' and fieldName in ('packDayConfirmCall','loadFollowUpCall','deliveryFollowCall')

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type) values('CustomerFile','serviceDomestic' ,'Update','amishra',now(),'amishra',now(),false,'','','');

Insert into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('STAR', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('STAR', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('SSCW', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),('SSCW', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('VOER', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('VOER', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('VORU', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('VORU', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('VOCZ', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('VOCZ', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('ADAM', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('ADAM', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('UTSI', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('UTSI', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGHK', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('UGHK', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('BOUR', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('BOUR', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('JOHN', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('JOHN', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGCN', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('UGCN', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('VOAO', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('VOAO', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('CWMS', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('CWMS', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGMY', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('UGMY', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGSG', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('UGSG', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('BONG', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('BONG', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGWW', '1', 'Y', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen'), ('UGWW', '-1', 'N', 10, 'OAEVALUATION',now(),'naveen',now(),'naveen');

Insert into refmaster (corpid, code, description,fieldlength,parameter,createdon,createdby,updatedon,updatedby) values ('STAR', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('STAR', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('SSCW', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('SSCW', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('VOER', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('VOER', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('VORU', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('VORU', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('VOCZ', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('VOCZ', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('ADAM', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('ADAM', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('UTSI', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('UTSI', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGHK', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('UGHK', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('BOUR', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),('BOUR', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('JOHN', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('JOHN', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGCN', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('UGCN', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('VOAO', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('VOAO', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('CWMS', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('CWMS', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGMY', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('UGMY', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGSG', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('UGSG', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('BONG', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('BONG', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'),  ('UGWW', '1', 'Y', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen'), ('UGWW', '-1', 'N', 10, 'DAEVALUATION',now(),'naveen',now(),'naveen');

ALTER TABLE `redsky`.`newsupdate` ADD COLUMN `ticketNo` VARCHAR(6) DEFAULT NULL AFTER `fileName`;

Insert into parametercontrol (corpid,parameter,comments,fieldlength,active,createdon,createdby,updatedon,updatedby,tsftflag,hybridflag,customtype) values( 'TSFT', 'OAEVALUATION', 'Survey Response Parameter', 10, true, now(), 'naveen', now(), 'naveen', 1, 1, '');

Insert into parametercontrol (corpid,parameter,comments,fieldlength,active,createdon,createdby,updatedon,updatedby,tsftflag,hybridflag,customtype) values( 'TSFT', 'DAEVALUATION', 'Survey Response Parameter', 10, true, now(), 'naveen', now(), 'naveen', 1, 1, ''); 

ALTER TABLE `redsky`.`accountline` ADD INDEX `Index_NetworkSyncID`(`networkSynchedId`);
