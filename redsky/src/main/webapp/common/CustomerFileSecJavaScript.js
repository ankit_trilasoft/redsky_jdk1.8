<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>

<script type="text/JavaScript">
	setTimeout("setEntitleValue()",2000);
	<c:if test="${not empty customerFile.id}">
		setTimeout("getFamilysize()",500);
	</c:if>
</script>
<script type="text/JavaScript">
  window.onload = function() {
	  <c:if test="${not empty customerFile.id}">
	  	getFamilysize();
	  </c:if>
	  setEntitleValue();
	  if (screen.width >= 1440 ){
			 var elem = document.getElementById("para1");				
			 elem.style.overflow = 'none';
			 elem.style.width = '100%';
	  }
	  if(screen.width == 1280){
    	  var elem = document.getElementById("para1");
    	  elem.style.overflow = 'auto';
    	  elem.style.width = '1160px';
      }
      if(screen.width == 1152){
    	  var elem = document.getElementById("para1");
    	  elem.style.overflow = 'auto';
    	  elem.style.width = '1035px';
      }
      if(screen.width == 1024){
    	  var elem = document.getElementById("para1");
    	  elem.style.overflow = 'auto';
    	  elem.style.width = '910px';
      }  
}
</script>
<script type="text/javascript">
function buttondisabled(){
	<c:if test="${not hasDataSecuritySet}">
	var billToCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value
	if(billToCode!=''){
		document.forms['customerFileForm'].elements['P'].disabled=true;
	}else{
	    document.forms['customerFileForm'].elements['P'].disabled=false;
	}
	 </c:if>
}
function resetbuttondisabled(){
	<c:if test="${not hasDataSecuritySet}">
	document.forms['customerFileForm'].elements['P'].disabled=false;
	</c:if>
}
function checkAccountCodeForAssignmentTypeOnlOad(){
	var accCode= document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
	if(accCode!=''){
		var results='${agentParentList2}';
		if(results != ''){
			findAssignmentFromParentAgentOnLoad(results);
        }
	}else{
		findAssignmentFromParentAgentOnLoad("false");
	}
}
function checkAccountCodeForAssignmentType(){
	var accCode= document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
	if(accCode!=''){
		 var url="findParentByAccCodeAjax.html?ajax=1&decorator=simple&popup=true&accCode="+encodeURI(accCode);
		   httpaccCode.open("GET", url, true); 
		   httpaccCode.onreadystatechange = handleHttpaccCode; 
		   httpaccCode.send(null);
	}else{
		 findAssignmentFromParentAgent("false");
	}
}
function handleHttpaccCode(){
	 if (httpaccCode.readyState == 4){	 
	         var results = httpaccCode.responseText
	         results = results.trim();
	         if(results != ''){
	        	 findAssignmentFromParentAgent(results);
	         }
	 }
}
function findAssignmentFromParentAgentOnLoad (target){
	var fieldValue='${customerFile.assignmentType}';
	if((target == 'true') ){
		var j=1;
	    var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
	    var len=findActiveLength('ASML',fieldValue,'');
			targetElement.length = parseInt(len)+1;
	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
			document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
			<c:forEach var="assignment1" items="${assignmentTypesASML_isactive}" varStatus="loopStatus">
	       	var entKey = "<c:out value="${assignment1.key}"/>";
	      	var entvalue = "<c:out value="${assignment1.value}"/>";
	      	if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
		     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text = entvalue;
				document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey.split('~')[0] ;					
				if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
					document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
				}
				j++;
	      	}
			</c:forEach>			
		}else{
			var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value; 
  		    var results ='${assignmentList}';
		    results = results.trim();
		    results=results.replace("[","");
		    results=results.replace("]","");
		    results=results.trim();
			   if(bCode!='' && results.length >= 1 && results  !='' && results  !=',') {
			         	var res = results.split(",");          	
				         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
								targetElement.length = res.length+1;
								document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = '';
								document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = '';
								for(i=1;i<=res.length+1;i++){
									if(res[i-1]!=undefined ||res[i-1]!=null){
									document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].text = res[i-1].trim();
									document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].value = res[i-1].trim();
									if(document.getElementById("assignmentType").options[i].value=='${customerFile.assignmentType}'){
										document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
									}
									}
								} 
		 		}else {
				     <c:if test="${parentAgentForOrderinitiation!='true'}">
			  	     var j=1;
				     var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
				     var len=findActiveLength('NOTASML',fieldValue,'');
				     targetElement.length = parseInt(len)+1;
				     document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
		  		     document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
				     <c:forEach var="assignment1" items="${assignmentTypes_isactive}" varStatus="loopStatus">
				       	var entKey = "<c:out value="${assignment1.key}"/>";
			 	      	var entvalue = "<c:out value="${assignment1.value}"/>";
			 	      	if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
				 	    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
					    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value = entKey.split('~')[0] ;					
					    if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
					    }
					    j++;
				 	}
			 	      </c:forEach>
			      </c:if>
			      <c:if test="${parentAgentForOrderinitiation=='true'}">
			 	      var j=1;
				      var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
				      var len=findActiveLength('ASML',fieldValue,'');
				      targetElement.length = parseInt(len)+1;
				      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
				      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
				      <c:forEach var="assignment1" items="${assignmentTypesASML_isactive}" varStatus="loopStatus">
				 	  var entKey = "<c:out value="${assignment1.key}"/>";
				 	  var entvalue = "<c:out value="${assignment1.value}"/>";
				 	  if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
					      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
					      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey.split('~')[0] ;					
					      if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
						 document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
					      }
			  		      j++;
				 	  }
			 	      </c:forEach>
			      </c:if>
		 		}//end
	       
		}
    document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
	 }
function findAssignmentFromParentAgent (target){
	var fieldValue='${customerFile.assignmentType}';
	if((target == 'true') ){
		var j=1;
	    var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
	    var len=findActiveLength('ASML',fieldValue,'');
			targetElement.length = parseInt(len)+1;
	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
			document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
			<c:forEach var="assignment1" items="${assignmentTypesASML_isactive}" varStatus="loopStatus">
	       	var entKey = "<c:out value="${assignment1.key}"/>";
	      	var entvalue = "<c:out value="${assignment1.value}"/>";
	      	if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
		     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text = entvalue;
				document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey.split('~')[0] ;					
				if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
					document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
				}
				j++;
	      	}
			</c:forEach>			
		}else{
	   var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value; 
	   if(bCode!='') { 
		   var url="findAssignment.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		   http51111.open("GET", url, true); 
		   http51111.onreadystatechange = handleHttp90; 
		   http51111.send(null); 
	   }else{
		     <c:if test="${parentAgentForOrderinitiation!='true'}">
	  	     var j=1;
		     var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
		     var len=findActiveLength('NOTASML',fieldValue,'');
		     targetElement.length = parseInt(len)+1;
		     document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
  		     document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
		     <c:forEach var="assignment1" items="${assignmentTypes_isactive}" varStatus="loopStatus">
		       	var entKey = "<c:out value="${assignment1.key}"/>";
	 	      	var entvalue = "<c:out value="${assignment1.value}"/>";
	 	      	if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
		 	    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
			    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value = entKey.split('~')[0] ;					
			    if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
				document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
			    }
			    j++;
		 	}
	 	      </c:forEach>
		      document.forms['customerFileForm'].elements['customerFile.assignmentType'].value=' ';
	      </c:if>
	      <c:if test="${parentAgentForOrderinitiation=='true'}">
	 	      var j=1;
		      var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
		      var len=findActiveLength('ASML',fieldValue,'');
		      targetElement.length = parseInt(len)+1;
		      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
		      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
		      <c:forEach var="assignment1" items="${assignmentTypesASML_isactive}" varStatus="loopStatus">
		 	  var entKey = "<c:out value="${assignment1.key}"/>";
		 	  var entvalue = "<c:out value="${assignment1.value}"/>";
		 	  if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
			      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
			      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey.split('~')[0] ;					
			      if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
				 document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
			      }
	  		      j++;
		 	  }
	 	      </c:forEach>
		      document.forms['customerFileForm'].elements['customerFile.assignmentType'].value=' ';
	      </c:if>
	     
	    }
		}
	 }
function handleHttp90(){
 if (http51111.readyState == 4){	 
         var results = http51111.responseText
         results = results.trim();
     	var fieldValue='${customerFile.assignmentType}';
         if(results.length >= 1){
         	var res = results.split("@");          	
	         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
					targetElement.length = res.length;
					for(i=0;i<res.length;i++){
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].text = res[i];
						document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].value = res[i];
						if(document.getElementById("assignmentType").options[i].value=='${customerFile.assignmentType}'){
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
						} 
					} 
	 		}else {
			     <c:if test="${parentAgentForOrderinitiation!='true'}">
		  	     var j=1;
			     var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
			     var len=findActiveLength('NOTASML',fieldValue,'');
			     targetElement.length = parseInt(len)+1;
			     document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
	  		     document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
			     <c:forEach var="assignment1" items="${assignmentTypes_isactive}" varStatus="loopStatus">
			       	var entKey = "<c:out value="${assignment1.key}"/>";
		 	      	var entvalue = "<c:out value="${assignment1.value}"/>";
		 	      	if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
			 	    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
				    document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value = entKey.split('~')[0] ;					
				    if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
					document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
				    }
				    j++;
			 	}
		 	      </c:forEach>
			      document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
		      </c:if>
		      <c:if test="${parentAgentForOrderinitiation=='true'}">
		 	      var j=1;
			      var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
			      var len=findActiveLength('ASML',fieldValue,'');
			      targetElement.length = parseInt(len)+1;
			      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
			      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
			      <c:forEach var="assignment1" items="${assignmentTypesASML_isactive}" varStatus="loopStatus">
			 	  var entKey = "<c:out value="${assignment1.key}"/>";
			 	  var entvalue = "<c:out value="${assignment1.value}"/>";
			 	  if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
				      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
				      document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey.split('~')[0] ;					
				      if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
					 document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
				      }
		  		      j++;
			 	  }
		 	      </c:forEach>
			      document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
		      </c:if>
	 		}//end
 		}
	}

function findAssignmentFromAccountCode(){
	var accCode= document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
	 if(accCode!=''){
		 var url="findParentByAccCodeAjax.html?ajax=1&decorator=simple&popup=true&accCode="+encodeURI(accCode);
		   httpaccCodeNew.open("GET", url, true); 
		   httpaccCodeNew.onreadystatechange = handleHttpaccCodeNew; 
		   httpaccCodeNew.send(null);
	 }
	 function handleHttpaccCodeNew(){
		 if (httpaccCodeNew.readyState == 4){	 
		         var results = httpaccCodeNew.responseText
		         results = results.trim();
		         if(results == 'T56292'){
		        	var j=1
		        	var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
		 	    	var len='${fn1:length(assignmentTypesASML)}';
		 			targetElement.length = parseInt(len)+1;
		 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
		 			document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
		 			<c:forEach var="assignment1" items="${assignmentTypesASML}" varStatus="loopStatus">
		 	       	var entKey = "<c:out value="${assignment1.key}"/>";
		 	      	var entvalue = "<c:out value="${assignment1.value}"/>";
		 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text = entvalue;
		 			document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey ;
		         	
		 			if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
		 				document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
		 			}
		 			j++;
		 			</c:forEach>
		         }
		 }
	 }
	if(document.forms['customerFileForm'].elements['customerFile.accountCode'].value == 'T56292' ){
	var j=1;
    var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
    var len='${fn1:length(assignmentTypesASML)}';
		targetElement.length = parseInt(len)+1;
     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
		document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
		<c:forEach var="assignment1" items="${assignmentTypesASML}" varStatus="loopStatus">
       	var entKey = "<c:out value="${assignment1.key}"/>";
      	var entvalue = "<c:out value="${assignment1.value}"/>";
     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text = entvalue;
		document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value =entKey ;					
		if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
			document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
		}
		j++;
		</c:forEach>
	}
}
var httpaccCode = getHTTPObject();
var httpaccCodeNew = getHTTPObject();
var http51 = getHTTPObject();
function getHTTPObject()
{
 var xmlhttp;
 if(window.XMLHttpRequest)
 {
     xmlhttp = new XMLHttpRequest();
 }
 else if (window.ActiveXObject)
 {
     xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
     if (!xmlhttp)
     {
         xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
     }
 }
 return xmlhttp;
}
</script>

<script type="text/javascript">
//alert(document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value);

</script>
<script type="text/javascript">
function getEntitleRest1(){
	var t=setTimeout("getEntitleRest()",1000);
	<c:if test="${not empty customerFile.id}">
		var fs=setTimeout("getFamilysize()",100);
	</c:if>
//	document.getElementById('resetEnttileVal').click();
}
function getEntitleRest(){
//	window.location.reload();
	<c:forEach var="entitlementMap" items="${entitlementMap}">
     var entKey = "<c:out value="${entitlementMap.key}"/>";
     if(document.getElementById(entKey) != null ){
     		document.getElementById(entKey).checked=false;
     }
	</c:forEach>
	document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = entReset;
		for ( var i = 0; i < listEnt.length; i++ ) {
			if(document.getElementById(listEnt[i]) != null ){
			 	document.getElementById(listEnt[i]).checked=true;  // listEnt is a global array need to be declare
			}
		}
}

var listEnt = new Array();
var entReset;
function setEntitleValue(){
	var entitle = document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value;
	entReset = entitle;
	if(entitle.length > 0){
		entitle = entitle.split('_?@').join(' '); 
		entitle = entitle.split('#?@');
		 for ( var i = 0; i < entitle.length; i++ ) {
			 if(document.getElementById(entitle[i]) != null){
			 	document.getElementById(entitle[i]).checked=true;
			 	listEnt.push(entitle[i]);
			 }
		 }
	}
}

function getValueEnt(str) {
	 str=str.split(' ').join('_?@'); 
	 var partnerEnti = document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value;
	 if(partnerEnti.length == 0){
		 document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = str+'#?@'+partnerEnti;
	 }else {
		 var currentTagTokens = partnerEnti.split( "#?@" );
		 var existingTags='';
		 var isExist = false;
		  for ( var i = 0; i < currentTagTokens.length; i++ ) {
		    if(currentTagTokens[i] != str){
			    if(currentTagTokens[i].length >0){
		    		existingTags = currentTagTokens[i]+'#?@'+existingTags;
			    }
		    }else{
		    	isExist = true;
		    }
		  }
		  if(isExist == false){
		  	document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = existingTags+'#?@'+str;
		  }else{
			  document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = existingTags;
		  }
	 }
}
function getValueEnt1() {}
function getValueEnt3() {
	document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value='';
	var parentEnt1='';
	<c:forEach var="entitlementMap" items="${entitlementMap}">
     var entKey = "chk_<c:out value="${entitlementMap.key}"/>";
     if(document.getElementById(entKey) != null && document.getElementById(entKey).checked == true){
      	   entKey = entKey.split(' ').join('_?@'); 
  			parentEnt1 = entKey+'#?@'+parentEnt1;
     }
	</c:forEach>
	 document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value=parentEnt1;
}

function getFamilysize(str){
	var fSize = document.forms['customerFileForm'].elements['customerFile.familysize'].value;
//	alert(fSize);
	var no='<%= count %>';
	if(no == 0 && str != 'FS'){
		document.forms['customerFileForm'].elements['customerFile.familysize'].readOnly= false;
		if(fSize == ''){
			document.forms['customerFileForm'].elements['customerFile.familysize'].value = no;
		}
		return;
	}
	if(no == 0 && str == 'FS'){
		if(parseInt(fSize) != fSize){
			 alert('Family size is not valid');
			 document.forms['customerFileForm'].elements['customerFile.familysize'].value = '';
			 return false;
		 }else {
			 for(var i = 0;i < fSize.length;i++){
				 if(fSize.charAt(i) == '.' || fSize.charAt(i) == ' '){
					 alert('Family size is not valid');
					 document.forms['customerFileForm'].elements['customerFile.familysize'].value = '';
					 return false;
				 }
			 }
		 }
		document.forms['customerFileForm'].elements['customerFile.familysize'].value = fSize;
		document.forms['customerFileForm'].elements['customerFile.familysize'].readOnly= false;
	}else {
		document.forms['customerFileForm'].elements['customerFile.familysize'].readOnly= true;
		document.forms['customerFileForm'].elements['customerFile.familysize'].value = no;
	}
}
function fillAccountCodeByBillToCode(){
    var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var url="fillAccountCodeByBillToCode.html?ajax=1&decorator=simple&popup=true&bCode=" + encodeURI(bCode);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseFillAcc;
    http4.send(null);
}
<c:set var="accountHollValidation" value="N" />
<configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
<c:set var="accountHollValidation" value="Y" />
</configByCorp:fieldVisibility>
function handleHttpResponseFillAcc(){
	 if (http4.readyState == 4){
           var results = http4.responseText
           results = results.trim();
           results=results.replace("[","");
           results=results.replace("]","");
           				if(results!=''){
           					if((results!='')&&(results!='NOTAC')&&(results!='AC')){
           						var	res = results.split("~");
    							document.forms['customerFileForm'].elements['customerFile.accountCode'].value=res[0];
    							document.forms['customerFileForm'].elements['customerFile.accountName'].value=res[1];
           					}else{
           						if((results!='')&&(results=='AC')){
    								document.forms['customerFileForm'].elements['customerFile.accountCode'].value = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    						        document.forms['customerFileForm'].elements['customerFile.accountName'].value = document.forms['customerFileForm'].elements['customerFile.billToName'].value;
           						}else{
           							<c:if test="${accountHollValidation=='N'}">
           							document.forms['customerFileForm'].elements['customerFile.accountCode'].value = '';
    						        document.forms['customerFileForm'].elements['customerFile.accountName'].value = '';
           							</c:if>
           							}
           					}           					
           					fillAccountFlag();
           					checkAccountCodeForAssignmentType();
           					checkPriceContract();
           				}
	 			   }
}

function fillAccountCodeByContract(){ 
    var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var url="fillAccountCodeByBillToCode.html?ajax=1&decorator=simple&popup=true&bCode=" + encodeURI(bCode);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseFillContract;
    http4.send(null);
}
 
function handleHttpResponseFillContract(){ 
	 if (http4.readyState == 4){
           var results = http4.responseText
           results = results.trim();
           results=results.replace("[","");
           results=results.replace("]",""); 
           				if(results!=''){
           					if((results!='')&&(results!='NOTAC')&&(results!='AC')){
           						var	res = results.split("~");
    							document.forms['customerFileForm'].elements['customerFile.accountCode'].value=res[0];
    							document.forms['customerFileForm'].elements['customerFile.accountName'].value=res[1];
           					}else{
           						if((results!='')&&(results=='AC')){
    								document.forms['customerFileForm'].elements['customerFile.accountCode'].value = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    						        document.forms['customerFileForm'].elements['customerFile.accountName'].value = document.forms['customerFileForm'].elements['customerFile.billToName'].value;
           						}else{
           							<c:if test="${accountHollValidation=='N'}">
           							document.forms['customerFileForm'].elements['customerFile.accountCode'].value = '';
    						        document.forms['customerFileForm'].elements['customerFile.accountName'].value = '';
           							</c:if>
           							}
           					} 
           				}
	 			   }
}


function fillBillToAuthority(){
	var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	if(bCode!='') {
	    var url="fillBillToAuthority.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http51.open("GET", url, true); 
	   	http51.onreadystatechange = handleHttp901; 
	   	http51.send(null);	     
     }
 }
function handleHttp901(){
    if (http51.readyState == 4){    	
    	var results = http51.responseText
    	results = results.trim();
    	if(results == undefined)
    		{
    		results='';
    		}
    	if(document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value==''){
		 	document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = results;
    	}
       }
	}

var http51 = getHTTPObject();
var http51111 = getHTTPObject();
function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)
    {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}
function billToCodeFieldValueCopy(){
	var bCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	if(bCode==''){
		document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value="";
	}
}
function findDefaultSettingFromPartnerOnload(){
	var cfContract = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	var cfSource = document.forms['customerFileForm'].elements['customerFile.source'].value;
	if(document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value=='undefined'){
		document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value='';
	}
	var cfBillToAuth = document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value;	
	var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	if((cfContract=='' || cfSource=='' || cfBillToAuth=='') && bCode!='') {
	     var results='${agentParentList1}';	     
	     if(results!=''){
		    	var	res = results.split("~");
			    	if(document.forms['customerFileForm'].elements['customerFile.contract'].value==''){
			    		if(res[0]!='NA'){
			    			document.forms['customerFileForm'].elements['customerFile.contract'].value=res[0];
			    		}
			    	}
			    	if(document.forms['customerFileForm'].elements['customerFile.source'].value==''){
			    		if(res[3]!='NA'){
			    			document.forms['customerFileForm'].elements['customerFile.source'].value = res[3];
			    		}
			    	}
			    	if(document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value==''){
			    		if(res[9]!='NA'){
			    			if(res[9]==undefined || res[9]=="undefined"){
			    				res[9]="";
			    			}
			    			document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = res[9];
			    		}
			    	}
	    		}
     }
}

function findDefaultSettingFromPartner(){
	var cfContract = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	var cfSource = document.forms['customerFileForm'].elements['customerFile.source'].value;
	var cfBillToAuth = document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value;
	var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	if((cfContract=='' || cfSource=='' || cfBillToAuth=='') && bCode!='') {
	    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http521.open("GET", url, true); 
	   	http521.onreadystatechange = handleHttp921; 
	   	http521.send(null);	     
     }
}
function handleHttp921(){
    if (http521.readyState == 4){    	
    	var results = http521.responseText
    	results = results.trim();
    	if(results!=''){
	    	var	res = results.split("~");
		    	if(document.forms['customerFileForm'].elements['customerFile.contract'].value==''){
		    		if(res[0]!='NA'){
		    			document.forms['customerFileForm'].elements['customerFile.contract'].value=res[0]; 
		    		}
		    	}
		    	if(document.forms['customerFileForm'].elements['customerFile.source'].value==''){
		    		if(res[3]!='NA'){
		    			document.forms['customerFileForm'].elements['customerFile.source'].value = res[3];
		    		}
		    	}
		    	if(document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value==''){
		    		if(res[9]!='NA'){
		    			if(res[9]==undefined || res[9]=="undefined"){
		    				res[9]="";
		    			}
		    			
		    			document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = res[9];
		    		}
		    	}
    		}
       	}
	}
var http521 = getHTTPObject();

</script>

<script type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
		var url = 'deleteFamilyDetails.html?id=${customerFile.id}&familyId='+targetElement;
		location.href = url;
	 }else{
		return false;
	}
}

var isLoadFamily = '';
var httpFamilyDetails = getHTTPObject();
var httpDeleteFamilyDetails = getHTTPObject();
function loadFamilyDetails(str){
	if('${customerFile.id}' == '' || '${customerFile.id}' == null){
		animatedcollapse.toggle('family');
		alert('Please save Customer File first before access Family Details.');
		return false;
	}else if(isLoadFamily ==''){
		isLoadFamily = 'YES';
		showOrHide(1);
		getFamilyDetails(str);
	}
}

function getFamilyDetails(str){
	showOrHide(1);
	var url="findFamilyDetailsAjax.html?ajax=1&decorator=simple&popup=true&id=${customerFile.id}";
	httpFamilyDetails.open("POST", url, true);
	httpFamilyDetails.onreadystatechange = function(){handleHttpResponseForFamilyDetails(str)};
	httpFamilyDetails.send(null);
}
function handleHttpResponseForFamilyDetails(str){
	if (httpFamilyDetails.readyState == 4){
		showOrHide(0);
        var results= httpFamilyDetails.responseText; 
 	    var familyDetailsDiv = document.getElementById("familyDetailsAjax");
 	   familyDetailsDiv.innerHTML = results;
 	   
 	  var element = document.forms['customerFileForm'].elements['customerFile.familysize'];
 	  element.readOnly= true;
 	  element.setAttribute("class", 'input-textUpper'); 
 	  
 	  var rowCount = document.getElementById('dsFamilyDetailsList').tBodies[0].rows.length;
 	  if(rowCount == 1){
 		 for (var i=0;i<rowCount;i++){
 	 		if(document.getElementById('dsFamilyDetailsList').tBodies[0].rows[i].id == 'empty'){
 	 			element.readOnly= false;
	 	 		element.setAttribute("class", 'input-text');
	 	 		var fsize = parseInt(0+'${customerFile.familysize}');
 	 			if(fsize > 0 && str == 'LOAD'){
 	 				element.value = '${customerFile.familysize}';
 	 				return;
 	 			}else{
 	 				element.value = 0;
 	 			}
 	 		}else{
 	 			element.value = rowCount;
 	 		}
 	 	 }
 	  }else{
 		 element.value = rowCount;
 	  }
 	}
}

function deleteFamilyDetails(id){
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
		showOrHide(1);
		var url="deleteFamilyDetailsAjax.html?ajax=1&decorator=simple&popup=true&id="+id;
		httpDeleteFamilyDetails.open("POST", url, true);
		httpDeleteFamilyDetails.onreadystatechange = handleHttpResponseForDeleteFamilyDetails;
		httpDeleteFamilyDetails.send(null);
	}
}
function handleHttpResponseForDeleteFamilyDetails(){
	if (httpDeleteFamilyDetails.readyState == 4){
        var results= httpDeleteFamilyDetails.responseText; 
        getFamilyDetails();
 	}
}

function resetFamilyDetails(){
	isLoadFamily = '';
	animatedcollapse.toggle('family');
}

</script>

<script language="JavaScript">
function CalulateTotalNumber()
{
	//alert("H");
	var TotalNumber;
	var N1=document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsFreight'].value;
	var N2=document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsOrigin'].value;
	var N3=document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsDestination'].value;
	var N4=document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsFreight'].value;
	var N5=document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsOrigin'].value;
	var N6=document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsDestination'].value;
	
	TotalNumber=Number(N1)+Number(N2)+Number(N3)+Number(N4)+Number(N5)+Number(N6);
	document.forms['customerFileForm'].elements['imfEntitlements.totalFreight'].value=TotalNumber.toFixed(2);
  
}

function CalulateTotalAirEntitlements()
{
	var TotalNumber=0*1;
	var N1=(document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsFreight'].value);
	var N2=(document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsOrigin'].value);
	var N3=(document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsDestination'].value);
	TotalNumber=(N1*1+N2*1+N3*1);
	
	document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsTotal'].value=TotalNumber.toFixed(2);
  	
}

function CalulateTotalSurfaceEntitlements()
{
	//alert("H");
	var TotalNumber=0*1;
	var N4=document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsFreight'].value;
	var N5=document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsOrigin'].value;
	var N6=document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsDestination'].value;
	
	TotalNumber=N4*1+N5*1+N6*1;
	document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsTotal'].value=TotalNumber.toFixed(2);
  
}

function CalulateTotalOfficeEntitlements()
{
	//alert("H");
	var TotalNumber=0*1;
	var N1=document.forms['customerFileForm'].elements['imfEntitlements.officeEntitlementsFreight'].value;
	var N2=document.forms['customerFileForm'].elements['imfEntitlements.officeEntitlementsOrigin'].value;
	var N3=document.forms['customerFileForm'].elements['imfEntitlements.officeEntitlementsDestination'].value;
	TotalNumber=N1*1+N2*1+N3*1;
	document.forms['customerFileForm'].elements['imfEntitlements.officeEntitlementsTotal'].value=TotalNumber.toFixed(2);
  
}

function CalulateTotalAutoEntitlements()
{
	//alert("H");
	var TotalNumber=0*1;
	var N1=document.forms['customerFileForm'].elements['imfEntitlements.autoEntitlementsFreight'].value;
	var N2=document.forms['customerFileForm'].elements['imfEntitlements.autoEntitlementsOrigin'].value;
	var N3=document.forms['customerFileForm'].elements['imfEntitlements.autoEntitlementsDestination'].value;
	TotalNumber=N1*1+N2*1+N3*1;
	document.forms['customerFileForm'].elements['imfEntitlements.autoEntitlementsTotal'].value=TotalNumber.toFixed(2);
  
}

function isFloat(targetElement)
{   var i;
var s = targetElement.value;
for (i = 0; i < s.length; i++)
{   
    var c = s.charAt(i);
    if (((c < "0") || (c > "9"))) {
    if (c == ".") {
    
    }else{
    alert("Only numbers are allowed here");
    //alert(targetElement.id);
    document.getElementById(targetElement.id).value='';
    document.getElementById(targetElement.id).select();
    return false;
    }
}
}
return true;
}



var r={
'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\=']/g,
'quotes':/['\''&'\"']/g,
'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
targetElement.value = targetElement.value.replace(r[w],'');
}

function chkSelect()
{

	if (checkFloat('customerFileForm','imfEntitlements.airEntitlementsWeight','Invalid data in Air Entitlements Weight') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsWeight'].focus();
          return false
       }
    if (checkFloat('customerFileForm','imfEntitlements.airEntitlementsFreight','Invalid data in Air Entitlements Freight') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsFreight'].focus();
          return false
       }
    if (checkFloat('customerFileForm','imfEntitlements.airEntitlementsOrigin','Invalid data in Air Entitlements Origin') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsOrigin'].focus();
          return false
       }
    if (checkFloat('customerFileForm','imfEntitlements.airEntitlementsDestination','Invalid data in Air Entitlements Destination') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.airEntitlementsDestination'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.surfaceEntitlementsWeight','Invalid data in Surface Entitlements Weight') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsWeight'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.surfaceEntitlementsFreight','Invalid data in Surface Entitlements Freight') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsFreight'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.surfaceEntitlementsOrigin','Invalid data in Surface Entitlements Origin') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsOrigin'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.surfaceEntitlementsDestination','Invalid data in Surface Entitlements Destination') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.surfaceEntitlementsDestination'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.autoEntitlementsWeight','Invalid data in Auto Entitlements Weight') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.autoEntitlementsWeight'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.autoEntitlementsFreight','Invalid data in Auto Entitlements Freight') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.autoEntitlementsFreight'].focus();
          return false
       }
       
       if (checkFloat('customerFileForm','imfEntitlements.autoEntitlementsOrigin','Invalid data in Auto Entitlements Origin') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.autoEntitlementsOrigin'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.autoEntitlementsDestination','Invalid data in Auto Entitlements Destination') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.autoEntitlementsDestination'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.officeEntitlementsWeight','Invalid data in Office Entitlements Weight') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.officeEntitlementsWeight'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.officeEntitlementsFreight','Invalid data in Office Entitlements Freight') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.officeEntitlementsFreight'].focus();
          return false
       } 
       
       if (checkFloat('customerFileForm','imfEntitlements.officeEntitlementsOrigin','Invalid data in Office Entitlements Origin') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.officeEntitlementsOrigin'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.officeEntitlementsDestination','Invalid data in Office Entitlements Destination') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.officeEntitlementsDestination'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.insurance','Invalid data in Insurance') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.insurance'].focus();
          return false
       }
     if (checkFloat('customerFileForm','imfEntitlements.rate','Invalid data in Rate') == false)
       {
          document.forms['customerFileForm'].elements['imfEntitlements.rate'].focus();
          return false
       }  
         
     
 }
 
function focusDate(target){
	document.forms['customerFileForm'].elements[target].focus();
}
	  
function findCrmDetails(){
	
	if('${customerFile.id}' == null || '${customerFile.id}'==''){
		animatedcollapse.toggle('crm');
		alert('Please save Customer File first before access CRM Details.');
	}else{
		new Ajax.Request('findCrmDetailAjax.html?ajax=1&decorator=simple&popup=true&cid=${customerFile.id}',
				  {
			   method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      var container = document.getElementById("crmDetailsAjax");
				      container.innerHTML = response;
				      setOnSelectBasedMethods(["findPeriodDateChanged();fieldValidate();crmFieldValueChanged()"]);
				      setCalendarFunctionalityForCrm();
				    },
				    
				    onFailure: function(){ 
					    //alert('Something went wrong...')
					}
			  });
		}
   	}
function deleteDate(evt,targetElement){
		var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==46){
  		targetElement.value = '';
  		findPeriodDateChanged();
  }else{
	  if(keyCode==8){
	  	targetElement.value = '';
	  	findPeriodDateChanged();
	  }
  }
}
function findPeriodDateChanged(){
	var day = '';
    var month = '';
    var year = '';
    var finalDate ='';
    var eDate = null;
    if((dbDateValue!=null && dbDateValue!='' ) && dbDateValue.indexOf('-')>-1){
    var mySplitResult = dbDateValue.split("-");
    day = mySplitResult[2];
    day = day.substring(0,2)
    month = mySplitResult[1];
    year = mySplitResult[0];
    
  if(month == 'Jan')  { month = "01";
   }  else if(month == 'Feb') { month = "02";
   } else if(month == 'Mar') { month = "03"
   }   else if(month == 'Apr')  { month = "04"
   }  else if(month == 'May') { month = "05"
   }  else if(month == 'Jun') { month = "06"
   }  else if(month == 'Jul') { month = "07"
   } else if(month == 'Aug') {month = "08"
   } else if(month == 'Sep')  {month = "09"
   } else if(month == 'Oct')  {month = "10"
   }  else if(month == 'Nov')  { month = "11"
   } else if(month == 'Dec')  {month = "12";
   }
    finalDate = month+"-"+day+"-"+year;
   date1 = finalDate.split("-");
   eDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  }
  var day2 = '';
   var month2 = '';
   var year2 = '';
   var finalDate2 ='';
   var sDate = null;
   var date2 = null;
   if(formDateValue=='loadBegin'){
	   date2 =  document.getElementById("loadBeginDate1").value;
   }else{
	   date2 =  document.getElementById("loadEndDate1").value;
   }
   if((date2!=null && date2!='' ) && date2.indexOf('-')>-1){
      var mySplitResult2 = date2.split("-");
       day2 = mySplitResult2[0];
       month2 = mySplitResult2[1];
       year2 = mySplitResult2[2];
      if(month2 == 'Jan')  { month2 = "01";
      }  else if(month2 == 'Feb') { month2 = "02";
      } else if(month2 == 'Mar') {month2 = "03"
      } else if(month2 == 'Apr') {month2 = "04"
      }  else if(month2 == 'May')  { month2 = "05"
      }  else if(month2 == 'Jun') { month2 = "06"
      } else if(month2 == 'Jul') { month2 = "07"
      } else if(month2 == 'Aug')  {month2 = "08"
      } else if(month2 == 'Sep')  { month2 = "09"
      } else if(month2 == 'Oct')  { month2 = "10"
      }  else if(month2 == 'Nov') { month2 = "11"
      } else if(month2 == 'Dec')  {month2 = "12";
      }
      finalDate2 = month2+"-"+day2+"-"+year2;
       date2 = finalDate2.split("-");
       sDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
   }
	if(+eDate!=+sDate){
	 document.getElementById("timeOfReturn").value='';
	}
}

var dbDateValue="";
var formDateValue="";
function calculateDate(dbDate,formDate){
	dbDateValue = dbDate;
	formDateValue = formDate;
}

function checkDateValue(formDate){
	if(formDate=='loadBegin'){
		   if(document.getElementById("loadBeginDate1").value==''){
			   document.getElementById("timeOfReturn").value='';
		   }
	   }else{
		   if(document.getElementById("loadEndDate1").value==''){
			   document.getElementById("timeOfReturn").value=''; 
		   }
	   }
}

function fieldValidate(){
	   var date1 = document.getElementById("loadBeginDate1").value;	 
	   var date2 = document.getElementById("loadEndDate1").value; 
	   var mySplitResult = date1.split("-");
	   var day = mySplitResult[0];
	   var month = mySplitResult[1];
	   var year = mySplitResult[2];
	  if(month == 'Jan'){month = "01";
	   }else if(month == 'Feb'){month = "02";
	   }else if(month == 'Mar'){month = "03"
	   }else if(month == 'Apr'){month = "04"
	   }else if(month == 'May'){month = "05"
	   }else if(month == 'Jun'){month = "06"
	   }else if(month == 'Jul'){month = "07"
	   }else if(month == 'Aug'){month = "08"
	   }else if(month == 'Sep'){month = "09"
	   }else if(month == 'Oct'){month = "10"
	   }else if(month == 'Nov'){month = "11"
	   }else if(month == 'Dec'){month = "12";
	   }
	   var finalDate = month+"-"+day+"-"+year;
	   var mySplitResult2 = date2.split("-");
	   var day2 = mySplitResult2[0];
	   var month2 = mySplitResult2[1];
	   var year2 = mySplitResult2[2];
	   if(month2 == 'Jan'){month2 = "01";
	   } else if(month2 == 'Feb'){month2 = "02";
	   }else if(month2 == 'Mar'){month2 = "03"
	   }else if(month2 == 'Apr'){month2 = "04"
	   }else if(month2 == 'May'){month2 = "05"
	   }else if(month2 == 'Jun'){month2 = "06"
	   }else if(month2 == 'Jul'){month2 = "07"
	   }else if(month2 == 'Aug'){month2 = "08"
	   }else if(month2 == 'Sep'){month2 = "09"
	   }else if(month2 == 'Oct'){month2 = "10"
	   }else if(month2 == 'Nov'){month2 = "11"
	   }else if(month2 == 'Dec'){month2 = "12";
	   }
	  var finalDate2 = month2+"-"+day2+"-"+year2;
	  date1 = finalDate.split("-");
	  date2 = finalDate2.split("-");
	  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
	  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
	  var daysApart = Math.round((eDate-sDate)/86400000);  
	  if(daysApart<0){
	    alert("To Date should be greater than  or equal to From Date");
	    document.getElementById("loadEndDate1").value='';
	    return false;
	  }
	}
	
function crmFieldValueChanged(){
	document.getElementById("isCrmFieldValueChanged").value='Yes';
}

function checkPeriodDate(temp) { 
	
   var mydate=new Date();
	var year=mydate.getFullYear()
	var year1=mydate.getFullYear()
	if (year < 1000)
	year+=1900
    var day = mydate.getDate();
	var day1=day-1
	var month=mydate.getMonth()+1
	var month1=month
		if(temp.value==''){
			document.getElementById("loadBeginDate1").value='';
			document.getElementById("loadEndDate1").value='';
	} 
	if(temp.value=='1Year'){
    year=year;
    year1=year+1;
 	month=month;
	month1=month1;
	day=day;
	day1=day1; 
	} 
		if(temp.value=='2years'){
			year=year;
			   year1=year+2;
			   month=month;
				month1=month1;
				day=day;
				day1=day1; 
		} 
		if(temp.value=='3years'){
			year=year;
			   year1=year+3;
			   month=month;
				month1=month1;
				day=day;
				day1=day1; 	
		}
		
		if(temp.value=='4years'){
			year=year;
			   year1=year+4;
			  month=month;
			month1=month1;
				day=day;
				day1=day1;  
		}
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		if(month1 == 1)month1="Jan";
		if(month1 == 2)month1="Feb";
		if(month1 == 3)month1="Mar";
		if(month1 == 4)month1="Apr";
		if(month1 == 5)month1="May";
		if(month1 == 6)month1="Jun";
		if(month1 == 7)month1="Jul";
		if(month1 == 8)month1="Aug";
		if(month1 == 9)month1="Sep";
		if(month1 == 10)month1="Oct";
		if(month1 == 11)month1="Nov";
		if(month1 == 12)month1="Dec";
		
		if (day<10){
			day="0"+day
		}
		if (day1<10){
			day1="0"+day1
		}
		var y=""+year; 
		var y1=""+year1;
		var fromdate=day+"-"+month+"-"+y.substring(2,4);
		var todate=day1+"-"+month1+"-"+y1.substring(2,4);
		
		if(temp.value!=''){
			document.getElementById("loadBeginDate1").value=fromdate;
			document.getElementById("loadEndDate1").value=todate;
		
		} 
		
}

  
</script>
<script type="text/JavaScript">
var r={specials:/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\|'&'\['&'\]'&'\,'&'\`'&'\='&'0'&'1'&'2'&'3'&'4'&'5'&'6'&'7'&'8'&'9'&'\.'&'\('&'\)'&'\+'&'\"'&'\/']/g,quotes:/['\''&'\"']/g}
function onlyCharsAllowSpouse(e,t,n){
	var i=e.which?e.which:e.keyCode;
	var s=i>=0&&i<=32||i>=65&&i<=90||i==222||i==35||i==36||i==37||i==39||i==190||i==46||i==189||i==45||i==95;
	t.value=t.value.replace(r[n],"");
	return s
	}
</script>
