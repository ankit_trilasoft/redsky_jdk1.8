package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RedskyBillingDao;
import com.trilasoft.app.model.RedskyBilling;
import com.trilasoft.app.service.DsPreviewTripManager;
import com.trilasoft.app.service.RedskyBillingManager;

public class RedskyBillingManagerImpl extends GenericManagerImpl<RedskyBilling, Long> implements RedskyBillingManager {
	RedskyBillingDao redskyBillingDao;
	public RedskyBillingManagerImpl(RedskyBillingDao redskyBillingDao){
		super(redskyBillingDao); 
        this.redskyBillingDao = redskyBillingDao;  
	}
	public List  putRedskyBillingList(String sessionCorpID) {
		
		return redskyBillingDao.putRedskyBillingList(sessionCorpID);
	}
	public List getRedskyBillingList(String corpID, int year) {
		
		return redskyBillingDao.getRedskyBillingList(corpID, year);
	}
	public List getRedskyBillingListDetails(String corpID, int month, int year) {
		return redskyBillingDao.getRedskyBillingListDetails(corpID, month, year);
	}
	public List gettotalCountOfSo(String corpID, int year) {
		return redskyBillingDao.gettotalCountOfSo(corpID, year);
	}
	public String checkByShipNumber(String corpID,String shipNumber){
		return redskyBillingDao.checkByShipNumber(corpID, shipNumber);
	}
	public int getDetailsByProcedure(int proc, Date date){
		return redskyBillingDao.getDetailsByProcedure(proc,date);
		
	}
}
