<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="cPortalResourceMgmtList.title" /></title>
<meta name="heading" content="<fmt:message key='cPortalResourceMgmtList.heading'/>" />
<c:if test="${param.popup}">
	<link rel="stylesheet" type="text/css" media="all"
		href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" />
</c:if>
<style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF; background-repeat:repeat-x; padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

.uploadtext {background-color:#FFFFFF;color:#464646;font-family:arial,verdana;font-size:11px;font-weight:normal;
text-decoration:none;
}
.input-textblack {border:1px solid #74B3DC;color:#000000;font-family:arial,verdana;font-size:12px;height:53px;
background-color:#ffffff;text-decoration:none;
}

.list-menublack {border:1px solid #7A8A99;color:#000000;font-family:arial,verdana;font-size:12px;height:20px;
background-color:#EFEFEF;overflow:hidden;padding:1px;text-decoration:none;
}
.input-upload {color:#000000;font-family:arial,verdana;font-size:12px;height:22px;overflow:hidden;padding:1px;
text-decoration:none;border-style: solid;border-width: thick;border-color: #0f0 !important;
}
.cssbutton {-x-system-font:none;color:#000000;font-family:arial;font-size:12px;font-size-adjust:none;font-stretch:normal;
font-style:normal;font-variant:normal;font-weight:bold;height:30px;line-height:normal;padding:2px;width:55px;
}
.appclass{background-color: #fff;border: 1px solid #74B3DC; margin:-1px -4px -1px -1px; padding: 0px;}
.text-area {
border:1px solid #219DD1; font: 11px arial;}


</style>
</head>
<s:form id="cPortalResourceMgmtForm" name="cPortalResourceMgmtForm" action="saveCPortalResourceAdmin" enctype="multipart/form-data"  method="post" validate="true" >
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="cportalResource.documentLocation"/>
<s:hidden name="cportalResource.fileContentType"/>
<s:hidden name="cportalResource.fileType"/>
<s:hidden name="cportalResource.fileSize"/>
<c:set var="id" value="${cportalResource.id}"/>
<s:hidden name="cportalResource.id"/>
<c:set var="description" value="${cportalResource.documentName}"/>
<div id="Layer1" style="width:100%">	
	<div id="otabs">
		<ul>
		<c:url value="frequentlyAskedList.html" var="url">
	</c:url>
	<c:url value="contractFilePolicy.html" var="url11"/>
			<li ><a class="current"><span>CPortal ResourceMgmt Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a href="${url11}"><span>Policy</span></a></li>
<li><a href="${url}"><span>FAQ</span></a></li>
		</ul>
	</div>	
	
	<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:12px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
			<table class="detailTabLabel" cellspacing="2" cellpadding="2" border="0" width="">
				<tbody>
				<tr>
					<td align="right" class="listwhitetext" style="width:100px;" >Document Type</td>
							
							<td width="90">
							<s:select name="cportalResource.documentType" cssClass="list-menu" list="%{cPortalResourceDocsList}" onchange="setDescription(this);" headerKey="" headerValue="" /></td>
							<td width="" align="right" class="listwhitetext">Exclude For Bill to Code</td>
							<td align="left"><s:textfield name="cportalResource.billToExcludes" cssClass="input-text" maxlength="200" size="53"/></td>
							<c:if test="${cportalResource.corpId=='SSCW'}">
							<td width="" align="right" class="listwhitetext"><b>e.g. 500270,500130,etc.</b></td>
							</c:if>
							<c:if test="${cportalResource.corpId=='STAR'}">
							<td width="" align="right" class="listwhitetext"><b>e.g. A30174,A30110,etc.</b></td>
							</c:if>
							</tr>
							<tr>
							<td align="right" class="listwhitetext" >Document Name</td>
							<td colspan="" valign="top"><s:textfield cssClass="input-text" size="35" name="cportalResource.documentName" />
							</td>
							<c:if test="${not empty cportalResource.id}">
					    	<td align="right" class="listwhitetext">File Name</td>
						    <td colspan="" valign="top" align="left"><s:textfield name="cportalResource.fileFileName" cssClass="input-text" readonly="true" size="53"/></td>
					        </c:if>
							</tr>
							<tr>
							<td align="right" class="listwhitetext" ><fmt:message key='cPortalResourceMgmt.originCountry'/></td>	
							<td colspan=""><s:select cssClass="list-menu" name="cportalResource.originCountry" list="%{ocountry}" cssStyle="width:205px" headerKey="" headerValue=""/></td>
										<td align="right" class="listwhitetext" >Language</td>
					<td colspan=""><s:select cssClass="list-menu" name="cportalResource.language" list="%{language}" cssStyle="width:205px" headerKey="" headerValue=""/></td>
					</tr>
					<tr>
					<td align="right" class="listwhitetext" ><fmt:message key='cPortalResourceMgmt.destinationCountry'/></td>
					<td colspan=""><s:select cssClass="list-menu" name="cportalResource.destinationCountry" list="%{dcountry}" cssStyle="width:205px" headerKey="" headerValue=""/></td>
							<td align="right" class="listwhitetext" >Add to Info Package</td>	
							<td colspan=""><s:select cssClass="list-menu" name="cportalResource.infoPackage" list="%{infoPackage}" cssStyle="width:205px" headerKey="" headerValue=""/></td>
                    </tr>
                    <tr>
                    <td colspan="4" style="margin: 0px;padding: 0px; margin-top: 0px;">
                    <table style="margin-bottom: 0px; padding: 0px;">
                    <tr>
                    <!--<td align="right" class="listwhitetext" width="100px"><fmt:message key='customerFile.billToCode'/></td>
                    --><s:hidden name="cportalResource.billToCode" ></s:hidden>
                     <s:hidden name="cportalResource.billToName" ></s:hidden>
                    <!--<td align="left"><s:textfield cssClass="input-text" name="cportalResource.billToCode" maxlength="10" size="10" onchange="findBillToName();"/></td>
                    <td width="22"><img class="openpopup" width="17" height="20" onclick="openPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
                    <td align="left"><s:textfield cssClass="input-text" name="cportalResource.billToName" size="40" readonly="true"/></td>
                    --></tr>
                    </table>
					</td>
					</tr>
                    <c:if test="${empty cportalResource.id}">
                    <tr>
					<td class="listwhitetext"  align="right" ><fmt:message key='myFile.file' /></td>
					<td colspan="3"><s:file name="file" label="%{getText('cPortalResourceMgmtForm.file')}" required="true" size="40" /></td>
					</tr>
				<tr>
				<td></td>
				<td style="padding-top:12px;"><s:submit key="button.upload" name="cportalResource.upload" method="saveCPortalResource" cssClass="cssbutton" cssStyle="margin-right:5px; height:25px; width:90px; padding:2px 0px 2px; font-size: 15" onclick="return validateFields();" />
				</td>
				</tr>			
				</c:if>
				 <tr>
					      <td></td>
					      <td colspan="10" align="left">
					      <c:if test="${not empty cPortalDocumentList}">
					        <display:table name="cPortalDocumentList" id="cPortalDocumentList" class="table" pagesize="10" requestURI="" style="width:100%;">
					        <display:column style="width:5%" title="#"><a onclick=""><c:out value="${cPortalDocumentList_rowNum}"/></a></display:column>
					        <display:column  title="Document Name" >
					        <a onclick="javascript:openWindow('CportalResourceMgmtImageServletAction.html?id=${cPortalDocumentList.id}&decorator=simple&popup=true',900,600);">
					        <c:out value="${cPortalDocumentList.documentName}" escapeXml="false"/></a>
					        </display:column>
					        <display:column property="fileFileName" title="File Name" maxLength="10" />
					        <display:column property="fileSize" title="Size"/>
					        <display:column property="updatedOn"  title="Updated On"  format="{0,date,dd-MMM-yyyy}" />
						    <display:column property="updatedBy"  title="Updated By"/></display:table>
					       </c:if>
					       </td>
					      </tr>
</tbody>
</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${cportalResource.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="cportalResource.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td style="width:120px"><fmt:formatDate value="${cportalResource.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						<c:if test="${not empty cportalResource.id}">
								<s:hidden name="cportalResource.createdBy"/>
								<td ><s:label name="createdBy" value="%{cportalResource.createdBy}"/></td>
							</c:if>
							<c:if test="${empty cportalResource.id}">
								<s:hidden name="cportalResource.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${cportalResource.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="cportalResource.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td style="width:120px"><fmt:formatDate value="${cportalResource.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty cportalResource.id}">
							<s:hidden name="cportalResource.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{cportalResource.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty cportalResource.id}">
							<s:hidden name="cportalResource.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div>
<c:if test="${empty cportalResource.id}">
<tr>	
			<td align="right">
			<input class="cssbutton" type="button" style="margin-right: 5px;height: 25px;width:75px;" value="Go To List"
			onclick="location.href='cPortalResources.html'" />
		</td>
		<s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px"/> 
	</tr>	
	</c:if>
	
	<c:if test="${not empty cportalResource.id}">
	<td><s:submit  key="button.save" cssClass="cssbutton" cssStyle="margin-right: 5px;height: 25px;width:55px;"/></td>
	<td align="right">
			<input class="cssbutton" type="button" style="margin-right: 5px;height: 25px;width:75px;" value="Go To List"
			onclick="location.href='cPortalResources.html'" />
		</td>
		<s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px"/> 
	</c:if>
</s:form>					

<script type="text/javascript">
function openPopWindow(){
	javascript:openWindow('partnersPopup.html?partnerType=&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=cportalResource.billToName&fld_code=cportalResource.billToCode');
}

function setDescription(targetElement){
	document.forms['cPortalResourceMgmtForm'].elements['cportalResource.documentName'].value = targetElement.options[targetElement.selectedIndex].text;
}

function validateFields(){
	if(document.forms['cPortalResourceMgmtForm'].elements['cportalResource.documentType'].value==""){
		alert("Select a Document type");
		return false;
	}else{
		if(document.forms['cPortalResourceMgmtForm'].elements['file'].value==""){
			alert("Select a file");
			return false;
		}else{
			return true;			
		}
	}
}
	
	
function findBillToName(){
    var billToCode = document.forms['cPortalResourceMgmtForm'].elements['cportalResource.billToCode'].value;
    if(billToCode==''){
    	document.forms['cPortalResourceMgmtForm'].elements['cportalResource.billToName'].value="";
	}
    if(billToCode!=''){
     var url="billToCodeName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    }
}	
	function handleHttpResponse2(){
       if (http2.readyState == 4){
             var results = http2.responseText
             results = results.trim();
             results=results.replace("[",'');
             results=results.replace("]",'');
          if(results.length >= 1){ 
             document.forms['cPortalResourceMgmtForm'].elements['cportalResource.billToName'].value=results;
             return true;
          }else{
            	alert("Bill To code not valid");    
				document.forms['cPortalResourceMgmtForm'].elements['cportalResource.billToCode'].value="";
				document.forms['cPortalResourceMgmtForm'].elements['cportalResource.billToName'].value="";
				return false;
          }
     }
}
var http2 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this CPortal ResourceMgmt Doc?");
	var did = targetElement;
	if (agree){
		location.href="deleteResourceDoc.html?id="+did+"";
	}
	else{
		return false;
	}
} 
</script>					