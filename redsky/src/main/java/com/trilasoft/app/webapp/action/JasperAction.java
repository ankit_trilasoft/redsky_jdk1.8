package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.Person;
import com.trilasoft.app.model.Reports;
import com.trilasoft.app.service.ReportsManager;

import net.sf.jasperreports.engine.JasperCompileManager;


public class JasperAction extends BaseAction {

	//basic List - it will serve as our dataSource later on
	private List myList;
	private Long reportId;
	private ReportsManager reportsManager;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.opensymphony.xwork.ActionSupport#execute()
	 */
	public String execute() throws Exception {

		// create some imaginary persons
		//Reports report=reportsManager.getReportId()
		Person p1 = new Person();
		p1.setFirstName("Patrick");
		p1.setLastName("Lightbuddie");
		
		Person p2 = new Person();
		p2.setFirstName("Jason");
		p2.setLastName("Carrora");

		Person p3 = new Person();
		p3.setFirstName("Alexandru");
		p3.setLastName("Papesco");

		Person p4 = new Person();
		p4.setFirstName("Jay");
		p4.setLastName("Boss");
		
		 
		/*HashMap<String, String> p1 = new HashMap<String, String>();
		p1.put("firstName", "Patrick from map");
		p1.put("lastName", "Lightbuddie");

		HashMap<String, String> p2 = new HashMap<String, String>();
		p2.put("firstName", "Jason");
		p2.put("lastName", "Carrora");

		HashMap<String, String> p3 = new HashMap<String, String>();
		p3.put("firstName", "Alexandru");
		p3.put("lastName", "Papesco");

		HashMap<String, String> p4 = new HashMap<String, String>();
		p4.put("firstName", "Jay");
		p4.put("lastName", "Boss");*/
		
		/*
		 * store everything in a list - normally, this should be coming from a
		 * database but for the sake of simplicity, I left that out
		 */
		myList = new ArrayList();
		myList.add(p1);
		myList.add(p2);
		myList.add(p3);
		myList.add(p4);

		/*
		 * Here we compile our xml jasper template to a jasper file.
		 * Note: this isn't exactly considered 'good practice'.
		 * You should either use precompiled jasper files (.jasper) or provide some kind of check
		 * to make sure you're not compiling the file on every request.
		 * If you don't have to compile the report, you just setup your data source (eg. a List)
		 */
		
		//Reports reports = ReportsManager.getReportId(reportId);
		//JasperCompileManager.compileReportToFile(
		//			"C:/Source/redsky/src/main/webapp/jasper/" + report.getReportFileName(),
		//			"C:/Source/redsky/src/main/webapp/jasper/" + report.getReportFileName());

		String myFile="ActiveShip";
		try {
			JasperCompileManager.compileReportToFile(
					"/usr/local/apache-tomcat-5.5.23/webapps/redsky/jasper/ActiveShip.jrxml",
					"/usr/local/apache-tomcat-5.5.23/webapps/redsky/jasper/ActiveShip.jasper");
			reportsManager.generateReport(reportId); 
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		//if all goes well ..
		return SUCCESS;
	}

	/**
	 * @return Returns the myList.
	 */
	public List getMyList() {
		return myList;
	}
	
	/**
	 * @return Returns the myList.
	 */
	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public ReportsManager getReportsManager() {
		return reportsManager;
	}

	public void setReportsManager(ReportsManager reportsManager) {
		this.reportsManager = reportsManager;
	}	
	
	

}
