package com.trilasoft.app.service.impl;
import com.trilasoft.app.dao.NewsUpdateDao;
import com.trilasoft.app.model.NewsUpdate;
import com.trilasoft.app.service.NewsUpdateManager;
import org.appfuse.service.impl.GenericManagerImpl;

import java.util.Date;
import java.util.List;

public class NewsUpdateManagerImpl extends GenericManagerImpl<NewsUpdate, Long> implements NewsUpdateManager
{
	NewsUpdateDao newsUpdateDao;

	public NewsUpdateManagerImpl(NewsUpdateDao newsUpdateDao) {
        super(newsUpdateDao);
        this.newsUpdateDao = newsUpdateDao;
    }

        public List<NewsUpdate> findByDetail(String details) {
        return newsUpdateDao.findByDetail(details);
    }
        public List findUpdatedDate(String corpId){
        	return newsUpdateDao.findUpdatedDate(corpId);
        }
        public List findDistinct(){
        	return newsUpdateDao.findDistinct();
        }
        public List findListOnCorpId(String corpId,Boolean addValues){
        	return newsUpdateDao.findListOnCorpId(corpId,addValues);
        }
        public List findListByModuleCorp(String module,String corpid, Date publishedDate,String isVisible,Boolean activeStatus)
        {
        	return newsUpdateDao.findListByModuleCorp(module,corpid,publishedDate,isVisible,activeStatus);
        }
        public List findByModule(String module,String corpid,Date publishedDate)
        {
        	return newsUpdateDao.findByModule(module,corpid,publishedDate);
        }
        public String findNewsUpdateDescForToolTip(Long id, String corpId)
  	  	{
  		  return newsUpdateDao.findNewsUpdateDescForToolTip(id,corpId);
  	  	}
}