package com.trilasoft.app.webapp.action;

import static java.lang.System.out;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.dao.hibernate.util.FileSizeUtil;
import com.trilasoft.app.model.CPortalResourceMgmt;
import com.trilasoft.app.service.CPortalResourceMgmtManager;
import com.trilasoft.app.service.RefMasterManager;

public class CPortalResourceMgmtAction extends BaseAction implements Preparable {

	private CPortalResourceMgmtManager cportalResourceManager;

	private List cportalResourceList;
	private List billToCodeList;
	private RefMasterManager refMasterManager;
	private CPortalResourceMgmt cportalResource;
	private Map<String, String> ocountry;
	private File file;
	private String fileNotExists;
	private String fileFileName;
	private String billTocode;
	private String billToName;
	private String documentType;
	private String documentName;
	private String fileType;
	private String description;
	private MimetypesFileTypeMap mimetypesFileTypeMap;
	private Map<String, String> dcountry;
	private static Map<String, String> cPortalResourceDocsList;
	private Long id;   
	private String sessionCorpID;
	private String partnerCode;
	private List cPortalDocumentList;
	private List accountportalResourceList;
	Date currentdate = new Date();
    private Map<String, String> jobType;
    private Map<String, String> modeType;

	static final Logger logger = Logger.getLogger(CPortalResourceMgmtAction.class);

	 


	// private List day;

	public CPortalResourceMgmtAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		mimetypesFileTypeMap = new MimetypesFileTypeMap();
		mimetypesFileTypeMap.addMimeTypes("text/plain txt text TXT");
		mimetypesFileTypeMap.addMimeTypes("application/msword doc");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-powerpoint ppt");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-excel xls xlsx");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-outlook msg");
		mimetypesFileTypeMap.addMimeTypes("application/pdf pdf");
		mimetypesFileTypeMap.addMimeTypes("image/jpeg jpg jpeg");
		mimetypesFileTypeMap.addMimeTypes("image/gif gif");
	}
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){  
			getComboList();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}
	
	public String resourceExtractInfo(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}

	public void resourceExtract() throws Exception { 
		List<CPortalResourceMgmt> resourceExtractList = cportalResourceManager.resourceExtracts(documentType,sessionCorpID); 
		HttpServletResponse response = getResponse();
		response.setContentType("application/vnd.ms-excel");
		ServletOutputStream outputStream = response.getOutputStream();
		File file = new File("resourceExtracts");
		response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls");
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0"); 
		outputStream.write("Document Type\t".getBytes());
		outputStream.write("Document Name\t".getBytes());
		outputStream.write("Job type\t".getBytes());
		outputStream.write("Mode\t".getBytes());
		outputStream.write("Exclude bill to code\t".getBytes());
		outputStream.write("Origin Country\t".getBytes());
		outputStream.write("Destination country\t".getBytes());
		outputStream.write("Bill to code\t".getBytes());
		outputStream.write("Language\t".getBytes());
		outputStream.write("Add to info package \t".getBytes()); 
		outputStream.write("Sequence\t".getBytes());
		outputStream.write("File name\t".getBytes());
		outputStream.write("\r\n".getBytes());
		Iterator it = resourceExtractList.iterator();

		while (it.hasNext()) {
			Object extracts = it.next();
		
		if (((CPortalResourceMgmt) extracts).getDocumentType() != null) {
			outputStream.write((((CPortalResourceMgmt) extracts).getDocumentType().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		}
		if (((CPortalResourceMgmt) extracts).getDocumentName() != null) {
			outputStream.write(( ((CPortalResourceMgmt) extracts).getDocumentName().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		} 
		if (((CPortalResourceMgmt) extracts).getJobType() != null) {
			outputStream.write((((CPortalResourceMgmt) extracts).getJobType().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		}
		if (((CPortalResourceMgmt) extracts).getMode() != null) {
			outputStream.write(( ((CPortalResourceMgmt) extracts).getMode().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		}
		if (((CPortalResourceMgmt) extracts).getBillToExcludes() != null) {
			outputStream.write(( ((CPortalResourceMgmt) extracts).getBillToExcludes().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		} 
		if (((CPortalResourceMgmt) extracts).getOriginCountry() != null) {
			outputStream.write((((CPortalResourceMgmt) extracts).getOriginCountry().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		} 
		if (((CPortalResourceMgmt) extracts).getDestinationCountry() != null) {
			outputStream.write(( ((CPortalResourceMgmt) extracts).getDestinationCountry().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		} 
		if (((CPortalResourceMgmt) extracts).getBillToCode() != null) {
			outputStream.write(( ((CPortalResourceMgmt) extracts).getBillToCode().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		} 
		if (((CPortalResourceMgmt) extracts).getLanguage() != null) {
			outputStream.write(( ((CPortalResourceMgmt) extracts).getLanguage().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		} 
		if (((CPortalResourceMgmt) extracts).getInfoPackage() != null) {
			outputStream.write(( ((CPortalResourceMgmt) extracts).getInfoPackage().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		}
		if (((CPortalResourceMgmt) extracts).getDocSequenceNumber() != null) {
			outputStream.write(( ((CPortalResourceMgmt) extracts).getDocSequenceNumber().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		} 
		if (((CPortalResourceMgmt) extracts).getFileFileName() != null) {
			outputStream.write(( ((CPortalResourceMgmt) extracts).getFileFileName().toString()).getBytes());
			outputStream.write("\t".getBytes());
		} 
		else { 
			outputStream.write("\t".getBytes());
		}
		outputStream.write("\r\n".getBytes());
	}
	}
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		dcountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		ocountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		jobType=refMasterManager.findByParameter(sessionCorpID, "JOB");
		cportalResourceList = cportalResourceManager.cPortalResourceMgmtList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
     public String listCPortalMgmts(){
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		dcountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		ocountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		accountportalResourceList = cportalResourceManager.accountportalResourceMgmtList(user.getParentAgent(),sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	 return SUCCESS;	 
     }
     @SkipValidation
 	public String searchcPortalList(){
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
 		ocountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
 		dcountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
 		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
 		accountportalResourceList=cportalResourceManager.FindsearchCriteria(cportalResource.getDocumentType(),cportalResource.getDocumentName(),cportalResource.getFileFileName(),cportalResource.getOriginCountry(),cportalResource.getDestinationCountry(),user.getParentAgent(),sessionCorpID);
 		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
 		return SUCCESS;	
 	}
	public void setId(Long id) {
		this.id = id;
	}

	
    private Map<String, String> language;
    private Map<String, String> infoPackage;
	public String getComboList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		ocountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		dcountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		language = refMasterManager.findByParameter(sessionCorpID, "LANGUAGE");
		infoPackage = refMasterManager.findByParameter(sessionCorpID, "INFOPACKAGE");
		cPortalResourceDocsList = refMasterManager.findByParameter(sessionCorpID, "CPORTAL_DOC_TYPE");
		jobType=refMasterManager.findByParameter(sessionCorpID, "JOB");
		modeType = refMasterManager.findByParameter(sessionCorpID, "MODE");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
      	return SUCCESS;
    }
	
	@SkipValidation
	public String searchcportalResource(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		ocountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		dcountry = refMasterManager.findCountry(sessionCorpID, "COUNTRY");
		cportalResourceList=cportalResourceManager.getBysearchCriteria(cportalResource.getDocumentType(),cportalResource.getDocumentName(),cportalResource.getFileFileName(),cportalResource.getOriginCountry(),cportalResource.getDestinationCountry(),cportalResource.getBillToCode(),cportalResource.getBillToName(),sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	 private List multiplChildBillToCode;
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList();
		if (id != null) {
			cportalResource = cportalResourceManager.get(id);
			if((cportalResource.getChildBillToCode()!=null) && (!cportalResource.getChildBillToCode().equals(""))){
				String[] serviceType = cportalResource.getChildBillToCode().split(",");
				try {
					multiplChildBillToCode=new ArrayList();
					for (String sType : serviceType) {
						multiplChildBillToCode.add(sType.trim());
					    }
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
			cPortalDocumentList=cportalResourceManager.findCPortalDocument(cportalResource.getBillToCode(),cportalResource.getDocumentType(),cportalResource.getDocumentName(),cportalResource.getFileFileName(),cportalResource.getLanguage(),cportalResource.getInfoPackage(),sessionCorpID);
		} else {
			cportalResource = new CPortalResourceMgmt();
			cportalResource.setCorpId(sessionCorpID);
			cportalResource.setUpdatedOn(new Date());
			cportalResource.setCreatedOn(new Date());
			}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String checkBillToCode()throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(!(cportalResource.getBillToCode().toString().trim().equals(""))){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			if(cportalResourceManager.vendorName(cportalResource.getBillToCode(), sessionCorpID).isEmpty()){
			return INPUT;
		}
		else{
			
			return SUCCESS;
		}
		}
		else {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	}
	@SkipValidation
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList();
		if(checkBillToCode().equals("input")){
			String key = "Bill To Code is not valid, please select again.";
			errorMessage(getText(key));
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		}
		boolean isNew = (cportalResource.getId() == null);
		
		if(isNew){
		if (file == null || file.length() > 10485760) {
			fileNotExists = "File cannot be uploaded as size of the file exceeded the maximum allowed 5MB";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		}
		if (!file.exists()) {
			fileNotExists = "File cannot be uploaded as the file is invalid";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		}
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "cportalResourceMgmtdoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
				Long kid;
				if(cportalResourceManager.getMaxId().get(0) == null){
					kid = 1L;
				}
				else{
					kid = Long.parseLong(cportalResourceManager.getMaxId().get(0).toString());
					kid = kid + 1;
				}
				InputStream stream = new FileInputStream(file);

				//write the file to the file specified
				OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + fileFileName);
				int bytesRead = 0;
				byte[] buffer = new byte[8192];

				while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
					bos.write(buffer, 0, bytesRead);
				}

				bos.close();
				stream.close();
             	InputStream copyStream = new FileInputStream(file);

				OutputStream boscopy = new FileOutputStream(uploadDir + fileFileName);
				int bytesReadCopy = 0;
				byte[] bufferCopy = new byte[8192];

				while ((bytesReadCopy = copyStream.read(bufferCopy, 0, 8192)) != -1) {
					boscopy.write(bufferCopy, 0, bytesReadCopy);
				}

				boscopy.close();
				copyStream.close();
				getRequest().setAttribute("documentLocation", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
		
		cportalResource.setFile(file);
		FileSizeUtil fileSizeUtil = new FileSizeUtil();
		cportalResource.setFileSize(fileSizeUtil.FindFileSize(file));
		cportalResource.setFileContentType(mimetypesFileTypeMap.getContentType(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName));
		cportalResource.setFileFileName(fileFileName);
		cportalResource.setDocumentLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName);
		cportalResource.setFileType(fileType);
		
		cportalResource.setUpdatedOn(new Date());
		cportalResource.setUpdatedBy(getRequest().getRemoteUser());
        if(isNew){
        	cportalResource.setCreatedOn(new Date());
        }
        cportalResource.setCorpId(sessionCorpID);
        String str="";
        String[] serviceType1 = cportalResource.getBillToExcludes().split(" ");
        for(int i=0;i<serviceType1.length;i++){
        	str+=serviceType1[i].trim();
        }
        cportalResource.setBillToExcludes(str);
        cportalResourceManager.save(cportalResource);

		String key = (isNew) ? "cPortalResourceMgmt.added" : "cPortalResourceMgmt.updated";
		saveMessage(getText(key));

		}
		if(!isNew){
			//cportalResource = cportalResourceManager.get(id);
			cportalResource.setUpdatedOn(new Date());
			cportalResource.setDocumentType(cportalResource.getDocumentType());
			cportalResource.setDocumentName(cportalResource.getDocumentName());
			cportalResource.setCorpId(sessionCorpID);
			cportalResource.setUpdatedBy(getRequest().getRemoteUser());
			String str="";
	        String[] serviceType1 = cportalResource.getBillToExcludes().split(" ");
	        for(int i=0;i<serviceType1.length;i++){
	        	str+=serviceType1[i];
	        }
	        cportalResource.setBillToExcludes(str);
			cportalResourceManager.save(cportalResource);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return "AfterEdit";
		}
		cPortalDocumentList=cportalResourceManager.findCPortalDocument(cportalResource.getBillToCode(),cportalResource.getDocumentType(),cportalResource.getDocumentName(),cportalResource.getFileFileName(),cportalResource.getLanguage(),cportalResource.getInfoPackage(),sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String saveCPortalResource() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList();
		if(checkBillToCode().equals("input")){
			String key = "Bill To Code is not valid, please select again.";
			errorMessage(getText(key));
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		}
		boolean isNew = (cportalResource.getId() == null);
		
		if(isNew){
		if (file == null || file.length() > 10485760) {
			fileNotExists = "File cannot be uploaded as size of the file exceeded the maximum allowed 5MB";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		}
		if (!file.exists()) {
			fileNotExists = "File cannot be uploaded as the file is invalid";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return INPUT;
		}
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "cportalResourceMgmtdoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
		File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
				Long kid;
				if(cportalResourceManager.getMaxId().get(0) == null){
					kid = 1L;
				}
				else{
					kid = Long.parseLong(cportalResourceManager.getMaxId().get(0).toString());
					kid = kid + 1;
				}
				InputStream stream = new FileInputStream(file);

				//write the file to the file specified
				OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + fileFileName);
				int bytesRead = 0;
				byte[] buffer = new byte[8192];

				while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
					bos.write(buffer, 0, bytesRead);
				}

				bos.close();
				stream.close();
             	InputStream copyStream = new FileInputStream(file);

				OutputStream boscopy = new FileOutputStream(uploadDir + fileFileName);
				int bytesReadCopy = 0;
				byte[] bufferCopy = new byte[8192];

				while ((bytesReadCopy = copyStream.read(bufferCopy, 0, 8192)) != -1) {
					boscopy.write(bufferCopy, 0, bytesReadCopy);
				}

				boscopy.close();
				copyStream.close();
				getRequest().setAttribute("documentLocation", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
		
		cportalResource.setFile(file);
		FileSizeUtil fileSizeUtil = new FileSizeUtil();
		cportalResource.setFileSize(fileSizeUtil.FindFileSize(file));
		cportalResource.setFileContentType(mimetypesFileTypeMap.getContentType(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName));
		cportalResource.setFileFileName(fileFileName);
		cportalResource.setDocumentLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName);
		cportalResource.setFileType(fileType);
		
		cportalResource.setUpdatedOn(new Date());
		cportalResource.setUpdatedBy(getRequest().getRemoteUser());
        if(isNew){
        	cportalResource.setCreatedOn(new Date());
        }
        cportalResource.setCorpId(sessionCorpID);
        String str="";
        String[] serviceType1 = cportalResource.getBillToExcludes().split(" ");
        for(int i=0;i<serviceType1.length;i++){
        	str+=serviceType1[i].trim();
        }
        cportalResource.setBillToExcludes(str);
        cportalResourceManager.save(cportalResource);

		String key = (isNew) ? "cPortalResourceMgmt.added" : "cPortalResourceMgmt.updated";
		saveMessage(getText(key));

		}
		if(!isNew){
			//cportalResource = cportalResourceManager.get(id);
			cportalResource.setUpdatedOn(new Date());
			cportalResource.setDocumentType(cportalResource.getDocumentType());
			cportalResource.setDocumentName(cportalResource.getDocumentName());
			cportalResource.setCorpId(sessionCorpID);
			cportalResource.setUpdatedBy(getRequest().getRemoteUser());
			String str="";
	        String[] serviceType1 = cportalResource.getBillToExcludes().split(" ");
	        for(int i=0;i<serviceType1.length;i++){
	        	str+=serviceType1[i].trim();
	        }
	        cportalResource.setBillToExcludes(str);
			cportalResourceManager.save(cportalResource);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return "AfterAdminEdit";
		}
		cPortalDocumentList=cportalResourceManager.findCPortalDocument(cportalResource.getBillToCode(),cportalResource.getDocumentType(),cportalResource.getDocumentName(),cportalResource.getFileFileName(),cportalResource.getLanguage(),cportalResource.getInfoPackage(),sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
public String deleteResourceDoc() {
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	   cportalResourceManager.remove(id);
	   list();
	 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
private List childList;
private List childBillTo=new ArrayList();
 public String childCodeList(){
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	   childList = cportalResourceManager.getChildList(partnerCode,sessionCorpID);
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;
	   
 }
public String billToCodeName(){  
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(partnerCode!=null){
        	billToCodeList=cportalResourceManager.vendorName(partnerCode,sessionCorpID);
        }else{
        	String message="The Vendor Name does not exist";
        	errorMessage(getText(message));
        }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	/**
	 * @param portalResourceMgmtManager the cPortalResourceMgmtManager to set
	 */
	/**
	 * @param cportalResourceManager the cportalResourceManager to set
	 */
	public void setCportalResourceManager(
			CPortalResourceMgmtManager cportalResourceManager) {
		this.cportalResourceManager = cportalResourceManager;
	}
	/**
	 * @param refMasterManager the refMasterManager to set
	 */
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getOcountry() {
		return ocountry;
	}

	public Map<String, String> getDcountry() {
		return dcountry;
	}
	/**
	 * @return the cPortalResourceDocsList
	 */
	public static Map<String, String> getCPortalResourceDocsList() {
		return cPortalResourceDocsList;
	}
	/**
	 * @param portalResourceDocsList the cPortalResourceDocsList to set
	 */
	public static void setCPortalResourceDocsList(
			Map<String, String> portalResourceDocsList) {
		cPortalResourceDocsList = portalResourceDocsList;
	}
	/**
	 * @return the cportalResource
	 */
	public CPortalResourceMgmt getCportalResource() {
		return cportalResource;
	}
	/**
	 * @param cportalResource the cportalResource to set
	 */
	public void setCportalResource(CPortalResourceMgmt cportalResource) {
		this.cportalResource = cportalResource;
	}
	/**
	 * @return the cportalResourceList
	 */
	public List getCportalResourceList() {
		return cportalResourceList;
	}
	/**
	 * @param cportalResourceList the cportalResourceList to set
	 */
	public void setCportalResourceList(List cportalResourceList) {
		this.cportalResourceList = cportalResourceList;
	}
	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}
	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}
	/**
	 * @return the fileNotExists
	 */
	public String getFileNotExists() {
		return fileNotExists;
	}
	/**
	 * @param fileNotExists the fileNotExists to set
	 */
	public void setFileNotExists(String fileNotExists) {
		this.fileNotExists = fileNotExists;
	}
	/**
	 * @return the fileFileName
	 */
	public String getFileFileName() {
		return fileFileName;
	}
	/**
	 * @param fileFileName the fileFileName to set
	 */
	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}
	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the billToCodeList
	 */
	public List getBillToCodeList() {
		return billToCodeList;
	}
	/**
	 * @param billToCodeList the billToCodeList to set
	 */
	public void setBillToCodeList(List billToCodeList) {
		this.billToCodeList = billToCodeList;
	}
	/**
	 * @return the partnerCode
	 */
	public String getPartnerCode() {
		return partnerCode;
	}
	/**
	 * @param partnerCode the partnerCode to set
	 */
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getBillTocode() {
		return billTocode;
	}
	public void setBillTocode(String billTocode) {
		this.billTocode = billTocode;
	}
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public Map<String, String> getLanguage() {
		return language;
	}
	public void setLanguage(Map<String, String> language) {
		this.language = language;
	}
	public Map<String, String> getInfoPackage() {
		return infoPackage;
	}
	public void setInfoPackage(Map<String, String> infoPackage) {
		this.infoPackage = infoPackage;
	}
	public List getcPortalDocumentList() {
		return cPortalDocumentList;
	}
	public void setcPortalDocumentList(List cPortalDocumentList) {
		this.cPortalDocumentList = cPortalDocumentList;
	}
	public List getAccountportalResourceList() {
		return accountportalResourceList;
	}
	public void setAccountportalResourceList(List accountportalResourceList) {
		this.accountportalResourceList = accountportalResourceList;
	}
	public Map<String, String> getJobType() {
		return jobType;
	}
	public void setJobType(Map<String, String> jobType) {
		this.jobType = jobType;
	}
	public List getChildBillTo() {
		return childBillTo;
	}
	public void setChildBillTo(List childBillTo) {
		this.childBillTo = childBillTo;
	}
	public List getMultiplChildBillToCode() {
		return multiplChildBillToCode;
	}
	public void setMultiplChildBillToCode(List multiplChildBillToCode) {
		this.multiplChildBillToCode = multiplChildBillToCode;
	}
	public List getChildList() {
		return childList;
	}
	public void setChildList(List childList) {
		this.childList = childList;
	}
	public Map<String, String> getModeType() {
		return modeType;
	}
	public void setModeType(Map<String, String> modeType) {
		this.modeType = modeType;
	}

	
}
