package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Location;

public interface XlsConverterDao extends GenericDao<Location,Long> {

	String saveData(String sessionCorpID, String file, String userName);

	String saveStorageData(String sessionCorpID, String file, String userName);
	
	String saveRateGridData(String sessionCorpID, String file, String userName,String charge, String contractName, String fileExt);
	
	String saveDelRateGridData(String sessionCorpID, String file,String userName, String charge, String contractName, String tempIds,String fileExt);
	String saveXlsFileDefaultAccountline(String sessionCorpID, String file, String userName,String fileExt,Boolean costElementFlag);
	public void updateDefaultAccountlinethroughProcedure(String sessionCorpID,Boolean costElementFlag,String userName);
}
