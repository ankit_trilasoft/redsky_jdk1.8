// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `contractChargesMandatory` BIt(1) DEFAULT b'0';

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('Billing','noInsurance' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','noInsurance' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insSubmitDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insSubmitDate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('CustomerFile','noInsurance' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`company` ADD COLUMN `quoteServices` VARCHAR(25) AFTER `numberOfPricePointUser`;

ALTER TABLE `redsky`.`inventorydata` ADD COLUMN `networkSynchedId` BIGINT(20) DEFAULT NULL;

ALTER TABLE `redsky`.`billing` ADD COLUMN `recSignedDisclaimerDate` DATETIME NULL DEFAULT NULL ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `maxupdatedon` DATETIME AFTER `contractChargesMandatory`;

ALTER TABLE `redsky`.`company` ADD COLUMN `surveyLinking` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`lumpsumInsurance` ADD COLUMN `networkSynchedId` BIGINT(20) DEFAULT NULL;

ALTER TABLE `redsky`.`dsfamilydetails` ADD COLUMN `comment` MEDIUMTEXT NULL DEFAULT NULL  AFTER `grade` ;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `PRV_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `HOM_closingDate` , ADD COLUMN `VIS_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `PRV_paymentResponsibility` , ADD COLUMN `TAX_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `VIS_paymentResponsibility` , ADD COLUMN `RNT_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `TAX_paymentResponsibility` , ADD COLUMN `AIO_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `RNT_paymentResponsibility` , ADD COLUMN `TRG_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `AIO_paymentResponsibility` , ADD COLUMN `LAN_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `TRG_paymentResponsibility` , ADD COLUMN `RPT_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `LAN_paymentResponsibility` , ADD COLUMN `COL_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `RPT_paymentResponsibility` , ADD COLUMN `TAC_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `COL_paymentResponsibility` , ADD COLUMN `HOM_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `TAC_paymentResponsibility` , ADD COLUMN `SCH_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `HOM_paymentResponsibility` , ADD COLUMN `EXP_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `SCH_paymentResponsibility` , ADD COLUMN `ONG_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `EXP_paymentResponsibility` , ADD COLUMN `TEN_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `ONG_paymentResponsibility` , ADD COLUMN `MMG_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `TEN_paymentResponsibility` , ADD COLUMN `CAR_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `MMG_paymentResponsibility` , ADD COLUMN `SET_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `CAR_paymentResponsibility` , ADD COLUMN `WOP_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `SET_paymentResponsibility` , ADD COLUMN `REP_paymentResponsibility` VARCHAR(65) NULL DEFAULT NULL  AFTER `WOP_paymentResponsibility` ,
ADD COLUMN `HSM_agentCode` VARCHAR(25) NULL DEFAULT NULL  AFTER `REP_paymentResponsibility` , ADD COLUMN `HSM_agentName` VARCHAR(225) NULL DEFAULT NULL  AFTER `HSM_agentCode` , ADD COLUMN `HSM_brokerage` VARCHAR(100) NULL DEFAULT NULL  AFTER `HSM_agentName` , ADD COLUMN `HSM_agentPhone` VARCHAR(45) NULL DEFAULT NULL  AFTER `HSM_brokerage` , ADD COLUMN `HSM_agentEmail` VARCHAR(65) NULL DEFAULT NULL  AFTER `HSM_agentPhone` ,
ADD COLUMN `HSM_estimatedHSRReferral` DECIMAL(19,2) NULL DEFAULT NULL  ,ADD COLUMN `HSM_actualHSRReferral` DECIMAL(19,2) NULL DEFAULT NULL ,
ADD COLUMN `reminderServices` TEXT NULL DEFAULT NULL ;

update refmaster set description='Mortgage Assistance' where parameter='SERVICE' and corpid='HSRG' and code='MTS' and flex1='Relo';

update refmaster set description='Destination School Services' where parameter='SERVICE' and corpid='HSRG' and code='DSS' and flex1='Relo';

INSERT INTO parametercontrol(corpID,parameter,comments,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('UTSI','QuoteAcceptReason','Reason For Accepting Quote',10,true,now(),'KKunal',now(),'KKunal',1,1,'');

Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('UTSI', '', '', 10, 'QuoteAcceptReason','en',now(),'KKunal',now(),'KKunal'),
('UTSI', 'Employee Preference', 'Employee Preference', 10, 'QuoteAcceptReason','en',now(),'KKunal',now(),'KKunal'),
('UTSI', 'Lower Bid By Competitor', 'Lower Bid By Competitor', 10, 'QuoteAcceptReason','en',now(),'KKunal',now(),'KKunal');

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `quoteAcceptReason` VARCHAR(25) AFTER `DSS_vendorCode`;

ALTER TABLE `redsky`.`miscellaneous` MODIFY COLUMN `carrier` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

ALTER TABLE `redsky`.`userdatasecurity` 
ADD COLUMN `updatedBy` VARCHAR(85) NULL DEFAULT NULL  AFTER `userdatasecurity_id` , 
ADD COLUMN `updatedOn` DATETIME NULL DEFAULT NULL  AFTER `updatedBy` ;


ALTER TABLE `redsky`.`user_role` 
ADD COLUMN `updatedBy` VARCHAR(85) NULL DEFAULT NULL  AFTER `role_id` , 
ADD COLUMN `updatedOn` DATETIME NULL DEFAULT NULL  AFTER `updatedBy` ;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `emailPrintOption` VARCHAR(45) DEFAULT '' AFTER `coordinatorForRelo`;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `accountLineCostElement` VARCHAR(50) AFTER `revisionDiscount`,
ADD COLUMN `accountLineScostElementDescription` VARCHAR(50) AFTER `accountLineCostElement`;

ALTER TABLE `redsky`.`pageactionpermission` ADD COLUMN `corpId` VARCHAR(15) AFTER `ROLE`;

update pageactionpermission set corpId='UTSI';

ALTER TABLE `redsky`.`systemdefault` DROP COLUMN `defaultSortforFileCabinet`;

ALTER TABLE `redsky`.`systemdefault` DROP COLUMN `sortOrder`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `defaultSortforFileCabinet` VARCHAR(45) AFTER `pricePointStartDate`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `sortOrderForFileCabinet` VARCHAR(45) AFTER `defaultSortforFileCabinet`;

ALTER TABLE `redsky`.`accessinfo` ADD COLUMN `networkSynchedId` BIGINT(20) NULL AFTER `customerFileId` ;

ALTER TABLE `redsky`.`dspdetails` 
ADD COLUMN `CAT_comment` TEXT NULL DEFAULT NULL  AFTER `reminderServices` , 
ADD COLUMN `CLS_comment` TEXT NULL DEFAULT NULL  AFTER `CAT_comment` , 
ADD COLUMN `CHS_comment` TEXT NULL DEFAULT NULL  AFTER `CLS_comment` , 
ADD COLUMN `PDT_comment` TEXT NULL DEFAULT NULL  AFTER `CHS_comment` , 
ADD COLUMN `RCP_comment` TEXT NULL DEFAULT NULL  AFTER `PDT_comment` , 
ADD COLUMN `SPA_comment` TEXT NULL DEFAULT NULL  AFTER `RCP_comment` , 
ADD COLUMN `TCS_comment` TEXT NULL DEFAULT NULL  AFTER `SPA_comment` , 
ADD COLUMN `MTS_comment` TEXT NULL DEFAULT NULL  AFTER `TCS_comment` , 
ADD COLUMN `DSS_comment` TEXT NULL DEFAULT NULL  AFTER `MTS_comment` ;

ALTER TABLE `redsky`.`app_user` MODIFY COLUMN `signature` VARCHAR(600) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE `redsky`.`customersurveyfeedback` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `corpID` VARCHAR(30),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `serviceOrderId` BIGINT(20),
  `shipNumber` VARCHAR(15),
  `serviceName` VARCHAR(10),
  `customerRating` BIGINT(20) UNSIGNED,
  `feedBack` TEXT,
  PRIMARY KEY(`id`)
)
ENGINE = InnoDB;

ALTER TABLE `redsky`.`workticket` ADD COLUMN `hse` Bit(1) DEFAULT b'0';

ALTER TABLE `redsky`.`emailsetup` ADD COLUMN `module` VARCHAR(80) DEFAULT NULL AFTER `fileNumber`;

ALTER TABLE `redsky`.`emailsetup` MODIFY COLUMN `fileNumber` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '';

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','accountLineCostElement' ,'Update','dkumar',now(),'dkumar',now(),false,'','','CMM/DMM'),
('AccountLine','accountLineScostElementDescription' ,'Update','dkumar',now(),'dkumar',now(),false,'','','CMM/DMM'),
('AccountLine','accountLineCostElement' ,'Create','dkumar',now(),'dkumar',now(),false,'','','CMM/DMM'),
('AccountLine','accountLineScostElementDescription' ,'Create','dkumar',now(),'dkumar',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`inventorypath` ADD COLUMN `networkSynchedId` BIGINT(20) NULL AFTER `locationId` ;

ALTER TABLE `redsky`.`company` MODIFY COLUMN `id` BIGINT(20) NOT NULL, ADD COLUMN `landingPageWelcomeMsg` TEXT AFTER `surveyLinking`;

DROP TABLE IF EXISTS `redsky`.`sodashboard`;
CREATE TABLE  `redsky`.`sodashboard` (
  `id` bigint(20) NOT NULL DEFAULT '0',
  `shipnumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(29) CHARACTER SET utf8 DEFAULT '',
  `uvlstatus` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `survey` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `estweight` varchar(21) CHARACTER SET utf8 DEFAULT NULL,
  `estvolume` decimal(19,2) DEFAULT NULL,
  `ldpkticket` varchar(10) CHARACTER SET utf8 DEFAULT '',
  `loading` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `actualweight` varchar(21) CHARACTER SET utf8 DEFAULT NULL,
  `actVolume` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `driver` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `truck` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deliveryticket` varchar(10) CHARACTER SET utf8 DEFAULT '',
  `delivery` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `claims` varchar(400) DEFAULT '',
  `qc` datetime DEFAULT NULL,
  `corpID` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `estSurvey` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `estLoding` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `estDelivery` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `surveyDt` varchar(45) DEFAULT '',
  `deliveryDt` varchar(45) DEFAULT '',
  `loadingDt` varchar(45) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `redsky`.`company` ADD COLUMN `agentTDREmail` bit(1) DEFAULT b'1' AFTER `landingPageWelcomeMsg`;

