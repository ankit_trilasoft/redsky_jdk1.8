<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
  
<head>   
    <title><fmt:message key="defaultAccountLineDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='defaultAccountLineDetail.heading'/>"/>
    <style><%@ include file="/common/calenderStyle.css"%></style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
   
<script type="text/javascript">
$('input,select,textfield :visible').each(function (i) {
	$(this).attr('tabindex', i + 1);

	}); 
var links = document.getElementsByTagName( 'a' );

for( var i = 0, j =  links.length; i < j; i++ ) {
    links[i].setAttribute( 'tabindex', '-1' );
}
</script>
<script type="text/javascript">
var formatQuantity1 = 0;
      var formatRate1 = 0;
      var formatAmount1 = 0;
      var formatbasis1 = '';
      
      
    formatQuantity =   function calcQuan(inDatum)
    {
    	formatQuantity1 = inDatum;
    	return  (inDatum);
	}

    formatRate =   function calcRate(inDatum){
    	formatRate1 = inDatum;
    	return  (inDatum);
    }
   
    formatAmount =   function calcAmount(inDatum)
    {
       if(formatbasis1=='cwt' || formatbasis1=='%age' ){
          return (formatRate1*formatQuantity1)/100;
       }
      else if(formatbasis1=='per 1000'){
          return (formatRate1*formatQuantity1)/1000;
       }
       else
       {
        return (formatRate1*formatQuantity1);
       }
      
    }
     formatbasis= function basis(inDatum)
    {
      formatbasis1=inDatum;
      return  (inDatum);  
    }
    
  function checkJobType(jobRequired){
  var jobTypeRequired = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.jobType'].value
  var contract = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value
  
  if(jobTypeRequired =="")
  {
  alert("Job Type is required field");
  return false
  }
  else if(contract==""){
  alert("Contract is required field");
  return false
  }
  else{
	  submit_form_Validate();
  return true;
  }
  
  }
  
  var form_submitted = false;
  function submit_form_Validate()
  {
  	
    if (form_submitted)
    {
      alert ("Your form has already been submitted. Please wait...");
      return false;
    }
    else
    {
      form_submitted = true;
      return true;
    }
  }
  	
    
    
function calculateTotal(){
	var quant = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.quantity'].value*1;
	var rat = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.rate'].value*1;
	var amou=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.amount'].value;
	var basi=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value;
		if( !(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.amount'].value == '') ){
			var valu = (quant*rat)
			 if(basi=='cwt' || basi=='%age'){
            var valu = (quant*rat)/100;
              }
             else if(basi=='per 1000'){
            var valu = (quant*rat)/1000;
              }
			var valu1 = Math.round(valu*100)/100;
			
			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.amount'].value = valu1;
    		
			}
}
  
    
    </script>
    
    
    
<SCRIPT LANGUAGE="JavaScript">
function checkVendorName(){
	var vendorId = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorCode'].value;
	if(vendorId == ''){
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorName'].value = '';
	}
    if(vendorId != ''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    } 
}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2(){
         if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
           		if(res.size() >= 2){ 
	           		if(res[2] == 'Approved'){
	           			 document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorName'].value = res[1];
	           		}else{
	           			alert("Vendor code is not approved" ); 
					    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorName'].value="";
			    		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorCode'].value="";
 						document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorCode'].select();
	           		}			
	           }else{
	                alert("Vendor code not valid");    
				    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorName'].value="";
				    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorCode'].value="";
	 				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorCode'].select();
			   }
      	}
}
 
 function getCommodity(){
    var commodityType = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.jobType'].value;
    var url;
    if(commodityType=='STO'|| commodityType=='STF'|| commodityType=='STL' || commodityType=='TPS' ){
      url="getCommodity.html?ajax=1&decorator=simple&popup=true&commodityParameter=" + encodeURI('COMMODITS');
     }
     else
     {
       url="getCommodity.html?ajax=1&decorator=simple&popup=true&commodityParameter=" + encodeURI('COMMODIT');
     }
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse100;
     http2.send(null);
}

function handleHttpResponse100()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                res = results.replace("{",'');
                res = res.replace("}",'');
                res = res.split(",");
                targetElement = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.commodity'];
				targetElement.length = res.length;
				var job = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.jobType'].value;
 				for(i=0;i<res.length;i++)
					{
					  if(res[i] == ''){
					  document.forms['defaultAccountLineForm'].elements['defaultAccountLine.commodity'].options[i].text = '';
					  document.forms['defaultAccountLineForm'].elements['defaultAccountLine.commodity'].options[i].value = '';
					}
					else
					{
					
					   stateVal = res[i].replace("=",'+');
					   stateVal=stateVal.split("+");
					   document.forms['defaultAccountLineForm'].elements['defaultAccountLine.commodity'].options[i].text=stateVal[1];
					   document.forms['defaultAccountLineForm'].elements['defaultAccountLine.commodity'].options[i].value=stateVal[0].trim();
					   if(job == "")
					   {
					   	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.commodity'].options[0].selected=true;
					  	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.commodity'].options[0].selected=true;
					   }
					}
             
             }
           
       }
   }             

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http3 = getHTTPObject();
    var http5 = getHTTPObject();
    var httpCE = getHTTPObject();
    var http5222 = getHTTPObject();
    var http52222 = getHTTPObject();
 
    
</script>




<script>
function vendorCodeForActgCode(){ 
   var vendorId = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorCode'].value;
	if(vendorId == ''){
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorName'].value = '';
	}
    if(vendorId != ''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    } 
} 



function getContract() {
	var custJobType = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.jobType'].value;
	//document.forms['defaultAccountLineForm'].elements['custJobType'].value= custJobType;
	var contractBillCode = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value;
	//document.forms['defaultAccountLineForm'].elements['contractBillCode'].value= contractBillCode;
	var custCreatedOn = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.createdOn'].value;
    var id=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.id'].value;
	var url="findContracOnJobBasis.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType)+"&contractBillCode="+encodeURI(contractBillCode)+"&custCreatedOn="+encodeURI(custCreatedOn)+"&id="+encodeURI(id);
     http5.open("GET", url, true);
     http5.onreadystatechange = handleHttpResponse3;
     http5.send(null);
	
}

function handleHttpResponse3(){
		if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
                targetElementVal=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value;
                targetElement = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].options[i].text = '';
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].options[i].value = '';
					}else{
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].options[i].text =res[i];
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].options[i].value =res[i];
					}
				}
				
				if(document.forms['defaultAccountLineForm'].elements['formStatus'].value == '1'){
				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value=targetElementVal;
				}
				else{
				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value='${defaultAccountLine.contract}';
				}
         }
}

function findBillToName(){
    var billToCode = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value;
    if(billToCode==''){
    	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToName'].value="";
	}
    if(billToCode!=''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse33;
     http2.send(null);
    }
}

function handleHttpResponse33(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim(); 
                var res = results.split("#"); 
           		if(res.size() >= 2){ 
	           		if(res[2] == 'Approved'){
	           			if(res[3]== '' || res[3] == 'null'){
	           				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToName'].value = res[1];
 							document.forms['defaultAccountLineForm'].elements['defaultAccountLine.customerEmployer'].value = res[1];
 							document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToName'].select();
		           		}else{
		           			//document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value=
						 	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToName'].value=res[1];
						 	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].select();
		           		}	
	           		}else{
	           			alert("Bill To code is not approved" ); 
					    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value="";
					 	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToName'].value="";
					 	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].select();
	           		}
	           }else{
	            alert("Bill To code not valid");    
				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value="";
				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToName'].value="";
				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].select();
			   }
           }
}

function openPopWindow(){
	javascript:openWindow('partnersPopup.html?partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=defaultAccountLine.billToName&fld_code=defaultAccountLine.billToCode');
}

function checkCostElementTemplate() { 
	<c:if test="${accountInterface=='Y'}">
    <c:if test="${costElementFlag}"> 
	var chargeCode = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value;
	var jobType = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.jobType'].value;
	var routing = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.route'].value;
	var contract = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value;
	var compDivis=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.companyDivision'].value;
	var url="findCostElementTemplate.html?ajax=1&decorator=simple&popup=true&chargeCode="+encodeURIComponent(chargeCode)+"&jobType="+encodeURIComponent(jobType)+"&routing="+encodeURIComponent(routing)+"&contract="+encodeURIComponent(contract)+"&soCompanyDivision="+encodeURIComponent(compDivis); 
	httpCE.open("GET", url, true);
	httpCE.onreadystatechange =handleHttpResponseCostElement;
	httpCE.send(null); 
     </c:if> </c:if>
}
function handleHttpResponseCostElement(){ 
	if (httpCE.readyState == 4){
	var results = httpCE.responseText
     results = results.trim();  
     results = results.replace('[','');
     results=results.replace(']',''); 
     var res = results.split("~"); 
     document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = res[0];
	 document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = res[1]; 
	 var ctype= document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value;
	 ctype=ctype.trim();
	if(ctype==''){
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value = res[2];
	}
	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.description'].value = res[3];
 } }
 
function checkChargeCode(){
	var chargeCodeValidationVal= document.forms['defaultAccountLineForm'].elements['chargeCodeValidationVal'].value
	if(chargeCodeValidationVal=='' || chargeCodeValidationVal==null){
    progressBarAutoSave('1');
    var category = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value;
	var chargeCode= document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value;
	var contract = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value;
	var compDivis=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.companyDivision'].value;
	var jobType=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.jobType'].value;
	var routing=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.route'].value;
	if(chargeCode!=''){
	if(category=='Internal Cost' && compDivis==''){
	alert("Please enter company division first.");
	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value='';
	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = '';
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = '';
	progressBarAutoSave('0');
    return false;
	
	}
	else if(category=='Internal Cost'){
//	 contract="Internal Cost Management";
//	 var url="defaultTemplateChargeCode.html?decorator=simple&popup=true&contract="+contract+"&chargeCode="+chargeCode; 
     var url="checkInternalCostChargeCode.html?ajax=1&decorator=simple&popup=true&accountCompanyDivision="+encodeURIComponent(compDivis)+"&chargeCode="+encodeURIComponent(chargeCode)+"&jobType="+encodeURIComponent(jobType)+"&routing="+encodeURIComponent(routing)+"&soCompanyDivision="+encodeURIComponent(compDivis); 
     http5.open("GET", url, true);
     http5.onreadystatechange = function(){ handleHttpResponse112('Internal');};
     http5.send(null);
	}else{ 
	if(contract=='' || contract==' '){
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value='';
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = '';
        document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = '';
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].focus();
		alert("There is no pricing contract: Please select.");
		progressBarAutoSave('0');
		return false;
	} else {
		
	var url="defaultTemplateChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+contract+"&chargeCode="+chargeCode+"&jobType="+encodeURIComponent(jobType)+"&routing="+encodeURIComponent(routing)+"&soCompanyDivision="+encodeURIComponent(compDivis); 
    http5.open("GET", url, true);
    http5.onreadystatechange =function(){ handleHttpResponse112('No Internal');};
    http5.send(null);
    }
    }
}else{
	 document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value = "";
     document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = "";
     document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = "";
     document.forms['defaultAccountLineForm'].elements['defaultAccountLine.description'].value = "";
}
	progressBarAutoSave('0');
	}
}
function handleHttpResponse112(temp){
		if (http5.readyState == 4){
                var results = http5.responseText
                
                results = results.trim(); 
                results = results.replace('[','');
                results=results.replace(']',''); 
                var res = results.split("#"); 
                if(results.length>6){
	                document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = res[1];
	                document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = res[2];
	                document.forms['defaultAccountLineForm'].elements['defaultAccountLine.description'].value = res[4];
                //alert("good charge code" );
		        	<c:if test="${costElementFlag}">
		        	try{
		                var ctype= document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value;
		          		 ctype=ctype.trim();
		          		if(ctype==''){
		          			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value = res[12];
		          		}
		        	}catch(e){
			        	}
		        	</c:if>
	                
                }else{
                    if(temp=='Internal'){
                    document.getElementById("ChargeNameDivId").style.display = "none";
                    alert("Charge code does not exist according the Company Division, Please select another" );
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].focus();
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value = "";
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = "";
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = "";
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.description'].value = "";
                    
                    }
                    else{
                    document.getElementById("ChargeNameDivId").style.display = "none";
                    alert("Charge code does not exist according the contract, Please select another" );
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].focus();
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value = "";
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = "";
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = "";
                    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.description'].value = "";
                    document.getElementById("ChargeNameDivId").style.display = "none";
                    }
				}
				progressBarAutoSave('0');
         }
}


function chk(){
    var category = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value;
	var contract = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value;
	var companyDiv=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.companyDivision'].value;
	if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'] == undefined && document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'] == undefined){
		recGlVal = 'secondDescription';
		payGlVal = 'thirdDescription';
	}else{
		recGlVal = 'defaultAccountLine.recGl';
		payGlVal = 'defaultAccountLine.payGl';
	}	
 if(category=='Internal Cost' && companyDiv==''){
    alert("Please enter company division first.");
    return false;
    }
	else if(category=='Internal Cost'){
	//  contract="Internal Cost Management";
	 javascript:openWindow('defaultChargesByComDiv.html?comDiv='+companyDiv+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=defaultAccountLine.description&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code=defaultAccountLine.chargeCode');
	 document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].select();
	} else {
	if(contract=='' || contract==' '){
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value='';
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = '';
        document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = '';
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].focus();
		alert("There is no pricing contract: Please select.");
		return false;
	}else{
		javascript:openWindow('defaultTemplateChargess.html?contract='+contract+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription='+recGlVal+'&fld_description=defaultAccountLine.description&fld_thirdDescription='+payGlVal+'&fld_code=defaultAccountLine.chargeCode');
	 document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].select();
	}
	} 
}

function valueChk(){
	var contract = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value;
	if(contract=='' || contract==' '){
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value='';
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = '';
        document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = '';
        return false;
	}
	var chargeCode= document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value;
	if(chargeCode !=''){
		checkChargeCode();
	}	
}
function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36) ; 
	}
	
function expenseOld() {
       var estimate=0;
       var estimateSell=0;
       var estimateround=0;
       var estimateSellRound=0;
       var estimateOther=0;
       var passPercentage=0;
       var passPercentageround=0;
       var quantity = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.quantity'].value;
       var quantityabs=Math.abs(quantity);
       var rate = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.rate'].value;
       var sellRate = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value;
   	   var estimatePassPercentage = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].value;
   	   estimate = (quantity*rate);
  	   estimateSell=(quantity*sellRate) 
       if( document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="cwt" || document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="%age")
       {
      	   estimate = estimate/100;
      	   estimateSell=estimateSell/100; 	  	
       }
       if( document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="per 1000")
       {
      	   estimate = estimate/1000;
      	   estimateSell=estimateSell/1000; 	  	
       } 
      	    estimateOther = ((estimate)*estimatePassPercentage/100);
      	    estimateround=Math.round(estimate*100)/100;
      	    estimateOtherRev=Math.round(estimateOther*100)/100;
      	    estimateSellRound=Math.round(estimateSell*100)/100;
      	    passPercentage=((estimateSell/estimate)*100);
      	    passPercentageround=Math.round(passPercentage);
  		    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.amount'].value = estimateround;
  		    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estimatedRevenue'].value = estimateSellRound;
  		    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].value=passPercentageround;		
 if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.amount'].value == '' || document.forms['defaultAccountLineForm'].elements['defaultAccountLine.amount'].value == 0){
  	   	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].value='';
  	   }
}

 function calculateRevenue() {
   	    var estimate=0;
   	    var sellRateround=0;
        var estimateround=0;
        var estimateRevenue=0;
        var estimateRevenueRound=0;
       var	estimatepasspercentage = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].value;
       var sellRate = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value;
       var quantity = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.quantity'].value;
       var quantityabs=Math.abs(quantity);
       var rate = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.rate'].value;
   	   estimate = (quantity*rate); 
   	   	if( document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="cwt" || document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="%age"){
       		estimate = estimate/100; 	  	
       	}  
	   
       	if( document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="per 1000"){
       		estimate = estimate/1000; 	  	
       	}
       	estimateRevenue=((estimate)*estimatepasspercentage/100)
       	estimateRevenueRound=Math.round(estimateRevenue*100)/100;
  	    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estimatedRevenue'].value=estimateRevenueRound;   
	     if( document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="cwt" || document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="%age"){
       	  if(quantity!=0){
       	  sellRate=(estimateRevenue/quantity)*100;
       	  sellRateround=Math.round(sellRate);
       	  }else{
       	  sellRateround=0;
       	  }
       	  document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value=sellRateround;	  	
       	}
	    else if( document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="per 1000"){
       	  if(quantity!=0){
       	  sellRate=(estimateRevenue/quantity)*1000;
       	  sellRateround=Math.round(sellRate);
       	  }else{
       	  sellRateround=0;
       	   }
       	  document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value=sellRateround;	  	
       	} else if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value!="cwt" || document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value!="%age"||document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value!="per 1000")
        {
         if(quantity!=0){
          sellRate=(estimateRevenue/quantity);
       	  sellRateround=Math.round(sellRate);
       	  }else{
       	  sellRateround=0;
       	   }
          document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value=sellRateround;	  	
	    }
 }
 	function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
	} 
	
function mockupForRevenue() {
   	   var estimate=0;
       var estimateround=0;
       var sellRateround=0;
       var estimatepasspercentageRound=0;
       var	estimatepasspercentage = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].value;
       var sellRate = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value;
       var quantity = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.quantity'].value;
       var quantityabs=Math.abs(quantity);
       var rate = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.rate'].value;
   	   var estimateRevenue = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estimatedRevenue'].value; 
   	   estimate = (quantity*rate);
   	   	if( document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="cwt" || document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="%age"){
       		  estimate = estimate/100;
       		  if(quantity!=0){
       		  sellRate=(estimateRevenue/quantity)*100;
       		  sellRateround=Math.round(sellRate);
       		  }else{
       		  sellRateround=0;
       		  }
       		  document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value=sellRateround;	  	
       	}
       	if( document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value=="per 1000"){
       		  estimate = estimate/1000;
       		  if(quantity!=0){
       		  sellRate=(estimateRevenue/quantity)*1000;
       		  sellRateround=Math.round(sellRate);
       		  }
       		  else{
       		  sellRateround=0;
       		  }
       		  document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value=sellRateround;	  	
       	}
       	else if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value!="cwt" || document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value!="%age"||document.forms['defaultAccountLineForm'].elements['defaultAccountLine.basis'].value!="per 1000")
  	    {
  	    if(quantity!=0){
  	        sellRate=estimateRevenue/quantity;
       	    sellRateround=Math.round(sellRate);
       	    }
       	   else{
       		  sellRateround=0;
       		  }
  	        document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value=sellRateround;
  	    }
  	    if(estimate!=0){
  	   estimatepasspercentage = (estimateRevenue/estimate)*100;
  	   estimatepasspercentageRound=Math.round(estimatepasspercentage);
  	   }else{
  	   estimatepasspercentageRound=0;
  	        }
  	   if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.amount'].value == '' || document.forms['defaultAccountLineForm'].elements['defaultAccountLine.amount'].value == 0){
  	   	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].value='';
  	   }else{
  	   	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].value=estimatepasspercentageRound;
  	   }
  	  
  	 } 
 function disableAmountRevenue() { 
  var rev = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value; 
     if(rev == "Internal Cost"){
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].readOnly=true;
     document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].className="input-textUpper" ;
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value=0;
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estimatedRevenue'].readOnly=true;
     document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estimatedRevenue'].className="input-textUpper" ;
     document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estimatedRevenue'].value=0;
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].readOnly=true;
     document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].className="input-textUpper" ;
     document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].value=0;
     
    }else{
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].readOnly=false;  
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estimatedRevenue'].readOnly=false; 
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].readOnly=false;
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].className="input-text" ;  
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estimatedRevenue'].className="input-text" ;
    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.markUp'].className="input-text" ;
     
    }
   }
   
   function changeStatus(){
	document.forms['defaultAccountLineForm'].elements['formStatus'].value = '1';
}  
	
function resetChargeCode(){
var category = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value;
if(category=='Internal Cost'){
       document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value = "";
       document.forms['defaultAccountLineForm'].elements['defaultAccountLine.recGl'].value = "";
       document.forms['defaultAccountLineForm'].elements['defaultAccountLine.payGl'].value = "";
       document.forms['defaultAccountLineForm'].elements['defaultAccountLine.description'].value = "";
       }
}
</script> 	
<script type="text/javascript">
var http54=getHTTPObject();
function resetContract(){
var custJobType = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.jobType'].value;
	var contractBillCode = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value;
	var custCreatedOn = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.createdOn'].value;
	var url="findContracOnJobBasis.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType)+"&contractBillCode="+encodeURI(contractBillCode)+"&custCreatedOn="+encodeURI(custCreatedOn);
     http54.open("GET", url, true);
     http54.onreadystatechange = handleHttpResponse2222;
     http54.send(null);
	
}

function handleHttpResponse2222(){
		if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
                
                targetElement = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].options[i].text = '';
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].options[i].value = '';
					}else{
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].options[i].text =res[i];
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].options[i].value =res[i];
					}
				}
				
				
				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value='${defaultAccountLine.contract}';
		 }
}

function getVatPercentEst(){ 
	<c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){ 
        if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].value=='${entry.key}') {
        document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatPercent'].value='${entry.value}'; 
        calculateVatAmtEst();
        relo="yes"; 
       }  }
    </c:forEach>
      if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].value==''){
      document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatPercent'].value=0;
      calculateVatAmtEst();
      }  
      </c:if> 
	}
function calculateVatAmtEst(){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	var estVatAmt=0;
	if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatPercent'].value!=''){
	var estVatPercent=eval(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatPercent'].value);
	var estimateRevenueAmount= eval(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estimatedRevenue'].value);
	<c:if test="${contractType}">
	  estimateRevenueAmount= eval(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.sellRate'].value);
	</c:if>
	estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
	estVatAmt=Math.round(estVatAmt*100)/100; 
	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatAmt'].value=estVatAmt;
	}
    </c:if>
}
function checkVatForReadonlyFieldsEst(var1){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	       var var2= document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].value;	      
	       if(var1=='first' && var2==''){   
	           document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatPercent'].readOnly=true;
	           document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatPercent'].className='input-textUpper';
	           }else{ 
	           document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatPercent'].readOnly=false;
	           document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatPercent'].className='input-text'; 
	           }	       
	     </c:if>  
	}
function checkFloatEstVat(temp)  { 
	    var check='';  
	    var i; 
		var s = temp.value;
		var fieldName = temp.name;  
		if(temp.value>100){
		alert("You cannot enter more than 100% VAT ")
		document.forms['defaultAccountLineForm'].elements[fieldName].select();
	    document.forms['defaultAccountLineForm'].elements[fieldName].value='0.00';
	    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatAmt'].value=''; 
		return false;
		}  
		var count = 0;
		var countArth = 0;
	    for (i = 0; i < s.length; i++) {   
	        var c = s.charAt(i); 
	        if(c == '.')  {
	        	count = count+1
	        }
	        if(c == '-')    {
	        	countArth = countArth+1
	        }
	        if(((i!=0)&&(c=='-')) || ((s=='-')&&(s.length==1)) || ((s=='.')&&(s.length==1))) 	{
	       	  alert("Invalid data in vat%." ); 
	          document.forms['defaultAccountLineForm'].elements[fieldName].select();
	          document.forms['defaultAccountLineForm'].elements[fieldName].value='';
	          document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatAmt'].value=''; 
	       	  return false;
	       	} 
	        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
	        	check='Invalid'; 
	        }  } 
	    if(check=='Invalid'){ 
	    alert("Invalid data in vat%." ); 
	    document.forms['defaultAccountLineForm'].elements[fieldName].select();
	    document.forms['defaultAccountLineForm'].elements[fieldName].value='';
	    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatAmt'].value=''; 
	    return false;
	    }  else{
	    s=Math.round(s*100)/100;
	    var value=""+s;
	    if(value.indexOf(".") == -1){
	    value=value+".00";
	    } 
	    if((value.indexOf(".")+3 != value.length)){
	    value=value+"0";
	    }
	    document.forms['defaultAccountLineForm'].elements[fieldName].value=value;
	    calculateVatAmtEst()
	    return true;
	}  }
	
function getExpVatPercentEst(){ 
	<c:if test="${systemDefaultVatCalculation=='true'}">
	 var relo="" 
     <c:forEach var="entry" items="${payVatPercentList}">
        if(relo==""){ 
        if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].value=='${entry.key}') {
        document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatPercent'].value='${entry.value}'; 
         
        relo="yes"; 
       }  }
    </c:forEach>
      if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].value==''){
      document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatPercent'].value=0;
       
      }  
      </c:if> 
	}
	
	 
/* function autoCompleterAjaxCallChargeCode(){
	var ChargeName = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value;
	var contract = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value;
	contract=contract.trim();
	var data="";
	if(contract!=""){
	if(ChargeName.length>=3){
	data = 'ChargeCodeAutocompleteAjax.html?ajax=1&searchName='+ChargeName+'&contract='+contract+'&decorator=simple&popup=true';
		$( "#chargeCode").autocomplete({				 
		      source: data
		    });
	}
	else{
		$( "#chargeCode").autocomplete({				 
		      source: null
		    });
	}
	}
	else{
		alert("There is no pricing contract: Please select.");
		document.getElementById("contract"+id).focus();
	}
}
 */


function autoCompleterAjaxCallChargeCode(divid){
	   
	 <configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
	document.getElementById('chargeCodeValidationVal').value='chargeVal';
	var ChargeName = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value;
	var contract = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].value;
	contract=contract.trim();
	var category = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.categories'].value;
	var companyDiv="";
	companyDiv=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.companyDivision'].value;
	var pageCondition="";
	
 	if(category=='Internal Cost'){
 		pageCondition='TemplateInternalCategory';
 	}
 	if(category!='Internal Cost'){
 		pageCondition='TemplateNonInternalCategory';
 	}
	var idval="chargeCode";
	var id="";
	var idCondition='';
	 if(category=='Internal Cost' && companyDiv==''){
		    alert("Please enter company division first.");
		    document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value='';
		    return false;
		    }
	if(contract!=""){
		if(ChargeName.length>=3){
            $.get("ChargeCodeAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
            	searchName:ChargeName,
            	contract: contract,
            	chargeCodeId:idval,
            	autocompleteDivId: divid,
            	id:id,
            	idCondition:idCondition,
            	categoryCode:category,
            	pageCondition:pageCondition,
            	searchcompanyDivision:companyDiv
            }, function(idval) {
                document.getElementById(divid).style.display = "block";
                $("#" + divid).html(idval)
            })
        } else {
            document.getElementById(divid).style.display = "none"
        }
	}
	else{
		alert("There is no pricing contract: Please select.");
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value='';
		document.forms['defaultAccountLineForm'].elements['defaultAccountLine.contract'].focus();
	}
	</configByCorp:fieldVisibility>	
}


 function copyChargeDetails(chargecode,chargecodeId,autocompleteDivId,id,idCondition){
	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value=chargecode;
	document.getElementById(autocompleteDivId).style.display = "none";
	document.forms['defaultAccountLineForm'].elements['chargeCodeValidationVal'].value='';
	document.forms['defaultAccountLineForm'].elements['chargeCodeValidationMessage'].value='NothingData';
	checkChargeCode();
	
}

 function closeMyChargeDiv(autocompleteDivId,chargecode,chargecodeId,id,idCondition){
	document.getElementById(autocompleteDivId).style.display = "none";
	 if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value==''){
	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.description'].value="";	
	}
	 document.forms['defaultAccountLineForm'].elements['chargeCodeValidationVal'].value='';
	 document.forms['defaultAccountLineForm'].elements['chargeCodeValidationMessage'].value='NothingData';
		checkChargeCode();
	}
	
 function closeMyChargeDiv2(chargecodeId,id){
	 if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.chargeCode'].value==''){
			document.forms['defaultAccountLineForm'].elements['defaultAccountLine.description'].value="";	
			}
	 document.forms['defaultAccountLineForm'].elements['chargeCodeValidationVal'].value='';
	 document.forms['defaultAccountLineForm'].elements['chargeCodeValidationMessage'].value='NothingData';
	}
 
 function enablePreviousFunction(checkValue){
	 		document.getElementById('ChargeNameDivId').style.display = "none";
	 	if(document.forms['defaultAccountLineForm'].elements['chargeCodeValidationMessage'].value!='NothingData'){	 
	 		document.forms['defaultAccountLineForm'].elements['chargeCodeValidationVal'].value='';
		 	checkChargeCode();
		// document.forms['defaultAccountLineForm'].elements['chargeCodeValidationVal'].value='AutoValue';
		// document.forms['defaultAccountLineForm'].elements['chargeCodeValidationMessage'].value=checkValue;
	 }
}	
 var httppayVatBilling = gethttppayVatBilling();
 function gethttppayVatBilling() {
 var xmlhttp;
 if(window.XMLHttpRequest) {
     xmlhttp = new XMLHttpRequest();
 }
 else if (window.ActiveXObject)  {
     xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
     if (!xmlhttp)  {
         xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
     }   } 
 return xmlhttp;
 }
 
 var httprecVatBilling = gethttprecVatBilling();
 function gethttprecVatBilling() {
 var xmlhttp;
 if(window.XMLHttpRequest) {
     xmlhttp = new XMLHttpRequest();
 }
 else if (window.ActiveXObject)  {
     xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
     if (!xmlhttp)  {
         xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
     }   } 
 return xmlhttp;
 }
 function flex1RecVatCheck(defaultVatBillingGroup,currentDefaultVatValue) { 
		<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"> 
		var companyDivision=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.companyDivision'].value;
		var vatBillingGroupFlexValue="";
		var defaultVatBillingGroupDesc="";
		var relo1="" 
		<c:forEach var="entry" items="${vatBillingGroups}">
		  if(relo1==""){ 
	      if(defaultVatBillingGroup=='${entry.key}') {
	    	  defaultVatBillingGroupDesc='${entry.value}';
	    	  relo1="yes";
	      }} 
		</c:forEach>
		var relo=""
			<c:forEach var="entry" items="${flex1RecCodeList}">
		  if(relo==""){ 
	        if(defaultVatBillingGroupDesc+"~"+companyDivision=='${entry.key}' || defaultVatBillingGroupDesc+"~NODATA"=='${entry.key}') {
	        	vatBillingGroupFlexValue='${entry.value}';
	           relo="yes";
	        }} 
		</c:forEach> 
		vatBillingGroupFlexValue = vatBillingGroupFlexValue+",";
		var currentDefaultVatValueTest =  currentDefaultVatValue+",";
		if(currentDefaultVatValue !="" && vatBillingGroupFlexValue.includes(currentDefaultVatValueTest)){
			 return true; 
		}else
		{
			return false;
			}
		</configByCorp:fieldVisibility >  
		  }
	function flex2PayVatCheck(defaultVatBillingGroup,currentDefaultVatValue) { 
		<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
		var companyDivision=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.companyDivision'].value;
		var vatBillingGroupFlexValue="";
		var defaultVatBillingGroupDesc="";
		var relo1="" 
		<c:forEach var="entry" items="${vatBillingGroups}">
		  if(relo1==""){ 
	      if(defaultVatBillingGroup=='${entry.key}') {
	    	  defaultVatBillingGroupDesc='${entry.value}';
	    	  relo1="yes";
	      }} 
		</c:forEach>
		var relo=""
			<c:forEach var="entry" items="${flex2PayCodeList}">
		  if(relo==""){ 
	        if(defaultVatBillingGroupDesc+"~"+companyDivision=='${entry.key}' || defaultVatBillingGroupDesc+"~NODATA"=='${entry.key}') {
	        	vatBillingGroupFlexValue='${entry.value}';
	           relo="yes";
	        }} 
		</c:forEach> 
		vatBillingGroupFlexValue = vatBillingGroupFlexValue+",";
		var currentDefaultVatValueTest =  currentDefaultVatValue+",";
		if(currentDefaultVatValue !="" && vatBillingGroupFlexValue.includes(currentDefaultVatValueTest)){
			 return true; 
		}else
		{
			return false;
			}
		</configByCorp:fieldVisibility >  
		  } 
		  
	function findDefaultVatFromPartnerForBill(){
		<c:if test="${systemDefaultVatCalculation=='true'}">

		var bCode = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value;
		if(bCode!='') {
		    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		    http5222.open("GET", url, true); 
		    http5222.onreadystatechange = handleHttp92222; 
		    http5222.send(null);	     
	     }</c:if>}
	function handleHttp92222(){
	    if (http5222.readyState == 4){    	
	    	var results = http5222.responseText
	    	results = results.trim();
	    	if(results!=''){
	    		var	res = results.split("~");
		    	var vatBillingGroupCheck=true;
		        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
		        vatBillingGroupCheck=false;
		        </configByCorp:fieldVisibility>
		        if(vatBillingGroupCheck){ 
			    	if(res[8]!='NA'){
				    	if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].value==''){
						 	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].value = res[8];
				    	}  }
		        }else{
		       	 var recVatDescr = '${defaultAccountLine.estVatDescr}'; 
			        	 if(res[8]!='NA' && res[12]!='NODATA'){
			        		 var chkVatBillingGroup=false;
			        		 chkVatBillingGroup = flex1RecVatCheck(res[12],res[8]); 
	                      if(chkVatBillingGroup){ 
	                     	  if(recVatDescr != res[8]){
	                       		 var agree = confirm("Updating Billtocode code will also change the Receivable VAT code. Do you wish to continue? ");
	             					if(agree){
	             						document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].value = res[8];
	             						getVatPercentEst();
	             					}
	                            }
	                      } 
	                       
	                       
			        	 }
			        
		        	
		        }
			    	}    	} }	





	function findDefaultVatFromPartnerForVendor(){
		<c:if test="${systemDefaultVatCalculation=='true'}">

		var bCode = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorCode'].value;
		if(bCode!='') {
		    var url="findDefaultValuesFromPartnerAjax.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		    http52222.open("GET", url, true); 
		    http52222.onreadystatechange = handleHttpStorageVat92222; 
		    http52222.send(null);	     
	     }</c:if>}
	function handleHttpStorageVat92222(){
	    if (http52222.readyState == 4){    	
	    	var results = http52222.responseText
	    	results = results.trim();
	    	if(results!=''){
	    		var	res = results.split("~");
		    	var vatBillingGroupCheck=true;
		        <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
		        vatBillingGroupCheck=false;
		        </configByCorp:fieldVisibility>
		        if(vatBillingGroupCheck){ 
			    	if(res[8]!='NA'){
				    	if(document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].value==''){
						 	document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].value = res[8];
				    	}  }
		        }else{
		       	 var payVatDescr = '${defaultAccountLine.estExpVatDescr}'; 
			        	 if(res[8]!='NA' && res[12]!='NODATA'){
			        		 var chkVatBillingGroup=false;
			        		 chkVatBillingGroup = flex2PayVatCheck(res[12],res[8]); 
	                      if(chkVatBillingGroup){ 
	                     	if(payVatDescr != res[8]){
	                       		 var agree = confirm("Updating Vendor code will also change the Payable VAT code. Do you wish to continue?");
	             					if(agree){
	             						document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].value = res[8];
	             						getExpVatPercentEst();
	             					}
	                            }
	                      } 
	                       
			        	 }
			          
		        	
		        }
			    	}    	} }
 //function to findoutRecVatDec  for billToCode
 function defaultAccountRecVATDesc()
 {
	 <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	     var partnerCode = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToCode'].value;
	     var defaultAccountLineid = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.id'].value;
	 	 var companyDivision=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.companyDivision'].value;
	 	 var estVatDescr ='${defaultAccountLine.estVatDescr}';
 		 var url="recVatDefaultAccountLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&companyDivision="+encodeURI(companyDivision)+"&id="+encodeURI(defaultAccountLineid)+"&estVatDescr="+encodeURI(estVatDescr);
 		 httprecVatBilling.open("GET", url, true);
		 httprecVatBilling.onreadystatechange = handleHttpResponserecVatBilling;
		 httprecVatBilling.send(null);
		 </configByCorp:fieldVisibility >
 }
 
 function handleHttpResponserecVatBilling() { 
	 if (httprecVatBilling.readyState == 4)
     {
       var results = httprecVatBilling.responseText
         results = results.trim();
       results = results.replace('[','');
       results=results.replace(']',''); 
       res = results.split("@");
       var  targetElement = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'];
			targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
 					console.log(res);
					if(res[i] == ''){
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].options[i].text = '';
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].options[i].value = stateVal[0];
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].options[i].text = stateVal[1];
					}
					}
 				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estVatDescr'].value = '${defaultAccountLine.estVatDescr}';
 				findDefaultVatFromPartnerForBill();
     }
}

 
//function to findoutPayVatDec  for vendorCode
 function defaultAccountPayVatWithVatGroup()
 {      
	 <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	    var partnerCode = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.vendorCode'].value;
	    var defaultAccountLineid = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.id'].value;
		var companyDivision=document.forms['defaultAccountLineForm'].elements['defaultAccountLine.companyDivision'].value;
		   var estExpVatDescr ='${defaultAccountLine.estExpVatDescr}';
 		var url="payVatDefaultAccountLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&companyDivision="+encodeURI(companyDivision)+"&id="+encodeURI(defaultAccountLineid)+"&estExpVatDescr="+encodeURI(estExpVatDescr);
 		httppayVatBilling.open("GET", url, true);
 		httppayVatBilling.onreadystatechange = handleHttpResponsepayVatBilling;
 		httppayVatBilling.send(null);
 		</configByCorp:fieldVisibility >
	
 }
 
 function handleHttpResponsepayVatBilling() { 
	 if (httppayVatBilling.readyState == 4)
     {   
      var results = httppayVatBilling.responseText
       results = results.trim();
       results = results.replace('[','');
       results=results.replace(']',''); 
       res = results.split("@");
       var targetElement = document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'];
			targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
 					console.log(res);
					if(res[i] == ''){
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].options[i].text = '';
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].options[i].value = stateVal[0];
					document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].options[i].text = stateVal[1];
					}
					}
 				document.forms['defaultAccountLineForm'].elements['defaultAccountLine.estExpVatDescr'].value = '${defaultAccountLine.estExpVatDescr}';
 				findDefaultVatFromPartnerForVendor();
     }
}
</script>	
	
</head>
<div id="Layer5" style="width:100%">
<div id="newmnav">
  <ul>
   <li id="newmnav1" style="background:#FFF"><a class="current""><span>Default Account Line Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	<li><a href="defaultAccountLines.html"><span>Default Account Line List</span></a></li>
	<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${defaultAccountLine.id}&tableName=defaultaccountline&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
</ul></div><div class="spn">&nbsp;</div>
 </div>

<s:form id="defaultAccountLineForm" action="saveDefaultAccountLine" method="post" onsubmit="getVatPercentEst();" validate="true">
<s:hidden name="chargeCodeValidationMessage" id="chargeCodeValidationMessage"/>
<s:hidden name="chargeCodeValidationVal" id="chargeCodeValidationVal"/> 
<s:hidden name="defaultAccountLine.id" value="%{defaultAccountLine.id}"/>
<s:hidden name="defaultAccountLine.corpID" /> 
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="firstDescription" />
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/> 
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.defaultAccountLine' }">
   <c:redirect url="/defaultAccountLines.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<div id="Layer1"  onkeydown="changeStatus();" style="width:100%"> 

<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;margin:0px;">
	<tbody>
		<tr>
			<td>
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-20px; "><span></span></div>
   <div class="center-content">
    <fieldset >
   <legend>Template Control Fields</legend>
				<table cellspacing="0" cellpadding="1" border="0">
					<tbody>
					
						<tr>
						    <td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.jobType"/><font color="red" size="2">*</font></td>
							<td width="15px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.contract" /><font color="red" size="2">*</font></td>
							<td width="15px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.mode"/></td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left"><s:select id="jobType1" cssClass="list-menu" name="defaultAccountLine.jobType" list="%{job}" headerKey="" headerValue="" cssStyle="width:260px" onchange="getContract();getCommodity();" tabindex=""/></td>
							<td width="10px"></td>
							<td align="left"><s:select cssClass="list-menu"   name="defaultAccountLine.contract" list="%{contracts}" headerKey="" headerValue=""  onchange="changeStatus();valueChk();" cssStyle="width:260px" tabindex=""/></td>
							<td width="10px"></td>
							<td align="left"><s:select cssClass="list-menu" name="defaultAccountLine.mode" list="%{mode}" cssStyle="width:205px" tabindex=""/></td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.billToCode"/></td>
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.billToName"/></td>
							<td width="15px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.route"/></td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left"><s:textfield cssClass="input-text"  name="defaultAccountLine.billToCode" cssStyle="width:100px" maxlength="20" onchange = "findBillToName();getContract();defaultAccountRecVATDesc();" tabindex=""/><img class="openpopup" width="17" height="20" align="top" onclick="openPopWindow();document.forms['defaultAccountLineForm'].elements['defaultAccountLine.billToName'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td width="10px"></td>
							<td align="left"><s:textfield cssClass="input-text"  name="defaultAccountLine.billToName" cssStyle="width:258px" maxlength="200" readonly="true" tabindex=""/></td>
						    <td width="10px"></td>
							<td align="left"><s:select cssClass="list-menu" name="defaultAccountLine.route" list="%{routing}" cssStyle="width:205px" tabindex=""/></td>
						</tr>
							<tr>
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.packMode"/></td>
							<td width="10px" colspan=""></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.commodity"/></td>
							<td width="10px"></td>
							<td align="left" class="listwhitetext" colspan="3"><fmt:message key="defaultAccountLine.serviceType"/></td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left"><s:select cssClass="list-menu" name="defaultAccountLine.packMode" list="%{pkmode}" cssStyle="width:260px" tabindex=""/></td>
							<td width="10px"></td>
							<c:choose>
								<c:when test="${defaultAccountLine.jobType =='STO' || defaultAccountLine.jobType =='STL' ||defaultAccountLine.jobType =='STF' ||defaultAccountLine.jobType =='TPS'}">
									<td align="left" ><s:select cssClass="list-menu" name="defaultAccountLine.commodity" list="%{commodits}" cssStyle="width:260px" tabindex=""/></td>
								 </c:when>
								 <c:otherwise>
									<td align="left" ><s:select cssClass="list-menu" name="defaultAccountLine.commodity" list="%{commodit}" cssStyle="width:260px" tabindex=""/></td>		
								 </c:otherwise>
							</c:choose>
							<td width="10px"></td>
							<td align="left" colspan="3"><s:select cssClass="list-menu" name="defaultAccountLine.serviceType" list="%{service}" cssStyle="width:205px" tabindex=""/></td> 
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.companyDivision"/><span style="padding-left:43px;">Equipment</span></td>
							<td width="10px"></td>
							<td align="left" class="listwhitetext">Origin&nbsp;Country</td>
							<td width="10px"></td>
							<td align="left" class="listwhitetext">Destination&nbsp;Country</td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left"><s:select cssClass="list-menu" name="defaultAccountLine.companyDivision" list="%{comDivis}" cssStyle="width:100px" headerKey="" headerValue="" onchange="resetChargeCode();defaultAccountRecVATDesc(); defaultAccountPayVatWithVatGroup();" tabindex=""/>
							<span style="padding-left:25px;"><s:select cssClass="list-menu" name="defaultAccountLine.equipment" list="%{EQUIP}" cssStyle="width:130px" onchange="changeStatus();" headerKey="" headerValue="" tabindex="" /></span>
							</td>
							<td width="10px"></td>
							<td><s:select cssClass="list-menu" name="defaultAccountLine.originCountry" list="%{ocountry}" cssStyle="width:260px" onchange="changeStatus();" headerKey="" headerValue="" tabindex="" /></td>
							<td width="10px"></td>
							<td><s:select cssClass="list-menu" name="defaultAccountLine.destinationCountry" list="%{dcountry}" cssStyle="width:205px" onchange="changeStatus();" headerKey="" headerValue="" tabindex="" /></td>					
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left" class="listwhitetext">Origin&nbsp;City</td>
							<td width="10px"></td>
							<td align="left" class="listwhitetext">Destination&nbsp;City</td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td><s:textfield cssClass="input-text" name="defaultAccountLine.originCity" maxlength="100" cssStyle="width:255px" onchange="changeStatus();" /></td>
							<td width="10px"></td>
							<td><s:textfield cssClass="input-text" name="defaultAccountLine.destinationCity" maxlength="100" cssStyle="width:255px" onchange="changeStatus();" /></td>					
						</tr>
						</table>
						</fieldset>
						</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

	<tr>
			<td>
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-20px; "><span></span></div>
   <div class="center-content">
    <fieldset >
   <legend>Line Output Fields</legend>
				<table cellspacing="0" cellpadding="1" border="0">
					<tbody>  
					<tr>
					<td width="10px"></td>
					<td align="left" class="listwhitetext" colspan="4"><fmt:message key="defaultAccountLine.orderNumber" /></td>
					</tr>
					<tr>
					<td width="10px"></td>
					 <td align="left"><s:textfield cssClass="input-text" name="defaultAccountLine.orderNumber" cssStyle="width:130px;text-align:right" maxlength="3" onkeydown="return onlyNumsAllowed(event)" tabindex=""/></td>
					</tr>	
						<tr>
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.categories"/></td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left"><s:select cssClass="list-menu" name="defaultAccountLine.categories" list="%{category}" cssStyle="width:132px" headerKey="" headerValue="" onchange="disableAmountRevenue();resetChargeCode();" tabindex=""/></td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.basis" /></td> 
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.vendorCode"/></td>
							
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.vendorName"/></td>
						 </tr>
						<tr>
							<td width="10px"></td>
							<td align="left"><s:select cssClass="list-menu"  name="defaultAccountLine.basis" list="%{basis}" headerKey="" headerValue=""  onchange="expenseOld();" cssStyle="width:132px" tabindex=""/></td>
							<c:if test="${accountInterface!='Y'}">
								<td width="10px"></td>
								<td align="left" width="125" ><s:textfield cssClass="input-text"  name="defaultAccountLine.vendorCode" cssStyle="width:130px" maxlength="20" onchange= "checkVendorName();defaultAccountPayVatWithVatGroup();" tabindex=""/><img align="top" class="openpopup" width="17" height="20" onclick="javascript:openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=defaultAccountLine.vendorName&fld_code=defaultAccountLine.vendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
							
								<td align="left" colspan="4"><s:textfield cssClass="input-text"  name="defaultAccountLine.vendorName" cssStyle="width:208px" maxlength="200" readonly="true" tabindex=""/></td>
							</c:if>
							<c:if test="${accountInterface=='Y'}">
								<td width="10px"></td>
								<td align="left"  width="125"><s:textfield cssClass="input-text"  name="defaultAccountLine.vendorCode" cssStyle="width:80px" maxlength="20" onchange = "checkVendorName();defaultAccountPayVatWithVatGroup();" tabindex=""/><img class="openpopup" width="17" height="20" align="top" onclick="javascript:openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=defaultAccountLine.vendorName&fld_code=defaultAccountLine.vendorCode');" src="<c:url value='/images/open-popup.gif'/>" /></td>
								
								<td align="left" colspan="4"><s:textfield cssClass="input-text"  name="defaultAccountLine.vendorName" cssStyle="width:186px" maxlength="200" readonly="true" tabindex=""/></td>
							</c:if>
						</tr>
						
						<tr>
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.chargeCode" /></td>
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.recGl" /></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.payGl" /></td>
						   
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left"><s:textfield cssClass="input-text" name="defaultAccountLine.chargeCode" id="chargeCode"  cssStyle="width:130px" maxlength="25" onblur="checkChargeCode();" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivId')" tabindex=""/><img class="openpopup" width="17" height="20" align="top" onclick="chk();"  src="<c:url value='/images/open-popup.gif'/>" />
							<div id="ChargeNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
							</td>
							<td width="10px"></td>
							<td align="left"><s:textfield cssClass="input-text" name="defaultAccountLine.recGl" cssStyle="width:80px; text-align:right" maxlength="20" readonly="true" tabindex="" onselect="enablePreviousFunction('checkValue');"/></td>
							<td align="left"><s:textfield cssClass="input-text" name="defaultAccountLine.payGl" cssStyle="width:187px;text-align:right" maxlength="20" readonly="true" tabindex=""/></td>
						   
						</tr>
						
						<tr>
							<td width="10px"></td>
							<td align="left" class="listwhitetext"><fmt:message key="defaultAccountLine.quantity"/></td>
						</tr>
						<tr>
							<td width="10px"></td>
							<td align="left"><s:textfield cssClass="input-text"  name="defaultAccountLine.quantity" cssStyle="width:130px;text-align:right" maxlength="20" onkeydown="return onlyFloatNumsAllowed(event)"   onchange="expenseOld();getVatPercentEst();" tabindex=""/></td>
						</tr>
						<tr>
						  <td ></td>
						 	<c:if test="${systemDefaultVatCalculation=='true'}">				
						    <td class="listwhitetext">Rec&nbsp;VAT&nbsp;Desc&nbsp;</td>
						    <td width="5px"></td>
						    <td width="" align="left" class="listwhitetext" >VAT %&nbsp;</td>	
						    </c:if>	
						  	<td align="left" class="listwhitetext"><fmt:message key="accountLine.estimateSellRate"/></td>
						    <td width="" align="left" class="listwhitetext">Est. Revenue</td>
						    <td align="left" class="listwhitetext" colspan="3">Mk%</td>						   			   						    
						</tr>
						<tr>						
						<td width="10px"></td>
						<c:if test="${systemDefaultVatCalculation=='true'}">					
						<td><s:select  cssClass="list-menu" name="defaultAccountLine.estVatDescr" cssStyle="width:100px" list="%{euVatList}" onchange="getVatPercentEst();changeStatus();checkVatForReadonlyFieldsEst('first');" headerKey="" headerValue=""  tabindex=""/>
						</td>
						<td width="10px"></td>
						<td><s:textfield cssClass="input-text" cssStyle="text-align:right" name="defaultAccountLine.estVatPercent" size="10" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="return checkFloatEstVat(this);getVatPercentEst();" maxLength="6" tabindex=""/></td>						
						</c:if>
						<td align="left"><s:textfield cssClass="input-text"  name="defaultAccountLine.sellRate" cssStyle="width:130px;text-align:right;margin-right:10px;" maxlength="15"  onkeydown="return onlyFloatNumsAllowed(event)"  onchange="expenseOld();getVatPercentEst();" tabindex=""/></td>
						 <td align="left"><s:textfield cssClass="input-text"  name="defaultAccountLine.estimatedRevenue" cssStyle="width:80px;text-align:right;margin-right:10px;"  onkeydown="return onlyFloatNumsAllowed(event)" onchange="mockupForRevenue();getVatPercentEst();" maxlength="15" tabindex=""/></td>
						<td align="left" colspan="3"><s:textfield cssClass="input-text"  name="defaultAccountLine.markUp" cssStyle="width:80px;text-align:right" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateRevenue();getVatPercentEst();" tabindex=""/></td>
						</tr>
						<!--#9962 Added Pay Vat Code -->
							<tr>
							<td width="10px"></td>
							<c:if test="${systemDefaultVatCalculation=='true'}">
						    <td class="listwhitetext">Pay&nbsp;VAT&nbsp;Desc&nbsp;</td>
						    <td width="5px"></td>
						    <td width="" align="left" class="listwhitetext" >VAT %&nbsp;</td>
						  </c:if>
							<td align="left" class="listwhitetext"><fmt:message key="accountLine.estimateRate"/></td>
							<td align="left" class="listwhitetext">Est. Expense</td>
						  </tr>
						<tr>
						<td width="10px"></td>	
						<c:if test="${systemDefaultVatCalculation=='true'}">					
						<td><s:select  cssClass="list-menu" name="defaultAccountLine.estExpVatDescr" cssStyle="width:100px" list="%{payVatList}" onchange="getExpVatPercentEst();changeStatus();" headerKey="" headerValue=""  tabindex=""/>
						</td>
						<td width="10px"></td>
						<td><s:textfield cssClass="input-text" cssStyle="text-align:right" name="defaultAccountLine.estExpVatPercent" size="10" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="return checkFloatEstVat(this);changeStatus();" maxLength="6" tabindex=""/></td>						
						</c:if>
						<td align="left"><s:textfield cssClass="input-text"  name="defaultAccountLine.rate" cssStyle="width:130px;text-align:right" maxlength="20"  onkeydown="return onlyFloatNumsAllowed(event)"  onchange="expenseOld();" tabindex=""/></td>
					   <td align="left"><s:textfield cssClass="input-text"  name="defaultAccountLine.amount" cssStyle="width:80px;text-align:right" maxlength="20"  onkeydown="return onlyFloatNumsAllowed(event)"  onchange="expenseOld();" tabindex=""/></td>
						</tr>
						<!--End Added Pay Vat Code -->
						
						<tr>						
						<td align="left" class="listwhitetext"><s:hidden name="defaultAccountLine.estVatAmt"  /></td> 
						</tr>
						<tr> 
						  <td width="10px"></td>
						   <td align="left" class="listwhitetext" colspan="6"><fmt:message key="defaultAccountLine.description" /></td>
						 </tr>
						 <tr>
						   <td width="10px"></td>
						   <td align="left" colspan="6" class="listwhitetext"><s:textarea readonly="" name="defaultAccountLine.description" rows="1" cols="87"  cssClass="textarea" tabindex=""/></td>
 						</tr>
						
					</tbody>
				</table>
				</fieldset>
				</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
			</td>
		</tr>
	</tbody>
</table>




<table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='defaultAccountLine.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td >
							<fmt:formatDate var="defaultAccountLineCreatedOnFormattedValue" value="${defaultAccountLine.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="defaultAccountLine.createdOn" value="${defaultAccountLineCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${defaultAccountLine.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='defaultAccountLine.createdBy' /></b></td>
							<c:if test="${not empty defaultAccountLine.id}">
								<s:hidden name="defaultAccountLine.createdBy"/>
								<td ><s:label name="createdBy" value="%{defaultAccountLine.createdBy}"/></td>
							</c:if>
							<c:if test="${empty defaultAccountLine.id}">
								<s:hidden name="defaultAccountLine.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='defaultAccountLine.updatedOn'/></b></td>
							<fmt:formatDate var="defaultAccountLineupdatedOnFormattedValue" value="${defaultAccountLine.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="defaultAccountLine.updatedOn" value="${defaultAccountLineupdatedOnFormattedValue}"/>
							<td ><fmt:formatDate value="${defaultAccountLine.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='defaultAccountLine.updatedBy' /></b></td>
							<c:if test="${not empty defaultAccountLine.id}">
								<s:hidden name="defaultAccountLine.updatedBy"/>
								<td ><s:label name="updatedBy" value="%{defaultAccountLine.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty defaultAccountLine.id}">
								<s:hidden name="defaultAccountLine.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
</div>

        
         <s:submit cssClass="cssbutton" method="save" key="button.save" onclick="return checkJobType('jobRequired');" cssStyle="width:60px; height:25px" tabindex=""/>
        
       <c:if test="${not empty defaultAccountLine.id}">  
     <input type="button" class="cssbutton" onclick="location.href='<c:url value="/editDefaultAccountLine.html"/>'"  
        value="<fmt:message key="button.add"/>" style="width:60px; height:25px" tabindex=""/>  
	 </c:if>
         <%-- <a href="editNewDefaultAccountLine.html?id=${claim.id}"><s:submit cssClass="button" method="add" key="button.add" cssStyle="width:87px; height:30px"/>   --%>
        <s:reset cssClass="cssbutton" key="Reset"  onclick="resetContract();"  cssStyle="width:60px; height:25px" tabindex=""/>   


</s:form>

<script type="text/javascript">   
	var f = document.getElementById('defaultAccountLineForm'); 
	f.setAttribute("autocomplete", "off");
    //Form.focusFirstElement($("defaultAccountLineForm"));
    getContract();
    disableAmountRevenue();
</script>  	
<script type="text/javascript">
try{  	
		document.getElementById('jobType1').focus();
	}catch(e){}
</script>
