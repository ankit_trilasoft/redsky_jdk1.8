delimiter $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `published_contract`(l_contract varchar(500), fromcorpid varchar(4), tocorpid varchar(4),l_createdby varchar(100))
BEGIN
declare msg varchar(50);
DECLARE bDone INT;
DECLARE l_recGL VARCHAR(16);
DECLARE l_paygl VARCHAR(16);
DECLARE l_costelement VARCHAR(50);
DECLARE l_description VARCHAR(50);
set msg='Published Contract Successfully!!!';

delete from contract where contract =l_contract and corpid=tocorpid;
DROP TABLE IF EXISTS contract_temp;
create table contract_temp (select * from contract where contract =l_contract and corpid=fromcorpid);
ALTER TABLE `redsky`.`contract_temp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update contract_temp set id=null,companyDivision='',corpid=tocorpid,createdby=l_createdby,createdon=now(),updatedby=l_createdby,updatedon=now();
insert into contract select * from contract_temp;
update contract set published='Y' where contract =l_contract and corpid=tocorpid;
drop table contract_temp;

delete from charges where contract =l_contract and corpid=tocorpid and status=true;
DROP TABLE IF EXISTS charges_temp;
create table charges_temp (select * from charges where contract =l_contract and corpid=fromcorpid and status=true);
ALTER TABLE `redsky`.`charges_temp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update charges_temp set id=null,corpid=tocorpid,createdby=l_createdby,createdon=now(),updatedby=l_createdby,updatedon=now();
insert into charges select * from charges_temp;
drop table charges_temp;

delete from rategrid where contractname =l_contract and corpid=tocorpid ;
DROP TABLE IF EXISTS rategrid_temp;
create table rategrid_temp (select * from rategrid where contractname =l_contract and corpid=fromcorpid);
ALTER TABLE `redsky`.`rategrid_temp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update rategrid_temp set id=null,corpid=tocorpid,createdby=l_createdby,createdon=now(),updatedby=l_createdby,updatedon=now();
insert into rategrid select * from rategrid_temp;
drop table rategrid_temp;


delete from countrydeviation where contractname =l_contract and corpid=tocorpid ;
DROP TABLE IF EXISTS countrydeviation_temp;
create table countrydeviation_temp (select * from countrydeviation where contractname =l_contract and corpid=fromcorpid);
ALTER TABLE `redsky`.`countrydeviation_temp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update countrydeviation_temp set id=null,corpid=tocorpid,createdby=l_createdby,createdon=now(),updatedby=l_createdby,updatedon=now();
insert into countrydeviation select * from countrydeviation_temp;
drop table countrydeviation_temp;


delete from contractaccount where contract =l_contract and corpid=tocorpid ;
DROP TABLE IF EXISTS contractaccount_temp;
create table contractaccount_temp (select * from contractaccount where contract =l_contract and corpid=fromcorpid);
ALTER TABLE `redsky`.`contractaccount_temp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update contractaccount_temp set id=null,corpid=tocorpid,createdby=l_createdby,createdon=now(),updatedby=l_createdby,updatedon=now();
insert into contractaccount select * from contractaccount_temp;
drop table contractaccount_temp;
update contractaccount set published='Y' where contract =l_contract and corpid=fromcorpid and agentcorpid=tocorpid and accountType='Agent';

call published_costelement(l_contract, fromcorpid, tocorpid ,l_createdby);

call published_rategrid(l_contract, fromcorpid, tocorpid);

call published_countrydeviation(l_contract, fromcorpid, tocorpid);

select msg;

END$$
