package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="mss")
public class Mss extends BaseObject{
	private Long id;
	private String corpId;
	private String soNumber;
	private String workTicketNumber;
	private String workTicketService;
	private String submitAs;
	private String billingDivision;
	private String affiliatedTo;
	private Boolean urgent = new Boolean(false);
	private String transportType;
	private String transportTypeRadio;
	private Date requestedOriginDate;
	private Date requestedDestinationDate;
	private Boolean serviceOverMultipleDays=new Boolean(false);
	private int weight;
	private Date loadingStartDate;
	private Date loadingEndDate;
	private Date packingStartDate;
	private Date packingEndDate;
	private Date deliveryStartDate;
	private Date deliveryEndDate;
	private String mssOrderNumber;
	private Boolean vip=new Boolean(false);
	private String originServiceCategory;
	private String originServices;	
	private String destinationServiceCtegory;
	private String destinationServices;	
	private String purchaseOrderNumber;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private Long workTicketId;	
	private String shipperFirstName;
	private String shipperLastName;
	private String shipperEmailAddress;
	private String shipperOriginAddress1;
	private String shipperOriginAddress2;
	private String shipperOriginCity;
	private String shipperOriginState;
	private String shipperOriginZip;
	private String shipperDestinationAddress1;
	private String shipperDestinationAddress2;
	private String shipperDestinationCity;
	private String shipperDestinationState;
	private String shipperDestinationZip;
	private String mssOrderOriginPhoneNumbers;
	private String mssOrderDestinationPhoneNumbers;
	private String location;
	private String companyDivision;
	
	
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Mss))
			return false;
		Mss castOther = (Mss) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(soNumber, castOther.soNumber)
				.append(workTicketNumber, castOther.workTicketNumber)
				.append(workTicketService, castOther.workTicketService)
				.append(submitAs, castOther.submitAs)
				.append(billingDivision, castOther.billingDivision)
				.append(affiliatedTo, castOther.affiliatedTo)
				.append(urgent, castOther.urgent)
				.append(transportType, castOther.transportType)
				.append(transportTypeRadio, castOther.transportTypeRadio)
				.append(requestedOriginDate, castOther.requestedOriginDate)
				.append(requestedDestinationDate,
						castOther.requestedDestinationDate)
				.append(serviceOverMultipleDays,
						castOther.serviceOverMultipleDays)
				.append(weight, castOther.weight)
				.append(loadingStartDate, castOther.loadingStartDate)
				.append(loadingEndDate, castOther.loadingEndDate)
				.append(packingStartDate, castOther.packingStartDate)
				.append(packingEndDate, castOther.packingEndDate)
				.append(deliveryStartDate, castOther.deliveryStartDate)
				.append(deliveryEndDate, castOther.deliveryEndDate)
				.append(mssOrderNumber, castOther.mssOrderNumber)
				.append(vip, castOther.vip)
				.append(originServiceCategory, castOther.originServiceCategory)
				.append(originServices, castOther.originServices)
				.append(destinationServiceCtegory,
						castOther.destinationServiceCtegory)
				.append(destinationServices, castOther.destinationServices)
				.append(purchaseOrderNumber, castOther.purchaseOrderNumber)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(workTicketId, castOther.workTicketId)
				.append(shipperFirstName, castOther.shipperFirstName)
				.append(shipperLastName, castOther.shipperLastName)
				.append(shipperEmailAddress, castOther.shipperEmailAddress)
				.append(shipperOriginAddress1, castOther.shipperOriginAddress1)
				.append(shipperOriginAddress2, castOther.shipperOriginAddress2)
				.append(shipperOriginCity, castOther.shipperOriginCity)
				.append(shipperOriginState, castOther.shipperOriginState)
				.append(shipperOriginZip, castOther.shipperOriginZip)
				.append(shipperDestinationAddress1,
						castOther.shipperDestinationAddress1)
				.append(shipperDestinationAddress2,
						castOther.shipperDestinationAddress2)
				.append(shipperDestinationCity,
						castOther.shipperDestinationCity)
				.append(shipperDestinationState,
						castOther.shipperDestinationState)
				.append(shipperDestinationZip, castOther.shipperDestinationZip)
				.append(mssOrderOriginPhoneNumbers,
						castOther.mssOrderOriginPhoneNumbers)
				.append(mssOrderDestinationPhoneNumbers,
						castOther.mssOrderDestinationPhoneNumbers)
				.append(location, castOther.location)
				.append(companyDivision, castOther.companyDivision)
				.isEquals();
	}


	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId).append(soNumber)
				.append(workTicketNumber).append(workTicketService)
				.append(submitAs).append(billingDivision).append(affiliatedTo)
				.append(urgent).append(transportType)
				.append(transportTypeRadio).append(requestedOriginDate)
				.append(requestedDestinationDate)
				.append(serviceOverMultipleDays).append(weight)
				.append(loadingStartDate).append(loadingEndDate)
				.append(packingStartDate).append(packingEndDate)
				.append(deliveryStartDate).append(deliveryEndDate)
				.append(mssOrderNumber).append(vip)
				.append(originServiceCategory).append(originServices)
				.append(destinationServiceCtegory).append(destinationServices)
				.append(purchaseOrderNumber).append(createdOn)
				.append(createdBy).append(updatedOn).append(updatedBy)
				.append(workTicketId).append(shipperFirstName)
				.append(shipperLastName).append(shipperEmailAddress)
				.append(shipperOriginAddress1).append(shipperOriginAddress2)
				.append(shipperOriginCity).append(shipperOriginState)
				.append(shipperOriginZip).append(shipperDestinationAddress1)
				.append(shipperDestinationAddress2)
				.append(shipperDestinationCity).append(shipperDestinationState)
				.append(shipperDestinationZip)
				.append(mssOrderOriginPhoneNumbers)
				.append(mssOrderDestinationPhoneNumbers).append(location).append(companyDivision)
				.toHashCode();
	}


	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpId", corpId)
				.append("soNumber", soNumber)
				.append("workTicketNumber", workTicketNumber)
				.append("workTicketService", workTicketService)
				.append("submitAs", submitAs)
				.append("billingDivision", billingDivision)
				.append("affiliatedTo", affiliatedTo)
				.append("urgent", urgent)
				.append("transportType", transportType)
				.append("transportTypeRadio", transportTypeRadio)
				.append("requestedOriginDate", requestedOriginDate)
				.append("requestedDestinationDate", requestedDestinationDate)
				.append("serviceOverMultipleDays", serviceOverMultipleDays)
				.append("weight", weight)
				.append("loadingStartDate", loadingStartDate)
				.append("loadingEndDate", loadingEndDate)
				.append("packingStartDate", packingStartDate)
				.append("packingEndDate", packingEndDate)
				.append("deliveryStartDate", deliveryStartDate)
				.append("deliveryEndDate", deliveryEndDate)
				.append("mssOrderNumber", mssOrderNumber)
				.append("vip", vip)
				.append("originServiceCategory", originServiceCategory)
				.append("originServices", originServices)
				.append("destinationServiceCtegory", destinationServiceCtegory)
				.append("destinationServices", destinationServices)
				.append("purchaseOrderNumber", purchaseOrderNumber)
				.append("createdOn", createdOn)
				.append("createdBy", createdBy)
				.append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy)
				.append("workTicketId", workTicketId)
				.append("shipperFirstName", shipperFirstName)
				.append("shipperLastName", shipperLastName)
				.append("shipperEmailAddress", shipperEmailAddress)
				.append("shipperOriginAddress1", shipperOriginAddress1)
				.append("shipperOriginAddress2", shipperOriginAddress2)
				.append("shipperOriginCity", shipperOriginCity)
				.append("shipperOriginState", shipperOriginState)
				.append("shipperOriginZip", shipperOriginZip)
				.append("shipperDestinationAddress1",
						shipperDestinationAddress1)
				.append("shipperDestinationAddress2",
						shipperDestinationAddress2)
				.append("shipperDestinationCity", shipperDestinationCity)
				.append("shipperDestinationState", shipperDestinationState)
				.append("shipperDestinationZip", shipperDestinationZip)
				.append("mssOrderOriginPhoneNumbers",
						mssOrderOriginPhoneNumbers)
				.append("mssOrderDestinationPhoneNumbers",
						mssOrderDestinationPhoneNumbers)
				.append("location", location)
				.append("companyDivision", companyDivision)
				.toString();
	}


	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	@Column
	public String getCorpId() {
		return corpId;
	}


	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	@Column
	public String getSoNumber() {
		return soNumber;
	}


	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}

	@Column
	public String getWorkTicketNumber() {
		return workTicketNumber;
	}


	public void setWorkTicketNumber(String workTicketNumber) {
		this.workTicketNumber = workTicketNumber;
	}

	@Column
	public String getWorkTicketService() {
		return workTicketService;
	}


	public void setWorkTicketService(String workTicketService) {
		this.workTicketService = workTicketService;
	}

	@Column
	public String getSubmitAs() {
		return submitAs;
	}


	public void setSubmitAs(String submitAs) {
		this.submitAs = submitAs;
	}
	@Column
	public Boolean getUrgent() {
		return urgent;
	}


	public void setUrgent(Boolean urgent) {
		this.urgent = urgent;
	}	
	
	@Column
	public Date getRequestedOriginDate() {
		return requestedOriginDate;
	}


	public void setRequestedOriginDate(Date requestedOriginDate) {
		this.requestedOriginDate = requestedOriginDate;
	}

	@Column
	public Date getRequestedDestinationDate() {
		return requestedDestinationDate;
	}


	public void setRequestedDestinationDate(Date requestedDestinationDate) {
		this.requestedDestinationDate = requestedDestinationDate;
	}

	@Column
	public Boolean getServiceOverMultipleDays() {
		return serviceOverMultipleDays;
	}


	public void setServiceOverMultipleDays(Boolean serviceOverMultipleDays) {
		this.serviceOverMultipleDays = serviceOverMultipleDays;
	}

	@Column
	public Date getLoadingStartDate() {
		return loadingStartDate;
	}


	public void setLoadingStartDate(Date loadingStartDate) {
		this.loadingStartDate = loadingStartDate;
	}

	@Column
	public Date getLoadingEndDate() {
		return loadingEndDate;
	}


	public void setLoadingEndDate(Date loadingEndDate) {
		this.loadingEndDate = loadingEndDate;
	}

	@Column
	public Date getPackingStartDate() {
		return packingStartDate;
	}


	public void setPackingStartDate(Date packingStartDate) {
		this.packingStartDate = packingStartDate;
	}

	@Column
	public Date getPackingEndDate() {
		return packingEndDate;
	}


	public void setPackingEndDate(Date packingEndDate) {
		this.packingEndDate = packingEndDate;
	}

	@Column
	public Date getDeliveryStartDate() {
		return deliveryStartDate;
	}


	public void setDeliveryStartDate(Date deliveryStartDate) {
		this.deliveryStartDate = deliveryStartDate;
	}
	@Column
	public Date getDeliveryEndDate() {
		return deliveryEndDate;
	}


	public void setDeliveryEndDate(Date deliveryEndDate) {
		this.deliveryEndDate = deliveryEndDate;
	}

	@Column
	public String getMssOrderNumber() {
		return mssOrderNumber;
	}


	public void setMssOrderNumber(String mssOrderNumber) {
		this.mssOrderNumber = mssOrderNumber;
	}
	@Column
	public String getOriginServiceCategory() {
		return originServiceCategory;
	}


	public void setOriginServiceCategory(String originServiceCategory) {
		this.originServiceCategory = originServiceCategory;
	}

	@Column
	public String getOriginServices() {
		return originServices;
	}


	public void setOriginServices(String originServices) {
		this.originServices = originServices;
	}

	@Column
	public String getDestinationServiceCtegory() {
		return destinationServiceCtegory;
	}


	public void setDestinationServiceCtegory(String destinationServiceCtegory) {
		this.destinationServiceCtegory = destinationServiceCtegory;
	}
	@Column
	public String getDestinationServices() {
		return destinationServices;
	}


	public void setDestinationServices(String destinationServices) {
		this.destinationServices = destinationServices;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}


	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Long getWorkTicketId() {
		return workTicketId;
	}


	public void setWorkTicketId(Long workTicketId) {
		this.workTicketId = workTicketId;
	}
	
	@Column
	public String getTransportType() {
		return transportType;
	}


	public void setTransportType(String transportType) {
		this.transportType = transportType;
	}

	@Column
	public String getTransportTypeRadio() {
		return transportTypeRadio;
	}

	public void setTransportTypeRadio(String transportTypeRadio) {
		this.transportTypeRadio = transportTypeRadio;
	}	
	@Column
	public Boolean getVip() {
		return vip;
	}


	public void setVip(Boolean vip) {
		this.vip = vip;
	}

	@Column
	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}


	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	@Column
	public String getShipperFirstName() {
		return shipperFirstName;
	}


	public void setShipperFirstName(String shipperFirstName) {
		this.shipperFirstName = shipperFirstName;
	}

	@Column
	public String getShipperLastName() {
		return shipperLastName;
	}


	public void setShipperLastName(String shipperLastName) {
		this.shipperLastName = shipperLastName;
	}

	@Column
	public String getShipperEmailAddress() {
		return shipperEmailAddress;
	}


	public void setShipperEmailAddress(String shipperEmailAddress) {
		this.shipperEmailAddress = shipperEmailAddress;
	}

	@Column
	public String getShipperOriginAddress1() {
		return shipperOriginAddress1;
	}


	public void setShipperOriginAddress1(String shipperOriginAddress1) {
		this.shipperOriginAddress1 = shipperOriginAddress1;
	}

	@Column
	public String getShipperOriginAddress2() {
		return shipperOriginAddress2;
	}


	public void setShipperOriginAddress2(String shipperOriginAddress2) {
		this.shipperOriginAddress2 = shipperOriginAddress2;
	}

	@Column
	public String getShipperOriginCity() {
		return shipperOriginCity;
	}


	public void setShipperOriginCity(String shipperOriginCity) {
		this.shipperOriginCity = shipperOriginCity;
	}

	@Column
	public String getShipperOriginState() {
		return shipperOriginState;
	}


	public void setShipperOriginState(String shipperOriginState) {
		this.shipperOriginState = shipperOriginState;
	}

	@Column
	public String getShipperOriginZip() {
		return shipperOriginZip;
	}


	public void setShipperOriginZip(String shipperOriginZip) {
		this.shipperOriginZip = shipperOriginZip;
	}

	@Column
	public String getShipperDestinationAddress1() {
		return shipperDestinationAddress1;
	}


	public void setShipperDestinationAddress1(String shipperDestinationAddress1) {
		this.shipperDestinationAddress1 = shipperDestinationAddress1;
	}

	@Column
	public String getShipperDestinationAddress2() {
		return shipperDestinationAddress2;
	}


	public void setShipperDestinationAddress2(String shipperDestinationAddress2) {
		this.shipperDestinationAddress2 = shipperDestinationAddress2;
	}
	@Column

	public String getShipperDestinationCity() {
		return shipperDestinationCity;
	}


	public void setShipperDestinationCity(String shipperDestinationCity) {
		this.shipperDestinationCity = shipperDestinationCity;
	}
	@Column

	public String getShipperDestinationState() {
		return shipperDestinationState;
	}


	public void setShipperDestinationState(String shipperDestinationState) {
		this.shipperDestinationState = shipperDestinationState;
	}

	@Column
	public String getShipperDestinationZip() {
		return shipperDestinationZip;
	}


	public void setShipperDestinationZip(String shipperDestinationZip) {
		this.shipperDestinationZip = shipperDestinationZip;
	}

	@Column
	public String getMssOrderOriginPhoneNumbers() {
		return mssOrderOriginPhoneNumbers;
	}


	public void setMssOrderOriginPhoneNumbers(String mssOrderOriginPhoneNumbers) {
		this.mssOrderOriginPhoneNumbers = mssOrderOriginPhoneNumbers;
	}
	@Column
	public String getMssOrderDestinationPhoneNumbers() {
		return mssOrderDestinationPhoneNumbers;
	}


	public void setMssOrderDestinationPhoneNumbers(
			String mssOrderDestinationPhoneNumbers) {
		this.mssOrderDestinationPhoneNumbers = mssOrderDestinationPhoneNumbers;
	}

	@Column
	public String getBillingDivision() {
		return billingDivision;
	}


	public void setBillingDivision(String billingDivision) {
		this.billingDivision = billingDivision;
	}

	@Column
	public String getAffiliatedTo() {
		return affiliatedTo;
	}


	public void setAffiliatedTo(String affiliatedTo) {
		this.affiliatedTo = affiliatedTo;
	}

	@Column
	public int getWeight() {
		return weight;
	}


	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Column
	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}

	@Column
	public String getCompanyDivision() {
		return companyDivision;
	}


	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

}
